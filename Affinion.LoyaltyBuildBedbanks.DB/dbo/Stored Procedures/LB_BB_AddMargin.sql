
/****** Object:  StoredProcedure [dbo].[LB_BB_AddMargin]    Script Date: 7/19/2016 7:21:17 PM ******/
CREATE PROCEDURE [dbo].[LB_BB_AddMargin](@providerId varchar(max), @marginPercentage decimal, @startDate date, @endDate date, @marginId int=null, @isactive int=null, @isglobal int=null, @ErrorListID int=0, @exists int OUTPUT)
as
begin
-- To Check if it is a global variable
 if @marginId>0 AND @isglobal=1
 begin
 		update LB_BB_MarginDetails set MarginPercentage=@marginPercentage,  DateModified=getdate() where MarginId=@marginId
 end

--To check if it is a edit operation
	if @marginId>0   
	begin
	--To check if margin Id is matching with same start and end dates
		if exists (select marginPercentage from LB_BB_MarginDetails where StartDate=@startDate and EndDate=@endDate and MarginID=@marginId)
		begin
		--update the margin
		update LB_BB_MarginDetails set StartDate=@startDate, EndDate=@endDate, MarginPercentage=@marginPercentage, DateModified=getdate(), IsActive=@isactive where MarginId=@marginId and ProviderID=@providerId
		set @exists=0
		end
		--To check whether the dates are overlapping
		else if exists (select marginPercentage from LB_BB_MarginDetails where ProviderID=@providerId AND ((@startDate BETWEEN StartDate AND  EndDate) OR (@endDate BETWEEN StartDate AND EndDate)))
		begin
		if exists(select marginPercentage from LB_BB_MarginDetails where ProviderID=@providerId AND (@startDate NOT BETWEEN StartDate AND EndDate) AND (@endDate NOT BETWEEN StartDate AND EndDate))
		begin
		update LB_BB_MarginDetails set StartDate=@startDate, EndDate=@endDate, MarginPercentage=@marginPercentage, DateModified=getdate(), IsActive=@isactive where MarginId=@marginId and ProviderID=@providerId
		set @exists=0
		end
		else
		BEGIN
		--return the true if dates are overlapping
		set @exists=1
		END
		end
		else
		begin
		--Otherwise update the margin
		update LB_BB_MarginDetails set StartDate=@startDate, EndDate=@endDate, MarginPercentage=@marginPercentage, DateModified=getdate(), IsActive=@isactive where MarginId=@marginId and ProviderID=@providerId
		set @exists=0
		end
	end
	--If it is an add operation
	else
	begin
	--Check if dates are overlapping with some other margin 
		if exists (select marginPercentage from LB_BB_MarginDetails where ProviderID=@providerId AND ((@startDate BETWEEN StartDate AND  EndDate) OR (@endDate BETWEEN StartDate AND EndDate)))
		begin
		-- return overlapping
		set @exists=1
		end
		else
		begin
		-- insert the new margin
		insert into LB_BB_MarginDetails (ProviderID, 
										MarginPercentage, 
										MarginType, 
										IsActive, 
										IsGlobal, 
										StartDate,
										EndDate,
										CreatedBy,
										DateAdded) 
							values (@providerId,
									@marginPercentage,
									'Provider',
									1,
									0,
									@startDate,
									@endDate,
									'admin',
									getdate())
		

		set @exists=0
		end
	end
	if @ErrorListID>0
		Delete from LB_BB_MarginErrorDetail where ListID=@ErrorListID and ProviderID=@providerId
end


GO

