﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Comms.Mail
{
    /// <summary>
    /// IMail Interface to get mail  data
    /// </summary>
    public interface IMail
    {
        /// <summary>
        /// Email Subject
        /// </summary>
        string Subject { get; set; }

        /// <summary>
        /// Email Body Content
        /// </summary>
        string Body { get; set; }

        /// <summary>
        /// List of Recevier Id
        /// </summary>
        List<Recepient> Recepients { get; }

        /// <summary>
        /// Sender Id
        /// </summary>
        string FromAddress { get; set; }

        /// <summary>
        /// Method to add Recevier id
        /// </summary>
        /// <param name="recepient"></param>
        void AddRecipient(Recepient recepient);
    }
}
