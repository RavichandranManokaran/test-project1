﻿CREATE TABLE [dbo].[LB_BB_MarginErrorList](
	[ListID] [int] IDENTITY(1,1) NOT NULL,
	[ListName] [varchar](50) NULL,
	[DateAdded] [datetime] NULL,
	[DateResolved] [datetime] NULL
) ON [PRIMARY]

GO