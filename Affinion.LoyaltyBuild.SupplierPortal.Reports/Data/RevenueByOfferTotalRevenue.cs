﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.SupplierPortal.Reports.Data
{
    /// <summary>
    /// Contains details for the items of revenue by offer report item
    /// </summary>
    public class RevenueByOfferTotalRevenue : RevenueBase
    {

    }

    /// <summary>
    /// Collection to manage Revenue by offer Report
    /// </summary>
    public class RevenueByOfferTotalRevenueCollection : IEnumerable<RevenueByOfferTotalRevenue>
    {
        //Private variables declaration
        private List<RevenueByOfferTotalRevenue> _items;

        /// <summary>
        /// Constructor
        /// </summary>
        public RevenueByOfferTotalRevenueCollection()
        {
            _items = new List<RevenueByOfferTotalRevenue>();
        }

        /// <summary>
        /// Add method to add item
        /// </summary>
        /// <param name="item"></param>
        public void Add(RevenueByOfferTotalRevenue item)
        {
            _items.Add(item);
        }

        /// <summary>
        /// Override method IEnumenrable class
        /// </summary>
        /// <returns></returns>
        public IEnumerator<RevenueByOfferTotalRevenue> GetEnumerator()
        {
            foreach (var item in _items)
                yield return item;
        }

        /// <summary>
        /// Override method IEnumenrable class
        /// </summary>
        /// <returns></returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            foreach (var item in _items)
                yield return item;
        }
    }
}
