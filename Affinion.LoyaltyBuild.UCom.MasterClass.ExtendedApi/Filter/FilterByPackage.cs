﻿using Affinion.LoyaltyBuild.UCom.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.Search.Facets;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi.Filter
{
    public class FilterByPackage : IHotelFilter
    {
        //public bool IncludeRoom(UCommerce.Documents.Product product, SearchCriteria criteria)
        //{
        //    if (product.Properties.Any(x => x.Value.Any(y => y.Key == DefinationConstant.OfferType && y.Value.ToString().Equals(criteria.PackageType, StringComparison.InvariantCultureIgnoreCase))))
        //    {
        //        return true;
        //    }
        //    return true;
        //}

        public bool IncludeHotel(UCommerce.Documents.Product product, SearchCriteria criteria)
        {
            return true;
        }

        public UCommerce.Search.Facets.Facet FilterRecordFacet(SearchCriteria criteria)
        {
            if (criteria.PackageType != "all" && !string.IsNullOrEmpty(criteria.PackageType))
            {
                var facet = new Facet();
                facet.FacetValues = new List<FacetValue>();
                facet.Name = ProductDefinationVairantConstant.PackageType;

                //In this example selected values are seperated by a '|' in the querystring value.
                foreach (var value in criteria.PackageType.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    facet.FacetValues.Add(new FacetValue() { Value = value });
                }
                return facet;
            }
            return null;
        }
    }
}
