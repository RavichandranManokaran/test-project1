﻿CREATE procedure [dbo].[LB_GetCampaignRevenue]
	@campaignItemId int
	As
BEGIN
	select uorderline.Sku as ProductSKU,CheckInDate as BookingDate,COUNT(uorderline.OrderLineId) as TotalBooking 
	,SUM(uorderline.Total - ordsubrate.Amount ) as TotalSale from uCommerce_OrderLineCampaignRelation ucr
	inner join LB_OrderLineSubrates ordstatus on ucr.OrderLineId = ordstatus.OrderLineID and ordstatus.OrderLineSubrateID = 6 -- 6 for paid orderline
	inner join LB_RoomReservation ur on ucr.OrderLineId = ur.orderlineid
    inner join uCommerce_OrderLine uorderline on uorderline.OrderLineId = ur.orderlineid
	inner join LB_OrderLineSubrates ordsubrate on uorderline.OrderLineId = ordsubrate.orderlineid and ordsubrate.SubrateItemCode = 'LBCom'
	where ucr.CampaignItemId  = @campaignItemId and ur.IsCancelled = 0
	group by CheckInDate,uorderline.OrderLineId, uorderline.Sku
END
