﻿CREATE TABLE [dbo].[Archive] (
    [ArchivalId]       UNIQUEIDENTIFIER NOT NULL,
    [ItemId]           UNIQUEIDENTIFIER NOT NULL,
    [ParentId]         UNIQUEIDENTIFIER NOT NULL,
    [Name]             NVARCHAR (256)   NOT NULL,
    [OriginalLocation] NVARCHAR (MAX)   NOT NULL,
    [ArchiveDate]      DATETIME         NOT NULL,
    [ArchivedBy]       NVARCHAR (50)    NOT NULL,
    [ArchiveName]      NVARCHAR (50)    NOT NULL,
    [DAC_Index]        INT              NULL,
    CONSTRAINT [PK_Archive] PRIMARY KEY NONCLUSTERED ([ArchivalId] ASC)
);


GO
CREATE CLUSTERED INDEX [DAC_Clustered]
    ON [dbo].[Archive]([DAC_Index] ASC);


GO
CREATE NONCLUSTERED INDEX [ndx_ItemId]
    ON [dbo].[Archive]([ItemId] ASC);

