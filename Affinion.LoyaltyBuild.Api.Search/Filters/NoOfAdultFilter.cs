﻿using Affinion.LoyaltyBuild.Api.Search.Data;
using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
using Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi;
using Sitecore.Data;
using UCommerce.EntitiesV2;
using UCommerce.Runtime;

namespace Affinion.LoyaltyBuild.Api.Search.Filters
{
    public class NoOfAdultFilter : ISearchFilter, ILoyaltyFilter
    {

        private readonly SearchKey _key;

        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="key"></param>
        public NoOfAdultFilter(SearchKey key)
        {
            _key = key;
        }

        /// <summary>
        /// Build search criteria
        /// </summary>
        /// <param name="criteria"></param>
        public void BuildSearch(ref UCom.MasterClass.ExtendedApi.SearchCriteria criteria)
        {
            if (_key.NoOfAdults != null)
            {
                criteria.NumberOfAdult = (int)_key.NoOfAdults;
            }
        }
    }
}
