﻿using System.Configuration;

namespace Affinion.LoyaltyBuild.BedBanks.Troika.Config
{
    public class Client : ConfigurationElement
    {
        [ConfigurationProperty("id", IsKey = true)]
        public string Id { get { return (string)this["id"]; } }

        [ConfigurationProperty("name", IsKey = true)]
        public string Name { get { return (string)this["name"]; } }

        [ConfigurationProperty("offerId", IsKey = true)]
        public int OfferId { get { return (int)this["offerId"]; } }

        [ConfigurationProperty("providerId", IsKey = true)]
        public int ProviderId { get { return (int)this["providerId"]; } }

        [ConfigurationProperty("itemId", IsKey = true)]
        public int ItemId { get { return (int)this["itemId"]; } }

        [ConfigurationProperty("clientOfferId", IsKey = true)]
        public int ClientOfferId { get { return (int)this["clientOfferId"]; } }

        [ConfigurationProperty("currencyCollection", IsDefaultCollection = true, IsKey = false, IsRequired = true)]
        public CurrencyCollection Members
        {
            get
            {
                return base["currencyCollection"] as CurrencyCollection;
            }

            set
            {
                base["currencyCollection"] = value;
            }
        }
    }
}