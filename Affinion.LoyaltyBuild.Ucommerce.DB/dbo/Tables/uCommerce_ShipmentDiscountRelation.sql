﻿CREATE TABLE [dbo].[uCommerce_ShipmentDiscountRelation] (
    [ShipmentDiscountRelationId] INT IDENTITY (1, 1) NOT NULL,
    [ShipmentId]                 INT NOT NULL,
    [DiscountId]                 INT NOT NULL,
    CONSTRAINT [PK_uCommerce_ShipmentDiscountRelation] PRIMARY KEY CLUSTERED ([ShipmentDiscountRelationId] ASC),
    CONSTRAINT [FK_uCommerce_ShipmentDiscountRelation_uCommerce_Discount] FOREIGN KEY ([DiscountId]) REFERENCES [dbo].[uCommerce_Discount] ([DiscountId]),
    CONSTRAINT [FK_uCommerce_ShipmentDiscountRelation_uCommerce_Shipment] FOREIGN KEY ([ShipmentId]) REFERENCES [dbo].[uCommerce_Shipment] ([ShipmentId])
);

