﻿Create PROCEDURE [dbo].[LB_GetChildAgeDetailsByOrderLineID]
	
	@OrderLineId as int
AS
BEGIN
	declare @isExist as Varchar(50)
	select @isExist= (select 1 from uCommerce_OrderProperty where [Key]='_ChildrenAge' and OrderLineId=@OrderLineId)

	if (@isExist is not null)
	Begin
		
		SELECT  
		Split.a.value('.', 'int') AS [Age]  
		FROM  (SELECT [Value],  
			 CAST ('<M>' + REPLACE([Value], ',', '</M><M>') + '</M>' AS XML) AS String  
			FROM  uCommerce_OrderProperty where OrderLineId=@OrderLineId and [Key]='_ChildrenAge') AS A CROSS APPLY String.nodes ('/M') AS Split(a); 
	END
END