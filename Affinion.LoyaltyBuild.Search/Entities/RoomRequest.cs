﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           RoomRequest.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Search.Entities
/// Description:           Represents a room
/// </summary>

#region Using Directives
using System.Collections.Generic; 
#endregion

namespace Affinion.LoyaltyBuild.Search.Entities
{
    public class RoomRequest
    {
        public RoomRequest()
        {
            ChildrenBeds = new List<ChildBedRequest>();
        }

        /// <summary>
        /// Adults count
        /// </summary>
        public int AdultCount { get; set; }

        public int ChildrenCount { get; set; }

        /// <summary>
        /// Get or sets the ages of children
        /// </summary>
        public ICollection<string> ChildrenAges { get; set; }

        /// <summary>
        /// Children beds
        /// </summary>
        public ICollection<ChildBedRequest> ChildrenBeds { get; set; }
       
    }
}
