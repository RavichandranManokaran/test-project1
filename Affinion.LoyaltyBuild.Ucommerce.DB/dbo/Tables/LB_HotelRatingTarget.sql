﻿CREATE TABLE [dbo].[LB_HotelRatingTarget] (
    [HotelRatingTargetId] INT            NOT NULL,
    [HotelRating]         NVARCHAR (500) NOT NULL,
    CONSTRAINT [PK_uCommerce_HotelRatingTarget] PRIMARY KEY CLUSTERED ([HotelRatingTargetId] ASC),
    CONSTRAINT [FK_uCommerce_HotelRatingTarget_uCommerce_Target] FOREIGN KEY ([HotelRatingTargetId]) REFERENCES [dbo].[uCommerce_Target] ([TargetId])
);

