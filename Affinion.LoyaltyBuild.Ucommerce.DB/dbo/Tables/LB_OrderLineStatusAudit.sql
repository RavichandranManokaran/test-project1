﻿CREATE TABLE [dbo].[LB_OrderLineStatusAudit] (
    [OrderLineStatusAuditId] INT          IDENTITY (1, 1) NOT NULL,
    [OrderLineId]       INT          NOT NULL,
    [StatusId]          INT          NOT NULL,
    [CreatedBy]         VARCHAR (50) NOT NULL,
    [CreatedOn]       DATETIME     NOT NULL,
    CONSTRAINT [uCommerce_PK_OrderLineStatusAudit] PRIMARY KEY CLUSTERED ([OrderLineStatusAuditId] ASC),
	CONSTRAINT [uCommerce_FK_OrderLineStatusAudit_OrderStatus] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[uCommerce_OrderStatus] ([OrderStatusId]),
    CONSTRAINT [uCommerce_FK_OrderLineStatusAudit_OrderLine] FOREIGN KEY ([OrderLineId]) REFERENCES [dbo].[uCommerce_OrderLine] ([OrderLineId]) ON DELETE CASCADE
);

