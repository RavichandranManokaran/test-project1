﻿using Affinion.LoyaltyBuild.Model.Product;
using Affinion.LoyaltyBuild.Model.Provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Api.Search.Data
{
    /// <summary>
    /// Search provider result
    /// </summary>
    [Serializable]
    public class SearchResult : IProvider
    {
        /// <summary>
        /// Provider Id
        /// </summary>
        public string ProviderId { get; set; }

        /// <summary>
        /// Property Refernce Id for BedBanks
        /// </summary>
        public string PropertyReferenceId { get; set; }

        /// <summary>
        /// Currency Id 
        /// </summary>
        public int CurrencyId { get; set; }

        /// <summary>
        /// SitecoreItemId
        /// </summary>
        public string SitecoreItemId { get; set; }

        /// <summary>
        /// provider type
        /// </summary>
        public ProviderType ProviderType { get; set; }

        /// <summary>
        /// provider name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// provider star ranking
        /// </summary>
        public int StarRanking { get; set; }

        /// <summary>
        /// provider local ranking
        /// </summary>
        public int LocalRanking { get; set; }
        
        /// <summary>
        /// List of products for that provider
        /// </summary>
        public IList<IProduct> Products { get; set; }
    }
}
