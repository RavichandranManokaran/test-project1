﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Offer
{
    public partial class AnnualReport : System.Web.UI.UserControl
    {
        public OfferDetail offerDetail;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.DataBind();
        }
    }
}