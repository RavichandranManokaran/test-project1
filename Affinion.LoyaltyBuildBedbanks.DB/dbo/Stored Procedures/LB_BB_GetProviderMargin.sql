
/****** Object:  StoredProcedure [dbo].[LB_BB_GetProviderMargin]    Script Date: 7/19/2016 7:26:36 PM ******/
CREATE procedure [dbo].[LB_BB_GetProviderMargin](@providerID int, @isactive int)
as
begin
if @isactive>=0
begin
select MarginID [ID], cast(StartDate as varchar) [DateFrom], cast(EndDate as varchar) [DateTo], MarginPercentage [Margin], IsActive [IsActive] from LB_BB_MarginDetails where ProviderID=@providerID and isactive=@isactive
union    
select MarginID [ID], MarginType [DateFrom], MarginType [DateTo], MarginPercentage [Margin], IsActive [IsActive] from LB_BB_MarginDetails where IsGlobal=1 and IsActive=@isactive
end
else
begin
select MarginID [ID], cast(StartDate as varchar) [DateFrom], cast(EndDate as varchar) [DateTo], MarginPercentage [Margin], IsActive [IsActive] from LB_BB_MarginDetails where ProviderID=@providerID
union    
select MarginID [ID], MarginType [DateFrom], MarginType [DateTo], MarginPercentage [Margin], IsActive [IsActive] from LB_BB_MarginDetails where IsGlobal=1
end
end


GO

