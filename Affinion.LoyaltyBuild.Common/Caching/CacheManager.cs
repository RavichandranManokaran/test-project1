﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           CacheManager.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common.Caching.CacheManager
/// Description:           Handle the custom affinion cache

using Sitecore;

namespace Affinion.LoyaltyBuild.Common.Caching
{
    public static class CacheManager
    {
        private static readonly AffinionCache Cache;

        static CacheManager()
        {
            Cache = new AffinionCache("AffinionCache",
                     StringUtil.ParseSizeString("10KB"));
        }

        /// <summary>
        /// Get cached value
        /// </summary>
        /// <param name="key">Key of the cached value</param>
        /// <returns>Cached value</returns>
        public static string GetCache(string key)
        {
            return Cache.GetString(key);
        }

        /// <summary>
        /// Set cache value
        /// </summary>
        /// <param name="key">Key of the cahce entry</param>
        /// <param name="value">Value to cache</param>
        public static void SetCache(string key, string value)
        {
            Cache.SetString(key, value);
        }
    }
}
