﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Maps
{
    public class PackageTypeTargetMap : SubclassMap<PackageTypeTarget>
    {
        // Methods
        public PackageTypeTargetMap()
        {
            base.Table("LB_PackageTypeTarget");
            base.KeyColumn("PackageTypeTargetId");
            base.Map(x => x.PackageTypeTargetId).ReadOnly();
            base.Map(x => x.PackageType).Not.Nullable();
        }

    }

}
