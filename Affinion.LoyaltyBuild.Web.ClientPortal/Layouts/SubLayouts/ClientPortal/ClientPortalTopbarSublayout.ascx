﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientPortalTopbarSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal.ClientPortalTopbarSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Sitecore.Data.Items" %>


<div class="container">
    <!-- The justified navigation menu is meant for single line per list item.
                  Multiple lines will require custom code not provided by Bootstrap. -->
    <div class="row coomon-outer-padding">
        <div class="col-md-3 col-sm-3 col-xs-6">
            <div class="logo">
                <sc:Placeholder ID="Placeholder1" Key="logo" runat="server" CssClass="img-responsive" alt="logo" />
            </div>
        </div>
        <div class="col-md-9 col-sm-9 col-xs-6">
            <div class="header-right-content-nav width-full">
                <div>
                    <div class="navbar navbar-default no-margin-bottom">
                        <div>
                            <!--// End of Logo And Search -->
                            <!-- Second NavBar -->
                            <!-- Secondary Navbar -->
                            <div class="nav-container">

                                <div id="res-navYour" class="navbar navbar-default" role="navigation">
                                    <div>
                                        <div class="navbar-header">
                                        </div>
                                        <div id="nav2">
                                            <ul class="nav navbar-nav nav2 nav-back">
                                                <li>
                                                    <a href="<%=this.BasketPageNavigation %>">
                                                        <div id="basketico" runat="server">
                                                            <div class="basket float-right header-right hidden-xs padding-left-small" id="basket-info">

                                                                <div id="basket-ico">
                                                                    <p class="pull-left">
                                                                        <i class="fa fa-briefcase"></i>
                                                                        <span class="basket-count">
                                                                            <asp:Literal ID="basketCount" runat="server" />
                                                                        </span>
                                                                    </p>
                                                                    <div id="DivBasketMessage" class="pull-left" runat="server">
                                                                        <%--<p class="pull-left padding-left-small">Your basket is empty</p>--%>
                                                                        <asp:Label CssClass="pull-left padding-left-small" runat="server" ID="BasketEmptyMsg"></asp:Label>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </a>
                                                </li>
                                                <sc:Placeholder ID="Placeholder2" Key="TopbarLinkArea" runat="server" CssClass="img-responsive" alt="logo" />
                                            </ul>
                                        </div>
                                        <!--/.nav-collapse -->
                                    </div>
                                    <!--/.container-fluid -->
                                </div>
                            </div>
                        </div>
                        <!--/.nav-collapse -->
                    </div>
                    <!--/.container-fluid -->
                </div>

                <div class="tp-number float-right hidden-xs">
                    <sc:Placeholder ID="TobbarRichText" Key="TobbarRichText" runat="server" />
                </div>
                <%--<div class="tp-number float-right hidden-xs">
                    <p>
                        <span><i class="fa fa-phone"></i></span><strong>
                            0123456789
                        </strong>
                        <sc:Placeholder ID="TobbarSimpleText" Key="TobbarSimpleText" runat="server" />
                    </p>
                </div>--%>
            </div>
        </div>
        <div class="sm-visibale">
            <div class="sm-visibale-inner">
                <%-- <div class="res-icon float-right visible-xs" id="ph-icon">
                    <a href="#"><i class="fa fa-phone"></i></a>
                </div>--%>
                <div class="res-icon float-right visible-xs" id="bck-icon">
                    <a href="<%=this.BasketPageNavigation %>">
                        <div id="basketicorespodive" runat="server">
                            <div id="basket-ico-respodive">
                                <p>
                                    <i class="fa fa-briefcase"></i>
                                    <%--<span>5</span>--%>
                                    <span class="basket-count">
                                        <asp:Literal ID="basketCountMobile" runat="server" />
                                    </span>
                                </p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="res-icon float-right visible-xs" id="fav-icon">
                    <a href="#"><i class="fa fa-heart"></i></a>
                </div>
                <%--<div class="sm-visibale">--%>
                <div id="DivSignInPay" class="res-icon float-right visible-xs" runat="server">
                    <div id="user-icon">
                        <a href="#"><i class="fa fa-user" data-toggle="modal" data-target="#exampleModal" data-whatever="@fat"></i></a>
                    </div>
                </div>
                <%--</div>--%>
            </div>
        </div>
    </div>
</div>


