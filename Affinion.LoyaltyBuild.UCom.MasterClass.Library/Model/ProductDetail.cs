﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    public class ProductDetail
    {
        public string ProductSKU { get; set; }

        //public string VariantSKU { get; set; }

        public string ProductName { get; set; }

        public int ProductId { get; set; }

        public List<PriceBand> PriceBand { get; set; }

        public int ProductType { get; set; }

        public decimal AdditionalPrice { get; set; }

        public int NumberOfAdult { get; set; }

        public int NumberOfChild { get; set; }

        public string VariantSku { get; set; }

        public decimal PackagePrice { get; set; }

        public decimal ProductPrice { get; set; }

        public string RelatedSKU { get; set; }

        public string RelatedAddOnSKU { get; set; }

        public string CombineSKU { get; set; }
    }
}
