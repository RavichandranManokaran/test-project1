﻿using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Availability;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Model;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Globalization;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Welcome
{
    /// <summary>
    /// class news
    /// </summary>
    public partial class News : System.Web.UI.UserControl
    {
        #region Private variables

        private readonly string currentUserRole = SitecoreUserProfile.GetItem(UserProfileKeys.currentLoggedinUserRole);
        private readonly string currentUserName = SitecoreUserProfile.GetItem(UserProfileKeys.loggedInUserName);
        private readonly string newsListQuery = @"fast:/sitecore/content/#admin-portal#/#supplier-setup#/#news#/news/*";
        private String loggedInSupplierDisplayName { get; set; }

        private Collection<Item> Country { get; set; }
        private Collection<Item> Location { get; set; }
        private Collection<Item> MapLocation { get; set; }
        private Collection<Item> SupplierType { get; set; }
        private List<NewsFields> newsFieldsList { get; set; }
        protected List<NewsFields> newsListPriority { get; set; }



        #endregion

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            GetSitecoreInfo();

            if (!IsPostBack)
            {
                try
                {
                    LoadNewsDetails();
                }
                catch (Exception ex)
                {
                    Diagnostics.WriteException(DiagnosticsCategory.SupplierPortal, ex, this);
                    throw;
                }
            }
        }

        /// <summary>
        /// ddl hotels selected index changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlHotels_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlHotels.SelectedValue != "0" && ddlHotels.SelectedItem.Text != string.Empty)
            {
                SitecoreUserProfile.AddItem(ddlHotels.SelectedValue, UserProfileKeys.selectedProviderId);
                SitecoreUserProfile.AddItem(ddlHotels.SelectedValue, UserProfileKeys.listBoxSelectedValue);
                SitecoreUserProfile.AddItem(ddlHotels.SelectedItem.Text, UserProfileKeys.listBoxSelectedText);
                FetchNewsBasedOnLanguage(ddlHotels.SelectedValue.ToString());
                this.RoomShortageBindGrid();
                this.OfferDetails();
            }

        }

        /// <summary>
        /// ddl Groups On Selected Index Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlGroups_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (ddlGroups.SelectedItem.Value != "0" && ddlGroups.SelectedItem.Text != string.Empty)
            {
                GroupHotelDropDown(ddlGroups.SelectedValue.ToString(), ddlGroups.SelectedItem.Text);
                SitecoreUserProfile.AddItem(ddlGroups.SelectedItem.Text, UserProfileKeys.groupListBoxSelectedText);

                hotelNameLabel.Visible = false;
                HotelName.Visible = false;

            }
        }

        /// <summary>
        ///ddl selected hotel click event 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlSelectedHotel_SelectedIndexChanged(object sender, EventArgs e)
        {
            var groupHotelListBoxSelectedText = SitecoreUserProfile.GetItem(UserProfileKeys.groupListBoxSelectedText);
            var loggedInSupplier = SitecoreUserProfile.GetItem(UserProfileKeys.loggedInSupplier);
            if (currentUserRole != null && currentUserRole.ToLower().Equals(Constants.grpHotelRole))
            {
                groupDetailSection.Visible = true;
                var loggedInSupplierDisplayName = SitecoreUserProfile.GetItem(UserProfileKeys.loggedinSupplierDisplayName);
                groupHotelNameLabel.Text = loggedInSupplierDisplayName;
            }
            else if (groupHotelListBoxSelectedText != null || groupHotelListBoxSelectedText != "")
            {
                groupDetailSection.Visible = true;

                var loggedInSupplierDisplayName = SitecoreUserProfile.GetItem(UserProfileKeys.loggedinSupplierDisplayName);
                groupHotelNameLabel.Text = groupHotelListBoxSelectedText;

            }
            if (ddlListHotel.SelectedValue != "0" && ddlListHotel.SelectedItem.Text != string.Empty)
            {

                SitecoreUserProfile.AddItem(ddlListHotel.SelectedValue, UserProfileKeys.dropDownSelectedValue);
                SitecoreUserProfile.AddItem(ddlListHotel.SelectedValue, UserProfileKeys.selectedProviderId);
                if (ddlListHotel.SelectedValue != "0" && ddlListHotel.SelectedValue != string.Empty)
                {
                    SitecoreUserProfile.AddItem(ddlListHotel.SelectedValue, UserProfileKeys.supplierTypeDropDown);
                    FetchNewsBasedOnLanguage(ddlListHotel.SelectedValue.ToString());
                    this.RoomShortageBindGrid();
                    this.OfferDetails();

                }
            }
        }

        /// <summary>
        /// supplier category drop down click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlsupplierCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (supplierCategoryDropDownList.SelectedValue.Trim() != null)
            {

                ddlGroupsSection.Visible = false;
                ddlGroups.Visible = false;

                if (supplierCategoryDropDownList.SelectedValue.Trim().Equals("Group"))
                {
                    SitecoreUserProfile.AddItem(supplierCategoryDropDownList.SelectedValue, UserProfileKeys.typeDropDownSelectedValue);
                    supplierTypeDropDownSection.Visible = false;
                    ddlHotels.Visible = false;
                    ddlHotelsSection.Visible = false;


                    if (ddlGroups.Items.Count > 1)
                    {
                        ddlGroupsSection.Visible = true;
                        ddlGroups.Visible = true;
                    }

                }
                else if (supplierCategoryDropDownList.SelectedValue.Trim().Equals("Independent"))
                {
                    SitecoreUserProfile.AddItem(supplierCategoryDropDownList.SelectedValue, UserProfileKeys.typeDropDownSelectedValue);
                    supplierTypeDropDownSection.Visible = true;
                    supplierTypeDropDownList.Visible = true;
                    ddlGroupsSection.Visible = false;
                    ddlGroups.Visible = false;

                    dropDownSection.Visible = false;

                }



            }

        }

        /// <summary>
        /// ddl Supplier type index changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlSupplierType_SelectedIndexChanged(object sender, EventArgs e)
        {

            Item[] hotelList = Sitecore.Context.Database.SelectItems(Constants.hotelPath);
            var supplierList = new ListItemCollection();
            ddlHotelsSection.Visible = false;
            ddlHotels.Visible = false;
            if (supplierTypeDropDownList.SelectedValue != null)
            {

                if (hotelList != null)
                {
                    foreach (var hotel in hotelList)
                    {
                        Sitecore.Data.Fields.ReferenceField droplinkFld = hotel.Fields["SupplierType"];
                        if (droplinkFld != null)
                        {
                            if (droplinkFld.TargetItem != null)
                            {
                                Sitecore.Data.Items.Item targetItem2 = droplinkFld.TargetItem;
                                if (targetItem2 != null && targetItem2.ID.ToString().Equals(supplierTypeDropDownList.SelectedValue, StringComparison.InvariantCultureIgnoreCase)&&string.IsNullOrWhiteSpace(hotel.Fields["Group"].Value))
                                {
                                    ListItem liBox = new ListItem();
                                    liBox.Text = SitecoreFieldsHelper.GetValue(hotel, "Name");
                                    liBox.Value = hotel.ID.ToString();
                                    if (liBox != null)
                                    {
                                        supplierList.Add(liBox);
                                    }
                                }
                            }



                        }
                    }
                    BindDropdown(ddlHotels, supplierList, "Text", "Value");
                    ddlHotels.Items.Insert(0, new ListItem { Text = "-select-", Value = "0" });
                    if (ddlHotels.Items.Count > 1)
                    {
                        ddlHotelsSection.Visible = true;
                        ddlHotels.Visible = true;
                    }

                }
            }

        }


        #region private methods

        /// <summary>
        /// get news details
        /// </summary>
        private void LoadNewsDetails()
        {
            var loggedInSupplier = SitecoreUserProfile.GetItem(UserProfileKeys.loggedInSupplier);
            var loggedInSupplierID = SitecoreUserProfile.GetItem(UserProfileKeys.loggedInProviderId);

            var loggedInSupplierDisplayName = SitecoreUserProfile.GetItem(UserProfileKeys.loggedinSupplierDisplayName);
            if (currentUserRole != null && loggedInSupplierID != null && currentUserRole.ToLower().Contains(Constants.grpHotelRole))
            {

                Item grpHotel = Sitecore.Context.Database.SelectItems(Constants.GroupHotelPath).Where(x => x.ID.ToString().Equals(loggedInSupplierID, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                if (grpHotel != null && !grpHotel.Equals(""))
                {
                    loggedInSupplierDisplayName = SitecoreFieldsHelper.GetValue(grpHotel, "Name");
                }
                GroupHotelDropDown(loggedInSupplierID, loggedInSupplierDisplayName);
                lastlogindate.Text = SitecoreUserProfile.GetItem(UserProfileKeys.lastLoggedInDate);
                LoggedInContact();
                lastlogindate.Visible = true;
                LastLoginLabel.Visible = true;
                WelcomeLabel.Visible = true;
                hotelNameLabel.Visible = false;
                HotelName.Visible = false;
                groupDetailSection.Visible = true;
                groupHotelNameLabel.Text = loggedInSupplierDisplayName;
            }
            else if (currentUserRole != null && (currentUserRole.ToLower().Contains(Constants.lbAdminRole) || currentUserRole.ToLower().Contains(Constants.superUser) || currentUserRole.ToLower().Contains(Constants.accountManagement) || currentUserRole.ToLower().Contains(Constants.marketing) || currentUserRole.ToLower().Contains(Constants.IT) || currentUserRole.ToLower().Contains(Constants.finance) || Sitecore.Context.IsAdministrator))
            {
                lastlogindate.Text = SitecoreUserProfile.GetItem(UserProfileKeys.lastLoggedInDate);
                lastlogindate.Visible = true;
                LastLoginLabel.Visible = true;
                WelcomeLabel.Visible = true;
                ContactName.Text = currentUserName;
                ContactName.Visible = true;
                SupplierCategoryDropDown();
                SupplierTypeDropDown();
                FillGroupDropDown();
            }
            else if (loggedInSupplier != null)
            {

                if (!String.IsNullOrWhiteSpace(loggedInSupplierID))
                {
                    SitecoreUserProfile.AddItem(loggedInSupplierID, UserProfileKeys.selectedProviderId);
                    FetchNewsBasedOnLanguage(loggedInSupplierID);
                }
                this.RoomShortageBindGrid();
                this.OfferDetails();

            }
        }

        /// <summary>
        /// Logged In Contact for display
        /// </summary>
        private void LoggedInContact()
        {
            string loggedInContactID = SitecoreUserProfile.GetItem(UserProfileKeys.loggedInContactID);

            if (!String.IsNullOrWhiteSpace(loggedInContactID))
            {
                Item loggedInContact = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(loggedInContactID));
                if (loggedInContact != null)
                {
                    ContactName.Text = !String.IsNullOrWhiteSpace(loggedInContact.DisplayName) ? loggedInContact.DisplayName : currentUserName;
                    ContactName.Visible = true;
                }

            }
        }


        /// <summary>
        /// Get the group hotel list to the dropdown list
        /// </summary>
        /// <param name="loggedInSupplier"></param>
        /// <param name="loggedinSupplierDisplayName"></param>
        private void GroupHotelDropDown(string loggedInSupplier, string loggedinSupplierDisplayName)
        {
            groupDetailSection.Visible = true;
            groupHotelNameLabel.Text = loggedinSupplierDisplayName;
            dropDownSection.Visible = false;
            ddlListHotel.Visible = false;

            if (Sitecore.Context.Database.SelectItems(Constants.hotelPath) != null)
            {
                Item[] Hotels = Sitecore.Context.Database.SelectItems(Constants.hotelPath);

                if (Hotels != null)
                {
                    Hotels.OrderBy(a => a.DisplayName);

                    var hotels = new ListItemCollection();
                    foreach (var hotel in Hotels)
                    {
                        Sitecore.Data.Fields.ReferenceField droplinkFld = hotel.Fields[Constants.group];
                        if (droplinkFld != null)
                        {
                            if (droplinkFld.TargetItem != null)
                            {
                                Sitecore.Data.Items.Item targetItem2 = droplinkFld.TargetItem;
                                if (targetItem2 != null && targetItem2.DisplayName != null && loggedInSupplier != null && targetItem2.ID.ToString().Equals(loggedInSupplier, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    ListItem li = new ListItem();
                                    li.Text = SitecoreFieldsHelper.GetValue(hotel, "Name");
                                    li.Value = hotel.ID.ToString();
                                    hotels.Add(li);

                                }
                            }
                        }
                    }
                    BindDropdown(ddlListHotel, hotels, "Text", "Value");
                    ddlListHotel.Items.Insert(0, new ListItem { Text = "-select-", Value = "0" });
                    if (ddlListHotel.Items.Count > 1)
                    {
                        dropDownSection.Visible = true;
                        ddlListHotel.Visible = true;
                    }

                }
            }
        }

        /// <summary>
        /// Supplier Category DropDown
        /// </summary>
        private void SupplierCategoryDropDown()
        {
            supplierCategoryDropDownSection.Visible = true;
            supplierCategoryLabel.Visible = true;

        }

        /// <summary>
        /// Supplier Type Drop Down
        /// </summary>
        private void SupplierTypeDropDown()
        {
            ListItem li = null;
            Item[] supplierTypeList = Sitecore.Context.Database.SelectItems(Constants.supplireTypePath) != null ? Sitecore.Context.Database.SelectItems(Constants.supplireTypePath) : null;
            if (supplierTypeList != null)
            {
                foreach (var suplierType in supplierTypeList)
                {
                    li = new ListItem();
                    li.Text = SitecoreFieldsHelper.GetValue(suplierType, "Name");
                    li.Value = suplierType.ID.ToString();
                    if (li != null)
                    {
                        supplierTypeDropDownList.Items.Add(li);
                    }
                }
            }

        }


        /// <summary>
        /// Fill Group Drop Down
        /// </summary>
        private void FillGroupDropDown()
        {

            var hotelsList = new ListItemCollection();
            Item[] groupList = Sitecore.Context.Database.SelectItems(Constants.GroupHotelPath);
            if (groupList != null)
            {

                foreach (var group in groupList)
                {
                    ListItem groupLi = new ListItem();
                    groupLi.Text = SitecoreFieldsHelper.GetValue(group, "Name");
                    groupLi.Value = group.ID.ToString();

                    hotelsList.Add(groupLi);

                }
                BindDropdown(ddlGroups, hotelsList, "Text", "Value");

                ddlGroups.Items.Insert(0, new ListItem { Text = "-select Group-", Value = "0" });



            }
        }


        #region fetchnews

        /// <summary>
        /// Fetch News Based On Language
        /// </summary>
        /// <param name="SupplyItemID"></param>
        private void FetchNewsBasedOnLanguage(string SupplyItemID)
        {

            LoggedInContact();
            var grpHotelName = SitecoreUserProfile.GetItem(UserProfileKeys.GroupHotelName);
            lastlogindate.Text = SitecoreUserProfile.GetItem(UserProfileKeys.lastLoggedInDate);
            Item supplyItem = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(SupplyItemID));
            if (supplyItem != null)
            {
                HotelName.Text = SitecoreFieldsHelper.GetValue(supplyItem, "Name");// supplyItem.DisplayName;
                hotelNameLabel.Visible = true;
                FilteredNewsItems(supplyItem);
            }

            if (newsFieldsList != null && newsFieldsList.Count > 0)
            {
                newsListPriority = new List<NewsFields>();
                newsListPriority = newsFieldsList.OrderBy(x => x.NewsPriority).ThenBy(x => x.FromDate).ToList();
                newsRepeater.DataSource = newsListPriority;
                newsRepeater.DataBind();
            }

        }

        /// <summary>
        /// map news fields to newsFieldsList
        /// </summary>
        /// <param name="filteredNews"></param>
        private void MapNewsFields(List<Item> filteredNews)
        {
            newsFieldsList = new List<NewsFields>();
            foreach (Item news in filteredNews)
            {

                NewsFields newsFields = new NewsFields();
                newsFields.NewsTitle = SitecoreFieldsHelper.GetItemFieldValue(news, Constants.title);
                newsFields.NewsDescription = SitecoreFieldsHelper.GetItemFieldValue(news, Constants.description);
                string customPriority = SitecoreFieldsHelper.GetItemFieldValue(news, Constants.Priority);
                newsFields.NewsPriority = !string.IsNullOrWhiteSpace(customPriority) ? Int32.Parse(customPriority) : 10;
                Sitecore.Data.Fields.DateField fromDate = news.Fields["FromDate"];
                newsFields.FromDate = fromDate != null ? DateTime.Parse(fromDate.ToString()) : (DateTime?)null;
                Sitecore.Data.Fields.DateField toDate = news.Fields["ToDate"];
                newsFields.ToDate = toDate != null ? DateTime.Parse(toDate.ToString()) : (DateTime?)null;
                newsFields.PublishDate = news.Statistics.Updated.ToString("dd/MM/yyyy");
                newsFieldsList.Add(newsFields);

            }
        }

        /// <summary>
        ///filter news items based on country location maplocation
        /// </summary>
        /// <param name="supplyItem"></param>
        private void FilteredNewsItems(Item supplyItem)
        {
            Item country = SitecoreFieldsHelper.GetDropLinkFieldTargetItem(supplyItem, Constants.Country);

            Item location = SitecoreFieldsHelper.GetDropLinkFieldTargetItem(supplyItem, Constants.Location);
            Item mapLocation = SitecoreFieldsHelper.GetDropLinkFieldTargetItem(supplyItem, Constants.MapLocation);
            Item supplierType = SitecoreFieldsHelper.GetDropLinkFieldTargetItem(supplyItem, Constants.SupplierType);

            var languageCultureCode = SitecoreFieldsHelper.GetItemFieldValue(country, Constants.Culture);
            Language itemLanguage = Language.Parse(languageCultureCode);
            List<Item> newsItems = Sitecore.Context.Database.SelectItems(newsListQuery).ToList();
            List<Item> ActiveNewsItems = new List<Item>();
            GetActiveNewsItems(itemLanguage, newsItems, ActiveNewsItems);
            List<Item> filteredNews = new List<Item>();
            if (ActiveNewsItems != null)
            {
                foreach (Item newsItem in ActiveNewsItems)
                {
                    Country = SitecoreFieldsHelper.GetMutiListItems(newsItem, Constants.Country);
                    Location = SitecoreFieldsHelper.GetMutiListItems(newsItem, Constants.Location);
                    MapLocation = SitecoreFieldsHelper.GetMutiListItems(newsItem, Constants.MapLocation);
                    SupplierType = SitecoreFieldsHelper.GetMutiListItems(newsItem, Constants.SupplierType);
                    if (((country != null && Country.Any(x => x.ID.Equals(country.ID)))
                        || (location != null && Location.Any(x => x.ID.Equals(location.ID)))
                        || (mapLocation != null && MapLocation.Any(x => x.ID.Equals(mapLocation.ID))))
                    && SupplierType.Where(x => x != null && supplierType != null).Any(x => x.ID.Equals(supplierType.ID)))
                    {
                        filteredNews.Add(newsItem);
                    }

                }
            }
            if (filteredNews != null && filteredNews.Count > 0)
            {
                filteredNews = filteredNews
                    .Where(x => x.Fields["FromDate"].Value != null
                        && x.Fields["ToDate"].Value != null
                        && ((DateField)(x.Fields["FromDate"])).DateTime.Date.AddDays(1) <= DateTime.Now.Date
                        && ((DateField)(x.Fields["ToDate"])).DateTime.Date.AddDays(1) >= DateTime.Now.Date
                        && ((DateField)(x.Fields["FromDate"])).DateTime.AddDays(1) > DateTime.Now.AddMonths(-13))
                    .ToList();
                MapNewsFields(filteredNews);

            }
        }

        /// <summary>
        /// Get the news items which is active
        /// </summary>
        /// <param name="itemLanguage"></param>
        /// <param name="newsItems"></param>
        /// <param name="ActiveNewsItems"></param>
        private void GetActiveNewsItems(Language itemLanguage, List<Item> newsItems, List<Item> ActiveNewsItems)
        {
            foreach (Item news in newsItems)
            {
                bool haslanguage = false;
                haslanguage = ItemManager.GetVersions(news, itemLanguage).Count > 0;
                if (haslanguage)
                {
                    var languageVersion = ItemManager.GetVersions(news, itemLanguage);
                    Sitecore.Data.Version itemVersion = Sitecore.Data.Version.Parse(languageVersion);
                    Item item = Sitecore.Context.Database.GetItem(news.ID, itemLanguage, itemVersion);
                    if (SitecoreFieldsHelper.CheckBoxChecked(item, "IsActive"))
                    {
                        ActiveNewsItems.Add(item);
                    }
                }

            }
        }

        #endregion


        /// <summary>
        /// Get the room shortage details to the data grid
        /// </summary>
        private void RoomShortageBindGrid()
        {
            DateTime startDate = DateTime.Now;
            DateTime twoMonths = startDate.AddMonths(2);
            DateTime endDate = twoMonths.AddDays(1);
            var list = new List<RoomShortage>();
            var sku = SitecoreUserProfile.SelectedSKU;

            ///need to add do actual implementation
            if (sku != null)
            {
                RoomAvailabilityService roomAvailabilityService = new RoomAvailabilityService();
                List<RoomAndOfferAvaialbityShortage> shortageDetails = new List<RoomAndOfferAvaialbityShortage>();
                shortageDetails = roomAvailabilityService.GetHotelRoomAndOfferAvailabilityByProvider(sku, startDate, endDate);
                shortageDetails = shortageDetails.OrderBy(x => x.Date).ToList();

                if (shortageDetails != null)
                {
                    var shortageRooms = shortageDetails
                        .Where(x => x.RoomShortage != null && x.RoomShortage.Count > 0)
                        .SelectMany(x => x.RoomShortage)
                        .Where(x => !string.IsNullOrEmpty(x.ProductSku))
                        .Select(x => new KeyValuePair<string, string>(x.ProductSku, x.RoomName))
                        .Distinct()
                        .ToList();

                    foreach (var item in shortageRooms)
                    {
                        var months = shortageDetails
                            .Where(x => x.RoomShortage != null && x.RoomShortage.Exists(y => y.ProductSku == item.Key))
                            .Select(x => x.Date.Month).OrderBy(x => x)
                            .Distinct()
                            .ToList();

                        foreach (var month in months)
                        {
                            var soldoutdates = shortageDetails
                            .Where(x => x.RoomShortage != null && x.RoomShortage.Exists(y => y.ProductSku == item.Key && y.AvailableRoom > 0 && y.IsSoldOut && (!y.IsCloseOut)) && x.Date.Month == month)
                            .Select(x => x.Date)
                            .OrderBy(x => x)
                            .ToList();
                            var soldoutdatestring = Enumerable
                                .Range(0, soldoutdates.Count)
                                .Select(i => soldoutdates[i].ToString(((i == 0) || (soldoutdates[i].Month != soldoutdates[i - 1].Month)) ? "dd" : "dd"))
                                .ToList();

                            var closedoutdates = shortageDetails
                                .Where(x => x.RoomShortage != null && x.RoomShortage.Exists(y => y.ProductSku == item.Key && y.IsCloseOut) && x.Date.Month == month)
                                .Select(x => x.Date)
                                .OrderBy(x => x)
                                .ToList();
                            var closedoutdatestring = Enumerable
                                .Range(0, closedoutdates.Count)
                                .Select(i => closedoutdates[i].ToString(((i == 0) || (closedoutdates[i].Month != closedoutdates[i - 1].Month)) ? "dd" : "dd"))
                                .ToList();

                            if (soldoutdates.Count > 0 || closedoutdates.Count > 0)
                            {
                                list.Add(new RoomShortage { Offer = item.Value, NumericMonth = month, Month = new DateTime(1900, month, 1).ToString("MMMM"), SoldOutDates = string.Join(", ", soldoutdatestring), ClosedOutDates = string.Join(", ", closedoutdatestring) });
                            }
                        }


                    }

                    RoomShortageGridView.DataSource = list.OrderBy(x => x.NumericMonth).ToList();
                    RoomShortageGridView.DataBind();
                }
            }

            if (list != null && list.Count == 0)
            {
                RoomShortageGridView.Visible = false;
                divRoomShortageDetails.Visible = false;
            }
            else
            {
                RoomShortageGridView.Visible = true;
                divRoomShortageDetails.Visible = true;
            }
        }

        /// <summary>
        /// Get the offer news details to the data grid
        /// </summary>
        private void OfferDetails()
        {
            DateTime startDate = DateTime.Now;
            DateTime endDate = startDate.AddDays(30);

            var sku = SitecoreUserProfile.SelectedSKU;
            //need to add do actual implementation
           // var ProviderId = SitecoreUserProfile.GetItem(UserProfileKeys.loggedInSupplier_ucom);
            if (sku != null)
            {
                CampaignSearch campaignSearch = new CampaignSearch();
                List<SupplierInviteDetail> supplierCampaign = campaignSearch.GetCampaignDetailBySupplier(sku, startDate, endDate);
                if (supplierCampaign != null && supplierCampaign.Count > 0)
                {
                    var list = new List<object>();
                    var campains = supplierCampaign
                        .Where(x => !x.IsSubscibe)
                        .Where(x => x.CampaignName != null)
                        .Select(x => new { Name = x.CampaignName });
                    list.AddRange(campains);
                    campaignOfferDetails.DataSource = list;
                    campaignOfferDetails.DataBind();
                }
            }


            if (campaignOfferDetails.DataSource == null || ((List<object>)campaignOfferDetails.DataSource).Count == 0)
            {
                campaignOfferDetails.Visible = false;
                divOfferShortageDetails.Visible = false;
            }
            else
            {
                campaignOfferDetails.Visible = true;
                divOfferShortageDetails.Visible = true;

            }
        }

        private void GetSitecoreInfo()
        {
            Item currentpageitem = Sitecore.Context.Item;
            SitecoreFieldsHelper.BindSitecoreText(currentpageitem, "Select type", Selecttype);

        }
        /// <summary>
        /// common method to bind drop down values
        /// </summary>
        private void BindDropdown(DropDownList ddlControl, object dataSource, string text, string value)
        {
            ddlControl.DataSource = dataSource;
            ddlControl.DataTextField = text;
            ddlControl.DataValueField = value;
            ddlControl.DataBind();
        }
        #endregion

        /// <summary>
        /// Room Shortage Details
        /// </summary>
        internal class RoomShortage
        {
            public string Offer { get; set; }
            public int NumericMonth { get; set; }
            public string Month { get; set; }
            public string SoldOutDates { get; set; }
            public string ClosedOutDates { get; set; }
        }



    }
}

