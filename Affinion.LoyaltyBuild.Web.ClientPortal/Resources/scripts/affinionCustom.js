/*jslint browser: true*/
/*global $, jQuery*/
// Affinion General Scripts

(function () {
    "use strict";
    jQuery(document).ready(function () {

        jQuery('#nodate').change(function () {
            if (this.checked)
                jQuery('.ondate').slideUp("slow");
            else
                jQuery('.ondate').slideDown("slow");
        });

        jQuery('#roomCount').change(function () {
            var roomCount = jQuery(this).val();
        });


        //// Child Age Selector
        //        $('#childrenCount').change(function () {
        //            var childCount = $(this).val(), i = 0, maxCount = Number(childCount) + 1;
        //            if (childCount === 0) {
        //                $('.ageGroupOuter').hide();
        //            } else {$('.ageGroupOuter').show(); }
        //            $('.age').hide();
        //            for (i; i < maxCount; i = i + 1) {
        //                $('#' + i).show();
        //            }




        //        });
        var childCount = jQuery('#NoOfChildren').val();
        SetAgeDropdownVisibility(childCount);

        // Child Age Selector
        jQuery('#NoOfChildren').change(function () {
            var childCount = jQuery(this).val();
            SetAgeDropdownVisibility(childCount);
        });

        function SetAgeDropdownVisibility(childCount) {
            var i = 0, maxCount = Number(childCount) + 1;

            if (childCount === '0') {
                jQuery('.ageGroupOuter').hide();

            } else {
                jQuery('.ageGroupOuter').show();
            }
            jQuery('.age').hide();

            for (i; i < maxCount; i = i + 1) {
                jQuery('#AgeDropdownDiv' + i).show();
            }
            //Set age dropdowns to default select value
            SetAgeDropdownDefaultValue();
        }

        function SetAgeDropdownDefaultValue() {
            jQuery('.stockSearchPanel .childAgeDropdown:hidden,.stockSearchPanel .childBedAgeDropdown:hidden').prop('selectedIndex', 0);
        }

        var roomCount = jQuery('#ChildBedSelectionNoOfRoomsDropDownList').val();

        ///Show room panels according to number of rooms selected
        SetRoomPanelVisibility();

        ///Show age dropdowns according to number of children selected
        SetAgeDropdownVisibilityOnChildBedSelection(roomCount);

        function SetAgeDropdownVisibilityOnChildBedSelection(roomCountParam) {

            jQuery('#room-availability .childBedAge').hide();
            jQuery('#room-availability .childBedType').hide();

            for (var i = 0; i < roomCountParam; i++) {
                var ageDropDownParentdiv = jQuery('#Room' + (i + 1) + 'Div');

                var childrenCount = jQuery('#Room' + (i + 1) + 'Div #NoOfChildrenDropDownList').val();

                for (var j = 0; j < childrenCount; j++) {
                    var ageDropdown = ageDropDownParentdiv.find("#AgeDropdownDiv" + (j + 1));
                    ageDropdown.show();

                    var bedTypeDropdown = ageDropDownParentdiv.find("#BedTypeDropDownDiv" + (j + 1));
                    bedTypeDropdown.show();
                }
            }
        }

        jQuery('.bedSelectionChildCount').change(function () {

            var roomCount = jQuery('#ChildBedSelectionNoOfRoomsDropDownList').val();

            SetAgeDropdownVisibilityOnChildBedSelection(roomCount);

            SetAgeDropdownDefaultValue();

        });


        function SetRoomPanelVisibility() {
            ///Hides all room panels
            jQuery('.roomPanels').hide();

            var roomCount = jQuery('#ChildBedSelectionNoOfRoomsDropDownList').val();

            ///Show room panels according to number of rooms selected
            for (var i = 0; i < roomCount; i++) {

                jQuery('#Room' + (i + 1) + 'Div').show();

            }

        }

        jQuery('#ChildBedSelectionNoOfRoomsDropDownList').change(function () {
            SetRoomPanelVisibility();
        });

    });

}());



