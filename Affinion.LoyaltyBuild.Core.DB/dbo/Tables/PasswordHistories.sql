﻿CREATE TABLE [dbo].[PasswordHistories] (
    [UserName]     NVARCHAR (128) NOT NULL,
    [Password]     NVARCHAR (128) NOT NULL,
    [PasswordSalt] NVARCHAR (MAX) NULL,
    [ChangedDate]  DATETIME       NOT NULL,
    [ID]           INT            IDENTITY (1, 1) NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);

