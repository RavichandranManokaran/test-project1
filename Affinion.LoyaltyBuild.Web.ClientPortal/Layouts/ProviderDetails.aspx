﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProviderDetails.aspx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.ProviderDetails" %>

<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ OutputCache Location="None" VaryByParam="none" %>

<%@ Import Namespace="Sitecore.Data.Items" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
    <title>
        <sc:text id="ProviderTitle" field="Name" runat="server" />
    </title>

    <script type="text/javascript" src="/Resources/scripts/jquery.min.js"></script>    
    <script type="text/javascript" src="/Resources/bootstrap_assets/javascripts/bootstrap.min.js"></script>
    <%--<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"> </script>--%>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&sensor=true"></script>

    <link rel="stylesheet" href="/Resources/styles/responsiveNav.css" type="text/css" />
    <link rel="stylesheet" href="/Resources/styles/bootstrap.min.css" />
    <link rel="stylesheet" href="/Resources/styles/affinionOfflinePortal.min.css" />
    <link rel="stylesheet" href="/Resources/styles/font-awesome.min.css" />
    <link href="/Resources/styles/theme1/cstheme.css" rel="stylesheet" />
    <link href="/Resources/styles/offlineportal.css" rel="stylesheet" />
                
    </head>
    <body>
        <form id="ProviderForm" runat="server">
            <div>
                <div class="modal-header">
                    <h4 class="modal-title">
                        <sc:text id="ProviderNameHead" field="Name" runat="server" />
                    </h4>
                </div>

                <div class="modal-body">
                <div>
                    <div>                        
                        <%--Navigations--%>
                        <div class="row">
                            <div class="col-xs-12 margin-common-inner-box">
                                <ul class="info-top-link list-group">
                                    <li class="display-block-val pull-left"><a href="#facilities" class="list-group-item alert-info" title="Facilities">Facilities </a></li>
                                    <li class="display-block-val pull-left"><a href="#goodToKnow" class="list-group-item alert-info" title="GoodToKnow">GoodToKnow</a> </li>
                                    <li class="display-block-val pull-left"><a href="#checkInCheckOutTime" class="list-group-item alert-info" title="CheckInCheckOutTime">Check In CheckOut Time</a> </li>                                    
                                </ul>
                            </div>
                        </div>
                        
                        <%--Contents--%>
                        <div class="row">
                            <div class="col-md-12 no-padding">
                                <div class="col-xs-12 col-md-7">
                                    <div class="hotel-image">
                                        <sc:image id="ProviderIamge" field="IntroductionImage" cssclass="img-responsive img-thumbnail" runat="server" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-5">
                                    <div class="hotel-search-box-detail">
                                        <div class="hotel-name-detail">
                                            <h2 class="no-margin pull-left">
                                                <sc:text id="ProviderName" field="Name" runat="server" />
                                            </h2>

                                            <div class="star-rank pull-left padding-left-small padding-top-small">
                                                <asp:Repeater ID="StarRepeater" runat="server">
                                                    <ItemTemplate>
                                                        <span class="fa fa-star"></span>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>

                                            <div class="hotel-name-detail-bottom width-full pull-left">
                                                <p style="color: #3c763d;">
                                                    <sc:text id="AddressLine1" field="AddressLine1" runat="server" />
                                                    <sc:text id="AddressLine2" field="AddressLine2" runat="server" />
                                                    <sc:text id="AddressLine3" field="AddressLine3" runat="server" />
                                                    <sc:text id="AddressLine4" field="AddressLine4" runat="server" />
                                                    <sc:text id="Town" field="Town" runat="server" />
                                                    <sc:text id="CountryName" field="CountryName" runat="server" />
                                                </p>
                                            </div>
                                        </div>

                                        <ul class="event">
                                            <asp:Repeater ID="FacilitiesRepeater" runat="server">
                                                <ItemTemplate>
                                                    <li>
                                                        <sc:image field="Icon" item="<%#(Item)Container.DataItem %>" runat="server" />
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                    </div>

                                    <div class="col-xs-12 no-padding">
                                        <div class="col-xs-12">
                                            <div class="info-map">
                                                <a href="#">
                                                    <div id="map_canvas" class="gmap gmap-off"></div>
                                                </a>
                                            </div>
                                       </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12">

                                <div class="content_bg_inner_box alert-info">
                                    <p>
                                        <sc:text id="Overview" field="Overview" runat="server" />
                                    </p>
                                </div>                                
                            </div>

                            <div class="col-xs-12">
                                <div class="detail-hotel margin-bottom-15">

                                    <div class="content_bg_inner_box light-box" id="facilities">
                                        <h2>Facilities </h2>
                                        <p>                                            
                                            <sc:text id="Facilities" field="Facility" runat="server" />
                                        </p>
                                    </div>

                                    <div class="content_bg_inner_box light-box" id="goodToKnow">
                                        <h2>Good To Know</h2>
                                        <p>
                                            <sc:text id="GoodToKnow" field="GoodToKnow" runat="server" />                                            
                                        </p>
                                    </div>                                    

                                    <div class="content_bg_inner_box light-box" id="checkInCheckOutTime">
                                        <h2>Check-In Check-Out Time</h2>

                                        Check in: 
                                        <sc:date field="CheckinTime" id="CheckinTime" runat="server" format="HH:mm tt" />
                                        <br />
                                        Check out: 
                                        <sc:date field="CheckoutTime" id="CheckoutTime" runat="server" format="HH:mm tt" />
                                    </div>                                    
                                    
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer width-full inline-block">
                <div class="col-xs-12">
                    <asp:Button runat="server" Text="Close" CssClass="btn btn-default" OnClientClick="closeWin()" UseSubmitBehavior="false" />
                </div>
            </div>
        </div>
    </form>
</body>
</html>

<script type="text/javascript">    

    //Store reference to the google map object
    var map;

    function initialize(location) {

        //Set the required Geo Information
        var latitude = '<%=latitude%>';
        var lognitude = '<%=lognitude%>';

        // Define the coordinates as a Google Maps LatLng Object
        var coords = new google.maps.LatLng(latitude, lognitude);

        //Map initialization variables
        var mapOptions = {
            center: new google.maps.LatLng(latitude, lognitude),            
            zoom: 9,
        };

        map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

        // Place the initial marker
        var marker = new google.maps.Marker({
            position: coords,
            draggable: true,
            map: map,
        });
    }

    $(document).ready(function () {
        if (navigator.geolocation) {
            // Call getCurrentPosition with success and failure callbacks
            initialize();
        }
        else {
            alert("Sorry, your browser does not support geolocation services.");
        }
    });

    function closeWin() {
        window.close();
        return false;
    }

</script>
