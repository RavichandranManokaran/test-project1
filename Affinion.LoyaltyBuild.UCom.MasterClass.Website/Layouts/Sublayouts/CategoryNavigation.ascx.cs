﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.Api;
using UCommerce.EntitiesV2;
using UCommerce.Extensions;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.Layouts.Sublayouts
{
    public partial class CategoryNavigation : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                BindTree(null, CatalogLibrary.GetRootCategories());
        }

        private void BindTree(TreeNode parent, IEnumerable<Category> categories)
        {
            foreach (var category in categories)
            {
                var treeNode = new TreeNode(category.DisplayName())
                {
                    NavigateUrl = "/store/category.aspx?category={0}".FormatWith(category.CategoryId),
                    Expanded = true
                };

                if (parent == null)
                {
                    CategoryTreeView.Nodes.Add(treeNode);
                }
                else
                {
                    parent.ChildNodes.Add(treeNode);
                }

                BindTree(treeNode, category.Categories);
            }
        }
    }
}