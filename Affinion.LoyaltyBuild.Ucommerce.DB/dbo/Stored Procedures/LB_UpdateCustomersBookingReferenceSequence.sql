﻿CREATE PROCEDURE [dbo].[LB_UpdateCustomersBookingReferenceSequence]
	@CustomerId as int,
	@LastSequence as int
AS
BEGIN
	UPDATE [dbo].[LB_CustomerAttribute]
	SET [LastSequence] = @LastSequence
	WHERE CustomerId = @CustomerId
END
