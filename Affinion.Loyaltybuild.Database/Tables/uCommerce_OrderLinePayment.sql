﻿CREATE TABLE [dbo].[uCommerce_OrderLinePayment]
(
	[OrderLinePaymentID] INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[OrderLineID] INT NOT NULL,
	[CurrencyTypeId] INT NOT NULL,
    [PaymentMethodID] INT NOT NULL,
	[ReferenceID] NVARCHAR(MAX) NOT NULL,
	[Amount] MONEY NOT NULL,
	[OrderPaymentID] INT NOT NULL,
	[CommissionAmountType] INT, 
    [DueTypeId] INT NULL


)
Go