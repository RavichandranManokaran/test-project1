﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           JsonCurrency.cs
/// Sub-system/Module:     Suplier Setup(VS project name)
/// Description:           Store Json as a object
/// </summary>
namespace Affinion.LoyaltyBuild.SupplierSetup.CurrencyConverter
{
    /// <summary>
    /// 
    /// </summary>
    public class JsonCurrency
    {
        public decimal Value { get; set; }
        public string Base { get; set; }
        public string Quote { get; set; }
    }
}
