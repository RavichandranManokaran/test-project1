﻿namespace Affinion.LoyaltyBuild.DataAccess.DatabaseScripts
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AffinionScriptData
    {
        public AffinionScriptType ScriptType { get; set; }

        public string ScriptName { get; set; }

        public string Script { get; set; }
    }

    public enum AffinionScriptType
    {
        TableCreation = 0,
        SPCreation
    }
}
