﻿IF NOT EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'SubrateItemCode' AND Object_ID = Object_ID(N'uCommerce_OrderLineDue'))
	ALTER TABLE [uCommerce_OrderLineDue] ADD
		[SubrateItemCode] VARCHAR(50)
GO

IF EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'SubrateId' AND Object_ID = Object_ID(N'uCommerce_OrderLineDue'))
	ALTER TABLE [uCommerce_OrderLineDue] DROP COLUMN [SubrateId]
GO

IF NOT EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'SubrateItemCode' AND Object_ID = Object_ID(N'uCommerce_OrderLineSubRates'))
	ALTER TABLE [uCommerce_OrderLineSubRates] ADD
		[SubrateItemCode] VARCHAR(50)
GO

IF EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'SubrateId' AND Object_ID = Object_ID(N'uCommerce_OrderLineSubRates'))
	ALTER TABLE [uCommerce_OrderLineSubRates] DROP COLUMN [SubrateId]
GO

IF NOT EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'CancellationAmount' AND Object_ID = Object_ID(N'uCommerce_OrderLineDue'))
	ALTER TABLE [uCommerce_OrderLineDue] ADD
		[CancellationAmount] MONEY
GO

IF NOT EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'CreatedBy' AND Object_ID = Object_ID(N'uCommerce_OrderLinePayment'))
	ALTER TABLE dbo.uCommerce_OrderLinePayment ADD
		CreatedBy VARCHAR(50) NULL,
		CreatedDate DATETIME NULL
GO

IF NOT EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'OrderLineID' AND Object_ID = Object_ID(N'uCommerce_OrderLineStatus'))
	ALTER TABLE dbo.uCommerce_OrderLineStatus ADD
		OrderLineID INT NOT NULL
GO

IF NOT EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'DueTypeId' AND Object_ID = Object_ID(N'uCommerce_OrderLinePayment'))
	ALTER TABLE dbo.uCommerce_OrderLinePayment ADD
		DueTypeId INT
GO

IF NOT EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'CommissionAmountType' AND Object_ID = Object_ID(N'uCommerce_OrderLinePayment'))
	ALTER TABLE dbo.uCommerce_OrderLinePayment ADD
		CommissionAmountType INT
GO

IF NOT EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'PaymentMethodId' AND Object_ID = Object_ID(N'uCommerce_OrderLineDue'))
	ALTER TABLE [uCommerce_OrderLineDue]
		ADD PaymentMethodId INT

IF NOT EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'DueTypeId' AND Object_ID = Object_ID(N'ucommerce_OrderLinePaymentDues'))
	ALTER TABLE ucommerce_OrderLinePaymentDues ADD 
		DueTypeId INT
GO

--*****************************************************************************************************************************************************************
--[uCommerce_DueTypes]

IF NOT EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'PaymentDueTypeId' AND Object_ID = Object_ID(N'uCommerce_DueTypes'))
	ALTER TABLE [dbo].[uCommerce_DueTypes]
		ADD [PaymentDueTypeId] INT
GO

--******************************************************************************************************************************************

IF NOT EXISTS(SELECT * FROM sys.columns 
            WHERE Name = N'OrderLineId' AND Object_ID = Object_ID(N'uCommerce_OrderLineStatus'))
	ALTER TABLE [dbo].[uCommerce_OrderLineStatus]
		ADD [OrderLineId] INT  NOT NULL
GO

--**********************************************************************************************************************************************

--*******************************************************************************************************************************************
--FUNCTION
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'affinion_IsPaidInFull') 
    AND xtype IN (N'FN', N'IF', N'TF')
)
BEGIN
	DROP FUNCTION [dbo].[affinion_IsPaidInFull]
END
GO

CREATE FUNCTION [dbo].[affinion_IsPaidInFull]
(
	@OrderLineId int
)
RETURNS BIT
AS
BEGIN
	DECLARE @IspaidInFull BIT
	SET @IspaidInFull = 0
	SELECT TOP 1 @IspaidInFull = 1 FROM uCommerce_OrderProperty OP
		INNER JOIN uCommerce_OrderLine OL ON OL.OrderId = OP.OrderId
	WHERE  
		OL.OrderLineId = @OrderLineId
		AND [Key] = '_ispaidinfull' 
		AND [Value]='true' 

	RETURN @IspaidInFull
END
GO

--**********************************************************************************************************************************************
--STORED PROCEDURES
IF(EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[affinion_AddBookingPayment]') AND type in (N'P', N'PC')))
	DROP PROCEDURE [dbo].[affinion_AddBookingPayment]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Prasanna SR
-- Create date: 12 Feb 2016
-- Description:	Add booking payment
-- =============================================
CREATE PROCEDURE [dbo].[affinion_AddBookingPayment]
	@OrderLineId INT,
	@CurrencyId INT,
	@PaymentMethodId INT,
	@Amount INT,
	@ReferenceId NVARCHAR(MAX),
	@CreatedBy VARCHAR(50),
	@CommissionAmount MONEY = NULL,
	@ProviderAmount MONEY = NULL,
	@ProcessingFee MONEY = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE 
		@OrderId INT,
		@OrderPaymentId INT,
		@OrderLinePaymentId INT
	SELECT @OrderId = OrderId FROM uCommerce_OrderLine WHERE OrderLineId = @OrderLineId

	INSERT INTO [dbo].[uCommerce_Payment]
        ([TransactionId]
        ,[PaymentMethodName]
        ,[Created]
        ,[PaymentMethodId]
        ,[Fee]
        ,[FeePercentage]
        ,[PaymentStatusId]
        ,[Amount]
        ,[OrderId]
        ,[FeeTotal]
        ,[ReferenceId])
     SELECT
        NULL
        ,PM.Name
        ,GETDATE()
        ,@PaymentMethodId
        ,0
        ,0
        ,1
        ,@Amount
        ,@OrderId
        ,0
        ,@ReferenceId
	FROM 
		uCommerce_PaymentMethod PM
	WHERE
		PaymentMethodId = @PaymentMethodId

	SET @OrderPaymentId = SCOPE_IDENTITY()

	IF(@ProviderAmount IS NOT NULL AND @ProviderAmount <> 0)
		INSERT INTO [dbo].[uCommerce_OrderLinePayment]
			([OrderLineID]
			,[CurrencyTypeId]
			,[PaymentMethodID]
			,[ReferenceID]
			,[Amount]
			,[OrderPaymentID]
			,[CreatedBy]
			,[CreatedDate]
			,[DueTypeId])
		 VALUES
			(@OrderLineId
			,@CurrencyId
			,@PaymentMethodId
			,@ReferenceId
			,@ProviderAmount
			,@OrderPaymentId
			,@CreatedBy
			,GETDATE(),
			1)

	IF(@CommissionAmount IS NOT NULL AND @CommissionAmount <> 0)
	BEGIN
		INSERT INTO [dbo].[uCommerce_OrderLinePayment]
			([OrderLineID]
			,[CurrencyTypeId]
			,[PaymentMethodID]
			,[ReferenceID]
			,[Amount]
			,[OrderPaymentID]
			,[CreatedBy]
			,[CreatedDate]
			,[DueTypeId])
		 VALUES
			(@OrderLineId
			,@CurrencyId
			,@PaymentMethodId
			,@ReferenceId
			,@CommissionAmount
			,@OrderPaymentId
			,@CreatedBy
			,GETDATE(),
			2)
	END
	
	IF(@ProcessingFee IS NOT NULL AND @ProcessingFee <> 0)
	BEGIN
		INSERT INTO [dbo].[uCommerce_OrderLinePayment]
			([OrderLineID]
			,[CurrencyTypeId]
			,[PaymentMethodID]
			,[ReferenceID]
			,[Amount]
			,[OrderPaymentID]
			,[CreatedBy]
			,[CreatedDate]
			,[DueTypeId])
		 VALUES
			(@OrderLineId
			,@CurrencyId
			,@PaymentMethodId
			,@ReferenceId
			,@ProcessingFee
			,@OrderPaymentId
			,@CreatedBy
			,GETDATE(),
			3)
	END

	DECLARE @UnMatchedAmount BIT = 1
	IF(@Amount IS NOT NULL AND @Amount>0)
	BEGIN
		DECLARE @Payments TABLE (DueTypeId INT, Amount MONEY)
		INSERT INTO @Payments
		SELECT [DueTypeId], SUM(Amount) FROM
		(
			SELECT [PaymentDueTypeId] DueTypeId, SUM(Amount) [Amount] FROM uCommerce_OrderLineDue OLD 
				INNER JOIN uCommerce_DueTypes DT ON OLD.DueTypeId = DT.DueTypeId
			WHERE 
				OLD.Amount > 0 AND 
				DT.[PaymentDueTypeId] IS NOT NULL AND 
				OLD.OrderLineID IN (SELECT OrderLineID FROM [dbo].[affinion_RelatedOrderLines](@OrderLineId))
			GROUP BY [PaymentDueTypeId]
			UNION
			SELECT DueTypeId, SUM(-Amount) [Amount] FROM uCommerce_OrderLinePayment OLP
			WHERE 
				OLP.Amount > 0 AND 
				OLP.OrderLineID IN (SELECT OrderLineID FROM [dbo].[affinion_RelatedOrderLines](@OrderLineId))
			GROUP BY DueTypeId
		)  Dues
		GROUP BY [DueTypeId]
		HAVING SUM(Amount) > 0

		IF (SELECT SUM(Amount) FROM @Payments) = @Amount
		BEGIN
			SET @UnMatchedAmount = 0

			INSERT INTO [dbo].[uCommerce_OrderLinePayment]
				([OrderLineID]
				,[CurrencyTypeId]
				,[PaymentMethodID]
				,[ReferenceID]
				,[Amount]
				,[OrderPaymentID]
				,[CreatedBy]
				,[CreatedDate]
				,[DueTypeId])
			 SELECT
				@OrderLineId
				,@CurrencyId
				,@PaymentMethodId
				,@ReferenceId
				,Amount
				,@OrderPaymentId
				,@CreatedBy
				,GETDATE()
				,DueTypeId
			FROM @Payments
		END
	END

	IF(@Amount IS NOT NULL AND @Amount<>0 AND (@Amount < 0 OR @UnMatchedAmount = 1))
	BEGIN
		INSERT INTO [dbo].[uCommerce_OrderLinePayment]
			([OrderLineID]
			,[CurrencyTypeId]
			,[PaymentMethodID]
			,[ReferenceID]
			,[Amount]
			,[OrderPaymentID]
			,[CreatedBy]
			,[CreatedDate]
			,[DueTypeId])
		 VALUES
			(@OrderLineId
			,@CurrencyId
			,@PaymentMethodId
			,@ReferenceId
			,@Amount
			,@OrderPaymentId
			,@CreatedBy
			,GETDATE()
			,2)
	END

END

GO
--******************************************************************************************************************************************
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[affinion_AddBookingStatus]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[affinion_AddBookingStatus]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Prasanna SR
-- Create date: Feb 12, 2016
-- Description:	Add booking status
-- =============================================
CREATE PROCEDURE [dbo].[affinion_AddBookingStatus]
	@OrderLineId INT,
	@Status INT = NULL,
	@CreatedBy VARCHAR(50)
AS
BEGIN
	DECLARE @OrderLineStatusId INT, @NewOrderRef VARCHAR(50)
	
	IF (@Status IS NULL)
	BEGIN
		--Confirmed
		SET @Status = 6
		--Cancelled
		SELECT TOP 1 @Status = 7, @NewOrderRef = NewOrderReference FROM uCommerce_RoomReservation WHERE IsCancelled = 1 AND OrderLineId = @OrderLineId
		--Set Provisional
		IF @NewOrderRef IS NULL AND EXISTS (SELECT SUM([Total]) [Total], [Currency] FROM
					(SELECT SUM(OLD.Amount) [Total],
							OLD.CurrencyTypeId [Currency]
					FROM uCommerce_OrderLine  OL
						INNER JOIN uCommerce_OrderLineDue OLD ON OLD.OrderLineID = OL.OrderLineId 
					WHERE
						ISNULL(OLD.IsDeleted,0) = 0 AND
						OLD.OrderLineID IN (SELECT OrderLineID FROM [dbo].[affinion_RelatedOrderLines](@OrderLineId))
					GROUP BY 
						OLD.CurrencyTypeId
					UNION ALL
					SELECT -SUM(OLP.Amount) [Total],
							OLP.CurrencyTypeId [Currency]
					FROM uCommerce_OrderLine  OL
						INNER JOIN uCommerce_OrderLinePayment OLP ON OLP.OrderLineID = OL.OrderLineId 
					WHERE
						OLP.OrderLineID IN (SELECT OrderLineID FROM [dbo].[affinion_RelatedOrderLines](@OrderLineId))
					GROUP BY 
						OLP.CurrencyTypeId) P
				GROUP BY [Currency]
				HAVING
					SUM([Total]) <> 0)
		BEGIN
			SET @Status = 5
		END
	END
	
	--add status log
	INSERT INTO [dbo].[uCommerce_OrderLineStatus]
        ([OrderLineID]
		,[StatusId]
        ,[CreatedBy]
        ,[CreatedDate])
     VALUES
        (@OrderLineId
		,@Status
        ,@CreatedBy
        ,GETDATE())

	SET @OrderLineStatusId = SCOPE_IDENTITY()

	--update current status
	UPDATE uCommerce_RoomReservation
	SET OrderLineStatusID = @OrderLineStatusId 
	WHERE OrderLineId = @OrderLineId
END

GO

--******************************************************************************************************************************************************
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[affinion_AddCancelDue]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[affinion_AddCancelDue]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Prasanna SR
-- Create date: 10 Jan 2016
-- Description:	Calculate and enter due on cancel 
-- =============================================
CREATE PROCEDURE [dbo].[affinion_AddCancelDue] 
	@OrderLineId INT,
	@CreatedBy VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @CancelFee MONEY,
		@SubrateCode VARCHAR(50),
		@CurrencyId INT,
		@CreatedDate DATETIME,
		@PaymentMethodId INT

	SELECT TOP 1 
		@CreatedDate = GETDATE(),
		@SubrateCode = [SubRateItemCode] ,
		@CancelFee = OS.[Amount] - OS.[Discount],
		@CurrencyId = PO.[CurrencyId]
	FROM [uCommerce_OrderLineSubrates] OS
		INNER JOIN [uCommerce_OrderLine] OL ON OS.[OrderLineId] = OL.[OrderLineId]
		INNER JOIN [uCommerce_PurchaseOrder] PO ON OL.[OrderId] = PO.[OrderId]
	WHERE OS.OrderLineId = @OrderLineId 
		AND [SubRateItemCode] IN ('CANFEE','LBCOM')
	ORDER BY [SubRateItemCode]
	
	--Set payment method id
	SELECT TOP 1
		@PaymentMethodId = PaymentMethodId
	FROM [uCommerce_OrderLine] OL
		INNER JOIN [uCommerce_OrderLinePayment] OLP ON OL.[OrderLineId] = OLP.[OrderLineId]
	WHERE OL.OrderLineId = @OrderLineId

	--Set the commision amount due as delete
	UPDATE [dbo].[uCommerce_OrderLineDue]
	SET [SubRateItemCode] = 'CANFEE',
		[DueTypeId] = 4,
		[Amount] = @CancelFee,
		[UpdatedBy] = @CreatedBy,
		[UpdatedDate] = @CreatedDate
	WHERE [OrderLineId] = @OrderLineId
		AND [SubRateItemCode] IN ('LBCOM','TRAFEE')
		
		
	--Add Refund for provider Amount
	IF EXISTS (SELECT 1 WHERE 1 = [dbo].[affinion_IsPaidInFull](@OrderLineId))
	BEGIN
		DECLARE @ProviderAmt MONEY
		SELECT TOP 1 @ProviderAmt = -(OL.Total - OS.Amount + ISNULL(OS.Discount, 0))
		FROM [uCommerce_OrderLineSubrates] OS
			INNER JOIN [uCommerce_OrderLine] OL ON OS.[OrderLineId] = OL.[OrderLineId]
		WHERE OS.OrderLineId = @OrderLineId 
			AND [SubRateItemCode] = 'LBCOM'

		EXEC [affinion_SaveOrderLineDue]
			NULL
			,@OrderLineId
			,''
			,NULL
			,@CurrencyId
			,6
			,@ProviderAmt
			,0
			,0
			,0
			,0
			,0
			,0
			,''
			,@CreatedBy
			,@CreatedDate
			,@PaymentMethodId
	END
END

GO

--***********************************************************************************************************************************************************
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[affinion_AddTranserDue]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[affinion_AddTranserDue]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Prasanna SR
-- Create date: 10 Jan 2016
-- Description:	Calculate and enter due on transfer
-- =============================================
CREATE PROCEDURE [dbo].[affinion_AddTranserDue] 
	@OrderLineId INT,
	@CreatedBy VARCHAR(50),
	@IsFreeTransfer BIT = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE 
		@OrderId INT,
		@BookingRef VARCHAR(50),
		@OldOrderLineId INT,
		@OldDepositFee INT,
		@OldCurrencyId INT,
		@OldProviderAmt MONEY,
		@TransferFee MONEY,
		@SubrateCode VARCHAR(50),
		@CurrencyId INT,
		@CreatedDate DATETIME,
		@CCProFee MONEY,
		@PaymentMethodId INT

	SELECT TOP 1 
		@CreatedDate = GETDATE(),
		@SubrateCode = [SubRateItemCode] ,
		@TransferFee = OS.[Amount] - OS.[Discount],
		@CurrencyId = PO.[CurrencyId],
		@BookingRef = RR.[OrderReference],
		@OrderId = OL.[OrderId]
	FROM [uCommerce_OrderLineSubrates] OS
		INNER JOIN [uCommerce_OrderLine] OL ON OS.[OrderLineId] = OL.[OrderLineId]
		INNER JOIN [uCommerce_RoomReservation] RR ON OL.[OrderLineId] = RR.[OrderLineId]
		INNER JOIN [uCommerce_PurchaseOrder] PO ON OL.[OrderId] = PO.[OrderId]
	WHERE OS.OrderLineId = @OrderLineId 
		AND [SubRateItemCode] IN ('LBCOM','TRAFEE')
	ORDER BY [SubRateItemCode] DESC
	
	--Set payment method id
	SELECT TOP 1
		@PaymentMethodId = PaymentMethodId
	FROM [uCommerce_OrderLine] OL
		INNER JOIN [uCommerce_OrderLinePayment] OLP ON OL.[OrderLineId] = OLP.[OrderLineId]
	WHERE OL.OrderLineId IN (SELECT OrderLineID FROM [dbo].[affinion_RelatedOrderLines](@OrderLineId)) 
	ORDER BY OrderLinePaymentID


	--Insert CC processing fee if exists
	IF EXISTS (SELECT 1 FROM [uCommerce_OrderLineSubrates] WHERE OrderLineID = @OrderLineId AND [SubRateItemCode] ='CCPROFEE')
	BEGIN
		SELECT TOP 1 @CCProFee = ([Amount] - [Discount]) FROM [uCommerce_OrderLineSubrates] WHERE OrderLineID = @OrderLineId AND [SubRateItemCode] ='CCPROFEE'

		EXECUTE [affinion_SaveOrderLineDue]
			NULL
			,@OrderLineId
			,'CCPROFEE'
			,NULL
			,@CurrencyId
			,3
			,@CCProFee
			,0
			,0
			,0
			,0
			,0
			,0
			,''
			,@CreatedBy
			,@CreatedDate
			,@PaymentMethodId
	END

	--Get the deposit fee info for previous item
	SELECT TOP 1 
		@OldOrderLineId = OL.[OrderLineID],
		@OldCurrencyId = PO.[CurrencyId],
		@OldProviderAmt = OL.Total - OS.[Amount] + OS.[Discount],
		@OldDepositFee = OS.[Amount] - OS.[Discount]
	FROM [uCommerce_OrderLineSubrates] OS
		INNER JOIN [uCommerce_OrderLine] OL ON OS.[OrderLineId] = OL.[OrderLineId]
		INNER JOIN [uCommerce_RoomReservation] RR ON OL.[OrderLineId] = RR.[OrderLineId]
		INNER JOIN [uCommerce_PurchaseOrder] PO ON OL.[OrderId] = PO.[OrderId]
	WHERE [SubRateItemCode] ='LBCOM'
		AND RR.[NewOrderReference] = @BookingRef
	
	--inset paid in full status
	INSERT INTO [dbo].[uCommerce_OrderProperty]
		([OrderId]
		,[OrderLineId]
		,[Key]
		,[Value])
	SELECT TOP 1 
		@OrderId
		,NULL
		,[Key]
		,[Value]
	FROM
		uCommerce_OrderLine OL
		INNER JOIN [uCommerce_PurchaseOrder] PO ON OL.OrderId = PO.OrderId
		INNER JOIN [uCommerce_OrderProperty] OP ON OL.OrderId = OP.OrderId
	WHERE 
		OL.[OrderLineId] = @OldOrderLineId AND
		OP.[Key] = '_isPaidinfull'

	--Insert trensfer fee if not free transfer
	IF (@IsFreeTransfer <> 1 AND @TransferFee > 0)
	BEGIN
		UPDATE [dbo].[uCommerce_OrderLineDue]
		SET [SubRateItemCode] = 'TRAFEE',
			[DueTypeId] = 5,
			[UpdatedBy] = @CreatedBy,
			[UpdatedDate] = @CreatedDate
		WHERE [OrderLineId] = @OldOrderLineId
		AND [SubRateItemCode] IN ('LBCOM','TRAFEE')

		EXEC [affinion_SaveOrderLineDue]
			NULL
			,@OrderLineId
			,'LBCOM'
			,NULL
			,@CurrencyId
			,2
			,@TransferFee
			,0
			,0
			,0
			,0
			,0
			,0
			,''
			,@CreatedBy
			,@CreatedDate
			,@PaymentMethodId
	END

	--if paid in full reund/change provider amount
	IF EXISTS (SELECT 1 WHERE 1 = [dbo].[affinion_IsPaidInFull](@OrderLineId))
	BEGIN 
		--Entry for refund due
		DECLARE @ProviderAmt MONEY
		SELECT TOP 1 @ProviderAmt = (OL.Total- OS.Amount + OS.Discount)
		FROM [uCommerce_OrderLineSubrates] OS
			INNER JOIN [uCommerce_OrderLine] OL ON OS.[OrderLineId] = OL.[OrderLineId]
		WHERE OS.OrderLineId = @OrderLineId 
			AND [SubRateItemCode] = 'LBCOM'
		
		DECLARE 
			@CalculatedAmount MONEY,
			@DueType INT

		SELECT @CalculatedAmount = @ProviderAmt - @OldProviderAmt,
			@DueType = CASE WHEN (@ProviderAmt - @OldProviderAmt) > 0 THEN 1 ELSE 6 END

		--set due if calulated amount is not 0
		IF (@CalculatedAmount <> 0)
		BEGIN
			EXEC [affinion_SaveOrderLineDue]
				NULL
				,@OrderLineId
				,''
				,NULL
				,@CurrencyId
				,@DueType
				,@CalculatedAmount
				,0
				,0
				,0
				,0
				,0
				,0
				,''
				,@CreatedBy
				,@CreatedDate
				,@PaymentMethodId
		END
	END
END
GO

--***************************************************************************************************************************************************
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[affinion_GetDueDetail]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[affinion_GetDueDetail]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[affinion_GetDueDetail]
(
	@DueId INT
) 
AS 
BEGIN

	SELECT [OrderLineDueId]
		,[OrderLineID]
		,[SubrateItemCode]
		,[DueTypeId]
		,[CurrencyTypeId]
		,[Amount]
		,[CommissionAmount]
		,[TransferAmount]
		,[ClientAmount]
		,[ProviderAmount]
		,[CollectionByProviderAmount]
		,[Note]
		,[OrderLinePaymentID]
		,[IsDeleted]
		,[CreatedBy]
		,[CreatedDate]
		,[UpdatedBy]
		,[UpdatedDate]
		,[CancellationAmount]
		,[PaymentMethodId]
	FROM 
		[dbo].[uCommerce_OrderLineDue]
	WHERE 
		[OrderLineDueId] = @DueId
END

GO

--**********************************************************************************************************************************************************
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[affinion_GetOfferName]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[affinion_GetOfferName]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[affinion_GetOfferName]
(
@orderlineid int
)
AS
BEGIN
SELECT [CampaignItemName] [OfferName]
	FROM uCommerce_OrderLine OL
		INNER JOIN [dbo].[uCommerce_PurchaseOrder] PO ON PO.OrderId = OL.OrderId
		INNER JOIN [dbo].[uCommerce_OrderLineDiscountRelation] OLD ON OL.OrderLineId = OLD.OrderLineId
		INNER JOIN [dbo].[uCommerce_Discount] D ON OLD.DiscountId = D.DiscountId
	WHERE
		OL.OrderLineId=@orderlineid
END
GO
--*********************************************************************************************************************************************************************
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[affinion_GetPaymentHistory]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[affinion_GetPaymentHistory]
GO

CREATE PROCEDURE [dbo].[affinion_GetPaymentHistory]
	@OrderLineId INT	
AS
BEGIN
	--Get Order line detail
	SELECT OL.OrderLineId, 
		RR.OrderReference 
	FROM uCommerce_OrderLine OL
		INNER JOIN uCommerce_RoomReservation RR ON OL.OrderLineId = RR.OrderLineId 
	WHERE OL.OrderLineId = @OrderLineId

	--Payment History Total
	SELECT SUM([Total]) [Total], [Currency] FROM
		(SELECT SUM(OLD.Amount) [Total],
				OLD.CurrencyTypeId [Currency]
		FROM uCommerce_OrderLine  OL
			INNER JOIN uCommerce_OrderLineDue OLD ON OLD.OrderLineID = OL.OrderLineId 
		WHERE
			ISNULL(OLD.IsDeleted,0) = 0 AND
			OLD.OrderLineID IN (SELECT OrderLineID FROM [dbo].[affinion_RelatedOrderLines](@OrderLineId))
		GROUP BY 
			OLD.CurrencyTypeId
		UNION ALL
		SELECT -SUM(OLP.Amount) [Total],
				OLP.CurrencyTypeId [Currency]
		FROM uCommerce_OrderLine  OL
			INNER JOIN uCommerce_OrderLinePayment OLP ON OLP.OrderLineID = OL.OrderLineId 
		WHERE
			OLP.OrderLineID IN (SELECT OrderLineID FROM [dbo].[affinion_RelatedOrderLines](@OrderLineId))
		GROUP BY 
			OLP.CurrencyTypeId) P
	GROUP BY [Currency]
	HAVING
		SUM([Total]) <> 0

	--PaymentHistory PaymentDues 
	SELECT	OLD.OrderLineDueId [OrderLineDueId],
			OLD.OrderLineID [OrderLineID],
			OLD.CurrencyTypeId [Currency],
			OLD.Amount [Amount],
			(SELECT max(Description) from uCommerce_ClientIDPaymentMethodMapping where clientid = ca.clientid and paymentmethodid = old.paymentmethodid) as [PaymentMethod], 
			--PM.Name [Name],
			DT.[Description] [Reason],
			OLD.CreatedDate [CreatedDate],
			OLD.CreatedBy [CreatedBy],
			DT.CanBeDeleted [CanBeDeleted]
	FROM uCommerce_OrderLineDue OLD 
		INNER JOIN uCommerce_DueTypes DT ON OLD.DueTypeId=dt.DueTypeId		
		inner join ucommerce_orderline ol on ol.orderlineid = old.OrderLineID
		inner join ucommerce_purchaseorder po on po.orderid = ol.OrderId 
		inner join ucommerce_customerattribute ca on ca.customerid = po.customerid
		LEFT JOIN uCommerce_PaymentMethod PM ON OLD.PaymentMethodID = PM.PaymentMethodId
	WHERE 
		ISNULL(OLD.IsDeleted,0) = 0 AND
		OLD.OrderLineID IN (SELECT OrderLineID FROM [dbo].[affinion_RelatedOrderLines](@OrderLineId))
	ORDER BY OLD.CreatedDate

	--Payment History Payments
	SELECT	OLP.OrderLinePaymentID [OrderLinePaymentId],
			OLP.OrderLineID [oderLineID],
			(SELECT max(Description) from uCommerce_ClientIDPaymentMethodMapping where clientid = ca.clientid and paymentmethodid = olp.paymentmethodid) as [PaymentMethod], 
			--PM.Name [Name],
			DT.[Description] [PaymentReason],
			OLP.Amount [Amount],
			OLP.ReferenceId [ReferenceId],
			OLP.CreatedDate [Createddate],
			OLP.CreatedBy [Createdby],
			OLP.CurrencyTypeId [Currency]			
	FROM uCommerce_OrderLinePayment OLP
		INNER JOIN uCommerce_PaymentMethod PM ON PM.PaymentMethodId = OLP.PaymentMethodID
		inner join ucommerce_orderline ol on ol.orderlineid = olp.OrderLineID
		INNER JOIN uCommerce_DueTypes DT ON DT.DueTypeId=OLP.DueTypeId	
		inner join ucommerce_purchaseorder po on po.orderid = ol.OrderId 
		inner join ucommerce_customerattribute ca on ca.customerid = po.customerid
	WHERE
		OLP.OrderLineID IN (SELECT OrderLineID FROM [dbo].[affinion_RelatedOrderLines](@OrderLineId))
	ORDER BY OLP.CreatedDate

	--Discount History
	SELECT [CampaignItemName] [Name], [AmountOffTotal] [Amount], PO.CurrencyId [Currency]
	FROM uCommerce_OrderLine OL
		INNER JOIN [dbo].[uCommerce_PurchaseOrder] PO ON PO.OrderId = OL.OrderId
		INNER JOIN [dbo].[uCommerce_OrderLineDiscountRelation] OLD ON OL.OrderLineId = OLD.OrderLineId
		INNER JOIN [dbo].[uCommerce_Discount] D ON OLD.DiscountId = D.DiscountId
	WHERE
		OL.OrderLineID IN (SELECT OrderLineID FROM [dbo].[affinion_RelatedOrderLines](@OrderLineId))
	ORDER BY OL.CreatedOn
END 
GO
--*********************************************************************************************************************************************************************************************
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[affinion_GetPaymentsWithoutRefunds]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[affinion_GetPaymentsWithoutRefunds]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[affinion_GetPaymentsWithoutRefunds]
	@OrderLineId INT,
	@PaymentTypeId INT,
	@CurrencyTypeId INT		
AS
BEGIN
	--Payment History Payments
	SELECT	OLP.ReferenceId [ReferenceId],
			SUM(OLP.Amount) [Amount],
			SUM(CASE WHEN DueTypeId = 1 THEN OLP.Amount ELSE 0 END) [ProviderAmount],
			SUM(CASE WHEN DueTypeId = 2 THEN OLP.Amount ELSE 0 END) [CommissionAmount],
			SUM(CASE WHEN DueTypeId = 3 THEN OLP.Amount ELSE 0 END) [ProcessingFee],
			MIN(OLP.CreatedDate) [Createddate]
	FROM uCommerce_OrderLinePayment OLP
	WHERE
		OLP.PaymentMethodID IN (SELECT B.PaymentMethodID FROM uCommerce_ClientIDPaymentMethodMapping A
			INNER JOIN uCommerce_ClientIDPaymentMethodMapping B ON A.[Description] = B.[Description]
			WHERE a.[PaymentMethodId] = @PaymentTypeId) AND
		OLP.CurrencyTypeId = @CurrencyTypeId AND
		OLP.OrderLineID IN (SELECT OrderLineID FROM [dbo].[affinion_RelatedOrderLines](@OrderLineId))
	GROUP BY  OLP.ReferenceId
END 
GO

--***********************************************************************************************************************************************************************************************************
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[affinion_ProcessOLDueAndOLPayments]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[affinion_ProcessOLDueAndOLPayments]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
/*
*/ 
CREATE PROCEDURE [dbo].[affinion_ProcessOLDueAndOLPayments]
(	 
	@OrderId INT=NULL,
	@IsPartialPayment BIT = 1	 
)
AS 
BEGIN
	IF (@OrderId IS NOT NULL)
	BEGIN	
		--Inserting data into ucommerce_orderlinedue based ON 'LBCOM'					
		INSERT INTO ucommerce_orderlinedue 
		(
			OrderLineID, 
			DueTypeId, 
			CurrencyTypeId, 
			Amount, 
			CommissionAmount, 
			TransferAmount,
			ClientAmount, 
			ProviderAmount, 
			CollectionByProviderAmount,
			Note, 
			OrderLinePaymentID, 
			IsDeleted, 
			CreatedBy, 
			CreatedDate, 
			UpdatedBy, 
			UpdatedDate,
			CancellationAmount, SubrateItemCode
		)
		SELECT 
			OLS.OrderLineID AS [OrderLineID],
			DT.DueTypeId AS [DueTypeId], 
			PO.CurrencyId AS [CurrencyTypeId],
			OLS.Amount-ISNULL(OLS.Discount,0) AS [Amount],
			0 AS [CommissionAmount],
			0 AS [TransferAmount],
			0 AS [ClientAmount],
			0 AS [ProviderAmount],
			0 AS [CollectionByProviderAmount],
			'' AS [Note],
			NULL AS [OrderLinePaymentID],
			0 AS [IsDeleted],
			OL.CreatedBy AS [CreatedBy],
			OL.CreatedOn AS [CreatedDate],
			'' AS [UpdatedBy],
			GETDATE() AS [UpdatedDate], 
			0 AS [CancellationAmount],
			OLS.SubrateItemCode AS [SubrateItemCode]
		FROM 
			uCommerce_OrderLineSubrates OLS
			INNER JOIN ucommerce_duetypes DT ON ols.SubrateItemCode = DT.SubrateItemCode  
			INNER JOIN ucommerce_orderline OL ON ol.OrderLineId = OLS.OrderLineID		
			INNER JOIN uCommerce_PurchaseOrder PO ON po.OrderId = OL.OrderId
		WHERE 
			OLS.SubrateItemCode = 'LBCOM' 
			AND Po.OrderId  = @OrderId

		--this is a full payment (param name need to be reverted
		IF (@IsPartialPayment = 0)
		BEGIN
			INSERT INTO ucommerce_orderlinedue 
			(
				OrderLineID, 
				DueTypeId, 
				CurrencyTypeId, 
				Amount, 
				CommissionAmount, 
				TransferAmount,
				ClientAmount, 
				ProviderAmount, 
				CollectionByProviderAmount,
				Note, 
				OrderLinePaymentID, 
				IsDeleted, 
				CreatedBy, 
				CreatedDate, 
				UpdatedBy, 
				UpdatedDate,
				CancellationAmount, SubrateItemCode
			)
			SELECT 
				OL.OrderLineID AS [OrderLineID],
				1 AS [DueTypeId], 
				PO.CurrencyId AS [CurrencyTypeId],
				OL.Total-OLS.Amount+ISNULL(OLS.Discount,0)  AS [Amount],
				0 AS [CommissionAmount],
				0 AS [TransferAmount],
				0 AS [ClientAmount],
				0 AS [ProviderAmount],
				0 AS [CollectionByProviderAmount],
				'' AS [Note],
				NULL AS [OrderLinePaymentID],
				0 AS [IsDeleted],
				OL.CreatedBy AS [CreatedBy],
				OL.CreatedOn AS [CreatedDate],
				'' AS [UpdatedBy],
				GETDATE() AS [UpdatedDate], 
				0 AS [CancellationAmount],
				'' AS [SubrateItemCode]
			FROM 
				uCommerce_OrderLineSubrates OLS
				INNER JOIN ucommerce_duetypes DT ON ols.SubrateItemCode = DT.SubrateItemCode  
				INNER JOIN ucommerce_orderline OL ON ol.OrderLineId = OLS.OrderLineID		
				INNER JOIN uCommerce_PurchaseOrder PO ON po.OrderId = OL.OrderId
			WHERE 
				OLS.SubrateItemCode = 'LBCOM' 
				AND Po.OrderId  = @OrderId
		END

		--Inserting data into ucommerce_orderlinedue based ON 'CCPROFEE'
		INSERT INTO ucommerce_orderlinedue 
		(
			OrderLineID, 
			DueTypeId, 
			CurrencyTypeId, 
			Amount, 
			CommissionAmount, 
			TransferAmount,
			ClientAmount, 
			ProviderAmount, 
			CollectionByProviderAmount,
			Note, 
			OrderLinePaymentID, 
			IsDeleted, 
			CreatedBy, 
			CreatedDate, 
			UpdatedBy, 
			UpdatedDate,
			CancellationAmount, SubrateItemCode
		)
		SELECT TOP 1 
			OLS.OrderLineID AS [OrderLineID],
			DT.DueTypeId AS [DueTypeId], 
			PO.CurrencyId AS [CurrencyTypeId],
			OLS.Amount AS [Amount],
			0 AS [CommissionAmount],
			0 AS [TransferAmount],
			0 AS [ClientAmount],
			0 AS [ProviderAmount],
			0 AS [CollectionByProviderAmount],
			'' AS [Note],
			NULL AS [OrderLinePaymentID],
			0 AS [IsDeleted],
			OL.CreatedBy AS [CreatedBy],
			OL.CreatedOn AS [CreatedDate],
			'' AS [UpdatedBy],
			GETDATE() AS [UpdatedDate], 
			0 AS [CancellationAmount],
			OLS.SubrateItemCode AS [SubrateItemCode]
		FROM 
			uCommerce_OrderLineSubrates OLS
			INNER JOIN ucommerce_duetypes DT ON ols.SubrateItemCode = DT.SubrateItemCode  
			INNER JOIN ucommerce_orderline OL ON ol.OrderLineId = OLS.OrderLineID		
			INNER JOIN uCommerce_PurchaseOrder PO ON po.OrderId = OL.OrderId
		WHERE  
			OLS.SubrateItemCode = 'CCPROFEE'  AND 
			OLS.OrderLineID in (Select orderlineid from ucommerce_orderline where orderid = @OrderId)
		ORDER BY ols.Amount DESC
		
		--Inserting data into ucommerce_orderlinepayment 
		INSERT INTO ucommerce_orderlinepayment
		(
			OrderLineID, 
			CurrencyTypeId, 
			PaymentMethodID, 
			Amount, 
			OrderPaymentID, 
			CreatedDate, 
			ReferenceID, 
			CreatedBy,
			DueTypeId
		)
		SELECT 
			ol.orderlineid AS [OrderLineID], 
			po.CurrencyId AS [CurrencyTypeId],
			p.PaymentMethodId AS [PaymentMethodID],
			old.Amount AS [Amount],
			p.PaymentId AS [OrderPaymentID],
			p.Created AS [CreatedDate],
			p.ReferenceId AS [ReferenceID], 
			ol.CreatedBy  AS [CreatedBy],
			old.DueTypeId
		FROM  uCommerce_OrderLineDue old 
			INNER JOIN ucommerce_orderline ol ON old.orderlineid = ol.orderlineid 			
			INNER JOIN uCommerce_PurchaseOrder po ON po.OrderId = ol.OrderId
			INNER JOIN ucommerce_payment p ON p.orderid = po.orderid
		WHERE
			PO.orderid = @OrderId

		UPDATE oldue SET oldue.PaymentMethodId = olp.PaymentMethodID
		FROM ucommerce_orderlinedue oldue, ucommerce_orderlinepayment olp
		WHERE oldue.orderlineid = olp.OrderLineID 
			AND oldue.OrderLineID in (Select orderlineid from ucommerce_orderline where orderid = @OrderId)
			AND oldue.OrderLinePaymentID is null

		--Orderline status date update
		declare @orderlineids table(orderlineid int, isupdated bit)
		insert into @orderlineids select orderlineid, 0 from uCommerce_OrderLine where orderid = @OrderId
		declare @currentorderlineid int
		while(select COUNT(1) from @orderlineids where isupdated = 0) > 0
		begin
			select top 1 @currentorderlineid = orderlineid from @orderlineids where isupdated = 0
			--call update status sp here
			exec [affinion_AddBookingStatus] @currentorderlineid, null, ''
		update @orderlineids set isupdated = 1 where orderlineid = @currentorderlineid
		end

		--update entry for special request in reservation notes
		insert into uCommerce_ReservationNotes
			select 3 as NoteTypeId, rr.OrderLineId, rr.SpecialRequest, getdate() as CreatedDate, '' as CreatedBy from uCommerce_RoomReservation rr
			inner join ucommerce_orderline ol on ol.orderlineid = rr.orderlineid
			inner join ucommerce_purchaseorder po on po.orderid = ol.OrderId 
			where po.orderid = @OrderId
			and ISNULL(rr.specialrequest,'') <> '' 
			and not exists (select 1 from uCommerce_ReservationNotes where OrderLineId = rr.OrderLineId and notetypeid = 3)
	END
END
GO

--************************************************************************************************************************************************************************************************
IF(EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[affinion_SaveOrderLineDue]') AND type in (N'P', N'PC')))
	DROP PROCEDURE [dbo].[affinion_SaveOrderLineDue]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[affinion_SaveOrderLineDue] 
(
    -- Add the parameters for the stored procedure here
	@OrderLineDueId INT = NULL,
	@OrderLineID INT,
    @SubrateItemCode VARCHAR(50),
	@OrderLinePaymentID INT,
	@CurrencyTypeId INT,
    @DueTypeId INT,
    @Amount MONEY ,
    @CommissionAmount MONEY ,
    @TransferAmount MONEY ,
	@CancellationAmount MONEY,
    @ClientAmount MONEY ,
    @ProviderAmount MONEY,
    @CollectionByProviderAmount MONEY ,
    @Note VARCHAR(MAX),
	@CreatedBy VARCHAR(50),
	@CreatedDate DATETIME,
	@PaymentMethodId INT
)
AS
BEGIN
	DECLARE @Result INT

	IF(@OrderLineDueId IS NULL)
	BEGIN
		--Insert a new Due
		INSERT INTO [dbo].[uCommerce_OrderLineDue]
			(
			[OrderLineID]
			,[SubrateItemCode]
			,[OrderLinePaymentID]
			,[CurrencyTypeId]
			,[DueTypeId]
			,[Amount]
			,[CommissionAmount]
			,[TransferAmount]
			,[CancellationAmount]
			,[CollectionByProviderAmount]
			,[ClientAmount]
			,[ProviderAmount]
			,[Note]
			,[CreatedDate]
			,[IsDeleted]
			,[PaymentMethodId]
			)
		VALUES
			(
			@OrderLineID,
			@SubrateitemCode,
			@OrderLinePaymentID,
			@CurrencyTypeId,
			@DueTypeId,
			@Amount,
			@CommissionAmount,
			@TransferAmount,
			@CancellationAmount,
			@ClientAmount, 
			@ProviderAmount,
			@CollectionByProviderAmount,
			@Note, 
			GETDATE(),
			0,
			@PaymentMethodId
			)
	
		SET @Result=SCOPE_IDENTITY();
	END
	ELSE
	BEGIN
		--Update Due
		UPDATE [dbo].uCommerce_OrderLineDue SET
			OrderLineId=@OrderLineID,
			DueTypeId=@DueTypeId,
			CurrencyTypeId=@CurrencyTypeId,
			Amount=@Amount,
			CommissionAmount=@CommissionAmount,
			TransferAmount=@TransferAmount,
			ClientAmount=@ClientAmount,
			ProviderAmount=@ProviderAmount,
			Note=@Note,
			UpdatedBy=@CreatedBy,
			UpdatedDate=GETDATE(),
			PaymentMethodID = @PaymentMethodId
		WHERE
			OrderLineDueId = @OrderLineDueId

		SET @Result =@OrderLineDueId;
	END
	
	--Return ID
	SELECT @Result
END

GO

--********************************************************************************************************************************************************************************************
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[affinion_TransferBooking]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[affinion_TransferBooking]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[affinion_TransferBooking] 
	-- Add the parameters for the stored procedure here
	@Note NVARCHAR(MAX), 
	@OrderLineId INT,
	@NoOfAdults INT,
	@NoOfChildren INT,
	@ChildrenAgeInfo VARCHAR(50),
	@CartId VARCHAR(50),
	@CreatedBy VARCHAR(50),
	@NewOrderLineId INT OUT
AS
BEGIN
	
	DECLARE @OrderReference VARCHAR(50)
	DECLARE @NewOrderId INT
	DECLARE @OrderId INT
	DECLARE @CustomerId INT

	SELECT @OrderId = PO.OrderId, @CustomerId = PO.CustomerId 
	FROM uCommerce_OrderLine OL
		INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId = PO.OrderId
	WHERE OrderLineId = @OrderLineId

	--get order reference
	SELECT @OrderReference = ISNULL(BookingReferenceId,'') + '-' + CAST((ISNULL(LastSequence,0)+1) AS VARCHAR(5))
		FROM uCommerce_CustomerAttribute
		WHERE CustomerId = @CustomerId
	UPDATE uCommerce_CustomerAttribute SET LastSequence = (ISNULL(LastSequence,0)+1) WHERE CustomerId = @CustomerId

	SELECT TOP 1  
		@NewOrderLineId =  RR.OrderLineId,
		@NewOrderId = PO.OrderId
	FROM
		uCommerce_RoomReservation RR 
		INNER JOIN uCommerce_OrderLine OL ON RR.OrderLineId = OL.OrderLineId
		INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId = PO.OrderId
	WHERE PO.BasketId = @CartId
	
	--Update no of adults and child and order reference
	UPDATE uCommerce_RoomReservation SET OrderReference = @OrderReference, NoOfAdult = @NoOfAdults, NoOfChild = @NoOfChildren WHERE OrderLineId = @NewOrderLineId


	-- Insert statements for procedure here	 
	INSERT INTO uCommerce_ReservationNotes 
		(
		NoteTypeId,
		OrderLineId,
		Note,
		CreatedBy,
		CreatedDate
		) 
	VALUES 
		(
		1,
		@OrderLineId,
		@Note,
		@CreatedBy,
		GETDATE()
		);

	--Update Customer
	UPDATE uCommerce_PurchaseOrder SET CustomerId = @CustomerId WHERE OrderId = @NewOrderId

	--insert order address
	INSERT INTO [dbo].[uCommerce_OrderAddress]
		([FirstName]
		,[LastName]
		,[EmailAddress]
		,[PhoneNumber]
		,[MobilePhoneNumber]
		,[Line1]
		,[Line2]
		,[PostalCode]
		,[City]
		,[State]
		,[CountryId]
		,[Attention]
		,[CompanyName]
		,[AddressName]
		,[OrderId]
		,[Line3]
		,[County]
		,[Branch]
		,[IsEmail]
		,[IsTerms])
	SELECT [FirstName]
		,[LastName]
		,[EmailAddress]
		,[PhoneNumber]
		,[MobilePhoneNumber]
		,[Line1]
		,[Line2]
		,[PostalCode]
		,[City]
		,[State]
		,[CountryId]
		,[Attention]
		,[CompanyName]
		,[AddressName]
		,@NewOrderId
		,[Line3]
		,[County]
		,[Branch]
		,[IsEmail]
		,[IsTerms]
	FROM [dbo].[uCommerce_OrderAddress]
	WHERE OrderId = @OrderId

			
	UPDATE uCommerce_RoomReservation 
	SET isCancelled = 1, NewOrderReference = @OrderReference 
	WHERE OrderLineId = @OrderlineId


	IF(@ChildrenAgeInfo IS NOT NULL and @ChildrenAgeInfo <> '')
	BEGIN
		DECLARE @xml as xml
		SET @xml = cast(('<X>'+replace(@ChildrenAgeInfo,',' ,'</X><X>')+'</X>') as xml)
		
		INSERT INTO [dbo].[uCommerce_RoomReservationAgeInfo] ([RoomReservationId], [OrderLineId], [Age])
		SELECT RR.RoomReservationId, RR.OrderLineId, N.value('.', 'varchar(10)') as value FROM @xml.nodes('X') as T(n)
		CROSS JOIN uCommerce_RoomReservation RR
		WHERE RR.OrderLineId = @NewOrderLineId
	END

	--Cancelling for existing OrderLineId
	EXEC [affinion_UpdateRoomAvailability] @OrderLineId, 1

	--Adding for new OrderLineId
	EXEC [affinion_UpdateRoomAvailability] @NewOrderLineId, 0

	UPDATE uCommerce_PurchaseOrder
	SET BasketId = '00000000-0000-0000-0000-000000000000'
	WHERE BasketId = @CartId
END
GO

--*************************************************************************************************************************************************************************************************
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetCustomerQuery]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[GetCustomerQuery]
GO
-- =============================================
--	Name		: GetCustomerQuery
--	Purpose		: To get the GetCustomerQuery
--	Author		: Antonysamy
--	Date		:	11/02/2016
-- =============================================

CREATE PROCEDURE [dbo].[GetCustomerQuery]
	(
	@OrderLineId	INT
	)
AS

BEGIN
SELECT   Q.QueryId
		,Q.QueryTypeId
		,Q.OrderLineId
		,Q.CustomerId
		,Q.Content
		,ISNULL(Q.IsSolved,0) IsSolved
		,ISNULL(PartnerId,0)
		,ISNULL(CampaignId,0)
		,ISNULL(ProviderId,0)
		,Q.CreatedDate
		,Q.CreatedBy
		,Q.UpdatedDate
		,Q.UpdatedBy
FROM uCommerce_CustomerQuery Q 
WHERE OrderLineId = @OrderLineId
Order by Q.QueryId desc
END
GO
--*****************************************************************************************************************************************************************************
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPostBreakEmailDetails]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[GetPostBreakEmailDetails]
GO
create PROCEDURE [dbo].[GetPostBreakEmailDetails]
(@ClientId AS varchar(50))
AS
	SELECT RR.CheckOutDate, OA.EmailAddress, CA.ClientId
	FROM   uCommerce_OrderLine AS O INNER JOIN
           uCommerce_PurchaseOrder AS P ON O.OrderId = P.OrderId INNER JOIN
           uCommerce_OrderAddress AS OA ON OA.OrderId = O.OrderId INNER JOIN
           uCommerce_RoomReservation AS RR ON O.OrderLineId = RR.OrderLineId INNER JOIN
           uCommerce_CustomerAttribute AS CA ON P.CustomerId = CA.CustomerId
	WHERE  (CA.ClientId = @ClientId) AND P.OrderStatusId=6

GO
--*************************************************************************************************************************************************************************************
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PurgeExistingBookings]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[PurgeExistingBookings]
GO
/*<summary>
Description: Stored procedure to purge Existing Bookings which has arrival date older than one year
</summary>*/
CREATE PROCEDURE [dbo].[PurgeExistingBookings]

	AS

	DECLARE @orderIdToDelete int
	DECLARE @orderLineIdToDelete int
	DECLARE @activeOrderLines int
	
	-- Select the Order Ids to be deleted (Arrival date older than one year)
	SELECT DISTINCT PO.OrderId into #orderIdTemp 
	  FROM [dbo].[uCommerce_PurchaseOrder] PO
	  Inner JOIN [dbo].[uCommerce_OrderLine] OL ON OL.OrderId = PO.OrderId 
	  Inner JOIN [dbo].uCommerce_RoomReservation RR ON RR.OrderLineId = OL.OrderLineId 
	  WHERE RR.CheckInDate < dateadd(year,-1,getdate())				--dateadd(dd,-23,getdate())

	WHILE exists (SELECT * from #orderIdTemp)
	BEGIN
				
		SELECT @orderIdToDelete = (SELECT top 1 orderid
						   FROM #orderIdTemp)

		SELECT @activeOrderLines = (SELECT count(RR.OrderLineId) 
									FROM [dbo].uCommerce_RoomReservation RR 
									Inner JOIN [dbo].uCommerce_OrderLine OL ON OL.OrderLineId = RR.OrderLineId 
									WHERE OL.OrderId = @orderIdToDelete 
									AND RR.CheckInDate >= dateadd(dd,-12,getdate())) --Set this to one year

		BEGIN TRANSACTION
			IF (@activeOrderLines = 0)
			/*  Order line items belonging to order is older than one year. Can delete the order line */
			BEGIN
				/* Get order lines to delete */
				SELECT DISTINCT OL.OrderLineId into #orderLineIdTemp 
				FROM [dbo].uCommerce_OrderLine OL 
				WHERE OL.OrderId = @orderIdToDelete

				While exists (Select * FROM #orderLineIdTemp)
				BEGIN
			
					SELECT @orderLineIdToDelete = (select top 1 OrderLineId
								   FROM #orderLineIdTemp)
			
					/* Delete from uCommerce_CommunicationHistory table */
					DELETE FROM [dbo].[uCommerce_CommunicationHistory] 
					WHERE OrderLineId = @orderLineIdToDelete

					/* Delete from uCommerce_CustomerQuery table */
					DELETE FROM [dbo].[uCommerce_CustomerQuery] 
					WHERE OrderLineId = @orderLineIdToDelete

					/* Delete from uCommerce_OrderLineDiscountRelation table */
					DELETE FROM [dbo].[uCommerce_OrderLineDiscountRelation] 
					WHERE OrderLineId = @orderLineIdToDelete

					/* Delete from uCommerce_OrderLineStatus table */
					DELETE FROM [dbo].[uCommerce_OrderLineStatus] 
					WHERE OrderLineId = @orderLineIdToDelete

					/* Delete from uCommerce_OrderProperty table */
					DELETE FROM [dbo].[uCommerce_OrderProperty] 
					WHERE OrderLineId = @orderLineIdToDelete

					/* Delete from uCommerce_ReservationNotes table */
					DELETE FROM [dbo].[uCommerce_ReservationNotes] 
					WHERE OrderLineId = @orderLineIdToDelete

					/* Delete from uCommerce_RoomReservation table */
					DELETE FROM [dbo].uCommerce_RoomReservation 
					WHERE OrderLineId = @orderLineIdToDelete

					/* Delete from uCommerce_OrderLine table */
					DELETE FROM [dbo].uCommerce_OrderLine 
					WHERE OrderLineId = @orderLineIdToDelete

					/* Delete from #orderLineIdTemp table */
					DELETE FROM #orderLineIdTemp
					WHERE OrderLineId = @orderLineIdToDelete
				END --End of while

				--Drop Temp Tables
				Drop Table #orderLineIdTemp
				
			/* Delete from uCommerce_OrderAddress table */
			DELETE FROM [dbo].[uCommerce_OrderAddress] 
			WHERE OrderId = @orderIdToDelete

			/* Delete from uCommerce_OrderStatusAudit table */
			DELETE FROM [dbo].[uCommerce_OrderStatusAudit] 
			WHERE OrderId = @orderIdToDelete

			/* Delete from Payment Property table all paymet property rows belonging to Payment done for the Order*/
			DELETE FROM [dbo].[uCommerce_PaymentProperty] 
			WHERE PaymentId in (Select PaymentId FROM uCommerce_Payment
								Where OrderId = @orderIdToDelete)
			
			
			/* Delete from uCommerce_Payment table */
			DELETE FROM [dbo].[uCommerce_Payment] 
			WHERE OrderId = @orderIdToDelete
			
			/* Delete from uCommerce_OrderProperty table */
			DELETE FROM [dbo].[uCommerce_OrderProperty] 
			WHERE OrderId = @orderIdToDelete
			
			/* Delete from uCommerce_PurchaseOrder table */
			DELETE FROM [dbo].[uCommerce_PurchaseOrder] 
			WHERE OrderId = @orderIdToDelete

			DELETE FROM #orderIdTemp
			WHERE OrderId = @orderIdToDelete

		END	--End If

		-- Rollback the transaction if there were any errors
		IF @@ERROR <> 0
		 BEGIN
			--Drop Temp Tables
			Drop Table #orderLineIdTemp
			Drop Table #orderIdTemp

			-- Rollback the transaction
			ROLLBACK

			-- Raise an error and return
			DECLARE @msg nvarchar(2048) = error_message()
			RAISERROR (@msg, 16, 1)
			RETURN
		 END

		--  Commit the deletion for selected Order Id
		COMMIT TRANSACTION
		
	END	--End While

	--Drop Temp Table
	 Drop Table #orderIdTemp

  GO
  --*************************************************************************************************************************************************************************************
 IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SearchCustomer]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[SearchCustomer]
GO

  Create PROCEDURE [dbo].[SearchCustomer](
	@FirstName nvarchar(512),
	@LastName nvarchar(512),
	@PhoneNumber nvarchar(50) = null,
	@AddressLine1 nvarchar(50)=null,
	@AddressLine2 nvarchar(50)=Null,
	@CountryId int = null,
	@LocationId int =null,
	@ClientId varchar(50)= null
)

AS

begin

SELECT  C.CustomerId FROM  [dbo].uCommerce_Customer  C
INNER JOIN  [dbo].uCommerce_CustomerAttribute CA
ON	C.CustomerId = CA.CustomerId 
WHERE	(@FirstName	IS NULL OR C.FirstName Like @FirstName + '%')
	AND (@LastName IS NULL OR C.LastName Like @LastName + '%') 
	AND (isnull(@PhoneNumber ,'')= '' OR  ( C.MobilePhoneNumber= @PhoneNumber))
	AND (isnull(@AddressLine1,'') ='' OR CA.AddressLine1 Like @AddressLine1 + '%') 
	AND (isnull(@AddressLine2,'') =''	 OR CA.AddressLine2 Like @AddressLine2 + '%')
	AND (isnull(@CountryId,0)	 = 0 OR (CA.CountryId = @CountryId))
	AND (isnull(@LocationId,0) = 0 OR (CA.LocationId = @LocationId))
	AND  (isnull(@ClientId,'') ='' OR (CA.ClientId = @ClientId))

end

Go

--***************************************************************************************************************************************************************************************
 IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SearchCustomerQuery]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[SearchCustomerQuery]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
/*<summary>
Description: Stored procedure to retrieve Existing Booking details for the given search criteria
<param name="@queryTypeID">querytype id</param>
<param name="@clientIds">Client Id or ',' seperated Id list</param>
<param name="@checkinDate">Booking Created Date</param>
<param name="@checkoutDate">Customer Arrival Date</param>
<param name="@firstname">Customer First Name</param>
<param name="@lastname">Customer Last Name</param>
<param name="@compaignGroupId"> Compaign Group Id</param>
<param name="@providerName">Hotel Name</param>
<param name="@bookingReference">Customer Booking reference Id</param>
<param name="@QueryStatusId">Current Order Status Id</param>
</summary>*/
CREATE PROCEDURE [dbo].[SearchCustomerQuery]
(	   
	   @queryTypeID int =NULL,
	   @clientId varchar(50) =NULL, 
	   @startDate Date=NULL, 
       @endDate Date=NULL,
	   @firstname nvarchar(512)=NULL, 
       @lastname nvarchar(512)=NULL, 
	   @campaignGroupId nvarchar(50) = NULL,
       @providerId nvarchar(50)=NULL, 
       @bookingReference nvarchar(50)=NULL,
	   @isSolved bit=NULL)
AS 
BEGIN
	SELECT 
	    CQ.QueryId,
	    CQ.Content,
		CA.ClientId,
		C.Name [ProviderName], -- provider name 
		CUS.FirstName,
		CUS.LastName, 
		CUS.EmailAddress,
		isnull(CUS.FirstName,'')+ ' ' + isnull(CUS.LastName,'') +'  ' + isnull(CA.AddressLine1,'') + ' ' + isnull(CA.AddressLine2,'') [Name],
		isnull(CUS.PhoneNumber,MobilePhoneNumber) PhoneNumber,
		CA.BookingReferenceId BookingReference,
		CQ.IsSolved,
		CQ.CreatedDate,
		CQ.UpdatedDate,
	    ( Select Count(1) from uCommerce_CustomerQuery_Attachments where QueryId = CQ.QueryId) HasAttachments
	FROM [dbo]. uCommerce_CustomerQuery CQ
		INNER JOIN [dbo].uCommerce_OrderLine OL ON OL.OrderLineId = CQ.OrderLineId
		INNER JOIN [dbo].uCommerce_PurchaseOrder PO ON OL.OrderId = PO.OrderId
		INNER JOIN [dbo].uCommerce_RoomReservation RR ON OL.OrderLineId = RR.OrderLineId 
		INNER JOIN [dbo].uCommerce_Product P ON OL.Sku = P.Sku
		INNER JOIN [dbo].uCommerce_CategoryProductRelation CPR ON P.ProductId = CPR.ProductId 
		INNER JOIN [dbo].uCommerce_Category C ON CPR.CategoryId = C.CategoryId
		INNER JOIN [dbo].uCommerce_Customer CUS ON PO.CustomerId = CUS.CustomerId 
		INNER JOIN [dbo].uCommerce_CustomerAttribute CA ON PO.CustomerId = CA.CustomerId 
	--	LEFT JOIN [dbo].[uCommerce_OrderLineCampaignRelation] OCR ON OL.OrderId = OCR.OrderLineId
	--	LEFT JOIN [dbo].[uCommerce_CampaignItem] CI ON OCR.CampaignItemId = CI.CampaignItemId
	--	LEFT JOIN [dbo].[uCommerce_Campaign] CM ON CM.CampaignId = CI.CampaignId
	WHERE (@firstname IS NULL OR CUS.FirstName Like @firstname + '%')
		AND (@lastname IS NULL OR CUS.LastName Like @lastname + '%') 
		AND (@clientId IS NULL OR CA.ClientId = @clientId)
		AND (@providerId IS NULL OR c.[Guid] = @providerId)
		AND (CONVERT(VARCHAR(11),CQ.CreatedDate,111) BETWEEN ISNULL(@startDate,'1900-1-1') AND ISNULL(@endDate, '2099-1-1'))
		AND (@bookingReference IS NULL OR CA.BookingReferenceId = @bookingReference)
		AND (@IsSolved IS NULL OR @IsSolved = CQ.IsSolved)
		AND (@campaignGroupId IS NULL OR @campaignGroupId = CQ.CampaignId)
		AND (@queryTypeID IS NULL OR @queryTypeID = CQ.QueryTypeId)
		AND CQ.OrderLineId > 0

union
SELECT  
		
		CQ.QueryId,
	    CQ.Content,
		CA.ClientId,
		'' [ProviderName], -- provider name 
		CUS.FirstName,
		CUS.LastName, 
		CUS.EmailAddress,
		isnull(CUS.FirstName,'')+ ' ' + isnull(CUS.LastName,'') +'  ' + isnull(CA.AddressLine1,'') + ' ' + isnull(CA.AddressLine2,'') [Name],
		isnull(CUS.PhoneNumber,MobilePhoneNumber) PhoneNumber,
		CA.BookingReferenceId BookingReference,
		CQ.IsSolved,
		CQ.CreatedDate,
		CQ.UpdatedDate,
	    ( Select Count(1) from uCommerce_CustomerQuery_Attachments where QueryId = CQ.QueryId) HasAttachments
	FROM [dbo]. uCommerce_CustomerQuery CQ
		--INNER JOIN [dbo].uCommerce_OrderLine OL ON OL.OrderLineId = CQ.OrderLineId
		--INNER JOIN [dbo].uCommerce_PurchaseOrder PO ON OL.OrderId = PO.OrderId
		--INNER JOIN [dbo].uCommerce_RoomReservation RR ON OL.OrderLineId = RR.OrderLineId 
		--INNER JOIN [dbo].uCommerce_Product P ON OL.Sku = P.Sku
		--INNER JOIN [dbo].uCommerce_CategoryProductRelation CPR ON P.ProductId = CPR.ProductId 
		--INNER JOIN [dbo].uCommerce_Category C ON CPR.CategoryId = C.CategoryId
		INNER JOIN [dbo].uCommerce_Customer CUS ON CQ.CustomerId = CUS.CustomerId 
		INNER JOIN [dbo].uCommerce_CustomerAttribute CA ON CUS.CustomerId = CA.CustomerId 
	--	LEFT JOIN [dbo].[uCommerce_OrderLineCampaignRelation] OCR ON OL.OrderId = OCR.OrderLineId
	--	LEFT JOIN [dbo].[uCommerce_CampaignItem] CI ON OCR.CampaignItemId = CI.CampaignItemId
	--	LEFT JOIN [dbo].[uCommerce_Campaign] CM ON CM.CampaignId = CI.CampaignId
	WHERE (@firstname IS NULL OR CUS.FirstName Like @firstname + '%')
		AND (@lastname IS NULL OR CUS.LastName Like @lastname + '%') 
		AND (@clientId IS NULL OR CQ.PartnerId = @clientId)
		AND (@providerId IS NULL OR CQ.ProviderId = @providerId)
		AND (CONVERT(VARCHAR(11),CQ.CreatedDate,111) BETWEEN ISNULL(@startDate,'1900-1-1') AND ISNULL(@endDate, '2099-1-1'))
		AND (@bookingReference IS NULL OR CA.BookingReferenceId = @bookingReference)
		AND (@IsSolved IS NULL OR @IsSolved = CQ.IsSolved)
		AND (@campaignGroupId IS NULL OR @campaignGroupId = CQ.CampaignId)
		AND (@queryTypeID IS NULL OR @queryTypeID = CQ.QueryTypeId)
		AND  CQ.CustomerId > 0
END
GO
--**************************************************************************************************************************************************************************************
 IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ValidateOfferAvailability]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[ValidateOfferAvailability]
GO
CREATE PROCEDURE [dbo].[ValidateOfferAvailability]
(@OrderId int,
@OrderLineId int,
@CheckInDateCount int, 
@CheckOutDateCount int,
@Sku NVARCHAR(512),
@CampaignID int,
@IsValid BIT OUTPUT)
AS
SET @IsValid = 1;

DECLARE @Quantity INT;

SELECT @Quantity = COUNT(OL.OrderLineId)
FROM uCommerce_OrderLine OL
WHERE OL.OrderId = @OrderId AND OL.Sku = @Sku

WHILE (@CheckInDateCount < @CheckOutDateCount)
BEGIN

DECLARE @Balance int;
SELECT @Balance = (OA.NumberAllocated - OA.NumberBooked)
FROM [uCommerce_OfferAvailability] OA
WHERE OA.DateCounter = @CheckInDateCount AND
	OA.ProviderOccupancyTypeID = @Sku AND
	OA.CampaignItem = @CampaignID

	IF (@Balance < @Quantity)
	BEGIN
		SET @IsValid = 0
	END

SET @CheckInDateCount = @CheckInDateCount + 1
END
GO
--********************************************************************************************************************************************************************************
--INSERT SCRIPTS


--***************************************************************************************************************************************************************************************************
--Insert Defination
declare @definationid int = 0
select @definationid = DefinitionId from uCommerce_Definition
where Name = 'hotel'
declare @datatypeid int = 0
select @datatypeid = DataTypeId from uCommerce_DataType where DefinitionName= 'SiteCoreCountry'

--print @datatypeid
if(@datatypeid <> 0)
begin
insert into uCommerce_DefinitionField(DataTypeId,DefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,BuiltIn,DefaultValue,Guid)
values (@datatypeid , @definationid,'Country',1,0,1,0,0,0,0,'',NEWID())
end

select @datatypeid = DataTypeId from uCommerce_DataType where DefinitionName= 'SiteCoreRegion'
if(@datatypeid <> 0)
begin
insert into uCommerce_DefinitionField(DataTypeId,DefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,BuiltIn,DefaultValue,Guid)
values (@datatypeid , @definationid,'Region',1,0,1,0,0,0,0,'',NEWID())
end

select @datatypeid = DataTypeId from uCommerce_DataType where DefinitionName= 'SiteCoreClient'
if(@datatypeid <> 0)
begin
insert into uCommerce_DefinitionField(DataTypeId,DefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,BuiltIn,DefaultValue,Guid)
values (@datatypeid , @definationid,'Client',1,0,1,0,0,0,0,'',NEWID())
end

select @datatypeid = DataTypeId from uCommerce_DataType where DefinitionName= 'SiteCoreHotelFacility'
if(@datatypeid <> 0)
begin
insert into uCommerce_DefinitionField(DataTypeId,DefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,BuiltIn,DefaultValue,Guid)
values (@datatypeid , @definationid,'HotelFacility',1,0,1,0,0,0,0,'',NEWID())
end

select @datatypeid = DataTypeId from uCommerce_DataType where DefinitionName= 'SiteCoreHotelTheme'
if(@datatypeid <> 0)
begin
insert into uCommerce_DefinitionField(DataTypeId,DefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,BuiltIn,DefaultValue,Guid)
values (@datatypeid , @definationid,'Theme',1,0,1,0,0,0,0,'',NEWID())
end

select @datatypeid = DataTypeId from uCommerce_DataType where DefinitionName= 'SiteCoreGroups'
if(@datatypeid <> 0)
begin
insert into uCommerce_DefinitionField(DataTypeId,DefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,BuiltIn,DefaultValue,Guid)
values (@datatypeid , @definationid,'Groups',1,0,1,0,0,0,0,'',NEWID())
end

select @datatypeid = DataTypeId from uCommerce_DataType where DefinitionName= 'SiteCoreSuperGroups'
if(@datatypeid <> 0)
begin
insert into uCommerce_DefinitionField(DataTypeId,DefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,BuiltIn,DefaultValue,Guid)
values (@datatypeid , @definationid,'SuperGroups',1,0,1,0,0,0,0,'',NEWID())
end


insert into uCommerce_DefinitionField(DataTypeId,DefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,BuiltIn,DefaultValue,Guid)
values (1 , @definationid,'SiteCoreItemGuid',1,0,1,0,0,0,0,'',NEWID())
go
--***************************************************************************************************************************************************************************************************************************************
--Insert offer defnation


declare @definationid int = 0
select @definationid = DefinitionId from uCommerce_Definition
where Name = 'Default Campaign Item'

declare @datatypeid int = 0
select @datatypeid = DataTypeId from uCommerce_DataType where DefinitionName= 'SiteCoreOfferType'
if(@datatypeid <> 0)
begin
insert into uCommerce_DefinitionField(DataTypeId,DefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,BuiltIn,DefaultValue,Guid)
values (@datatypeid , @definationid,'OfferType',1,0,1,0,0,0,0,'',NEWID())
end
Go
--*************************************************************************************************************************************************************************************************************************************************
--insert query
Go
update uCommerce_DataType
set DefinitionName = 'SiteCoreSupplierType'
where TypeName = 'HotelType'
Go

update uCommerce_DataType
set DefinitionName = 'SiteCoreLocation'
where TypeName = 'Location'
Go
update uCommerce_DataType
set DefinitionName = 'SiteCoreHotelRanking'
where TypeName = 'HotelRating'
Go
update uCommerce_DataType
set DefinitionName = 'SiteCoreOccupancyTypes'
where TypeName = 'RoomType'
Go
update uCommerce_DataType
set DefinitionName = 'SiteCoreAddOn'
where TypeName = 'ProductType'
Go

if exists (select 1 from uCommerce_DataType where typename = 'Country' )
begin
	insert into uCommerce_DataType (TypeName,Nullable,ValidationExpression,BuiltIn,DefinitionName,Deleted,Guid,CreatedOn,
	CreatedBy,ModifiedOn,ModifiedBy)
	values ('Country',1,'',0,'SiteCoreCountry',0,NEWID(),GETDATE(),'admin',GETDATE(),'admin'),
	('Region',1,'',0,'SiteCoreRegion',0,NEWID(),GETDATE(),'admin',GETDATE(),'admin'),
	('Theme',1,'',0,'SiteCoreHotelTheme',0,NEWID(),GETDATE(),'admin',GETDATE(),'admin'),
	('HotelFacility',1,'',0,'SiteCoreHotelFacility',0,NEWID(),GETDATE(),'admin',GETDATE(),'admin'),
	('Client',1,'',0,'SiteCoreClient',0,NEWID(),GETDATE(),'admin',GETDATE(),'admin'),
	('OfferType',1,'',0,'SiteCoreOfferType',0,NEWID(),GETDATE(),'admin',GETDATE(),'admin'),
	('Groups',1,'',0,'SiteCoreGroups',0,NEWID(),GETDATE(),'admin',GETDATE(),'admin'),
	('SuperGroups',1,'',0,'SiteCoreSuperGroups',0,NEWID(),GETDATE(),'admin',GETDATE(),'admin')
end
Go
--**************************************************************************************************************************************************************************************************************************
--update payment method mapping
Update  [uCommerce_ClientIDPaymentMethodMapping] set Description='Credit / Laser / Debit Card' where PaymentMethodId = 11
Update  [uCommerce_ClientIDPaymentMethodMapping] set Description='Credit / Laser / Debit Card', ShowInList = 0 where PaymentMethodId = 8

--***************************************************************************************************************************************************************************************************************************
--update due types
update [uCommerce_DueTypes] set Description='Deposit as Transfer Fee', PaymentDueTypeId = 2 where DueTypeId = 5
update [uCommerce_DueTypes] set Description ='Deposit as Cancellation Fee', PaymentDueTypeId = 2 where DuetypeId = 4
update uCommerce_DueTypes set Description = 'Commision of the Break Fee', PaymentDueTypeId = 2 where DueTypeId = 2
update uCommerce_DueTypes set Description = 'Provider Amount', PaymentDueTypeId = 1 where DueTypeId = 1
update uCommerce_DueTypes set Description = 'Processing Fee', PaymentDueTypeId = 3 where DueTypeId = 3

---------------------------

UPDATE olpd SET olpd.DueTypeID = old.DueTypeId
FROM uCommerce_OrderLinePaymentDues olpd INNER JOIN uCommerce_OrderLineDue old
ON olpd.OrderLineDueId = old.OrderLineDueId
WHERE olpd.DueTypeId IS NULL
GO

UPDATE old SET old.PaymentMethodId = olp.PaymentMethodId
FROM uCommerce_OrderLineDue old INNER JOIN uCommerce_OrderLinePayment olp
ON old.OrderLinePaymentID = olp.OrderLinePaymentID
GO

UPDATE olp SET olp.DueTypeId = dt.PaymentDueTypeId
FROM uCommerce_OrderLinePayment olp inner join uCommerce_OrderLineDue oldu on olp.orderlinepaymentid = oldu.orderlinepaymentid
inner join uCommerce_DueTypes dt on oldu.duetypeid = dt.duetypeid
where oldu.orderlinepaymentid is not null
go