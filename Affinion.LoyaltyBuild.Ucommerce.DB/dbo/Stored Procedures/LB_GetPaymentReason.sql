﻿CREATE Procedure [dbo].[LB_GetPaymentReason]
AS
BEGIN
	select
	DueTypeId,
	[Description]
	from
	LB_DueTypes where ShowInList = 1
END
