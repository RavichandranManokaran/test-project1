﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Marketing.Targets;
using UCommerce.Licensing;
using UCommerce.Infrastructure;
using Affinion.LoyaltyBuild.UCom.Common.Constants;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class ClientTarget : Target, IOrderLineTarget, IEntity, ITarget
    {
        private int? _hashCode;

        public virtual bool IsSatisfiedBy(OrderLine orderLine)
        {
            OrderProperty searchIgnor = orderLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.IgnorCampaignSearchCriteria);
            if (searchIgnor != null)
                return true;

            return true;
        }



        // Methods
        public new static IQueryable<ClientTarget> All()
        {
            return GetRepo().Select();
        }

        public override void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<ClientTarget, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            ClientTarget objA = obj as ClientTarget;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<ClientTarget, bool>> expression)
        {
            return All().Any<ClientTarget>(expression);
        }

        public static IList<ClientTarget> Find(Expression<Func<ClientTarget, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<ClientTarget>();
        }

        public static ClientTarget FirstOrDefault(Expression<Func<ClientTarget, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<ClientTarget>();
        }

        public new static ClientTarget Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<ClientTarget> GetRepo()
        {
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<ClientTarget>>();

        }


        public override void Save()
        {
            GetRepo().Save(this);
        }

        public static ClientTarget SingleOrDefault(Expression<Func<ClientTarget, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties
        public override int Id
        {
            get
            {
                return this.ClientTargetId;
            }
        }

        public virtual int ClientTargetId { get; protected set; }

        public virtual string ClientGUID { get; set; }

        public virtual int CampaginItem { get; set; }

    }
}
