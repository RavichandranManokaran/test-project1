﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LanguageSelectorSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.Tests.Layouts.SubLayouts.LanguageSelectorSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Sitecore.Data.Items" %>
<asp:Repeater runat="server" ID="LanguageSelector">
    <HeaderTemplate>
        <ul>
    </HeaderTemplate>
    <ItemTemplate>
        <li>
            <a href="<%# Eval("Url") %>">
                <img src="<%# Eval("ImageUrl") %>" alt="<%# Eval("Name") %>" title="<%# Eval("Name") %>" /></a>
        </li>
    </ItemTemplate>
    <FooterTemplate>
        </ul>
    </FooterTemplate>
</asp:Repeater>
