﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DestinationSubLayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal.DestinationSubLayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Sitecore.Data.Items" %>

<asp:Repeater ID="DestinationRepeater" runat="server">

    <HeaderTemplate>
        <div class="visible-xs">
            <div class="top-desti-ti margin-bottom-common">
                <a id="collapse-button-tit" href="#">
                    <span class="glyphicon glyphicon-road"></span>Show Top Destination
                </a>
            </div>
        </div>
        <div class="header-top-deatail-tit" id="top-des">
            <h3 class="margin-bottom-common header-top-deatail">Top Destination</h3>
    </HeaderTemplate>
    <ItemTemplate>

        <div class="top-des-blog-sec">
            <p>
                <sc:Image Field="Des_Image" Item="<%#(Item)Container.DataItem %>" Alt="" CssClass="img-responsive img-enlarge" runat="server" />
                <a href="#"><%#Affinion.LoyaltyBuild.Common.Utilities.SitecoreFieldsHelper.GetValue((Item)Container.DataItem,"Des_Img_href") %></a>
            </p>
        </div>

    </ItemTemplate>
    <FooterTemplate>
        </div>
    </FooterTemplate>
</asp:Repeater>

