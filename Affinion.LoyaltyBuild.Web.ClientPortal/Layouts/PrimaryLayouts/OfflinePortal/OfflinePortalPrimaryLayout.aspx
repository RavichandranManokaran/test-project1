﻿<%@ Page Language="c#" CodePage="65001" AutoEventWireup="true" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.PrimaryLayouts.OfflinePortal.OfflinePortalPrimaryLayout" CodeBehind="OfflinePortalPrimaryLayout.aspx.cs" %>

<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ OutputCache Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title id="pageTitle" runat="server"></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="CODE_LANGUAGE" content="C#" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <%--<link href="/default.css" rel="stylesheet" />--%>
    <sc:visitoridentification runat="server" />

    <!-- Meta, title, CSS, favicons, etc. -->
    <%--<meta charset="utf-8"/>--%>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="Bootstrap, a sleek, intuitive, and powerful mobile first front-end framework for faster and easier web development." />
    <meta name="keywords" content="HTML, CSS, JS, JavaScript, framework, bootstrap, front-end, frontend, web development" />
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors" />

    <%=this.RenderJavascriptVariables()%>

    <script type="text/javascript" src="/Resources/scripts/jquery.min.js"></script>
    <script type="text/javascript" src="/Resources/scripts/validate/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/Resources/scripts/validate/additional-methods.min.js"></script>
    <script type="text/javascript" src="/Resources/scripts/jquery.simplePagination.js"></script>
    <script type="text/javascript" src="/Resources/bootstrap_assets/javascripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="/Resources/scripts/jquery-ui.js"></script>
    <script type="text/javascript" src="/Resources/scripts/affinionCustom.js"></script>
    <script src="scripts/script.js"></script>
    <script src="scripts/fotorama.js"></script>
    <script src="scripts/images.js"></script>
    <script type="text/javascript" src="scripts/jssor.slider-20.mini.js"></script>
    <script type="text/javascript" src="scripts/detail-gallary.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="/Resources/scripts/CardCheck.js"></script>
    <script type="text/javascript">
        google.charts.load('current', { 'packages': ['corechart'] });
    </script>
    <script type="text/javascript" src="/handlers/cs/OfflinePortalAlertsHandler.ashx"></script>
    <link rel="stylesheet" href="/Resources/styles/responsiveNav.css" type="text/css" />
    <link rel="stylesheet" href="/Resources/styles/jquery-ui.css" />
    <link rel="stylesheet" href="/Resources/styles/bootstrap.min.css" />
    <link rel="stylesheet" href="/Resources/styles/affinionOfflinePortal.min.css" />
   
    <%--<link rel="stylesheet" href="/Resources/styles/jquery.bxslider.css" />--%>
    <link rel="stylesheet" href="/Resources/styles/theme1/offlineTheme.css" />
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">

    <script type="text/javascript" src="/Resources/scripts/affinionCustom.js"></script>
    <link rel="stylesheet" href="/Resources/styles/simplePagination.css"  />
    <link rel="stylesheet" href="/Resources/styles/fonts.css" />
    <link rel="stylesheet" href="/Resources/styles/font-awesome.min.css" />
    <link rel="stylesheet" href="/Resources/styles/bootstrap-social.css" />
    <link rel="shortcut icon" type="image/x-icon" href="/Resources/images/favicon.ico" />
    <link  rel="stylesheet" href=/Resources/styles/img-gallary.css"  />
    <!-- top menu -->
    <link rel="stylesheet" href="/Resources/styles/top-menu.css" />
    <script src="/Resources/scripts/top-menu.js" type="text/javascript"></script>    
    <link rel="stylesheet" href="/Resources/styles/customerService.min.css" />
    <link rel="stylesheet" href="/Resources/styles/offlineportal.css" />
    
    <!-- bxSlider Javascript file -->
    <!-- bxSlider CSS file -->

    <!---offlineportal paymenthistory-->
    <link  rel="stylesheet" href="/Resources/styles/affinionPaymentHistory.css"  />
    <script type="text/javascript" src="/resources/styles/cs/loadingoverlay.min.js"></script>
    <script type="text/javascript" src="/resources/scripts/cs/common.js"></script>
    <script type="text/javascript">
        $(function () {
            jQuery("#collapse-button").click(function () {
                jQuery("#accordion").toggle();
            });
        });
        jQuery(function () {
            jQuery("#collapse-button-tit").click(function () {
                jQuery("#top-des").toggle();
            });
        });

        //$(function () {
        //    //$("#check-in-date").datepicker();
        //    $("#CheckinDate").datepicker();
        //});
        //$(function () {
        //    //$("#check-out-date").datepicker();
        //    $("#CheckoutDate").datepicker();
        //});

        function activate_accordion(course_id) {
            var active_item;
            if (course_id == false) {
                active_item = false;
            } else {
                active_item = $('.accordion div[id="course-' + course_id + '"]').attr('active');
            }

            $(".accordion").accordion({
                collapsible: true,
                active: parseInt(active_item),
                heightStyle: "content",
                icons: {
                    "header": "ui-icon-plus",
                    "activeHeader": "ui-icon-minus"
                }
            });
        }


    </script>

</head>

<body class="cbp-spmenu-push">
    <div class="outer-container">
        <form method="post" runat="server" id="mainform">
            <div class="container-outer width-full">
                <div class="container-outer-inner width-full header-top-bar">

                    <sc:placeholder id="topbarofflineportal" key="topbarofflineportal" runat="server" />

                </div>
            </div>

             <sc:Placeholder ID="menubarofflineportal" Key="menubarofflineportal" runat="server" />

            <div class="body-container">
                <div class="container">

                    <sc:placeholder id="breadcrumbofflineportal" key="breadcrumbofflineportal" runat="server" />
                    <sc:placeholder id="menubuttons" key="menubuttons" runat="server" />
                    <sc:placeholder id="maincontentofflineportal" key="maincontentofflineportal" runat="server" />

                </div>
            </div>

            <sc:placeholder id="footerofflineportal" key="footerofflineportal" runat="server" />


            <!-- ================================================== -->
            <script type="text/javascript">
                $(document).ready(function () {
                    activate_accordion(40);
                    jQuery("table tr:even").css({ "background-color": "#fafefe", "border-top": "solid 1px #cff2f6", "border-bottom": "solid 1px #cff2f6" });
                    jQuery("table tr:odd").css("background-color", "#fff");

                    $('#nodate').change(function () {
                        if (this.checked)
                            $('.ondate').slideUp("slow");
                        else
                            $('.ondate').slideDown("slow");
                    });

                    $('.tbl-calendar thead tr').removeAttr("style");
                    $('.tbl-calendar thead tr').removeAttr("style");

                    //nav
                    var res_nav = document.querySelectorAll();

                    setLanguage(getSelected);
                    var dt = res_nav[0].dataset;

                    var getSelected;
                    var currentLanguage_db = "English";

                    function setLanguage(getSelected) {
                        if (getSelected !== undefined) {
                            currentLanguage_db = getSelected;
                            $(res_nav[0]).text(currentLanguage_db);
                        }
                        else {
                            $(res_nav[0]).text(currentLanguage_db);
                        }
                    }

                    setLanguage(currentLanguage_db);

                    $('#lang-globe .dropdown-menu li a').click(function (e) {
                        var getSelected = $(this).text();
                        setLanguage(getSelected);
                    });

                    /*Carousel: Init : data-ride='carousel':removed */
                    $(".carousel").carousel();

                });

               
               
            </script>


        </form>
    </div>
    <div id="dialog-confirm"></div>
</body>

</html>
