﻿using System.Xml.Serialization;

namespace Affinion.LoyaltyBuild.BedBanks.Troika.Model.Coldfusion
{
    public class ColdFusionDouble
    {
        [XmlAttribute("cftype")]
        public string CfType = "DOUBLE";

        [XmlText]
        public string Value { get; set; }
    }
}
