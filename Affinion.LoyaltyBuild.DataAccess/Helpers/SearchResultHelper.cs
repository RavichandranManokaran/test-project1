﻿using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.DataAccess.Helpers
{
    public static class SearchResultHelper
    {
        public static LBSearhResult GetAvailableProductList(List<string> sku, int startDate, int endDate)
        {
            try
            {
                LBSearhResult results = new LBSearhResult();

                //UCommerce.Infrastructure.Configuration.RuntimeConfigurationSection.ConnectionString
                DataConnection dataConnection = new DataConnection();

                //RuntimeConfigurationSection runtimeConfigurationSection = new RuntimeConfigurationSection();
                //string connectionString = runtimeConfigurationSection.ConnectionString;
                using (SqlConnection sqlConnection = new SqlConnection(dataConnection.ConnectionString))
                {
                    //DataObject dataObject = new DataObject();
                    SqlCommand sqlCommand = new SqlCommand("GetProductByCategoryIds", sqlConnection);
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.Parameters.Add(new SqlParameter("@productid", string.Join(",", sku)));
                    sqlCommand.Parameters.Add(new SqlParameter("@startdate", startDate));
                    sqlCommand.Parameters.Add(new SqlParameter("@enddate", endDate));

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    DataSet ds = new DataSet();
                    sqlConnection.Open();
                    adapter.SelectCommand = sqlCommand;
                    adapter.Fill(ds);
                    sqlConnection.Close();


                    if (ds.Tables.Count > 0)
                    {
                        var counter = 0;
                        if (ds.Tables[counter].Rows.Count > 0)
                        {
                            counter = 0;
                            List<LBProductVariant> productList = new List<LBProductVariant>();
                            for (int i = 0; i < ds.Tables[counter].Rows.Count; i++)
                            {
                                LBProductVariant products = new LBProductVariant();
                                products.SKU = ds.Tables[counter].Rows[i]["SKU"].ToString();
                                products.ProductId = Convert.ToInt32(ds.Tables[counter].Rows[i]["ProductId"].ToString());
                                products.Name = ds.Tables[counter].Rows[i]["Name"].ToString();
                                products.VSKU = ds.Tables[counter].Rows[i]["VSKU"].ToString();
                                products.IsVariant = Convert.ToBoolean(ds.Tables[counter].Rows[i]["IsVariant"].ToString());
                                productList.Add(products);
                            }
                            results.Product = productList;
                        }
                        counter = 1;
                        if (ds.Tables[counter].Rows.Count > 0)
                        {

                            List<AdditionalInfo> categoryProperty = new List<AdditionalInfo>();
                            for (int i = 0; i < ds.Tables[counter].Rows.Count; i++)
                            {
                                AdditionalInfo productproperties = new AdditionalInfo();
                                productproperties.SKU = ds.Tables[counter].Rows[i]["SKU"].ToString();
                                productproperties.VSKU = ds.Tables[counter].Rows[i]["VSKU"].ToString();
                                productproperties.ProductId = Convert.ToInt32(ds.Tables[counter].Rows[i]["ProductId"].ToString());
                                productproperties.Name = ds.Tables[counter].Rows[i]["Name"].ToString();
                                productproperties.Value = ds.Tables[counter].Rows[i]["Value"].ToString();
                                categoryProperty.Add(productproperties);
                            }
                            results.ProductProperty = categoryProperty;
                        }

                        counter = 2;
                        if (ds.Tables[counter].Rows.Count > 0)
                        {

                            List<AdditionalInfo> productProperty = new List<AdditionalInfo>();
                            for (int i = 0; i < ds.Tables[counter].Rows.Count; i++)
                            {
                                AdditionalInfo productproperties = new AdditionalInfo();
                                productproperties.SKU = ds.Tables[counter].Rows[i]["SKU"].ToString();
                                productproperties.VSKU = ds.Tables[counter].Rows[i]["VSKU"].ToString();
                                productproperties.ProductId = Convert.ToInt32(ds.Tables[counter].Rows[i]["ProductId"].ToString());
                                productproperties.Name = ds.Tables[counter].Rows[i]["Name"].ToString();
                                productproperties.Value = ds.Tables[counter].Rows[i]["Value"].ToString();
                                productProperty.Add(productproperties);
                            }
                            results.ProductVariantProperty = productProperty;
                        }

                        counter = 3;
                        if (ds.Tables[counter].Rows.Count > 0)
                        {

                            List<RoomOfferAvailability> roomAvailabilitys = new List<RoomOfferAvailability>();
                            for (int i = 0; i < ds.Tables[counter].Rows.Count; i++)
                            {
                                RoomOfferAvailability roomAvailability = new RoomOfferAvailability();
                                roomAvailability.SKU = ds.Tables[counter].Rows[i]["SKU"].ToString();
                                roomAvailability.VSKU = ds.Tables[counter].Rows[i]["VSKU"].ToString();
                                roomAvailability.Availability = Convert.ToInt32(ds.Tables[counter].Rows[i]["Availability"].ToString());
                                roomAvailability.DateCounter = Convert.ToInt32(ds.Tables[counter].Rows[i]["DateCounter"].ToString());
                                roomAvailability.Price = Convert.ToDecimal(ds.Tables[counter].Rows[i]["Price"].ToString());
                                Guid priceBand;
                                if(Guid.TryParse(ds.Tables[counter].Rows[i]["PriceBand"].ToString() , out priceBand) && priceBand != Guid.Empty)
                                {
                                    roomAvailability.PriceBand = priceBand;
                                }
                                roomAvailability.OfferId = 0;
                                roomAvailabilitys.Add(roomAvailability);
                            }
                            results.RoomAvailability = roomAvailabilitys;
                        }

                        counter = 4;
                        if (ds.Tables[counter].Rows.Count > 0)
                        {
                            List<RoomOfferAvailability> offerAvailabilitys = new List<RoomOfferAvailability>();
                            for (int i = 0; i < ds.Tables[counter].Rows.Count; i++)
                            {
                                RoomOfferAvailability offerAvailability = new RoomOfferAvailability();
                                offerAvailability.SKU = ds.Tables[counter].Rows[i]["SKU"].ToString();
                                offerAvailability.VSKU = ds.Tables[counter].Rows[i]["VSKU"].ToString();
                                offerAvailability.Availability = Convert.ToInt32(ds.Tables[counter].Rows[i]["Availability"].ToString());
                                offerAvailability.OfferId = Convert.ToInt32(ds.Tables[counter].Rows[i]["OfferId"].ToString());
                                offerAvailability.DateCounter = Convert.ToInt32(ds.Tables[counter].Rows[i]["DateCounter"].ToString());
                                offerAvailability.PriceBand = Guid.Empty;
                                offerAvailabilitys.Add(offerAvailability);
                            }
                            results.OfferAvailability = offerAvailabilitys;
                        }
                    }
                }
                results.Category = new List<LBProduct>();
                Guid tmp = default(Guid);
                results.Category.AddRange(sku.Select(x => new LBProduct() { SKU = x }));
                return results;

            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.DataAccess, exception, null);
                throw;
            }
        }

        public static bool UpdatePrice(int dateCounter, bool isAddon, decimal totalPrice,decimal packagePrice, Guid priceband, string addOnSku)
        {
            try
            {
                LBSearhResult results = new LBSearhResult();

                DataConnection dataConnection = new DataConnection();

                using (SqlConnection sqlConnection = new SqlConnection(dataConnection.ConnectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand("UpdatePrice", sqlConnection);
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.Parameters.Add(new SqlParameter("@datecounter", dateCounter));
                    sqlCommand.Parameters.Add(new SqlParameter("@IsAddOn", isAddon));
                    sqlCommand.Parameters.Add(new SqlParameter("@TotalPrice", totalPrice));
                    sqlCommand.Parameters.Add(new SqlParameter("@PackagePrice", packagePrice));
                    sqlCommand.Parameters.Add(new SqlParameter("@PriceBand", priceband));
                    sqlCommand.Parameters.Add(new SqlParameter("@AddOnSku", addOnSku));

                    sqlConnection.Open();
                    sqlCommand.ExecuteNonQuery();
                    sqlConnection.Close();
                }
                return true;

            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.DataAccess, exception, null);
                throw;
            }
        }
    }
}
