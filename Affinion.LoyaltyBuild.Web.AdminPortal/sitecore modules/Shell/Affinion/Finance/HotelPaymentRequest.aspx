﻿<%@ Page Title="" Language="C#" MasterPageFile="~/sitecore modules/Shell/Affinion/Finance/MainLayout.Master" AutoEventWireup="true" CodeBehind="HotelPaymentRequest.aspx.cs" Inherits="Affinion.LoyaltyBuild.AdminPortal.sitecore_modules.Shell.Affinion.Finance.HotelPaymentRequest" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {

            $('input[id="DateRange"]').daterangepicker();

            $('#DateRange').daterangepicker({
                locale: { cancelLabel: 'Clear' }
            });

            function cb(start, end) {
                $('#DateRange span').html(start + ' - ' + end);
            }
            cb(moment().subtract(29, 'days'), moment());

            $('#DateRange').daterangepicker({
                ranges: {
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    'Last Two Month': [moment().subtract(2, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    'Last Four Month': [moment().subtract(4, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    'Last Six Month': [moment().subtract(6, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            jQuery("#DateUpTo").datepicker({
                defaultDate: new Date(),
                dateFormat: "dd/mm/yy",
            numberOfMonths: 1,
            showOn: "both",
            buttonImage: "images/calendar.png",
            buttonImageOnly: false,
            buttonClass: "glyphicon-calendar"
     }).next(".ui-datepicker-trigger").addClass("btn btn-default glyphicon glyphicon-calendar calander-icon pull-left");

     $('#DateRange').on('cancel.daterangepicker', function (ev, picker) {
         //do something, like clearing an input
         $('#DateRange').val('');
         Validation();
     });

     $('#DateRange').on('apply.daterangepicker', function (ev, picker) {
         $("#DateUpTo").val('');
         Validation();
     });


     if ($('#DateUpTo').val() != "")
         $('#DateRange').val('');

     $("#chkSelectAll").click(function () {
         if ($(this).is(":checked")) {
             $("#ResultGridView input[id*='chkSelect']:checkbox").attr("checked", function () {
                 if (!$(this).is(":disabled"))
                     this.checked = true;
             });
             
             $("#hdnSelectAll").val("true");
         }
         else {
             $("#ResultGridView input[id*='chkSelect']:checkbox").attr("checked", function () {
                 this.checked = false;
             });
             $("#hdnSelectAll").val("false");
         }
         UpdateCheckUnCheck("SelectAll", $(this).is(":checked"),0,0);
         Validation();

     });

     $("#ResultGridView input[id*='chkSelect']:checkbox").click(function () {
         if (!$(this).is(":checked")) {
             $("#hdnSelectAll").val("false");
             $("#ResultGridView input[id*='chkSelectAll']:checkbox").attr("checked", function () {
                 this.checked = false;
             });
         }
         else
         {
             $("#hdnSelectAll").val("true");
         }

         var OrderID = 0;
         var OrderLineID = 0;
         var checkedIndex = $('input[type="checkbox"]').index(this);
         var grdResultView = document.getElementById('ResultGridView');
         var hiddenField = grdResultView.rows[checkedIndex].getElementsByTagName("input");
         if (hiddenField != null && hiddenField != "") {
             for (var i = 0; i < hiddenField.length; i++) {
                 if (hiddenField[i].type == "hidden") {
                     if (hiddenField[i].id == "hdnOrderID") {
                         OrderID = hiddenField[i].value;
                     }
                     if (hiddenField[i].id == "hdnOrderLineID") {
                         OrderLineID = hiddenField[i].value;
                     }
                 }
             }
             UpdateCheckUnCheck("Select", $(this).is(":checked"), OrderID, OrderLineID);
             Validation();
         }
         
     });

     if ($("#hdnSelectAll").val() == "true") {
         if ($("#hdnSelect").val() == "true") {
             $("#ResultGridView input[id*='chkSelectAll']:checkbox").attr("checked", function () {
                 this.checked = true;
             });
         }
         else {
             $("#hdnSelectAll").val() = "false";
             $("#ResultGridView input[id*='chkSelectAll']:checkbox").attr("checked", function () {
                 this.checked = false;
             });

         }
     }
    
     $("#DateUpTo").change(function () {
         $('#DateRange').val('');
         Validation();
     });


     $("#ResultGridView input[id*='txtSuggestedAmount']:text").on("keypress keyup blur", function (event) {
         $(this).val($(this).val().replace(/[^0-9\.\d{1,2}]/g, ''));
         if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
             event.preventDefault();
         }
         if ($(this).val().split(".")[1].length > 2) {
             this.value = parseFloat(this.value).toFixed(2);
         }
         Validation();
     });

     $("#AddManualPaymentRequest").click(function () {
         location.href = "/sitecore modules/Shell/Affinion/Finance/ManualHotelPaymentRequest.aspx";
     });

     if ($("#hdnDateRange").val() == "false") {
         $('#DateRange').val('');
     }

     Validation();

   });


    function UpdateCheckUnCheck(selectmode, status, orderID, orderLineID) {
            $.ajax(
                {
                    type: "POST",
                    url: "/sitecore modules/Shell/Affinion/Finance/HotelPaymentRequest.aspx/UpdateSelectedCheck",
                    data: '{SelectMode: "' + selectmode + '",Status: "' + Boolean(status) + '",OrderID: "' + parseInt(orderID) + '" ,OrderLineID: "' + parseInt(orderLineID) + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        $("#hdnSelect").val("true");
                    },
                    complete: function (response) {
                        $("#hdnSelect").val("true");
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
        }

    function Validation() {
        var DateRange = $("#DateRange").val();
        var DateUpTo = $("#DateUpTo").val();

        if (DateRange != "" || DateUpTo != "")
        {
            $("#PaymentRequest").removeAttr('disabled');
            $("#btnGo").removeAttr('disabled');
            $("#hdnDateRange").val("true");
        }
            
        else
        {
            $("#PaymentRequest").attr('disabled', 'disabled');
            $("#btnGo").attr('disabled', 'disabled');
            $("#hdnDateRange").val("false");
        }
           
        if ($("#hdnSelect").val() == "true" || $("#hdnSelectAll").val() == "true") {
            $("#SavePaymentRequest").removeAttr('disabled');
        }
        else {
            $("#SavePaymentRequest").attr('disabled', 'disabled');
        }
    }

   
 </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuItems" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Title" runat="server">
    Hotel Payment Request
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Body" runat="server">
   <div class="row">
			<div class="col-md-12">
                 <div>
                    <h4 class="title-text font-weight-bold"><asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label></h4>
                </div>  
			</div>
		</div>
     <div class="panel-heading bg-primary">
                                    <h4 class="panel-title">
                                        <span>Search Hotel Payment Request :<a id="AddManualPaymentRequest" href="#" style="color:red">Add new Payment Requests</a> <asp:Label ID="lblTotalRecords" runat="server" Text="0"></asp:Label> records(s)</span>
                                        <a data-toggle="collapse" data-parent="#accordion1" href="#collapse0" class="show-hide-list">
                                            <span class="glyphicon glyphicon-triangle-top pull-right"></span>
                                        </a>
                                    </h4>
                                </div>
     <div id="collapse0" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="col-xs-6 no-padding">
                                                    <div class="col-xs-4 no-padding-left">
                                                        <div><b>Date Range:</b><span class="required">*</span></div>
                                                        <div class="form-group">
                                                            <div class='input-group date'>
                                                                <input type="text" id="DateRange"  runat="server" clientidmode="Static" class="form-control datepicker width-100 pull-left font-size-12" tabindex="1" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <div><b>Date up to:</b></div>
                                                        <div class="form-group">
                                                            <div class='input-group date'>
                                                                <asp:TextBox ID="DateUpTo" runat="server" clientidmode="Static" class="form-control datepicker width-85 pull-left font-size-12" tabindex="2" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <div><b>Provider</b></div>
                                                        <div>
                                                            <asp:DropDownList ID="Provider" ClientIDMode="Static" runat="server" CssClass="form-control font-size-12 dropdown-field" TabIndex="3">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 no-padding">
                                                    <div class="col-xs-4">
                                                        <div><b>Partner</b></div>
                                                        <div>
                                                            <asp:DropDownList ID="Partner" ClientIDMode="Static" runat="server" CssClass="form-control font-size-12 dropdown-field" TabIndex="4">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-4" style="display:none">
                                                        <div><b>Spa Only</b></div>
                                                        <div>
                                                            <asp:DropDownList ID="SpaOnly" ClientIDMode="Static" runat="server" CssClass="form-control font-size-12 dropdown-field" TabIndex="5">
                                                                <asp:ListItem Value="">Select Spa Only</asp:ListItem>
                                                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                <asp:ListItem Value="2">No</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-4 no-padding-right">
                                                        <div><b>Payment Status</b></div>
                                                        <div>
                                                            <asp:DropDownList ID="PaymentStatus" ClientIDMode="Static" runat="server" CssClass="form-control font-size-12 dropdown-field" TabIndex="6">
                                                                <asp:ListItem Value="">Select Payment Status</asp:ListItem>
                                                                <asp:ListItem Value="1">Paid</asp:ListItem>
                                                                <asp:ListItem Value="0">Unpaid</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-xs-12">
                                                <div class="col-xs-9 no-padding"></div>
                                                 <div class="col-xs-3 no-padding-right">
                                                    <asp:Button type="button" ID="PaymentRequest" runat="server" ClientIDMode="Static" Text="Get Payment Request" class="btn btn-primary pull-right fullWidth" TabIndex="7" OnClick="PaymentRequest_Click" />
                                                </div   
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 marT15">
                                                <div class="col-xs-9 no-padding">
                                                    <label class="pull-right">Search:</label>
                                                </div>
                                                <div class="col-xs-3">
                                                                <asp:TextBox  ID="SearchBox" clientidmode="Static" runat="server" class="form-control demondsearch" placeholder="search" />
                                                                <asp:Button type="button" ID="btnGo" runat="server" ClientIDMode="Static" Text="Go" class="btn btn-primary pull-right mar0" TabIndex="8" OnClick="btnGo_Click"/>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 marT15">
                                                <div class="row" id="htmlReportGrid" runat="server">
                                                    <div class="col-sm-12">
                                                        <div>
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading claim_titl">
                                                                    <h4 class="panel-title">Payment Request reports.
                                                                    </h4>
                                                                </div>
                                                                <div id="totalCount" runat="server" class="totalCountpad">Payment request details reports Total of <span><%= TotalOfRecords %> </span>record(s) found. </div>
                                                                <div class="panel-body alert-info">
                                                                    <%-- Result Grid--%>
                                                                    <div runat="server" id="ResultDiv" clientidmode="Static">
                                                                        <div id="data-grid" class="table-responsive">
                                                                            <asp:GridView ID="ResultGridView" runat="server" ClientIDMode="Static" Width="100%" SelectedRowStyle-Font-Bold="true" class="table table-striped table-bordered" AutoGenerateColumns="False" AllowPaging="true" AllowSorting="true" PageSize="10"
                                                                                PagerSettings-NextPageText=" Next " PagerSettings-PreviousPageText=" Previous " PagerSettings-Mode="NumericFirstLast" PagerSettings-FirstPageText=" First " PagerSettings-LastPageText=" Last " PagerSettings-Position="Bottom" PagerStyle-Font-Bold="true" PagerStyle-CssClass="pagination-grid" OnPageIndexChanging="ResultGridView_PageIndexChanging" OnRowDataBound="ResultGridView_RowDataBound" OnSorting="ResultGridView_Sorting" >
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="Booking Reference" SortExpression="BookingRefNo">
                                                                                        <ItemTemplate>
                                                                                            <table style="width: 100%">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblBookingRefNo" runat="server" Text='<%# Bind("BookingRefNo") %>'></asp:Label></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblBookingID" runat="server" Text='<%# Bind("BookingID") %>'></asp:Label></td>
                                                                                                </tr>
                                                                                                 <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblOfferName" runat="server" Text='<%# Bind("OfferName") %>'></asp:Label></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblOfferBased" runat="server" Text='<%# Bind("OfferBased") %>' ForeColor="Red" Font-Bold="true"></asp:Label></td>
                                                                                                </tr>
                                                                                                 <tr>
                                                                                                    <td>
                                                                                                        <asp:HiddenField ID="hdnOrderID" runat="server" ClientIDMode="Static" Value='<%# Bind("OrderID") %>'></asp:HiddenField></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:HiddenField ID="hdnOrderLineID" runat="server" ClientIDMode="Static" Value='<%# Bind("OrderLineID") %>'></asp:HiddenField></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblSupplierPaymentId" runat="server" Text='<%# Bind("SupplierPaymentId") %>'  style="display:none"></asp:Label></td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Arrival</br>Departure" SortExpression="CheckInDate">
                                                                                        <ItemTemplate>
                                                                                            <table style="width: 100%">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblArrivalDate" runat="server" Text='<%# Bind("CheckInDate","{0:dd/MM/yyyy}") %>'></asp:Label></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblDepartureDate" runat="server" Text='<%# Bind("CheckOutDate","{0:dd/MM/yyyy}") %>'></asp:Label></td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Client" SortExpression="CustomerName">
                                                                                        <ItemTemplate>
                                                                                            <table style="width: 100%">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblCustomerName" runat="server" Text='<%# Bind("CustomerName") %>'></asp:Label></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblAdultsCount" runat="server" Text='<%# Bind("AdultsCount") %>'></asp:Label></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblChildrenCount" runat="server" Text='<%# Bind("ChildrenCount") %>'></asp:Label></td>
                                                                                                </tr>
                                                                                                 <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblCustomerID" runat="server" Text='<%# Bind("CustomerID") %>' Visible="false"></asp:Label></td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Booking Status" SortExpression="BookingStatus">
                                                                                        <ItemTemplate>
                                                                                            <table style="width: 100%">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblBookingStatus" runat="server" Text='<%# Bind("BookingStatus") %>'></asp:Label></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblBookingDate" runat="server" Text='<%# Bind("BookingDate","{0:dd/MM/yyyy}") %>'></asp:Label> </td>
                                                                                                </tr>
                                                                                               
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblBookingTime" runat="server" Text='<%# Bind("BookingDate","{0:hh:mm:ss}") %>'></asp:Label> </td>
                                                                                                </tr>
                                                                                                                                   
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Accommodation" SortExpression="HotelName">
                                                                                        <ItemTemplate>
                                                                                            <table style="width: 100%">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblAccommodationType" runat="server" Text='<%# Bind("AccommodationType") %>'></asp:Label></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblHotelName" runat="server" Text='<%# Bind("HotelName") %>'></asp:Label></td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Requested Dtae</br>Requested By" SortExpression="RequestedDate">
                                                                                        <ItemTemplate>
                                                                                            <table style="width: 100%">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblRequestedDate" runat="server" Text='<%# Bind("RequestedDate","{0:dd/MM/yyyy}") %>'></asp:Label></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblRequestedBy" runat="server" Text='<%# Bind("RequestedBy") %>'></asp:Label></td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Suggested</br>Amount" SortExpression="SuggestedAmount">
                                                                                        <ItemTemplate>
                                                                                            <table style="width: 100%">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:TextBox ID="txtSuggestedAmount" runat="server" ClientIDMode="Static" Text='<%# Bind("SuggestedAmount") %>'></asp:TextBox></td>
                                                                                                </tr>
                                                                                                 <tr>
                                                                                                     <td>
                                                                                                         <asp:Label ID="lblCurrency" runat="server" Text='<%# Bind("Currency") %>'></asp:Label></td>
                                                                                                 </tr>
                                                                                                 <tr>
                                                                                                     <td>
                                                                                                         <asp:Label ID="lblCurrencyID" runat="server" Text='<%# Bind("CurrencyID") %>' style="display:none"></asp:Label></td>
                                                                                                 </tr>

                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Oracle</br>ID" SortExpression="OracleID">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblOracleID" runat="server" Text='<%# Bind("OracleID") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField>
                                                                                        <HeaderTemplate>
                                                                                            <table style="width: 100%">
                                                                                                <tr>
                                                                                                    <td colspan="2">
                                                                                                        <asp:Label ID="lblStatus" runat="server" Text="Payment Status"></asp:Label></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblSelectAll" runat="server" Text="Select All:"></asp:Label></td>
                                                                                                    <td>
                                                                                                        <asp:CheckBox ID="chkSelectAll" ClientIDMode="Static" runat="server" /></td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </HeaderTemplate>
                                                                                        <ItemTemplate>
                                                                                            <table style="width: 100%">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:CheckBox ID="chkSelect" ClientIDMode="Static" runat="server" /></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblAmount" runat="server" Text='<%# Bind("SuggestedAmount") %>'></asp:Label> <asp:Label ID="lblSuggestedCurrency" runat="server" Text='<%# Bind("Currency") %>'></asp:Label></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblClientID" runat="server" Text='<%# Bind("ClientID") %>' style="display:none"></asp:Label></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblProviderID" runat="server" Text='<%# Bind("ProviderID") %>' style="display:none"></asp:Label></td>
                                                                                                </tr>
                                                                                                 <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblPaymentStatus" runat="server" Text='<%# Bind("PaymentStatus") %>' style="display:none"></asp:Label></td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>

                                                                                <EmptyDataTemplate>
                                                                                    ---No Records---
                                                                                </EmptyDataTemplate>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-xs-12">
                                                <asp:Button type="button" ID="SavePaymentRequest" runat="server" ClientIDMode="Static" Text="Save Payment Request" class="btn btn-primary pull-right" TabIndex="9" OnClick="SavePaymentRequest_Click" />
                                                <asp:HiddenField ID="hdnSelectAll" ClientIDMode="Static" runat="server" />
                                                <asp:HiddenField ID="hdnSelect" ClientIDMode="Static"  runat="server" />
                                                 <asp:HiddenField ID="hdnDateRange" ClientIDMode="Static" Value="true"  runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
</asp:Content>
