﻿using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.Search.Helpers;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports;
using Sitecore.Data;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.Reports
{
    public partial class AgentPerformanceConfirmedBookingReport : System.Web.UI.UserControl
    {
        #region private variables

        private IReportsService iReportsService;
        private const string DdlPartnerDefaultText = "Select a partner";
        private const string DdlAgentDefaultText = "Select a Agent";
        private const string TotalText = "Total";
        private Dictionary<string, string> _sitecoreMessages;
        private GridView gridView;

        protected String LblDateText { get; set; }
        protected String LblTotalBookingMade { get; set; }
        protected String LblTotalBookingConfirmed { get; set; }
        protected String LblAgentName { get; set; }
        private List<AgentPerformanceDetail> lstAgentPerformanceDetails { get; set; }

        #endregion

        public AgentPerformanceConfirmedBookingReport()
        {
            iReportsService = new ReportsService();
            lstAgentPerformanceDetails = new List<AgentPerformanceDetail>();
        }

        /// <summary>
        /// page init event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Init(object sender, EventArgs e)
        {
            //bind sitecore text
            BindSitecoreText();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

            //calling BindDropDown
            if (!IsPostBack)
            {
                FillDropdown();
            }

            PrintButtonSection.Visible = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ListAgentPerformance_Click(object sender, EventArgs e)
        {
            PopulateDataOnGrid();
        }

        private void PopulateDataOnGrid()
        {
            if (!String.IsNullOrEmpty(ddlPartner.SelectedValue) && !ddlPartner.SelectedValue.Equals("0"))
            {
                DateTime startDate = new DateTime();
                DateTime endDate = new DateTime();
                if (DateTime.TryParse(txtDateFrom.Text, out startDate))
                    if (DateTime.TryParse(txtDateTo.Text, out endDate))
                    {
                        LblTotalBookingMade = LblTotalBookingMade.Replace("{0}", startDate.ToString("dd/MM/yyyy") + "-" + endDate.ToString("dd/MM/yyyy"));
                        LblTotalBookingConfirmed = LblTotalBookingConfirmed.Replace("{0}", startDate.ToString("dd/MM/yyyy") + "-" + endDate.ToString("dd/MM/yyyy"));
                    }
                //Repo Call to Fetch Data from DB
                lstAgentPerformanceDetails = iReportsService.GetAgentPerformanceDetails(ddlAgentDropDown.SelectedValue, startDate.Date, endDate.Date, ddlPartner.SelectedValue);

                var agentList = lstAgentPerformanceDetails.Select(x => x.AgentName).Distinct();

                if (agentList != null && agentList.Any())
                {
                    reportRepeater.DataSource = agentList;
                    reportRepeater.ItemDataBound += rpt_ItemDataBound;
                    reportRepeater.DataBind();
                }
            }
        }
        /// <summary>
        /// repeater data bound event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rpt_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                gridView = e.Item.FindControl("ResultGridView") as GridView;
                HtmlGenericControl divControl = e.Item.FindControl("ResultDiv") as HtmlGenericControl;

                divControl.Visible = true;
                var list = lstAgentPerformanceDetails.Where(x => x.AgentName.Equals(e.Item.DataItem.ToString())).OrderBy(f => f.Date).ToList();
                gridView.DataSource = list;
                gridView.DataBind();

                string userName = string.Format("{0}\\{1}", Sitecore.Security.Domains.Domain.GetDomain("extranet"), e.Item.DataItem.ToString());
                
                if (Sitecore.Security.Accounts.User.Exists(userName))
                {
                    Sitecore.Security.Accounts.User user = Sitecore.Security.Accounts.User.FromName(userName, true);
                    Sitecore.Security.UserProfile profile = user.Profile;
                    gridView.HeaderRow.Cells[0].Text = profile.FullName;
                    string totalText = GetMessage(TotalText);
                    totalText = totalText.Replace("{0}", profile.FullName);
                    gridView.FooterRow.Cells[1].Text = totalText;
                }

                gridView.FooterRow.CssClass = "tblfooter";
                gridView.FooterRow.Cells[2].Text = list.Sum(x => x.TotalBookingMade).ToString();
                gridView.FooterRow.Cells[3].Text = list.Sum(x => x.TotalBookingConfirmed).ToString();
                if (gridView.Rows.Count > 0)
                    PrintButtonSection.Visible = true;
                else
                    PrintButtonSection.Visible = false;
            }
        }                
        /// <summary>
        /// Loading Client Name in dropdown
        /// </summary>
        private void FillDropdown()
        {
            Database contextDb = Sitecore.Context.Database;
            var ClientITems = new ListItemCollection();
            var items = contextDb.SelectItems(Constants.FilteredClientItems) != null ? contextDb.SelectItems(Constants.FilteredClientItems).ToList() : null;
            if (items != null)
            {
                foreach (var item in items)
                {
                    ListItem liBox = new ListItem();
                    liBox.Text = SitecoreFieldsHelper.GetValue(item, "Name");
                    liBox.Value = item.ID.ToString();
                    if (liBox != null)
                    {
                        ClientITems.Add(liBox);
                    }

                }
                BindDropdown(ddlPartner, ClientITems, "Text", "Value");
                ddlPartner.Items.Insert(0, new ListItem { Text = GetMessage(DdlPartnerDefaultText), Value = "0" });

            }

        }
        /// <summary>
        /// to bind the sitecore text
        /// </summary>
        private void BindSitecoreText()
        {
            _sitecoreMessages = new Dictionary<string, string>();
            Item currentPageItem = Sitecore.Context.Item;
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Please Select", PleaseSelectLabel);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Date From", DateFromLabel);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Date To", DateToLabel);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Partner", PartnerLabel);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "List Agent Performance", ListAgentPerformanceTxt);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Date From is Required", DateFromRequiredLabel);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Please Select Partner", PartnerRequiredLabel);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Date To is Required", ToDateRequiredLabel);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Greater than Equal", CompareDatesLabel);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Print", PrintLabel);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Export To Excel", ExcelLabel);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "AgentName", AgentLabel);
            

            LblDateText = SitecoreFieldsHelper.GetItemFieldValue(currentPageItem, "Date");
            LblTotalBookingMade = SitecoreFieldsHelper.GetItemFieldValue(currentPageItem, "Total Booking Made");
            LblTotalBookingConfirmed = SitecoreFieldsHelper.GetItemFieldValue(currentPageItem, "Total Booking Confirmed First Time");
            _sitecoreMessages.Add(DdlPartnerDefaultText, SitecoreFieldsHelper.GetValue(currentPageItem, DdlPartnerDefaultText));
            _sitecoreMessages.Add(TotalText, SitecoreFieldsHelper.GetValue(currentPageItem, TotalText));
            _sitecoreMessages.Add(DdlAgentDefaultText, SitecoreFieldsHelper.GetValue(currentPageItem, DdlAgentDefaultText));

        }



        /// <summary>
        /// Bind Drop down
        /// </summary>
        /// <param name="ddlControl"></param>
        /// <param name="dataSource"></param>
        /// <param name="text"></param>
        /// <param name="value"></param>
        private void BindDropdown(DropDownList ddlControl, object dataSource, string text, string value)
        {
            ddlControl.DataSource = dataSource;
            ddlControl.DataTextField = text;
            ddlControl.DataValueField = value;
            ddlControl.DataBind();
        }

        /// <summary>
        /// Get the Message from the SiteCore By Passing the Key
        /// </summary>
        /// <param name="key"></param>
        /// <returns>Value from SiteCore Message Array</returns>
        private string GetMessage(string key)
        {
            if (_sitecoreMessages.ContainsKey(key))
                return _sitecoreMessages[key];

            return string.Empty;
        }

        protected void ddlPartner_OnSelectedIndex(object sender, EventArgs e)
        {
            ddlAgentDropDown.Items.Clear();
            if (!String.IsNullOrEmpty(ddlPartner.SelectedValue) && !ddlPartner.SelectedValue.Equals("0"))
            {
                Item partnerItem = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(ddlPartner.SelectedValue));

                if (partnerItem != null)
                {
                    String callCenterName = SitecoreFieldsHelper.GetDropListFieldValue(partnerItem, "CallCentre");

                    var callCentreAgentList = SearchHelper.GetCallCentreUsersOnCallCentreName(callCenterName);

                    if (callCentreAgentList != null && callCentreAgentList.Any())
                    {
                        var usernameList = new List<String>();
                        var ClientITems = new ListItemCollection();
                        callCentreAgentList.ForEach(f =>
                            {
                                ListItem liBox = new ListItem();
                                var userName = GetUserNameFromDomainLogonID(SitecoreFieldsHelper.GetValue(f, "User"));
                                liBox.Text = userName;
                                liBox.Value = userName;
                                usernameList.Add(userName);

                                ClientITems.Add(liBox);
                            });

                        BindDropdown(ddlAgentDropDown, ClientITems, "Text", "Value");

                        ddlAgentDropDown.Items.Insert(0, new ListItem
                                                            {
                                                                Text = "All Agents",
                                                                Value = string.Join(",", usernameList.Distinct().ToArray())
                                                            }
                                                     );

                    }
                }
            }
        }

        protected void btnExportIntoExcel_OnClick(object sender, EventArgs e)
        {
            PopulateDataOnGrid();
            Context.Response.ClearContent();
            Context.Response.ContentType = "application/ms-excel";
            Context.Response.AddHeader("content-disposition", string.Format("attachment;filename=AgentPerformanceReport_{0}.xls", DateTime.Now.ToString("yyyyMMdd_HHmm")));
            Context.Response.Charset = "";
            System.IO.StringWriter stringwriter = new System.IO.StringWriter();
            HtmlTextWriter htmlwriter = new HtmlTextWriter(stringwriter);
            reportRepeater.RenderControl(htmlwriter);
            stringwriter.Write(System.Web.HttpUtility.HtmlDecode(hidGridView.Value));
            Context.Response.Write(stringwriter.ToString());
            Context.Response.End();
        }

        private string GetUserNameFromDomainLogonID(string domainLoginId)
        {
            return (string.IsNullOrEmpty(domainLoginId) || !domainLoginId.Contains('\\'))
                                                ? domainLoginId
                                                :Convert.ToString(domainLoginId.Split('\\')[1]).Trim();
        }
    }
}