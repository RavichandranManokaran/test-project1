﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Security;
using Sitecore.Security.Accounts;
using System.Text.RegularExpressions;
using Sitecore.Data.Items;
using Affinion.LoyaltyBuild.Common.Utilities;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Helper
{
    public static class SitecoreUserProfile
    {
        /// <summary>
        /// Add item to User profile
        /// </summary>
        /// <param name="objectToSession"></param>
        /// <param name="profileName"></param>
        public static void AddItem(string objectToSession, string profileName)
        {
            User.Current.Profile.SetCustomProperty(profileName, objectToSession);
            User.Current.Profile.Save();
        }        
        
        /// <summary>
        /// Retrive cached item
        /// </summary>
        /// <typeparam name="T">Type of Session Item</typeparam>
        /// <param name="sessionName">Name of Session Item</param>
        /// <returns>Session Item as type</returns>
        public static string GetItem(string profileName)
        {
            try
            {
                return User.Current.Profile.GetCustomProperty(profileName);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Clear all custom property
        /// </summary>
        public static void ClearCustomProperty()
        {
            var allCustomProperties = User.Current.Profile.GetCustomPropertyNames();

            foreach (var item in allCustomProperties)
            {
                User.Current.Profile.RemoveCustomProperty(item);
                User.Current.Profile.Save();
            }

        }

        /// <summary>
        /// Get selected hotel Sku
        /// </summary>
        public static string SelectedSKU
        {
            get
            {
                var providerId = GetItem(UserProfileKeys.selectedProviderId);
                var sku = string.Empty;

                //if not null
                if (!string.IsNullOrWhiteSpace(providerId))
                {
                    Item selectedProvider = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(providerId));
                    if (selectedProvider != null)
                    {
                        return SitecoreFieldsHelper.GetItemFieldValue(selectedProvider, "SKU");
                    }
                }

                return null;

            }
        }
       


    }
    public abstract class UserProfileKeys
    {
        public const string loggedInSupplier = "supplierName";
        public const string loggedInUserName = "UserName";
        public const string loggedInContactName = "contactName";
        public const string GroupHotelName = "grpHotelName";
        public const string lastLoggedInDate = "LastLoggedInDate";
        public const string currentLoggedinUserRole = "LoggedInUserRole";
        public const string currentSupplierItemForUpdate = "CurrentItemForUpdate";
        public const string dropDownSelectedValue = "DropDownSelectedValue";
        public const string currentContactItemForUpdate = "CurrentContactItemForUpdate";
        public const string loggedInContactDisplayName = "LoggedInContactDisplayName";
        public const string loggedinSupplierDisplayName = "loggedinSupplierDisplayName";
       
        public const string listBoxSelectedValue = "listBoxSelectedValue";
        public const string supplierTypeDropDown = "supplierTypeDropDown";
        public const string typeDropDownSelectedValue = "typeDropDownSelectedValue";
        public const string listBoxSelectedText = "listBoxSelectedText";
        public const string groupListBoxSelectedText = "groupListBoxSelectedText";
        public const string loggedinDomainUser = "loggedinDomainUser";
        public const string loggedInUserAdmin = "loggedInUserAdmin";
        public const string loggedInProviderId = "loggedInProviderId";
        public const string selectedProviderId = "selectedProviderId";
        public const string loggedInContactID = "loggedInContactID";

        

    }


}