﻿using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.EntitiesV2;
using UCommerce.Presentation.Views.Marketing;
using UCommerce.Presentation.Web;
using UCommerce.Presentation.Web.Controls;
using Affinion.LoyaltyBuild.Common.Instrumentation;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.Ucommerce.Marketing.Awards
{
    public partial class FreeSkuUi : ViewEnabledControl<IEditCampaignItemView>, IConfigurable
    {
        List<DataTypeEnum> datatypeList;

        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            FreeSkuAward freeSkuData = this.DataSource as FreeSkuAward;

            DataType freeSku = DataType.All().Where(x => x.TypeName == "ProductType").ToList().FirstOrDefault();
            if (freeSku != null)
            {
                datatypeList = freeSku.DataTypeEnums.ToList();
            }
            this.DeleteButton.OnClientClick = string.Format("return confirm('Delete');");
            string[] freeSkus = freeSkuData.FreeSkuType.Split('|');
            chklstfreeSku.Items.Clear();
            foreach (var item in datatypeList)
            {
                //string freeSku = item.Value;
                ListItem items = new ListItem();
                items.Text = item.Value;
                items.Selected = freeSkus.Any(x => x.ToString() == item.Value);
                chklstfreeSku.Items.Add(items);
            }

            this.ReadOnlyMessage.Text = this.TargetToString(freeSkuData);

        }

        /// <summary>
        /// Save Room detail
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveButton_Click(object sender, ImageClickEventArgs e)
        {
            FreeSkuAward freeSkuData = this.DataSource as FreeSkuAward;
            this.ReadOnlyMessage.Text = this.TargetToString(freeSkuData);

            FreeSkuAward dataSource = this.DataSource as FreeSkuAward;
            string freeSku = string.Empty;
            foreach (ListItem item in chklstfreeSku.Items)
            {
                if (item.Selected)
                {
                    freeSku += item.Value + "|";
                }
            }
            if (freeSku.Length > 0)
            {
                freeSku = freeSku.Substring(0, freeSku.Length - 1);
            }
            dataSource.FreeSkuType = freeSku;
            dataSource.Save();
            this.ReadOnlyMessage.Text = this.TargetToString(dataSource);
            this.ToggleEditor();
        }

        /// <summary>
        /// enable edit & display view in page
        /// </summary>
        private void ToggleEditor()
        {
            this.NonEditModePlaceHolder.Visible = !this.NonEditModePlaceHolder.Visible;
            this.EditButton.Visible = !this.EditButton.Visible;
            this.EditModePlaceHolder.Visible = !this.EditModePlaceHolder.Visible;
            this.SaveButton.Visible = !this.SaveButton.Visible;
        }

        /// <summary>
        /// View Roomtype Act
        /// </summary>
        /// <param name="FreeSkuAward"></param>
        /// <returns></returns>
        private string TargetToString(FreeSkuAward FreeSkuAward)
        {

            string[] freeSkuItem = FreeSkuAward.FreeSkuType.Split('|').Where(x => !string.IsNullOrEmpty(x)).ToArray();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendFormat("Free SKU");
            stringBuilder.AppendFormat("{0} ", string.Join(",", freeSkuItem));
            return stringBuilder.ToString();


        }

        /// <summary>
        /// Delete room type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteButton_Click(object sender, ImageClickEventArgs e)
        {
            FreeSkuAward roomTypeData = this.DataSource as FreeSkuAward;
            if (roomTypeData != null)
            {
                roomTypeData.Delete();
            }
            this.Page.Response.Redirect(this.Page.Request.Url.ToString(), true);
        }

        /// <summary>
        /// Edit room type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditButton_Click(object sender, ImageClickEventArgs e)
        {
            this.ToggleEditor();
        }

        /// <summary>
        /// get set for apply
        /// </summary>
        public bool EnabledForApply
        {
            get;
            set;
        }

        /// <summary>
        /// get set for display
        /// </summary>
        public bool EnabledForDisplay
        {
            get;
            set;
        }

        /// <summary>
        /// Get set for datasource
        /// </summary>
        public object DataSource
        {
            get;
            set;

        }

        /// <summary>
        /// Create new Room Type
        /// </summary>
        /// <returns></returns>
        public object New()
        {
            var FreeSkuAward = new FreeSkuAward();
            try
            {
                FreeSkuAward.CampaignItem = View.CampaignItem;
                FreeSkuAward.Name = "Free SKU";
                FreeSkuAward.FreeSkuType = string.Empty;
                FreeSkuAward.Save();
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.MasterClassBusinessLogic, ex, this);
                throw;
            }
            this.DataSource = FreeSkuAward;
            return FreeSkuAward;
        }
    }
}