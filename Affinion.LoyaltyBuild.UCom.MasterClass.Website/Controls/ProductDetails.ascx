﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductDetails.ascx.cs" Inherits="Affinion.LoyaltyBuild.UCom.MasterClass.Website.Controls.ProductDetails" %>

<h2>
    <asp:Label runat="server" id="ProductSkuLabel"/>
</h2>
<asp:label runat="server" id="ProductPriceLabel"/>
<asp:HyperLink runat="server" id="ProductDetailsLink" Text="More details"/>