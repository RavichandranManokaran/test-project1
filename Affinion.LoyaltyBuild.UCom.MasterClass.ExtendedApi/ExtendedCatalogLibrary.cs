﻿using Affinion.LoyaltyBuild.UCom.Common.Constants;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Availability;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Component;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce;
using UCommerce.Catalog;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Marketing;
using UCommerce.Marketing.TargetingContextAggregators;
using UCommerce.Marketing.Targets.TargetResolvers;
using UCommerce.Runtime;
using UCommerce.Api;
using Affinion.LoyaltyBuild.UCom.Common.Extension;
using UCommerce.Marketing.Awards.AwardResolvers;
using UCommerce.Search.Facets;
using UCommerce.Search;
using NHibernate;
using NHibernate.Linq;
using Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi.Filter;
using System.Collections.ObjectModel;
using System.Reflection;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Queries;
using Affinion.LoyaltyBuild.DataAccess.Helpers;
using Raven.Json.Linq;
using Raven.Client.Linq;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Subrate;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi
{
    public static class LoyaltyBuildCatalogLibrary
    {
        public static LBPriceCalculation CalculatePrice(Product product, SearchCriteria criteria = null, ProductCatalog catalog = null)
        {


            return new LBPriceCalculation(product, criteria, catalog);
        }

        public static IList<UCommerce.Search.Facets.Facet> GetSelectedFacets(SearchCriteria criteira)
        {
            var facetsForQuerying = new List<Facet>(); // The facets we return

            var parameters = new Dictionary<string, string>();

            if (!string.IsNullOrEmpty(criteira.Country))
            {
                //criteira.Location = "UNITED KINGDOM";
                var facet = new Facet();
                facet.FacetValues = new List<FacetValue>();
                facet.Name = ProductDefinationConstant.Country;

                //In this example selected values are seperated by a '|' in the querystring value.
                foreach (var value in criteira.Country.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    facet.FacetValues.Add(new FacetValue() { Value = value });
                }
                facetsForQuerying.Add(facet);
            }

            if (!string.IsNullOrEmpty(criteira.Location))
            {
                //criteira.Location = "UNITED KINGDOM";
                var facet = new Facet();
                facet.FacetValues = new List<FacetValue>();
                facet.Name = ProductDefinationConstant.Location;

                //In this example selected values are seperated by a '|' in the querystring value.
                foreach (var value in criteira.Location.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    facet.FacetValues.Add(new FacetValue() { Value = value });
                }
                facetsForQuerying.Add(facet);
            }
            return facetsForQuerying;
        }

        //public static List<ProductAvaialbility> GetProductAvaialbilty(SearchAvaialbility searhAvaialbility)
        //{
        //    List<ProductAvaialbility> pavailability = new List<ProductAvaialbility>();
        //    int startDate = searhAvaialbility.CheckInDate.ConvetDatetoDateCounter();

        //    int endDate = searhAvaialbility.CheckOutDate.ConvetDatetoDateCounter();
        //    foreach (var item in searhAvaialbility.ProductDetail)
        //    {
        //        List<ProdutDayWiseAvaialbility> pdAvaialbility = new List<ProdutDayWiseAvaialbility>();
        //        for (int i = startDate; i < endDate; i++)
        //        {
        //            ProdutDayWiseAvaialbility ava = new ProdutDayWiseAvaialbility();
        //            ava.Date = i;
        //            ava.Price = new Random().Next(1, 100);
        //            ava.RoomAvaialbility = new Random().Next(1, 20);
        //            pdAvaialbility.Add(ava);
        //        }
        //        pavailability.Add(new ProductAvaialbility() { DayWiseAvaialbility = pdAvaialbility, Price = pdAvaialbility.Sum(x => x.Price), RoomAvaialbility = pdAvaialbility.Min(x => x.RoomAvaialbility), SKU = item.Split('@')[0], VariantSKU = item.Split('@')[1] });
        //    }
        //    return pavailability;
        //}

        public static List<Facet> PreparefacetQuery(SearchCriteria objSearch)
        {
            ICollection<IHotelFilter> hotelfilters = GetHotelFilters();
            //objSearch.Location = "London";
            //objSearch.Facility = "hiking";
            //objSearch.RoomType = "Single";
            //criteria.PackageType = "Bread & Break Fast";
            var facetsForQuerying = new List<Facet>(); // The facets we return
            foreach (var filter in hotelfilters)
            {
                Facet facetQuery = filter.FilterRecordFacet(objSearch);
                if (facetQuery != null)
                {
                    facetsForQuerying.Add(facetQuery);
                }
            }
            return facetsForQuerying;

        }

        public static List<Product> GetProducts(string keyword, string definitionName, int catalogId = 0)
        {
            try
            {
                if (string.IsNullOrEmpty(keyword) || string.IsNullOrEmpty(definitionName))
                    return new List<Product>();

                //HibernatingRhinos.Profiler.Appender.NHibernate.NHibernateProfiler.Initialize();
                IQueryable<Product> products = products = Product.All().Where(p => p.Name.ToLowerInvariant().Contains(keyword.ToLowerInvariant()) && p.ProductDefinition.Name.ToLowerInvariant().Contains(definitionName.ToLowerInvariant()));
                List<Product> filteredProducts = new List<Product>();

                if (catalogId == 0)
                {
                    filteredProducts = products.ToList();
                }
                else
                {
                    foreach (Product product in products)
                    {
                        bool match = product.CategoryProductRelations.Any(c => c.Category.ProductCatalog.Id == catalogId);
                        if (match)
                        {
                            filteredProducts.Add(product);
                        }
                    }
                }

                return filteredProducts;
            }
            catch (Exception ex)
            {
                return new List<Product>();
            }
        }

        public static List<Category> GetCategories(string keyword, string definitionName, int catalogId = 0)
        {
            try
            {
                if (string.IsNullOrEmpty(keyword) || string.IsNullOrEmpty(definitionName))
                    return new List<Category>();

                // HibernatingRhinos.Profiler.Appender.NHibernate.NHibernateProfiler.Initialize();
                IQueryable<Category> query = Category.All().Where(c => c.Definition.Name.ToLowerInvariant() == definitionName.ToLowerInvariant() && c.Name.Contains(keyword));
                List<Category> filteredCategories = new List<Category>();

                if (catalogId == 0)
                {
                    filteredCategories = query.ToList();
                }
                else
                {
                    foreach (Category category in query)
                    {
                        bool match = category.ProductCatalog.Id == catalogId;
                        if (match)
                        {
                            filteredCategories.Add(category);
                        }
                    }
                }

                return filteredCategories;
            }
            catch (Exception ex)
            {
                return new List<Category>();
            }
        }

        /// <summary>
        /// Get the parent category of a product by the definition id of the parent
        /// </summary>
        /// <param name="product">the product</param>
        /// <param name="definitionId">definition id of the parent</param>
        /// <returns>Returns the parent category</returns>
        public static Category GetParentCategory(Product product, string definitionName)
        {
            try
            {
                if (product == null || string.IsNullOrEmpty(definitionName))
                    return null;

                Category category = null;
                Product parentProduct = product.ParentProduct;

                if (parentProduct != null)
                {
                    // if the parent product is not null this is a variant product
                    // call recursively till parent is not a product, then it is a category
                    GetParentCategory(parentProduct, definitionName);
                }
                else
                {
                    category = product.GetCategories().Where(c => c.Definition.Name.ToLowerInvariant() == definitionName.ToLowerInvariant()).FirstOrDefault();
                }

                return category;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Get the parent category of a product by the definition id of the parent
        /// </summary>
        /// <param name="category">the category</param>
        /// <param name="definitionId">definition id of the parent</param>
        /// <returns>Returns the parent category</returns>
        public static Category GetParentCategory(Category category, string definitionName)
        {
            try
            {
                if (category == null || string.IsNullOrEmpty(definitionName))
                    return null;

                Category parentCategory = category.ParentCategory;

                if (parentCategory != null)
                {
                    if (parentCategory.Definition.Name.ToLowerInvariant() == definitionName.ToLowerInvariant())
                    {
                        return parentCategory;
                    }
                    else
                    {
                        // recursive function till parent found or null
                        GetParentCategory(parentCategory, definitionName);
                    }
                }

                return parentCategory;
            }
            catch (Exception ex)
            {
                return null;
            }
        }



        public static List<HotelDetail> SearchHotels(SearchCriteria criteria)
        {
            List<HotelProductDetail> hotelProductDetail = new List<HotelProductDetail>();
            if (!string.IsNullOrEmpty(criteria.Themes))
            {
                criteria.Themes = criteria.Themes.ToLower();
            }
            if (!string.IsNullOrEmpty(criteria.Facility))
            {
                criteria.Facility = criteria.Facility.ToLower();
            }
            if (!string.IsNullOrEmpty(criteria.Experience))
            {
                criteria.Experience = criteria.Experience.ToLower();
            }
            
            //TODO : Need to change code & category value will set from Client portal & Offline Portal.
            //criteria.SearchCategory = Category.All().Where(x => x.CategoryId == 1078).FirstOrDefault();
            List<LBChildCategory> childCategory = ObjectFactory
                .Instance
                .Resolve<IRepository<LBChildCategory>>()
                .Select(new GetAllChildCategoryQuery(criteria.SearchCategory.CategoryId)).ToList();
            Category currentCategory = criteria.SearchCategory;
            List<int> categoryId = childCategory.Select(x => x.CategoryId).ToList();
            var query = UCommerce.Api.SearchLibrary.FacetedQuery();

            List<UCommerce.Documents.Product> filterProduct = null;
            //if searched based on product
            if (criteria.SearchHotel != null)
            {
                query = query.Where(x => x.Id == criteria.SearchHotel.Value);
                filterProduct = query.WithFacets(PreparefacetQuery(criteria)).ToList();
            }
            else
            {
                query = query.Where(x => x.CategoryIds.Any(id => id.In<int>(categoryId)));
                filterProduct = UCommerce.Api.SearchLibrary.GetProductsFor(criteria.SearchCategory, PreparefacetQuery(criteria)).ToList();
            }

            // List<UCommerce.Documents.Product> filterProduct = query.WithFacets(PreparefacetQuery(criteria)).ToList();

            //List<UCommerce.Documents.Product> filterProduct = UCommerce.Api.SearchLibrary.GetProductsFor(criteria.SearchCategory, PreparefacetQuery(criteria)).ToList();
            // List<Facet> facetDat = UCommerce.Api.SearchLibrary.GetFacetsFor(criteria.SearchCategory, new List<Facet>()).ToList();
            List<string> vSku = new List<string>();
            foreach (var item in filterProduct)
            {
                if (item.Variants != null && item.Variants.Any())
                {
                    vSku.AddRange(item.Variants.Select(x => x.VariantSku));
                }
            }
            List<Availability> roomAvaialbility = GetRoomAvailabilityWithPrice(vSku, criteria.ArrivalDate.ConvetDatetoDateCounter(), criteria.ArrivalDate.ConvetDatetoDateCounter() + criteria.Night - 1);
            List<HotelDetail> productDetail = new List<HotelDetail>();
            foreach (var item in filterProduct)
            {
                HotelDetail detail = new HotelDetail();
                detail.Name = item.Name;
                detail.HotelProductDetail = new List<HotelProductDetail>();
                try
                {
                    Dictionary<string, object> property = item.Properties.Select(x => x.Value).FirstOrDefault();
                    var sitecoreItemId = property.FirstOrDefault(i => i.Key.Equals(ProductDefinationConstant.SiteCoreItemGuid));
                    if (!sitecoreItemId.Equals(default(KeyValuePair<string, string>)))
                    {
                        detail.ItemId = sitecoreItemId.Value.ToString();
                    }
                    var starRating = property.FirstOrDefault(i => i.Key.Equals(ProductDefinationConstant.HotelRating));
                    if (!starRating.Equals(default(KeyValuePair<string, string>)))
                    {
                        int ranking = 0;
                        if (int.TryParse(starRating.Value.ToString(), out ranking))
                        {
                            detail.StarRanking = ranking;
                        }
                    }
                    var localRating = property.FirstOrDefault(i => i.Key.Equals(ProductDefinationConstant.Recommended));
                    if (!localRating.Equals(default(KeyValuePair<string, string>)))
                    {
                        int ranking = 0;
                        if (int.TryParse(localRating.Value.ToString(), out ranking))
                        {
                            detail.LocalRanking = ranking;
                        }
                    }
                }
                catch (Exception er)
                { }
                foreach (var rooms in item.Variants)
                {
                    Availability aData = roomAvaialbility.FirstOrDefault(x => x.SKU == rooms.Sku && x.VaiantSKU == rooms.VariantSku) ?? new Availability();
                    Dictionary<string, object> property = rooms.Properties.Select(x => x.Value).FirstOrDefault();
                    HotelProductDetail hotelRoomDetail = new HotelProductDetail();
                    hotelRoomDetail.Availability = aData.AvailableRoom;
                    hotelRoomDetail.CampaignItemId = 0;
                    hotelRoomDetail.DiscountedPrice = 0;
                    detail.SKU = rooms.Sku;
                    hotelRoomDetail.IsDiscounted = false;
                    hotelRoomDetail.MaxAdults = property[ProductDefinationVairantConstant.MaxNumberOfAdults] != null ? Convert.ToInt32(property[ProductDefinationVairantConstant.MaxNumberOfAdults].ToString()) : 0;
                    hotelRoomDetail.MaxChildren = property[ProductDefinationVairantConstant.MaxNumberOfChildren] != null ? Convert.ToInt32(property[ProductDefinationVairantConstant.MaxNumberOfChildren].ToString()) : 0;
                    hotelRoomDetail.MaxInfants = property[ProductDefinationVairantConstant.MaxNumberOfInfants] != null ? Convert.ToInt32(property[ProductDefinationVairantConstant.MaxNumberOfInfants].ToString()) : 0;
                    hotelRoomDetail.MaxPeople = property[ProductDefinationVairantConstant.MaxNumberOfPeople] != null ? Convert.ToInt32(property[ProductDefinationVairantConstant.MaxNumberOfPeople].ToString()) : 0;
                    hotelRoomDetail.Price = aData.Price;
                    hotelRoomDetail.Sku = rooms.Sku;
                    hotelRoomDetail.SubrateApplied = true;
                    hotelRoomDetail.VariatSku = rooms.VariantSku;
                    hotelRoomDetail.Name = rooms.Name;
                    hotelRoomDetail.RoomAvailability = aData.DayWiseAvailability;
                    detail.HotelProductDetail.Add(hotelRoomDetail);
                }
                productDetail.Add(detail);
            }
            //if (productDetail != null && productDetail.Any())
            //{
            //    List<string> sku = productDetail.Where(x => !string.IsNullOrEmpty(x.SKU)).Select(x => x.SKU).ToList(); ;
            //    List<HotelProductDetail> campaignList = GetApplicableCampaign(sku, criteria);
            //    if (campaignList != null && campaignList.Any())
            //    {
            //        for (int i = 0; i < productDetail.Count; i++)
            //        {
            //            List<HotelProductDetail> applicableCampaign = campaignList.Where(x => x.Sku == productDetail[i].SKU).ToList();
            //            if (applicableCampaign != null && applicableCampaign.Any())
            //            {
            //                productDetail[i].HotelProductDetail.AddRange(applicableCampaign);
            //            }
            //        }
            //    }
            //}
            return productDetail;

        }

        public static List<Availability> GetRoomPrice(string Sku, string variantSKU, int startDate, int endDate)
        {
            List<Availability> availabilty = GetRoomAvailabilityWithPrice(new List<string>() { variantSKU }, startDate, endDate);
            if (!string.IsNullOrEmpty(Sku))
            {
                availabilty = availabilty.Where(x => x.SKU == Sku).ToList();
            }
            return availabilty;
        }

        public static List<Availability> GetRoomAvailabilityWithPrice(List<string> variantSKU, int startDate, int endDate)
        {
            List<LBAvailaiblity> roomAvaialbility = ObjectFactory
                .Instance
                .Resolve<IRepository<LBAvailaiblity>>()
                .Select(new AvailabilityByDateQuery(string.Join(",", variantSKU), startDate, endDate)).ToList();

            List<Availability> data = new List<Availability>();
            foreach (var item in roomAvaialbility.GroupBy(x => new { x.Sku, x.VariantSku }))
            {
                Availability roomAva = new Availability();
                roomAva.SKU = item.Key.Sku;
                roomAva.VaiantSKU = item.Key.VariantSku;
                roomAva.StartDate = startDate;
                roomAva.EndDate = endDate;
                List<DayWiseAvaialbility> dwAvailability = new List<DayWiseAvaialbility>();
                for (int i = startDate; i <= endDate; i++)
                {
                    LBAvailaiblity availaiblitydata = item.FirstOrDefault(x => x.DateCounter == i);
                    if (availaiblitydata == null)
                        availaiblitydata = new LBAvailaiblity();
                    dwAvailability.Add(new DayWiseAvaialbility() { AvailableRoom = (availaiblitydata.IsCloseOut || availaiblitydata.Price == 0) ? 0 : availaiblitydata.AvaialbileRoom, DateCounter = availaiblitydata.DateCounter, Price = availaiblitydata.Price, PriceBand = availaiblitydata.Priceband });
                }
                //foreach (var ava in item)
                //{
                //    dwAvailability.Add(new DayWiseAvaialbility() { AvailableRoom = (ava.IsCloseOut || ava.Price == 0) ? 0 : ava.AvaialbileRoom, DateCounter = ava.DateCounter, Price = ava.Price, PriceBand = ava.Priceband });
                //}
                roomAva.AvailableRoom = dwAvailability.Min(x => x.AvailableRoom);
                roomAva.Price = dwAvailability.Sum(x => x.Price);
                roomAva.DayWiseAvailability = dwAvailability;
                data.Add(roomAva);
            }
            return data;
        }

        public static void GetPromotions(string sku, SearchCriteria objSearch)
        {

        }

        public static HotelDetail GetHotelsRoomAndPromotion(SearchCriteria criteria, String Sku)
        {

            List<Product> filterProduct = Product.All().Where(x => x.Sku == Sku).ToList();
            List<string> vSku = new List<string>();
            foreach (var item in filterProduct)
            {
                if (item.Variants != null && item.Variants.Any())
                {
                    vSku.AddRange(item.Variants.Select(x => x.VariantSku));
                }
            }
            List<Availability> roomAvaialbility = GetRoomAvailabilityWithPrice(vSku, criteria.ArrivalDate.ConvetDatetoDateCounter(), criteria.ArrivalDate.ConvetDatetoDateCounter() + criteria.Night - 1);
            List<HotelDetail> productDetail = new List<HotelDetail>();
            foreach (var item in filterProduct)
            {
                HotelDetail detail = new HotelDetail();
                detail.Name = item.Name;
                detail.HotelProductDetail = new List<HotelProductDetail>();
                try
                {
                    ProductProperty sitecoreItemId = item.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationConstant.SiteCoreItemGuid);// [ProductDefinationConstant.SiteCoreItemGuid];
                    if (sitecoreItemId != null)
                    {
                        detail.ItemId = sitecoreItemId.Value.ToString();
                    }
                }
                catch (Exception er)
                { }
                foreach (var rooms in item.Variants)
                {
                    Availability aData = roomAvaialbility.FirstOrDefault(x => x.SKU == rooms.Sku && x.VaiantSKU == rooms.VariantSku) ?? new Availability();
                    List<ProductProperty> property = rooms.ProductProperties.ToList();
                    HotelProductDetail hotelRoomDetail = new HotelProductDetail();
                    hotelRoomDetail.Availability = aData.AvailableRoom;
                    hotelRoomDetail.CampaignItemId = 0;
                    hotelRoomDetail.DiscountedPrice = 0;
                    hotelRoomDetail.IsDiscounted = false;
                    ProductProperty adulltProdery = item.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationVairantConstant.MaxNumberOfAdults);
                    if (adulltProdery != null)
                    {
                        hotelRoomDetail.MaxAdults = Convert.ToInt32(adulltProdery.Value);
                    }
                    ProductProperty childProdery = item.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationVairantConstant.MaxNumberOfChildren);
                    if (childProdery != null)
                    {
                        hotelRoomDetail.MaxAdults = Convert.ToInt32(childProdery.Value);
                    }
                    ProductProperty infrantProperty = item.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationVairantConstant.MaxNumberOfInfants);
                    if (adulltProdery != null)
                    {
                        hotelRoomDetail.MaxAdults = Convert.ToInt32(infrantProperty.Value);
                    }
                    ProductProperty peopleProdery = item.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationVairantConstant.MaxNumberOfPeople);
                    if (peopleProdery != null)
                    {
                        hotelRoomDetail.MaxAdults = Convert.ToInt32(peopleProdery.Value);
                    }
                    hotelRoomDetail.Price = aData.Price;
                    hotelRoomDetail.Sku = item.Sku;
                    hotelRoomDetail.SubrateApplied = true;
                    hotelRoomDetail.VariatSku = rooms.VariantSku;
                    hotelRoomDetail.RoomAvailability = aData.DayWiseAvailability;
                    detail.HotelProductDetail.Add(hotelRoomDetail);
                }
                productDetail.Add(detail);
            }
            return productDetail.FirstOrDefault();

        }


        private static ICollection<IHotelFilter> GetHotelFilters()
        {
            ICollection<IHotelFilter> filters = new Collection<IHotelFilter>();
            try
            {
                Type[] types = Assembly.GetExecutingAssembly().GetTypes().
                    Where(t =>
                        String.Equals(t.Namespace, "Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi.Filter", StringComparison.Ordinal) &&
                        Type.Equals(t.GetInterface("IHotelFilter"), typeof(IHotelFilter))
                    ).ToArray();

                foreach (Type type in types)
                {
                    IHotelFilter instance = (IHotelFilter)Activator.CreateInstance(type);
                    filters.Add(instance);
                }

                return filters;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private static ICollection<IProductFilter> GetProductFilters()
        {
            ICollection<IProductFilter> filters = new Collection<IProductFilter>();
            try
            {
                Type[] types = Assembly.GetExecutingAssembly().GetTypes().
                    Where(t =>
                        String.Equals(t.Namespace, "Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi.Filter", StringComparison.Ordinal) &&
                        Type.Equals(t.GetInterface("IProductFilter"), typeof(IProductFilter))
                    ).ToArray();

                foreach (Type type in types)
                {
                    IProductFilter instance = (IProductFilter)Activator.CreateInstance(type);
                    filters.Add(instance);
                }

                return filters;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static List<HotelProductDetail> GetApplicableCampaign(List<string> SKU, SearchCriteria searchCriteria, string varianSku = null, int promotionid = 0)
        {
            ICatalogContext catalogContext = ObjectFactory.Instance.Resolve<ICatalogContext>();
            IDiscountService discountService = ObjectFactory.Instance.Resolve<IDiscountService>();
            IPricingService pricingService = ObjectFactory.Instance.Resolve<IPricingService>();
            ITaxService taxService = ObjectFactory.Instance.Resolve<ITaxService>();

            int sDate = searchCriteria.ArrivalDate.ConvetDatetoDateCounter();
            int eDate = sDate + searchCriteria.Night - 1;
            LBSearhResult result = SearchResultHelper.GetAvailableProductList(SKU, sDate, eDate);

            List<int> supplierInviteForCampaign = result.OfferAvailability.Select(x => x.OfferId).Distinct().ToList();
            Currency cDetail = Currency.All().FirstOrDefault();
            string suppliierId = string.Empty;

            List<LBProductVariant> product = string.IsNullOrWhiteSpace(varianSku) ? result.Product : result.Product.Where(x => x.VSKU.Equals(varianSku)).ToList();
            //.Where(x=>x.VSKU.Equals(varianSku));

            List<CampaignItem> activeCampaignItems = ObjectFactory
               .Instance
               .Resolve<IRepository<CampaignItem>>()
               .Select(new LBActiveCampaignItemsQuery(searchCriteria.ArrivalDate, searchCriteria.ArrivalDate.AddDays(searchCriteria.Night - 1))).ToList();

            //IRepository<CampaignItem> CampaignItemRepository = ObjectFactory.Instance.Resolve<IRepository<CampaignItem>> 
            PurchaseOrder purchaseOrder = new PurchaseOrder
            {
                BillingCurrency = cDetail,
                ProductCatalogGroup = catalogContext.CurrentCatalogGroup
            };

            List<HotelProductDetail> productDetail = new List<HotelProductDetail>();
            LBMarketingService service = new LBMarketingService(ObjectFactory.Instance.Resolve<IRepository<CampaignItem>>(), ObjectFactory.Instance.Resolve<ITargetingContextAggregator>(), ObjectFactory.Instance.Resolve<ITargetAggregator>(), ObjectFactory.Instance.Resolve<IAwardAggregator>());


            if (purchaseOrder.ProductCatalogGroup != null)
            {
                activeCampaignItems = activeCampaignItems.Where<CampaignItem>(delegate(CampaignItem x)
                {
                    if (!x.Campaign.ProductCatalogGroups.Contains(purchaseOrder.ProductCatalogGroup))
                    {
                        return !x.Campaign.ProductCatalogGroups.Any<ProductCatalogGroup>();
                    }
                    return true;
                }).ToList<CampaignItem>();
            }

            IRoomAvailabilityService roomAvailabityService = new RoomAvailabilityService();
            foreach (CampaignItem item in activeCampaignItems)
            {
                if ((promotionid != 0) && item.CampaignItemId != promotionid)
                {
                    continue;
                }
                if (!supplierInviteForCampaign.Any(x => x == item.CampaignItemId))
                {
                    continue;
                }

                CampaignItemProperty itemProperty = ((CampaignItemProperty)item[CampaignDataTyeConstant.IsApprove]);
                bool isApproveCampaign = false;
                if (itemProperty != null)
                {
                    if (bool.TryParse(itemProperty.Value, out isApproveCampaign))
                    {

                    }
                }
                if (!isApproveCampaign)
                {
                    continue;
                }

                List<string> sitecorePriceBand = result.RoomAvailability.Select(x => x.PriceBand.ToString()).Distinct().ToList();
                SubrateService subrateService = new SubrateService();
                List<RommCommission> commissiondata = subrateService.GetCommissionFromPriceBand(sitecorePriceBand, searchCriteria.Night);

                List<string> offerSKU = result.OfferAvailability.Where(x => x.OfferId == item.CampaignItemId).Select(x => x.SKU).Distinct().ToList();
                foreach (var oSku in offerSKU)
                {
                    List<RoomOfferAvailability> oAvaialbility = result.OfferAvailability.Where(x => x.SKU == oSku).ToList();
                    List<RoomOfferAvailability> rAvaialbility = result.RoomAvailability.Where(x => x.SKU == oSku).ToList();
                    List<AdditionalInfo> pProperty = result.ProductProperty.Where(x => x.SKU == oSku).ToList();
                    List<LBProductVariant> addOnProduct = new List<LBProductVariant>();
                    List<LBProductVariant> ignoreProduct = new List<LBProductVariant>();
                    foreach (var rooms in product.Where(x => x.SKU == oSku && !string.IsNullOrEmpty(x.VSKU)))
                    {
                        List<AdditionalInfo> pvProperty = result.ProductVariantProperty.Where(x => x.SKU == oSku && x.VSKU == rooms.VSKU).ToList();
                        string foodType = GetPropertyValue(pvProperty, ProductDefinationVairantConstant.FoodType);
                        string roomType = GetPropertyValue(pvProperty, ProductDefinationVairantConstant.RoomType);
                        string pType = GetPropertyValue(pvProperty, ProductDefinationVairantConstant.PackageType);
                        if (!string.IsNullOrEmpty(foodType) && string.IsNullOrEmpty(roomType))
                        {
                            addOnProduct.Add(rooms);
                        }
                        else if (!string.IsNullOrEmpty(foodType) && !string.IsNullOrEmpty(roomType))
                        {
                            ignoreProduct.Add(rooms);
                        }
                    }
                    foreach (var rooms in product.Where(x => x.SKU == oSku && !string.IsNullOrEmpty(x.VSKU)))
                    {
                        if (!String.IsNullOrEmpty(varianSku))
                        {
                            if (rooms.VSKU != varianSku)
                            {
                                continue;
                            }

                        }
                        int roomAvaialbility = FindRoomAvailbilityBySKU(rAvaialbility, sDate, eDate, rooms.VSKU);

                        //TODO : Change logic for Commission Calculation
                        decimal commissionAmount = 0;
                        if (rAvaialbility != null && rAvaialbility.Any())
                        {
                            RommCommission data = commissiondata.FirstOrDefault(x => x.PriceBandId == rAvaialbility.FirstOrDefault().PriceBand.ToString());
                            if (data != null)
                            {
                                commissionAmount = data.Amount;
                            }
                        }
                        int offerAvaialbility = FindOfferAvailbilityBySKU(oAvaialbility, sDate, eDate, item.CampaignItemId, rooms.VSKU);
                        if (roomAvaialbility > 0 && offerAvaialbility > 0)
                        {
                            purchaseOrder.OrderLines.Clear();
                            //new List<OrderLine>();
                            List<AdditionalInfo> pvProperty = result.ProductVariantProperty.Where(x => x.SKU == oSku && x.VSKU == rooms.VSKU).ToList();
                            decimal roomPrice = 0;
                            if (rAvaialbility != null && rAvaialbility.Any(x => x.SKU == rooms.SKU && x.VSKU == rooms.VSKU))
                            {
                                roomPrice = rAvaialbility.Where(x => x.SKU == rooms.SKU && x.VSKU == rooms.VSKU).Sum(x => x.Price);
                            }
                            purchaseOrder.AddOrderLine(AddProductToOrderLineInMemory(rooms, roomPrice, searchCriteria, pProperty, pvProperty, cDetail, false, commissionAmount));
                            while (purchaseOrder.Discounts.Count > 0)
                            {
                                Discount discount = purchaseOrder.Discounts.First<Discount>();
                                purchaseOrder.RemoveDiscount(discount);
                            }

                            AddOnTarget targets = null; // AddOnTarget.All().Where(x => x.CampaignItem.CampaignItemId == item.Id).FirstOrDefault();
                            if (targets != null)
                            {
                                int requiredAddOnProduct = targets.AddOn.Split('|').Length;
                                int existProductCount = 0;
                                foreach (var addontarget in targets.AddOn.Split('|'))
                                {
                                    foreach (var addon in addOnProduct)
                                    {
                                        List<AdditionalInfo> addOnpvProperty = result.ProductVariantProperty.Where(x => x.SKU == oSku && x.VSKU == rooms.VSKU).ToList();

                                        List<decimal> addOnPrice = roomAvailabityService.GetAddOnPrice(addon.VSKU, addon.SKU);
                                        if (addOnpvProperty != null && addOnpvProperty.Any())
                                        {
                                            //TODO : Count Commission for Add On
                                            purchaseOrder.AddOrderLine(AddProductToOrderLineInMemory(rooms, addOnPrice[1], searchCriteria, pProperty, addOnpvProperty, cDetail, false, commissionAmount));
                                            existProductCount++;
                                        }
                                    }
                                }
                                if (requiredAddOnProduct != existProductCount)
                                    continue;
                            }
                            if (service.Apply(purchaseOrder, item))
                            {
                                List<Discount> discoutList = new List<Discount>();
                                decimal discountOrderLine = 0;
                                foreach (var orderLines in purchaseOrder.OrderLines)
                                {
                                    if (orderLines.Discounts != null && orderLines.Discounts.Any())
                                    {
                                        discountOrderLine = orderLines.Discounts.Sum(x => x.AmountOffTotal);
                                        discoutList.AddRange(orderLines.Discounts);
                                    }
                                }

                                HotelProductDetail hotelRoomDetail = new HotelProductDetail();
                                hotelRoomDetail.Availability = offerAvaialbility;
                                hotelRoomDetail.CampaignItemId = item.CampaignItemId;
                                hotelRoomDetail.DiscountedPrice = 0;
                                hotelRoomDetail.IsDiscounted = true;
                                AdditionalInfo adulltProdery = pvProperty.FirstOrDefault(x => x.Name == ProductDefinationVairantConstant.MaxNumberOfAdults);
                                if (adulltProdery != null)
                                {
                                    hotelRoomDetail.MaxAdults = Convert.ToInt32(adulltProdery.Value);
                                }
                                AdditionalInfo childProdery = pvProperty.FirstOrDefault(x => x.Name == ProductDefinationVairantConstant.MaxNumberOfChildren);
                                if (childProdery != null)
                                {
                                    hotelRoomDetail.MaxChildren = Convert.ToInt32(childProdery.Value);
                                }
                                AdditionalInfo infrantProperty = pvProperty.FirstOrDefault(x => x.Name == ProductDefinationVairantConstant.MaxNumberOfInfants);
                                if (infrantProperty != null)
                                {
                                    hotelRoomDetail.MaxInfants = Convert.ToInt32(infrantProperty.Value);
                                }
                                AdditionalInfo peopleProdery = pvProperty.FirstOrDefault(x => x.Name == ProductDefinationVairantConstant.MaxNumberOfPeople);
                                if (peopleProdery != null)
                                {
                                    hotelRoomDetail.MaxPeople = Convert.ToInt32(peopleProdery.Value);
                                }
                                hotelRoomDetail.Sku = rooms.SKU;
                                hotelRoomDetail.SubrateApplied = true;
                                hotelRoomDetail.VariatSku = rooms.VSKU;
                                //hotelRoomDetail.RoomAvailability = oAvaialbility.Select(x => new DayWiseAvaialbility() { AvailableRoom = x.Availability, DateCounter = x.DateCounter, Price = 0 }).ToList();
                                hotelRoomDetail.Name = string.Format("{0} ({1})", rooms.Name, item.Name);
                                if (rAvaialbility != null && rAvaialbility.Any(x => x.SKU == rooms.SKU && x.VSKU == rooms.VSKU))
                                {
                                    hotelRoomDetail.Price = rAvaialbility.Where(x => x.SKU == rooms.SKU && x.VSKU == rooms.VSKU).Sum(x => x.Price) - discountOrderLine;
                                }
                                hotelRoomDetail.DiscountedPrice = discountOrderLine;
                                productDetail.Add(hotelRoomDetail);
                            }
                        }
                    }
                }
            }
            purchaseOrder = null;
            return productDetail;
        }



        public static int FindRoomAvailbilityBySKU(List<RoomOfferAvailability> offerAvailaiblity, int startDateCounter, int endDateCounter, string VSKU)
        {
            int offerRoomavailability = 0;
            for (int i = startDateCounter; i <= (endDateCounter); i++)
            {
                RoomOfferAvailability availability = offerAvailaiblity.FirstOrDefault(x => x.DateCounter == (i) && x.VSKU == VSKU);
                if (availability != null)
                {
                    if (i == startDateCounter)
                        offerRoomavailability = availability.Availability;
                    if (availability.Availability <= offerRoomavailability)
                    {
                        offerRoomavailability = availability.Availability;
                    }
                }
                else
                    return 0;
            }
            return offerRoomavailability;
        }

        public static int FindOfferAvailbilityBySKU(List<RoomOfferAvailability> offerAvailaiblity, int startDateCounter, int endDateCounter, int campaignItemId, string VSKU)
        {
            int offerRoomavailability = 0;
            for (int i = startDateCounter; i <= (endDateCounter); i++)
            {
                RoomOfferAvailability availability = offerAvailaiblity.FirstOrDefault(x => x.DateCounter == (i) && x.OfferId == campaignItemId && x.VSKU == VSKU);
                if (availability != null)
                {
                    if (i == startDateCounter)
                        offerRoomavailability = availability.Availability;
                    if (availability.Availability <= offerRoomavailability)
                    {
                        offerRoomavailability = availability.Availability;
                    }
                }
                else
                    return 0;
            }
            return offerRoomavailability;
        }

        private static OrderLine AddProductToOrderLineInMemory(LBProductVariant product, decimal price, SearchCriteria searchCriteria, List<AdditionalInfo> productProperty, List<AdditionalInfo> variantProperty, Currency orderCurrency, bool addOnProduct = false, decimal commissionAmount = 0)
        {
            List<OrderProperty> orderProperties = searchCriteria.CreateOrderLineProperty();
            if (addOnProduct)
            {
                orderProperties.Add(new OrderProperty() { Key = OrderPropertyConstants.IgnorInSearch, Value = "1" });
            }
            orderProperties.Add(new OrderProperty() { Key = OrderPropertyConstants.ForSearch, Value = "1" });

            orderProperties.Add(new OrderProperty() { Key = OrderPropertyConstants.HotelType, Value = GetPropertyValue(productProperty, ProductDefinationConstant.HotelType) });
            orderProperties.Add(new OrderProperty() { Key = OrderPropertyConstants.HotelRating, Value = GetPropertyValue(productProperty, ProductDefinationConstant.HotelRating) });
            orderProperties.Add(new OrderProperty() { Key = OrderPropertyConstants.Location, Value = GetPropertyValue(productProperty, ProductDefinationConstant.Location) });
            orderProperties.Add(new OrderProperty() { Key = OrderPropertyConstants.RoomType, Value = GetPropertyValue(variantProperty, ProductDefinationVairantConstant.RoomType) });
            orderProperties.Add(new OrderProperty() { Key = BookingOrderConstant.CommissionAmount, Value = commissionAmount.ToString() });
            orderProperties.Add(new OrderProperty() { Key = BookingOrderConstant.ProviderAmount, Value = (price - commissionAmount).ToString() });
            Money productPrice = new Money(price, orderCurrency);
            OrderLine orderLine = new OrderLine
            {
                Sku = product.SKU,
                VariantSku = product.VSKU,
                Price = productPrice.Value,
                VAT = 0,
                VATRate = 0,
                Quantity = 1,
                OrderProperties = orderProperties
            };
            return orderLine;
        }

        private static string GetPropertyValue(List<AdditionalInfo> data, string propertyName, string defaultValue = null)
        {
            AdditionalInfo info = data.FirstOrDefault(x => x.Name == propertyName);
            if (info != null)
            {
                return info.Value;
            }
            if (!string.IsNullOrEmpty(defaultValue))
            {
                return defaultValue;
            }
            return string.Empty;
        }
    }
}
