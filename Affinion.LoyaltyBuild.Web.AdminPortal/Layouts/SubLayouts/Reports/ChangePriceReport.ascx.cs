﻿using Affinion.LoyaltyBuild.AdminPortal.Model;
using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.UCom.Common.Constants;
using Affinion.LoyaltyBuild.UCom.Common.Extension;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports;
using Newtonsoft.Json;
using Sitecore.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.EntitiesV2;

namespace Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.Reports
{
    public partial class ChangePriceReport : System.Web.UI.UserControl
    {
        static List<PriceChangeReportData> lstPriceBandReport = null;
        Sitecore.Data.Database sitecoreDatabase = Sitecore.Configuration.Factory.GetDatabase("master");

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadCountryDropDown();
                LoadThemeDropDown();
                LoadStarsDropDown();
                LoadPriceBandFromDropDown();

                ListItem allLocation = new ListItem("All Location", "0");
                ddlLocation.Items.Insert(0, allLocation);
                ListItem allMapLocation = new ListItem("All MapLocation", "0");
                ddlMapLocation.Items.Insert(0, allMapLocation);
            }
        }

        public List<PriceChangeReportData> PriceBand()
        {
            Database contextDb = Sitecore.Context.Database;
            ReportsService r = new ReportsService();

            var startDate = InputDateFormatting(txtFromDate.Value);
            var toDate = InputDateFormatting(txtToDate.Value);
            var allDates = new List<DateTime>();

            for (DateTime date = startDate; date <= toDate; date = date.AddDays(1))
                allDates.Add(date);

            var strChangePrice = r.PriceChangeReport(startDate, toDate);
            List<PriceChangeReportData> lstPrice = new List<PriceChangeReportData>();

            foreach (var item in strChangePrice)
            {
                var itemwithremove = item.Replace("[", "").Replace("]", "");
                PriceChangeReportData priceChange = JsonConvert.DeserializeObject<PriceChangeReportData>(itemwithremove);
                priceChange.ProviderName = UCommerce.Api.CatalogLibrary.GetProduct(priceChange.Sku).Name;

                Product existingProduct = !string.IsNullOrEmpty(priceChange.Sku) ? Product.All().Where(x => x.Sku == priceChange.Sku && x.ParentProduct == null).FirstOrDefault() : null;
                ProductProperty propertyCountry = existingProduct.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationConstant.Country);
                var country = propertyCountry.Value;
                ProductProperty propertyLocation = existingProduct.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationConstant.Location);
                var location = propertyLocation.Value;
                ProductProperty propertyMaplocation = existingProduct.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationConstant.Region);
                var mapLocation = propertyMaplocation.Value;

                ProductProperty propertyTheme = existingProduct.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationConstant.Theme);
                var theme = propertyTheme.Value;

                ProductProperty propertyStarRating = existingProduct.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationConstant.HotelRating);
                var starRating = propertyStarRating.Value;

                var packageProducts = Product.FirstOrDefault(i => i.VariantSku == priceChange.VariantSku).ProductProperties.
                     FirstOrDefault(x => x.ProductDefinitionField.Name.Equals(ProductDefinationVairantConstant.RoomType));

                //Map RoomType 
                if (packageProducts != null)
                {
                    priceChange.RoomType = packageProducts.Value;
                }

                //Map PriceBand
                var items = contextDb.GetItem(Constants.Pricaband);
                if (items != null)
                {
                    var priceBand = items.Axes.GetDescendants().Where(a => a.TemplateName.Equals("PriceBand")).FirstOrDefault(p => p.ID.ToString().ToUpper() == "{" + priceChange.PriceBand.ToUpper() + "}");
                    if (priceBand != null)
                    {
                        priceChange.PriceBand = priceBand.Name + " (" + priceBand.Fields["Rate"] + ")";
                    }

                }

                var flag = false;


                if ((country == ddlCountry.SelectedItem.Text || ddlCountry.SelectedValue == "0") &&
                    (location == ddlLocation.SelectedItem.Text || ddlLocation.SelectedValue == "0") &&
                    (mapLocation == ddlMapLocation.SelectedItem.Text || ddlMapLocation.SelectedValue == "0") &&
                    (theme == ddlTheme.SelectedItem.Text || ddlTheme.SelectedValue == "0") &&
                    (starRating == ddlStarsRating.SelectedItem.Text || ddlStarsRating.SelectedValue == "0"))
                    flag = true;

                if (flag)
                    lstPrice.Add(priceChange);
            }

            List<PriceChangeReportData> lstFilterBand = new List<PriceChangeReportData>();

            var results = (from p in lstPrice
                           group p by p.VariantSku into g
                           select new { VariantSku = g.Key }).ToList();

            foreach (var item in allDates)
            {
                var dateItem = lstPrice.Where(w => DateTimeExtension.GetDateFromDateCount(w.DateCounter).ToShortDateString() == item.ToShortDateString()).ToList();
                if (dateItem != null && dateItem.Count() > 0)
                {
                    foreach (var itemvariance in results)
                    {
                        var datebyItems = dateItem.Where(w => w.VariantSku == itemvariance.VariantSku).OrderBy(o => o.DateCreated).ToList();
                        if (datebyItems.Count == 1)
                        {
                            lstFilterBand.Add(new PriceChangeReportData
                            {
                                PriceBandFrom = datebyItems[0].PriceBand,
                                PriceBandTo = datebyItems[0].PriceBand,
                                PriceFrom = Convert.ToString(datebyItems[0].Price),
                                PriceTo = Convert.ToString(datebyItems[0].Price),
                                ProviderName = datebyItems[0].ProviderName,
                                DateCreated = DateTimeExtension.GetDateFromDateCount(datebyItems[0].DateCounter),
                                RoomType = datebyItems[0].RoomType
                            });
                        }
                        for (int j = 0; j < datebyItems.Count - 1; j++)
                        {
                            var minItem = datebyItems[j];
                            var maxItem = new PriceChangeReportData();
                            if (datebyItems.Count() == 1)
                                maxItem = minItem;
                            else
                                maxItem = datebyItems[j + 1];

                            lstFilterBand.Add(new PriceChangeReportData
                            {
                                PriceBandFrom = minItem.PriceBand,
                                PriceBandTo = maxItem.PriceBand,
                                PriceFrom = Convert.ToString(minItem.Price),
                                PriceTo = Convert.ToString(maxItem.Price),
                                ProviderName = maxItem.ProviderName,
                                DateCreated = DateTimeExtension.GetDateFromDateCount(maxItem.DateCounter),
                                RoomType = maxItem.RoomType
                            });
                        }
                    }
                }
            }


            var lstItems = lstFilterBand.Where(p => ((ddlPriceBandFrom.SelectedValue == "0" && p.PriceBandFrom != null) ||
                  p.PriceBandFrom == ddlPriceBandFrom.SelectedItem.Text) &&
                  ((ddlPriceBandTo.SelectedValue == "0" && p.PriceBandTo != null) ||
                  p.PriceBandTo == ddlPriceBandTo.SelectedItem.Text)).ToList();

            return lstItems;
        }

        #region Private Method

        private void LoadGrid()
        {
            lstPriceBandReport = PriceBand();

            if (lstPriceBandReport != null && lstPriceBandReport.Count > 0)
                this.btnExport.Attributes.Remove("disabled");

            grdvwChangeRates.DataSource = lstPriceBandReport;
            grdvwChangeRates.DataBind();
        }

        private void LoadCountryDropDown()
        {
            if (ddlCountry.SelectedIndex == 0)
            {
                ListItem allLocation = new ListItem("All Location", "0");
                ddlLocation.Items.Insert(0, allLocation);
                ListItem allMapLocation = new ListItem("All MapLocation", "0");
                ddlMapLocation.Items.Insert(0, allMapLocation);
            }

            Database contextDb = Sitecore.Context.Database;
            var CountryItems = new ListItemCollection();
            dynamic items = contextDb.GetItem(Constants.Countries) != null ? contextDb.GetItem(Constants.Countries).GetChildren() : null;
            if (items != null)
            {
                foreach (var item in items)
                {
                    ListItem liBox = new ListItem();
                    liBox.Text = SitecoreFieldsHelper.GetValue(item, "CountryName");
                    liBox.Value = item.ID.ToString();
                    if (liBox != null)
                    {
                        CountryItems.Add(liBox);
                    }
                }

                BindDropdown(ddlCountry, CountryItems, "Text", "Value");

                ListItem allCountry = new ListItem("All Country", "0");
                ddlCountry.Items.Insert(0, allCountry);

            }
        }

        private void LoadLocationDropDown()
        {
            Database contextDb = Sitecore.Context.Database;
            var LocationItems = new ListItemCollection();
            dynamic items = contextDb.GetItem(Constants.Location) != null ? contextDb.GetItem(Constants.Location).GetChildren() : null;
            if (items != null)
            {
                foreach (var item in items)
                {
                    if (SitecoreFieldsHelper.GetDropLinkFieldValue(item, "Country", "CountryName") == ddlCountry.SelectedItem.Text)
                    {
                        if (SitecoreFieldsHelper.CheckBoxChecked(item, "IsActive"))
                        {
                            ListItem liBox = new ListItem();
                            liBox.Text = SitecoreFieldsHelper.GetValue(item, "Name");
                            liBox.Value = item.ID.ToString();
                            if (liBox != null)
                            {
                                LocationItems.Add(liBox);
                            }
                        }
                    }
                }
                BindDropdown(ddlLocation, LocationItems, "Text", "Value");

                ListItem allLocation = new ListItem("All Location", "0");
                ddlLocation.Items.Insert(0, allLocation);

            }
        }

        private void LoadMapLocationDropDown()
        {
            Database contextDb = Sitecore.Context.Database;
            var maplocationItems = new ListItemCollection();
            dynamic items = contextDb.GetItem(Constants.MapLocation) != null ? contextDb.GetItem(Constants.MapLocation).GetChildren() : null;
            if (items != null)
            {
                foreach (var item in items)
                {
                    if (SitecoreFieldsHelper.GetDropLinkFieldValue(item, "Location", "Name") == ddlLocation.SelectedItem.Text)
                    {
                        if (SitecoreFieldsHelper.CheckBoxChecked(item, "IsActive"))
                        {
                            ListItem liBox = new ListItem();
                            liBox.Text = SitecoreFieldsHelper.GetValue(item, "Name");
                            liBox.Value = item.ID.ToString();
                            if (liBox != null)
                            {
                                maplocationItems.Add(liBox);
                            }
                        }
                    }
                }
                BindDropdown(ddlMapLocation, maplocationItems, "Text", "Value");

                ListItem allMapLocation = new ListItem("All MapLocation", "0");
                ddlMapLocation.Items.Insert(0, allMapLocation);

            }
        }

        private void LoadThemeDropDown()
        {
            Database contextDb = Sitecore.Context.Database;
            var themeItems = new ListItemCollection();
            var items = contextDb.GetItem(Constants.ThemesPath);
            if (items != null)
            {
                var themeItem = items.Axes.GetDescendants().Where(a => a.TemplateName.Equals("Theme"));
                foreach (var item in themeItem)
                {
                    ListItem liBox = new ListItem();
                    liBox.Text = item.Name;
                    liBox.Value = item.ID.ToString();
                    if (liBox != null)
                    {
                        themeItems.Add(liBox);
                    }
                }
                BindDropdown(ddlTheme, themeItems, "Text", "Value");

                ListItem allThemes = new ListItem("All Themes", "0");
                ddlTheme.Items.Insert(0, allThemes);

            }
        }

        private void LoadStarsDropDown()
        {
            Database contextDb = Sitecore.Context.Database;
            var starItems = new ListItemCollection();
            var items = contextDb.GetItem(Constants.StarRankingsPath);
            if (items != null)
            {
                var starItem = items.Axes.GetDescendants().Where(a => a.TemplateName.Equals("Ranking"));
                foreach (var item in starItem)
                {
                    ListItem liBox = new ListItem();
                    liBox.Text = item.Name;
                    liBox.Value = item.ID.ToString();
                    if (liBox != null)
                    {
                        starItems.Add(liBox);
                    }
                }
                BindDropdown(ddlStarsRating, starItems, "Text", "Value");

                ListItem allStars = new ListItem("All Stars", "0");
                ddlStarsRating.Items.Insert(0, allStars);
            }
        }

        private void LoadPriceBandFromDropDown()
        {
            Database contextDb = Sitecore.Context.Database;
            var priceBand = new ListItemCollection();
            var items = contextDb.GetItem(Constants.Pricaband);
            if (items != null)
            {
                var itemsChildren = items.Axes.GetDescendants().Where(a => a.TemplateName.Equals("PriceBand"));
                foreach (var item in itemsChildren)
                {
                    ListItem liBox = new ListItem();
                    liBox.Text = item.Name + " (" + item.Fields["Rate"].Value + ")";
                    liBox.Value = item.ID.ToString();
                    if (liBox != null)
                    {
                        priceBand.Add(liBox);
                    }
                }
                BindDropdown(ddlPriceBandFrom, priceBand, "Text", "Value");

                ListItem allPriceBand = new ListItem("All PriceBand", "0");
                ddlPriceBandFrom.Items.Insert(0, allPriceBand);

                BindDropdown(ddlPriceBandTo, priceBand, "Text", "Value");
                ddlPriceBandTo.Items.Insert(0, allPriceBand);
            }

        }

        //<summary>
        //Bind Drop down
        //</summary>
        //<param name="ddlControl"></param>
        //<param name="dataSource"></param>
        //<param name="text"></param>
        //<param name="value"></param>
        private void BindDropdown(DropDownList ddlControl, object dataSource, string text, string value)
        {
            ddlControl.DataSource = dataSource;
            ddlControl.DataTextField = text;
            ddlControl.DataValueField = value;
            ddlControl.DataBind();
        }


        #endregion

        protected void RetrieveData_Click(object sender, EventArgs e)
        {
            LoadGrid();
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadLocationDropDown();
        }

        protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadMapLocationDropDown();
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Clear();
                Response.Buffer = true;
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Charset = string.Empty;
                string FileName = CreateFileName();

                using (StringWriter strwritter = new StringWriter())
                {
                    HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);

                    GridView tempGrid = grdvwChangeRates;
                    tempGrid.AllowPaging = false;
                    tempGrid.CssClass = "table table-striped table-bordered";
                    tempGrid.AutoGenerateColumns = false;

                    tempGrid.DataSource = lstPriceBandReport;
                    tempGrid.DataBind();

                    Page.Form.Controls.Clear();

                    Page.Form.Controls.Add(tempGrid);

                    tempGrid.GridLines = GridLines.Both;
                    tempGrid.HeaderStyle.Font.Bold = true;
                    Page.Form.RenderControl(htmltextwrtter);
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName + ".xls");
                    Response.Write(strwritter.ToString());
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.AdminPortal, ex, this);
                throw;
            }

        }

        private string CreateFileName()
        {
            return "ChangeRatesReports" + DateTime.Now.ToString();
        }

        protected void grdvwChangeRates_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grdvwChangeRates.PageIndex = e.NewPageIndex;
            LoadGrid();
        }

        private DateTime InputDateFormatting(string date)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(date))
                {
                    return default(DateTime);
                }
                else
                {
                    DateTime tmpDateTime;
                    try
                    {
                        tmpDateTime = DateTime.Parse(date);
                        return tmpDateTime;
                    }
                    catch
                    {
                        string[] words = date.Split('/');
                        string newstring = words[1] + "/" + words[0] + "/" + words[2];
                        DateTime dt = Convert.ToDateTime(newstring);
                        return dt;
                    }
                }
            }
            catch
            {
                return default(DateTime);
            }

        }

    }
}

