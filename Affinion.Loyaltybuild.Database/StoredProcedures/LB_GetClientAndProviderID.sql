CREATE PROCEDURE [dbo].[LB_GetClientAndProviderID]
@OrderId INT=0,
@OrderLineId INT=0
AS
BEGIN

 IF @OrderId <> 0 AND @OrderId >0
	BEGIN  
		SET @OrderLineId=(SELECT TOP 1 OrderLineId FROM uCommerce_OrderLine WHERE OrderId=@OrderId order by OrderLineId desc)
	END
    SELECT 
        CAT.ClientId,
        CA.[Guid] [ProviderId],
        C.ISOCode [Currency]                                                    
	FROM [dbo].uCommerce_OrderLine OL 
					INNER JOIN [dbo].uCommerce_PurchaseOrder PO ON PO.OrderID =OL.OrderId
					INNER JOIN [dbo].LB_RoomReservation RR on RR.OrderLineId = OL.OrderLineId 
					INNER JOIN [dbo].[uCommerce_Product] P ON OL.Sku = P.Sku 
					INNER JOIN [dbo].[uCommerce_CategoryProductRelation] CPR ON P.ProductId = CPR.ProductId
					INNER JOIN [dbo].[uCommerce_Category] CA ON CA.CategoryId = CPR.CategoryId 
					LEFT JOIN  [dbo].LB_CustomerAttribute CAT ON PO.CustomerId =CAT.CustomerId
					INNER JOIN [dbo].uCommerce_Currency C ON PO.CurrencyId=C.CurrencyId
	WHERE 
		OL.OrderLineId=@OrderLineId                                                                
END  

