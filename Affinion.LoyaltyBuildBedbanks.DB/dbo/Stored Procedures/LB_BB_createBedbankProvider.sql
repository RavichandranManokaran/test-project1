CREATE PROCEDURE [dbo].[LB_BB_createBedbankProvider]
       
       -- Input parameters 
       @pi_propertyid int = 1,
       @PI_AVAILABILITYSOURCEID int = 1,
       @pi_providerName varchar(200),
       @pi_addressline1 varchar(250),
       @pi_addressline2 varchar(250),
       @pi_addressline3 varchar(250),
       @pi_addressline4 varchar(250),
       @pi_addressline5 varchar(250),
       @pi_phone varchar(50),
       @pi_fax varchar(50),
       @pi_lat varchar(50),
       @pi_long varchar(50),
       @pi_dbname varchar(50),
       --Return parameters
       @po_lbproviderid varchar(250) OUTPUT,
       @po_returnstatus INT OUTPUT,
       @po_returnmsg VARCHAR(200) OUTPUT

       
AS
set nocount on;
BEGIN

       --Assume transaction will fail, set the return variable to -1.
       SET @po_returnstatus = -1;
       SET @po_returnmsg = null;

       
       DECLARE @orig geography = geography::Point(CAST(@pi_lat as float), CAST(@pi_long as float), 4326);
       DECLARE @ID int
       DECLARE @table as table(ProductId int, SitecoreItemId varchar(max), ProviderName varchar(max), latitude varchar(max), longitude varchar(max))
       BEGIN TRY


	   DECLARE @productTName varchar(max)
	   set @productTName= @pi_dbname+'.[dbo].uCommerce_Product'
	   declare @productAlias as table(
	   [ProductId] int,
	   [ParentProductId] int,
	   [Sku] nvarchar(30),
	   [VariantSku] nvarchar(30),
	   [Name] nvarchar(512),
	   [DisplayOnSite] bit,
	   [ThumbnailImageMediaId] nvarchar(100),
	   [PrimaryImageMediaId] nvarchar(100),
	   [Weight] decimal(18, 4),
	   [ProductDefinitionId] int,
	   [AllowOrdering] bit,
	   [ModifiedBy] nvarchar(50),
	   [ModifiedOn] datetime,
	   [CreatedOn] datetime,
	   [CreatedBy] nvarchar(50),
	   [Rating] float,
	   [Guid] uniqueidentifier)

	INSERT INTO @productAlias EXEC('SELECT * from '+@productTName)

	DECLARE @proProTName varchar(max)
	set @proProTName = @pi_dbname+'.[dbo].uCommerce_ProductProperty'
	DECLARE @propertyAlias as table(
	[ProductPropertyId] int,
	[Value] nvarchar(max) ,
	[ProductDefinitionFieldId] int,
	[ProductId] int
	)

	INSERT INTO @propertyAlias EXEC('SELECT * From '+@proProTName)
       IF NOT EXISTS (SELECT TOP 1 * FROM LB_BB_provider WHERE availabilityProviderID = @PI_AVAILABILITYSOURCEID AND availabilityProviderRecordID = @pi_propertyid)                             
       BEGIN
       
  INSERT INTO @table 
  select latlong.ProductId, latlong.sitecoreItemId, pro.Name, latlong.Latitude, latlong.Longitude
  from
  (
  select ProductId,  [1055] sitecoreItemId, [1044] providerName, [4062] Latitude, [4063] Longitude
  from
  (
  select ProductId, [ProductDefinitionFieldId], Value from @propertyAlias
  )src
  pivot
  (
  max([Value])
  for [ProductDefinitionFieldId] IN ([4062], [1055], [4063], [1044])
  )piv) latlong
  inner join @productAlias pro on pro.ProductId=latlong.ProductId where pro.VariantSku is null 
  --select * from @table
SET @po_lbproviderid = (SELECT TOP 1 LBPR.[SitecoreItemId]
                 from @table LBPR
                 where LBPR.Latitude is not null and LBPR.Longitude is not null
				 and LBPR.latitude!='' and LBPR.longitude!='' 
                 --and LBPR.Latitude like '[0-9]%' and LBPR.Longitude like '[0-9]%'
                 and isnumeric(LBPR.Latitude) = 1 and isnumeric(LBPR.Longitude) = 1
                 -- LAT LONG CO ORDINATES MUST BE WITH 250 METRES OF EACH OTHER TO CONSIDER A MATCH
                 and @orig.STDistance(geography::Point(isnull(cast(LBPR.Latitude as float),0), isnull(cast(LBPR.Longitude as float),0), 4326)) < 250
                 -- SOUNDEX VALUE ON THE NAME OF THREE AND ABOVE
                 and DIFFERENCE ( LBPR.ProviderName , @pi_providerName ) > 3)
                 
				--set @po_lbproviderid = ('{'+UPPER(@po_lbproviderid)+'}')
				                                  
				if @po_lbproviderid IS NOT NULL
				begin
					INSERT INTO LB_BB_provider
					(
					availabilityProviderID
					,providerName
					,availabilityProviderRecordID
					,LBPROVIDERID
					,addressline1
					,addressline2
					,addressline3
					,addressline4
					,addressline5
					,phone
					,Fax
					,lat
					,long
					,isSuppressed
					,isMultipleMatch)
					VALUES
					(
					1
					, @pi_providerName
					, @pi_propertyid
					-- NOW PULL OUT THE DEFAULT MARGIN FROM THE CONFIG.
					--, (SELECT Value from dbo._config where ID = 1)
					,@po_lbproviderid   
					,@pi_addressline1
					,@pi_addressline2
				    ,@pi_addressline3
					,@pi_addressline4
					,@pi_addressline5
					,@pi_phone
					,@pi_fax
					,@pi_lat
					,@pi_long
					,0
					,1)
				end
				else
				begin
					INSERT INTO LB_BB_provider
					(
						availabilityProviderID
						,providerName
						,availabilityProviderRecordID
						,LBPROVIDERID
						,addressline1
						,addressline2
						,addressline3
						,addressline4
						,addressline5
						,phone
						,Fax
						,lat
						,long
						,isSuppressed
						,isMultipleMatch)
					 VALUES
					(
						1
						, @pi_providerName
						, @pi_propertyid
						-- NOW PULL OUT THE DEFAULT MARGIN FROM THE CONFIG.
						--, (SELECT Value from dbo._config where ID = 1)
						,0   
						,@pi_addressline1
						,@pi_addressline2
						,@pi_addressline3
						,@pi_addressline4
						,@pi_addressline5
						,@pi_phone
						,@pi_fax
						,@pi_lat
						,@pi_long
						,0
						,0)
					end
       SET @ID = SCOPE_IDENTITY() 
       
       Insert into LB_BB_providerMargin
       Values(@ID,1111,99999,(SELECT Value from dbo.LB_BB_config where ID = 1),1,77,GETDATE())
       END
       ELSE
       BEGIN
       Select @po_lbproviderid = isnull(lbProviderID,0) from LB_BB_provider
    WHERE availabilityProviderID = @PI_AVAILABILITYSOURCEID AND availabilityProviderRecordID = @pi_propertyid
       END
                    
    END TRY   
       BEGIN CATCH     
        PRINT 'here'
       INSERT INTO LB_BB_provider
       (
       availabilityProviderID
       ,providerName
       ,availabilityProviderRecordID
       ,LBPROVIDERID
       ,addressline1
       ,addressline2
       ,addressline3
       ,addressline4
       ,addressline5
       ,phone
       ,Fax
       ,lat
       ,long
       ,isSuppressed
       ,isMultipleMatch)
       VALUES
       (
       1
       , @pi_providerName
       , @pi_propertyid
       -- NOW PULL OUT THE DEFAULT MARGIN FROM THE CONFIG.
       --, (SELECT Value from dbo._config where ID = 1)
       ,0  
       ,@pi_addressline1
       ,@pi_addressline2
       ,@pi_addressline3
       ,@pi_addressline4
       ,@pi_addressline5
       ,@pi_phone
       ,@pi_fax
       ,@pi_lat
       ,@pi_long
       ,0
	   ,0)
       
       SET @ID = SCOPE_IDENTITY() 
       
       Insert into LB_BB_providerMargin
       Values(@ID,1111,99999,(SELECT Value from dbo.LB_BB_config where ID = 1),1,77,GETDATE())
       
              SET @po_returnstatus = error_number();
              SET @po_returnmsg = error_message();
              RETURN;
       END CATCH
       
       SET @po_returnstatus = 0;
       SET @po_returnmsg = 'Completed successfully.';  
END