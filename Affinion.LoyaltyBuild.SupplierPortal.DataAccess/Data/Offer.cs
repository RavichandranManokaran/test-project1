﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.SupplierPortal.DataAccess.Data
{
    /// <summary>
    /// Contains offer info details
    /// </summary>
    public class Offer
    {
        /// <summary>
        /// offer id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// offer name
        /// </summary>
        public string Name { get; set; }
    }
}
