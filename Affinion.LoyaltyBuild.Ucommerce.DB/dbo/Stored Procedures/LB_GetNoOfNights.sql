﻿CREATE PROCEDURE [dbo].[LB_GetNoOfNights]
	(@OrderLineId INT)
AS
SELECT distinct DATEDIFF(day,RR.CheckInDate,RR.CheckOutDate ) AS DiffDate   
FROM LB_RoomReservation RR
WHERE RR.OrderLineId= @OrderLineId


