﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/
namespace Affinion.LoyaltyBuild.Api.Booking.Data.Payment
{
    /// <summary>
    /// Entity containing informaiton about DueHistoryItem
    /// </summary> 
    public class DueHistoryItem
    {
        /// <summary>
        /// Orderlinedueid
        /// </summary>
        public int OrderLineDueId { get; set; }

        /// <summary>
        /// orderlineid
        /// </summary>
        public int OrderLineId { get; set; }

        /// <summary>
        /// Currency Types
        /// </summary>
        public int CurrencyTypeId { get; set; }

        /// <summary>
        ///  Due Amount
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Due payment method
        /// </summary>
        public string PaymentMethod { get; set; }

        /// <summary>
        /// Payment reason
        /// </summary>      
        public string Reason { get; set; }

        /// <summary>
        /// Can be deleted flag
        /// </summary>
        public bool CanBeDeleted { get; set; }

        /// <summary>
        /// Createdby
        /// </summary>
        public string CreatedBy { get; set; }

        /// <summary>
        /// Created date
        /// </summary>
        public DateTime CreatedDate { get; set; }

    }
}
