CREATE PROCEDURE [dbo].[LB_AddOrderLineSubrates] 
(@SubrateTVP [TVP_uCommerce_Subrates] READONLY,
@orderId int)
AS
BEGIN
 INSERT INTO [dbo].[LB_OrderLineSubrates] (OrderLineID, SubrateItemCode, Amount, Discount, IsIncludedInUnitPrice)
 SELECT [OrderLineID],[SubrateItemCode],[Amount],[Discount],[IsIncludedInUnitPrice]
 FROM @SubrateTVP
 WHERE OrderLineID NOT IN (SELECT OrderLineID FROM LB_OrderLineSubrates)
END