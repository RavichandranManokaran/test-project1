﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalApplicablePromotions.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortalApplicablePromotions" %>


<form id="form1" runat="server">
    <div id="availabilitysection" class="row no-margin" runat="server" visible="false">
        <div class="col-xs-12">
            <div class="col-xs-12 no-padding">Price:
                <asp:Label ID="promoPriceLabel" runat="server" Text="" /></div>
            <div class="col-xs-3 no-padding martop10">Quantity:</div>
            <div class="col-xs-9 martop10">
                <asp:DropDownList ID="availabilityDropDown" runat="server" CssClass="form-control availabilityDropDown"></asp:DropDownList></div>

        </div>
        <div class="col-xs-12 martop10">
            <div class="col-xs-4 no-padding-right">
                <button type="button" id="promoBtnCancel" data-dismiss="modal" class="btn-co1 pop_paddng">
                    Cancel
                </button>
            </div>
            <div class="col-xs-4" id="divAddToBasketPromo" runat="server">
                <button type="button" id="PromoBtnAddToCart" data-dismiss="modal" class="btn-co1 pop_paddng" onclick="CS.BasketService.AddToBasket(null)">
                    Add to basket
                </button>
            </div>
            <div class="col-xs-4 no-padding-left">
                <button type="button" id="PromoBtnCheckout" data-dismiss="modal" class="btn-co1 pop_paddng" onclick="CS.BasketService.AddToBasket('checkout')">Check Out</button>
            </div>
        </div>
    </div>
    <div id="ZeroavailabilitySection" runat="server" visible="false">
        <span>This promotion is not applicable for the selected criteria</span>
    </div>
</form>




