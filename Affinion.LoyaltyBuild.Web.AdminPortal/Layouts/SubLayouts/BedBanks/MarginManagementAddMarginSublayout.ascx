﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MarginManagementAddMarginSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.BedBanks.MarginManagementAddMarginSublayout" %>
<style type="text/css">
.txtBox
{
width: 112% !important;
}
</style>	
<div class="col-sm-10 no-padding bedbank-rightpanel">
						<div class="row no-margin ">
							<div class="col-sm-12">
								<h3><sc:Text id="scTitle" runat="server" Field="Title Text"/></h3>
							</div>   
						</div>
						<div class="row no-margin">
                            <span class="label label-danger"><asp:Literal ID="litExists" runat="server" Text="Date Overlap"></asp:Literal></span>
							<div class="col-sm-12 marB15">
								<div class="col-md-4 no-padding" id="divDateFrom" runat="server">
									<div class="col-md-5 no-padding"><sc:Text id="scDateFrom" runat="server" Field="Date From Text"/></div>
									<div class="col-md-7 no-padding">
										<div class="control-group form-group">
											<div class="controls input-append date form_date input-group date" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
												<asp:TextBox ID="txtDateFrom" runat="server" CssClass="form-control txtBox" />
												<span class="add-on input-group-addon"><i class="icon-th glyphicon glyphicon-calendar"></i></span>
                                            </div>  
											<input type="hidden" id="dtp_input2" value="" /><br/>
										</div>
                                       <asp:RequiredFieldValidator ID="rfvDateFrom" runat="server" ValidationGroup="confirm" ControlToValidate="txtDateFrom" ErrorMessage="*Required Field" ForeColor="Red"></asp:RequiredFieldValidator>
									</div>
								</div>
								<div class="col-md-8 no-padding"></div>
							</div>
							<div class="col-sm-12 marB15">
								<div class="col-md-4 no-padding" id="divDateTo" runat="server">
									<div class="col-md-5 no-padding"><sc:Text id="scDateTo" runat="server" Field="Date To Text"/></div>
									<div class="col-md-7 no-padding">
										<div class="control-group form-group">
											<div class="controls input-append date form_date input-group date" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
												<asp:TextBox ID="txtDateTo" runat="server" CssClass="form-control txtBox" />
                                                <span class="add-on input-group-addon"><i class="icon-th glyphicon glyphicon-calendar"></i></span>
											</div>
											<input type="hidden" id="dtp_input2" value="" /><br/>
										</div>
                                         <asp:RequiredFieldValidator ID="rfvDateTo" runat="server" ValidationGroup="confirm" ControlToValidate="txtDateTo" ErrorMessage="*Required Field" ForeColor="Red"></asp:RequiredFieldValidator>
									</div>
								</div>
								<div class="col-md-8 no-padding"></div>
							</div>
							<div class="col-sm-12 marB15">
								<div class="col-md-4 no-padding">
									<div class="col-md-5 no-padding"><sc:Text id="scMargin" runat="server" Field="Margin Percentage Text"/></div>
									<div class="col-md-7 no-padding">
										<asp:TextBox ID="txtMargin" runat="server"/>
                                        <asp:RequiredFieldValidator ID="rfvMargin" runat="server" ValidationGroup="confirm" ControlToValidate="txtMargin" ErrorMessage="*Required Field" ForeColor="Red"></asp:RequiredFieldValidator>
									</div>
								</div>
								<div class="col-md-8 no-padding"></div>
							</div>
							<div class="col-sm-12 marB15">
								<div class="col-md-4 no-padding">
									<div class="col-md-5 no-padding"><asp:literal ID="litActive" runat="server"></asp:literal></div>
									<div class="col-md-7 no-padding">
										<asp:CheckBox ID="chkActive" runat="server" />
									</div>
								</div>
								<div class="col-md-8 no-padding"></div>
							</div>
							<div class="col-sm-12 marB15"> 
								<button type="submit" runat="server" id="btnConfirm" onserverclick="btnConfirm_ServerClick" ValidationGroup="confirm" class="btn btn-default font-size-12 pull-right marL"><sc:Text id="scConfirm" runat="server" Field="Confirm Button Text"/></button> 
								<button type="button" runat="server" id="btnCancel" onserverclick="btnCancel_ServerClick" class="btn btn-default font-size-12 pull-right marL"><sc:Text id="scCancel" runat="server" Field="Cancel Button Text"/></button> 
							</div>
						</div>
					</div>



		<script type="text/javascript">
		    $(function () {
		        /*--FOR DATE----*/
		        var date = new Date();
		        var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
		    $('.form_date').datetimepicker({
		        weekStart: 1,
		        todayBtn:  1,
		        autoclose: 1,
		        todayHighlight: 1,
		        startView: 2,
		        minView: 2,
		        forceParse: 0,
		        format: 'yyyy-mm-dd',
		        startDate: today
		    });
		    }); 
		</script>
   <script>
       $(window).on('load', function () {
           target = $('.bedbank-rightpanel').height();
           $('.bedbank-leftpanel').height(target);
       });
   </script>