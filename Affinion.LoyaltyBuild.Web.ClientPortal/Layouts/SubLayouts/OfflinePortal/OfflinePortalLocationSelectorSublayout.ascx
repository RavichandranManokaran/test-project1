﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalLocationSelectorSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalLocationSelectorSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="col-xs-12 no-padding">
    <div class="detail-info">
        <div class="detail-info-header">
            <div class="row">
                <div class="col-xs-9">
                    <h2 class="details-header no-margin">Select Available Locations
                    </h2>
                </div>
                <div class="col-xs-3">
                    <span class="glyphicon glyphicon-map-marker"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <p>
                    Accommodation providers are subject to availability based on the dates you select. If you can't find what you are looking for, select another date and search again.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="select-loc" aria-label="..." role="group">
                    <asp:DropDownList ID="AvailableLocationDropDown" runat="server" CssClass="form-control"></asp:DropDownList>                   
                </div>
            </div>
        </div>
    </div>
</div>
