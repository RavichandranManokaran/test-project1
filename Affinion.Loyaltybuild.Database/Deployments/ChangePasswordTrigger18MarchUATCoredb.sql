﻿/****** Object:  Trigger [dbo].[trPasswordUpdate]    Script Date: 15-03-2016 ******/

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'PasswordHistories')
	CREATE TABLE [dbo].[PasswordHistories](
		[UserName] [nvarchar](128) NOT NULL,
		[Password] [nvarchar](128) NOT NULL,
		[PasswordSalt] [nvarchar](max) NULL,
		[ChangedDate] [datetime] NOT NULL,
		 CONSTRAINT [PK_dbo.PasswordHistories] PRIMARY KEY CLUSTERED 
		(
			[UserName] ASC,
			[Password] ASC
		)
	)
GO

/* create asp membership table if not exist*/

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'aspnet_Membership')
	CREATE TABLE [dbo].[aspnet_Membership](
		[ApplicationId] [uniqueidentifier] NOT NULL,
		[UserId] [uniqueidentifier] NOT NULL,
		[Password] [nvarchar](128) NOT NULL DEFAULT(0),
		[PasswordFormat] [int] NOT NULL,
		[PasswordSalt] [nvarchar](128) NOT NULL,
		[MobilePIN] [nvarchar](16) NULL,
		[Email] [nvarchar](256) NULL,
		[LoweredEmail] [nvarchar](256) NULL,
		[PasswordQuestion] [nvarchar](256) NULL,
		[PasswordAnswer] [nvarchar](128) NULL,
		[IsApproved] [bit] NOT NULL,
		[IsLockedOut] [bit] NOT NULL,
		[CreateDate] [datetime] NOT NULL,
		[LastLoginDate] [datetime] NOT NULL,
		[LastPasswordChangedDate] [datetime] NOT NULL,
		[LastLockoutDate] [datetime] NOT NULL,
		[FailedPasswordAttemptCount] [int] NOT NULL,
		[FailedPasswordAttemptWindowStart] [datetime] NOT NULL,
		[FailedPasswordAnswerAttemptCount] [int] NOT NULL,
		[FailedPasswordAnswerAttemptWindowStart] [datetime] NOT NULL,
		[Comment] [ntext] NULL,
		PRIMARY KEY NONCLUSTERED 
		(
			[UserId] ASC
		)
	)
GO

/* create trigger for change password*/

IF EXISTS (
    SELECT *
    FROM sys.objects
    WHERE [type] = 'TR' AND [name] = 'trPasswordUpdate'
    )
    DROP TRIGGER [dbo].[trPasswordUpdate];
GO
CREATE TRIGGER [dbo].[trPasswordUpdate]
   ON  [dbo].[aspnet_Membership]
   AFTER UPDATE
AS
    BEGIN
         
        INSERT INTO dbo.PasswordHistories 
        (
            UserName, 
            [Password], 
            PasswordSalt,
ChangedDate
        ) 
        SELECT
            (select username from  aspnet_users where userId = deleted.userID),
            deleted.[Password], 
            deleted.PasswordSalt,
GetDate()
        FROM
            deleted INNER JOIN inserted ON deleted.UserId = inserted.UserId
        WHERE
            (deleted.[Password] <> inserted.[Password]) OR (deleted.PasswordSalt <> inserted.PasswordSalt)
    END
GO

