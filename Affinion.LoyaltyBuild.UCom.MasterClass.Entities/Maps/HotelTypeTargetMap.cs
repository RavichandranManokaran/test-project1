﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Maps
{
    public class HotelTypeTargetMap : SubclassMap<HotelTypeTarget>
    {
        // Methods
        public HotelTypeTargetMap()
        {
            base.Table("LB_HotelTypeTarget");
            base.KeyColumn("HotelTypeTargetId");
            base.Map(x => x.HotelTypeTargetId).ReadOnly();
            base.Map(x => x.HotelType).Not.Nullable();
        }

    }



}