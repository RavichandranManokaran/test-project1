﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Api.Booking.Data.Basket
{
    /// <summary>
    /// Transfer booking request
    /// </summary>
    public class TransferRequest : ITransferRequest
    {
        /// <summary>
        /// OrderLine id
        /// </summary>
        public int OrderLineid { get; set; }

        /// <summary>
        /// Note to Provider
        /// </summary>
        public string NoteToProvider { get; set; }

        /// <summary>
        /// no of adults
        /// </summary>
        public int NoOdAdult { get; set; }

        /// <summary>
        /// no of children
        /// </summary>
        public int NoOfChildren { get; set; }

        /// <summary>
        /// Childern Age Info
        /// </summary>
        public string ChildrenAgeInfo { get; set; }

        /// <summary>
        /// Basket id
        /// </summary>
        public string BasketId { get; set; }

        ///<summary>
        /// Createdby userlogin
        /// </summary>
        public string CreatedBy { get; set; }

        /// <summary>
        /// Set if this is a free transfer
        /// </summary>
        public bool IsFreeTransfer { get; set; }

        /// <summary>
        /// Booking reference
        /// </summary>
        public string BookingReferenceNumber { get; set; }
    }
}
