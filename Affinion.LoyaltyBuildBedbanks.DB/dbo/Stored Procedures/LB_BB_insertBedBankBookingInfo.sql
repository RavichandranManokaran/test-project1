

/****** Object:  StoredProcedure [dbo].[LB_BB_insertBedBankBookingInfo]    Script Date: 7/19/2016 7:27:35 PM ******/
---- ====================================================
---- Description:	Tii SP to Create, Read, Update, Delete client to or from the client table.
---- Rev 0.2 05-JUN-2014 MC		Added new ClientID Filter (it uses the @pi_userID parameter)
---- Rev 0.1 08-APR-2011		Created new SP.
---- 
---- RAISERROR ('mesage text %a', severity#, state#, @pi_); 
----			%s/%u is optional;	
----			severity < 10 The error is returned to the caller if RAISERROR is run
----			severity > 11 transfers control to the associated CATCH block.
----			severity > 20 terminates the database connection.
----			state#   set to 1
----			@pi_ optional, must be used if %s/%u is used in the message text
----
---- ====================================================


CREATE PROCEDURE [dbo].[LB_BB_insertBedBankBookingInfo]
	--

	@pi_availabilityProviderID int,
	@pi_availabilityproviderRecordID int,
	@pi_bookingId int,
	@pi_roomType varchar(50),
	@pi_mealBasis varchar(150),
	@pi_externalBookingReference varchar(150),
	@pi_WholeSaleAmount money,
	@pi_propertyMarginID int,
	@pi_lbBookingReference varchar(max),
	@pi_marginAmount decimal,
	@pi_marginPercentage decimal,
	
	--Return parameters
	@po_returnstatus INT OUTPUT,
	@po_returnmsg VARCHAR(200) OUTPUT,
	@po_scopeid NUMERIC OUTPUT

AS

BEGIN

	--Assume transaction will fail, set the return variable to -1.
	SET @po_returnstatus = -1;
	SET @po_returnmsg = null;
	SET @po_scopeid = 0;
	
	BEGIN TRY
		
		INSERT INTO [dbo].[LB_BB_bedbankBooking]
           ([bookingID]
           ,[bedbankProviderID]
           ,[roomType]
           ,[mealBasis]
           ,[LBOrderLineID]
           ,[WholeSaleAmount]
           ,[propertyMarginID]
		   ,[LBBookingReference]
		   ,[MarginAmount]
		   ,[MarginPercentage]
		   ,[Location]
		   ,[Map Location])
		 VALUES
			   (@pi_bookingId
			   ,@pi_availabilityproviderRecordID
			   ,@pi_roomType
			   ,@pi_mealBasis
			   ,@pi_externalBookingReference
			   ,@pi_WholeSaleAmount
			   ,@pi_propertyMarginID
			   ,@pi_lbBookingReference
			   ,@pi_marginAmount
			   ,@pi_marginPercentage
			   ,(select addressline3 from LB_BB_provider where availabilityProviderRecordID= @pi_availabilityproviderRecordID) 
			   ,(select addressline4 from LB_BB_provider where availabilityProviderRecordID=@pi_availabilityproviderRecordID)
			   )
		


    END TRY
	
	BEGIN CATCH
		SET @po_returnstatus = error_number();
		SET @po_returnmsg = error_message();
		RETURN;
	END CATCH
	
	SET @po_returnstatus = 0;
	SET @po_returnmsg = 'Completed successfully.';
	
END








GO

