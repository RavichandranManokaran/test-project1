﻿#region Using Directives
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.SupplierSetup.PollingManager;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Security;
using Sitecore.Security.Accounts;
using Sitecore.SecurityModel;
using System;
using System.Collections.ObjectModel;
using System.Linq;
#endregion

namespace Affinion.LoyaltyBuild.Web.Test.Layouts.SubLayouts.ProviderSetup.PollingManager
{
    public partial class AddPollSubLayout : System.Web.UI.UserControl
    {
        #region Constant Variables
        private const string WebDB = "web";
        private const string MasterDB = "master";
        private const string AnswerTemplatePath = "/sitecore/templates/User Defined/Polling/Answer";
        private const string PollTemplatePath = "/sitecore/templates/User Defined/Polling/Polls";
        private const string AnswerItemPath = "fast:/sitecore/content/Home/Answers/*";
        private const string totPollCountPerPoll = "TotalPollCountForPoll";
        private const string PollManagerItemPath = "/sitecore/content/Home/PollingManagerDB";
        private const string TotalPollCountAnswerItemField = "TotalPollCount";
        private const string PollIDQueryString = "PollID";
        private const string VotingRadioButton = "voteRadioButton";
        private const string PollingManagerItemId = "{03B59032-00F1-4131-A529-4C5A714C7124}";
        private const string JsonItemFiledName = "JsonFile";
        private const string JsonItem = "JsonFile";
        #endregion
        
        //create connection with the context database
        private static Database db = Factory.GetDatabase(WebDB);
        private static Database masterDB = Factory.GetDatabase(MasterDB);
        private string pollID = null;
        string jsonString = null;
        string selectedRadioButton = null;
        string user = null;
        JsonCreator json = new JsonCreator();

        #region Page_Load region
        /// <summary>
        /// Load existings poll questions and answers
        /// </summary>
        private void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Call loadAnswer function
                LoadAnswers();
                pollID = Request.QueryString[PollIDQueryString];
                if (this.IsPostBack)
                {
                    selectedRadioButton = Request["voteRadioButton"];
                    user = TrackUser(pollID);
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.SupplierSetup, ex, this);
            }
            //Handled a 
        }
        #endregion

        #region LoadAnswer Region
        /// <summary>
        /// Function that load answers that are relavant to a poll question
        /// </summary>
        private void LoadAnswers()
        {
            //get the answers related to an item
            var answerList = db.SelectItems(AnswerItemPath);
            //Bind Answers to the repeater.
            answerGenerator.DataSource = answerList;
            //make an event for answerGenerator repeater
            answerGenerator.DataBind();
        }
        #endregion

        #region AddPollCount region
        /// <summary>
        /// This function will store the total poll count of the users into sitecore database
        /// </summary>
        /// <param name="answerID">Item ID of the answer</param>
        private void AddPollCount(string answerID)
        {
            using (new SecurityDisabler())
            {
                //get the item
                Item answerItem = masterDB.Items[answerID];
                //get the template of that perticular item

                //edit the template fields
                answerItem.Editing.BeginEdit();
                var previousTotVal = answerItem.Fields[TotalPollCountAnswerItemField].Value;
                var newTotVal = Convert.ToInt32(previousTotVal) + 1;
                answerItem.Fields[TotalPollCountAnswerItemField].Value = newTotVal.ToString();

                answerItem.Editing.EndEdit();

                //call calculateAvgPoll method
                double avg = CalculateAvgPoll(newTotVal, pollID); // POLLID should passed
            }
        }
        #endregion

        #region CalculateAvgPoll region
        /// <summary>
        /// Return the average amount of polls.
        /// </summary>
        /// <param name="pollCount">Get the count of the polls</param>
        /// <returns>Avarage points for each answer</returns>
        /// <param name="pollID">Poll ID</param>
        /// <returns></returns>
        private double CalculateAvgPoll(int pollCount, string pollID)
        {
            //get answers as list related to one poll
            var answerList = db.SelectItems("fast:/sitecore/content/Home/Answers/*[@PollID = '{" + pollID + "}']").ToList();

            //get the number of answers related to one poll
            double numOfAnswers = answerList.Count;
            double totPolCount = Convert.ToDouble(db.Items[pollID].Fields[totPollCountPerPoll].Value);

            foreach (var item in answerList)
            {
                //increment the pollcount by one according to the answer given by the user
                int pollCountofItem = Convert.ToInt32(item.Fields[TotalPollCountAnswerItemField].Value);
                totPolCount += pollCountofItem;
            }

            using (new SecurityDisabler())
            {
                Item currentPollItem = masterDB.Items[pollID];
                currentPollItem.Editing.BeginEdit();
                currentPollItem.Fields[totPollCountPerPoll].Value = totPolCount.ToString();
                currentPollItem.Editing.EndEdit();
            }
            //Calculate the avarage of the each answer of the poll
            double avgPollCount = (pollCount / totPolCount) * 100;
            return avgPollCount;
        }
        #endregion

        #region TrackUser_region

        /// <summary>
        /// Get the logged in user profile or mac address of the user
        /// </summary>
        /// <returns>mac address or name of the profile</returns>
        private string TrackUser(string pollId)
        {
            UserProfile curUserProfile = User.Current.Profile;
            jsonString = db.Items[PollManagerItemPath].Fields[JsonItem].Value;

            Collection<UserDetails> detailList = new Collection<UserDetails>();

            //If user has already voted
            if (!string.IsNullOrEmpty(jsonString))
            {
                detailList = JsonCreator.ReadJsonObject(jsonString);
                foreach (var item in detailList)
                {
                    if (item.itemID.Equals(pollId) && item.userInfo.Equals(curUserProfile.FullName))
                    {
                        pollSubmitButton.Enabled = false;
                        break;
                    }
                }
                return string.Empty;
            }
            else
            {
                return curUserProfile.FullName;
            }

        }

        #endregion

        #region WriteJsonToItemField region
        /// <summary>
        /// Write Json data to the sitecore item field
        /// </summary>
        private void WriteJsonToItemField()
        {
            using (new SecurityDisabler())
            {
                //get the item
                Item pollManagerItem = masterDB.Items[PollingManagerItemId];

                //edit the template fields
                pollManagerItem.Editing.BeginEdit();

                pollManagerItem.Fields[JsonItemFiledName].Value = jsonString;

                pollManagerItem.Editing.EndEdit();
            }
        }
        #endregion

        #region SubmitButton_Click region
        /// <summary>
        /// Submit button Onclick event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SubmitButton_Click(object sender, EventArgs e)
        {
            //call GenerateJsonObject method 
            if (!string.IsNullOrEmpty(user))
            {
                string previousJsonString = db.Items[PollManagerItemPath].Fields[JsonItem].Value;
                jsonString = json.GenerateJsonObject(previousJsonString, pollID, user);
                //increase the poll count by one
                AddPollCount(selectedRadioButton);
                //write generated Json object to the item field
                WriteJsonToItemField();
            }
            else
            {
                //give an appriate message.
            }
        }
        #endregion
    }
}