﻿Create procedure [dbo].[LB_GetConfirmationDetails] 
(
@orderid int
)
AS
Begin
select OP.[value] AS GuestName, RN.Note AS SpecialRequest,OP.OrderLineId as OrderLineId
from uCommerce_OrderProperty OP
left join LB_ReservationNotes RN ON OP.OrderLineId=RN.OrderLineId
where [key]='_GuestName' and RN.NoteTypeId=3 and OP.OrderId=@orderid


END