﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           OfflinePaymentPage.aspx.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal
/// Description:           Loading Realex payment gateway
/// </summary>

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Affinion.Loyaltybuild.BusinessLogic.Helper;
using Affinion.LoyaltyBuild.Common.Exceptions;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Subrate;
using Sitecore.Data.Items;
using UCommerce;
using UCommerce.Api;
using UCommerce.EntitiesV2;
using UCommerce.Runtime;
using UCommerce.Transactions.Payments;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Order;
using AffinionConstants = Affinion.LoyaltyBuild.Common.Constants;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Pages
{
    public partial class OfflinePaymentPage : System.Web.UI.Page
    {
        #region Properties
        
        /// <summary>
        /// OrderId for which payment has to be made
        /// </summary>
        private string OrderId { get; set; }

        /// <summary>
        /// Amount to be paid for the corresponding Order Id
        /// </summary>
        private decimal AmountToPay { get; set; }

        #endregion

        #region Protected methods

        /// <summary>
        /// The Page Load event
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Event Arguments</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Get the payment method from uCommerce Database with name "RealexOffline"
                PaymentMethod paymentMethod = PaymentMethod.SingleOrDefault(x => x.Name == AffinionConstants.OfflinePortalPaymentType);

                if (paymentMethod == null)
                {
                    throw new AffinionException("Defined Payment Method Service cannot be found");
                }

                ///Get the current basket
                //Basket basket = Request.Cookies["_clientId"];
                Basket basket = SiteContext.Current.OrderContext.GetBasket();
                ///Get current purchase order
                PurchaseOrder purchaseOrder = basket.PurchaseOrder;
                OrderId = basket.PurchaseOrder.OrderId.ToString();

                decimal checkoutAmount = 0.00m;

                // Get the Checout Amount from the extended property of Purchase Order which is set in Basket Page
                decimal.TryParse(purchaseOrder[AffinionConstants.OrderCheckoutAmountColumn], out checkoutAmount);
                if (checkoutAmount > 0)
                {
                    AmountToPay = checkoutAmount;
                    

                    ///Build the page and load the payment gateway
                    ExecutePaymentMethodService(paymentMethod, purchaseOrder);
                }
                else
                {
                    Diagnostics.Trace(DiagnosticsCategory.ClientPortal, "Payment Amount not defined.");
                    throw new AffinionException("Payment Amount not defined.");
                }

            }
            catch (Exception exception)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, exception.Message);
                throw new AffinionException("Error loading Realex payment page", exception);
            }
        }

        /// <summary>
        /// Captures the error in page level
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The parameters</param>
        protected void Page_Error(object sender, EventArgs e)
        {
            // Get last error from the server
            Exception exception = Server.GetLastError();
            string errorPage = this.GetErrorPage(exception);

            // Handle error pages
            if (!string.IsNullOrEmpty(errorPage))
            {
                HttpContext.Current.RewritePath(errorPage);
                //Response.Redirect(errorPage);
            }
        }

        #endregion

        #region Public methods
        /// <summary>
        /// Execute payment method service to build and load payment gateway
        /// </summary>
        /// <param name="paymentMethod">Selected payment method</param>
        /// <param name="purchaseOrder">Current purchase order</param>
        /// <param name="amount">Amount to be paid</param>
        public void ExecutePaymentMethodService(PaymentMethod paymentMethod, PurchaseOrder purchaseOrder)
        {
            var cultureInfo = new CultureInfo(CultureInfo.InvariantCulture.LCID);

            ///Create a new payment request to 
            var paymentRequest = new PaymentRequest(
                        purchaseOrder,
                        paymentMethod,
                        new Money(AmountToPay,
               new CultureInfo(CultureInfo.InvariantCulture.LCID), purchaseOrder.BillingCurrency)

               );

            ///Build and load the payment page
            Payment payment = paymentMethod.GetPaymentMethodService().RequestPayment(paymentRequest);
            
            SetSession(purchaseOrder.OrderGuid.ToString(), AmountToPay.ToString(), paymentRequest.PurchaseOrder.OrderId.ToString());
        }
        #endregion

        #region Private methods

        /// <summary>
        /// Gets the error page
        /// </summary>
        /// <param name="exception">The generated exception</param>
        /// <returns>returns the error page url as a string</returns>
        private string GetErrorPage(Exception exception)
        {
            if (exception is InvalidOperationException)
            {
                return "Pages/GenericErrorPage.aspx";
            }
            else
            {
                return "Pages/GenericErrorPage.aspx";
                // return string.Empty;
            }
        }

        private void SetSession(string oderId, string amount, string responseOther)
        {
            Session["ResponseOrderId"] = oderId;
            Session["ResponseAmount"] = amount;
            Session["ResponseOther"] = responseOther;
        }

        #endregion
    }
}