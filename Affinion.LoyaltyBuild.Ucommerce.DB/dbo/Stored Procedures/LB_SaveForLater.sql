﻿CREATE PROCEDURE [dbo].[LB_SaveForLater](@CustomerID int,@HotelID Varchar(50), @Curdate datetime)
AS
Declare
@indb int
BEGIN
Select @indb=count(providerID) from LB_Savedforlater where providerID=@HotelID and customerID=@CustomerID
if(@indb=0)
begin
INSERT INTO LB_Savedforlater VALUES(@CustomerID, @HotelID, (select FirstName from uCommerce_Customer where CustomerID=@CustomerID), (select FirstName from uCommerce_Customer where CustomerID=@CustomerID), @Curdate, @Curdate)
end 
END

