﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sitecore.Shell.Framework.Commands;
using Sitecore.Web;
using Sitecore.Data.Items;
using Sitecore.Data;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration.AuthoringExperience.PageEditor.Base;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration.AuthoringExperience.PageEditor
{
    class FieldEditorButtonForAbstractField : FieldEditorButtonForSingleField
    {
        /// <summary>
        /// Determine if the command button should be displayed or hidden.
        /// </summary>
        public override CommandState QueryState(CommandContext context)
        {
            return base.QueryState(context, "abstract");
        }
    }
}
