﻿namespace Affinion.Loyaltybuild.BedBanks.CAPI.Model
{
    /// <summary>
    /// Summary description for Distance
    /// </summary>
    public class HotelDistance
    {
        public double Amount { get; set; }
        public string From { get; set; }
        public string Unit { get; set; }

        public HotelDistance(double pAmount, string pFrom, string pUnit)
        {
            Amount = pAmount;
            From = pFrom;
            Unit = pUnit;
        }
    }
}