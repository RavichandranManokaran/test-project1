﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           BasketHelper.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Utilities
/// Description:           Configuration Helper Class to read configuration settings in Web.config file
/// </summary>

#region Using Directives
using Affinion.LoyaltyBuild.Common.Exceptions;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.SessionHelper;
using Affinion.LoyaltyBuild.Common.uCommerceUtility;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using UCommerce.Api;
using UCommerce.EntitiesV2;
using UCommerce.Xslt;

#endregion
namespace Affinion.LoyaltyBuild.Common.Utilities
{
    /// <summary>
    /// BasketHelper
    /// </summary>
    public static class BasketHelper
    {
        /// <summary>
        /// Add item to basket
        /// </summary>
        /// <param name="commandArgument">string array</param>
        public static void AddToBasket(string[] commandArgument, int qty, BasketInfo basketInfo = null)
        {
            try
            {
                int currencyId = 0;
                if (IsBasketExists())
                {
                    if (Int32.TryParse(commandArgument[1], out currencyId))
                    {
                        TransactionLibrary.AddToBasket(qty, commandArgument[0], null, false, true, currencyId);
                        TransactionLibrary.GetBasket(false).Save();
                        Library.ExecuteBasketPipeline();

                        // Call custom method to insert data
                        UpdateBasketCustomData(basketInfo);
                    }
                }
                else
                {
                    if (Int32.TryParse(commandArgument[1], out currencyId))
                    {
                        TransactionLibrary.AddToBasket(qty, commandArgument[0], null, false, true, currencyId);
                        TransactionLibrary.GetBasket(true).Save();
                        Library.ExecuteBasketPipeline();

                        // Call custom method to insert data
                        UpdateBasketCustomData(basketInfo);
                    }
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(null, exception.InnerException);
            }
        }

        /// <summary>
        /// Use to remove order line by Sku Id
        /// </summary>
        /// <param name="sku">order Sku id</param>
        public static void RemoveCartItemBySkuId(string sku)
        {
            try
            {
                if (IsBasketExists())
                {
                    var order = UCommerce.Api.TransactionLibrary.GetBasket(false).PurchaseOrder.OrderLines.Where(o => o.Sku == sku).FirstOrDefault();
                    if (order != null)
                    {
                        bool isRemoved = UCommerce.Api.TransactionLibrary.GetBasket(false).PurchaseOrder.OrderLines.Remove(order);
                        if (isRemoved)
                            UCommerce.Api.TransactionLibrary.GetBasket(false).PurchaseOrder.Save();
                    }
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(null, exception.InnerException);
            }
        }

        /// <summary>
        /// Get the number of item in basket
        /// </summary>
        /// <returns></returns>
        public static int GetBasketCount()
        {
            try
            {
                if (IsBasketExists())
                    return UCommerce.Api.TransactionLibrary.GetBasket(false).PurchaseOrder.OrderLines.Count;
                else
                    return 0;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(null, exception.InnerException);
            }
        }

        /// <summary>
        /// Clear the basket
        /// </summary>
        public static void ClearBasket()
        {
            try
            {
                if (IsBasketExists())
                {
                    UCommerce.Api.TransactionLibrary.GetBasket(false).PurchaseOrder.OrderLines.Clear();
                    UCommerce.Api.TransactionLibrary.GetBasket(false).PurchaseOrder.Delete();
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(null, exception.InnerException);
            }
        }

        /// <summary>
        /// Check the basket exists
        /// </summary>
        /// <returns></returns>
        private static bool IsBasketExists()
        {
            return (UCommerce.Api.TransactionLibrary.HasBasket());
        }

        /// <summary>
        /// Add to custom fields to basket related tables
        /// </summary>
        /// <param name="basketInfo"></param>
        private static void UpdateBasketCustomData(BasketInfo basketInfo)
        {
            try
            {
                uCommerceConnectionFactory.UpdateCustomOrderLineData(basketInfo, GetBasketOrderId());
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(null, exception.InnerException);
            }
        }

        /// <summary>
        /// Get the purchase Order Id
        /// </summary>
        /// <returns></returns>
        private static int GetBasketOrderId()
        {
            try
            {
                int orderID = 0;
                if (IsBasketExists())
                {
                    orderID = TransactionLibrary.GetBasket(false).PurchaseOrder.OrderId;
                    //orderID = TransactionLibrary.GetBasket(false).PurchaseOrder.OrderLines.FirstOrDefault().OrderLineId;
                }
                return orderID;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(null, exception.InnerException);
            }
        }


        /// <summary>
        /// Get the list of order lines
        /// </summary>
        /// <returns></returns>
        public static DataSet GetPurchaseOrderLines()
        {
            try
            {
                //ICollection<OrderLine> orderLines = null;
                DataSet ds = new DataSet(); 
                if (IsBasketExists())
	            {
                    ds = uCommerceConnectionFactory.GetPurchaseOrderLines(GetBasketOrderId()); 
	            }
                return ds;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(null, exception.InnerException);
            }
        }
    }
}
