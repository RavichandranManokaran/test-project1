﻿using System;
using System.Collections.Generic;

namespace Affinion.Loyaltybuild.BedBanks.CAPI.Model
{
    /// <summary>
    /// Name:           Hotel
    /// Author:         Adam Berry
    /// Date:           25/01/2016
    /// CAPI Version :  v1.0
    /// 
    /// Description:
    /// 
    /// Main Data Class for Hotel Object. Instance of this class hold all key information 
    /// about available Hotels from CAPI.
    /// 
    /// Instances of this class are created directly from the JSON feed, and bound to
    /// the results page.
    /// </summary>
    [Serializable]
    public class Hotel
    {
        public string HotelId { get; set; }
        public string Name { get; set; }

        public HotelAddress Address { get; set; }
        public HotelDistance Distance { get; set; }

        public string CurrencyCode { get; set; }
        public double LowestAvailableRate { get; set; }
        public string LowestAvailableRateRoomName { get; set; }

        public List<HotelDescription> Descriptions { get; set; }

        public double Rating { get; set; }

        public HotelImage MainImage { get; set; }
        public List<HotelImage> Images { get; set; }
    }
}