﻿namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
    using Sitecore.Data.Items;
    using Affinion.LoyaltyBuild.Common;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Exceptions;

    public partial class ClientPortalRecentlySeen : BaseSublayout
    {
        protected string recentlySeenText { get; private set; }
        protected string closeText { get; private set; }
        int customerId;
        static Sitecore.Data.ID topSearchFieldId = Sitecore.Data.ID.Parse(Constants.TopSearchFieldIdString);
        static Sitecore.Data.ID hotelInformationTemplateID = Sitecore.Data.ID.Parse(Constants.HotelInformationTemplateIdString);
        int recentlySeenCount = 0;
        int maxRecentlySeenCount = 1;
        int overallCount = 3;
        string cookieName = "RecentlySeenHotels";

        protected void Page_Load(object sender, EventArgs e)
        {
            BindSitecoreTexts();
            this.customerId = GetCustomerId();

            this.SetCurrentHotelData();

            this.BindRecentlySeenHotels();
            this.BindTopSearchHotels();
            enableScript.Visible = !Sitecore.Context.PageMode.IsPageEditor;
        }

        private void BindSitecoreTexts()
        {
            Item dataSourceItem = this.GetDataSource();
            recentlySeenText = (dataSourceItem != null && dataSourceItem.Fields["TextRecentlySeen"] != null) ? dataSourceItem.Fields["TextRecentlySeen"].Value : string.Empty;
            closeText = (dataSourceItem != null && dataSourceItem.Fields["TextClose"] != null) ? dataSourceItem.Fields["TextClose"].Value : string.Empty;
            SitecoreFieldsHelper.BindSitecoreText(dataSourceItem, "TextRecentlySeen", recentlyseenHotel);
        }

        /// <summary>
        /// set current hotel Id and to display RecentlySeen Hotel from Cookie or From Database
        /// </summary>
        private void SetCurrentHotelData()
        {
            //To check whether the page is hotel information
            if (Sitecore.Context.Item.TemplateID.Equals(hotelInformationTemplateID))
            {
                string currenthotelID = Request.QueryString["id"];
                if (!string.IsNullOrWhiteSpace(currenthotelID))
                {
                    if (this.customerId != 0)
                    {
                        uCommerceConnectionFactory.setRecentlySeen(this.customerId, currenthotelID);
                    }
                    else
                    {
                        this.UpdateCookieValue(currenthotelID);
                    }
                }
            }
        }

        private void BindRecentlySeenHotels()
        {
            List<Item> hotelItems = new List<Item>();
            hotelItems = (this.customerId != 0) ? GetHotels() : GetHotelsFromCookie();
            this.recentlySeenCount = hotelItems.Count;
            rptRSeenHotelList.DataSource = hotelItems;
            rptRSeenHotelList.DataBind();
        }

        private void BindTopSearchHotels()
        {
            rptTopSearchHotel.DataSource = GetTopHotelForClients();
            rptTopSearchHotel.DataBind();
        }

        /// <summary>
        /// to check that the user is loggedIn or offlineUser 
        /// </summary>
        /// <returns></returns>
        private int GetCustomerId()
        {
            bool loggedIn = true;
            if (loggedIn)
            {
                int userId = 67;
                return userId;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Get Hotel Information from Sitecore based on Hotel Ids getting from Database
        /// </summary>
        /// <returns></returns>
        private List<Item> GetHotels()
        {
            List<string> Ids = new List<string>();
            Ids = GetHotelIdsFromDb();
            List<Item> hotelItem = new List<Item>();
            foreach (string id in Ids)
            {
                hotelItem.Add(Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(id)));
            }
            return hotelItem;
        }

        /// <summary>
        /// Get the List of Hotel Ids from Database
        /// </summary>
        /// <returns></returns>
        private List<string> GetHotelIdsFromDb()
        {
            DataSet ds = uCommerceConnectionFactory.getRecentlySeen(this.customerId);
            List<string> Ids = new List<string>();
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                Ids.Add(ds.Tables[0].Rows[i][0].ToString());
            }
            return Ids;
        }

        /// <summary>
        /// Get Hotel Information from Sitecore based on Hotel Ids getting from Cookies
        /// </summary>
        /// <returns></returns>
        private List<Item> GetHotelsFromCookie()
        {
            List<Item> hotelItems = new List<Item>();
            if (Request.Cookies[this.cookieName] != null && !string.IsNullOrWhiteSpace(Request.Cookies[this.cookieName].Value))
            {
                string hotelId = Request.Cookies[this.cookieName].Value;
                if (!string.IsNullOrWhiteSpace(hotelId))
                {
                    hotelItems.Add(Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(hotelId)));
                }
            }
            return hotelItems;
        }

        /// <summary>
        /// Get Top Searched Hotels 
        /// </summary>
        /// <returns>List of top searched Hotel</returns>
        private List<Item> GetTopHotelForClients()
        {
            List<Item> topHotelsList = new List<Item>();
            string query = "ancestor-or-self::*[@@templateid='" + Constants.ClientPortalRootTemplateIdString + "']//*[@@templateid='" + Constants.TopSearchTemplateIdString + "']";
            Item topHotels = Sitecore.Context.Item.Axes.SelectSingleItem(query);
            int topHotelsCount = 0;
            if (topHotels != null && topHotels.Fields[topSearchFieldId] != null && !string.IsNullOrWhiteSpace(topHotels.Fields[topSearchFieldId].Value))
            {
                Sitecore.Data.Fields.MultilistField topSearchMultilistField = topHotels.Fields[topSearchFieldId];
                if (topSearchMultilistField.TargetIDs.Length > 0)
                {
                    topHotelsList = topSearchMultilistField.GetItems().ToList<Item>();
                }

                if (this.recentlySeenCount >= this.maxRecentlySeenCount)
                {
                    topHotelsCount = this.overallCount - this.maxRecentlySeenCount;
                }
                else if (this.recentlySeenCount < this.maxRecentlySeenCount)
                {
                    topHotelsCount = this.overallCount - this.recentlySeenCount;
                }

                topHotelsList = (topHotelsCount > 0 && topHotelsCount <= topHotelsList.Count) ? topHotelsList.Take(topHotelsCount).ToList<Item>() : topHotelsList;
            }
            return topHotelsList;
        }

        /// <summary>
        /// Set Hotel Ids in Cookies
        /// </summary>
        /// <param name="currenthotelID"></param>
        private void UpdateCookieValue(string currenthotelID)
        {
            if (Request.Cookies[this.cookieName] != null)
            {
                Response.Cookies.Remove(this.cookieName);
            }

            HttpCookie recentlySeenCookie = new HttpCookie(this.cookieName);
            recentlySeenCookie.Value = currenthotelID;
            recentlySeenCookie.Expires = DateTime.Now.AddDays(365);
            Response.Cookies.Add(recentlySeenCookie);
        }


        /// <summary>
        /// Bind ranking related fields
        /// </summary>
        /// <param name="item">Sitecore item</param>
        private List<int> BindStarRankings(Item item)
        {
            List<int> listOfStars = new List<int>();
            ///Bind star rank
            string ranking = SitecoreFieldsHelper.GetDropLinkFieldValue(item, "StarRanking", "Name");

            if (!string.IsNullOrWhiteSpace(ranking))
            {
                int starRank;

                bool result = Int32.TryParse(ranking, out starRank);

                if (result)
                {
                    //starRank = int.Parse(ranking);
                    listOfStars = Enumerable.Repeat(starRank, starRank).ToList();
                }
                else
                {
                    Diagnostics.WriteException(DiagnosticsCategory.ClientPortal,
                        new AffinionException("Unable to parse star rank:" + item.DisplayName), this);
                }
            }
            return listOfStars;
        }

        /// <summary>
        /// Repeater for Showing hotels getting from Cookies or Database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rptRSeenHotelList_DataBinding(object sender, RepeaterItemEventArgs e)
        {
            Item hotelItem = e.Item.DataItem as Item;
            if (hotelItem != null)
            {
                Item locationItem = (hotelItem.Fields["Location"] != null && !string.IsNullOrWhiteSpace(hotelItem.Fields["Location"].Value)) ? ((Sitecore.Data.Fields.LookupField)hotelItem.Fields["Location"]).TargetItem : null;
                Item countryItem = (locationItem != null && locationItem.Fields["Country"] != null && !string.IsNullOrWhiteSpace(locationItem.Fields["Country"].Value)) ? ((Sitecore.Data.Fields.LookupField)locationItem.Fields["Country"]).TargetItem : null;

                string location = locationItem != null ? locationItem.Fields["Name"].Value : string.Empty;
                string country = countryItem != null ? countryItem.Fields["CountryName"].Value : string.Empty;

                Image hotelImage = e.Item.FindControl("imgHotelImage") as Image;
                Label hotelName = e.Item.FindControl("lblHotelName") as Label;
                Label hotelLocation = e.Item.FindControl("lblLocation") as Label;
                HyperLink linkHotelName = e.Item.FindControl("linkHotelName") as HyperLink;
                Repeater starRepeater = e.Item.FindControl("rptStarRating") as Repeater;

                Sitecore.Data.Fields.ImageField imgField = hotelItem.Fields["IntroductionImage"];
                hotelImage.ImageUrl = ((imgField.MediaItem) != null) ? Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField.MediaItem) : string.Empty;
                hotelName.Text = hotelItem.Fields["Name"].Value;
                hotelLocation.Text = (!string.IsNullOrWhiteSpace(location) && !string.IsNullOrWhiteSpace(country)) ? string.Format("{0}, {1}", location, country) : string.Format("{0}{1}", location, country);
                linkHotelName.NavigateUrl = "/hotelinformation?id=" + hotelItem.ID.ToString() + "&NoOfRooms=1&NoOfAdults=1&NoOfChildren=0";

                starRepeater.DataSource = BindStarRankings(hotelItem);
                starRepeater.DataBind();
            }
        }

        /// <summary>
        /// Repeater for showing Top Search Hotels
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rptTopSearchHotel_DataBinding(object sender, RepeaterItemEventArgs e)
        {
            Item hotelItem = e.Item.DataItem as Item;
            if (hotelItem != null)
            {
                Item locationItem = (hotelItem.Fields["Location"] != null && !string.IsNullOrWhiteSpace(hotelItem.Fields["Location"].Value)) ? ((Sitecore.Data.Fields.LookupField)hotelItem.Fields["Location"]).TargetItem : null;
                Item countryItem = (locationItem != null && locationItem.Fields["Country"] != null && !string.IsNullOrWhiteSpace(locationItem.Fields["Country"].Value)) ? ((Sitecore.Data.Fields.LookupField)locationItem.Fields["Country"]).TargetItem : null;

                string location = locationItem != null ? locationItem.Fields["Name"].Value : string.Empty;
                string country = countryItem != null ? countryItem.Fields["CountryName"].Value : string.Empty;

                Image hotelImage = e.Item.FindControl("imgHotelImage") as Image;
                Label hotelName = e.Item.FindControl("lblHotelName") as Label;
                Label hotelLocation = e.Item.FindControl("lblLocation") as Label;
                HyperLink linkHotelName = e.Item.FindControl("linkHotelName") as HyperLink;
                Repeater starRepeater = e.Item.FindControl("rptStarRating") as Repeater;

                Sitecore.Data.Fields.ImageField imgField = hotelItem.Fields["IntroductionImage"];
                hotelImage.ImageUrl = ((imgField.MediaItem) != null) ? Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField.MediaItem) : string.Empty;
                hotelName.Text = hotelItem.Fields["Name"].Value;
                hotelLocation.Text = (!string.IsNullOrWhiteSpace(location) && !string.IsNullOrWhiteSpace(country)) ? string.Format("{0}, {1}", location, country) : string.Format("{0}{1}", location, country);
                linkHotelName.NavigateUrl = "/hotelinformation?id=" + hotelItem.ID.ToString() + "&NoOfRooms=1&NoOfAdults=1&NoOfChildren=0";

                starRepeater.DataSource = BindStarRankings(hotelItem);
                starRepeater.DataBind();
            }
        }
    }
}