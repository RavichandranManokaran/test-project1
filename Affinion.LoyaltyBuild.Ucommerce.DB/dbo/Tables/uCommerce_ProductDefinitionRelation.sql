﻿CREATE TABLE [dbo].[uCommerce_ProductDefinitionRelation] (
    [ProductDefinitionRelationId] INT IDENTITY (1, 1) NOT NULL,
    [ProductDefinitionId]         INT NOT NULL,
    [ParentProductDefinitionId]   INT NOT NULL,
    [SortOrder]                   INT CONSTRAINT [DF_uCommerce_ProductDefinitionRelation_SortOrder] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_uCommerce_ProductDefinitionRelation] PRIMARY KEY CLUSTERED ([ProductDefinitionRelationId] ASC)
);

