﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Handler.Offer
{
    /// <summary>
    /// Summary description for SubscribeForNewOffer
    /// </summary>
    public class SubscribeForNewOffer : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                context.Response.ContentType = "text/plain";
                string offerId = context.Request["offerId"];
                string supplierInviteId = context.Request["supplierInviteId"];
                context.Response.Write(LoadUserControl(context, offerId, supplierInviteId));
            }
            catch (Exception er)
            {
                context.Response.Write("ERROR" + er.Message.ToString());
                Sitecore.Diagnostics.Log.Error(er.Message, new object());
            }
            context.Response.End();
        }



        protected static string LoadUserControl(HttpContext context, string offerId, string supplierInviteId)
        {
            using (Page page = new Page())
            {
                StringBuilder s = new StringBuilder();
                page.EnableViewState = false;
                CampaignDetail offerDetails = new CampaignDetail();
                AvailabilityHelper staticDataForRoom = new AvailabilityHelper(context);
                int campaignItemId = Convert.ToInt32(offerId);
                offerDetails = staticDataForRoom.GetCampaignDescriptionById(Convert.ToInt32(campaignItemId), Convert.ToInt32(supplierInviteId));
                return offerDetails.Content;
              /*  PlaceHolder f = new PlaceHolder();


                using (StringWriter writer = new StringWriter(CultureInfo.CurrentCulture))
                {
                    Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Offer.SubscribeForNewOffer subscribePopup = (Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Offer.SubscribeForNewOffer)page.LoadControl("~/Layouts/SubLayouts/SupplierPortal/Controls/Offer/SubscribeForNewOffer.ascx");
                    subscribePopup.Detail = offerDetails;
                    subscribePopup.CampaignItemId = campaignItemId;
                    subscribePopup.SupplierInviteId = Convert.ToInt32(supplierInviteId);
                    f.Controls.Add(subscribePopup);
                    page.Controls.Add(f);
                    HttpContext.Current.Server.Execute(page, writer, false);
                    s.Clear();
                    s.Append(writer.ToString());
                }
                string html = Regex.Replace(s.ToString(), "<input[^>]*id=\"(__VIEWSTATE)\"[^>]*>", string.Empty, RegexOptions.IgnoreCase);
                return html;*/
            }

        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}