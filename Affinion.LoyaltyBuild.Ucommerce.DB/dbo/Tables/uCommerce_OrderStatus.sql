﻿CREATE TABLE [dbo].[uCommerce_OrderStatus] (
    [OrderStatusId]       INT            IDENTITY (1, 1) NOT NULL,
    [Name]                NVARCHAR (50)  NOT NULL,
    [Sort]                INT            CONSTRAINT [uCommerce_DF_OrderStatus_Sort] DEFAULT ((0)) NOT NULL,
    [RenderChildren]      BIT            CONSTRAINT [uCommerce_DF_OrderStatus_RenderChildren] DEFAULT ((0)) NOT NULL,
    [RenderInMenu]        BIT            CONSTRAINT [uCommerce_DF_OrderStatus_RenderInMenu] DEFAULT ((1)) NOT NULL,
    [NextOrderStatusId]   INT            NULL,
    [ExternalId]          NVARCHAR (50)  NULL,
    [IncludeInAuditTrail] BIT            CONSTRAINT [uCommerce_DF_OrderStatus_IncludeInAuditTrail] DEFAULT ((1)) NOT NULL,
    [Order]               INT            NULL,
    [AllowUpdate]         BIT            CONSTRAINT [uCommerce_DF_OrderStatus_AllowUpdate] DEFAULT ((1)) NOT NULL,
    [AlwaysAvailable]     BIT            CONSTRAINT [uCommerce_DF_OrderStatus_AlwaysAvailable] DEFAULT ((0)) NOT NULL,
    [Pipeline]            NVARCHAR (128) NULL,
    [AllowOrderEdit]      BIT            CONSTRAINT [uCommerce_OrderStatus_AllowOrderEdit] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [uCommerce_PK_OrderStatus] PRIMARY KEY CLUSTERED ([OrderStatusId] ASC),
    CONSTRAINT [uCommerce_FK_OrderStatus_OrderStatus1] FOREIGN KEY ([NextOrderStatusId]) REFERENCES [dbo].[uCommerce_OrderStatus] ([OrderStatusId])
);

