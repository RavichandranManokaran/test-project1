﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class SeasonUnitDiscountAwardDetail : IEntity
    {
        // Fields
        private int? _hashCode;

        // Methods
        public new static IQueryable<SeasonUnitDiscountAwardDetail> All()
        {
            return GetRepo().Select();
        }

        public virtual void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<SeasonUnitDiscountAwardDetail, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            SeasonUnitDiscountAwardDetail objA = obj as SeasonUnitDiscountAwardDetail;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<SeasonUnitDiscountAwardDetail, bool>> expression)
        {
            return All().Any<SeasonUnitDiscountAwardDetail>(expression);
        }

        public static IList<SeasonUnitDiscountAwardDetail> Find(Expression<Func<SeasonUnitDiscountAwardDetail, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<SeasonUnitDiscountAwardDetail>();
        }

        public static SeasonUnitDiscountAwardDetail FirstOrDefault(Expression<Func<SeasonUnitDiscountAwardDetail, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<SeasonUnitDiscountAwardDetail>();
        }

        public new static SeasonUnitDiscountAwardDetail Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<SeasonUnitDiscountAwardDetail> GetRepo()
        {
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<SeasonUnitDiscountAwardDetail>>();

        }


        public virtual void Save()
        {
            GetRepo().Save(this);
        }

        public static SeasonUnitDiscountAwardDetail SingleOrDefault(Expression<Func<SeasonUnitDiscountAwardDetail, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties

        public virtual DateTime StartDate { get; set; }

        public virtual DateTime EndDate { get; set; }

        public virtual decimal AmountOff { get; set; }

        public virtual SeasonUnitDiscountAward SeasonUnitDiscountAward { get; set; }

        public virtual int SeasonUnitDiscountAwardDetailId { get; set; }

        public virtual bool IsPercentage { get; set; }

        public virtual bool OnOrderLine { get; set; }
        

        public virtual int Id
        {
            get
            {
                return this.SeasonUnitDiscountAwardDetailId;
            }
        }
    }
}
