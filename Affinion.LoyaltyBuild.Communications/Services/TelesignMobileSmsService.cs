﻿/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           TelesignMobileSmsServeice.cs
/// Sub-system/Module:     Communications(VS project name)
/// Description:           Sms Service Implementation for Telesign Mobile
/// </summary>

#region Using Directives
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
#endregion

namespace Affinion.LoyaltyBuild.Communications.SmsService
{

    public class TelesignMobileSmsService : IMessageService
    {

        #region Private Properties
        private string userName = string.Empty;
        private string password = string.Empty;
        private int numberOfRequests;
        private Collection<ResponseError> responseErrors = new Collection<ResponseError>();
        private int messageCharacterCount;
        #endregion

        #region Public Properties
        public string Message { get; set; }
        public string SourceAddress { get; set; }
        public int DestinationAddress { get; set; }
        public string TypeOfMessage { get; set; }
        public string BaseUrl { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Base data for URL
        /// </summary>
        public TelesignMobileSmsService()
        {
            try
            {
                userName = ConfigurationHelper.GetWebConfigAppSettingValue("TelesignMobileUserName", "user");
                password = ConfigurationHelper.GetWebConfigAppSettingValue("TelesignMobilePassword", "pass");
                SourceAddress = ConfigurationHelper.GetWebConfigAppSettingValue("TelesignMobileSourceAddress", "Test");
                BaseUrl = ConfigurationHelper.GetWebConfigAppSettingValue("TelesignMobileBaseUrl", "http://smsc5.routotelecom.com/SMSsend");
                TypeOfMessage = ConfigurationHelper.GetWebConfigAppSettingValue("TelesignMobileTypeOfMessage", "sms");
                int.TryParse(ConfigurationHelper.GetWebConfigAppSettingValue("TelesignMobileMessageCharacterCount", "160"), out  messageCharacterCount);
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Communications, ex, this);
                throw;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Send message to Single Receiver
        /// </summary>
        /// <param name="to">Receivers Number</param>
        /// <param name="message">Message</param>
        /// <returns>Response Status</returns>
        public ResponseStatus Send(string to, string message)
        {
            try
            {
                Message = message;

                ///Number count validation
                if (String.IsNullOrEmpty(to))
                {
                    NoValidNumberList();
                }

                ///add numbet to the list
                List<string> numbers = to.Split(',').ToList();
                IList<string> numberList = ValidNumbers(numbers);

                ///Message char count check and set type
                if (message.Length > messageCharacterCount)
                {
                    this.TypeOfMessage = "LongSMS";
                }

                return this.SendMessage(numberList, message);
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Communications, ex, this);
                throw;
            }


        }

        /// <summary>
        /// Send message to Multiple Receivers
        /// </summary>
        /// <param name="to">Receivers Numbers</param>
        /// <param name="message">Message</param>
        /// <returns>Response Status</returns>
        public ResponseStatus Send(IList<string> to, string message)
        {
            try
            {
                Message = message;
                IList<string> validNumberList = ValidNumbers(to);

                ///Message char count check and set type
                if (message.Length > messageCharacterCount)
                {
                    TypeOfMessage = "LongSMS";
                }

                ///Check the numbers count
                if (validNumberList.Count <= 10)
                {

                    return this.SendMessage(validNumberList, message);

                }
                else
                {
                    ///Group numbers to group of 10
                    IList<List<string>> numbers = NumberListSplit(validNumberList, 10);
                    ResponseStatus status = ResponseStatus.Failed;

                    foreach (List<string> numberSets in numbers)
                    {
                        status = this.SendMessage(numberSets, message);
                    }

                    return status;
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Communications, ex, this);
                throw;
            }
        }

        /// <summary>
        ///  Send message to Single Receiver with the source address 
        /// </summary>
        /// <param name="from">Source Address</param>
        /// <param name="to">Receiver</param>
        /// <param name="message">Messate</param>
        /// <returns>Response Status</returns>
        public ResponseStatus Send(string from, string to, string message)
        {
            this.SourceAddress = from;
            return this.Send(to, message);
        }

        /// <summary>
        ///  Send message to Multiple Receivers with the source address 
        /// </summary>
        /// <param name="from">Source Address</param>
        /// <param name="to">Receiver</param>
        /// <param name="message">Messate</param>
        /// <returns>Response Status</returns>
        public ResponseStatus Send(string from, IList<string> to, string message)
        {
            this.SourceAddress = from;
            return this.Send(to, message);
        }

        /// <summary>
        /// Get the list of errors
        /// </summary>
        /// <returns>List of errors</returns>
        public ICollection<ResponseError> GetErrors()
        {
            return responseErrors;
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Send a message to a given non empty number list
        /// </summary>
        /// <param name="numberList">Numer List</param>
        /// <param name="message">Message</param>
        /// <returns>Response Status</returns>
        private ResponseStatus SendMessage(IList<string> numberList, string message)
        {
            try
            {
                if (numberList.Count == 0)
                {
                    NoValidNumberList();

                }

                using (var webClient = new WebClient())
                {
                    ///set post parameters
                    var postParameters = SetPostParameters(numberList, message);

                    var response = webClient.UploadValues(BaseUrl, postParameters);
                    string result = Encoding.UTF8.GetString(response);

                    this.numberOfRequests++;

                    return this.GetResponseStatus(result);
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Communications, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Get the response status depend on the raw result from the sms service
        /// </summary>
        /// <param name="result">Response Result</param>
        /// <returns>Response Status</returns>
        private ResponseStatus GetResponseStatus(string result)
        {
            if (!result.Equals("success"))
            {
                ///Add the error to error list
                responseErrors.Add(
                    new ResponseError
                    {
                        Error = ResponseDescription(result),
                        RawResponse = result
                    });
            }

            if (responseErrors.Count == 0)
            {
                return ResponseStatus.SuccessAll;
            }
            else if (responseErrors.Count == this.numberOfRequests)
            {
                return ResponseStatus.Failed;
            }
            else
            {
                return ResponseStatus.SuccessWithErrors;
            }
        }

        /// <summary>
        /// Number Validation
        /// </summary>
        /// <param name="numbers">Number List</param>
        /// <returns>Valid Numbers</returns>
        private static IList<string> ValidNumbers(IList<string> numbers) //TODO
        {
            return numbers;
        }

        /// <summary>
        /// Url Encoding
        /// </summary>
        /// <param name="str">String to encode</param>
        /// <returns>Url encoded string</returns>
        private static String Encode(String str)
        {
            return HttpUtility.UrlEncode(str);
        }

        /// <summary>
        /// Seperate number list to list
        /// </summary>
        /// <param name="source">Number List</param>
        /// <returns>Grouped number lists</returns>
        private IList<List<string>> NumberListSplit(IList<string> source, int count)
        {
            return source
                .Select((x, i) => new { Index = i, Value = x })
                .GroupBy(x => x.Index / count)
                .Select(x => x.Select(v => v.Value).ToList())
                .ToList();
        }

        /// <summary>
        /// Set Post Parameters
        /// </summary>
        /// <param name="numbers">List of numbers</param>
        /// <param name="message">Message</param>
        /// <returns>Post parameter values</returns>
        private NameValueCollection SetPostParameters(IList<string> numbers, string message)
        {
            //Convert Number list to comma seperated string
            var toNumbers = string.Join(",", numbers);

            var postParameters = new NameValueCollection
            {
                {"user", userName},
                {"pass", password},
                {"ownnum", SourceAddress},
                {"number", toNumbers},
                {"message", Encode(message)},
                {"type", TypeOfMessage}
            };

            return postParameters;
        }

        //TODO: Tested only for auth_failed
        /// <summary>
        /// possible responses from our SMS gateway when the message is 
        /// submitted from the client’s side. 
        /// </summary>
        /// <param name="code">Response Code</param>
        /// <returns>Response Description</returns>
        private static string ResponseDescription(string code)
        {
            switch (code.Trim())
            {
                case "success":
                    return "Sending successful";

                case "error":
                    return "Not all required parameters are present";

                case "auth_failed":
                    return "Incorrect username and/or password and/or not allowed IPaddress";

                case "wrong_number":
                    return "The number contains non-numeric characters";

                case "not_allowed":
                    return "You are not allowed to send to this number";

                case "too_many_numbers":
                    return "Sending to more than 10 numbers per request";

                case "no_message":
                    return "Either the required parameter 'message' is missing or message body is empty";

                case "too_long":
                    return "Message is too long";

                case "wrong_type":
                    return "An incorrect message type was selected";

                case "wrong_message":
                    return "vCalendar or vCard contains wrong message";

                case "wrong_format":
                    return "The wrong message format was selected";

                case "bad_operator":
                    return "Wrong operator code";

                case "failed":
                    return "Internal error";

                case "sys_error":
                    return "The system error";

                case "No credits left":
                    return "User has no credits";

                default:
                    return "Error";
            }

        }

        /// <summary>
        /// No valid number list 
        /// </summary>
        /// <returns>ResponseStatus</returns>
        private ResponseStatus NoValidNumberList()
        {
            ///Add the error to error list
            responseErrors.Add(
                new ResponseError
                {
                    Error = "No Valid Number Found",
                    RawResponse = "No Valid Number Found"
                });
            return ResponseStatus.Failed;
        }
        #endregion

    }
}
