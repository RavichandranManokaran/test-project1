﻿CREATE TABLE [dbo].[uCommerce_ProductReviewComment] (
    [ProductReviewCommentId] INT            IDENTITY (1, 1) NOT NULL,
    [ProductReviewId]        INT            NOT NULL,
    [CustomerId]             INT            NULL,
    [CreatedOn]              DATETIME       NOT NULL,
    [ModifiedOn]             DATETIME       NOT NULL,
    [ModifiedBy]             NVARCHAR (50)  NULL,
    [CultureCode]            NVARCHAR (60)  NULL,
    [Comment]                NVARCHAR (MAX) NULL,
    [Helpful]                BIT            CONSTRAINT [DF_uCommerce_ProductReviewComment_Helpful] DEFAULT ((0)) NOT NULL,
    [Unhelpful]              BIT            CONSTRAINT [DF_uCommerce_ProductReviewComment_Unhelpful] DEFAULT ((0)) NOT NULL,
    [Ip]                     NVARCHAR (50)  NOT NULL,
    [ProductReviewStatusId]  INT            NOT NULL,
    CONSTRAINT [PK_uCommerce_ProductReviewComment] PRIMARY KEY CLUSTERED ([ProductReviewCommentId] ASC),
    CONSTRAINT [FK_uCommerce_ProductReviewComment_uCommerce_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[uCommerce_Customer] ([CustomerId]),
    CONSTRAINT [FK_uCommerce_ProductReviewComment_uCommerce_ProductReview] FOREIGN KEY ([ProductReviewId]) REFERENCES [dbo].[uCommerce_ProductReview] ([ProductReviewId]),
    CONSTRAINT [FK_uCommerce_ProductReviewComment_uCommerce_ProductReviewStatus] FOREIGN KEY ([ProductReviewStatusId]) REFERENCES [dbo].[uCommerce_ProductReviewStatus] ([ProductReviewStatusId])
);

