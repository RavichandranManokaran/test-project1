﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model
{
    public class ProvisionalBookingFilter
    {
        /// <summary>
        /// Booking Reference
        /// </summary>
        public string BookingReference { get; set; }
        /// <summary>
        /// ProviderName
        /// </summary>
        public string partner { get; set; }
        /// <summary>
        /// DateFrom 
        /// </summary>
        public DateTime DateFrom { get; set; }
        /// <summary>
        /// DateTo
        /// </summary>
         public DateTime DateTo { get; set; }
        /// <summary>
        /// PartialPayment within specific date
        /// </summary>
        public int partialpayment {get; set;}
        /// <summary>
        /// Reports Orderby
        /// </summary>
        public string reportandorderby{get; set;}
        
    }
}
