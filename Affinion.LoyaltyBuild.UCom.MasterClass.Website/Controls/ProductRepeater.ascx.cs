﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.Controls
{
	public partial class ProductRepeater : System.Web.UI.UserControl
	{
		public IEnumerable<UCommerce.EntitiesV2.Product> Products { get; set; } 
		protected void Page_Load(object sender, EventArgs e)
		{
			ProductRepeaterInternal.DataSource = Products;
			ProductRepeaterInternal.DataBind();
		}
	}
}