﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sitecore.Data;
using Sitecore.Data.Items;

namespace Affinion.LoyaltyBuild.BedBanks.General
{
    public class BedBanksFacility
    {
        /// <summary>
        /// Bed banks facility Name
        /// </summary>
        public string FacilityName { get; set; }

        /// <summary>
        /// Bed Banks Facility Id
        /// </summary>
        public string FacilityId { get; set; }

        /// <summary>
        /// Sitecore Facility Item Id
        /// </summary>
        public Item SitecoreFacilityItem { get; set; }

        /// <summary>
        /// Sitecore Facility name
        /// </summary>
        public string SitecoreFacilityName { get; set; }
    }
}
