CREATE PROCEDURE [dbo].[LB_GetOracleInvoiceData]
@OracleFileID INT=NULL,
@RequestedFrom VARCHAR(10)=NULL
AS
BEGIN

	IF @RequestedFrom='AP'
	BEGIN
			SELECT  DISTINCT
				SupplierPaymentId [InvoiceID] ,
				OrderDate [InvoiceDate],
				OracleId [VendorNumber],
				SupplierAddressCode [VendorSiteCode],
				OrderCurrencyIsoCode [CurrencyCode],
				SuggestedAmount [Amount],
				PPD.SiteCoreItemGuid [ProviderID],
				CONVERT(VARCHAR(10),FORMAT(BI.CheckOutDate,'dd/MM/yyyy'))+' '+BI.BookingNo+' '+(C.FirstName+' '+C.LastName) [Description],
				OP.ClientId [ClientId]
			FROM 
				LB_Supplier_Payments LSP 
				INNER JOIN ucommerce_OrderLine OL ON LSP.OrderId=OL.OrderId AND LSP.OrderLineId=OL.OrderLineId
				INNER JOIN LB_BookingInfo BI ON OL.OrderLineId=BI.OrderLineId
				INNER JOIN (
				   SELECT OrderLineId,OrderId,_ClientId [ClientId]
					FROM 
					(
					  SELECT OrderLineId,OrderId, [key], [value]
					  FROM uCommerce_OrderProperty 
					) src
					PIVOT
					(
					  MAX([value])
					  FOR [key] IN (_ClientId)
					) piv
				) OP ON LSP.OrderId=OP.OrderId AND LSP.OrderLineId=OP.OrderLineId
				INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId=PO.OrderId AND LSP.OrderId=OL.OrderId
				INNER JOIN LB_CustomerProperty CP ON PO.CustomerId=CP.CustomerId
				INNER JOIN uCommerce_Customer C ON CP.CustomerId=C.CustomerId
				INNER JOIN Ucommerce_product P ON OL.Sku = P.Sku
				INNER JOIN (
				   SELECT ProductId,SiteCoreItemGuid
					FROM 
					(
					  SELECT ProductId,PD.Name [key], PP.value
					  FROM uCommerce_ProductDefinitionField PD 
					  INNER JOIN uCommerce_ProductProperty PP ON PD.ProductDefinitionFieldId=PP.ProductDefinitionFieldId
					  WHERE PD.Name IN ('SiteCoreItemGuid')
					) src
					pivot
					(
					  MAX([value])
					  FOR [key] in (SiteCoreItemGuid)
					) piv
				) PPD ON PPD.ProductId=P.ProductId 
			WHERE 
				LSP.OracleFileId=@OracleFileID AND
				PO.CustomerId IS NOT NULL AND
				LSP.PostedAmount IS NOT NULL
			ORDER BY 
				SupplierPaymentId ASC
	END
	ELSE IF @RequestedFrom='AR'
	BEGIN
			SELECT  DISTINCT
				LVP.InvoiceId [InvoiceID] ,
				LVP.OrderDate [InvoiceDate],
				LVP.CustomerId [CustomerId],
				LVP.CustomerAddressCode [CustomerAddressCode],
				LVP.OrderCurrencyIsoCode [CurrencyCode],
				LVP.NetAmount [Amount],
				CONVERT(VARCHAR(10),FORMAT(CheckOutDate,'dd/MM/yyyy'))+' '+BI.BookingNo+' '+(C.FirstName+' '+C.LastName) [Description],
				PPD.SiteCoreItemGuid [ProviderID],
				LVP.SupplierVatPaymentId [SupplierVatPaymentId],
				LVP.LineNumber [LineNumber],
				OP.ClientId [ClientId]
			FROM 
				LB_VAT_Payments LVP 
				INNER JOIN ucommerce_OrderLine OL ON LVP.OrderId=OL.OrderId AND LVP.OrderLineId=OL.OrderLineId
				INNER JOIN LB_BookingInfo BI ON OL.OrderLineId=BI.OrderLineId
				INNER JOIN (
				   SELECT OrderLineId,OrderId,_ClientId [ClientId]
					FROM 
					(
					  SELECT OrderLineId,OrderId, [key], [value]
					  FROM uCommerce_OrderProperty 
					) src
					PIVOT
					(
					  MAX([value])
					  FOR [key] IN (_ClientId)
					) piv
				) OP ON LVP.OrderId=OP.OrderId AND LVP.OrderLineId=OP.OrderLineId
				INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId=PO.OrderId AND LVP.OrderId=OL.OrderId
				INNER JOIN LB_CustomerProperty CP ON PO.CustomerId=CP.CustomerId
				INNER JOIN uCommerce_Customer C ON CP.CustomerId=C.CustomerId
				INNER JOIN Ucommerce_product P ON OL.Sku = P.Sku
				INNER JOIN (
				   SELECT ProductId,SiteCoreItemGuid
					FROM 
					(
					  SELECT ProductId,PD.Name [key], PP.value
					  FROM uCommerce_ProductDefinitionField PD 
					  INNER JOIN uCommerce_ProductProperty PP ON PD.ProductDefinitionFieldId=PP.ProductDefinitionFieldId
					  WHERE PD.Name IN ('SiteCoreItemGuid')
					) src
					PIVOT
					(
					  MAX([value])
					  FOR [key] IN (SiteCoreItemGuid)
					) piv
				) PPD ON PPD.ProductId=P.ProductId 
			WHERE 
				LVP.OracleFileId=@OracleFileID AND
				PO.CustomerId IS NOT NULL AND
				LVP.PostedVatAmount IS NOT NULL
			ORDER BY 
				SupplierVatPaymentId ASC
	END						

END



