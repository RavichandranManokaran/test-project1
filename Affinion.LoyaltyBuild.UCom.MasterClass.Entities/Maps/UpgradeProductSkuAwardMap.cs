﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Maps
{
    public class UpgradeProductSkuAwardMap : SubclassMap<UpgradeProductSkuAward>
    {
        // Methods
        public UpgradeProductSkuAwardMap()
        {
            base.Table("LB_UpgradeProductSkuAward");
            base.KeyColumn("UpgradeProductSkuAwardId");
            base.Map(x => x.UpgradeProductSkuAwardId).ReadOnly();
            base.Map(x => x.UpgradedRoomType).Not.Nullable();
        }

    }
}