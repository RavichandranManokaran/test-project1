﻿CREATE TABLE [dbo].[uCommerce_ProductCatalogGroupPaymentMethodMap] (
    [ProductCatalogGroupId] INT NOT NULL,
    [PaymentMethodId]       INT NOT NULL,
    CONSTRAINT [uCommerce_PK_ProductCatalogGroupPaymentMethodMap] PRIMARY KEY CLUSTERED ([ProductCatalogGroupId] ASC, [PaymentMethodId] ASC),
    CONSTRAINT [uCommerce_FK_ProductCatalogGroupPaymentMethodMap_PaymentMethod] FOREIGN KEY ([PaymentMethodId]) REFERENCES [dbo].[uCommerce_PaymentMethod] ([PaymentMethodId]),
    CONSTRAINT [uCommerce_FK_ProductCatalogGroupPaymentMethodMap_ProductCatalogGroup] FOREIGN KEY ([ProductCatalogGroupId]) REFERENCES [dbo].[uCommerce_ProductCatalogGroup] ([ProductCatalogGroupId])
);

