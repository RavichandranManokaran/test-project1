﻿CREATE PROCEDURE [dbo].[LB_GetCustomerDetails](
    @Email nvarchar(255) = NULL, 
	@ClientId varchar(50) = NULL, 
	@CustomerId int = NULL)
AS 

SELECT C.CustomerId, 
       C.FirstName, 
       C.LastName,   
       C.EmailAddress, 
       C.MobilePhoneNumber,
	   ISNULL(C.LanguageName,'0') AS LanguageName,
       C.PhoneNumber, 
       CA._ClientId [ClientId],
	   CA._Title [Title],
	   ISNULL(CA._AddressLine1,UA.Line1) [AddressLine1], 
       ISNULL(CA._AddressLine2,UA.Line2) [AddressLine2], 
       ISNULL(CA._AddressLine3,UA.City)  [AddressLine3], 
       CA._CountryId [CountryId], 
       CA._LocationId [LocationId],
	   CA._BranchId [BranchId], 
	   CA._Subscribe [Subscribe], 
	   CA._BookingReference [BookingReference], 
	   CA._Active [Active],
	   CA._SubscribeType [SubscribeType], 
	   CA._Store [Store], 
	   CA._TermsandCondition [TermsandCondition]
FROM [dbo].uCommerce_Customer C
	 INNER JOIN ( 
				select CustomerId, _ClientId, _Title,  _AddressLine1, _AddressLine2, _AddressLine3, _CountryId, _LocationId, _BranchId, _Subscribe, _BookingReference, _Active,_SubscribeType,
				_Store, _TermsandCondition
				from 
				(
				  SELECT CustomerId, [key] , [value]
				  from LB_CustomerProperty 
				) src
				pivot
				(
				  max([value])
				  for [key] in (_ClientId, _Title,  _AddressLine1, _AddressLine2, _AddressLine3, _CountryId, _LocationId, _BranchId, _Subscribe, _BookingReference, _Active,_SubscribeType, 
				  _Store, _TermsandCondition)
				) piv
			)  CA ON C.CustomerId = CA.CustomerId
			  Left Outer Join
			  uCommerce_Address UA
			  ON
			  UA.CustomerId = C.CustomerId
WHERE 
	C.EmailAddress = ISNULL(@Email,C.EmailAddress) 
	AND REPLACE(REPLACE(CA._ClientId,'{',''),'}','') = ISNULL(REPLACE(REPLACE(@ClientId,'{',''),'}',''),CA._ClientId) 
	AND CA.CustomerId = ISNULL(@CustomerId,CA.CustomerId) 
