﻿/*<summary>
Description: Stored procedure to retrieve Existing Booking Status List
</summary>*/

CREATE PROCEDURE [dbo].[LB_GetBookingStatusList]
AS 
BEGIN
	SELECT OrderStatusId, 
		Name
	FROM [dbo].LB_OrderStatus 
	WHERE OrderStatusId=5 or OrderStatusId=7 or OrderStatusId=6
END

GO