﻿CREATE PROCEDURE [dbo].[LB_UpdateAvailability]
(@OrderLineId INT,
@CheckInDateCount INT, 
@CheckOutDateCount INT,
@Sku NVARCHAR(512))
AS

WHILE (@CheckInDateCount < @CheckOutDateCount)
BEGIN

UPDATE [LB_RoomAvailability]
SET
	NumberBooked = NumberBooked + 1
WHERE DateCounter = @CheckInDateCount AND ProviderOccupancyTypeID = @Sku

SET @CheckInDateCount = @CheckInDateCount + 1
END