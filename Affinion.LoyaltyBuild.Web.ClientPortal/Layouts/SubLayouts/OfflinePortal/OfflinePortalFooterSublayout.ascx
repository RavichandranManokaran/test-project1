﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalFooterSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalFooterSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<!-- Site footer -->
<div class="width-full footer-container">
    <div class="container-outer-inner width-full copyright-bar accent4-bg">
        <div class="container">
            <div class="copyright-text padding-common">
                <p id="Copyright">Copyright &copy; [year] Loyaltybuild. All rights reserved.</p>
            </div>
        </div>
    </div>
    <div class="scroll-top-wrapper ">
        <span class="scroll-top-inner"></span>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {

        jQuery("#Copyright").text(function () {
            return jQuery(this).text().replace("[year]", new Date().getFullYear());
        });
    });

</script>