﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.EntitiesV2.Queries;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Queries
{
    public class LBActiveCampaignItemsQuery : ICannedQuery<CampaignItem>
    {
        /// <summary>
        /// _StartDate
        /// </summary>
        private readonly DateTime _startDate;
        /// <summary>
        /// _endDate
        /// </summary>
        private readonly DateTime _endDate;

        /// <summary>
        /// LBActiveCampaignItemsQuery
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        public LBActiveCampaignItemsQuery(DateTime startDate, DateTime endDate)
        {
            _startDate = startDate;
            _endDate = endDate;
        }

        /// <summary>
        /// IEnumerable  Type CampaignItem
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public IEnumerable<CampaignItem> Execute(ISession session)
        {
            IEnumerable<CampaignItem> source = session.CreateQuery("SELECT campaignItem \r\n\t\t\t\t\t\t\t\tFROM \r\n\t\t\t\t\t\t\t\t\tCampaignItem campaignItem\r\n\t\t\t\t\t\t\t\tWHERE \r\n\t\t\t\t\t\t\t\t\tcampaignItem.Enabled\r\n\t\t\t\t\t\t\t\t\tAND NOT campaignItem.Deleted\r\n\t\t\t\t\t\t\t\t\tAND campaignItem.Campaign.Enabled\r\n\t\t\t\t\t\t\t\t\tAND NOT campaignItem.Campaign.Deleted\r\n\t\t\t\t\t\t\t\t\tAND campaignItem.Campaign.StartsOn <= :latestTimeOfToday\r\n\t\t\t\t\t\t\t\t\tAND campaignItem.Campaign.EndsOn >= :earliestTimeOfToday").SetParameter<DateTime>("latestTimeOfToday", _startDate).SetParameter<DateTime>("earliestTimeOfToday", _endDate).SetCacheable(false).Future<CampaignItem>();
            session.CreateQuery("SELECT campaignItem \r\n\t\t\t\t\t\t\t\t\tFROM \r\n\t\t\t\t\t\t\t\t\t\tCampaignItem campaignItem\r\n\t\t\t\t\t\t\t\t\t\tINNER JOIN FETCH campaignItem.Campaign campaign\r\n\t\t\t\t\t\t\t\t\t\tLEFT OUTER JOIN FETCH campaign.ProductCatalogGroups\r\n\t\t\t\t\t\t\t\t\tWHERE \r\n\t\t\t\t\t\t\t\t\t\tcampaignItem.Enabled\r\n\t\t\t\t\t\t\t\t\t\tAND NOT campaignItem.Deleted\r\n\t\t\t\t\t\t\t\t\t\tAND campaignItem.Campaign.Enabled\r\n\t\t\t\t\t\t\t\t\t\tAND NOT campaignItem.Campaign.Deleted\r\n\t\t\t\t\t\t\t\t\t\tAND campaignItem.Campaign.StartsOn <= :latestTimeOfToday\r\n\t\t\t\t\t\t\t\t\t\tAND campaignItem.Campaign.EndsOn >= :earliestTimeOfToday").SetParameter<DateTime>("latestTimeOfToday", _startDate).SetParameter<DateTime>("earliestTimeOfToday", _endDate).SetCacheable(false).Future<CampaignItem>();
            return (from x in source.ToList<CampaignItem>()
                    where (x.Campaign.StartsOn <= _startDate) && (x.Campaign.EndsOn >= _endDate)
                    orderby x.Campaign.Priority, x.Priority
                    select x);

            
        }


    }
}
