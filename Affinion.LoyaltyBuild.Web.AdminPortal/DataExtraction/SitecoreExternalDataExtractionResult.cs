﻿using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Affinion.LoyaltyBuild.AdminPortal.DataExtraction
{
    public class SitecoreExternalDataExtractionResult
    {
        /// <summary>
        /// Item Property
        /// </summary>
        public Item ContextItem { get; set; }

        /// <summary>
        /// Exception Type Property
        /// </summary>
        public Exception Exception { get; set; }

        /// <summary>
        /// Errortype Property
        /// </summary>
        public SitecoreExternalDataExtractionErrorType ErrorType { get; set; }
    }

    /// <summary>
    /// enum types
    /// </summary>
    public enum SitecoreExternalDataExtractionErrorType
    {
        Suceeded=0,
        SuceededWithErrors,
        Failed
    }
}