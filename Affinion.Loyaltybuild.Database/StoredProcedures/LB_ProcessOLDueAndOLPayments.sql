﻿IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LB_ProcessOLDueAndOLPayments]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[LB_ProcessOLDueAndOLPayments]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
/*
*/ 
CREATE PROCEDURE [dbo].[LB_ProcessOLDueAndOLPayments]
(	 
	@OrderId INT=NULL,
	@IsPartialPayment BIT = 1	 
)
AS 
BEGIN
	IF (@OrderId IS NOT NULL)
	BEGIN	
		--Inserting data into ucommerce_orderlinedue based ON 'LBCOM'					
		INSERT INTO LB_orderlinedue 
		(
			OrderLineID, 
			DueTypeId, 
			CurrencyTypeId, 
			Amount, 
			CommissionAmount, 
			TransferAmount,
			ClientAmount, 
			ProviderAmount, 
			CollectionByProviderAmount,
			Note, 
			OrderLinePaymentID, 
			IsDeleted, 
			CreatedBy, 
			CreatedDate, 
			UpdatedBy, 
			UpdatedDate,
			CancellationAmount, SubrateItemCode
		)
		SELECT 
			OLS.OrderLineID AS [OrderLineID],
			DT.DueTypeId AS [DueTypeId], 
			PO.CurrencyId AS [CurrencyTypeId],
			OLS.Amount-ISNULL(OLS.Discount,0) AS [Amount],
			0 AS [CommissionAmount],
			0 AS [TransferAmount],
			0 AS [ClientAmount],
			0 AS [ProviderAmount],
			0 AS [CollectionByProviderAmount],
			'' AS [Note],
			NULL AS [OrderLinePaymentID],
			0 AS [IsDeleted],
			OL.CreatedBy AS [CreatedBy],
			OL.CreatedOn AS [CreatedDate],
			'' AS [UpdatedBy],
			GETDATE() AS [UpdatedDate], 
			0 AS [CancellationAmount],
			OLS.SubrateItemCode AS [SubrateItemCode]
		FROM 
			LB_OrderLineSubrates OLS
			INNER JOIN LB_duetypes DT ON ols.SubrateItemCode = DT.SubrateItemCode  
			INNER JOIN ucommerce_orderline OL ON ol.OrderLineId = OLS.OrderLineID		
			INNER JOIN uCommerce_PurchaseOrder PO ON po.OrderId = OL.OrderId
		WHERE 
			OLS.SubrateItemCode = 'LBCOM' 
			AND Po.OrderId  = @OrderId

		--this is a full payment (param name need to be reverted
		
		BEGIN
			INSERT INTO LB_orderlinedue 
			(
				OrderLineID, 
				DueTypeId, 
				CurrencyTypeId, 
				Amount, 
				CommissionAmount, 
				TransferAmount,
				ClientAmount, 
				ProviderAmount, 
				CollectionByProviderAmount,
				Note, 
				OrderLinePaymentID, 
				IsDeleted, 
				CreatedBy, 
				CreatedDate, 
				UpdatedBy, 
				UpdatedDate,
				CancellationAmount, SubrateItemCode
			)
			SELECT 
				OL.OrderLineID AS [OrderLineID],
				1 AS [DueTypeId], 
				PO.CurrencyId AS [CurrencyTypeId],
				OL.Total-OLS.Amount+ISNULL(OLS.Discount,0)  AS [Amount],
				0 AS [CommissionAmount],
				0 AS [TransferAmount],
				0 AS [ClientAmount],
				0 AS [ProviderAmount],
				0 AS [CollectionByProviderAmount],
				'' AS [Note],
				NULL AS [OrderLinePaymentID],
				0 AS [IsDeleted],
				OL.CreatedBy AS [CreatedBy],
				OL.CreatedOn AS [CreatedDate],
				'' AS [UpdatedBy],
				GETDATE() AS [UpdatedDate], 
				0 AS [CancellationAmount],
				'' AS [SubrateItemCode]
			FROM 
				LB_OrderLineSubrates OLS
				INNER JOIN LB_duetypes DT ON ols.SubrateItemCode = DT.SubrateItemCode  
				INNER JOIN ucommerce_orderline OL ON ol.OrderLineId = OLS.OrderLineID		
				INNER JOIN uCommerce_PurchaseOrder PO ON po.OrderId = OL.OrderId
				INNER JOIN uCommerce_OrderProperty OP ON OL.OrderLineId=OP.OrderLineId
			WHERE 
				OLS.SubrateItemCode = 'LBCOM' 
				AND Po.OrderId  = @OrderId
				AND OP.[Key]='PaymentMethod'
				AND OP.Value IN ('Full','Partial')
		END

		--Inserting data into ucommerce_orderlinedue based ON 'CCPROFEE'
		INSERT INTO LB_orderlinedue 
		(
			OrderLineID, 
			DueTypeId, 
			CurrencyTypeId, 
			Amount, 
			CommissionAmount, 
			TransferAmount,
			ClientAmount, 
			ProviderAmount, 
			CollectionByProviderAmount,
			Note, 
			OrderLinePaymentID, 
			IsDeleted, 
			CreatedBy, 
			CreatedDate, 
			UpdatedBy, 
			UpdatedDate,
			CancellationAmount, SubrateItemCode
		)
		SELECT TOP 1 
			OLS.OrderLineID AS [OrderLineID],
			DT.DueTypeId AS [DueTypeId], 
			PO.CurrencyId AS [CurrencyTypeId],
			OLS.Amount AS [Amount],
			0 AS [CommissionAmount],
			0 AS [TransferAmount],
			0 AS [ClientAmount],
			0 AS [ProviderAmount],
			0 AS [CollectionByProviderAmount],
			'' AS [Note],
			NULL AS [OrderLinePaymentID],
			0 AS [IsDeleted],
			OL.CreatedBy AS [CreatedBy],
			OL.CreatedOn AS [CreatedDate],
			'' AS [UpdatedBy],
			GETDATE() AS [UpdatedDate], 
			0 AS [CancellationAmount],
			OLS.SubrateItemCode AS [SubrateItemCode]
		FROM 
			LB_OrderLineSubrates OLS
			INNER JOIN LB_duetypes DT ON ols.SubrateItemCode = DT.SubrateItemCode  
			INNER JOIN ucommerce_orderline OL ON ol.OrderLineId = OLS.OrderLineID		
			INNER JOIN uCommerce_PurchaseOrder PO ON po.OrderId = OL.OrderId
		WHERE  
			OLS.SubrateItemCode = 'CCPROFEE'  AND 
			OLS.OrderLineID in (Select orderlineid from ucommerce_orderline where orderid = @OrderId)
		ORDER BY ols.Amount DESC
		
		--Inserting data into ucommerce_orderlinepayment 
		INSERT INTO LB_orderlinepayment
		(
			OrderLineID, 
			CurrencyTypeId, 
			PaymentMethodID, 
			Amount, 
			OrderPaymentID, 
			CreatedDate, 
			ReferenceID, 
			CreatedBy,
			DueTypeId
		)
		SELECT 
			ol.orderlineid AS [OrderLineID], 
			po.CurrencyId AS [CurrencyTypeId],
			p.PaymentMethodId AS [PaymentMethodID],
			CASE WHEN DueTypeId=1 THEN OP.Value
			ELSE old.Amount 
			END AS [Amount],
			p.PaymentId AS [OrderPaymentID],
			p.Created AS [CreatedDate],
			convert(nvarchar(100), po.OrderGuid) as [ReferenceID],
			--p.ReferenceId AS [ReferenceID], 
			ol.CreatedBy  AS [CreatedBy],
			old.DueTypeId
		FROM  LB_OrderLineDue old 
			INNER JOIN ucommerce_orderline ol ON old.orderlineid = ol.orderlineid 			
			INNER JOIN uCommerce_PurchaseOrder po ON po.OrderId = ol.OrderId
			INNER JOIN ucommerce_payment p ON p.orderid = po.orderid
			LEFT JOIN uCommerce_OrderProperty OP ON ol.OrderLineId = OP.OrderLineId 
		WHERE
			PO.orderid = @OrderId
			AND OP.[Key]='ProviderAmountPaidCheckOut'
			

		UPDATE oldue SET oldue.PaymentMethodId = olp.PaymentMethodID
		FROM LB_orderlinedue oldue, LB_orderlinepayment olp
		WHERE oldue.orderlineid = olp.OrderLineID 
			AND oldue.OrderLineID in (Select orderlineid from ucommerce_orderline where orderid = @OrderId)
			AND oldue.OrderLinePaymentID is null

		--Orderline status date update
		declare @orderlineids table(orderlineid int, isupdated bit)
		insert into @orderlineids select orderlineid, 0 from uCommerce_OrderLine where orderid = @OrderId
		declare @currentorderlineid int
		while(select COUNT(1) from @orderlineids where isupdated = 0) > 0
		begin
			select top 1 @currentorderlineid = orderlineid from @orderlineids where isupdated = 0
			--call update status sp here
			exec [LB_AddBookingStatus] @currentorderlineid, null, ''
		update @orderlineids set isupdated = 1 where orderlineid = @currentorderlineid
		end

		--update entry for special request in reservation notes
		insert into LB_ReservationNotes
			select 3 as NoteTypeId, rr.OrderLineId, rr.SpecialRequest, getdate() as CreatedDate, '' as CreatedBy from LB_RoomReservation rr
			inner join ucommerce_orderline ol on ol.orderlineid = rr.orderlineid
			inner join ucommerce_purchaseorder po on po.orderid = ol.OrderId 
			where po.orderid = @OrderId
			and ISNULL(rr.specialrequest,'') <> '' 
			and not exists (select 1 from LB_ReservationNotes where OrderLineId = rr.OrderLineId and notetypeid = 3)
	END
END
