﻿CREATE TABLE [dbo].[uCommerce_ReservationNotes]
(
	[NotesId] INT NOT NULL PRIMARY KEY IDENTITY, 
    [NoteTypeId] INT NOT NULL, 
	[OrderLineId] INT NOT NULL, 
    [Note] NVARCHAR(MAX) NOT NULL, 
    [CreatedDate] DATETIME NOT NULL DEFAULT GETDATE(),
	[CreatedBy] NVARCHAR(50)
    CONSTRAINT [FK_uCommerce_ReservationNotes_NoteTypes] FOREIGN KEY ([NoteTypeId]) REFERENCES [uCommerce_NoteTypes]([NoteTypeId]),
	CONSTRAINT [FK_uCommerce_ReservationNotes_OrderLine] FOREIGN KEY ([OrderLineId]) REFERENCES [uCommerce_OrderLine]([OrderLineId])
)
