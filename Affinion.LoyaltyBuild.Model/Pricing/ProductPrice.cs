﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Model.Pricing
{
    /// <summary>
    /// Day level pricing
    /// </summary>
    public class ProductPrice : Price
    {
        /// <summary>
        /// Sku
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Sku
        /// </summary>
        public string Sku { get; set; }

        /// <summary>
        /// Variant Sku
        /// </summary>
        public string VariantSku { get; set; }
        
        /// <summary>
        /// Date of price
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Related Prices
        /// </summary>
        public IList<ProductPrice> RelatedPrice { get; set; }
    }
}
