﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model
{
    public class OracleInvoiceData
    {
        /*AP Report Columns*/
        public long InvoiceID { get; set; }
        public DateTime InvoiceDate { get; set; }
        public long VendorNumber { get; set; }
        public string VendorSiteCode { get; set; }
        public string CurrencyCode { get; set; }
        public decimal Amount { get; set; }
        public string PaymentTerms { get; set; }
        public int LineNumber { get; set; }
        public string TaxCode { get; set; }
        public string Description { get; set; }
        public string ProviderID { get; set; }

        /*AR Report Columns*/
        public int CustomerId { get; set; }
        public string CustomerAddressCode { get; set; }
        public int SourceInvoiceLineId { get; set; }
        public string RecepitTerms { get; set; }

        public string ClientID { get; set; }
    }
}
