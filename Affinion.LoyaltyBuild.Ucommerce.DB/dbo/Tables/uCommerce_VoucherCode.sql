﻿CREATE TABLE [dbo].[uCommerce_VoucherCode] (
    [VoucherCodeId] INT            IDENTITY (1, 1) NOT NULL,
    [TargetId]      INT            NOT NULL,
    [NumberUsed]    INT            CONSTRAINT [DF_uCommerce_VoucherCode_NumberUsed] DEFAULT ((0)) NOT NULL,
    [MaxUses]       INT            NOT NULL,
    [Code]          NVARCHAR (512) NOT NULL,
    CONSTRAINT [PK_uCommerce_VoucherCode_1] PRIMARY KEY CLUSTERED ([VoucherCodeId] ASC),
    CONSTRAINT [FK_uCommerce_VoucherCode_uCommerce_VoucherCode] FOREIGN KEY ([TargetId]) REFERENCES [dbo].[uCommerce_VoucherTarget] ([VoucherTargetId])
);

