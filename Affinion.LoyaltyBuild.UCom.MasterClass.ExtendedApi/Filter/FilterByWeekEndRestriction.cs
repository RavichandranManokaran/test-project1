﻿using Affinion.LoyaltyBuild.UCom.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.Search.Facets;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi.Filter
{
    public class FilterByWeekEndRestriction : IHotelFilter
    {
        public bool IncludeHotel(UCommerce.Documents.Product product, SearchCriteria criteria)
        {
            return true;
        }

        public UCommerce.Search.Facets.Facet FilterRecordFacet(SearchCriteria criteria)
        {
            if (criteria.Night == 1 && (criteria.ArrivalDate.DayOfWeek == DayOfWeek.Saturday || criteria.ArrivalDate.DayOfWeek == DayOfWeek.Sunday))
            {
                var facet = new Facet();
                facet.FacetValues = new List<FacetValue>();
                facet.Name = ProductDefinationConstant.WeekendSerchRestriction;
                facet.FacetValues.Add(new FacetValue() { Value = "False" });
                return facet;
            }
            return null;
        }
    }
}
