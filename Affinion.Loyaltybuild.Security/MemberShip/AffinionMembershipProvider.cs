﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           AffinionMembershipProvider.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Security
/// Description:           SqlMembershipProvider override for AffinionMembershipProvider.
/// </summary>

#region Using Directives
using Affinion.LoyaltyBuild.Common.Exceptions;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using System;
using System.Web.Security;
using System.Linq;
using Affinion.Loyaltybuild.Security.MemberShip;
using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.Audit;
#endregion

namespace Affinion.LoyaltyBuild.Security.MemberShip
{
    public class AffinionMembershipProvider : SqlMembershipProvider
    {

        Helper membershipHelper = new Helper();

        public override bool ValidateUser(string username, string password)
        {
            //unlock user
            HandleUnlockAccount(username);

            var isValid = base.ValidateUser(username, password);
            var role = Roles.GetRolesForUser(username).FirstOrDefault();
            if (role != null)
            {
                switch (role.ToLower().Trim())
                {
                    case Constants.SupplierPortal.hotelRole:
                    case Constants.SupplierPortal.hotelSupervisor:
                    case Constants.SupplierPortal.grpHotelRole:
                    case Constants.SupplierPortal.lbAdminRole:
                    case Constants.SupplierPortal.superUser:
                    case Constants.SupplierPortal.accountManagement:
                    case Constants.SupplierPortal.marketing:
                    case Constants.SupplierPortal.finance:
                    case Constants.SupplierPortal.IT:
                        isValid = RoleHelper.ValidateSupplierPortalRole(username, role, isValid);
                        break;
                    default:
                        break;
                }
            }

            //log user
            LogUser(username, isValid);

            return isValid;
        }

        /// <summary>
        /// Membership provider ChangePassword function override 
        /// </summary>
        /// <param name="username">user name</param>
        /// <param name="oldPassword">old password</param>
        /// <param name="newPassword">new password</param>
        /// <returns>Return true if success false otherwise</returns>
        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            try
            {
                MembershipUser domainUser = Membership.GetUser(username);
                if (domainUser != null && domainUser.IsLockedOut)
                {
                    domainUser.UnlockUser();
                }
                /// Check if newpassword and oldpassword are same
                if (membershipHelper.IsInPasswordHistory(username, newPassword))
                {
                    return false;
                    //Throw new argument ArgumentException
                   // throw new PasswordInHistoryException("New password exist in password history records.");

                }
                else if (base.ChangePassword(username, oldPassword, newPassword))
                {
                    /// Record the changed password data
                    if (membershipHelper.RecordHistoryPassword(username, newPassword))
                    {
                        return true;
                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.Security, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Membership provider ResetPassword function override 
        /// </summary>
        /// <param name="username">user name</param>
        /// <returns>Returns new password</returns>
        public override string ResetPassword(string username, string passwordAnswer=null)
        {
            try
            {
                MembershipUser domainUser = Membership.GetUser(username);
                if (domainUser != null && domainUser.IsLockedOut)
                {
                    domainUser.UnlockUser();
                }

                return base.ResetPassword(username, passwordAnswer);
            }
            catch (Exception ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.Security, ex, this);
                throw;
            }
          

        } 
       
        /// <summary>
        /// Membership provider CreateUser function override 
        /// </summary>
        /// <param name="username">username</param>
        /// <param name="password">password</param>
        /// <param name="email">email</param>
        /// <param name="passwordQuestion">password Question</param>
        /// <param name="passwordAnswer">password Answer</param>
        /// <param name="isApproved">isApproved</param>
        /// <param name="providerUserKey">provider User Key</param>
        /// <param name="status">status</param>
        /// <returns>MembershipUser</returns>
        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            try
            {
                /// call CreateUser base class method 
                MembershipUser user = base.CreateUser(username, password, email, passwordQuestion, passwordAnswer, isApproved, providerUserKey, out status);

                if (user != null)
                {
                    membershipHelper.RecordHistoryPassword(username, password);
                }
                return user;
            }
            catch (Exception ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.Security, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Membership provider DeleteUser function override 
        /// </summary>
        /// <param name="username">username</param>
        /// <param name="deleteAllRelatedData">delete All Related Data</param>
        /// <returns>Return true if success false otherwise</returns>
        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            try
            {
                /// call DeleteUser base class method 
                if (base.DeleteUser(username, deleteAllRelatedData))
                {
                    return membershipHelper.DeleteUserPasswordHistory(username);

                }
                return false;
            }
            catch (Exception ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.Security, ex, this);
                throw;
            }
        }


        /// <summary>
        /// Unlock the locked out user account.
        /// </summary>
        /// <param name="args">Arguments</param>
        private void HandleUnlockAccount(string username)
        {
            var currentUser = Membership.GetUser(username);

            if (currentUser != null)
            {
                if (currentUser.IsLockedOut)
                {
                    DateTime lastLockout = currentUser.LastLockoutDate.ToLocalTime();
                    DateTime unlockDate = lastLockout.Add(new TimeSpan(0, 10, 0));
                    if (DateTime.Now > unlockDate)
                    {
                        currentUser.UnlockUser();
                    }
                }
            }
        }

        /// <summary>
        /// Logs the user logon attempts.
        /// </summary>
        /// <param name="args">Arguments</param>
        private void LogUser(string usermame, bool isValid)
        {
            AuditData auditRecord = new AuditData()
            {
                AuditName = AuditNames.Login,
                Message = isValid ? usermame + " logged in successfully" : usermame + " login attempt failed",
                ObjectType = AuditObjectType.CustomTableInCore,
                Operation = AuditOperations.Read,
                Section = AuditSection.AdminPortalSitecore,
                Date = DateTime.Today,
                Time = DateTime.Now,
                Username = usermame,
                Remarks = "Initated from Affinion Membership Provider class"
            };

            auditRecord.AddAdditionalData("Function", "LogUser");
            auditRecord.AddAdditionalData("Module", "MembershipProvider");

            // auditRecord.AddAdditionalData("NumberOfAttempts", "");

            AuditTrail.Instance.Log(auditRecord);

            //AuditSearchData searchData = new AuditSearchData()
            //{
            //    AuditName = AuditNames.Login
            //};

            //var records = AuditTrail.Instance.Read(searchData);
            //var test = "";
            // membershipHelper.AddAuditLog(usermame, isValid);
        }



    }
}
