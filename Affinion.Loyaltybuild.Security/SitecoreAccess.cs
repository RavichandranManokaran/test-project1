﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SitecoreAccess.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Security
/// Description:           Security class for Sitecore Access
/// </summary>

#region Using Statemants
using System;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Security;
using Sitecore.Security.AccessControl;
using Sitecore.Security.Accounts;
using System.Collections.Generic;
using System.Web.Security;
using Sitecore.Security.Authentication;
using System.Security.Authentication;
using Affinion.LoyaltyBuild.Common.Exceptions;
using Sitecore.Web;
using Sitecore.Links;
#endregion

namespace Affinion.Loyaltybuild.Security
{
    public static class SitecoreAccess
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteName"></param>
        public static void CreateSiteRoles(string siteName, List<string> roleName)
        {
            try
            {
                foreach (var role in roleName)
                {
                    CreateRole("sitecore", siteName + "_" + role);
                    AddRoleToRole(GetFullRoleName("sitecore", role, siteName), GetFullRoleName("sitecore", role));
                }
            }
            catch (Exception ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.Security, ex, typeof(SitecoreAccess));
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="domain"></param>
        /// <param name="roleName"></param>
        private static void CreateRole(string domain, string roleName)
        {
            try
            {
                string fullName = GetFullRoleName(domain, roleName);

                if (!Sitecore.Security.Accounts.Role.Exists(fullName))
                {
                    System.Web.Security.Roles.CreateRole(fullName);
                }
            }
            catch (Exception ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.Security, ex, typeof(SitecoreAccess));
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberRole"></param>
        /// <param name="parentRole"></param>
        private static void AddRoleToRole(string memberRole, string parentRole)
        {
            try
            {
                if (RolesInRolesManager.RolesInRolesSupported &&
                        !RolesInRolesManager.IsRoleInRole(Role.FromName(memberRole), Role.FromName(parentRole), false))
                {
                    RolesInRolesManager.AddRoleToRole(Role.FromName(memberRole), Role.FromName(parentRole));
                }
            }
            catch (Exception ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.Security, ex, typeof(SitecoreAccess));
                throw;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="domain"></param>
        /// <param name="role"></param>
        /// <param name="siteName"></param>
        /// <returns></returns>
        private static string GetFullRoleName(string domain, string role, string siteName)
        {
            try
            {
                if (!string.IsNullOrEmpty(domain) && !string.IsNullOrEmpty(role))
                {
                    if (!string.IsNullOrEmpty(siteName))
                    {

                        return domain + "\\" + siteName + "_" + role;
                    }

                    return domain + "\\" + role;

                }
            }
            catch (Exception ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.Security, ex, typeof(SitecoreAccess));
                throw;
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="domain"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        private static string GetFullRoleName(string domain, string role)
        {

            return GetFullRoleName(domain, role, null);
        }

        /// <summary>
        /// Set Right Asses to an Item
        /// </summary>
        /// <param name="item">Item</param>
        /// <param name="account">Account</param>
        /// <param name="right">Right</param>
        /// <param name="rightState">Right State</param>
        /// <param name="propagationType">Property Type</param>
        public static void SetRight(Item item, Account account, AccessRight right,
            AccessPermission rightState, PropagationType propagationType)
        {
            try
            {
                AccessRuleCollection accessRules = item.Security.GetAccessRules();

                if (propagationType == PropagationType.Any)
                {
                    accessRules.Helper.RemoveExactMatches(account, right);
                }
                else
                {
                    accessRules.Helper.RemoveExactMatches(account, right, propagationType);
                }

                if (rightState != AccessPermission.NotSet)
                {
                    if (propagationType == PropagationType.Any)
                    {
                        accessRules.Helper.AddAccessPermission(account, right, PropagationType.Entity, rightState);
                        accessRules.Helper.AddAccessPermission(account, right, PropagationType.Descendants, rightState);
                    }
                    else
                    {
                        accessRules.Helper.AddAccessPermission(account, right, propagationType, rightState);
                    }
                }
                item.Security.SetAccessRules(accessRules);
            }
            catch (Exception ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.Security, ex, typeof(SitecoreAccess));
                throw;
            }
        }

        /// <summary>
        /// Set Access Right to an Item
        /// </summary>
        /// <param name="strDatabase">Database Name</param>
        /// <param name="strItem">Item Name</param>
        /// <param name="strAccount">Account Name</param>
        /// <param name="strRight">Right Access</param>
        /// <param name="rightState">Permisssion</param>
        /// <param name="propagationType">Property Type</param>
        public static void SetRight(string strDatabase, string strItem, string strAccount,
            string strRight, AccessPermission rightState, PropagationType propagationType)
        {

            try
            {
                Database db = Sitecore.Configuration.Factory.GetDatabase(strDatabase);
                Item item = db.GetItem(strItem);
                AccountType accountType = AccountType.User;

                if (SecurityUtility.IsRole(strAccount))
                {
                    accountType = AccountType.Role;
                }

                Account account = Account.FromName(strAccount, accountType);
                AccessRight right = AccessRight.FromName(strRight);
                SetRight(item, account, right, rightState, propagationType);
            }
            catch (Exception ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.Security, ex, typeof(SitecoreAccess));
                throw;
            }
        }

        /// <summary>
        /// Check given user has given permission 
        /// for the given item
        /// </summary>
        /// <param name="user">User</param>
        /// <param name="right">Permission type</param>
        /// <param name="path">Item path</param>
        /// <param name="database">Database</param>
        /// <returns></returns>
        public static bool IsAllowed(User user, AccessRight right, string path, string database)
        {
            try
            {
                if (user != null && right != null && !string.IsNullOrEmpty(path) && !string.IsNullOrEmpty(database))
                {
                    Database db = Sitecore.Configuration.Factory.GetDatabase(database);

                    if (db != null)
                    {
                        Item item = db.GetItem(path);
                        if (item != null)
                        {
                            return AuthorizationManager.IsAllowed(item, right, user);
                        }
                    }
                }
                else
                {
                    throw new ArgumentNullException();
                }

                return false;
            }
            catch (Exception ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.Security, ex, typeof(SitecoreAccess));
                throw;
            }

        }

        /// <summary>
        /// Update password
        /// </summary>
        /// <param name="userName">user name</param>
        /// <param name="oldPassword">old password</param>
        /// <param name="newPassword">new password</param>
        /// <returns></returns>
        public static bool UpdatePassword(string userName, string oldPassword, string newPassword)
        {
            try
            {
                AuthenticationHelper authHelper = new AuthenticationHelper(AuthenticationManager.Provider);

                //check to see if the existing password is correct
                if (!authHelper.ValidateUser(userName, oldPassword))
                {
                    throw new AuthenticationException("Incorrect Current password.");

                }
                else
                {
                    Affinion.LoyaltyBuild.Security.MemberShip.Helper membershipHelper = new Affinion.LoyaltyBuild.Security.MemberShip.Helper();
                    //get the current user
                    MembershipUser user = Membership.GetUser(userName);
                    if (membershipHelper.IsInPasswordHistory(userName, newPassword))
                    {
                       throw new PasswordInHistoryException("New password exist in password history records.");
                        
                    }
                    else if (user.ChangePassword(oldPassword, newPassword))
                    {
                        return true;
                    }
                    else
                    {
                        throw new AuthenticationException();

                    }
                }
            }
            catch (PasswordInHistoryException ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, typeof(SitecoreAccess));
                throw;
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, typeof(SitecoreAccess));
                throw;
            }

        }

        /// <summary>
        /// User Logoff
        /// </summary>
        public static void LogOff()
        {
            try
            {
                string redirectUrl =string.Empty;
                AuthenticationManager.Logout();
                Item itm = Sitecore.Context.Database.GetItem(Sitecore.Context.Site.StartPath);
                if(itm!=null)
                    redirectUrl = LinkManager.GetItemUrl(itm);
                WebUtil.Redirect(redirectUrl);
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, typeof(SitecoreAccess));
                throw;
            }
        }

        /// <summary>
        ///  Extranet user Logon
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="persistent"></param>
        /// <returns></returns>
        public static bool LogOn(string userName, string password, bool persistent)
        {
            try
            {
                return AuthenticationManager.Login(userName, password, persistent);
            }
            catch (Exception ex)
            {

                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, typeof(SitecoreAccess));
                throw;
            }
        }

        /// <summary>
        /// Check if password has expiered of the given user
        /// </summary>
        /// <param name="user">Membership user</param>
        /// <param name="timeSpanToExpirePassword">Password expire timespan</param>
        /// <returns>True if  password expiered false otherwise</returns>     
        public static bool HasUserPasswordExpired(MembershipUser user, TimeSpan timeSpanToExpirePassword)
        {
            try
            {
                return user.LastPasswordChangedDate.ToLocalTime().Add(timeSpanToExpirePassword) <= DateTime.Now;
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, typeof(SitecoreAccess));
                throw;
            }
        }

        /// <summary>
        /// Get the domain user
        /// </summary>
        /// <param name="domainName">domain name</param>
        /// <param name="username">user name</param>
        /// <returns>Membership user</returns>
        public static MembershipUser GetDomainUser(string domainName, string username)
        {
            try
            {
                string domainUser = domainName + @"\" + username;

                return Membership.GetUser(domainUser);
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, typeof(SitecoreAccess));
                throw;
            }
        }


    }
}
