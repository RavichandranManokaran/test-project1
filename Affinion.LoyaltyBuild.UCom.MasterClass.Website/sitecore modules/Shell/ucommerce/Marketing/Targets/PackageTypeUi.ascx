﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PackageTypeUi.ascx.cs" Inherits="Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.Ucommerce.Marketing.Targets.PackageTypeUi" %>
<table cellpadding="0" cellspacing="0" style="width: 100%;">
    <tr>
        <td>
            <asp:PlaceHolder runat="server" ID="NonEditModePlaceHolder">
                <asp:Literal runat="server" ID="ReadOnlyMessage"></asp:Literal>
            </asp:PlaceHolder>

            <asp:PlaceHolder runat="server" ID="EditModePlaceHolder" Visible="false">
                <%--<asp:TextBox runat="server" ID="RoomTypeText" />--%>
                <asp:DropDownList runat="server" ID="drpPackageType" />
                <asp:RequiredFieldValidator ID="RequiredValidator" runat="server"
                    ControlToValidate="drpPackageType" Display="Dynamic" />&nbsp;
			    <asp:Localize ID="Localize1" runat="server" meta:resourcekey="OnLabel" /><br />
            </asp:PlaceHolder>
        </td>
        <td style="width: 50px; text-align: right; vertical-align: top;">
            <asp:ImageButton ID="EditButton" runat="server" ImageUrl="../../Images/pencil.png" meta:resourcekey="Edit" OnClick="EditButton_Click" />
            <asp:ImageButton ID="SaveButton" runat="server" ImageUrl="../../Images/save.gif" meta:resourcekey="Save" Visible="false" OnClick="SaveButton_Click" ValidationGroup="MinOrderAmountGroup" />
            <asp:ImageButton ID="DeleteButton" runat="server" ImageUrl="../../Images/ui/cross.png" meta:resourcekey="Delete" OnClick="DeleteButton_Click" /><br />
        </td>
    </tr>
</table>

