﻿/****** Object:  StoredProcedure [dbo].[LB_GetBookingDetail]    Script Date: 2/12/2016 11:40:56 AM ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LB_GetBookingDetail]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].LB_GetBookingDetail
GO

/****** Object:  StoredProcedure [dbo].[LB_GetBookingDetail]    Script Date: 2/12/2016 11:40:56 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[LB_GetBookingDetail]
(
	@OrderLineID int
) 
AS 
BEGIN
	SELECT 
		PO.OrderId,
		OL.OrderLineId,
		RR.OrderReference [BookingReferance],
		PO.CustomerID,
		CAT.ClientId,
		PO.CurrencyId,
		RR.CheckIndate [CheckIndate] ,
		RR.CheckOutdate [CheckOutdate],
		OL.CreatedOn [Reservation], 
		(SELECT MAX(CreatedDate) FROM uCommerce_OrderLineStatus WHERE OrderLineId = OL.OrderLineId AND OrderStatusId = 6) [Confirmation],
		RR.CreatedBy [CreateBy],
		OLS.CreatedDate [Statusdate],
		OLS.CreatedBY [LastUpdate],
		DATEDIFF(DAY, RR.CheckInDate,RR.checkOutdate) as [NoOfNights], 
		OA.FirstName,
		OA.LastName,
		OA.AddressName [Address],
		OA.PhoneNumber [Phonenumber],
		OA.EmailAddress [Email],
		1 [No.ofRooms],
		RR.NoOfAdult [NoOfAdult],
		RR.NoOfChild [NoOfChild],
		RR.[NoOfGuest] [NoOfGuest],
		RR.GuestName [GuestName],
		(select STUFF((SELECT ', ' + CAST(RoomReservationAgeInfo.Age AS VARCHAR(10))
			FROM LB_RoomReservationAgeInfo RoomReservationAgeInfo
			WHERE RoomReservationAgeInfo.OrderLineId=OL.OrderLineId 
              FOR XML PATH('')), 1, 2, '')) [ChildAge],
		OL.Sku [Sku],
		OL.Total [Price],
		OL.Discount,
		P.[Name] [OccupancyType],
		CA.[Guid] [ProviderId],
		OL.Total [TotalPrice],
		RR.BookingThrough [BookingThrough],
		RR.IsCancelled,
		UCUS.LanguageName
		FROM 
		[dbo].uCommerce_OrderLine OL 
		INNER JOIN [dbo].uCommerce_PurchaseOrder PO ON PO.OrderID =OL.OrderId
		INNER JOIN [dbo].LB_RoomReservation RR on RR.OrderLineId = OL.OrderLineId 
		INNER JOIN [dbo].[uCommerce_Product] P ON OL.Sku = P.Sku 
		INNER JOIN [dbo].[uCommerce_CategoryProductRelation] CPR ON P.ProductId = CPR.ProductId
		INNER JOIN [dbo].[uCommerce_Category] CA ON CA.CategoryId = CPR.CategoryId 
		LEFT JOIN [dbo].LB_CustomerAttribute CAT ON PO.CustomerId =CAT.CustomerId
		LEFT JOIN [dbo].LB_OrderAddress OA ON OA.OrderId = OL.OrderId 
		LEFT JOIN [dbo].uCommerce_OrderLineStatus OLS on RR.OrderLineId = OLS.OrderLineId AND RR.[OrderLineStatusID] = OLS.[OrderLineStatusID]
	    LEFT JOIN [dbo].uCommerce_Customer UCUS on PO.CustomerID = UCUS.CustomerId
		WHERE 
		OL.OrderLineID= @OrderLineID
	
		--Subrate Items
		SELECT 
				OrderLineSubrateId,
				OrderLineID,
				OLS.[SubrateItemCode],
				Amount,
				Discount,
				DisplayName
				FROM LB_OrderLineSubrates OLS
				LEFT JOIN LB_SubrateItem SI on OLS.SubrateItemCode = SI.SubrateItemCode
		WHERE 
			OrderLineID = @OrderLineID
		--Payment History Total
		SELECT SUM([Total]) [Total], [Currency] FROM
		(SELECT SUM(OLD.Amount) [Total],
				OLD.CurrencyTypeId [Currency]
		FROM uCommerce_OrderLine  OL
			INNER JOIN LB_OrderLineDue OLD ON OLD.OrderLineID = OL.OrderLineId 
		WHERE
			ISNULL(OLD.IsDeleted,0) = 0 AND
			OLD.OrderLineID IN (SELECT OrderLineID FROM [dbo].[LB_FnRelatedOrderLines](@OrderLineId))
		GROUP BY 
			OLD.CurrencyTypeId
		UNION ALL
		SELECT -SUM(OLP.Amount) [Total],
				OLP.CurrencyTypeId [Currency]
		FROM uCommerce_OrderLine  OL
			INNER JOIN LB_OrderLinePayment OLP ON OLP.OrderLineID = OL.OrderLineId 
		WHERE
			OLP.OrderLineID IN (SELECT OrderLineID FROM [dbo].[LB_FnRelatedOrderLines](@OrderLineId))
		GROUP BY 
			OLP.CurrencyTypeId) P
	GROUP BY [Currency]
	HAVING
		SUM([Total]) <> 0
END

