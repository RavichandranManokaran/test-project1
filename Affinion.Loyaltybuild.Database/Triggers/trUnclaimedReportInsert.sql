﻿CREATE TRIGGER [dbo].[trUnclaimedReportInsert] 
   ON  [dbo].[uCommerce_OrderlinePayment] 
   AFTER INSERT 
AS 
    BEGIN 
         
        INSERT INTO uCommerce_OrdelinePaymentRequest 
        ( 
            OrderLinePaymentID, 
            ActualProviderPaymentAmount 
        ) 
      
                SELECT OrderLinePaymentID, 
                        Amount 
                FROM 
                        INSERTED 
                WHERE 
                        DueTypeId = 1 
    END 
GO