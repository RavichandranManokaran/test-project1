﻿/****** Object:  StoredProcedure [dbo].[LB_CancelBooking]    Script Date: 2/12/2016 3:06:06 PM ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LB_CancelBooking]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[LB_CancelBooking]
GO


/****** Object:  StoredProcedure [dbo].[LB_CancelBooking]    Script Date: 2/12/2016 3:06:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LB_CancelBooking] 
	-- Add the parameters for the stored procedure here
	@Note NVARCHAR(MAX), 
	@CancelReason NVARCHAR(MAX),
	@OrderLineId INT,
	@CreatedBy VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--Check If already cancelled
	IF EXISTS (SELECT 1 FROM LB_RoomReservation WHERE isCancelled = 1 AND OrderLineId = @OrderlineId)
		THROW 50000,'ALREADY CANCELLED',1
		
    -- Insert statements for procedure here	 
	INSERT INTO LB_ReservationNotes 
		(
		NoteTypeId,
		OrderLineId,
		Note,
		CreatedBy,
		CreatedDate
		) 
	VALUES 
		(
		2,
		@OrderLineId,
		@Note,
		@CreatedBy,
		GETDATE()
		);
		
	INSERT INTO LB_ReservationNotes 
		(
		NoteTypeId,
		OrderLineId,
		Note,
		CreatedBy,
		CreatedDate
		) 
	VALUES  
		(
		1,
		@OrderLineId,
		@CancelReason,
		@CreatedBy,
		GETDATE()
		);
		
	UPDATE LB_RoomReservation SET isCancelled = 1 WHERE OrderLineId = @OrderlineId
	
END
GO

