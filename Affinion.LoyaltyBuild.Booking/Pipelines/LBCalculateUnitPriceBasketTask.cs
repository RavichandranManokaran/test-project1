﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.Pipelines;
using UCommerce.Pipelines.AddToBasket;
using UCommerce.Pipelines.UpdateLineItem;

namespace Affinion.LoyaltyBuild.Api.Booking.Pipelines
{
    /// <summary>
    /// calculate unit price task
    /// </summary>
    public class LBCalculateUnitPriceBasketTask : IPipelineTask<IPipelineArgs<AddToBasketRequest, AddToBasketResponse>>
    {
        /// <summary>
        /// Execute method
        /// </summary>
        /// <param name="subject"></param>
        /// <returns></returns>
        public PipelineExecutionResult Execute(IPipelineArgs<AddToBasketRequest, AddToBasketResponse> subject)
        {
            //do nothing here
            return PipelineExecutionResult.Success;
        }
    }
}
