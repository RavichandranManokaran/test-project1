﻿
namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.WebControls;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Order;
    using Affinion.LoyaltyBuild.Web.ClientPortal.Helper;
    using Sitecore.Data.Items;
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Email;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Api.Booking.Service;
    using Affinion.LoyaltyBuild.Model.Common;
    using System.Data;
    using Affinion.LoyaltyBuild.BedBanks.Troika;
    using System.Xml.Serialization;
    using System.IO;
    using Affinion.LoyaltyBuild.BedBanks.JacTravel.Message;
    using System.Text;
    using Affinion.LoyaltyBuild.BedBanks.Helper;
    using data = Affinion.LoyaltyBuild.Api.Booking.Data;
    using System.Web.UI;


    public partial class ClientPortalManageMyBooking : BaseSublayout
    {
        #region Template Field Names
        static string headingPreTextFieldName = "Heading Pretext";
        static string bookingReferenceNumberFieldName = "Booking Reference Number Text";
        static string phoneNumberFieldName = "Phone Number Text";
        static string checkinFieldName = "Check In Text";
        static string checkoutTextFieldName = "Check Out Text";
        static string priceFieldName = "Price Text";
        static string fromTextFieldName = "From Text";
        static string untilTextFieldName = "Until Text";
        static string roomTextFieldName = "Room Text";
        static string nightTextFieldName = "Night Text";

        static string printButtonFieldName = "Print Button Text";
        static string showMapFieldName = "Show Map Text";
        static string emailPropertyFieldName = "Email Property Text";
        static string viewPolicyFieldName = "View Policy Text";
        static string cancelBookingFieldName = "Cancel Booking Text";
        static string priceDetailsFieldName = "Price Details Text";

        static string modalPriceDetailsHeadingFieldName = "Modal Price Details Heading";
        static string modalTotalPriceFieldName = "Total Price Text";
        static string modalTaxDetailsFieldName = "Tax Details";
        static string priceBreakFieldName = "FullPriceBreakText";
        static string payableAtAccomodationFieldName = "Payable At Accommodation Text";
        static string modalPayAtAccoInfoFieldName = "Pay at Accommodation Info";
        static string modalCloseButtonFieldName = "Modal Close Button Text";
        static string cancelSuccess = "SuccessMessage";
        static string alreadyCancelled = "AlreadyCancelled";
        static string invalidOrder = "InvalidOrder";
        static string cancelFailed = "FailureMsg";
        static string termsAndConditionsBodyContent = "Body";
        static string termsAndConditionsBodyTitle = "Title";
        static string termsAndConditionsTitle = "TermsCondtions";
        static string no = "No";
        static string yes = "Yes";
        static string cancelTitle = "CancelTitle";

        private IBookingService _bookingService;

        static Item currentItem = Sitecore.Context.Item;

        #endregion Template Field Names

        int OrderLineId = 0;
        #region Properties

        public string GetHostUrl { get; set; }
        protected string latitude { get; set; }
        protected string lognitude { get; set; }
        protected string hotelName { get; set; }
        protected BaseBooking Booking { get; set; }
        private IOrderService orderSvc;
        int bbBookingReference { get; set; }
        public MailService mail;
        string bbProviderId = string.Empty;
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            this.BindSitecoreTexts();
            string orderLineIdQs = Request.QueryString["olid"];
            int.TryParse(orderLineIdQs, out OrderLineId);
            GetHostUrl = Request.Url.ToString();
            if (GetHostUrl.Contains("https:")) 
            {
                GetHostUrl = GetHostUrl.Replace("https://", "http://");
                Response.Redirect(GetHostUrl, true); ;
            }
            if (!IsPostBack)
            {
                if (!string.IsNullOrWhiteSpace(orderLineIdQs) && OrderLineId > 0)
                {
                    ManageBookingDetailsOrderService();
                    PopulateStaticText();
                }
                else
                {
                    //Display Error Message
                    SublayoutWrapper.Visible = false;
                    NoDataError.Visible = true;
                }
            }
            Item termsAndConditionsItem = this.GetDataSource();
            SetSitecoreTextInLiteral(termsAndConditionsItem, termsConditionsTitle, termsAndConditionsBodyTitle);
            SetSitecoreTextInLiteral(termsAndConditionsItem, termsConditionsContent, termsAndConditionsBodyContent);
        }


        private void ManageBookingDetailsOrderService()
        {
            IOrderService orderSvc = ContainerFactory.Instance.GetInstance<IOrderService>();
            Booking bookingInfo = orderSvc.GetBooking(OrderLineId);
            JtPropertyDetailsResponse propertyDetails = null;
            if (bookingInfo != null)
            {
                bbProviderId = "c5ee30ab-067f-4169-96ee-3017009ebec3";
                DateTime checkinDate = bookingInfo.CheckInDate;
                litCheckinDayOfWeek.Text = checkinDate.DayOfWeek.ToString();
                litCheckinDay.Text = checkinDate.Day.ToString();
                System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
                var monthName = new System.Globalization.DateTimeFormatInfo().GetAbbreviatedMonthName(checkinDate.Month);
                litCheckinMonthYear.Text = string.Format("{0} {1}", monthName.ToUpper(), checkinDate.Year);
                litCheckinTime.Text = checkinDate.ToString("hh:mm");

                DateTime checkOutDate = bookingInfo.CheckOutDate;
                litCheckoutDayOfWeek.Text = checkOutDate.DayOfWeek.ToString();
                litCheckoutDay.Text = checkOutDate.Day.ToString();
                var checkOutMonthName = new System.Globalization.DateTimeFormatInfo().GetAbbreviatedMonthName(checkOutDate.Month);
                litCheckoutMonthYear.Text = string.Format("{0} {1}", checkOutMonthName.ToUpper(), checkOutDate.Year);
                litCheckoutTime.Text = checkOutDate.ToString("hh:mm");

                litNumberOfNightsCount.Text = bookingInfo.NumberOfNights.ToString();

                litCurrencySymbol.Text = litCurrencySymbolModal.Text = litCurrencySymbolRoomPrice.Text = bookingInfo.CurrencyText;
                litPrice.Text = bookingInfo.Price.ToString("0.00");

                litPriceBreak.Text = litCurrencySymbolRoomPrice.Text + " " + bookingInfo.Deposit.ToString("0.00");

                litTotalRoomPrice.Text = " " + bookingInfo.Price.ToString("0.00");
                litPriceModal.Text = " " + bookingInfo.PayableAtHotel.ToString("0.00");
                litBookingReferenceNumber.Text = bookingInfo.BookingReference;

                string latLong = string.Empty;
                if (bookingInfo.AccomodationInfo != null)
                {
                    latLong = bookingInfo.AccomodationInfo.GeoCoordinates;
                    litRoomCount.Text = bookingInfo.NumberOfRooms.ToString();

                    HotelPageLink.Text = bookingInfo.AccomodationInfo.Title;
                    lblHotelName.Text = hotelName = bookingInfo.AccomodationInfo.Title;
                    //litHotelNameModal.Text = bookingInfo.AccomodationInfo.Title;
                    HotelPageLink.NavigateUrl = "/hotelinformation?id=" + bookingInfo.AccomodationInfo.HotelItemId + "&CheckInDate=" + DateTime.Now.ToString("dd/MM/yyyy") + "&CheckOutDate=" + DateTime.Now.AddDays(1).ToString("dd/MM/yyyy") + "&NoOfNights=" + bookingInfo.NumberOfNights;
                    AddressLine1.Text = bookingInfo.AccomodationInfo.AddressLine1;
                    AddressLine2.Text = bookingInfo.AccomodationInfo.AddressLine2;
                    AddressLine3.Text = bookingInfo.AccomodationInfo.AddressLine3;
                    AddressLine4.Text = bookingInfo.AccomodationInfo.AddressLine4;
                    AddressTown.Text = bookingInfo.AccomodationInfo.Town;
                    AddressCountry.Text = bookingInfo.AccomodationInfo.CountryName;
                    hypEmailProperty.NavigateUrl = string.Format("mailto:{0}", bookingInfo.AccomodationInfo.EmailId);
                    litPhoneNumber.Text = bookingInfo.AccomodationInfo.Phone;

                    if (!string.IsNullOrWhiteSpace(bookingInfo.AccomodationInfo.ImageUrl))
                    {
                        imgHotelImage.ImageUrl = bookingInfo.AccomodationInfo.ImageUrl;
                    }
                    else
                    {
                        imgHotelImage.Visible = false;
                    }
                }
                else if (bookingInfo.ProviderId == bbProviderId)
                {
                    //Call the property details from the DB and
                    DataTable bookingData = TroikaDataController.GetBookingDetailsForCancellation(bookingInfo.OrderLineId);
                    if (bookingData != null && bookingData.Rows.Count > 0)
                    {
                        if (bookingData.Rows[0][0] != null)
                        {
                            string xml = bookingData.Rows[0][0].ToString();
                            propertyDetails = GetPropertyDetails(xml);
                            if (propertyDetails != null)
                            {
                                litRoomCount.Text = bookingInfo.NumberOfRooms.ToString();
                                HotelPageLink.Text = propertyDetails.PropertyName;
                                lblHotelName.Text = hotelName = propertyDetails.PropertyName;
                                //litHotelNameModal.Text = propertyDetails.PropertyName;
                                HotelPageLink.NavigateUrl = "/hotelinformation?PropertyId=" + propertyDetails.PropertyId + "&PropertyRefId=" + propertyDetails.PropertyReferenceId + "&CheckInDate=" + DateTime.Now.ToString("dd/MM/yyyy") + "&CheckOutDate=" + DateTime.Now.AddDays(1).ToString("dd/MM/yyyy") + "&NoOfNights=" + bookingInfo.NumberOfNights;
                                AddressLine1.Text = propertyDetails.Address1;
                                AddressLine2.Text = propertyDetails.Address2;
                                AddressLine3.Text = propertyDetails.Region;
                                AddressLine4.Text = string.Empty;
                                AddressTown.Text = propertyDetails.TownCity;
                                AddressCountry.Text = propertyDetails.Country;
                                hypEmailProperty.NavigateUrl = string.Format("mailto:{0}", string.Empty);
                                litPhoneNumber.Text = propertyDetails.Telephone;

                                if (!string.IsNullOrWhiteSpace(propertyDetails.MainImage))
                                {
                                    imgHotelImage.ImageUrl = propertyDetails.CMSBaseURL + propertyDetails.MainImage;
                                }
                                else
                                {
                                    imgHotelImage.Visible = false;
                                }
                            }
                        }
                    }

                }

                if (!string.IsNullOrWhiteSpace(latLong))
                {
                    List<string> latLongList = latLong.Split(',').ToList<string>();
                    latitude = latLongList[0];
                    lognitude = latLongList[1];
                }
                else if (propertyDetails != null)
                {
                    latitude = propertyDetails.Latitude;
                    lognitude = propertyDetails.Longitude;
                }
                else
                {
                    latitude = "0";
                    lognitude = "0";
                }

                if (bookingInfo.StatusID == 7)
                {
                    CancelButtonWrapper.Visible = false;
                }
                hypPrintButton.NavigateUrl = "/managebookingprint?olid=" + OrderLineId;
            }
        }

        private JtPropertyDetailsResponse GetPropertyDetails(string xml)
        {
            JtPropertyDetailsResponse propertyDetails = null;
            if (!xml.Equals(String.Empty))
            {
                //Serialize the result
                XmlRootAttribute xRoot = new XmlRootAttribute
                {
                    ElementName = "PropertyDetailsResponse",
                    IsNullable = true
                };

                XmlSerializer serializer = new XmlSerializer(typeof(JtPropertyDetailsResponse), xRoot);

                try
                {
                    MemoryStream memStream = new MemoryStream(Encoding.UTF8.GetBytes(xml));
                    propertyDetails = (JtPropertyDetailsResponse)serializer.Deserialize(memStream);
                }
                catch (Exception exc)
                {
                    throw new Exception(exc.Message);
                }
            }
            return propertyDetails;
        }

        private void PopulateStaticText()
        {
            
            SetSitecoreTextInLiteral(currentItem, litBookingHeadingPretext, headingPreTextFieldName);
            SetSitecoreTextInLiteral(currentItem, litBookingReferenceText, bookingReferenceNumberFieldName);
            SetSitecoreTextInLiteral(currentItem, litPhoneText, phoneNumberFieldName);
            SetSitecoreTextInLiteral(currentItem, litCheckinText, checkinFieldName);
            SetSitecoreTextInLiteral(currentItem, litCheckOutText, checkoutTextFieldName);
            SetSitecoreTextInLiteral(currentItem, litPriceText, priceFieldName);
            SetSitecoreTextInLiteral(currentItem, litFromText, fromTextFieldName);
            SetSitecoreTextInLiteral(currentItem, litUntilText, untilTextFieldName);
            SetSitecoreTextInLiteral(currentItem, litRoomText, roomTextFieldName);
            SetSitecoreTextInLiteral(currentItem, litNightText, nightTextFieldName);

            SetSitecoreTextInLiteral(currentItem, litPrintButtonText, printButtonFieldName);
            SetSitecoreTextInLiteral(currentItem, litMapDirection, showMapFieldName);
            SetSitecoreTextInLiteral(currentItem, litEmailText, emailPropertyFieldName);
            SetSitecoreTextInLiteral(currentItem, litViewPolicy, viewPolicyFieldName);
            SetSitecoreTextInLiteral(currentItem, litCancelButtonText, cancelBookingFieldName);
            SetSitecoreTextInLiteral(currentItem, litPriceDetailsButtonText, priceDetailsFieldName);

            //SetSitecoreTextInLiteral(currentItem, litTaxDetails, modalTaxDetailsFieldName);
            //SetSitecoreTextInLiteral(currentItem, litPayAtAccoInfo, modalPayAtAccoInfoFieldName);
            SetSitecoreTextInLiteral(currentItem, litModalCloseButton, modalCloseButtonFieldName);
            SetSitecoreTextInLiteral(currentItem, litPriceModalTitle, modalPriceDetailsHeadingFieldName);
            SetSitecoreTextInLiteral(currentItem, litTotalPriceModal, modalTotalPriceFieldName);
            SetSitecoreTextInLiteral(currentItem, litPriceBreakText, priceBreakFieldName);
            SetSitecoreTextInLiteral(currentItem, litPayableAtAccomodationText, payableAtAccomodationFieldName);

            SetSitecoreTextInLiteral(currentItem, litTermsConditionsTitle, termsAndConditionsTitle);
            SetSitecoreTextInLiteral(currentItem, litYes, yes);
            SetSitecoreTextInLiteral(currentItem, litNo, no);
            SetSitecoreTextInLiteral(currentItem, litCancelYourBooking, cancelTitle);

        }


        private void SetSitecoreTextInLiteral(Item currentItem, Literal literalControl, string fieldName)
        {
            literalControl.Text = (currentItem != null && currentItem.Fields[fieldName] != null && !string.IsNullOrWhiteSpace(currentItem.Fields[fieldName].Value)) ? currentItem.Fields[fieldName].Value : string.Empty;
        }

        protected void Button_YesClick(object sender, EventArgs e)
        {
            orderSvc = ContainerFactory.Instance.GetInstance<OrderService>();
            _bookingService = ContainerFactory.Instance.GetInstance<BookingService>();
            Booking myBooking = orderSvc.GetBooking(OrderLineId);
            mail = new MailService(myBooking.ClinetId);
            Booking = orderSvc.GetBaseBooking(OrderLineId);

            if (Validate())
            {
                ValidationResult cancelFlag = _bookingService.CancelBooking(new Api.Booking.Data.Basket.CancelRequest()
                {
                    OrderLineId = OrderLineId,
                    NoteToProvider = string.Empty,
                    CancelReason = string.Empty,
                    CancelledBy = SitecoreUserProfileHelper.GetItem(UserProfileKeys.CreatedBy)
                });

                if (cancelFlag.IsSuccess)
                {
                    CancelButtonWrapper.Visible = false;
                    successMessageWrapper.Visible = true;
                    if (myBooking.ProviderId.Equals(bbProviderId))
                    {
                        mail.GenerateEmail(MailTypes.BedBanksCustomerCancellaton, OrderLineId); //to check anything else need to be done
                    }
                    else
                    {
                        mail.GenerateEmail(MailTypes.CustomerCancellation, OrderLineId);
                        mail.GenerateEmail(MailTypes.ProviderCancellation, OrderLineId);
                    }
                }
                else
                {
                    errorMessageWrapper.Visible = true;
                }

                /*
                ValidationResponse result = orderSvc.CancelBooking(new CancelBookingInfo()
                {
                    OrderLineid = OrderLineId,
                    noteToProvider = string.Empty,
                    cancelReason = string.Empty,
                    CreatedBy = SitecoreUserProfileHelper.GetItem(UserProfileKeys.CreatedBy),
                    paymentDetails = null
                });
                if (result.IsSuccess)
                {
                    //SetMessage(false);
                    CancelButtonWrapper.Visible = false;
                    //SetSitecoreTextInLiteral(currentItem, cancelBookingStatus, cancelSuccess);
                    successMessageWrapper.Visible = true;
                    mail.GenerateEmail(MailTypes.CustomerCancellation, OrderLineId);
                    mail.GenerateEmail(MailTypes.ProviderCancellation, OrderLineId);
                }
                else
                {
                    //SetSitecoreTextInLiteral(currentItem, cancelBookingStatus, cancelFailed);
                    errorMessageWrapper.Visible = true;                    
                    //SetMessage();
                    
                }
                */
            }
        }




        /// <summary>
        ///  Check if it as a valid order item
        /// </summary>            
        /// <returns>Validation success/cancel flag</returns> 
        private bool Validate()
        {
            if (Booking == null
                || Booking.OrderLineId != OrderLineId
                )
            {
                //SetSitecoreTextInLiteral(currentItem, cancelBookingStatus, invalidOrder);
                invalidOrderMessageWrapper.Visible = true;
                // SetMessage();
                return false;
            }
            else if (Booking.StatusID == 7)
            {
                //SetSitecoreTextInLiteral(currentItem, cancelBookingStatus, alreadyCancelled);
                alreadyCancelledMessageWrapper.Visible = true;
                CancelButtonWrapper.Visible = false;
                //SetMessage();
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sets the alert message to the user
        /// </summary>            
        /// <returns></returns> 
        //private void SetMessage(bool isError = true)
        //{
        //    if (isError)
        //        divAlertMsg.Attributes.Add("class", "alert alert-danger");
        //    else
        //        divAlertMsg.Attributes.Add("class", "alert alert-success");
        //}

        protected void btnPrintConfirm_Click(object sender, EventArgs e)
        {
            Response.Redirect("/managebooking?olid=" + OrderLineId);
        }

        private void BindSitecoreTexts()
        {
            Item conditionalMessageItem = this.GetItemFromRenderingParameterPath("Conditional Messages Link");
            string bookingCancellationErrorMessageFieldName = "Booking Cancellation Error Message";
            string bookingCancelSuccessMessageFieldName = "Booking Cancel Success Message";
            string invalidOrderMessageFieldName = "Invalid Order Message";
            string bookingalreadyCancelledMessageFieldName = "Booking already Cancelled Message";

            SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, bookingCancellationErrorMessageFieldName, txtErrorText);
            SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, bookingCancelSuccessMessageFieldName, txtSuccessText);
            SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, invalidOrderMessageFieldName, txtInvalidOrderText);
            SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, bookingalreadyCancelledMessageFieldName, txtAlreadyCancelledText);

        }

        protected void btnCancel_ServerClick(object sender, EventArgs e)
        {
            orderSvc = ContainerFactory.Instance.GetInstance<OrderService>();
            Booking = orderSvc.GetBaseBooking(OrderLineId);
            _bookingService = ContainerFactory.Instance.GetInstance<BookingService>();
            data.PreCancelBookingResonse preCancel = _bookingService.ValidateCancelBooking(Booking.OrderLineId);
            if (preCancel.IsSuccess)
            {
                litCancellationCost.Text = preCancel.CancellationCost.ToString();

            }
            else
            {
                litCancellationCost.Text = "There is some error in Cancellation";
                errorMessageWrapper.Visible = true;
            }
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "open", "openPopup();", true);
        }
    }
}