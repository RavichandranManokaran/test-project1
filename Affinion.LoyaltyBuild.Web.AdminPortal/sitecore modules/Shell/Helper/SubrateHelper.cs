﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Subrate;
using Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;

namespace Affinion.LoyaltyBuild.AdminPortal.Helper
{
    public class SubrateHelper
    {

        public string ProviderId { get; set; }
        public string ClientId { get; set; }

        private ISubrate _subrateServices;
        Sitecore.Data.Database context = Sitecore.Context.Database;

        public SubrateHelper()
        {
            this._subrateServices = new SubrateService();
        }
      
        public SubrateHelper(string providerId, string clientId)
        {
            this.ProviderId = providerId;
            this.ClientId = clientId;
        }

        public List<SubrateTemplatesDetail> GetAllSubrateTemplate()
        {
            List<SubrateTemplatesDetail> subrateView = new List<SubrateTemplatesDetail>();
            subrateView = _subrateServices.GetAllSubRateTemplate();
            return subrateView;
        }

        public List<SubrateItemDetail> GetAllSubRateItems()
        {
            List<SubrateItemDetail> subrateView = new List<SubrateItemDetail>();
            subrateView = _subrateServices.GetAllSubRateItems();
            return subrateView;
        }

        public List<SubratesDetail> GetSubratesBySintelItem(string ItemID)
        {
            return _subrateServices.GetSubRateByItemId(ItemID);
        }

        public int SaveSubrates(List<SubratesDetail> objSubrates, string ItemID)
        {
            return _subrateServices.SaveSubrates(objSubrates, ItemID);
        }

        public int DeleteSubrate(string ItemID, string ClientID = "Admin")
        {
            return _subrateServices.DeleteSubrate(ItemID, ClientID);
        }

        public string SaveTemplate(SubrateTemplatesDetail objsrtemplate)
        {
            return _subrateServices.SaveTemplate(objsrtemplate);
        }
        public int DeleteTemplate(string SubrateTemplateID)
        {
            return _subrateServices.DeleteTemplate(SubrateTemplateID);
        }
    }
}