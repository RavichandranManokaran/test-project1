﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model.Payment;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/
namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    /// <summary>
    /// Base boooking info
    /// </summary>
    public class BaseBooking
    {
        /// <summary>
        /// Order id
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// Order line id
        /// </summary>
        public int OrderLineId { get; set; }

        /// <summary>
        /// Booking Reference no
        /// </summary>
        public string BookingReference { get; set; }

        /// <summary>
        /// IsCancelled flag
        /// </summary>
        public bool IsCancelled { get; set; }

        /// <summary>
        /// Check in / Arrival date
        /// </summary>
        public DateTime CheckInDate { get; set; }



        /// <summary>
        /// StatusID in OrderStatusAudit
        /// </summary>
        public int StatusID { get; set; }

    }

    /// <summary>
    /// Entity to hold order item information
    /// </summary>
    public class Booking : BaseBooking
    {
        public Booking()
        {
            this.Customer = new Customer();
            this.BookingNotes = new List<BookingNotes>();
            this.BookingSubrates = new List<BookingSubrate>();
            this.DueByCurrencies = new List<TotalDueItem>();
        }

        /// <summary>
        /// No of guest
        /// </summary>
        public int NoOfGuest { get; set; }

        /// <summary>
        /// Child age
        /// </summary>
        public string ChildAge { get; set; }

        /// <summary>
        /// Guest name
        /// </summary>
        public string GuestName { get; set; }

        /// <summary>
        /// client id
        /// </summary>
        public string ClinetId { get; set; }

        /// <summary>
        /// Currency of that booking
        /// </summary>
        public string CurrencyText { get; set; }

        /// <summary>
        /// Currency of that booking
        /// </summary>
        public int CurrencyId { get; set; }

        /// <summary>
        /// Number Of Rooms
        /// </summary>
        public int NumberOfRooms { get; set; }

        /// <summary>
        /// Check out / Departure date
        /// </summary>
        public DateTime CheckOutDate { get; set; }

        /// <summary>
        /// Reservation Date
        /// </summary>
        public DateTime ReservationDate { get; set; }

        /// <summary>
        /// Confirmation Date
        /// </summary>
        public DateTime Confirmation { get; set; }

        /// <summary>
        /// Status Date
        /// </summary>
        public DateTime StatusDate { get; set; }

        /// <summary>
        /// Unit price of the room
        /// </summary>
        public Decimal Price { get; set; }

        /// <summary>
        /// Deposit amount
        /// </summary>
        public Decimal Deposit { get; set; }

        ///<summary>
        /// Processing fee in transfering
        /// </summary>
        public decimal ProcessingFee { get; set; }

        /// <summary>
        /// Total Amount including all subrate charges
        /// </summary>
        public Decimal TotalAmount { get; set; }

        /// <summary>
        /// Amount payable at hotel
        /// </summary>
        public Decimal PayableAtHotel { get; set; }

        /// <summary>
        /// Provider guid
        /// </summary>
        public string ProviderId { get; set; }

        /// <summary>
        /// Occupancy Type
        /// </summary>
        public string OccupancyType { get; set; }

        /// <summary>
        /// Accomodation details
        /// </summary>
        public IAccomodation AccomodationInfo { get; set; }

        /// <summary>
        /// Number of Adults
        /// </summary>
        public int NumberOfAdults { get; set; }

        /// <summary>
        /// Number of Children
        /// </summary>
        public int NumberOfChildren { get; set; }

        /// <summary>
        /// Number Of Nights
        /// </summary>
        public int NumberOfNights { get; set; }

        /// <summary>
        /// Booking Method
        /// </summary>
        public String BookingThrough { get; set; }

        /// <summary>
        /// Created By
        /// </summary>
        public String CreatedBy { get; set; }

        /// <summary>
        /// UpdatedBy
        /// </summary>
        public String UpdatedBy { get; set; }

        /// <summary>
        /// Customer for the order
        /// </summary>
        public Customer Customer { get; set; }

        /// <summary>
        /// Customer notes info
        /// </summary>
        public List<BookingNotes> BookingNotes { get; set; }

        /// <summary>
        /// Subrates info
        /// </summary>
        public List<BookingSubrate> BookingSubrates { get; set; }

        /// <summary>
        /// get due by currencies
        /// </summary>
        public List<TotalDueItem> DueByCurrencies { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal LBCommissionFee { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal CreditCardProcessingFee { get; set; }

        /// <summary>
        /// Cancellation Message for Bed Banks
        /// </summary>
        public string BedBanksCancellationMessage { get; set; }
    }
}
