﻿
CREATE TABLE [dbo].[uCommerce_RoomReservationAgeInfo](
	[ReservationAgeId] [int] IDENTITY(1,1) NOT NULL,
	[RoomReservationId] [int] NULL,
	[OrderLineId] [int] NULL,
	[Age] [int] NULL,
 CONSTRAINT [PK_uCommerce_RoomReservationAgeInfo] PRIMARY KEY CLUSTERED 
(
	[ReservationAgeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


