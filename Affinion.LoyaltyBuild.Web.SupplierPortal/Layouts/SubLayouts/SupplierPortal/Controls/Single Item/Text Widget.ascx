﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Text Widget.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Single_Item.Text_Widget" %>

<asp:Panel runat="server" ID="pnl">
 <div class="text-widget sidebar-block">
  <h2><sc:FieldRenderer ID="frTitle" runat="server" FieldName="Title" /></h2>
  <sc:FieldRenderer ID="frText" runat="server" FieldName="Text" />
 </div>
</asp:Panel>