﻿namespace Affinion.LoyaltyBuild.BedBanks.Troika.Model
{
    public class AvailabilityFacility
    {
        public string FeatureID { get; set; }
        public string FeatureName { get; set; }
        public string ProviderID { get; set; }
    }
}
