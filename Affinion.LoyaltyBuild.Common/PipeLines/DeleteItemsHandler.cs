﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           DeleteItemsHandler.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Handler to remove media items : using DeleteItemsHandler pipeline
/// </summary>

#region Using Directives

using Affinion.LoyaltyBuild.Common.Instrumentation;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Text;
using Sitecore.Web.UI.Sheer;
using System;

#endregion

namespace Affinion.LoyaltyBuild.Common.PipeLines
{
    public class DeleteItemsHandler
    {
        #region Constant variable

        /// <summary>
        /// Path of supplier Item Template
        /// </summary>
        private const string SupplierItemTemplatePath = "/sitecore/templates/Affinion/AdminPortal/Supplier Setup/SupplierDetails";

        /// <summary>
        /// Path of Supplier Setup Media Library folder location
        /// </summary>
        private const string SupplierMediaFolder = "/sitecore/media library/Affinion/AdminPortal/Supplier Setup/{0}/{1}";

        /// <summary>
        /// Items text
        /// </summary>
        private const string ItemsText = "items";

        /// <summary>
        /// Pipe character
        /// </summary>
        private const char PipeCharacter = '|';

        #endregion

        #region Public Methods

        /// <summary>
        /// Handler to remove media items through pipeline
        /// </summary>
        /// <param name="args"></param>
        public void Delete(ClientPipelineArgs args)
        {
            try
            {
                /// Arg items stored as list string
                ListString items = new ListString(args.Parameters[ItemsText], PipeCharacter);
                /// Get Sitecore Master DB
                Database database = Sitecore.Configuration.Factory.GetDatabase(Constants.MasterDatabase);
                /// Remove media items of supplier
                RemoveMediaItems(items, database);
                return;
            }
            catch (Exception exception)
            {                
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, this);
                throw;
            }            
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Method used to remove media items for the supplier
        /// </summary>
        /// <param name="items"></param>
        /// <param name="database"></param>
        private void RemoveMediaItems(ListString items, Database database)
        {
            /// Get supplier template Id
            ID supplierTemplateId = database.GetItem(SupplierItemTemplatePath).ID;
            
            /// Loop and find appropriate item
            foreach (string item in items)
            {
                Item itm = database.GetItem(item);                
                if (itm != null && (itm.Template.ID == supplierTemplateId))
                {
                    Item supplierTypeFieldValue = database.GetItem(itm.Fields[Constants.SupplierTypeText].Value);
                    if (supplierTypeFieldValue != null)
                    {
                        /// Find required media path
                        string mediaItemPath = string.Format(SupplierMediaFolder, supplierTypeFieldValue.DisplayName, itm.DisplayName);
                        /// Remove media items if exists
                        /// 
                       var mediaItem =  database.GetItem(mediaItemPath);
                       if (mediaItem != null && !mediaItem.ID.IsNull)
                       {
                           Utilities.ItemHelper.RemoveItem(mediaItemPath, database);
                       }
                    }                    
                }
                return;
            }
        }

        #endregion
    }
}
