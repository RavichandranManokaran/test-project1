﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model;

/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/
namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports
{
    /// <summary>
    /// Interface to define the report related actions
    /// </summary>
    public interface IReportsService
    {
        /// <summary>
        /// Gets the UnClaimedReceipts information based receiptDate
        /// </summary>
        /// <param name="receiptDate">Receipt Date</param>
        /// <returns>UnClaimed Receipts details</returns>
        List<UnClaimedReceipts> GetUnClaimedReceipts(DateTime receiptDate);

        // <summary>
        /// Gets the finance overall revenue information based on search criteria
        /// </summary>
        /// <param name="searchCriteria">SearchCriteria</param>
        /// <returns>FinanceOverallRevenue</returns>
        List<FinanceOverallRevenue> GetFinanceOverallRevenueReport(SearchCriteria searchCriteria);

        // <summary>
        /// Gets the finance booking information based on search criteria
        /// </summary>
        /// <param name="searchCriteria">SearchCriteria</param>
        /// <returns>FinanceBooking</returns>
        List<FinanceBooking> GetFinanceBookingReport(SearchCriteria searchCriteria);

        // <summary>
        /// Gets the daily oracle finance information based on search criteria
        /// </summary>
        /// <param name="searchCriteria">SearchCriteria</param>
        /// <returns>DailyOracleFinance</returns>
        List<DailyOracleFinance> GetDailyOracleFinanceReport(SearchCriteria searchCriteria);
        
        // <summary>
        /// Gets the no_of_booking based on date as yesterday,today,week,month,year
        /// </summary>
        /// <param name=""></param>
        /// <returns>BookingCounterReport</returns>
        BookingCounterReport GetBookingCounterReport();

        /// <summary>
        /// Get Agent Performance Details
        /// </summary>
        /// <returns></returns>
        List<AgentPerformanceDetail> GetAgentPerformanceDetails(string createdby, DateTime fromdate, DateTime todate, string clientIds);


        /// <summary>
        /// Get Provisional Booking Based on input Filters
        /// </summary>
        /// <param name="searchfilter"></param>
        /// <returns></returns>
        List<ProvisionalBooking> GetProvisionalBooking(ProvisionalBookingFilter searchfilter);


        #region LWP-1000,1001
        // <summary>
        /// Get the payment request based on the search criteria
        /// </summary>
        /// <param name="searchCriteria">SearchCriteria</param>
        /// <returns>List<APInvoice></returns>
        List<APInvoice> GetPaymentRequest(SearchCriteria searchCriteria);

        // <summary>
        /// Get the payment request based on the search criteria
        /// </summary>
        /// <param name="searchCriteria">SearchCriteria</param>
        /// <returns>APInvoice</returns>
        APInvoice GetManualPaymentRequest(SearchCriteria searchCriteria);

        // <summary>
        /// Add payment request 
        /// </summary>
        /// <param name="lstAPOraclePayment">lstAPOraclePayment</param>
        /// <returns>ValidationResponse</returns>
        ValidationResponse APPaymentRequest(List<APOraclePayment> lstAPOraclePayment, string requestFrom);

        // <summary>
        /// Get the demand details based on the search criteria
        /// </summary>
        /// <param name="searchCriteria">SearchCriteria</param>
        /// <returns> List<APInvoice></returns>
        List<APInvoice> GetDemands(SearchCriteria searchCriteria);

        // <summary>
        /// AR payment request 
        /// </summary>
        /// <param name="lstAPOraclePayment">lstAPOraclePayment</param>
        /// <returns>ValidationResponse</returns>
        ValidationResponse ARPaymentRequest(List<AROraclePayment> lstAROraclePayment);

        // <summary>
        /// Get the oracle file information.
        /// </summary>
        /// <returns> List<OracleFileInfo></returns>
        List<OracleFileInfo> GetOracleFileInfo();

        // <summary>
        /// Get the data for the file .
        /// </summary>
        /// <returns> List<OracleInvoiceData></returns>
        List<OracleInvoiceData> GetOracleInvoiceData(int oracleFileID, string requestFrom);

         // <summary>
        /// Get the oracle sales invoice details based on the search criteria
        /// </summary>
        /// <param name="searchCriteria">SearchCriteria</param>
        /// <returns> List<ARInvoice></returns>
        List<ARInvoice> GetVATPaymentRequest(SearchCriteria searchCriteria);

        ValidationResponse DeleteHotelDemands(int orderID, int orderLineID);

        ValidationResponse DeleteVATPayment(int SupplierVatPaymentId);

        List<FinanceSummaryReport> GetARSummaryReport(SearchCriteria searchCriteria);

        List<ProviderAvailabilityAlert> GetProviderAvailabilityAlertReport(SearchCriteria searchCriteria);

        #endregion


        #region LWP-534
        /// <summary>
        /// To Get Report on My Bookings, To Return Today, Yesterday, WTD, MTD, YTD as Response with Comparison with OtherUsers in Total Count
        /// </summary>
        /// <param name="loggedOnUser">LoggedInUserId</param>
        /// <param name="clientIds">ListOfClientIdAsCommaSeperated</param>
        /// <returns></returns>
        List<MyTodaysBooking> GetMyTodaysBooking(string loggedOnUser, string clientIds);
        #endregion

    }
}
