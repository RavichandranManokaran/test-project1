﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using UCommerce.EntitiesV2;
using UCommerce.Presentation.Views.Marketing;
using UCommerce.Marketing.Awards;
using UCommerce.Presentation.Web.Controls;
using UCommerce.Presentation.Web;


namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.Ucommerce.Awards
{
    public partial class FixPriceUi : ViewEnabledControl<IEditCampaignItemView>, IConfigurable
    {


        protected void Page_Load(object sender, EventArgs e)
        {
            FixPriceAward dataSource = (FixPriceAward)this.DataSource;
            this.FixPriceLabel.Text = string.Format("Special price: {0}", dataSource.AmountOff.ToString());
            this.FixPriceText.Text = dataSource.AmountOff.ToString();
            this.DecimalValidator.ErrorMessage = string.Format("{0}", "Please Enter Only Number");
            this.RequiredValidator.ErrorMessage = string.Format("{0}", "Please Enter Only Number");
            this.DeleteButton.OnClientClick = string.Format("return confirm('{0}');", "Are you want to delete award");
        }

        // Methods
        protected void DeleteButton_Click(object sender, ImageClickEventArgs e)
        {


            if (this.AwardId != 0)
            {
                (from x in UCommerce.EntitiesV2.Award.All()
                 where x.AwardId == this.AwardId
                 select x).SingleOrDefault<UCommerce.EntitiesV2.Award>().Delete();
            }
            this.Page.Response.Redirect(this.Page.Request.Url.ToString(), true);
        }

        protected void EditButton_Click(object sender, ImageClickEventArgs e)
        {
            this.ToggleEditor();
        }

        public object New()
        {
            this.DataSource = new FixPriceAward();
            ((FixPriceAward)this.DataSource).CampaignItem = base.View.CampaignItem;
            if (((FixPriceAward)this.DataSource).AmountOff < 1M)
            {
                ((FixPriceAward)this.DataSource).AmountOff = 0M;
            }
            if (string.IsNullOrEmpty(((FixPriceAward)this.DataSource).Name))
            {
                ((FixPriceAward)this.DataSource).Name = "Special Price";
            }
            ((FixPriceAward)this.DataSource).Save();
            return this.DataSource;
        }

        protected void SaveButton_Click(object sender, ImageClickEventArgs e)
        {
            FixPriceAward award = (this.DataSource == null) ? new FixPriceAward() : ((FixPriceAward)this.DataSource);
            CampaignItem campaignItem = base.View.CampaignItem;
            award.AmountOff = decimal.Parse(this.FixPriceText.Text);
            award.CampaignItem = campaignItem;
            award.Save();
            this.FixPriceLabel.Text = string.Format("Special price: {0}", FixPriceText.Text.ToString());
            this.ToggleEditor();
        }

        private void ToggleEditor()
        {
            this.NonEditModePlaceHolder.Visible = !this.NonEditModePlaceHolder.Visible;
            this.EditButton.Visible = !this.EditButton.Visible;
            this.EditModePlaceHolder.Visible = !this.EditModePlaceHolder.Visible;
            this.SaveButton.Visible = !this.SaveButton.Visible;
        }

        // Properties
        public int AwardId
        {
            get
            {
                if (this.DataSource == null)
                {
                    return 0;
                }
                return ((UCommerce.EntitiesV2.Award)this.DataSource).AwardId;
            }
        }

        public int CampaignItemId
        {
            get
            {
                return base.View.CampaignItem.CampaignItemId;
            }
        }

        public object DataSource { get; set; }

    }



}