﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalPaymentConfirmationSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalPaymentConfirmationSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Sitecore.Data.Items" %>
<%@ Import Namespace="Affinion.LoyaltyBuild.Common.SessionHelper" %>

<div style="visibility:hidden;margin-top:60px"></div>
<div class="alert alert-danger" id="dangerDiv" runat="server">
    <strong>Sorry!</strong> Your payment is rejected.
</div>


