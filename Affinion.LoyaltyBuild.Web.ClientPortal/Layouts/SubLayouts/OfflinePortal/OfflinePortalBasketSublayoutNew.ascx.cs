﻿
using bookinghelper = Affinion.LoyaltyBuild.Api.Booking.Helper;
using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
using System;
using ucommapi = UCommerce.Api;
using System.Web.UI.WebControls;
using model = Affinion.LoyaltyBuild.Api.Booking.Data;
using Affinion.LoyaltyBuild.Model.Provider;
using Affinion.LoyaltyBuild.Model.Product;
using Affinion.LoyaltyBuild.Api.Booking.Data;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.Api.Booking.Service;
using Affinion.LoyaltyBuild.Api.Booking.Helper;
using Affinion.LoyaltyBuild.Common;
using Sitecore.Data.Items;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Affinion.LoyaltyBuild.Search.Data;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{

    public partial class OfflinePortalBasketSublayoutNew : BaseSublayout
    {
        protected String GuestCount { get; set; }
        protected String SetAccommodationText { get; set; }
        protected String AccomodationDetailsRequiredMsg { get; set; }

        public int CurrencyIdforBB { get; set; }

        /// <summary>
        /// Page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
       protected void Page_Load(object sender, EventArgs e)
       {
           //if customer mapping is complete
           Sitecore.Events.Event.Subscribe("lb:CustomerMappingSuccess", CustomerMappingSuccess);

           //display empty on basket empty
           DisplayEmptyMessage();

           //bind sitecore texts
           BindSitecoreText();

           //Logic if the basket is empty
           if (!ucommapi.TransactionLibrary.HasBasket())
           {
               return;
           }

           //set checkbox values
           SetCheckBoxValues();

           var basket = bookinghelper.BasketHelper.GetBasket();

           rptProviders.DataSource = basket.Bookings.GroupBy(i => i.ProviderId);
           rptProviders.DataBind();
           if (basket.Bookings.Any(i => i.Type == ProductType.BedBanks))
               depositCheckBox.Enabled = false;

           litProFee.Text = CurrencyHelper.FormatCurrency(basket.ProcessingFee, basket.CurrencyId);
           litPayableAccomodation.Text = CurrencyHelper.FormatCurrency(basket.TotalPayableAccomodation, basket.CurrencyId);
           litPayableToday.Text = CurrencyHelper.FormatCurrency(basket.TotalPayableToday, basket.CurrencyId);
           litTotalAmount.Text = CurrencyHelper.FormatCurrency(basket.TotalAmount, basket.CurrencyId);
       }
        
       /// <summary>
       /// Displays a Message when Basket is Empty 
       /// </summary>
       private void DisplayEmptyMessage()
       {
           EmptyBasket.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "EmptyBasketMessage");
           if (!BasketHelper.HasBasket)
           {
               divEmptyBasket.Style.Remove("display");
               divBookings.Visible = false;
               divFees.Visible = false;
           }
       }

       /// <summary>
       /// page unload event
       /// </summary>
       /// <param name="sender"></param>
       /// <param name="e"></param>
       protected void Page_Unload(object sender, EventArgs e)
       {
           Sitecore.Events.Event.Unsubscribe("lb:CustomerMappingSuccess", CustomerMappingSuccess);
       }

       /// <summary>
       /// Event raised on client validation success
       /// </summary>
       /// <param name="sender"></param>
       /// <param name="e"></param>
       private void CustomerMappingSuccess(object sender, EventArgs e)
       {
           PaymentSection.Visible = true;
       }

       /// <summary>
       /// set check box values for payment portion and type
       /// </summary>
       private void SetCheckBoxValues()
       {
           var methods = bookinghelper.BasketHelper.GetAllPaymentMethods();

           cashCheckBox.Enabled = false;
           chequeCheckBox.Enabled = false;
           voucherCheckBox.Enabled = false;
           poCheckBox.Enabled = false;

           partialCheckBox.Enabled = false;

           cashCheckBox.InputAttributes.Add("value", Affinion.LoyaltyBuild.Common.Constants.PaymentType.Cash);
           chequeCheckBox.InputAttributes.Add("value", Affinion.LoyaltyBuild.Common.Constants.PaymentType.Cheque);
           creditDebitCheckBox.InputAttributes.Add("value", Affinion.LoyaltyBuild.Common.Constants.PaymentType.CreditCard);
           voucherCheckBox.InputAttributes.Add("value", Affinion.LoyaltyBuild.Common.Constants.PaymentType.Voucher);
           poCheckBox.InputAttributes.Add("value", Affinion.LoyaltyBuild.Common.Constants.PaymentType.PostOffice);

           depositCheckBox.InputAttributes.Add("value", Affinion.LoyaltyBuild.Common.Constants.PaymentMethod.DepositOnly);
           fullCheckbox.InputAttributes.Add("value", Affinion.LoyaltyBuild.Common.Constants.PaymentMethod.FullPayment);
           partialCheckBox.InputAttributes.Add("value", Affinion.LoyaltyBuild.Common.Constants.PaymentMethod.PartialPayment);
       }

       /// <summary>
       /// Proceed to payment
       /// </summary>
       /// <param name="sender"></param>
       /// <param name="e"></param>
       protected void ProceedToPayment(object sender, EventArgs e)
       {

           var basket = BasketHelper.GetBasket();

           basket.Order[Constants.OrderCheckoutAmountColumn] = basket.TotalPayableToday.ToString("F");
           basket.Order.Save();

           string redirectUrl = Request.Url.ToString().Replace("http:", "https:");
           redirectUrl = redirectUrl.Replace(Request.RawUrl, "/paymentgateway");
           Response.Redirect(redirectUrl, false);
       }

        /// <summary>
        /// bind repeater for bookings
        /// </summary>
        protected void BookingsBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var booking = (model.Booking)e.Item.DataItem;
                CurrencyInfo ci = new CurrencyInfo();
                var provider = booking.GetProvider();
                var product = booking.GetProduct();
                HtmlGenericControl errataDiv = e.Item.FindControl("errataInfoDiv") as HtmlGenericControl;
                switch (booking.Type)
                {
                    case ProductType.Room:
                    case ProductType.Package:
                        var loyaltyProvider = (Hotel)provider;
                        var roomBooking = (RoomBooking)booking;
                        if (!string.IsNullOrEmpty(roomBooking.GuestName))
                            ((HtmlInputCheckBox)e.Item.FindControl("chkValidateAcc")).Checked = true;
                        ((Panel)e.Item.FindControl("pnlRoom")).Visible = true;
                        ((Panel)e.Item.FindControl("pnlBedBanks")).Visible = false;
                        errataDiv.Visible = false;
                        ((Literal)e.Item.FindControl("litRoomCheckInDate")).Text = string.Format("{0}", roomBooking.CheckinDate.ToString("dddd, MMMM dd, yyyy"), loyaltyProvider.CheckInTime);
                        ((Literal)e.Item.FindControl("litRoomCheckOutDate")).Text = string.Format("{0}", roomBooking.CheckoutDate.ToString("dddd, MMMM dd, yyyy"), loyaltyProvider.CheckOutTime);
                        ((Literal)e.Item.FindControl("litRoomNights")).Text = roomBooking.NoOfNights.ToString();
                        ((Literal)e.Item.FindControl("litRoomQuantity")).Text = roomBooking.Quantity.ToString();
                        ((Literal)e.Item.FindControl("litRoomName")).Text = product.Name;
                        ((Literal)e.Item.FindControl("litRoomPrice")).Text = CurrencyHelper.FormatCurrency((decimal)roomBooking.Price, roomBooking.CurrencyId);
                        break;
                    case ProductType.BedBanks:
                        var bedbankProvider = (BedBankHotel)provider;
                        var bbRoombooking = (BedBanksBooking)booking;
                        CurrencyIdforBB = bbRoombooking.CurrencyId;
                        decimal tempOldPrice = 0; decimal tempNewPrice = 0;
                        decimal.TryParse(bbRoombooking.OldPrice.ToString("#.##"), out tempOldPrice);
                        decimal.TryParse(bbRoombooking.Price.ToString("#.##"), out tempNewPrice);
                        Item conditionalMessageItem = GetConditionalMessageItem();
                        if (!string.IsNullOrEmpty(bbRoombooking.LeadGuestFirstName))
                            ((HtmlInputCheckBox)e.Item.FindControl("chkValidateAccBB")).Checked = true;
                        ((Panel)e.Item.FindControl("pnlBedBanks")).Visible = true;
                        ((Panel)e.Item.FindControl("pnlRoom")).Visible = false;
                        errataDiv.Visible = true;
                        ((Literal)e.Item.FindControl("bblitRoomCheckInDate")).Text = string.Format("{0}", bbRoombooking.CheckinDate.ToString("dddd,MMMM dd,yyyy"));
                        ((Literal)e.Item.FindControl("bblitRoomCheckOutDate")).Text = string.Format("{0}", bbRoombooking.CheckoutDate.ToString("dddd, MMMM dd, yyyy"));
                        ((Literal)e.Item.FindControl("bblitRoomNights")).Text = bbRoombooking.Duration;
                        ((Literal)e.Item.FindControl("bblitRoomQuantity")).Text = bbRoombooking.Quantity.ToString();
                        ((Literal)e.Item.FindControl("bblitRoomName")).Text = bbRoombooking.RoomInfo;
                        ((Literal)e.Item.FindControl("bblitRoomPrice")).Text = CurrencyHelper.FormatCurrency((decimal)bbRoombooking.OldPrice, bbRoombooking.CurrencyId);
                        ((Literal)e.Item.FindControl("cancellationHeader")).Text = SitecoreFieldsHelper.GetItemFieldValue(conditionalMessageItem, "Bed Banks Cancellation Header");
                        ((Literal)e.Item.FindControl("nonTransferableText")).Text = SitecoreFieldsHelper.GetItemFieldValue(conditionalMessageItem, "Bed Banks Non Transferable Message");
                        HtmlGenericControl divOldPrice = e.Item.FindControl("PriceDiv") as HtmlGenericControl;
                        Literal litNewPrice = e.Item.FindControl("bblitNewRoomPrice") as Literal;
                        Literal litBBPriceInfo = e.Item.FindControl("litBBPriceInfo") as Literal;
                        litNewPrice.Visible = false;
                        litBBPriceInfo.Visible = false;
                        Repeater errataRepeater = (Repeater)e.Item.FindControl("ErrataRepeater");
                        Repeater cancelCriteriaRepeater = (Repeater)e.Item.FindControl("CancellationCriteriaRepeater");
                        errataRepeater.DataSource = bbRoombooking.GetErrataFromDB(bbRoombooking.OrderLineId);
                        errataRepeater.DataBind();
                        cancelCriteriaRepeater.DataSource = bbRoombooking.CancelCriterias;
                        cancelCriteriaRepeater.DataBind();
                        if (!(tempOldPrice == tempNewPrice))
                        {
                            litNewPrice.Visible = true;
                            litBBPriceInfo.Visible = true;
                            divOldPrice.Attributes.Add("style", "text-decoration:line-through");
                            litNewPrice.Text = CurrencyHelper.FormatCurrency((decimal)bbRoombooking.Price, bbRoombooking.CurrencyId);
                            litBBPriceInfo.Text = SitecoreFieldsHelper.GetItemFieldValue(conditionalMessageItem, "Bed Banks Price Change Message");
                        }
                        break;
                }
            }
        }

        /// <summary>
        /// Get conditional message item
        /// </summary>
        private Item GetConditionalMessageItem()
        {
            Item conditionalMessageItem = this.GetItemFromRenderingParameterPath("Conditional Messages Link");

            if (conditionalMessageItem != null)
                return conditionalMessageItem;
            else
                return null;
        }
        /// <summary>
        /// bind repeater for bookings
        /// </summary>
        protected void ProvidersBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var bookings = (IGrouping<string, model.Booking>)e.Item.DataItem;
                var booking = bookings.First();

                var provider = booking.GetProvider();
                
                ((Literal)e.Item.FindControl("litHotelNameHeader")).Text = provider.Name;

                var starRepeater = ((Repeater)e.Item.FindControl("rptStar"));
                starRepeater.DataSource = System.Linq.Enumerable.Range(1, ((IGenericProvider)provider).StarRanking);
                starRepeater.DataBind();

                var rptBookings = ((Repeater)e.Item.FindControl("rptBookings"));
                rptBookings.DataSource = bookings;
                rptBookings.DataBind();
            }
        }

        /// <summary>
        /// Bind errata info for bed banks
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ErrataBound(object sender, RepeaterItemEventArgs e)
        {
            var dataItem = (BedBankErrata)e.Item.DataItem;
            Literal literalerrataSubject = e.Item.FindControl("BBerrataSubject") as Literal;
            Literal literalerrataDescription = e.Item.FindControl("BBerrataDescription") as Literal;

            literalerrataSubject.Text = dataItem.Subject;
            literalerrataDescription.Text = dataItem.Description;
        }

        /// <summary>
        /// Bind cancellation criteria for bed banks
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CancellationCriteriaBound(object sender, RepeaterItemEventArgs e)
        {
            var dataItem = (BedBankCancelCriteria)e.Item.DataItem;
            Literal literalcancelCriteria = e.Item.FindControl("cancelCriteria") as Literal;
            Item conditionalMessageItem = GetConditionalMessageItem();
            literalcancelCriteria.Text = string.Format(SitecoreFieldsHelper.GetItemFieldValue(conditionalMessageItem, "Bed Banks Cancellation Message"), dataItem.StartDate.ToString("dd/MM/yyyy"), dataItem.EndDate.ToString("dd/MM/yyyy"), CurrencyHelper.FormatCurrency(dataItem.Amount, CurrencyIdforBB));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lblRemove_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)(sender);
            string skuId = btn.CommandArgument;
            Affinion.Loyaltybuild.BusinessLogic.Helper.BasketHelper.RemoveCartItemBySkuId(skuId);

            //Rfresh the page
            Response.Redirect(Request.RawUrl);
        }

        /// <summary>
        /// bind sitecore text and alerts
        /// </summary>
        private void BindSitecoreText()
        {
            Item currentPageItem = Sitecore.Context.Item;
            if (currentPageItem != null)
            {
                AccomodationDetailsRequiredMsg = SitecoreFieldsHelper.GetItemFieldValue(currentPageItem, "Accomodation Details Required");
                GuestCount = SitecoreFieldsHelper.GetItemFieldValue(currentPageItem, "GuestCount");
                SetAccommodationText = SitecoreFieldsHelper.GetItemFieldValue(currentPageItem, "Set Accommodation Text");
            }
        }

        protected void ButtonAddAnotherBreak_Click(object sender, EventArgs e)
        {
            Response.Redirect("/hotels");
        }
    }
}

