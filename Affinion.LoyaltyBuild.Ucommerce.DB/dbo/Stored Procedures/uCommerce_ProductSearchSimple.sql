﻿
CREATE PROCEDURE [dbo].[uCommerce_ProductSearchSimple] 
	@SearchTerm NVARCHAR(MAX),
	@LimitToCatalogIds NVARCHAR(MAX) -- comma separeted string of ints
AS
	SET NOCOUNT ON

	-- Escape special characters
	SELECT @SearchTerm = 
					REPLACE( 
					REPLACE( 
					REPLACE( 
					REPLACE(
					REPLACE(
					REPLACE( @SearchTerm
					,    '\', '\\'  )
					,	 '--', ''   )
					,	 '''', '\''')                
					,    '%', '\%'  )
					,    '_', '\_'  )
					,    '[', '\['  )

	SELECT @SearchTerm = '%' + @SearchTerm + '%'

	SELECT DISTINCT
		 Product.*
	FROM
		Product
		JOIN ProductProperty
			ON ProductProperty.ProductId = Product.ProductId
		JOIN ProductDefinitionField
			ON ProductDefinitionField.ProductDefinitionFieldId = ProductProperty.ProductDefinitionFieldId
		LEFT JOIN CategoryProductRelation
			ON CategoryProductRelation.ProductId = Product.ProductId
		LEFT JOIN Category
			ON Category.CategoryId = CategoryProductRelation.CategoryId
		LEFT JOIN ProductCatalog
			ON ProductCatalog.ProductCatalogId = Category.ProductCatalogId
		LEFT JOIN dbo.ParseArrayToTable(@LimitToCatalogIds, ';', 1) LimitToCatalogIdsTable
			ON LimitToCatalogIdsTable.NumericValue = ProductCatalog.ProductCatalogId OR @LimitToCatalogIds = '' OR @LimitToCatalogIds IS NULL
	WHERE
		(
			CategoryProductRelation.CategoryId IS NOT NULL
			AND
			(
				Product.Sku LIKE @SearchTerm
				OR
				Product.VariantSku LIKE @SearchTerm
				OR
				Product.Name LIKE @SearchTerm
				OR
				ProductProperty.Value LIKE @SearchTerm
			)
		)	
		OR
		(	-- Include products not in any category, which also matches the search term
			CategoryProductRelation.CategoryId IS NULL
			AND
			(	-- But only if no catalog ids to limit to were provided
				@LimitToCatalogIds IS NULL 
				OR
				@LimitToCatalogIds = ''
			)
			AND
			(
				Product.Sku LIKE @SearchTerm
				OR
				Product.VariantSku LIKE @SearchTerm
				OR
				Product.Name LIKE @SearchTerm
				OR
				ProductProperty.Value LIKE @SearchTerm
			)
		)				

