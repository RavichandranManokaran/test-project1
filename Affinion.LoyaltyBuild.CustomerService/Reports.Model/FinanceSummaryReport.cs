﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model
{
    public class FinanceSummaryReport
    {
        public string HotelName { get; set; }
        public int NumberOfSales { get; set; }
        public decimal Amount { get; set; }
        public string CurrencyCode { get; set; }
    }
}
