﻿using Affinion.LoyaltyBuild.Model.Provider;
using System.Collections.Generic;

namespace Affinion.LoyaltyBuild.Api.Search.Data
{
    public class ProviderCollection : List<SearchResult>, IProviderCollection<SearchResult>
    {
        private ProviderType _serviceType;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="serviceType"></param>
        public ProviderCollection(ProviderType serviceType)
        {
            _serviceType = serviceType;
        }

        /// <summary>
        /// gets service type
        /// </summary>
        public ProviderType CollectionType
        {
            get { return _serviceType; }
        }
    }
}
