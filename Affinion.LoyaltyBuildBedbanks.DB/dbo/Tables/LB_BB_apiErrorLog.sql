/****** Object:  Table [dbo].[LB_BB_apiErrorLog]    Script Date: 7/19/2016 7:57:10 PM ******/

CREATE TABLE [dbo].[LB_BB_apiErrorLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Method] [varchar](max) NULL,
	[Source] [int] NULL,
	[Severity] [int] NULL,
	[System] [int] NULL,
	[Detail] [varchar](max) NULL,
	[Type] [varchar](max) NULL,
	[Code] [varchar](max) NULL,
	[ErrorCode] [varchar](max) NULL,
	[ExtendedInfo] [varchar](max) NULL,
	[Message] [varchar](max) NULL,
	[StackTrace] [varchar](max) NULL,
	[datecreated] [datetime] NULL,
	[bbResponse] [varchar](max) NULL,
 CONSTRAINT [PK__apiErrorLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[LB_BB_apiErrorLog] ADD  DEFAULT (getdate()) FOR [datecreated]
GO

