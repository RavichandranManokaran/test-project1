﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalAlerts.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalAlerts" %>
<%@ Import namespace="System.Linq" %>
var Alerts = {
<%if(Alerts != null){
    var sections = Alerts.Fields.GroupBy(field => field.Section);
    foreach (var section in sections)
    {%>  
        "<%= section.Key %>" :
        {
        <%foreach (var field in section){%>
            "<%=field.Name %>" : "<%=field.Value %>",
        <%}%>
        },
    <%} 
}%>
}