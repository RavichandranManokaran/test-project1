﻿CREATE TABLE [dbo].[uCommerce_Booking_Availability](
	[AvailabilityID] [int] NOT NULL,
	[ProviderID] [int] NOT NULL,
	[ProviderOccupancyTypeID] INT NOT NULL, 
	[NumberAllocated] [int] NOT NULL,
	[NumberBooked] [int] NOT NULL,
	[AvailabilityDate] [datetime] NOT NULL,
	[DateCounter] [int] NOT NULL,
	[Createdby] [nvarchar](50) NOT NULL,
	[Updatedby] [nchar](50) NOT NULL,
	[Creationdate] [datetime] NOT NULL,
	[Updationdate] [datetime] NOT NULL,
	[IsStopSellOn] [bit] NOT NULL,
	[IsCloseOut] [bit] NOT NULL,
    CONSTRAINT [PK_Booking_Availability] PRIMARY KEY CLUSTERED 
(
	[AvailabilityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO