﻿namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Helper
{
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using System.Text;
    using Affinion.LoyaltyBuild.Communications.Services;
    using Affinion.LoyaltyBuild.Common.Exceptions;


    public class PropertyHelper
    {
      
        public AffinionException SendPropertyInfoMail(string queryString)
        {
            AffinionException exception = null;

            try
            {
                Item supplierPortalSiteSettings = Sitecore.Context.Database.GetItemByKey("SupplierPortalSiteSettings");

                if (supplierPortalSiteSettings == null)
                    throw new AffinionException("PropertyHelper : SendPropertyInfoMail : SupplierPortalSiteSettings item not found");

                Item emailItem = supplierPortalSiteSettings.Axes.GetDescendants().Where(i => i.Name == "property-information-email" && i.TemplateName == "BaseEmail").FirstOrDefault();

                if (emailItem == null)
                    throw new AffinionException("PropertyHelper : SendPropertyInfoMail : Email item not found");

                string hostName = SitecoreFieldsHelper.GetValue(supplierPortalSiteSettings, "HostName");
                string port = SitecoreFieldsHelper.GetValue(supplierPortalSiteSettings, "Port");
                string subject = SitecoreFieldsHelper.GetValue(supplierPortalSiteSettings, "Subject");
                string body = SitecoreFieldsHelper.GetValue(supplierPortalSiteSettings, "EmailBody");
                string sender = SitecoreFieldsHelper.GetValue(supplierPortalSiteSettings, "SenderEmail");
                string receiver = SitecoreFieldsHelper.GetValue(supplierPortalSiteSettings, "ReceiverEmail");

                if (string.IsNullOrEmpty(hostName))
                    throw new AffinionException("PropertyHelper : SendPropertyInfoMail : hostName is not defined in Supplier Portal Site Settings");


                StringBuilder url = new StringBuilder();
                url.Append("http://");
                url.Append(hostName);

                if (!string.IsNullOrEmpty(port))
                {
                    url.Append(":");
                    url.Append(port);
                }

                url.Append(string.Format("/sitecore modules/Shell/Affinion/SupplierInformationUdate/SupplierInformationAcceptance.aspx?message={0}",queryString));

                    
                string encodedUrl = HttpUtility.UrlEncode(url.ToString());

                if (body.Contains("{{url}}"))
                {
                    body.Replace("{{url}}", encodedUrl);
                }
                else
                {
                    body += encodedUrl;
                }

                // dispatch mail
                EmailService mailService = new EmailService();
                mailService.Send(sender, receiver, subject, body, true);

                return null;
            }
            catch (AffinionException ex)
            {
                exception = ex;
                return exception;
            }
            catch(Exception ex)
            {
                exception = new AffinionException("Property specific information change mail sent failed", ex);
                return exception;
            }
        }
    }
}
   