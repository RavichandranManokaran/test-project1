﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Helpers;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Basket
{
    /// <summary>
    /// Service that provides methods to perform basket related actions
    /// </summary>
    public class BasketService : IBasketService
    {
        //private variables
        private OrderService _orderService;

        /// <summary>
        /// constructor
        /// </summary>
        public BasketService(OrderService orderService)
        {
            _orderService = orderService;
        }

        /// <summary>
        /// Gets the items from the basket
        /// </summary>
        /// <returns>List of Booking Information</returns>
        public List<Booking> GetItemsInBasket()
        {
            List<Booking> basket = new List<Booking>();

            if (UCommerce.Api.TransactionLibrary.HasBasket())
            {
                foreach (var item in UCommerce.Api.TransactionLibrary.GetBasket().PurchaseOrder.OrderLines)
                {
                    basket.Add(_orderService.GetBooking(item.OrderLineId));
                }
            }
             
            return basket;
        }
    }
}
