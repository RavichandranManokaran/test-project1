﻿CREATE PROCEDURE [dbo].[LB_SearchBooking] 
(
	@OrderReference VARCHAR(50),
	@GuestName NVARCHAR(100),
	@CheckInDate DATE
)
AS
BEGIN
DECLARE @orderLineID INT
    SELECT 
		@orderLineID = a.OrderLineId 
	FROM LB_BookingInfo  a
		join uCommerce_OrderLine b on a.OrderLineId = b.OrderLineId 
		join uCommerce_OrderAddress  c on b.OrderId = c.OrderId 
	WHERE 
		BookingNo = @OrderReference 
		AND [checkInDate]  = Convert(date,CAST(@CheckInDate as datetime) ,105)
		AND c.LastName = @GuestName
	RETURN @orderLineID
END
