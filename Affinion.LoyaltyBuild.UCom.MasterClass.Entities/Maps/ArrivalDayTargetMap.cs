﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class ArrivalDayTargetMap : SubclassMap<ArrivalDayTarget>
    {
        // Methods
        public ArrivalDayTargetMap()
        {
            base.Table("LB_ArrivalDayTarget");
            base.KeyColumn("ArrivalDayTargetId");
            base.Map(x => x.ArrivalDayTargetId).ReadOnly();
            base.Map(x => x.ArrivalDay).Not.Nullable();
        }

    }

}
