CREATE PROCEDURE [dbo].[LB_GetFinanceOverallRevenueReport]
@StartDate DATETIME=NULL,
@EndDate DATETIME=NULL,
@BookingMethodID INT=0,
@SubTypeReportID INT=0
AS
BEGIN 
	DECLARE @bookingThrough VARCHAR(20)
	DECLARE @BookingThroguh table
	(
		BookingMethod varchar(100)
	)
	DELETE FROM @BookingThroguh
  
	 IF(@BookingMethodID=1)
	 BEGIN
		  INSERT INTO @BookingThroguh VALUES('Online')
	 END
	 ELSE IF(@BookingMethodID=2) 
	 BEGIN 
		  INSERT INTO @BookingThroguh VALUES('Offline')
	 END 
	 ELSE IF(@BookingMethodID=3)
	 BEGIN 
		  INSERT INTO @BookingThroguh VALUES('Online')
		  INSERT INTO @BookingThroguh VALUES('Offline')
	 END  
	  
	 IF(@SubTypeReportID=1)
	 BEGIN      
		   SELECT 
						OL.CreatedOn,
						COUNT(OL.CreatedOn) AS 'Bookings',
						OL.ProductName AS ClientOffer,
						CA.ClientId AS ClientID,
						C.ISOCode AS CurrencyName,
						SUM(OL.Total)+SUM(OLS.Amount)  AS TotalPrice,
						OL.VAT,
						SUM(OL.Total) AS LoyaltyBuildRevenue	
					FROM LB_RoomReservation RS
						INNER JOIN uCommerce_OrderLine OL on RS.OrderLineId=OL.OrderLineId
						INNER JOIN LB_OrderLineSubrates OLS ON OL.OrderLineID=OLS.OrderLineID
						INNER JOIN LB_OrderLinePayment OLP ON OLS.OrderLineID=OLP.OrderLineID
						INNER JOIN uCommerce_Currency C ON OLP.CurrencyTypeId=C.CurrencyId
						INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId=PO.OrderId AND PO.CustomerId IS NOT NULL
						INNER JOIN uCommerce_Customer CU ON PO.CustomerId=CU.CustomerId
						INNER JOIN LB_CustomerAttribute CA ON CU.CustomerId=CA.CustomerId
					WHERE 
					   RS.BookingThrough IS NOT NULL AND
					   RS.BookingThrough IN (SELECT BookingMethod FROM @BookingThroguh) AND
					   OLS.SubrateItemCode='CCPROFEE' AND
					   CAST(OL.CreatedOn AS DATE) >= CAST(@StartDate AS DATE) AND  CAST(OL.CreatedOn AS DATE) <= CAST(@EndDate AS DATE) AND
					   OL.VAT IS NOT NULL
					GROUP BY 
					   OL.ProductName,C.ISOCode,CA.ClientId,OL.VAT,OL.CreatedOn ORDER BY OL.CreatedOn DESC 
       
	 END
	 ELSE IF(@SubTypeReportID=2)
	 BEGIN 
		  SELECT 
						OL.CreatedOn,
						COUNT(OL.CreatedOn) AS 'Bookings',
						OL.ProductName AS ClientOffer,
						CA.ClientId AS ClientID,
						C.ISOCode AS CurrencyName,
						SUM(OL.Total) AS TotalPrice,
						OL.VAT,
						SUM(OL.Total) AS LoyaltyBuildRevenue	
					FROM LB_RoomReservation RS
						INNER JOIN uCommerce_OrderLine OL on RS.OrderLineId=OL.OrderLineId
						INNER JOIN LB_OrderLineSubrates OLS ON OL.OrderLineID=OLS.OrderLineID
						INNER JOIN LB_OrderLinePayment OLP ON OLS.OrderLineID=OLP.OrderLineID
						INNER JOIN uCommerce_Currency C ON OLP.CurrencyTypeId=C.CurrencyId
						INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId=PO.OrderId AND PO.CustomerId IS NOT NULL
						INNER JOIN uCommerce_Customer CU ON PO.CustomerId=CU.CustomerId
						INNER JOIN LB_CustomerAttribute CA ON CU.CustomerId=CA.CustomerId
					WHERE 
					   RS.BookingThrough IS NOT NULL AND
					   RS.BookingThrough IN (SELECT BookingMethod FROM @BookingThroguh) AND
					   OLS.SubrateItemCode <> 'CCPROFEE' AND
					   CAST(OL.CreatedOn AS DATE) >= CAST(@StartDate AS DATE) AND  CAST(OL.CreatedOn AS DATE) <= CAST(@EndDate AS DATE) AND
					   OL.VAT IS NOT NULL
					 GROUP BY 
					   OL.ProductName,C.ISOCode,CA.ClientId,OL.VAT,OL.CreatedOn ORDER BY OL.CreatedOn DESC 
	 END  
END 


		      
			 
GO

