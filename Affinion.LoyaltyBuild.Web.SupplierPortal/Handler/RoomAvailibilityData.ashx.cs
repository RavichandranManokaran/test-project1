﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Availability;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;



namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Handler
{
    /// <summary>
    /// Summary description for RoomAvailibilityData
    /// </summary>
    public class RoomAvailibilityData : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                context.Response.ContentType = "text/plain";
                var jss = new JavaScriptSerializer();
                AvailabilityHelper provider = new AvailabilityHelper(context);
                string json = new StreamReader(context.Request.InputStream).ReadToEnd();
                List<ChangeRoomAvailabilityByDate> sData = jss.Deserialize<List<ChangeRoomAvailabilityByDate>>(json);

                List<ChangeRoomAvailabilityByDate> filterData = new List<ChangeRoomAvailabilityByDate>(); 
                List<RoomAvailabilityByDate> existingData = provider.GetAvailabilityy(sData.Min(x => x.DateCounter), sData.Max(x => x.DateCounter));
                foreach (var item in sData)
                {
                    // If Room Value is  0 And there is no existing data found then not add that record
                    if (item.AvailableRoom == 0)
                    {
                        //check existing item or 
                        var existing = existingData.FirstOrDefault(x => x.DateCounter == item.DateCounter && x.ProductSku == item.VariantSku);
                        if (existing !=null || item.IsCloseOut || !string.IsNullOrEmpty(item.PriceBand))
                        {
                            filterData.Add(item);
                        }
                    }
                    else
                    {
                        filterData.Add(item);
                    }
                }
                
                List<string> priceBandId = filterData.Select(x => x.PriceBand).Distinct().ToList();
                List<Item> data = new List<Item>();
                foreach (var item in priceBandId)
                {
                    Item currentItem = item!=null?Sitecore.Context.Database.GetItem(item):null;
                  if (currentItem != null)
                  {
                      data.Add(currentItem);
                  }
                }
                foreach (var item in filterData)
                {
                    if (item.ProductType == 1)
                    {
                        Item sitecoreItem = data.FirstOrDefault(x => x.ID.Guid.ToString() == item.PriceBand);
                        if (sitecoreItem != null)
                        {
                            decimal rate = 0;
                            if (sitecoreItem.Fields["Rate"] != null && decimal.TryParse(sitecoreItem.Fields["Rate"].ToString(), out rate))
                            {
                                item.Price = rate;
                            }
                        }
                    }
                    else if (item.ProductType == 2)
                    {
                        List<decimal> price = provider.GetAddonPrice(item.ProductSku , item.VariantSku);
                        item.Price = price[0];
                        item.PackagePrice = price[1];
                    }
                    else if (item.ProductType == 3)
                    {
                        Item sitecoreItem = data.FirstOrDefault(x => x.ID.Guid.ToString() == item.PriceBand);
                        if (sitecoreItem != null)
                        {
                            decimal rate = 0;
                            if (sitecoreItem.Fields["Rate"] != null && decimal.TryParse(sitecoreItem.Fields["Rate"].ToString(), out rate))
                            {
                                item.Price = rate;
                                item.PackagePrice = rate + item.AddOnPrice;
                            }
                        }
                    }
                }
                provider.SaveRoomAvailibility(filterData);
                context.Response.Write("SUCCESS");
            }
            catch (Exception er)
            {
                context.Response.Write("ERROR" + er.Message.ToString());
                Sitecore.Diagnostics.Log.Error(er.Message, new object());
            }

        }




        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}