
/****** Object:  Table [dbo].[LB_BB_provider]    Script Date: 7/19/2016 8:02:09 PM ******/

CREATE TABLE [dbo].[LB_BB_provider](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[availabilityProviderID] [int] NOT NULL,
	[providerName] [varchar](200) NOT NULL,
	[availabilityProviderRecordID] [int] NOT NULL,
	[lbProviderID] [varchar](250) NULL,
	[addressline1] [varchar](250) NOT NULL,
	[addressline2] [varchar](250) NULL,
	[addressline3] [varchar](250) NULL,
	[addressline4] [varchar](250) NULL,
	[addressline5] [varchar](250) NULL,
	[phone] [varchar](50) NULL,
	[Fax] [varchar](250) NULL,
	[lat] [varchar](50) NULL,
	[long] [varchar](50) NULL,
	[isSuppressed] [int] NULL,
	[IsMultipleMatch] [int] NULL,
 CONSTRAINT [PK__provider] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[LB_BB_provider] ADD  DEFAULT ((0)) FOR [isSuppressed]
GO

ALTER TABLE [dbo].[LB_BB_provider]  WITH CHECK ADD  CONSTRAINT [FK__provider__availabilityProvider] FOREIGN KEY([availabilityProviderID])
REFERENCES [dbo].[LB_BB_availabilityProvider] ([ID])
GO

ALTER TABLE [dbo].[LB_BB_provider] CHECK CONSTRAINT [FK__provider__availabilityProvider]
GO

