﻿CREATE TABLE [dbo].[uCommerce_ProductCatalogGroupShippingMethodMap] (
    [ProductCatalogGroupId] INT NOT NULL,
    [ShippingMethodId]      INT NOT NULL,
    CONSTRAINT [uCommerce_PK_ProductCatalogGroupShippingMethodMap] PRIMARY KEY CLUSTERED ([ProductCatalogGroupId] ASC, [ShippingMethodId] ASC),
    CONSTRAINT [uCommerce_FK_ProductCatalogGroupShippingMethodMap_ProductCatalogGroup] FOREIGN KEY ([ProductCatalogGroupId]) REFERENCES [dbo].[uCommerce_ProductCatalogGroup] ([ProductCatalogGroupId]),
    CONSTRAINT [uCommerce_FK_ProductCatalogGroupShippingMethodMap_ShippingMethod] FOREIGN KEY ([ShippingMethodId]) REFERENCES [dbo].[uCommerce_ShippingMethod] ([ShippingMethodId])
);

