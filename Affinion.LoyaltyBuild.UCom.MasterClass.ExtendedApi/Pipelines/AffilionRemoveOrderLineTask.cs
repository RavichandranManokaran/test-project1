﻿using Affinion.LoyaltyBuild.UCom.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Pipelines;
using UCommerce.Pipelines.Transactions.Baskets.AddToBasket;
using UCommerce.Pipelines.UpdateLineItem;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi.Pipelines
{
    public class AffilionRemoveOrderLineTask : IPipelineTask<IPipelineArgs<UpdateLineItemRequest, UpdateLineItemResponse>>
    {
        // Methods
        public PipelineExecutionResult Execute(IPipelineArgs<UpdateLineItemRequest, UpdateLineItemResponse> subject)
        {
            if (subject.Request.Quantity == 0)
            {
                List<OrderLine> existingOrderLine = subject.Request.OrderLine.PurchaseOrder.OrderLines.ToList();
                var refOrderLine = subject.Request.OrderLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.RefOrderId);
                if (refOrderLine != null)
                {
                    int index = existingOrderLine.FindIndex(x => x.OrderLineId == Convert.ToInt32(refOrderLine.Value));
                    var orderPropertyremove = subject.Request.OrderLine.PurchaseOrder.OrderLines.ElementAt(index).OrderProperties.FirstOrDefault(x => x.Value.Equals(subject.Request.OrderLine.Sku));
                    if (orderPropertyremove != null)
                    {
                        subject.Request.OrderLine.PurchaseOrder.OrderLines.ElementAt(index).OrderProperties.Remove(orderPropertyremove);
                    }
                }
                List<OrderLine> removealbeOrderLine = new List<OrderLine>();
                foreach (var item in subject.Request.OrderLine.OrderProperties)
                {
                   var relatedOrderLineIndex =  existingOrderLine.FindIndex(x => x.Sku.Equals(item.Value) && x.OrderProperties.Any(y=>y.Key == OrderPropertyConstants.RefOrderId && y.Value.Equals(subject.Request.OrderLine.OrderLineId.ToString())));
                   if (relatedOrderLineIndex != -1)
                   {
                       removealbeOrderLine.Add(existingOrderLine[relatedOrderLineIndex]);
                   }
                }
                foreach (var item in removealbeOrderLine)
                {
                    subject.Request.OrderLine.PurchaseOrder.RemoveOrderLine(item);
                }
                subject.Request.OrderLine.PurchaseOrder.RemoveOrderLine(subject.Request.OrderLine);
            }
            return PipelineExecutionResult.Success;
        }
    }

}

