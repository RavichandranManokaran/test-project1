﻿CREATE PROCEDURE LB_AddBookingPayment
	@OrderLineId INT,
	@CurrencyId INT,
	@OrderPaymentId INT,
	@PaymentMethodId INT,
	@Amount INT,
	@ReferenceId NVARCHAR(MAX),
	@CreatedBy VARCHAR(50),
	@CommissionAmount MONEY = NULL,
	@ProviderAmount MONEY = NULL,
	@ProcessingFee MONEY = NULL
	--@tmp_DueTypeId varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE 
		@OrderId INT,
		@OrderLinePaymentId INT,
		@BookingId VARCHAR(MAX)
	SELECT @OrderId = OrderId FROM uCommerce_OrderLine WHERE OrderLineId = @OrderLineId
	SELECT @BookingId = BookingId FROM LB_BookingInfo WHERE OrderLineId = @OrderLineID

	IF (@OrderPaymentId = 0)
	BEGIN
		INSERT INTO [dbo].[uCommerce_Payment]
			([TransactionId]
			,[PaymentMethodName]
			,[Created]
			,[PaymentMethodId]
			,[Fee]
			,[FeePercentage]
			,[PaymentStatusId]
			,[Amount]
			,[OrderId]
			,[FeeTotal]
			,[ReferenceId])
		 SELECT
			NULL
			,PM.Name
			,GETDATE()
			,@PaymentMethodId
			,0
			,0
			,1
			,@Amount
			,@OrderId
			,0
			,@ReferenceId
		FROM 
			uCommerce_PaymentMethod PM
		WHERE
			PaymentMethodId = @PaymentMethodId

		SET @OrderPaymentId = SCOPE_IDENTITY()
	END

	IF(@ProviderAmount IS NOT NULL AND @ProviderAmount <> 0)
		INSERT INTO [dbo].[LB_OrderLinePayment]
			([OrderLineID]
			,[CurrencyTypeId]
			,[BookingId]
			,[PaymentMethodID]
			,[ReferenceID]
			,[TotalAmount]
			,[ProviderAmount]
			,[ProcessingFeeAmount]
			,[CommissionAmount]
			,[OrderPaymentID]
			,[CreatedBy]
			,[CreatedDate]
			--,[DueTypeId]
			)
		 VALUES
			(@OrderLineId
			,@CurrencyId
			,@BookingId
			,@PaymentMethodId
			,@ReferenceId
			,@ProviderAmount
			,@ProviderAmount
			,0
			,0
			,@OrderPaymentId
			,@CreatedBy
			,GETDATE()
			--1
			)

	IF(@CommissionAmount IS NOT NULL AND @CommissionAmount <> 0)
	BEGIN
		INSERT INTO [dbo].[LB_OrderLinePayment]
			([OrderLineID]
			,[CurrencyTypeId]
			,[BookingId]
			,[PaymentMethodID]
			,[ReferenceID]
			,[TotalAmount]
			,[ProviderAmount]
			,[ProcessingFeeAmount]
			,[CommissionAmount]
			,[OrderPaymentID]
			,[CreatedBy]
			,[CreatedDate]
			--,[DueTypeId]
			)
		 VALUES
			(@OrderLineId
			,@CurrencyId
			,@BookingId
			,@PaymentMethodId
			,@ReferenceId
			,@CommissionAmount
			,0
			,0
			,@CommissionAmount
			,@OrderPaymentId
			,@CreatedBy
			,GETDATE()
			--2
			)
	END
	
	IF(@ProcessingFee IS NOT NULL AND @ProcessingFee <> 0)
	BEGIN
		INSERT INTO [dbo].[LB_OrderLinePayment]
			([OrderLineID]
			,[CurrencyTypeId]
			,[BookingId]
			,[PaymentMethodID]
			,[ReferenceID]
			,[TotalAmount]
			,[ProviderAmount]
			,[ProcessingFeeAmount]
			,[CommissionAmount]
			,[OrderPaymentID]
			,[CreatedBy]
			,[CreatedDate]
			--,[DueTypeId]
			)
		 VALUES
			(@OrderLineId
			,@CurrencyId
			,@BookingId
			,@PaymentMethodId
			,@ReferenceId
			,@ProcessingFee
			,0
			,@ProcessingFee
			,0
			,@OrderPaymentId
			,@CreatedBy
			,GETDATE()
			--3
			)
	END

	DECLARE @UnMatchedAmount BIT = 1
	IF(@Amount IS NOT NULL AND @Amount>0)
	BEGIN
		DECLARE @Payments TABLE (Amount MONEY,ProviderAmount Money,ProcessingFeeAmount Money,CommissionAmount Money)
		INSERT INTO @Payments
		  
		SELECT  SUM(Amount),SUM(ProviderAmount),SUM(ProcessingFeeAmount),Sum(CommissionAmount) FROM
		(SELECT  SUM(TotalAmount) [Amount],SUM(ProviderAmount) [ProviderAmount],SUM(ProcessingFeeAmount) [ProcessingFeeAmount],SUM(CommissionAmount) [CommissionAmount] 
		FROM LB_OrderLineDue OLD 
				INNER JOIN LB_DueTypes DT ON OLD.DueTypeId = dt.DueTypeId
			WHERE 
				isnull (OLD.IsDeleted,0)=0 AND
				OLD.OrderLineID IN (SELECT OrderLineID FROM [dbo].[LB_FnRelatedOrderLines](@OrderLineId))
			UNION
			SELECT  SUM(-TotalAmount) [Amount],SUM(-ProviderAmount) [ProviderAmount],SUM(-ProcessingFeeAmount) [ProcessingFeeAmount],SUM(-CommissionAmount) [CommissionAmount]
			  FROM LB_OrderLinePayment OLP
			WHERE 
				OLP.OrderLineID IN (SELECT OrderLineID FROM [dbo].[LB_FnRelatedOrderLines](@OrderLineId))
		
		)  Dues
		

		IF (SELECT ISNULL(SUM(Amount),0) FROM @Payments) = @Amount
		BEGIN
			SET @UnMatchedAmount = 0

			INSERT INTO [dbo].[LB_OrderLinePayment]
				([OrderLineID]
				,[CurrencyTypeId]
				,[BookingId]
				,[PaymentMethodID]
				,[ReferenceID]
				,[TotalAmount]
				,[ProviderAmount]
				,[CommissionAmount]
				,[ProcessingFeeAmount]
				,[OrderPaymentID]
				,[CreatedBy]
				,[CreatedDate]
				--,[DueTypeId]
				)
			 SELECT
				@OrderLineId
				,@CurrencyId
				,@BookingId
				,@PaymentMethodId
				,@ReferenceId
				,Amount
				,ProviderAmount
				,CommissionAmount
				,ProcessingFeeAmount
				,@OrderPaymentId
				,@CreatedBy
				,GETDATE()
				--,DueTypeId
			FROM @Payments
		END
	END

	IF(@Amount IS NOT NULL AND @Amount<>0 AND (@Amount < 0 OR @UnMatchedAmount = 1))
	BEGIN
		INSERT INTO [dbo].[LB_OrderLinePayment]
			([OrderLineID]
			,[CurrencyTypeId]
			,[BookingId]
			,[PaymentMethodID]
			,[ReferenceID]
			,[TotalAmount]
			,[ProviderAmount]
			,[ProcessingFeeAmount]
			,[CommissionAmount]
			,[OrderPaymentID]
			,[CreatedBy]
			,[CreatedDate]
			--,[DueTypeId]
			)
		 select 
			@OrderLineId
			,@CurrencyId
			,@BookingId
			,@PaymentMethodId
			,@ReferenceId
			,@Amount
			,0
			,0
			,@Amount
			,@OrderPaymentId
			,@CreatedBy
			,GETDATE()
	END

END
