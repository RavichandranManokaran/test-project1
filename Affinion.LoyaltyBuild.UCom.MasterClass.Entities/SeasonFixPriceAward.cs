﻿using Affinion.LoyaltyBuild.UCom.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;
using Affinion.LoyaltyBuild.UCom.Common.Extension;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class SeasonFixPriceAward : Award, IEntity
    {
        public SeasonFixPriceAward()
        {
            this.Details = new List<SeasonFixPriceAwardDetail>();
        }
        // Fields
        private int? _hashCode;

        // Methods

        public override void Apply(PurchaseOrder order, IList<OrderLine> orderLines)
        {
            if (orderLines != null && orderLines.Any() && this.Details != null)
            {
                List<SeasonFixPriceAwardDetail> seasonAward = this.Details.ToList();
                foreach (OrderLine line in orderLines)
                {
                    OrderProperty arrivalDateProperty = line.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.ArrivalDateKey);
                    if (arrivalDateProperty != null)
                    {
                        DateTime arrivalDate = arrivalDateProperty.Value.ParseFormatDateTime(true);
                        OrderProperty nights = line.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.NightKey);
                        if (nights != null)
                        {
                            int noOfNights = 0;
                            if (int.TryParse(nights.Value, out noOfNights))
                            {
                                arrivalDate = arrivalDate.AddDays(noOfNights);
                            }
                        }

                        SeasonFixPriceAwardDetail applyAward = seasonAward.FirstOrDefault(x => arrivalDate >= x.StartDate && arrivalDate <= x.EndDate);
                        if (applyAward != null)
                        {
                            Discount discount = this.CreateDiscount(order, orderLines);
                            discount.AmountOffTotal = (line.Price * line.Quantity) - applyAward.AmountOff;
                            order.AddDiscount(discount);
                            discount.AddOrderLine(line);
                            line.AddDiscount(discount);
                        }
                    }
                }
            }
        }

        // Methods
        public new static IQueryable<SeasonFixPriceAward> All()
        {
            return GetRepo().Select();
        }

        public override void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<SeasonFixPriceAward, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            SeasonFixPriceAward objA = obj as SeasonFixPriceAward;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<SeasonFixPriceAward, bool>> expression)
        {
            return All().Any<SeasonFixPriceAward>(expression);
        }

        public static IList<SeasonFixPriceAward> Find(Expression<Func<SeasonFixPriceAward, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<SeasonFixPriceAward>();
        }

        public static SeasonFixPriceAward FirstOrDefault(Expression<Func<SeasonFixPriceAward, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<SeasonFixPriceAward>();
        }

        public new static SeasonFixPriceAward Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<SeasonFixPriceAward> GetRepo()
        {
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<SeasonFixPriceAward>>();

        }


        public override void Save()
        {
            GetRepo().Save(this);
        }

        public static SeasonFixPriceAward SingleOrDefault(Expression<Func<SeasonFixPriceAward, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties
        public virtual IList<SeasonFixPriceAwardDetail> Details { get; set; }

        public virtual int SeasonFixPriceAwardId { get; protected set; }

        public override int Id
        {
            get
            {
                return this.SeasonFixPriceAwardId;
            }
        }

        public virtual string Content { get; set; }
    }




}
