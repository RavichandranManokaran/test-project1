﻿CREATE PROCEDURE [dbo].[LB_GetPriceChangeReport]

@DateFrom Datetime,
@DateTo  Datetime

As
Begin

Select 
	AdditionalData,
	[Time] as CreatedDate
From 
	LB_AuditTrail 
Where 
[Message] = 'LB_RoomAvailability' 
 And 
([Date] >= @DateFrom and [Date] <= @DateTo)

End