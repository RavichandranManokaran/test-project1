﻿CREATE TABLE [dbo].[ChangePasswordHistory]
(
	[ID] [INT] IDENTITY NOT NULL,
	[UniqueId] [nvarchar](128) NOT NUll,
	[UserName] [nvarchar](128) NOT NULL,

CONSTRAINT [PK_dbo.ChangePassword] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
	
)
)
