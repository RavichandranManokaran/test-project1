﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{

    public class SubRate : IEntity
    {
        private int? _hashCode;

        // Methods
        public static IQueryable<SubRate> All()
        {
            return GetRepo().Select();
        }

        public virtual void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<SubRate, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            SubRate objA = obj as SubRate;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<SubRate, bool>> expression)
        {
            return All().Any<SubRate>(expression);
        }

        public static IList<SubRate> Find(Expression<Func<SubRate, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<SubRate>();
        }

        public static SubRate FirstOrDefault(Expression<Func<SubRate, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<SubRate>();
        }

        public static SubRate Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<SubRate> GetRepo()
        {
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<SubRate>>();
            
        }

        public virtual void Save()
        {
            try
            {
                GetRepo().Save(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static SubRate SingleOrDefault(Expression<Func<SubRate, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties

        public virtual int SubRateId { get; protected set; }

        public virtual int Id
        {
            get
            {
                return this.SubRateId;
            }
        }

        public virtual string SubrateItemCode { get; set; }

        public virtual string ItemID { get; set; }

        public virtual decimal VAT { get; set; }

        public virtual string ClientID { get; set; }

        public virtual string Currency { get; set; }

        public virtual bool IsPercentage { get; set; }

        public virtual DateTime CreatedOn { get; set; }

        public virtual DateTime UpdatedOn { get; set; }

        public virtual string CreatedBy { get; set; }


        public virtual decimal Price { get; set; }
    }
}
