﻿
ALTER TABLE [uCommerce_OrderLineDue] ADD
	[SubrateItemCode] VARCHAR(50)
GO
ALTER TABLE [uCommerce_OrderLineDue] DROP COLUMN [SubrateId]
GO
ALTER TABLE [uCommerce_OrderLineSubRates] ADD
	[SubrateItemCode] VARCHAR(50)
GO
ALTER TABLE [uCommerce_OrderLineSubRates] DROP COLUMN [SubrateId]
GO

ALTER TABLE [uCommerce_OrderLineDue] ADD
	[CancellationAmount] MONEY
GO

ALTER TABLE dbo.uCommerce_OrderLinePayment ADD
	CreatedBy VARCHAR(50) NULL,
	CreatedDate DATETIME NULL
GO

ALTER TABLE dbo.uCommerce_OrderLineStatus ADD
	OrderLineID INT NOT NULL
GO


ALTER TABLE dbo.uCommerce_OrderLinePayment ADD
	DueTypeId INT
GO