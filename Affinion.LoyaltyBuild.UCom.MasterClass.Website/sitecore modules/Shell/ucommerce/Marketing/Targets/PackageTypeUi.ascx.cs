﻿using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using Affinion.LoyaltyBuild.UCom.MasterClass.Website.Configuration;
using Affinion.LoyaltyBuild.UCom.MasterClass.Website.CustomSiteCoreDataType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.Presentation.Views.Marketing;
using UCommerce.Presentation.Web.Controls;
using UCommerce.Presentation.Web.Marketing;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.Ucommerce.Marketing.Targets
{
    public partial class PackageTypeUi : ViewEnabledControl<IEditCampaignItemView>, ITargetUi
    {
        List<string> datatypeList;

        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (datatypeList == null)
            //{
            //    DataType PackageType = DataType.All().Where(x => x.TypeName == DataTypeConstant.PackageType).ToList().FirstOrDefault();
            //    if (PackageType != null)
            //    {
            //        datatypeList = PackageType.DataTypeEnums.ToList();
            //    }
            //    datatypeList.Insert(0, new DataTypeEnum() { Value = "" });
            //}
            datatypeList = SiteConfiguration.GetSiteCoreItem(CustomDataTypeConstant.PackageType);
            drpPackageType.Items.Clear();
            drpPackageType.Items.Add("Select Package type");
            if (datatypeList != null)
            {
                foreach (var item in datatypeList)
                {
                    drpPackageType.Items.Add(new ListItem() { Text = item, Value = item });
                }
            }
            PackageTypeTarget PackageTypeData = this.DataSource as PackageTypeTarget;
            this.DeleteButton.OnClientClick = string.Format("return confirm('Delete');");
            if (datatypeList.Any(x => x == PackageTypeData.PackageType))
            {
                drpPackageType.SelectedValue = PackageTypeData.PackageType;
            }
            this.ReadOnlyMessage.Text = this.TargetToString(PackageTypeData);
        }

        /// <summary>
        /// Save Package detail
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveButton_Click(object sender, ImageClickEventArgs e)
        {
            PackageTypeTarget PackageTypeData = this.DataSource as PackageTypeTarget;
            PackageTypeData.PackageType = drpPackageType.SelectedValue;
            PackageTypeData.Save();
            this.ReadOnlyMessage.Text = this.TargetToString(PackageTypeData);
            this.ToggleEditor();
        }

        /// <summary>
        /// enable edit & display view in page
        /// </summary>
        private void ToggleEditor()
        {
            this.NonEditModePlaceHolder.Visible = !this.NonEditModePlaceHolder.Visible;
            this.EditButton.Visible = !this.EditButton.Visible;
            this.EditModePlaceHolder.Visible = !this.EditModePlaceHolder.Visible;
            this.SaveButton.Visible = !this.SaveButton.Visible;
        }

        /// <summary>
        /// View Packagetype Act
        /// </summary>
        /// <param name="PackageTypeTarget"></param>
        /// <returns></returns>
        private string TargetToString(PackageTypeTarget PackageTypeTarget)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendFormat("Package type ");
            stringBuilder.AppendFormat("{0} ", PackageTypeTarget.PackageType);
            return stringBuilder.ToString();
        }

        /// <summary>
        /// Delete Package type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteButton_Click(object sender, ImageClickEventArgs e)
        {
            PackageTypeTarget PackageTypeData = this.DataSource as PackageTypeTarget;
            if (PackageTypeData != null)
            {
                PackageTypeData.Delete();
            }
            this.Page.Response.Redirect(this.Page.Request.Url.ToString(), true);
        }

        /// <summary>
        /// Edit Package type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditButton_Click(object sender, ImageClickEventArgs e)
        {
            this.ToggleEditor();
        }

        /// <summary>
        /// get set for apply
        /// </summary>
        public bool EnabledForApply
        {
            get;
            set;
        }

        /// <summary>
        /// get set for display
        /// </summary>
        public bool EnabledForDisplay
        {
            get;
            set;
        }

        /// <summary>
        /// Get set for datasource
        /// </summary>
        public object DataSource
        {
            get;
            set;

        }

        /// <summary>
        /// Create new Package Type
        /// </summary>
        /// <returns></returns>
        public object New()
        {
            var packageTypeTarget = PackageTypeTarget.FirstOrDefault(x => x.CampaignItem.CampaignItemId == View.CampaignItem.CampaignItemId) ?? new PackageTypeTarget();
            try
            {
                packageTypeTarget.CampaignItem = View.CampaignItem;
                packageTypeTarget.EnabledForApply = true;
                packageTypeTarget.PackageType = string.Empty;
                packageTypeTarget.Save();
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.MasterClassWebsite, ex, this);
                throw;
            }
            this.DataSource = packageTypeTarget;
            return packageTypeTarget;
        }
    }
}