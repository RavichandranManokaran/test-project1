﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SuperValuHotelInfoSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.SuperValuHotelInfoSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Sitecore.Data.Items" %>

<%--needs to be removed. this is just inserting some space.--%>
<div class="width-full">
      <div class="row slider margin-common">
      <div class="col-md-12">
      </div>
      </div>
</div> 

<div class="container">

    <div class="row">
        <asp:Literal ID="DemoText" runat="server" />
    </div>
    <asp:Repeater ID="ShortcutButtons" runat="server">

        <HeaderTemplate>
            <div class="col-md-12">
            <div class="top-bar float-right">
        </HeaderTemplate>
        <ItemTemplate>
            <div class="loc1 float-left">
                <sc:Link Field="Link" ID="ShortcutLink" Item="<%#(Item)Container.DataItem %>" runat="server">
                    <sc:Image Field="DesktopImage" ID="ShortcutImage" Item="<%#(Item)Container.DataItem %>" runat="server" />
                </sc:Link>
            </div>
        </ItemTemplate>
        <FooterTemplate>
            </div>
            </div>
        </FooterTemplate>

    </asp:Repeater>

    <!-- Example row of columns -->
    <div class="row">
        <div class="col-md-12">
            <div class="col-xs-12 col-md-4">
                
                <sc:Placeholder ID="leftpanel" Key="leftpanel" runat="server" /> 

            </div>

            <div class="col-xs-12 col-md-8">
                <div class="col-md-12">
                    <div class="search-result-top">
                        <p>Durbin: 147 hotels near Kensington</p>
                    </div>

                </div>

                <asp:Repeater ID="HotelsList" runat="server">

                    <ItemTemplate>

                        <div class="col-md-12">
                            <div class="search-info">
                                <div class="col-xs-12 col-md-4 no-padding-left no-padding-right">
                                    <div class="search-pho">
                                        <sc:Image Field="PreviewImage" ID="HotelImage" CssClass="img-responsive img-enlarge" runat="server" Item="<%#(Item)Container.DataItem %>" />
                                    </div>
                                    <div class="price">
                                        <h4 class="no-margin"><asp:Literal ID="PackagePrice" Text="&euro; 110.00" runat="server" /></h4>
                                        <p class="no-margin"><asp:Literal ID="PackageName" Text="4* Double From" runat="server" /></p>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <div class="search-info-detail">
                                        <div class="search-info-detail-top">
                                            <div class="col-xs-12">
                                                <h2 class="no-margin"><sc:Text Field="Name" ID="HotelName" runat="server" Item="<%#(Item)Container.DataItem %>" /></h2>
                                                <h5 class="no-margin"><sc:Text Field="City" ID="HotelCity" runat="server" Item="<%#(Item)Container.DataItem %>" />, <sc:Text Field="Country" ID="HotelCountry" runat="server" Item="<%#(Item)Container.DataItem %>" /></h5>

                                                <div class="search-info-description">
                                                    <p>
                                                        <sc:Text Field="Summary" ID="HotelSummary" runat="server" Item="<%#(Item)Container.DataItem %>" />
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-8">
                                                
                                                <asp:Repeater ID="HotelFacilities" runat="server">

                                                    <HeaderTemplate>
                                                        <ul class="event">
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <li>
                                                            <sc:Link Field="Link" ID="FacilityLink" runat="server" Item="<%#(Item)Container.DataItem %>">
                                                                <sc:Image Field="DesktopImage" ID="FacilityImage" runat="server" Item="<%#(Item)Container.DataItem %>" />
                                                            </sc:Link>
                                                        </li>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </ul>
                                                    </FooterTemplate>

                                                </asp:Repeater>

                                                <div class="travel-rating">
                                                    <p>TripAdvisor Traveller Rating</p>
                                                </div>
                                                <div class="travel-rating-icon">
                                                    <img src="/Resources/images/rating.png" alt="">
                                                </div>
                                                <div class="travel-rating-review">
                                                    <p>723 Reviews </p>
                                                </div>

                                            </div>
                                            <div class="col-xs-12 col-md-4">
                                            </div>

                                            <div class="col-xs-12 col-md-8">
                                                <div class="book-detail-hr">
                                                    <a href="#">More...</a>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-4">
                                                <button type="submit" class="btn btn-primary float-right btn-bg btn-book">Book Now</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </ItemTemplate>

                </asp:Repeater>
                
            </div>
        </div>

    </div>

</div>
