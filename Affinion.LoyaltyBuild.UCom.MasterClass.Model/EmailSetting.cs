﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    public class EmailSetting
    {
        public string SMTPServer { get; set; }

        public string SMTPPort { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string SupplierFrom { get; set; }

        public string SupplierTo { get; set; }

        public string LBAdmin { get; set; }

    }
}
