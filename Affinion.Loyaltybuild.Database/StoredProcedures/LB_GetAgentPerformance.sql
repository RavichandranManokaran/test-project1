﻿--********************************************************************************************************************************************************************************
--CREATE FUNCTION SCRIPTS For AGENT PERFORMANCE
--********************************************************************************************************************************************************************************

CREATE FUNCTION SplitString
(    
      @Input NVARCHAR(MAX),
      @Character CHAR(1)
)
RETURNS @Output TABLE (
      Item NVARCHAR(1000)
)
AS
BEGIN
      DECLARE @StartIndex INT, @EndIndex INT
 
      SET @StartIndex = 1
      IF SUBSTRING(@Input, LEN(@Input) - 1, LEN(@Input)) <> @Character
      BEGIN
            SET @Input = @Input + @Character
      END
 
      WHILE CHARINDEX(@Character, @Input) > 0
      BEGIN
            SET @EndIndex = CHARINDEX(@Character, @Input)
           
            INSERT INTO @Output(Item)
            SELECT SUBSTRING(@Input, @StartIndex, @EndIndex - 1)
           
            SET @Input = SUBSTRING(@Input, @EndIndex + 1, LEN(@Input))
      END
 
      RETURN
END
GO


--********************************************************************************************************************************************************************************
--CREATE STORED PROCEDURE SCRIPTS For AGENT PERFORMANCE
--********************************************************************************************************************************************************************************

CREATE PROCEDURE [dbo].[LB_GetAgentPerformance] (@createdby VARCHAR(max), 
                                              @fromdate  DATE, 
                                              @todate    DATE) 
AS 
  BEGIN 
      SELECT A.createdby [AgentName], 
             A.createddate, 
             A.bookingscount, 
             Isnull(B.confirmedbookingscount, 0) confirmedbookingscount 
      FROM   (SELECT createdby, 
                     createddate, 
                     Count(1) AS bookingscount 
              FROM   LB_RoomReservation 
              WHERE  createdby IN (SELECT * 
                                   FROM   dbo.Splitstring(@createdby, ',')) 
                     AND createddate BETWEEN @fromdate AND @todate 
              GROUP  BY createddate, 
                        createdby) A 
             LEFT OUTER JOIN (SELECT RR.createdby, 
                                     RR.createddate, 
                                     Count(1) AS confirmedbookingscount 
                              FROM   LB_RoomReservation RR 
                              WHERE  EXISTS 
                             (SELECT 1 
                              FROM   [dbo].ucommerce_orderlinestatus AS 
                                     ols 
                              WHERE  OLS.statusid = 6 
                                     AND OLS.orderlineid = 
                                         RR.orderlineid) 
                                     AND RR.createddate BETWEEN 
                                         @fromdate AND @todate 
                                     AND RR.createdby IN (SELECT * 
                                                          FROM 
                                         dbo.Splitstring(@createdby, 
                                         ',')) 
                              GROUP  BY RR.createddate, 
                                        RR.createdby) B 
                          ON A.createdby = B.createdby 
                             AND A.createddate = B.createddate 
  END 

GO 