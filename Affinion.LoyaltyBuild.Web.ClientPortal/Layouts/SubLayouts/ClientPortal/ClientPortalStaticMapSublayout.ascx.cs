﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           ClientPortalStaticMapSublayout.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.ClientPortal
/// Description:           ClientPortalStaticMap load 
/// </summary>

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal
{
    using System;

    public partial class ClientPortalStaticMapSublayout : System.Web.UI.UserControl
    {
        /// <summary>
        /// Gets the value indicationg show or hide the model popup html
        /// This causes a javascript issue in page editor (jquery conflicts with prototype js)
        /// </summary>
        protected bool ShowModelSection 
        { 
            get
            {
                return !(Sitecore.Context.PageMode.IsPageEditor || Sitecore.Context.PageMode.IsPageEditorEditing);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // code goes here                       
        }                     
    }
}