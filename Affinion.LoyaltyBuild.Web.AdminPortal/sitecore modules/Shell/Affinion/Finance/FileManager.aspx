﻿<%@ Page Title="" Language="C#" MasterPageFile="~/sitecore modules/Shell/Affinion/Finance/MainLayout.Master" AutoEventWireup="true" CodeBehind="FileManager.aspx.cs" Inherits="Affinion.LoyaltyBuild.AdminPortal.sitecore_modules.Shell.Affinion.Finance.FileManager" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuItems" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Title" runat="server">
    File Manager
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Body" runat="server">
     <div class="row">
			<div class="col-md-12">
                 <div>
                    <h4 class="title-text font-weight-bold"><asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label></h4>
                </div>  
			</div>
		</div>
    <div class="panel-heading bg-primary">
                                        <h4 class="panel-title">
                                            <span>Oracle File Status updates</span>
                                            <a data-toggle="collapse" data-parent="#accordion1" href="#collapse0" class="show-hide-list">
                                                <span class="glyphicon glyphicon-triangle-top pull-right"></span>
                                            </a>
                                        </h4>
                                    </div>
    <div id="collapse0" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div runat="server" id="ResultDiv" clientidmode="Static">
                                                    <div id="data-grid" class="table-responsive">
                                                        <asp:GridView ID="ResultGridView" runat="server" ClientIDMode="Static" Width="100%" SelectedRowStyle-Font-Bold="true" class="table table-striped table-bordered" AutoGenerateColumns="False" AllowPaging="true" PageSize="10"
                                                                                PagerSettings-NextPageText=" Next " PagerSettings-PreviousPageText=" Previous " PagerSettings-Mode="NumericFirstLast" PagerSettings-FirstPageText=" First " PagerSettings-LastPageText=" Last " PagerSettings-Position="Bottom" PagerStyle-Font-Bold="true" PagerStyle-CssClass="pagination-grid" OnPageIndexChanging="ResultGridView_PageIndexChanging" OnRowDataBound="ResultGridView_RowDataBound">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Oracle File Name">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblOracleFileName" runat="server" Text='<%# Bind("OracleFileName") %>'></asp:Label>
                                                                        <asp:Label ID="lblSequenceNo" runat="server" Text='<%# Bind("SequenceNo") %>' Visible="false"></asp:Label>
                                                                        <asp:LinkButton ID="lnkDownload" runat="server" Text="DownLoad File" OnClick="lnkDownload_Click" Font-Bold="True" ForeColor="#0066FF"></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Oracle File Type">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblOracleFileType" runat="server" Text='<%# Bind("OracleFileType") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Status">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblOracleFileStatus" runat="server" Text='<%# Bind("OracleFileStatus") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>

                                                            <EmptyDataTemplate>
                                                                ---No Records---
                                                            </EmptyDataTemplate>

<PagerSettings FirstPageText=" First " LastPageText=" Last " Mode="NumericFirstLast" NextPageText=" Next " PreviousPageText=" Previous "></PagerSettings>

<PagerStyle CssClass="pagination-grid" Font-Bold="True"></PagerStyle>

                                                            <SelectedRowStyle Font-Bold="True"></SelectedRowStyle>
                                                        </asp:GridView>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
</asp:Content>
