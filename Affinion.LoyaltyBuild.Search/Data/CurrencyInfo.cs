﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           CurrencyInfo.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Search
/// Description:           Code is used to store currency information.
/// </summary>
namespace Affinion.LoyaltyBuild.Search.Data
{
    using Affinion.LoyaltyBuild.Common.Exceptions;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Search.Helpers;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Stores the currency information
    /// </summary>
    public class CurrencyInfo
    {
        /// <summary>
        /// Gets or sets the currency code
        /// </summary>
        public string CurrencyCode { get; set; }

        /// <summary>
        /// Gets or sets the ucommerce price group
        /// </summary>
        public string PriceGroup { get; set; }

        /// <summary>
        /// Gets or sets the language iso code from sitecore
        /// </summary>
        public string Language { get; set; }

        /// <summary>
        /// Gets or sets the price number format. ex: f2, f3, f0, etc.
        /// </summary>
        public string PriceNumberFormat { get; set; }

        /// <summary>
        /// Gets or sets the currency format
        /// </summary>
        public string CurrencyFormat { get; set; }

        /// <summary>
        /// Gets or sets errors
        /// </summary>
        public Exception Errors { get; set; }

        /// <summary>
        /// Initializes a new object and assign default values
        /// </summary>
        public CurrencyInfo()
        {
            this.UpdateCurrency(string.Empty);       
        }

        /// <summary>
        /// Initializes a new object and assign default values
        /// </summary>
        public CurrencyInfo(string currencyCode)
        {
            this.UpdateCurrency(currencyCode);
        }

        /// <summary>
        /// Get the price with currency details
        /// </summary>
        /// <param name="price">The final price (price - discount)</param>
        /// <returns>Returns a formatted string</returns>
        public string GetPriceWithCurrency(decimal price)
        {
            try
            {
                string currencyCode = this.CurrencyCode;
                string symbol = StoreHelper.GetCurrencySymbol(currencyCode);
                string formattedPrice = price.ToString(this.PriceNumberFormat);
                string priceWithCurrency = this.CurrencyFormat.Replace("{{symbol}}", symbol).Replace("{{price}}", formattedPrice);

                return priceWithCurrency;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Search, new AffinionException("Check the price formatings in currency section of the client portal site.", ex), typeof(SupplierData));
                return "---";
            }
        }

        /// <summary>
        /// Initialize for the first time
        /// </summary>
        public void UpdateCurrency(string currencyCode)
        {
            Item rootItem = ItemHelper.RootItem;
            if(rootItem == null)
            {
                this.Errors = new AffinionException("Root item template not found in ancestors.");
                return;
            }

            Collection<Item> allowedCurrencies = SitecoreFieldsHelper.GetMutiListItems(ItemHelper.RootItem, "AllowedCurrencies");
            if (allowedCurrencies != null && allowedCurrencies.Count > 0)
            {
                if (string.IsNullOrEmpty(currencyCode))
                {
                    this.UpdateAttributes(allowedCurrencies[0]);
                }                
                else
                {
                    Item currency = allowedCurrencies.Where(i => i.Name.ToLowerInvariant() == currencyCode.ToLowerInvariant()).FirstOrDefault();
                    this.UpdateAttributes(currency);
                }
            }
            else
            {
                this.Errors = new AffinionException("No currencies found in the root item.");
                this.UpdateAttributes(null);
            }

            this.Errors = null;
        }

        /// <summary>
        /// Update attributes with sitecore item data
        /// </summary>
        /// <param name="currencyItem">The selected currency item</param>
        private void UpdateAttributes(Item currencyItem)
        {
            if (currencyItem != null)
            {
                this.CurrencyCode = SitecoreFieldsHelper.GetValue(currencyItem, "CurrencyCode");
                this.CurrencyFormat = SitecoreFieldsHelper.GetDropLinkFieldValue(currencyItem, "PriceWithCurrencyFormat", "Title");
                this.Language = SitecoreFieldsHelper.GetDropLinkFieldValue(currencyItem, "Language", "Iso");
                this.PriceGroup = SitecoreFieldsHelper.GetValue(currencyItem, "PriceGroup");
                this.PriceNumberFormat = SitecoreFieldsHelper.GetDropLinkFieldValue(currencyItem, "PriceNumberFormat", "Title");
            }
            else
            {
                // ucommerce ships with this price group
                this.PriceGroup = "EUR 15 pct";
                this.CurrencyCode = "EUR";

                // sitecore ships with this language
                this.Language = "en";

                this.PriceNumberFormat = "f2";
                this.CurrencyFormat = "{{symbol}} {{price}}";
            }
        }
    }
}
