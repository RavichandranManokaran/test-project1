﻿CREATE TABLE [dbo].[Links] (
    [ID]             UNIQUEIDENTIFIER CONSTRAINT [DF_Links_ID] DEFAULT (newid()) NOT NULL,
    [SourceDatabase] NVARCHAR (150)   NOT NULL,
    [SourceItemID]   UNIQUEIDENTIFIER NOT NULL,
    [SourceLanguage] NVARCHAR (50)    CONSTRAINT [DF_Links_SourceLanguage] DEFAULT ('') NOT NULL,
    [SourceVersion]  INT              CONSTRAINT [DF_Links_SourceVersion] DEFAULT ((0)) NOT NULL,
    [SourceFieldID]  UNIQUEIDENTIFIER NOT NULL,
    [TargetDatabase] NVARCHAR (150)   NOT NULL,
    [TargetItemID]   UNIQUEIDENTIFIER NOT NULL,
    [TargetLanguage] NVARCHAR (50)    CONSTRAINT [DF_Links_TargetLanguage] DEFAULT ('') NOT NULL,
    [TargetPath]     NTEXT            NOT NULL,
    [TargetVersion]  INT              CONSTRAINT [DF_Links_TargetVersion] DEFAULT ((0)) NOT NULL,
    [DAC_Index]      INT              NULL
);


GO
CREATE CLUSTERED INDEX [DAC_Clustered]
    ON [dbo].[Links]([DAC_Index] ASC);


GO
CREATE NONCLUSTERED INDEX [ndxSourceItemID]
    ON [dbo].[Links]([SourceItemID] ASC);


GO
CREATE NONCLUSTERED INDEX [ndxTargetItemID]
    ON [dbo].[Links]([TargetItemID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [ndxID]
    ON [dbo].[Links]([ID] ASC);

