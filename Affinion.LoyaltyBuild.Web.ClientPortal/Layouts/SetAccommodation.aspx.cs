﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SetAccommodation.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal
/// Description:           Configuration Helper Class to read configuration settings in Web.config file
/// </summary>
#region Using Directives
using Affinion.Loyaltybuild.BusinessLogic.Helper;
using Affinion.LoyaltyBuild.Common.SessionHelper;
using Affinion.LoyaltyBuild.Common.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Affinion.LoyaltyBuild.Common.Exceptions;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using System.Data;
using System.Web.UI.HtmlControls;
using Sitecore.Data;
using Sitecore.Data.Items;
using common = Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.Common.CardValidationCheck;
using Affinion.LoyaltyBuild.Model.Product;
using Affinion.LoyaltyBuild.Api.Booking.Data;
using Affinion.LoyaltyBuild.Api.Booking;
using ucom = UCommerce.EntitiesV2;
using UCommerce.EntitiesV2;
using UCommerce.Runtime;

#endregion
namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts
{
    public partial class SetAccommodation : System.Web.UI.Page
    {
        private List<RoomReservation> roomReservations = new List<RoomReservation>();
        private List<RoomReservationAgeInfo> roomReservationAgeInfo = new List<RoomReservationAgeInfo>();
        private List<int> ChildAgeList = new List<int>();
        int oId;
        public string selectedTheme { get; set; }

        string OrderLineId = string.Empty;
        //Repeater RepeaterChildAge = null;

        //use to manuplate the dropdown list
        private string GuestRange = string.Empty;
        Item PageItem = Sitecore.Context.Item;
        private string SelectText = "Select Title";
        OrderLine orderLine = null;
        /// <summary>
        /// Page load related operations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

            Basket basket = SiteContext.Current.OrderContext.GetBasket();
            ucom.PurchaseOrder order = basket.PurchaseOrder;

            Uri pageUri = Request.Url;
            QueryStringHelper helper = new QueryStringHelper(pageUri);
            OrderLineId = !string.IsNullOrEmpty(helper.GetValue("orderid")) ? helper.GetValue("orderid") : string.Empty;
            int.TryParse(OrderLineId, out oId);
            //Add dynamic order property to an order line
            this.orderLine = order.OrderLines.Where(x => x.OrderLineId == oId).FirstOrDefault();

            //load selected theme
            selectedTheme = ThemeSelector.LoadSelectedTheme();
            LabelValidateFieldErrorMsg.Text = string.Empty;


            if (!IsPostBack)
            {
                string Rooms = helper.GetValue("Rooms");
                GuestRange = helper.GetValue("GuestRange");

                int count = 0;
                var isValid = Int32.TryParse(Rooms, out count);

                if (string.IsNullOrEmpty(OrderLineId))
                {
                    return;
                }

                // Populate the accodamation informaiton from database
                PopulateAccommodationData(count);

                // Populate the list of child ages

                PopulateChildAgeFromDB();
            }
        }

        protected string PassedOrderLineId
        {
            get
            {
                return OrderLineId;
            }
        }

        /// <summary>
        /// Use to populate the AccommodationData
        /// </summary>
        private void PopulateAccommodationData(int roomCount)
        {
            try
            {
                List<RoomReservation> roomReservations = new List<RoomReservation>();

                // Call Database to retrive saved data.
                List<RoomReservation> ListRoomReservation = BasketHelper.GetRoomReservationByOrderLineId(OrderLineId);
                if (ListRoomReservation.Count > 0)
                {
                    foreach (var roomReservationData in ListRoomReservation)
                    {
                        int oid;
                        int.TryParse(roomReservationData.OrderLineId, out oid);
                        if (oid == orderLine.OrderLineId)
                        {
                            roomReservationData.Title = string.IsNullOrEmpty(orderLine.GetOrderProperty(Constants.LeadGuestTitle).Value) ? string.Empty : orderLine.GetOrderProperty(Constants.LeadGuestTitle).Value;
                            roomReservationData.FirstName = string.IsNullOrEmpty(orderLine.GetOrderProperty(Constants.LeadGuestFirstName).Value) ? string.Empty : orderLine.GetOrderProperty(Constants.LeadGuestFirstName).Value;
                            roomReservationData.LastName = string.IsNullOrEmpty(orderLine.GetOrderProperty(Constants.LeadGuestLastName).Value) ? string.Empty : orderLine.GetOrderProperty(Constants.LeadGuestLastName).Value;
                        }
                    }

                    this.RepeaterGuestAccordian.DataSource = ListRoomReservation;
                    this.RepeaterGuestAccordian.DataBind();

                    //this.TextSpecialRequest.Text = ListRoomReservation[0].SpecialRequest;
                }
                else
                {
                    for (int i = 0; i < roomCount; i++)
                    {
                        roomReservations.Add(new RoomReservation { FirstName = string.Empty, LastName = string.Empty, NoOfGuest = 1, GuestName = string.Empty, OrderLineId = string.Empty, SpecialRequest = string.Empty });
                    }
                    this.RepeaterGuestAccordian.DataSource = roomReservations.GetRange(0, roomCount);
                    this.RepeaterGuestAccordian.DataBind();
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Populate ChildAge From DB
        /// </summary>
        private void PopulateChildAgeFromDB()
        {
            try
            {
                //Clear the list
                ChildAgeList.Clear();

                // Fill the age into the list
                ChildAgeList = BasketHelper.PopulateChildAgeFromDB(OrderLineId.Trim());
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Save Guest details in to DB
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            #region Local variables
            string guestName = string.Empty;
            string specialRequest = string.Empty;
            int numberOfGuest = 0;
            int numberOfChild = 0;
            int numberOfAdult = 0;

            TextBox TextGuestFullName = null;
            TextBox TextSpecialRequest = null;
            DropDownList DropdownGuest = null;
            DropDownList DropdownChild = null;
            DropDownList DropdownAdult = null;
            Repeater ChildAge = null;
            DropDownList ddlTitle = null;
            TextBox txtFirstName = null;
            TextBox txtLastName = null;

            //HiddenField HiddenRoomReservationId = null;

            #endregion


            try
            {
                Uri pageUri = Request.Url;
                QueryStringHelper helper = new QueryStringHelper(pageUri);
                string orderLineId = helper.GetValue("orderid");
                string roomReservationId = string.Empty;

                foreach (RepeaterItem item in RepeaterGuestAccordian.Items)
                {
                    if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                    {
                        DropdownGuest = (DropDownList)item.FindControl("DropdownGuest");
                        DropdownChild = (DropDownList)item.FindControl("DropDownNoOfChildren");
                        DropdownAdult = (DropDownList)item.FindControl("DropDownNoOfAdults");
                        TextGuestFullName = (TextBox)item.FindControl("TextGuestFullName");
                        TextSpecialRequest = (TextBox)item.FindControl("TextSpecialRequest");
                        ddlTitle = item.FindControl("ddlTitle") as DropDownList;
                        txtFirstName = item.FindControl("txtFirstName") as TextBox;
                        txtLastName = item.FindControl("txtLastName") as TextBox;

                        //HiddenRoomReservationId = (HiddenField)item.FindControl("HiddenRoomReservationId");

                        if (ddlTitle != null && txtLastName != null && txtFirstName != null)
                        {
                            guestName = CredictCardCheck.CredictCard(ddlTitle.SelectedItem.Text) + " " + CredictCardCheck.CredictCard(txtFirstName.Text) + " " + CredictCardCheck.CredictCard(txtLastName.Text);
                            //if (TextGuestFullName != null)
                            //    guestName = CredictCardCheck.CredictCard(TextGuestFullName.Text.Trim());      //Old code for full guest Name modified by harsh

                            orderLine[Constants.LeadGuestTitle] = ddlTitle.SelectedItem.Text;
                            orderLine[Constants.LeadGuestFirstName] = txtFirstName.Text;
                            orderLine[Constants.LeadGuestLastName] = txtLastName.Text;
                        }

                        if (TextSpecialRequest != null)
                            specialRequest = CredictCardCheck.CredictCard(TextSpecialRequest.Text);
                        orderLine[Constants.Request] = specialRequest;
                        orderLine.Save();
                        if (DropdownGuest != null)
                            numberOfGuest = Convert.ToInt32(DropdownGuest.SelectedValue);

                        if (DropdownAdult != null)
                            numberOfAdult = Convert.ToInt32(DropdownAdult.SelectedValue);

                        if (DropdownChild != null)
                            numberOfChild = Convert.ToInt32(DropdownChild.SelectedValue);

                        //if(HiddenRoomReservationId != null)
                        //    roomReservationId = HiddenRoomReservationId.Value.ToString();
                        // Populate list of roomReservations
                        roomReservations.Add(new RoomReservation { OrderLineId = orderLineId, NoOfGuest = numberOfGuest, GuestName = guestName, NoOfAdults = numberOfAdult, NoOfChildren = numberOfChild, SpecialRequest = specialRequest });

                        ChildAge = (Repeater)item.FindControl("RepeaterChildAge");
                    }

                    // Get the child selection form another repearter
                    if (ChildAge != null)
                    {
                        foreach (RepeaterItem ageDrpList in ChildAge.Items)
                        {
                            if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                            {
                                var ageDropdown = (DropDownList)ageDrpList.FindControl("DropDownAge");
                                if (ageDropdown != null)
                                {
                                    roomReservationAgeInfo.Add(new RoomReservationAgeInfo { Age = int.Parse(ageDropdown.SelectedValue), OrderLineId = Convert.ToInt32(orderLineId) });
                                }
                            }
                        }
                    }
                }
                if (guestName.Length > 0)
                {
                    TextGuestFullName.CssClass = "form-control";
                    LabelValidateFieldErrorMsg.Text = string.Empty;
                    // Save to database
                    BasketHelper.AddGuestDetails(roomReservations);

                    //Save child Age details
                    string GuestChildAge = string.Empty;
                    foreach (var age in roomReservationAgeInfo)
                    {
                        GuestChildAge = GuestChildAge + (age.Age.ToString() + ",");
                    }
                    GuestChildAge = GuestChildAge.TrimEnd(',');
                    BasketHelper.AddGuestChildAgeDetails(GuestChildAge, orderLineId);

                    //Response.Redirect(Request.RawUrl);
                    //Old Code BY Colombo Team
                    this.ClientScript.RegisterStartupScript(this.GetType(), "saveAndCloseWin", "saveAndCloseWin()", true);
                    //New by Dev Team CHN
                    //this.ClientScript.RegisterStartupScript(this.GetType(), "saveAndCloseWin", "window.close();", true);
                }
                else
                {
                    TextGuestFullName.CssClass = "form-control has-error-input";
                    //LabelValidateFieldErrorMsg.Text = "Guest name cannot be blank";
                    string messageFailureText = (PageItem != null && PageItem.Fields["Guest Name Required Message"] != null) ? PageItem.Fields["Guest Name Required Message"].Value : string.Empty;
                    LabelValidateFieldErrorMsg.Text = messageFailureText;
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Bound item on data binding of repeater
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RepeaterGuestAccordian_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            // Default value is = 4
            int numberRange = 4;
            bool result = Int32.TryParse(GuestRange, out numberRange);
            TextBox TextGuestFullName = (TextBox)e.Item.FindControl("TextGuestFullName");
            TextGuestFullName.Visible = false;
            if (e != null)
            {
                DropDownList DropdownGuest = (DropDownList)e.Item.FindControl("DropdownGuest");
                DropDownList DropdownTitle = (DropDownList)e.Item.FindControl("ddlTitle");
                TextBox FirstName = e.Item.FindControl("txtFirstName") as TextBox;
                TextBox LastName = e.Item.FindControl("txtLastName") as TextBox;
                string fNameTxt = SitecoreFieldsHelper.GetItemFieldValue(Sitecore.Context.Item, "First Name Placeholder Text");
                string lNameTxt = SitecoreFieldsHelper.GetItemFieldValue(Sitecore.Context.Item, "Last Name Placeholder Text");
                FirstName.Attributes.Add("Placeholder", "First Name");
                LastName.Attributes.Add("Placeholder", "Last Name");
                TextBox TextSpecialRequest = (TextBox)e.Item.FindControl("TextSpecialRequest");

                //HiddenField HiddenRoomReservationId = (HiddenField)e.Item.FindControl("HiddenRoomReservationId");

                DropDownList DropDownNoOfAdults = (DropDownList)e.Item.FindControl("DropDownNoOfAdults");
                DropDownList DropDownNoOfChildren = (DropDownList)e.Item.FindControl("DropDownNoOfChildren");

                // Child Age repeater
                Repeater RepeaterChildAge = (Repeater)e.Item.FindControl("RepeaterChildAge");

                LoadTitles(DropdownTitle);
                RoomReservation rooms = e.Item.DataItem as RoomReservation;
                if (rooms != null && !string.IsNullOrEmpty(rooms.Title))
                {
                    DropdownTitle.Items.FindByText(rooms.Title).Selected = true;
                }
                DropdownGuest.DataSource = Enumerable.Range(1, numberRange);
                DropdownGuest.DataBind();

                // Binds Adults and child details
                DropDownNoOfChildren.SelectedIndexChanged += DropDownNoOfChildren_SelectedIndexChanged;

                if (orderLine != null && orderLine.OrderLineId > 0 && orderLine.GetOrderProperty(Constants.ProductType).Value.ToLower().Equals("bedbanks")) //TODO: Variant SKU should come from sitcore
                {
                    int MaxAdults = 0;
                    int.TryParse(orderLine.GetOrderProperty("MaxAdults").Value, out MaxAdults);
                    int MaxChild = 0;
                    int.TryParse(orderLine.GetOrderProperty("MaxChildren").Value, out MaxChild);
                    if (MaxAdults > 0)
                    {
                        DropDownNoOfAdults.DataSource = Enumerable.Range(1, MaxAdults);
                    }
                    else
                    {
                        int adults = 0;
                        int.TryParse(orderLine.GetOrderProperty("_NoOfAdult").Value, out adults);
                        DropDownNoOfAdults.DataSource = Enumerable.Range(1, adults);
                    }
                    DropDownNoOfAdults.DataBind();
                    if (MaxChild > 0)
                    {
                        DropDownNoOfChildren.DataSource = Enumerable.Range(0, MaxChild);
                    }
                    else
                    {
                        int child = 0;
                        int.TryParse(orderLine.GetOrderProperty("_NoOfChildren").Value, out child);
                        if (child > 0)
                            DropDownNoOfChildren.DataSource = Enumerable.Range(0, child);
                        else
                            DropDownNoOfChildren.DataSource = Enumerable.Range(0, 1);
                    }
                    DropDownNoOfChildren.DataBind();
                }
                else
                {
                    DropDownNoOfAdults.DataSource = Enumerable.Range(1, 4);
                    DropDownNoOfAdults.DataBind();

                    DropDownNoOfChildren.DataSource = Enumerable.Range(1, 11);
                    DropDownNoOfChildren.DataBind();
                }

                ListItem selectNoOfAdults = new ListItem("Please Select", "0");
                DropDownNoOfAdults.Items.Insert(0, selectNoOfAdults);

                ListItem selectNoOfChildren = new ListItem("Please Select", "0");
                DropDownNoOfChildren.Items.Insert(0, selectNoOfChildren);

                DropdownGuest.SelectedValue = (((RoomReservation)(e.Item.DataItem)).NoOfGuest).ToString();
                DropDownNoOfAdults.SelectedValue = (((RoomReservation)(e.Item.DataItem)).NoOfAdults).ToString();
                DropDownNoOfChildren.SelectedValue = (((RoomReservation)(e.Item.DataItem)).NoOfChildren).ToString();

                // Bind the Dropdown child
                BindChildAges(RepeaterChildAge, ((RoomReservation)(e.Item.DataItem)).NoOfChildren);
            }
        }

        private void LoadTitles(DropDownList ddlTitle)
        {
            Sitecore.Data.Database currentDb = Sitecore.Context.Database;

            //Load location data to a locationList
            Item titleList = currentDb.GetItem(Constants.TitleListPath);
            ddlTitle.AppendDataBoundItems = true;
            if (titleList != null)
            {
                var titles = (from loc in titleList.Children
                              orderby loc.DisplayName
                              select loc.DisplayName);

                //Bind locationList data to the drop down
                //TitleDropDown.SelectedIndex = 0;
                ddlTitle.DataSource = titles != null ? titles.ToList() : null;
                ddlTitle.DataBind();
            }
            SelectText = SitecoreFieldsHelper.GetItemFieldValue(Sitecore.Context.Item, "Title DropDown Default Text");
            ListItem selectTitle = new ListItem(SelectText, 0.ToString());
            ddlTitle.Items.Insert(0, selectTitle);
        }



        /// <summary>
        /// Use to bind the RepeaterChildAge when there is a record
        /// </summary>
        /// <param name="RepeaterChildAge"></param>
        /// <param name="childCount"></param>
        private void BindChildAges(Repeater RepeaterChildAge, int childCount)
        {
            List<int> childAges = new List<int>();

            ChildAgeList = BasketHelper.PopulateChildAgeFromDB(OrderLineId);
            ChildAgeList.Reverse();

            for (int i = 0; i < childCount; i++)
            {
                childAges.Add(i);
            }

            RepeaterChildAge.DataSource = childAges;
            RepeaterChildAge.DataBind();

            // ToDO: Write SP to get the current age for that particular child and set the seleted value

            if (RepeaterChildAge != null)
            {
                // use to iterate the ChildAgeList positions
                int count = 0;
                foreach (RepeaterItem ageDrpList in RepeaterChildAge.Items)
                {
                    //if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                    //{
                    var ageDropdown = (DropDownList)ageDrpList.FindControl("DropDownAge");
                    if (ageDropdown != null)
                    {
                        if (ChildAgeList.Count > 0 && ChildAgeList[0] != 0)
                        {
                            ageDropdown.SelectedValue = Convert.ToString(ChildAgeList[count] - 1);
                        }
                        else
                        {
                            ageDropdown.SelectedValue = "";
                        }
                        // roomReservationAgeInfo.Add(new RoomReservationAgeInfo { Age = ageDropdown.SelectedIndex + 1, OrderLineId = Convert.ToInt32(orderLineId), RoomReservationId = roomReservationId });
                    }
                    else
                    {
                        ageDropdown.SelectedValue = "";
                    }

                    count++;
                    //}
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DropDownNoOfChildren_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<int> childAges = new List<int>();
            DropDownList DropDownNoOfChildren = sender as DropDownList;


            // Bind Child age repeater with No of children selection
            for (int i = 0; i < Convert.ToInt32(DropDownNoOfChildren.SelectedValue); i++)
            {
                childAges.Add(i);
            }

            foreach (var item in DropDownNoOfChildren.Parent.Controls)
            {
                if (item.GetType().Name.Equals("Repeater"))
                {
                    if (item != null)
                    {
                        Repeater RepeaterChildAge = item as Repeater;
                        RepeaterChildAge.DataSource = childAges;
                        RepeaterChildAge.DataBind();
                    }
                }
            }
        }

    }
}
