﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           Settings.cs
/// Sub-system/Module:     ClientSetup(VS project name)
/// Description:           Settings create uCommerce store and related basic details
/// </summary>

#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2; 
#endregion

namespace Affinion.LoyaltyBuild.ClientSetup.ClientSetup
{
    /// <summary>
    /// Initalize the default setting related to uCommerce
    /// </summary>
    internal sealed class Settings
    {
        //private void Configure()
        //{            
        //    CreateProductDefinitions();            
        //}

        public void Configure(string newDefinition)
        {
            CreateProductDefinitions(newDefinition);   
        }

        /// <summary>
        /// Use to create Product definition for several type
        /// </summary>
        //private void CreateProductDefinitions()
        //{
        //    CreateSVHotelProductDefinition();            
        //}


        /// <summary>
        /// Use to create Product definition for several type
        /// </summary>
        /// <param name="newDefinition">newDefinition</param>
        private static void CreateProductDefinitions(string newDefinition)
        {
            CreateSVHotelProductDefinition(newDefinition);
        }

        /// <summary>
        /// Create new definition
        /// </summary>
        /// <param name="newDefinition">newDefinition</param>
        private static void CreateSVHotelProductDefinition(string newDefinition)
        {
            var productDefinition = CreateProductDefinition(string.Concat(newDefinition, "HotelOffers"));
        }

        /// <summary>
        /// Create Product Definition
        /// </summary>
        /// <param name="name">name of category definition</param>
        /// <returns>newly created ProductDefinition</returns>
        private static ProductDefinition CreateProductDefinition(string name)
        {
            var productDefinition = ProductDefinition.SingleOrDefault(d => d.Name == name) ?? new ProductDefinition();

            productDefinition.Name = name;
            productDefinition.Save();

            return productDefinition;
        }
    }
}
