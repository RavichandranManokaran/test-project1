﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion Inernational. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           OccupancyTypeControlAdapter.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.SupplierSetup.CustomDataTypes
/// Description:           Adding custom data type OccupancyType 
/// </summary>

#region Using derectives
using System.Web.UI;
using UCommerce.Presentation.Web.Controls;
#endregion

namespace Affinion.LoyaltyBuild.SupplierSetup.CustomDataTypes
{
    public class DropDownListControlAdapter : IControlAdapter
    {
        public bool Adapts(Control control)
        {
            //We can easy tell that we know to Adapt and get a value from a SafeDropDownList
            //If we can cast the control to one of that type, we knows hhow to get the value.
            return (control as SafeDropDownList != null);
        }

        public object GetValue(Control control)
        {
            //Since the framework was told that we adapt the control, it will ask for the value.
            //We know we can cast it, so we do that and call SelectedValue.
            return (control as SafeDropDownList).SelectedValue;
        }

    }
}
