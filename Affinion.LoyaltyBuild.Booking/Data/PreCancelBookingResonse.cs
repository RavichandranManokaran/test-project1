﻿using Affinion.LoyaltyBuild.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Api.Booking.Data
{
    /// <summary>
    /// Pre cancel response
    /// </summary>
    public class PreCancelBookingResonse : ValidationResult
    {
        /// <summary>
        /// cancellation cost
        /// </summary>
        public double CancellationCost { get; set; }
    }

}
