﻿CREATE TABLE [dbo].[LB_SubrateTemplates] (
    [SubrateTemplateID]   INT            IDENTITY (1, 1) NOT NULL,
    [SubrateTemplateCode] NVARCHAR (50)  NOT NULL,
    [SubrateTemplate]     NVARCHAR (250) NOT NULL,
    [CreatedOn]           DATETIME       CONSTRAINT [DF_uCommerce_SubrateTemplates_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [UpdateOn]            DATETIME       CONSTRAINT [DF_uCommerce_SubrateTemplates_UpdateOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]           NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_uCommerce_SubrateTemplates] PRIMARY KEY CLUSTERED ([SubrateTemplateID] ASC)
);

