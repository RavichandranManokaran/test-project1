﻿CREATE TABLE [dbo].[uCommerce_DefinitionFieldDescription] (
    [DefinitionFieldDescriptionId] INT            IDENTITY (1, 1) NOT NULL,
    [CultureCode]                  NVARCHAR (60)  NOT NULL,
    [DisplayName]                  NVARCHAR (255) NOT NULL,
    [Description]                  NVARCHAR (MAX) NOT NULL,
    [DefinitionFieldId]            INT            NOT NULL,
    CONSTRAINT [PK_DefinitionFieldDescription] PRIMARY KEY CLUSTERED ([DefinitionFieldDescriptionId] ASC),
    CONSTRAINT [FK_uCommerce_DefinitionFieldDescription_uCommerce_DefinitionField] FOREIGN KEY ([DefinitionFieldId]) REFERENCES [dbo].[uCommerce_DefinitionField] ([DefinitionFieldId])
);

