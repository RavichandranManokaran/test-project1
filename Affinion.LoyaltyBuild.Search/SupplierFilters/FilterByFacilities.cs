﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           FilterByFacilities.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Search (VS project name)
/// Description:           Filter by Facilities
/// </summary>
/// 
using System;
using System.Web;
using System.Collections.Generic;
namespace Affinion.LoyaltyBuild.Search.SupplierFilters
{
    public class FilterByFacilities : ISupplierFilter
    {
        public bool IncludeSupplier(SupplierFilterData filterData)
        {
           
            if (filterData != null && filterData.SearchTerm != null && filterData.ResultsItem == null || string.IsNullOrEmpty(filterData.ResultsItem.Facilities) || string.IsNullOrEmpty(filterData.SearchTerm.Facilities))
            {
                return true;
            }
            
            string resultItemFacilities = filterData.ResultsItem.Facilities.ToLowerInvariant();
            string keywordFacilities = filterData.SearchTerm.Facilities.ToLowerInvariant();

            if (keywordFacilities == "all")
            {
                return true;
            }

            return FilterResultContent(keywordFacilities, resultItemFacilities);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="keywordValue"></param>
        /// <param name="resultItemValue"></param>
        /// <returns></returns>
        private bool FilterResultContent(string keywordValue, string resultItemValue)
        {
            foreach (var value in keywordValue.Split(','))
            {
                string currentkeywordFacilities ;
                currentkeywordFacilities = value;
                bool flag = (resultItemValue.Contains(currentkeywordFacilities));
               
                if(!flag)
                {
                    return false;
                }                
            }
            return true;
        }

    }
}
