///<summary>
/// � 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           ThemeChangePrimaryLayout.cs
/// Sub-system/Module:     Client portal(NewThemeSelect)
/// Description:           load the css colour names from sitecore and it binds to ThemeChangeSublayout. 
///  </summary>
namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.PrimaryLayouts
{
    #region Using Directives
    using Sitecore.Data.Items;
    using System;
    using System.Web.UI;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using System.Web.UI.HtmlControls;

    #endregion

    public partial class ThemeChangePrimaryLayout : BasePrimaryLayout
    {
        /// <summary>
        /// Gets the main form
        /// </summary>
        public override HtmlForm MainForm
        {
            get
            {
                return this.mainform;
            }
        }

        /// <summary>
        /// Bind theme change item from sitecore
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                //geeting items from sitecore and assign to the currentItem veriable
                Item currentItem = Sitecore.Context.Item;
                //current item value assign to field variable
                string Field = currentItem.Fields["Theme"].Value;

                if (Field == "Green")
                {
                    string path = "~/../Resources/styles/" + Field + "/" + Field + ".css";
                    //path is taking and reffer stylesheet using its ID and replace the href 
                    lnkCSS.Attributes["href"] = path;
                }

                else if (Field == "Orange")
                {
                    string path = "~/../Resources/styles/" + Field + "/" + Field + ".css";
                    //path is taking and reffer stylesheet using its ID and replace the href 
                    lnkCSS.Attributes["href"] = path;
                }

                else if (Field == "Gray")
                {
                    string path = "~/../Resources/styles/" + Field + "/" + Field + ".css";
                    //path is taking and reffer stylesheet using its ID and replace the href 
                    lnkCSS.Attributes["href"] = path;
                }

                else if (Field == "null")
                {
                    string path = "~/../Resources/styles/" + "Orange" + "/" + "Orange" + ".css";
                    //path is taking and reffer stylesheet using its ID and replace the href 
                    lnkCSS.Attributes["href"] = path;
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientSetup, ex, this);
            }
        }
    }
}

