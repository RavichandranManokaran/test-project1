﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SeasonUnitDiscountAwardUi.ascx.cs" Inherits="Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.Ucommerce.Marketing.Awards.SeasonUnitDiscountAwardUi" %>
<style>
    .width80 {
        display: inline !important;
    }
</style>
<table cellpadding="0" cellspacing="0" style="width: 100%;">
    <tr>
        <td>
            <asp:Localize ID="Localize1" runat="server" meta:resourcekey="AmountOffUnit" />
            <asp:PlaceHolder runat="server" ID="NonEditModePlaceHolder">
                <div id="dvGrid" style="padding: 10px;">
                    <asp:Label ID="lblError" Text="" runat="server" ForeColor="Red"></asp:Label>
                    <asp:GridView ID="GrdSeasonUnitDiscount" runat="server"
                        AutoGenerateColumns="false" Font-Names="Arial"
                        AllowPaging="true" ShowFooter="true"
                        OnPageIndexChanging="OnPaging" OnRowEditing="EditSeasonUnitDiscountDetail"
                        OnRowUpdating="UpdateAwardDetail" OnRowCancelingEdit="CancelEdit" 
                        OnRowDataBound="GrdSeasonUnitDiscount_RowDataBound"
                        PageSize="10">
                        <Columns>
                            <asp:TemplateField ItemStyle-Width="50px" HeaderText="Start Date">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdDetailId" runat="server" Value='<%# Eval("SeasonUnitDiscountAwardDetailId") %>'></asp:HiddenField>
                                    <asp:Label ID="lblStartDate" runat="server" Text='<%# Eval("StartDate", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:HiddenField ID="hdDetailId" runat="server" Value='<%# Eval("SeasonUnitDiscountAwardDetailId") %>'></asp:HiddenField>
                                    <asp:TextBox ID="txtStartDate" runat="server" Text='<%# Eval("StartDate", "{0:dd/MM/yyyy}")%>' placeholder="dd/MM/yyyy"></asp:TextBox>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtStartDate" runat="server" placeholder="dd/MM/yyyy"></asp:TextBox>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="50px" HeaderText="End Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%# Eval("EndDate", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtEndDate" runat="server" Text='<%# Eval("EndDate", "{0:dd/MM/yyyy}")%>' placeholder="dd/MM/yyyy" ValidationGroup="update"></asp:TextBox>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtEndDate" runat="server" placeholder="dd/MM/yyyy"></asp:TextBox>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="25px" HeaderText="Discount">
                                <ItemTemplate>
                                    <asp:Label ID="lblAmountOff" runat="server" Text='<%# Eval("AmountOff") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtAmountOff" runat="server" Text='<%# Eval("AmountOff")%>' ValidationGroup="update"></asp:TextBox>
                                    <asp:RequiredFieldValidator ValidationGroup="update" ID="RequiredValidator" runat="server" ErrorMessage="*Required" ControlToValidate="txtAmountOff" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtAmountOff" runat="server" ValidationGroup="insert"></asp:TextBox>
                                    <asp:RequiredFieldValidator ValidationGroup="insert" ID="RequiredValidator" runat="server" ErrorMessage="*Required" ControlToValidate="txtAmountOff" ForeColor="Red" Display="Dynamic"></asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="25px" HeaderText="(%) Discount">
                                <ItemTemplate>
                                    <asp:Label ID="lblPercentage" runat="server" Text='<%# Eval("IsPercentage").ToString() == "True" ? "Yes" : "No" %>' ></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:CheckBox ID="chkPercentage" runat="server" Checked='<%# Eval("IsPercentage")%>'></asp:CheckBox>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:CheckBox ID="chkPercentage" runat="server" ValidationGroup="insert"></asp:CheckBox>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Width="25px" HeaderText="On OrderLine">
                                <ItemTemplate>
                                    <asp:Label ID="lblOrderLine" runat="server" Text='<%# Eval("OnOrderLine").ToString() == "True" ? "Yes" : "No" %>' ></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:CheckBox ID="chkOnOrderLine" runat="server" Checked='<%# Eval("OnOrderLine")%>'></asp:CheckBox>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:CheckBox ID="chkOnOrderLine" runat="server" ValidationGroup="insert"></asp:CheckBox>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkRemove" runat="server"
                                        CommandArgument='<%# Eval("SeasonUnitDiscountAwardDetailId")%>'
                                        OnClientClick="return confirm('Do you want to delete?')"
                                        Text="Delete" OnClick="DeleteAwardDetail"></asp:LinkButton>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Button ID="btnAdd" runat="server" Text="Add"
                                        OnClick="AddNewAwardDetail" />
                                </FooterTemplate>

                            </asp:TemplateField>
                            <asp:CommandField ShowEditButton="True" ControlStyle-Width="80px" ControlStyle-CssClass="width80" ValidationGroup="update" />
                        </Columns>
                        <AlternatingRowStyle BackColor="#C2D69B" />
                    </asp:GridView>
                </div>

            </asp:PlaceHolder>

            <asp:PlaceHolder runat="server" ID="EditModePlaceHolder" Visible="false"></asp:PlaceHolder>
        </td>
        <td style="width: 50px; text-align: right; vertical-align: top;">
            <asp:ImageButton ID="DeleteButton" runat="server" ImageUrl="../../Images/ui/cross.png" meta:resourcekey="Delete" OnClick="DeleteButton_Click" /><br />
        </td>
    </tr>
</table>
