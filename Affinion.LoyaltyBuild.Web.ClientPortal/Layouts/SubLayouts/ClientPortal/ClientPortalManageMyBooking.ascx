﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientPortalManageMyBooking.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal.ClientPortalManageMyBooking" %>

<div class="col-sm-12 no-padding">
    <div class="accordian-outer">
        <div class="panel-group" id="accordion">
            <div class="panel panel-default">
                <div class="panel-heading bg-primary">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseBooking" class="show-hide-list"><sc:text id="heading" field="SearchBookingPanelHeading" runat="server" />
                            <asp:Literal ID="litHeading" runat="server" /></a>
                    </h4>
                </div>
                <div id="collapseBooking" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <asp:PlaceHolder ID="SublayoutWrapper" runat="server">
                            <div class="body-container">
                                <div class="container booking-detail-info row">
                                    <div class="row" id="headerbtn">
                                        <div class="col-md-12">
                                            <div class="cal-table-top-right float-right" style="margin-right: 30px;">
                                                <%--<button id="btnPrintConfirm" type="button" class="btn btn-primary btn-bg btn-sm" onserverclick="btnPrintConfirm_Click" runat="server">
                            <span class="glyphicon glyphicon-print" aria-hidden="true" />
                            <asp:Literal ID="litPrintButtonText" runat="server" />
                        </button>--%>
                                                <asp:HyperLink ID="hypPrintButton" Target="_blank" CssClass="btn btn-primary btn-bg btn-sm" runat="server">
                                                    <span class="glyphicon glyphicon-print" aria-hidden="true" />
                                                    <asp:Literal ID="litPrintButtonText" runat="server" />
                                                </asp:HyperLink>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="divPrint" runat="server" style="margin-right: 30px;">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h1 class="breadcrumb-top">
                                                    <asp:Literal ID="litBookingHeadingPretext" runat="server" />
                                                    <asp:Label ID="lblHotelName" runat="server" Style="display: none;"></asp:Label>
                                                    <asp:HyperLink ID="HotelPageLink" runat="server" Style="color: rgb(3, 113, 178); font-size: 28px" /></h1>
                                                <span class="no-margin accent54-f">
                                                    <asp:Literal ID="litBookingReferenceText" runat="server" />
                                                    <strong>
                                                        <asp:Literal ID="litBookingReferenceNumber" runat="server" />
                                                    </strong>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="content_bg_inner_box alert-info bookng">
                                                    <div class="col-md-3 col-xs-12 bordr-rght">
                                                        <div class="address">
                                                            <div class="addressLine1">
                                                                <asp:Label ID="AddressLine1" runat="server" />
                                                            </div>
                                                            <div class="addressLine2">
                                                                <asp:Label ID="AddressLine2" runat="server" />
                                                            </div>
                                                            <div class="addressLine3">
                                                                <asp:Label ID="AddressLine3" runat="server" />
                                                            </div>
                                                            <div class="addressLine4">
                                                                <asp:Label ID="AddressLine4" runat="server" />
                                                            </div>
                                                            <div class="addressTown">
                                                                <asp:Label ID="AddressTown" runat="server" />
                                                            </div>
                                                            <div class="addressCountry" runat="server">
                                                                <asp:Label ID="AddressCountry" runat="server" />
                                                            </div>
                                                        </div>
                                                        <p class="accent33-f margin-common-top">
                                                            <span class="fa fa-map-marker" style="color: #38b3fc;"></span>
                                                            <a href="javascript:void(0)" id="btnShow">
                                                                <strong>
                                                                    <asp:Literal ID="litMapDirection" runat="server" />
                                                                </strong>
                                                            </a>
                                                        </p>
                                                        <span class="margin-common-top" style="float: left; width: 100%;">
                                                            <asp:Literal ID="litPhoneText" runat="server" />
                                                            <asp:Literal ID="litPhoneNumber" runat="server" />
                                                            <p class="accent33-f">
                                                                <span class="glyphicon glyphicon-envelope" style="color: #38b3fc;"></span>
                                                                <asp:HyperLink ID="hypEmailProperty" runat="server">
                                                                    <i class="bicon-email"></i>
                                                                    <strong>
                                                                        <asp:Literal ID="litEmailText" runat="server" />
                                                                    </strong>
                                                                </asp:HyperLink>
                                                                <br />
                                                                <%--<a href="#" id="viewPolicyLink">--%>
                                                                <strong>
                                                                    <asp:Literal ID="litViewPolicy" runat="server" /></strong>
                                                                <%--</a>--%>
                                                            </p>
                                                            <!--<input id="emal" class="form-control" type="text" placeholder="">-->
                                                        </span>
                                                    </div>

                                                    <div class="col-md-3 col-xs-12 bordr-rght">
                                                        <asp:Image ID="imgHotelImage" CssClass="hotl_img" runat="server" />
                                                    </div>
                                                    <div class="col-md-3 col-xs-12 bordr-rght box_size">
                                                        <div class="date_bg">
                                                            <div class="chk_in date_brdr">
                                                                <asp:Literal ID="litCheckinText" runat="server" />
                                                                <span class="date_disp">
                                                                    <h1 class="dte_no col-xs-offset-2"><strong>
                                                                        <asp:Literal ID="litCheckinDay" runat="server" /></strong></h1>
                                                                    <asp:Literal ID="litCheckinMonthYear" runat="server" />
                                                                    <br />
                                                                    <asp:Literal ID="litCheckinDayOfWeek" runat="server" />
                                                                    <br>
                                                                    <span class="glyphicon glyphicon-time"></span>
                                                                    <asp:Literal ID="litFromText" runat="server" />
                                                                    <asp:Literal ID="litCheckinTime" runat="server" />
                                                                </span>
                                                            </div>

                                                            <div class="chk_in">
                                                                <asp:Literal ID="litCheckOutText" runat="server" />
                                                                <span class="date_disp">
                                                                    <h1 class="dte_no col-xs-offset-2">
                                                                        <strong>
                                                                            <asp:Literal ID="litCheckoutDay" runat="server" />
                                                                        </strong>
                                                                    </h1>
                                                                    <asp:Literal ID="litCheckoutMonthYear" runat="server" />
                                                                    <br />
                                                                    <asp:Literal ID="litCheckoutDayOfWeek" runat="server" />
                                                                    <br>
                                                                    <span class="glyphicon glyphicon-time"></span>
                                                                    <asp:Literal ID="litUntilText" runat="server" />
                                                                    <asp:Literal ID="litCheckoutTime" runat="server" />
                                                                </span>
                                                            </div>

                                                            <span class="btn btn-primary btn-bg btn-sm  col-xs-offset-3" style="margin-top: 11%;">
                                                                <span class="nightsdetails">
                                                                    <asp:Literal ID="litNumberOfNightsCount" runat="server" />
                                                                    <asp:Literal ID="litNightText" runat="server" />
                                                                </span>
                                                                <span class="roomDetails">
                                                                    <asp:Literal ID="litRoomCount" runat="server" />
                                                                    <asp:Literal ID="litRoomText" runat="server" />
                                                                </span>
                                                            </span>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-3 col-xs-12">
                                                        <span class="price_bg">
                                                            <asp:Literal ID="litPriceText" runat="server" />
                                                        </span>
                                                        <br>
                                                        <h1><strong>
                                                            <asp:Literal ID="litCurrencySymbol" runat="server" />
                                                            <asp:Literal ID="litPrice" runat="server" /></strong></h1>
                                                        <span>
                                                            <a class="accent33-f" href="#" data-toggle="modal" data-target="#viewPriceDetails"><span class="glyphicon glyphicon-list-alt"></span>
                                                                <asp:Literal ID="litPriceDetailsButtonText" runat="server" />
                                                            </a>
                                                        </span>
                                                        <asp:Panel ID="CancelButtonWrapper" runat="server" CssClass="btn_top">
                                                            <button class="btn btn-danger btn-bg" id="btnCancel" type="submit" runat="server" onserverclick="btnCancel_ServerClick" data-target="#cancelbookingModal">
                                                                <span style="top: 2px;" class="glyphicon glyphicon-remove"></span>
                                                                <asp:Literal ID="litCancelButtonText" runat="server" /></button>
                                                        </asp:Panel>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="viewPriceDetails" role="dialog">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">
                                                &times;
                                            </button>
                                            <h4 class="modal-title">
                                                <asp:Literal ID="litPriceModalTitle" runat="server" />
                                            </h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="content_bg_inner_box alert-info">
                                                <div class="col-sm-12 no-padding">
                                                    <div class="col-sm-4 no-padding-right">
                                                        <strong>
                                                            <asp:Literal ID="litTotalPriceModal" runat="server" />
                                                        </strong>
                                                         <h1><strong>
                                                             <asp:Literal ID="litCurrencySymbolRoomPrice" runat="server" />
                                                            <asp:Literal ID="litTotalRoomPrice" runat="server" />
                                                        </strong></h1>
                                                    </div>
                                                    <div class="col-sm-4 no-padding-right">
                                                        <strong>
                                                            <asp:Literal ID="litPriceBreakText" runat="server" />
                                                        </strong>
                                                        <h1><strong>
                                                            <asp:Literal ID="litPriceBreak" runat="server" />
                                                        </strong></h1>
                                                    </div>
                                                    <div class="col-sm-4 no-padding-right">
                                                        <strong>
                                                            <asp:Literal ID="litPayableAtAccomodationText" runat="server"/>
                                                        </strong>
                                                        <h1>
                                                            <asp:Literal ID="litCurrencySymbolModal" runat="server" />
                                                            <asp:Literal ID="litPriceModal" runat="server" /></h1>
                                                        <%--<span>
                                                            <asp:Literal ID="litTaxDetails" runat="server" />
                                                        </span>--%>
                                                    </div>
                                                    <%--<div class="col-sm-4 no-padding-right">
                                                        <div class="col-sm-1 accent54-f paybill">
                                                            <span class="glyphicon glyphicon-menu-right"></span>
                                                        </div>
                                                        <div class="col-sm-10">
                                                            <span class="accent54-f">
                                                                <asp:Literal ID="litPayAtAccoInfo" runat="server" />
                                                               <strong><asp:Literal ID="litHotelNameModal" runat="server" /></strong>
                                                            </span>
                                                        </div>
                                                    </div>--%>
                                                </div>
                                                <div class="col-sm-12 no-padding">
                                                    <button type="button" class="btn btn-default float-right" data-dismiss="modal">
                                                        <asp:Literal ID="litModalCloseButton" runat="server" />
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div id="dialog" class="modal-title" style="display: none; resize: none">
                                <div id="mapcanvas" style="height: 380px; width: 580px;"></div>
                            </div>
                            <div class="modal fade" id="cancelbookingModal">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">
                                                <asp:Literal ID="litCancelYourBooking" runat="server" /></h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="container">
                                                <div class="col-sm-12">
                                                    Cancellation Cost for this Booking will be:
                            <asp:Literal ID="litCancellationCost" runat="server"></asp:Literal>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" id="cancelCheckbox" class="martop10" value=""><a href="#" class="btn btn-link padL0" onclick="return false" data-toggle="modal" data-target="#termsAndConditionsModal" role="button"><asp:Literal ID="litTermsConditionsTitle" runat="server" /></a></label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <button type="button" runat="server" onserverclick="Button_YesClick" class="btn btn-primary yesButton" id="yesClick" disabled="disabled">
                                                        <asp:Literal ID="litYes" runat="server" /></button>
                                                    <button type="button" class="btn btn-danger" id="noClick" disabled="disabled">
                                                        <asp:Literal ID="litNo" runat="server" /></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div id="successMessageWrapper" class="alert alert-success" runat="server" visible="false">
                                <sc:text id="txtSuccessText" runat="server" visible="false" />
                            </div>
                            <div id="errorMessageWrapper" class="alert alert-danger" runat="server" visible="false">
                                <sc:text id="txtErrorText" runat="server" visible="false" />
                            </div>
                            <div id="alreadyCancelledMessageWrapper" class="alert alert-danger" runat="server" visible="false">
                                <sc:text id="txtAlreadyCancelledText" runat="server" visible="false" />
                            </div>
                            <div id="invalidOrderMessageWrapper" class="alert alert-danger" runat="server" visible="false">
                                <sc:text id="txtInvalidOrderText" runat="server" visible="false" />
                            </div>

                            <!-- Alert message content -->
                            <%--<div id="divAlertMsg" runat="server"> 
            <asp:Literal ID="cancelBookingStatus" runat="server" />
        </div>--%>

                            <!---Terms and conditions pop up --->

                            <div class="modal fade" id="termsAndConditionsModal">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">
                                                <asp:Literal ID="termsConditionsTitle" runat="server" /></h4>
                                        </div>
                                        <div class="modal-body" style="max-height: 500px; overflow-y: auto;">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <asp:Literal ID="termsConditionsContent" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </asp:PlaceHolder>
                        <asp:Panel ID="NoDataError" runat="server" Visible="false">
                            No Record(s) Found.
                        </asp:Panel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(function () {
        jQuery("#btnShow").click(function () {
            var latitude = '<%=latitude%>';
            var lognitude = '<%=lognitude%>';
            var coords = new google.maps.LatLng(latitude, lognitude);
            jQuery("#dialog").dialog({
                modal: true,
                //Put Your hotel title here
                title: '<%=hotelName%>',
                width: 620,
                height: 450,
                resizable: false,
                open: function () {
                    var mapOptions = {
                        center: coords,
                        zoom: 9,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    }
                    var map = new google.maps.Map(jQuery("#mapcanvas")[0], mapOptions);

                    var marker = new google.maps.Marker({
                        position: coords,
                        // draggable: true,
                        map: map,
                    });
                }
            });

        });

        jQuery("#noClick").click(function () {
            jQuery('#cancelbookingModal').modal('toggle');
        });

        var url = window.location.href;
        if (url.indexOf("?") > 0) {
            $("#collapse1").collapse('hide');
            $("#collapseBooking").collapse('show');
        }
        else {
            $("#collapse1").collapse('show');
            $("#collapseBooking").collapse('hide');
        }
    });



    jQuery(function () {
        jQuery("#cancelCheckbox").click(function () {
            if (jQuery(this).is(':checked')) {
                jQuery('.yesButton').removeAttr("disabled");
                jQuery('#noClick').removeAttr("disabled");
            }
            else {
                jQuery('.yesButton').attr("disabled", "true");
                jQuery('#noClick').attr("disabled", "true");
            }
        });

    });

    function openPopup() {
        jQuery('#cancelbookingModal').modal('toggle');
    }
</script>
