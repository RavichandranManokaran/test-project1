﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           NameValueCollectionExtensions.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           NameValueCollection Extensions
/// </summary>

using System.Collections.Specialized;

namespace Affinion.LoyaltyBuild.Common.Utilities
{
    public static class NameValueCollectionExtensions
    {
        /// <summary>
        /// Check query string keys
        /// </summary>
        /// <param name="qString">Query string</param>
        /// <param name="key">key</param>
        /// <returns>Returns true if given key found, false otherwise</returns>
        public static bool HasKey(this NameValueCollection qString, string key)
        {
            if (qString != null)
            {
                foreach (string qkey in qString.Keys)
                {
                    if (qkey.Equals(key))
                    {
                        return true;
                    }
                }
            }
            
            return false;
        }
    }

}
