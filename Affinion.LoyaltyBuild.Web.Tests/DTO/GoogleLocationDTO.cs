﻿

namespace Affinion.LoyaltyBuild.Web.Tests.DTO
{
    /// <summary>
    /// DTO Class for Google Location data
    /// </summary>
    public class GoogleLocationDTO
    {
    
        /// <summary>
        /// Gets or Sets Location Name
        /// </summary>
        public string LocationName { get; set; }

        /// <summary>
        /// Gets or Sets Latitude
        /// </summary>
        public string Latitude { get; set; }

        /// <summary>
        /// Gets or Sets Longitude
        /// </summary>
        public string Longitude { get; set; }

        /// <summary>
        /// Gets or Sets the Description
        /// </summary>
        public string Description { get; set; }

    }
}