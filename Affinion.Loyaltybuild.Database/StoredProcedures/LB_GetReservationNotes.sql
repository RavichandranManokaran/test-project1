-- =============================================
--	Name		: GetReservationNotes
--	Purpose		: To get the ReservationNotes
--	Author		: Antonysamy
--	Date		:	04/02/2016
-- =============================================

Create PROCEDURE [dbo].[LB_GetReservationNotes]
	(
		@OrderLineId	INT
	)
AS
BEGIN
		SELECT   R.NotesId
		,R.NoteTypeId
		,N.[Description]
		,R.OrderLineId
		,R.Note
		,R.CreatedDate
		,R.CreatedBy
		FROM LB_ReservationNotes R 
		left join LB_NoteTypes N  on N.NoteTypeId = R.NoteTypeId
		WHERE OrderLineId = @OrderLineId
		Order by R.NoteTypeId , R.CreatedBy 
END
