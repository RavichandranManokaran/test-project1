﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2.Maps;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Maps
{
    public class PromotionAuditTrailMap : BaseClassMap<PromotionAuditTrail>
    {
        // Methods
        public PromotionAuditTrailMap()
        {
            base.Table("LB_PromotionAuditTrail");
            base.Id(x => x.PromotionAuditTrailId, "PromotionAuditTrailId").GeneratedBy.Identity();
            Map(x => x.Message).Nullable();
            Map(x => x.Record).Column("Record");
            Map(x => x.EntityName).Nullable();
            Map(x => x.AuditedBy).Not.Nullable();
            Map(x => x.AuditedDate).Not.Nullable();
        }
    }
}
