﻿using System;
namespace Affinion.Loyaltybuild.BedBanks.CAPI.Model
{
    /// <summary>
    /// Name:           Hotel Address
    /// Author:         Adam Berry
    /// Date:           26/01/2016
    /// CAPI Version :  v1.0
    /// 
    /// Description:
    /// 
    /// Main Data Class for HotelAddress Object. Instance of this class hold all address information 
    /// about available Hotels from CAPI.
    /// 
    /// Instances of this class are created directly from the JSON feed, and attached to
    /// the main Hotel Object.
    /// </summary>
    public class HotelAddress
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string GeoCode { get; set; }
        public string Name { get; set; }
        public string PostalCode { get; set; }

        public string FullAddress
        {
            get
            {
                string fullAddress = "";

                if (!String.IsNullOrEmpty(Address1)) { fullAddress += Address1 + ", "; }
                if (!String.IsNullOrEmpty(Address2)) { fullAddress += Address2 + ", "; }
                if (!String.IsNullOrEmpty(City)) { fullAddress += City + ", "; }
                if (!String.IsNullOrEmpty(State)) { fullAddress += State + ", "; }
                if (!String.IsNullOrEmpty(Country)) { fullAddress += Country + ", "; }
                if (!String.IsNullOrEmpty(PostalCode)) { fullAddress += PostalCode; }

                return fullAddress;
            }
        }

        public HotelAddress(string pAddress1, string pAddress2, string pCity, string pState, string pCountry, string pGeocode, string pName, string pPostalcode)
        {
            Address1 = pAddress1;
            Address2 = pAddress2;
            City = pCity;
            State = pState;
            Country = pCountry;
            GeoCode = pGeocode;
            Name = pName;
            PostalCode = pPostalcode;
        }
    }
}