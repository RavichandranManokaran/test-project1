﻿CREATE TABLE [dbo].[uCommerce_ProductProperty] (
    [ProductPropertyId]        INT            IDENTITY (1, 1) NOT NULL,
    [Value]                    NVARCHAR (MAX) NULL,
    [ProductDefinitionFieldId] INT            NOT NULL,
    [ProductId]                INT            NOT NULL,
    CONSTRAINT [uCommerce_PK_ProductProperty] PRIMARY KEY CLUSTERED ([ProductPropertyId] ASC),
    CONSTRAINT [uCommerce_FK_ProductProperty_Product] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[uCommerce_Product] ([ProductId]),
    CONSTRAINT [uCommerce_FK_ProductProperty_ProductDefinitionField] FOREIGN KEY ([ProductDefinitionFieldId]) REFERENCES [dbo].[uCommerce_ProductDefinitionField] ([ProductDefinitionFieldId]),
    CONSTRAINT [Unique_ProductProperty_ProductId_ProductDefinitionFieldId] UNIQUE NONCLUSTERED ([ProductId] ASC, [ProductDefinitionFieldId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_uCommerce_ProductProperty_ProductId]
    ON [dbo].[uCommerce_ProductProperty]([ProductId] ASC);

