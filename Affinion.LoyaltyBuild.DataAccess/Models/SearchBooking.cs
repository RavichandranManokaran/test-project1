﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.DataAccess.Models
{
    public class SearchBooking
    {
        public string Client { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Provider { get; set; }
        public DateTime ReservationFrom { get; set; }
        public DateTime ReservationTo { get; set; }
        public DateTime ArrivalDate { get; set; }
        public DateTime ReservationDate { get; set; }
        public string BookingReference { get; set; }
        public string BookingMethod { get; set; }
        public int BookingStatus { get; set; }
        public int OrderStatus { get; set; }
        public string EmailValidation { get; set; }
        public string NumericValidation { get; set; }
        #region LWD-1010
        public bool IsListOut { get; set; }
        public string SortExpresssion { get; set; }
        public string SortDirection { get; set; }
        public int PageIndex { get; set; }
        public int SearchDateBy { get; set; }
        public string BookingReferenceMethod { get; set; }
        public string StrReservationFrom { get; set; }
        public string StrReservationTo { get; set; }
        public string StrArrivalDate { get; set; }
        public string StrReservationDate { get; set; }
        #endregion
       
    }
}
