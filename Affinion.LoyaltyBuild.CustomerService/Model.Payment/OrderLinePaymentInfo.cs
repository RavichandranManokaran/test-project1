﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/
namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model.Payment
{
    /// <summary>
    /// Entity containing Orderline Payment related information
    /// </summary>
    public class  OrderLinePaymentInfo
    {
        /// <summary>
        /// OrderLineId
        /// </summary>
        public int OrderLineId { get; set; }

        /// <summary>
        /// BookingReference
        /// </summary>
        public string BookingReference { get; set; }
        
        /// <summary>
        /// Total dues By currency types
        /// </summary>
        public List<TotalDueItem> DueByCurrencies { get; set; }

        /// <summary>
        /// List of Due History
        /// </summary>
        public List<DueHistoryItem> DueHistory { get; set; }

        /// <summary>
        /// List of  payment history items
        /// </summary>
        public List<PaymentHistoryItem> PaymentHistory { get; set; }

        /// <summary>
        /// List of  discount history items
        /// </summary>
        public List<DiscountHistoryItem> DiscountHistory { get; set; }
    }
}
