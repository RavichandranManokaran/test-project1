﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.Common.Constants
{
    public static class SiteCoreCommsionConstant
    {
        public const string FeePercentage = "Fees Percentage";
        public const string Fees = "Fees";
        public const string CreaditCardFee = "CreaditCard Fee";
        public const string CancellationFee = "Cancellation Fee";
        public const string LBCommision = "LB Commission";
        public const string TransferFee = "Transfer Fee";
    }
    public static class ProductDefinationVairantConstant
    {

        public const string RoomType = "RoomType";
        public const string AddOn = "AdOns";
        public const string FoodType = "FoodType";
        public const string PackageType = "PackageType";
        public const string MaxNumberOfAdults = "MaxNumberOfAdults";
        public const string MaxNumberOfChildren = "MaxNumberOfChildren";
        public const string MaxNumberOfPeople = "MaxNumberOfPeople";
        public const string MaxNumberOfInfants = "MaxNumberOfInfants";
        public const string MinPrice = "MinPrice";
        public const string MaxPrice = "MaxPrice";
        public const string IsDeleted = "IsDeleted";
        public const string IsSellableStandAlone = "IsSellableStandAlone";
        public const string IsUnlimited = "IsUnlimited";
        public const string IsActive = "IsActive";
        public const string RelatedSKU = "RelatedSKU";
        public const string RelatedAddOnSKU = "RelatedAddOnSKU";
    }

    public static class BookingSessionConstant
    {
        public const string CancellationToken = "CANTOKEN";
        public const string CancellationCharge = "CANCHARGE";
    }
    public static class ProductDefinationConstant
    {
      //  public const string Experience = "HotelExperience";
        public const string HotelRoom = "HotelRoom";
        public const string AddOn = "Add On";
        public const string RoomType = "RoomType";
        public const string ProductType = "ProductType";
        public const string Adult = "MaxNumberOfAdults";
        public const string Child = "MaxNumberOfChildren";
        public const string Hotel = "Hotel";
        public const string Location = "Location";
        public const string Country = "Country";
        public const string Region = "Region";
        public const string HotelRating = "StarRating";
        public const string HotelType = "SupplierType";
        public const string GeoCoOrdinate = "GeoCoordinate";
        public const string Latitude = "Latitude";
        public const string Longitute = "Longitude";
        public const string SupplierType = "SupplierType";
        public const string Theme = "Theme";
        public const string Groups = "Groups";
        public const string Experience = "Experience";
        public const string HotelFacility = "Facility";
        static public readonly string Client = "Client";
        public const string RelatedSKU = "RelatedSKU";
        public const string SuperGroups = "SuperGroups";
        public const string SiteCoreItemGuid = "SiteCoreItemGuid";
        public const string Recommended = "Recommended";
        public const string WeekendSerchRestriction = "WeekendSerchRestriction";
    }
    public static class CampaignDataTyeConstant
    {
        public const string IsApprove = "isapprove";
        public const string IsInviteSend = "isinvitesend";
        public const string ApproveMessage = "approve";
        public const string RejectMessage = "reject";
        public const string InviteMessage = "invite";
        public const string PackageName = "invite";
    }
    public static class DefinationConstant
    {

        public const string Independent = "independent";

        public const string HotelCategory = "Hotel";
        public const string HotelType = "HotelType";
        public const string Location = "Location";
        public const string HotelRating = "HotelRating";
        public const string CampaginItemDefination = "DefaultAct";
        public const string Country = "Country";
        public const string Region = "Region";
        public const string Client = "Client";
        public const string HotelFacility = "HotelFacility";
        public const string Experience = "HotelExperience";
        public const string Theme = "Theme";
        public const string Groups = "Groups";
        public const string SuperGroups = "SuperGroups";
        public const string SiteCoreItemGuid = "SiteCoreItemGuid";
        public const string OfferType = "OfferType";
        public const string PaymentMethod = "Paymentmethod";
    }

    public static class DataTypeConstant
    {
        public const string HotelType = "HotelType";
        public const string Location = "Location";
        public const string HotelRating = "HotelRating";
        public const string RoomType = "RoomType";
        public const string ProductType = "ProductType";

    }

    public static class SUBRateConstant
    {
        public const string CacellationFee = "CANFEE";
        public const string CCProcessingFee = "CCPROFEE";
        public const string LBCommision = "LBCOM";
        public const string TransferFee = "TRAFEE";


    }
}
