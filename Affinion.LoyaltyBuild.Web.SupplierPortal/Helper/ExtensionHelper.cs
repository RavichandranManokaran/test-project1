﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Helper
{
    public static class ExtensionHelper
    {
        public static bool IsGuid(this string str)
        {
            return !string.IsNullOrEmpty(str) &&
                (new Regex(@"^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$", RegexOptions.Compiled)).IsMatch(str);
        }
    }
}