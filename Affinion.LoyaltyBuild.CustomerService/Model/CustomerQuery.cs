﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    /// <summary>
    /// Booking query info
    /// </summary>
    public class CustomerQuery
    {
        /// <summary>
        /// Query Id
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Query Type Id
        /// </summary>
        public int QueryTypeId { get; set; }
        
        /// <summary>
        /// Query Type 
        /// </summary>
        public string QueryType { get; set; }

        /// <summary>
        /// Customer ClientId / Provider Id
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// Client name
        /// </summary>
        public string ClientName { get; set; }

        /// <summary>
        /// Gets or sets the CompaignGroupID 
        /// </summary>
        public string CampaignGroupID { get; set; }

        /// <summary>
        /// campaign group
        /// </summary>
        public string CampaignGroup { get; set; }

        /// <summary>
        /// Gets or sets the ProviderId 
        /// </summary>
        public string ProviderId { get; set; }

        /// <summary>
        /// Provider Name
        /// </summary>
        public string ProviderName { get; set; }

        /// <summary>
        /// CustomerId
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// FirstName
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// LastName
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// OrderLineId
        /// </summary>
        public int? OrderLineId { get; set; }

        /// <summary>
        /// BookingReference
        /// </summary>
        public string BookingReference { get; set; }

        /// <summary>
        /// Content
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// IsSolved
        /// </summary>
        public bool IsSolved { get; set; }
      
        /// <summary>
        /// Created Date
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// CreatedBy
        /// </summary>
        public string CreatedBy { get; set; }

        /// <summary>
        /// set Attachment
        /// </summary>
        public bool HasAttachment { get; set; }

        /// <summary>
        /// attachments
        /// </summary>
        public List<QueryAttachment> Attachments { get; set; }
    }

    public class QueryAttachment
    {
        /// <summary>
        /// query id
        /// </summary>
        public int QueryId { get; set; }

        /// <summary>
        /// AttachmentId
        /// </summary>
        public int AttachmentId { get; set; }

        /// <summary>
        /// FileType
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// FileType
        /// </summary>
        public string FileType { get; set; }

        /// <summary>
        /// FileContent
        /// </summary>
        public byte[] FileContent { get; set; }
    }
}
