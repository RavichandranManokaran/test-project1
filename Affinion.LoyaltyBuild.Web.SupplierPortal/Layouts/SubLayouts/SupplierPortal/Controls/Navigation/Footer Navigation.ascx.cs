﻿using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;
using Sitecore.Data.Items;
using Sitecore.Links;
using System;
using System.Collections.Generic;
using System.Web.Security;
using System.Web.UI.WebControls;


namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Navigation
{
    /// <summary>
    /// Class footer navigation
    /// </summary>
    public partial class Footer_Navigation : BaseSublayout
    {

        #region private variables

        private Dictionary<string, string> _sitecoreMessages;

        #endregion


        public Footer_Navigation()
        {
            this._sitecoreMessages = new Dictionary<string, string>();
        }

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Load(object sender, EventArgs e)
        {
            //to bind the sitecore field values
            BindSitecoreValues();

            Sitecore.Diagnostics.Tracer.Info("Footer Navigation - Page Load");
            Sitecore.Diagnostics.Profiler.StartOperation("Footer Navigation - Page Load");

            Item baseItem = SiteConfiguration.GetFooterLinksItem();
            List<Item> nodes = new List<Item>();
            if (baseItem != null)
            {
                foreach (Item footerLink in baseItem.Children)
                {
                    Item i = Sitecore.Context.Database.GetItem(footerLink["Top Level Item"]);
                    if (i != null && SiteConfiguration.DoesItemExistInCurrentLanguage(i)) { nodes.Add(i); }
                }
                rptMenu.DataSource = nodes;
                rptMenu.DataBind();
            }
            Sitecore.Diagnostics.Profiler.EndOperation("Footer Navigation - Page Load");
        }

        /// <summary>
        ///repeater menu ItemDataBound event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rptMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Item node = e.Item.DataItem as Item;
                {
                    HyperLink MenuLink = e.Item.FindControl("MenuLink") as HyperLink;
                    Literal MenuText = e.Item.FindControl("MenuText") as Literal;

                    if (MenuLink != null && MenuText != null)
                    {

                        MenuText.Text = node["Menu Title"];
                        Item changePasswordFooterItemID = Sitecore.Context.Database.GetItem(Constants.ChangePassFooterLinkID);
                        Item qandAFooterItemID = Sitecore.Context.Database.GetItem(Constants.QandALink);
                        if (node.ID.Equals(changePasswordFooterItemID.ID))
                        {
                            var domain = Sitecore.Security.Domains.Domain.GetDomain(Constants.suppliers);
                            var userName = SitecoreUserProfile.GetItem(UserProfileKeys.loggedInUserName);
                            string domainUser = domain.Name + @"\" + userName;
                            Item loginPageItem = Sitecore.Context.Database.GetItem(Constants.LogOnPageItemId);
                            if (loginPageItem != null)
                            {
                                MenuLink.CssClass = "changePasswordLink";
                                MenuLink.Attributes.Add("data-toggle", "modal");
                                MenuLink.Attributes.Add("data-target", "#chngPswrd");
                                MenuLink.Attributes.Add("style", "cursor:pointer");
                            }
                        }
                        else
                        {
                            Sitecore.Data.Fields.LinkField lnkFld = node.Fields["Redirect Link"];
                            if (lnkFld != null)
                            {
                                Sitecore.Data.Fields.FileField fileFld = node.Fields["PDF"];
                                string lnkUrl = (lnkFld.TargetItem != null) ? LinkManager.GetItemUrl(lnkFld.TargetItem) : string.Empty;
                                string fileId = (fileFld != null && fileFld.MediaItem != null) ? fileFld.MediaID.ToString() : string.Empty;
                                MenuLink.NavigateUrl = (!string.IsNullOrWhiteSpace(lnkUrl) && !string.IsNullOrWhiteSpace(fileId)) ? string.Format("{0}?fileId={1}", lnkUrl, fileId) : lnkUrl;
                                MenuLink.Target = lnkFld.Target;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// submit button click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            string domainUserName = SitecoreUserProfile.GetItem(UserProfileKeys.loggedinDomainUser);
            if (!String.IsNullOrWhiteSpace(domainUserName))
            {

                string currentPassword = OldPasswordText.Text;
                string newPassword = NewPasswordText.Text;
                string ConfirmPassword = ConfirmPasswordText.Text;
                bool status = false;

                if (newPassword.Equals(ConfirmPassword))
                {
                    using (new Sitecore.SecurityModel.SecurityDisabler())
                    {
                        MembershipUser user = Membership.GetUser(domainUserName, true);
                        if (user != null)
                        {
                            Affinion.LoyaltyBuild.Security.MemberShip.Helper membershipHelper = new Affinion.LoyaltyBuild.Security.MemberShip.Helper();
                            if (membershipHelper.IsInPasswordHistory(domainUserName, newPassword))
                            {
                                ShowAlert(GetMessage(Constants.PasswordExistsInHistory));
                                return;
                            }
                            else
                            {
                                try
                                {
                                    status = user.ChangePassword(currentPassword, newPassword);
                                    if (status)
                                    {
                                        ShowAlert(GetMessage(Constants.PasswordChanged));

                                    }
                                    else
                                    {
                                        ShowAlert(GetMessage(Constants.ProvideOldPasswordCorrectly));
                                    }
                                }
                                catch (ArgumentException ex)
                                {
                                    ShowAlert(GetMessage(Constants.NewPasswordDoesNotMatchWithRegex));

                                    //log
                                    Diagnostics.WriteException(DiagnosticsCategory.SupplierPortal, ex, this);

                                }
                                catch (Exception ex)
                                {
                                    ShowAlert(GetMessage(Constants.PasswordHasNotBeenChanged));

                                    //Log 
                                    Diagnostics.WriteException(DiagnosticsCategory.SupplierPortal, ex, this);
                                }
                            }
                        }
                    }
                }
                else
                {
                    ShowAlert(GetMessage(Constants.PasswordDoesNotMatch));

                }
            }
        }


        #region private methods
        /// <summary>
        /// Bind Sitecore Field Values
        /// </summary>
        private void BindSitecoreValues()
        {
            Item conditionalMessageItem = this.GetItemFromRenderingParameterPath("Conditional Messages Link");
            if (conditionalMessageItem != null)
            {

                _sitecoreMessages.Add(Constants.PasswordExistsInHistory, SitecoreFieldsHelper.GetItemFieldValue(conditionalMessageItem, Constants.PasswordExistsInHistory));
                _sitecoreMessages.Add(Constants.PasswordChanged, SitecoreFieldsHelper.GetItemFieldValue(conditionalMessageItem, Constants.PasswordChanged));
                _sitecoreMessages.Add(Constants.ProvideOldPasswordCorrectly, SitecoreFieldsHelper.GetItemFieldValue(conditionalMessageItem, Constants.ProvideOldPasswordCorrectly));
                _sitecoreMessages.Add(Constants.NewPasswordDoesNotMatchWithRegex, SitecoreFieldsHelper.GetItemFieldValue(conditionalMessageItem, Constants.NewPasswordDoesNotMatchWithRegex));
                _sitecoreMessages.Add(Constants.PasswordHasNotBeenChanged, SitecoreFieldsHelper.GetItemFieldValue(conditionalMessageItem, Constants.PasswordHasNotBeenChanged));
                _sitecoreMessages.Add(Constants.PasswordDoesNotMatch, SitecoreFieldsHelper.GetItemFieldValue(conditionalMessageItem, Constants.PasswordDoesNotMatch));
                _sitecoreMessages.Add(Constants.AuthenticationFailed, SitecoreFieldsHelper.GetItemFieldValue(conditionalMessageItem, Constants.AuthenticationFailed));
            }

        }

        /// <summary>
        /// Get Message
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private string GetMessage(string key)
        {
            if (_sitecoreMessages.ContainsKey(key))
                return _sitecoreMessages[key];

            return string.Empty;
        }

        /// <summary>
        /// Show alert message
        /// </summary>
        /// <param name="alertMessage"></param>
        /// <returns></returns>
        private void ShowAlert(string alertMessage)
        {
            this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('" +alertMessage + "')", true);

        }
        #endregion
    }
}


