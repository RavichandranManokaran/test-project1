﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SelectPdf;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Order;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Sitecore.Data.Items;
using System.IO;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic;
using System.Data;
using Affinion.LoyaltyBuild.BedBanks.Troika;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Message;
using System.Xml.Serialization;
using System.Text;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal
{
    public partial class ClientPortalManageBookingPrint : System.Web.UI.UserControl
    {
        #region Template Field Names
        static string headingPreTextFieldName = "Heading Pretext";
        static string bookingReferenceNumberFieldName = "Booking Reference Number Text";
        static string phoneNumberFieldName = "Phone Number Text";
        static string checkinFieldName = "Check In Text";
        static string checkoutTextFieldName = "Check Out Text";
        static string priceFieldName = "Price Text";
        static string fromTextFieldName = "From Text";
        static string untilTextFieldName = "Until Text";
        static string roomTextFieldName = "Room Text";
        static string nightTextFieldName = "Night Text";

        //static string showMapFieldName = "Show Map Text";
        static string emailPropertyFieldName = "Email Property Text";
        //static string viewPolicyFieldName = "View Policy Text";
        //static string cancelBookingFieldName = "Cancel Booking Text";
        //static string priceDetailsFieldName = "Price Details Text";

        static Item currentItem = Sitecore.Context.Item;

        #endregion Template Field Names

        int OrderLineId = 3257;
        protected string latitude { get; set; }
        protected string lognitude { get; set; }
        protected string hotelName { get; set; }
        private bool startConversion = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            string orderLineIdQs = Request.QueryString["olid"];
            int.TryParse(orderLineIdQs, out OrderLineId);
            if (!IsPostBack)
            {

                if (!string.IsNullOrWhiteSpace(orderLineIdQs) && OrderLineId > 0)
                {
                    ManageBookingDetailsOrderService();
                    PopulateStaticText();
                }
                startConversion = true;
                //else
                //{
                //    //Display Error Message
                //    SublayoutWrapper.Visible = false;
                //    NoDataError.Visible = true;
                //}
            }
            //printBookingPage();

        }

        private void ManageBookingDetailsOrderService()
        {
            IOrderService orderSvc = ContainerFactory.Instance.GetInstance<IOrderService>();
            JtPropertyDetailsResponse propertyDetails = null;
            /* mock service call should be removed */
            //MockOrderService mockOrder = new MockOrderService();
            Booking bookingInfo = orderSvc.GetBooking(OrderLineId);
            string bbProviderId = "c5ee30ab-067f-4169-96ee-3017009ebec3";
            if (bookingInfo != null)
            {
                DateTime checkinDate = bookingInfo.CheckInDate;
                litCheckinDayOfWeek.Text = checkinDate.DayOfWeek.ToString();
                litCheckinDay.Text = checkinDate.Day.ToString();
                System.Globalization.DateTimeFormatInfo mfi = new System.Globalization.DateTimeFormatInfo();
                var monthName = new System.Globalization.DateTimeFormatInfo().GetAbbreviatedMonthName(checkinDate.Month);
                litCheckinMonthYear.Text = string.Format("{0} {1}", monthName.ToUpper(), checkinDate.Year);
                litCheckinTime.Text = checkinDate.ToString("hh:mm");

                DateTime checkOutDate = bookingInfo.CheckOutDate;
                litCheckoutDayOfWeek.Text = checkOutDate.DayOfWeek.ToString();
                litCheckoutDay.Text = checkOutDate.Day.ToString();
                var checkOutMonthName = new System.Globalization.DateTimeFormatInfo().GetAbbreviatedMonthName(checkOutDate.Month);
                litCheckoutMonthYear.Text = string.Format("{0} {1}", checkOutMonthName.ToUpper(), checkOutDate.Year);
                litCheckoutTime.Text = checkOutDate.ToString("hh:mm");
                litBookingReferenceNumber.Text = bookingInfo.BookingReference;
                litRoomCount.Text = bookingInfo.NumberOfRooms.ToString();
                litCurrencySymbol.Text = bookingInfo.CurrencyText;
                litPrice.Text = bookingInfo.Price.ToString("0.00");
                litNumberOfNightsCount.Text = bookingInfo.NumberOfNights.ToString();
                string latLong = string.Empty;
                if (bookingInfo.AccomodationInfo != null)
                {
                    latLong = bookingInfo.AccomodationInfo.GeoCoordinates;
                    HotelPageName.Text = bookingInfo.AccomodationInfo.Title;
                    //HotelPageLink.NavigateUrl = "/hotelinformation?id=" + bookingInfo.AccomodationInfo.HotelItemId;
                    litAccomodationAddress.Text = bookingInfo.AccomodationInfo.Address;
                    hypEmailProperty.NavigateUrl = string.Format("mailto:{0}", bookingInfo.AccomodationInfo.EmailId);
                    litPhoneNumber.Text = bookingInfo.AccomodationInfo.Phone;
                }
                else if (bookingInfo.ProviderId == bbProviderId)
                {
                    DataTable bookingData = TroikaDataController.GetBookingDetailsForCancellation(bookingInfo.OrderLineId);
                    if (bookingData != null && bookingData.Rows.Count > 0)
                    {
                        if (bookingData.Rows[0][0] != null)
                        {
                            string xml = bookingData.Rows[0][0].ToString();
                            propertyDetails = GetPropertyDetails(xml);
                            if (propertyDetails != null)
                            {
                                latLong = propertyDetails.Latitude+","+propertyDetails.Longitude;
                                HotelPageName.Text = propertyDetails.PropertyName;
                                litAccomodationAddress.Text = propertyDetails.FullAddress;
                                hypEmailProperty.NavigateUrl = string.Format("mailto:{0}", string.Empty);
                                litPhoneNumber.Text = propertyDetails.Telephone;
                            }
                        }
                    }
                }

                //litPriceModal.Text = bookingInfo.PayableAtHotel.ToString("0.00");

                if (bookingInfo.AccomodationInfo!= null && !string.IsNullOrWhiteSpace(bookingInfo.AccomodationInfo.ImageUrl))
                {
                    imgHotelImage.ImageUrl = bookingInfo.AccomodationInfo.ImageUrl;
                }
                else if (propertyDetails != null)
                {
                    imgHotelImage.ImageUrl = propertyDetails.CMSBaseURL + propertyDetails.MainImage;
                }
                else
                {
                    imgHotelImage.Visible = false;
                }

                if (!string.IsNullOrWhiteSpace(latLong))
                {
                    List<string> latLongList = latLong.Split(',').ToList<string>();
                    latitude = latLongList[0];
                    lognitude = latLongList[1];
                }
                else
                {
                    latitude = "0";
                    lognitude = "0";
                }

                //CancelButtonWrapper.Visible = !bookingInfo.IsCancelled;
            }
        }

        private JtPropertyDetailsResponse GetPropertyDetails(string xml)
        {
            JtPropertyDetailsResponse propertyDetails = null;
            if (!xml.Equals(String.Empty))
            {
                //Serialize the result
                XmlRootAttribute xRoot = new XmlRootAttribute
                {
                    ElementName = "PropertyDetailsResponse",
                    IsNullable = true
                };

                XmlSerializer serializer = new XmlSerializer(typeof(JtPropertyDetailsResponse), xRoot);

                try
                {
                    MemoryStream memStream = new MemoryStream(Encoding.UTF8.GetBytes(xml));
                    propertyDetails = (JtPropertyDetailsResponse)serializer.Deserialize(memStream);
                }
                catch (Exception exc)
                {
                    throw new Exception(exc.Message);
                }
            }
            return propertyDetails;
        }

        private void PopulateStaticText()
        {
            SetSitecoreTextInLiteral(currentItem, litBookingHeadingPretext, headingPreTextFieldName);
            SetSitecoreTextInLiteral(currentItem, litBookingReferenceText, bookingReferenceNumberFieldName);
            SetSitecoreTextInLiteral(currentItem, litPhoneText, phoneNumberFieldName);
            SetSitecoreTextInLiteral(currentItem, litCheckinText, checkinFieldName);
            SetSitecoreTextInLiteral(currentItem, litCheckOutText, checkoutTextFieldName);
            SetSitecoreTextInLiteral(currentItem, litPriceText, priceFieldName);
            SetSitecoreTextInLiteral(currentItem, litFromText, fromTextFieldName);
            SetSitecoreTextInLiteral(currentItem, litUntilText, untilTextFieldName);
            SetSitecoreTextInLiteral(currentItem, litRoomText, roomTextFieldName);
            SetSitecoreTextInLiteral(currentItem, litNightText, nightTextFieldName);

            //SetSitecoreTextInLiteral(currentItem, litMapDirection, showMapFieldName);
            SetSitecoreTextInLiteral(currentItem, litEmailText, emailPropertyFieldName);
            //SetSitecoreTextInLiteral(currentItem, litViewPolicy, viewPolicyFieldName);
            //SetSitecoreTextInLiteral(currentItem, litCancelButtonText, cancelBookingFieldName);
            //SetSitecoreTextInLiteral(currentItem, litPriceDetailsButtonText, priceDetailsFieldName);

        }
        private void SetSitecoreTextInLiteral(Item currentItem, Literal literalControl, string fieldName)
        {
            literalControl.Text = (currentItem != null && currentItem.Fields[fieldName] != null && !string.IsNullOrWhiteSpace(currentItem.Fields[fieldName].Value)) ? currentItem.Fields[fieldName].Value : string.Empty;
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (startConversion)
            {
                /// get html of the page
                TextWriter myWriter = new StringWriter();
                HtmlTextWriter htmlWriter = new HtmlTextWriter(myWriter);
                //this.RenderControl(htmlWriter);
                divPrint.RenderControl(htmlWriter);
                // instantiate a html to pdf converter object
                HtmlToPdf converter = new HtmlToPdf();

                // create a new pdf document converting the html string of the page
                PdfDocument doc = converter.ConvertHtmlString(myWriter.ToString(), Request.Url.AbsoluteUri);

                // save pdf document
                doc.Save(Response, true, "Your Booking Confirmation.pdf");

                // close pdf document
                doc.Close();
            }
        }
    }
}