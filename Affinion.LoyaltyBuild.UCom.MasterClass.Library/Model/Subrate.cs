﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    public class SubratesDetail
    {
        public SubratesDetail() { }
        public SubratesDetail(string SubrateItemCode, string ItemID, decimal price, decimal VAT, Boolean IsPercentage,
            string ClientID, string CreatedBy, DateTime UpdtedOn, string currency)
        {
            this.SubrateItemCode = SubrateItemCode;
            this.ItemID = ItemID;
            this.VAT = VAT;
            this.IsPercentage = IsPercentage;
            this.ClientID = ClientID;
            this.CreatedBy = CreatedBy;
            this.UpdtedOn = UpdtedOn;
            this.Price = price;
            this.Currency = currency;
        }
        /// <summary>
        ///SubrateItemCode
        /// </summary>
        public string SubrateItemCode { get; set; }

        /// <summary>
        ///ItemName
        /// </summary>
        public string ItemName { get; set; }

        /// <summary>
        ///ItemID
        /// </summary>
        public string ItemID { get; set; }

        /// <summary>
        ///VAT
        /// </summary>
        public decimal VAT { get; set; }

        /// <summary>
        ///Price
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        ///IsPercentage
        /// </summary>
        public Boolean IsPercentage { get; set; }

        /// <summary>
        ///ClientID
        /// </summary>
        public string ClientID { get; set; }

        /// <summary>
        ///CreatedBy
        /// </summary>
        public string CreatedBy { get; set; }

        /// <summary>
        ///UpdtedOn
        /// </summary>
        public DateTime UpdtedOn { get; set; }

        /// <summary>
        ///Currency
        /// </summary>
        public string Currency { get; set; }

        /// <summary>
        ///Calculation for VAT Amount
        /// </summary>
        public decimal VATAmount
        {
            get
            {
                return (Math.Round(((Price * VAT) / 100), 2));
            }
            set { }
        }

        /// <summary>
        ///Calculation for TotalAmount
        /// </summary>
        public decimal TotalAmount { get { return Price + VATAmount; } set { } }


    }

    public class SubrateItemDetail
    {
        /// <summary>
        /// SubrateItemDetail
        /// </summary>
        /// <param name="SubrateItemCode">SubrateItemCode</param>
        /// <returns>SubrateItemDetail</returns> 
        public SubrateItemDetail(string SubrateItemCode, string ItemName)
        {

            this.SubrateItemCode = SubrateItemCode;
            this.ItemName = ItemName;
        }

        public int SubrateItemID { get; set; }
        public string SubrateItemCode { get; set; }
        public string ItemName { get; set; }
    }


    public class OrderLineCommsion
    {
        public int OrderLineId { get; set; }

        public List<OrderLinePriceBand> OrderLinePriceBand { get; set; } 
    }

    public class OrderLinePriceBand
    {
        public string Name { get; set; }

        public decimal Price { get; set; }

        public List<PriceBandCommision> LBSubrate { get; set; }
    }

    public class PriceBandCommision
    {
        public string Code { get; set; }

        public decimal Commssion { get; set; }

        public bool IsPercentage { get; set; }
    }

    public class SubrateTemplatesDetail
    {
        public SubrateTemplatesDetail() { }
        public SubrateTemplatesDetail(string SubrateTemplateCode, string TemplateName, string CreatedBy, DateTime UpdtedOn)
        {
            this.SubrateTemplateCode = SubrateTemplateCode;
            this.TemplateName = TemplateName;
            this.CreatedBy = CreatedBy;
            this.UpdateOn = UpdtedOn;
        }
        /// <summary>
        ///SubrateTemplateID
        /// </summary>
        public int SubrateTemplateID { get; set; }

        /// <summary>
        ///SubrateTemplateCode
        /// </summary>
        public string SubrateTemplateCode { get; set; }

        /// <summary>
        ///TemplateName
        /// </summary>
        public string TemplateName { get; set; }

        /// <summary>
        ///UpdateOn
        /// </summary>
        public DateTime UpdateOn { get; set; }

        /// <summary>
        ///CreatedBy
        /// </summary>
        public string CreatedBy { get; set; }
    }
}
