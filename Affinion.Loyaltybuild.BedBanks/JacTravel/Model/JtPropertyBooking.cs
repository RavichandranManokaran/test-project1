﻿using System;
using System.Xml.Serialization;

namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Model
{
    [Serializable()]
    [XmlType(TypeName = "PropertyBooking")]
    public class JtPropertyBooking
    {
        public string PropertyBookingReference { get; set; }
        public string Supplier { get; set; }
        public string SupplierReference { get; set; }
    }
}
