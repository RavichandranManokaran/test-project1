﻿using System.Collections.Generic;

namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Model
{
    public class RoomBooking
    {
        public string PropertyRoomTypeID { get; set; }
        public string BookingToken { get; set; }
        public string MealBasisID { get; set; }
        public string Adults { get; set; }
        public string Children { get; set; }
        public string Infants { get; set; }
        public List<JtChildAge> ChildAges { get; set; }
        public List<JtGuest> Guests { get; set; }
    }
}
