﻿/****** Object:  StoredProcedure [dbo].[LB_GetPaymentsWithoutRefunds]    Script Date: 2/12/2016 11:40:41 AM ******/
DROP PROCEDURE [dbo].[LB_GetPaymentsWithoutRefunds]
GO

/****** Object:  StoredProcedure [dbo].[LB_GetPaymentsWithoutRefunds]    Script Date: 2/12/2016 11:40:41 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LB_GetPaymentsWithoutRefunds]
	@OrderLineId INT,
	@PaymentTypeId INT,
	@CurrencyTypeId INT		
AS
BEGIN
	--Payment History Payments
	SELECT	OLP.ReferenceId [ReferenceId],
			SUM(OLP.Amount) [Amount],
			SUM(CASE WHEN DueTypeId = 1 THEN OLP.Amount ELSE 0 END) [ProviderAmount],
			SUM(CASE WHEN DueTypeId = 2 THEN OLP.Amount ELSE 0 END) [CommissionAmount],
			SUM(CASE WHEN DueTypeId = 3 THEN OLP.Amount ELSE 0 END) [ProcessingFee],
			MIN(OLP.CreatedDate) [Createddate]
	FROM LB_OrderLinePayment OLP
	WHERE
		OLP.PaymentMethodID IN (SELECT B.PaymentMethodID FROM uCommerce_ClientIDPaymentMethodMapping A
			INNER JOIN uCommerce_ClientIDPaymentMethodMapping B ON A.[Description] = B.[Description]
			WHERE a.[PaymentMethodId] = @PaymentTypeId) AND
		OLP.CurrencyTypeId = @CurrencyTypeId AND
		OLP.OrderLineID IN (SELECT OrderLineID FROM [dbo].[LB_FnRelatedOrderLines](@OrderLineId))
	GROUP BY  OLP.ReferenceId
END 
GO

