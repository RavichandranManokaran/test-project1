﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Payment;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;
using Affinion.LoyaltyBuild.AdminPortal.Extensions;

namespace Affinion.LoyaltyBuild.AdminPortal.sitecore_modules.Shell.Affinion.Finance
{
    public partial class HotelDemands : System.Web.UI.Page
    {
        IReportsService iReportsService = new ReportsService();
        SearchCriteria objSearchCriteria = new SearchCriteria();
        protected int TotalOfRecords { get; private set; }
        APOraclePayment objAPOraclePayment = null;
        APInvoice objAPInvoice = null;
        private IPaymentService _paymentService;
        private static string _sortDirection = "DESC";
        private static string _sortExpresssion = "HotelID";

        private static List<APInvoice> _lbAPInvoiceDemands = new List<APInvoice>();
        public static List<APInvoice> LBAPInvoiceDemands
        {
            get
            {
                return _lbAPInvoiceDemands;
            }
            set
            {
                _lbAPInvoiceDemands = value;
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (LBAPInvoiceDemands != null)
                    LBAPInvoiceDemands.Clear();
                BindCurrency();
            }
            this.lblMessage.Visible = false;
        }

        private void GetDemands(string searchData)
        {
            try
            {
                iReportsService = new ReportsService();
                objSearchCriteria = new SearchCriteria();

                string[] dates = this.DateRange.Value.ToString().Split('-');
                objSearchCriteria.DateRange = InputDateFormatting(dates[0]);
                objSearchCriteria.DateUpTo = InputDateFormatting(dates[1]);
                if (!string.IsNullOrEmpty(this.Currency.SelectedValue))
                    objSearchCriteria.Currency = this.Currency.SelectedValue;
                if (!string.IsNullOrEmpty(this.PaymentStatus.SelectedValue))
                    objSearchCriteria.PaymentStatus = int.Parse(this.PaymentStatus.SelectedValue);
                else
                    objSearchCriteria.PaymentStatus = null;
                if (!string.IsNullOrEmpty(searchData))
                    objSearchCriteria.SearchText = searchData.Trim();
                else
                    objSearchCriteria.SearchText = null;

                 LBAPInvoiceDemands = iReportsService.GetDemands(objSearchCriteria);

                 SetRecordCount();

                this.ResultGridView.DataSource = LBAPInvoiceDemands.SortList(_sortExpresssion, _sortDirection).ToList(); 
                this.ResultGridView.DataBind();

                if (LBAPInvoiceDemands != null && LBAPInvoiceDemands.Count > 0 && ResultGridView != null && ResultGridView.HeaderRow != null)
                {
                   CheckBox chkSelectAll = (CheckBox)ResultGridView.HeaderRow.FindControl("chkSelectAll");
                   int count = LBAPInvoiceDemands.Where(lst => lst.PaymentStatus == 0).ToList().Count;
                    if(count==0)
                    {
                        chkSelectAll.Enabled = false;
                    }
                }

            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.AdminPortal, ex, null);
                throw;
            }

        }

        private void SetRecordCount()
        {
            TotalOfRecords = 0;
            lblTotalRecords.Text = "0";
            if (LBAPInvoiceDemands != null && LBAPInvoiceDemands.Count > 0)
            {
                TotalOfRecords = LBAPInvoiceDemands.Count;
                lblTotalRecords.Text = LBAPInvoiceDemands.Count.ToString();
            }
            else
            {
                LBAPInvoiceDemands = new List<APInvoice>();
            }

        }

        private void PersistSupplierPaymentDemands(APInvoice objAPInvoice)
        {
            if (LBAPInvoiceDemands != null && LBAPInvoiceDemands.Count > 0)
            {
                LBAPInvoiceDemands.Where(lst => lst.OrderID == objAPInvoice.OrderID && lst.OrderLineID == objAPInvoice.OrderLineID).ToList().
                    ForEach(lstUpdate =>
                    {
                        lstUpdate.IsChecked = objAPInvoice.IsChecked;
                    });
            }
        }

        protected void Demands_Click(object sender, EventArgs e)
        {
            GetDemands(string.Empty);
            SearchBox.Text = string.Empty;
        }

        private DateTime InputDateFormatting(string date)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(date))
                {
                    return default(DateTime);
                }
                else
                {
                    DateTime tmpDateTime;
                    try
                    {
                        tmpDateTime = DateTime.Parse(date);
                        return tmpDateTime;
                    }
                    catch
                    {
                        string[] words = date.Split('/');
                        string newstring = words[1] + "/" + words[0] + "/" + words[2];
                        DateTime dt = Convert.ToDateTime(newstring);
                        return dt;
                    }
                }
            }
            catch
            {
                return default(DateTime);
            }

        }

        protected void PostToOracle_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (GridViewRow gvRow in this.ResultGridView.Rows)
                {
                    objAPInvoice = new APInvoice();
                    HiddenField hdnOrderID = (HiddenField)gvRow.FindControl("hdnOrderID");
                    HiddenField hdnOrderLineID = (HiddenField)gvRow.FindControl("hdnOrderLineID");
                    CheckBox chkSelect = (CheckBox)gvRow.FindControl("chkSelect");

                    if (chkSelect.Checked)
                        objAPInvoice.IsChecked = true;
                    else
                        objAPInvoice.IsChecked = false;

                    objAPInvoice.OrderID = int.Parse(hdnOrderID.Value);
                    objAPInvoice.OrderLineID = int.Parse(hdnOrderLineID.Value);
                    PersistSupplierPaymentDemands(objAPInvoice);

                }
                if (LBAPInvoiceDemands != null && LBAPInvoiceDemands.Count > 0)
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = string.Empty;
                    if (LBAPInvoiceDemands.Where(lst => lst.IsChecked == true).ToList().Count == 0)
                    {
                        lblMessage.Text = "Please select hotel demand details";
                        return;
                    }

                    List<APOraclePayment> LBSupplierPaymentDemands = new List<APOraclePayment>();

                   foreach (APInvoice objAPInvoice in LBAPInvoiceDemands)
                   {
                       objAPOraclePayment = new APOraclePayment();
                       objAPOraclePayment.ProviderID = objAPInvoice.ProviderID;
                       objAPOraclePayment.OrderId = objAPInvoice.OrderID;
                       objAPOraclePayment.OrderDate = objAPInvoice.BookingDate;
                       objAPOraclePayment.CurrencyCode = objAPInvoice.Currency;
                       objAPOraclePayment.CurrencyID = objAPInvoice.CurrencyID;
                       objAPOraclePayment.EuroRate = 0;
                       objAPOraclePayment.CustomerID = objAPInvoice.CustomerID;
                       objAPOraclePayment.SuggestedAmount = objAPInvoice.SuggestedAmount;
                       objAPOraclePayment.PostedAmount = objAPInvoice.SuggestedAmount;
                       objAPOraclePayment.SupplierPaymentId = objAPInvoice.SupplierPaymentId;
                       objAPOraclePayment.OrderLineID = objAPInvoice.OrderLineID;
                       objAPOraclePayment.CreatedBy = Sitecore.Security.Accounts.User.Current.DisplayName;
                       objAPOraclePayment.UpdatedBy = Sitecore.Security.Accounts.User.Current.DisplayName;
                       objAPOraclePayment.IsChecked = objAPInvoice.IsChecked;
                       if (objAPInvoice.IsChecked && objAPInvoice.PaymentStatus == 0)
                       LBSupplierPaymentDemands.Add(objAPOraclePayment);
                   }

                    List<APOraclePayment> lstAPOraclePayment = new List<APOraclePayment>();
                    lstAPOraclePayment = LBSupplierPaymentDemands.Where(lst => lst.IsChecked == true).ToList();
                    iReportsService = new ReportsService();
                    ValidationResponse objValidationResponse = iReportsService.APPaymentRequest(lstAPOraclePayment,"HotelDemands");

                    if (objValidationResponse.IsSuccess)
                    {
                        this.SearchBox.Text = string.Empty;
                        ClearValues();
                        GetDemands(string.Empty);

                    }
                    lblMessage.Text = "Hotel demand" + objValidationResponse.Message.ToString();
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.AdminPortal, ex, null);
                throw;
            }

        }

        private void BindCurrency()
        {
            _paymentService = ContainerFactory.Instance.GetInstance<IPaymentService>();
            List<CurrencyDetail> currencyList = new List<CurrencyDetail>();
            currencyList = _paymentService.GetCurrencyListDetails();
            Currency.DataSource = currencyList;
            Currency.DataTextField = "Name";
            Currency.DataValueField = "Name";
            Currency.DataBind();
            Currency.Items.Insert(0, new ListItem { Text = "All", Value = "" });
        }

        protected void ResultGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
          
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblPostedDate = (Label)e.Row.FindControl("lblPostedDate");
                HiddenField hdnOrderID = (HiddenField)e.Row.FindControl("hdnOrderID");
                HiddenField hdnOrderLineID = (HiddenField)e.Row.FindControl("hdnOrderLineID");
                Label lblPaidInOracle = (Label)e.Row.FindControl("lblPaidInOracle");
                Label lblDatePostedToOracle = (Label)e.Row.FindControl("lblDatePostedToOracle");
                CheckBox chkSelect = (CheckBox)e.Row.FindControl("chkSelect");
                ImageButton btnHotelDemandDelete = (ImageButton)e.Row.FindControl("btnHotelDemandDelete");

                if (lblPaidInOracle.Text == "0")
                {
                    int count = 0;
                    if (LBAPInvoiceDemands != null && LBAPInvoiceDemands.Count > 0)
                    {
                        count = LBAPInvoiceDemands.Where(lst => lst.OrderID == int.Parse(hdnOrderID.Value) && lst.OrderLineID == int.Parse(hdnOrderLineID.Value) && lst.IsChecked == true).ToList().Count;
                    }

                    if (count > 0)
                    {
                        chkSelect.Checked = true;
                        
                    }
                    lblPaidInOracle.Text = "UNPAID";
                    if (!Sitecore.Security.Accounts.User.Current.DisplayName.ToLower().ToString().Contains("admin"))
                    {
                        btnHotelDemandDelete.Enabled = false;
                    }
                }
                else 
                {
                    if (lblPaidInOracle.Text == "1")
                        lblPaidInOracle.Text = "PAID";
                    else
                        lblPaidInOracle.Text = "PENDING";

                    chkSelect.Checked = false;
                    chkSelect.Attributes.Add("style", "display:none");
                    btnHotelDemandDelete.Visible = false;
                }
               
                DateTime postedDate = InputDateFormatting(lblDatePostedToOracle.Text);
                if (postedDate == DateTime.MinValue)
                {
                    lblPostedDate.Text = string.Empty;
                    lblDatePostedToOracle.Text = string.Empty;
                }

                
            }
        }

        protected void ResultGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            foreach (GridViewRow row in ResultGridView.Rows)
            {
                objAPInvoice = new APInvoice();
                var chkBox = row.FindControl("chkSelect") as CheckBox;

                HiddenField hdnOrderID = (HiddenField)row.FindControl("hdnOrderID");
                HiddenField hdnOrderLineID = (HiddenField)row.FindControl("hdnOrderLineID");
                Label lblCurrency = (Label)row.FindControl("lblCurrency");

                if (chkBox.Checked)
                {
                    objAPInvoice.IsChecked = true;
                }
                else
                {
                    objAPInvoice.IsChecked = false;
                }

                objAPInvoice.OrderID = int.Parse(hdnOrderID.Value);
                objAPInvoice.OrderLineID = int.Parse(hdnOrderLineID.Value);
                PersistSupplierPaymentDemands(objAPInvoice);
            }
            ResultGridView.PageIndex = e.NewPageIndex;
            SetRecordCount();
            this.ResultGridView.DataSource = LBAPInvoiceDemands.SortList(_sortExpresssion, _sortDirection).ToList();
            this.ResultGridView.DataBind();

            if (LBAPInvoiceDemands != null && LBAPInvoiceDemands.Count > 0 && ResultGridView != null && ResultGridView.HeaderRow != null)
            {
                CheckBox chkSelectAll = (CheckBox)ResultGridView.HeaderRow.FindControl("chkSelectAll");
                if (hdnSelectAll.Value == "true")
                {
                    chkSelectAll.Checked = true;
                }
                else
                {
                    chkSelectAll.Checked = false;
                }
            }
           
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            try
            {
                ClearValues();
                GetDemands(SearchBox.Text);
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.AdminPortal, ex, null);
                throw;
            }

        }
        public void ClearValues()
        {
            hdnSelect.Value = "false";
            hdnSelectAll.Value = "false";
            LBAPInvoiceDemands.Clear();
        }

        protected void btnHotelDemandDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton button = sender as ImageButton;
            GridViewRow gvrow = button.NamingContainer as GridViewRow;
            HiddenField hdnOrderID = (HiddenField)gvrow.FindControl("hdnOrderID");
            HiddenField hdnOrderLineID = (HiddenField)gvrow.FindControl("hdnOrderLineID");
            int OrderID = 0;
            int OrderLineID = 0;
            if (!string.IsNullOrEmpty(hdnOrderID.Value))
                OrderID = (Convert.ToInt32(hdnOrderID.Value));
            if (!string.IsNullOrEmpty(hdnOrderLineID.Value))
                OrderLineID = (Convert.ToInt32(hdnOrderLineID.Value));
            try
            {
                ValidationResponse objValidationResponse = iReportsService.DeleteHotelDemands(OrderID, OrderLineID);

                if (objValidationResponse.IsSuccess)
                {
                    ClearValues();
                    GetDemands(string.Empty);

                }
                this.lblMessage.Visible = true;
                lblMessage.Text = objValidationResponse.Message.ToString();
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.AdminPortal, ex, null);
                throw;
            }
        }

         [System.Web.Services.WebMethod]
       public static bool UpdateSelectedCheck(string SelectMode, bool Status, Int32 OrderID, Int32 OrderLineID)
       {
           bool isSuccess = false;
           if (SelectMode == "SelectAll")
           {
               if (LBAPInvoiceDemands != null && LBAPInvoiceDemands.Count > 0)
               {
                   if (Status)
                   {
                       LBAPInvoiceDemands.ToList().ForEach(lst => { lst.IsChecked = true; });
                   }
                   else
                   {
                       LBAPInvoiceDemands.ToList().ForEach(lst => { lst.IsChecked = false; });
                   }
               }
           }
           else if (SelectMode == "Select" && OrderID > 0 && OrderLineID>0)
           {
               if (LBAPInvoiceDemands != null && LBAPInvoiceDemands.Count > 0)
               {
                   if (Status)
                   {
                       LBAPInvoiceDemands.Where(lst => lst.OrderID == OrderID && lst.OrderLineID == OrderLineID).ToList().ForEach(lst => { lst.IsChecked = true; });
                   }
                   else
                   {
                       LBAPInvoiceDemands.Where(lst => lst.OrderID == OrderID && lst.OrderLineID == OrderLineID).ToList().ForEach(lst => { lst.IsChecked = false; });
                   }
               }
           }

           if (LBAPInvoiceDemands != null && LBAPInvoiceDemands.Count > 0)
           {
               int count = 0;
               count = LBAPInvoiceDemands.Where(lst => lst.IsChecked == true).ToList().Count;
               if(count>0)
               {
                   isSuccess = true;
               }
           }

           return isSuccess;
       }

         protected void ResultGridView_Sorting(object sender, GridViewSortEventArgs e)
         {
             SetSortDirection(_sortDirection);
             if (e != null && e.SortExpression != null)
                 _sortExpresssion = e.SortExpression.ToString();

             if (LBAPInvoiceDemands != null && LBAPInvoiceDemands.Count > 0)
             {
                 SetRecordCount();
                 this.ResultGridView.DataSource =LBAPInvoiceDemands.SortList(_sortExpresssion,_sortDirection).ToList();
                 this.ResultGridView.DataBind();
             }
             else
             {
                 this.ResultGridView.DataSource = LBAPInvoiceDemands;
                 this.ResultGridView.DataBind();
             }

         }

         protected void SetSortDirection(string sortDirection)
         {
             if (sortDirection == "ASC")
             {
                 _sortDirection = "DESC";
             }
             else
             {
                 _sortDirection = "ASC";
             }
         }
    
    }
}