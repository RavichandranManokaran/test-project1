﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           ILocationService.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.ExternalServices.LocationService
/// Description:           ILocationService interface is uded to define generic methods which can be used inorder to
///                        Get all the locations and filters list of locations in a defined DTO type. 

#region Directives

using System.Collections.Generic;

#endregion Directives

namespace Affinion.LoyaltyBuild.ExternalServices.LocationService.Services
{
    /// <summary>
    /// Location Service Interface
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ILocationService<T>
    {
        /// <summary>
        /// Gets all the location service related data
        /// </summary>
        /// <returns>returns a list of data objects(DTO) preffered list of locations</returns>
        List<T> GetLocations();

        /// <summary>
        /// Get a list of location based on the selected location
        /// </summary>
        /// <param name="location">location name</param>
        /// <returns>Filtered list of location details</returns>
        List<T> GetLocations(string location);
    }
}
