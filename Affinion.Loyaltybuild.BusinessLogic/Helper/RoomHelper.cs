﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           RoomHelper.cs
/// Sub-system/Module:     Affinion.Loyaltybuild.BusinessLogic.Helper
/// Description:           Helper methods related to room
/// </summary>

using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Search;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Availability;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UCommerce.EntitiesV2;

namespace Affinion.Loyaltybuild.BusinessLogic.Helper
{
    public static class RoomHelper
    {
        /// <summary>
        /// Gets the processed list of subscribed offers from current supplier
        /// </summary>
        /// <returns>Returns a collection of offers</returns>
        public static SupplierData GetOffers(Item supplierItem, Item clientItem, DateTime StartDate, DateTime EndDate)
        {

            try
            {

                if (supplierItem == null || clientItem == null || StartDate == DateTime.MinValue || EndDate == DateTime.MinValue)
                {
                    return new SupplierData();
                }


                ///Get supplier details
                SupplierData supplier = StoreHelper.GetProducts(supplierItem, clientItem);
                Collection<OfferDataItem> filteredOffersList = new Collection<OfferDataItem>();

                if (supplier == null)
                {
                    return new SupplierData();
                }

                for (int i = 0; i < supplier.Offers.Count; i++)
                {
                    OfferDataItem offer = supplier.Offers[i];

                    ///Get ucommerce category
                    Category uCommerceCategory = Category.FirstOrDefault(x => x.CategoryId == SitecoreFieldsHelper.GetInteger(supplierItem, "UCommerceCategoryID", 0));

                    RoomAvailabilityService roomAvailabilityService = new RoomAvailabilityService();
                    List<RoomAvailabilityDetail> roomAvailabilityDetails = roomAvailabilityService.FindRoomAvailability(Convert.ToString(uCommerceCategory.Guid), offer.OfferSku, StartDate, EndDate, false);

                    ///Set room availability
                    offer.RoomAvailability = roomAvailabilityDetails;

                }


                return supplier;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                return new SupplierData();
            }
        }
    }
}
