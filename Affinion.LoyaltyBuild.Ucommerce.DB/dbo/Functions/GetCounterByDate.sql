﻿CREATE FUNCTION [dbo].[GetCounterByDate]
(
	@date datetime 	 
)
RETURNS INT
AS
BEGIN

	DECLARE @Counter INT;
	DECLARE @input_date DATETIME
	
	SELECT @input_date = CAST('1/1/1990' AS Datetime)

	SET @Counter = DATEdiff(DAY,@input_date,@date)

	RETURN @Counter

END
