﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class OrderLineCampaignRelation : IEntity
    {
        private int? _hashCode;

        // Methods
        public static IQueryable<OrderLineCampaignRelation> All()
        {
            return GetRepo().Select();
        }

        public virtual void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<OrderLineCampaignRelation, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            OrderLineCampaignRelation objA = obj as OrderLineCampaignRelation;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<OrderLineCampaignRelation, bool>> expression)
        {
            return All().Any<OrderLineCampaignRelation>(expression);
        }

        public static IList<OrderLineCampaignRelation> Find(Expression<Func<OrderLineCampaignRelation, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<OrderLineCampaignRelation>();
        }

        public static OrderLineCampaignRelation FirstOrDefault(Expression<Func<OrderLineCampaignRelation, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<OrderLineCampaignRelation>();
        }

        public static OrderLineCampaignRelation Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<OrderLineCampaignRelation> GetRepo()
        {
            //if (_repo != null)
            //{
            //    return _repo;
            //}
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<OrderLineCampaignRelation>>();
            
        }

        public virtual void Save()
        {
            try
            {
                GetRepo().Save(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static OrderLineCampaignRelation SingleOrDefault(Expression<Func<OrderLineCampaignRelation, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties

        public virtual int OrderLineCampaignRelationId { get; protected set; }

        public virtual int Id
        {
            get
            {
                return this.OrderLineCampaignRelationId;
            }
        }

        public virtual int OrderId { get; set; }

        public virtual int OrderLineId { get; set; }

        public virtual int DiscountId { get; set; }

        public virtual int CampaignItemId { get; set; }


    }
}
