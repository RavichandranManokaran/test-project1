﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           QueryStringHelper.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Query string Helper Class
/// </summary>

#region Using Statemants
using Affinion.LoyaltyBuild.Common.Instrumentation;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;
#endregion

namespace Affinion.LoyaltyBuild.Common.Utilities
{
    public class QueryStringHelper
    {
        #region Private Variables
        private string baseUrlString = string.Empty;
        private Dictionary<string, string> paramNameDictionary = new Dictionary<string, string>();
        //private NameValueCollection queryStringParameters = new NameValueCollection();
        private UriBuilder builder;
        #endregion    

        public const string encryptParamName = "_e";

        public NameValueCollection QueryStringParameters { get; set; }

        #region Constructors
        /// <summary>
        /// QueryStringHelper Constructor
        /// </summary>
        /// <param name="baseUrl">Base Url</param>
        public QueryStringHelper(Uri baseUrl)
        {
            try
            {
                if (baseUrl != null)
                {
                    // extract the query strings
                    this.builder = new UriBuilder(baseUrl);
                    this.QueryStringParameters = HttpUtility.ParseQueryString(builder.Query);

                    //set base url
                    this.baseUrlString = baseUrl.AbsoluteUri;
                }

            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                throw;
            }
        }

        /// <summary>
        /// constructor overload
        /// </summary>
        /// <param name="request">Http request</param>
        public QueryStringHelper(HttpRequest request)
            : this(request != null ? request.Url : null)
        {
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Set parameter value
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void SetValue(string key, string value)
        {
            try
            {
                if (this.paramNameDictionary.ContainsKey(key))
                {
                    this.paramNameDictionary[key] = value;
                }
                else
                {
                    this.paramNameDictionary.Add(key, value);
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                throw;
            }
        }

        /// <summary>
        /// Create query string
        /// </summary>
        /// <param name="encrypt">If True values will be encrypted</param>
        /// <returns>Uri</returns>
        public Uri GetUrl(bool encrypt)
        {
            try
            {
                if (!string.IsNullOrEmpty(this.baseUrlString))
                {
                    builder.Port = -1;

                    if (encrypt)
                    {
                        //check if url already encrypted
                        if (!IsEncrypted())
                        {
                            this.SetValue(encryptParamName, "1");
                        }
                    }

                    foreach (var param in QueryStringParameters.AllKeys)
                    {
                        if (encrypt && !param.Equals(encryptParamName))
                        {
                            QueryStringParameters[param] = Cryptography.Encrypt(QueryStringParameters[param]);
                        }
                    }
                    ///create string parameters and assign values
                    ///skip encrypt parameter value encryption
                    foreach (var param in paramNameDictionary)
                    {
                        if (encrypt && !param.Key.Equals(encryptParamName))
                        {
                            QueryStringParameters[param.Key] = Cryptography.Encrypt(param.Value);
                        }
                        else
                        {
                            QueryStringParameters[param.Key] = param.Value;
                        }
                    }

                    builder.Query = QueryStringParameters.ToString();
                    return builder.Uri;
                }
                return null;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                throw;
            }
        }

        /// <summary>
        /// GetQueryString overload
        /// </summary>
        /// <returns>Uri</returns>
        public Uri GetUrl()
        {
            return GetUrl(false);
        }

        /// <summary>
        /// Get value
        /// </summary>
        /// <param name="key">key</param>
        /// <returns>value</returns>
        public string GetValue(string key)
        {
            try
            {
                // if encrypted decrypt and return
                if (IsEncrypted() && (key != encryptParamName))
                {
                    return Cryptography.Decrypt(this.QueryStringParameters.Get(key));
                }

                // else return the raw value
                return this.QueryStringParameters.Get(key);
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                throw;
            }
        }
       #endregion

        #region Private Methods
        /// <summary>
        /// Check if url encrypted
        /// </summary>
        /// <returns>True If Encrypted</returns>
        private bool IsEncrypted()
        {
            return (!string.IsNullOrEmpty(QueryStringParameters.Get(encryptParamName)) &&
                (QueryStringParameters.Get(encryptParamName)).Equals("1"));
        }
        #endregion
    }
}
