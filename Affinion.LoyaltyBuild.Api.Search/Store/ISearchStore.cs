﻿using Affinion.LoyaltyBuild.Api.Search.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Api.Search.Store
{
    /// <summary>
    /// Search store interface
    /// </summary>
    interface ISearchStore
    {
        /// <summary>
        /// Get serach key
        /// </summary>
        /// <returns></returns>
        SearchKey GetSearchKey();

        /// <summary>
        /// Save search key
        /// </summary>
        /// <param name="key"></param>
        void SetSearchKey(SearchKey key);

        /// <summary>
        /// Get search result
        /// </summary>
        /// <returns></returns>
        PagedProviderCollection GetSearchResult();

        /// <summary>
        /// Set search result
        /// </summary>
        /// <param name="collection"></param>
        void SetSearchRestult(PagedProviderCollection collection);
    }
}
