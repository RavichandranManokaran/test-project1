﻿using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Communications.Services;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Customer;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Email;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

//<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
/// </summary>

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Handlers.CS
{
    /// <summary>
    /// Summary description for CommunicationHistoryHandler
    /// </summary>
    public class CommunicationHistoryHandler : IHttpHandler
    {

        private NameValueCollection _history = null;
        public EmailService email;
        public MailService mail;

        public Booking boooking;
        public CommunicationHistoryService historyService;

        /// <summary>
        /// Process request
        /// </summary>
        /// <param name="context">HttpContext object</param>
        /// <returns></returns>
        public void ProcessRequest(HttpContext context)
        {
            int historyId = 0;        
            try
            {
                _history = HttpUtility.ParseQueryString(context.Request.Url.Query.ToLower());

                historyId = Convert.ToInt32(_history["historyId"]);
                if (historyId == 0)
                {
                    throw new Exception("invalid request");
                }
                historyService = new CommunicationHistoryService();
                CommunicationHistoryInfo comminfo = new CommunicationHistoryInfo();
                comminfo = historyService.GetCommucationHistoryByHistoryID(historyId);
                string fromEmail = comminfo.From;
                string toEmail = comminfo.To;
                string subject = comminfo.Subject;
                string emailText = comminfo.EmailHtmlText;
                email = new EmailService();
                email.Send(fromEmail, toEmail, subject, emailText, true);
                context.Response.Write("1");     
            }
            catch (Exception ex)
            {
                context.Response.Write("0");
                Diagnostics.WriteException(DiagnosticsCategory.BusinessLogic, ex, this);
            }
        }

        /// <summary>
        /// Property IsReusable
        /// </summary>       
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}