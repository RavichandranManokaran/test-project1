﻿using System.Xml.Serialization;

namespace Affinion.LoyaltyBuild.BedBanks.Troika.Model.Coldfusion
{
    public class ColdFusionVarChar
    {
        [XmlAttribute("cftype")]
        public string CfType = "VARCHAR";

        [XmlText]
        public string Value { get; set; }
    }
}
