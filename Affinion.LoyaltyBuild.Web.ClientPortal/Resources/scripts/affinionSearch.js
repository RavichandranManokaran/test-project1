﻿jQuery(document).ready(function () {

    jQuery('#NoOfChildrenDropDownList').change(function (e) {

        new ValidateSearchCriteria(e);
    });

    jQuery('.childBedAgeDropdown').change(function (e) {

        new ValidateSearchCriteria(e);
    });


    jQuery('.childBedTypeDropdown').change(function (e) {

        new ValidateSearchCriteria(e);
    });

});

function ValidateSearchCriteria(element) {

    jQuery(".roomPanels:visible").each(function () {
        var roomPanel = this;

        var noOfChildrenDropdown = jQuery(this).find('#NoOfChildrenDropDownList');

        var noOfchildren = Number(noOfChildrenDropdown.val());

        if (noOfchildren > 0) {

            var lessThanTwoChildrenCount = getLessThantwoChildrenCount(this, noOfchildren);
            //var parentsBedCount = getParentsBedCount(this, noOfchildren);

            var parentsBed = { isOccupied: false, age: 0, bedTypeDropdown: null };

            var ageDropdown, bedTypeDropdown, selectedAge, selectedBedIndex, selectedAgeIndex;

            for (i = 0; i < noOfchildren; i++) {

                var parentsBedCount = getParentsBedCount(this, noOfchildren);

                ageDropdown = jQuery(this).find('#AgeDropdownList' + (i + 1));

                bedTypeDropdown = jQuery(this).find('#BedTypeDropDown' + (i + 1));

                enableAllOptions(bedTypeDropdown);

                selectedAge = Number(ageDropdown.val());
                selectedBedIndex = bedTypeDropdown.prop('selectedIndex');
                selectedAgeIndex = ageDropdown.prop('selectedIndex');

                //If age is more than five disable parents bed option and select extra bed
                if (selectedAge > 5) {

                    bedTypeDropdown.find('option').eq(0).prop('disabled', true);
                    bedTypeDropdown.prop('selectedIndex', 1);
                }
                    //Single less than two scenario
                    //This child has to be in parents bed
                    //So select parents bed option and disable other options
                else if (lessThanTwoChildrenCount === 1 && ((selectedAge <= 2 && selectedAge > 0) || selectedAgeIndex === 1)) {

                    bedTypeDropdown.find('option').eq(1).prop('disabled', true);
                    bedTypeDropdown.prop('selectedIndex', 0);
                    parentsBed.isOccupied = true;
                    parentsBed.bedTypeDropdown = bedTypeDropdown;
                    //If user selects 0-12 months then store 1
                    parentsBed.age = selectedAgeIndex === 1 ? 1 : Number(selectedAge);

                }
                    //Parents bed is selected and age is less than or equal to 2.
                else if (selectedBedIndex === 0 && (Number(selectedAge) <= 2 || selectedAgeIndex === 1)) {


                    if (parentsBed.isOccupied) {
                        //Parents bed is selected and age is less than or equal to 2.
                        //But parents bed is already occupied by another child.
                        //So put that child in to extra bed

                        if (element.target.id === bedTypeDropdown.attr('id')) {

                            parentsBed.bedTypeDropdown.prop('selectedIndex', 1);
                            parentsBed.bedTypeDropdown = bedTypeDropdown;
                            parentsBed.age = selectedAgeIndex === 1 ? 1 : Number(selectedAge);
                        }
                        else {
                            //Parent bed is occupied, But not by this child
                            bedTypeDropdown.prop('selectedIndex', 1);

                        }
                    }
                    else {
                        //Parents bed is selected and age is less than or equal to 2.
                        //Nobody occupies parents bed
                        //So mark parents bed as occupied
                        parentsBed.isOccupied = true;
                        parentsBed.age = selectedAgeIndex === 1 ? 1 : Number(selectedAge);
                        parentsBed.bedTypeDropdown = bedTypeDropdown;
                    }
                }
                    //Selected parents bed for a child between 3 to 5.
                    //But parents bed is already occupied
                else if (selectedBedIndex === 0 && Number(selectedAge) >= 2 && Number(selectedAge) <= 5 && parentsBed.isOccupied) {

                    //If the parents bed is occupied by a child older than 2, Put that child to extra bed
                    if (parentsBed.age > 2) {
                        parentsBed.bedTypeDropdown.prop('selectedIndex', 1);
                        parentsBed.bedTypeDropdown = bedTypeDropdown;
                        parentsBed.age = Number(selectedAge);
                    }
                    else {
                        //If the parents bed is occupied by a child less than two, put this child in to extra bed
                        bedTypeDropdown.prop('selectedIndex', 1);
                        bedTypeDropdown.find('option').eq(0).prop('disabled', true);
                    }

                }
                    //Selected parents bed for a child between 3 to 5.
                    //and parents bed is not occupied
                else if (selectedBedIndex === 0 && Number(selectedAge) >= 2 && Number(selectedAge) <= 5 && !parentsBed.isOccupied) {
                    //If no children less than 2, then 3 to 5 child must occupy the parents bed
                    if (lessThanTwoChildrenCount === 0) {
                        parentsBed.isOccupied = true;
                        parentsBed.age = Number(selectedAge);
                        parentsBed.bedTypeDropdown = bedTypeDropdown;
                    }
                        //If there are less than 2 children, Then one of them should occupy parents bed
                    else {
                        bedTypeDropdown.prop('selectedIndex', 1);
                        bedTypeDropdown.find('option').eq(0).prop('disabled', true);
                    }
                }
                    //Disable parents bed option for children older than two because it is already occupied by less than 2 child
                else if (selectedBedIndex === 1 && Number(selectedAge) > 2 && Number(selectedAge) <= 5 && parentsBed.isOccupied) {

                    if (parentsBed.age <= 2) {
                        bedTypeDropdown.prop('selectedIndex', 1);
                        bedTypeDropdown.find('option').eq(0).prop('disabled', true);
                    }

                }

                if (parentsBedCount === 0 && (Number(selectedAge) <= 2 || selectedAgeIndex === 1) && (!parentsBed.isOccupied || parentsBed.age > 2)) {

                    // if (lessThanTwoChildrenCount === 1 || element.target.id !== bedTypeDropdown.attr('id')) {
                    if (element.target.id !== bedTypeDropdown.attr('id')) {
                        parentsBed.isOccupied = true;
                        parentsBed.age = selectedAgeIndex === 1 ? 1 : Number(selectedAge);
                        bedTypeDropdown.prop('selectedIndex', 0);
                        parentsBed.bedTypeDropdown = bedTypeDropdown;
                    }

                }


            }

        }
    });
}

function enableAllOptions(dropDownList) {
    dropDownList.find('option').prop('disabled', false);
}

function getLessThantwoChildrenCount(panel, noOfchildren) {
    var count = 0;
    //var selectedAgeIndex;

    jQuery(panel).find('.childBedAgeDropdown:visible').prop("selectedIndex", function (i, val) {
        if (val <= 3 && val >= 1) {
            count++;
        }
    });

    // for (i = 0; i < noOfchildren; i++) {

    // var ageDropdown = jQuery(panel).find('#AgeDropdownList' + (i + 1));
    // selectedAgeIndex = ageDropdown.prop('selectedIndex');

    // if (ageDropdown.val() <= 2 || selectedAgeIndex === 1) {
    // count++;
    // }
    // }
    
    return count;


}

function getParentsBedCount(panel, noOfchildren) {


    var count = 0;
    jQuery(panel).find('.childBedTypeDropdown:visible').prop("selectedIndex", function (i, val) {
        if (val === 0) {
            count = count + 1;
        }
    });

    return count;
}


function validateFieldsForSubmit() {
    var isValid = true;

    jQuery(".roomPanels:visible").each(function () {
        var roomPanel = jQuery(this);

        var noOfChildrenDropdown = jQuery(this).find('#NoOfChildrenDropDownList');

        var noOfchildren = noOfChildrenDropdown.val();


    });
    //isValid=false;

    jQuery('.childBedAgeDropdown').each(function () {


        //When no value is selected
        if (jQuery(this).closest(".childBedAge").css('display') === 'block' && DropDownDefaultValue0ece8eb2eee54d44a57d45200d6edc5c === this.value) {
            showErrorMessage(ChildAgeRequiredErrorMessage42dc6be294574b13bc5d42f008087203);
            isValid = false;
        }

    });
    return isValid;
}

function showErrorMessage(error) {

    jQuery("#ClientSideErrorPanel").text(error);
    jQuery("#ClientSideErrorPanel").show();
}