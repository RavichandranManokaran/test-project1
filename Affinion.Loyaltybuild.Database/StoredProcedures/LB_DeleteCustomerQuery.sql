
/****** Object:  StoredProcedure [dbo].[LB_DeleteCustomerQuery]    Script Date: 2/16/2016 11:40:56 AM ******/
DROP PROCEDURE [dbo].[LB_DeleteCustomerQuery]

GO

/****** Object:  StoredProcedure [dbo].[LB_DeleteCustomerQuery]    Script Date: 2/16/2016 11:40:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LB_DeleteCustomerQuery]
(
	@QueryId int
) 
AS 
BEGIN
	DELETE FROM [dbo].[LB_CustomerQuery] 
	WHERE QueryId = @QueryId	
END


GO
