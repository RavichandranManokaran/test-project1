﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfferDetailsSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal.Offers.OfferBodySublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>


    <div class="body-container">
        <div class="container">

            <!-- Example row of columns -->
            <div class="row contentarea-outer">

                <div class="col-sm-12">

                    <div class="campaign offer_intro">
                        <sc:FieldRenderer ID="OfferBodyRenderer" FieldName="Body" runat="server" />
                    </div>
                </div>
            </div>

        </div>
    </div>




