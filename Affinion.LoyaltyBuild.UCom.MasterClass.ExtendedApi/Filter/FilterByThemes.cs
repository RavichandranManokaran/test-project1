﻿using Affinion.LoyaltyBuild.UCom.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.Search.Facets;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi.Filter
{
    public class FilterByThemes : IHotelFilter
    {
        public bool IncludeHotel(UCommerce.Documents.Product product, SearchCriteria criteria)
        {
            if (string.IsNullOrEmpty(criteria.Themes))
            {
                return true;
            }
            if (criteria.Themes == "all")
            {
                return true;
            }
            if (!string.IsNullOrEmpty(criteria.Themes))
            {
                KeyValuePair<string, Dictionary<string, object>> experienceProperty = product.Properties.FirstOrDefault(x => x.Value.Any(y => y.Key == ProductDefinationConstant.Theme));
                if (experienceProperty.Value != null)
                {
                    foreach (var item in criteria.Themes.Split(','))
                    {
                        bool found = experienceProperty.Value.Any(x => x.Value.ToString().Contains(item));
                        if (!found)
                        {
                            return false;
                        }
                    }

                }
            }
            return true;
        }


        public UCommerce.Search.Facets.Facet FilterRecordFacet(SearchCriteria criteria)
        {
            if (criteria.Themes != "all" && !string.IsNullOrEmpty(criteria.Themes))
            {
                var facet = new Facet();
                facet.FacetValues = new List<FacetValue>();
                facet.Name = ProductDefinationConstant.Theme;

                //In this example selected values are seperated by a '|' in the querystring value.
                foreach (var value in criteria.Themes.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    facet.FacetValues.Add(new FacetValue() { Value = value });
                }
                return facet;
            }
            return null;

        }
    }
}
