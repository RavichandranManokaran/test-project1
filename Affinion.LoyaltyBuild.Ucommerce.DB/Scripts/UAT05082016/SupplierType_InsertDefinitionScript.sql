﻿Declare @datatypeid int, @definitionid int
select @definitionid=ProductDefinitionId from uCommerce_ProductDefinition where Name='Hotel'
if not exists (select 1 from uCommerce_DataType where DefinitionName = 'SiteCoreSupplierType' )
begin
       insert into uCommerce_DataType (TypeName,Nullable,ValidationExpression,BuiltIn,DefinitionName,Deleted,Guid,CreatedOn,
       CreatedBy,ModifiedOn,ModifiedBy)
       values ('SupplierType',1,'',0,'SiteCoreSupplierType',0,NEWID(),GETDATE(),'admin',GETDATE(),'admin')

	   select @datatypeid=DataTypeId from uCommerce_DataType where [DefinitionName]='SiteCoreSupplierType'

	   insert into uCommerce_ProductDefinitionField(DataTypeId,ProductDefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,Facet,Guid,IsVariantProperty)
       values(@datatypeid,@definitionid,'SupplierType',1,0,1,0,0,0,1,NEWID(),0)
end