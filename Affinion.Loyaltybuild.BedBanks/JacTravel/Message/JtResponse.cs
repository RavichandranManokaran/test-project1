﻿using Affinion.LoyaltyBuild.BedBanks.JacTravel.Model;

namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Message
{
    /// <summary>
    /// Base Class for a JacTravel Response Object
    /// </summary>
    public class JtResponse
    {
        private ReturnStatus _returnStatus;
        public ReturnStatus ReturnStatus
        {
            get { return _returnStatus; }
            set { _returnStatus = value; }
        }

        private long _responseTime { get; set; }
        public long ResponseTime
        {
            get { return _responseTime; }
            set { _responseTime = value; }
        }

        private string _extraInfo { get; set; }
        public string ExtraInfo
        {
            get { return _extraInfo; }
            set { _extraInfo = value; }
        }
    }
}
