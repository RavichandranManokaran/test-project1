﻿namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Model
{
    public class JtLocation
    {
        public string Country { get; set; }
        public int RegionId { get; set; }
        public string Region { get; set; }
        public int ResortId { get; set; }
        public string Resort { get; set; }

        public string FullName
        {
            get
            {
                return Region + ", " + Country;
            }
        }
    }
}
