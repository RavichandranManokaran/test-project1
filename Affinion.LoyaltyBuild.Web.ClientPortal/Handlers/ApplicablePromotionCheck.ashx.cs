﻿using Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi;
using Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts;
using Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Handlers
{
    /// <summary>
    /// Summary description for ApplicablePromotionCheck
    /// </summary>
    public class ApplicablePromotionCheck : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {


            try
            {
                //write html response
                context.Response.Write(LoadUserControl(context));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        /// <summary>
        /// Load User Control
        /// </summary>
        /// <param name="packageDate"></param>
        /// <param name="provider"></param>
        /// <param name="sku"></param>
        /// <param name="variantSku"></param>
        /// <returns></returns>
        protected static string LoadUserControl(HttpContext context)
        {
            using (Page page = new Page())
            {
                string sku = context.Request.Params["sku"];
                string variantsku = context.Request.Params["variantsku"];
                string promotionid = context.Request.Params["promotionid"];
                string checkoutdate = context.Request.Params["checkoutdate"];
                string availability = context.Request.Params["availability"];
                string date = context.Request.Params["date"];
                string noofnights = context.Request.Params["noofnights"];
                string roomtype = context.Request.Params["roomtype"];
                string noofadults = context.Request.Params["noofadults"];
                string noofchildren = context.Request.Params["noofchildren"];
                string currencyid = context.Request.Params["currencyid"];
                DateTime arrivaldate = Convert.ToDateTime(date);
                DateTime checkoutDate = Convert.ToDateTime(checkoutdate);
                int parsenoofNights;
                int pid;
                int parsenoofAdults;
                int parsenoofChild;
                int currencyID;

                int.TryParse(promotionid, out pid);
                int.TryParse(noofnights, out parsenoofNights);
                int.TryParse(noofadults, out parsenoofAdults);
                int.TryParse(noofchildren, out parsenoofChild);
                int.TryParse(currencyid, out currencyID);
                StringBuilder s = new StringBuilder();

                PlaceHolder f = new PlaceHolder();
                SearchCriteria searchCriteria = new SearchCriteria();
                searchCriteria.ArrivalDate = arrivaldate;
                searchCriteria.RoomType = roomtype;
                searchCriteria.NumberOfAdult = parsenoofAdults;
                searchCriteria.NumberOfChild = parsenoofChild;
                searchCriteria.Night = parsenoofNights;

                var skus = new List<string>();
                skus.Add(sku);
                var result = LoyaltyBuildCatalogLibrary.GetApplicableCampaign(skus, searchCriteria, variantsku, pid);

                using (StringWriter writer = new StringWriter(CultureInfo.CurrentCulture))
                {
                    OfflinePortalApplicablePromotions opap = (OfflinePortalApplicablePromotions)page.LoadControl("~/Layouts/SubLayouts/OfflinePortal/OfflinePortalApplicablePromotions.ascx");
                    opap.ListHotelProductDetail = result;
                    opap.CurrencyID = currencyID;
                    f.Controls.Add(opap);
                    page.Controls.Add(f);
                    HttpContext.Current.Server.Execute(page, writer, false);
                    s.Clear();
                    s.Append(writer.ToString());
                }
                return s.ToString();

            }
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}