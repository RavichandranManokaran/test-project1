﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2.Maps;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Maps
{
    public class OrderLineCampaignRelationMap : BaseClassMap<OrderLineCampaignRelation>
    {
        // Methods
        public OrderLineCampaignRelationMap()
        {
            base.Table("uCommerce_OrderLineCampaignRelation");
           // Id(x => x.OrderLineCampaignRelationId).Not.Nullable();
            base.Id(x => x.OrderLineCampaignRelationId, "OrderLineCampaignRelationId").GeneratedBy.Identity();
            base.Map(x => x.OrderId).Not.Nullable();
            base.Map(x => x.OrderLineId).Nullable();
            base.Map(x => x.DiscountId).Not.Nullable();
            base.Map(x => x.CampaignItemId).Not.Nullable();
        }
    }
}

