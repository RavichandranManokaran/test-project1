﻿using Affinion.LoyaltyBuild.BedBanks.Troika;
using Affinion.LoyaltyBuild.Common.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.BedBanks
{
    public partial class MarginManagementAddMarginSublayout : System.Web.UI.UserControl
    {
        string propertyId;
        string type;
        string marginId;

        /// <summary>
        /// Load the page according to add edit for margins
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            litExists.Visible = false;
            litActive.Text = !string.IsNullOrEmpty(Sitecore.Context.Item.Fields["Is Active Text"].Value) ? Sitecore.Context.Item.Fields["Is Active Text"].Value : string.Empty;
            QueryStringHelper helper = new QueryStringHelper(Request.Url);
            type = (!string.IsNullOrEmpty(helper.GetValue("type"))) ? helper.GetValue("type") : string.Empty;
            marginId = (!string.IsNullOrEmpty(helper.GetValue("MarginId"))) ? helper.GetValue("MarginId") : string.Empty;
            propertyId = helper.GetValue("propertyId");
            if (!string.IsNullOrEmpty(type) && type.Equals("add"))    // if it is a add operation
            {
                litActive.Visible = false;
                chkActive.Visible = false;
            }
            if (!string.IsNullOrEmpty(type) && type.Equals("defaultedit")) // if it is a default margin edit operation
            {
                txtDateFrom.Visible = false;
                txtDateTo.Visible = false;
                litActive.Visible = false;
                chkActive.Visible = false;
                divDateFrom.Visible = false;
                divDateTo.Visible = false;
            }
            if (!IsPostBack)
            {
                DataTable data = TroikaDataController.GetMarginForUpdate(marginId);
                if (!string.IsNullOrEmpty(type) && type.Equals("edit"))
                {
                    txtDateFrom.Text = data.Rows[0][1].ToString();
                    txtDateTo.Text = data.Rows[0][2].ToString();
                    txtMargin.Text = data.Rows[0][0].ToString();
                    if (data.Rows[0][3].ToString().Equals("True"))
                    {
                        chkActive.Checked = true;
                    }
                }
                if (!string.IsNullOrEmpty(type) && type.Equals("defaultedit"))
                {
                    txtMargin.Text = data.Rows[0][0].ToString();
                }
            }
        }

        /// <summary>
        /// perform edit or add operation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnConfirm_ServerClick(object sender, EventArgs e)
        {
            txtDateFrom.ReadOnly = false;
            txtDateTo.ReadOnly = false;
            string dateFrom = txtDateFrom.Text;
            string dateTo = txtDateTo.Text;
            string percentage = txtMargin.Text;
            int recordExists = 0;
            QueryStringHelper helper = new QueryStringHelper(Request.Url);
            string errorListId = helper.GetValue("ErrorListId");
            int eListId;
            if (!string.IsNullOrEmpty(dateFrom) && !string.IsNullOrEmpty(dateTo) && !string.IsNullOrEmpty(percentage) && !string.IsNullOrEmpty(propertyId) && !string.IsNullOrEmpty(type) && type.Equals("add"))
            {
                if (!string.IsNullOrEmpty(errorListId))
                {
                    int.TryParse(errorListId, out eListId);
                    recordExists = TroikaDataController.AddProviderMargin(propertyId, percentage, dateFrom, dateTo,"0", "0", "1", eListId);
                }
                else
                {
                    recordExists = TroikaDataController.AddProviderMargin(propertyId, percentage, dateFrom, dateTo);
                }
            }
            else
            {
                string check = "0";
                if (chkActive.Checked)
                {
                    check = "1";
                }
                if (!string.IsNullOrEmpty(type) && type.Equals("defaultedit"))
                {
                    recordExists = TroikaDataController.AddProviderMargin(propertyId, percentage, string.Empty, string.Empty, marginId, "1", "1");
                }
                else
                {
                    int.TryParse(errorListId, out eListId);
                    if (eListId > 0)
                    {
                        recordExists = TroikaDataController.AddProviderMargin(propertyId, percentage, dateFrom, dateTo, marginId, "0", check, eListId);
                    }
                    else
                    {
                        recordExists = TroikaDataController.AddProviderMargin(propertyId, percentage, dateFrom, dateTo, marginId, "0", check);
                    }
                }

            }

            string previousPage = Sitecore.Context.Item.Parent.Paths.ContentPath;

            if (recordExists == 1)
            {
                litExists.Visible = true;
            }
            else
            {
                string pageUrl = string.Empty;
                string errorList = helper.GetValue("ErrorListId");
                if (!string.IsNullOrEmpty(errorList))
                {
                    pageUrl = SitecoreFieldsHelper.GetItemFieldValue(Sitecore.Context.Item, "Margin Details Page Link");
                    if (!string.IsNullOrEmpty(pageUrl))
                        Response.Redirect(pageUrl + "?propertyId=" + propertyId + "&ErrorListId=" + errorList);
                }
                else
                {
                    pageUrl = SitecoreFieldsHelper.GetItemFieldValue(Sitecore.Context.Item, "Margin Details Page Link");
                    if (!string.IsNullOrEmpty(pageUrl))
                        Response.Redirect(pageUrl + "?propertyId=" + propertyId);
                }
            }
        }

        /// <summary>
        /// Go To previous page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_ServerClick(object sender, EventArgs e)
        {
            string pageUrl = string.Empty;
            QueryStringHelper helper = new QueryStringHelper(Request.Url);
            string errorList = helper.GetValue("ErrorListId");
            if (!string.IsNullOrEmpty(errorList))
            {
                pageUrl = SitecoreFieldsHelper.GetItemFieldValue(Sitecore.Context.Item, "Margin Details Page Link");
                if(!string.IsNullOrEmpty(pageUrl))
                    Response.Redirect(pageUrl+"?ErrorListId=" + errorList+"&propertyId="+propertyId);
            }
            else
            {
                pageUrl = SitecoreFieldsHelper.GetItemFieldValue(Sitecore.Context.Item, "Margin Details Page Link");
                if(!string.IsNullOrEmpty(pageUrl))
                    Response.Redirect(pageUrl + "?propertyId=" + propertyId);
            }
        }
    }
}