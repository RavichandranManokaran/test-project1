﻿CREATE TABLE [dbo].[LB_CustomerQuery] (
    [QueryId]     INT            IDENTITY (1, 1) NOT NULL,
    [QueryTypeId] INT            NOT NULL,
    [OrderLineId] INT            NULL,
    [CustomerId]  INT            NULL,
    [Content]     NVARCHAR (MAX) NOT NULL,
    [IsSolved]    BIT            NULL,
    [PartnerId]   NVARCHAR (100) NULL,
    [CampaignId]  NVARCHAR (100) NULL,
    [ProviderId]  NVARCHAR (100) NULL,
    [CreatedDate] DATETIME       DEFAULT (getdate()) NOT NULL,
    [CreatedBy]   NVARCHAR (50)  NULL,
    [UpdatedDate] DATETIME       DEFAULT (getdate()) NULL,
    [UpdatedBy]   NVARCHAR (50)  NULL,
    PRIMARY KEY CLUSTERED ([QueryId] ASC)
);

