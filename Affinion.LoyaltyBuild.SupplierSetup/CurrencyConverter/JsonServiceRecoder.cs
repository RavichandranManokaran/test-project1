﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           JsonServiceRecoder.cs
/// Sub-system/Module:     Suplier Setup(VS project name)
/// Description:            
/// </summary>


#region Using Directives
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.SecurityModel;
using System;
using System.IO;
using System.Net;
#endregion

namespace Affinion.LoyaltyBuild.SupplierSetup.CurrencyConverter
{


    /// <summary>
    ///Implemented Task scheduler runs every 12 hours to record json response from Xignite. 
    /// </summary>
    public class JsonServiceRecoder
    {
        public string Message { get; set; }

        #region Public Method
        //Task Sheduler method
        public void Run()
        {
            try
            {
                CreateSitecoreJsonItem();

            }
            catch (Exception ex)
            {

                Diagnostics.WriteException(DiagnosticsCategory.SupplierSetup, ex, this);
                throw;
            }

        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Xignite json Service Request
        /// </summary>
        /// <returns></returns>
        private string GetJasonServicedata()
        {
            //var request = null;
            try
            {
                //URL 
                string apiURL = String.Format("http://globalcurrencies.xignite.com/xGlobalCurrencies.json/GetRealTimeRateTable?Symbols={0}{1}{2}&PriceType=Mid&_token={3}", "USD", "EUR", "LKR", "205930747C684388B109D6BEEB12876A");

                // construct the request
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiURL);
                request.Method = "GET";


                // get the result
                using (WebResponse response = request.GetResponse())
                {
                    using (StreamReader reader = new StreamReader(response.GetResponseStream(), System.Text.Encoding.ASCII))
                    {

                        var Jsondata = reader.ReadToEnd();

                        //writing to the log for testing purpose
                        Log.Info(Jsondata, this);
                       
                        return Jsondata;


                    }

                }

            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.SupplierSetup, ex, this);
                throw;
            }
        }


        /// <summary>
        /// Create Sitecore item with response data
        /// </summary>
        private void CreateSitecoreJsonItem()
        {
            try
            {
                //Use a security disabler to allow changes
                using (new SecurityDisabler())
                {
                    //You want to alter the item in the master database, so get the item from there
                    Database db = Factory.GetDatabase("master");
                    Item item = db.Items["/sitecore/content/DemoStore/CurrencyData"];

                    //Begin editing
                    item.Editing.BeginEdit();

                    //perform the editing
                    item.Fields["ExchangeRates"].Value = GetJasonServicedata();

                    //Close the editing state
                    item.Editing.EndEdit();
                }

            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.SupplierSetup, ex, this);
                throw;
            }

        }
        #endregion
    }


}

