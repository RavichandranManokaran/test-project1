﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;
using Sitecore.Data.Items;
using Sitecore.Links;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Affinion.LoyaltyBuild.UCom.Common.Extension;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Model;
using Affinion.LoyaltyBuild.Common.Utilities;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.Availability.SupplierPortal.Controls
{


    public partial class AvailabilityView : System.Web.UI.UserControl
    {
        public string Disable;
        public List<ProviderAvialbilityData> avialbilityView;

        protected List<ProductDetail> productDetail;

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            DateTime startDate = DateTime.Now;
            DateTime endDate = new DateTime(startDate.Year, startDate.Month, 1).AddMonths(1).AddDays(-1);
            datepickerFrom.Value = startDate.ConvertDateToString();
            datepickerTo.Value = endDate.ConvertDateToString();
            budatepickerFrom.Value = startDate.ConvertDateToString();
            budatepickerTo.Value = endDate.ConvertDateToString();
            serverdate.Value = startDate.ConvertDateToString();
            string offerUrl = LinkManager.GetItemUrl(SiteConfiguration.GetOfferItem());
            if (!string.IsNullOrEmpty(offerUrl))
            {
                offerurl.Value = offerUrl;
            }
            if (Request["fromdate"] != null)
            {
                DateTime fromDate = Request["fromdate"].ToString().ParseFormatDateTime();
                if (fromDate < startDate)
                {
                    fromDate = startDate;
                }
                datepickerFrom.Value = fromDate.ConvertDateToString();
            }
            if (Request["todate"] != null)
            {
                DateTime todate = Request["todate"].ToString().ParseFormatDateTime();
                if (todate > startDate.AddDays(89))
                {
                    todate = endDate;
                }
                datepickerTo.Value = todate.ConvertDateToString();
            }

            String loggedInProvider_ucom = SitecoreUserProfile.GetItem(SessionKeys.loggedInSupplier_ucom);
            // Guid ucomCategoryId;
            bool validGuid = true;//Guid.TryParse(loggedInProvider_ucom, out ucomCategoryId);
            AvailabilityHelper helper = new AvailabilityHelper(HttpContext.Current);
            List<Item> addOnItems = new List<Item>();
            if (validGuid)
            {
                #region add oons
                var selectedSupplier = SitecoreUserProfile.GetItem(UserProfileKeys.selectedProviderId);
                if (!string.IsNullOrWhiteSpace(selectedSupplier))
                {
                    Item selectedProviderItem = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(selectedSupplier));
                    if (selectedProviderItem != null)
                    {
                        foreach (Item child in selectedProviderItem.Children)
                        {
                            if (child != null && child.Template.ID == Sitecore.Data.ID.Parse(Constants.AddOnsFolderTemplateID))
                            {
                                foreach (Item childItem in child.Children)
                                {
                                    if (childItem.Template.ID == Sitecore.Data.ID.Parse(Constants.AddOnsTemplateID))
                                    {
                                        //if (SitecoreFieldsHelper.CheckBoxChecked(childItem, "IsActive"))
                                        //{
                                            addOnItems.Add(childItem);
                                        //}
                                    }
                                }
                            }
                        }
                    }
                    if (addOnItems != null && addOnItems.Count > 0)
                    {
                        List<AddOns> listAddOns = new List<AddOns>();
                        foreach (var item in addOnItems)
                        {
                            decimal val=0;
                            AddOns addOn = new AddOns();
                            addOn.ProductName = SitecoreFieldsHelper.GetItemFieldValue(item, "Title");
                            addOn.ProductPrice =!String.IsNullOrWhiteSpace(SitecoreFieldsHelper.GetItemFieldValue(item, "Price"))?Decimal.Parse(SitecoreFieldsHelper.GetItemFieldValue(item, "Price")):val;
                            addOn.PackagePrice = !String.IsNullOrWhiteSpace(SitecoreFieldsHelper.GetItemFieldValue(item, "PackagePrice"))?Decimal.Parse(SitecoreFieldsHelper.GetItemFieldValue(item, "PackagePrice")):val;
                            addOn.LBShare = !String.IsNullOrWhiteSpace(SitecoreFieldsHelper.GetItemFieldValue(item, "LBShare")) ? Decimal.Parse(SitecoreFieldsHelper.GetItemFieldValue(item, "LBShare")) : val;
                            addOn.LBSharePKG = !String.IsNullOrWhiteSpace(SitecoreFieldsHelper.GetItemFieldValue(item, "LBShare_PKG")) ? Decimal.Parse(SitecoreFieldsHelper.GetItemFieldValue(item, "LBShare_PKG")) : val;                                
                            addOn.ProviderPrice = addOn.ProductPrice - addOn.LBShare;
                            addOn.ProviderPKGPrice = addOn.PackagePrice - addOn.LBSharePKG;
                            listAddOns.Add(addOn);

                        }
                        if (listAddOns.Count > 0)
                        {
                            addOnsRepeater.DataSource = listAddOns;
                            addOnsRepeater.DataBind();
                        }
                    }
                    this.productDetail = helper.GetProductDetail();


                }
                #endregion
            }
            else
            {
                productDetail = new List<ProductDetail>();
            }
            try
            {
                SupplierInfo data = helper.GetLoggedInSupplierDetailById();
                if (data != null)
                {
                    lblInfo.Text = string.Format("{0}{1}{2}{3}{4}{5}", data.SupplierName,
                        !string.IsNullOrEmpty(data.Group) ? string.Format(" /{0}", data.Group) : string.Empty,
                        !string.IsNullOrEmpty(data.SuperGroup) ? string.Format(" /{0}", data.SuperGroup) : string.Empty,
                        !string.IsNullOrEmpty(data.MapLocation) ? string.Format(", {0}", data.MapLocation) : string.Empty,
                        !string.IsNullOrEmpty(data.Country) ? string.Format(", {0}", data.Country) : string.Empty,
                        !string.IsNullOrEmpty(data.StarRating) ? string.Format(", Rating: {0}", data.StarRating) : string.Empty);
                }
            }
            catch (Exception er)
            {
                Sitecore.Diagnostics.Log.Error(er.Message, new object());
            }
            litbulkuploadhelp.Text = SiteConfiguration.GetHelpContentByType(3);
            litHelp.Text = SiteConfiguration.GetHelpContentByType(1);
            checkRoles();

            string currentUserRole = SitecoreUserProfile.GetItem(UserProfileKeys.currentLoggedinUserRole);
            if (currentUserRole != null && !(currentUserRole.ToLower().Contains(Constants.grpHotelRole)))
            {
                bckHomeButton.Visible = false;
            }
        }

        /// <summary>
        /// check Roles
        /// </summary>
        public void checkRoles()
        {
            string currentUserRole = SitecoreUserProfile.GetItem(UserProfileKeys.currentLoggedinUserRole);
            if (currentUserRole.ToLower().Contains(Constants.accountManagement) || currentUserRole.ToLower().Contains(Constants.marketing) || currentUserRole.ToLower().Contains(Constants.IT) || currentUserRole.ToLower().Contains(Constants.superUser))
            {
                Disable = "";
            }
            else if (currentUserRole.ToLower().Contains(Constants.grpHotelRole) || currentUserRole.ToLower().Contains(Constants.hotelRole) || currentUserRole.ToLower().Contains(Constants.hotelSupervisor) || currentUserRole.ToLower().Contains(Constants.lbAdminRole))
            {
                Disable = "";
            }
            else if (currentUserRole.ToLower().Contains(Constants.finance))
            {
                Disable = "disabled";
            }
        }

        /// <summary>
        /// bckHomeButton Click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void bckHomeButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("/welcome-page/group-availability");
        }

    }
}