﻿CREATE TABLE [dbo].[uCommerce_DataType] (
    [DataTypeId]           INT              IDENTITY (1, 1) NOT NULL,
    [TypeName]             NVARCHAR (50)    NOT NULL,
    [Nullable]             BIT              NOT NULL,
    [ValidationExpression] NVARCHAR (512)   NOT NULL,
    [BuiltIn]              BIT              CONSTRAINT [uCommerce_DF_DataType_BuiltIn] DEFAULT ((0)) NOT NULL,
    [DefinitionName]       NVARCHAR (512)   NOT NULL,
    [Deleted]              BIT              CONSTRAINT [DF__uCommerce__Delet__4A4E069C] DEFAULT ((0)) NOT NULL,
    [Guid]                 UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [CreatedOn]            DATETIME         CONSTRAINT [DF_uCommerce_DataType_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]            NVARCHAR (50)    DEFAULT ('') NOT NULL,
    [ModifiedOn]           DATETIME         CONSTRAINT [DF_uCommerce_DataType_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]           NVARCHAR (50)    DEFAULT ('') NOT NULL,
    CONSTRAINT [uCommerce_PK_DataType] PRIMARY KEY CLUSTERED ([DataTypeId] ASC)
);

