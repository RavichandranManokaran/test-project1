﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Login Page.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.Login_Page" %>
<%@ Import Namespace="Sitecore.Configuration" %>
<%@ Import Namespace="Sitecore.SecurityModel.License" %>



    <div class="login-form-body">
        <div class="container">
            <div class="login-form-body-inner">
                <div class="login-form-body-inner-title" id="emostitle" runat="server">
                    <h3> Supply Management Login</h3>
                </div>
                <div class="login-form-body-inner-form" id="LoginSection" runat="server">

                    <div>
                        <%-- <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" display="Dynamic"  ControlToValidate="txtUsername" ErrorMessage="*Required" class="val-error-msg"></asp:RequiredFieldValidator>--%>
                        <%-- <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2"  Display ="Dynamic" ControlToValidate="Password1" ErrorMessage="*Required" class="val-error-msg"></asp:RequiredFieldValidator>
                         <asp:Label runat="server" ID="lblMessage" class="err_msg"></asp:Label>--%>
                    </div>
                    <div class="col-md-12 login-form-margin" id="ctl15">

                        <span>Username: </span>
                        <asp:TextBox runat="server" ID="txtUsername" autocomplete="off" class="form-control"></asp:TextBox>

                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtUsername" ErrorMessage="*Required" class="val-error-msg"></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-12 login-form-margin" id="ctl20">
                        <span>Password: </span>
                        <asp:TextBox runat="server" type="Password" autocomplete="off" ID="Password1" class="form-control"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="Password1" ErrorMessage="*Required" class="val-error-msg"></asp:RequiredFieldValidator>

                    </div>
                    <div class="col-md-12 login-form-margin" id="ctl25">
                        <asp:Button runat="server" ID="btnGo" class="btn btn-primary btn-bg" Text="Log on" OnClick="LoginSubmit_Click" Style="height: 38px"></asp:Button>
                        <asp:Label runat="server" ID="lblMessage" class="err_msg"></asp:Label>
                         
                    </div>
                    <div class="col-md-12 login-form-margin" id="resetPassword" visible="false" runat="server">
                       <a href="<%=SCChangePasswordPageUrl%>?Username=<%=DomainUser%>" title="reset password">Please Reset your password</a>
                    </div>
                    
                    <div class="col-md-12 login-form-margin" id="ctl30">
                        <div class="col-md-4 no-padding" id="ctl31">

                            <asp:LinkButton ID="LinkButton2" runat="server" OnClick="ForgotUsername_Click" CausesValidation="False">Forgot Username</asp:LinkButton>
                        </div>
                        <div class="col-md-5 no-padding" id="ctl32">
                            <asp:LinkButton ID="LinkButton3" runat="server" OnClick="Forgot_ChangePassword_Click" CausesValidation="False">Forgot/Change Password</asp:LinkButton>
                        </div>
                        <div class="col-md-3 no-padding" id="ctl33">
                           <!-- <a href="https://www.emos.ie/index.cfm?area=login&action=openPDF">Password Guide</a>-->
                            <a href="<%=SCPasswordGuideUrl%>" target="_blank" title="Download Form">Password Guide </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 login-form-margin10" id="help" runat="server">
                    <span>Need Help ?                       
                        <a herf="javascript:void(0)" class="alink" data-toggle="modal" data-target="#myModal">Contact Us</a></span>
                </div>
            </div>
        </div>
        <div id="ForgotUserNameSection" class="login-form-body" runat="server" visible="false">
            <div class="container" id="sec2">
                <div class="login-form-body-inner" id="sec2inner">
                    <div class="login-form-body-inner-form" id="logininnerform" runat="server">
                        <div class="login-form-body-inner-title" id="forgotusername">
                            <h3>Forgot Username</h3>
                        </div>
                        Please enter the booking confirmation email address
                       <div class="col-md-12 login-form-margin" id="email">
                           Email<asp:TextBox runat="server" ID="emailBox" class="form-control"></asp:TextBox>
                           <br />
                           <asp:Label runat="server" ID="EmailLabel"></asp:Label>
                           <asp:Button ID="Button1" CssClass="btn log_btn" runat="server" Text="Submit" OnClick="ForgotUserNameSubmit_Click" />
                           <asp:Button ID="Button2" CssClass="btn log_btn" runat="server" Text="Back" OnClick="BtnBack_Click" />
                           <asp:Label runat="server" ID="forgotUserNameLableMsg"></asp:Label>
                       </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="ForgotPasswordSection" class="login-form-body" runat="server" visible="false">
            <div class="container" id="sec3">
                <div class="login-form-body-inner" id="sec3inner">
                    <div class="login-form-body-inner-form" id="changepwdinner" runat="server">

                        <div class="login-form-body-inner-title" id="changepassword">
                            <h3>Password Reset</h3>
                        </div>
                        <p>Please insert a valid username and you will be notified to your current email address</p>
                        <div class="col-md-12 login-form-margin" id="email1">
                            Username
                           <asp:TextBox runat="server" ID="usernameText" class="form-control"></asp:TextBox>
                            <br />
                            <asp:Button ID="Button3" runat="server" CssClass="btn log_btn" Text="Submit" OnClick="FotgotPasswordSubmit_Click" />
                            <asp:Button ID="Button4" runat="server" CssClass="btn log_btn" Text="Back" OnClick="BtnBack_Click" />
                            <asp:Label runat="server" ID="changePwdLabel"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
    </div>

  

  <!--Contact Us Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><%=SCContactUsHeader%></h4>
        </div>
        <div class="modal-body">
          <p><%=SCContactUsMessage%></p>
        </div>        
      </div>
      
    </div>
  </div>
  
</div>