﻿using System;
using System.Xml.Serialization;

namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Model
{
    [Serializable]
    [XmlType(TypeName = "ChildAge")]
    public class JtChildAge
    {
        public string Age { get; set; }
    }
}
