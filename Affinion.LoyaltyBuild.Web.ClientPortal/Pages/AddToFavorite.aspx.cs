﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Affinion.LoyaltyBuild.DataAccess;
using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Pages
{
    public partial class AddToFavorite : System.Web.UI.Page
    {

        public string listId { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            string newListName, Id;
            newListName = Request.QueryString["listname"];
            Id = Request.QueryString["id"];
            if (newListName != null)
            {
                int iListId = addNewList(newListName);
                listId = iListId.ToString();
                if (!string.IsNullOrWhiteSpace(newListName) && iListId > 0)
                {
                    lblList.Text = "<span>" + newListName + " - </span>" + string.Format("<a href='/myfavouritelist?ListID={0}' target='_blank' title='Go To List'>Go to List</a>", (iListId - 1));
                    hidAddToFavoriteSuccess.Visible = true;
                }
                else
                {
                    hidAddToFavoriteSuccess.Visible = false;
                }
            }
            else if (Id != null)
            {
                string hotelId=Request.QueryString["hotelid"];
                if(!string.IsNullOrWhiteSpace(hotelId))
                {
                    int Lid;
                    int.TryParse(Id, out Lid);
                    saveHotelToList(Lid, hotelId.Substring(0, 38));
                }
            }
        }

        private void saveHotelToList(int id, string hotel)
        {
            uCommerceConnectionFactory.SaveHotelToList(id, hotel);
        }

        private int addNewList(string listName)
        {
            int CustomerId;
            int.TryParse(Request.QueryString["CustomerId"], out CustomerId);
            string HotelId=System.Net.WebUtility.HtmlDecode(Request.QueryString["HotelId"]);
            return uCommerceConnectionFactory.AddListandSaveHotel(CustomerId,  HotelId.Substring(0,38), listName);
        }
    }
}