﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SuperValuHome2Sublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal.SuperValuHome2Sublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="container">

    <sc:Placeholder Key="packagepreview" runat="server" />
    
    <div class="row footer-top midle-content-margin-top">
        <div class="col-md-6 col-sm-6">
            <div class="adv-blog">
                <sc:Placeholder Key="advblog" runat="server" />
            </div>
            <div class="resource">
                <sc:Placeholder Key="resourcesection" runat="server" />
            </div>
        </div>
        <div class="col-md-6 col-sm-6">
            <sc:Placeholder Key="destinationsection" runat="server" />
        </div>
    </div>

</div>
