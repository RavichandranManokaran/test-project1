﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Maps
{
    //class CampaignItemSubrateMap
    //{
    //}
    public class CampaignItemSubrateMap : SubclassMap<CampaignItemSubrate>
    {
        // Methods
        public CampaignItemSubrateMap()
        {
            base.Table("uCommerce_CampaignItemSubrate");
            base.KeyColumn("CampaignSubRateId");
            base.Map(x => x.CampaignSubRateId).ReadOnly();
            base.References<CampaignItem>((Expression<Func<CampaignItemSubrate, CampaignItem>>)(x => x.CampaignItem));
            base.References<SubRate>((Expression<Func<CampaignItemSubrate, SubRate>>)(x => x.SubRate));
            base.Map(x => x.Price).Not.Nullable();
            base.Map(x => x.IsPercentageAmount).Not.Nullable();
        }
    }
}

