﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.EntitiesV2;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using UCommerce.Presentation.Views.Marketing;
using UCommerce.Presentation.Web.Controls;
using UCommerce.Presentation.Web.Marketing;
using Affinion.LoyaltyBuild.Common.Instrumentation;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.ucommerce.Marketing.Targets
{
    public partial class ArrivalDayUi : ViewEnabledControl<IEditCampaignItemView>, ITargetUi
    {
       // List<DataTypeEnum> datatypeList;

        protected void Page_Load(object sender, EventArgs e)
        {
            ArrivalDayTarget arrivalDayTypeData = this.DataSource as ArrivalDayTarget;
            this.DeleteButton.OnClientClick = string.Format("return confirm('Delete');");
            string[] days = arrivalDayTypeData.ArrivalDay.Split('|');
            chklstArrivalDay.Items.Clear();
            foreach (DayOfWeek item in Enum.GetValues(typeof(DayOfWeek)))
            {
                ListItem items = new ListItem();
                items.Text = item.ToString();
                items.Value = ((int)item).ToString();
                items.Selected = days.Any(x => x.ToString() == ((int)item).ToString());
                chklstArrivalDay.Items.Add(items);
            }

            this.ReadOnlyMessage.Text = this.TargetToString(arrivalDayTypeData);
        }

        protected void SaveButton_Click(object sender, ImageClickEventArgs e)
        {
            ArrivalDayTarget dataSource = this.DataSource as ArrivalDayTarget;
            string arrivalDay = string.Empty;
            foreach (ListItem item in chklstArrivalDay.Items)
            {
                if (item.Selected)
                {
                    arrivalDay += item.Value + "|";
                }
            }
            if (arrivalDay.Length > 0)
            {
                arrivalDay = arrivalDay.Substring(0, arrivalDay.Length - 1);
            }
            dataSource.ArrivalDay = arrivalDay;
            dataSource.Save();
            this.ReadOnlyMessage.Text = this.TargetToString(dataSource);
            this.ToggleEditor();
        }

        private void ToggleEditor()
        {
            this.NonEditModePlaceHolder.Visible = !this.NonEditModePlaceHolder.Visible;
            this.EditButton.Visible = !this.EditButton.Visible;
            this.EditModePlaceHolder.Visible = !this.EditModePlaceHolder.Visible;
            this.SaveButton.Visible = !this.SaveButton.Visible;
        }

        private string TargetToString(ArrivalDayTarget ArrivalDayTarget)
        {
            string[] arrivalDays = ArrivalDayTarget.ArrivalDay.Split('|').Where(x => !string.IsNullOrEmpty(x)).ToArray();
            string.Join(",", arrivalDays.Select(x => (Enum.Parse(typeof(DayOfWeek), x).ToString())));
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendFormat("Arrival Day ");
            stringBuilder.AppendFormat("{0} ", string.Join(",", arrivalDays.Select(x => (Enum.Parse(typeof(DayOfWeek), x).ToString()))));
            return stringBuilder.ToString();
        }

        protected void DeleteButton_Click(object sender, ImageClickEventArgs e)
        {
            ArrivalDayTarget dataSource = this.DataSource as ArrivalDayTarget;
            if (dataSource != null)
            {
                dataSource.Delete();
            }
            this.Page.Response.Redirect(this.Page.Request.Url.ToString(), true);
        }

        protected void EditButton_Click(object sender, ImageClickEventArgs e)
        {
            this.ToggleEditor();
        }


        public bool EnabledForApply
        {
            get;
            set;
        }

        public bool EnabledForDisplay
        {
            get;
            set;
        }

        public object DataSource
        {
            get;
            set;

        }

        public object New()
        {
            var ArrivalDayTarget = new ArrivalDayTarget();
            try
            {
                ArrivalDayTarget.CampaignItem = View.CampaignItem;
                ArrivalDayTarget.EnabledForApply = true;
                ArrivalDayTarget.ArrivalDay = string.Empty;
                ArrivalDayTarget.Save();
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.MasterClassWebsite, ex, this);
                throw;
            }
            this.DataSource = ArrivalDayTarget;
            return ArrivalDayTarget;
        }
    }
}