﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SimpleImageSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.Common.Layouts.SubLayouts.Common.SimpleImageSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<p id="PlaceholderText" runat="server">Set the datasource: Required Fields: DesktopImage</p>
<sc:Link Field="Link" ID="Link" runat="server">
    <sc:Image Field="DesktopImage" CssClass="img-responsive" ID="Image" runat="server" />
</sc:Link>

