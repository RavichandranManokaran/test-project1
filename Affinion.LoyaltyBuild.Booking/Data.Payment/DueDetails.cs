﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/
namespace Affinion.LoyaltyBuild.Api.Booking.Data.Payment
{
    /// <summary>
    /// Entity containing Due Details
    /// </summary>
    public class DueDetails
    {
        /// <summary>
        /// OrderlineDueID
        /// </summary>
        public int? OrderLineDueId { get; set; }
        
        /// <summary>
        /// OrderlineId
        /// </summary>        
        public int OrderLineId { get; set; }
        
        ///// <summary>
        ///// SubrateItemCode
        ///// </summary>       
        //public int SubrateItemCode { get; set; }
        
        /// <summary>
        /// DuetypeId
        /// </summary>      
        public Guid DueTypeId { get; set; }
        
        /// <summary>
        /// Currency Type Id
        /// </summary>       
        public int CurrencyTypeId { get; set; }

        /// <summary>
        /// Amount
        /// </summary>    
        public decimal Amount { get; set; }
       
        /// <summary>
        /// Processing fee
        /// </summary>       
        public decimal CommissionAmount { get; set; }

        ///// <summary>
        ///// Transferring fee
        ///// </summary>        
        //public decimal TransferAmount { get; set; }
        
        ///// <summary>
        ///// Cancellation fee
        ///// </summary>
        //public decimal CancellationAmount { get; set; }

        ///// <summary>
        ///// Client Fee
        ///// </summary>      
        //public decimal ClientAmount { get; set; }

        /// <summary>
        /// Provider Fee
        /// </summary>
        public decimal ProviderAmount { get; set; }

        ///// <summary>
        ///// Collection fee by Provider
        ///// </summary>
        //public decimal CollectionByProviderAmount { get; set; }

        /// <summary>
        /// Notes
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// Payment ID
        /// </summary>       
        public int OrderLinePaymentID { get; set; }

        /// <summary>
        /// Isdeleted flag
        /// </summary>
        public int IsDeleted { get; set; }
        
        /// <summary>
        /// Due Created By
        /// </summary>
        public string CreatedBy { get; set; }

        /// <summary>
        /// Due Created Date
        /// </summary>
        public DateTime CreatedDate { get; set; }
        
        /// <summary>
        /// Due Updated By
        /// </summary>
        public string UpdatedBy { get; set; }

        /// <summary>
        /// Due Updated Date
        /// </summary>
        public DateTime UpdatedDate { get; set; }
        /// <summary>
        /// Payment method id
        /// </summary>
        public int PaymentMethodId { get; set; } 

        /// <summary>
        /// Processing fee
        /// </summary>
        public decimal ProcessingFeeAmount { get; set; }
    }
}
