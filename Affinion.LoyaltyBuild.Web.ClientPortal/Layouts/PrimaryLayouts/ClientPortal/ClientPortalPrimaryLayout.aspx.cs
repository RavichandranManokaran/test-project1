///<summary>
/// � 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           ClientPortalPrimaryLayout.aspx.cs
/// Sub-system/Module:    Affinion.LoyaltyBuild.Web.ClientPortal
/// Description:           Master layout of the ClientPortal
/// </summary>
namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.PrimaryLayouts.ClientPortal
{
    #region Using Namespaces
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Sitecore.Data.Fields;
    using Sitecore.Data.Items;
    using System.Collections;
    using System.Collections.Generic;
    using System.Web.UI.HtmlControls;
    using Affinion.LoyaltyBuild.Common;
    #endregion

    public partial class ClientPortalPrimaryLayout : BasePrimaryLayout
    {
        
        private Hashtable dataHashTable = new Hashtable();
        private Dictionary<string, dynamic> dictionary = new Dictionary<string, dynamic>();

        //Gets and sets the theme for primary layout
        public string selectedTheme { get; set; }

        
        /// <summary>
        /// Gets the main form
        /// </summary>
        public override HtmlForm MainForm
        {
            get
            {
                return this.mainform;
            }
        }

        public string Favicon
        {
            get
            {
                return Sitecore.Context.Site.Properties["favicon"];
            }
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            //Set portal type
            SiteSettings.PortalType = Constants.PortalType.Online;

            //load the selected theme
            selectedTheme = ThemeSelector.LoadSelectedTheme();
            Item currentItem = Sitecore.Context.Item;
            pageTitle.InnerHtml = SitecoreFieldsHelper.GetValue(currentItem, "BrowserTitle"); ;
            
        }

        public dynamic GetPageData(string key)
        {
            dynamic value = null;
            if (dictionary.ContainsKey(key))
            {
                dictionary.TryGetValue(key, value);
            }

            return value;
        }

        public bool SetPageData(string key, dynamic value)
        {
            dictionary.Add(key, value);
            return true;
        }

    }
}
