﻿CREATE TABLE [dbo].[LB_CommunicationHistory]
(
	[HistoryId] INT NOT NULL PRIMARY KEY IDENTITY, 
    [CommunicationTypeId] INT NOT NULL, 
	[OrderLineId] INT NOT NULL, 
    [CreatedDate] DATETIME NOT NULL DEFAULT GETDATE(),
	[From] VARCHAR(100),
	[To] VARCHAR(100),
	[EmailSubject] NVARCHAR(250),
	[EmailText] NVARCHAR(MAX),
	[EmailHtml] NVARCHAR(MAX)
    CONSTRAINT [FK_LB_CommunicationHistory_CommunicationTypes] FOREIGN KEY ([CommunicationTypeId]) REFERENCES [LB_CommunicationTypes]([CommunicationTypeId]),
	[status] BIT NOT NULL, 
    CONSTRAINT [FK_LB_CommunicationHistory_OrderLine] FOREIGN KEY ([OrderLineId]) REFERENCES [uCommerce_OrderLine]([OrderLineId])
)
