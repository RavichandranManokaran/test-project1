﻿CREATE TABLE [dbo].[LB_BB_MarginErrorDetail](
	[IssueID] [int] IDENTITY(1,1) NOT NULL,
	[ListID] [int] NULL,
	[ProviderID] [varchar](50) NULL,
	[DateAdded] [datetime] NULL,
	[DateResolved] [datetime] NULL
) ON [PRIMARY]

GO