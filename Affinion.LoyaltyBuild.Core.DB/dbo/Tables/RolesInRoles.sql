﻿CREATE TABLE [dbo].[RolesInRoles] (
    [Id]              UNIQUEIDENTIFIER CONSTRAINT [DF_RolesInRoles_Id] DEFAULT (newid()) NOT NULL,
    [MemberRoleName]  NVARCHAR (256)   NOT NULL,
    [TargetRoleName]  NVARCHAR (256)   NOT NULL,
    [ApplicationName] NVARCHAR (256)   NOT NULL,
    [Created]         DATETIME         NOT NULL,
    [DAC_Index]       INT              NULL
);


GO
CREATE CLUSTERED INDEX [DAC_Clustered]
    ON [dbo].[RolesInRoles]([DAC_Index] ASC);


GO
CREATE NONCLUSTERED INDEX [ndxMembers]
    ON [dbo].[RolesInRoles]([MemberRoleName] ASC);


GO
CREATE NONCLUSTERED INDEX [ndxTarget]
    ON [dbo].[RolesInRoles]([TargetRoleName] ASC);

