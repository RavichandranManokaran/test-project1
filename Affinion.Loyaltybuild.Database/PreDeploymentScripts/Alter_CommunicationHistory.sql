﻿alter table ucommerce_communicationhistory add [status] bit

  insert into uCommerce_CommunicationTypes (Name) values ('PartialConfirmationEmailRemainder')
  insert into uCommerce_CommunicationTypes (Name) values ('CustomerConfirmationOnline')
  insert into uCommerce_CommunicationTypes (Name) values ('ProviderConfirmationOnline')
  insert into uCommerce_CommunicationTypes (Name) values ('PartialCustomerConfirmationOnline')
