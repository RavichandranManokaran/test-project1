﻿CREATE TABLE [dbo].[LB_Savedforlater] (
    [SavedlaterId] INT              IDENTITY (1, 1) NOT NULL,
    [customerID]   INT              NOT NULL,
    [ProviderID]   UNIQUEIDENTIFIER NOT NULL,
    [CreatedBy]    NCHAR (10)       NULL,
    [Updatedby]    NCHAR (10)       NULL,
    [Createddate]  DATETIME         NULL,
    [updatedate]   DATETIME         NULL,
    CONSTRAINT [PK_uCommerce_Savedforlater] PRIMARY KEY CLUSTERED ([SavedlaterId] ASC)
);

