﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientPortalManageBookingPrint.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal.ClientPortalManageBookingPrint" %>
<div id="divPrint" runat="server">
    <style type="text/css">
    .bookprint{
				font-family:"Segoe UI";
				color:#0371b2;
				font-size:14px;
				line-height:1.42857;
				font-weight:lighter;
			}
			.row{
				margin-left:0px;
				margin-right:15px;
				margin-bottom:0px;
			}
			.col-md-12{
				width:96%;
				float:left;
				position:relative;
				padding-left:15px;
				padding-right:15px;
			}
			.breadcrumb-top{
				margin-top:15px;
			}
			h1{
				font-size:28px;
				margin-bottom:8.5px;
				font-family:inherit;
				font-weight:500;
				line-height:1.1;
				color:inherit;
			}
			h1 span{
				font-size:28px;
				color:inherit;
			}
			.no-margin{
				margin:0px;
			}
			.accent54-f{
				color:#0FAD00;
			}
			.bookng{
				margin-top:5%;
				margin-bottom:5%;
			}
			.alert-info{
				background-color:rgba(3,129,203,0.05);
				border-color:rgba(3,129,203,0.2);
				color:rgba(3,129,203,0.9);
			}
			.content_bg_inner_box{
				padding:12px;
				display:inline-block;
				border-style:solid;
				border-width:1px;
				width:100%;
			}
			.col-md-3{
				width:20%;
				float:left;
				position:relative;
				padding-left:15px;
				padding-right:15px;
                height:268px;
			}
			.bordr-rght{
				border-right:1px dotted rgba(3, 129, 203, 0.2);
			}
			.margin-common-top{
				margin-top:20px;
			}
			.accent33-f{
				color:#38b3fc;
			}
			P{
				margin:0 0 10px;
			}
			a{
				font-size:12px;
			}
			a:link{
				color:#0088cc;
				text-decoration:none;
			}
			.hotl_img{
				height:268px;
				max-width:100%;
			}
			img{
				vertical-align:middle;
				border:0;
			}
			.date_bg{
				float:left;
			}
			.chk_in{
				float:left;
				width:70px;
				margin-left:18px;
			}
			.date_disp{
				font-size:12px;
			}
			.dte_no{
				margin-top:4%;
				margin-bottom:2px;
			}
			.col-xs-offset-3{
				margin-left:25%;
			}
			.btn-primary{
				background:#0d7ec1;
				color:#ffffff;
				border-color:#2e6da4;
			}
			.btn-sm{
				padding:5px 10px;
				font-size:12px;
				line-height:1.5;
				border-radius:3px;
			}
			.btn{
				display:inline-block;
				margin-bottom:0;
				font-weight:normal;
				text-align:center;
				vertical-align:middle;
				touch-action:manipulation;
				cursor:pointer;
				white-space:nowrap;
				border:1px solid transparent;
			}
			.box_size{
				padding-bottom:0px;
			}
			.price_bg{
				margin-left:25%;
				
			}

    </style>
    <div class="bookprint">
                <div class="row">
                <div class="col-md-12">
                    <h1 class="breadcrumb-top">
                        <asp:Literal ID="litBookingHeadingPretext" runat="server" />
                        <asp:Label ID="HotelPageName" runat="server" style="color:rgb(3, 113, 178); font-size:28px"/></h1>
                    <span class="no-margin accent54-f">
                        <asp:Literal ID="litBookingReferenceText" runat="server" />
                        <strong>
                            <asp:Literal ID="litBookingReferenceNumber" runat="server" />
                        </strong>
                    </span>
                </div>
            </div>
            
                <div class="row">
                <div class="col-md-12">
                    <div class="content_bg_inner_box alert-info bookng">
                        <div class="col-md-3 col-xs-12 bordr-rght">
                            <asp:Literal ID="litAccomodationAddress" runat="server" />
                            
                            <span class="margin-common-top" style="float: left; width: 100%;">
                                <asp:Literal ID="litPhoneText" runat="server" />
                                <asp:Literal ID="litPhoneNumber" runat="server" />
                                <p class="accent33-f">
                                    <span class="glyphicon glyphicon-envelope" style="color: #38b3fc;"></span>
                                    <asp:HyperLink ID="hypEmailProperty" runat="server">
                                        <i class="bicon-email"></i>
                                        <strong>
                                            <asp:Literal ID="litEmailText" runat="server" />
                                        </strong>
                                    </asp:HyperLink>
                                    <br>
                                </p>
                            </span>
                        </div>

                        <div class="col-md-3 col-xs-12 bordr-rght">
                            <asp:Image ID="imgHotelImage" CssClass="hotl_img" runat="server" />
                        </div>
                        <div class="col-md-3 col-xs-12 bordr-rght box_size">
                            <div class="date_bg">
                                <div class="chk_in date_brdr">
                                    <asp:Literal ID="litCheckinText" runat="server" />
                                    <span class="date_disp">
                                        <h1 class="dte_no col-xs-offset-3"><strong>
                                            <asp:Literal ID="litCheckinDay" runat="server" /></strong></h1>
                                        <asp:Literal ID="litCheckinMonthYear" runat="server" />
                                        <br />
                                        <asp:Literal ID="litCheckinDayOfWeek" runat="server" />
                                        <br>
                                        <span class="glyphicon glyphicon-time"></span>
                                        <asp:Literal ID="litFromText" runat="server" />
                                        <asp:Literal ID="litCheckinTime" runat="server" />
                                    </span>
                                </div>

                                <div class="chk_in">
                                    <asp:Literal ID="litCheckOutText" runat="server" />
                                    <span class="date_disp">
                                        <h1 class="dte_no col-xs-offset-3">
                                            <strong>
                                                <asp:Literal ID="litCheckoutDay" runat="server" />
                                            </strong>
                                        </h1>
                                        <asp:Literal ID="litCheckoutMonthYear" runat="server" />
                                        <br />
                                        <asp:Literal ID="litCheckoutDayOfWeek" runat="server" />
                                        <br>
                                        <span class="glyphicon glyphicon-time"></span>
                                        <asp:Literal ID="litUntilText" runat="server" />
                                        <asp:Literal ID="litCheckoutTime" runat="server" />
                                    </span>
                                </div>

                                <span class="btn btn-primary btn-bg btn-sm  col-xs-offset-3" style="margin-top: 11%;">
                                    <span class="nightsdetails">
                                        <asp:Literal ID="litNumberOfNightsCount" runat="server" />
                                        <asp:Literal ID="litNightText" runat="server" />
                                    </span>
                                    <span class="roomDetails">
                                        <asp:Literal ID="litRoomCount" runat="server" />
                                        <asp:Literal ID="litRoomText" runat="server" />
                                    </span>
                                </span>
                            </div>

                        </div>
                        <div class="col-md-3 col-xs-12">
                            <span class="price_bg">
                                <asp:Literal ID="litPriceText" runat="server" />
                            </span>
                            <br>
                            <h1><strong>
                                <asp:Literal ID="litCurrencySymbol" runat="server" />
                                <asp:Literal ID="litPrice" runat="server" /></strong></h1>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>