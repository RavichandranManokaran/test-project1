﻿CREATE PROCEDURE [dbo].[LB_GetCustomerBookingReferenceDetails]
	@CustomerId as int
AS
BEGIN
	SELECT BookingReferenceId, LastSequence	FROM  LB_CustomerAttribute
	WHERE (CustomerId = @CustomerId)
END
