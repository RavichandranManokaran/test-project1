﻿using System;
using System.Xml.Serialization;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Model;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Message.Interfaces;

namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Message
{
    [Serializable]
    public class JtPreBookResponse : JtResponse, IJtResponse
    { 
        public string PreBookingToken { get; set; }
        public double TotalPrice { get; set; }
        public double TotalCommission { get; set; }
        public double VATOnCommission { get; set; }
        
        public JtCancellation[] Cancellations { get; set; }
    }
}
