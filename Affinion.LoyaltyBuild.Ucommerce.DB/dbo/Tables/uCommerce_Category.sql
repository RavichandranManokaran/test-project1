﻿CREATE TABLE [dbo].[uCommerce_Category] (
    [CategoryId]       INT              IDENTITY (1, 1) NOT NULL,
    [Name]             NVARCHAR (60)    NOT NULL,
    [ImageMediaId]     NVARCHAR (100)   NULL,
    [DisplayOnSite]    BIT              CONSTRAINT [uCommerce_DF_Category_DisplayOnSite] DEFAULT ((1)) NOT NULL,
    [CreatedOn]        DATETIME         CONSTRAINT [uCommerce_DF_Category_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [ParentCategoryId] INT              NULL,
    [ProductCatalogId] INT              NOT NULL,
    [ModifiedOn]       DATETIME         NOT NULL,
    [ModifiedBy]       NVARCHAR (50)    NULL,
    [Deleted]          BIT              CONSTRAINT [uCommerce_DF_Category_Deleted] DEFAULT ((0)) NOT NULL,
    [SortOrder]        INT              CONSTRAINT [DF_uCommerce_Category_SortOrder] DEFAULT ((0)) NOT NULL,
    [CreatedBy]        NVARCHAR (255)   NULL,
    [DefinitionId]     INT              CONSTRAINT [DF_uCommerce_Category_DefinitionId] DEFAULT ((1)) NOT NULL,
    [Guid]             UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    CONSTRAINT [uCommerce_PK_Category] PRIMARY KEY CLUSTERED ([CategoryId] ASC),
    CONSTRAINT [FK_uCommerce_Category_ParentCategory] FOREIGN KEY ([ParentCategoryId]) REFERENCES [dbo].[uCommerce_Category] ([CategoryId]),
    CONSTRAINT [FK_uCommerce_Category_uCommerce_Definition] FOREIGN KEY ([DefinitionId]) REFERENCES [dbo].[uCommerce_Definition] ([DefinitionId]),
    CONSTRAINT [uCommerce_FK_Category_ProductCatalog] FOREIGN KEY ([ProductCatalogId]) REFERENCES [dbo].[uCommerce_ProductCatalog] ([ProductCatalogId])
);


GO
CREATE NONCLUSTERED INDEX [IX_Category]
    ON [dbo].[uCommerce_Category]([Name] ASC, [ProductCatalogId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_uCommerce_Category_ProductCatalogId]
    ON [dbo].[uCommerce_Category]([ProductCatalogId] ASC, [Deleted] ASC)
    INCLUDE([CategoryId], [Name], [ImageMediaId], [DisplayOnSite], [CreatedOn], [ParentCategoryId], [ModifiedOn], [ModifiedBy], [SortOrder], [CreatedBy], [DefinitionId], [Guid]);


GO
CREATE NONCLUSTERED INDEX [IX_uCommerce_Category_ParentCategoryId]
    ON [dbo].[uCommerce_Category]([ParentCategoryId] ASC, [Deleted] ASC);

