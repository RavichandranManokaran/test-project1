﻿<%@ OutputCache Location="None" VaryByParam="none" %><%@ Page Title="" Language="C#" MasterPageFile="~/sitecore modules/Shell/Affinion/ExternalDataHandling/Main.Master" AutoEventWireup="true" CodeBehind="AddOfferData.aspx.cs" Inherits="Affinion.LoyaltyBuild.AdminPortal.sitecore_modules.Shell.Affinion.ExternalDataHandling.AddOfferData" %>

<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="container-outer width-full overflow-hidden-att app-bg-cl">
        <div class="container-outer-inner width-full">
            <div class="container">
                <div class="col-md-6 sup-login-layout-outer">
                    <div class="sup-login-layout">
                        <div class="sup-login-layout-inner">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="login-header">
                                        <img src="/Resources/images/LBLogo-home.png" alt="logo" title="" class="img-responsive">
                                    </div>
                                </div>
                            </div>
                            <%-- <form>--%>
                            <div class="col-md-12">
                                <div class="input-set">
                                    <div class="col-md-12">
                                        <h2 class="text-center">Offer specific information</h2>
                                    </div>
                                    <div class="col-md-12">
                                        <p>An error occured:</p>
                                        <textarea runat="server" id="ErrorMessage" style="margin: 0px;height: 150px;width: 400px;padding: 10px;color: #dd0011;"></textarea>
                                    </div>
                                </div>
                            </div>
                            <%-- </form>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
