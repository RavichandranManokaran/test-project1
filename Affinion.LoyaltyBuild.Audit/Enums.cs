﻿namespace Affinion.LoyaltyBuild.Audit
{
    public enum AuditNames
    {
        Login = 0,
        Register,
        ChangesInAvailability,
        Promotion,
        Undefined
    }

    public enum AuditOperations
    {
        Add = 0,
        Edit,
        Read,
        Delete,
        Activate,
        Deactivate,
        Lock,
        Unlock,
        Undefined
    }

    public enum AuditObjectType
    {
        Sitecore = 0,
        Ucommerce,
        CustomTableInUcommerce,
        CustomTableInCore,
        Undefined
    }

    public enum AuditSection
    {
        ClientPortal = 0,
        AdminPortalSitecore,
        AdminPortalUcommerce,
        OfflinePortal,
        SupplierPortal,
        Undefined
    }
}
