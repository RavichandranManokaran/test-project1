﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           StoreHelper.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common.Utilities
/// Description:           Store helper
/// </summary>


namespace Affinion.LoyaltyBuild.Common.Utilities
{
    #region Using directives
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Search;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Availability;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
    using Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using UCommerce.Api;
    using UCommerce.EntitiesV2; 
    #endregion

    public static class StoreHelper
    {
        /// <summary>
        /// Get supplier's products if it is associated with the current client
        /// </summary>
        /// <param name="supplier">Sitecore supplier item</param>
        /// <returns>Returns supplier products</returns>
        public static SupplierData GetProducts(Item supplier)
        {
            if (supplier == null)
            {
                return null;
            }

            Item client = SitecoreFieldsHelper.GetLookupFieldTargetItem(ItemHelper.RootItem, "ClientDetails");

            return GetProducts(supplier, client);
        }

        /// <summary>
        /// Get supplier's products if it is associated with the given client
        /// </summary>
        /// <param name="supplier">Sitecore supplier item</param>
        /// <param name="client">The client to check the supplier relationship</param>
        /// <returns>Returns supplier products</returns>
        public static SupplierData GetProducts(Item supplier, Item client)
        {
            SearchKey searchTerm = new SearchKey();
            searchTerm.CheckinDate = DateTime.Now;
            searchTerm.CheckoutDate = DateTime.Now.AddDays(42);

            return GetProducts(supplier, client, searchTerm);
        }

        /// <summary>
        /// Get supplier's products if it is associated with the given client
        /// </summary>
        /// <param name="supplier">Sitecore supplier item</param>
        /// <param name="client">The client to check the supplier relationship</param>
        /// <param name="searchTerm">The search term to get offers</param>
        /// <returns>Returns supplier products</returns>
        public static SupplierData GetProducts(Item supplier, Item client, SearchKey searchTerm)
        {
            try
            {
                SupplierData products = new SupplierData();
                string clientName = SitecoreFieldsHelper.GetValue(client, "Name", client.Name);
                string priceGroup = SitecoreFieldsHelper.GetLookupFieldTargetItemValue(client, "PriceGroup", "Title");

                int categoryId = SitecoreFieldsHelper.GetInteger(supplier, "UCommerceCategoryID", 0);
                Category hotel = null;
                decimal lowestPrice = 0;
                string lowestClientPrice = string.Empty;
                string lowestPriceOccupancyId = string.Empty;
                int lowestPriceCurrencyId = 0;

                try
                {
                    hotel = CatalogLibrary.GetCategory(categoryId);
                }
                catch (ArgumentException ex)
                {
                    return null;
                }

                List<Product> roomProducts = hotel.Products.Where(p => MatchProductToDefinition(p, "hotelroom")).ToList();

                foreach (Product product in roomProducts)
                {
                    OfferDataItem offerItem = new OfferDataItem(hotel, product, priceGroup, supplier);
                    Collection<OfferDataItem> offers = GetCampaignOffers(offerItem, hotel, searchTerm);

                    foreach (OfferDataItem offer in offers)
                    {
                        if (lowestPrice == 0 || (offer.Price > 0 && offer.Price < lowestPrice))
                        {
                            lowestPrice = offer.Price;
                            lowestClientPrice = offer.PriceWithCurrency;
                            lowestPriceOccupancyId = offer.OccupancyTypeId;
                        }

                        //if (offer.Price > 0)
                        //{
                        products.Offers.Add(offer);
                        //}
                    } 
                }

                products.ClientId = client.ID.ToString();
                products.ClientName = clientName;
                products.LowestPrice = lowestPrice;
                products.LowestPriceWithCurrency = lowestClientPrice;
                products.LowestPriceCurrencyId = lowestPriceCurrencyId;
                products.LowestPriceOccupancyTypeId = lowestPriceOccupancyId;

                return products;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, typeof(SupplierData));
                return null;
            }
        }

        /// <summary>
        /// Get supplier's addon products
        /// </summary>
        /// <param name="supplier">Sitecore supplier item</param>
        /// <returns>Returns supplier products</returns>
        public static Collection<AddonProduct> GetAddons(Item supplier)
        {
            if (supplier == null)
            {
                return null;
            }

            Item client = SitecoreFieldsHelper.GetLookupFieldTargetItem(ItemHelper.RootItem, "ClientDetails");

            return GetAddons(supplier, client);
        }

        /// <summary>
        /// Get supplier's products if it is associated with the given client
        /// </summary>
        /// <param name="supplier">Sitecore supplier item</param>
        /// <param name="client">The client to check the supplier relationship</param>
        /// <returns>Returns supplier products</returns>
        public static Collection<AddonProduct> GetAddons(Item supplier, Item client)
        {
            try
            {
                Collection<AddonProduct> addonProducts = new Collection<AddonProduct>();
                string clientName = SitecoreFieldsHelper.GetValue(client, "Name", client.Name);
                int categoryId = SitecoreFieldsHelper.GetInteger(supplier, "UCommerceCategoryID", 0);
                Category hotel = null;

                try
                {
                    hotel = CatalogLibrary.GetCategory(categoryId);
                }
                catch (ArgumentException ex)
                {
                    return null;
                }

                // List<Product> roomProducts = ProductLibrary.GetAddonProudct(categoryId);
                List<Product> roomProducts = hotel.Products.Where(p => p.ProductDefinition.Name == "Add On").ToList();

                foreach (Product product in roomProducts)
                {
                    AddonProduct addonItem = new AddonProduct();
                    addonItem.Name = product.Name;
                    addonItem.Sku = product.Sku;
                    SearchCriteria criteria = new SearchCriteria();
                    LBPriceCalculation price = LoyaltyBuildCatalogLibrary.CalculatePrice(product, criteria, hotel.ProductCatalog);
                    addonItem.Price = price.YourPrice.Amount.Value;
                    addonProducts.Add(addonItem);
                }

                return addonProducts;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, typeof(SupplierData));
                return null;
            }
        }

        /// <summary>
        /// Match the product to the given definition
        /// </summary>
        /// <param name="product">The product</param>
        /// <param name="productDefinitionName">The string name of product definition</param>
        /// <returns>Returns true if matched</returns>
        private static bool MatchProductToDefinition(Product product, string productDefinitionName)
        {
            bool include = product.DisplayOnSite;
            include = include && product.AllowOrdering;
            include = include && product.ProductDefinition.Name.Replace(" ", "").Equals(productDefinitionName, StringComparison.InvariantCultureIgnoreCase);
            return include;
        }


        private static Collection<OfferDataItem> GetCampaignOffers(OfferDataItem baseProduct, Category supplier, SearchKey searchTerm)
        {
            Collection<OfferDataItem> offers = new Collection<OfferDataItem>();
            CampaignSearch campaignSearch = new CampaignSearch();
            List<Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model.SupplierInviteDetail> campaignDetails = campaignSearch.GetCampaignDetailBySupplier(supplier.Guid.ToString(), searchTerm.CheckinDate, searchTerm.CheckoutDate);
            
            foreach (var campaignDetail in campaignDetails)
            {
                OfferDetail offerDetail = campaignSearch.GetCampaignById(supplier.Guid.ToString(), campaignDetail.CampaignItemId, campaignDetail.SupplierInviteId);
                OfferDataItem adapterItem = (OfferDataItem)baseProduct.Clone();
                adapterItem.OfferName = offerDetail.OfferName;
                adapterItem.Price = offerDetail.HotelRate;
                adapterItem.DiscountedPrice = offerDetail.HotelDiscountedRate;

                offers.Add(adapterItem);
            }

            offers.Insert(0, baseProduct);

            return offers;
        }
    }
}
