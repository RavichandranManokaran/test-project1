﻿CREATE PROCEDURE [dbo].[LB_GetPaymentsWithoutRefunds]
	@OrderLineId INT,
	@PaymentTypeId INT,
	@CurrencyTypeId INT		
AS
BEGIN

Declare @ClientID varchar(max)
select @ClientID = Value from uCommerce_OrderProperty
where [key]='_ClientId' and OrderLineId=@OrderLineId

	--Payment History Payments
	SELECT	OLP.ReferenceId [ReferenceId],
			SUM(OLP.TotalAmount) [Amount],
			SUM(OLP.ProviderAmount) [ProviderAmount],
			SUM(OLP.CommissionAmount) [CommissionAmount],
			SUM(OLP.ProcessingFeeAmount) [ProcessingFee],
			MIN(OLP.CreatedDate) [Createddate]
	FROM LB_OrderLinePayment OLP
	WHERE
		OLP.PaymentMethodID IN (SELECT B.PaymentMethodID FROM [dbo].[LB_FnClientPaymentMethods](@ClientID) A
			INNER JOIN [dbo].[LB_FnClientPaymentMethods](@ClientID)  B ON A.[Description] = B.[Description]
			WHERE a.[PaymentMethodId] = @PaymentTypeId) AND
		OLP.CurrencyTypeId = @CurrencyTypeId AND
		OLP.OrderLineID IN (SELECT OrderLineID FROM [dbo].[LB_FnRelatedOrderLines](@OrderLineId))
	GROUP BY  OLP.ReferenceId
END 
