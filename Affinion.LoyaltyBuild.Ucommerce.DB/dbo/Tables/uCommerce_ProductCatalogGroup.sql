﻿CREATE TABLE [dbo].[uCommerce_ProductCatalogGroup] (
    [ProductCatalogGroupId]         INT              IDENTITY (1, 1) NOT NULL,
    [Name]                          NVARCHAR (128)   NOT NULL,
    [Description]                   NTEXT            NULL,
    [EmailProfileId]                INT              NOT NULL,
    [CurrencyId]                    INT              NOT NULL,
    [DomainId]                      NVARCHAR (255)   NULL,
    [OrderNumberId]                 INT              NULL,
    [Deleted]                       BIT              CONSTRAINT [uCommerce_DF_ProductCatalogGroup_Deleted] DEFAULT ((0)) NOT NULL,
    [CreateCustomersAsMembers]      BIT              CONSTRAINT [uCommerce_DF_ProductCatalogGroup_CreateCustomersAsMembers] DEFAULT ((0)) NOT NULL,
    [MemberGroupId]                 NVARCHAR (255)   NULL,
    [MemberTypeId]                  NVARCHAR (255)   NULL,
    [ProductReviewsRequireApproval] BIT              CONSTRAINT [DF_uCommerce_ProductCatalogGroup_ProductReviewsRequireApproval] DEFAULT ((0)) NOT NULL,
    [Guid]                          UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [CreatedOn]                     DATETIME         CONSTRAINT [DF_uCommerce_ProductCatalogGroup_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]                     NVARCHAR (50)    DEFAULT ('') NOT NULL,
    [ModifiedOn]                    DATETIME         CONSTRAINT [DF_uCommerce_ProductCatalogGroup_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]                    NVARCHAR (50)    DEFAULT ('') NOT NULL,
    CONSTRAINT [uCommerce_PK_CatalogGroup] PRIMARY KEY CLUSTERED ([ProductCatalogGroupId] ASC),
    CONSTRAINT [uCommerce_FK_ProductCatalogGroup_Currency] FOREIGN KEY ([CurrencyId]) REFERENCES [dbo].[uCommerce_Currency] ([CurrencyId]),
    CONSTRAINT [uCommerce_FK_ProductCatalogGroup_EmailProfile] FOREIGN KEY ([EmailProfileId]) REFERENCES [dbo].[uCommerce_EmailProfile] ([EmailProfileId]),
    CONSTRAINT [uCommerce_FK_ProductCatalogGroup_OrderNumbers] FOREIGN KEY ([OrderNumberId]) REFERENCES [dbo].[uCommerce_OrderNumberSerie] ([OrderNumberId])
);

