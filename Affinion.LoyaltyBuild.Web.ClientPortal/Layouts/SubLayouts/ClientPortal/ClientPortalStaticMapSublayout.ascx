﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientPortalStaticMapSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal.ClientPortalStaticMapSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Affinion.LoyaltyBuild.Search" %>
<%@ Import Namespace="Affinion.LoyaltyBuild.Search.Data" %>
<%@ Import Namespace="Sitecore.Data.Items" %>

<script type="text/javascript">
    jQuery(document).ready(function () {
        if (MapLocations === undefined || MapLocations == null || MapLocations == "undefined" || MapLocations.Locations == undefined || MapLocations.Locations.length < 1) {
            jQuery('#gsmap').hide();
        }
        else {
            var mark = "markers=";
            var markAppend = "";
            var location = "markers=";
            var locationAppend = MapLocations.Locations[0].Latitude + "," + MapLocations.Locations[0].Longitute;
            var url = window.location.pathname;
            var PageName = url.substring(url.lastIndexOf('/') + 1);

            jQuery.each(MapLocations.Locations, function (i, item) {
                markAppend += item.Latitude + "," + item.Longitute + "|";
            });

            mark += markAppend;
            location += locationAppend;
            jQuery('#staticmap').attr('src', jQuery('#staticmap').attr('src') + location);
        }
    });
</script>

<div class="col-xs-12 no-padding">
    <div class="map-sm-location margin-common-inner-box" title="Map view" id="gsmap">
        <a href="#" data-toggle="modal" data-target="#dialog-map">
            <img id="staticmap" src="http://maps.google.com/maps/api/staticmap?zoom=2&size=175x175&" alt="" class="img-responsive img-thumbnail img-enlarge" />
        </a>
    </div>
</div>
<% if(this.ShowModelSection) { %>
<!-- Modal -->
<div class="modal fade" id="dialog-map" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><span class="fa fa-map-marker"></span>Map</h4>
            </div>
            <div class="modal-body">
                <div id="map_canvas" class="large-gmap"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<% } %>

<script type="text/javascript">
    //Store reference to the google map object
    var map;

    function initializeMap(location) {
        //Set the required Geo Information

        // Define the coordinates as a Google Maps LatLng Object

        //if (MapLocations.Locations > 0) {

        var mapOptions = {
            center: new google.maps.LatLng(MapLocations.Locations[0].Latitude, MapLocations.Locations[0].Longitute),
            //center: coords,
            zoom: 6,
        };

        map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);


        jQuery.each(MapLocations.Locations, function (i, item) {
            //Map initialization variables         

            var marker;

            marker = new google.maps.Marker({
                position: new google.maps.LatLng(item.Latitude, item.Longitute),
                url: "/hotelinformation?id=" + item.SupplierId,
                map: map
            });
            google.maps.event.addListener(marker, 'click', function () {
                window.location.href = marker.url;
            });
        });
        //}

    }

    jQuery(document).ready(function () {

        jQuery('#dialog-map').on('shown.bs.modal', function () {

            if (navigator.geolocation) {
                // Call getCurrentPosition with success and failure callbacks
                //navigator.geolocation.getCurrentPosition(initializeMap);
                initializeMap();
            }
            else {
                alert("Sorry, your browser does not support geolocation services.");
            }

        });
    });
</script>
