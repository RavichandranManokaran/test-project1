﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class NightTargetMap : SubclassMap<NightTarget>
    {
        // Methods
        public NightTargetMap()
        {
            base.Table("LB_NightTarget");
            base.KeyColumn("NightTargetId");
            base.Map(x => x.NightTargetId).ReadOnly();
            base.Map(x => x.Night).Not.Nullable();
            base.Map(x => x.LastArrivalDay).Nullable();
        }

    }

}
