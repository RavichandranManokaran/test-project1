
/****** Object:  StoredProcedure [dbo].[LB_BB_performBedbankMappings]    Script Date: 7/19/2016 7:28:46 PM ******/

---- ====================================================
---- Description:	Tii SP to Create, Read, Update, Delete client to or from the client table.
---- Rev 0.2 05-JUN-2014 MC		Added new ClientID Filter (it uses the @pi_userID parameter)
---- Rev 0.1 08-APR-2011		Created new SP.
---- 
---- RAISERROR ('mesage text %a', severity#, state#, @pi_); 
----			%s/%u is optional;	
----			severity < 10 The error is returned to the caller if RAISERROR is run
----			severity > 11 transfers control to the associated CATCH block.
----			severity > 20 terminates the database connection.
----			state#   set to 1
----			@pi_ optional, must be used if %s/%u is used in the message text
----
---- ====================================================

CREATE PROCEDURE [dbo].[LB_BB_performBedbankMappings]
	--


	--Return parameters
	@po_returnstatus INT OUTPUT,
	@po_returnmsg VARCHAR(200) OUTPUT,
	@po_scopeid NUMERIC OUTPUT

AS

BEGIN

	--Assume transaction will fail, set the return variable to -1.
	SET @po_returnstatus = -1;
	SET @po_returnmsg = null;
	SET @po_scopeid = 0;
	
	BEGIN TRY
		
	
Delete from LB_BB_mappings

Update LB_BB_Location set isMatched = 0

Declare @country varchar(100), @RegionID int, @Region varchar(150), @ResortID int, @Resort varchar(150), @LocationID int, @maplocationid int, @isMatched bit
DECLARE db_cursor CURSOR FOR  
SELECT Country, RegionID,Region,ResortID,Resort,isMatched from LB_BB_Location
where Country in ('England','Scotland','Wales','Northern Ireland', 'Republic of Ireland','France','Norway','Sweden','Denmark','Finland','Spain','Italy')
and ismatched = 0

OPEN db_cursor   
FETCH NEXT FROM db_cursor INTO @Country,@RegionID,@Region,@ResortID,@Resort,@isMatched   

WHILE @@FETCH_STATUS = 0   
BEGIN   
      
       Select @MaplocationID = MaplocationID from syn_emosMaplocation
       where Maplocationname = @Region
       and Status = 1
       
       If @@ROWCOUNT = 1
       BEGIN
		INSERT INTO LB_BB_mappings Values ('Location',@RegionID,'Region','MapLocation',@MapLocationID,'MapLocationID')
 		
		Update LB_BB_Location set isMatched = 1
		where  RegionID = @RegionID
		and Country = @country
       END
       
       IF @@ROWCOUNT = 0
        BEGIN
         Select @LocationID = LocationID from syn_emosLocation
			where LocationName = @Region
       If @@ROWCOUNT = 1
        BEGIN
        INSERT INTO LB_BB_mappings
		 Select distinct 'Location',@RegionID,'Region','MapLocation',Ml.Maplocationid,'MapLocationID'  from syn_emosMaplocation ml
		 inner join syn_emosProvider sep on sep.maplocationID = ml.maplocationid
		 and ProviderTypeID in (1,2) and sep.isActive = 1 and ml.status = 1
		 inner join syn_emosLocation l on l.LocationID = sep.LocationID
		 and l.LocationID in (Select LocationID from syn_emosLocation
			where LocationName = @Region)
		
		Update LB_BB_Location set isMatched = 2
		where  RegionID = @RegionID
		and Country = @country
		END
       END
       
      

       FETCH NEXT FROM db_cursor INTO @Country,@RegionID,@Region,@ResortID,@Resort,@isMatched        
END   

CLOSE db_cursor   
DEALLOCATE db_cursor



    END TRY
	
	BEGIN CATCH
		SET @po_returnstatus = error_number();
		SET @po_returnmsg = error_message();
		RETURN;
	END CATCH
	
	SET @po_returnstatus = 0;
	SET @po_returnmsg = 'Completed successfully.';
	
END







GO

