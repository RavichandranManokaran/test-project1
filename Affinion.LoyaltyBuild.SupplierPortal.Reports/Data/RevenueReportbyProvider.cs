﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.SupplierPortal.Reports.Data
{
    /// <summary>
    /// Contains details for the items of revenue report item
    /// </summary>
    public class RevenueReportbyProvider : RevenueReport
    {
        /// <summary>
        /// Hotel ID no
        /// </summary>
        public int HotelId { get; set; }

        /// <summary>
        /// Hotel/Provider name
        /// </summary>
        public string HotelName { get; set; }
    }

    /// <summary>
    /// Collection to manage Revenue Report
    /// </summary>

    public class RevenueReportbyProviderCollection : IEnumerable<RevenueReportbyProvider>
    {
        //Private variables declaration
        private List<RevenueReportbyProvider> _items;

        //To display row count
        public int Count
        {
            get { return _items.Count; }
        }
        /// <summary>
        /// Constructor
        /// </summary>
        public RevenueReportbyProviderCollection()
        {
            _items = new List<RevenueReportbyProvider>();
        }

        /// <summary>
        /// Add method to add item
        /// </summary>
        /// <param name="item"></param>
        public void Add(RevenueReportbyProvider item)
        {
            _items.Add(item);
        }

        /// <summary>
        /// Override method IEnumenrable class
        /// </summary>
        /// <returns></returns>
        public IEnumerator<RevenueReportbyProvider> GetEnumerator()
        {
            foreach (var item in _items)
                yield return item;
        }

        /// <summary>
        /// Override method IEnumenrable class
        /// </summary>
        /// <returns></returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            foreach (var item in _items)
                yield return item;
        }

    }
}
