﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    public class FilterSupplier
    {
        public List<string> Country { get; set; }
        public List<string> Region { get; set; }
        public List<string> Location { get; set; }
        public List<string> HotelType { get; set; }
        public List<string> Theme { get; set; }
        public List<string> Group { get; set; }
        public List<string> Experience { get; set; }
        public List<string> Ratting { get; set; }
    }
}
