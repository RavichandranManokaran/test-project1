﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    public class BookingCounter
    {
        /// <summary>
        /// ClientName
        /// </summary>
        public string ClientName { get; set; }

        /// <summary>
        /// ClientId
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// Bookings
        /// </summary>
        /// 
        public int Bookings { get; set; }
    }

    public class BookingCounterReport
    {
        /// <summary>
        /// Today
        /// </summary>
        /// 
        public List<BookingCounter> Today { get; set; }
        /// <summary>
        /// Bookings
        /// </summary>
        /// 
        public List<BookingCounter> Yesterday { get; set; }
        /// <summary>
        /// Yesterday
        /// </summary>
        /// 
        public List<BookingCounter> Week { get; set; }
        /// <summary>
        /// Week
        /// </summary>
        /// 
        public List<BookingCounter> Month { get; set; }
        /// <summary>
        /// Month
        /// </summary>
        /// 
        public List<BookingCounter> Year { get; set; }
        /// <summary>
        /// Year
        /// </summary>
        /// 
    }
}
