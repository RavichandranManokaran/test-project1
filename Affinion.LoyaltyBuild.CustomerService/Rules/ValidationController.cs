﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sitecore.Data;
using Sitecore.Data.Items;
using RuleModel = Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Rules.Model;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;


namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Validators
{
    public class ValidationController : IValidatonController
    {
        public ValidationController()
        {

        }
        /// <summary>
        /// To Execute the Validations for the provided Input
        /// </summary>
        /// <param name="request">ValidationRequest</param>
        /// <returns>ValidationResponse</returns>
        public RuleModel.ValidationResponse Execute(RuleModel.ValidationRequest request)
        {
            return ExecuteRules(request,BuiltRuleForRequest(request));
        }

        private string GetSiteCoreIdOfApplication(string application)
        {
            string RuleListPath = @"fast:/sitecore/content/#admin-portal#/global/#_supporting-content#/#rules-apply#/*[@Title='{0}']";
            try
            {
                var fullRuleList = ExecuteAndReturnItems(string.Format(RuleListPath, application));
                return (fullRuleList == null || !fullRuleList.Any())
                        ? string.Empty
                        : fullRuleList.FirstOrDefault().ID.ToString();
            }
            catch (Exception ex)
            {
                //Need to Implement
            }

            return string.Empty;
        }

        //private string GetRuleCategoryId(RuleModel.RuleCategory Category)
        //{
        //    try
        //    {
        //        var enumType = Category.GetType();
        //        var siteCoreName = enumType.GetField(Category.ToString()).GetCustomAttributes(false).OfType<DescriptionAttribute>().FirstOrDefault().Description;

        //        string RuleListPath = @"fast:/sitecore/content/#admin-portal#/global/#_supporting-content#/#rule-categories#/*[@@templatename='RuleCategory']";

        //        var ruleCategoriesList = ExecuteAndReturnItems(RuleListPath);
        //        return (ruleCategoriesList == null || !ruleCategoriesList.Any())
        //                ? string.Empty
        //                : ruleCategoriesList.Any(f => f.Name.Equals(siteCoreName))
        //                    ? ruleCategoriesList.Where(f => f.Name.Equals(siteCoreName)).FirstOrDefault().ID.ToString()
        //                    : string.Empty;

        //    }
        //    catch (Exception ex)
        //    {
        //        //Need to Implement
        //    }
        //    return string.Empty;
        //}

        private Item[] ExecuteAndReturnItems(string query)
        {
            return Sitecore.Context.Database.SelectItems(query);
        }

        private List<Rules.Model.Validators> BuiltRuleForRequest(RuleModel.ValidationRequest validationRequest)
        {

            List<RuleModel.Validators> response = new List<RuleModel.Validators>();

            try
            {
                var applicationSiteCoreId = GetSiteCoreIdOfApplication(validationRequest.RequestingApplication.ToString());

                //To Add Application SiteCore Id if found, else Pass nothing to fast Query
                applicationSiteCoreId = (string.IsNullOrEmpty(applicationSiteCoreId))
                                        ? string.Empty
                                        : string.Format(" and @Rule apply='%{0}%'", applicationSiteCoreId);

                var ruleNameParameter = (string.IsNullOrEmpty(validationRequest.RuleName))
                                        ? string.Empty
                                        : string.Format(" and @RuleName='{0}'", validationRequest.RuleName);

               // var categoryId = GetRuleCategoryId(validationRequest.RuleCategory);

                //var ruleCategory = (string.IsNullOrEmpty(categoryId))
                //                        ? string.Empty
                //                        : string.Format(" and @Rule category='{0}'", categoryId);

                string RuleListPath = @"fast:/sitecore/content/#admin-portal#/global/#_supporting-content#/Rules/ClientLevel/*[@@templatename='RuleName' and @Client selection='%{0}%' and @active='{1}'{2}{3}]";
               
                //Passed Parameters ->Client Id, active, applicationSiteCoreId if any, RuleName if any
                var searchPattern = string.Format(RuleListPath, validationRequest.ClientId, "1", applicationSiteCoreId, ruleNameParameter);

                var fullRuleList = ExecuteAndReturnItems(searchPattern);

                if (fullRuleList != null && fullRuleList.Any())
                {
                   // var SelectedCategoryName = validationRequest.RuleCategory.GetType().GetField(validationRequest.RuleCategory.ToString()).GetCustomAttributes(false).OfType<DescriptionAttribute>().FirstOrDefault().Description;
                    var ruleCategoryFieldKey = "rule category";
                    foreach (var eachRule in fullRuleList)
                    {
                        if (eachRule.Fields != null && eachRule.Fields.Any() && eachRule.Fields.Any(f => ruleCategoryFieldKey.Equals(f.Key) && validationRequest.RuleCategory.Equals(f.Value, StringComparison.OrdinalIgnoreCase)))
                        {
                            var validator = GetValidatorFromItem(eachRule);
                            if (validator != null)
                                response.Add(validator);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //To Work on exception Part
            }
            return response;
        }

        public string GetAttributeOfType(RuleModel.RuleCategory enumVal)
        {
            var type = enumVal.GetType();
            var memInfo = type.GetMember(enumVal.ToString());
            var attributes = memInfo[0].GetCustomAttributes(typeof(RuleModel.RuleCategory), false);
            return (attributes.Length > 0) ? (String)attributes[0] : string.Empty;
        }

        private RuleModel.Validators GetValidatorFromItem(Item item)
        {
            if (item == null)
                return null;
            try
            {
                RuleModel.Validators validator = new RuleModel.Validators()
                {
                    CompareWith = item.Fields["Rule Value"].Value,
                    ErrorMessage = item.Fields["Error Message"].Value,
                    RuleName = item.Fields["RuleName"].Value
                };

                validator.Category = item.Fields["Rule category"].Value;
                validator.OperatorToPerform = item.Fields["Operator"].Value.ParseIntoEnum<RuleModel.ValidatorOperator>();

                return validator;

            }
            catch
            {
                //need to Work on this
            }

            return null;
        }

        private RuleModel.ValidationResponse ExecuteRules(RuleModel.ValidationRequest request,List<RuleModel.Validators> ruleList)
        {            

            if (ruleList == null)
                throw new NullReferenceException("Internal Error");

            var response = new RuleModel.ValidationResponse(true);

            switch (request.RuleCategory)
            {
                case RuleModel.RuleCategory.PartnerValidation:
                    {
                        var ruleName = request.RuleName;
                        response.PartnerValidation = new Dictionary<string, bool>();

                        if (string.IsNullOrEmpty(ruleName))
                        {
                            if (ruleList.Any())
                            {
                                foreach (var rule in ruleList)
                                {
                                    var status = CustomValidationForPartnerRules(rule);
                                    response.PartnerValidation.Add(rule.RuleName, status);
                                }
                            }
                        }
                        else
                        {
                            if (ruleList.Any(f => f.RuleName.Equals(ruleName, StringComparison.OrdinalIgnoreCase)))
                            {
                                var status = CustomValidationForPartnerRules(ruleList.Where(f => f.RuleName.Equals(ruleName, StringComparison.OrdinalIgnoreCase)).FirstOrDefault());
                                response.PartnerValidation.Add(ruleName, status);
                            }
                            else
                            {
                                response.PartnerValidation.Add(ruleName, false);
                            }
                        }
                    }

                    break;
                case RuleModel.RuleCategory.Content:
                    {
                        if (!ruleList.Any())
                            return response;

                        var inputToValidate = request.InputToValidate;
                        foreach (var rule in ruleList)
                        {
                            var status = CustomValidationForContent(inputToValidate, rule);
                            if (!status)
                            {
                                response.IsValidationSuccess = false;
                                response.ErrorMessage = rule.ErrorMessage;
                                return response;    //No Need to Run Remaining Rules Even if 1 Rule Fails
                            }
                        }
                    }
                    break;

                default:
                    throw new NotImplementedException("Rule Category Implementation Not Yet Done");
                    break;
            }


            
            return response;
        }
        /// <summary>
        /// To Validate the partner Rules.
        /// Return True if Validation needs to be Done
        /// Return False if Validation Not Required to Perform
        /// </summary>
        /// <param name="validationToPerform"></param>
        /// <param name="RuleName"></param>
        /// <returns></returns>
        private bool CustomValidationForPartnerRules(RuleModel.Validators validationToPerform)
        {            
            bool valueToCheck = false;

            return (bool.TryParse(validationToPerform.CompareWith, out valueToCheck) && valueToCheck);
        }

        private bool CustomValidationForContent(string input, RuleModel.Validators validationToPerform)
        {
            //if Null We are assigning as Empty to avoid exceptions. Also to Handle on Partner validation
            var validationValue = validationToPerform.CompareWith?? string.Empty;

            switch (validationToPerform.OperatorToPerform)
            {
                case RuleModel.ValidatorOperator.Contains:
                    {
                        if (!input.Contains(validationValue))
                        {
                            var toCompareWithAsList = validationValue.Split(',').ToList();
                            if (toCompareWithAsList.Any(f => input.Equals(f, StringComparison.CurrentCulture)))
                                return true;
                        }
                        return true;
                    }
                case RuleModel.ValidatorOperator.Equal:
                    {
                        if (validationToPerform.Category.Equals(RuleModel.RuleCategory.Content))
                            return input.Equals(validationValue, StringComparison.OrdinalIgnoreCase);
                                               
                        return true;
                    }
                case RuleModel.ValidatorOperator.GreaterThan:
                    {
                        return input.ConvertToLong() > validationValue.ConvertToLong();
                    }

                case RuleModel.ValidatorOperator.GreaterThanEqual:
                    {
                        return input.ConvertToLong()>=validationValue.ConvertToLong();
                    }

                case RuleModel.ValidatorOperator.LessThan:
                    {
                        return input.ConvertToLong() < validationValue.ConvertToLong();
                    }

                case RuleModel.ValidatorOperator.LessThanEqual:
                    {
                        return input.ConvertToLong() <= validationValue.ConvertToLong();
                    }

                case RuleModel.ValidatorOperator.Left:
                    {
                        var toCompareWithAsList = validationValue.Split(',').ToList();
                        return toCompareWithAsList.Any(e =>
                            {
                                return (e.Equals(input.Substring(0, (e.Length))));
                            });
                    }
                case RuleModel.ValidatorOperator.Right:
                    {
                        var toCompareWithAsList = validationValue.Split(',').ToList();
                        return toCompareWithAsList.Any(e =>
                        {
                            return (e.Equals(input.GetRightNCharacters(e.Length)));
                        });
                    }
                case RuleModel.ValidatorOperator.MaximumLength:
                    {
                        long maxLength = 0;
                        return long.TryParse(validationValue, out maxLength) && (input.Length <= maxLength);
                    }
                case RuleModel.ValidatorOperator.MinimumLength:
                    {
                        long minLength = 0;
                        return long.TryParse(validationValue, out minLength) && (input.Length >= minLength);
                    }
            }

            return true;
        }
    }
}