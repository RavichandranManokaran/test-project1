﻿CREATE PROCEDURE [dbo].[LB_RoomShortage]
as
begin
declare @fromdate datetime = getdate(), @todate datetime = getdate()+30

select distinct ProviderID, P.sku, p.Name, [dbo].[GetDateByCounter](DateCounter) [DateCounter] from LB_RoomAvailability ra
inner join ucommerce_product p on p.sku = ra.ProviderOccupancyTypeID
Where 
[dbo].[GetDateByCounter](DateCounter) between @fromdate and @todate and
((ra.NumberAllocated >= 1 and (ra.NumberAllocated-ra.NumberBooked) <= 1) or
(ra.NumberAllocated = 1 and (ra.NumberAllocated-ra.NumberBooked) = 0))
end
