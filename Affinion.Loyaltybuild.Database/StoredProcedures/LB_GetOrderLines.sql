﻿CREATE PROCEDURE [dbo].[LB_GetOrderLines]
(@OrderId INT)
AS
BEGIN
	SELECT OL.OrderLineId, OL.Sku, OL.Quantity, OL.ProductName, RR.CheckInDate, RR.CheckOutDate
	FROM   uCommerce_OrderLine AS OL LEFT JOIN
		   LB_RoomReservation AS RR ON OL.OrderLineId = RR.OrderLineId
	WHERE  (OL.OrderId = @OrderId)
END
