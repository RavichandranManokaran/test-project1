﻿<%@ Page Title="" Language="C#" MasterPageFile="~/sitecore modules/Shell/Affinion/Finance/MainLayout.Master" AutoEventWireup="true" CodeBehind="ManualHotelPaymentRequest.aspx.cs" Inherits="Affinion.LoyaltyBuild.AdminPortal.sitecore_modules.Shell.Affinion.Finance.ManualHotelPaymentRequest" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {

            Validation();

            $("#txtBookingRefNo").on("keypress keyup blur", function (event) {
                Validation();
            });
        });

        function Validation() {
            var isEnabled = false;
            var bookingRefNo = $("#txtBookingRefNo").val();

            if (bookingRefNo != "")
                isEnabled = true;
            else
                isEnabled = false;

            if ($("#txtBookingRefNo").val() != "") {
                $("#RetrieveBooking").removeAttr('disabled');
            }
            else {
                $("#RetrieveBooking").attr('disabled', 'disabled');
                $("#ManualPaymentRequest").attr('disabled', 'disabled');
            }
        }
 </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuItems" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Title" runat="server">
    Manual Hotel Payment Request
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Body" runat="server">
    <div class="row">
			<div class="col-md-12">
                <div>
                    <h4 class="title-text font-weight-bold"><asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label></h4>
                </div>  
			</div>
		</div>
    <div class="panel-heading bg-primary">
									<h4 class="panel-title">
									<span>Search Bookings :</span>
									<a data-toggle="collapse" data-parent="#accordion1" href="#collapse0" class="show-hide-list">
									<span class="glyphicon glyphicon-triangle-top pull-right"></span>
									</a>
									</h4>
								</div>
	<div id="collapse0" class="panel-collapse collapse in">
									<div class="panel-body">
										<div class="row">
											<div class="col-xs-12"> 
												<div class="col-xs-6 no-padding">
													<div class="col-xs-4 no-padding">
														<div><b>Booking Reference</b></div>
														<div><asp:TextBox ID="txtBookingRefNo" ClientIDMode="Static" runat="server" class="form-control"/></div>
													</div>
													<div class="col-xs-1 no-padding">
														<div><b>&nbsp;</b></div>
														<div class="text-center"><b>-</b></div>
													</div>
													<div class="col-xs-2 no-padding">
														<div><b>&nbsp;</b></div>
														<div><asp:TextBox ID="txtBookingSeq" runat="server" class="form-control"/></div>
													</div>
													<div class="col-xs-5">
														<div><b>&nbsp;</b></div>
														<div><asp:Button ID="RetrieveBooking" Text="Retrieve Booking" ClientIDMode="Static" runat="server" class="btn btn-primary mar0" OnClick="RetrieveBooking_Click" /></div>
													</div>
												</div>
												<div class="col-xs-6 no-padding"></div>
											</div>	
										</div>
										<div class="row">
											<div ID="divRetrieveBookingDetails" runat="server" class="col-xs-12 marT15 hotelrequest">
												<table class="table table-striped table-bordered">
													<thead>
													  <tr>
														<th></th>
														<th></th>
														<th></th>
													  </tr>
													</thead>
													<tbody>
													  <tr>
														<td>
															<ul class="fa-ul">
																<li><i class="fa-li fa fa-check-square"></i><b><asp:Label ID="lblBookingStatus" Text="Booking successfully identified" runat="server"></asp:Label></b></li>
															</ul>
														</td>
														<td>
															<ul class="fa-ul">
																<li><i class="fa-li fa fa-check-square"></i>Arrival Date <asp:Label ID="lblArrivalDate" runat="server"></asp:Label> </li>
																<li><i class="fa-li fa fa-check-square"></i>Departure Date <asp:Label ID="lblDepartureDate" runat="server"></asp:Label> </li>
															</ul>
														</td>
														<td>
															<ul class="fa-ul">
																<li><i class="fa-li fa fa-check-square"></i>First Name - <asp:Label ID="lblFirstName" runat="server"></asp:Label> </li>
																<li><i class="fa-li fa fa-check-square"></i>Last Name - <asp:Label ID="lblLastName" runat="server"></asp:Label> </li>
															</ul>
														</td>
													  </tr>
													   <tr>
														<td>
															<ul class="fa-ul">
																<li><i class="fa-li fa fa-check-square"></i>Provider Name - <asp:Label ID="lblProviderName" runat="server"></asp:Label>  </li>
																<li><i class="fa-li fa fa-check-square"></i>Item - <asp:Label ID="lblItem" runat="server"></asp:Label></li>
															</ul>
														</td>
														<td>
															<ul class="fa-ul">
																<li><i class="fa-li fa fa-check-square"></i>First Confirmed on <asp:Label ID="lblFirstConfirmed" runat="server"></asp:Label> </li>
															</ul>
														</td>
														<td>
															<ul class="fa-ul">
																<li><i class="fa-li fa fa-check-square"></i>Request is after Departure Date</li>
															</ul>
														</td>
													  </tr>
													   <tr>
														<td>
                                                            <ul class="fa-ul">
																<li class="required" id="paymentrequestwitinmonth" runat="server" visible="false"><i class="fa-li fa fa-check-square"></i>Payment request is within the time limit of six month - four month</li>
															</ul>
														</td>
														<td>
															<ul class="fa-ul">
																<li id="Paymenteligible" runat="server"><i class="fa-li fa fa-check-square"></i></li>
															</ul>
														</td>
														<td>
															<ul class="fa-ul">
																<li class="required" id="PaymentPaidDate" runat="server" visible="false"><i class="fa-li fa fa-check-square"></i>Provider has been paid on <asp:Label ID="lblPaidDate" runat="server"></asp:Label></li>
															</ul>
														</td>
													  </tr>
													  <tr>
														<td>
															<ul class="fa-ul">
																<li><i class="fa-li fa fa-check-square"></i>Suggested Payment Amount : 1 room(s) @ <asp:Label ID="lblSuggestedAmount" runat="server"></asp:Label> = <b><asp:Label ID="lblTotalAmount" runat="server"></asp:Label></b></li>
															</ul>
														</td>
														<td>
															<ul class="fa-ul">
																<li class="required" id="PaymentRequestedDate" runat="server" visible="false" ><i class="fa-li fa fa-check-square"></i>Payment already Requested on <asp:Label ID="lblPaymentRequestedDate" runat="server"></asp:Label></li>
															</ul>
														</td>
														<td>
															<asp:Button ID="ManualPaymentRequest" ClientIDMode="Static" runat="server" Text="Request Payment" class="btn btn-primary marL30" OnClick="ManualPaymentRequest_Click"/>
														</td>
													  </tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
</asp:Content>
