﻿CREATE PROCEDURE [dbo].[LB_AddFinanceCode]
@OrderId INT=0,
@OrderLineId INT =0,
@Company VARCHAR(100)=NULL,
@MerchantIdentificationNo BIGINT=0,
@ClientName VARCHAR(100)=NULL,
@Department VARCHAR(100)=NULL,
@ProductCode VARCHAR(100)=NULL,
@GlCode VARCHAR(100)=NULL,
@ClientCode VARCHAR(100)=NULL,
@Description VARCHAR(100)=NULL
AS
BEGIN

IF @OrderId <> 0 AND @OrderId >0
BEGIN
SET @OrderLineId=(SELECT OrderLineId FROM uCommerce_OrderLine WHERE OrderId=@OrderId)
END
 
                INSERT INTO uCommerce_FinanceCode 
                    (
						OrderLineId,
						Company,
						MerchantIdentificationNo,
						ClientName,
						Department,
						ProductCode,
						GlCode,
						ClientCode,
						Description
					)			
				VALUES
				(
						@OrderLineId,
						@Company,
						@MerchantIdentificationNo ,
						@ClientName,
						@Department,
						@ProductCode,
						@GlCode,
						@ClientCode,
						@Description
				)
				
END  




