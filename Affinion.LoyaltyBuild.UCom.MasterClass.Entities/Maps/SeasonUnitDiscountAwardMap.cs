﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Maps
{
    public class SeasonUnitDiscountAwardMap : SubclassMap<SeasonUnitDiscountAward>
    {
        public SeasonUnitDiscountAwardMap()
        {
            base.Table("uCommerce_SeasonUnitDiscountAward");
            base.KeyColumn("SeasonUnitDiscountAwardId");
            base.Map(x => x.SeasonUnitDiscountAwardId).ReadOnly();
            base.Map(x => x.Content).ReadOnly();
            base.HasMany<SeasonUnitDiscountAwardDetail>(x => x.Details).KeyColumn("SeasonUnitDiscountAwardId");
        }
    }

}
