﻿if not exists (select 1 from uCommerce_DataType where typename = 'Country' )
begin
	insert into uCommerce_DataType (TypeName,Nullable,ValidationExpression,BuiltIn,DefinitionName,Deleted,Guid,CreatedOn,
	CreatedBy,ModifiedOn,ModifiedBy)
	values ('Country',1,'',0,'SiteCoreCountry',0,NEWID(),GETDATE(),'admin',GETDATE(),'admin'),
	('Region',1,'',0,'SiteCoreRegion',0,NEWID(),GETDATE(),'admin',GETDATE(),'admin'),
	('Theme',1,'',0,'SiteCoreHotelTheme',0,NEWID(),GETDATE(),'admin',GETDATE(),'admin'),
	('HotelFacility',1,'',0,'SiteCoreHotelFacility',0,NEWID(),GETDATE(),'admin',GETDATE(),'admin'),
	('Client',1,'',0,'SiteCoreClient',0,NEWID(),GETDATE(),'admin',GETDATE(),'admin'),
	('OfferType',1,'',0,'SiteCoreOfferType',0,NEWID(),GETDATE(),'admin',GETDATE(),'admin'),
	('Groups',1,'',0,'SiteCoreGroups',0,NEWID(),GETDATE(),'admin',GETDATE(),'admin'),
	('SuperGroups',1,'',0,'SiteCoreSuperGroups',0,NEWID(),GETDATE(),'admin',GETDATE(),'admin')
end
Go
--Insert Defination
declare @definationid int = 0
select @definationid = DefinitionId from uCommerce_Definition
where Name = 'hotel'
declare @datatypeid int = 0
select @datatypeid = DataTypeId from uCommerce_DataType where DefinitionName= 'SiteCoreCountry'

--print @datatypeid
if(@datatypeid <> 0)
begin
insert into uCommerce_DefinitionField(DataTypeId,DefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,BuiltIn,DefaultValue,Guid)
values (@datatypeid , @definationid,'Country',1,0,1,0,0,0,0,'',NEWID())
end

select @datatypeid = DataTypeId from uCommerce_DataType where DefinitionName= 'SiteCoreRegion'
if(@datatypeid <> 0)
begin
insert into uCommerce_DefinitionField(DataTypeId,DefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,BuiltIn,DefaultValue,Guid)
values (@datatypeid , @definationid,'Region',1,0,1,0,0,0,0,'',NEWID())
end

select @datatypeid = DataTypeId from uCommerce_DataType where DefinitionName= 'SiteCoreClient'
if(@datatypeid <> 0)
begin
insert into uCommerce_DefinitionField(DataTypeId,DefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,BuiltIn,DefaultValue,Guid)
values (@datatypeid , @definationid,'Client',1,0,1,0,0,0,0,'',NEWID())
end

select @datatypeid = DataTypeId from uCommerce_DataType where DefinitionName= 'SiteCoreHotelFacility'
if(@datatypeid <> 0)
begin
insert into uCommerce_DefinitionField(DataTypeId,DefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,BuiltIn,DefaultValue,Guid)
values (@datatypeid , @definationid,'HotelFacility',1,0,1,0,0,0,0,'',NEWID())
end

select @datatypeid = DataTypeId from uCommerce_DataType where DefinitionName= 'SiteCoreHotelTheme'
if(@datatypeid <> 0)
begin
insert into uCommerce_DefinitionField(DataTypeId,DefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,BuiltIn,DefaultValue,Guid)
values (@datatypeid , @definationid,'Theme',1,0,1,0,0,0,0,'',NEWID())
end

select @datatypeid = DataTypeId from uCommerce_DataType where DefinitionName= 'SiteCoreGroups'
if(@datatypeid <> 0)
begin
insert into uCommerce_DefinitionField(DataTypeId,DefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,BuiltIn,DefaultValue,Guid)
values (@datatypeid , @definationid,'Groups',1,0,1,0,0,0,0,'',NEWID())
end

select @datatypeid = DataTypeId from uCommerce_DataType where DefinitionName= 'SiteCoreSuperGroups'
if(@datatypeid <> 0)
begin
insert into uCommerce_DefinitionField(DataTypeId,DefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,BuiltIn,DefaultValue,Guid)
values (@datatypeid , @definationid,'SuperGroups',1,0,1,0,0,0,0,'',NEWID())
end
go

/****** Object:  StoredProcedure [dbo].[SearchCustomerQuery]    Script Date: 2/14/2016 2:32:41 PM ******/

IF(EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SearchCustomerQuery]') AND type in (N'P', N'PC')))
	DROP PROCEDURE [dbo].[SearchCustomerQuery]
GO



/****** Object:  StoredProcedure [dbo].[SearchCustomerQuery]    Script Date: 2/14/2016 2:32:41 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
/*<summary>
Description: Stored procedure to retrieve Existing Booking details for the given search criteria
<param name="@queryTypeID">querytype id</param>
<param name="@clientIds">Client Id or ',' seperated Id list</param>
<param name="@checkinDate">Booking Created Date</param>
<param name="@checkoutDate">Customer Arrival Date</param>
<param name="@firstname">Customer First Name</param>
<param name="@lastname">Customer Last Name</param>
<param name="@compaignGroupId"> Compaign Group Id</param>
<param name="@providerName">Hotel Name</param>
<param name="@bookingReference">Customer Booking reference Id</param>
<param name="@QueryStatusId">Current Order Status Id</param>
</summary>*/
CREATE PROCEDURE [dbo].[SearchCustomerQuery]
(	   
	   @queryTypeID int =NULL,
	   @clientId varchar(50) =NULL, 
	   @startDate Date=NULL, 
       @endDate Date=NULL,
	   @firstname nvarchar(512)=NULL, 
       @lastname nvarchar(512)=NULL, 
	   @campaignGroupId nvarchar(50) = NULL,
       @providerId nvarchar(50)=NULL, 
       @bookingReference nvarchar(50)=NULL,
	   @isSolved bit=NULL)
AS 
BEGIN
	SELECT 
	    CQ.QueryId,
	    CQ.Content,
		CA.ClientId,
		C.Name [ProviderName], -- provider name 
		CUS.FirstName,
		CUS.LastName, 
		CUS.EmailAddress,
		isnull(CUS.FirstName,'')+ ' ' + isnull(CUS.LastName,'') +'  ' + isnull(CA.AddressLine1,'') + ' ' + isnull(CA.AddressLine2,'') [Name],
		isnull(CUS.PhoneNumber,MobilePhoneNumber) PhoneNumber,
		RR.OrderReference BookingReference,
		CQ.IsSolved,
		CQ.CreatedDate,
		CQ.UpdatedDate,
	    ( Select Count(1) from uCommerce_CustomerQuery_Attachments where QueryId = CQ.QueryId) HasAttachments
	FROM [dbo]. uCommerce_CustomerQuery CQ
		INNER JOIN [dbo].uCommerce_OrderLine OL ON OL.OrderLineId = CQ.OrderLineId
		INNER JOIN [dbo].uCommerce_PurchaseOrder PO ON OL.OrderId = PO.OrderId
		INNER JOIN [dbo].uCommerce_RoomReservation RR ON OL.OrderLineId = RR.OrderLineId 
		INNER JOIN [dbo].uCommerce_Product P ON OL.Sku = P.Sku
		INNER JOIN [dbo].uCommerce_CategoryProductRelation CPR ON P.ProductId = CPR.ProductId 
		INNER JOIN [dbo].uCommerce_Category C ON CPR.CategoryId = C.CategoryId
		INNER JOIN [dbo].uCommerce_Customer CUS ON PO.CustomerId = CUS.CustomerId 
		INNER JOIN [dbo].uCommerce_CustomerAttribute CA ON PO.CustomerId = CA.CustomerId 
	--	LEFT JOIN [dbo].[uCommerce_OrderLineCampaignRelation] OCR ON OL.OrderId = OCR.OrderLineId
	--	LEFT JOIN [dbo].[uCommerce_CampaignItem] CI ON OCR.CampaignItemId = CI.CampaignItemId
	--	LEFT JOIN [dbo].[uCommerce_Campaign] CM ON CM.CampaignId = CI.CampaignId
	WHERE (@firstname IS NULL OR CUS.FirstName Like @firstname + '%')
		AND (@lastname IS NULL OR CUS.LastName Like @lastname + '%') 
		AND (@clientId IS NULL OR CA.ClientId = @clientId)
		AND (@providerId IS NULL OR c.[Guid] = @providerId)
		AND (CONVERT(VARCHAR(11),CQ.CreatedDate,111) BETWEEN ISNULL(@startDate,'1900-1-1') AND ISNULL(@endDate, '2099-1-1'))
		AND (@bookingReference IS NULL OR RR.OrderReference = @bookingReference)
		AND (@IsSolved IS NULL OR @IsSolved = CQ.IsSolved)
		AND (@campaignGroupId IS NULL OR @campaignGroupId = CQ.CampaignId)
		AND (@queryTypeID IS NULL OR @queryTypeID = CQ.QueryTypeId)
		AND CQ.OrderLineId > 0

union
SELECT  
		
		CQ.QueryId,
	    CQ.Content,
		CA.ClientId,
		'' [ProviderName], -- provider name 
		CUS.FirstName,
		CUS.LastName, 
		CUS.EmailAddress,
		isnull(CUS.FirstName,'')+ ' ' + isnull(CUS.LastName,'') +'  ' + isnull(CA.AddressLine1,'') + ' ' + isnull(CA.AddressLine2,'') [Name],
		isnull(CUS.PhoneNumber,MobilePhoneNumber) PhoneNumber,
		CA.BookingReferenceId BookingReference,
		CQ.IsSolved,
		CQ.CreatedDate,
		CQ.UpdatedDate,
	    ( Select Count(1) from uCommerce_CustomerQuery_Attachments where QueryId = CQ.QueryId) HasAttachments
	FROM [dbo]. uCommerce_CustomerQuery CQ
		--INNER JOIN [dbo].uCommerce_OrderLine OL ON OL.OrderLineId = CQ.OrderLineId
		--INNER JOIN [dbo].uCommerce_PurchaseOrder PO ON OL.OrderId = PO.OrderId
		--INNER JOIN [dbo].uCommerce_RoomReservation RR ON OL.OrderLineId = RR.OrderLineId 
		--INNER JOIN [dbo].uCommerce_Product P ON OL.Sku = P.Sku
		--INNER JOIN [dbo].uCommerce_CategoryProductRelation CPR ON P.ProductId = CPR.ProductId 
		--INNER JOIN [dbo].uCommerce_Category C ON CPR.CategoryId = C.CategoryId
		INNER JOIN [dbo].uCommerce_Customer CUS ON CQ.CustomerId = CUS.CustomerId 
		INNER JOIN [dbo].uCommerce_CustomerAttribute CA ON CUS.CustomerId = CA.CustomerId 
	--	LEFT JOIN [dbo].[uCommerce_OrderLineCampaignRelation] OCR ON OL.OrderId = OCR.OrderLineId
	--	LEFT JOIN [dbo].[uCommerce_CampaignItem] CI ON OCR.CampaignItemId = CI.CampaignItemId
	--	LEFT JOIN [dbo].[uCommerce_Campaign] CM ON CM.CampaignId = CI.CampaignId
	WHERE (@firstname IS NULL OR CUS.FirstName Like @firstname + '%')
		AND (@lastname IS NULL OR CUS.LastName Like @lastname + '%') 
		AND (@clientId IS NULL OR CQ.PartnerId = @clientId)
		AND (@providerId IS NULL OR CQ.ProviderId = @providerId)
		AND (CONVERT(VARCHAR(11),CQ.CreatedDate,111) BETWEEN ISNULL(@startDate,'1900-1-1') AND ISNULL(@endDate, '2099-1-1'))
		AND (@bookingReference IS NULL OR CA.BookingReferenceId = @bookingReference)
		AND (@IsSolved IS NULL OR @IsSolved = CQ.IsSolved)
		AND (@campaignGroupId IS NULL OR @campaignGroupId = CQ.CampaignId)
		AND (@queryTypeID IS NULL OR @queryTypeID = CQ.QueryTypeId)
		AND  CQ.CustomerId > 0
END
GO




/* affinion Process OLDueAndOLPayments stored procedure*/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[affinion_ProcessOLDueAndOLPayments]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[affinion_ProcessOLDueAndOLPayments]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
/*
*/ 
CREATE PROCEDURE [dbo].[affinion_ProcessOLDueAndOLPayments]
(	 
	@OrderId INT=NULL,
	@IsPartialPayment BIT = 1	 
)
AS 
BEGIN
	IF (@OrderId IS NOT NULL)
	BEGIN	
		--Inserting data into ucommerce_orderlinedue based ON 'LBCOM'					
		INSERT INTO ucommerce_orderlinedue 
		(
			OrderLineID, 
			DueTypeId, 
			CurrencyTypeId, 
			Amount, 
			CommissionAmount, 
			TransferAmount,
			ClientAmount, 
			ProviderAmount, 
			CollectionByProviderAmount,
			Note, 
			OrderLinePaymentID, 
			IsDeleted, 
			CreatedBy, 
			CreatedDate, 
			UpdatedBy, 
			UpdatedDate,
			CancellationAmount, SubrateItemCode
		)
		SELECT 
			OLS.OrderLineID AS [OrderLineID],
			DT.DueTypeId AS [DueTypeId], 
			PO.CurrencyId AS [CurrencyTypeId],
			OLS.Amount-ISNULL(OLS.Discount,0) AS [Amount],
			0 AS [CommissionAmount],
			0 AS [TransferAmount],
			0 AS [ClientAmount],
			0 AS [ProviderAmount],
			0 AS [CollectionByProviderAmount],
			'' AS [Note],
			NULL AS [OrderLinePaymentID],
			0 AS [IsDeleted],
			OL.CreatedBy AS [CreatedBy],
			OL.CreatedOn AS [CreatedDate],
			'' AS [UpdatedBy],
			GETDATE() AS [UpdatedDate], 
			0 AS [CancellationAmount],
			OLS.SubrateItemCode AS [SubrateItemCode]
		FROM 
			uCommerce_OrderLineSubrates OLS
			INNER JOIN ucommerce_duetypes DT ON ols.SubrateItemCode = DT.SubrateItemCode  
			INNER JOIN ucommerce_orderline OL ON ol.OrderLineId = OLS.OrderLineID		
			INNER JOIN uCommerce_PurchaseOrder PO ON po.OrderId = OL.OrderId
		WHERE 
			OLS.SubrateItemCode = 'LBCOM' 
			AND Po.OrderId  = @OrderId

		--this is a full payment (param name need to be reverted
		IF (@IsPartialPayment = 0)
		BEGIN
			INSERT INTO ucommerce_orderlinedue 
			(
				OrderLineID, 
				DueTypeId, 
				CurrencyTypeId, 
				Amount, 
				CommissionAmount, 
				TransferAmount,
				ClientAmount, 
				ProviderAmount, 
				CollectionByProviderAmount,
				Note, 
				OrderLinePaymentID, 
				IsDeleted, 
				CreatedBy, 
				CreatedDate, 
				UpdatedBy, 
				UpdatedDate,
				CancellationAmount, SubrateItemCode
			)
			SELECT 
				OL.OrderLineID AS [OrderLineID],
				1 AS [DueTypeId], 
				PO.CurrencyId AS [CurrencyTypeId],
				OL.Total-OLS.Amount+ISNULL(OLS.Discount,0)  AS [Amount],
				0 AS [CommissionAmount],
				0 AS [TransferAmount],
				0 AS [ClientAmount],
				0 AS [ProviderAmount],
				0 AS [CollectionByProviderAmount],
				'' AS [Note],
				NULL AS [OrderLinePaymentID],
				0 AS [IsDeleted],
				OL.CreatedBy AS [CreatedBy],
				OL.CreatedOn AS [CreatedDate],
				'' AS [UpdatedBy],
				GETDATE() AS [UpdatedDate], 
				0 AS [CancellationAmount],
				'' AS [SubrateItemCode]
			FROM 
				uCommerce_OrderLineSubrates OLS
				INNER JOIN ucommerce_duetypes DT ON ols.SubrateItemCode = DT.SubrateItemCode  
				INNER JOIN ucommerce_orderline OL ON ol.OrderLineId = OLS.OrderLineID		
				INNER JOIN uCommerce_PurchaseOrder PO ON po.OrderId = OL.OrderId
			WHERE 
				OLS.SubrateItemCode = 'LBCOM' 
				AND Po.OrderId  = @OrderId
		END

		--Inserting data into ucommerce_orderlinedue based ON 'CCPROFEE'
		INSERT INTO ucommerce_orderlinedue 
		(
			OrderLineID, 
			DueTypeId, 
			CurrencyTypeId, 
			Amount, 
			CommissionAmount, 
			TransferAmount,
			ClientAmount, 
			ProviderAmount, 
			CollectionByProviderAmount,
			Note, 
			OrderLinePaymentID, 
			IsDeleted, 
			CreatedBy, 
			CreatedDate, 
			UpdatedBy, 
			UpdatedDate,
			CancellationAmount, SubrateItemCode
		)
		SELECT TOP 1 
			OLS.OrderLineID AS [OrderLineID],
			DT.DueTypeId AS [DueTypeId], 
			PO.CurrencyId AS [CurrencyTypeId],
			OLS.Amount AS [Amount],
			0 AS [CommissionAmount],
			0 AS [TransferAmount],
			0 AS [ClientAmount],
			0 AS [ProviderAmount],
			0 AS [CollectionByProviderAmount],
			'' AS [Note],
			NULL AS [OrderLinePaymentID],
			0 AS [IsDeleted],
			OL.CreatedBy AS [CreatedBy],
			OL.CreatedOn AS [CreatedDate],
			'' AS [UpdatedBy],
			GETDATE() AS [UpdatedDate], 
			0 AS [CancellationAmount],
			OLS.SubrateItemCode AS [SubrateItemCode]
		FROM 
			uCommerce_OrderLineSubrates OLS
			INNER JOIN ucommerce_duetypes DT ON ols.SubrateItemCode = DT.SubrateItemCode  
			INNER JOIN ucommerce_orderline OL ON ol.OrderLineId = OLS.OrderLineID		
			INNER JOIN uCommerce_PurchaseOrder PO ON po.OrderId = OL.OrderId
		WHERE  
			OLS.SubrateItemCode = 'CCPROFEE'  AND 
			OLS.OrderLineID in (Select orderlineid from ucommerce_orderline where orderid = @OrderId)
		ORDER BY ols.Amount DESC
		
		--Inserting data into ucommerce_orderlinepayment 
		INSERT INTO ucommerce_orderlinepayment
		(
			OrderLineID, 
			CurrencyTypeId, 
			PaymentMethodID, 
			Amount, 
			OrderPaymentID, 
			CreatedDate, 
			ReferenceID, 
			CreatedBy,
			DueTypeId
		)
		SELECT 
			ol.orderlineid AS [OrderLineID], 
			po.CurrencyId AS [CurrencyTypeId],
			p.PaymentMethodId AS [PaymentMethodID],
			old.Amount AS [Amount],
			p.PaymentId AS [OrderPaymentID],
			p.Created AS [CreatedDate],
			convert(nvarchar(100), po.OrderGuid) as [ReferenceID],
			--p.ReferenceId AS [ReferenceID], 
			ol.CreatedBy  AS [CreatedBy],
			old.DueTypeId
		FROM  uCommerce_OrderLineDue old 
			INNER JOIN ucommerce_orderline ol ON old.orderlineid = ol.orderlineid 			
			INNER JOIN uCommerce_PurchaseOrder po ON po.OrderId = ol.OrderId
			INNER JOIN ucommerce_payment p ON p.orderid = po.orderid
		WHERE
			PO.orderid = @OrderId

		UPDATE oldue SET oldue.PaymentMethodId = olp.PaymentMethodID
		FROM ucommerce_orderlinedue oldue, ucommerce_orderlinepayment olp
		WHERE oldue.orderlineid = olp.OrderLineID 
			AND oldue.OrderLineID in (Select orderlineid from ucommerce_orderline where orderid = @OrderId)
			AND oldue.OrderLinePaymentID is null

		--Orderline status date update
		declare @orderlineids table(orderlineid int, isupdated bit)
		insert into @orderlineids select orderlineid, 0 from uCommerce_OrderLine where orderid = @OrderId
		declare @currentorderlineid int
		while(select COUNT(1) from @orderlineids where isupdated = 0) > 0
		begin
			select top 1 @currentorderlineid = orderlineid from @orderlineids where isupdated = 0
			--call update status sp here
			exec [affinion_AddBookingStatus] @currentorderlineid, null, ''
		update @orderlineids set isupdated = 1 where orderlineid = @currentorderlineid
		end

		--update entry for special request in reservation notes
		insert into uCommerce_ReservationNotes
			select 3 as NoteTypeId, rr.OrderLineId, rr.SpecialRequest, getdate() as CreatedDate, '' as CreatedBy from uCommerce_RoomReservation rr
			inner join ucommerce_orderline ol on ol.orderlineid = rr.orderlineid
			inner join ucommerce_purchaseorder po on po.orderid = ol.OrderId 
			where po.orderid = @OrderId
			and ISNULL(rr.specialrequest,'') <> '' 
			and not exists (select 1 from uCommerce_ReservationNotes where OrderLineId = rr.OrderLineId and notetypeid = 3)
	END
END
 