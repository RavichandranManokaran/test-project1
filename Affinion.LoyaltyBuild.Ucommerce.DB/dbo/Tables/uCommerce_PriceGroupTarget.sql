﻿CREATE TABLE [dbo].[uCommerce_PriceGroupTarget] (
    [PriceGroupTargetId] INT            NOT NULL,
    [PriceGroupName]     NVARCHAR (150) NULL,
    PRIMARY KEY CLUSTERED ([PriceGroupTargetId] ASC)
);

