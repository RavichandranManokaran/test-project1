﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce;
using UCommerce.EntitiesV2.Maps;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Maps
{
    public class SupplierOfferUnSubScribeMap : BaseClassMap<SupplierOfferUnSubScribe>
    {
        // Methods
        public SupplierOfferUnSubScribeMap()
        {
            base.Cache.ReadWrite();
            base.Cache.Region(UCommerce.Constants.Cache.Regions.MarketingFoundation);
            base.Table("LB_SupplierOfferUnSubScribe");
            base.Id(x => x.SupplierOfferUnSubScribeId, "SupplierOfferUnSubScribeId").GeneratedBy.Identity();
            Map(x => x.Reason).Not.Nullable();
            Map(x => x.CategoryGuid).Not.Nullable();
            Map(x => x.CampaignItem).Not.Nullable();
            Map(x => x.CreatedDate).Nullable();
            Map(x => x.CreatedBy).Nullable();
          //  Map(x => x.).Not.Nullable();
        }
    }
}

