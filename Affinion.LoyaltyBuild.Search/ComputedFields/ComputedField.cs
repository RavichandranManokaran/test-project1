﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           BasePrimaryLayout.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Search (VS project name)
/// Description:           Calculate the value to be stored in the lucene index
/// </summary>
namespace Affinion.LoyaltyBuild.Search.ComputedFields
{
    using Sitecore.ContentSearch;
    using Sitecore.ContentSearch.ComputedFields;
    using Sitecore.Data;
    using System;

    /// <summary>
    /// Calculate the value to be stored in the lucene index
    /// </summary>
    public abstract class ComputedField : IComputedIndexField
    {
        private readonly Lazy<Database> contentDatabase = new Lazy<Database>(() => Database.GetDatabase("web"));

        public string FieldName { get; set; }

        public string ReturnType { get; set; }

        protected Database ContentDatabase
        {
            get { return contentDatabase.Value; }
        }

        /// <summary>
        /// Calculates the field value
        /// </summary>
        /// <param name="indexable">The indexable item</param>
        /// <returns>Returns the calculated value</returns>
        public abstract object ComputeFieldValue(IIndexable indexable);
    }
}
