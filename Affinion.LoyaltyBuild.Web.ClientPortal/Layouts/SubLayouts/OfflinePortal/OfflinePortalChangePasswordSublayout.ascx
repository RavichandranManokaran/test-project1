﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalChangePasswordSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalChangePasswordSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="body-container">
    <div class="container">
        <div class="form-outer">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <strong>
                                <sc:Text Field="FormTitle" runat="server" />
                            </strong>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <asp:Panel ID="PanelPasswordExpiered" runat="server" Visible="false" align="center">
                            <div runat="server" class="alert alert-warning">
                                <b>
                                    <sc:Text Field="PasswordExpiered" runat="server" />
                                </b>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="PanelSuccess" runat="server" Visible="false" align="center">
                            <div runat="server" class="alert alert-success">
                                <b>
                                    <asp:HyperLink align="center" ID="HyperLinkContinue" runat="server" Visible="false">                                       
                                    </asp:HyperLink>
                                </b>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="PanelEdit" runat="server">
                            <div runat="server" id="errorMessage" class="alert alert-danger">
                                <asp:Literal ID="literalMessagePassword" runat="server" />
                            </div>

                            <div class="form-group">
                                <sc:Text Field="CurrentPasssword" runat="server" />
                                <asp:TextBox ID="TextBoxPasswordOld" TextMode="Password" runat="server" class="form-control" placeholder="Current Password" required="" />
                            </div>
                            <div class="form-group">
                                <sc:Text Field="NewPassword" runat="server" />

                                <asp:RegularExpressionValidator ID="RegularExpressionValidatorPassword" runat="server" ErrorMessage="Password doesn't mach the requisites" ForeColor="Red" ControlToValidate="textBoxPasswordNew"></asp:RegularExpressionValidator>
                                <asp:TextBox ID="TextBoxPasswordNew" TextMode="Password" runat="server" class="form-control" placeholder="New Password" required="" />
                                <br />

                                <p class="alert alert-warning">
                                    <sc:Text Field="PasswordHint" runat="server" />
                                </p>
                            </div>
                            <div class="form-group">
                                <sc:Text Field="ConfirmPassword" runat="server" />
                                <asp:CompareValidator ID="CompareValidatorRePassword" runat="server" ErrorMessage="The Confirm New Password must match the New Password" ControlToCompare="textBoxPasswordNew" ControlToValidate="textBoxPasswordConfirm" ForeColor="Red"></asp:CompareValidator>
                                <asp:TextBox ID="TextBoxPasswordConfirm" TextMode="Password" runat="server" class="form-control" placeholder="New Password" required="" />
                            </div>
                            <asp:Button ID="ButtonEditPassword" Text="Submit" OnClick="ButtonEditPassword_Click" runat="server" class="btn btn-sm btn-default" />

                            <asp:Button ID="ButtonCancel" Text="Cancel" OnClick="ButtonCancel_Click" runat="server" class="btn btn-sm btn-default" formnovalidate=""  CausesValidation="False" />                         

                        </asp:Panel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
