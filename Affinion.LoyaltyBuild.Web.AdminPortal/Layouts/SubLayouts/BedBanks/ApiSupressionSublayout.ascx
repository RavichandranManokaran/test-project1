﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ApiSupressionSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.BedBanks.ApiSupressionSublayout" %>
<%--<html>
   <head>
		<title>Bed Bank Provider API Suppression</title>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Bootstrap, a sleek, intuitive, and powerful mobile first front-end framework for faster and easier web development.">
		<meta name="keywords" content="HTML, CSS, JS, JavaScript, framework, bootstrap, front-end, frontend, web development">
		<meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
		
		
		
		<link href="datePicker/jquery-ui.custom.min.css" rel="stylesheet" type="text/css"/>	
		<link rel="stylesheet" href="styles/bootstrap.min.css">
		<link rel="stylesheet" href="styles/jquery.dataTables.min.css">
		 
		<link href="styles/responsiveNav.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="styles/affinion.min.css">
		<link rel="stylesheet" href="styles/affinion-bedbank.css">
		<link rel="stylesheet" href="styles/theme1/theme1.css">
		<link rel="stylesheet" href="styles/font-awesome.min.css">
		<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
		
   
   </head>
   <body>--%>
     <%--<div class="container-outer width-full overflow-hidden-att">
         <div class="container-outer-inner width-full bedbank-header-top-bar">
            <div class="container-fluid">
              <div class="row coomon-outer-padding">
                  <div class="col-md-6 col-xs-6">
                     <div class="logo">
                        <a href="home.html"><img src="images/Loyaltybuild_Black_Blue_RGB.png"  class="img-responsive" alt="logo" ></a>
                     </div>
                  </div>
                  <div class="col-md-6 col-xs-6">
					<div class="navbar navbar-default">
						<div class="container-fluid">
							<ul class="nav navbar-nav">
								<li class="dropdown">
									<a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-cog"></span>Bed Bank Admin Options
									<span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li><a href="#">Page 1</a></li>
										<li><a href="#">Page 2</a></li>
										<li><a href="#">Page 3</a></li> 
									</ul>
								</li>
								<li class="dropdown">
									<a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-user"></span>Sitecore User
									<span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li><a href="#">Page 1</a></li>
										<li><a href="#">Page 2</a></li>
										<li><a href="#">Page 3</a></li> 
									</ul>
								</li>
							</ul>
						</div>
					</div>
                  </div>
                  <div class="sm-visibale">
                       
                     <div class="sm-visibale-inner">
                         <div class="res-icon float-right visible-xs" id="fav-icon">
                           <a href="#"><i class="fa fa-heart"></i></a>
                        </div>
                        <div class="res-icon float-right visible-xs" id="bck-icon">
                           <a href="#">
                              <div id="basket-ico-respodive">
                                 <p><i class="fa fa-briefcase"></i> 
                                    <span>5</span>
                                 </p>
                              </div>
                           </a>
                        </div>
                        <div class="res-icon float-right visible-xs" id="user-icon">
                           <a href="#"><i class="fa fa-user" data-toggle="modal" data-target="#exampleModal" data-whatever="@fat"></i></a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
    </div>--%>
	<%--<div class="body-container">
		<div class="container-fluid"> 
			<div class="content">
				<div class="content-body">
				<div class="row no-margin">--%>
					<%--<div class="col-sm-2 no-padding bedbank-leftpanel">
						<div class="navbar navbar-default" role="navigation">
							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse navbar-ex1-collapse">
								<ul class="nav navbar-nav">
									<li class="active"><a href="#">Dashboard</a></li>
									<li><a href="#">Link 1</a></li>
									<li><a href="#">Link 2</a></li>
									<li><a href="#">Link 3</a></li>
								</ul>
							</div><!-- /.navbar-collapse -->
						</div>
					</div>--%>
					<%--<div class="col-sm-10 no-padding bedbank-rightpanel">--%>
						<div class="row no-margin ">
							<div class="col-sm-12">
								<h3><sc:text field="Title" runat="server"/></h3>
							</div>
							<div class="col-sm-3 no-padding-right">
								<div class="accordian-outer">
									<div class="panel-group" id="accordion">
										<div class="panel panel-default">
											<div class="panel-heading bg-primary">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="show-hide-list"><sc:text field="ProviderTitle" runat="server"/></a>
												</h4>
											</div>
											<div id="collapse1" class="panel-collapse collapse in">
												<div class="panel-body"> 
													<div class="row margin-row">
														<div class="col-md-12 no-padding">
															<div class="col-md-12 no-padding">
                                                                <%-- %><asp:dropdownlist ID="ddlProvider" runat="server" AppendDataBoundItems="false" OnSelectedIndexChanged="ddlProvider_SelectedIndexChanged">
                                                                    <asp:ListItem Text="--Select Provider--" Value="" />
                                                                    <%--<asp:ListITem Text="JACTRAVEL" Value="" />
                                                                </asp:dropdownlist>--%>
																<select class="form-control font-size-12" >
																  <option>JACTRAVEL</option>
																  <option>JACTRAVEL</option>
																  <option>JACTRAVEL</option>
																  <option>JACTRAVEL</option>
																</select>
															</div>
															<div class="col-md-12 no-padding"><span class="font-size-12"><sc:text field="SelectProviderMessage" runat="server"/></span></div>
														</div>
													</div>												
												</div>
											</div> 
										</div>    
									</div>
								</div>
								<div class="accordian-outer">
									<div class="panel-group" id="accordion">
										<div class="panel panel-default">
											<div class="panel-heading bg-primary">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="show-hide-list"><sc:text field="ClientTitle" runat="server"/></a>
												</h4>
											</div>
											<div id="collapse2" class="panel-collapse collapse in">
												<div class="panel-body"> 
													<div class="row margin-row">
														<div class="col-md-12 no-padding">
															<div class="col-md-12 no-padding">
																<select class="form-control font-size-12" >
																  <option>Loyaltybuild</option>
																  <option>Coop</option>
																  <option>Super value</option>
																  <option>Loyaltybuild</option>
																</select>
															</div>
															<div class="col-md-12 no-padding"><span class="font-size-12"><sc:text field="SelectClientMessage" runat="server" /></span></div>
														</div>
													</div>												
												</div>
											</div> 
										</div>    
									</div>
								</div>
							</div>
							<div class="col-sm-9">
								<div class="accordian-outer">
									<div class="panel-group" id="accordion">
										<div class="panel panel-default">
											<div class="panel-heading bg-primary">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion" href="#collapse3" class="show-hide-list"><sc:text field="TurnOnOffMessage" runat="server" /></a>
												</h4>
											</div>
											<div id="collapse3" class="panel-collapse collapse in">
												<div class="panel-body"> 
													<div class="row margin-row">
														<div class="col-sm-12 no-padding">   
															<div class="table-responsive table-bordered">
                                                                
                                                                <table id="myTable" class="table table-striped dt-responsive nowrap" runat="server">
																	<thead>
																		<tr>
																			<th><sc:text field="NameText" runat="server" /></th>
																			<th><sc:text field="RegionText" runat="server" /></th>
																			<th><sc:text field="CountryText" runat="server" /></th> 
																			<th><sc:text field="StatusText" runat="server" /></th>
																		</tr>
																	</thead>
																	<tbody>
																		<tr>
																			<td><span>Britannia Manchester Airport‎</span></td>
																			<td><span>England</span></td>
																			<td><span>Manchester</span></td>
																			<td>
																				<button class="bedbank-btn" type="submit" onclick="javascript:">
																					<span class="glyphicon glyphicon-ok-sign Gicon"></span>
																				</button>
																			</td>
																		</tr>
																		<tr>
																			<td><span>Britannia Country House‎</span></td>
																			<td><span>England</span></td>
																			<td><span>Manchester</span></td>
																			<td>
																				<button class="bedbank-btn" type="submit">
																					<span class="glyphicon glyphicon-ok-sign Gicon"></span>
																				</button>
																			</td>
																		</tr>
																		<tr>
																			<td><span>Britannia Country City Center</span></td>
																			<td><span>England</span></td>
																			<td><span>Manchester</span></td>
																			<td>
																				<button class="bedbank-btn" type="submit">
																					<span class="glyphicon glyphicon-remove-sign Ricon"></span>
																				</button>
																			</td>
																		</tr>
																		<tr>
																			<td><span>Sacha</span></td>
																			<td><span>England</span></td>
																			<td><span>Manchester</span></td>
																			<td>
																				<button class="bedbank-btn" type="submit">
																					<span class="glyphicon glyphicon-ok-sign Gicon"></span>
																				</button>
																			</td>
																		</tr>
																		<tr>
																			<td><span>Britannia Country House‎</span></td>
																			<td><span>England</span></td>
																			<td><span>Manchester</span></td>
																			<td>
																				<button class="bedbank-btn" type="submit">
																					<span class="glyphicon glyphicon-remove-sign Ricon"></span>
																				</button>
																			</td>
																		</tr>
																		<tr>
																			<td><span>Britannia Country City Center</span></td>
																			<td><span>England</span></td>
																			<td><span>Manchester</span></td>
																			<td>
																				<button class="bedbank-btn" type="submit">
																					<span class="glyphicon glyphicon-ok-sign Gicon"></span>
																				</button>
																			</td>
																		</tr>
																		<tr>
																			<td><span>Sacha</span></td>
																			<td><span>England</span></td>
																			<td><span>Manchester</span></td>
																			<td>
																				<button class="bedbank-btn" type="submit">
																					<span class="glyphicon glyphicon-ok-sign Gicon"></span>
																				</button>
																			</td>
																		</tr>
<tr>
																			<td><span>Britannia Manchester Airport‎</span></td>
																			<td><span>England</span></td>
																			<td><span>Manchester</span></td>
																			<td>
																				<button class="bedbank-btn" type="submit">
																					<span class="glyphicon glyphicon-remove-sign Ricon"></span>
																				</button>
																			</td>
																		</tr>
																		<tr>
																			<td><span>Britannia Country House‎</span></td>
																			<td><span>England</span></td>
																			<td><span>Manchester</span></td>
																			<td>
																				<button class="bedbank-btn" type="submit">
																					<span class="glyphicon glyphicon-ok-sign Gicon"></span>
																				</button>
																			</td>
																		</tr>
																		<tr>
																			<td><span>Britannia Country City Center</span></td>
																			<td><span>England</span></td>
																			<td><span>Manchester</span></td>
																			<td>
																				<button class="bedbank-btn" type="submit">
																					<span class="glyphicon glyphicon-ok-sign Gicon"></span>
																				</button>
																			</td>
																		</tr>
																		<tr>
																			<td><span>Sacha</span></td>
																			<td><span>England</span></td>
																			<td><span>Manchester</span></td>
																			<td>
																				<button class="bedbank-btn" type="submit">
																					<span class="glyphicon glyphicon-ok-sign Gicon"></span>
																				</button>
																			</td>
																		</tr>
																		<tr>
																			<td><span>Britannia Country House‎</span></td>
																			<td><span>England</span></td>
																			<td><span>Manchester</span></td>
																			<td>
																				<button class="bedbank-btn" type="submit">
																					<span class="glyphicon glyphicon-ok-sign Gicon"></span>
																				</button>
																			</td>
																		</tr>
																		<tr>
																			<td><span>Britannia Country City Center</span></td>
																			<td><span>England</span></td>
																			<td><span>Manchester</span></td>
																			<td>
																				<button class="bedbank-btn" type="submit">
																					<span class="glyphicon glyphicon-ok-sign Gicon"></span>
																				</button>
																			</td>
																		</tr>
																		<tr>
																			<td><span>Sacha</span></td>
																			<td><span>England</span></td>
																			<td><span>Manchester</span></td>
																			<td>
																				<button class="bedbank-btn" type="submit">
																					<span class="glyphicon glyphicon-ok-sign Gicon"></span>
																				</button>
																			</td>
																		</tr>																		
																	</tbody>
																</table>
															</div>	
														</div>
													</div>
												</div>
											</div> 
										</div>    
									</div>
								</div>
							</div> 
						</div>
					<%--</div>
				</div>
				</div>
			</div>
		</div>
	</div>--%>
      <!-- Site footer -->
        <%-- <div class="container-outer-inner width-full copyright-bar">
            <div class="container">
               <div class="copyright-text">
                  <p> &copy; 2016 <a href="#">Loyaltybuild</a>. All rights reserved.</p>
               </div>
            </div>
         </div>--%>
     
	<!--<script src="scripts/jquery-1.10.2.js"></script>
		<script src="scripts/jquery.min.js"></script>-->
		
		<%--<script src="scripts/jquery-1.12.3.js"></script>
		<script src="scripts/jquery.dataTables.min.js"></script>
		<script src="bootstrap_assets/javascripts/bootstrap.min.js"></script> --%>

<%--   </body>
</html>--%>
<script>
    
</script>