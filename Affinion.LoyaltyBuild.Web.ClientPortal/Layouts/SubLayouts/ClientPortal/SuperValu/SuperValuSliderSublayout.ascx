﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SuperValuSliderSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.SuperValuSliderSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Sitecore.Data.Items" %>

<link href="/Resources/styles/jquery.bxslider.css" rel="stylesheet" /> 

<script src="/Resources/scripts/jquery.bxslider.min.js"></script> 
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('.bxslider').bxSlider({ auto: true, autoControls: true });
    });
</script> 

<p id="PlaceholderText" runat="server">Set the datasource: Required Fields: DesktopImage</p>

<asp:Repeater ID="ImageSlider" runat="server">

    <HeaderTemplate>

        <div class="width-full">
            <div class="row slider margin-common">
                <div class="col-md-12">
                    <div class="slider-top">
                        <div id="bb1" class="LBranding">
                            <ul class="bxslider">
    </HeaderTemplate>

    <ItemTemplate>
                                <li>
                                    <sc:Image Field="DesktopImage" CssClass="img-responsive img-enlarge"  Item="<%#(Item)Container.DataItem %>" runat="server" />
                                </li>
    </ItemTemplate>

    <FooterTemplate>
                            </ul>
                        <div class="container container-inner-relative">
                        </div>
                        </div>
                     </div>
                </div>
            </div>
        </div>

    </FooterTemplate>

</asp:Repeater>
