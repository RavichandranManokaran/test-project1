﻿namespace Affinion.LoyaltyBuild.Web.Tests.Layouts.SubLayouts
{
    using Affinion.LoyaltyBuild.Communications;
    using Affinion.LoyaltyBuild.Communications.SmsService;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public partial class SmsSublayout : System.Web.UI.UserControl
    {
        private void Page_Load(object sender, EventArgs e)
        {
            LabelResponse.Text = "";
            TextBoxMessage.Text = "";
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            try
            {
                IMessageService smsService = new TelesignMobileSmsService();

                //TelesignMobileSmsServeice smsService1 = TelesignMobileSmsServeice.Instance;

                string number = "947731245698";

                ResponseStatus stat = smsService.Send(number, TextBoxMessage.Text);
                var r =  smsService.GetErrors().Select(err => err.Error);
                //string response1 = smsService1.sendSmsHttp(numbers, "Test Massage1");
                LabelResponse.Text = number + " | " + stat + "<br/>" + String.Join("<br/>", r.ToArray());
            }
            catch (Exception ex)
            {
                LabelResponse.Text = ex.Message;
            }
        }

        protected void ButtonMulti10_Click(object sender, EventArgs e)
        {
            try
            {
                IMessageService smsService = new TelesignMobileSmsService();

                List<string> numbers = new List<string>();

                //TelesignMobileSmsServeice smsService1 = TelesignMobileSmsServeice.Instance;

                numbers.Add("947731245691");
                numbers.Add("947731245692");
                numbers.Add("947731245693");
                numbers.Add("947731245694");
                numbers.Add("947731245695");
                numbers.Add("947731245696");
                numbers.Add("947731245697");
                numbers.Add("947731245698");
                numbers.Add("947731245699");
                numbers.Add("947731245610");

                ResponseStatus stat = smsService.Send(numbers, TextBoxMessage.Text);
                //string response1 = smsService1.sendSmsHttp(numbers, "Test Massage1");
                var r = smsService.GetErrors().Select(err => err.Error);
                LabelResponse.Text = stat.ToString() + "<br/>" + String.Join("<br/>", r.ToArray());
            }
            catch (Exception ex)
            {
                LabelResponse.Text = ex.Message;
            }
        }

        protected void ButtonMulti11_Click(object sender, EventArgs e)
        {
            try
            {
                IMessageService smsService = new TelesignMobileSmsService();

                List<string> numbers = new List<string>();

                //TelesignMobileSmsServeice smsService1 = TelesignMobileSmsServeice.Instance;

                numbers.Add("947731245691");
                numbers.Add("947731245692");
                numbers.Add("947731245693");
                numbers.Add("947731245694");
                numbers.Add("947731245695");
                numbers.Add("947731245696");
                numbers.Add("947731245697");
                numbers.Add("947731245698");
                numbers.Add("947731245699");
                numbers.Add("947731245610");
                numbers.Add("94773124561");
                ResponseStatus stat = smsService.Send(numbers, TextBoxMessage.Text);
                //string response1 = smsService1.sendSmsHttp(numbers, "Test Massage1");
                var r = smsService.GetErrors().Select(err => err.Error);
                LabelResponse.Text = stat.ToString() + "<br/>" + String.Join("<br/>", r.ToArray());
            }
            catch (Exception ex)
            {
                LabelResponse.Text = ex.Message;
            }
        }
    }
}