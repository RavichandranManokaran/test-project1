﻿using Affinion.LoyaltyBuild.Api.Booking.Data;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.UCom.Common.Constants;
using Sitecore.Data.Items;
using Sitecore.Security.Accounts;
using Sitecore.SecurityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Api.Booking.Helper
{
    static class RuleHelper
    {
        /// <summary>
        /// Get payment method
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="portalType"></param>
        /// <param name="providerType"></param>
        /// <param name="paymentType"></param>
        /// <param name="campaignItemId"></param>
        /// <returns></returns>
        public static string GetPaymentMethod(string clientId, string portalType, string providerType, string paymentType, int? campaignItemId = null)
        {
            var rules = GetPaymentRules(clientId);

            //return default value
            return GetPaymentMethod(rules, portalType, providerType, paymentType, campaignItemId);
        }

        /// <summary>
        /// Get payment method
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="portalType"></param>
        /// <param name="providerType"></param>
        /// <param name="paymentType"></param>
        /// <param name="campaignItemId"></param>
        /// <returns></returns>
        public static string GetPaymentMethod(List<PaymentRule> rules, string portalType, string providerType, string paymentType, int? campaignItemId = null)
        {
            string offerPaymentMethod = string.Empty;

            //Check payment method at campaign level
            if (campaignItemId != null)
            {
                var property = UCommerce.EntitiesV2.CampaignItemProperty.FirstOrDefault(i => i.CampaignItem.CampaignItemId == campaignItemId.Value && i.DefinitionField.Name == DefinationConstant.PaymentMethod);
                if (property != null)
                {
                    offerPaymentMethod = property.Value;
                }
            }

            //find matching payment method
            foreach (var rule in rules)
            {
                var match = (string.IsNullOrEmpty(rule.PaymentType) || rule.PaymentType == paymentType) &&
                    (string.IsNullOrEmpty(rule.Portal) || rule.Portal == portalType) &&
                    (string.IsNullOrEmpty(rule.ProviderType) || rule.ProviderType == providerType);

                if (match)
                    return (!rule.OfferOverrides || string.IsNullOrEmpty(offerPaymentMethod)) ? rule.PaymentMethod : offerPaymentMethod;

            }

            //return default value
            return string.Empty;
        }
        
        /// <summary>
        /// Gets the Payment Rule Information 
        /// </summary>
        /// <param name="Clientid">Clientid</param>
        /// <returns>Object of PaymentRuleModel list</returns>
        public static List<PaymentRule> GetPaymentRules(string clientId)
        {
            List<PaymentRule> listPaymentRule = new List<PaymentRule>();

            using (new SecurityDisabler())
            {
                if (!string.IsNullOrEmpty(clientId))
                {
                    Item clientItem = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(clientId));
                    if (clientItem != null)
                    {

                        var childItems = clientItem.Axes.GetDescendants().Where(x => x.TemplateID.ToString().Equals("{DF848F88-81B3-4646-B931-5404C25BACAA}", StringComparison.InvariantCultureIgnoreCase)).ToList();
                        if (childItems != null)
                        {
                            foreach (Item childItem in childItems)
                            {
                                PaymentRule paymentRule = new PaymentRule();
                                paymentRule.ProviderType = SitecoreFieldsHelper.GetDropLinkFieldValue(childItem, "Provider Type", "__display name");
                                paymentRule.Portal = SitecoreFieldsHelper.GetDropLinkFieldValue(childItem, "Portal", "__display name");
                                paymentRule.PaymentType = SitecoreFieldsHelper.GetDropLinkFieldValue(childItem, "Payment Type", "__display name");
                                paymentRule.PaymentMethod = SitecoreFieldsHelper.GetDropLinkFieldValue(childItem, "Payment Method", "__display name");
                                paymentRule.OfferOverrides = SitecoreFieldsHelper.CheckBoxChecked(childItem, "OfferOverrides");

                                listPaymentRule.Add(paymentRule);
                            }
                            return listPaymentRule;
                        }
                    }
                }
                return null;
            }
        }

    }
}
