﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddPollSubLayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.Test.Layouts.SubLayouts.ProviderSetup.PollingManager.AddPollSubLayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Sitecore.Data.Items" %>

<sc:Text Field="Question" runat="server" />
<div id="answerGeneraterRepeaterDiv" runat="server">
    <asp:Repeater ID="answerGenerator" runat="server">
        <ItemTemplate>
            <input type="radio" name="voteRadioButton" value="<%#((Item)Container.DataItem).ID.ToGuid().ToString() %>" />
            <sc:Text Field="Answer" Item="<%#(Item)Container.DataItem %>" runat="server" />
            <sc:Text Field="UserDetails" Item="<%#(Item)Container.DataItem %>" runat="server" />
        </ItemTemplate>
    </asp:Repeater>
    <asp:Button ID="pollSubmitButton" runat="server" Text="Submit" OnClientClick="OnSubmitButtonClick()" OnClick="SubmitButton_Click" />
</div>








