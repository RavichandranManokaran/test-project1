﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           IEmailService.cs
/// Sub-system/Module:     Communications(VS project name)
/// Description:           Email Sevice Interface
/// </summary>

using System.Collections.Generic;
using System.IO;
using System.Net.Mail;

namespace Affinion.LoyaltyBuild.Communications
{
    public interface IEmailService
    {
        /// <summary>
        /// Email Send 
        /// </summary>
        /// <param name="from">From</param>
        /// <param name="to">To</param>
        /// <param name="subject">Subject</param>
        /// <param name="message">Email Body</param>
        /// <param name="isHtml">Is body Html</param>
        void Send(string from, string to, string subject, string message, bool isHtml);

        /// <summary>
        /// Email send
        /// </summary>
        /// <param name="from">From</param>
        /// <param name="to">To</param>
        /// <param name="bcc">Cc list</param>
        /// <param name="subject">Subject</param>
        /// <param name="message">Email Body</param>
        /// <param name="isHtml">Is body Html</param>
        void Send(string from, string to, IList<string> cc, string subject, string message, bool isHtml);

        /// <summary>
        /// Email Send
        /// </summary>
        /// <param name="mail">MailMessage</param>
        void Send(MailMessage mail);

        /// <summary>
        /// Email send
        /// </summary>
        /// <param name="from">From</param>
        /// <param name="to">To</param>
        /// <param name="subject">Subject</param>
        /// <param name="message">Email Body</param>
        /// <param name="isHtml">Is body Html</param>
        /// <param name="stream">Stream</param>
        /// <param name="filename">fileName</param>
        /// <param name="mimetype">mimetype</param>
        void Send(string from, string to, string subject, string message, bool isHtml, Stream stream, string filename, string mimetype);

        /// <summary>
        /// Send Email With CC And Attachment
        /// </summary>
        /// <param name="from">From</param>
        /// <param name="to">To</param>
        /// <param name="cc">CC</param>
        /// <param name="subject">Mail Subject</param>
        /// <param name="message">Mail Message</param>
        /// <param name="isHtml">Is body HTML</param>
        /// <param name="stream">Stream</param>
        /// <param name="filename">FileName</param>
        /// <param name="mimetype">mimetype</param>
        void Send(string from, string to, IList<string> cc, string subject, string message, bool isHtml, Stream stream, string filename, string mimetype);

        /// <summary>
        /// Send Email With Multiple ToAddress
        /// </summary>
        /// <param name="from">From</param>
        /// <param name="to">To</param>       
        /// <param name="subject">Mail Subject</param>       
        /// <param name="isHtml">Is body HTML</param>
        void Send(string from, IList<string> to, string subject, bool isHtml);

    }
}
