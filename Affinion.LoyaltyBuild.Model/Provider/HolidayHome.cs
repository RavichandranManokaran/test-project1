﻿using Affinion.LoyaltyBuild.Model.Product;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Model.Provider
{
    /// <summary>
    /// Holiday home model
    /// </summary>
    public class HolidayHome : IProvider, ISitecoreProvider , IGenericProvider
    {
        /// <summary>
        /// Provider Id
        /// </summary>
        public string ProviderId { get; set; }

        /// <summary>
        /// SitecoreItemId
        /// </summary>
        public string SitecoreItemId { get; set; }

        /// <summary>
        /// provider type
        /// </summary>
        public ProviderType ProviderType { get; set; }

        /// <summary>
        /// Sitecore item for that provider
        /// </summary>
        public Item SitecoreItem { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Star Ranking
        /// </summary>
        public int StarRanking { get; set; }
        
        /// <summary>
        /// Address
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Facilities
        /// </summary>
        public List<Item> Facilities { get; set; }

        /// <summary>
        /// List of products for that provider
        /// </summary>
        public IList<IProduct> Products { get; set; }

        /// <summary>
        /// Loyalty hotel type
        /// </summary>
        public string HotelType { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Address
        /// </summary>
        public string Phone { get; set; }
    }
}
