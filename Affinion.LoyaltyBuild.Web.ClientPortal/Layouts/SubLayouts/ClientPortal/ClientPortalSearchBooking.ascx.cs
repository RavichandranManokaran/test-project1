﻿

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
    using Sitecore.Data.Items;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Order;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Email;
    using Affinion.LoyaltyBuild.Common;
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.Loyaltybuild.BusinessLogic.Helper;

    public partial class ClientPortalSearchBooking : BaseSublayout
    {
        

        protected void Page_Load(object sender, EventArgs e)
        {
            this.BindSitecoreTexts();
            //this.PopulateStaticText();
        }

        //protected void calCheckinDate_SelectionChanged(object sender, EventArgs e)
        //{
        //    txtCheckInDate.Text = calCheckinDate.SelectedDate.ToShortDateString();
        //    calCheckinDate.Visible = !calCheckinDate.Visible;
        //}

        //protected void btnShowCalnedar_Click(object sender, EventArgs e)
        //{
        //    calCheckinDate.Visible = !calCheckinDate.Visible;
        //}

        //private void PopulateStaticText()
        //{
        //    Item currentItem = Sitecore.Context.Item;
        //    SetSitecoreTextInLiteral(currentItem, litHeading, headingFieldName);
        //    SetSitecoreTextInLiteral(currentItem, litSubHeading, subHeadeingFieldName);
        //    SetSitecoreTextInLiteral(currentItem, litBookingRefNo, bookingRefNoFieldName);
        //    SetSitecoreTextInLiteral(currentItem, litName, customerNameFieldName);
        //    SetSitecoreTextInLiteral(currentItem, litCheckIn, checkinDateFieldName);
        //    btnSubmit.Text = (currentItem != null && currentItem.Fields[submitButtonFieldName] != null && !string.IsNullOrWhiteSpace(currentItem.Fields[submitButtonFieldName].Value)) ? currentItem.Fields[submitButtonFieldName].Value : string.Empty;
        //}

        private void SetSitecoreTextInLiteral(Item currentItem, Literal literalControl, string fieldName)
        {
            literalControl.Text = (currentItem != null && currentItem.Fields[fieldName] != null && !string.IsNullOrWhiteSpace(currentItem.Fields[fieldName].Value)) ? currentItem.Fields[fieldName].Value : string.Empty;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            //calCheckinDate.Visible = false;
            string bookingReferenceNumber = txtReferenceNumber.Text;
            string customerName = txtName.Text;
            DateTime checkinDate = Convert.ToDateTime(txtCheckInDate.Text);
            int orderLineId = uCommerceConnectionFactory.SearchBooking(bookingReferenceNumber, customerName, checkinDate);
            
            if (orderLineId != 0)
            {
                Response.Redirect("/searchbooking?olid=" + orderLineId);

            }
            else
            {
                //lblError.Visible = true;
                Response.Redirect("/searchbooking");
                errorMessageWrapper.Visible = true;
            }
        }
        protected void btnRetrive_Click(object sender, EventArgs e)
        {
            OrderService order = ContainerFactory.Instance.GetInstance<OrderService>();
            //MailService managedetails = new MailService(Constants.HardCodedClientId);
            var client = this.GetClientId();

            MailService managedetails = new MailService(client);
           
            string email = txtEmail.Text;
            string lastName = txtLastName.Text;
            List<int> orderlineIds = order.GetBookingEmailAddress(email, lastName);
            if (orderlineIds != null)
            {
                foreach (int orderlineId in orderlineIds)
                {
                    managedetails.GenerateEmail(MailTypes.ManageBookingMailConfirmation, orderlineId);
                    //mailSentMessage.Visible = true;  
                    emailMessageWrapper.Visible = true;
                }
            }
           
        }

        private void BindSitecoreTexts()
        {
            Item conditionalMessageItem = this.GetItemFromRenderingParameterPath("Conditional Messages Link");
            string requiredFieldMessageFieldName = "Required Field Message";
            string checkinDateMessageFieldName = "Check in Date Message";
            string emailisRequiredMessageFieldName = "Email is Required Message";
            string bookingIDValidationMessageFieldName = "BookingID Validation Message";
            string emailValidationMessageFieldName = "Email Validation Message";
            string bookingsnotFoundMessageFieldName = "Bookings not Found Message";
            string retrieveDetailstoEmailMessageFieldName = "Retrieve Details to Email Message";
            string notValidCharacterMessageFieldName = "Not Valid Character Message";
            
            SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, requiredFieldMessageFieldName, txtReferenceNumberRequiredText);
            SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, bookingIDValidationMessageFieldName, txtReferenceNumberRegularText);
            SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, requiredFieldMessageFieldName, txtNameRequiredText);
            SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, checkinDateMessageFieldName, txtCheckInDateRequiredText);
            SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, emailisRequiredMessageFieldName, txtEmailRequiredText);
            SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, requiredFieldMessageFieldName, txtLastNameRequiredText);
            SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, emailValidationMessageFieldName, txtEmailRegularText);
            SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, bookingsnotFoundMessageFieldName, txtErrorText);
            SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, retrieveDetailstoEmailMessageFieldName, txtMailSentMessageText);
            SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, notValidCharacterMessageFieldName, txtNameRegularText);
            SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, notValidCharacterMessageFieldName, txtLastNameRegularText);

        }


        /// <summary>
        /// Get the ClientId
        /// </summary>
        private string GetClientId()
        {
            Item clientItem = SitecoreFieldsHelper.GetLookupFieldTargetItem(ItemHelper.RootItem, Constants.ClientDetailsFieldName);
            return clientItem.ID.Guid.ToString();
        }

    }
}