﻿<%@ page language="C#" autoeventwireup="true" codebehind="ReportPrimaryLayout.aspx.cs" inherits="Affinion.LoyaltyBuild.AdminPortal.Layouts.PrimaryLayouts.Reports.ReportPrimaryLayout" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Wel Come</title>


    <link rel="stylesheet" href="/ResourcesAdmin/styles/responsiveNav.css" type="text/css" />
    <link rel="stylesheet" href="/ResourcesAdmin/styles/jquery-ui.css" />
    <link rel="stylesheet" href="/ResourcesAdmin/styles/affinionAdminPortal.min.css" />
   
    <link  href="/ResourcesAdmin/styles/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link  href="/ResourcesAdmin/styles/bootstrap-theme.css" rel="stylesheet" type="text/css" /> 
    <link  href="/ResourcesAdmin/styles/bootstrap-theme.min.css" rel="stylesheet" type="text/css" /> 
    <link  href="/ResourcesAdmin/datePicker/jquery-ui.custom.min.css" rel="stylesheet" type="text/css" />
    <link  href="/ResourcesAdmin/styles/affinion-reportadmin.min.css" rel="stylesheet" type="text/css" />
    <link  href="/ResourcesAdmin/styles/affinion-report.css" rel="stylesheet" type="text/css" />
    <link  href="/ResourcesAdmin/styles/themereport.css" rel="stylesheet" type="text/css" />
    <link href="/ResourcesAdmin/styles/CustomeStyle.css" rel="stylesheet" />

    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
    <link href="/ResourcesAdmin/styles/daterangepicker.css" rel="stylesheet" />
   
    <script type="text/javascript" src="/ResourcesAdmin/scripts/jquery.min.js"></script>
   <%-- <script type="text/javascript" src="/ResourcesAdmin/scripts/validate/jquery.validate.min.js"></script>--%>
    <%--<script type="text/javascript" src="/ResourcesAdmin/scripts/validate/additional-methods.min.js"></script>--%>
    <script type="text/javascript" src="/ResourcesAdmin/bootstrap_assets/javascripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="/ResourcesAdmin/scripts/jquery-ui.js"></script>
    <script type="text/javascript" src="/ResourcesAdmin/scripts/affinionCustom.js"></script>
    <script type="text/javascript" src="/ResourcesAdmin/scripts/affinion-framework.js"></script>
    <script type="text/javascript" src="/ResourcesAdmin/scripts/affinion-plugins.js"></script>
    <script type="text/javascript" src="/ResourcesAdmin/scripts/html5shiv.js"></script>

  <%--  <script type="text/javascript" src="scripts/script.js"></script>--%>
  <%--  <script type="text/javascript" src="scripts/fotorama.js"></script>--%>
  <%-- <script type="text/javascript" src="scripts/images.js"></script>--%>
  <%--  <script type="text/javascript" src="scripts/jssor.slider-20.mini.js"></script>--%>
  <%-- <script type="text/javascript" src="scripts/detail-gallary.js"></script>--%>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  
    

    <!--Language Links Datepicker-->
    <script src="/ResourcesAdmin/scripts/languages/datepicker-zh-CN.js" type="text/javascript"></script>
    <script src="/ResourcesAdmin/scripts/languages/datepicker-fr.js" type="text/javascript"></script>
    <script src="/ResourcesAdmin/scripts/languages/datepicker-en-US.js" type="text/javascript"></script>
    <!-- ================================================== -->
    <script src="/ResourcesAdmin/scripts/rs-navigation.js" type="text/javaScript"></script>
    <script src="../../../ResourcesAdmin/scripts/moment.js"></script>
    <script src="../../../ResourcesAdmin/scripts/daterangepicker.js"></script>
</head>
<body class="cbp-spmenu-push">
    <form  action="/" method="post" runat="server" id="form1">
    <div class="container-outer width-full overflow-hidden-att app-bg">
        <div class="top-header-login width-full">

            <div class="row">
                <div class="col-md-12 header-bg">
                    <div class="sup-login-layout-inner">
                        <div class="col-md-2 no-padding">
                            <div class="login-header">
                              <%--  <span class="glyphicon glyphicon-th pull-left hmenu_icn"></span>--%>
                                <img src="../../../ResourcesAdmin/images/LoyaltybuildLogo.png" alt="LoyalityBuild" title="LoyalityBuild" class="pull-left img-responsive">
                            </div>
                        </div>
                       <%-- <div class="col-md-10 no-padding">
                            <div class="header-right">--%>
                               <%-- <div class="col-md-12 right-header-top">
                                    <div>
                                        <div class="col-md-8">
                                        </div>
                                        <div class="col-md-4">
                                            <div class="col-md-6">
                                                <div class="date-time">
                                                    <!--<<p class="no-margin">Thu, 27 Aug 2015 10:00:27</p>-->
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="top-hader-logo">
                                                     
                                                    <!--<a title="" href="#"><img class="pull-left img-responsive" title="" alt="logo" src="images/webpublish_brand_logo.gif"></a>-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>--%>
                                <div class="col-md-12 right-header-middle">

                                    <div class="col-md-7"></div>
                                    <div class=" float-right">
                                        <!-- <div class="logoff">
                                        <ul>
                                        <li>Welcome</li>
                                         <li>John David</li>
                                          <li><button type="submit" class="btn btn-primary btn-xs">Logout</button>  </li>  
                                        </ul>
                                         </div>   -->
                                    </div>

                                </div>
                                <div class="col-md-12 right-header-bottom">
                                    <div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="nav-container">

                <!-- Global Navbar -->
                <div class="navbar-wrapper">
                    <sc:placeholder id="topbar" runat="server" key="topbar" />
                    <%--<div class="">--%>

                        <%--<!<div class="navbar navbar-default navbar-fixed-top" role="navigation">
                            <span class="pull-left rs-topleft-links" id="rs-topleft-links">

                                
                                <!--<span class="rs-topleft-links-home">
            <a class="rs-right-border" tabindex="1" href="#">Home</a>          
            <a class="" href="#" tabindex="2">Contact</a>
          </span>-->
                            </span>

                            <div>
                                <div class="navbar-header">
                                    <button id="rs-navtrigger" type="button" class="navbar-toggle collapsed" data-toggle="collapse">
                                        <span class="rs-only"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>
                                <div id="rs-nav-Main" tabindex="3">

                                    <ul class="nav navbar-nav navbar-right" id="rs-nav-user">
                                        <li class="dropdown-right rs-margin-2">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="label label-primary">Welcome</span> <span class="rs-font-black">John</span> <b class="caret rs-caret"></b></a>
                                            <ul class="dropdown-menu">

                                                <li><a href="#">My Profile</a>
                                                </li>
                                                <li><a href="#">Logoff</a>
                                                </li>

                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>--%>
                    <%--</div>--%>
                </div>

                <!-- //End Of Global Navbar -->

                <!-- Logo And Search -->
                <div class="row">

                    <div class="col-md-4 rs-nav-master-icon-group">
                        <div class="row">
                            <div class="rs-res-lang">
                                <div class="col-xs-10 col-md-offset-5">
                                    <div class="col-md-4">
                                        <div>
                                            <span class="rs-social-icons-holder"><span class="rs-social-icons">
                                                <a href="#" target="_blank" class="rs-icon rs-social-icon-twitter"></a>
                                                <a href="#" target="_blank" class="rs-icon rs-social-icon-youtube"></a>
                                            </span>

                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row rs-onresponsive-center">
                        </div>
                    </div>
                </div>

                <!--// End of Logo And Search -->

                <!-- Second NavBar -->

                <!-- Secondary Navbar -->

                <%--<div class="navbar navbar-default" role="navigation" id="rs-res-nav">
                    <div class="container-fluid rs-noPadding-left-right">
                        <div class="navbar-header">
                        </div>--%>
                       <%-- <div class="navbar-collapse collapse in ty-navbar">
                            <ul class="nav navbar-nav rs-taylor-nav2  rs-nav-back">
                                <li class="dropdown"><a href="#" class="active">Reports <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Arrivals Report</a></li>
                                        <li><a href="#">Availability Report</a></li>
                                        <li><a href="#">Restricted Rooms Report</a></li>
                                        <li><a href="#">Revenue Report</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>--%>
                        <!--/.nav-collapse -->
                   <%-- </div>--%>
                    <!--/.container-fluid -->
                </div>
            </div>

        </div>

        <div class="container-outer width-full overflow-hidden-att app-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                          <sc:placeholder id="maincontentadminportal" runat="server" key="main-contentadmin" />
                    </div>
                </div>
            </div>
        </div>
        <!-- footer -->
        <div class="col-md-12 footer-client-top">
            <div class="footer-client col-md-12">
                <div class="footer-nav">
                    <ul>
                        <li><a href="#">Loyalitybuild Terms and Conditions</a></li>
                    <!--    <li><a href="#">User Manual</a></li>
                        <li><a href="#">Change Passwords</a></li> 
                        <li class="border-none"><a href="#">General Q & A</a></li> -->
                    </ul>
                </div>
            </div>
        </div>
    </form>
        <script type="text/javascript">
            /*
                var res_nav = document.querySelectorAll();

                setLanguage(getSelected);
                var dt = res_nav[0].dataset;

                var getSelected;
                var currentLanguage_db = "English";



                function setLanguage(getSelected) {

                    if (getSelected !== undefined) {

                        currentLanguage_db = getSelected;

                        $(res_nav[0]).text(currentLanguage_db);


                    } else {

                        $(res_nav[0]).text(currentLanguage_db);

                    }

                }

                setLanguage(currentLanguage_db);




                $('#lang-globe .dropdown-menu li a').click(function (e) {

                    var getSelected = $(this).text();
                    setLanguage(getSelected);

                });
                */
                /*Carousel: Init : data-ride='carousel':removed */
              //  $(".carousel").carousel();


          //  });
            
        </script>
</body>
</html>
