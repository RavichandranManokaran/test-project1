﻿/****** Object:  StoredProcedure [dbo].[LB_TransferBooking]    Script Date: 2/12/2016 3:06:06 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LB_TransferBooking]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[LB_TransferBooking]
GO

/****** Object:  StoredProcedure [dbo].[LB_TransferBooking]    Script Date: 2/12/2016 3:06:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LB_TransferBooking] 
	-- Add the parameters for the stored procedure here
	@Note NVARCHAR(MAX), 
	@OrderLineId INT,
	@NoOfAdults INT,
	@NoOfChildren INT,
	@ChildrenAgeInfo VARCHAR(50),
	@CartId VARCHAR(50),
	@CreatedBy VARCHAR(50),
	@NewOrderLineId INT OUT,
	@OrderReference VARCHAR(50)
AS
BEGIN
	
	--DECLARE @OrderReference VARCHAR(50)
	DECLARE @NewOrderId INT
	DECLARE @OrderId INT
	DECLARE @CustomerId INT

	SELECT @OrderId = PO.OrderId, @CustomerId = PO.CustomerId 
	FROM uCommerce_OrderLine OL
		INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId = PO.OrderId
	WHERE OrderLineId = @OrderLineId

	--get order reference
	--SELECT @OrderReference = ISNULL(BookingReferenceId,'') + '-' + CAST((ISNULL(LastSequence,0)+1) AS VARCHAR(5))
	--	FROM LB_CustomerAttribute
	--	WHERE CustomerId = @CustomerId
	--UPDATE LB_CustomerAttribute SET LastSequence = (ISNULL(LastSequence,0)+1) WHERE CustomerId = @CustomerId

	SELECT TOP 1  
		@NewOrderLineId =  RR.OrderLineId,
		@NewOrderId = PO.OrderId
	FROM
		LB_RoomReservation RR 
		INNER JOIN uCommerce_OrderLine OL ON RR.OrderLineId = OL.OrderLineId
		INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId = PO.OrderId
	WHERE PO.BasketId = @CartId
	
	--Update no of adults and child and order reference
	UPDATE LB_RoomReservation SET OrderReference = @OrderReference, NoOfAdult = @NoOfAdults, NoOfChild = @NoOfChildren WHERE OrderLineId = @NewOrderLineId


	-- Insert statements for procedure here	 
	INSERT INTO LB_ReservationNotes 
		(
		NoteTypeId,
		OrderLineId,
		Note,
		CreatedBy,
		CreatedDate
		) 
	VALUES 
		(
		1,
		@OrderLineId,
		@Note,
		@CreatedBy,
		GETDATE()
		);

	--Update Customer
	UPDATE uCommerce_PurchaseOrder SET CustomerId = @CustomerId WHERE OrderId = @NewOrderId

	--insert order address
	INSERT INTO [dbo].[LB_OrderAddress]
		([FirstName]
		,[LastName]
		,[EmailAddress]
		,[PhoneNumber]
		,[MobilePhoneNumber]
		,[Line1]
		,[Line2]
		,[PostalCode]
		,[City]
		,[State]
		,[CountryId]
		,[Attention]
		,[CompanyName]
		,[AddressName]
		,[OrderId]
		,[Line3]
		,[County]
		,[Branch]
		,[IsEmail]
		,[IsTerms])
	SELECT [FirstName]
		,[LastName]
		,[EmailAddress]
		,[PhoneNumber]
		,[MobilePhoneNumber]
		,[Line1]
		,[Line2]
		,[PostalCode]
		,[City]
		,[State]
		,[CountryId]
		,[Attention]
		,[CompanyName]
		,[AddressName]
		,@NewOrderId
		,[Line3]
		,[County]
		,[Branch]
		,[IsEmail]
		,[IsTerms]
	FROM [dbo].[LB_OrderAddress]
	WHERE OrderId = @OrderId

			
	UPDATE LB_RoomReservation 
	SET isCancelled = 1, NewOrderReference = @OrderReference 
	WHERE OrderLineId = @OrderlineId


	IF(@ChildrenAgeInfo IS NOT NULL and @ChildrenAgeInfo <> '')
	BEGIN
		DECLARE @xml as xml
		SET @xml = cast(('<X>'+replace(@ChildrenAgeInfo,',' ,'</X><X>')+'</X>') as xml)
		
		INSERT INTO [dbo].[LB_RoomReservationAgeInfo] ([RoomReservationId], [OrderLineId], [Age])
		SELECT RR.RoomReservationId, RR.OrderLineId, N.value('.', 'varchar(10)') as value FROM @xml.nodes('X') as T(n)
		CROSS JOIN LB_RoomReservation RR
		WHERE RR.OrderLineId = @NewOrderLineId
	END

	--Cancelling for existing OrderLineId
	EXEC [LB_UpdateRoomAvailability] @OrderLineId, 1

	--Adding for new OrderLineId
	EXEC [LB_UpdateRoomAvailability] @NewOrderLineId, 0

	UPDATE uCommerce_PurchaseOrder
	SET BasketId = '00000000-0000-0000-0000-000000000000'
	WHERE BasketId = @CartId
END
GO
