﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Affinion.LoyaltyBuild.Api.Booking.Data.Payment;
using Affinion.LoyaltyBuild.Api.Booking.Extension;
using System.Transactions;
using Affinion.LoyaltyBuild.Common;
using System.Data.SqlClient;
using Affinion.LoyaltyBuild.Model.Common;
/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/
namespace Affinion.LoyaltyBuild.Api.Booking.Service
{
    /// <summary>
    /// Service that provides methods to perform all Payment related actions
    /// </summary>
    public class PaymentService : IPaymentService
    {
        //private string
        private string _connectionString = "Ucommerce";
        
        /// <summary>
        /// Constructor
        /// </summary>
        public PaymentService()
        {
        }

        #region IPaymentService Implementation

        /// <summary>
        /// Gets the OrderlinePayment Information 
        /// </summary>
        /// <param name="orderLineId">orderLineId</param>
        /// <returns>Object of OrderLinePaymentInfo</returns>
        public OrderLinePaymentInfo GetOrderLinePaymentInfo(int orderLineId)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);

            var sql = "LB_GetPaymentHistory";

            //the result object
            OrderLinePaymentInfo result = null;

            //create command
            using (var command = db.GetStoredProcCommand(sql))
            {
                db.AddInParameter(command, "OrderLineId", DbType.Int32, orderLineId);

                //execute reader
                using (var reader = db.ExecuteReader(command))
                {
                    //get booking referece
                    if (reader.Read())
                    {
                        result = new OrderLinePaymentInfo
                        {
                            OrderLineId = orderLineId,
                            BookingReference = reader.Value<string>(1)
                        };

                        //get total due by currency
                        if (reader.NextResult())
                            result.DueByCurrencies = GetDueByCurrencies(reader).ToList();

                        //get due history
                        if (reader.NextResult())
                            result.DueHistory = GetDueHistory(reader).ToList();

                        //get payment history
                        if (reader.NextResult())
                            result.PaymentHistory = GetPaymentHistory(reader).ToList();

                        //get payment history
                        if (reader.NextResult())
                            result.DiscountHistory = GetDiscountHistory(reader).ToList();
                    }

                }
            }
            return result;
        }

        /// <summary>
        /// Gets the dues information 
        /// </summary>
        /// <param name="orderLineId">orderLineId</param>
        /// <param name="currencyTypeId">currencyTypeId</param>     
        public List<TotalDueItem> GetDuesByOrderlineID(int orderLineId, int? currencyTypeId)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "LB_GetDuesByOrderLineID";

            //the result object
            var result = new List<TotalDueItem>();

            //create command
            using (var command = db.GetStoredProcCommand(sql))
            {
                db.AddInParameter(command, "OrderLineId", DbType.Int32, orderLineId);
                command.Parameters.Add(new SqlParameter { SqlValue = currencyTypeId ?? (object)DBNull.Value, ParameterName = "CurrencyTypeId" });

                //execute reader
                using (var reader = db.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        var item = new TotalDueItem
                        {
                            Amount = reader.Value<decimal>(0),
                            CurrencyTypeId = reader.Value<int>(1),
                            OrderLineID = reader.Value<int>(2)
                        };
                        result.Add(item);
                    }
                }
            }
            return result;
        }


        /// <summary>
        /// Saves the due details
        /// </summary>
        /// <param name="dueDetails">dueDetails</param>
        /// <returns>ValidationResult object with success/failure flag</returns>
        public ValidationResult SaveDue(DueDetails dueDetails)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "LB_SaveOrderLineDue";

            //the result object
            var result = new ValidationResult();

            //create command
            
            //create command
            using (var scope = new TransactionScope())
            {
                try
                {
                    var command = db.GetStoredProcCommand(sql);
                    db.AddInParameter(command, "OrderLineDueId", DbType.Int32, dueDetails.OrderLineDueId);
                    db.AddInParameter(command, "OrderLineId", DbType.Int32, dueDetails.OrderLineId);
                    db.AddInParameter(command, "CurrencyTypeId", DbType.Int32, dueDetails.CurrencyTypeId);
                    db.AddInParameter(command, "PaymentMethodId", DbType.Int32, dueDetails.PaymentMethodId);
                    db.AddInParameter(command, "DueTypeId", DbType.Guid, dueDetails.DueTypeId);
                    db.AddInParameter(command, "TotalAmount", DbType.Decimal, dueDetails.Amount);
                    db.AddInParameter(command, "CommissionAmount", DbType.Decimal, dueDetails.CommissionAmount);
                    db.AddInParameter(command, "ProviderAmount", DbType.Decimal, dueDetails.ProviderAmount);
                    db.AddInParameter(command, "ProcessingFeeAmount", DbType.Decimal, dueDetails.ProcessingFeeAmount);
                    db.AddInParameter(command, "Note", DbType.String, dueDetails.Note);
                    db.AddInParameter(command, "CreatedDate", DbType.DateTime, DateTime.Now);
                    db.AddInParameter(command, "CreatedBy", DbType.String, dueDetails.CreatedBy);
                    //execute data
                    result.Id = db.ExecuteScalar(command).ToString();

                    var statusCommand = db.GetStoredProcCommand("LB_UpdateStatusOnDueAndPay");
                    db.AddInParameter(statusCommand, "OrderLineId", DbType.Int32, dueDetails.OrderLineId);
                    db.AddInParameter(statusCommand, "CreatedBy", DbType.String, dueDetails.CreatedBy);
                    db.ExecuteNonQuery(statusCommand);

                    result.IsSuccess = true;
                    result.Message = "Due details saved successfully";
                    scope.Complete();
                }
                catch
                {
                    result.Message = "Error saving due";
                }
            }
            

            //return result
            return result;
        }

        /// <summary>
        /// Deletes a due item with given DueId
        /// </summary>
        /// <param name="dueId">dueId</param>
        /// <returns>ValidationResult object with success/failure flag</returns>
        public ValidationResult DeleteDue(int dueId, string reason)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "LB_DeleteDue";

            //the result object
            var result = new ValidationResult();

            //create command
            using (var scope = new TransactionScope())
            {
                try
                {
                    var command = db.GetStoredProcCommand(sql);
                    db.AddInParameter(command, "OrderLineDueId", DbType.Int32, dueId);
                    db.AddInParameter(command, "Reason", DbType.String, reason);
                    db.ExecuteNonQuery(command);

                    result.IsSuccess = true;
                    result.Message = "Due deleted.";
                    scope.Complete();
                }
                catch
                {
                    result.Message = "Error deleting due";
                }
            }
            
            //return result
            return result;
        }


        /// <summary>
        /// Get the Due Details for the particular DueId
        /// </summary>
        /// <param name="dueId">dueId</param>
        /// <returns>DueDetails object</returns>
        public DueDetails GetDueDetail(int dueId)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            //var db = factory.Create(_connectionString);
            var sql = "LB_GetDueDetail";
            //the result object

            DueDetails result = null;

            //create command
            using (var command = db.GetStoredProcCommand(sql))
            {
               
                db.AddInParameter(command, "DueId", DbType.Int32, dueId);
                //execute reader

                using (var reader = db.ExecuteReader(command))
                {
                    if (reader.Read())
                    {
                        result = new DueDetails
                        {
                            OrderLineDueId = reader.Value<int>("OrderLineDueId"),
                            DueTypeId = reader.Value<Guid>("DueTypeId"),
                            CurrencyTypeId = reader.Value<int>("CurrencyTypeId"),
                            Amount = Math.Round(reader.Value<decimal>("TotalAmount"),2),
                            CommissionAmount = Math.Round(reader.Value<decimal>("CommissionAmount"),2),
                            ProviderAmount = Math.Round(reader.Value<decimal>("ProviderAmount"),2),
                             ProcessingFeeAmount=Math.Round(reader.Value<decimal>("ProcessingFeeAmount"),2),
                            PaymentMethodId=reader.Value<int>("PaymentMethodId"),
                            Note = reader.Value<string>("Note")

                        };
                    }

                }
            }

            //return result
            return result;
        }

        /// <summary>
        /// Add a payment 
        /// </summary>
        /// <param name="payment">payment</param>
        /// <returns>ValidationResult object with success/failure flag</returns>
        public ValidationResult AddPayment(PaymentDetail payment)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "LB_AddBookingPayment";

            //the result object
            var result = new ValidationResult();

            //create command
            using (var scope = new TransactionScope())
            {
                try
                {
                    var command = db.GetStoredProcCommand(sql);
                    db.AddInParameter(command, "OrderLineId", DbType.Int32, payment.OrderLineID);
                    db.AddInParameter(command, "CurrencyId", DbType.Int32, payment.CurrencyTypeId);
                    db.AddInParameter(command, "OrderPaymentId", DbType.Int32, payment.OrderLinePaymentID);
                    db.AddInParameter(command, "PaymentMethodId", DbType.Int32, payment.PaymentMethodID);
                    db.AddInParameter(command, "Amount", DbType.Decimal, payment.Amount);
                    db.AddInParameter(command, "ReferenceId", DbType.String, payment.ReferenceID);
                    db.AddInParameter(command, "CreatedBy", DbType.String, payment.CreatedBy);
                    if (payment is CardPaymentDetail)
                    {
                        db.AddInParameter(command, "CommissionAmount", DbType.Decimal, ((CardPaymentDetail)payment).CommissionAmount);
                        db.AddInParameter(command, "ProviderAmount", DbType.Decimal, ((CardPaymentDetail)payment).ProviderAmount);
                        db.AddInParameter(command, "ProcessingFee", DbType.Decimal, ((CardPaymentDetail)payment).ProcessingFee);
                    }
                    db.ExecuteNonQuery(command);
                    
                    var statusCommand = db.GetStoredProcCommand("LB_UpdateStatusOnDueAndPay");
                    db.AddInParameter(statusCommand, "OrderLineId", DbType.Int32, payment.OrderLineID);
                    db.AddInParameter(statusCommand, "CreatedBy", DbType.String, payment.CreatedBy);
                    db.ExecuteNonQuery(statusCommand);

                    result.IsSuccess = true;
                    result.Message = "Your payment is saved Successfully";
                    scope.Complete();
                }
                catch
                {
                    result.IsSuccess = false;
                    result.Message = "Error in saving payment";
                }
            }

            //return result
            return result;
        }

        /// <summary>
        /// Gets the list of payment methods
        /// </summary>
        /// <returns> List of Payment menthods</returns>
        public List<PaymentMethod> GetPaymentMethods(string clientId)
        {
            List<PaymentMethod> paymentInfo = new List<PaymentMethod>();
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "LB_GetPaymentMethodsByClientID";
           
            
            
            using (var command = db.GetStoredProcCommand(sql))
            {
                db.AddInParameter(command, "@ClientId", DbType.String, clientId);
                using (var reader = db.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        var tmpPaymentMethod = new PaymentMethod
                        {
                            Id = reader.GetInt32(0),
                            Name = reader.GetString(1)
                        };
                    paymentInfo.Add(tmpPaymentMethod);
                    }                       
                }
            } 
            return paymentInfo;
        }

        /// <summary>
        /// Gets the payment reasons
        /// </summary>
        /// <returns> List of Payment Reasons</returns>
        public List<PaymentReason> GetPaymentReasons()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "LB_GetPaymentReason";

            var res = new List<PaymentReason>();

            using (var command = db.GetStoredProcCommand(sql))
            {
                using (var reader = db.ExecuteReader(command))
                {
                    while (reader.Read())
                        res.Add(new PaymentReason
                            {
                                DueId = reader.GetGuid(0),
                                DueReason = reader.GetString(1)
                            });
                }

                return res;
            }

        }

        /// <summary>
        /// Gets the available payment list for refund
        /// </summary>
        /// <param name="orderLineId">orderLineId</param>
        /// <param name="paymentType">paymentType</param>
        /// <param name="currencyType">currencyType</param>
        /// <returns>List of Payment History Item</returns>
        public List<RefundableItem> GetAvailablePaymentsForRefund(int orderLineId, int paymentType, int currencyType)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            //var db = factory.Create(_connectionString);
            var sql = "LB_GetPaymentsWithoutRefunds";
            //the result object

            var result = new List<RefundableItem>();

            //create command
            using (var command = db.GetStoredProcCommand(sql))
            {
                db.AddInParameter(command, "OrderLineId", DbType.Int32, orderLineId);
                db.AddInParameter(command, "PaymentTypeId", DbType.Int32, paymentType);
                db.AddInParameter(command, "CurrencyTypeId", DbType.Int32, currencyType);
                //execute reader

                using (var reader = db.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        var item = new RefundableItem
                        {
                            ProviderAmount = reader.Value<decimal>("ProviderAmount"),
                            CommissionAmount = reader.Value<decimal>("CommissionAmount"),
                            ProcessingFee = reader.Value<decimal>("ProcessingFee"),
                            Amount = reader.Value<decimal>("Amount"),
                            ReferenceId = reader.Value<string>("ReferenceId"),
                            CreatedDate = reader.Value<DateTime>("Createddate"),
                        };
                        result.Add(item);
                    }
                }
            }

            //return result
            return result;
        }


        public List<Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model.CurrencyDetail> GetCurrencyListDetails()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "LB_GetAllCurrencies";

            var res = new List<Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model.CurrencyDetail>();

            using (var command = db.GetStoredProcCommand(sql))
            {
                using (var reader = db.ExecuteReader(command))
                {
                    while (reader.Read())
                        res.Add(new Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model.CurrencyDetail
                        {
                            //CurrencyGUID  = reader.GetString(2),
                            CurrencyID = reader.GetInt32(0),
                            Name = reader.GetString(1)

                        });
                }

                return res;
            }

        }
                   
        #endregion

        #region Private Methods

        /// <summary>
        /// Get Payment History details
        /// </summary>
        /// <param name="reader">reader</param>
        /// <returns>Emumerable - PaymentHistoryItems</returns>
        private IEnumerable<PaymentHistoryItem> GetPaymentHistory(IDataReader reader)
        {
            while (reader.Read())
            {
                var item = new PaymentHistoryItem
                {
                    OrderLinePaymentid = reader.Value<int>("OrderLinePaymentId"),
                    OrderLineId = reader.Value<int>("oderLineID"),
                    PaymentMethod = reader.Value<string>("PaymentMethod"),
                    PaymentReason = reader.Value<string>("PaymentReason"),
                    Amount = reader.Value<decimal>("Amount"),
                    ReferenceId = reader.Value<string>("ReferenceId"),
                    CreatedDate = reader.Value<DateTime>("Createddate"),
                    CreatedBy = reader.Value<string>("Createdby"),
                    CurrencyTypeId = reader.Value<int>("Currency")
                };

                yield return item;
            }
        }


        /// <summary>
        /// Get Due details by Currency
        /// </summary>
        /// <param name="reader">reader</param>
        /// <returns>Enumerable - Total Due Item</returns>
        private IEnumerable<TotalDueItem> GetDueByCurrencies(IDataReader reader)
        {
            while (reader.Read())
            {
                var item = new TotalDueItem
                {

                    Amount = reader.Value<decimal>("Total"),
                    CurrencyTypeId = reader.Value<int>("Currency")
                };

                yield return item;
            }

        }

        /// <summary>
        /// Get Due History Details
        /// </summary>
        /// <param name="reader">reader</param>
        /// <returns>Enumerable - Due History Item</returns>
        private IEnumerable<DueHistoryItem> GetDueHistory(IDataReader reader)
        {

            while (reader.Read())
            {
                var item = new DueHistoryItem
                {
                    OrderLineDueId = reader.Value<int>("OrderLineDueId"),
                    OrderLineId = reader.Value<int>("OrderLineID"),
                    CurrencyTypeId = reader.Value<int>("Currency"),
                    Amount = reader.Value<decimal>("Amount"),
                    PaymentMethod = reader.Value<string>("PaymentMethod"),
                    Reason = reader.Value<string>("Reason"),
                    CreatedDate = reader.Value<DateTime>("CreatedDate"),
                    CreatedBy = reader.Value<string>("CreatedBy"),
                    CanBeDeleted = reader.Value<bool>("CanBeDeleted")
                };

                yield return item;
            }

        }

        /// <summary>
        /// Get Discount History information
        /// </summary>
        /// <param name="reader">reader</param>
        /// <returns>Enumerable - DiscountHistoryItem</returns>
        private IEnumerable<DiscountHistoryItem> GetDiscountHistory(IDataReader reader)
        {
            while (reader.Read())
            {
                var item = new DiscountHistoryItem
                {
                    DiscountName = reader.Value<string>("Name"),
                    Amount = reader.Value<decimal>("Amount"),
                    CurrencyTypeId = reader.Value<int>("Currency")
                };

                yield return item;
            }
        }
        #endregion

    }
}
