﻿CREATE PROCEDURE [dbo].[LB_GetRoomReservationAgeInfo]
( 
  @Id int 
)
AS
SELECT       AI.Age
FROM            LB_RoomReservationAgeInfo AS  AI
WHERE        (AI.OrderLineId = @Id)