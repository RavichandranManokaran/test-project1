﻿CREATE TABLE [dbo].[uCommerce_PriceGroup] (
    [PriceGroupId] INT              IDENTITY (1, 1) NOT NULL,
    [Name]         NVARCHAR (128)   NOT NULL,
    [CurrencyId]   INT              NOT NULL,
    [VATRate]      DECIMAL (18, 4)  NOT NULL,
    [Description]  NVARCHAR (MAX)   NULL,
    [CreatedOn]    DATETIME         CONSTRAINT [uCommerce_DF_PriceGroup_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    NVARCHAR (50)    NOT NULL,
    [ModifiedOn]   DATETIME         CONSTRAINT [uCommerce_DF_PriceGroup_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]   NVARCHAR (50)    NOT NULL,
    [Deleted]      BIT              CONSTRAINT [uCommerce_DF_PriceGroup_Deleted] DEFAULT ((0)) NOT NULL,
    [Guid]         UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    CONSTRAINT [uCommerce_PK_PriceGroup] PRIMARY KEY CLUSTERED ([PriceGroupId] ASC),
    CONSTRAINT [uCommerce_FK_PriceGroup_Currency] FOREIGN KEY ([CurrencyId]) REFERENCES [dbo].[uCommerce_Currency] ([CurrencyId])
);

