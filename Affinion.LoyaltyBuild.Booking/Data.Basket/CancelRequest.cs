﻿using Affinion.LoyaltyBuild.Model.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Api.Booking.Data.Basket
{
    /// <summary>
    /// Cancel request
    /// </summary>
    public class CancelRequest : ICancelRequest
    {
        /// <summary>
        /// int orderline id
        /// </summary>
        public int OrderLineId { get; set; }

        /// <summary>
        /// cancel reason
        /// </summary>
        public string CancelReason { get; set; }

        /// <summary>
        /// note to provider
        /// </summary>
        public string NoteToProvider { get; set; }

        /// <summary>
        /// cancelled by
        /// </summary>
        public string CancelledBy { get; set; }
              
    }
}
