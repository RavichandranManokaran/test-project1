﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Handler.Offer
{
    /// <summary>
    /// Summary description for RemoveOffer
    /// </summary>
    public class RemoveOffer : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                context.Response.ContentType = "text/plain";
                string offerId = context.Request["offerId"];
                string supplierInviteId = context.Request["supplierInviteId"];
                context.Response.Write(LoadUserControl(context, offerId, supplierInviteId));
            }
            catch (Exception er)
            {
                context.Response.Write("ERROR" + er.Message.ToString());
                Sitecore.Diagnostics.Log.Error(er.Message, new object());
            }
            context.Response.End();
        }



        protected static string LoadUserControl(HttpContext context, string offerId, string supplierInviteId)
        {
            using (Page page = new Page())
            {
                StringBuilder s = new StringBuilder();
                page.EnableViewState = false;
                OfferDetail offerDetails = new OfferDetail();
                AvailabilityHelper staticDataForRoom = new AvailabilityHelper(context);
                int campaignItemId = Convert.ToInt32(offerId);
                offerDetails = staticDataForRoom.GetCampaignById(Convert.ToInt32(campaignItemId), Convert.ToInt32(supplierInviteId), true, false);
                PlaceHolder f = new PlaceHolder();


                using (StringWriter writer = new StringWriter(CultureInfo.CurrentCulture))
                {
                    Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Offer.RemoveOffer removeOfferPopup = (Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Offer.RemoveOffer)page.LoadControl("~/Layouts/SubLayouts/SupplierPortal/Controls/Offer/RemoveOffer.ascx");
                    removeOfferPopup.offerDetail = offerDetails;
                    removeOfferPopup.CampaignItemId = campaignItemId;
                    removeOfferPopup.SupplierInviteId = Convert.ToInt32(supplierInviteId);
                    f.Controls.Add(removeOfferPopup);
                    page.Controls.Add(f);
                    HttpContext.Current.Server.Execute(page, writer, false);
                    s.Clear();
                    s.Append(writer.ToString());
                }
                string html = Regex.Replace(s.ToString(), "<input[^>]*id=\"(__VIEWSTATE)\"[^>]*>", string.Empty, RegexOptions.IgnoreCase);
                return html;
            }

        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}