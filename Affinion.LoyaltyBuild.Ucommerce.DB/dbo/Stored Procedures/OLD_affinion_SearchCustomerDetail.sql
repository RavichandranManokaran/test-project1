﻿CREATE PROCEDURE [dbo].[OLD_affinion_SearchCustomerDetail] 
(
	   @clientId varchar(50) = NULL, 
	   @firstname nvarchar(512)= NULL, 
       @lastname nvarchar(512)= NULL,
	   @AddressLine1 nvarchar(100)= NULL,
	   @AddressLine2 nvarchar(100)= NULL,
	   @campaignGroupId int = NULL,
       @providerId nvarchar(50)= NULL, 
	   @PhoneNumber nvarchar(50) = NULL,
	   @CountryId	int = NULL,
	   @LocationId 	int = NULL
)
AS 
BEGIN

DECLARE @count int = 0
	SELECT 
	 C.CustomerId
	,C.FirstName
	,C.LastName
	,C.EmailAddress
	,C.PhoneNumber
	,C.MobilePhoneNumber
	,C.MemberId
	,CA.ClientId
	,CA.Title
	,CA.AddressLine1
	,CA.AddressLine2
	,CA.AddressLine3
	,CA.CountryId
	,CA.LocationId
	,CA.BranchId
	,CA.BookingReferenceId
	,CA.Subscribe
	,CA.Active
	INTO  #Customer
	FROM 
	[dbo].[uCommerce_Customer] C
	LEFT JOIN [dbo]. uCommerce_CustomerAttribute CA
	on C.CustomerId = CA.CustomerId
	--  INNER JOIN [dbo].uCommerce_CustomerAttribute CA ON PO.CustomerId = CA.CustomerId 
	--	LEFT JOIN [dbo].[uCommerce_CampaignItem] CI ON OCR.CampaignItemId = CI.CampaignItemId
	--	LEFT JOIN [dbo].[uCommerce_Campaign] CM ON CM.CampaignId = CI.CampaignId
	WHERE	(@firstname		IS NULL OR C.FirstName		Like @firstname + '%')
		AND (@lastname		IS NULL OR C.LastName		Like @lastname + '%') 
		AND (@PhoneNumber	IS NULL OR C.PhoneNumber	= @PhoneNumber) 
		AND (@CountryId		IS NULL OR CA.CountryId		= @CountryId)
		AND (@LocationId	IS NULL OR CA.LocationId	= @LocationId)
		AND (@clientId		IS NULL OR CA.ClientId		= @clientId)
		--AND (@providerId IS NULL OR c.[Guid] = @providerId)
		--AND (@campaignGroupId IS NULL OR @campaignGroupId = CM.CampaignId)
		SELECT @count =count(1) FROM #Customer
		IF @count = 1
		BEGIN
			SELECT PO.CustomerId,ol.*,RR.* FROM 
			uCommerce_PurchaseOrder PO
			inner join uCommerce_OrderLine OL
			ON PO.OrderId = OL.OrderId
			inner join LB_RoomReservation RR
			ON OL.OrderLineId = RR.OrderLineId
			inner join #Customer C ON PO.CustomerId = C.CustomerId
		END
END

/*
Select * from uCommerce_PurchaseOrder join 
Select * from uCommerce_OrderLine
Select * from LB_RoomReservation
DECLARE @Customer TABLE( 

	CustomerId int,
	FirstName nvarchar(512),
	LastName nvarchar(512),
	EmailAddress nvarchar(255),
	PhoneNumber nvarchar(50),
	MobilePhoneNumber nvarchar(50),
	MemberId 
	ClientId
	Title
	AddressLine1
	AddressLine2
	AddressLine3
	CountryId
	LocationId
name varchar(30) NOT NULL, 
location varchar(30) NOT NULL 
); 
Lwp
941,944,946
*/

Select * from #Customer

drop table #Customer
