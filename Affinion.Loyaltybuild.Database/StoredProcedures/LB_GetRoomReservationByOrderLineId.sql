﻿CREATE PROCEDURE [dbo].[LB_GetRoomReservationByOrderLineId](@OrderLineId int)
AS
BEGIN
  SELECT * FROM LB_RoomReservation
  WHERE OrderLineId = @OrderLineId
END
