﻿using Affinion.LoyaltyBuild.UCom.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;
using UCommerce.Marketing.Targets;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class AddOnTarget : Target, IOrderLineTarget, IEntity, ITarget, IDisplayTarget
    {
        private int? _hashCode;

        public virtual bool IsSatisfiedBy(OrderLine orderLine)
        {
            OrderProperty searchIgnor = orderLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.IgnorCampaignSearchCriteria);
            if (searchIgnor != null)
                return true;

            OrderProperty forSearch = orderLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.ForSearch);
            if (forSearch != null)
            {
                return true;
            }
            string[] addon = this.AddOn.Split('|');
            if (addon != null && addon.Any())
            {
                bool addOnAdded = addon.Where(y => !string.IsNullOrEmpty(y)).All(x => orderLine.OrderProperties != null && orderLine.OrderProperties.Any(y => y.Key.Equals(x)));
                return addOnAdded;
            }
            return true;


        }

        public virtual bool IsSatisfiedBy(UCommerce.Marketing.TargetingContext targetingContext)
        {
            return true;
        }

        
        // Methods
        public new static IQueryable<AddOnTarget> All()
        {
            return GetRepo().Select();
        }

        public override void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<AddOnTarget, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            AddOnTarget objA = obj as AddOnTarget;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<AddOnTarget, bool>> expression)
        {
            return All().Any<AddOnTarget>(expression);
        }

        public static IList<AddOnTarget> Find(Expression<Func<AddOnTarget, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<AddOnTarget>();
        }

        public static AddOnTarget FirstOrDefault(Expression<Func<AddOnTarget, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<AddOnTarget>();
        }

        public new static AddOnTarget Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<AddOnTarget> GetRepo()
        {
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<AddOnTarget>>();
        }


        public override void Save()
        {
            GetRepo().Save(this);
        }

        public static AddOnTarget SingleOrDefault(Expression<Func<AddOnTarget, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties
        public override int Id
        {
            get
            {
                return this.AddOnTargetId;
            }
        }

        public virtual int AddOnTargetId { get; protected set; }

        public virtual string AddOn { get; set; }





    }
}
