﻿using Affinion.LoyaltyBuild.UCom.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.Search.Facets;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi.Filter
{
    public class FilterByFacilities : IHotelFilter
    {
        public bool IncludeHotel(UCommerce.Documents.Product product, SearchCriteria criteria)
        {
            if (string.IsNullOrEmpty(criteria.Facility))
            {
                return true;
            }
            if (criteria.Facility == "all")
            {
                return true;
            }
            if (!string.IsNullOrEmpty(criteria.Facility))
            {
                KeyValuePair<string, Dictionary<string, object>> facilityProperty = product.Properties.FirstOrDefault(x => x.Value.Any(y => y.Key == ProductDefinationConstant.HotelFacility));
                if (facilityProperty.Value != null)
                {
                    foreach (var item in criteria.Facility.Split(','))
                    {
                        bool found = facilityProperty.Value.Any(x => x.Value.ToString().Contains(item));
                        if (!found)
                        {
                            return false;
                        }
                    }

                }
            }
            return false;
        }


        public UCommerce.Search.Facets.Facet FilterRecordFacet(SearchCriteria criteria)
        {
            if (criteria.Facility != "all" && !string.IsNullOrEmpty(criteria.Facility))
            {
                var facet = new Facet();
                facet.FacetValues = new List<FacetValue>();
                facet.Name = ProductDefinationConstant.HotelFacility; // keeping as experience until facility adds to hotel definition

                //facet.FacetValues.Add(new FacetValue() { Value = criteria.Facility });
//                In this example selected values are seperated by a '|' in the querystring value.
                foreach (var value in criteria.Facility.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    facet.FacetValues.Add(new FacetValue() { Value = value });
                }
                return facet;
            }
            return null;
        }
    }
}
