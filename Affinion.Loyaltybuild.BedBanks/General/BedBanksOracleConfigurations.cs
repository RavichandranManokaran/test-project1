﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.BedBanks.General
{
    public class BedBanksOracleConfigurations
    {
        /// <summary>
        /// Oracle Id
        /// </summary>
        public string OracleId { get; set; }

        /// <summary>
        /// Oracle Id for VAT
        /// </summary>
        public string OracleIDforVAT { get; set; }

        /// <summary>
        /// Vat number
        /// </summary>
        public string VatNumber { get; set; }

        /// <summary>
        /// Customer Address code
        /// </summary>
        public string CustomerAddressCode { get; set; }

        /// <summary>
        /// Supplier Address Code
        /// </summary>
        public string SupplierAddresCode { get; set; }

        /// <summary>
        /// AP Payment Terms
        /// </summary>
        public string ApPaymentTerms { get; set; }

        /// <summary>
        /// AP Line Number
        /// </summary>
        public string ApLineNumber { get; set; }

        /// <summary>
        /// AP Tax code
        /// </summary>
        public string ApTaxCode { get; set; }

        /// <summary>
        /// AP Nominal Code
        /// </summary>
        public string ApNominalCode { get; set; }

        /// <summary>
        /// AP Campaign Accounting ID
        /// </summary>
        public string ApCampaignAccountingId { get; set; }

        /// <summary>
        /// AR Nominal Code
        /// </summary>
        public string ArNominalCode { get; set; }

        /// <summary>
        /// AR Campaign Accounting Id
        /// </summary>
        public string ArCampaignAccountingId { get; set; }

        /// <summary>
        /// AR Receipt terms
        /// </summary>
        public string ArReceiptTerms { get; set; }

        /// <summary>
        /// AR Tax code
        /// </summary>
        public string ArTaxCode { get; set; }

        /// <summary>
        /// AR Invoice Id
        /// </summary>
        public string ArInvoiceId { get; set; }
    }
}
