﻿using Affinion.LoyaltyBuild.Api.Search.Data;
using Affinion.LoyaltyBuild.Api.Search.Helpers;
using Affinion.LoyaltyBuild.Common.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal
{
    public partial class SearchResult : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // var searchResult = SearchHelper.SearchHotels(new SearchKey(HttpContext.Current));
            QueryStringHelper queryHelper = new QueryStringHelper(this.Request.Url);
            var searchResult = SearchHelper.SearchHotels(new SearchKey(this.Context), SetSortOptions(queryHelper));
            var currentPage = SearchHelper.GetPageData(searchResult, 1);

            rptSearchResult.DataSource = currentPage;
            rptSearchResult.DataBind();
        }

        /// <summary>
        /// Sets Search Options for
        /// </summary>
        /// <param name="helper"></param>
        /// <returns></returns>
        private SearchOptions SetSortOptions(QueryStringHelper helper)
        {
            SearchOptions tmpSearchOptions = new SearchOptions();
            if (helper.GetValue("sortmode") != null)
            {
                switch (helper.GetValue("sortmode"))
                {
                    case "Price":
                        tmpSearchOptions.SortOn = SortField.Price;
                        break;
                    case "SupplierName":
                        tmpSearchOptions.SortOn = SortField.Name;
                        break;
                    case "Rate":
                        tmpSearchOptions.SortOn = SortField.Rate;
                        break;
                    case "Recommend":
                        tmpSearchOptions.SortOn = SortField.Recommended;
                        break;
                }
            }
            if (helper.GetValue("sortValue") != null)
            {
                switch (helper.GetValue("sortValue"))
                {
                    case "0":
                        tmpSearchOptions.SortDirection = Affinion.LoyaltyBuild.Api.Search.Data.SortDirection.Ascending;
                        break;
                    case "1":
                        tmpSearchOptions.SortDirection = Affinion.LoyaltyBuild.Api.Search.Data.SortDirection.Descending;
                        break;
                }
            }
            return tmpSearchOptions;
        }
    }
}