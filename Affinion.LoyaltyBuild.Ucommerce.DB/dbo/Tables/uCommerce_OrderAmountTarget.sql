﻿CREATE TABLE [dbo].[uCommerce_OrderAmountTarget] (
    [OrderAmountTargetId] INT             NOT NULL,
    [MinAmount]           DECIMAL (18, 2) NOT NULL,
    CONSTRAINT [PK_OrderAmountTarget] PRIMARY KEY CLUSTERED ([OrderAmountTargetId] ASC)
);

