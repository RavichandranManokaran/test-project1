﻿using Affinion.LoyaltyBuild.SupplierPortal.Reports.Data;
using Affinion.LoyaltyBuild.SupplierPortal.Reports.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.SupplierPortal.Reports
{
    /// <summary>
    /// Report service interface to get report data
    /// </summary>
    public interface IReportService
    {

        /// <summary>
        /// Get arrival report
        /// </summary>
        /// <param name="ArrivalReportFilter"></param>
        /// <returns>List of booking</returns>
        ArrivalReportCollection GetArrivalReport(ArrivalReportFilter arrivalReportFilter);
        
        /// <summary>
        /// Get restricted rooms report 
        /// </summary>
        /// <param name="RestrictedRoomsFilter"></param>        
        /// <returns>List of restricted rooms</returns>
        RestrictedRoomsCollection GetRestrictedRooms(RestrictedRoomsFilter restrictedRoomsFilter);

        /// <summary>
        /// Get availability report 
        /// </summary>       
        /// <param name="AvailabilityReportFilter"></param>
        /// <returns>List of available rooms</returns>
        AvailabilityCollection GetAvailability(AvailabilityReportFilter availabilityReportFilter);

        /// <summary>
        /// Get Revenue report 
        /// </summary>       
        /// <param name="RevenueReportFilter"></param>
        /// <returns>List of Revenue report for total revenue</returns>
        RevenueReportCollection GetRevenueReport(RevenueReportFilter revenueReportFilter);

        /// <summary>
        /// Get Revenue report
        /// </summary>
        /// <param name="RevenueReportFilter"></param>
        /// <returns>List of Revenue report for List Revenue</returns>
        RevenueReportbyProviderCollection GetRevenueReportbyProvider(RevenueReportFilter revenueReportFilter);

    }
}
