﻿CREATE TABLE [dbo].[uCommerce_AmountOffOrderLinesAward] (
    [AmountOffOrderLinesAwardId] INT             NOT NULL,
    [AmountOff]                  DECIMAL (18, 2) NOT NULL,
    CONSTRAINT [PK_uCommerce_AmountOffOrderLinesAward] PRIMARY KEY CLUSTERED ([AmountOffOrderLinesAwardId] ASC),
    CONSTRAINT [FK_uCommerce_AmountOffOrderLinesAward_uCommerce_Award] FOREIGN KEY ([AmountOffOrderLinesAwardId]) REFERENCES [dbo].[uCommerce_Award] ([AwardId])
);

