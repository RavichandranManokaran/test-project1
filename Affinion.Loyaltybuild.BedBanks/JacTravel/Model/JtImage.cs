﻿using System;
using System.Xml.Serialization;

namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Model
{
    [Serializable]
    [XmlType(TypeName = "Image")]
    public class JtImage
    {
        public string Image { get; set; }
    }
}
