﻿//<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           RestrictedRoomsReport.ascx.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.SupplierPortal
/// Description:           SubLayout for RestrictedRooms Report generation
/// </summary>
#region using directives
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.SupplierPortal.DataAccess.Service;
using Affinion.LoyaltyBuild.SupplierPortal.DataAccess.Test;
using Affinion.LoyaltyBuild.SupplierPortal.Reports.Filters;
using Affinion.LoyaltyBuild.SupplierPortal.Reports.Service;
using Affinion.LoyaltyBuild.SupplierPortal.Reports.Test;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration.SupplierPortal.Reports;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration.SupplierPortal.User;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;
using Microsoft.Reporting.WebForms;
using Sitecore.Data.Items;
using System;
using System.Reflection;
using System.Security;
using System.Security.Permissions;
#endregion

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Reports
{
    /// <summary>
    /// Restricted rooms report user control
    /// </summary>
    public partial class RestrictedRoomsReport : ReportBase
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public RestrictedRoomsReport()
            : base(new ProviderService(), new ReportService())
        {

        }

        /// <summary>
        /// page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            BindSitecoreText();
            dateFromCalendarExtender.StartDate = DateTime.Now.AddMonths(-12);
            dateToCalendarExtender.EndDate = DateTime.Now.AddMonths(12);
            if (!IsPostBack)
                BindDropdown(ddlProvider, ProviderList, "Name", "Id");
            if (UserHelper.CurrentUserRole == UserRole.Supplier)
            {
                providerLabel.Visible = false;
                providerDropDown.Visible = false;
            }
        }

        /// <summary>
        /// display reports click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Display_Click(object sender, EventArgs e)
        {
            DateTime Datefrom = Convert.ToDateTime(txtDatefrom.Text);
            DateTime DateTo = Convert.ToDateTime(txtDateto.Text);

            if (Datefrom <= DateTo)
            {
                TimeSpan daycount = Convert.ToDateTime(DateTo).Subtract(Convert.ToDateTime(Datefrom));
                int dacount1 = Convert.ToInt32(daycount.Days);
                if ((dacount1 < 366))
                {
                    CreateRestrictedRoomsReport();
                }
                else
                {
                    cusValDateTo.IsValid = false;
                }
            }
        }

        /// <summary>
        /// Create restricted rooms report
        /// </summary>
        private void CreateRestrictedRoomsReport()
        {
            /// <summary>
            /// get the report assembly to load report definition of rdlc files
            /// </summary>
            
            Assembly assembly = Assembly.GetAssembly(typeof(Affinion.LoyaltyBuild.SupplierPortal.Reports.IReportService));

            /// <summary>
            /// get date from 
            /// </summary>
            
            var dateFrom = GetDateValue(txtDatefrom.Text);
            /// <summary>
            /// get date to
            /// </summary>
            
            var dateTo = GetDateValue(txtDateto.Text);
            /// <summary>
            /// get provider id 
            /// </summary>
            

            int? providerId = null;
            string showProvider;
            if (UserHelper.CurrentUserRole == UserRole.Supplier)
            {
                var sku = SitecoreUserProfile.SelectedSKU;
                if (!String.IsNullOrWhiteSpace(sku))
                {
                    var provider = _providerService.GetProviderById(sku);
                    if (provider != null)
                        providerId = provider.Id;
                }
                
                showProvider = "false";
            }
            else
            {
                int tempProvider = 0;
                if (int.TryParse(ddlProvider.SelectedValue, out tempProvider))
                    providerId = tempProvider;
                showProvider = "true";
            }
            /// <summary>
            /// load report
            /// </summary>
            using (var stream = assembly.GetManifestResourceStream("Affinion.LoyaltyBuild.SupplierPortal.Reports.Reports.RestrictedRoomReport.rdlc"))
            {
                var objShowRecord = _reportService.GetRestrictedRooms(new RestrictedRoomsFilter()
                {
                    DateFrom = dateFrom,
                    DateTo = dateTo,
                    ProviderId = providerId
                });
                this.rvDisplayrecord.KeepSessionAlive = false;
                this.rvDisplayrecord.AsyncRendering = false;
                rvDisplayrecord.ProcessingMode = ProcessingMode.Local;
                rvDisplayrecord.LocalReport.SetBasePermissionsForSandboxAppDomain(new PermissionSet(PermissionState.Unrestricted));
                rvDisplayrecord.LocalReport.LoadReportDefinition(stream);
                var datasource = new ReportDataSource("DataSetRestrictedRoom", objShowRecord);
                rvDisplayrecord.LocalReport.SetParameters(new ReportParameter("ShowProvider", showProvider));
                rvDisplayrecord.LocalReport.DataSources.Clear();
                rvDisplayrecord.LocalReport.DataSources.Add(datasource);
            }
        }
        public void BindSitecoreText()
        {
            Item currentPageItem = Sitecore.Context.Item;
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Title", RestrictedTitle);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "ReportInformation", AvailabityReportInformation);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "DateFrom", datefromText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "DateTo", dateToText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Provider", ProviderText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Submit", SubmitText);
        }
    }
}
