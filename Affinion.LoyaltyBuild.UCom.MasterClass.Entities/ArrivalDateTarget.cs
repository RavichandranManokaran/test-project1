﻿
using Affinion.LoyaltyBuild.UCom.Common.Constants;
using Affinion.LoyaltyBuild.UCom.Common.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;
using UCommerce.Marketing.Targets;


namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class ArrivalDateTarget : Target, IOrderLineTarget, IEntity, ITarget
    {
        private int? _hashCode;

        public virtual bool IsSatisfiedBy(OrderLine orderLine)
        {
            OrderProperty searchIgnor = orderLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.IgnorCampaignSearchCriteria);
            if (searchIgnor != null)
                return true;

            OrderProperty arrivalDateProperty = orderLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.ArrivalDateKey);
            if (arrivalDateProperty != null)
            {
                DateTime arrivalDate = arrivalDateProperty.Value.ParseFormatDateTime(true);
                if (arrivalDate >= this.ArrivalFromDate && arrivalDate <= this.ArrivalToDate)
                {
                    return true;
                }

            }
            return false;
        }



        // Methods
        public new static IQueryable<ArrivalDateTarget> All()
        {
            return GetRepo().Select();
        }

        public override void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<ArrivalDateTarget, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            ArrivalDateTarget objA = obj as ArrivalDateTarget;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<ArrivalDateTarget, bool>> expression)
        {
            return All().Any<ArrivalDateTarget>(expression);
        }

        public static IList<ArrivalDateTarget> Find(Expression<Func<ArrivalDateTarget, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<ArrivalDateTarget>();
        }

        public static ArrivalDateTarget FirstOrDefault(Expression<Func<ArrivalDateTarget, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<ArrivalDateTarget>();
        }
        
        public static new ArrivalDateTarget Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<ArrivalDateTarget> GetRepo()
        {
            
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<ArrivalDateTarget>>();
        }


        public override void Save()
        {
            GetRepo().Save(this);
        }

        public static ArrivalDateTarget SingleOrDefault(Expression<Func<ArrivalDateTarget, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties
        public override int Id
        {
            get
            {
                return this.ArrivalDateTargetId;
            }
        }

        public virtual int ArrivalDateTargetId { get; protected set; }

        public virtual DateTime ArrivalFromDate { get; set; }

        public virtual DateTime ArrivalToDate { get; set; }



    }
}
