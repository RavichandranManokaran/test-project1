﻿CREATE TABLE [dbo].[LB_SubrateItem] (
    [SubRateItemCode] NVARCHAR (50)  NOT NULL,
    [DisplayName]     NVARCHAR (150) NULL,
    CONSTRAINT [PK_uCommerce_SubrateItem] PRIMARY KEY CLUSTERED ([SubRateItemCode] ASC)
);

