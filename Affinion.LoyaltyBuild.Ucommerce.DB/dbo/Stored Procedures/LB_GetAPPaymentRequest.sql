CREATE PROCEDURE [dbo].[LB_GetAPPaymentRequest]
@DateRange DATETIME=NULL,
@DateUpTo DATETIME=NULL,
@Provider VARCHAR(MAX)=NULL,
@Partner VARCHAR(MAX)=NULL,
@Spa VARCHAR(5)=NULL,
@PaymentStatus VARCHAR(10) =NULL,
@SearchText VARCHAR(MAX)=NULL
AS
BEGIN
DECLARE @SearchData VARCHAR(MAX)=NULL

IF @SearchText IS NOT NULL
BEGIN
	SET @SearchData = '%' + @SearchText + '%'
END

IF @DateUpTo IS NOT NULL AND @DateRange IS NULL
BEGIN
	DECLARE @CurrentMonth INT=0
	DECLARE @MonthDateUpTo INT=0

	SET @CurrentMonth=MONTH(GETDATE())
	SET @MonthDateUpTo=MONTH(@DateUpTo)   

	IF  @CurrentMonth = @MonthDateUpTo 
	BEGIN
		SET @DateUpTo= EOMONTH(@DateUpTo, -1) 
	END
END

        SELECT NULL AS RequestedBy,* FROM LB_APPaymentRequestView
		WHERE
		(@Partner IS NULL OR  ClientId = @Partner) AND
		(@Provider IS NULL OR  ProviderID = @Provider) AND
		(@PaymentStatus IS NULL OR  PaymentStatus = @PaymentStatus) AND
		(
		         @SearchData IS NULL OR 
				 ( 
				       CONVERT(VARCHAR,OrderId) LIKE  @SearchData OR
					   HotelName LIKE  @SearchData OR
					   BookingRefNo LIKE  @SearchData OR
					   CustomerName LIKE  @SearchData 
				 )
		 ) 
		AND
		(
			(@DateRange IS NOT NULL AND (CAST(CheckInDate AS DATE) <= CAST(@DateUpTo AS DATE)  AND  CAST(CheckOutDate AS DATE) >= CAST(@DateRange AS DATE)))
			OR
			(@DateRange IS  NULL AND CAST(CheckInDate AS DATE) <= CAST(@DateUpTo AS DATE))
		)

END

