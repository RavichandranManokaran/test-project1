﻿CREATE TABLE [dbo].[uCommerce_FavoriteList](
	[ListID] [int] IDENTITY(100,1) NOT NULL,
	[CustomerID] [int] NULL,
	[ListName] [varchar](50) NULL,
	[HotelID] [varchar](1000) NULL,
	[CreatedBy] [varchar](50) NULL,
	[UpdatedBy] [varchar](50) NULL,
	[DateCreated] [datetime] NULL,
	[DateLastUpdated] [datetime] NULL,
 CONSTRAINT [PK_uCommerce_FavoriteList] PRIMARY KEY CLUSTERED 
(
	[ListID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO