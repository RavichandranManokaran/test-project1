﻿using Affinion.LoyaltyBuild.BedBanks.Troika;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Affinion.LoyaltyBuild.Common.Utilities;

namespace Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.BedBanks
{
    public partial class MarginManagementSearch : System.Web.UI.UserControl
    {
        private DataTable data;
        protected void Page_Load(object sender, EventArgs e)
        {
            //txtDateFrom.ReadOnly = true;
            //txtDateTo.ReadOnly = true;
            if (!IsPostBack)
            { 
            addMarginDiv.Visible = false;
            divSuccess.Visible = false;
            divWarn.Visible = false;
            }
        }

        /// <summary>
        /// Go to view the margin detail for the property
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvSearchData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName != "EditMargin") return;
            string queryString = e.CommandArgument.ToString();
            string pageUrl = SitecoreFieldsHelper.GetItemFieldValue(Sitecore.Context.Item, "Margin Detail Page Link");
            Response.Redirect(pageUrl + "?propertyId=" + queryString);
        }

        /// <summary>
        /// Populate the property based on search criteria
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSubmit_ServerClick(object sender, EventArgs e)
        {
            GetProviders();
            if (this.data != null)
            {
                gvSearchData.DataSource = data;
                gvSearchData.DataBind();
                if (data.Rows.Count > 0)
                    addMarginDiv.Visible = true;
            }
        }

        private void GetProviders()
        {
            string country = !string.IsNullOrEmpty(txtCountry.Text) ? txtCountry.Text : string.Empty;
            string location = !string.IsNullOrEmpty(txtLocation.Text) ? txtLocation.Text : string.Empty;
            string mapLocation = !string.IsNullOrEmpty(txtMapLocation.Text) ? txtMapLocation.Text : string.Empty;
            string provider = !string.IsNullOrEmpty(txtProvider.Text) ? txtProvider.Text : string.Empty;
            this.data = TroikaDataController.providerDetailForMargin(country, location, mapLocation, provider);
        }


        /// <summary>
        /// Print the given grid to a excel sheet
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnExport_ServerClick(object sender, EventArgs e)
        {
            if (gvSearchData != null && gvSearchData.PageCount > 0)
            { 
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "provider.xls"));
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            //Change the Header Row back to white color
            gvSearchData.HeaderRow.Style.Add("background-color", "#FFFFFF");
            //Applying stlye to gridview header cells
            for (int i = 0; i < gvSearchData.HeaderRow.Cells.Count; i++)
            {
                gvSearchData.HeaderRow.Cells[i].Style.Add("background-color", "#df5015");
            }
            gvSearchData.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();
            }
        }

        protected void btnAddMargin_ServerClick(object sender, EventArgs e)
        {
            txtDateFrom.ReadOnly = false;
            txtDateTo.ReadOnly = false;
            string dateFrom = txtDateFrom.Text;
            string dateTo = txtDateTo.Text;
            decimal margin;
            decimal.TryParse(txtMargin.Text, out margin);
            GetProviders();
            if (this.data != null)
            {
                string providerList = string.Empty;
                foreach (DataRow provider in data.Rows)
                {
                    providerList = providerList + provider[0].ToString() + ",";
                }
                providerList = providerList.TrimEnd(',');
                DataTable listName = TroikaDataController.SetMarginForMultipleProvider(providerList, margin, dateFrom, dateTo);
                addMarginDiv.Visible = true;
                if (listName != null && listName.Rows.Count > 0 && listName.Rows[0] != null)
                {
                    //TODO
                    string pageUrl = SitecoreFieldsHelper.GetItemFieldValue(Sitecore.Context.Item, "Margin Error Page Link");
                    linkErrorList.NavigateUrl = pageUrl + "?ListID=" + listName.Rows[0][1];
                    linkErrorList.Text = listName.Rows[0][0].ToString();
                    divWarn.Visible = true;
                    divSuccess.Visible = false;
                }
                else
                {
                    divSuccess.Visible = true;
                    divWarn.Visible = false;
                }
            }
        }
    }
}
