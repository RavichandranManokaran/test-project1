﻿CREATE TABLE [dbo].[uCommerce_DataTypeEnumDescription] (
    [DataTypeEnumDescriptionId] INT            IDENTITY (1, 1) NOT NULL,
    [DataTypeEnumId]            INT            NOT NULL,
    [CultureCode]               NVARCHAR (60)  NOT NULL,
    [DisplayName]               NVARCHAR (512) NOT NULL,
    [Description]               NTEXT          NULL,
    CONSTRAINT [uCommerce_PK_DataTypeEnumDescription] PRIMARY KEY CLUSTERED ([DataTypeEnumDescriptionId] ASC),
    CONSTRAINT [uCommerce_FK_DataTypeEnumDescription_DataTypeEnum] FOREIGN KEY ([DataTypeEnumId]) REFERENCES [dbo].[uCommerce_DataTypeEnum] ([DataTypeEnumId])
);

