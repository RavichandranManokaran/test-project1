﻿Create PROCEDURE [dbo].[LB_GetBookingDetail] (@OrderLineID int)

AS

BEGIN

WITH BookingDetails (OrderId,CustomerID,CurrencyId,OrderLineId,Sku,Price,Discount,TotalPrice,Reservation,ClientId,BookingReferance, 

			CheckIndate,CheckOutdate,StatusId,CreateBy,Confirmation,Statusdate,LastUpdate,NoOfNights,FirstName,LastName,[Address],[Phonenumber],[Email]

			,OccupancyType,ProviderId,LanguageName,IsCancelled,Note)

AS

(

		 SELECT

				PO.OrderId, 

				PO.CustomerID,

				PO.CurrencyId,

				OL.OrderLineId, 

				OL.Sku [Sku], 

				OL.Total [Price], 

				OL.Discount, 

				OL.Total [TotalPrice],

				OL.CreatedOn [Reservation],

				CAT.Value [ClientId],

				BI.BookingNo [BookingReferance], 

				BI.CheckIndate [CheckIndate], 

				BI.CheckOutdate [CheckOutdate], 

				BI.StatusId [StatusId], 

				BI.CreatedBy [CreateBy],

				(SELECT MAX(CreatedDate) FROM LB_BookingInfo WHERE OrderLineId = OL.OrderLineId AND StatusId = 6) [Confirmation],

				(SELECT MAX(CreatedOn) FROM LB_OrderLineStatusAudit WHERE OrderLineId = OL.OrderLineId ) [Statusdate], 

				(SELECT MAX(CreatedBy) FROM LB_OrderLineStatusAudit WHERE OrderLineId = OL.OrderLineId ) [LastUpdate],

				DATEDIFF(DAY, BI.CheckInDate, BI.checkOutdate) AS [NoOfNights],

				OA.FirstName, 

				OA.LastName, 

				OA.AddressName [Address], 

				OA.PhoneNumber [Phonenumber], 

				OA.EmailAddress [Email],

				OL.ProductName [OccupancyType],

				P.[Guid] [ProviderId],

				UCUS.LanguageName,

				(CASE WHEN BI.StatusId=7 THEN 1 ELSE 0 END)[IsCancelled],
				(SELECT TOP 1 Note FROM [dbo].LB_ReservationNotes RR WHERE RR.OrderLineId = OL.OrderLineId and NoteTypeId=2 order by NotesId desc ) [Note] 

	

		FROM [dbo].uCommerce_OrderLine OL

					  INNER JOIN [dbo].uCommerce_PurchaseOrder PO ON PO.OrderID = OL.OrderId

					  INNER JOIN [dbo].LB_BookingInfo BI ON BI.OrderLineId = OL.OrderLineId

					  INNER JOIN Ucommerce_product P ON OL.Sku = P.Sku AND p.ParentProductId IS  NULL

					  INNER JOIN LB_CustomerProperty CAT ON PO.CustomerId = CAT.CustomerId AND CAT.[Key]='_ClientId'  

					  LEFT JOIN [dbo].uCommerce_OrderAddress OA ON OA.OrderId = OL.OrderId

					  LEFT JOIN [dbo].uCommerce_Customer UCUS ON PO.CustomerID = UCUS.CustomerId

		WHERE 

				OL.OrderLineID = @OrderLineID

				 

	)





SELECT 

		OrderId,CustomerID,CurrencyId,OrderLineId,Sku,Price,Discount,TotalPrice,Reservation,ClientId,BookingReferance, 

		CheckIndate,CheckOutdate,StatusId,CreateBy,Confirmation,Statusdate,LastUpdate,NoOfNights,FirstName,LastName,

		[Address],[Phonenumber],[Email],OccupancyType,ProviderId,LanguageName,IsCancelled,

		_NoOfAdult [NoOfAdult],_NoOfChildren [NoOfChild],_NoOfGuest [NoOfGuest],_GuestName [GuestName],

		_NoOfRooms [NoOfRooms],_ChildrenAge [ChildAge], _BookingThrough [BookingThrough], _providerAmount [providerAmount],

		 _LBCommissionFee [CommissionFee], ProcessingFee [CreditcardProcessingfee],Note

	FROM 

		(SELECT BD.*, [key], [value] 

		FROM uCommerce_OrderProperty CP 

		INNER JOIN BookingDetails BD ON BD.OrderId = CP.OrderId AND BD.OrderLineId = CP.OrderLineId

		) src

	PIVOT

		( MAX([value]) FOR [key] IN (_NoOfAdult, _NoOfChildren, _NoOfGuest, _GuestName, _NoOfRooms, _ChildrenAge, _BookingThrough, _providerAmount, _LBCommissionFee, ProcessingFee)

		) piv

	



			--Payment History Total

SELECT SUM([Total]) [Total], [Currency] FROM

(SELECT SUM(OLD.TotalAmount) [Total],

			OLD.CurrencyTypeId [Currency]



		FROM uCommerce_OrderLine OL

		INNER JOIN LB_OrderLineDue OLD ON OLD.OrderLineID = OL.OrderLineId

		WHERE ISNULL(OLD.IsDeleted, 0) = 0

              AND OLD.OrderLineID IN (SELECT OrderLineID FROM [dbo].[LB_FnRelatedOrderLines](@OrderLineId))

        GROUP BY OLD.CurrencyTypeId

  UNION ALL

        SELECT

          -SUM(OLP.TotalAmount) [Total],

          OLP.CurrencyTypeId [Currency]

  FROM uCommerce_OrderLine OL

       INNER JOIN LB_OrderLinePayment OLP ON OLP.OrderLineID = OL.OrderLineId

  WHERE OLP.OrderLineID IN (SELECT OrderLineID

  FROM [dbo].[LB_FnRelatedOrderLines](@OrderLineId))

  GROUP BY OLP.CurrencyTypeId) P

  GROUP BY [Currency]

  HAVING SUM([Total]) <> 0





  END