﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BookingDetailsSubLayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal.BookingDetailsSubLayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="book-top">
    <div class="booking-detail">
        <div style="opacity: 1 !important;">
            <div class="booking-top width-full divider">
                <h4 class="no-margin">Find the best deals on accommodation</h4>
            </div>
            <div class="booking-deatail width-full booking-margin-top">
                <h5>Detaile Info</h5>
                <input type="text" class="form-control" placeholder="Detaile Info" aria-describedby="basic-addon1" />
            </div>
            <div class="booking-deatail-order width-full  inline-block">
                <div class="booking-deatail-order-left width-half float-left">
                    <h5>When to order</h5>
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-default glyphicon glyphicon-calendar calander-icon" type="button"></button>
                        </span>
                        <input type="text" class="form-control booking-controle" placeholder="When to order..." id="todatepicker" />
                    </div>
                </div>
                <div class="booking-deatail-order-right width-half float-left">
                    <h5>When from order</h5>
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-default glyphicon glyphicon-calendar calander-icon" type="button"></button>
                        </span>
                        <input type="text" class="form-control booking-controle" placeholder="When from order..." id="fromdatepicker">
                    </div>
                </div>
            </div>
            <div class="booking-deatail-order-room width-full inline-block">
                <h5 class="no-margin-bottom">Select Rooms</h5>
                <div class="booking-common-three-box float-left">
                    <h5>Rooms</h5>
                    <div class="btn-group" role="group" aria-label="...">
                        <select class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>

                </div>
                <div class="booking-common-three-box float-left">
                    <h5>Adults</h5>
                    <div class="btn-group" role="group" aria-label="...">
                        <select class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>

                </div>
                <div class="booking-common-three-box float-left">
                    <h5>Children</h5>
                    <div class="btn-group" role="group" aria-label="...">
                        <select class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>

                </div>
            </div>
            <div class="booking-deatail-choose-pack width-full inline-block">

                <h5>Choose Your Package </h5>
                <select class="form-control styled-select">
                    <option selected="selected" value="746">Show me everything</option>
                    <option value="205">Breakfast</option>
                    <option value="252">Breakfast + 1 Dinner</option>
                    <option value="748">Breakfast  - Inspired Collection</option>
                    <option value="248">Breakfast + 1 Dinner + Spa</option>
                    <option value="238">Self Catering Accommodation</option>
                    n&gt;
                </select>
                <button type="submit" class="btn btn-primary booking-margin-top float-right btn-bg">Submit</button>

            </div>
        </div>
    </div>
</div>
