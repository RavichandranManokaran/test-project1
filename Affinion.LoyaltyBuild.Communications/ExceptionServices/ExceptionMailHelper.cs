﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           ExceptionMailHelper.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Communications.ExceptionServices
/// Description:           Handling Exception notification Email 
/// </summary>
#region Using Directives
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.Communications.Services;
using Sitecore.Data;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using System.Net.Mail;
#endregion

namespace Affinion.LoyaltyBuild.Communications.ExceptionServices
{
    public class ExceptionMailHelper
    {
        #region Public Properties
        public string senderEmail { get; set; }
        public string receiverEmail { get; set; }
        public string mailSubject { get; set; }
        public string msgBody { get; set; }
        public Item exceptionEmailItem { get; set; }
        public string bccEmail { get; set; }
        Database currentDb = Sitecore.Context.Database;
        Item rootItem = Sitecore.Context.Database.GetItem(Sitecore.Context.Site.RootPath);
        #endregion

        #region public methods
        /// <summary>
        /// method for send to exception mail and get paticular PostBreakEmail sitecore item
        /// </summary>
        /// <param name="exception"></param>
        public void SendExceptionEmail(string exception, int orderId)
        {
            try
            {
                if (orderId == 0)
                    return;

                string postBreakEmail = string.Concat(rootItem.Paths.FullPath, "/global/_supporting-content/exceptionemail");
                exceptionEmailItem = currentDb.GetItem(postBreakEmail).Children.FirstOrDefault(i => i.TemplateName == "PostBreakEmail");
                setExceptionMail(exceptionEmailItem, exception, orderId);
            }
            catch (Exception )
            {
                Diagnostics.Trace(DiagnosticsCategory.Communications, "Error occured when processing  SendExceptionEmail() fot IT team");
            }
        }

        /// <summary>
        /// Set the properties for the email message and pass the object to send the message
        /// </summary>
        /// <param name="exception"></param>
        /// <param name="itemName"></param>
        public void SendExceptionEmail(string exception, string itemName, string userName, string userEmail)
        {
            try
            {
                string postBreakEmail = string.Concat(rootItem.Paths.FullPath, "/global/_supporting-content/clientexceptionemail");
                if (string.IsNullOrEmpty(itemName))
                {
                    return;
                }
                else
                {
                    string emailItemPath = string.Concat(postBreakEmail, "/", itemName);
                    exceptionEmailItem = currentDb.GetItem(emailItemPath);
                    if (exceptionEmailItem == null)
                    {
                        return;
                    }
                }

                SetExceptionMail(exceptionEmailItem, exception, userName, userEmail);
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Communications, ex, this);
                throw;
            }
        }

        #endregion

        #region private methods
        /// <summary>
        /// method for setvalue to exception mail properties
        /// </summary>
        /// <param name="mailItem">get sitecore item that mail parameters values are saved</param>
        /// <param name="exception"></param>
        public void setExceptionMail(Item mailItem, string exception, int orderId)
        {
            IEmailService emailService = new EmailService();
            senderEmail = SitecoreFieldsHelper.GetValue(mailItem, "senderEmail");
            receiverEmail = SitecoreFieldsHelper.GetValue(mailItem, "receiverEmail", string.Empty);
            mailSubject = SitecoreFieldsHelper.GetValue(mailItem, "Subject");
            msgBody = SitecoreFieldsHelper.GetValue(mailItem, "EmailBody");
            List<string> errMsg = new List<string>();
            string orderIdOfError = string.Format("{0}{1}", "The order id is ", orderId.ToString());
            string emailBody = null;
            if (orderId != 0)
            {
                emailBody = string.Format(msgBody, orderIdOfError, exception);
            }
            else
            {
                emailBody = string.Format(msgBody, "", exception);
            }


            if (string.IsNullOrEmpty(senderEmail))
            {
                Diagnostics.Trace(DiagnosticsCategory.Communications, "SenderEmail that  Field of" + mailItem.DisplayName + "is not configured");
                return;
            }
            if (string.IsNullOrEmpty(receiverEmail))
            {
                Diagnostics.Trace(DiagnosticsCategory.Communications, "ReceiverEmail that  Field of " + mailItem.DisplayName + " is not configured");
                return;
            }
            if (string.IsNullOrEmpty(mailSubject))
            {
                Diagnostics.Trace(DiagnosticsCategory.Communications, "MassageSubject that  Field of " + mailItem.DisplayName + " is not configured");
                return;
            }
            if (string.IsNullOrEmpty(msgBody))
            {
                Diagnostics.Trace(DiagnosticsCategory.Communications, "MassageBody that  Field of " + mailItem.DisplayName + " is not configured");
                return;
            }
            //send mail
            emailService.Send(senderEmail, receiverEmail, mailSubject, emailBody, true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mailItem"></param>
        /// <param name="exception"></param>
        private void SetExceptionMail(Item mailItem, string exception, string name, string email)
        {
            IEmailService emailService = new EmailService();
            senderEmail = SitecoreFieldsHelper.GetValue(mailItem, "senderEmail", string.Empty);
            receiverEmail = email;
            bccEmail = SitecoreFieldsHelper.GetValue(mailItem, "BccEmail");
            mailSubject = SitecoreFieldsHelper.GetValue(mailItem, "Subject");
            msgBody = SitecoreFieldsHelper.GetValue(mailItem, "EmailBody");
            msgBody = msgBody.Replace("[@Username]", name);
            List<string> errMsg = new List<string>();

            string emailBody = string.Format(msgBody, exception);

            if ((string.IsNullOrEmpty(senderEmail)) || (string.IsNullOrEmpty(receiverEmail)) || (string.IsNullOrEmpty(mailSubject)) || (string.IsNullOrEmpty(msgBody)) || (string.IsNullOrEmpty(bccEmail)))
            {
                //throw new AffinionException("SenderEmail that  Field of" + mailItem.DisplayName + "is not configured");
                Diagnostics.Trace(DiagnosticsCategory.Communications, "Unable to send email: please check values in email Item (sender, receiver, subject, message, bcc)");
                return;
            }
            else
            {
                //add the email properties to an object
                MailMessage mailObject = new MailMessage();
                mailObject.From = new MailAddress(senderEmail);
                mailObject.To.Add(new MailAddress(receiverEmail));
                mailObject.Bcc.Add(new MailAddress(bccEmail));
                mailObject.Subject = mailSubject;
                mailObject.Body = msgBody;
                mailObject.IsBodyHtml = true;

                //send mail
                emailService.Send(mailObject);
            }
            //if (string.IsNullOrEmpty(receiverEmail))
            //{
            //    throw new AffinionException("ReceiverEmail that  Field of " + mailItem.DisplayName + " is not configured");
            //}
            //if (string.IsNullOrEmpty(mailSubject))
            //{
            //    throw new AffinionException("MassageBody that  Field of " + mailItem.DisplayName + " is not configured");
            //}
            //if (string.IsNullOrEmpty(msgBody))
            //{
            //    throw new AffinionException("MassageBody that  Field of " + mailItem.DisplayName + " is not configured");
            //}


        }
        #endregion
    }
}
