﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProvisionalBookingReport.ascx.cs" Inherits="Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.Reports.ProvisionalBookingReport" %>
    <%@ register tagprefix="sc" namespace="Sitecore.Web.UI.WebControls" assembly="Sitecore.Kernel" %>
        <%@ import namespace="Sitecore.Data.Items" %>

            <div class="marB65">
                <div class="Provisional Report">
                    <div>
                        <h3><sc:text id="scProvisional" runat="server"/></h3>
                    </div>
                    <div>
                        <sc:text id="scPleaseselect" runat="server" />

                    </div>
                    <div>

                    </div>
                    <div>
                        <div class="row report-filter container-fluid">
                            <div class="col-xs-12 marT10">
                                <div class="row">
                                    <div class="col-md-3">
                                        <sc:text id="scDateFrom" runat="server" />
                                        <span class="required">*</span>
                                    </div>
                                    <div class="col-md-3 ">
                                        <div class="input-group date">
                                            <asp:TextBox class="form-control txtDateFrom" ID="txtDateFrom" runat="server" MaxLength="50"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="dateFromRequiredField" runat="server" style="color:red" ControlToValidate="txtDateFrom" Display="Dynamic" validationgroup="ReportValidationGroup">
                                                <sc:text id="scDateFromRequired" runat="server" />
                                            </asp:RequiredFieldValidator>

                                        </div>
                                        <div class="form-group">
                                            <div class="input-group date">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <sc:Text id="scPartner" runat="server" />
                                        <span class="required">*</span>`
                                    </div>
                                    <div class="col-md-3">

                                        <asp:DropDownList ID="ddlPartner" runat="server" CssClass="form-control" AppendDataBoundItems="false">
                                            <asp:ListItem Text="--Select Partner--" Value="0" Selected="True" />
                                        </asp:DropDownList>

                                        <asp:RequiredFieldValidator ID="partnerRequiredField" runat="server" style="color:red" ControlToValidate="ddlPartner" Display="Dynamic" InitialValue="0" validationgroup="ReportValidationGroup">
                                            <sc:text id="scPartnerRequired" runat="server" />
                                        </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-md-3">
                                        <sc:text id="scDateTo" runat="server" />
                                        <span class="required">*</span>
                                    </div>
                                    <div class="col-md-3 marT5">

                                        <div class="form-group">
                                            <div class="input-group date">
                                                <asp:TextBox class="form-control txtDateTo" ID="txtDateTo" runat="server" MaxLength="50"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="dateToRequiredField" runat="server" style="color:red" ControlToValidate="txtDateTo" Display="Dynamic" validationgroup="ReportValidationGroup">
                                                    <sc:text id="scDateToRequired" runat="server" />
                                                </asp:RequiredFieldValidator>

                                            </div>


                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="checkbox" id="chkShowBooking" checked="checked" runat="server" />
                                            <sc:text id="scShowbookingconfirmation" runat="server" />

                                        </div>
                                        <div class="col-md-3">
                                            <sc:text id="scReportorderby" runat="server" />

                                        </div>
                                        <div class="col-md-3">
                                            <asp:DropDownList ID="ddlReportAndOrderBy" runat="server" CssClass="form-control" AppendDataBoundItems="false">
                                                <asp:ListItem>ArrivalDate</asp:ListItem>
                                                <asp:ListItem>ReservationDate</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <sc:text id="scPartialpayment" runat="server" />

                                        </div>
                                        <div class="col-md-3">
                                            <asp:TextBox ID="txtPartialPayment" runat="server" MaxLength="50"></asp:TextBox>
                                        </div>
                                        <div class="col-md-3"></div>
                                        <div class="col-md-3">
                                            <asp:CompareValidator ID="compareDatesField" ControlToCompare="txtDateFrom" style="color:red" ControlToValidate="txtDateTo" Type="Date" Operator="GreaterThanEqual" Display="Dynamic" validationgroup="ReportValidationGroup" runat="server">
                                                <sc:text id="CompareDatesLabel" runat="server" />
                                            </asp:CompareValidator>


                                            <button type="button" OnServerClick="btnListProvisional_Click" validationgroup="ReportValidationGroup" runat="server">
                                                <sc:text id="scListProvisionalText" runat="server" />
                                            </button>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div id="showBooking" runat="server" visible="false" class="booking marT20 ">
                        <h2><sc:text id="scListProvisionalBooking" runat="server"/><asp:label ID="lblCount" CssClass="booking-date" runat="server"></asp:label></h2>
                        <div id="provisionalbooking" runat="server">
                            <asp:GridView ID="grdvwProvisional" runat="server" CssClass="table table-striped marTB10" AutoGenerateColumns="False">
                                <Columns>


                                    <asp:TemplateField HeaderText="BookingReference">
                                        <ItemTemplate>
                                            <a href="http://lbofflineportalqa.virtusa.com/bookingdetails?oref=<%#Eval(" BookingReference ") %> " target="_blank">
                                                <%#Eval( "BookingReference") %>
                                            </a>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="Provider" HeaderText="Provider"></asp:BoundField>
                                    <asp:BoundField DataField="ReservationDate" HeaderText="ReservationDate"></asp:BoundField>
                                    <asp:BoundField DataField="ArrivalDate" HeaderText="ArrivalDate"></asp:BoundField>
                                    <asp:BoundField DataField="CustomerName" HeaderText="Customer"></asp:BoundField>
                                    <asp:BoundField DataField="PhoneNumber" HeaderText="Phone Number"></asp:BoundField>
                                    <asp:TemplateField HeaderText="EmailId">
                                        <ItemTemplate>
                                            <a href="mailto:<%#Eval(" EmailId ") %>">
                                                <%#Eval( "EmailId") %>
                                            </a>
                                        </ItemTemplate>
                                    </asp:TemplateField>




                                </Columns>
                            </asp:GridView>
                        </div>


                        <div class="col-sm-2">
                            <button type="button" cCssClass="btn-primary pull-right book_report " OnServerClick="buttonConvertExcel_Click" runat="server">
                                <sc:text id="scConvertToExcel" runat="server" />
                            </button>

                        </div>
                        <div class="col-sm-4 col-xm-12">

                            <button type="button" cCssClass="btn-primary pull-right book_report " formtarget="_blank" OnServerClick="PrintButton_Click" runat="server">
                                <sc:text id="scPrint" runat="server" />
                            </button>

                        </div>



                    </div>

                </div>




                <script type="text/javascript">
                    $(function () {
                        jQuery('.txtDateFrom').datepicker({
                            defaultDate: new Date(),
                            dateFormat: "<%=Affinion.LoyaltyBuild.Common.Utilities.DateTimeHelper.MonthDateFormat%>",
                            numberOfMonths: 1,
                            showOn: "both",
                            buttonImage: "images/calendar.png",
                            buttonImageOnly: false,
                            buttonClass: "glyphicon-calendar"
                        }).next(".ui-datepicker-trigger").addClass("btn btn-default glyphicon glyphicon-calendar calander-icon pull-left");

                        jQuery('.txtDateTo').datepicker({
                            defaultDate: new Date(),
                            dateFormat: "<%=Affinion.LoyaltyBuild.Common.Utilities.DateTimeHelper.MonthDateFormat%>",
                            minDate: -180,
                            maxDate: 0,
                            numberOfMonths: 1,
                            showOn: "both",
                            buttonImage: "images/calendar.png",
                            buttonImageOnly: false,
                            buttonClass: "glyphicon-calendar"
                        }).next(".ui-datepicker-trigger").addClass("btn btn-default glyphicon glyphicon-calendar calander-icon pull-left");

                        $('#chkShowBooking').change(function () {
                            var status = $(this).is(":checked");
                            if (status)
                                $('#txtPartialPayment').prop("disabled", false);
                            else
                                $('#txtDateFrom').prop("disabled", true);
                        });
                    });
                </script>