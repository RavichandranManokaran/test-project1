﻿namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    #region Using Statements
    using Affinion.Loyaltybuild.BusinessLogic.Helper;
    using Affinion.LoyaltyBuild.Common;
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using LBDataAccess = Affinion.LoyaltyBuild.DataAccess.Models;
    using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
    using Affinion.LoyaltyBuild.Search.Helpers;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Linq;
    using System.Web.UI.WebControls;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Customer;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Web.ClientPortal.Helper;

    #endregion

    public partial class OfflinePortalAddCustomerQuery : BaseSublayout
    {

        #region private constant variable

        /// <summary>
        /// Client item path
        /// </summary>
        private const string ClientItemPath = "/sitecore/content/admin-portal/client-setup/{0}";
        Database contextDb = Sitecore.Context.Database;
        #endregion

        private QueryService _customerQueryService = new QueryService();
        private CustomerQuery _customerQuery;
        private string SelectText = "Please Select";
        private string _createdBy = string.Empty;
        static int customerId;
        #region Page Load
        private void Page_Load(object sender, EventArgs e)
        {
            Page.MaintainScrollPositionOnPostBack = false;
            _createdBy = SitecoreUserProfileHelper.GetItem(UserProfileKeys.CreatedBy);

            if (!IsPostBack)
            {
                InitPage();
            }
            ButtonInsert.Enabled = false;
        }
        #endregion

        #region protected Methods
        protected ValidationResponse ValidationResponse { get; private set; }
        protected void Search_Click(object sender, EventArgs e)
        {
            //add.Visible = false;
            //search.Visible = true;
        }

        protected void ButtonInsert_Click(object sender, EventArgs e)
        {

            ValidationResponse saveFlag = new ValidationResponse();
            if (!string.IsNullOrEmpty(CustomerId.Value))
            {
                if (Convert.ToInt32(CustomerId.Value) > 0)
                {
                    _customerQuery = new CustomerQuery();
                    _customerQuery.QueryTypeId = Convert.ToInt32(QueryType.SelectedValue);
                    _customerQuery.ClientId = ClientDropDown.SelectedValue;
                    _customerQuery.CampaignGroupID = ddlCompaignGroup.SelectedValue;
                    _customerQuery.ProviderId = ddlProvider.SelectedValue;
                    _customerQuery.Content = Notes.Value;
                    _customerQuery.IsSolved = IsSolved.SelectedItem.Value == "Yes" ? true : false;
                    _customerQuery.CreatedBy = _createdBy;
                    _customerQuery.CustomerId = Convert.ToInt32(CustomerId.Value);
                    saveFlag = _customerQueryService.SaveCustomerQuery(_customerQuery);
                    ValidationResponse = saveFlag;
                }
                else
                {
                    saveFlag.IsSuccess = false;
                    saveFlag.Message = "Invalid Customer";
                    ValidationResponse = saveFlag;
                }
            }
        }

        protected void Email_TextChanged(object sender, EventArgs e)
        {
            //if (!string.IsNullOrEmpty(Email.Text) && !string.IsNullOrWhiteSpace(Email.Text))
            //{
            //    SubscribeCheckBox.Enabled = true;
            //}

            ValidateFields();
        }

        protected void ClientDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ClientDropDown.SelectedIndex != 0)
            {
                StoreDropDown.Enabled = true;
                Database currentDB = Sitecore.Context.Database;
                Item clientItem = currentDB.GetItem(ClientDropDown.SelectedValue);

                if (clientItem != null)
                {
                    this.LoadClientSpecificDropDown("Branches", "BranchLocationName", "BranchLocationName", StoreDropDown, clientItem.Name);
                }

                ValidateFields();
            }
        }

        //protected void SearchReset_Click(object sender, EventArgs e)
        //{
        //    //ResetRearchForm();
        //}

        protected void Reset_Click(object sender, EventArgs e)
        {
            // ResetEditForm(true);
        }

        protected void ButtonSearch_Click(object sender, EventArgs e)
        {
            int countryId;
            int locationId;

            ValidationResponse search = new ValidationResponse();
            if (Page.IsValid)
            {
                var customer = new Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model.Customer();
                customer.FirstName = FirstNameSearch.Value;
                customer.LastName = LastNameSearch.Value;
                customer.AddressLine1 = AddressLine1Search.Value;
                customer.AddressLine2 = AddressLine2Search.Value;

                if(ClientDropDownSearch.SelectedIndex>0)
                    customer.ClientId = ClientDropDownSearch.SelectedValue;

                if (int.TryParse(CountryDropDown.SelectedValue, out countryId))
                {
                    customer.CountryId = countryId;
                }

                if (int.TryParse(LocationDropDown.SelectedValue, out locationId))
                {
                    customer.LocationId = locationId;
                }

               
                 search = _customerQueryService.SearchCustomer(customer);
                ValidationResponse = search;
                if (search.IsSuccess)
                    ButtonInsert.Enabled = true;
                else
                    ButtonInsert.Enabled = false;

                CustomerId.Value = Convert.ToString(search.Id);
            }
        }

        protected void ResultGridView_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        protected void AddAndSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                this.RecordCustomer();
            }
        }

        protected void EditAndSave_Click(object sender, EventArgs e)
        {
            //errormessage.Visible = false;

            if (Page.IsValid)
            {
                //   this.RecordCustomer(true);
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// initialize the page
        /// </summary>
        private void InitPage()
        {
            try
            {
                ValidationResponse = null;
                ValidationResponse msg = new ValidationResponse();
                msg.IsSuccess = true;
                msg.Message = "Search customer for Validations";
                ValidationResponse = msg;

                //Load Drop downs
                this.LoadDropDowns();
                //Load Regular Expression Validate
                this.LoadSitecoreFieldData();

                //Bind Table Headers

            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Load Drop Downs
        /// </summary>
        private void LoadDropDowns()
        {
            try
            {
                Item dataSourceitem = this.GetDataSource();
                this.LoadCountryDropDownSearch();
                this.LoadLocationDropDownSearch();
                this.LoadCountryDropDown();
                this.LoadLocationDropDown();
                this.LoadTitles();
                this.LoadClientDropDown();
                this.BindQueryType();
                this.BindClientDropDown();
                this.LoadClientDropDown();
                this.BindCompaignGroup(dataSourceitem);
                this.BindProvider();
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        /// <summary>
        /// LoadRegular Expression Validate
        /// </summary>
        private void LoadSitecoreFieldData()
        {
            try
            {
                //this.Search.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Search", "Search");
                ////this.SearchReset.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Reset", "Reset");
                //this.Reset.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Reset", "Reset");
                //this.SuccessLiteral.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Success", "Operation Succeeded");
                //this.SelectText = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "PleaseSelectText", "Please Select");

            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }

        }

        /// <summary>
        /// Fire validate
        /// </summary>
        private void ValidateFields()
        {
           // this.RegularExpressionValidatorSecondaryPhone.Validate();
           // this.RegularExpressionValidatorEmail.Validate();
           // this.RegularExpressionValidatorPhone.Validate();
           // this.RegularExpressionValidatorSecondaryPhone.Validate();
        }

        /// <summary>
        /// Write customer records to db
        /// </summary>
        private void RecordCustomer()
        {
            try
            {
                int result;
                ValidationResponse SaveFlag = new ValidationResponse();
                LBDataAccess.Customer customer = CreateCustomerObj();
                result = uCommerceConnectionFactory.AddCustomer(customer);
                if (result > 0)
                {
                    SaveFlag.IsSuccess = true;
                    SaveFlag.Message = "Customer added Suucessfully";
                }
                else
                {
                    SaveFlag.IsSuccess = false;
                    SaveFlag.Message = "Error occurred when customer failed";
                }
                ValidationResponse = null;
                ValidationResponse = SaveFlag;
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Populate Location DropDown
        /// </summary>
        private void LoadLocationDropDownSearch()
        {
            try
            {
                //Bind locationList data to the drop down
                LocationDropDownSearch.DataSource = uCommerceConnectionFactory.GetLocationList();
                LocationDropDownSearch.DataTextField = "Location";
                LocationDropDownSearch.DataValueField = "LocationTargetId";
                LocationDropDownSearch.DataBind();
                ListItem selectLocation = new ListItem(SelectText, string.Empty);
                LocationDropDownSearch.Items.Insert(0, selectLocation);
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }

        }

        /// <summary>
        ///  Populate Country DropDown
        ///// </summary>
        private void LoadCountryDropDownSearch()
        {
            try
            {
                //Get Ucommerce Country list
                var countryList = UCommerce.Api.TransactionLibrary.GetCountries();

                //Bind countryList data to the drop down
                CountryDropDownSearch.DataSource = UCommerceHelper.GetDropdownDataSource(countryList != null ? countryList.ToList() : null);
                CountryDropDownSearch.DataTextField = "Text";
                CountryDropDownSearch.DataValueField = "Value";
                CountryDropDownSearch.DataBind();
                ListItem selectCountry = new ListItem(SelectText, string.Empty);
                CountryDropDownSearch.Items.Insert(0, selectCountry);
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Load Client Specific Drop Down
        /// </summary>
        private void LoadClientSpecificDropDown(string dropdownFieldName, string targetItemsTextField, string targetItemsValueField, ListControl dropdown, string client)
        {
            try
            {
                //query for the client item in admin portal
                Item clientItem = Sitecore.Context.Database.GetItem("/sitecore/content/admin-portal/client-setup/" + client + "");
                if (clientItem != null)
                {
                    //Bind data to drop-down
                    Collection<Item> items = SitecoreFieldsHelper.GetMutiListItems(clientItem, dropdownFieldName);
                    dropdown.DataSource = SitecoreFieldsHelper.GetDropdownDataSource(items, targetItemsTextField, targetItemsValueField);
                    dropdown.DataTextField = "Text";
                    dropdown.DataValueField = "Value";
                    dropdown.DataBind();
                    //Set the default list item
                    ListItem select = new ListItem(SelectText, string.Empty);
                    dropdown.Items.Insert(0, select);
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Populate Titles
        ///// </summary>
        private void LoadTitles()
        {
            Sitecore.Data.Database currentDb = Sitecore.Context.Database;

            //Load location data to a locationList
            Item titleList = currentDb.GetItem(Constants.TitleListPath);

            if (titleList != null)
            {
                var titles = (from loc in titleList.Children
                              orderby loc.DisplayName
                              select loc.DisplayName);

                //Bind locationList data to the drop down
                TitleDropDown.DataSource = titles != null ? titles.ToList() : null;
                TitleDropDown.DataBind();
            }

            ListItem selectTitle = new ListItem(SelectText, string.Empty);
            TitleDropDown.Items.Insert(0, selectTitle);
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        private IList<string> GetClientList()
        {
            try
            {
                /// Get Client items
                //Sitecore.Data.Database currentDb = Sitecore.Context.Database;
                var clientItems = SearchHelper.GetUserSpecificClientItems(); //currentDb.SelectItems(ClientListQuery);

                if (clientItems != null)
                {
                    /// Extract Clients Filtered Items
                    var clientIds = (from client in clientItems
                                     orderby client.DisplayName
                                     select client.ID.Guid.ToString());
                    return clientIds != null ? clientIds.ToList() : null;
                }

                return null;
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Load Client Drop Down
        /// </summary>
        private void LoadClientDropDown()
        {
            /// Get Client items
            //Sitecore.Data.Database currentDb = Sitecore.Context.Database;
            var clientItems = SearchHelper.GetUserSpecificClientItems();//currentDb.SelectItems(ClientListQuery);

            if (clientItems != null)
            {
                /// Extract Clients Filtered Items
                var clientFilterList = (from client in clientItems
                                        orderby client.DisplayName
                                        select new { DiplayName = client.DisplayName, Id = client.ID.Guid });

                /// Load Client List Drop down
                this.ClientDropDown.DataSource = clientFilterList != null ? clientFilterList.ToList() : null;

                /// Set data text and data value fields
                ClientDropDown.DataTextField = "DiplayName";
                ClientDropDown.DataValueField = "Id";

                this.ClientDropDown.DataBind();
            }

            ListItem selectClient = new ListItem(SelectText, string.Empty);
            ClientDropDown.Items.Insert(0, selectClient);

        }

        private DataSet GetCustomerDetailes()
        {
            try
            {
                return uCommerceConnectionFactory.GetCustomerDetailes(CreateCustomerObj(), GetClientIdListString());
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }

        }

        /// <summary>
        /// Convert string list to comma separated string
        /// Prepare string list for SQL IN query
        /// </summary>
        /// <returns>comma separated string id list</returns>
        private string GetClientIdListString()
        {
            var clientList = GetClientList();
            string clientIdList = "{0}";//Dummy

            if (clientList != null)
            {
                //Convert string list to comma separated string as "{6C464DB8},{6451DB8}"
                clientIdList = string.Join(",", clientList);
            }

            return clientIdList;
        }

        /// <summary>
        /// Create Customer Obj
        /// </summary>
        /// <returns>Customer</returns>
        private LBDataAccess.Customer CreateCustomerObj()
        {
            try
            {
                string firstName = FirstName.Text;
                string lastName = LastName.Text;

                int countryId;
                int locationId;

                //id column 
                LBDataAccess.Customer customer = new LBDataAccess.Customer();
                customer.CustomerId = 0;
                customer.Title = TitleDropDown.SelectedValue;
                customer.FirstName = firstName.Length > 1 ? firstName.First().ToString().ToUpper().Trim() + firstName.Substring(1) : firstName.ToUpper().Trim();
                customer.LastName = lastName.Length > 1 ? lastName.First().ToString().ToUpper().Trim() + lastName.Substring(1) : lastName.ToUpper().Trim();
                customer.AddressLine1 = string.IsNullOrEmpty(AddressLine1.Text) ? null : AddressLine1.Text.Trim();
                customer.AddressLine2 = string.IsNullOrEmpty(AddressLine2.Text) ? null : AddressLine2.Text.Trim();
                customer.AddressLine3 = string.IsNullOrEmpty(AddressLine3.Text) ? null : AddressLine3.Text.Trim();
                customer.Phone = string.IsNullOrEmpty(SecondaryPhone.Text) ? null : SecondaryPhone.Text.Trim();
                customer.MobilePhone = string.IsNullOrEmpty(PrimaryPhone.Text) ? null : PrimaryPhone.Text.Trim();
                customer.EmailAddress = string.IsNullOrEmpty(Email.Text) ? null : Email.Text.Trim();
                customer.ClientId = ClientDropDown.SelectedValue;
                //customer.BranchId = StoreDropDown.SelectedValue;
                customer.Active = 1;
                customer.BookingReferenceId = string.Empty;

                if (int.TryParse(CountryDropDown.SelectedValue, out countryId))
                {
                    customer.CountryId = countryId;
                }

                if (int.TryParse(LocationDropDown.SelectedValue, out locationId))
                {
                    customer.LocationId = locationId;
                }

                return customer;
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Reset Search Form
        /// </summary>
        //private void ResetRearchForm()
        //{
        //    this.SearchFirstName.Text = string.Empty;
        //    this.SearchLastName.Text = string.Empty;
        //    this.SearchAddressLine1.Text = string.Empty;
        //    this.SearchPrimaryPhone.Text = string.Empty;
        //    this.LoadSearchLocationDropDown();
        //    this.LoadSearchCountryDropDown();
        //    this.divResult.Visible = false;
        //}

        ///// <summary>
        ///// Reset Edit form
        ///// </summary>
        //private void ResetEditForm(bool clearMessages = false)
        //{
        //    FirstName.Text = string.Empty;
        //    LastName.Text = string.Empty;
        //    AddressLine1.Text = string.Empty;
        //    AddressLine2.Text = string.Empty;

        //    this.LoadCountryDropDown();
        //    this.LoadLocationDropDown();
        //    this.LoadTitles();
        //    this.LoadClientDropDown();
        //    if (clearMessages)
        //    {
        //        successmessage.Visible = false;
        //        errormessage.Visible = false;

        //    }
        //}
        #endregion

        #region "Add Client"
        /// <summary>
        ///  Populate Search Country DropDown
        /// </summary>


        /// <summary>
        /// Populate Location DropDown
        /// </summary>
        private void LoadLocationDropDown()
        {
            try
            {
                //Bind locationList data to the drop down
                LocationDropDown.DataSource = uCommerceConnectionFactory.GetLocationList();
                LocationDropDown.DataTextField = "Location";
                LocationDropDown.DataValueField = "LocationTargetId";
                LocationDropDown.DataBind();
                ListItem selectLocation = new ListItem(SelectText, string.Empty);
                LocationDropDown.Items.Insert(0, selectLocation);
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }

        }

        /// <summary>
        ///  Populate Country DropDown
        /// </summary>
        private void LoadCountryDropDown()
        {
            try
            {
                //Get Ucommerce Country list
                var countryList = UCommerce.Api.TransactionLibrary.GetCountries();

                //Bind countryList data to the drop down
                CountryDropDown.DataSource = UCommerceHelper.GetDropdownDataSource(countryList != null ? countryList.ToList() : null);
                CountryDropDown.DataTextField = "Text";
                CountryDropDown.DataValueField = "Value";
                CountryDropDown.DataBind();
                ListItem selectCountry = new ListItem(SelectText, string.Empty);
                CountryDropDown.Items.Insert(0, selectCountry);
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        #endregion

        #region "DropDown  Member"
        private void BindData(Item dataSourceitem)
        {

        }

        //To bind the Customer Query type  
        private void BindQueryType()
        {
            DataTable customerQueryTypes = null;
            customerQueryTypes = _customerQueryService.GetCustomerQueryTypes();
            QueryType.DataSource = customerQueryTypes;
            QueryType.DataValueField = "QueryTypeId";
            QueryType.DataTextField = "QueryType";
            QueryType.DataBind();
            //Set the default list item
            ListItem select = new ListItem(SelectText, string.Empty);
            QueryType.Items.Insert(0, select);
        }

        //<summary>
        //Bind Client data to drop downs
        //</summary>
        /// <summary>
        /// Bind Client data to drop downs
        /// </summary>
        private void BindClientDropDown()
        {
            try
            {
                if (SearchHelper.GetUserSpecificClientItems() != null)
                {
                    /// Extract Clients Filtered Items
                    var clientFilterList = (from client in SearchHelper.GetUserSpecificClientItems()
                                            orderby client.DisplayName
                                            select new { DiplayName = client.DisplayName, ItemName = client.ID.Guid.ToString() }).ToList();

                    /// Load Client List Dropdown
                    this.ClientDropDownSearch.DataSource = clientFilterList;

                    /// Set data text and data value fields
                    ClientDropDownSearch.DataTextField = "DiplayName";
                    ClientDropDownSearch.DataValueField = "ItemName";

                    this.ClientDropDownSearch.DataBind();
                }

                this.ClientDropDownSearch.Items.Insert(0, Constants.PleaseSelectText);

                /// Assign respective client images to dropdown
                //foreach (ListItem clientListItem in ClientDropDownSearch.Items)
                //{
                //    if (!string.Equals(clientListItem.Text, Constants.PleaseSelectText))
                //    {
                //        Item clientItem = contextDb.GetItem(string.Format(ClientItemPath, clientListItem.Value));
                //        clientListItem.Attributes.Add("data-image", GetImageURL(clientItem, "Customer Service Image"));
                //    }
                //}
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, null);
                throw;
            }
        }



        private void BindCompaignGroup(Item dataSourceitem)
        {
            if (dataSourceitem != null)
            {
                Dictionary<string, string> campaignDatasource = new Dictionary<string, string>();
                string showAllText = SitecoreFieldsHelper.GetValue(dataSourceitem, "ShowAllLabel");
                if (SitecoreFieldsHelper.GetBoolean(dataSourceitem, "DisplayShowAllLabel"))
                {
                    ListItem showAllItem = new ListItem(showAllText, "");
                    this.ddlCompaignGroup.AppendDataBoundItems = true;
                    this.ddlCompaignGroup.Items.Insert(0, showAllItem);
                }

                Sitecore.Data.Fields.MultilistField offerGroupField = dataSourceitem.Fields["OfferGroups"];
                List<Sitecore.Data.ID> multiListTargetIds = (offerGroupField != null && !string.IsNullOrWhiteSpace(offerGroupField.Value)) ? offerGroupField.TargetIDs.ToList<Sitecore.Data.ID>() : null;
                foreach (var id in multiListTargetIds)
                {
                    Item campaignItem = Sitecore.Context.Database.GetItem(id);
                    if (campaignItem != null)
                    {
                        campaignDatasource.Add(campaignItem.ID.ToString(), campaignItem.Fields["Title"].Value);
                    }
                }

                this.ddlCompaignGroup.DataSource = campaignDatasource;
                this.ddlCompaignGroup.DataTextField = "Value";
                this.ddlCompaignGroup.DataValueField = "Key";
                this.ddlCompaignGroup.DataBind();
            }
        }

        private void BindProvider()
        {
            Dictionary<string, string> providerDatasource = new Dictionary<string, string>();

            ListItem showAllItem = new ListItem("Select a Provider", "");
            this.ddlProvider.AppendDataBoundItems = true;
            this.ddlProvider.Items.Insert(0, showAllItem);

            Item[] hotels = Sitecore.Context.Database.SelectItems(Constants.BasketPageQueryPath);
            foreach (var hotel in hotels)
            {
                if (hotel != null)
                {
                    providerDatasource.Add(hotel.ID.ToString(), hotel.Fields["Name"].Value);
                }
            }

            this.ddlProvider.DataSource = providerDatasource;
            this.ddlProvider.DataTextField = "Value";
            this.ddlProvider.DataValueField = "Key";
            this.ddlProvider.DataBind();
        }

        /// <summary>
        /// Method used to Get Image URL of Sitecore Image field
        /// </summary>
        /// <param name="currentItem"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        private string GetImageURL(Item currentItem, string fieldName)
        {
            string imageURL = string.Empty;
            Sitecore.Data.Fields.ImageField imageField = currentItem.Fields[fieldName];
            if (imageField != null && imageField.MediaItem != null)
            {
                Sitecore.Data.Items.MediaItem image = new Sitecore.Data.Items.MediaItem(imageField.MediaItem);
                imageURL = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image));
            }
            return imageURL;
        }
        #endregion

      

    }
}
