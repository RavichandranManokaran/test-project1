﻿using Affinion.LoyaltyBuild.SupplierPortal.Reports.Data;
using Affinion.LoyaltyBuild.SupplierPortal.Reports.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.SupplierPortal.Reports.Test
{
    public class TestReportService : IReportService
    {
        /// <summary>
        /// Get arrival report
        /// </summary>        
        /// <returns>List of booking</returns>
        public ArrivalReportCollection GetArrivalReport(ArrivalReportFilter arrivalReportFilter)
        {
            var data = new ArrivalReportCollection();

            data.Add(new ArrivalReport
            {                
                BookingReference = "sdasdads",
                OfferName = "This is a test offer",
                CollectAmount=5623,
                FirstName = "Test",
                LastName = "Last",
                Line1 = "Line 1",
                Line2 = "Line 2",
                PhoneNumber = "213123213",
                NumberOfAdults = 2,
                NumberOfChildren = 0,
                MobilePhoneNumber = "12312321",
                Arrival = DateTime.Now,
                Departure = DateTime.Now,
                ReservationDate = DateTime.Now,
                BookingThrough="call center",
                BookingStatus = "Confirmed",
                Accommodation = "1 Upgrade double",
                BookingStatusDate=DateTime.Now,
                RowColor="Pink",
                Note="Confirmed availability"
            });
            data.Add(new ArrivalReport
            {
                BookingReference = "sdasdads",
                OfferName = "My 2 nd offer",
                CollectAmount=98645,
                FirstName = "Rajesh",
                LastName = "Ganesh",
                Line1 = "No line 2",
                Line2 = "adrline2",
                PhoneNumber = "865876",
                NumberOfAdults = 2,
                NumberOfChildren = 3,
                ChildAge = "4,5,8",               
                MobilePhoneNumber = "368379357",
                Arrival = DateTime.Now,
                Departure = DateTime.Now,
                ReservationDate = DateTime.Now,
                BookingThrough="online",
                BookingStatus = "Confirmed",
                BookingStatusDate = DateTime.Now,
                Accommodation = "4* Upgrade double",                
                RowColor="Pink"
            });
            data.Add(new ArrivalReport
            {
                BookingReference = "sdasdads",
                OfferName = "Offer my offer 3 nd offer",
                CollectAmount=986458,
                FirstName = "Raj",
                LastName = "Ganesh",
                Line1 = "No line 2",
                Line2 = "No line 3",
                PhoneNumber = "523573",
                NumberOfAdults = 2,
                NumberOfChildren = 3,
                ChildAge = "7,8,15",
                MobilePhoneNumber = "968985445",
                Arrival = DateTime.Now,
                Departure = DateTime.Now,
                ReservationDate = DateTime.Now,
                BookingThrough = "online",
                BookingStatus = "Confirmed",
                BookingStatusDate = DateTime.Now,
                Accommodation = "1 family"
            });
            data.Add(new ArrivalReport
            {
                BookingReference = "sdasdads",
                OfferName = "This is a test offer 4",
                CollectAmount=6578,
                FirstName = "AAA",
                LastName = "LastBBB",
                Line1 = "Line 1",
                Line2 = "Line 2",
                PhoneNumber = "6565123213",
                NumberOfAdults = 2,
                NumberOfChildren = 0,
                MobilePhoneNumber = "12312321",
                Arrival = DateTime.Now,
                Departure = DateTime.Now,
                ReservationDate = DateTime.Now,
                BookingThrough = "online",
                BookingStatus = "Confirmed",
                Accommodation = "4* Family",
                BookingStatusDate = DateTime.Now,
                Note="Confirmed availability"
            });
            data.Add(new ArrivalReport
            {
                BookingReference = "sdasdads",
                OfferName = "This is a test offer 5",
                CollectAmount=20000,
                FirstName = "Xyz",
                LastName = "Lastxyz",
                Line1 = "Line 1",
                Line2 = "Line 2",
                PhoneNumber = "25375713",
                NumberOfAdults = 2,
                NumberOfChildren = 2,
                ChildAge="8,6",
                MobilePhoneNumber = "123152781",
                Arrival = DateTime.Now,
                Departure = DateTime.Now,
                ReservationDate = DateTime.Now,
                BookingThrough="call center",
                BookingStatus = "Confirmed",
                Accommodation = "4* Standard",
                BookingStatusDate = DateTime.Now,
                RowColor = "Lavender",
                Note="Confirmed availability"
            });

            return data;
        }

        /// <summary>
        /// Get restricted rooms report 
        /// </summary>
        /// <param name="RestrictedRoomsFilter"></param>        
        /// <returns>List of restricted rooms</returns>
        public RestrictedRoomsCollection GetRestrictedRooms(RestrictedRoomsFilter restrictedRoomsFilter)
        {
            var data = new RestrictedRoomsCollection();
            data.Add(new RestrictedRoomsReport
                {
                    HotelName = "101",
                    OfferName="1 night",
                    Month=DateTime.Now,
                    DatesRestricted="10,20,30",

                });
            data.Add(new RestrictedRoomsReport
            {
                HotelName = "102",
                OfferName = "1 night+Dinner",
                Month = DateTime.Now,
                DatesRestricted = "15,20,22",

            });
            return data;
            //throw new NotImplementedException();
        }

        /// <summary>
        /// Get availability report 
        /// </summary>
        /// <param name="AvailabilityReportFilter"></param>
        /// <returns>List of available rooms</returns>
        public AvailabilityCollection GetAvailability(AvailabilityReportFilter availabilityReportFilter)
        {
            var data = new AvailabilityCollection();
            data.Add(new AvailabilityReprot
                {
                    HotelName = "1004",
                    RoomType="Queen",
                    Month=DateTime.Now,
                    SoldOutDates="2,4,8,16,22",
                    ClosedOutDates="26,27",

                });
            data.Add(new AvailabilityReprot
            {
                HotelName = "1243",
                RoomType="King",
                Month=DateTime.Now,
                SoldOutDates="3,4,8,15,17,23",
                ClosedOutDates="29,30",

            });
            return data;
            //throw new NotImplementedException();
        }

        /// <summary>
        /// Get Revenue report 
        /// </summary>
        /// <param name="RevenueReportFilter"></param>
        /// <returns>List of Revenue report for total revenue</returns>
        public RevenueReportCollection GetRevenueReport(RevenueReportFilter revenueReportFilter)
        {
           var data = new RevenueReportCollection();
            data.Add(new RevenueReport
                {
                    OfferName="First Offer",
                    NumberOfBookings = 140,
                    TotalRevenue = 50,
                    Date=DateTime.Now,
                });

            data.Add(new RevenueReport
            {
                OfferName="Second Offer",
                NumberOfBookings = 450,
                TotalRevenue = 100,
                Date=DateTime.Now,
            });
            data.Add(new RevenueReport
            {
                OfferName="Third Offer",
                NumberOfBookings = 540,
                TotalRevenue = 150,
                Date = DateTime.Now,
            });
            data.Add(new RevenueReport
            {
                OfferName="Offer 4",
                NumberOfBookings = 190,
                TotalRevenue = 80,
                Date = DateTime.Now,
            });
            return data;
            //throw new NotImplementedException();
        }

        /// <summary>
        /// Get Revenue report
        /// </summary>
        /// <param name="RevenueReportFilter"></param>
        /// <returns>List of Revenue report for List Revenue</returns>
        public RevenueReportbyProviderCollection GetRevenueReportbyProvider(RevenueReportFilter revenueReportFilter)
        {
            var data = new RevenueReportbyProviderCollection();
            data.Add(new RevenueReportbyProvider
                {
                    HotelName = "Armada Hotel",
                    OfferName="First offer",
                    NumberOfBookings = 1370,
                    TotalRevenue = 126,
                    Date=DateTime.Now,

                });
            data.Add(new RevenueReportbyProvider
            {
                HotelName = "Amirtha Hotel",
                OfferName="Second offer",
                NumberOfBookings = 1670,
                TotalRevenue = 150,
                Date=DateTime.Now,

            });
            data.Add(new RevenueReportbyProvider
            {
                HotelName = "ITC",
                OfferName="Offer 3",
                NumberOfBookings = 180,
                TotalRevenue = 16,
                Date = DateTime.Now,

            });
            data.Add(new RevenueReportbyProvider
            {
                HotelName = "Trident",
                OfferName="Fourth Offer",
                NumberOfBookings = 1960,
                TotalRevenue = 1265,
                Date = DateTime.Now,

            });
            data.Add(new RevenueReportbyProvider
            {
                HotelName = "Arm Hotel",
                OfferName="Offer five",
                NumberOfBookings = 170,
                TotalRevenue = 856,
                Date = DateTime.Now,

            });
            return data;
            // throw new NotImplementedException();
        }

    }
}