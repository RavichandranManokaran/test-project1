﻿using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Customer;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Email;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Order;
//<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           OfflinePortalCommunicationHistroySublayout.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.OfflinePortal
/// Description:           Used to Bind data to OfflinePortalCommunicationHistroySublayout subLayout
/// </summary>
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Affinion.LoyaltyBuild.Communications;
using Affinion.LoyaltyBuild.Communications.Services;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic;
using Affinion.LoyaltyBuild.Api.Booking.Service;
using Affinion.LoyaltyBuild.Model.Product;


namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    public partial class OfflinePortalCommunicationHistroySublayout : BaseSublayout
    {

        #region Properties
        private IOrderService _orderService;
        private int _orderLineId;
        private ICommunicationHistoryService communicationHistoryService;

        protected List<CommunicationHistoryInfo> CommunicationHistoryDetails { get; set; }
        protected ValidationResponse ValidationResponse { get; private set; }

        public EmailService email;
        public MailService mail;

        public Booking boooking;
        public CommunicationHistoryService historyService;
        
        
        //_orderLineId = 0;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            _orderService = ContainerFactory.Instance.GetInstance<IOrderService>();

            
            // mail = new MailService("3D8A574F-933A-4AB8-BF61-370283B38AA2");

            int olid;
            int.TryParse(this.QueryString("olid"), out olid);
            _orderLineId = olid;
            boooking = _orderService.GetBooking(_orderLineId);
            BookingService book = new BookingService();
            var bedBanksBooking = book.GetBooking(_orderLineId);
            if(bedBanksBooking!=null && bedBanksBooking.Type==ProductType.BedBanks)
            {
                btn_regenerateProviderEmail.Disabled = true;
            }
            mail = new MailService(boooking.ClinetId);
            communicationHistoryService = new CommunicationHistoryService();
            CommunicationHistoryDetails = communicationHistoryService.GetCommunicationHistoryByOrderLineID(_orderLineId);
        }

        protected void btn_regenerateClientEmail_Click(object sender, EventArgs e)
        {
            mail.GenerateEmail(MailTypes.CustomerConfirmation, _orderLineId);
            if (boooking.IsCancelled)
            {
                mail.GenerateEmail(MailTypes.CustomerCancellation, _orderLineId);
            }
        }

        protected void btn_regenerateProviderEmail_Click(object sender, EventArgs e)
        {
            mail.GenerateEmail(MailTypes.ProviderConfirmation, _orderLineId);
            if (boooking.IsCancelled)
            {
                mail.GenerateEmail(MailTypes.ProviderCancellation, _orderLineId);
            }
        }

        protected void btn_resend_Click(object sender, EventArgs e)
        {
            try
            {
                historyService = new CommunicationHistoryService();
                List<CommunicationHistoryInfo> comminfo = new List<CommunicationHistoryInfo>();
                comminfo = historyService.GetCommunicationHistoryByOrderLineID(_orderLineId);
                string fromEmail = comminfo.FirstOrDefault().From;
                string toEmail = comminfo.FirstOrDefault().To;
                string subject = comminfo.FirstOrDefault().Subject;
                string emailText = comminfo.FirstOrDefault().EmailHtmlText;
                email = new EmailService();
                email.Send(fromEmail, toEmail, subject, emailText, true);
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.BusinessLogic, ex, this);
            }
        }
    }
}