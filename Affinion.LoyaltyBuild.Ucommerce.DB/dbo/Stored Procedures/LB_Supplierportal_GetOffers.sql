﻿CREATE PROCEDURE [dbo].[Lb_supplierportal_getoffers] 

@ProductSku nvarchar(30) 

AS 
  BEGIN 
      SELECT DISTINCT CI.campaignitemid, 
                      CI.NAME 
      FROM   uCommerce_Product p 
             INNER JOIN LB_OfferAvailability OA on p.Sku=OA.Sku

 INNER JOIN uCommerce_CampaignItem CI on CI.CampaignItemId =OA.CampaignItem

 WHERE @ProductSku=p.Sku
  END 