﻿
/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           ThemeSelector.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common.Utilities
/// Description:           Theme Selector clas for client portal and offline portal
/// </summary>

#region Using Directives
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#endregion

namespace Affinion.LoyaltyBuild.Common.Utilities
{
    public class ThemeSelector
    {
        /// <summary>
        /// Retur the selected theme in client portal
        /// </summary>
        /// <returns></returns>
        public static string LoadSelectedTheme()
        {
            Item currentItem = Sitecore.Context.Item;
            Item root = ItemHelper.RootItem;
            string pageTheme = GetThemeName(root);
            return pageTheme;
        }

        /// <summary>
        /// Get the selected theme name
        /// </summary>
        /// <param name="rootItem"></param>
        /// <returns></returns>
        private static string GetThemeName(Item rootItem)
        {
            Item themeItem = SitecoreFieldsHelper.GetDropLinkFieldTargetItem(rootItem, "ThemeDetails");
            string theme = SitecoreFieldsHelper.GetValue(themeItem, "Title", "theme1");
            return theme;
        }
    }
}
