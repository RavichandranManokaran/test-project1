﻿using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.SupplierPortal.DataAccess;
using Affinion.LoyaltyBuild.SupplierPortal.DataAccess.Service;
using Affinion.LoyaltyBuild.SupplierPortal.DataAccess.Test;
using Affinion.LoyaltyBuild.SupplierPortal.Reports.Filters;
using Affinion.LoyaltyBuild.SupplierPortal.Reports.Service;
using Affinion.LoyaltyBuild.SupplierPortal.Reports.Test;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration.SupplierPortal.Reports;
using Microsoft.Reporting.WebForms;
using Sitecore.Data.Items;
using System;
using System.Reflection;
using System.Security;
using System.Security.Permissions;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Reports
{
    /// <summary>
    /// Arival report sublayout user control
    /// </summary>
    public partial class ArrivalReport : ReportBase
    {
        protected readonly IOfferService _offerService;

        public ArrivalReport()
            : base(new ProviderService(), new ReportService())
        {
            _offerService = new OfferService();
        }

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            BindSitecoreText();
            
            if (!Page.IsPostBack && !Page.IsCallback)
            {
                //load the provider dropdown
                BindDropdown(ddlProvider, ProviderList, "Name", "SitecoreId");
                dateFromCalendarExtender.StartDate = DateTime.Now.AddMonths(-12);
                dateToCalendarExtender.EndDate = DateTime.Now.AddMonths(12);
            }
        }
        /// <summary>
        /// Show report click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnList_Click(object sender, EventArgs e)
        {
            DateTime Datefrom = Convert.ToDateTime(txtDatefrom.Text);
            DateTime DateTo = Convert.ToDateTime(txtDateTo.Text);
            lblHotelname.Text = ddlProvider.SelectedItem.Text;

            if (Datefrom <= DateTo)
            {
                TimeSpan daycount = Convert.ToDateTime(DateTo).Subtract(Convert.ToDateTime(Datefrom));
                int dacount1 = Convert.ToInt32(daycount.Days);
                if ((dacount1 < 366))
                {
                    CreateArrivalReport();           
                }
                else
                {                    
                    cusValDateTo.IsValid = false;
                                     
                }
                
            }
            
        }

        private void CreateArrivalReport()
        {
            //get the report assembly to load report definition of rdlc files
            Assembly assembly = Assembly.GetAssembly(typeof(Affinion.LoyaltyBuild.SupplierPortal.Reports.IReportService));

            //get booking id
            var bookingId = GetStringValue(txtbookingInfor.Text);
            //get client name
            var clientName = GetStringValue(txtClientName.Text);
            //get date from
            var dateFrom = GetDateValue(txtDatefrom.Text);
            //get date to
            var dateTo = GetDateValue(txtDateTo.Text);

            //get offer id
            var offerId = GetIntValue(ddlOfferName.SelectedValue);
            //get provider id
            int? providerId = GetIntValue(ddlProvider.SelectedValue);

            var reportandOrderby = GetStringValue(ddlReportOrder.SelectedValue);

            using (var stream = assembly.GetManifestResourceStream("Affinion.LoyaltyBuild.SupplierPortal.Reports.Reports.ArrivalReport.rdlc"))
            {
                //bookingId, clientName, dateFrom, offerId, dateTo, providerId, null
                var objTransferredIn = _reportService.GetArrivalReport(new ArrivalReportFilter()
                {
                    BookingReference = bookingId,
                    ClientName = clientName,
                    DateFrom = dateFrom,
                    OfferId = offerId,
                    DateTo = dateTo,
                    Provider = providerId,
                    ReportOrderBy = reportandOrderby,
                    BookingStatus=ddlBookingStatus .SelectedValue
                });
                this.rvShowRecord.KeepSessionAlive = false;
                this.rvShowRecord.AsyncRendering = false;
                rvShowRecord.ProcessingMode = ProcessingMode.Local;
                var datasourcein = new ReportDataSource("ArrivalReport", objTransferredIn);
                rvShowRecord.LocalReport.SetBasePermissionsForSandboxAppDomain(new PermissionSet(PermissionState.Unrestricted));
                rvShowRecord.LocalReport.LoadReportDefinition(stream);
                rvShowRecord.LocalReport.SetParameters(new ReportParameter("RowCount", objTransferredIn.Count.ToString()));
                rvShowRecord.LocalReport.DataSources.Clear();
                rvShowRecord.LocalReport.DataSources.Add(datasourcein);
                rvShowRecord.PageCountMode = PageCountMode.Actual;
            }

        }

        /// <summary>
        /// Provider dropdown change event to load the offers based on provider
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlProvider_SelectedIndexChanged(object sender, EventArgs e)
        {
           // var offers = _offerService.GetParticipatingOffersByProvider(int.Parse(ddlProvider.SelectedValue));
            
            //BindDropdown(ddlOfferName, offers, "Name", "Id");
            string productSku=string.Empty;
            if (!String.IsNullOrWhiteSpace(ddlProvider.SelectedValue) && ddlProvider.SelectedValue.Length>1)
            {
                Item selectedProviderItem=Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(ddlProvider.SelectedValue));
                if(selectedProviderItem!=null)
                {
                    productSku=SitecoreFieldsHelper.GetItemFieldValue(selectedProviderItem,"SKU");
                }
            }

            if (!string.IsNullOrWhiteSpace(productSku))
            {
                ddlOfferName.DataSource = _offerService.GetParticipatingOffersByProvider(productSku);
                ddlOfferName.DataTextField = "Name";
                ddlOfferName.DataValueField = "Id";
                ddlOfferName.DataBind();
                ddlOfferName.Items.Insert(0, new ListItem { Text = "-All-", Value = "0" });
            }
        }
        public void BindSitecoreText()
        {
            Item currentPageItem = Sitecore.Context.Item;
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Title",ArrivalReportText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Arrival Report Information", ArrivalReportInformation);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "BookingReference", BookingReferenceText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "ClientName", ClientNameText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "FirstOrLastNameText", FirstNameOrLastText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "DateFrom", DateFromText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "DateTo", DateToText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "OfferName", OfferNameText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Report and Orderby", ReportAndOrderByText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Provider", ProviderText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Booking Status", BookingStatusText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Submit", btnListText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Never Transferred", NeverTransferredText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Transferred In", TransferredInText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Transferred Out", TransferredOutText);





        }
    }
}
