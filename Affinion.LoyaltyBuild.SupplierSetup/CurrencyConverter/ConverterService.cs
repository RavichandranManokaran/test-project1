﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           JsonReader.cs
/// Sub-system/Module:     Suplier Setup(VS project name)
/// Description:           code will be used to Currency convension
/// </summary>


#region Using Directives
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.SupplierSetup.CurrencyCoverter;
using System;
using System.Collections.Generic;
using System.Linq;

#endregion


namespace Affinion.LoyaltyBuild.SupplierSetup.CurrencyConverter
{
    public class ConverterService : ICurrencyConverter
    {
        #region Public Methods
        /// <summary>
        /// Convert base on from and to and return Decimal value
        /// </summary>
        /// <param name="fromCurrency">From</param>
        /// <param name="toCurrency">To</param>
        /// <returns></returns>
        public decimal Convert(string fromCurrency, string toCurrency)
        {
            try
            {
                //reader class
                JsonReader reader = new JsonReader();
                //query Exchange rate
                var Currencydata = from cc in reader.getResponse.Currencylist
                                   where cc.Base.Equals(fromCurrency) && cc.Quote.Equals(toCurrency)
                                   select cc.Value;

                //list
                List<decimal> decimalValues = Currencydata.ToList<decimal>();
                //element value
                decimal result = decimalValues.Count > 0 ? decimalValues[0] : 0;
                return result;

            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.SupplierSetup, ex, this);
                throw;
            }


        }
        #endregion



        //public string Token
        //{
        //    get { throw new NotImplementedException("Yet to be implimented"); }
        //}

        //decimal ICurrencyConverter.Convert(float value, string from, string to)
        //{
        //    throw new NotImplementedException("Yet to be implimented");
        //}

        //public void SetCulture(System.Globalization.CultureInfo culture)
        //{
        //    throw new NotImplementedException("Yet to be implimented");
        //}

        //public void Convert(float value)
        //{
        //    throw new NotImplementedException("Yet to be implimented");
        //}

        //public void Convert(float value, string from)
        //{
        //    throw new NotImplementedException("Yet to be implimented");
        //}

        //public decimal Convert(float value, string from, string to)
        //{
        //    throw new NotImplementedException("Yet to be implimented");
        //}

        //public void Convert(float value, string from, string to1, string to2, string to3, string mid)
        //{
        //    throw new NotImplementedException("Yet to be implimented");
        //} 

    }
}
