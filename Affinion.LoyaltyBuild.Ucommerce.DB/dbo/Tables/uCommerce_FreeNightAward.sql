﻿CREATE TABLE [dbo].[uCommerce_FreeNightAward] (
    [FreeNightAwardId] INT NOT NULL,
    [NoOfNight]        INT NOT NULL,
    CONSTRAINT [PK_uCommerce_FreeNightAward] PRIMARY KEY CLUSTERED ([FreeNightAwardId] ASC),
    CONSTRAINT [FK_uCommerce_FreeNightAward_uCommerce_Award] FOREIGN KEY ([FreeNightAwardId]) REFERENCES [dbo].[uCommerce_Award] ([AwardId])
);

