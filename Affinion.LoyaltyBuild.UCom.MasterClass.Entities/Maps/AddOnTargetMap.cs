﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class AddOnTargetMap : SubclassMap<AddOnTarget>
    {
        // Methods
        public AddOnTargetMap()
        {
            base.Table("LB_AddOnTarget");
            base.KeyColumn("AddOnTargetId");
            base.Map(x => x.AddOnTargetId).ReadOnly();
            base.Map(x => x.AddOn).Not.Nullable();
        }

    }

}
