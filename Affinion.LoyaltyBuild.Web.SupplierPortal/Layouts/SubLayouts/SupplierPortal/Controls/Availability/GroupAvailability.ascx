﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GroupAvailability.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Availability.GroupAvailability" %>

<div class="body-container">
    <div class="container">
        <div class="form-outer">
            <div class="col-md-12">
                <div class="panel panel-default" style="border: none;">
                    <div class="middle-body-container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-3 no-padding">
                                    <h4 class="cal-header">Group Availability</h4>
                                </div>
                                <div class="col-md-9 no-padding">
                                    <div class="col-sm-1 col-xs-1 no-padding-right"></div>
                                    <div class="col-sm-2 col-xs-2 no-padding-right">
                                        <span class="glyphicon glyphicon-th-large"></span>
                                        <a href="#" title="Color Code" data-trigger="hover" data-toggle="popover" data-placement="bottom">Color Code </a>
                                        <div id="colorcodeContent" style="display: none">
                                            <p><b style="color: green">Green</b> - Room type or date has NO limitations or restrictions</p>
                                            <p><b style="color: blue">Blue</b> -  Room type or date has ANY limitations or restrictions</p>
                                            <p><b style="color: Red">Red </b>- Room Type or date has been CLOSED OUT</p>
                                            <p><b style="color: yellow">Yellow</b> - Room type or date has been SOLD</p>
                                        </div>
                                        <script>
                                            $('[data-toggle=popover]').popover({
                                                content: $('#colorcodeContent').html(),
                                                html: true
                                            })
                                            .click(function () {
                                                $(this).popover('show');
                                            });
                                        </script>
                                    </div>
                                    <div class="col-sm-2 col-xs-2 no-padding-right"><b>Choose Date:</b></div>
                                    <div class="col-sm-3 col-xs-3 no-padding-right ">
                                        <span class="bask-form-titl pull-left">From:</span>
                                        <%--<input type="text" id="datepicker1" class="form-control pull-left" />--%>
                                       <%--  <asp:TextBox ID="txtStartDate" runat="server" CssClass="form-control pull-left Datepicker" ToolTip="Start Date"></asp:TextBox>--%>
                                        <input readonly="true" type="text" id="datepickerFrom" placeholder="DD/MM/YYYY" runat="server" class="form-control Datepicker" clientidmode="Static" />
                                        <asp:RequiredFieldValidator ID="rfvStartDate" runat="server" ControlToValidate="datepickerFrom" ErrorMessage="*Required" ValidationGroup="vgp"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-sm-3 col-xs-3 no-padding-right">
                                        <span class="bask-form-titl  pull-left">To:</span>
                                        <%--<input type="text" id="datepicker2" class="form-control pull-left" />--%>
                                       <%--  <asp:TextBox ID="txtEndDate" runat="server" CssClass="form-control pull-left Datepicker1" ToolTip="End Date"></asp:TextBox>--%>
                                        <input readonly="true" type="text" id="datepickerTo" placeholder="DD/MM/YYYY" runat="server" class="form-control Datepicker1" clientidmode="Static" />
                                        <input readonly="true" type="hidden" id="serverdate" placeholder="DD/MM/YYYY" runat="server" class="form-control" clientidmode="Static" />
                                        <asp:RequiredFieldValidator ID="rfvEndDate" runat="server" ControlToValidate="datepickerTo" ErrorMessage="*Required" ValidationGroup="vgp"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-sm-1 col-xs-1 no-padding-right">
                                        <%--<button type="button" class="btn  btn-info">View <span class="glyphicon glyphicon-right-arrow" aria-hidden="true"></span></button>--%>
                                        <button type="button" class="btn  btn-info" onclick="Supplier.ManageAvailability.ViewGroupHotelAvailabilityByDateRange(0)">View <span class="glyphicon glyphicon-right-arrow" aria-hidden="true"></span></button>
                                       <%-- <asp:Button ID="btnView" runat="server" CssClass="btn  btn-info" Text="View" OnClick="btnView_Click" ValidationGroup="vgp" />--%>
                                    </div>
                                </div>
                            </div>
                        </div>
<div class="container-outer width-full overflow-hidden-att app-bg">
    <div class="row">
        <div class="col-md-12"></div>
    </div>
    <div class="middle-body-container">
        <div class="row">
            <div class="col-md-12">
                 <h4 class="cal-header"></h4>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="table-cal-header">
                    <div class="col-md-2 no-padding" id="leftbar">
                      
                        <div class="offers-content">
                            <%-- <input type="hidden" name="offerurl" id="offerurl" runat="server" clientidmode="Static" />--%>
                            <input type="hidden" name="pagetype" id="pagetype" value="manageavailability" clientidmode="Static" />
                            <%-- <asp:Panel ID="dvOfferContent" runat="server" Visible="false" ClientIDMode="Static">
                            </asp:Panel>--%>
                        </div>
                    </div>
                    <div class="col-md-10 no-padding-right bindclick" id="page-content-wrapper2">
                        <div class="row">
                            <div class="col-md-12">
                                <div class=" cal-table ">
                                    
                                </div>
                            </div>

                            
                        <div id="printable" class="padding-top12">
                            <div class="cal-table-top no-padding btnpanel" style="display: none">
                                <div class="col-md-12 padding0">
                                  
                                </div>
                            </div>
                            <div id="dv_0">
                                <asp:Panel ID="dvContent" runat="server" ClientIDMode="Static">
                                </asp:Panel>
                            </div>
                            <div class="cal-table-top btnpanel" style="display: none">
                                <div class="col-md-12 padding0">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   <script>
       $(document).ready(function () {
           $(".Datepicker,.Datepicker1").datepicker({
               showOn: "both",
               //buttonImage: Rgen.Config.DatePicker.Image,
               buttonImage: '/Resources/datePicker/images/calendar.gif',
               buttonImageOnly: true
           });
       });
   </script>
