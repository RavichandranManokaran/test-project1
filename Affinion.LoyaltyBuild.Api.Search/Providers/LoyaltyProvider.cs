﻿using Affinion.LoyaltyBuild.Api.Search.Data;
using Affinion.LoyaltyBuild.Api.Search.Filters;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Api.Search.Providers
{
    /// <summary>
    /// Loyaltybuild provider service
    /// </summary>
    public class LoyaltyProvider : ISearchProvider
    {
        /// <summary>
        /// Search providers based on filter
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        public ProviderCollection SearchProviders(IList<ISearchFilter> filters)
        {
            var criteria = new SearchCriteria();

            filters.Any(i =>
            {
                ILoyaltyFilter loyaltyFilter = i as ILoyaltyFilter;

                if (loyaltyFilter != null)
                    loyaltyFilter.BuildSearch(ref criteria);
                return false;
            });

            var hotels = LoyaltyBuildCatalogLibrary.SearchHotels(criteria);
            ProviderCollection lbProviderCollection = ConvertHotelDetailListToProviderCollection(hotels);
            return lbProviderCollection;
        }

        /// <summary>
        /// Search providers based on filter asyncronusly
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        public async Task<ProviderCollection> SearchProvidersAsync(IList<ISearchFilter> filters)
        {
            return await Task.Run<ProviderCollection>(() =>
                SearchProviders(filters));
        }

        public ProviderCollection ConvertHotelDetailListToProviderCollection(List<HotelDetail> lbHotels)
        {
            ProviderCollection lbProviderCollection = new ProviderCollection(ServiceType.LoyaltyBuild);;
            foreach (var lbHotelDetailList in lbHotels)
            {
                foreach (var lbHotelProductDetail in lbHotelDetailList.HotelProductDetail)
                {
                    SearchResult searchResultItem = new SearchResult();
                    searchResultItem.ProviderId = lbHotelProductDetail.Sku;
                    searchResultItem.ProviderType = ServiceType.LoyaltyBuild;
                    searchResultItem.SitecoreItemId = lbHotelDetailList.ItemId;
                    lbProviderCollection.Add(searchResultItem);
                }
            }
            return lbProviderCollection;
        }
    }
}
