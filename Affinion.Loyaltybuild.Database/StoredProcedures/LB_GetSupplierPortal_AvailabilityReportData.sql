﻿CREATE PROCEDURE [dbo].[LB_GetSupplierPortal_AvailabilityReportData] 
															@DateFrom DATE,
															@DateTo DATE,
															@Provider INT
  AS
  BEGIN
  
		SELECT
      Category.Categoryid [HotelId],
      Category.Name [HotelName],
      Product.Name [RoomType],
      CAST('1/1/1990' AS Datetime) + RoomAvailability.DateCounter [Date],
      1 [ClosedOutDate],
	  0 [SoldOutDate]
    FROM
      Ucommerce_product Product 
	  INNER JOIN Ucommerce_categoryproductrelation Categoryproductrelation ON Product.Productid = Categoryproductrelation.Productid
	  INNER JOIN Ucommerce_category Category ON Categoryproductrelation.Categoryid = Category.Categoryid	
      INNER JOIN LB_RoomAvailability RoomAvailability ON Product.Sku  = RoomAvailability.[ProviderOccupancyTypeID]
    WHERE
      RoomAvailability.Iscloseout = 1
      AND (CAST('1/1/1990' AS Datetime) + RoomAvailability.DateCounter between @DateFrom and @DateTo)
      AND (@Provider=0 OR @Provider IS NULL OR @Provider = Category.Categoryid)
    UNION
    SELECT
      Category.Categoryid [HotelId],
      Category.Name [Hotel Name],
      Product.Name [Room Type],
      CAST('1/1/1990' AS Datetime) + RoomAvailability.DateCounter [Date],
      0 [ClosedOutDate],
	  1 [SoldOutDate]
    FROM
	  Ucommerce_product Product 
	  INNER JOIN Ucommerce_categoryproductrelation Categoryproductrelation ON Product.Productid = Categoryproductrelation.Productid
	  INNER JOIN Ucommerce_category Category ON Categoryproductrelation.Categoryid = Category.Categoryid	
      INNER JOIN LB_RoomAvailability RoomAvailability ON Product.Sku  = RoomAvailability.[ProviderOccupancyTypeID]
    WHERE
      RoomAvailability.Numberallocated = RoomAvailability.Numberbooked
      AND (CAST('1/1/1990' AS Datetime) + RoomAvailability.DateCounter between @DateFrom and @DateTo)
      AND (@Provider=0 OR @Provider IS NULL OR @Provider = Category.Categoryid)
	  AND RoomAvailability.Iscloseout =0
	
  END