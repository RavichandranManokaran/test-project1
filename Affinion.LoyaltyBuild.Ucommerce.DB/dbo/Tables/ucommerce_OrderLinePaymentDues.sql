﻿CREATE TABLE [dbo].[ucommerce_OrderLinePaymentDues] (
    [PaymentDueId]       BIGINT IDENTITY (1, 1) NOT NULL,
    [OrderLinePaymentId] INT    NULL,
    [OrderLineDueId]     INT    NULL,
    [Amount]             MONEY  NULL,
    [DueTypeId]          INT    NULL
);

