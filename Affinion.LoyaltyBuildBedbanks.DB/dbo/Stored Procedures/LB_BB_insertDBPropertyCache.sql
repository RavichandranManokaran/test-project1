CREATE PROCEDURE [dbo].[LB_BB_insertDBPropertyCache]
	--
	@pi_availabilityProviderID int,
	@pi_propertyID int,
	@pi_xmlresponse varchar(Max),
	@pi_expirytime datetime,
	@pi_providerName varchar(200),
    @pi_addressline1 varchar(250),
    @pi_addressline2 varchar(250),
    @pi_addressline3 varchar(250),
    @pi_addressline4 varchar(250),
    @pi_addressline5 varchar(250),
    @pi_phone varchar(50),
    @pi_fax varchar(50),
    @pi_lat varchar(50),
    @pi_long varchar(50),
	@pi_dbname varchar(50),
	--Return parameters
	@po_returnstatus INT OUTPUT,
	@po_returnmsg VARCHAR(200) OUTPUT,
	@po_scopeid NUMERIC OUTPUT,
	@lbProviderId VARCHAR(200) OUTPUT	
AS
DECLARE
		@proOutputMsg varchar(200),
		@proOutputStatus varchar(200)
BEGIN

	--Assume transaction will fail, set the return variable to -1.
	SET @po_returnstatus = -1;
	SET @po_returnmsg = null;
	SET @po_scopeid = 0;
	
	BEGIN TRY
	if NOT EXISTS (Select TOP 1 * from LB_BB_providerDetails where _ProviderID=@pi_propertyID and availabilityProviderID=@pi_availabilityProviderID)
	begin
		INSERT INTO LB_BB_providerDetails
           (_ProviderID
           ,availabilityProviderID
           ,XmlResponse
           ,expiryTime)
		 VALUES
			   (@pi_propertyID
			   ,@pi_availabilityProviderID
			   ,@pi_xmlresponse
			   ,@pi_expirytime)
	EXEC LB_BB_createBedbankProvider @pi_propertyid, @pi_availabilityProviderID, @pi_providerName, @pi_addressline1, @pi_addressline2, @pi_addressline3, @pi_addressline4, @pi_addressline5, @pi_phone, @pi_fax, @pi_lat, @pi_long, @pi_dbname, @po_lbproviderid=@lbProviderId out, @po_returnmsg=@proOutputMsg out, @po_returnstatus=@proOutputStatus out		
    end
	END TRY
	
	BEGIN CATCH
		SET @po_returnstatus = error_number();
		SET @po_returnmsg = error_message();
		RETURN;
	END CATCH
	
	SET @po_returnstatus = 0;
	SET @po_returnmsg = 'Completed successfully.';
	
END