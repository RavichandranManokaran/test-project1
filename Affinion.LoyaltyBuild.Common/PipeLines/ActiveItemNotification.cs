﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           ActiveItemNotification.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Genarate  Notification icon based on active or inactive states./// </summary>
#region Using Directives
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Sitecore.Collections;
using Sitecore.Data.Items;
using Sitecore.Events;
using Sitecore.SecurityModel;
using Sitecore.Data;
using System;
#endregion

namespace Affinion.LoyaltyBuild.Common.PipeLines
{
    public class ActiveItemNotification
    {
        #region private properties
        private const string path = "/sitecore/content/admin-portal/global/_supporting-content/icons/";
        #endregion


        #region public methods
        /// <summary>
        /// check current item  and call ValidPortal method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public void OnItemSaved(object sender, EventArgs args)
        {
            if (!string.Equals(Sitecore.Context.Database.Name, "master"))
            {
                return;
            }

            Item activeIcon = Sitecore.Context.Database.Items[path + "active-icon"];
            Item inactiveIcon = Sitecore.Context.Database.Items[path + "inative-icon"];

            Item item = Event.ExtractParameter(args, 0) as Item;

            if (!string.Equals(item.Database.Name, "master"))
            {
                return;
            }

            string itemFieldName = "Active";

            if (item.Name != "$name")
            {
                ID itemTemplate = item.TemplateID;
                string templateValue = itemTemplate.ToString();

                if (templateValue == "{4CFC54E0-069A-4344-85E3-2BD99DA576E6}")
                {
                    if (item.Fields[itemFieldName] != null)
                    {
                        try
                        {
                            string fieldValue = item.Fields[itemFieldName].Value;

                            using (new SecurityDisabler())
                            {
                                item.Editing.BeginEdit();
                                if (fieldValue == "1")
                                {
                                    item.Fields["__icon"].Value = activeIcon["__Icon"];
                                    ValidPortal(item, fieldValue);
                                }
                                else
                                {
                                    item.Fields["__icon"].Value = inactiveIcon["__Icon"];
                                    ValidPortal(item, fieldValue);
                                }
                                item.Editing.EndEdit();
                                item.Editing.AcceptChanges();
                            }
                        }
                        catch (Exception ex)
                        {
                            Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                            throw;
                        }
                    }
                }
            }


        }
        #endregion

        #region private methods
        /// <summary>
        /// Update portal item icon based on client details item active cheackbox state
        /// </summary>
        /// <param name="clientDetailsItem">Client setup details item</param>
        /// <param name="cheackState">Client setup details item's Active field valu</param>        
        private void ValidPortal(Item clientDetailsItem, string fieldValue)
        {
            Item rootItem = Sitecore.Configuration.Factory.GetDatabase("master").Items["/sitecore/content/client-portal"];
            ChildList children = new ChildList(rootItem);
            Item activeIcon = Sitecore.Configuration.Factory.GetDatabase("master").Items[path + "active-icon"];
            Item inactiveIcon = Sitecore.Configuration.Factory.GetDatabase("master").Items[path + "inative-icon"];

            foreach (Item portalItem in children)
            {
                if (clientDetailsItem.Name == portalItem.Name)
                {
                    using (new SecurityDisabler())
                    {
                        portalItem.Editing.BeginEdit();

                        if (fieldValue == "1")
                        {
                            portalItem.Fields["__icon"].Value = activeIcon["__Icon"];
                        }
                        else
                        {
                            portalItem.Fields["__icon"].Value = inactiveIcon["__Icon"];
                        }
                        portalItem.Editing.EndEdit();
                    }
                    break;
                }
            }
        }
        #endregion
    }
}
