﻿using Affinion.LoyaltyBuild.Model.Provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Search.Data
{
    public class ProviderInfo
    {

        /// <summary>
        /// Gets or sets the offer name
        /// </summary>
        public string ProviderID { get; set; }

        /// <summary>
        /// Gets or sets the offer name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the offer sku
        /// </summary>
        public string Sku { get; set; }

        /// <summary>
        /// Gets or sets the offer sku
        /// </summary>
        public string VariatSku { get; set; }

        /// <summary>
        /// Gets or sets the RoomAvailability
        /// </summary>
        public int RoomAvailability { get; set; }

        /// <summary>
        /// Gets or sets the Offer Price
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Gets or sets the Number of Nights
        /// </summary>
        public int NoOfNights { get; set; }

        /// <summary>
        /// Gets or sets the currency
        /// </summary>
        public string PriceCurrency { get; set; }

        /// <summary>
        /// Gets or sets the ProviderType
        /// </summary>
        public ProviderType ProviderType { get; set; }

        /// <summary>
        /// Gets or sets the avilablity date
        /// </summary>
        public DateTime AvailabilityDate { get; set; }

        /// <summary>
        /// DateCounter
        /// </summary>
        public int DateCounter { get; set; }

        /// <summary>
        /// BookingTokenForBB
        /// </summary>
        public string BookingTokenForBB { get; set; }

        /// <summary>
        /// MealBasisId
        /// </summary>
        public string BBMealBasisId { get; set; }

        /// <summary>
        /// MealBasis
        /// </summary>
        public string BBMealBasis { get; set; }
        /// <summary>
        /// MarginId
        /// </summary>
        public int BBMarginId { get; set; }

        /// <summary>
        /// Margin Percentage for BedBanks
        /// </summary>
        public decimal BBMarginPercentage { get; set; }
       
        /// <summary>
        /// MealBasisId
        /// </summary>
        public string UComCurrencyId { get; set; }


    }
}
