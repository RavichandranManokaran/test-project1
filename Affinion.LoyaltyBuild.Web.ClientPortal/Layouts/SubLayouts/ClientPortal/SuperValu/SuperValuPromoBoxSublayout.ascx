﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SuperValuPromoBoxSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.SuperValuPromoBoxSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="reason-outer">
    <div class="reason">
        <div class="top-title">
            <asp:Literal ID="PlaceholderText" Text="Set the datasource: Required Fields: Title, Subtitle" runat="server" />
            <h1><sc:Text Field="Title" ID="Title" runat="server" /></h1>
            <p><sc:Text Field="Subtitle" ID="Subtitle" runat="server" /></p>
        </div>
        <div class="description">
            <ul>
                <sc:Placeholder ID="promoboxlink" Key="promoboxlink" runat="server" />
            </ul>
        </div>
    </div>
</div>
