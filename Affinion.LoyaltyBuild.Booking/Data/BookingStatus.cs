﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Api.Booking.Data
{
    /// <summary>
    /// Booking status
    /// </summary>
    public enum BookingStatus
    {
        Basket = 1,
        NewOrder=2,
        CompletedOrder=3,
        Invoiced=5,
        Confirmed = 6,
        Cancelled = 7,
        Transferred = 8
    }
}
