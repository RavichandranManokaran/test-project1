﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Api.Booking.Data.Basket
{
    /// <summary>
    /// Add to basket bed banks request
    /// </summary>
    public class AddBedBanksRequest : IAddToBasketRequest
    {
        /// <summary>
        /// Property Id
        /// </summary>
        [OrderProperty(Constants.PropertyId)]
        
        public int PropertyID { get; set; }
        
        /// <summary>
        /// Pre Booking Token
        /// </summary>
        [OrderProperty(Constants.PreBookingToken)]
        public string PreBookingToken { get; set; }

        /// <summary>
        /// Property Name
        /// </summary>
        [OrderProperty(Constants.PropertyName)]
        public string PropertyName { get; set; }

        /// <summary>
        /// ArrivalDate
        /// </summary>
        [OrderProperty(Constants.CheckinDate)]
        public DateTime CheckinDate { get; set; }

        /// <summary>
        /// Departure Date
        /// </summary>
        [OrderProperty(Constants.CheckoutDate)]
        public DateTime CheckoutDate { get; set; }

        /// <summary>
        /// Duration
        /// </summary>
        [OrderProperty(Constants.Nights)]
        public int NoofNights { get; set; }

        /// <summary>
        /// NoOfRooms Property
        /// </summary>
        [OrderProperty(Constants.NoOfRooms)]
        public int NoOfRooms { get; set; }

        /// <summary>
        /// NoOfAdults Property
        /// </summary>
        [OrderProperty(Constants.NoOfAdults)]
        public int NoOfAdults { get; set; }

        /// <summary>
        /// NoOfChildren Property
        /// </summary>
        [OrderProperty(Constants.NoOfChildren)]
        public int NoOfChildren { get; set; }

        /// <summary>
        /// ChildrenAges Property
        /// </summary>
        [OrderProperty(Constants.ChildrenAges)]
        public string ChildrenAges { get; set; }

        /// <summary>
        /// Image Url
        /// </summary>
        [OrderProperty(Constants.ImageUrl)]
        public string ImageUrl { get; set; }

        /// <summary>
        /// Star Ranking
        /// </summary>
        [OrderProperty(Constants.StarRanking)]
        public int StarRanking { get; set; }

        /// <summary>
        /// LeadGuestTitle
        /// </summary>
        [OrderProperty(Constants.LeadGuestTitle)]
        public string LeadGuestTitle { get; set; }

        /// <summary>
        /// LeadGuestFirstName
        /// </summary>
        [OrderProperty(Constants.LeadGuestFirstName)]
        public string LeadGuestFirstName { get; set; }

        /// <summary>
        /// LeadGuestLastName
        /// </summary>
        [OrderProperty(Constants.LeadGuestLastName)]
        public string LeadGuestLastName { get; set; }

        /// <summary>
        /// LeadGuestAddress1
        /// </summary>
        [OrderProperty(Constants.LeadGuestAddress1)]
        public string LeadGuestAddress1 { get; set; }

        /// <summary>
        /// LeadGuestPostcode
        /// </summary>
        [OrderProperty(Constants.LeadGuestPostcode)]
        public string LeadGuestPostcode { get; set; }

        /// <summary>
        /// LeadGuestBookingCountryID
        /// </summary>
        [OrderProperty(Constants.LeadGuestBookingCountryID)]
        public string LeadGuestBookingCountryID { get; set; }

        /// <summary>
        /// LeadGuestPhone
        /// </summary>
        [OrderProperty(Constants.LeadGuestPhone)]
        public string LeadGuestPhone { get; set; }

        /// <summary>
        /// LeadGuestEmail
        /// </summary>
        [OrderProperty(Constants.LeadGuestEmail)]
        public string LeadGuestEmail { get; set; }

        /// <summary>
        /// ContractArrangementID
        /// </summary>
        [OrderProperty(Constants.ContractArrangementID)]
        public string ContractArrangementID { get; set; }

        /// <summary>
        /// Special Request
        /// </summary>
        [OrderProperty(Constants.Request)]
        public string Request { get; set; }

        /// <summary>
        /// TradeReference
        /// </summary>
        [OrderProperty(Constants.TradeReference)]
        public string TradeReference { get; set; }


        public string CCCardTypeID { get; set; }
        public string CCIssueNumber { get; set; }
        public string CCAmount { get; set; }

        /// <summary>
        /// RoomInfo
        /// </summary>
        [OrderProperty(Constants.RoomInfo)]
        public string RoomInfo { get; set; }

        /// <summary>
        /// MealBasisID
        /// </summary>
        [OrderProperty(Constants.MealBasisId)]
        public string MealBasisID { get; set; }

        /// <summary>
        /// RoomToken
        /// </summary>
        [OrderProperty(Constants.RoomToken)]
        public string RoomToken { get; set; }

        /// <summary>
        /// IsDirect Flag
        /// </summary>
        [OrderProperty(Constants.IsDirect)]
        public string IsDirect { get; set; }

        /// <summary>
        /// ClientId
        /// </summary>
        [OrderProperty(Constants.ClientId)]
        public string ClientId { get; set; }

        public string RoomType { get; set; }
        public string WholesaleAmount { get; set; }

        /// <summary>
        /// MarginIdApplied
        /// </summary>
        [OrderProperty(Constants.MarginId)]
        public int MarginIdApplied { get; set; }

        /// <summary>
        /// CurrencyID Property
        /// </summary>
        [OrderProperty(Constants.CurrencyId)]
        public int CurrencyId { get; set; }

        /// <summary>
        /// Sku
        /// </summary>
        public string Sku { get; set; }

        /// <summary>
        /// Variant Sku
        /// </summary>
        public string VariantSku { get; set; }

        /// <summary>
        /// BookingThrough Property
        /// </summary>
        [OrderProperty(Constants.BookingThrough)]
        public string BookingThrough { get; set; }

        /// <summary>
        /// Maximum adults
        /// </summary>
        [OrderProperty(Constants.MaxAdults)]
        public int MaxAdults { get; set; }

        /// <summary>
        /// Max children
        /// </summary>
        [OrderProperty(Constants.MaxChildren)]
        public int MaxChildren { get; set; }

        /// <summary>
        /// Max children
        /// </summary>
        [OrderProperty(Constants.MinAdults)]
        public int MinAdults { get; set; }

        /// <summary>
        /// Old Price during search
        /// </summary>
        [OrderProperty(Constants.OldPrice)]
        public string OldPrice { get; set; }

        /// <summary>
        /// Destination Property
        /// </summary>
        [OrderProperty(Constants.Destination)]
        public string Destination { get; set; }

        /// <summary>
        /// OrderLineId Property
        /// </summary>
        [OrderProperty(Constants.OrderLineId)]
        public int OrderLineId { get; set; }

        /// <summary>
        /// CreatedBy Property
        /// </summary>
        [OrderProperty(Constants.CreatedBy)]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Margin Percentage for BedBanks
        /// </summary>
        [OrderProperty(Constants.MarginPercentage)]
        public decimal BBMarginPercentage { get; set; }

        /// <summary>
        /// BedBank Currency ID
        /// </summary>
        [OrderProperty(Constants.BBCurrencyId)]
        public string BBCurrencyId { get; set; }

        /// <summary>
        /// PropertyRefernceID of room
        /// </summary>
        [OrderProperty(Constants.PropertyReferenceId)]
        public string PropertyReferenceId { get; set; }
    }
}
