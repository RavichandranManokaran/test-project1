﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SitecoreFieldsHelper.cs
/// Sub-system/Module:     Common(VS project name)
/// Description:           Contains helper methods to validate and get values from sitecore fields
/// </summary>
namespace Affinion.LoyaltyBuild.Common.Utilities
{
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Sitecore.Data;
    using Sitecore.Data.Fields;
    using Sitecore.Data.Items;
    using Sitecore.SecurityModel;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Web.UI.WebControls;

    /// <summary>
    /// Contains helper methods to validate and get values from sitecore fields
    /// </summary>
    [CLSCompliant(true)]
    public static class SitecoreFieldsHelper
    {
        //#region Private Fields
        //private const string path = "/sitecore/content/admin-portal/global/_supporting-content/icons/";
        //#endregion

        //#region Properties
        //public static Item ActiveIcon
        //{
        //    get
        //    {
        //        return Sitecore.Configuration.Factory.GetDatabase("master").Items[Constants.IconPath + "active-icon"];
        //    }
        //}
        //public static Item InactiveIcon
        //{
        //    get
        //    {
        //        return Sitecore.Configuration.Factory.GetDatabase("master").Items[Constants.IconPath + "inative-icon"];
        //    }
        //}
        //#endregion

        #region Common methods

        /// <summary>
        /// Checks against null items and empty field values
        /// </summary>
        /// <param name="item">The item needs to be read</param>
        /// <param name="fieldName">The field name needs to be read</param>
        /// <returns>Returns true if the item is null or field not found or field value is empty</returns>
        public static bool IsNullOrEmpty(Item item, string fieldName)
        {
            try
            {
                return !(item != null && item.Fields[fieldName] != null && !string.IsNullOrEmpty(item[fieldName]));
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, new object());
                throw;
            }
        }

        /// <summary>
        /// Reads an integer value with null and default value handling
        /// </summary>
        /// <param name="item">The item needs to be read</param>
        /// <param name="fieldName">The field name needs to be read</param>
        /// <param name="defaultValue">The default value to return in case the validation fails</param>
        /// <returns>The field value as an integer or default value</returns>
        public static int GetInteger(Item item, string fieldName, int defaultValue)
        {
            int value = 0;
            if (int.TryParse(SitecoreFieldsHelper.GetValue(item, fieldName, defaultValue.ToString()), out value))
            {
                return value;
            }

            return defaultValue;
        }

        /// <summary>
        /// Reads an bool value with null handling
        /// </summary>
        /// <param name="item">The item needs to be read</param>
        /// <param name="fieldName">The field name needs to be read</param>
        /// <returns>The field value as a boolean</returns>
        public static bool GetBoolean(Item item, string fieldName)
        {
            return SitecoreFieldsHelper.GetValue(item, fieldName, string.Empty) == "1";
        }

        /// <summary>
        /// Reads the field value with null handling and replace mustache notation tags with the relevent values from the mustache notation
        /// </summary>
        /// <param name="item">The item needs to be read</param>
        /// <param name="fieldName">The field name needs to be read</param>
        /// <param name="mustacheContext">The mustache notation context as a dictionary</param>
        /// <returns>The field value or an empty string if the field or item not found</returns>
        public static string GetValue(Item item, string fieldName, Dictionary<string, string> mustacheContext)
        {
            string itemValue = SitecoreFieldsHelper.GetValue(item, fieldName, string.Empty);

            if (mustacheContext != null)
            {
                foreach (string key in mustacheContext.Keys)
                {
                    itemValue = itemValue.Replace(key, mustacheContext[key]);
                }
            }

            return itemValue;
        }

        /// <summary>
        /// Reads the field value with null handling
        /// </summary>
        /// <param name="item">The item needs to be read</param>
        /// <param name="fieldName">The field name needs to be read</param>
        /// <returns>The field value or an empty string if the field or item not found</returns>
        public static string GetValue(Item item, string fieldName)
        {
            return SitecoreFieldsHelper.GetValue(item, fieldName, string.Empty);
        }

        /// <summary>
        /// Reads the field value with null handling
        /// </summary>
        /// <param name="item">The item needs to be read</param>
        /// <param name="fieldName">The field name needs to be read</param>
        /// <returns>The field value or an empty string if the field or item not found</returns>
        public static string GetItemFieldValue(Item currentItem, string fieldName)
        {
            if (currentItem != null && currentItem.Fields[fieldName] != null && !string.IsNullOrWhiteSpace(currentItem.Fields[fieldName].Value))
            {
                return currentItem.Fields[fieldName].Value;
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Reads the field value with null handling and returns default value if read fails
        /// </summary>
        /// <param name="item">The item needs to be read</param>
        /// <param name="fieldName">The field name needs to be read</param>
        /// <param name="defaultValue">The default value to return in case the validation fails</param>
        /// <returns>The field value or the default value if the field or item not found</returns>
        public static string GetValue(Item item, string fieldName, string defaultValue)
        {
            try
            {
                return SitecoreFieldsHelper.IsNullOrEmpty(item, fieldName) ? defaultValue : item.Fields[fieldName].Value;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, new object());
                throw;
            }
        }

        /// <summary>
        /// Checks for a given field value contains in the given keyword. 
        /// If the field value is empty item name will be compared with the key.
        /// Match is case insensitive.
        /// </summary>
        /// <param name="item">Item to read field</param>
        /// <param name="fieldName">Field to read value</param>
        /// <param name="match">Keyword to match</param>
        /// <returns>Returns true if match found</returns>
        public static bool Contains(Item item, string fieldName, string match)
        {
            string value = SitecoreFieldsHelper.GetValue(item, fieldName);
            value = string.IsNullOrEmpty(value) ? item.Name : value;

            if (string.IsNullOrEmpty(value) || string.IsNullOrEmpty(match))
            {
                return false;
            }

            return value.ToLowerInvariant().IndexOf(match.ToLowerInvariant()) >= 0;
        }

        /// <summary>
        /// Checks for given keyword contains in the given field value. 
        /// If the field value is empty item name will be compared with the key.
        /// Match is case insensitive.
        /// </summary>
        /// <param name="item">Item to read field</param>
        /// <param name="fieldName">Field to read value</param>
        /// <param name="match">Keyword to match</param>
        /// <returns>Returns true if match found</returns>
        public static bool ContainsIn(Item item, string fieldName, string match)
        {
            string value = SitecoreFieldsHelper.GetValue(item, fieldName);
            value = string.IsNullOrEmpty(value) ? item.Name : value;

            if (string.IsNullOrEmpty(value) || string.IsNullOrEmpty(match))
            {
                return false;
            }

            return match.ToLowerInvariant().IndexOf(value.ToLowerInvariant()) >= 0;
        }

        /// <summary>
        /// Reads a multilist and return the data source for asp dropdowns
        /// </summary>
        /// <param name="item">The item to read the multilist field</param>
        /// <param name="targetItemsTextField">The field to read text value for drop downs</param>
        /// <returns>Returns a list item collection with text of the given field and value of the item ID</returns>
        public static ListItemCollection GetDropdownDataSource(IEnumerable<Item> items, string targetItemsTextField)
        {
            try
            {
                return GetDropdownDataSource(items, targetItemsTextField, string.Empty);
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                throw;
            }
        }

        /// <summary>
        /// Reads a multilist and return the data source for asp dropdowns
        /// </summary>
        /// <param name="item">The item to read the multilist field</param>
        /// <param name="targetItemsTextField">The field to read text value for drop downs</param>
        /// <param name="targetItemsValueField">The field to read value for drop downs</param>
        /// <returns>Returns a list item collection with text of the given field and value of the item ID</returns>
        public static ListItemCollection GetDropdownDataSource(IEnumerable<Item> items, string targetItemsTextField, string targetItemsValueField)
        {
            try
            {
                if (items == null || string.IsNullOrEmpty(targetItemsTextField))
                {
                    return new ListItemCollection();
                }

                ListItemCollection listItemCollection = new ListItemCollection();

                foreach (Item currentItem in items)
                {
                    ListItem listItem = new ListItem();

                    listItem.Text = currentItem.Fields[targetItemsTextField].Value;

                    if (string.IsNullOrEmpty(targetItemsValueField))
                    {
                        listItem.Value = currentItem.ID.ToString();
                    }
                    else
                    {
                        listItem.Value = currentItem.Fields[targetItemsValueField].Value;
                    }

                    listItemCollection.Add(listItem);
                }

                return listItemCollection;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                throw;
            }
        }

        /// <summary>
        /// Method to bind Sitecore text in usercontrol
        /// </summary>
        /// <param name="sourceitem">The data source Item for text control</param>
        /// <param name="fieldName">The field name whose value should be bound</param>
        /// <param name="textControl">The sitecore Text user control</param>
        public static void BindSitecoreText(Item sourceitem, string fieldName, Sitecore.Web.UI.WebControls.Text textControl)
        {
            if (textControl == null)
            {
                return;
            }
            else if (sourceitem == null || string.IsNullOrWhiteSpace(fieldName))
            {
                textControl.Visible = false;
            }
            else
            {
                textControl.Item = sourceitem;
                textControl.Field = fieldName;
                textControl.Visible = true;
            }
        }
        /// </summary>returns selected item from drop list
        /// <param name="sourceitem">The data source Item for text control</param>
        /// <param name="fieldName">The field name whose value should be bound</param>
        public static Item GetSelectedItemFromDroplistField(Item item, string fieldName)
        {
            Field field = item.Fields[fieldName];
            if (field == null || string.IsNullOrEmpty(field.Value))
            {
                return null;
            }

            var fieldSource = field.Source ?? string.Empty;
            var selectedItemPath = fieldSource.TrimEnd('/') + "/" + field.Value;
            return item.Database.GetItem(selectedItemPath);
        }

        public static bool CheckBoxChecked(Item item, string fieldName)
        {
            Sitecore.Data.Fields.CheckboxField value = item.Fields[fieldName];

            return value != null && value.Checked;
        }

        #endregion

        #region MutiList field methods

        /// <summary>
        /// Reads a multilist and return the data source for asp dropdowns
        /// </summary>
        /// <param name="item">The item to read the multilist field</param>
        /// <param name="fieldName">The multilist field name</param>
        /// <param name="targetItemsTextField">The field to read text value for drop downs</param>
        /// <returns>Returns a list item collection with text of the given field and value of the item ID</returns>
        public static ListItemCollection GetDropdownDataSource(Item item, string fieldName, string targetItemsTextField)
        {
            try
            {
                if(item == null || string.IsNullOrEmpty(fieldName) || string.IsNullOrEmpty(targetItemsTextField))
                {
                    return new ListItemCollection();
                }

                var items = SitecoreFieldsHelper.GetMutiListItems(item, fieldName);
                return GetDropdownDataSource(items, targetItemsTextField);
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                throw;
            }
        }

        /// <summary>
        /// Get the multilist field values, store it in a list and return the item list
        /// </summary>
        /// <param name="fieldName">MultiList field name</param>
        /// <returns>Item List</returns>
        public static Collection<Item> GetMutiListItems(string fieldName)
        {
            return SitecoreFieldsHelper.GetMutiListItems(Sitecore.Context.Item, fieldName);
        }

        /// <summary>
        /// Read the multilist field values and return the list of target items
        /// </summary>
        /// <param name="fieldName">Field Name of the multilist</param>
        /// <param name="refitem">Item</param>
        /// <returns></returns>
        public static Collection<Item> GetMutiListItems(Item item, string fieldName)
        {
            Collection<Item> itemList = new Collection<Item>();

            try
            {
                //If the field name is empty
                if (!string.IsNullOrEmpty(fieldName))
                {
                    //get the mutilist field values
                    MultilistField multilistField = item.Fields[fieldName];

                    //if the multilist field is empty return an empty list
                    if (multilistField != null)
                    {
                        //convert item array to list
                        foreach (Item currentItem in multilistField.GetItems())
                        {
                            bool active = currentItem.Fields["IsActive"] == null || (currentItem.Fields["IsActive"] != null && currentItem.Fields["IsActive"].Value == "1");
                            if(active)
                            {
                                itemList.Add(currentItem);
                            }
                        }

                        // itemList = new Collection<Item>(multilistField.GetItems());
                    }
                }

                return itemList;
            }
            catch (NullReferenceException ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                throw;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                throw;
            }

        }

        /// <summary>
        /// Checks for an Item in the given multilist field
        /// </summary>
        /// <param name="item">Source item</param>
        /// <param name="field">Field to read multilist items</param>
        /// <param name="match">Item to match</param>
        /// <returns>Returns true if match found</returns>
        public static bool MultiListContains(Item item, string field, Item match)
        {
            bool matchFound = false;
            Collection<Item> items = SitecoreFieldsHelper.GetMutiListItems(item, field);

            foreach (Item currentItem in items)
            {
                if(currentItem.ID.ToString() == match.ID.ToString())
                {
                    matchFound = true;
                    break;
                }
            }

            return matchFound;
        }

        #endregion

        #region DropLink field methods

        public static string GetDropLinkFieldValue(Item item, string dropLinkFieldName, string targetItemFieldName)
        {
            try
            {
                Item targetItem = GetDropLinkFieldTargetItem(item, dropLinkFieldName);

                if (targetItem == null || string.IsNullOrEmpty(targetItemFieldName))
                {
                    return string.Empty;
                }

                var field = targetItem.Fields[targetItemFieldName];

                if(field == null)
                {
                    return string.Empty;
                }

                return field.Value;

            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                throw;
            }
        }

        public static Item GetDropLinkFieldTargetItem(Item item, string dropLinkFieldName)
        {
            try
            {
                if(item == null || string.IsNullOrEmpty(dropLinkFieldName))
                {
                    return null;
                }

                LookupField lookupField = item.Fields[dropLinkFieldName];

                if (lookupField == null)
                {
                    return null;
                }

                return lookupField.TargetItem;

            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                throw;
            }
        }

        #endregion

        #region droplist field methods

        public static string GetDropListFieldValue(Item item, string dropListFieldName)
        {
            try
            {
                if (item == null || string.IsNullOrEmpty(dropListFieldName))
                {
                    return null;
                }

                ReferenceField lookupField = item.Fields[dropListFieldName];

                if (lookupField == null)
                {
                    return null;
                }

                return lookupField.Value;

            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                throw;
            }
        }

        #endregion

        #region Lookup field methods

        /// <summary>
        /// Reads value from a given field of the target item of a given lookup field
        /// </summary>
        /// <param name="item">The item to read the lookup field</param>
        /// <param name="lookupFieldName">The lookup field name</param>
        /// <param name="targetFieldName">The text field name of the target item to read values</param>
        /// <returns>Returns a value of a text field of a target item</returns>
        public static string GetLookupFieldTargetItemValue(Item item, string lookupFieldName, string targetFieldName)
        {
            Item targetItem = GetLookupFieldTargetItem(item, lookupFieldName);

            if (targetItem != null && targetItem.Fields[targetFieldName] != null)
            {
                return targetItem.Fields[targetFieldName].Value;
            }

            return string.Empty;
        }

        /// <summary>
        /// Reads value from a given field of the target item of a given lookup field
        /// </summary>
        /// <param name="item">The item to read the lookup field</param>
        /// <param name="lookupFieldName">The lookup field name</param>
        /// <param name="targetFieldName">The text field name of the target item to read values</param>
        /// <returns>Returns a value of a text field of a target item</returns>
        public static Item GetLookupFieldTargetItem(Item item, string lookupFieldName)
        {
            if (item != null && !string.IsNullOrEmpty(lookupFieldName) && item.Fields[lookupFieldName] != null)
            {
                LookupField lookupField = (LookupField)item.Fields[lookupFieldName];

                if (lookupField == null)
                {
                    return null;
                }

                return lookupField.TargetItem;
            }

            return null;
        }

        #endregion

        #region Link field methods

        /// <summary>
        /// Get the item and the field name and return the url and alter tag
        /// </summary>
        /// <param name="item">Item</param>
        /// <param name="fieldName">field name</param>
        /// <returns>AnchorTagHelperData object</returns>
        public static AnchorTagHelperData GetUrl(Item item, string fieldName)
        {
            AnchorTagHelperData tagHelper = new AnchorTagHelperData();
            string link = string.Empty;

            tagHelper.Url = tagHelper.AlterTag = string.Empty;
            LinkField lf = item.Fields[fieldName];

            try
            {
                switch (lf.LinkType.ToLower())
                {
                    case "internal":
                        // Use LinkMananger for internal links, if link is not empty
                        ReferenceField referenceField = (ReferenceField)item.Fields[fieldName];
                        if (lf != null && lf.TargetItem != null)
                        {
                            tagHelper.Url = lf.TargetItem != null ? Sitecore.Links.LinkManager.GetItemUrl(lf.TargetItem) : string.Empty;
                            tagHelper.AlterTag = lf.Text;
                        }
                        else if (referenceField != null && referenceField.TargetItem != null)
                        {
                            
                            Item targetItem = referenceField.TargetItem;
                            tagHelper.Url = Sitecore.Links.LinkManager.GetItemUrl(targetItem);
                            tagHelper.AlterTag = targetItem.DisplayName;
                        }
                        break;
                    case "media":
                        // Use MediaManager for media links, if link is not empty
                        tagHelper.Url = lf.TargetItem != null ? Sitecore.Resources.Media.MediaManager.GetMediaUrl(lf.TargetItem) : string.Empty;
                        tagHelper.AlterTag = lf.Text;
                        break;
                    case "external":
                        // Just return external links
                        tagHelper.Url = lf.Url;
                        tagHelper.AlterTag = lf.Text;
                        break;
                    default:
                        // Just please the compiler, this
                        // condition will never be met
                        tagHelper.Url = lf.Url;
                        tagHelper.AlterTag = lf.Text;
                        break;
                }
                return tagHelper;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, new object());
                throw;
            }
        }

        /// <summary>
        /// Get the field name and return the link and the alter tag as a obejct of AnchorTagHelper class 
        /// </summary>
        /// <param name="fieldName">sitecore item field name</param>
        /// <returns>AnchorTagHelper class object</returns>
        public static AnchorTagHelperData GetUrl(string fieldName)
        {
            return GetUrl(Sitecore.Context.Item, fieldName);
        }

        /// <summary>
        /// Get the item and return the Url of the perticular item
        /// </summary>
        /// <param name="item">Item</param>
        /// <returns>AnchorTagHelperData class object</returns>
        public static AnchorTagHelperData GetUrlOfItem(Item item)
        {
            //create an instence from AnchorTagHelperData class
            AnchorTagHelperData tagHelper = new AnchorTagHelperData();

            try
            {
                //Use LinkMananger for items, if item is not empty
                if (item == null)
                {
                    return tagHelper;
                }

                if (Sitecore.Context.Site.RootPath == item.Paths.FullPath)
                {
                    tagHelper.Url = "/";
                }
                else
                {
                    tagHelper.Url = Sitecore.Links.LinkManager.GetItemUrl(item);
                }

                tagHelper.AlterTag = item.DisplayName;
                return tagHelper;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, new object());
                throw;
            }
        }

        #endregion

        #region Utility Methods


        #endregion

        #region Private Methods

        
        
        #endregion
    }
}
