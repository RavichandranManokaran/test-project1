﻿//<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           ClientPortalMapSublayout.ascx.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal.ClientPortalMapSublayout
/// Description:           SubLayout for google map
/// </summary>

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts
{
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.PrimaryLayouts.ClientPortal;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI;

    /// <summary>
    /// Google map
    /// </summary>
    public partial class ClientPortalMapSublayout : BaseSublayout
    {
        /// <summary>
        /// Latitude
        /// </summary>
        public string Latitude { get; set; }
        /// <summary>
        /// Longitude
        /// </summary>
        public string Longitude { get; set; }

        /// <summary>
        /// Page load
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    RenderMapLocations();

                    ClientPortalPrimaryLayout page = this.Page as ClientPortalPrimaryLayout;

                    bool hideMap = string.IsNullOrWhiteSpace(page.GetPageData("HideMap")) ? false : page.GetPageData("HideMap");

                    if (hideMap)
                    {
                        GoogleMap.Visible = false;
                    }
                    else
                    {

                        dynamic coordinates = page.GetPageData("GeoCoordinates");
                        string geoCoordinates = coordinates;
                        if (!string.IsNullOrWhiteSpace(geoCoordinates))
                        {
                            string[] geoCoordinatesArray = geoCoordinates.Split(',');

                            if (geoCoordinatesArray.Length == 2)
                            {
                                Latitude = geoCoordinatesArray[0];
                                Longitude = geoCoordinatesArray[1];
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, null);
                throw;
            }

        }

        /// <summary>
        /// Mark map locations
        /// </summary>
        private void RenderMapLocations()
        {

            List<String> oGeocodeList = new List<String>();
            string destination = Request.QueryString["Destination"];

            if (!string.IsNullOrWhiteSpace(destination))
            {

                ///Get locations folder item
                Item locationsItem = Sitecore.Context.Database.GetItem("/sitecore/content/admin-portal/global/_supporting-content/locations");

                ///Get map locations folder
                Item mapLocationsItem = Sitecore.Context.Database.GetItem("/sitecore/content/admin-portal/global/_supporting-content/map-locations");

                ///Get all location with country or name matching search keyword
                IEnumerable<Item> locations = locationsItem.Axes.GetDescendants().Where(a => a.TemplateName.Equals("Location") && a.Fields["IsActive"].Value == "1" && (a.Fields["Country"].Value.ToLowerInvariant().Contains(destination.ToLowerInvariant()) || a.Fields["Name"].Value.ToLowerInvariant().Contains(destination.ToLowerInvariant())));

                if (!locations.Any())
                {
                    ///Hide map control when no map locations available
                    GoogleMap.Visible = false;
                    return;
                }
                ///Get all map locations with template MapLocation
                IEnumerable<Item> mapLocations = mapLocationsItem.Axes.GetDescendants().Where(a => a.TemplateName.Equals("MapLocation"));

                ///Find map location for each matching location
                foreach (var location in locations)
                {
                    string name = location.Fields["Name"].Value;

                    foreach (var mapLocation in mapLocations)
                    {
                        string locationName = SitecoreFieldsHelper.GetDropLinkFieldValue(mapLocation, "Location", "Name");
                        if (locationName.Equals(name))
                        {
                            string mapCoordinates = SitecoreFieldsHelper.GetValue(mapLocation, "MapLocation");
                            //string mapCoordinates = mapLocation.Fields["MapLocation"].Value;

                            if (!string.IsNullOrWhiteSpace(mapCoordinates) && mapCoordinates.Split(',').Length == 2)
                            {
                                oGeocodeList.Add("'" + mapCoordinates + "'");
                            }
                        }

                    }
                }

            }

            if (oGeocodeList.Count <= 0)
            {
                ///Hide map control when no map locations available
                GoogleMap.Visible = false;
            }

            ///Get all locations into one string
            var geocodevalues = string.Join(",", oGeocodeList.ToArray());

            ClientScriptManager clientScriptManager = Page.ClientScript;
            clientScriptManager.RegisterArrayDeclaration("locationList", geocodevalues);

        }
    }
}