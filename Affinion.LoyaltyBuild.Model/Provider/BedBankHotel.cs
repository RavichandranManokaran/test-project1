﻿using Affinion.LoyaltyBuild.Model.Product;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Model.Provider
{
    /// <summary>
    /// Bed bank hotel
    /// </summary>
    public class BedBankHotel : IProvider, IGenericProvider
    {
        /// <summary>
        /// Provider Id
        /// </summary>
        public string ProviderId { get; set; }

        /// <summary>
        /// provider name
        /// </summary>
        public string ProviderName { get; set; }

        /// <summary>
        /// PropertyReference ID
        /// </summary>
        public string PropertyReferenceId { get; set; }

        /// <summary>
        /// SitecoreItemId
        /// </summary>
        public string SitecoreItemId { get; set; }

        /// <summary>
        /// provider type
        /// </summary>
        public ProviderType ProviderType { get; set; }

        /// <summary>
        /// IntroductionImage
        /// </summary>
        public string IntroductionImage { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Address
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Address Line 1
        /// </summary>
        public string AddressLine1 { get; set; }

        /// <summary>
        /// Address Line 2
        /// </summary>
        public string AddressLine2 { get; set; }

        ///// <summary>
        ///// Town
        ///// </summary>
        //public string Town { get; set; }

        /// <summary>
        /// Country
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Region
        /// </summary>
        public string Region { get; set; }

        /// <summary>
        /// Postcode
        /// </summary>
        public string Postcode { get; set; }

        /// <summary>
        /// Latitude
        /// </summary>
        public string Latitude { get; set; }

        /// <summary>
        /// Longitude
        /// </summary>
        public string Longitude { get; set; }

        /// <summary>
        /// Short description
        /// </summary>
        public string StrapLine { get; set; }
        
        /// <summary>
        /// Description
        /// </summary>
        public string Description { get; set; }

        ///// <summary>
        ///// Overview
        ///// </summary>
        //public string Overview { get; set; }

        /// <summary>
        /// Base URL
        /// </summary>
        public string CMSBaseUrl { get; set; }

        /// <summary>
        /// Hotel Images 
        /// </summary>
        public List<string> Images { get; set; }

        /// <summary>
        /// Star Ranking
        /// </summary>
        public int StarRanking { get; set; }

        /// <summary>
        /// Minimun Adults
        /// </summary>
        public int MinAdults { get; set; }

        /// <summary>
        /// Maximum adults
        /// </summary>
        public int MaxAdults { get; set; }

        /// <summary>
        /// Maximum children
        /// </summary>
        public int MaxChildren { get; set; }

        /// <summary>
        /// Facilities
        /// </summary>
        public List<Item> Facilities { get; set; }

        /// <summary>
        /// List of products for that provider
        /// </summary>
        public IList<IProduct> Products { get; set; }

        /// <summary>
        /// Ucommerce currency ID
        /// </summary>
        public string CurrencyId { get; set; }

        /// <summary>
        /// BedBanks currency ID
        /// </summary>
        public string BBCurrencyId { get; set; }
        
        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Address
        /// </summary>
        public string Phone { get; set; }
    }
}
