﻿CREATE TABLE [dbo].[uCommerce_DynamicOrderPropertyTarget] (
    [DynamicOrderPropertyTargetId] INT            NOT NULL,
    [Key]                          NVARCHAR (255) NOT NULL,
    [Value]                        NVARCHAR (MAX) NOT NULL,
    [CompareMode]                  INT            NOT NULL,
    [TargetOrderLine]              BIT            CONSTRAINT [DF_uCommerce_DynamicOrderPropertyTarget_TargetOrderLine] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_uCommerce_DynamicOrderPropertyTarget] PRIMARY KEY CLUSTERED ([DynamicOrderPropertyTargetId] ASC)
);

