﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           LocationPickerDialog.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.Common
/// Description:           Google map location picker custom field popup dialog code base
/// </summary>

using Sitecore.Web;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Web.UI.Pages;
using Sitecore.Web.UI.Sheer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Affinion.LoyaltyBuild.Web.Common.CustomFields.GoogleMapLocationPicker
{
    public class LocationPickerDialog : DialogForm
    {
        #region Control
        //Textbox to store selected geographic location cordinates
        public Edit TextBox_SelectedLocation;

        #endregion

        #region Control Event
        /// <summary>
        /// Set selected location to the textbox
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            var value = WebUtil.GetQueryString("value");
            if (!string.IsNullOrWhiteSpace(value) && string.IsNullOrWhiteSpace(TextBox_SelectedLocation.Value))
            {
                TextBox_SelectedLocation.Value = value;
            }
        }

        /// <summary>
        /// Event to run when click ok after selecting a position on the map
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected override void OnOK(object sender, System.EventArgs args)
        {
            SheerResponse.SetDialogValue(TextBox_SelectedLocation.Value);
            base.OnOK(sender, args);
        }

        #endregion
    }
}