﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalSearchCustomerQuery.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalSearchCustomerQuery" %>
<%@ Import Namespace="Affinion.LoyaltyBuild.Common.Utilities" %>
<script type="text/javascript" src="/Resources/scripts/affinionCustom.js"></script>

<script src="/Resources/scripts/jquery.nanoscroller.js"></script>
<script src="/Resources/scripts/callscroller.js"></script>

<script src="/Resources/scripts/jquery.dd.min.js"></script>
<link href="/Resources/styles/dd.css" rel="stylesheet" />

<div class="col-sm-12">
    <div class="col-sm-0">
    </div>
    <div class="col-sm-12">
        <a class="btn btn-default add-break pull-right margin-row width-full" href="<%=GetUrl("/AddCustomerEnquiry?") %>"><sc:text id="AddCustomerEnquiryButton" field="AddCustomerEnquiryButton" runat="server" /></a>
    </div>
</div>

<div class="hotel-search-box">
    <h3> <sc:text id="SearchBoxTitleText" field="SearchBoxTitle" runat="server" /> </h3>
    <div class="booking-top width-full divider">
    </div>
    <div class="col-xs-12">
        <h5><sc:text id="PartnerText" field="Partner" runat="server" /></h5>
        <asp:DropDownList CssClass="form-control styled-select tech" ID="ClientList" ClientIDMode="Static" runat="server"></asp:DropDownList>
    </div>

    <div class="col-xs-12">
        <h5><sc:text id="QueryTypeText" field="QueryType" runat="server" /></h5>
        <asp:DropDownList CssClass="form-control font-size-12" ID="QueryType" runat="server" ClientIDMode="Static" AppendDataBoundItems="true">
            <asp:ListItem Selected="True" Text="Please select" Value=""></asp:ListItem>
        </asp:DropDownList>
    </div>
    <div class="ondate">
        <div class="col-xs-12 ">
           <h5> <sc:text id="StartDateText" field="StartDate" runat="server" /> </h5>
            <div class="form-group">
                <input type="text" id="StartDate" runat="server" clientidmode="Static" class="form-control datepicker width-85 pull-right font-size-12" placeholder="Start date">
            </div>
        </div>
        <div class="col-xs-12 ">
            <h5><sc:text id="EndDateText" field="EndDate" runat="server" /></h5>
            <div class="width-full">
                <span class="input-group-btn"></span>
                <input type="text" id="EndDate" runat="server" clientidmode="Static" class="form-control datepicker width-85 pull-right font-size-12" placeholder="End date">
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <h5> <sc:text id="FirstNameText" field="FirstName" runat="server" /> </h5>
        <input type="text" clientidmode="Static" id="txtFirstName" runat="server" class="form-control font-size-12" placeholder="First Name">
    </div>
    <div class="col-xs-12">
        <h5> <sc:text id="LastNameText" field="LastName" runat="server" /> </h5>
        <input type="text" clientidmode="Static" id="txtLastName" runat="server" class="form-control font-size-12" placeholder="Last Name">
    </div>
    <div class="col-xs-12">
        <h5> <sc:text id="CampaignGroupText" field="CampaignGroup" runat="server" /> </h5>
        <asp:DropDownList ClientIDMode="Static" CssClass="form-control font-size-12" ID="ddlCompaignGroup" runat="server"></asp:DropDownList>
    </div>
    <div class="col-xs-12">
        <h5> <sc:text id="ProviderText" field="Provider" runat="server" /> </h5>
        <asp:DropDownList ClientIDMode="Static" CssClass="form-control font-size-12" ID="ddlProvider" runat="server"></asp:DropDownList>
    </div>
    <div class="col-xs-12">
        <h5> <sc:text id="BookingRefText" field="BookingRef" runat="server" /> </h5>
        <input type="text" id="bookingref" clientidmode="Static" runat="server" class="form-control font-size-12" placeholder="Booking Ref">
    </div>
    <div class="col-xs-12">
        <h5> <sc:text id="IsResolvedText" field="IsResolved" runat="server" /> </h5>
        <div id="IsSolved" class="editor-field.radio-button-list">
            <asp:RadioButtonList ID="IsSolved" Name="IsSolved" CssClass="form-control font-size-12" runat="server" ClientIDMode="Static" RepeatDirection="Horizontal" RepeatLayout="Table">
                <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                <asp:ListItem Text="No" Value="No"></asp:ListItem>
                <asp:ListItem Text="Both" Value="Both"></asp:ListItem>
            </asp:RadioButtonList>
        </div>
    </div>
    <div>
        <div class="col-xs-12">
            <div class="col-md-6 col-xs-12">
                <button type="button" id="btnSearch" class="btn btn-primary booking-margin-top btn-bg border-none" >Search</button>
            </div>
            <div class="col-md-6 col-xs-12">
                <button type="reset" class="btn btn-primary booking-margin-top btn-bg border-none">Reset</button>
            </div>
            <div class="col-md-6 col-xs-12">
                <button type="button" id="btnGraph" class="btn btn-primary booking-margin-top btn-bg border-none" title="Chart"><span class="glyphicon glyphicon-stats"></span></button>
            </div>
            <div class="col-md-6 col-xs-12">
                <button type="button" id="btnPrint" class="btn btn-primary booking-margin-top btn-bg border-none" title="Print"><span class="glyphicon glyphicon-print"></span></button>
            </div>
        </div>
    </div>
</div>
<div>
    <div class="modal fade bs-example-modal-lg" id="dvQuery" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header  alert-info">
                    <h4 class="modal-title">Customer Query</h4>
                </div>
                <div class="modal-body">
                    <p id="QueryDetail"></p>
                </div>

            </div>
        </div>
    </div>
    <div class="modal fade bs-example-modal-sm" id="dvmessage" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header  alert-info">
                    <h4 class="modal-title">Information</h4>
                </div>
                <div class="modal-body">
                    <p id="msgcontent"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="Button3" class="btn btn-info" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="hdselectedqueryid" value="0" />
<script type="text/javascript">
    jQuery(function () {
        jQuery("#StartDate").datepicker({
            //defaultDate: "+1w",
            defaultDate: new Date(),
            dateFormat: "<%=DateTimeHelper.DateFormatJavaScript%>",
            minDate: -90,
            maxDate: 0,
            numberOfMonths: 1,
            showOn: "both",
            buttonImage: "images/calendar.png",
            buttonImageOnly: false,
            buttonClass: "glyphicon-calendar"
        }).next(".ui-datepicker-trigger").addClass("btn btn-default glyphicon glyphicon-calendar calander-icon pull-right");

        jQuery("#EndDate").datepicker({
            //defaultDate: "+1w",
            defaultDate: new Date(),
            dateFormat: "<%=DateTimeHelper.DateFormatJavaScript%>",
            minDate: 0,
            maxDate: 0,
            numberOfMonths: 1,
            showOn: "both",
            buttonImage: "images/calendar.png",
            buttonImageOnly: false,
            buttonClass: "glyphicon-calendar"
        }).next(".ui-datepicker-trigger").addClass("btn btn-default glyphicon glyphicon-calendar calander-icon pull-right");
        var msclient = $("#ClientList").msDropdown({ roundedCorner: false });
        $(".hotel-search-box button[type='reset']").click(function () {
            setTimeout(function () { document.getElementById("ClientList").refresh(); }, 500);
        });
        //IsSubmitSearchButtonDisabled();

        //msclient.on("change", function () {
        //    IsSubmitSearchButtonDisabled();
        //});

        //function IsSubmitSearchButtonDisabled() {
        //    if (($("#ClientList option:selected").index()) == 0) {
        //        $('button#btnSearch').prop('disabled', true);
        //    }
        //    else {
        //        $('button#btnSearch').prop('disabled', false);
        //    }
        //}

        $('button#btnSearch').click(function () {
            LoadQueries(null);
        });

        $('button#btnGraph').click(function () {
            LoadQueries("graph=true");
        });

        function LoadQueries(queryStr) {
            var obj = {};
            if ($("#ClientList").val() != "Please Select")
                obj.ClientID = $("#ClientList").val()
            if ($("#QueryType").val() != "")
                obj.QueryTypeID = $("#QueryType").val()
            if ($("#StartDate").val() != "")
                obj.StartDate = new Date($("#StartDate").val().split("/").reverse().join("-"))
            if ($("#EndDate").val() != "")
                obj.EndDate = new Date($("#EndDate").val().split("/").reverse().join("-"))
            if ($("#txtFirstName").val() != "")
                obj.FirstName = $("#txtFirstName").val()
            if ($("#txtLastName").val() != "")
                obj.LastName = $("#txtLastName").val()
            if ($("#ddlCompaignGroup").val() != "")
                obj.CampaignGroupID = $("#ddlCompaignGroup").val()
            if ($("#ddlProvider").val() != "")
                obj.ProviderId = $("#ddlProvider").val()
            if ($("#bookingref").val() != "")
                obj.BookingReference = $("#bookingref").val()
            if ($('#<%= IsSolved.ClientID %> input:checked').val() != "") {
                var IsSolved = $('#<%= IsSolved.ClientID %> input:checked').val()
                if (IsSolved == "Yes")
                    obj.IsResolved = true;
                else if (IsSolved == "No")
                    obj.IsResolved = false;
            }
            CS.CommunicationService.LoadCustomerQueries(obj, queryStr);
        }

        $('button#btnPrint').click(function () {
            var divContents = $("#custqueryresult").html();
            //Get the HTML of whole page
            var printWindow = window.open('Print_Page', 'scrollbars=yes');
            printWindow.document.write('<html><head><title>Search Customer Query</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
            printWindow.close();

        });


    });
</script>
<script>
   
</script>
