CREATE TABLE [dbo].[uCommerce_FreeNightAward](
	[FreeNightAwardId] [int] NOT NULL,
	[NoOfNight] [int] NOT NULL,
 CONSTRAINT [PK_uCommerce_FreeNightAward] PRIMARY KEY CLUSTERED 
(
	[FreeNightAwardId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[uCommerce_FreeNightAward]  WITH CHECK ADD  CONSTRAINT [FK_uCommerce_FreeNightAward_uCommerce_Award] FOREIGN KEY([FreeNightAwardId])
REFERENCES [dbo].[uCommerce_Award] ([AwardId])
GO

ALTER TABLE [dbo].[uCommerce_FreeNightAward] CHECK CONSTRAINT [FK_uCommerce_FreeNightAward_uCommerce_Award]
GO


