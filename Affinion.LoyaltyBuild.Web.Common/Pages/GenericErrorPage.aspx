﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GenericErrorPage.aspx.cs" Inherits="Affinion.LoyaltyBuild.Web.Common.Pages.GenericErrorPage" %>

<!DOCTYPE html>
<html>
<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <%--<title></title>--%>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Bootstrap, a sleek, intuitive, and powerful mobile first front-end framework for faster and easier web development.">
    <meta name="keywords" content="HTML, CSS, JS, JavaScript, framework, bootstrap, front-end, frontend, web development">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <script src="scripts/jquery.min.js"></script>
    <script src="bootstrap_assets/javascripts/bootstrap.min.js"></script>
    <script src="scripts/jquery-1.10.2.js"></script>
    <script src="scripts/jquery.bxslider.min.js"></script>
    <link rel="stylesheet" href="/Resources/styles/bootstrap.min.css">
    <link href="/Resources/styles/responsiveNav.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/Resources/styles/affinion.min.css">
    <link rel="stylesheet" href="/Resources/styles/<%= this.SelectedTheme%>/theme.css">
    <link href="/Resources/styles/affinion-dev.css" rel="stylesheet" />

    <!-- theme css -->
    <!--<link rel="stylesheet" href="/Resources/styles/theme-red.css" />-->

    <%--<link href="styles/jquery.bxslider.css" rel="stylesheet" />--%>
    <%--    <link rel="stylesheet" href="styles/bootstrap-social.css">--%>
    <%--<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">--%>

    <script type="text/javascript">
        function redirectToHome() {
            window.location.href = 'http://affinionlbclientportal';
            return false;
        }

    </script>
</head>
<body>
    <div class="outer-container">
        <div class="container-outer width-full overflow-hidden-att">
            <div class="error-page-top header-top-bar">
            </div>
        </div>
        <div class="body-container">
            <form id="form1" runat="server">
                <div class="container">
                    <div class="col-md-12">
                        </br>
                 </br>
                <h1 class="genric_err">Sorry. An error occured. Please contact Call Center on +94123456789-10 for further support.</h1>
                    </div>
                    <div class="col-md-12">

                        <div class="pull-left">
                            <a class="btn btn-default add-break pull-right" href="/">Return Home</a>
                            <%--<Button class="btn btn-default add-break pull-right" ID="ReturnHome" onclick="redirectToHome();" >Return Home</Button>--%>
                        </div>
                    </div>
                </div>

                <div class="hide">
                    <%= this.ExceptionMessage%>
                </div>

            </form>
        </div>
        <!-- Site footer -->
        <div class="width-full">
            <div class="footer-container">
                <div class="row footer-bottom margin-common">
                    <div class="footer">
                        <div class="container-outer-inner width-full footer-bottom-bar">
                            <div class="container">
                            </div>
                        </div>
                    </div>
                    <div class="container-outer-inner width-full pay-icon">
                        <div class="container">
                        </div>
                    </div>
                    <div class="container-outer-inner width-full copyright-bar">
                        <div class="container">
                            <div class="copyright-text">
                                <p>&copy; 2016 Loyaltybuild. All rights reserved.</p>
                            </div>
                        </div>
                    </div>
                    <div class="scroll-top-wrapper "><span class="scroll-top-inner"></span></div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
