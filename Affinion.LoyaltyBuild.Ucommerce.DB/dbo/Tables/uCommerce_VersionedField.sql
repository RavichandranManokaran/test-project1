﻿CREATE TABLE [dbo].[uCommerce_VersionedField] (
    [VersionedFieldId] INT              IDENTITY (1, 1) NOT NULL,
    [ItemId]           UNIQUEIDENTIFIER NOT NULL,
    [FieldId]          UNIQUEIDENTIFIER NOT NULL,
    [Language]         VARCHAR (MAX)    NULL,
    [Version]          INT              NULL,
    [FieldValue]       VARCHAR (MAX)    NULL
);

