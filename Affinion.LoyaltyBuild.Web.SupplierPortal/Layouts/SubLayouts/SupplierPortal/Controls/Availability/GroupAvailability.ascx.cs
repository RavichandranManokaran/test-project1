﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;
using Sitecore.Data.Items;
using Sitecore.Links;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Affinion.LoyaltyBuild.UCom.Common.Extension;
using Affinion.LoyaltyBuild.Common.Utilities;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Availability
{
    public partial class GroupAvailability : System.Web.UI.UserControl
    {
        public List<ProviderAvialbilityData> avialbilityView;
        public List<ProductDetail> productDetail;
       
        protected void Page_Load(object sender, EventArgs e)
        {
            DateTime startDate = DateTime.Now;
            DateTime endDate = new DateTime(startDate.Year, startDate.Month, 1).AddMonths(1).AddDays(-1);
            datepickerFrom.Value = startDate.ConvertDateToString();
            datepickerTo.Value = endDate.ConvertDateToString();
            
            serverdate.Value = startDate.ConvertDateToString();
           
            if (Request["fromdate"] != null)
            {
                DateTime fromDate = Request["fromdate"].ToString().ParseFormatDateTime();
                if (fromDate < startDate)
                {
                    fromDate = startDate;
                }
                datepickerFrom.Value = fromDate.ConvertDateToString();
            }
            if (Request["todate"] != null)
            {
                DateTime todate = Request["todate"].ToString().ParseFormatDateTime();
                if (todate > startDate.AddDays(89))
                {
                    todate = endDate;
                }
                datepickerTo.Value = todate.ConvertDateToString();
            }
            String loggedInProvider_ucom = SitecoreUserProfile.GetItem(SessionKeys.loggedInSupplier_ucom);
            Guid ucomCategoryId;
            bool validGuid = Guid.TryParse(loggedInProvider_ucom, out ucomCategoryId);
            AvailabilityHelper helper = new AvailabilityHelper(HttpContext.Current);
            if (validGuid)
            {

                productDetail = helper.GetProductDetail();
            }
            else
            {
                productDetail = new List<ProductDetail>();
            }
        }
        public ListItemCollection GetHotelInGroup()
        {
            var loggedInSupplier = SitecoreUserProfile.GetItem(UserProfileKeys.loggedInSupplier);
            var loggedInSupplierID = SitecoreUserProfile.GetItem(UserProfileKeys.loggedInProviderId);
            var loggedInSupplierDisplayName = SitecoreUserProfile.GetItem(UserProfileKeys.loggedinSupplierDisplayName);
            var hotels = new ListItemCollection();
            if (Sitecore.Context.Database.SelectItems(Constants.hotelPath) != null)
            {
                Item[] Hotels = Sitecore.Context.Database.SelectItems(Constants.hotelPath);

                if (Hotels != null)
                {
                    Hotels.OrderBy(a => a.DisplayName);
                    
                    foreach (var hotel in Hotels)
                    {
                        Sitecore.Data.Fields.ReferenceField droplinkFld = hotel.Fields[Constants.group];
                        if (droplinkFld != null)
                        {
                            if (droplinkFld.TargetItem != null)
                            {
                                Sitecore.Data.Items.Item targetItem2 = droplinkFld.TargetItem;
                                if (targetItem2 != null && targetItem2.DisplayName != null && loggedInSupplier != null && targetItem2.ID.ToString().Equals(loggedInSupplierID, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    ListItem li = new ListItem();
                                    li.Text = SitecoreFieldsHelper.GetValue(hotel, "Name");
                                    li.Value = hotel.ID.ToString();
                                    hotels.Add(li);
                                }
                            }
                        }
                    }
                }
            }
            return hotels;
        }

        protected void HotelNameLink_Click(object sender, EventArgs e)
        {
            LinkButton lnkBtn = (LinkButton)(sender);
            string hotelId = lnkBtn.CommandArgument;
            //Item hotel = Sitecore.Context.Database.SelectItems(Constants.hotelPath).Where(x => x.ID.ToString().Equals(hotelId, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
            //if (hotel != null && !hotel.Equals(""))
            //{
            //    if (hotel.Fields["UCommerceCategoryID"].Value != null)
            //    {
            //        SitecoreUserProfile.AddItem(hotel.Fields["UCommerceCategoryID"].Value, UserProfileKeys.loggedInSupplier_ucom);
            //    }
            //}
            Response.Redirect("/welcome-page/manage-availablity");
        }

       
    }
}