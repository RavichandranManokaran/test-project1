﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           UcommerceCustomPrice.cs
/// Sub-system/Module:     Client Setup(VS project name)
/// Description:           Overiding Ucommerce PriceService class for currency convension
/// </summary>


#region Use Directives
using Affinion.LoyaltyBuild.Common.Instrumentation;
using System;
using UCommerce;
using UCommerce.Catalog;  
#endregion


namespace Affinion.LoyaltyBuild.SupplierSetup.CurrencyConverter
{
    public class UcommerceCustomPrice : PricingService
    {
        /// <summary>
        /// Ovriding Price Service Class convension.
        /// </summary>
        /// <param name="product">product</param>
        /// <param name="priceGroup">priceGroup</param>
        /// <returns></returns>
        #region Public Methods
        public  override Money GetProductPrice(UCommerce.EntitiesV2.Product product, UCommerce.EntitiesV2.PriceGroup priceGroup)
        {
            try
            {
                //Orginal price
                var originalPrice = base.GetProductPrice(product, priceGroup);
                //converted price 
                var ToCurrecy = base.GetProductPrice(product, priceGroup);

                ConverterService cc = new ConverterService();
                //get Exchange rate
                var EXrate = cc.Convert(originalPrice.Currency.ISOCode, "AUD");

                //orginal price calculated based on the exchange Rate
                var convertedPrice = originalPrice.Value * EXrate;

                //To currency profile
                ToCurrecy.Currency.ISOCode = "AUD";

                //return Decimal amount and Currency 
                return new Money(convertedPrice, ToCurrecy.Currency);

            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.SupplierSetup, ex, this);
                throw;
            }

        } 
        #endregion
    }
}
