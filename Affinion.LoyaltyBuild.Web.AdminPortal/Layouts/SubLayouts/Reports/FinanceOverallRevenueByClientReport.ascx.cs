﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Sitecore.Data.Items;
using Affinion.LoyaltyBuild.Common.Utilities;

namespace Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.Reports
{
    public partial class FinanceOverallRevenueByClientReport : System.Web.UI.UserControl
    {
        List<FinanceOverallRevenue> lstFinanceOverallRevenue = null;
        protected int TotalOfRecords { get; private set; }
        private decimal exchangeGBP =1, exchangeSEK =1, exchangeDKK = 1, exchangeNOK = 1, exchangeCHF = 1, exchangeUSD = 1;
        protected void Page_Load(object sender, EventArgs e)
        {
            
            //Binding Sitecore Text
            BindCoreText();

        }

        protected void Search_Click(object sender, EventArgs e)
        {
            try
            {
                BindSearchData();
            }
            catch(Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.AdminPortal, ex, null);
                throw;
            }

        }
        protected void ResultGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ResultGridView.PageIndex = e.NewPageIndex;
            BindSearchData();
        }

        private void BindSearchData()
        {
            IReportsService iReportsService = new ReportsService();
            SearchCriteria objSearchCriteria = new SearchCriteria();
            DateTime startDate = new DateTime();
            DateTime endDate = new DateTime();

            objSearchCriteria.BookingMethodID = int.Parse(this.BookingMethod.SelectedItem.Value);
            objSearchCriteria.SubTypeReportID = int.Parse(this.SubTypeReport.SelectedItem.Value);
            if (DateTime.TryParse(this.StartDate.Value, out startDate))
                objSearchCriteria.StartDate = startDate;
            if (DateTime.TryParse(this.EndDate.Value, out endDate))
                objSearchCriteria.EndDate = endDate;
            lstFinanceOverallRevenue = iReportsService.GetFinanceOverallRevenueReport(objSearchCriteria);
            UpdateClientNameAndExchangeRate();

            this.ResultGridView.DataSource = lstFinanceOverallRevenue;
            this.ResultGridView.DataBind();

        }

        private void UpdateClientNameAndExchangeRate()
        {
            if(!string.IsNullOrEmpty(GBP.Value))
                exchangeGBP = decimal.Parse(GBP.Value);
            if (!string.IsNullOrEmpty(SEK.Value))
                exchangeSEK = decimal.Parse(SEK.Value);
            if (!string.IsNullOrEmpty(DKK.Value))
                exchangeDKK = decimal.Parse(DKK.Value);
            if (!string.IsNullOrEmpty(NOK.Value))
                exchangeNOK = decimal.Parse(NOK.Value);
            if (!string.IsNullOrEmpty(CHF.Value))
                exchangeCHF = decimal.Parse(CHF.Value);
            if (!string.IsNullOrEmpty(USD.Value))
                exchangeUSD = decimal.Parse(USD.Value);

            if (exchangeGBP == 0)
                exchangeGBP = 1;
            if (exchangeSEK == 0)
                exchangeSEK = 1;
            if (exchangeDKK == 0)
                exchangeDKK = 1;
            if (exchangeNOK==0)
                exchangeNOK = 1;
            if (exchangeCHF==0)
                exchangeCHF = 1;
            if (exchangeUSD==0)
                exchangeUSD = 1;

            if(lstFinanceOverallRevenue!=null && lstFinanceOverallRevenue.Count >0)
            {
                TotalOfRecords = 0;
                foreach(FinanceOverallRevenue objFinanceOverallRevenue in  lstFinanceOverallRevenue)
                {
                    Item clientItem = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(objFinanceOverallRevenue.ClientID));
                    objFinanceOverallRevenue.ClientName = clientItem.DisplayName;
                    decimal totalAmount = 0;
                    totalAmount = objFinanceOverallRevenue.TotalPrice;
                    TotalOfRecords += objFinanceOverallRevenue.Bookings;
                    Currency currency;
                    if (Enum.TryParse(objFinanceOverallRevenue.CurrencyName, out currency))
                    {
                        switch (currency)
                        {
                            case Currency.GBP:
                                objFinanceOverallRevenue.AccountingRates = totalAmount * exchangeGBP;
                                objFinanceOverallRevenue.BudgetRates = totalAmount * exchangeGBP;
                                break;
                            case Currency.SEK:
                                objFinanceOverallRevenue.AccountingRates = totalAmount * exchangeSEK;
                                objFinanceOverallRevenue.BudgetRates = totalAmount * exchangeSEK;
                                break;
                            case Currency.DKK:
                                objFinanceOverallRevenue.AccountingRates = totalAmount * exchangeDKK;
                                objFinanceOverallRevenue.BudgetRates = totalAmount * exchangeDKK;
                                break;
                            case Currency.NOK:
                                objFinanceOverallRevenue.AccountingRates = totalAmount * exchangeNOK;
                                objFinanceOverallRevenue.BudgetRates = totalAmount * exchangeNOK;
                                break;
                            case Currency.CHF:
                                objFinanceOverallRevenue.AccountingRates = totalAmount * exchangeCHF;
                                objFinanceOverallRevenue.BudgetRates = totalAmount * exchangeCHF;
                                break;
                            case Currency.USD:
                                objFinanceOverallRevenue.AccountingRates = totalAmount * exchangeUSD;
                                objFinanceOverallRevenue.BudgetRates = totalAmount * exchangeUSD;
                                break;
                            case Currency.EUR:
                                objFinanceOverallRevenue.AccountingRates = totalAmount;
                                objFinanceOverallRevenue.BudgetRates = totalAmount;
                                break;

                            default:
                                objFinanceOverallRevenue.AccountingRates = 0;
                                objFinanceOverallRevenue.BudgetRates = 0;
                                break;
                        }
                    }
                }
            }
        }

        private enum Currency
        {
            STC,
            SEK,
            EUR,
            GBP,
            USD,
            NOK,
            DKK,
            CHF
        }

        /// <summary>
        /// Binding SitecoreTexts
        /// </summary>  
        private void BindCoreText()
        {
            Item currentItem = Sitecore.Context.Item;
            //SitecoreFieldsHelper.BindSitecoreText(currentItem, "Finance Overall Revenue by client report", scOracleReport);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Fill in the parameters for your report", scFill);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Enter a value for GBP", scGBP);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Enter a value for SEK", scSEk);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Enter a value for DKK", scDKK);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Enter a value for NOK", scNOK);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Enter a value for CHF", scCHF);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Enter a value for USD", scUSD);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Booking Method", scBookingMethod);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "SubType Of Report", scSubType);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Filter by date", scFilter);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Start Date", scStartDate);

            SitecoreFieldsHelper.BindSitecoreText(currentItem, "End Date", scEndDate);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Booking details reports", scBookingReport);

           
            
        }
    }
}