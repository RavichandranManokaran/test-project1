﻿using Affinion.LoyaltyBuild.Api.Search.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.SessionState;

namespace Affinion.LoyaltyBuild.Api.Search.Store
{
    class SessionSearchStorage : ISearchStore
    {
        private readonly  string _searchKeySessionId = "_searchKey";
        private readonly string _searchResultSessionId = "_searchResult";
        private HttpSessionState _session;

        /// <summary>
        /// Constructor
        /// </summary>
        public SessionSearchStorage()
        {
            _session = HttpContext.Current.Session;
        }

        /// <summary>
        /// Get serach key
        /// </summary>
        /// <returns></returns>
        public SearchKey GetSearchKey()
        {
            return (SearchKey)_session[_searchKeySessionId];
        }

        /// <summary>
        /// Save search key
        /// </summary>
        /// <param name="key"></param>
        public void SetSearchKey(SearchKey key)
        {
            _session[_searchKeySessionId] = key;
        }

        /// <summary>
        /// Get search result
        /// </summary>
        /// <returns></returns>
        public PagedProviderCollection GetSearchResult()
        {
            return (PagedProviderCollection)_session[_searchResultSessionId];
        }

        /// <summary>
        /// Set search result
        /// </summary>
        /// <param name="collection"></param>
        public void SetSearchRestult(PagedProviderCollection collection)
        {
            _session[_searchResultSessionId] = collection;
        }
    }
}
