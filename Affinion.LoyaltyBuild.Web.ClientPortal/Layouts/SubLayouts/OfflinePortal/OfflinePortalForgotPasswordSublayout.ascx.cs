﻿//<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           ForgotPasswordSublayout.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal
/// Description:           Used to Bind data to ForgotPasswordSublayout subLayout
/// </summary>

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    #region Using Directives
    using Affinion.Loyaltybuild.Security;
    using Affinion.LoyaltyBuild.Common;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Communications.Services;
    using Affinion.LoyaltyBuild.SupplierPortal.DataAccess;
    using Affinion.LoyaltyBuild.SupplierPortal.DataAccess.Service;
    using Sitecore.Data.Items;
    using Sitecore.Security.AccessControl;
    using Sitecore.Security.Accounts;
    using Sitecore.Text;
    using Sitecore.Web;
    using System;
    using System.Configuration;
    using System.Net.Mail;
    using System.Security.Authentication;
    using System.Web;
    using System.Web.Security;
    #endregion

    public partial class OfflinePortalForgotPasswordSublayout : System.Web.UI.UserControl
    {
        #region Private Properties
        private string SuccessMessage { get; set; }
        private string FailierMessage { get; set; }
        private string UserNotExist { get; set; }
        private string FromAddress { get; set; }
        private string FromDisplayName { get; set; }
        private string Subject { get; set; }
        private string EmailBody { get; set; }
        private string ChangePasswordPage { get; set; }
        private string WelcomePage { get; set; }
        #endregion

        protected readonly ILoginService loginSvc;
        public OfflinePortalForgotPasswordSublayout()
        {
            this.loginSvc = new LoginService();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            InitPage();

            //If page editor mode, no redirection
            if (!Sitecore.Context.PageMode.IsPageEditor && Sitecore.Context.User.IsAuthenticated)
            {
                Response.Redirect(WelcomePage);
            }
        }


        protected void ButtonForgot_Click(object sender, EventArgs e)
        {
            try
            {
                string domainUser = Sitecore.Context.Domain.GetFullName(TextBoxUser.Text);
                if (!User.Exists(domainUser))
                {
                    throw new AuthenticationException(UserNotExist);
                }
                else
                {

                    if (IsAuthorizedUser(User.FromName(domainUser, false)))
                    {

                        if (System.Web.Security.Membership.EnablePasswordReset && !Membership.RequiresQuestionAndAnswer)
                        {
                            //Reset password is enabled

                            ResetPasswordEnebled(domainUser);
                        }
                        else
                        {
                            throw new ConfigurationErrorsException("Cannot retrieve or reset passwords.");
                        }
                    }
                    else
                    {
                        throw new AuthenticationException(UserNotExist);
                    }
                }
            }
            catch (AuthenticationException ex)
            {
                ErrorMessage(ex.Message);
            }
            catch (SmtpException ex)
            {
                ErrorMessage(ex.Message);
            }
            catch (ArgumentException ex)
            {
                ErrorMessage(UserNotExist);

                //Log
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
            }
            catch (Exception ex)
            {
                //Log
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Error messages
        /// </summary>
        /// <param name="message"></param>
        private void ErrorMessage(string message)
        {
            literalMessage.Text = message;
            divError.Visible = true;
        }

        /// <summary>
        /// Reset Password enabled
        /// </summary>
        private void ResetPasswordEnebled(string username)
        {
            MembershipUser user = Membership.GetUser(username);

            string password = user.ResetPassword();

            if (!string.IsNullOrEmpty(password))
            {
                /// get username(without domain name)
                string userShortname = Sitecore.Context.Domain.GetShortName(TextBoxUser.Text);

                //send mail
                SendMail(username, password, user.Email);

                //message
                divError.Visible = false;
                divsuccess.Visible = true;
                PanelEdit.Visible = false;
                literalSuccess.Text = SuccessMessage;

            }
        }

        /// <summary>
        /// Check User Authrization
        /// </summary>
        /// <returns></returns>
        private static bool IsAuthorizedUser(User user)
        {
            using (new UserSwitcher(ItemHelper.ContentEditingUser))
            {
                return SitecoreAccess.IsAllowed(user, AccessRight.ItemRead, Sitecore.Context.Site.RootPath, Sitecore.Context.Database.ToString());
            }
        }


        /// <summary>
        /// Create Email body
        /// </summary>
        /// <param name="username">username</param>
        /// <param name="password">password</param>
        /// <returns>Email body</returns>
        private string MessageBody(string username, string password,string uniqueId)
        {
            try
            {
                return EmailBody.Replace("[username]", username).Replace("[password]", password).Replace("[returnUrl]",  Sitecore.Context.Site.HostName+ChangePasswordPage+"?id=" + uniqueId);

            }
            catch (Exception ex)
            {
                //Log
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Return link
        /// </summary>
        private string ReturnLink
        {
            get
            {
                try
                {
                    string host = HttpContext.Current.Request.Url.Host;

                    UriBuilder uribuilder = new UriBuilder(host + ChangePasswordPage);

                    //set query string params
                    UrlString url = new UrlString(uribuilder.Uri.ToString());
                    url.Append("passreset", "1");
                    return url.GetUrl();
                }
                catch (Exception ex)
                {

                    //Log
                    Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                    throw;
                }
            }

        }

        /// <summary>
        /// Create mail divError
        /// </summary>
        /// <param name="username">username</param>
        /// <param name="password">password</param>
        /// <param name="email">mail</param>
        /// <returns>mail divError</returns>
        private void SendMail(string username, string password, string email)
        {
            try
            {
                var r = new Random();
                string uniqueNumber = loginSvc.GenerateRandomNumbers(r);
                loginSvc.InsertChangePasswordHistory(uniqueNumber, username);
                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress(FromAddress, FromDisplayName);
                    mail.To.Add(new MailAddress(email, username));
                    mail.Subject = Subject;
                    mail.IsBodyHtml = true;
                    mail.Body = MessageBody(username, password, uniqueNumber);

                    //Mail service
                    EmailService service = new EmailService();
                    //send mail
                    service.Send(mail);

                }
            }
            catch (Exception ex)
            {
                //Log
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }

        }

        /// <summary>
        /// Get url from link field
        /// </summary>
        /// <param name="item">Item</param>
        /// <param name="fieldName">Field name</param>
        /// <returns>Url</returns>
        private static string GetLink(Item item, string fieldName)
        {
            var link = SitecoreFieldsHelper.GetUrl(item, fieldName);
            return link.Url;
        }

        /// <summary>
        /// Page Settings
        /// </summary>
        private void SetSettings()
        {
            try
            {
                using (new UserSwitcher(ItemHelper.ContentEditingUser))
                {
                    string itemPath = Constants.OfflinePortalSettings;
                    Item settings = Sitecore.Context.Database.GetItem(itemPath);

                    if (settings != null)
                    {
                        //Links
                        this.ChangePasswordPage = GetLink(settings, "ChangePassword");
                        this.WelcomePage = GetLink(settings, "Welcome");
                    }
                }
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }

        }

        /// <summary>
        /// Initialize Page
        /// </summary>
        private void InitPage()
        {
            try
            {

                //Buttons
                this.ButtonForgot.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Submit", "Submit");

                //Messages
                this.SuccessMessage = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Success", "New Password details has been sent to your email.");
                this.FailierMessage = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Failier", "Failied");
                this.UserNotExist = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "InvalidUser", "Invalid User");

                //Email
                this.FromAddress = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "FromAddress", "supplyportal@mail.com");
                this.FromDisplayName = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "FromDisplayName", "Call Centre Support");
                this.EmailBody = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "EmailBody", "Email Body");
                this.Subject = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Subject", "Password Reset");

                SetSettings();

            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }
    }


}