﻿CREATE TABLE [dbo].[uCommerce_DueTypes]
(
                [DueTypeId] INT IDENTITY(1,1) NOT NULL PRIMARY KEY ,
				[Description] VARCHAR(MAX),
                [IsActive] BIT NOT NULL,
                [CanBeDeleted] BIT NOT NULL,
                [ShowInList] BIT NOT NULL,
				[PaymentDueTypeId] INT
)
GO