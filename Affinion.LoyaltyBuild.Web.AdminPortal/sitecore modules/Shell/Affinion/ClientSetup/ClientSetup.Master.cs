﻿namespace Affinion.LoyaltyBuild.AdminPortal.ClientSetup
{
    using System;
    using System.Web;

    /// <summary>
    /// Master page for client setup screens
    /// </summary>
    public partial class ClientSetup : System.Web.UI.MasterPage
    {
        /// <summary>
        /// Code to execute on page load
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The parameter</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // redirect the user to login page if not logged in
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Response.Redirect("/sitecore/login?returnUrl=" + HttpContext.Current.Request.Path);
            }
        }
    }
}