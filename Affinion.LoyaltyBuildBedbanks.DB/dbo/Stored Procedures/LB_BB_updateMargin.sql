
/****** Object:  StoredProcedure [dbo].[LB_BB_updateMargin]    Script Date: 7/19/2016 7:28:55 PM ******/


---- ====================================================
---- Description:	RETURNS MARGIN FOR A PROPERTY BASED ON AVAILABILITY SOURCE
---- Written: RICHARD
---- Created: 2016.04.19
---- ====================================================
CREATE PROCEDURE [dbo].[LB_BB_updateMargin]
	
	-- Input parameters 
	@pi_id int,
	@pi_Datefrom int,
	@pi_Dateto int,
	@pi_isactive int,	
	@pi_userid int,
	@pi_marginPercentage money,
	
	--Return parameters
	@po_returnstatus INT OUTPUT,
	@po_returnmsg VARCHAR(200) OUTPUT

	
AS
set nocount on;
BEGIN

	--Assume transaction will fail, set the return variable to -1.
	SET @po_returnstatus = -1;
	SET @po_returnmsg = null;

	
	DECLARE @PROVIDERID int, @Datefrom int, @DateTo int, @MarginPercentage Money, @ISACTIVE bit
	
	
	
	BEGIN TRY
	
	Select @PROVIDERID  = _ProviderID, @Datefrom=DateFrom,@DateTo=DateTo,@MarginPercentage=marginPercentage,@ISACTIVE=isactive  from LB_BB_providerMargin where ID = @pi_id
	
	IF ( @Datefrom <> @pi_Datefrom or @DateTo <> @pi_DateTo or @MarginPercentage <> @pi_marginPercentage or @ISACTIVE <> @pi_isactive )
	BEGIN
		IF @Datefrom = '1111' or @DateTo = '99999' or @Datefrom = '0' or @DateTo = '0'
		BEGIN
			Insert into LB_BB_providerMargin
				Values (@PROVIDERID,'1111','99999',@pi_marginPercentage,'1',@pi_userid,GETDATE())

			Update LB_BB_providerMargin set isactive = -1
			where ID = @pi_id
		END
		ELSE
			BEGIN
			Insert into LB_BB_providerMargin
				Values (@PROVIDERID,@pi_Datefrom,@pi_Dateto,@pi_marginPercentage,@pi_isactive,@pi_userid,GETDATE())

			Update LB_BB_providerMargin set isactive = case when isactive = 0 then 0 else -1 end
			where ID = @pi_id
		END	
	END	      
    END TRY	
	BEGIN CATCH	  
		
	
		SET @po_returnstatus = error_number();
		SET @po_returnmsg = error_message();
		RETURN;
	END CATCH
	
	SET @po_returnstatus = 0;
	SET @po_returnmsg = 'Completed successfully.';	
END






GO

