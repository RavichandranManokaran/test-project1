﻿using Affinion.LoyaltyBuild.Api.Booking.Data.Basket;
using Affinion.LoyaltyBuild.Api.Booking.Helper;
using Affinion.LoyaltyBuild.Api.Booking.Extension;
using Affinion.LoyaltyBuild.BedBanks.Helper;
using Affinion.LoyaltyBuild.BedBanks.JacTravel;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Model;
using Affinion.LoyaltyBuild.Model.Common;
using Affinion.LoyaltyBuild.Model.Product;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using UCommerce.Api;
using UCommerce.EntitiesV2;
using data = Affinion.LoyaltyBuild.Api.Booking.Data;
using Sitecore.Data.Items;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.BedBanks.General;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Message;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Api.Booking.Data;

namespace Affinion.LoyaltyBuild.Api.Booking.Service
{
    /// <summary>
    /// Booking service
    /// </summary>
    public class BookingService : IBookingService
    {
        private string _connectionString;
        private ILogger _logger;

        /// <summary>
        /// Constructor
        /// </summary>
        public BookingService()
        {
            _logger = new Logger();
            _connectionString = "uCommerce";
        }


        /// <summary>
        /// Get basket
        /// </summary>
        /// <returns></returns>
        public data.BasketInfo GetBasket()
        {
            var basket = TransactionLibrary.GetBasket();
            if (basket == null)
                return null;

            return GetPurchaseOrder(basket.PurchaseOrder.OrderId);
        }

        /// <summary>
        /// Get basket
        /// </summary>
        /// <returns></returns>
        public data.BasketInfo GetPurchaseOrder(int orderId)
        {
            var basketInfo = new data.BasketInfo(orderId);
            var items = new List<data.Booking>();


            //loop through all bookings
            foreach (var item in PurchaseOrder.FirstOrDefault(i => i.OrderId == orderId).OrderLines.OrderBy(i => i.OrderLineId))
            {
                var booking = GetBooking(item.OrderLineId);

                //add to parent if it is a child booking or keep it the original list
                if (!booking.IsChildBooking)
                {
                    items.Add(booking);
                }
            }
            basketInfo.Bookings = items;

            //return basket info
            return basketInfo;
        }

        /// <summary>
        /// Get booking by orderlineid
        /// </summary>
        /// <param name="orderLineId"></param>
        /// <returns></returns>
        public data.Booking GetBooking(int orderLineId)
        {
            var orderLine = OrderLine.FirstOrDefault(i => i.OrderLineId == orderLineId);
            if (orderLine == null)
                return null;

            var productType = ProductType.Room;

            Enum.TryParse<ProductType>(orderLine[Constants.ProductType], out productType);

            return GetBooking(orderLineId, productType);
        }

        /// <summary>
        /// get booking by orderline nad product type
        /// </summary>
        /// <param name="orderLineId"></param>
        /// <param name="productType"></param>
        /// <returns></returns>
        public data.Booking GetBooking(int orderLineId, ProductType productType)
        {
            data.Booking booking = null;

            switch (productType)
            {
                case ProductType.Room:
                case ProductType.Package:
                    booking = new data.RoomBooking(orderLineId);
                    break;
                case ProductType.Addon:
                    booking = new data.AddOnBooking(orderLineId);
                    break;
                case ProductType.BedBanks:
                    booking = new data.BedBanksBooking(orderLineId);
                    break;
            }

            if (booking != null)
            {
                DatabaseProviderFactory factory = new DatabaseProviderFactory();
                Database db = factory.Create(_connectionString);
                var sql = "LB_GetBookingInfo";

                //create command
                using (var command = db.GetStoredProcCommand(sql))
                {
                    db.AddInParameter(command, "OrderLineId", DbType.Int32, orderLineId);

                    var status = BookingStatus.Basket;
                    //execute reader
                    using (var reader = db.ExecuteReader(command))
                    {
                        if (reader.Read())
                        {
                            booking.BookingReferenceNo = reader.Value<string>("BookingNo");
                            booking.CurrentStatusDate = reader.NullValue<DateTime>("CurrentStatusDate");
                            booking.ConfirmedStatusDate = reader.NullValue<DateTime>("ConfirmedStatusDate");
                            Enum.TryParse<BookingStatus>((reader.Value<int>("StatusId")).ToString(), out status);
                            booking.Status = status;
                            booking.CreatedBy = reader.Value<string>("CreatedBy");
                            booking.CreatedDate = reader.Value<DateTime>("CreatedDate");
                            booking.UpdateBy = reader.Value<string>("UpdatedBy");
                            booking.UpdateDate = reader.Value<DateTime>("UpdatedDate");
                            booking.CheckinDate = reader.Value<DateTime>("CheckInDate");
                            booking.CheckoutDate = reader.Value<DateTime>("CheckOutDate");
                        }
                    }
                }
            }

            return booking;
        }

        #region Cancel Booking

        /// <summary>
        /// Validate cancel Booking
        /// </summary>
        /// <param name="orderLineId"></param>
        /// <returns></returns>
        public data.PreCancelBookingResonse ValidateCancelBooking(int orderLineId)
        {
            data.PreCancelBookingResonse response = new data.PreCancelBookingResonse();
            var booking = GetBooking(orderLineId);

            //check if it a valid booking
            if (booking == null)
                response.Message = BookingAlerts.GetAlert(BookingAlerts.InvalidBooking);
            else if (booking.Status == data.BookingStatus.Cancelled || booking.Status == data.BookingStatus.Transferred)
                response.Message = BookingAlerts.GetAlert(BookingAlerts.AlreadyCancelled);
            else if (booking.CheckinDate <= DateTime.Now.Date)
                response.Message = BookingAlerts.GetAlert(BookingAlerts.PastCancelDeadline);
            else if (booking.Type == ProductType.BedBanks)
            {
                //if bed banks booking
                var bedBanks = (data.BedBanksBooking)booking;

                JacTravelApiWrapper jac = new JacTravelApiWrapper();
                BedBanksSettings bbs = new BedBanksSettings(Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(booking.ClientId)));
                BedBanksCurrency actualCurr = new BedBanksCurrency();
                actualCurr.BedBanksCurrencyId = booking.OrderLine.GetOrderProperty(Constants.BBCurrencyId).Value;
                bbs.BedBanksActualCurrency = actualCurr;
                string BBbookingReferecnceNo = (booking.OrderLine.GetOrderProperty(Constants.BBBookingReferenceId) != null && string.IsNullOrEmpty(booking.OrderLine.GetOrderProperty(Constants.BBBookingReferenceId).Value)) ? string.Empty : booking.OrderLine.GetOrderProperty(Constants.BBBookingReferenceId).Value;
                var cancelResponse = jac.PreCancel(BBbookingReferecnceNo, bbs);

                //check response status
                if (cancelResponse.ReturnStatus != null && Convert.ToBoolean(cancelResponse.ReturnStatus.Success))
                {
                    double marginPercentage = 0, price = 0, marginAmt = 0;
                    double.TryParse(booking.OrderLine.GetOrderProperty(Constants.MarginPercentage).Value, out marginPercentage);
                    double.TryParse(booking.OrderLine.GetOrderProperty(Constants.Price).Value, out price);
                    marginAmt = price * (marginPercentage / 100);
                    booking[Constants.Cancellationfee] = booking.CommissionAmount.ToString();
                    booking[Constants.BBCancellationCharges] = (cancelResponse.CancellationCost + marginAmt).ToString();
                    booking[Constants.BBCancellationToken] = cancelResponse.CancellationToken;
                    booking.Save();
                    bool status = false;
                    response.IsSuccess = bool.TryParse(cancelResponse.ReturnStatus.Success, out status);
                    response.CancellationCost = cancelResponse.CancellationCost + marginAmt;
                    response.Message = String.Format(BookingAlerts.GetAlert(BookingAlerts.ValidateCancel), CurrencyHelper.FormatCurrency((decimal)cancelResponse.CancellationCost, booking.CurrencyId));
                }
                else
                {
                    response.Message = BookingAlerts.GetAlert(BookingAlerts.ErrorCancelling);
                }
            }
            else
            {
                booking[Constants.Cancellationfee] = booking.CommissionAmount.ToString();
                response.CancellationCost = (double)booking.CommissionAmount;
                response.Message = String.Format(BookingAlerts.GetAlert(BookingAlerts.ValidateCancel), CurrencyHelper.FormatCurrency((decimal)response.CancellationCost, booking.CurrencyId));
                booking.Save();
                response.IsSuccess = true;
            }

            //return response
            return response;
        }

        /// <summary>
        /// Cancel Booking
        /// </summary>
        /// <param name="cancelRequest"></param>
        /// <returns></returns>
        public ValidationResult CancelBooking(ICancelRequest cancelRequest)
        {
            ValidationResult response = new ValidationResult();
            var booking = GetBooking(cancelRequest.OrderLineId);

            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);

            //call bedbanks service to cancel for bedbanks item
            if (booking.Type == ProductType.BedBanks)
            {
                var bedBanksBooking = (data.BedBanksBooking)booking;
                JacTravelApiWrapper jac = new JacTravelApiWrapper();
                BedBanksSettings bbs = new BedBanksSettings(Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(bedBanksBooking.ClientId)));
                BedBanksCurrency actualCurr = new BedBanksCurrency();
                actualCurr.BedBanksCurrencyId = booking.OrderLine.GetOrderProperty(Constants.BBCurrencyId).Value;
                bbs.BedBanksActualCurrency = actualCurr;
                //TODO:get the values from order property and pass
                double marginPercentage = 0, price = 0, marginAmt = 0;
                double.TryParse(booking.OrderLine.GetOrderProperty(Constants.MarginPercentage).Value, out marginPercentage);
                double.TryParse(booking.OrderLine.GetOrderProperty(Constants.Price).Value, out price);
                marginAmt = price * (marginPercentage / 100);
                string cancellationToken = string.IsNullOrEmpty(booking.OrderLine.GetOrderProperty(Constants.BBCancellationToken).Value) ? string.Empty : booking.OrderLine.GetOrderProperty(Constants.BBCancellationToken).Value;
                string cancellationCharges = string.IsNullOrEmpty(booking.OrderLine.GetOrderProperty(Constants.BBCancellationCharges).Value) ? string.Empty : booking.OrderLine.GetOrderProperty(Constants.BBCancellationCharges).Value;
                double cancellationCost = 0;
                double.TryParse(cancellationCharges, out cancellationCost);
                cancellationCost = cancellationCost - marginAmt;
                var cancelResponse = jac.Cancel(bedBanksBooking.BookingReferenceNo, cancellationCost, cancellationToken, bbs);
                if (cancelResponse.ReturnStatus != null)
                {
                    response.IsSuccess = Convert.ToBoolean(cancelResponse.ReturnStatus.Success);
                    response.Message = cancelResponse.ReturnStatus.Exception;
                }
            }

            if (booking.Type != ProductType.BedBanks || response.IsSuccess)
            {
                try
                {
                    using (var scope = new TransactionScope())
                    {

                        var cancelCommand = db.GetStoredProcCommand("LB_CancelBooking");
                        db.AddInParameter(cancelCommand, "OrderLineId", DbType.Int32, cancelRequest.OrderLineId);
                        db.AddInParameter(cancelCommand, "Note", DbType.String, cancelRequest.NoteToProvider ?? string.Empty);
                        db.AddInParameter(cancelCommand, "CancelReason", DbType.String, cancelRequest.CancelReason ?? string.Empty);
                        db.AddInParameter(cancelCommand, "CreatedBy", DbType.String, cancelRequest.CancelledBy ?? string.Empty);
                        db.ExecuteNonQuery(cancelCommand);

                        var canceldueCommand = db.GetStoredProcCommand("LB_AddCancelDue");
                        db.AddInParameter(canceldueCommand, "OrderLineId", DbType.Int32, cancelRequest.OrderLineId);
                        db.AddInParameter(canceldueCommand, "CreatedBy", DbType.String, cancelRequest.CancelledBy ?? string.Empty);
                        db.ExecuteNonQuery(canceldueCommand);

                        if (booking.Type != ProductType.BedBanks)
                        {
                            var updateStausCommand = db.GetStoredProcCommand("LB_UpdateAvailability");
                            db.AddInParameter(updateStausCommand, "OrderLineId", DbType.Int32, cancelRequest.OrderLineId);
                            db.AddInParameter(updateStausCommand, "IsCancel", DbType.Boolean, true);
                            db.ExecuteNonQuery(updateStausCommand);
                        }

                        var updateStatusCommand = db.GetStoredProcCommand("LB_AddBookingStatus");
                        db.AddInParameter(updateStatusCommand, "OrderLineId", DbType.Int32, cancelRequest.OrderLineId);
                        db.AddInParameter(updateStatusCommand, "Status", DbType.Int32, 7);
                        db.AddInParameter(updateStatusCommand, "CreatedBy", DbType.String, cancelRequest.CancelledBy ?? string.Empty);
                        db.ExecuteNonQuery(updateStatusCommand);

                        response.IsSuccess = true;

                        //if success commit transaction
                        if (response.IsSuccess)
                            scope.Complete();
                    }

                }
                catch (Exception ex)
                {
                    _logger.WriteException(DiagnosticsCategory.Bookings, ex, this);
                    response.IsSuccess = false;
                }

                if (response.IsSuccess)
                    response.Message = BookingAlerts.GetAlert(BookingAlerts.CancelledSuccessfuly);
                else
                    response.Message = BookingAlerts.GetAlert(BookingAlerts.ErrorCancelling);
            }

            //return response
            return response;
        }

        #endregion #region Cancel Booking

        #region Transfer Booking

        /// <summary>
        /// Transfer Booking
        /// </summary>
        /// <param name="transferRequest"></param>
        /// <returns></returns>
        public ValidationResult TransferBooking(ITransferRequest transferRequest)
        {
            var result = new ValidationResult();

            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);

            var isFreeTranfer = GetIsFreeTransfer(transferRequest.OrderLineid);
            try
            {

                using (var scope = new TransactionScope())
                {
                    var transferCommand = db.GetStoredProcCommand("LB_TransferBooking");
                    db.AddInParameter(transferCommand, "Note", DbType.String, transferRequest.NoteToProvider ?? string.Empty);
                    db.AddInParameter(transferCommand, "OrderLineId", DbType.Int32, transferRequest.OrderLineid);
                    db.AddInParameter(transferCommand, "NoOfAdults", DbType.Int32, transferRequest.NoOdAdult);
                    db.AddInParameter(transferCommand, "NoOfChildren", DbType.Int32, transferRequest.NoOfChildren);
                    db.AddInParameter(transferCommand, "ChildrenAgeInfo", DbType.String, transferRequest.ChildrenAgeInfo ?? string.Empty);
                    db.AddInParameter(transferCommand, "CartId", DbType.String, transferRequest.BasketId);
                    db.AddInParameter(transferCommand, "CreatedBy", DbType.String, transferRequest.CreatedBy ?? string.Empty);
                    db.AddOutParameter(transferCommand, "NewOrderLineId", DbType.Int32, 8);
                    db.AddInParameter(transferCommand, "BookingNo", DbType.String, transferRequest.BookingReferenceNumber);
                    db.ExecuteNonQuery(transferCommand);
                    int newOrderLineId = (int)db.GetParameterValue(transferCommand, "NewOrderLineId");

                    var transferDueCommand = db.GetStoredProcCommand("LB_AddTransferDue");
                    db.AddInParameter(transferDueCommand, "@OldOrderLineId", DbType.Int32, transferRequest.OrderLineid);
                    db.AddInParameter(transferDueCommand, "@NewOrderLineId", DbType.Int32, newOrderLineId);
                    db.AddInParameter(transferDueCommand, "IsFreeTransfer", DbType.Boolean, isFreeTranfer);
                    db.AddInParameter(transferDueCommand, "CreatedBy", DbType.String, transferRequest.CreatedBy ?? string.Empty);
                    db.ExecuteNonQuery(transferDueCommand);

                    var updateStatusCommand = db.GetStoredProcCommand("LB_AddBookingStatus");
                    db.AddInParameter(updateStatusCommand, "OrderLineId", DbType.Int32, transferRequest.OrderLineid);
                    db.AddInParameter(updateStatusCommand, "Status", DbType.Int32, 8);
                    db.AddInParameter(updateStatusCommand, "CreatedBy", DbType.String, transferRequest.CreatedBy ?? string.Empty);
                    db.ExecuteNonQuery(updateStatusCommand);

                    var updateStatusNewCommand = db.GetStoredProcCommand("LB_AddBookingStatus");
                    db.AddInParameter(updateStatusNewCommand, "OrderLineId", DbType.Int32, newOrderLineId);
                    db.AddInParameter(updateStatusNewCommand, "CreatedBy", DbType.String, transferRequest.CreatedBy ?? string.Empty);
                    db.ExecuteNonQuery(updateStatusNewCommand);

                    result.IsSuccess = true;
                    scope.Complete();
                }

            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                _logger.WriteException(DiagnosticsCategory.Bookings, ex, this);
            }

            if (result.IsSuccess)
                result.Message = BookingAlerts.GetAlert(BookingAlerts.TransferSuccessful);
            else
                result.Message = BookingAlerts.GetAlert(BookingAlerts.InvalidBooking);

            return result;
        }

        /// <summary>
        /// Get the transfer history details for the item
        /// </summary>
        /// <param name="orderLineId">order line id</param>
        /// <returns>list of transfer history</returns>
        public List<data.TransferHistoryItem> GetBookingTransferHistory(int orderLineId)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "LB_GetTransferHistoryDetail";

            //the result object
            var result = new List<data.TransferHistoryItem>();

            //create command
            using (var command = db.GetStoredProcCommand(sql))
            {
                db.AddInParameter(command, "OrderLineId", DbType.Int32, orderLineId);

                //execute reader
                using (var reader = db.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        data.TransferHistoryItem tmpresult = new data.TransferHistoryItem
                        {
                            OrderLineId = reader.Value<int>("OrderLineId"),
                            SupplierName = reader.Value<string>("SupplierName"),
                            ArrivalDate = reader.Value<DateTime>("ArrivalDate"),
                            DepartureDate = reader.Value<DateTime>("DepartureDate"),
                            NoOfRooms = reader.Value<int>("NoOfRooms"),
                            NoOfNights = reader.Value<int>("NoOfNights"),
                            NoOfPeople = reader.Value<int>("NoOfPeople"),
                            ReservationDate = reader.Value<DateTime>("ReservationDate")
                        };
                        result.Add(tmpresult);
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Get if is free transfer
        /// </summary>
        /// <param name="orderLineId"></param>
        /// <returns></returns>
        public bool GetIsFreeTransfer(int orderLineId)
        {
            var clientID = GetBooking(orderLineId).ClientId;

            //get allowed free tranfers
            int allowedSameHotelTransfers = 0;
            int allowedDifferentHotelTransfers = 0;
            Item clientItem = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(clientID));
            if (clientItem != null)
            {
                int.TryParse(SitecoreFieldsHelper.GetValue(clientItem, "TransferAllowedWithinSameProvider") ?? string.Empty, out allowedSameHotelTransfers);
                int.TryParse(SitecoreFieldsHelper.GetValue(clientItem, "TransferAllowedWithDifferentProvider") ?? string.Empty, out allowedDifferentHotelTransfers);
            }

            //get used free transfers
            int usedSameHotelTransfers = 0,
                usedDifferentHotelTransfers = 0;
            var result = GetBookingTransferHistory(orderLineId);

            for (int i = 0; i < result.Count - 1; i++)
            {
                if (result[i].ProviderId == result[i + 1].ProviderId)
                    usedSameHotelTransfers += 1;
                else
                    usedDifferentHotelTransfers += 1;
            }

            //get provider ids for tranferrred and basket item
            var currentCategoryId = string.Empty;
            if (result != null && result.Count > 0)
            {
                data.TransferHistoryItem objTransferHistoryItem = result.Where(i => i.OrderLineId == orderLineId).FirstOrDefault();
                if (objTransferHistoryItem != null && objTransferHistoryItem.ProviderId != null)
                    currentCategoryId = result.FirstOrDefault(i => i.OrderLineId == orderLineId).ProviderId;
            }
            var basketOrderLine = UCommerce.Api.TransactionLibrary.GetBasket(false).PurchaseOrder.OrderLines.FirstOrDefault();
            var basketCategoryId = basketOrderLine.Sku;

            //check is free tranfer
            return (currentCategoryId == basketCategoryId && usedSameHotelTransfers < allowedSameHotelTransfers)
                || (currentCategoryId != basketCategoryId && usedDifferentHotelTransfers < allowedDifferentHotelTransfers);
        }

        #endregion


        /// <summary>
        /// Booking status
        /// </summary>
        /// <param name="orderLineId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public bool UpdateStatus(int orderLineId, data.BookingStatus status)
        {
            return true;
        }

        /// <summary>
        /// Booking status
        /// </summary>
        /// <param name="orderLineId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public bool UpdateBookingReference(int orderLineId, string bookingReference)
        {
            return true;
        }
    }
}
