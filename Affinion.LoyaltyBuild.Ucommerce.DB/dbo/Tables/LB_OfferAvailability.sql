﻿CREATE TABLE [dbo].[LB_OfferAvailability] (
    [OfferRoomAvailabilityId] UNIQUEIDENTIFIER NOT NULL,
    [CampaignItem]            INT              NOT NULL,
    [SKU]                     NVARCHAR (30)    NOT NULL,
    [VariantSku]              NVARCHAR (30)    NOT NULL,
    [NumberAllocated]         INT              NOT NULL,
    [NumberBooked]            INT              NOT NULL,
    [DateCounter]             INT              NOT NULL,
    [CreatedBy]               NVARCHAR (150)   NULL,
    [UpdateBy]                NVARCHAR (150)   NULL,
    [DateCreated]             DATETIME         NULL,
    [DateLastUpdate]          DATETIME         NULL,
    [IsRestrictedForSale]     BIT              NOT NULL,
    CONSTRAINT [PK_uCommerce_OfferRoomAvailability] PRIMARY KEY CLUSTERED ([OfferRoomAvailabilityId] ASC)
);

