﻿CREATE PROCEDURE [dbo].[LBSupplierPortal_RestrictedRoom]
     @DateFrom DateTime,
     @DateTo DateTime,
     @Provider INT
 AS
 BEGIN
  SELECT p.ProductId as [HotelId],
         p.[Name] as [HotelName],
	     CI.CampaignItemId as [OfferId],
	     CI.[Name] as [OfferName],
	   	 OA.DateCounter [DatesRestricted],
		 count(1) [count]
   FROM LB_OfferAvailability OA
       INNER JOIN uCommerce_Product P on P.Sku=OA.Sku AND p.ParentProductId Is NULL
       INNER JOIN uCommerce_CampaignItem CI on CI.CampaignItemId=OA.CampaignItem
   WHERE
       CI.[Enabled]=1 AND
       CI.[Deleted]=0 AND
       OA.NumberAllocated>0 AND
      ((OA.[DateCounter] between @DateFrom AND @DateTo) OR (OA.[DateCounter] BETWEEN @DateFrom AND @DateTo)) AND 
	   p.ProductId=@Provider 
   GROUP BY  p.ProductId, p.[Name],CI.CampaignItemId,CI.[Name], OA.DateCounter
END

