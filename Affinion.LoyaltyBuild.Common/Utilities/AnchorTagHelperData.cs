﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           AnchorTagHelperData.cs
/// Sub-system/Module:     Common(VS project name)
/// Description:           Contains the data class for link field functions
/// </summary>
namespace Affinion.LoyaltyBuild.Common.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Anchor tag Data class 
    /// </summary>
    public class AnchorTagHelperData
    {
        /// <summary>
        /// Gets or sets the link URL
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Gets or sets the alter tag
        /// </summary>
        public string AlterTag { get; set; }
    }
}
