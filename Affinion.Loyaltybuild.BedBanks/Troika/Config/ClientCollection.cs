﻿using System;
using System.Configuration;

namespace Affinion.LoyaltyBuild.BedBanks.Troika.Config
{
    public class ClientCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new Client();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((Client)element).Id;
        }

        protected override string ElementName
        {
            get
            {
                return "client";
            }
        }

        protected override bool IsElementName(string elementName)
        {
            return !String.IsNullOrEmpty(elementName) && elementName == "client";
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.BasicMap;
            }
        }

        public Client this[int index]
        {
            get
            {
                return base.BaseGet(index) as Client;
            }
        }

        public new Client this[string key]
        {
            get
            {
                return base.BaseGet(key) as Client;
            }
        }
    }
}