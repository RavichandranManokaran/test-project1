﻿CREATE PROCEDURE [dbo].[LB_GetBookingConfirmationDetails]( @Id int )
AS
BEGIN
	SELECT P.ProductId, P.Sku, L.OrderLineId,CR.CategoryId, L.OrderId, L.Sku AS Expr1, L.ProductName, L.Price, L.Quantity, L.CreatedOn, L.Discount, L.VAT, L.Total, L.VATRate, L.VariantSku, L.ShipmentId, L.UnitDiscount, 
           L.CreatedBy, L.PaidInFull, RR.CheckInDate, RR.CheckOutDate, RR.GuestName, RR.NoOfAdult AS AdultCount, RR.NoOfChild AS ChildCount, RR.SpecialRequest, OA.EmailAddress, OA.PhoneNumber, 
           OA.AddressName, OA.FirstName, OA.LastName, RR.OrderReference,RN.Note
	FROM   uCommerce_OrderLine AS L INNER JOIN
           uCommerce_Product AS P ON L.Sku = P.Sku INNER JOIN
           uCommerce_CategoryProductRelation AS CR ON P.ProductId = CR.ProductId INNER JOIN
           LB_RoomReservation AS RR ON L.OrderLineId = RR.OrderLineId LEFT OUTER JOIN
           LB_OrderAddress AS OA ON L.OrderId = OA.OrderId  LEFT OUTER JOIN
           LB_ReservationNotes AS RN ON L.OrderLineId = RN.OrderLineId
	WHERE  (L.OrderId = @Id)
END
GO