﻿CREATE TABLE [dbo].[uCommerce_ProductDefinitionField] (
    [ProductDefinitionFieldId] INT              IDENTITY (1, 1) NOT NULL,
    [DataTypeId]               INT              NOT NULL,
    [ProductDefinitionId]      INT              NOT NULL,
    [Name]                     NVARCHAR (512)   NOT NULL,
    [DisplayOnSite]            BIT              CONSTRAINT [uCommerce_DF_ProductDefinitionField_DisplayOnSite] DEFAULT ((0)) NOT NULL,
    [IsVariantProperty]        BIT              CONSTRAINT [uCommerce_DF_ProductDefinitionField_IsVariantProperty] DEFAULT ((0)) NOT NULL,
    [Multilingual]             BIT              NOT NULL,
    [RenderInEditor]           BIT              NOT NULL,
    [Searchable]               BIT              CONSTRAINT [uCommerce_DF_ProductDefinitionField_Searchable] DEFAULT ((0)) NOT NULL,
    [Deleted]                  BIT              CONSTRAINT [uCommerce_DF_ProductDefinitionField_Deleted] DEFAULT ((0)) NOT NULL,
    [SortOrder]                INT              CONSTRAINT [DF_uCommerce_ProductDefinitionField_SortOrder] DEFAULT ((0)) NOT NULL,
    [Facet]                    BIT              DEFAULT ((0)) NOT NULL,
    [Guid]                     UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    CONSTRAINT [uCommerce_PK_ProductDefinitionField] PRIMARY KEY CLUSTERED ([ProductDefinitionFieldId] ASC),
    CONSTRAINT [uCommerce_FK_ProductDefinitionField_DataType] FOREIGN KEY ([DataTypeId]) REFERENCES [dbo].[uCommerce_DataType] ([DataTypeId]),
    CONSTRAINT [uCommerce_FK_ProductDefinitionField_ProductDefinition] FOREIGN KEY ([ProductDefinitionId]) REFERENCES [dbo].[uCommerce_ProductDefinition] ([ProductDefinitionId])
);

