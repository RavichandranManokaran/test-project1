﻿CREATE TABLE [dbo].[LB_ClientTarget] (
    [ClientTargetId] INT             NOT NULL,
    [ClientGUID]     NVARCHAR (1000) NOT NULL,
    [CampaginItem]   INT             NOT NULL,
    CONSTRAINT [PK_uCommerce_Client] PRIMARY KEY CLUSTERED ([ClientTargetId] ASC),
    CONSTRAINT [FK_uCommerce_ClientTarget_uCommerce_Target] FOREIGN KEY ([ClientTargetId]) REFERENCES [dbo].[uCommerce_Target] ([TargetId])
);

