﻿CREATE PROCEDURE [dbo].[LB_RecentlySeen](@CustomerID int,@HotelID varchar(50))
as
declare
@HotelCount int
Begin
select @HotelCount=count(HotelID) from LB_RecentlySeenHotels where CustomerID=@CustomerID and HotelID=@HotelID
if(@HotelCount>0)
begin
delete from LB_RecentlySeenHotels where CustomerID=@CustomerID 
end
Insert into LB_RecentlySeenHotels values(@CustomerID,@HotelID,NULL,NULL, SYSDATETIME())
end

