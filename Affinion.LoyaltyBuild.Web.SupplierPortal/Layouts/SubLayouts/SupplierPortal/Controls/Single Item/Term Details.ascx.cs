﻿using Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration.SiteUI.Base;
using System;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Single_Item
{
    public partial class Term_Details : SitecoreUserControlBase
    {
        private void Page_Load(object sender, EventArgs e)
        {
            DefinitionLabel.Text = GetDictionaryText("Definition");
            UsageLabel.Text = GetDictionaryText("Usage");
        }
    }
}