/*jslint browser: true*/
/*global $, jQuery*/

/*
Custom ResponsiveMenu 
*/
// Anonymous Function Wrapper

(function () {
    "use strict";
    //Responsive Menu Handler
    //Read State 
    function pushMenu() {
        var menuRight, body;
        menuRight = $('#ty-res-nav');
        body = $("body");
        $(body).toggleClass("cbp-spmenu-push-toleft");
        $(menuRight).toggleClass("cbp-spmenu-open");
        // body...
    }
    //No Push Menu
    function noPushMenu() {
        var body = $("body");
        $("#ty-res-nav").removeClass("cbp-spmenu-open");
        $(body).removeClass("cbp-spmenu-push-toleft");
    }
    /*Enable Responsive Menu*/
    function enableResMenu() {

        $(".cbp-spmenu-push").css({
            "right": "240px"
        });
        $("#ty-res-nav").removeClass();
        $("#ty-res-nav").addClass("cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right ty-margin-top-navi");
        $(".ty-nav-back").removeClass("ty-nav-back");
        $("#nav2").removeClass("ty-navbar");
        $(".ty-noPadding-left-right").addClass("ty-5padding");
        $(".ty-social-icons").prependTo('#ty-topleft-links');
        //Menu Replacement
        $("#ty-nav-user").appendTo("#nav2"); // Login user
        $("#lang-globe").appendTo("#nav2"); // Language selector
        /*Global Menu Re-Structure Enable On Responsive */
        $(".ty-topleft-links-home").appendTo("#nav2");

        $(".ty-topleft-links-home").show();
        $("#lang-globe").addClass("ty-taylor-nav2 ty-nav-responsive");
        $("#ty-nav-user").removeClass();
        $("#ty-nav-user").addClass("nav navbar-nav ty-taylor-nav2 ty-nav-responsive");
        $("#ty-nav-user").show();
        //        init();

    }
    /*Disable Responsive Menu*/
    function disableResMenu() {

        $(".cbp-spmenu-push").css({
            "right": "0"
        });
        $("#nav2").addClass("ty-navbar");
        $("#ty-res-nav").removeClass();
        $("#ty-res-nav").addClass("navbar navbar-default");
        $(".ty-taylor-nav2").addClass("ty-nav-back");
        $("#ty-nav-user").appendTo("#ty-nav-Main");
        $("#ty-nav-user").removeClass();
        $(".ty-5padding").addClass("ty-noPadding-left-right");
        $(".ty-noPadding-left-right").removeClass("ty-5padding");
        $("#ty-nav-user").addClass("nav navbar-nav navbar-right");
        $("#lang-globe").appendTo(".lang-globe-holder-nav1");
        /*Global Menu Re-Structure Disable On Responsive */
        $(".ty-topleft-links-home").appendTo("#ty-topleft-links");

        $("#lang-globe").removeClass();
        $("#lang-globe").addClass('nav navbar-nav');
        $(".ty-social-icons").prependTo('.ty-social-icons-holder');
    }

    $(document).on('click', '*', function (evt) {
        var resNav = $('#ty-res-nav'),
            trigger = $('#ty-navtrigger');
        if ($(this).is(trigger)) {
            evt.stopPropagation(); // stop the event propagating to ancestral elements
            trigger.css("background", "#0093d0");
            pushMenu();
        } else if (!$(this).closest(resNav).length) {
            //<-- on click outside, hide menu   
            trigger.css("background", "#0093d0");
            noPushMenu();
        }
    });
    //Document Ready Event Handler for ResMenu
    $(document).ready(function () {
        var width, height;
        width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
        height = (window.innerHeight > 0) ? window.innerHeight : screen.height;
        if (width <= 767) {
            enableResMenu();
            $("#nav2").css("max-height", (height - 60));
        } else {
            disableResMenu();
        }
    });
    //On Resize CrosBrowser Handler
    $(window).on('resize', function () {
        noPushMenu();
        var width, height;
        width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
        height = (window.innerHeight > 0) ? window.innerHeight : screen.height;
        if (width <= 767) {
            enableResMenu();
            $("#nav2").css("max-height", (height - 60));
        } else {
            disableResMenu();
        }
    });
}());
//End of Wrapper Annon