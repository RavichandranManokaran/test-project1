﻿CREATE TABLE [dbo].[uCommerce_Customer](
	[CustomerId] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](512) NOT NULL,
	[LastName] [nvarchar](512) NOT NULL,
	[EmailAddress] [nvarchar](255) NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[MobilePhoneNumber] [nvarchar](50) NULL,
	[MemberId] [nvarchar](255) NULL,
	[LanguageName] [nvarchar](25) NULL,
 CONSTRAINT [uCommerce_PK_Customer] PRIMARY KEY CLUSTERED 
(
	[CustomerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


