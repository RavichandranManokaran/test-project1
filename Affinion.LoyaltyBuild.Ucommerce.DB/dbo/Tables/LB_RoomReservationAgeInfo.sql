﻿CREATE TABLE [dbo].[LB_RoomReservationAgeInfo] (
    [ReservationAgeId]  INT IDENTITY (1, 1) NOT NULL,
    [RoomReservationId] INT NULL,
    [OrderLineId]       INT NULL,
    [Age]               INT NULL,
    CONSTRAINT [PK_uCommerce_RoomReservationAgeInfo] PRIMARY KEY CLUSTERED ([ReservationAgeId] ASC)
);

