declare @definationid int = 0
select @definationid = DefinitionId from uCommerce_Definition
where Name = 'hotel'
declare @datatypeid int = 0
select @datatypeid = DataTypeId from uCommerce_DataType where DefinitionName= 'SiteCoreCountry'
--print @datatypeid
if(@datatypeid <> 0)
begin
insert into uCommerce_DefinitionField(DataTypeId,DefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,BuiltIn,DefaultValue,Guid)
values (@datatypeid , @definationid,'Country',1,0,1,0,0,0,0,'',NEWID())
end
select @datatypeid = DataTypeId from uCommerce_DataType where DefinitionName= 'SiteCoreRegion'
if(@datatypeid <> 0)
begin
insert into uCommerce_DefinitionField(DataTypeId,DefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,BuiltIn,DefaultValue,Guid)
values (@datatypeid , @definationid,'Region',1,0,1,0,0,0,0,'',NEWID())
end

select @datatypeid = DataTypeId from uCommerce_DataType where DefinitionName= 'SiteCoreClient'
if(@datatypeid <> 0)
begin
insert into uCommerce_DefinitionField(DataTypeId,DefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,BuiltIn,DefaultValue,Guid)
values (@datatypeid , @definationid,'Client',1,0,1,0,0,0,0,'',NEWID())
end

select @datatypeid = DataTypeId from uCommerce_DataType where DefinitionName= 'SiteCoreHotelFacility'
if(@datatypeid <> 0)
begin
insert into uCommerce_DefinitionField(DataTypeId,DefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,BuiltIn,DefaultValue,Guid)
values (@datatypeid , @definationid,'HotelFacility',1,0,1,0,0,0,0,'',NEWID())
end

select @datatypeid = DataTypeId from uCommerce_DataType where DefinitionName= 'SiteCoreHotelTheme'
if(@datatypeid <> 0)
begin
insert into uCommerce_DefinitionField(DataTypeId,DefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,BuiltIn,DefaultValue,Guid)
values (@datatypeid , @definationid,'Theme',1,0,1,0,0,0,0,'',NEWID())
end

select @datatypeid = DataTypeId from uCommerce_DataType where DefinitionName= 'SiteCoreGroups'
if(@datatypeid <> 0)
begin
insert into uCommerce_DefinitionField(DataTypeId,DefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,BuiltIn,DefaultValue,Guid)
values (@datatypeid , @definationid,'Groups',1,0,1,0,0,0,0,'',NEWID())
end

select @datatypeid = DataTypeId from uCommerce_DataType where DefinitionName= 'SiteCoreSuperGroups'
if(@datatypeid <> 0)
begin
insert into uCommerce_DefinitionField(DataTypeId,DefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,BuiltIn,DefaultValue,Guid)
values (@datatypeid , @definationid,'SuperGroups',1,0,1,0,0,0,0,'',NEWID())
end


insert into uCommerce_DefinitionField(DataTypeId,DefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,BuiltIn,DefaultValue,Guid)
values (1 , @definationid,'SiteCoreItemGuid',1,0,1,0,0,0,0,'',NEWID())



