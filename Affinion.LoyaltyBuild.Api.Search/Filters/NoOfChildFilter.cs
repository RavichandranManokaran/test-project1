﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Affinion.LoyaltyBuild.Api.Search.Data;
using Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi;
using Sitecore.Data;
using UCommerce.EntitiesV2;
using UCommerce.Runtime;
using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;

namespace Affinion.LoyaltyBuild.Api.Search.Filters
{
    public class NoOfChildFilter : ISearchFilter, ILoyaltyFilter
    {
        private readonly SearchKey _key;

        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="key"></param>
        public NoOfChildFilter(SearchKey key)
        {
            _key = key;
        }

        /// <summary>
        /// Build search criteria
        /// </summary>
        /// <param name="criteria"></param>
        /// 
        public void BuildSearch(ref UCom.MasterClass.ExtendedApi.SearchCriteria criteria)
        {          
            if (_key.NoOfChildren != null)
            {
                criteria.NumberOfChild = (int)_key.NoOfChildren;
            }
        }

        
    }
}
