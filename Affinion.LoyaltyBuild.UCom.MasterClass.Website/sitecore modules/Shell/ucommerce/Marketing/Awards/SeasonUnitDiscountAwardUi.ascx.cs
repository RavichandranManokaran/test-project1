﻿using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.EntitiesV2;
using UCommerce.Presentation.Views.Marketing;
using UCommerce.Presentation.Web;
using UCommerce.Presentation.Web.Controls;
using Affinion.LoyaltyBuild.UCom.Common.Extension;


namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.Ucommerce.Marketing.Awards
{
    public partial class SeasonUnitDiscountAwardUi : ViewEnabledControl<IEditCampaignItemView>, IConfigurable
    {


        protected void Page_Load(object sender, EventArgs e)
        {
            lblError.Text = string.Empty;
            SeasonUnitDiscountAward dataSource = (SeasonUnitDiscountAward)this.DataSource;
            BindData();
            this.DeleteButton.OnClientClick = string.Format("return confirm('{0}');", "Are you want to delete award");
        }

        #region Grid Event
        protected void EditSeasonUnitDiscountDetail(object sender, GridViewEditEventArgs e)
        {
            GrdSeasonUnitDiscount.EditIndex = e.NewEditIndex;
            ViewState["EditIndex"] = e.NewEditIndex;
            BindData();
        }

        protected void CancelEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GrdSeasonUnitDiscount.EditIndex = -1;
            ViewState["EditIndex"] = -1;
            BindData();
        }


        protected void AddNewAwardDetail(object sender, EventArgs e)
        {

            string startDate = ((TextBox)GrdSeasonUnitDiscount.FooterRow.FindControl("txtStartDate")).Text;
            string endDate = ((TextBox)GrdSeasonUnitDiscount.FooterRow.FindControl("txtEndDate")).Text;
            string amountOff = ((TextBox)GrdSeasonUnitDiscount.FooterRow.FindControl("txtAmountOff")).Text;
            bool chkPercentage = ((CheckBox)GrdSeasonUnitDiscount.FooterRow.FindControl("chkPercentage")).Checked;
            bool chkOnOrderLine = ((CheckBox)GrdSeasonUnitDiscount.FooterRow.FindControl("chkOnOrderLine")).Checked;
            bool isValid;
            string msg;
            ValidateGrid(startDate, endDate, amountOff, out isValid, out msg);
            if (isValid)
            {
                List<SeasonUnitDiscountAwardDetail> awrddetails = ((SeasonUnitDiscountAward)this.DataSource).Details.ToList();
                if (awrddetails != null && awrddetails.Any(x => ((startDate.ParseFormatDateTime() >= x.StartDate && startDate.ParseFormatDateTime() <= x.EndDate)
                    || (endDate.ParseFormatDateTime() >= x.StartDate && endDate.ParseFormatDateTime() <= x.EndDate)
                    )))
                {
                    lblError.Text = "Date Selection is invalid";
                }
                else
                {
                    SeasonUnitDiscountAwardDetail awardDetail = new SeasonUnitDiscountAwardDetail();
                    awardDetail.StartDate = startDate.ParseFormatDateTime();
                    awardDetail.EndDate = endDate.ParseFormatDateTime();
                    awardDetail.AmountOff = Convert.ToInt32(amountOff);
                    awardDetail.IsPercentage = chkPercentage;
                    awardDetail.OnOrderLine = chkOnOrderLine;
                    awardDetail.SeasonUnitDiscountAward = ((SeasonUnitDiscountAward)this.DataSource);
                    awardDetail.Save();
                    ((SeasonUnitDiscountAward)this.DataSource).Details.Add(awardDetail);
                    //((SeasonUnitDiscountAward)this.DataSource).Save();
                    BindData();
                }
            }
            else
            {
                lblError.Text = msg;
            }
        }


        protected void UpdateAwardDetail(object sender, GridViewUpdateEventArgs e)
        {

            string detailId = ((HiddenField)GrdSeasonUnitDiscount.Rows[e.RowIndex].FindControl("hdDetailId")).Value;
            string startDate = ((TextBox)GrdSeasonUnitDiscount.Rows[e.RowIndex].FindControl("txtStartDate")).Text;
            string endDate = ((TextBox)GrdSeasonUnitDiscount.Rows[e.RowIndex].FindControl("txtEndDate")).Text;
            string amountOff = ((TextBox)GrdSeasonUnitDiscount.Rows[e.RowIndex].FindControl("txtAmountOff")).Text;
            bool chkPercentage = ((CheckBox)GrdSeasonUnitDiscount.Rows[e.RowIndex].FindControl("chkPercentage")).Checked;
            bool chkOnOrderLine = ((CheckBox)GrdSeasonUnitDiscount.Rows[e.RowIndex].FindControl("chkOnOrderLine")).Checked;
            int awarddetailId = 0;
            bool isValid;
            string msg;
            ValidateGrid(startDate, endDate, amountOff, out isValid, out msg);
            if (isValid)
            {
                if (int.TryParse(detailId, out awarddetailId))
                {
                    List<SeasonUnitDiscountAwardDetail> awrddetails = ((SeasonUnitDiscountAward)this.DataSource).Details.ToList();
                    if (awrddetails != null && awrddetails.Any(x => x.SeasonUnitDiscountAwardDetailId != awarddetailId &&
                        ((startDate.ParseFormatDateTime() >= x.StartDate && startDate.ParseFormatDateTime() <= x.EndDate)
                        ||
                        (endDate.ParseFormatDateTime() >= x.StartDate && endDate.ParseFormatDateTime() <= x.EndDate)
                        )))
                    {
                        lblError.Text = "Date Selection is invalid";
                    }
                    else
                    {
                        SeasonUnitDiscountAwardDetail awardDetail = SeasonUnitDiscountAwardDetail.Get(awarddetailId);
                        awardDetail.StartDate = startDate.ParseFormatDateTime();
                        awardDetail.EndDate = endDate.ParseFormatDateTime();
                        awardDetail.AmountOff = Convert.ToInt32(amountOff);
                        awardDetail.IsPercentage = chkPercentage;
                        awardDetail.OnOrderLine = chkOnOrderLine;
                        awardDetail.SeasonUnitDiscountAward = ((SeasonUnitDiscountAward)this.DataSource);
                        awardDetail.Save();
                        GrdSeasonUnitDiscount.EditIndex = -1;
                        ViewState["EditIndex"] = -1;
                        BindData();
                    }
                }
            }
            else
            {
                lblError.Text = msg;
            }

        }

        private void ValidateGrid(string startDate, string endDate, string amountOff, out bool isValid, out string msg)
        {
            lblError.Text = string.Empty;
            isValid = true;
            msg = string.Empty;
            if (!startDate.ValidDate())
            {
                isValid = false;
                msg += "Start Date Invaid. ";
            }
            if (!endDate.ValidDate())
            {
                isValid = false;
                msg += "End Date Invaid. ";
            }
            if (string.IsNullOrEmpty(amountOff))
            {
                isValid = false;
                msg += "Price Required. ";
            }
            int amount;
            if (!int.TryParse(amountOff, out amount))
            {
                isValid = false;
                msg += "Price is invalid. ";
            }
            if (isValid)
            {
                DateTime dtStartDate = startDate.ParseFormatDateTime();
                DateTime dtEndDate = endDate.ParseFormatDateTime();
                if (dtStartDate.Date > dtEndDate.Date)
                {
                    isValid = false;
                    msg += "StartDate is less then or equal to end date";
                }
            }
        }

        private void BindData(int currentPageIndex = 0)
        {
            SeasonUnitDiscountAward dataSource = (SeasonUnitDiscountAward)this.DataSource;
            if (dataSource != null && dataSource.Details != null && dataSource.Details.Any())
            {
                //dataSource.Details = new List<SeasonFixPriceAwardDetail>();
                //dataSource.Details.Add(new SeasonFixPriceAwardDetail() { AmountOff = 0, StartDate = DateTime.Now, EndDate = DateTime.Now, SeasonFixPriceAwardDetailId = -1 });
                GrdSeasonUnitDiscount.DataSource = dataSource.Details;
                GrdSeasonUnitDiscount.PageIndex = currentPageIndex;
                GrdSeasonUnitDiscount.DataBind();
            }

            else
            {
                List<SeasonUnitDiscountAwardDetail> data = new List<SeasonUnitDiscountAwardDetail>();
                data.Add(new SeasonUnitDiscountAwardDetail() { AmountOff = 0, StartDate = DateTime.Now, EndDate = DateTime.Now, SeasonUnitDiscountAwardDetailId = -1 });
                GrdSeasonUnitDiscount.DataSource = data;
                GrdSeasonUnitDiscount.PageIndex = currentPageIndex;
                GrdSeasonUnitDiscount.DataBind();
            }

            /*  if (dataSource.Details.Count == 0)
              {
                  dataSource.Details = new List<SeasonUnitDiscountAwardDetail>();
                  dataSource.Details.Add(new SeasonUnitDiscountAwardDetail() { AmountOff = 0, IsPercentage = false, StartDate = DateTime.Now, EndDate = DateTime.Now, SeasonUnitDiscountAwardDetailId = -1 });
              }
              GrdSeasonUnitDiscount.DataSource = dataSource.Details;
              GrdSeasonUnitDiscount.PageIndex = currentPageIndex;
              GrdSeasonUnitDiscount.DataBind();*/


        }

        protected void ValidateDate(object sender, ServerValidateEventArgs e)
        {
            string controlName = ((BaseValidator)sender).ControlToValidate;
            var control = FindControl(controlName) as TextBox;
            try
            {
                e.Value.ParseFormatDateTime();
                e.IsValid = true;
            }
            catch
            {
                e.IsValid = false;
            }
        }


        protected void DeleteAwardDetail(object sender, EventArgs e)
        {
            LinkButton lnkRemove = (LinkButton)sender;
            int detailId = 0;
            if (int.TryParse(lnkRemove.CommandArgument, out detailId))
            {
                SeasonUnitDiscountAward dataSource = (SeasonUnitDiscountAward)this.DataSource;
                if (dataSource != null && dataSource.Details != null)
                {
                    SeasonUnitDiscountAwardDetail removeeDetail = dataSource.Details.FirstOrDefault(x => x.SeasonUnitDiscountAwardDetailId == detailId);
                    if (removeeDetail != null)
                    {
                        dataSource.Details.Remove(removeeDetail);
                        removeeDetail.Delete();
                        dataSource.Save();
                    }
                    BindData();
                }
            }
        }



        protected void OnPaging(object sender, GridViewPageEventArgs e)
        {
            BindData(e.NewPageIndex);
        }

        #endregion



        // Methods
        protected void DeleteButton_Click(object sender, ImageClickEventArgs e)
        {
            if (this.AwardId != 0)
            {
                (from x in UCommerce.EntitiesV2.Award.All()
                 where x.AwardId == this.AwardId
                 select x).SingleOrDefault<UCommerce.EntitiesV2.Award>().Delete();
            }
            this.Page.Response.Redirect(this.Page.Request.Url.ToString(), true);
        }

        public object New()
        {
            //SeasonUnitDiscountAward priceAward = new SeasonUnitDiscountAward();
            SeasonUnitDiscountAward priceAward = SeasonUnitDiscountAward.All().FirstOrDefault(x => x.CampaignItem.CampaignItemId == View.CampaignItem.CampaignItemId) ?? new SeasonUnitDiscountAward();
            priceAward.CampaignItem = base.View.CampaignItem;
            priceAward.Content = base.View.CampaignItem.Name;
            priceAward.Name = "Season Unit Price";
            this.DataSource = priceAward;
            ((SeasonUnitDiscountAward)this.DataSource).Save();
            return this.DataSource;
        }

        public int AwardId
        {
            get
            {
                if (this.DataSource == null)
                {
                    return 0;
                }
                return ((UCommerce.EntitiesV2.Award)this.DataSource).AwardId;
            }
        }

        public int CampaignItemId
        {
            get
            {
                return base.View.CampaignItem.CampaignItemId;
            }
        }

        public object DataSource { get; set; }

        protected void GrdSeasonUnitDiscount_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null && ((SeasonUnitDiscountAwardDetail)e.Row.DataItem).SeasonUnitDiscountAwardDetailId == -1)
            {
                e.Row.Visible = false;
            }
        }

    }



}