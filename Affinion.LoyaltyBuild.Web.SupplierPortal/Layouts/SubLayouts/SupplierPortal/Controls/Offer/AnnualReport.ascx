﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AnnualReport.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Offer.AnnualReport" %>

<div class="content_bg_inner_box alert-info">
    <div class="col-sm-12">
        <strong>Offer Name: <%= offerDetail.OfferName %></strong>
    </div>
    <div class="col-sm-12 padding-top12">
        <div class="row">
            <div class="col-sm-8">Bookings received over last 12 months by offer</div>
            <div class="col-sm-4"><%= offerDetail.BookingReceived %></div>
        </div>
        <div class="row">
            <div class="col-sm-8">Revenue generated over last 12 months by offer</div>
            <div class="col-sm-4"><%= (offerDetail.RevenueGenerated == 0 ? "0" : offerDetail.RevenueGenerated.ToString("0.##")) %> </div>
        </div>
        <div class="row">
            <div class="col-sm-8">Future bookings by offer</div>
            <div class="col-sm-4"><%= offerDetail.FutureBooking %></div>
        </div>
        <div class="row">
            <div class="col-sm-8">Future Revenue by offer</div>
            <div class="col-sm-4"><%= (offerDetail.FutureArrival == 0 ? "0" : offerDetail.FutureArrival.ToString("0.##")) %> </div>
        </div>
    </div>
</div>

