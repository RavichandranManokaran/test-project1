﻿TRUNCATE table uCommerce_DueTypes
GO

INSERT INTO [dbo].[uCommerce_DueTypes]
           ([Description]
           ,[IsActive]
           ,[CanBeDeleted]
           ,[ShowInList]
           ,[SubrateItemCode])
     VALUES
           ('Break Fee'
           ,1
           ,0
           ,0
           ,'')
INSERT INTO [dbo].[uCommerce_DueTypes]
           ([Description]
           ,[IsActive]
           ,[CanBeDeleted]
           ,[ShowInList]
           ,[SubrateItemCode])
     VALUES
           ('Commision on Break Fee'
           ,1
           ,0
           ,0
           ,'LBCOM')
INSERT INTO [dbo].[uCommerce_DueTypes]
           ([Description]
           ,[IsActive]
           ,[CanBeDeleted]
           ,[ShowInList]
           ,[SubrateItemCode])
     VALUES
           ('Credit Card Processing Fee'
           ,1
           ,1
           ,1
           ,'CCPROFEE')
INSERT INTO [dbo].[uCommerce_DueTypes]
           ([Description]
           ,[IsActive]
           ,[CanBeDeleted]
           ,[ShowInList]
           ,[SubrateItemCode])
     VALUES
           ('Cacellation Fee'
           ,1
           ,1
           ,1
           ,'CANFEE')
INSERT INTO [dbo].[uCommerce_DueTypes]
           ([Description]
           ,[IsActive]
           ,[CanBeDeleted]
           ,[ShowInList]
           ,[SubrateItemCode])
     VALUES
           ('Transfer Fee'
           ,1
           ,1
           ,1
           ,'TRAFEE')
INSERT INTO [dbo].[uCommerce_DueTypes]
           ([Description]
           ,[IsActive]
           ,[CanBeDeleted]
           ,[ShowInList]
           ,[SubrateItemCode])
     VALUES
           ('Refund'
           ,1
           ,1
           ,1
           ,'')