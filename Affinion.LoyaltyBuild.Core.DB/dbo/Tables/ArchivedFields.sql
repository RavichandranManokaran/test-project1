﻿CREATE TABLE [dbo].[ArchivedFields] (
    [RowId]      UNIQUEIDENTIFIER NOT NULL,
    [ArchivalId] UNIQUEIDENTIFIER NOT NULL,
    [VersionId]  UNIQUEIDENTIFIER NOT NULL,
    [FieldId]    UNIQUEIDENTIFIER NOT NULL,
    [Value]      NVARCHAR (MAX)   NOT NULL,
    [Created]    DATETIME         NOT NULL,
    [Updated]    DATETIME         NOT NULL,
    [DAC_Index]  INT              NULL
);


GO
CREATE CLUSTERED INDEX [DAC_Clustered]
    ON [dbo].[ArchivedFields]([DAC_Index] ASC);


GO
CREATE NONCLUSTERED INDEX [ndx_ArchivalId]
    ON [dbo].[ArchivedFields]([ArchivalId] ASC);

