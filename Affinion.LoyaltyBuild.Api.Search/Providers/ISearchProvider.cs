﻿using Affinion.LoyaltyBuild.Api.Search.Data;
using Affinion.LoyaltyBuild.Api.Search.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Api.Search.Providers
{
    /// <summary>
    /// Serach provider base interface
    /// </summary>
    public interface ISearchProvider
    {
        /// <summary>
        /// Search providers based on filter
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        ProviderCollection SearchProviders(IList<ISearchFilter> filters);

        /// <summary>
        /// Search providers based on filter asyncronusly
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        Task<ProviderCollection> SearchProvidersAsync(IList<ISearchFilter> filters);
    }
}
