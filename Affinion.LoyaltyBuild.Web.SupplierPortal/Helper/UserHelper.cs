﻿using Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration.SupplierPortal.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Helper
{
    public class UserHelper
    {
        /// <summary>
        /// Get logged in user role
        /// </summary>
        public static UserRole CurrentUserRole
        {
            get
            {
                switch ((SitecoreUserProfile.GetItem(UserProfileKeys.currentLoggedinUserRole)??string.Empty).ToLower())
                {
                    case Constants.hotelRole:
                        return UserRole.Supplier;
                    case Constants.hotelSupervisor:
                        return UserRole.Supplier;
                    case Constants.grpHotelRole:
                        return UserRole.SupplierGroupAdmin;
                    default:
                        return UserRole.LoyaltyAdmin;
                }
            }
        }
    }
}