﻿using Affinion.LoyaltyBuild.UCom.MasterClass.Website.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using UCommerce.EntitiesV2.Definitions;
using UCommerce.Presentation.Web.Controls;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.CustomSiteCoreDataType
{
    public class SiteCoreDropdownControlFactory : IControlFactory
    {
        private readonly List<string> supportedDataType = new List<string>() {
            CustomDataTypeConstant.Location,CustomDataTypeConstant.Country,CustomDataTypeConstant.SupplierType, CustomDataTypeConstant.Region,
            CustomDataTypeConstant.HotelRanking, //CustomDataTypeConstant.HotelTheme,  //CustomDataTypeConstant.HotelFacility,CustomDataTypeConstant.Client, 
            CustomDataTypeConstant.OccupancyTypes,CustomDataTypeConstant.Groups, CustomDataTypeConstant.SuperGroups,CustomDataTypeConstant.OfferType,
            CustomDataTypeConstant.PaymentMethod,
            CustomDataTypeConstant.AddOn,
            CustomDataTypeConstant.PackageType,
            CustomDataTypeConstant.Commission,
            CustomDataTypeConstant.Recommended,
            CustomDataTypeConstant.SingleClient,
            CustomDataTypeConstant.Currency
        };
        private string CurrentDataType { get; set; }


        public System.Web.UI.Control GetControl(UCommerce.EntitiesV2.Definitions.IProperty property)
        {
            var dropDownList = new SafeDropDownList();
            var locations = SiteConfiguration.GetSiteCoreItem(CurrentDataType);
            if (locations != null && locations.Any())
            {
                foreach (var location in locations.Where(l => !string.IsNullOrEmpty(l)))
                {
                    var listItem = new ListItem();
                    string data = property.GetValue() != null ? property.GetValue().ToString() : string.Empty; 
                    if (CurrentDataType == "SiteCoreCommission")
                    {
                        listItem.Value = location.Split('|')[1].Replace("{","").Replace("}","");
                        listItem.Text = location.Split('|')[0];
                        listItem.Selected = (data == listItem.Value);
                    }
                    else
                    {
                        listItem.Value = location.ToLower();
                        listItem.Text = location;
                        listItem.Selected = (data == location.ToLower().ToString());
                    }
                    //We'll mark this item as selected if the value of the property is equal to the price group id.
                   

                    dropDownList.Items.Add(listItem);
                }
                dropDownList.Items.Insert(0, new ListItem() { Text = string.Format("Select {0}", CurrentDataType), Value = "" });
            }
            return dropDownList;
        }

        public bool Supports(UCommerce.EntitiesV2.DataType dataType)
        {
            CurrentDataType = dataType.DefinitionName;
            return supportedDataType.Any(x => (x.Equals(dataType.DefinitionName)));
        }
    }
}