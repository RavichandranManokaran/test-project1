﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FreeNightAwardUi.ascx.cs" Inherits="Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.Ucommerce.Marketing.Awards.FreeNightAwardUi" %>
<table cellpadding="0" cellspacing="0" style="width: 100%;">
    <tr>
        <td>
            <asp:Localize ID="Localize1" runat="server" meta:resourcekey="FreeNight" />
            <asp:PlaceHolder runat="server" ID="NonEditModePlaceHolder">
                <asp:Label runat="server" ID="FreeNightLabel" />
            </asp:PlaceHolder>

            <asp:PlaceHolder runat="server" ID="EditModePlaceHolder" Visible="false">
                <asp:TextBox runat="server" ID="FreeNighText" /><br />
                <asp:RegularExpressionValidator ID="DecimalValidator" runat="server" ControlToValidate="FreeNighText"
                    Style="color: Red;" ValidationExpression="^\d+$" ValidationGroup="GrpFreeNighText"
                    Display="Dynamic" />
                <asp:RequiredFieldValidator ID="RequiredValidator" runat="server" ValidationGroup="GrpFreeNighText"
                    ControlToValidate="FreeNighText" Display="Dynamic" />
            </asp:PlaceHolder>
        </td>
        <td style="width: 50px; text-align: right; vertical-align: top;">
            <asp:ImageButton ID="EditButton" runat="server" ImageUrl="../../Images/ui/pencil.png" meta:resourcekey="Edit" OnClick="EditButton_Click" />
            <asp:ImageButton ID="SaveButton" runat="server" ImageUrl="../../Images/ui/save.gif" meta:resourcekey="Save" Visible="false" OnClick="SaveButton_Click" ValidationGroup="GrpFixPriceText" />
            <asp:ImageButton ID="DeleteButton" runat="server" ImageUrl="../../Images/ui/cross.png" meta:resourcekey="Delete" OnClick="DeleteButton_Click" /><br />
        </td>
    </tr>
</table>
