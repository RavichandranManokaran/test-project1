/*global window: false */
/*global jQuery: false */
/*global Rgen: false */
/*global console: false */
/*global $: false */

/**
 * This is the main module that will use througout the Rgen web site.
 *
 * @namespace Rgen
 * Version:0.1.3
 */
window.Rgen = window.Rgen || {};


(function (rgen, $) {
    "use strict";

    /**
     * This object will store basic configuration rpoperties for the site.
     *
     * @class Config
     * @static
     * @protected
     */
    rgen.Config = {
        /**
         * Using this property to false can ignore all Rgen.log() events to the console.
         *
         * @property debugging
         * @type Boolean
         * @default true
         */
        Debugging: true,
        /**
         * Using this attribute can set global animation speeds.
         *
         * @property {Object} Animation
         * @property {Number} Animation.Fast=200 This property defines the time interval for a fast animation
         * @property {Number} Animation.Medium=400 This property defines the time interval for a medium speed animation
         * @property {Number} Animation.Slow=800 This property defines the time interval for a slow animation
         */
        Animation: {
            Fast: 200,
            Medium: 400,
            Slow: 800
        },

        /**
         * Using this attribute can set global datePicker properties.
         *
         * @property {Object} DatePicker
         *
         */
        DatePicker: {
            format: 'dd/mm/yy',
            Image: '/ManageAvailability/datePicker/images/calendar.gif',
            Text: '<span class="glyphicon glyphicon-calendar"></span>',
            DefaultDate: new Date(1990, 0, 1)
        }

    };

    function private_Extend(nameSpace, plug) {
        if (rgen.hasOwnProperty(nameSpace)) {
            $.extend(rgen[nameSpace], plug);
        } else {
            if (rgen.Config.debuging) {
                throw "Namespace not found!";
            }
        }
    }

    /**
     * This object will store variables that should be defined as global variables.
     *
     * @class Global
     * @static
     */
    rgen.Global = {
        Add: function (valuePair) {
            try {
                private_Extend('Global', valuePair);
            } catch (error) {
                rgen.Util.log(error.name + '::' + error.message, 'error');
            }
        }
    };

    /**
     * This object will store variables that should be defined as global variables.
     *
     * @class Plugins
     * @static
     */
    rgen.Plugins = {
        Add: function (plugin) {
            try {
                if ((typeof plugin !== "function") && (typeof plugin !== "object")) {
                    throw new this.Exception.CustomException('New Plugin', 'Not an Object or Function');
                }

                private_Extend('Plugins', plugin);
            } catch (error) {
                rgen.Util.log(error.name + '::' + error.message, 'error');
            }
        }
    };

    /**
     * This object will hold utility methods that can be used across the web site.
     *
     * @class Util
     * @static
     */
    rgen.Util = {
        /**
         * This is the alternative method for console.log() which will log to console only if Rgen.Config.debugging is True.
         *
         * @method log
         *
         * @param {string} msg Message need to be log to the console
         * @param {string} [msgType] This is an optional parameter which will change the color of
         * the text that will log to the console
         * @param {string} msgType.warning Orange colored text will log to the console
         * @param {string} msgType.error Red colored text will log to the console
         *
         * @return {property} undefined
         */

        /*Check If the Console Is Available or Not*/
        hasConsole: function(){
            if (typeof console == "object") {
                return true;
            }
            else{
                return false;
            }
        },

        log: function (msg, msgType) {



            if (rgen.Config.Debugging) {
                if(Rgen.Util.hasConsole()){
                    if (msgType === undefined) {
                        if (this.browser.msie) {
                            console.log("Rgen::Message::" + msg);
                        } else {
                            console.log("%cRgen::Message::" + msg, "color:green");
                        }
                    } else if (msgType.toLowerCase() === "warning") {
                        if (this.browser.msie) {
                            console.log("Rgen::Warning::" + msg);
                        } else {
                            console.log("%cRgen::Warning::" + msg, "color:orange");
                        }
                    } else if (msgType.toLowerCase() === "error") {
                        if (this.browser.msie) {
                            console.log("Rgen::Error::" + msg);
                        } else {
                            console.log("%cRgen::Error::" + msg, "color:red");
                        }
                    }
                }

            }
        },
        /**
         * Using this method can identify the current browser. Identification is limited to IE, FF, Chrome, Safari.
         *
         * @method browser
         * @static
         *
         * @return {object} {msie:boolean, firefox:boolean, chrome:boolean, safari:boolean}
         */
        browser: (function () {
            var userAgent = window.navigator.userAgent;
            if (userAgent.search(/MSIE/img) > -1) {
                return {
                    msie: true
                };
            }
            if (userAgent.search(/Firefox/img) > -1) {
                return {
                    firefox: true
                };
            }
            if (userAgent.search(/Chrome/img) > -1) {
                return {
                    chrome: true
                };
            }
            if (userAgent.search(/Safari/img) > -1) {
                return {
                    safari: true
                };
            }
        }()),
        findKey: function (obj, key) {
            var k = {};
            for (k in obj) {
                if (obj.hasOwnProperty(k)) {
                    if (k === key) {
                        return obj[key];
                    }
                }
            }
        },
        clearForm: function (elem) {
            var jQobj;
            if (elem instanceof jQuery) {
                jQobj = $(elem);
            } else if (elem.indexOf('#') > -1 || elem.indexOf('.') > -1) {
                jQobj = $(elem);
            } else if (typeof elem === 'string' || elem instanceof String) {
                jQobj = $("#" + elem);
            }
            jQobj.find('input:text, input:password, input:file, textarea').val('');
            jQobj.find('select option:selected').removeAttr('selected');
            jQobj.find('input:checkbox, input:radio').removeAttr('checked');

        }

    };

    /**
     * This object will store variables that should be defined as global variables.
     *
     * @class Exception
     * @static
     */
    rgen.Exception = {
        CustomException: function (n, m) {
            this.name = n;
            this.message = m;
        }
    };
}(window.Rgen, jQuery));