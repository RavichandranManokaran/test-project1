﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    public class FinanceCode
    {
        public string ClientId { get; set; }
        public string ProviderId { get; set; }
        public int OrderLineId { get; set; }
        public string Company { get; set; }
        public long MerchantIdentificationNo { get; set; }
        public string ClientName { get; set; }
        public string Department { get; set; }
        public string ProductCode { get; set; }
        public string GlCode { get; set; }
        public string ClientCode { get; set; }
        public string Description { get; set; }
        public string Currency { get; set; }
        public int OrderId { get; set; }
    }
}
