﻿CREATE TABLE [dbo].[uCommerce_Address] (
    [AddressId]         INT            IDENTITY (1, 1) NOT NULL,
    [Line1]             NVARCHAR (512) NOT NULL,
    [Line2]             NVARCHAR (512) NULL,
    [PostalCode]        NVARCHAR (50)  NOT NULL,
    [City]              NVARCHAR (512) NOT NULL,
    [State]             NVARCHAR (512) NULL,
    [CountryId]         INT            NOT NULL,
    [Attention]         NVARCHAR (512) NULL,
    [CustomerId]        INT            NOT NULL,
    [CompanyName]       NVARCHAR (512) NULL,
    [AddressName]       NVARCHAR (512) NOT NULL,
    [FirstName]         NVARCHAR (512) NULL,
    [LastName]          NVARCHAR (512) NULL,
    [EmailAddress]      NVARCHAR (255) NULL,
    [PhoneNumber]       NVARCHAR (50)  NULL,
    [MobilePhoneNumber] NVARCHAR (50)  NULL,
    CONSTRAINT [uCommerce_PK_Address] PRIMARY KEY CLUSTERED ([AddressId] ASC),
    CONSTRAINT [uCommerce_FK_Address_Country] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[uCommerce_Country] ([CountryId]),
    CONSTRAINT [uCommerce_FK_Address_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[uCommerce_Customer] ([CustomerId])
);

