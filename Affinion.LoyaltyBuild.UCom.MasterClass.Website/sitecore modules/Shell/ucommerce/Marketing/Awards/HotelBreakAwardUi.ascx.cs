﻿using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.EntitiesV2;
using UCommerce.Presentation.Views.Marketing;
using UCommerce.Presentation.Web;
using UCommerce.Presentation.Web.Controls;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.Ucommerce.Marketing.Awards
{
    public partial class HotelBreakAwardUi : ViewEnabledControl<IEditCampaignItemView>, IConfigurable
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HotelBreakAward dataSource = (HotelBreakAward)this.DataSource;
            this.HotelBreakupLabel.Text = string.Format("Discount On Hotel Break : {0}", dataSource.DiscountAmount.ToString() + " " + (dataSource.IsPercentage ? "Percentage" : "Unit"));
            this.DiscountAmount.Text = dataSource.DiscountAmount.ToString();
            chkIsPercentage.Checked = dataSource.IsPercentage;
            this.DecimalValidator.ErrorMessage = string.Format("{0}", "Please Enter Only Number");
            this.RequiredValidator.ErrorMessage = string.Format("{0}", "Please Enter Only Number");
            this.DeleteButton.OnClientClick = string.Format("return confirm('{0}');", "Are you want to delete award");
        }

        // Methods
        protected void DeleteButton_Click(object sender, ImageClickEventArgs e)
        {
            if (this.AwardId != 0)
            {
                (from x in UCommerce.EntitiesV2.Award.All()
                 where x.AwardId == this.AwardId
                 select x).SingleOrDefault<UCommerce.EntitiesV2.Award>().Delete();
            }
            this.Page.Response.Redirect(this.Page.Request.Url.ToString(), true);
        }

        protected void EditButton_Click(object sender, ImageClickEventArgs e)
        {
            this.ToggleEditor();
        }

        public object New()
        {
            this.DataSource = new HotelBreakAward();
            ((HotelBreakAward)this.DataSource).CampaignItem = base.View.CampaignItem;
            if (((HotelBreakAward)this.DataSource).DiscountAmount < 1M)
            {
                ((HotelBreakAward)this.DataSource).DiscountAmount = 0M;
            }
            if (string.IsNullOrEmpty(((HotelBreakAward)this.DataSource).Name))
            {
                ((HotelBreakAward)this.DataSource).Name = "Discount On Hotel Break";
            }
            ((HotelBreakAward)this.DataSource).DiscountAmount = 0;
            ((HotelBreakAward)this.DataSource).OnOrderLine = true;
            ((HotelBreakAward)this.DataSource).IsPercentage = false;

            ((HotelBreakAward)this.DataSource).Save();
            return this.DataSource;
        }

        protected void SaveButton_Click(object sender, ImageClickEventArgs e)
        {
            HotelBreakAward award = (this.DataSource == null) ? new HotelBreakAward() : ((HotelBreakAward)this.DataSource);
            CampaignItem campaignItem = base.View.CampaignItem;
            award.DiscountAmount = decimal.Parse(this.DiscountAmount.Text);
            award.OnOrderLine = true;
            award.IsPercentage = chkIsPercentage.Checked;
            award.CampaignItem = campaignItem;
            award.Save();
            this.HotelBreakupLabel.Text = string.Format("Discount On Hotel Break : {0}", award.DiscountAmount.ToString() + " " + (award.IsPercentage ? "Percentage" : "Unit"));
            this.ToggleEditor();
        }

        private void ToggleEditor()
        {
            this.NonEditModePlaceHolder.Visible = !this.NonEditModePlaceHolder.Visible;
            this.EditButton.Visible = !this.EditButton.Visible;
            this.EditModePlaceHolder.Visible = !this.EditModePlaceHolder.Visible;
            this.SaveButton.Visible = !this.SaveButton.Visible;
        }

        // Properties
        public int AwardId
        {
            get
            {
                if (this.DataSource == null)
                {
                    return 0;
                }
                return ((UCommerce.EntitiesV2.Award)this.DataSource).AwardId;
            }
        }

        public int CampaignItemId
        {
            get
            {
                return base.View.CampaignItem.CampaignItemId;
            }
        }

        public object DataSource { get; set; }

    }

}