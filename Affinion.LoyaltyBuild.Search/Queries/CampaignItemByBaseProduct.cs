﻿using Affinion.LoyaltyBuild.Search.Data;
using NHibernate;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.EntitiesV2.Queries;

namespace Affinion.LoyaltyBuild.Search.Queries
{
    class CampaignItemByBaseProduct : ICannedQuery<CampaignItem>
    {
        private readonly OfferDataItem baseProduct;
        private readonly SearchKey searchTerm;

        public CampaignItemByBaseProduct(OfferDataItem baseProductItem, SearchKey searchTermItem)
        {
            this.baseProduct = baseProductItem;
            this.searchTerm = searchTermItem;
        }

        public IEnumerable<CampaignItem> Execute(ISession session)
        {
            OccupancyType occupancyType = this.baseProduct.OccupancyType;
            var campaigns = session.Query<Campaign>().Where(x => (x.StartsOn <= this.searchTerm.CheckinDate) && (x.EndsOn >= this.searchTerm.CheckoutDate)).Fetch(x => x.CampaignItems).ToList();
            List<CampaignItem> campaginItemList = new List<CampaignItem>();
            foreach (var item in campaigns)
            {
                campaginItemList.AddRange(item.CampaignItems.Where(x => IncludeCampaignItem(x)));
            }

            return campaginItemList;
        }

        private bool IncludeCampaignItem(CampaignItem item)
        {
            bool include = item.Enabled;
            foreach (var property in item.CampaignItemProperties)
            {
                string val = property.Value;
            }

            return include;
        }
    }
}
