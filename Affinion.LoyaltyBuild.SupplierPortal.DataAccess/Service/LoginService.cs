﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
using Affinion.LoyaltyBuild.SupplierPortal.DataAccess.Data;
using System.Data;
using Affinion.LoyaltyBuild.Common.Instrumentation;

namespace Affinion.LoyaltyBuild.SupplierPortal.DataAccess.Service
{
    /// <summary>
    /// Login Service
    /// </summary>
    public class LoginService:ILoginService
    {
        /// <summary>
        /// insert chage password history
        /// </summary>
        /// <returns></returns>
        public void InsertChangePasswordHistory(string uniqueid, string username)
        {
            uCommerceConnectionFactory.AddUniqueId(uniqueid, username);
        }

        /// <summary>
        /// select chage password history
        /// </summary>
        /// <returns>username</returns>
        public string SelectChangePasswordHistory(string uniqueid)
        {
            try
            {
                DataSet ds = uCommerceConnectionFactory.SelectChangePasswordHistory(uniqueid);
                if (ds != null && ds.Tables[0].Rows.Count>0)
                {
                    string username = !string.IsNullOrWhiteSpace(ds.Tables[0].Rows[0][0].ToString()) ? ds.Tables[0].Rows[0][0].ToString() : string.Empty;
                    return username;
                }
                return string.Empty;

            }
            catch(Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.SupplierPortal, ex, uniqueid);
                throw;

            }
        }

        /// <summary>
        /// delete chage password history
        /// </summary>
        /// <returns></returns>
        public void DeleteChangePasswordHistory(string username)
        {
            uCommerceConnectionFactory.DeleteChangePasswordHistory(username);
        }


        /// <summary>
        /// Generate random numbers
        /// </summary>
        /// <param name="r">Random number</param>
        public string GenerateRandomNumbers(Random r)
        {
            int maxdigits = 10; // Change to needed # of digits
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < maxdigits; i++)
            {
                result.Append(r.Next(10)); // Append a number from 0 to 9
            }
            string key = result.ToString();
            return key;
        }
    }
}
