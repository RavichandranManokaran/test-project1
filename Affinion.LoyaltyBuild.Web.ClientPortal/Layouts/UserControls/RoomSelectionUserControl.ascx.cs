﻿#region Using Directives

using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.Search.Entities;
using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Data.Items;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Affinion.LoyaltyBuild.Search.Data;

#endregion
namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.UserControls
{
    public partial class RoomSelectionUserControl : System.Web.UI.UserControl
    {
        private string roomId = string.Empty;

        #region Properties
        public Item DataSource { get; set; }

        /// <summary>
        /// A zero based room identification number
        /// </summary>
        public string RoomId
        {
            get
            {
                return roomId;
            }
            set
            {
                roomId = value;

                RoomNumber.InnerText = string.Format("{0} {1}", SitecoreFieldsHelper.GetValue(DataSource, "RoomLabel"), roomId);
            }
        }
        public string AdultsCount
        {
            get
            {
                return NoOfAdults.Text;
            }

            set
            {
                NoOfAdults.Text = value;
            }
        }

        public string ChildrenCount
        {
            get
            {
                return NoOfChildrenDropDownList.SelectedValue;
            }

            set
            {
                NoOfChildrenDropDownList.SelectedValue = value;
            }
        }

        public int NoOfChildren
        {
            get
            {
                int count;

                if (int.TryParse(ChildrenCount, out count))
                {
                    return count;
                }
                else
                {
                    throw new InvalidCastException("ChildrenCount: " + ChildrenCount);
                }
            }


        }

        public ICollection<string> ChildrenAges
        {
            get
            {
                ICollection<string> childAges = new List<string>();

                for (int i = 0; i < NoOfChildren; i++)
                {
                    DropDownList dropDownList = ChildAgePanel.FindControl(string.Format("AgeDropdownList{0}", i + 1)) as DropDownList;

                    childAges.Add(dropDownList.SelectedValue);
                    //childAgesParameters = string.Format("{0}{1}{2}", childAgesParameters, "&ChildAge=", dropDownList.SelectedValue);
                }

                return childAges;
            }
        }

        public ICollection<ChildBedRequest> ChildBeds
        {
            get
            {
                ICollection<ChildBedRequest> childbedRequests = new List<ChildBedRequest>();

                for (int i = 0; i < NoOfChildren; i++)
                {
                    DropDownList dropDownList = ChildAgePanel.FindControl(string.Format("AgeDropdownList{0}", i + 1)) as DropDownList;

                    DropDownList childBedDropDownList = ChildAgePanel.FindControl(string.Format("BedTypeDropDown{0}", i + 1)) as DropDownList;

                    ChildBedRequest childBed = new ChildBedRequest();

                    childBed.Age = dropDownList.SelectedValue;
                    childBed.IsExtraBed = childBedDropDownList.SelectedIndex == 0 ? false : true;
                    childbedRequests.Add(childBed);
                    //childAgesParameters = string.Format("{0}{1}{2}", childAgesParameters, "&ChildAge=", dropDownList.SelectedValue);
                }

                return childbedRequests;
            }
        }


        #endregion

        #region Protected Methods

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            CreateChildAgeDropdownLists();

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Uri requestUrl = Request.Url;

            string requestQuery = requestUrl.Query;

            BindData(DataSource);

            if (!this.IsPostBack && !string.IsNullOrEmpty(requestQuery))
            {
                //Bind data to the text boxes in the result page
                PrefillControls(requestUrl);
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// prefill the textboxes in the results page according to the values entered to perform search.
        /// </summary>
        /// <param name="requestUrl"></param>
        private void PrefillControls(Uri requestUrl)
        {

            QueryStringHelper helper = new QueryStringHelper(requestUrl);

            this.NoOfAdults.SelectedValue = helper.GetValue(string.Format("Room{0}AdultsCount", RoomId));
            this.NoOfChildrenDropDownList.SelectedValue = helper.GetValue(string.Format("Room{0}ChildrenCount", RoomId));

        }

        private void CreateChildAgeDropdownLists()
        {
            Item item = DataSource;
            int maxChildren = SitecoreFieldsHelper.GetInteger(item, "MaxChildren", 3);
            int maxChildAge = SitecoreFieldsHelper.GetInteger(item, "MaxChildAge", 13);

            IList<string> childAgesCollection = new List<string>();

            Uri requestUrl = Request.Url;
            SearchKey searchKey = new SearchKey(requestUrl, null);

            if (!IsPostBack)
            {

                childAgesCollection = GetChildAgesCollection(childAgesCollection, requestUrl);
            }

            for (int i = 0; i < maxChildren; i++)
            {

                HtmlGenericControl innerDiv = new HtmlGenericControl("div");

                ChildBedRequest childBed = null;

                if (searchKey != null && searchKey.SelectedRooms != null)
                {
                    RoomRequest selectedRoom = searchKey.SelectedRooms.ElementAtOrDefault(int.Parse(roomId) - 1);
                    if (selectedRoom != null)
                    {
                        childBed = selectedRoom.ChildrenBeds.ElementAtOrDefault(i);
                    }
                }

                CreateDropDownList(string.Format("AgeDropdownList{0}", (i + 1)), innerDiv, maxChildAge, childBed);

                innerDiv.Attributes.Add("role", "group");

                HtmlGenericControl outerDiv = CreateOuterDiv(i);

                outerDiv.Controls.Add(innerDiv);

                ChildAgePanel.Controls.Add(outerDiv);

                HtmlGenericControl outerDivBedType = CreateBedTypePanel(item, i, childBed);

                ChildAgePanel.Controls.Add(outerDivBedType);

            }

        }

        private static HtmlGenericControl CreateOuterDiv(int controlId)
        {
            HtmlGenericControl outerDiv = new HtmlGenericControl("div");

            HtmlGenericControl titleSpan = new HtmlGenericControl("h5");
            titleSpan.InnerText = string.Format("Child {0} age", (controlId + 1));

            ///Add child age title span
            outerDiv.Controls.Add(titleSpan);

            outerDiv.Attributes.Add("class", "childBedAge col-xs-12 col-md-6 roomOn");
            outerDiv.Attributes.Add("id", String.Format("AgeDropdownDiv{0}", (controlId + 1)));
            return outerDiv;
        }

        private static IList<string> GetChildAgesCollection(IList<string> childAgesCollection, Uri requestUrl)
        {
            QueryStringHelper helper = new QueryStringHelper(requestUrl);

            string childAgesParameter = helper.GetValue("ChildAge");

            if (childAgesParameter != null)
            {
                childAgesCollection = childAgesParameter.Split(',');
            }
            return childAgesCollection;
        }

        private static HtmlGenericControl CreateBedTypePanel(Item item, int dropDownId, ChildBedRequest childBed)
        {
            HtmlGenericControl outerDivBedType = new HtmlGenericControl("div");
            HtmlGenericControl innerDivBedType = new HtmlGenericControl("div");

            HtmlGenericControl bedTypeTitleSpan = new HtmlGenericControl("h5");
            bedTypeTitleSpan.InnerText = "Bed Type";

            outerDivBedType.Attributes.Add("class", "roomOn childBedType col-xs-12 col-md-6");
            outerDivBedType.Attributes.Add("id", String.Format("BedTypeDropDownDiv{0}", (dropDownId + 1)));
            outerDivBedType.Controls.Add(bedTypeTitleSpan);
            outerDivBedType.Controls.Add(innerDivBedType);
            CreateBedTypeDropDownList(string.Format("BedTypeDropDown{0}", (dropDownId + 1)), innerDivBedType, item, childBed);
            return outerDivBedType;
        }

        private static void CreateBedTypeDropDownList(string bedTypeDropdownId, Control parent, Item item, ChildBedRequest childBed)
        {
            List<ListItem> items = new List<ListItem>();

            string adultsBed = SitecoreFieldsHelper.GetValue(item, "InAdultsBedText");
            string extraBed = SitecoreFieldsHelper.GetValue(item, "ExtraBedText");

            items.Add(new ListItem(adultsBed, adultsBed));
            items.Add(new ListItem(extraBed, extraBed));

            //items.Sort(delegate(ListItem item1, ListItem item2) { return item1.Text.CompareTo(item2.Text); });

            DropDownList dropDownList = new DropDownList();
            dropDownList.ID = bedTypeDropdownId;
            dropDownList.ClientIDMode = ClientIDMode.Static;

            dropDownList.Attributes.Add("class", "roomOn form-control childBedTypeDropdown");
            dropDownList.Items.AddRange(items.ToArray());

            dropDownList.DataBind();
            dropDownList.SelectedValue = adultsBed;
            if (childBed != null)
            {
                dropDownList.SelectedValue = childBed.IsExtraBed ? extraBed : adultsBed;
            }

            parent.Controls.Add(dropDownList);

        }

        //private static void CreateDropDownList(string ageDropdownId, Control parent, int maxChildAge, string selectedValue)
        private static void CreateDropDownList(string ageDropdownId, Control parent, int maxChildAge, ChildBedRequest childBed)
        {
            if (parent == null)
            {
                throw new ArgumentNullException("parent");
            }

            ArrayList listItems = new ArrayList();

            ///Add default option
            //listItems.Add("Select");
            listItems.Add(LanguageReader.GetText(Constants.DefaultDropDownTextDictionaryKey, "Select"));


            ///Add zero to twelve months option
            //listItems.Add(Constants.ZeroToTwelveMonths);
            listItems.Add(LanguageReader.GetText(Constants.ZeroToTwelveMonthsDictionaryKey, "0"));


            for (int i = 0; i < maxChildAge; i++)
            {
                listItems.Add(i + 1);

            }

            DropDownList dropDownList = new DropDownList();
            dropDownList.ID = ageDropdownId;
            dropDownList.ClientIDMode = ClientIDMode.Static;

            dropDownList.Attributes.Add("class", "roomOn form-control childBedAgeDropdown");
            dropDownList.DataSource = listItems;
            dropDownList.DataBind();

            if (childBed != null)
            {
                dropDownList.SelectedValue = childBed.Age;
            }


            parent.Controls.Add(dropDownList);
        }

        /// <summary>
        /// Binds data to controls
        /// </summary>
        private void BindData(Item item)
        {
            if (item != null)
            {

                this.AdultsLabel.Item =
                this.ChildrenLabel.Item = item;


                // read sitecore values
                string destinationText = SitecoreFieldsHelper.GetValue(item, "DestinationLabel");


                this.BindDropDowns(item);
            }
        }

        /// <summary>
        /// Bind data to drop downs
        /// </summary>
        /// <param name="item">The item to read data</param>
        private void BindDropDowns(Item item)
        {
            // read sitecore values
            int maxRooms = SitecoreFieldsHelper.GetInteger(item, "MaxRooms", 5);
            int maxAdults = SitecoreFieldsHelper.GetInteger(item, "MaxAdults", 8);
            int maxChildren = SitecoreFieldsHelper.GetInteger(item, "MaxChildren", 7);
            string showAllText = SitecoreFieldsHelper.GetValue(item, "ShowAllLabel");



            string selectedNoOfAdults = NoOfAdults.SelectedValue;

            this.NoOfAdults.DataSource = Enumerable.Range(1, maxAdults);
            this.NoOfAdults.DataBind();
            NoOfAdults.SelectedValue = selectedNoOfAdults;

            string selectedNoOfChildren = NoOfChildrenDropDownList.SelectedValue;

            this.NoOfChildrenDropDownList.DataSource = Enumerable.Range(0, maxChildren + 1);
            this.NoOfChildrenDropDownList.DataBind();
            NoOfChildrenDropDownList.SelectedValue = selectedNoOfChildren;


        }

        #endregion

    }
}