﻿using System;
using System.Xml.Serialization;

namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Model
{
    [Serializable]
    [XmlType(TypeName = "Facility")]
    public class JtFacility
    {
        public string FacilityType { get; set; }
        public int FacilityID { get; set; }
        public string Facility { get; set; }
        public string Notes { get; set; }
        public string CostInformation { get; set; }
    }
}
