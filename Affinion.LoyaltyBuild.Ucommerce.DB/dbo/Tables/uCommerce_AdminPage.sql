﻿CREATE TABLE [dbo].[uCommerce_AdminPage] (
    [AdminPageId] INT            IDENTITY (1, 1) NOT NULL,
    [FullName]    NVARCHAR (256) NOT NULL,
    [ActiveTab]   NVARCHAR (256) CONSTRAINT [uCommerce_AdminPage_ActiveTab] DEFAULT ('') NOT NULL,
    CONSTRAINT [uCommerce_PK_AdminPage] PRIMARY KEY CLUSTERED ([AdminPageId] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_AdminPage]
    ON [dbo].[uCommerce_AdminPage]([FullName] ASC);

