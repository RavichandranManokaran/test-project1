﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Maps
{
    public class SeasonFixPriceAwardDetailMap : ClassMap<SeasonFixPriceAwardDetail>
    {
        // Methods
        public SeasonFixPriceAwardDetailMap()
        {
            base.Table("uCommerce_SeasonFixPriceAwardDetail");
            base.Id(x => x.SeasonFixPriceAwardDetailId, "SeasonFixPriceAwardDetailId").GeneratedBy.Identity();
            base.Map(x => x.AmountOff).Not.Nullable();
            base.Map(x => x.StartDate).Not.Nullable();
            base.Map(x => x.EndDate).Not.Nullable();
            base.References<SeasonFixPriceAward>(x => x.SeasonFixPriceAward).Column("SeasonFixPriceAwardId");// .Cascade.SaveUpdate();
        }
    }
}