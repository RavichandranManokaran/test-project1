﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SupplierSearchResults.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Search
/// Description:           Categorize and stores search results
/// </summary>
using System.Collections.Generic;
namespace Affinion.LoyaltyBuild.Search.Data
{

    /// <summary>
    /// Categorize and stores search results
    /// </summary>
    public class HotelSupplierResults
    {
        public HotelSearchResultItem[] AllResults { get; set; }

        public HotelSearchResultItem[] ResultsBySupplierName { get; set; }

        public HotelSearchResultItem[] ResultsByLocation { get; set; }
        
    }
}
