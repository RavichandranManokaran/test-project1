﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.SupplierPortal.Reports.Data
{
    /// <summary>
    /// Contains details for the items of revenue by offer report item
    /// </summary>
    public class RevenueByOfferListRevenue:RevenueBase
    {
        /// <summary>
        /// Provider Name
        /// </summary>
        public string ProviderName { get; set; }

        /// <summary>
        /// Campaign Name
        /// </summary>

        public string CampaignName { get; set; }
    }

    /// <summary>
    /// Collection to manage Revenue By Offer Report
    /// </summary>

    public class RevenueByOfferListRevenueCollection : IEnumerable<RevenueByOfferListRevenue>
    {
        //Private variables declaration
        private List<RevenueByOfferListRevenue> _items;

        /// <summary>
        /// Constructor
        /// </summary>
        public RevenueByOfferListRevenueCollection()
        {
            _items = new List<RevenueByOfferListRevenue>();
        }

        /// <summary>
        /// Add method to add item
        /// </summary>
        /// <param name="item"></param>
        public void Add(RevenueByOfferListRevenue item)
        {
            _items.Add(item);
        }

        /// <summary>
        /// Override method IEnumenrable class
        /// </summary>
        /// <returns></returns>
        public IEnumerator<RevenueByOfferListRevenue> GetEnumerator()
        {
            foreach (var item in _items)
                yield return item;
        }

        /// <summary>
        /// Override method IEnumenrable class
        /// </summary>
        /// <returns></returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            foreach (var item in _items)
                yield return item;
        }
    }
}
