﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Collections.Generic;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Model;

namespace Affinion.LoyaltyBuild.BedBanks.JacTravel
{
    public static class JtDbCreator
    {
        private const string DbName = "JacTravel";

        public static string CreateDatabase(string connStr)
        {
            string output = "";

            using (SqlConnection myConn = new SqlConnection(connStr))
            {
                output += KillDbConnections(myConn);
                output += "<br/>";

                string str = "IF EXISTS(SELECT * FROM sys.databases WHERE name='" + DbName + "')" +
                                   "DROP DATABASE " + DbName + " " +
                                   "CREATE DATABASE " + DbName + " ON PRIMARY " +
                                   "(NAME = " + DbName + "_Data, " +
                                   "FILENAME = 'C:\\Temp\\" + DbName + "Data.mdf', " +
                                   "SIZE = 5MB, MAXSIZE = 10MB, FILEGROWTH = 10%) " +
                                   "LOG ON (NAME = " + DbName + "_Log, " +
                                   "FILENAME = 'C:\\Temp\\" + DbName + "Log.ldf', " +
                                   "SIZE = 1MB, " +
                                   "MAXSIZE = 5MB, " +
                                   "FILEGROWTH = 10%)";

                SqlCommand myCommand = new SqlCommand(str, myConn);
                try
                {
                    myConn.Open();
                    myCommand.ExecuteNonQuery();
                    return output + "Database is Created Successfully";
                }
                catch (Exception ex)
                {
                    return output + ex;
                }
            }
        }

        public static string CreateLocationTable(string connStr)
        {
            using (SqlConnection myConn = new SqlConnection(connStr))
            {
                const string str = "USE " + DbName + " " +
                               "CREATE TABLE [dbo].[jtLocation]( " +
                                    "[Country] [varchar](250) NOT NULL, " +
                                    "[RegionID] [int] NOT NULL, " +
                                    "[Region] [varchar](250) NOT NULL, " +
                                    "[ResortID] [int] NOT NULL, " +
                                    "[Resort] [varchar](250) NOT NULL " +
                               ") ON [PRIMARY] ";

                SqlCommand myCommand = new SqlCommand(str, myConn);
                try
                {
                    myConn.Open();
                    myCommand.ExecuteNonQuery();
                    return "Location Table is Created Successfully";
                }
                catch (Exception ex)
                {
                    return ex.ToString();
                }
            }
        }

        public static string ImportLocationData(string connStr)
        {
            string output = "";

            using (SqlConnection myConn = new SqlConnection(connStr))
            {
                string sql = "USE JacTravel ";
            
                // Open the text file using a stream reader.
                using (StreamReader sr = new StreamReader(HttpContext.Current.Server.MapPath("~/Resources/Location.txt")))
                {
                    int counter = 0;
                    string line;

                    // Read the file and display it line by line.
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (counter != 0)
                        {
                            // Read the stream to a string, and write the string to the console.
                            string[] singleLocation = line.Split('|');

                            JtLocation loc = new JtLocation
                            {
                                Country = singleLocation[0],
                                RegionId = Int32.Parse(singleLocation[1]),
                                Region = singleLocation[2],
                                ResortId = Int32.Parse(singleLocation[3]),
                                Resort = singleLocation[4]
                            };

                            //Now do the insert to the db
                            sql += "INSERT Into jtLocation VALUES ('" + loc.Country.Replace("'", "''") + "', " + loc.RegionId + ", '" + loc.Region.Replace("'", "''") + "', " + loc.ResortId + ", '" + loc.Resort.Replace("'", "''") + "') ";

                            output += "Location Added : " + loc.Resort + ", " + loc.Region + ", " + loc.Country + "<br/>";
                        }

                        counter++;
                    }

                    output += counter + " Lines Read <br/>";
                }

                SqlCommand myCommand = new SqlCommand(sql, myConn);
                try
                {
                    myConn.Open();
                    myCommand.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    output += "<br/><br/><b><u>Sql Exception</u></b><br/>";
                    output += ex.ToString();
                    output += "<br/><br/>";
                }
            }

            return output;
        }

        public static List<JtLocation> GetLocations(string connStr)
        {
            List<JtLocation> locations = new List<JtLocation>();

            using (SqlConnection myConn = new SqlConnection(connStr))
            {
                SqlCommand myCommand = new SqlCommand("USE JacTravel SELECT DISTINCT Country, RegionID, Region FROM JtLocation", myConn);

                try
                {
                    myConn.Open();

                    using (SqlDataReader reader = myCommand.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                JtLocation location = new JtLocation
                                {
                                    Country = reader[0].ToString(),
                                    RegionId = Int32.Parse(reader[1].ToString()),
                                    Region = reader[2].ToString()
                                };
                                //location.ResortId = Int32.Parse(reader[3].ToString());
                                //location.Resort = reader[4].ToString();
                                locations.Add(location);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Database error occured in GetLocations" + ex.Message, ex);
                }
            }
            return locations;
        }

        public static List<string> GetCountries(string connStr)
        {
            List<string> countries = new List<string>();

            using (SqlConnection myConn = new SqlConnection(connStr))
            {
                SqlCommand myCommand = new SqlCommand("USE JacTravel SELECT DISTINCT Country FROM JtLocation", myConn);

                try
                {
                    myConn.Open();

                    using (SqlDataReader reader = myCommand.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                countries.Add(reader[0].ToString());
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Database error occured in GetLocations" + ex.Message, ex);
                }
            }

            return countries;
        }

        private static string KillDbConnections(SqlConnection myConn)
        {
            const string str = "DECLARE @SQL varchar(max) " +
                               "SELECT @SQL = COALESCE(@SQL,'') + 'Kill ' + Convert(varchar, SPId) + ';' " +
                               "FROM MASTER..SysProcesses " +
                               "WHERE DBId = DB_ID('" + DbName + "') AND SPId <> @@SPId " +
                               "EXEC(@SQL)";

            SqlCommand myCommand = new SqlCommand(str, myConn);
            try
            {
                myConn.Open();
                myCommand.ExecuteNonQuery();
                return "Connections Emptied Successfully";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            finally
            {
                if (myConn.State == ConnectionState.Open)
                {
                    myConn.Close();
                }
            }
        }
    }
}
