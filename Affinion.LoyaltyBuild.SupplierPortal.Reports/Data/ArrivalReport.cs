﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.SupplierPortal.Reports.Data
{
    /// <summary>
    /// Base arrival report item used by all three arrival report items
    /// </summary>
    public class ArrivalReport
    {
        /// <summary>
        /// Booking reference no
        /// </summary>
        public string BookingReference { get; set; }

        /// <summary>
        /// Offer name
        /// </summary>
        public string OfferName { get; set; }

        /// <summary>
        /// Client First name
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Client Last name
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Arrival  date
        /// </summary>
        public DateTime? Arrival { get; set; }

        /// <summary>
        /// Arrival  date
        /// </summary>
        public DateTime? Departure { get; set; }

        /// <summary>
        /// No of people
        /// </summary>
        public int? NumberOfAdults { get; set; }

        /// <summary>
        /// No of people
        /// </summary>
        public int? NumberOfChildren { get; set; }

        /// <summary>
        /// Reservation date
        /// </summary>
        public DateTime? ReservationDate { get; set; }

        /// <summary>
        /// Accommodation details
        /// </summary>
        public string Accommodation { get; set; }

        /// <summary>
        /// Booking status information
        /// </summary>
        public string BookingStatus { get; set; }

        /// <summary>
        /// Booking status information
        /// </summary>
        public DateTime? BookingStatusDate { get; set; }

        /// <summary>
        /// Acknowledgement
        /// </summary>
        public string Acknowledgement { get; set; }

        /// <summary>
        /// AddressLine1
        /// </summary>
        public string Line1 { get; set; }

        /// <summary>
        /// Address Line2
        /// </summary>
        public string Line2 { get; set; }

        /// <summary>
        /// City
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Region
        /// </summary>
        public string Region { get; set; }

        /// <summary>
        /// Country
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// PhoneNumber
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// MobilePhoneNUmber
        /// </summary>
        public string MobilePhoneNumber { get; set; }

        /// <summary>
        /// Amount collected from client (Paid amt-vat-commision)
        /// </summary>
        public decimal? CollectAmount { get; set; }

        /// <summary>
        /// Booking through onile/offline
        /// </summary>
        public string BookingThrough { get; set; }


        /// <summary>
        /// Child Age
        /// </summary>
        public string ChildAge { get; set; }

        /// <summary>
        /// Row colour
        /// </summary>
        public string RowColor { get; set; }

        /// <summary>
        /// HotelName
        /// </summary>
        public string HotelName { get; set; }

        /// <summary>
        /// note
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// CurrencyId
        /// </summary>
        public int? CurrencyId { get; set; }
    }

    public class ArrivalReportCollection : IEnumerable<ArrivalReport>
    {
        //Private variables declaration
        private List<ArrivalReport> _items;

        /// <summary>
        /// Constructor
        /// </summary>
        public ArrivalReportCollection()
        {
            _items = new List<ArrivalReport>();
        }

        /// <summary>
        /// Add method to add item
        /// </summary>
        /// <param name="item"></param>
        public void Add(ArrivalReport item)
        {
            _items.Add(item);
        }

        /// <summary>
        /// Override method IEnumenrable class
        /// </summary>
        /// <returns></returns>
        public IEnumerator<ArrivalReport> GetEnumerator()
        {
            foreach (var item in _items)
                yield return item;

        }

        /// <summary>
        /// Override method IEnumenrable class
        /// </summary>
        /// <returns></returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            foreach (var item in _items)
                yield return item;

        }

        /// <summary>
        /// Count
        /// </summary>
        public int Count
        {
            get { return _items.Count; }
        }
    }
}
