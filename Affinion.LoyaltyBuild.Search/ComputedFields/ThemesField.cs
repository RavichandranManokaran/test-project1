﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           ThemesField.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Search
/// Description:           This field will generate and store the readable value instead of the Id of "Themes" field of SupplierDetails template
///</summary>
namespace Affinion.LoyaltyBuild.Search.ComputedFields
{
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Newtonsoft.Json;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;

    /// <summary>
    /// Generates the value for the themes field
    /// </summary>
    public class ThemesField : ComputedField
    {
        /// <summary>
        /// Calculates the field value
        /// </summary>
        /// <param name="indexable">The indexable item</param>
        /// <returns>Returns the calculated value</returns
        public override object ComputeFieldValue(Sitecore.ContentSearch.IIndexable indexable)
        {
            try
            {
                var supplier = ContentDatabase.GetItem(ID.Parse(indexable.Id));
                Collection<Item> themesList = SitecoreFieldsHelper.GetMutiListItems(supplier, "Themes");
                List<string> themes = new List<string>();

                foreach (Item item in themesList)
                {
                    themes.Add(SitecoreFieldsHelper.GetValue(item, "Name"));
                }

                themes = themes.OrderBy(theme => theme).ToList();

                return JsonConvert.SerializeObject(themes);
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                return string.Empty;
            }
        }
    }
}
