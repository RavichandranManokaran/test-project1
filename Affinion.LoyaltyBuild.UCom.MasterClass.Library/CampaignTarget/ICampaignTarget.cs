﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.CampaignTarget
{
    /// <summary>
    /// Interface to define the campaign target related actions
    /// </summary>
    public interface ICampaignTarget
    {
        /// <summary>
        /// GetArrivalDateTargetByCampaignId
        /// </summary>
        /// <param name="campaignItemId">campaignItemId</param>
        /// <returns><list>CampaginItemDateRange</list>GetArrivalDateTargetByCampaignId</returns>
        List<CampaginItemDateRange> GetArrivalDateTargetByCampaignId(int campaignItemId);

        /// <summary>
        /// GetReservaionDateTargetByCampaignId
        /// </summary>
        /// <param name="campaignItemId">campaignItemId</param>
        /// <returns><list>CampaginItemDateRange</list>GetReservaionDateTargetByCampaignId</returns>
        List<CampaginItemDateRange> GetReservaionDateTargetByCampaignId(int campaignItemId);

        /// <summary>
        /// GetClientTargetByCampaignId
        /// </summary>
        /// <param name="campaignItemId">campaignItemId</param>
        /// <returns><list>ClientList</list>GetClientTargetByCampaignId</returns>
        List<ClientList> GetClientTargetByCampaignId(int campaignItemId);

        
        
    }
}
