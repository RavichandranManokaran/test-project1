﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AvailabilityReport.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Reports.AvailabilityReport" %>
<%@ Register TagPrefix="ajaxToolkit" Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<script language="javascript" type="text/javascript">
    //No.of Days Difference
    function DateDifference(sender, args) {
        var startDate = Date.parse(document.getElementById('tbStartDate').value);
        var endDate = Date.parse(document.getElementById('tbEndDate').value);
        var timeDiff = endDate - startDate;

        daysDiff = Math.floor(timeDiff / (1000 * 60 * 60 * 24));

        if (daysDiff > 366) {
            args.IsValid = false;
        }
        else {
            args.IsValid = true;
        }
    }
</script>


<style type="text/css">

     #maincontent_0_rvShowRecord_ctl09
       {

           max-height:350px;

       }     


</style>

<div class="supplier-report">
    <div>
        <h3 class="title-text font-weight-bold"> <sc:Text id="AvailabityReportTitle" runat="server" /></h3>
    </div>
    <div>
         <sc:Text id="AvailabityReportInformation" runat="server" />
        <asp:Label ID="lblshow" runat="server"></asp:Label>
    </div>
     <div class="col-xs-12">
        <asp:ValidationSummary ID="valsum" CssClass="alert alert-danger validation-summary" runat="server" ShowValidationErrors="true"  DisplayMode="BulletList" ShowSummary="true" />
    </div>
    <asp:UpdatePanel ID="uplFilters" runat="server">
        <ContentTemplate>
            <ajaxToolkit:CalendarExtender ID="dateFromCalendarExtender" runat="server" Animated="false"
                TargetControlID="txtDatefrom"
                PopupPosition="BottomLeft"
                Format="dd/MM/yyyy"
                PopupButtonID="btnDatfrom" />
            <ajaxToolkit:CalendarExtender ID="dateToCalendarExtender" runat="server" Animated="false"
                TargetControlID="txtDateTo"
                PopupPosition="BottomLeft"
                Format="dd/MM/yyyy"
                PopupButtonID="btnDateTo"/>
            <div class="row report-filter">
                <div class="col-xs-10">
                    <div class="row">
                        <div class="col-xs-2">
                             <sc:Text id="datefromText" runat="server" />
                            <asp:Label ID="lbldatefrom" runat="server"  AssociatedControlID="txtdatefrom"></asp:Label><span class="required">*</span>
                             </div>
                        <div class="col-xs-3">
                            <div class="input-group">
                                <asp:RequiredFieldValidator ID="datefromRequiredFieldValidator" ErrorMessage="From date is required" CssClass="highligh-error" ControlToValidate="txtDatefrom" runat="server" />
                                <asp:TextBox CssClass="form-control" ID="txtDatefrom" runat="server" MaxLength="50"></asp:TextBox>
                                <div class="input-group-addon">
                                    <asp:LinkButton ID="btnDatfrom" runat="server" CssClass="calendar glyphicon glyphicon-calendar" />
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-2" runat="server" id="providerLabel">
                            <label></label> <sc:Text id="ProviderText" runat="server" /><span class="required">*</span>
                        </div>
                        <div class="col-xs-3" runat="server" id="providerDropDown">                            
                            <asp:RequiredFieldValidator ID="rqvalProvider" ErrorMessage="Provider is required" CssClass="highligh-error" InitialValue="0" ControlToValidate="ddlProvider" runat="server" />
                            <asp:DropDownList CssClass="form-control" ID="ddlProvider" AutoPostBack="true" runat="server" AppendDataBoundItems="false">
                                <asp:ListItem Selected="True" Text="-Select-" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                             <sc:Text id="dateToText" runat="server" />
                            <asp:Label ID="lbldateto" runat="server"  AssociatedControlID="txtdateto"></asp:Label><span class="required">*</span>
                        </div>
                        <div class="col-xs-3">
                            <div class="input-group">
                                <asp:CompareValidator ID="cmpVal2" ControlToCompare="txtDatefrom"  CssClass="highligh-error"
									 ControlToValidate="txtDateTo" Type="Date" Operator="GreaterThanEqual"   
									 ErrorMessage="Start date should be greater than or equal to end date" runat="server"></asp:CompareValidator>
                                <asp:RequiredFieldValidator ID="datetoRequiredFieldValidator" ErrorMessage="To date is required" CssClass="highligh-error" ControlToValidate="txtDateTo" runat="server" />
                                <asp:CustomValidator ID="cusValDateTo" ErrorMessage="Report can only be viewed for 12 months at a time" CssClass="highligh-error" runat="server" ControlToValidate="txtDateTo" Display="None"></asp:CustomValidator>
                                <asp:TextBox CssClass="form-control" ID="txtDateto" runat="server" MaxLength="50"></asp:TextBox>

                                <div class="input-group-addon">
                                    <asp:LinkButton ID="btnDateTo" runat="server" CssClass="calendar glyphicon glyphicon-calendar" />
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-3">
                          <%--  <asp:Button ID="btnDisplay" runat="server" OnClick="Display_Click" Text="Submit" CssClass="btn btn-default"></asp:Button>--%>
                            <button id="btnDisplay" runat="server" onclick="Display_Click"  CssClass="btn btn-default">
                                 <sc:Text id="SubmitText" runat="server" />
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnDisplay" />
        </Triggers>
    </asp:UpdatePanel>
</div>
<div>
    <rsweb:ReportViewer ID="rvDisplayrecord" runat="server" Height="577px" Width="100%"  AsyncRendering="false"></rsweb:ReportViewer>
</div>



