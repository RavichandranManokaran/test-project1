﻿using Affinion.LoyaltyBuild.BedBanks.JacTravel.Model;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Model
{
    [Serializable()]
    [XmlType(TypeName = "SearchDetails")]
    public class JtSearchDetails
    {
        public string ArrivalDate { get; set; }
        public string Duration { get; set; }
        public string RegionID { get; set; }
        public string MealBasisID { get; set; }
        public string MinStarRating { get; set; }
        public List<JtSearchRoomRequest> RoomRequests { get; set; }
        public List<JtPropertyReferenceId> PropertyReferenceIDs { get; set; }
    }
}
