﻿CREATE TABLE [dbo].[uCommerce_Country] (
    [CountryId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]      NVARCHAR (128) NOT NULL,
    [Culture]   NVARCHAR (60)  NOT NULL,
    [Deleted]   BIT            DEFAULT ((0)) NOT NULL,
    CONSTRAINT [uCommerce_PK_Country] PRIMARY KEY CLUSTERED ([CountryId] ASC)
);

