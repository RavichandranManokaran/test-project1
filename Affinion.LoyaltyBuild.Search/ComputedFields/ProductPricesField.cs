﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           ProductPricesField.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Search
/// Description:           This field will generate and store the product prices assigned to client
/// </summary>
namespace Affinion.LoyaltyBuild.Search.ComputedFields
{
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Search.Data;
    using Affinion.LoyaltyBuild.Search.Helpers;
    using Newtonsoft.Json;
    using Sitecore.ContentSearch;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// This field will generate and store the product prices assigned to client
    /// </summary>
    public class ProductPricesField : ComputedField
    {
        /// <summary>
        /// Calculates the field value
        /// </summary>
        /// <param name="indexable">The indexable item</param>
        /// <returns>Returns the calculated value</returns>
        public override object ComputeFieldValue(IIndexable indexable)
        {
            try
            {
                Item supplier = ContentDatabase.GetItem(ID.Parse(indexable.Id));
                List<Item> clientsList = new List<Item>();
                List<ProductPrice> prices = new List<ProductPrice>();
                Item clientsFolder = ContentDatabase.GetItemByKey("ClientSetupFolder");

                if (clientsFolder != null)
                {
                    clientsList = clientsFolder.Axes.GetDescendants().Where(i => i.TemplateName == "ClientDetails").ToList<Item>();
                }

                foreach (Item client in clientsList)
                {
                    bool isSupplierSelected = SitecoreFieldsHelper.MultiListContains(client, "AllowedSuppliers", supplier);

                    if (isSupplierSelected)
                    {
                        SearchKey searchTerm = new SearchKey();
                        searchTerm.CheckinDate = DateTime.Now;
                        searchTerm.CheckoutDate = DateTime.Now.AddDays(1);
                        searchTerm.Client = client;

                        SupplierData offersList = StoreHelper.GetProducts(supplier, client, searchTerm);

                        if (offersList != null)
                        {
                            //var offers = offersList.Offers.GroupBy(x => x.OccupancyTypeId).Select(x => x.OrderBy(y => y.Price)).Select(x => x.First());


                            //foreach (OfferDataItem offer in offers)
                            foreach (OfferDataItem offer in offersList.Offers)
                            {
                                ProductPrice price = new ProductPrice();

                                price.ClientId = client.ID.ToString();
                                price.OccupancyTypeId = offer.OccupancyTypeId;
                                price.Price = offer.Price;
                                price.offerType = offer.OfferType;
                                price.CampaignItemId = offer.CampaignItemId;
                                prices.Add(price);
                            }
                        }
                    }
                }

                return JsonConvert.SerializeObject(prices);
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                return string.Empty;
            }
        }
    }
}
