﻿
CREATE TABLE [dbo].[LB_PortalTypeTarget](
	[PortalTypeTargetId] [int] NOT NULL,
	[PortalType] [varchar](250) NOT NULL,
 CONSTRAINT [PK_uCommerce_PortalTypeTarget] PRIMARY KEY CLUSTERED 
(
	[PortalTypeTargetId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[LB_PortalTypeTarget]  WITH CHECK ADD  CONSTRAINT [FK_uCommerce_PortalTypeTarget_uCommerce_Target] FOREIGN KEY([PortalTypeTargetId])
REFERENCES [dbo].[uCommerce_Target] ([TargetId])
GO

ALTER TABLE [dbo].[LB_PortalTypeTarget] CHECK CONSTRAINT [FK_uCommerce_PortalTypeTarget_uCommerce_Target]
GO