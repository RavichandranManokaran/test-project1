﻿CREATE TABLE [dbo].[uCommerce_OrderLineSubrates]
(
	[OrderLineSubrateID] INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[OrderLineID] INT NOT NULL,
	[SubrateID] INT NOT NULL,
	[Amount] MONEY NOT NULL,
	[Discount] MONEY NULL,
	[IsIncludedInUnitPrice] BIT NOT NULL
	)
	Go
