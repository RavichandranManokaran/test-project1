﻿namespace Affinion.LoyaltyBuild.DataAccess.DatabaseScripts
{
    using System;

    /// <summary>
    /// Interface for executing database scripts programatically
    /// </summary>
    public interface IAffinionDatabaseScript
    {
        /// <summary>
        /// Gets the connection string to connect to the database
        /// </summary>
        /// <returns>Returns the connection string</returns>
        string GetConnectionStringName();

        /// <summary>
        /// Execute the database scripts if not already executed
        /// </summary>
        bool ExecuteScripts();
    }
}
