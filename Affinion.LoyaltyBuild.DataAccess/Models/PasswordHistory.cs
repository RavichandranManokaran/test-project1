﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           PasswordHistory.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.DataAccess
/// Description:           PasswordHistory data model.
/// </summary>

#region Using Directives
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema; 
#endregion

namespace Affinion.LoyaltyBuild.DataAccess.Models
{
    public class PasswordHistory
    {
        /// <summary>
        /// User name
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        
        /// <summary>
        /// User name
        /// </summary>
        
        public string UserName { get; set; }

        /// <summary>
        /// Password
        /// </summary>
        
        public string Password { get; set; }

        /// <summary>
        /// Password Salt
        /// </summary>
        public string PasswordSalt { get; set; }

        /// <summary>
        /// Changed date
        /// </summary>
        public DateTime ChangedDate { get; set; }
    }
}
