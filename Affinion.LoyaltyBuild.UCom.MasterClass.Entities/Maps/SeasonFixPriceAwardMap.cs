﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Maps
{
    public class SeasonFixPriceAwardMap : SubclassMap<SeasonFixPriceAward>
    {
        public SeasonFixPriceAwardMap()
        {
            base.Table("uCommerce_SeasonFixPriceAward");
            base.KeyColumn("SeasonFixPriceAwardId");
            base.Map(x => x.SeasonFixPriceAwardId).ReadOnly();
            base.Map(x => x.Content).ReadOnly();
            base.HasMany<SeasonFixPriceAwardDetail>(x => x.Details).KeyColumn("SeasonFixPriceAwardId");//.Cascade.All().AsBag();
        }
    }

}
