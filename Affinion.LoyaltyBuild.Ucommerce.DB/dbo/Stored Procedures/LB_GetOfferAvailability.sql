﻿CREATE procedure LB_GetOfferAvaialibility
@VariantSKU varchar(max),
@sdate int,
@edate int
As
select 
 CampaignItem,
 (NumberAllocated - NumberBooked) as AvaialbileRoom,
 Sku,
 VariantSku,
 DateCounter,
 IsRestrictedForSale IsRestricted
from LB_OfferAvailability lba
 join uCommerce_ParseArrayToTable (@VariantSKU,',',0) tmp on lba.VariantSku = tmp.stringvalue
where DateCounter>= @sdate and datecounter <= @edate