﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           DecryptButtonEvent.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Handles events of custom ribbon button added for decryption
/// </summary>

#region Using Directives
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Data.Items;
using Sitecore.Shell.Framework.Commands;
using System;
using System.Security.Cryptography;
#endregion

namespace Affinion.LoyaltyBuild.Common.RibbonEvents
{
    public class DecryptButtonEvent : Command
    {
        #region Private Constants
        /// <summary>
        /// Custom message for exception occurs when decrypting
        /// </summary>
        private const string DecryptExceptionMessage = "Decryption failed for the selected item";
        /// <summary>
        /// Format exception error message
        /// </summary>
        private const string SpecificDecryptExceptionMessage = "Selected item cannot be decrypted";
        /// <summary>
        /// Custom message popup header
        /// </summary>
        private const string DecryptExceptionMessageHeader = "Invalid Data";
        /// <summary>
        /// Custom message header for popup showing decrypted value
        /// </summary>
        private const string DecryptedValueMessageHeader = "Setting Decrypted";
        /// <summary>
        /// Field name of the sitecore item
        /// </summary>
        private const string ItemFieldName = "Value";
        #endregion

        #region Public Methods
        /// <summary>
        /// Execute for the click event of the custom button
        /// </summary>
        /// <param name="context">Current command context</param>
        public override void Execute(CommandContext context)
        {
            try
            {
                /// Get the currently editing sitecore item to decrypt
                Item editingItem = context.Items[0];
                /// Value of the sitecore item
                string fieldValue = editingItem.Fields[ItemFieldName].Value;
                /// Decrypt field value
                string decryptedValue = Cryptography.Decrypt(fieldValue);
                /// Show decrypted value as a popup message
                Sitecore.Context.ClientPage.ClientResponse.Alert(decryptedValue, null, DecryptedValueMessageHeader);
            }
            catch (CryptographicException crytographyException)// Handlea specific exception when decrypting value
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, crytographyException, typeof(DecryptButtonEvent));
                // TODO: Handle in a general way(error page or a error label in layout)
                Sitecore.Context.ClientPage.ClientResponse.Alert(SpecificDecryptExceptionMessage, null, DecryptExceptionMessageHeader);
            }
            catch (FormatException formatException)// Handlea specific exception when decrypting value
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, formatException, typeof(DecryptButtonEvent));
                // TODO: Handle in a general way(error page or a error label in layout)
                Sitecore.Context.ClientPage.ClientResponse.Alert(SpecificDecryptExceptionMessage, null, DecryptExceptionMessageHeader);
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, typeof(DecryptButtonEvent));
                /// Show custom Error message 
                // TODO: Handle in a general way(error page or a error label in layout)
                Sitecore.Context.ClientPage.ClientResponse.Alert(DecryptExceptionMessage, null, DecryptExceptionMessageHeader);
            }
        }
        #endregion
    }
}
