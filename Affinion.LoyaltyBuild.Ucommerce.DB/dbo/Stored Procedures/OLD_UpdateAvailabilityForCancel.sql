﻿CREATE PROCEDURE [dbo].[OLD_UpdateAvailabilityForCancel]
					@orderlineid int
AS
BEGIN
	UPDATE LB_RoomAvailability 
	SET NumberBooked=(NumberBooked-1)	
	FROM ucommerce_orderline ol
		INNER JOIN LB_RoomAvailability ra ON ol.sku = ra.VariantSku
		INNER JOIN LB_RoomReservation rr ON ol.orderlineid = rr.orderlineid
	WHERE 
		ol.orderlineid = @orderlineid 
		AND 
		rr.checkindate <= dbo.GetDateByCounter(ra.datecounter) 
		AND 
		rr.checkoutdate > dbo.GetDateByCounter(ra.datecounter)
END

