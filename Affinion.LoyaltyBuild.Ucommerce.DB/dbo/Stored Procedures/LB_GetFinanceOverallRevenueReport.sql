﻿CREATE PROCEDURE [dbo].[LB_GetFinanceOverallRevenueReport]
@StartDate DATETIME=NULL,
@EndDate DATETIME=NULL,
@BookingMethodID INT=0,
@SubTypeReportID INT=0
AS
BEGIN 
	DECLARE @bookingThrough VARCHAR(20)
	
  
	 IF(@BookingMethodID=1)
	 BEGIN
		  SET @bookingThrough='Online'
	 END
	 ELSE IF(@BookingMethodID=2) 
	 BEGIN 
		  SET @bookingThrough='Offline'
	 END 
	 ELSE IF(@BookingMethodID=3)
	 BEGIN 
		  SET @bookingThrough=NULL
	 END  
	  
	 IF(@SubTypeReportID = 1)
	 BEGIN      
		   SELECT 
						OL.CreatedOn,
						COUNT(OL.CreatedOn) AS 'Bookings',
						OL.ProductName AS ClientOffer,
						OP.ClientId [ClientId],
						C.ISOCode AS CurrencyName,
						SUM(OL.Total)+SUM(CAST(isnull(OP.CreditCardProcessingFee,0) as float))  AS TotalPrice,
						OL.VAT,
						SUM(OL.Total) AS LoyaltyBuildRevenue	
					FROM LB_BookingInfo BI 
						INNER JOIN uCommerce_OrderLine OL on BI.OrderLineId=OL.OrderLineId
						INNER JOIN (
										select OrderLineId, _ClientId [ClientId],_BookingThrough [BookingThrough], _CreditCardProcessingFee [CreditCardProcessingFee]
										from 
										(
											select OrderLineId, [key], [value]
											from uCommerce_OrderProperty where [Key] IN ('_ClientId','_BookingThrough','_CreditCardProcessingFee')
										) src
										pivot
										(
											max([value])
											for [key] in ( _ClientId,_BookingThrough, _CreditCardProcessingFee)
										) piv
									) OP on  OL.OrderLineId = OP.OrderLineId
						INNER JOIN LB_OrderLinePayment OLP ON OP.OrderLineID=OLP.OrderLineID
						INNER JOIN uCommerce_OrderProperty OLPR ON OLPR.OrderLineID=OP.OrderLineID
						INNER JOIN uCommerce_Currency C ON OLP.CurrencyTypeId=C.CurrencyId
						INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId=PO.OrderId AND PO.CustomerId IS NOT NULL
						INNER JOIN uCommerce_Customer CU ON PO.CustomerId=CU.CustomerId
						
					WHERE 
					   OP.BookingThrough IS NOT NULL AND
					   (@bookingThrough IS NULL OR OP.BookingThrough LIKE '%'+@bookingThrough+'%') AND
					   CAST(OL.CreatedOn AS DATE) >= CAST(@StartDate AS DATE) AND  CAST(OL.CreatedOn AS DATE) <= CAST(@EndDate AS DATE) AND
					   OL.VAT IS NOT NULL
					GROUP BY 
					   OL.ProductName,C.ISOCode,OP.ClientId,OL.VAT,OL.CreatedOn ORDER BY OL.CreatedOn DESC 
       
	 END
	 ELSE IF(@SubTypeReportID = 2)
	 BEGIN 
		  SELECT 
						OL.CreatedOn,
						COUNT(OL.CreatedOn) AS 'Bookings',
						OL.ProductName AS ClientOffer,
						OP.ClientId [ClientId],
						C.ISOCode AS CurrencyName,
						SUM(OL.Total) AS TotalPrice,
						OL.VAT,
						SUM(OL.Total) AS LoyaltyBuildRevenue	
					FROM LB_BookingInfo BI
						INNER JOIN uCommerce_OrderLine OL on BI.OrderLineId=OL.OrderLineId
						INNER JOIN (
										select OrderLineId, _ClientId [ClientId],_BookingThrough [BookingThrough]
										from 
										(
											select OrderLineId, [key], [value]
											from uCommerce_OrderProperty where [Key] IN ('_ClientId','_BookingThrough')
										) src
										pivot
										(
											max([value])
											for [key] in ( _ClientId,_BookingThrough)
										) piv
									) OP on  OL.OrderLineId = OP.OrderLineId
						INNER JOIN LB_OrderLinePayment OLP ON OP.OrderLineID=OLP.OrderLineID
						INNER JOIN uCommerce_Currency C ON OLP.CurrencyTypeId=C.CurrencyId
						INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId=PO.OrderId AND PO.CustomerId IS NOT NULL
						INNER JOIN uCommerce_Customer CU ON PO.CustomerId=CU.CustomerId
						
					WHERE 
					   OP.BookingThrough IS NOT NULL AND
					   (@bookingThrough IS NULL OR OP.BookingThrough LIKE '%'+@bookingThrough+'%') AND
					   CAST(OL.CreatedOn AS DATE) >= CAST(@StartDate AS DATE) AND  CAST(OL.CreatedOn AS DATE) <= CAST(@EndDate AS DATE) AND
					   OL.VAT IS NOT NULL
					 GROUP BY 
					   OL.ProductName,C.ISOCode,OP.ClientId,OL.VAT,OL.CreatedOn ORDER BY OL.CreatedOn DESC 
	 END  
END 


