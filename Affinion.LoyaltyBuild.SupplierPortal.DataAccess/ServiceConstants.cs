﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.SupplierPortal.DataAccess
{
    // <summary>
    /// service constants
    /// </summary>
    public class ServiceConstants
    {
        #region sitecore Item path

        /// <summary>
        /// provide path
        /// </summary>
        public const String ProviderPath = "/sitecore/content/#admin-portal#/#supplier-setup#//*[@@templateid='{A7235C94-2EF5-4A3B-87DB-0D4DCF10B4DB}']";

        /// <summary>
        ///group provider
        /// </summary>
        public const String GroupProviderPath = "/sitecore/content/#admin-portal#/#supplier-setup#/#global#/#_supporting-content#/#groups#//*[@@templateid='{26507FB6-AFD2-4DCC-B56F-AD1BB5D60CFA}']";

        /// <summary>
        /// provider type
        /// </summary>
        public const String ClientPath = "/sitecore/content/#admin-portal#/#client-setup#//*[@@templateid='{4CFC54E0-069A-4344-85E3-2BD99DA576E6}']";

        /// <summary>
        /// client path
        /// </summary>
        public const String ProviderTypePath = "/sitecore/content/#admin-portal#/#supplier-setup#/#global#/#_supporting-content#/#supplier-types#//*[@@templateid='{3E85F816-1D87-4FFA-8261-820C92266305}']";
        #endregion

        #region constant fileds
        /// <summary>
        ///uCommerce category id
        /// </summary>
        public const String UCommerceCategoryID = "UCommerceCategoryID";

        /// <summary>
        ///uCommerce category id
        /// </summary>
        public const String Sku = "SKU";

        /// <summary>
        ///Supplier type field
        /// </summary>
        public const String SupplierType = "SupplierType";

          /// <summary>
        ///Supplier type field
        /// </summary>
        public const String Group = "Group";
       

        #endregion
    }
}
