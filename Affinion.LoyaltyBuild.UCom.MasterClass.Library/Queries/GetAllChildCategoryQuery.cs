﻿using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using NHibernate;
using NHibernate.Transform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2.Queries;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Queries
{
    public class GetAllChildCategoryQuery : ICannedQuery<LBChildCategory>
    {
        /// <summary>
        /// CampaignItemId
        /// </summary>
        private readonly int categoryId;

        /// <summary>
        /// CampaignRevenueQuery 
        /// </summary>
        /// <param name="categoryId"></param>
        public GetAllChildCategoryQuery(int categoryId)
        {
            this.categoryId = categoryId;
        }

        /// <summary>
        /// IEnumerable type CampaignRevenue
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public IEnumerable<LBChildCategory> Execute(ISession session)
        {
            try
            {
                return session.CreateSQLQuery("exec GetAllChildCategory :@categoryid").SetParameter("@categoryid", categoryId)
                .SetResultTransformer(Transformers.AliasToBean<LBChildCategory>())
                .List<LBChildCategory>().ToList();
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.MasterClassBusinessLogic, ex, this);
                return new List<LBChildCategory>().AsEnumerable();
            }

        }


    }
}
