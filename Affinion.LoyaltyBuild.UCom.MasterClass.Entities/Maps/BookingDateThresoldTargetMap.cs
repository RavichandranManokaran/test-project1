﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class BookingDateThresoldTargetMap : SubclassMap<BookingDateThresoldTarget>
    {
        // Methods
        public BookingDateThresoldTargetMap()
        {
            base.Table("LB_BookingDateThresoldTarget");
            base.KeyColumn("BookingDateThresoldTargetId");
            base.Map(x => x.BookingDateThresoldTargetId).ReadOnly();
            base.Map(x => x.MinHours).Nullable();
            base.Map(x => x.MaxHours).Nullable();
        }

    }

}
