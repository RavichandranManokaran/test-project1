﻿
using Affinion.LoyaltyBuild.UCom.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;
using UCommerce.Marketing.Targets;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class RoomTypeTarget : Target, IOrderLineTarget, IEntity, ITarget
    {
        private int? _hashCode;

        public virtual bool IsSatisfiedBy(OrderLine orderLine)
        {
            OrderProperty searchIgnor = orderLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.IgnorCampaignSearchCriteria);
            if (searchIgnor != null)
                return true;
            OrderProperty forSearch = orderLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.ForSearch);
            if (forSearch != null)
            {
                OrderProperty roomTypes = orderLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.RoomType);
                if (roomTypes != null)
                {
                    return this.RoomType.Equals(roomTypes.Value, StringComparison.InvariantCultureIgnoreCase);
                }
                return false;
            }
            else
            {
                Product p = Product.All().Where(x => x.Sku == orderLine.Sku && x.VariantSku == orderLine.VariantSku).FirstOrDefault();
                if (p.ProductProperties != null && p.ProductProperties.Any(x => x.ProductDefinitionField.Name.Equals(ProductDefinationConstant.RoomType, StringComparison.InvariantCultureIgnoreCase) && x.Value.Equals(this.RoomType, StringComparison.InvariantCultureIgnoreCase)))
                {
                    return true;
                }
            }
            return false;


        }


        // Methods
        public new static IQueryable<RoomTypeTarget> All()
        {
            return GetRepo().Select();
        }

        public override void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<RoomTypeTarget, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            RoomTypeTarget objA = obj as RoomTypeTarget;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<RoomTypeTarget, bool>> expression)
        {
            return All().Any<RoomTypeTarget>(expression);
        }

        public static IList<RoomTypeTarget> Find(Expression<Func<RoomTypeTarget, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<RoomTypeTarget>();
        }

        public static RoomTypeTarget FirstOrDefault(Expression<Func<RoomTypeTarget, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<RoomTypeTarget>();
        }

        public new static RoomTypeTarget Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<RoomTypeTarget> GetRepo()
        {
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<RoomTypeTarget>>();
            
        }


        public override void Save()
        {
            GetRepo().Save(this);
        }

        public static RoomTypeTarget SingleOrDefault(Expression<Func<RoomTypeTarget, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties
        public override int Id
        {
            get
            {
                return this.RoomTypeTargetId;
            }
        }

        public virtual int RoomTypeTargetId { get; protected set; }

        public virtual string RoomType { get; set; }






    }
}
