﻿using System;

namespace ASR
{
    public class RootItemNotFoundException:Exception
    {
        public RootItemNotFoundException(string message)
            : base(message)
        {
        }
    }
}
