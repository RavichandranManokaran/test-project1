CREATE PROCEDURE [dbo].[LB_APPaymentRequest]
@OraclePaymentXML NVARCHAR(MAX)=NULL,
@RequestFrom VARCHAR(20)=NULL
AS
BEGIN
DECLARE @XmlDocumentHandle INT
DECLARE @XmlDocument NVARCHAR(MAX)
DECLARE @OracleFileSequenceNo INT
DECLARE @OracleFileName VARCHAR(50)

SET @XmlDocument =@OraclePaymentXML

DECLARE  @APOraclePayment TABLE
(
SupplierPaymentId  INT,
OrderId INT,
OrderLineId INT,
OrderDate  DATETIME,
NominalCode VARCHAR(50),
CampaignAccountingId  VARCHAR(20),
OrderCurrencyId INT,
OrderCurrencyIsoCode  VARCHAR(5),
EuroRate DECIMAL(11,2),
CustomerId INT,
SuggestedAmount DECIMAL(11,2),
PostedAmount DECIMAL(11,2),
OracleID VARCHAR(50),
SupplierAddressCode NVARCHAR(50),
LineNumber INT,
CreatedBy VARCHAR(80),
UpdatedBy VARCHAR(80),
IsDeleted TINYINT
)

EXEC sp_xml_preparedocument @XmlDocumentHandle OUTPUT, @XmlDocument

SET @OracleFileSequenceNo=(SELECT SequenceNo FROM LB_OracleFile WHERE IsClosedOff=0 AND OracleFileType='AP')

IF @OracleFileSequenceNo IS NULL OR @OracleFileSequenceNo=0
BEGIN
          SET @OracleFileSequenceNo=(SELECT MAX(SequenceNo)+1 FROM LB_OracleFile WHERE IsClosedOff=1 AND OracleFileType='AP')
		  IF @OracleFileSequenceNo IS NULL OR @OracleFileSequenceNo=0
		  BEGIN
		       SET @OracleFileName='AP1'
			   SET @OracleFileSequenceNo=1
		  END
		  ELSE
		  BEGIN
		       SET @OracleFileName='AP'+CONVERT(VARCHAR,@OracleFileSequenceNo)
		  END
          INSERT INTO LB_OracleFile(OracleFileName,OracleFileType,SequenceNo) VALUES(@OracleFileName,'AP',@OracleFileSequenceNo)
END

INSERT INTO @APOraclePayment
SELECT    *
FROM      OPENXML (@XmlDocumentHandle, '/APOraclePayment/APOraclePayment',2)
           WITH 
		   (
		         SupplierPaymentId  INT,
                 OrderId INT,
				 OrderLineId INT,
				 OrderDate  DATETIME,
                 NominalCode VARCHAR(50),
				 CampaignAccountingId  VARCHAR(20),
                 OrderCurrencyId INT,
				 OrderCurrencyIsoCode  VARCHAR(5),
                 EuroRate DECIMAL(11,2),
				 CustomerId INT,
				 SuggestedAmount DECIMAL(11,2),
				 PostedAmount DECIMAL(11,2),
				 OracleID VARCHAR(50),
				 SupplierAddressCode NVARCHAR(50),
				 LineNumber INT,
				 CreatedBy VARCHAR(80),
				 UpdatedBy VARCHAR(80),
				 IsDeleted TINYINT

			)
EXEC sp_xml_removedocument @XmlDocumentHandle

INSERT INTO LB_Supplier_Payments
          ( 
                 OrderId,
				 OrderLineId,
				 OrderDate,
                 NominalCode,
				 CampaignAccountingId,
                 OrderCurrencyId,
				 OrderCurrencyIsoCode,
                 EuroRate,
				 CustomerId,
				 SuggestedAmount,
				 OracleID,
				 OracleFileId,
				 SupplierAddressCode,
				 LineNumber,
				 CreatedBy,
				 DateCreated
		     ) 
             SELECT  
					 OrderId,OrderLineId,OrderDate,NominalCode,CampaignAccountingId,OrderCurrencyId,OrderCurrencyIsoCode,
					 EuroRate,CustomerId,SuggestedAmount,OracleID,@OracleFileSequenceNo AS OracleFileId,SupplierAddressCode,
					 LineNumber,CreatedBy,GETDATE()
			 FROM 
					 @APOraclePayment
			 WHERE 
					 SupplierPaymentId=0 

IF @RequestFrom='HotelDemands'
BEGIN
UPDATE  
       LB_S_P SET 
	               LB_S_P.PostedAmount=APOP.PostedAmount,
				   LB_S_P.DateUpdated=GETDATE(),
				   LB_S_P.DatePostedToOracle=GETDATE()
					
FROM 
       LB_Supplier_Payments LB_S_P,@APOraclePayment APOP
WHERE 
       LB_S_P.SupplierPaymentId=APOP.SupplierPaymentId AND

	   APOP.SupplierPaymentId >0
END

ELSE IF @RequestFrom='HotelPaymentRequest'
BEGIN
UPDATE  
       LB_S_P SET 
	               LB_S_P.SuggestedAmount=APOP.SuggestedAmount,
				   LB_S_P.DateUpdated=GETDATE()
					
FROM 
       LB_Supplier_Payments LB_S_P,@APOraclePayment APOP
WHERE 
       LB_S_P.SupplierPaymentId=APOP.SupplierPaymentId AND

	   APOP.SupplierPaymentId >0
END



END