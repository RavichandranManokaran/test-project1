﻿<%@ control language="C#" autoeventwireup="true" codebehind="OfflinePortalPaymentByCurrencies.ascx.cs" inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalPaymentByCurrencies" %>

<%if(PaymentInfo != null) {%>
    <%if(PaymentInfo.DueByCurrencies.Count > 0){ %>
        <div class="panel-heading pay_bg">
            <span class="pric-eu"><strong>Total Amount </strong></span>
            <span class="pay_optn"><strong>Payment</strong></span>
        </div>
        <%foreach(var item in PaymentInfo.DueByCurrencies) {%>
        <div class="panel-heading">
            <span class="pric-eu cur-<%=item.CurrencyTypeId %>"><strong><%= item.Amount.ToString("f") %></strong> </span>
            <span class="pay_optn">
                <a href="/<%=(item.Amount>0?"offlineportalpayment":"offlineportalrefund")  %>?OLID=<%=PaymentInfo.OrderLineId %>&CID=<%=item.CurrencyTypeId %>&amount=<%=item.Amount%>">Pay Now</a>
                <asp:hyperlink id="Hyperlink1" text="Pay Now" navigateurl="" runat="server" Visible="false"> </asp:hyperlink>
            </span>
        </div>
        <%} %>
    <%} %>
    <%else{ %>
        <div class="panel-heading">
            <span class="pric-eu"><strong>0.00</strong> </span>
            <span class="pay_optn">
                 <a href="javascript:void(0);" onclick="NoPayClick()">Pay Now</a>
            </span>
        </div>
    <%} %>
<%} %>
