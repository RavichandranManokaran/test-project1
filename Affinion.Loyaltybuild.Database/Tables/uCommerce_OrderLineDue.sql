﻿CREATE TABLE [dbo].[uCommerce_OrderLineDue]
(
                [OrderLineDueId] INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
                [OrderLineID] INT NOT NULL,
				[SubrateID] INT NULL,
                [DueTypeId] INT NOT NULL,
				[CurrencyTypeId] INT NOT NULL,
                [Amount] MONEY NOT NULL,
                [CommissionAmount] MONEY NOT NULL,
                [TransferAmount] MONEY NOT NULL,
                [ClientAmount] MONEY NOT NULL,
                [ProviderAmount] MONEY NOT NULL,
                [CollectionByProviderAmount] MONEY NOT NULL,
                [Note] VARCHAR(MAX) NOT NULL,
                [OrderLinePaymentID] INT NULL,
                [IsDeleted] BIT NULL,
                [CreatedBy] VARCHAR(50) NULL,
                [CreatedDate] DATETIME NULL,
                [UpdatedBy] VARCHAR(50) NULL,
                [UpdatedDate] DATETIME NULL
)
Go