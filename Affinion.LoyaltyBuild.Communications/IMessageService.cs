﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           ICommunicationService.cs
/// Sub-system/Module:     Communications(VS project name)
/// Description:           Communication Sevice Interface
/// </summary>

#region Using Directives
using System.Collections.Generic; 
#endregion

namespace Affinion.LoyaltyBuild.Communications
{

    public interface IMessageService
    {
        #region Sinlge Receiver
        /// <summary>
        /// Send Message
        /// </summary>
        /// <param name="to">Sinlge Receiver</param>
        /// <param name="message">Message</param>
        /// <returns>Return True if success otherwise False</returns>
        ResponseStatus Send(string to, string message);

        /// <summary>
        /// Send Message
        /// </summary>
        /// <param name="from">Sender</param>
        /// <param name="to">Single Receiver</param>
        /// <param name="message">Message</param>
        /// <returns>Return True if success otherwise False</returns>
        ResponseStatus Send(string from, string to, string message); 
        #endregion

        #region Mutiple Receivers
        /// <summary>
        /// Send Message
        /// </summary>
        /// <param name="to">Mutiple Receivers</param>
        /// <param name="message">Message</param>
        /// <returns>Return True if success otherwise False</returns>
        ResponseStatus Send(IList<string> to, string message);

        /// <summary>
        /// Send Message
        /// </summary>
        /// <param name="from">Sender</param>
        /// <param name="to">Mutiple Receivers</param>
        /// <param name="message">Message</param>
        /// <returns>Return True if success otherwise False</returns>
        ResponseStatus Send(string from, IList<string> to, string message); 
        #endregion

        /// <summary>
        /// Get error list
        /// </summary>
        /// <returns>Returns error list</returns>
        ICollection<ResponseError> GetErrors();
    }
}
