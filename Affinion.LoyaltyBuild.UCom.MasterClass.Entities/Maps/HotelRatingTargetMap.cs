﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Maps
{
    public class HotelRatingTargetMap : SubclassMap<HotelRatingTarget>
    {
        // Methods
        public HotelRatingTargetMap()
        {
            base.Table("LB_HotelRatingTarget");
            base.KeyColumn("HotelRatingTargetId");
            base.Map(x => x.HotelRatingTargetId).ReadOnly();
            base.Map(x => x.HotelRating).Not.Nullable();
        }

    }



}