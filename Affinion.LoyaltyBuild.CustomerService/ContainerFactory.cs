﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Affinion.LoyaltyBuild.Api.Booking.Service;

/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic
{
    public class ContainerFactory
    {
        /// <summary>
        /// private proprties
        /// </summary>
        private static object obj = new object();
        private static ContainerFactory _factory = null;
        private Dictionary<Type, Func<object>> registrations = new Dictionary<Type, Func<object>>();

        /// <summary>
        /// private constructor to avoid instance creation
        /// </summary>
        private ContainerFactory()
        {
            Register<Order.IOrderService, Order.OrderService>();
            Register<Basket.IBasketService, Basket.BasketService>();
            Register<Payment.IPaymentService, Payment.PaymentService>();
            Register<Email.IMailService, Email.MailService>();
            Register<Provider.IProviderService, Provider.ProviderService>();
            Register<Customer.ICustomerService, Customer.CustomerService>();
            Register<Customer.IQueryService, Customer.QueryService>();
            Register<Affinion.LoyaltyBuild.Api.Booking.Service.IPaymentService, Affinion.LoyaltyBuild.Api.Booking.Service.PaymentService>();
        }

        /// <summary>
        /// get singleton instance
        /// </summary>
        public static ContainerFactory Instance
        {
            get
            {
                if (_factory == null)
                {
                    lock (obj)
                    {
                        if (_factory == null)
                            _factory = new ContainerFactory();
                    }
                }

                return _factory;
            }
        }

        /// <summary>
        /// register instnce
        /// </summary>
        /// <typeparam name="TService"></typeparam>
        /// <typeparam name="TImpl"></typeparam>
        public void Register<TService, TImpl>() where TImpl : TService
        {
            this.registrations.Add(typeof(TService), () => this.GetInstance(typeof(TImpl)));
        }
        
        /// <summary>
        /// register instnce
        /// </summary>
        /// <typeparam name="TService"></typeparam>
        /// <typeparam name="TImpl"></typeparam>
        public void Register<TService>(Func<TService> instanceCreator)
        {
            this.registrations.Add(typeof(TService), () => instanceCreator());
        }

        /// <summary>
        /// register instnce
        /// </summary>
        /// <typeparam name="TService"></typeparam>
        /// <typeparam name="TImpl"></typeparam>
        public void RegisterSingleton<TService>(TService instance)
        {
            this.registrations.Add(typeof(TService), () => instance);
        }

        /// <summary>
        /// register instnce
        /// </summary>
        /// <typeparam name="TService"></typeparam>
        /// <typeparam name="TImpl"></typeparam>
        public void RegisterSingleton<TService>(Func<TService> instanceCreator)
        {
            var lazy = new Lazy<TService>(instanceCreator);
            this.Register<TService>(() => lazy.Value);
        }

        /// <summary>
        /// get instance
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetInstance<T>()
        {
            return (T)GetInstance(typeof(T));
        }

        #region Private Methods

        /// <summary>
        /// Get instance
        /// </summary>
        /// <typeparam name="serviceType"></typeparam>
        /// <returns></returns>
        private object GetInstance(Type serviceType)
        {
            Func<object> creator;
            if (this.registrations.TryGetValue(serviceType, out creator)) return creator();
            else if (!serviceType.IsAbstract) return this.CreateInstance(serviceType);
            else throw new InvalidOperationException("No registration for " + serviceType);
        }

        /// <summary>
        /// Get instance
        /// </summary>
        /// <typeparam name="serviceType"></typeparam>
        /// <returns></returns>
        private object CreateInstance(Type implementationType)
        {
            var ctor = implementationType.GetConstructors().Single();
            var parameterTypes = ctor.GetParameters().Select(p => p.ParameterType);
            var dependencies = parameterTypes.Select(this.GetInstance).ToArray();
            return Activator.CreateInstance(implementationType, dependencies);
        }

        #endregion
    }
}
