﻿using System;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration.SiteUI.Base;
using Sitecore.Links;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Single_Item
{
    public partial class AbstractSpot : SitecoreSingleItemUserControl
    {
        private void Page_Load(object sender, EventArgs e)
        {
            WriteAlertsIfNeeded();
            HideIfNoVersionUnlessPageEditing();

            if (IsDataSourceItemNull)
            {
                if (IsPageEditorEditing)
                {
                    pnlAbstract.Visible = false;
                }
            }
            else
            {
                LinkTo.NavigateUrl = LinkManager.GetItemUrl(DataSourceItem);
                LinkTo.Text = String.Format("{0}&nbsp;»", GetDictionaryText("Learn more"));
            }
        }
    }
}