﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SessionHelper.cs
/// Sub-system/Module:     Common(VS project name)
/// Description:           Contains the data class for link field functions
/// </summary>
#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Affinion.LoyaltyBuild.Model.Provider;
#endregion

namespace Affinion.LoyaltyBuild.Common.SessionHelper
{
    /// <summary>
    /// Class to store basket related information
    /// </summary>
    public class BasketInfo
    {
        public string Sku { get; set; }
        public string VariantSku { get; set; }
        public string SupplierId { get; set; }
        public string OrderLineId { get; set; }
        public string OrderLineReference { get; set; }

        public string CheckinDate { get; set; }
        public string CheckoutDate { get; set; }
        public string NoOfRooms { get; set; }

        public string NoOfAdults { get; set; }
        public string NoOfChildren { get; set; }
        public string Nights { get; set; }

        public string ProductType { get; set; }
        public string OfferGroup { get; set; }
        public string RoomType { get; set; }

        public string Rooms { get; set; }
        public string Adults { get; set; }
        public string People { get; set; }
        public string Price { get; set; }
        public string BookingDeposit { get; set; }

        public string EmailAddress { get; set; }
        public string Location { get; set; }
        public string DepositPayableToday { get; set; }
        public string ProcessingFee { get; set; }

        public string PayableAtAccommodation { get; set; }
        public string TotalDepositPayableToday { get; set; }
        public string TotalPayableAtAccommodation { get; set; }

        public string TotalPointsStickersUsed { get; set; }
        public string TotalPrice { get; set; }
        public string TotalPayableToday { get; set; }
        public string CategoryId { get; set; }

        public string GuestName { get; set; }
        public string Email { get; set; }
        public string Discount { get; set; }
        public string Country { get; set; }
        public string AddressInfo { get; set; }
        public string SpecialRequest { get; set; }
        public string BookingThrough { get; set; }
        public string PaymentType { get; set; }
        public string ProviderType { get; set; }
        public string CreatedBy { get; set; }
        public string ChildrenAges { get; set; }


        //Added by Rajesh
        public decimal ProviderAmount { get; set; }
        public decimal Commissionamount { get; set; }


        /// <summary>
        /// Old orderline Id
        /// </summary>
        public string OldOrderLineId { get; set; }

        public Sitecore.Data.Items.Item SitecoreItem { get; set; }

        //Other information required for bedbanks
        public string BBPropertyName { get; set; }
        public string BBStarRanking { get; set; }
        public string BBImageUrl { get; set; }
        public string OldPrice { get; set; }

        public string CancellationStartDate { get; set; }
        public string CancellationEndDate { get; set; }
        public string CancellationAmount { get; set; }

        public List<BedBankCancelCriteria> CancelCriteria { get; set; }
        public List<BedBankErrata> Errata { get; set; }      
    }
}
