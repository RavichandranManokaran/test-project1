﻿CREATE TABLE [dbo].[uCommerce_Award] (
    [AwardId]        INT           IDENTITY (1, 1) NOT NULL,
    [CampaignItemId] INT           NOT NULL,
    [Name]           NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_uCommerce_Award] PRIMARY KEY CLUSTERED ([AwardId] ASC),
    CONSTRAINT [FK_uCommerce_Award_uCommerce_CampaignItem] FOREIGN KEY ([CampaignItemId]) REFERENCES [dbo].[uCommerce_CampaignItem] ([CampaignItemId])
);

