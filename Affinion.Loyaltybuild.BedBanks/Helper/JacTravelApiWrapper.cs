﻿using Affinion.LoyaltyBuild.BedBanks.General;
using Affinion.LoyaltyBuild.BedBanks.JacTravel;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Message;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Model;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Model.Room;
using Affinion.LoyaltyBuild.BedBanks.Troika;
using Affinion.LoyaltyBuild.BedBanks.Troika.Config;
using Affinion.LoyaltyBuild.BedBanks.Troika.Model;
using Affinion.LoyaltyBuild.BedBanks.Troika.Model.Coldfusion;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Linq;
using System.Data;

namespace Affinion.LoyaltyBuild.BedBanks.Helper
{
    public class JacTravelApiWrapper
    {

        public JtSearchResponse Search(BBsearchCriteria searchCriteria)
        {
            JtSearchResponse searchRes = null;
            if (searchCriteria != null)
            {
                searchRes = JtController.Search(searchCriteria.RegionId, searchCriteria.ArrivalDate, searchCriteria.Duration, searchCriteria.RoomInfo, searchCriteria.BedBankData);               
            }
            return searchRes;
        }

        public JtSearchResponse SearchOnPropRef(string pRegionId, string pArrivalDate, string pDuration, string pNoOfRooms, string pNoOfAdults, string pNoOfChildren, string pChildAge, BedBanksSettings bbs, List<string> propRefId)
        {
            string pRoomInfo = GetRoomType(pNoOfRooms, pNoOfAdults, pNoOfChildren, pChildAge);

            JtSearchResponse searchRes = JtController.SearchOnPropReferenceId(pRegionId, pArrivalDate, pDuration, pRoomInfo, bbs, propRefId);

            return searchRes;
        }

        public JtPreBookResponse PreBook(string pPropertyID, DateTime pArrivalDate, string pDuration, string pRoomToken, string pMealBasisID,string pNoOfRooms,string pNoOfAdults,string pNoOfChildren,string pChildAge, BedBanksSettings bbs)
        {
            string pRoomInfo = GetRoomType(pNoOfRooms, pNoOfAdults, pNoOfChildren, pChildAge);

            bool pIsDirect = CheckIsDirect(pRoomToken);
            
            JtPreBookResponse preBookRes = JtController.PreBook(pPropertyID, pArrivalDate.ToString("yyyy-MM-dd"), pDuration, pRoomToken, pIsDirect, pMealBasisID, pRoomInfo, bbs);

            return preBookRes;
        }

        public static string GetRoomType(string pNoOfRooms, string pNoOfAdults, string pNoOfChildren, string pChildAge)
        {
            if (int.Parse(pNoOfRooms) > 0)
            {
                int noOfRooms = 0;
                int noOfAdults = 0;
                int noOfChildren = 0;
                int.TryParse(pNoOfRooms, out noOfRooms);
                int.TryParse(pNoOfAdults, out noOfAdults);
                int.TryParse(pNoOfChildren, out noOfChildren);
                string[] childAge = !string.IsNullOrEmpty(pChildAge) ? pChildAge.Split(',').ToArray() : null;
                var roomInfo = new List<string>();
                int childCount = 0;

                if (noOfRooms > 0 && noOfAdults > 0)
                {
                    for (int roomCount = noOfRooms; roomCount > 0; roomCount--)
                    {
                        int roomAdultValue;
                        int.TryParse(Math.Ceiling(decimal.Parse((noOfAdults / roomCount).ToString())).ToString(), out roomAdultValue);
                        int roomChildValue = 0;
                        string childAges = string.Empty;

                        if (noOfChildren > 0 && childAge != null)
                        {
                            int.TryParse(Math.Ceiling(decimal.Parse((noOfChildren / roomCount).ToString())).ToString(), out roomChildValue);
                            childAges = "," + string.Join("/", childAge.Skip(childCount).Take(roomChildValue));
                            childCount += roomChildValue;
                        }
                        roomInfo.Add(string.Format("{0},{1},0{2}", roomAdultValue.ToString(), roomChildValue.ToString(), childAges));

                        noOfAdults = noOfAdults - roomAdultValue;
                        noOfChildren = noOfChildren - roomChildValue;
                    }
                }
                return string.Join("|", roomInfo);
            }
            return null;
        }

        public JtBookResponse Book(string pPropertyID, string pPreBookingToken, string pArrivalDate, string pDuration, string pTitle, string pFirstName, string pLastName, string pAddressLine1, string pPostcode, string pBookingCountryID, string pPhone, string pEmail, string pRequest, string pRoomToken, bool pIsDirect, string pMealBasisID, string pRoomInfo, string pRoomType, decimal pWholesaleAmount, int pMarginIdApplied, int lBOrderLineId, BedBanksSettings bbs)
        {
            JtBookResponse bookRes = JtController.Book(pPropertyID, pPreBookingToken, pArrivalDate, pDuration, pTitle, pFirstName, pLastName, pAddressLine1, pPostcode, pBookingCountryID, pPhone, pEmail, pRequest, pRoomToken, pIsDirect, pMealBasisID, pRoomInfo, bbs);
            return bookRes;
        }


        public JtPreCancelResponse PreCancel(string pBookingReference, BedBanksSettings bbs)
        {
            JtPreCancelResponse preCancelRes = JtController.PreCancel(pBookingReference, bbs);

            return preCancelRes;
        }

        public JtCancelResponse Cancel(string pBookingReference, double pCancellationCost, string pCancellationToken, BedBanksSettings bbs)
        {
            JtCancelResponse cancelRes = JtController.Cancel(pBookingReference, pCancellationCost, pCancellationToken, bbs);

            return cancelRes;
        }

        public PropertyInfo PropertyInfoTroikaFormat(string pPropertyId, BedBanksCurrency curr)
        {
            //Make the call to the Property Details API
            JtPropertyDetailsResponse propertyDetailsRes = JtController.GetSinglePropertyDetails(pPropertyId);

            //Get the currency from the Config File
            //Currency curr = TroikaConfigController.GetClientCurrency(pClientID, pJacCurrencyID);

            if (!ReferenceEquals(null, curr))
            {
                int curId;
                int.TryParse(curr.BedBanksCurrencyId, out curId);
                //Now construct the object to send back
                PropertyInfo propInfo = new PropertyInfo
                {
                    BaseCurrencyId = curId,
                    CampaignProviderLingualText = string.Empty,
                    Email = string.Empty,
                    Fax = propertyDetailsRes.Fax,
                    LanguageID = string.Empty,
                    Latitude = propertyDetailsRes.Latitude,
                    Longitude = propertyDetailsRes.Longitude,
                    MultiLineAddress = propertyDetailsRes.FullAddress,
                    Phone = propertyDetailsRes.Telephone,
                    PictureFileName = propertyDetailsRes.CMSBaseURL + propertyDetailsRes.MainImage,
                    ProviderId = propertyDetailsRes.PropertyId,
                    ProviderName = propertyDetailsRes.PropertyName,
                    StarRanking = "5",
                    WebInformation = propertyDetailsRes.Description,
                    ReturnStatus = propertyDetailsRes.ReturnStatus,
                    ResponseTime = propertyDetailsRes.ResponseTime,
                    PropertyReferenceID=propertyDetailsRes.PropertyReferenceId
                };

                return propInfo;
            }
            else
            {
                throw new Exception("Problem locating currency in Config File");
            }
        }        

        private static bool CheckIsDirect(string bookingToken)
        {
            int temp = 0;
            return int.TryParse(bookingToken, out temp);
        }

        //public BasketItem BasketInfoTroikaFormat(string pPropertyId, string pTotalCost, string pJacCurrencyID, string pClientID)
        //{
        //    //Get the Client from the Config file
        //    Client cl = TroikaConfigController.GetClient(pClientID);

        //    //Get the Currency from the Config file
        //    //Currency curr = TroikaConfigController.GetClientCurrency(pClientID, pJacCurrencyID);
        //    BedBanksCurrency curr = bedBankSettings.BedBanksActualCurrency;

        //    if (!ReferenceEquals(null, curr) && !ReferenceEquals(null, cl))
        //    {
        //        //Call the Property Details API
        //        JtPropertyDetailsResponse propertyDetailsRes = JtController.GetSinglePropertyDetails(pPropertyId);

        //        //USE LINQ TO GET

        //        //Construct the Basket Item from the Result
        //        BasketItem item = new BasketItem
        //        {
        //            AvailabilitySourceId = "1",
        //            BedBankProviderId = pPropertyId,
        //            ClientOfferID = cl.ClientOfferId.ToString(),
        //            ItemID = clientItem.ID.ToString(),
        //            ItemName = clientItem.Name,
        //            ItemRanking = "",
        //            ItemTypeId = "",
        //            Latitude = propertyDetailsRes.Latitude,
        //            Longitude = propertyDetailsRes.Longitude,
        //            MaxNumberofAdults ="10",
        //            MaxNumberofChildren = "10",
        //            MaxNumberTotal = "",
        //            PictureFileName = "",
        //            CountryId = "",
        //            ProviderEmail = "",
        //            ProviderLanguageId = bedBankSettings.BedBanksDefaultLocale,
        //            ProviderId = cl.ProviderId.ToString(),
        //            ProviderName = propertyDetailsRes.PropertyName,
        //            StarRanking ="",
        //            LocalRanking = "",
        //            MapLocationName = propertyDetailsRes.Resort,
        //            LocationId = new BedBankLocations().BedBanksLocationId,
        //            LocationName = propertyDetailsRes.Region,
        //            OfferId = cl.OfferId.ToString(),
        //            WebIntroduction = "",
        //            CoicrId = curr.CoicrId.ToString(),
        //            ItemPaymentLevel = "",
        //            RateId = curr.RateId.ToString(),
        //            RateTypeId = "",
        //            Priority = "",
        //            Rate = pTotalCost,
        //            CurrencyCode = curr.BedBanksCurrencyId,
        //            CurrencyName = curr.CurrencyName,
        //            RateTypeDescription = "",
        //            NumberofNights = "",
        //            ProviderTypeId = "",
        //            NumberofAdults = "10",
        //            NumberofChildren = "10",
        //            ItemExtraDescription = "",
        //            ReturnStatus = propertyDetailsRes.ReturnStatus,
        //            ResponseTime = propertyDetailsRes.ResponseTime
        //        };

        //        return item;
        //    }
        //    else
        //    {
        //        throw new Exception("Problem locating client and/or currency in Config File");
        //    }
        //}


        //public Availability SearchTroikaFormat(string pRegionId, string pArrivalDate, string pDuration, int pJacCurrencyID, string pClientID, string pRoomInfo)
        //{
        //    var extraInfo = "";

        //    Stopwatch sw = new Stopwatch();
        //    sw.Start();

        //    //Get the Currency from the Config file
        //    //Currency curr = TroikaConfigController.GetClientCurrency(pClientID, pJacCurrencyID.ToString());
        //    BedBanksCurrency curr = bedBankSettings.BedBanksActualCurrency;
        //    if (!ReferenceEquals(null, curr))
        //    {
        //        long s1 = sw.ElapsedMilliseconds;
        //        JtSearchResponse searchRes = JtController.Search(pRegionId, pArrivalDate, pDuration, pRoomInfo,pJacCurrencyID);

        //        long e1 = sw.ElapsedMilliseconds;
        //        long t1 = (e1 - s1);
        //        extraInfo += "----JT Reported Response Time: " + searchRes.ResponseTime + "|";
        //        extraInfo += "----Search Function Time: " + t1 + "|";
        //        extraInfo += searchRes.ExtraInfo;

        //        //Create lists for our three resultsets
        //        List<AvailabilityProvider> providers = new List<AvailabilityProvider>();
        //        List<AvailabilityRate> rates = new List<AvailabilityRate>();
        //        List<AvailabilityFacility> facilities = new List<AvailabilityFacility>();

        //        List<FacilityMapping> facilityMappings = TroikaDataController.GetFacilityMappings(1);

        //        int itemID = 1000;

        //        foreach (PropertyResult pr in searchRes.PropertyResults)
        //        {

        //            //Insert Provider Info to External DB
        //            int lbProviderId = TroikaDataController.InsertProviderRecord(Int32.Parse(pr.PropertyID), pr.PropertyName ?? "", pr.PropertyDetails.Latitude.ToString(), pr.PropertyDetails.Longitude.ToString(), pr.PropertyDetails.Address1 ?? "", pr.PropertyDetails.Address2 ?? "", pr.PropertyDetails.TownCity ?? "", pr.PropertyDetails.County ?? "", pr.PropertyDetails.Country ?? "", pr.PropertyDetails.Telephone ?? "", "a@a.com");

        //            //Facilities - work this out first as we need to add to the first resultset
        //            foreach (JtFacility f in pr.PropertyDetails.Facilities)
        //            {
        //                foreach (FacilityMapping fm in facilityMappings)
        //                {
        //                    if (f.FacilityID == fm.FacilityID && facilities.FindIndex(x => x.FeatureID == fm.FeatureID.ToString()) == -1)
        //                    {
        //                        //Third Resultset
        //                        AvailabilityFacility af = new AvailabilityFacility()
        //                        {
        //                            FeatureID = fm.FeatureID.ToString(),
        //                            FeatureName = fm.FeatureName,
        //                            ProviderID = pr.PropertyID
        //                        };
        //                        facilities.Add(af);
        //                    }
        //                }
        //            }

        //            DateTime intDateCalc = new DateTime(1990, 1, 1);
        //            string[] s = pArrivalDate.Split('-');
        //            DateTime dtArrivalDate = new DateTime(int.Parse(s[0]), int.Parse(s[1]), int.Parse(s[2]));
        //            int startDate = (int)(dtArrivalDate - intDateCalc).TotalDays;
        //            int endDate = startDate + int.Parse(pDuration);

        //            Tuple<decimal, int> calculateMarginPlusId = TroikaDataController.CalculateMargin(pr.PropertyID, startDate, endDate, 1);

        //            foreach (JtRoomType rt in pr.RoomTypes)
        //            {
        //                decimal commissionTotal = rt.Total * (calculateMarginPlusId.Item1 / 100);
        //                int marginIdApplied = calculateMarginPlusId.Item2;
        //                decimal fullTotal = rt.Total + commissionTotal;

        //                Tuple<decimal, decimal> roundedFigures = CurrencyController.GetRoundedValues(fullTotal, commissionTotal);

        //                //Erratum
        //                var erratumText = "";
        //                foreach (Erratum e in rt.Errata)
        //                {
        //                    erratumText += "<p><u>" + e.Subject + "</u><br/>" + e.Description + "</p>";
        //                }

        //                //First Resultset
        //                AvailabilityProvider ap = new AvailabilityProvider
        //                {
        //                    Available = 11,
        //                    AvailabilitySourceId = 1,
        //                    BaseCurrencyId = curr.BedBanksCurrencyId,
        //                    BasedOnArrivalDate = "",
        //                    CheckedOnDate = "",
        //                    ClientOfferId = 238,
        //                    ExtraDescription = rt.MealBasis,
        //                    HasFeaturedTheme = 0,
        //                    IgnoreProvider = new ColdFusionBit { Value = "0" },
        //                    InfoBoxText = "",
        //                    IsCore = 1,
        //                    IsHiddenGem = 0,
        //                    ItemDescriptor = "",
        //                    ItemId = itemID.ToString(),
        //                    ItemName = rt.RoomType,
        //                    ItemStarRanking = 0,
        //                    ItemTypeId = "-101",
        //                    Latitude = new ColdFusionVarChar { Value = pr.PropertyDetails.Latitude },
        //                    LocalRanking = 1,
        //                    LocationName = pr.PropertyDetails.Region,
        //                    LoipId = 0,
        //                    Longitude = new ColdFusionVarChar { Value = pr.PropertyDetails.Longitude },
        //                    MapLocationId = 0,
        //                    MapLocationName = pr.PropertyDetails.Resort,
        //                    MaxNumberOfAdults = int.Parse(pr.PropertyDetails.MaxAdults),
        //                    MaxNumberOfChildren = int.Parse(pr.PropertyDetails.MaxChildren),
        //                    MaxNumberTotal = 0,
        //                    NightsStay = "",
        //                    NominalHotelRateAtClientOfferToProviderLevel = 0,
        //                    NominalHotelRateAtProviderToItemLevel = 0,
        //                    NumReviews = 0,
        //                    PercentageHotelRateAtClientOfferToProviderLevel = 0,
        //                    PercentageHotelRateAtProviderToItemLevel = 0,
        //                    PictureFileName = "",
        //                    ProviderCountryId = 1131,
        //                    ProviderEmail = "",
        //                    LbProviderId = lbProviderId,
        //                    ProviderId = pr.PropertyID,
        //                    ProviderLanguageId = 0,
        //                    ProviderName = pr.PropertyName,
        //                    RatingImage = "",
        //                    Savings = 0,
        //                    StarRanking = (int)Convert.ToDouble(pr.Rating),
        //                    TimeLimitedOfferNoCancelation = 0,
        //                    Value = fullTotal.ToString(),
        //                    WebIntroduction =
        //                        string.IsNullOrEmpty(pr.PropertyDetails.Description) ? "" : pr.PropertyDetails.Description,
        //                    ErratumText = erratumText,
        //                    MealBasisId = rt.MealBasisID
        //                };

        //                //Work out if its third party bed banks stock or not
        //                if (rt.PropertyRoomTypeID == "0")
        //                {
        //                    ap.IsDirect = "false";
        //                    ap.RoomToken = rt.BookingToken;
        //                }
        //                else
        //                {
        //                    ap.IsDirect = "true";
        //                    ap.RoomToken = rt.PropertyRoomTypeID;
        //                }

        //                //work out feature listing string
        //                string featureListing = "";
        //                foreach (AvailabilityFacility af in facilities)
        //                {
        //                    if (af.ProviderID == pr.PropertyID)
        //                    {
        //                        featureListing += af.FeatureID + ",";
        //                    }
        //                }
        //                ap.FeatureListing = featureListing;

        //                providers.Add(ap);

        //                //Second Resultset
        //                AvailabilityRate ar = new AvailabilityRate
        //                {
        //                    CurrencyCodeId = curr.BedBanksCurrencyId,
        //                    CurrencyName = curr.CurrencyName,
        //                    ItemId = itemID.ToString(),
        //                    IsCore = 1,
        //                    CountryId = 1131,
        //                    ClientOfferId = 238,
        //                    RateId = pr.PropertyReferenceID,
        //                    Value = roundedFigures.Item1.ToString("#.##"),
        //                    WholesaleValue = rt.Total.ToString("#.##"),
        //                    CommissionValue = roundedFigures.Item2.ToString("#.##"),
        //                    MarginIdApplied = marginIdApplied
        //                };
        //                rates.Add(ar);

        //                itemID++;
        //            }
        //        }

        //        //STOP
        //        sw.Stop();

        //        extraInfo += "----Total after Search Call: " + (sw.ElapsedMilliseconds - e1) + "|";
        //        extraInfo += "----Total in LBAPI: " + sw.ElapsedMilliseconds + "|";
        //        extraInfo += "----Additional Time in LBAPI: " + (sw.ElapsedMilliseconds - searchRes.ResponseTime) + "|";

        //        Availability a = new Availability
        //        {
        //            AvailabilityProviders = providers,
        //            AvailabilityRates = rates,
        //            AvailabilityFacilities = facilities,
        //            ReturnStatus = searchRes.ReturnStatus,
        //            ResponseTime = searchRes.ResponseTime,
        //            ExtraInfo = extraInfo
        //        };

        //        return a;
        //    }
        //    else
        //    {
        //        throw new Exception("Problem locating currency in Config File");
        //    }
        //}
    }
}
