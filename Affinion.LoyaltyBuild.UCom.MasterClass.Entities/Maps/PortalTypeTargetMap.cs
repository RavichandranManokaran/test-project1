﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Maps
{
    public class PortalTypeTargetMap : SubclassMap<PortalTypeTarget>
    {
        // Methods
        public PortalTypeTargetMap()
        {
            base.Table("LB_PortalTypeTarget");
            base.KeyColumn("PortalTypeTargetId");
            base.Map(x => x.PortalTypeTargetId).ReadOnly();
            base.Map(x => x.PortalType).Not.Nullable();
        }

    }

}
