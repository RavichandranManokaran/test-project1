﻿using System;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration.SiteUI.Base;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Single_Item
{
    public partial class FeaturedQuotation : SitecoreSingleItemUserControl
    {
        private void Page_Load(object sender, EventArgs e)
        {
            WriteAlertsIfNeeded();
            HideIfNoVersionUnlessPageEditing();

            if (IsDataSourceItemNull)
            {
                if (IsPageEditorEditing)
                {
                    pnlQuotation.Visible = false;
                }
            }
        }
    }
}