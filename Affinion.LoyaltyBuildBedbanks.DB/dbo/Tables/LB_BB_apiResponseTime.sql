/****** Object:  Table [dbo].[LB_BB_apiResponseTime]    Script Date: 7/19/2016 7:57:21 PM ******/
CREATE TABLE [dbo].[LB_BB_apiResponseTime](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[availabilityProviderID] [int] NOT NULL,
	[method] [varchar](250) NOT NULL,
	[responsetime] [float] NOT NULL,
	[datecreated] [datetime] NOT NULL,
 CONSTRAINT [PK__ApiReponseTime] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



