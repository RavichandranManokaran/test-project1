﻿namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Helper
{
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using System.Text;
    using Affinion.LoyaltyBuild.Communications.Services;
    using Affinion.LoyaltyBuild.Common.Exceptions;

    /// <summary>
    /// Dispatch alerts from supplier portal
    /// </summary>
    public class AlertsHelper
    {
        /// <summary>
        /// Dispatch a mail alert when suppliers change offer specific information
        /// </summary>
        /// <param name="offerId">The ucommerce offer id</param>
        /// <param name="supplierId">The sitecore supplier id</param>
        /// <param name="message">the offer information which need to be saved</param>
        /// <returns>Returns null if the function executes successfully or return exception if failed</returns>
        public AffinionException DispatchOfferInfoMail(string offerId, string supplierId, string message)
        {
            AffinionException exception = null;

            try
            {
                Item adminPortalSettings = Sitecore.Context.Database.GetItemByKey("AdminPortalSiteSettings");

                if (adminPortalSettings == null)
                    throw new AffinionException("AlertsHelper : DispatchOfferInfoMail : AdminPortalSiteSettings item not found");

                Item emailItem = adminPortalSettings.Axes.GetDescendants().Where(i => i.Name == "offer-specific-information-email" && i.TemplateName == "BaseEmail").FirstOrDefault();

                if (emailItem == null)
                    throw new AffinionException("AlertsHelper : DispatchOfferInfoMail : Email item not found");

                string hostName = SitecoreFieldsHelper.GetValue(adminPortalSettings, "HostName");
                string port = SitecoreFieldsHelper.GetValue(adminPortalSettings, "Port");
                string subject = SitecoreFieldsHelper.GetValue(adminPortalSettings, "Subject");
                string body = SitecoreFieldsHelper.GetValue(adminPortalSettings, "EmailBody");
                string sender = SitecoreFieldsHelper.GetValue(adminPortalSettings, "SenderEmail");
                string receiver = SitecoreFieldsHelper.GetValue(adminPortalSettings, "ReceiverEmail");

                if (string.IsNullOrEmpty(hostName))
                    throw new AffinionException("AlertsHelper : DispatchOfferInfoMail : hostName is not defined in Admin Portal Site Settings");

                StringBuilder url = new StringBuilder();
                url.Append("http://");
                url.Append(hostName);

                if (!string.IsNullOrEmpty(port))
                {
                    url.Append(":");
                    url.Append(port);
                }

                url.Append(string.Format("/sitecore modules/Shell/Affinion/ExternalDataHandling/AddOfferData.aspx?offerid={0}&supplier={1}&message={2}&version={3}", offerId, supplierId, message, DateTime.Now.Ticks.ToString()));

                string encodedUrl = HttpUtility.UrlEncode(url.ToString());

                if (body.Contains("{{url}}"))
                {
                    body.Replace("{{url}}", encodedUrl);
                }
                else
                {
                    body += encodedUrl;
                }

                // dispatch mail
                EmailService mailService = new EmailService();
                mailService.Send(sender, receiver, subject, body, true);

                return null;
            }
            catch (AffinionException ex)
            {
                exception = ex;
                return exception;
            }
            catch(Exception ex)
            {
                exception = new AffinionException("Offer specific information change mail sent failed", ex);
                return exception;
            }
        }
    }
}