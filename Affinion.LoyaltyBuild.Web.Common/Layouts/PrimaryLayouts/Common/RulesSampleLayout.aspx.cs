///<summary>
/// � 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           RulesSampleLayout.aspx.cs
/// Sub-system/Module:     Web.Common(VS project name)
/// Description:           Primary layout for rules pages
/// </summary>

namespace Affinion.LoyaltyBuild.Web.Common.Layouts.PrimaryLayouts.Common
{
    using System;
    using System.Web;
    using System.Web.UI;

    public partial class RulesSampleLayout : Page
    {
       
    }
}
