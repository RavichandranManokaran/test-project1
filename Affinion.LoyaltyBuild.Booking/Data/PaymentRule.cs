﻿using System;

namespace Affinion.LoyaltyBuild.Api.Booking.Data
{
    /// <summary>
    /// Payment Rule Model
    /// </summary>
    class PaymentRule
    {
        /// <summary>
        /// Provider Type
        /// </summary>
        public String ProviderType { get; set; }
        /// <summary>
        /// Portal
        /// </summary>
        public string Portal { get; set; }
        /// <summary>
        /// Payment Type
        /// </summary>
        public string PaymentType { get; set; }
        /// <summary>
        /// Payment Method
        /// </summary>
        public string PaymentMethod { get; set; }
        /// <summary>
        /// Check OfferOverrides
        /// </summary>
        public bool OfferOverrides { get; set; }
    }
}
