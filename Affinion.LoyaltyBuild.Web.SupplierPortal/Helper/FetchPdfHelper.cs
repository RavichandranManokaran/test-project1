﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           FetchPdfHelper.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Utilities
/// Description:           Configuration Helper Class to fetch Pdf from sitecore
/// </summary>

#region Using Directives
using Sitecore;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
#endregion

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Helper
{
    /// <summary>
    /// Fetch Pdf from sitecore
    /// </summary>
    public class FetchPdfHelper
    {
        /// <summary>
        ///get file url
        /// </summary>
        public static string GetFileUrl(Item item, string fieldName)
        {
            String mediaUrl = "";
            FileField field = item.Fields[fieldName];
            if ((field != null) && (field.MediaItem != null))
            {
                mediaUrl = MediaManager.GetMediaUrl(field.MediaItem);
                mediaUrl = StringUtil.EnsurePrefix('/', mediaUrl);
            }
            return mediaUrl;
        }
    }
}