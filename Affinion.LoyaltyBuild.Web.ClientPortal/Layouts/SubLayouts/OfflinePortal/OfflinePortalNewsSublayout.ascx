﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalNewsSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalNewsSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Sitecore.Data.Items" %>

<script type="text/javascript">
    $(document).on('click', '.panel-heading span.clickable', function (e) {
        var $this = $(this);
        if (!$this.hasClass('panel-collapsed')) {
            $this.parents('.panel').find('.panel-body').slideUp();
            $this.addClass('panel-collapsed');
            $this.find('i').removeClass('glyphicon-minus').addClass('glyphicon-plus');
        } else {
            $this.parents('.panel').find('.panel-body').slideDown();
            $this.removeClass('panel-collapsed');
            $this.find('i').removeClass('glyphicon-plus').addClass('glyphicon-minus');
        }
    });
    $(document).on('click', '.panel div.clickable', function (e) {
        var $this = $(this);
        if (!$this.hasClass('panel-collapsed')) {
            $this.parents('.panel').find('.panel-body').slideUp();
            $this.addClass('panel-collapsed');
            $this.find('i').removeClass('glyphicon-minus').addClass('glyphicon-plus');
        } else {
            $this.parents('.panel').find('.panel-body').slideDown();
            $this.removeClass('panel-collapsed');
            $this.find('i').removeClass('glyphicon-plus').addClass('glyphicon-minus');
        }
    });
    $(document).ready(function () {
        $('.panel-heading span.clickable').click();
        $('.panel div.clickable').click();
    });

</script>


<div class="column">
    <div class="news-ticker">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><asp:Label id="NewsTitle" runat="server" /></h3>
                <span class="pull-right clickable panel-collapsed"><i class="glyphicon glyphicon-minus"></i></span>
            </div>

            <div id="Errormessage" class="alert alert-danger" runat="server" visible="false" align="center">
                <b>
                    <asp:Literal ID="literalErrorMessage" runat="server" Text="No News Found" />
                </b>
            </div>
            <asp:Repeater ID="News" runat="server">

                <HeaderTemplate>
                    <div class="panel-body">
                        <ul id="ticker1" class="no-padding">
                </HeaderTemplate>
                <ItemTemplate>
                    <li class="display-block-val">
                        <div class="content_bg_inner_box alert-info">
                            <h4>
                                <sc:text field="Title" item="<%#(Item)Container.DataItem %>" runat="server" />
                            </h4>
                            <span class="display-block-val news-feed-text">
                                <sc:text field="Description" item="<%#(Item)Container.DataItem %>" runat="server" />
                            </span>
                            <span class="days">
                                <sc:text field="__Updated" item="<%#(Item)Container.DataItem %>" runat="server" />
                            </span>
                        </div>

                    </li>
                </ItemTemplate>
                <FooterTemplate>
                    </ul>    
                            </div>
                </FooterTemplate>

            </asp:Repeater>
        </div>

    </div>
</div>



