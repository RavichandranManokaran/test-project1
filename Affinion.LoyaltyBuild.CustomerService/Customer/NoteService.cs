﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/
namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Customer
{
    /// <summary>
    /// Service that provides methods to perform Customer Notes related actions
    /// </summary>
    public class NoteService : INoteService
    {
        //private variables     
        private string _connectionString = "uCommerce";

        /// <summary>
        /// constructor
        /// </summary>
        public NoteService()
        {  }


        #region INoteService Implementation

        /// <summary>
        /// To Save Customer Notes
        /// </summary>
        /// <param name="reservationNotes">Booking Notes</param>
        /// <returns> True or False  </returns>
        public ValidationResponse SaveReservationNote(BookingNotes bookingNotes)
        {
          
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "LB_ManageReservationNotes";
            var result = new ValidationResponse();
            try
            { 
            using (var command = db.GetStoredProcCommand(sql))
            {
                db.AddInParameter(command, "@OrderLineId",  DbType.Int32, bookingNotes.OrderLineId);
                db.AddInParameter(command, "@NoteTypeId",   DbType.Int32, bookingNotes.TypeId);
                db.AddInParameter(command, "@Note",         DbType.String, bookingNotes.Note);
                db.AddInParameter(command, "@CreatedBy",    DbType.String, bookingNotes.CreatedBy);
                db.ExecuteNonQuery(command);
                result.IsSuccess = true;
                result.Message = "Notes saved successfully"; 
               
            }
                }
            catch
            {
                result.IsSuccess = false;
                result.Message = "Error occurred while saving Notes";

            }
            return result;
        }

        /// <summary>
        /// To get the Customer Notes
        /// </summary>
        /// <param name="OrderLineID">OrderLineID</param>
        /// <returns>BookingNotes object List  </returns>
        public List<BookingNotes> GetReservationNotes(int orderLineID)
        {
            List<BookingNotes> bookingNotes = new List<BookingNotes>();
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);

            var sql = "LB_GetReservationNotes";
            using (var command = db.GetStoredProcCommand(sql))
            {
                db.AddInParameter(command, "@OrderLineId", DbType.Int32, orderLineID);
                using (var reader = db.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        var bookingNote = new BookingNotes{
                        Id = Convert.ToInt32(reader["NotesId"]),
                        TypeId = Convert.ToInt32(reader["NoteTypeId"]),
                        NoteType = Convert.ToString(reader["Description"]),
                        Note = Convert.ToString(reader["Note"]),
                        OrderLineId = Convert.ToInt32(reader["OrderLineId"]),
                        CreatedBy = Convert.ToString(reader["CreatedBy"]),
                        CreatedDate = Convert.ToDateTime(reader["CreatedDate"])
                        };
                        bookingNotes.Add(bookingNote);
                    }
                }
            }
            return bookingNotes;
        }

        #endregion
    }
}
