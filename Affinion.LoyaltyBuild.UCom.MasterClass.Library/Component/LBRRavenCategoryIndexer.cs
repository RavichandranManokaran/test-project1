﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using Documents = UCommerce.Documents;
using UCommerce.Search;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Component
{
    public class LBRRavenCategoryIndexer : IIndexer<Category>, IDeindexer<Category>, IIndexer<IEnumerable<Category>>, IDeindexer<IEnumerable<Category>>
    {
        // Fields
        private readonly IIndexer<IEnumerable<ProductCatalog>> _productCatalogIndexer;

        // Methods
        public LBRRavenCategoryIndexer(IIndexer<IEnumerable<ProductCatalog>> productCatalogIndexer)
        {
            this._productCatalogIndexer = productCatalogIndexer;
        }

        public void Deindex(Category subject)
        {
            this._productCatalogIndexer.Index(new List<ProductCatalog> { subject.ProductCatalog });
        }

        public void Deindex(IEnumerable<Category> subjects)
        {
            this._productCatalogIndexer.Index((from x in subjects select x.ProductCatalog).Distinct<ProductCatalog>().ToList<ProductCatalog>());
        }

        public void Index(IEnumerable<Category> subjects)
        {
            this._productCatalogIndexer.Index((from x in subjects select x.ProductCatalog).Distinct<ProductCatalog>().ToList<ProductCatalog>());
        }

        public void Index(Category subjects)
        {
            this._productCatalogIndexer.Index(new List<ProductCatalog> { subjects.ProductCatalog });
        }
    }

}