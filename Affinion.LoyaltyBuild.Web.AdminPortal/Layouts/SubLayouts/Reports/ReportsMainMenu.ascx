﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportsMainMenu.ascx.cs" Inherits="Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.Reports.ReportsMainMenu" %>
<%--<div class=""> 
<asp:Repeater ID="Mainmenu" runat="server" >
     <ItemTemplate>
        
         
     </ItemTemplate>
     
 </asp:Repeater>
</div>--%>
<div class="navbar-collapse collapse in ty-navbar ">
<ul class="nav navbar-nav ty-taylor-nav2 ty-nav-back" style="list-style-position:inside">
     <asp:Repeater ID="Mainmenu" runat="server" OnItemDataBound="MainmenuItemDataBound">
         <HeaderTemplate>Reports </HeaderTemplate>
      <ItemTemplate >
     <%--  <li id="MenuLi" runat="server" >
            <a  href="<%# Eval("Link") %>" class=""><%# Eval("Text") %></a>
       </li>--%>
          <li id="MenuLi" runat="server">
                            <asp:HyperLink ID="MenuLink" runat="server">
                                <asp:Literal ID="MenuText" runat="server" /></asp:HyperLink><asp:PlaceHolder ID="phSubMenu" runat="server" />
          </li>
      </ItemTemplate>
     </asp:Repeater>
    </ul>

    </div>