﻿using Affinion.LoyaltyBuild.UCom.Common.Constants;
using Affinion.LoyaltyBuild.UCom.Common.Extension;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Availability;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Component;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce;
using UCommerce.Api;
using UCommerce.Catalog;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Marketing;
using UCommerce.Marketing.Awards.AwardResolvers;
using UCommerce.Marketing.TargetingContextAggregators;
using UCommerce.Marketing.Targets.TargetResolvers;
using UCommerce.Runtime;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi
{

    public class ProductDiscountDetail
    {
        public ProductDiscountDetail()
        {
            this.CampaignDetail = new List<ProductCampaignDetail>();
        }
        public string ProductSku { get; set; }

        public List<ProductCampaignDetail> CampaignDetail { get; set; }
    }
    public class ProductCampaignDetail
    {

        public int CampaignItemId { get; set; }

        public string CampaignName { get; set; }

        public decimal TotalPrice { get; set; }

        public int OfferAvailability { get; set; }

        public List<Discount> DiscountPrice { get; set; }
    }

    public class LBPriceCalculation : PriceCalculation
    {
        private SearchCriteria searchCriteria;
        public ProductDiscountDetail ApplicableItem;
        public LBPriceCalculation(Product product, SearchCriteria criteria, ProductCatalog catalog)
            : this(ObjectFactory.Instance.Resolve<IDiscountService>(), ObjectFactory.Instance.Resolve<IPricingService>(), ObjectFactory.Instance.Resolve<ITaxService>(), ObjectFactory.Instance.Resolve<IOrderContext>(), ObjectFactory.Instance.Resolve<ICatalogContext>(), product, criteria, catalog)
        {
            this.searchCriteria = criteria;
        }

        public LBPriceCalculation(IDiscountService discountService, IPricingService pricingService, ITaxService taxService, IOrderContext orderContext, ICatalogContext catalogContext, Product product, SearchCriteria criteria, ProductCatalog catalog)
            : this(discountService, pricingService, taxService, orderContext, catalogContext, product, criteria != null && criteria.PriceGroup != null ? criteria.PriceGroup : (catalog != null) ? catalog.PriceGroup : catalogContext.CurrentPriceGroup, (catalog != null) ? catalog.ShowPricesIncludingVAT : catalogContext.CurrentCatalog.ShowPricesIncludingVAT, criteria)
        {
            this.searchCriteria = criteria;
        }
        public LBPriceCalculation(IDiscountService discountService,
            IPricingService pricingService, ITaxService taxService,
            IOrderContext orderContext, ICatalogContext catalogContext,
            Product product, PriceGroup priceGroup, bool includeTaxInAmount, SearchCriteria criteria)
            : base(discountService, pricingService, taxService, orderContext, catalogContext, product, priceGroup, includeTaxInAmount)
        {
            this.searchCriteria = criteria;

            Money productPrice = pricingService.GetProductPrice(product, priceGroup);
            if ((productPrice != null) && (priceGroup != null))
            {
                Money money2 = taxService.CalculateTax(product, priceGroup, productPrice);
                Currency currency = productPrice.Currency ?? Currency.All().First<Currency>();
                Money money3 = new Money((productPrice.Value * searchCriteria.Night), currency);
                Price price = new Price
                {
                    AmountExclTax = money3,
                    AmountInclTax = money3 + money2
                };
                this.ListPrice = price;
                this.ListPrice.Amount = includeTaxInAmount ? this.ListPrice.AmountInclTax : this.ListPrice.AmountExclTax;
                Money unitPrice = new Money(this.GetDiscount(catalogContext, discountService, pricingService, taxService, priceGroup, product), currency);
                ProductDiscountDetail cItemList = GetApplicableCampaign(catalogContext, discountService, pricingService, taxService, priceGroup, product, searchCriteria);
                this.ApplicableItem = cItemList;
                Money money5 = taxService.CalculateTax(product, priceGroup, unitPrice);
                this.Discount = new Price();
                this.Discount.AmountExclTax = unitPrice;
                this.Discount.AmountInclTax = unitPrice + money5;
                this.Discount.Amount = includeTaxInAmount ? this.Discount.AmountInclTax : this.Discount.AmountExclTax;
                Money money6 = money3 - unitPrice;
                Money money7 = taxService.CalculateTax(product, priceGroup, money6);
                this.YourPrice = new Price();
                this.YourPrice.AmountExclTax = money6;
                this.YourPrice.AmountInclTax = money6 + money7;
                this.YourPrice.Amount = includeTaxInAmount ? this.YourPrice.AmountInclTax : this.YourPrice.AmountExclTax;
                this.YourTax = money7;
            }

        }

        private decimal GetDiscount(ICatalogContext catalogContext, IDiscountService discountService, IPricingService pricingService, ITaxService taxService, PriceGroup priceGroup, Product product)
        {
            return 0;
            //Guard.Against.Null<PriceGroup>(priceGroup, "Supplied price group cannot be null.");
            //Guard.Against.Null<Currency>(priceGroup.Currency, "Price group {0} has no currency assigned.".FormatWith(new object[] { priceGroup.Name }));
            //PurchaseOrder purchaseOrder = new PurchaseOrder
            //{
            //    BillingCurrency = priceGroup.Currency,
            //    ProductCatalogGroup = catalogContext.CurrentCatalogGroup
            //};

            //List<OrderProperty> orderProperties = searchCriteria.CreateOrderLineProperty();
            //Money productPrice = pricingService.GetProductPrice(product, priceGroup);
            //OrderLine orderLine = new OrderLine
            //{
            //    Sku = product.Sku,
            //    VariantSku = product.VariantSku,
            //    Price = productPrice.Value,
            //    VAT = taxService.CalculateTax(priceGroup, productPrice).Value,
            //    VATRate = priceGroup.VATRate,
            //    Quantity = 1,
            //    OrderProperties = orderProperties
            //};
            //purchaseOrder.AddOrderLine(orderLine);
            //discountService.ApplyAwards(purchaseOrder);
            
            //return orderLine.Discounts.Sum<Discount>(((Func<Discount, decimal>)(x => x.AmountOffTotal)));
        }

        public ProductDiscountDetail GetApplicableCampaign(ICatalogContext catalogContext, IDiscountService discountService, IPricingService pricingService, ITaxService taxService, PriceGroup priceGroup, Product product, SearchCriteria searchCriteria)
        {


            string suppliierId = string.Empty;
            Category productCategory = product.GetCategories().FirstOrDefault();
            if (productCategory == null)
            {
                return null;
            }
            suppliierId = productCategory.Guid.ToString();
            PurchaseOrder purchaseOrder = new PurchaseOrder
            {
                BillingCurrency = priceGroup.Currency,
                ProductCatalogGroup = catalogContext.CurrentCatalogGroup
            };


            ProductDiscountDetail productDetail = new ProductDiscountDetail();

            List<Product> addonProduct = ProductLibrary.GetAddonProudct(productCategory.Id);

            LBMarketingService service = new LBMarketingService(ObjectFactory.Instance.Resolve<IRepository<CampaignItem>>(), ObjectFactory.Instance.Resolve<ITargetingContextAggregator>(), ObjectFactory.Instance.Resolve<ITargetAggregator>(), ObjectFactory.Instance.Resolve<IAwardAggregator>());
            DateTime startDate = searchCriteria.ArrivalDate;
            DateTime endDate = searchCriteria.ArrivalDate.ConvertDateToString().ParseFormatDateTime().AddDays(searchCriteria.Night).AddSeconds(-1);
            IList<CampaignItem> activeCampaignItems = service.GetActiveCampaignItemsByDate(startDate, endDate);

            //Get Supplier to filter Active campaign on supplier

            productDetail.CampaignDetail = new List<ProductCampaignDetail>();

            if (purchaseOrder.ProductCatalogGroup != null)
            {
                activeCampaignItems = activeCampaignItems.Where<CampaignItem>(delegate(CampaignItem x)
                {
                    if (!x.Campaign.ProductCatalogGroups.Contains(purchaseOrder.ProductCatalogGroup))
                    {
                        return !x.Campaign.ProductCatalogGroups.Any<ProductCatalogGroup>();
                    }
                    return true;
                }).ToList<CampaignItem>();
            }
            List<SupplierInvite> supplierInviteForCampaign = SupplierInvite.All().Where(x => x.Sku == suppliierId && x.IsSubscribe).ToList();
            IRoomAvailabilityService roomAvailabityService = new RoomAvailabilityService();
            foreach (CampaignItem item in activeCampaignItems)
            {
                if (!supplierInviteForCampaign.Any(x => x.CampaignItem == item.CampaignItemId))
                {
                    continue;
                }
                
                purchaseOrder.OrderLines.Clear();
                //new List<OrderLine>();
                purchaseOrder.AddOrderLine(AddProductToOrderLineInMemory(pricingService, taxService, priceGroup, product, searchCriteria));
                while (purchaseOrder.Discounts.Count > 0)
                {
                    Discount discount = purchaseOrder.Discounts.First<Discount>();
                    purchaseOrder.RemoveDiscount(discount);
                }

                AddOnTarget targets = null; // AddOnTarget.All().Where(x => x.CampaignItem.CampaignItemId == item.Id).FirstOrDefault();
                if (targets != null)
                {
                    int requiredAddOnProduct = targets.AddOn.Split('|').Length;
                    int existProductCount = 0;
                    foreach (var addontarget in targets.AddOn.Split('|'))
                    {
                        foreach (var addon in addonProduct)
                        {
                            List<ProductProperty> productProperty = addon.ProductProperties.Where(x => !x.ProductDefinitionField.Deleted && x.Value.Equals(addontarget, StringComparison.CurrentCultureIgnoreCase) && x.ProductDefinitionField.Name.Equals("ProductType", StringComparison.InvariantCultureIgnoreCase)).ToList();
                            if (productProperty != null && productProperty.Any() && addon.PriceGroupPrices != null && addon.PriceGroupPrices.Any())
                            {
                                purchaseOrder.AddOrderLine(AddProductToOrderLineInMemory(pricingService, taxService, priceGroup, addon, searchCriteria, true));
                                existProductCount++;
                            }
                        }
                    }
                    if (requiredAddOnProduct != existProductCount)
                        continue;
                }
                if (service.Apply(purchaseOrder, item))
                {
                    List<Discount> discoutList = new List<Discount>();
                    decimal discountOrderLine = 0;
                    foreach (var orderLines in purchaseOrder.OrderLines)
                    {
                        if (orderLines.Discounts != null && orderLines.Discounts.Any())
                        {
                            discountOrderLine = orderLines.Discounts.Sum(x => x.AmountOffTotal);
                            discoutList.AddRange(orderLines.Discounts);
                        }
                    }
                    List<OfferAvailability> offerAvailability = roomAvailabityService.FindOfferAvailability(suppliierId, product.Sku, searchCriteria.ArrivalDate, searchCriteria.ArrivalDate.AddDays(searchCriteria.Night - 1), item.CampaignItemId);
                    int offerAvailabilityCount = FindOfferAvailbility(offerAvailability, searchCriteria.ArrivalDate.ConvetDatetoDateCounter(), searchCriteria.ArrivalDate.AddDays(searchCriteria.Night - 1).ConvetDatetoDateCounter(), item.CampaignItemId);
                    decimal totalPrice = (purchaseOrder.OrderLines.Sum(x => x.Price * x.Quantity) - discountOrderLine);
                    productDetail.CampaignDetail.Add(new ProductCampaignDetail() { CampaignItemId = item.CampaignItemId, CampaignName = item.Name, DiscountPrice = discoutList, TotalPrice = totalPrice, OfferAvailability = offerAvailabilityCount });

                }
            }
//            purchaseOrder = null;
            return productDetail;
        }

        public int FindOfferAvailbility(List<OfferAvailability> offerAvailaiblity, int startDateCounter, int endDateCounter, int campaignItemId)
        {
            int offerRoomavailability = 0;
            for (int i = startDateCounter; i <= (endDateCounter); i++)
            {
                OfferAvailability availability = offerAvailaiblity.FirstOrDefault(x => x.AvailabilityDate.ConvetDatetoDateCounter() == (i) && x.Id == campaignItemId);
                if (availability != null)
                {
                    if (i == startDateCounter)
                        offerRoomavailability = availability.OfferAvailableRoom;
                    if (availability.OfferAvailableRoom <= offerRoomavailability)
                    {
                        offerRoomavailability = availability.OfferAvailableRoom;
                    }
                }
                else
                    return 0;
            }
            return offerRoomavailability;
        }

        private OrderLine AddProductToOrderLineInMemory(IPricingService pricingService, ITaxService taxService, PriceGroup priceGroup, Product product, SearchCriteria searchCriteria, bool addOnProduct = false)
        {
            List<OrderProperty> orderProperties = searchCriteria.CreateOrderLineProperty();
            if (addOnProduct)
            {
                orderProperties.Add(new OrderProperty() { Key = OrderPropertyConstants.IgnorInSearch, Value = "1" });
            }
            orderProperties.Add(new OrderProperty() { Key = OrderPropertyConstants.ForSearch, Value = "1" });
            Money productPrice = pricingService.GetProductPrice(product, priceGroup);
            OrderLine orderLine = new OrderLine
            {
                Sku = product.Sku,
                VariantSku = product.VariantSku,
                Price = productPrice.Value,
                VAT = taxService.CalculateTax(priceGroup, productPrice).Value,
                VATRate = priceGroup.VATRate,
                Quantity = searchCriteria.Night,
                OrderProperties = orderProperties
            };
            return orderLine;

        }



    }
}
