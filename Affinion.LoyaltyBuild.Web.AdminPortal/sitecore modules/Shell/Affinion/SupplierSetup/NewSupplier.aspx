﻿<%@ Page Title="" Language="C#" MasterPageFile="~/sitecore modules/Shell/Affinion/SupplierSetup/SupplierSetup.Master" AutoEventWireup="true" CodeBehind="NewSupplier.aspx.cs" Inherits="Affinion.LoyaltyBuild.AdminPortal.sitecore_modules.Shell.Affinion.SupplierSetup.NewSupplier" %>

<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ OutputCache Location="None" VaryByParam="none" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container-outer width-full overflow-hidden-att app-bg-cl">
        <div class="container-outer-inner width-full">
            <div class="container">
                <div class="col-md-6 sup-login-layout-outer">
                    <div class="sup-login-layout">
                        <div class="sup-login-layout-inner">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="login-header">
                                        <img src="/Resources/images/LBLogo-home.png" alt="logo" title="" class="img-responsive" />
                                    </div>
                                </div>
                            </div>

                                <div class="col-md-12">
                                    <div class="input-set">
                                        <div class="col-md-12">
                                            <h2 class="text-center">Supplier Setup</h2>
                                        </div>
                                        <div class="col-md-12">
                                            <p>Supplier Name</p>
                                            <asp:TextBox ID="TextBoxSupplierName" placeholder="Supplier Name" runat="server" class="form-control" autocomplete="off" required=""></asp:TextBox>
                                        </div>
                                        <div class="col-md-12">
                                            <p>Supplier Type</p>
                                            <asp:DropDownList ID="DropDownListSuppliertype" CssClass="form-control" runat="server"></asp:DropDownList>
                                        </div>
                                        <div class="col-md-12">
                                            <p>Supplier super group</p>
                                            <asp:DropDownList ID="DropDownListSupplierSuperGroup" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="DropDownListSupplierSuperGroupSelectedIndexChanged"></asp:DropDownList>
                                        </div>
                                        <div class="col-md-12">
                                            <p>Supplier group</p>
                                            <asp:DropDownList ID="DropDownListSupplierGroup" CssClass="form-control" runat="server"></asp:DropDownList>
                                        </div>
                                        <%--<div class="col-md-12">
                                            <p>Default language</p>
                                            <asp:DropDownList ID="DropDownListLanguage" CssClass="form-control" runat="server"></asp:DropDownList>
                                        </div>--%>
                                        <div class="col-md-12">
                                            <p id="LabelValidation" style="color:red;font:bold" runat="server"></p>
                                        </div>
                                        <div>
                                            <div class="sup-btn-top">
                                                <div class="col-md-12">
                                                    <asp:Button ID="ButtonCreate" runat="server" Text="Create" OnClick="ButtonCreateClick" class="sup-btn" />
                                                </div>
                                                <div class="col-md-12">
                                                    <input type="reset" value="Cancel" class="sup-btn" onclick="redirectToLaunchpad()" />
                                                </div>
                                                <p class="bis-txt-center"></p>
                                                <a class="bis-back" href="#"></a>
                                            </div>
                                        </div>                                        
                                    </div>
                                </div>
  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function redirectToLaunchpad() {
            window.location.replace("/sitecore/shell/sitecore/client/Applications/Launchpad/");
        }
    </script>
</asp:Content>
