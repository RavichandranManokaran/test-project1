﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.AdminPortal.sitecore_modules.Shell.Affinion.SupplierInformationUdate
{
    public partial class SupplierInformationAcceptance : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // redirect the user to login page if not logged in
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Response.Redirect("/sitecore/login?returnUrl=" + HttpContext.Current.Request.Path);
            }
        }
    }
}