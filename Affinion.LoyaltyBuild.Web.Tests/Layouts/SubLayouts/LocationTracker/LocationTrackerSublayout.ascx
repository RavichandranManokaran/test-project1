﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LocationTrackerSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.Tests.Layouts.SubLayouts.LocationTracker.LocationTrackerSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<style>
    #map-canvas {
        width: 800px;
        height: 800px;
    }
</style>
<asp:ScriptManager ID="LocationTrackerScriptManager" runat="server">
    <Services>
        <asp:ServiceReference Path="http://AffinionTesting/WebServices/LocationTrackerWebService.asmx" />
    </Services>
</asp:ScriptManager>

<script type="text/javascript">
    // This function calls the Web Service method. 


    function GetGeoLocationDetails(location) {

        //Get the selected location
        var locationDropDownList = document.getElementById('<%=LocationsDropdownList.ClientID%>');
        var selectedValue = locationDropDownList.options[locationDropDownList.selectedIndex].value;

        Affinion.LoyaltyBuild.Web.Tests.WebServices.LocationTrackerWebService.GetSelectedLocation(selectedValue, SucceededCallback, ErrorCallBack);

    }

    // This is the callback function that
    // processes the Web Service return value.
    function SucceededCallback(result) {

        //Get the json object
        var arr_from_json = JSON.parse(result);                

        //Get the latitude of the selected location
        var latitude = arr_from_json[0].Latitude;

        //Get the longtitude of the selected location
        var longitude = arr_from_json[0].Longitude;
               

        initialize(arr_from_json, latitude, longitude);
    }

    function ErrorCallBack(result) {
        alert(result.get_message());
    }

</script>

<script src="https://maps.googleapis.com/maps/api/js"></script>
<script>
    function initialize(sublocations, lat, lon) {
        var mapCanvas = document.getElementById('map-canvas');
        var location = new google.maps.LatLng(lat, lon);
        var mapOptions = {
            center: location,
            zoom: 12,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(mapCanvas, mapOptions);
        setMarkers(map, sublocations);
    }

    /*
    Set Markers to sub locations
    */
    function setMarkers(map, sub_loc_arr)
    {

        var shape = {
            coords: [1, 1, 1, 20, 18, 20, 18, 1],
            type: 'poly'
        };


        for (var i = 1; i < sub_loc_arr.length; i++) 
        {
            var sub_loc = sub_loc_arr[i];
            var subLatLng = new google.maps.LatLng(sub_loc.Latitude, sub_loc.Longitude);
                

            /*Uncomment to add a customized image as the map icon*/
            //var image = {
            //    url: 'Maplocation.png',
            //    size: new google.maps.Size(20, 32),
            //    // The origin for this image is 0,0.
            //    origin: new google.maps.Point(0, 0),
            //    // The anchor for this image is the base of the flagpole at 0,32.
            //    anchor: new google.maps.Point(0, 32)
            //};

            
            var marker = new google.maps.Marker({
                map: map,
                //icon:image,/*un-comment to add an image as the map icon*/
                position: subLatLng,
                shape: shape,
                title: sub_loc.LocationName
            });
		   
            var contentString = '<div id="content">' +
     '<div id="siteNotice">' +
     '</div>' +
     '<h1 id="firstHeading" class="firstHeading">' + sub_loc.Description + '</h1>' +
     '<div id="bodyContent">' +
     '<p><b>Uluru</b></p>' + sub_loc.Description +
     '<p>Attribution: Uluru, <a href="https://en.wikipedia.org/w/index.php?title=Uluru&oldid=297882194">' +
     'https://en.wikipedia.org/w/index.php?title=Uluru</a> ' +
     '(last visited June 22, 2009).</p>' +
     '</div>' +
     '</div>';
           

            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });

            //Call listen Marker function
            listenMarker(map, infowindow, marker);
		
            


        }
    
    }

    //Get the info window load
    function listenMarker(map, infowindow, marker) {
        // so marker is associated with the closure created for the listenMarker function call
        google.maps.event.addListener(marker, 'click', function () {
            infowindow.open(map, marker);
        });
    }
          
    
    google.maps.event.addDomListener(window, 'load', initialize);
</script>

<%--Calender --%>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<script>
    $(function () {
        $("#ArrivalDateID").datepicker();
    });

    $(function () {
        $("#DepartureDateID").datepicker();
    });
</script>




<table>
    <tr>
        <td>
            <h3>
                <sc:text id="SelectLocationText" runat="server" field="SelectLocation" />
            </h3>
        </td>
        <td>
            <asp:DropDownList ID="LocationsDropdownList" runat="server"></asp:DropDownList>
        </td>
        <td>
            <h3>
                <sc:text id="ArrivalDateText" runat="server" field="ArrivalDate" />
            </h3>
        </td>
        <td>
            <input type="text" id="ArrivalDateID" />
        </td>
        <td>
            <h3>
                <sc:text id="DepartureDateText" runat="server" field="DepartureDate" />
            </h3>
        </td>
        <td>
            <input type="text" id="DepartureDateID" />
        </td>
        <td>
            <asp:Button runat="server" Text="Get the Location" OnClientClick="GetGeoLocationDetails();return false;" />
        </td>        
    </tr>
    <tr>
        <td>
            <asp:Label ID="ErrorLabel" runat="server" Text=""></asp:Label>
        </td>
    </tr>
</table>
<br />
<table>
    <tr>
        <td>
            <div id="map-canvas"></div>
        </td>
    </tr>
</table>
