﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.SupplierPortal.Reports.Data
{

    /// <summary>
    /// Contains details for the arrival booking transferred out report item
    /// </summary>
   public class ArrivalBookingTransferredIn:ArrivalBase
    { 
   
   }
 /// <summary>
    /// Collection to manage Arrival booking transferred out
    /// </summary>
   public class ArrivalBookingTransferredInCollection : IEnumerable<ArrivalBookingTransferredIn>, IList<ArrivalBookingTransferredIn>
    {
        //Private variables declaration
        private List<ArrivalBookingTransferredIn> _items;

        /// <summary>
        /// Constructor
        /// </summary>
        public ArrivalBookingTransferredInCollection()
        {
            _items = new List<ArrivalBookingTransferredIn>();
        }

        /// <summary>
        /// Add method to add item
        /// </summary>
        /// <param name="item"></param>
        public void Add(ArrivalBookingTransferredIn item)
        {
            _items.Add(item);
        }

        /// <summary>
        /// Override method IEnumenrable class
        /// </summary>
        /// <returns></returns>
        public IEnumerator<ArrivalBookingTransferredIn> GetEnumerator()
        {
            foreach (var item in _items)
                yield return item;
        }

        /// <summary>
        /// Override method IEnumenrable class
        /// </summary>
        /// <returns></returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            foreach (var item in _items)
                yield return item;
        }


        public int IndexOf(ArrivalBookingTransferredIn item)
        {
            throw new NotImplementedException();
        }

        public void Insert(int index, ArrivalBookingTransferredIn item)
        {
            throw new NotImplementedException();
        }

        public void RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        public ArrivalBookingTransferredIn this[int index]
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }


        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(ArrivalBookingTransferredIn item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(ArrivalBookingTransferredIn[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public int Count
        {
            get { return _items.Count; }
        }

        public bool IsReadOnly
        {
            get { throw new NotImplementedException(); }
        }

        public bool Remove(ArrivalBookingTransferredIn item)
        {
            throw new NotImplementedException();
        }
    }
}



