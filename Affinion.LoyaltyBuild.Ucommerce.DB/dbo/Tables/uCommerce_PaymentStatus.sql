﻿CREATE TABLE [dbo].[uCommerce_PaymentStatus] (
    [PaymentStatusId] INT           NOT NULL,
    [Name]            NVARCHAR (50) NOT NULL,
    CONSTRAINT [uCommerce_PK_PaymentStatus] PRIMARY KEY CLUSTERED ([PaymentStatusId] ASC)
);

