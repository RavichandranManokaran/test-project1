﻿CREATE TABLE [dbo].[LB_OrderLineDue] (
    [OrderLineDueId]             INT NOT NULL IDENTITY,
    [OrderLineID]                INT           NOT NULL,
    [BookingId]				UNIQUEIDENTIFIER   NOT NULL,
    [DueTypeId]                  UNIQUEIDENTIFIER           NOT NULL,
    [CurrencyTypeId]             INT           NOT NULL,
    [PaymentMethodId]            INT           NULL,
    [TotalAmount]                     MONEY         NOT NULL,
    [CommissionAmount]           MONEY         NOT NULL,
    [ProviderAmount]             MONEY         NOT NULL,
    [ProcessingFeeAmount] MONEY         NOT NULL,
    [VatAmount] MONEY         NOT NULL,
    [Note]                       VARCHAR (MAX) NOT NULL,
    [IsDeleted]                  BIT           NULL,
    [CreatedBy]                  NVARCHAR (50)  NULL,
    [CreatedDate]                DATETIME      NULL,
    [UpdatedBy]                  NVARCHAR (50)  NULL,
    [UpdatedDate]                DATETIME      NULL,
    [ParentOrderLineDueId] INT NULL, 
    PRIMARY KEY CLUSTERED ([OrderLineDueId] ASC)
);

