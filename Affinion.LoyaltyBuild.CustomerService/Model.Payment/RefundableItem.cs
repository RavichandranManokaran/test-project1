﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model.Payment
{
    public class RefundableItem
    {
        /// <summary>
        /// ReferenceId
        /// </summary>
        public string ReferenceId { get; set; }

        /// <summary>
        /// Amount
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// ProviderAmount
        /// </summary>
        public decimal ProviderAmount { get; set; }

        /// <summary>
        /// CommissionAmount
        /// </summary>
        public decimal CommissionAmount { get; set; }

        /// <summary>
        /// Processing Fee
        /// </summary>
        public decimal ProcessingFee { get; set; }

        /// <summary>
        /// CreatedDate
        /// </summary>
        public DateTime CreatedDate { get; set; }
    }
}
