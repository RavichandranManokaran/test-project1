

CREATE TABLE [dbo].[uCommerce_subscriber](
	[SubscriberID] [int] IDENTITY(100,1) NOT NULL,
	[ClientID] [varchar](100) NULL,
	[CustomerID] [int] NULL,
	[EmailID] [varchar](100) NULL,
	[SubscriptionID] [int] NULL,
	[SubscribeDate] [date] NULL,
	[UnsubscribeReason] [varchar](1000) NULL,
	[UnsubscribeDate] [date] NULL,
 CONSTRAINT [PK_uCommerce_subscribe] PRIMARY KEY CLUSTERED 
(
	[SubscriberID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

