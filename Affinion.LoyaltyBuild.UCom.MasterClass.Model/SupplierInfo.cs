﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
 PROPRIETARY INFORMATION The information contained herein (the 
 'Proprietary Information') is highly confidential and proprietary to and 
 constitutes trade secrets of Affinion International. The Proprietary Information 
 is for Affinion International use only and shall not be published, 
 communicated, disclosed or divulged to any person, firm, corporation or 
 other legal entity, directly or indirectly, without the prior written 
 consent of Affinion International.
 */
namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    /// <summary>
    /// Entity to hold the supplier information 
    /// This data comes from the sitecore
    /// </summary>

    public class SupplierInfo
    {
        /// <summary>
        ///Supplier Name
        /// </summary>
        public string SupplierName { get; set; }

        /// <summary>
        ///SupplierAddress
        /// </summary>
        public string SupplierAddress { get; set; }

        /// <summary>
        ///SupplierType
        /// </summary>
        public string SupplierType { get; set; }

        /// <summary>
        ///StarRating
        /// </summary>
        public string StarRating { get; set; }

        /// <summary>
        ///offer Name
        /// </summary>
        public string CampaignName { get; set; }

        /// <summary>
        ///ArrivalDatesPeriod
        /// </summary>
        public string ArrivalDatesPeriod { get; set; }

        /// <summary>
        ///ReservationDatesPeriod
        /// </summary>
        public string ReservationDatesPeriod { get; set; }

        /// <summary>
        ///Client
        /// </summary>
        public string Client { get; set; }


        /// <summary>
        ///Country
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        ///OptedBy
        /// </summary>
        public string OptedBy { get; set; }

        /// <summary>
        ///Email
        /// </summary>
        public string Email { get; set; }

        public string Group { get; set; }

        public string SuperGroup { get; set; }

        public string MapLocation { get; set; }

        /// <summary>
        ///OptedDate
        /// </summary> 
        public DateTime OptedDate { get; set; }
    }
}
