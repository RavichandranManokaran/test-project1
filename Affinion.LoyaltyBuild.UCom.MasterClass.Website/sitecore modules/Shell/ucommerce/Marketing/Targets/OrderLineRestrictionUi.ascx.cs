﻿using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.Presentation.Views.Marketing;
using UCommerce.Presentation.Web.Controls;
using UCommerce.Presentation.Web.Marketing;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.Ucommerce.Marketing.Targets
{
    public partial class OrderLineRestrictionUi : ViewEnabledControl<IEditCampaignItemView>, ITargetUi
    {
        //List<DataTypeEnum> datatypeList;

        protected void Page_Load(object sender, EventArgs e)
        {
            OrderLineRestrictionTarget dataSource = this.DataSource as OrderLineRestrictionTarget;
            this.DeleteButton.OnClientClick = string.Format("return confirm('Delete');");
            OrderLineText.Text = dataSource.NoOfOrderLine.ToString();
            this.ReadOnlyMessage.Text = this.TargetToString(dataSource);
        }

        protected void SaveButton_Click(object sender, ImageClickEventArgs e)
        {
            OrderLineRestrictionTarget dataSource = this.DataSource as OrderLineRestrictionTarget;
            dataSource.NoOfOrderLine= Convert.ToInt32(OrderLineText.Text);
            dataSource.Save();
            this.ReadOnlyMessage.Text = this.TargetToString(dataSource);
            this.ToggleEditor();
        }

        private void ToggleEditor()
        {
            this.NonEditModePlaceHolder.Visible = !this.NonEditModePlaceHolder.Visible;
            this.EditButton.Visible = !this.EditButton.Visible;
            this.EditModePlaceHolder.Visible = !this.EditModePlaceHolder.Visible;
            this.SaveButton.Visible = !this.SaveButton.Visible;
        }

        private string TargetToString(OrderLineRestrictionTarget OrderLineRestrictionTarget)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendFormat("No. Of Orderline to apply  ");
            stringBuilder.AppendFormat("{0} \n", OrderLineRestrictionTarget.NoOfOrderLine);
            return stringBuilder.ToString();
        }

        protected void DeleteButton_Click(object sender, ImageClickEventArgs e)
        {
            OrderLineRestrictionTarget dataSource = this.DataSource as OrderLineRestrictionTarget;
            if (dataSource != null)
            {
                dataSource.Delete();
            }
            this.Page.Response.Redirect(this.Page.Request.Url.ToString(), true);
        }

        protected void EditButton_Click(object sender, ImageClickEventArgs e)
        {
            this.ToggleEditor();
        }


        public bool EnabledForApply
        {
            get;
            set;
        }

        public bool EnabledForDisplay
        {
            get;
            set;
        }

        public object DataSource
        {
            get;
            set;

        }

        public object New()
        {
            
            var orderLineRestrictionTarget = OrderLineRestrictionTarget.FirstOrDefault(x=>x.CampaignItem.CampaignItemId == View.CampaignItem.CampaignItemId) ?? new OrderLineRestrictionTarget();
            try
            {
                orderLineRestrictionTarget.CampaignItem = View.CampaignItem;
                orderLineRestrictionTarget.EnabledForApply = true;
                orderLineRestrictionTarget.NoOfOrderLine = 0;
                orderLineRestrictionTarget.Save();
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.MasterClassWebsite, ex, this);
                throw;
            }
            this.DataSource = orderLineRestrictionTarget;
            return orderLineRestrictionTarget;
        }
    }
}