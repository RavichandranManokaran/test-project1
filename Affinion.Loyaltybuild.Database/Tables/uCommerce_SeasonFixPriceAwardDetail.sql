CREATE TABLE [dbo].[uCommerce_SeasonFixPriceAwardDetail](
	[SeasonFixPriceAwardDetailId] [int] IDENTITY(1,1) NOT NULL,
	[SeasonFixPriceAwardId] [int] NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[AmountOff] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK_uCommerce_SeasonFixPriceAwardDetail] PRIMARY KEY CLUSTERED 
(
	[SeasonFixPriceAwardDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[uCommerce_SeasonFixPriceAwardDetail]  WITH CHECK ADD  CONSTRAINT [FK_uCommerce_SeasonFixPriceAwardDetail_uCommerce_SeasonFixPriceAward] FOREIGN KEY([SeasonFixPriceAwardId])
REFERENCES [dbo].[uCommerce_SeasonFixPriceAward] ([SeasonFixPriceAwardId])
GO

ALTER TABLE [dbo].[uCommerce_SeasonFixPriceAwardDetail] CHECK CONSTRAINT [FK_uCommerce_SeasonFixPriceAwardDetail_uCommerce_SeasonFixPriceAward]
GO


