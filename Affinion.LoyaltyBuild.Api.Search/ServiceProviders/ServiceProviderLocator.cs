﻿using Affinion.LoyaltyBuild.Model.Provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Api.Search.ServiceProviders
{
    /// <summary>
    /// service locator
    /// </summary>
    public class ServiceProviderLocator : IProviderService
    {
        /// <summary>
        /// Get service provider by type
        /// </summary>
        /// <param name="providerType"></param>
        /// <returns></returns>
        public IServiceProvider GetServiceProvider(ProviderType providerType)
        {
            //return new Mock.MockServiceProvider();

            switch (providerType)
            {
                case ProviderType.BedBanks:
                    return new BedBanksProvider();
                case ProviderType.LoyaltyBuild:
                case ProviderType.Mixed:
                    return new LoyaltyProvider();
            }

            return null;
        }


        /// <summary>
        /// Get search providers matching the client
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IServiceProvider> GetServiceProvidersByClient(string clientId)
        {
            //return new List<IServiceProvider> { new Mock.MockServiceProvider() };


            IList<IServiceProvider> providerList = new List<IServiceProvider>();

            LoyaltyProvider lbSearchProvider = new LoyaltyProvider();
            providerList.Add(lbSearchProvider);

            BedBanksProvider bbSearchProvider = new BedBanksProvider();
            providerList.Add(bbSearchProvider);

            return providerList;
        }
    }
}
