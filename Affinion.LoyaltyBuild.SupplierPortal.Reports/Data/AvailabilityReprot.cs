﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.SupplierPortal.Reports.Data
{
    /// <summary>
    /// Availability data to get from DB
    /// </summary>
    public class AvailabilityData
    {
        /// <summary>
        /// Hotel id
        /// </summary>
        public int HotelId { get; set; }

        /// <summary>
        /// Hotel/Provider name
        /// </summary>
        public string HotelName { get; set; }

        /// <summary>
        /// Room type string
        /// </summary>
        public string RoomType { get; set; }

        /// <summary>
        /// Date on which the room is closed out or sold out
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// is closed out flag for the date
        /// </summary>
        public bool IsClosedOut { get; set; }

        /// <summary>
        /// Is sold out flag for the date
        /// </summary>
        public bool IsSoldOut { get; set; }

        /// <summary>
        /// Date on which the room is closed out or sold out
        /// </summary>
        public DateTime FirstDate { get; set; }
    }


    /// <summary>
    /// Contains details for availability report item
    /// </summary>
    public class AvailabilityReprot
    {
        /// <summary>
        /// Hotel id
        /// </summary>
        public int HotelId { get; set; }

        /// <summary>
        /// Hotel/Provider name
        /// </summary>
        public string HotelName { get; set; }

        /// <summary>
        /// Room type string
        /// </summary>
        public string RoomType { get; set; }

        /// <summary>
        /// Month string
        /// </summary>
        public DateTime Month { get; set; }

        /// <summary>
        /// List of closed out dates for the month seperated by commas
        /// </summary>
        public string ClosedOutDates { get; set; }

        /// <summary>
        /// List of sold out dates for the month seperated by commas
        /// </summary>
        public string SoldOutDates { get; set; }
    }

    /// <summary>
    /// Collection to manage Availability
    /// </summary>
    public class AvailabilityCollection : IEnumerable<AvailabilityReprot>
    {
        //Private variables declaration
        private List<AvailabilityReprot> _items;

        /// <summary>
        /// Constructor
        /// </summary>
        public AvailabilityCollection()
        {
            _items = new List<AvailabilityReprot>();
        }

        /// <summary>
        /// Add method to add item
        /// </summary>
        /// <param name="item"></param>
        public void Add(AvailabilityReprot item)
        {
            _items.Add(item);
        }

        /// <summary>
        /// Override method IEnumenrable class
        /// </summary>
        /// <returns></returns>
        public IEnumerator<AvailabilityReprot> GetEnumerator()
        {
            foreach (var item in _items)
                yield return item;
        }

        /// <summary>
        /// Override method IEnumenrable class
        /// </summary>
        /// <returns></returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            foreach (var item in _items)
                yield return item;
        }
    }
}
