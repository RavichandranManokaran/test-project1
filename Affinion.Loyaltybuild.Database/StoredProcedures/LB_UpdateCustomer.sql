﻿/*<summary>
Description: Stored procedure to update customer details for the given customer ID
<param name="@customerId">Customer Id</param>
<param name="@FirstName">Customer First name</param>
<param name="@LastName">The Customer Last Name</param>
<param name="@EmailAddress">The Customer email address</param>
<param name="@MobilePhone">The Customer Mobile Phone</param>
<param name="@Phone">The Customer Phone</param>
<param name="@ClientId">The Client Id</param>
<param name="@Title">The Customer Title</param>
<param name="@AddressLine1">The Customer Addressline 1</param>
<param name="@AddressLine2">The Customer Addressline 2</param>
<param name="@AddressLine3">The Customer Addressline 3</param>
<param name="@CountryId">The Customer Id</param>
<param name="@LocationId">The Customer Id</param>
<param name="@Language">The Customer Id</param>
</summary>*/

CREATE PROCEDURE [dbo].[LB_UpdateCustomer](
	@CustomerId int,
	@FirstName nvarchar(512),
	@LastName nvarchar(512),
	@EmailAddress nvarchar(255),
	@Phone nvarchar(50)=Null,
	@MobilePhone nvarchar(50),
	@LanguageName nvarchar(255)=null,
	@ClientId varchar(50),
	@Title varchar(50)=Null,
	@AddressLine1 nvarchar(50),
	@AddressLine2 nvarchar(50)=Null,
	@AddressLine3 nvarchar(50)=Null,
	@CountryId int,
	@LocationId int,
	@BranchId VARCHAR(50)=Null, 
	@Subscribe int, 
	@Active int)	
AS
BEGIN
	/*BookingReferenceId should not get updated*/

	BEGIN TRANSACTION
	/*Update uCommerce_Customer table with edited data*/
	UPDATE [dbo].uCommerce_Customer  
		SET FirstName=@FirstName,
		LastName = @LastName,
		EmailAddress = @EmailAddress,
		PhoneNumber = @Phone,
		MobilePhoneNumber = @MobilePhone,
		LanguageName = @LanguageName
		Where CustomerId = @CustomerId

	   /*If an Error Occurs Rollback Add*/
	   If @@ERROR<>0
	   BEGIN
		ROLLBACK
		RETURN
	   END
	
	  /*Update uCommerce_Customer_Attribute table with edited data*/
	  UPDATE [dbo].LB_CustomerAttribute
		SET ClientId = @ClientId,
		Title = @Title,
		AddressLine1 = @AddressLine1,
		AddressLine2 = @AddressLine2,
		AddressLine3 = @AddressLine3,
		CountryId = @CountryId,
		LocationId = @LocationId,
		BranchId = @BranchId, 
		Subscribe = @Subscribe, 
		Active = @Active
		Where CustomerId = @CustomerId

	  /*If an Error Occurs Rollback Add*/
	  If @@ERROR<>0
	   BEGIN
		ROLLBACK
		RETURN
	   END

	COMMIT
END

