﻿using Affinion.LoyaltyBuild.Model.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi;
using Affinion.LoyaltyBuild.UCom.Common.Extension;
using Affinion.LoyaltyBuild.Web.ClientPortal.ViewModels;
using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
using System.Data;
using UCommerce.Infrastructure;
using UCommerce.EntitiesV2;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Availability;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Queries;
using Affinion.LoyaltyBuild.UCom.MasterClass.Model;
using Affinion.LoyaltyBuild.UCom.Common.Constants;
using Affinion.LoyaltyBuild.Api.Search.Data;
using Affinion.LoyaltyBuild.Api.Booking.Helper;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Helper
{
    /// <summary>
    /// Helper class for search
    /// </summary>
    public class HotelSearchHelper
    {
        /// <summary>
        /// Get price and availability by date
        /// </summary>
        /// <param name="products"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="noOfNights"></param>
        /// <returns></returns>
        public static List<PriceAvailabilityByDate> GetAvailabilityAndPriceByDate(IList<IProduct> products, SearchKey searchKey, DateTime fromDate, DateTime toDate, int noOfNights)
        {
            var toDateCounter =toDate.ConvetDatetoDateCounter();

            var availabilty = LoyaltyBuildCatalogLibrary.GetRoomAvailabilityWithPrice(
                products.Where(i => i.Type != ProductType.BedBanks).Select(i => i.VariantSku).ToList(),
                fromDate.ConvetDatetoDateCounter(),
                toDateCounter + noOfNights - 1);

            var result = new List<PriceAvailabilityByDate>();

            foreach (var item in availabilty)
            {
                foreach(var subItem in item.DayWiseAvailability)
                {
                    if(subItem.DateCounter <= toDateCounter)
                    {
                        var items = item.DayWiseAvailability
                        .Where(i => i.DateCounter >= subItem.DateCounter &&
                        i.DateCounter < (subItem.DateCounter + noOfNights) &&
                        i.AvailableRoom > 0).ToList();
                        if(items.Count == noOfNights)
                        {
                            result.Add(new PriceAvailabilityByDate
                            {
                                Sku = item.SKU,
                                NoOfNights = noOfNights,
                                VariantSku = item.VaiantSKU,
                                RefId = item.VaiantSKU,
                                Date = subItem.DateCounter.GetDateFromDateCount(),
                                Availability = items.Min(i => i.AvailableRoom),
                                Price = items.Sum(i => i.Price),
                                CurrencyId = products.First(i => i.VariantSku == item.VaiantSKU).CurrencyId,
                                Type = products.First(i => i.VariantSku == item.VaiantSKU).Type
                            });
                        }
                    }
                }
            }

            var resultset = result.Where(p => p.Type != ProductType.Addon).ToList();

            //bed banks availability
            int temp = 0; 
            resultset.AddRange(products.Where(i => i.Type == ProductType.BedBanks)
                .Cast<BedBankProduct>()
                .Select(i => new BBPriceAvailabilityByDate
                {
                    Sku = i.Sku,
                    NoOfNights = searchKey.NoOfNights,
                    VariantSku = i.VariantSku,
                    Date = searchKey.CheckInDate.Value.AddDays(7 * searchKey.Week),
                    Availability = searchKey.NoOfRooms,
                    Price = i.FinalPrice,
                    CurrencyId = i.CurrencyId,
                    NoOfAdults = searchKey.NoOfAdults,
                    NoOfChildren = searchKey.NoOfChildren,
                    PreBookingToken = i.TokenForBooking,
                    MealBasisId = i.MealBasisID,
                    ProviderId = i.ProviderId,
                    RefId = i.MealBasisID,
                    Type = i.Type,
                    PropertyReferenceId = i.PropertyReferenceId,
                    RoomInfo = i.Name,
                    OldPrice = i.FinalPrice,
                    MarginId = i.MarginId,
                    MarginPercentage = i.MarginPercentage,
                    BBCurrencyId = i.BBCurrencyId,
                    IsDirectStock = int.TryParse(i.TokenForBooking, out temp)
                }));

            if(BasketHelper.HasBasket)
            {
                foreach(var booking in BasketHelper.GetBasket().Bookings)
                {
                    string refId = booking.OrderLine.VariantSku;
                    if (booking.Type == ProductType.BedBanks)
                        refId = ((Api.Booking.Data.BedBanksBooking)booking).MealBasisId;

                    foreach (var item in resultset
                        .Where(i => i.RefId == refId && i.Date >= booking.CheckinDate && i.Date < booking.CheckoutDate))
                    {
                        item.Availability = item.Availability - booking.Quantity;
                    }
                }
            }

            return resultset;
        }

        /// <summary>
        /// Get offer availability by date
        /// </summary>
        /// <param name="VariantSkus"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="noOfNights"></param>
        /// <param name="currencyID"></param>
        /// <returns></returns>
        public static List<PromotionAvailabilityByDate> GetOfferAvailabilityAndPriceByDate(List<string> VariantSkus, DateTime fromDate, DateTime toDate, int noOfNights,int currencyID)
        {
            var result = new List<PromotionAvailabilityByDate>();
            var fromDatecounter = fromDate.ConvetDatetoDateCounter();
            var toDateCounter = toDate.ConvetDatetoDateCounter();
            //var availabilty = new List<OfferRoomAvailability>();

            List<LBOfferAvailability> availabilty = ObjectFactory.Instance.Resolve<IRepository<LBOfferAvailability>>().Select(new PromotionByDateQuery(string.Join(",", VariantSkus), fromDatecounter, toDateCounter)).ToList();
            

            foreach (var item in availabilty)
            {
                if (item.DateCounter <= toDateCounter)
                {
                    var items = availabilty.Where(i => i.DateCounter >= item.DateCounter &&
                    i.DateCounter < (item.DateCounter + noOfNights) &&
                    i.VariantSku == item.VariantSku && i.CampaignItem == item.CampaignItem &&
                    i.AvaialbileRoom > 0).ToList();
                    var product = Product.FirstOrDefault(i => i.Sku == item.Sku && i.VariantSku == item.VariantSku);

                    var productType = GetProductType(product);
                   
                    if (items.Count == noOfNights)
                    {
                        result.Add(new PromotionAvailabilityByDate
                        {
                            PromotionId = item.CampaignItem,
                            Sku = item.Sku,
                            NoOfNights = noOfNights,
                            VariantSku = item.VariantSku,
                            Date = item.DateCounter.GetDateFromDateCount(),
                            Availability = items.Min(i => (i.AvaialbileRoom)),
                            Price = 0,
                            Type =  productType==1 ? ProductType.Room : (productType==3 ? ProductType.Package : 0),
                            CurrencyId =currencyID
                            
                        });
                    }
                }
            }

            var resultset = result.Where(p => p.Type != ProductType.Addon).ToList();
            return resultset;
        }

        /// <summary>
        /// Get Product Type
        /// </summary>
        /// <param name="item"></param>
        /// <param name="ignoreNotSellable"></param>
        /// <returns></returns>
        private static int GetProductType(Product item, bool ignoreNotSellable = false)
        {
            int type = -1;
            if (item != null)
            {
                ProductProperty addOn = item[ProductDefinationVairantConstant.FoodType];
                ProductProperty packagType = item[ProductDefinationVairantConstant.PackageType];
                ProductProperty relatedSKU = item[ProductDefinationVairantConstant.RelatedSKU];
                ProductProperty roomType = item[ProductDefinationVairantConstant.RoomType];
                if (packagType != null && !string.IsNullOrEmpty(packagType.Value))
                {
                    //Package
                    type = 3;
                }
                else if (roomType != null && !string.IsNullOrEmpty(roomType.Value))
                {
                    //Room
                    type = 1;
                }
                else
                {
                    ProductProperty sellableAddon = item[ProductDefinationVairantConstant.IsSellableStandAlone];
                    bool sellable = false;
                    if (sellableAddon != null && !string.IsNullOrEmpty(sellableAddon.Value))
                    {
                        sellable = Convert.ToBoolean(sellableAddon.Value);
                    }
                    if (!sellable)
                    {
                        if (ignoreNotSellable)
                            type = -1;
                    }
                    else
                    {
                        //Add on
                        type = 2;
                    }
                }
            }
            return type;
        }

    }
}