﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SubscribeHelper.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common.Utilities
/// Description:           Subscribe helper
/// </summary>

using Affinion.LoyaltyBuild.Common.Instrumentation;
using Sitecore.Data.Items;
using System;
using System.Net.Mail;

namespace Affinion.LoyaltyBuild.Common.Utilities
{
    public static class SubscribeHelper
    {
        /// <summary>
        /// Create Subscription
        /// </summary>
        /// <param name="email"></param>
        /// <param name="rootItem"></param>
        /// <returns></returns>
        public static bool CreateSubscription(string email, Item rootItem, string relativeFolderPath)
        {
            //Subscribelabel.Text = string.Empty;
            try
            {
                if (rootItem == null)
                {
                    throw new ArgumentNullException("rootItem", "Root item is null");
                }

                if (!string.IsNullOrWhiteSpace(email))
                {
                    MailAddress mailaddress = new MailAddress(email);

                    if (mailaddress != null)
                    {
                        ///Get the root item of the Subscription folder
                        //Item rootItem = Sitecore.Context.Database.GetItem(Sitecore.Context.Site.RootPath);
                        //string parentItemPath = rootItem.Paths.FullPath + Constants.FolderName;
                        string parentItemPath = rootItem.Paths.FullPath + relativeFolderPath;

                        string templatePath = Constants.SubscribeTemplatePath;

                        ///Get the email from the text box
                        string itemName = email;

                        ///Replace "@" sign with "at" and "." sign with "dot" for item name
                        string newItemName = SitecoreItemHelper.ResetItemName(itemName, "@", "at");
                        string completeName = SitecoreItemHelper.ResetItemName(newItemName, "\\.", "dot");

                        Item itemExist = ItemExist(itemName, parentItemPath);

                        if (itemExist != null)
                        {
                            throw new ArgumentException(Constants.EmailExist);
                            //Subscribelabel.Text = Constants.EmailExist;
                        }
                        else
                        {
                            ///Create item for email address
                            return CreateChildItem(itemName, parentItemPath, templatePath, completeName);
                        }
                    }

                }
                throw new ArgumentNullException("email", "Email is null");
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, typeof(SubscribeHelper));
                throw;
            }
        }

        /// <summary>
        /// Delete Subscription
        /// </summary>
        /// <param name="email"></param>
        /// <param name="rootItem"></param>
        /// <returns></returns>
        public static bool DeleteSubscription(string email, Item rootItem, string relativeFolderPath)
        {
            //Subscribelabel.Text = string.Empty;
            try
            {
                if (rootItem == null)
                {
                    throw new ArgumentNullException("rootItem", "Root item is null");
                }

                if (!string.IsNullOrWhiteSpace(email))
                {
                    MailAddress mailaddress = new MailAddress(email);

                    if (mailaddress != null)
                    {
                        ///Get the root item of the Subscription folder
                        //Item rootItem = Sitecore.Context.Database.GetItem(Sitecore.Context.Site.RootPath);
                        //string parentItemPath = rootItem.Paths.FullPath + Constants.FolderName;
                        string parentItemPath = rootItem.Paths.FullPath + relativeFolderPath;

                        ///Get the email from the text box
                        string itemName = email;

                        ///Replace "@" sign with "at" and "." sign with "dot" for item name
                        string newItemName = SitecoreItemHelper.ResetItemName(itemName, "@", "at");
                        string completeName = SitecoreItemHelper.ResetItemName(newItemName, "\\.", "dot");

                        Item myItem = ItemExist(itemName, parentItemPath);

                        if (myItem != null)
                        {
                            using (new Sitecore.Security.Accounts.UserSwitcher(ItemHelper.ContentEditingUser))
                            {
                                ///delete item for email address
                                if (myItem.Recycle() != null)
                                {
                                    return true;
                                }
                                return false;
                            }
                        }
                        else
                        {
                            throw new ArgumentException(Constants.EmailNotFound);
                        }
                    }

                }
                throw new ArgumentNullException("email", "Email is null");
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, typeof(SubscribeHelper));
                throw;
            }

        }

        /// <summary>
        /// Check the item exists in the subscription folder
        /// </summary>
        /// <param name="itemName">email address</param>
        /// <param name="folderPath">path of the subscription folder</param>
        /// <returns></returns>
        private static Item ItemExist(string itemName, string folderPath)
        {
            try
            {
                Item[] folderItems = Sitecore.Context.Database.SelectItems(string.Concat(folderPath, "/*"));

                Item subscribeItem = null;

                foreach (Item folderItem in folderItems)
                {
                    if (!itemName.Equals(folderItem.Fields[Constants.EmailID].Value))
                    {
                        subscribeItem = null;
                    }
                    else
                    {
                        subscribeItem = folderItem;
                        break;
                    }
                }
                return subscribeItem;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, typeof(SubscribeHelper));
                throw;
            }
        }


        /// <summary>
        /// Create ChildItem
        /// </summary>
        /// <param name="email">email</param>
        /// <param name="parentItemPath">parent Item Path</param>
        /// <param name="templatePath">template Path</param>
        /// <param name="completeName">complete Name</param>
        /// <returns></returns>
        private static bool CreateChildItem(string email, string parentItemPath, string templatePath, string completeName)
        {
            try
            {
                ///Create Item
                ItemHelper.AddItem(Sitecore.Context.Database, parentItemPath, templatePath, completeName);

                ///Get the item ID of the newly created item
                var newItem = Sitecore.Context.Database.GetItem(string.Concat(parentItemPath, Constants.SlashText, completeName));

                if (!newItem.ID.IsNull)
                {
                    using (new Sitecore.Security.Accounts.UserSwitcher(ItemHelper.ContentEditingUser))
                    {
                        newItem.Editing.BeginEdit();

                        ///Add values to the Email filed.
                        newItem.Fields[Constants.EmailID].Value = email;
                        newItem.Editing.EndEdit();
                    }

                    return true;
                }

                //return false;
                throw new ArgumentNullException("parentItemPath", Constants.ErrorMesage);

            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, typeof(SubscribeHelper));
                throw;
            }
        }

    }
}
