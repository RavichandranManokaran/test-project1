﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Linq;
using UCommerce.EntitiesV2;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Queries
{
	public class ProductByPropertiesQuery : UCommerce.EntitiesV2.Queries.ICannedQuery<Product>
	{
		/// <summary>
		/// Private fieldName
		/// </summary>
        private readonly string _fieldName;
		
        /// <summary>
        /// Private Value
        /// </summary>
        private readonly string _value;
        
		/// <summary>
		/// ProductBy PropertiesQuery
		/// </summary>
		/// <param name="fieldName"></param>
		/// <param name="value"></param>
        public ProductByPropertiesQuery(string fieldName, string value)
		{
			_fieldName = fieldName;
			_value = value;
		}

		/// <summary>
		/// IEnumerable Type Product
		/// </summary>
		/// <param name="session"></param>
		/// <returns></returns>
        public IEnumerable<Product> Execute(ISession session)
		{
			IQueryable<Product> query = session
				.Query<Product>()
				.Where(
					product =>
						product.ProductProperties.Any
							(property =>
								property.Value == _value &&
								property.ProductDefinitionField.Name == _fieldName));

			return query;
		}
	}
}
