﻿using Affinion.LoyaltyBuild.BedBanks.Troika;
using Affinion.LoyaltyBuild.Common.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.BedBanks
{
    public partial class MarginManagementMarginDetail : System.Web.UI.UserControl
    {
        string propertyId;
        string isactive;
        QueryStringHelper helper;
        /// <summary>
        /// populate the margin detail for the given property ID
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            helper = new QueryStringHelper(Request.Url);
            this.propertyId = !string.IsNullOrEmpty(helper.GetValue("propertyId")) ? helper.GetValue("propertyId") : string.Empty;
            this.isactive = "1";
            if (!IsPostBack)
            {
                DataSet data = TroikaDataController.GetProviderMargin(propertyId, isactive);
                if (data != null)
                {
                    gvMarginData.DataSource = data;
                    gvMarginData.DataBind();
                }
            }
        }

        /// <summary>
        /// Go to Margin Management Search Page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void backBtn_ServerClick(object sender, EventArgs e)
        {
            string errorListId = helper.GetValue("ErrorListId");
            string previousPage;
            if (!string.IsNullOrEmpty(errorListId))
            {
                previousPage = SitecoreFieldsHelper.GetItemFieldValue(Sitecore.Context.Item, "Margin Error Edit Page Link");
                if (!string.IsNullOrEmpty(previousPage))
                    Response.Redirect(previousPage + "?ListID=" + errorListId);
            }
            else
            {
                previousPage = SitecoreFieldsHelper.GetItemFieldValue(Sitecore.Context.Item, "Margin Search Page Link");
                if (!string.IsNullOrEmpty(previousPage))
                    Response.Redirect(previousPage); //need to change it 
            }
        }

        protected void gvMarginData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName != "editMargin")
                return;
            string marginId = e.CommandArgument.ToString();
            string errorListId = helper.GetValue("ErrorListId");
            string pageUrl = SitecoreFieldsHelper.GetItemFieldValue(Sitecore.Context.Item, "Add and Edit Page Link");
            if (marginId.Equals("1")) // need to check how to check for default margin
            {
                if (!string.IsNullOrEmpty(pageUrl))
                    Response.Redirect(pageUrl + "?MarginId=" + marginId + "&propertyId=" + propertyId + "&type=defaultedit");
            }
            else
            {
                if (!string.IsNullOrEmpty(errorListId))
                {
                    if (!string.IsNullOrEmpty(pageUrl))
                        Response.Redirect(pageUrl + "?MarginId=" + marginId + "&ErrorListId="+errorListId+"&propertyId=" + propertyId + "&type=edit");
                }
                else
                {
                    if (!string.IsNullOrEmpty(pageUrl))
                        Response.Redirect(pageUrl + "?MarginId=" + marginId + "&propertyId=" + propertyId + "&type=edit");
                }
            }

        }


        /// <summary>
        /// Set the color for the type of margin
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvMarginData_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[0].Text == "Default Margin")
                {
                    e.Row.BackColor = Color.LightGray;  //base margin
                    e.Row.ForeColor = Color.Black;
                    return;
                }
                string[] formats = { "MMM dd yyyy HH:mmtt", "yyyy-MM-dd" };
                DateTime dateFrom, dateTo;

                bool dateFromBool = DateTime.TryParseExact(e.Row.Cells[0].Text, formats, System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.None, out dateFrom);
                bool dateToBool = DateTime.TryParseExact(e.Row.Cells[1].Text, formats, System.Globalization.CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTo);

                if (dateFrom != null && dateTo != null)
                {
                    e.Row.Cells[0].Text = dateFrom.ToString("dd/MM/yyyy");
                    e.Row.Cells[1].Text = dateTo.ToString("dd/MM/yyyy");
                    if ((DateTime.Now.Date >= dateFrom.Date) && (DateTime.Now.Date <= dateTo.Date) && dateFromBool && dateToBool)
                    {
                        e.Row.BackColor = Color.LightGreen;   //present margin
                    }
                    else if (DateTime.Now.Date < dateFrom.Date && dateFromBool && dateToBool)
                    {
                        e.Row.BackColor = Color.FromArgb(255,204,255);  //future margin
                    }
                    else if (DateTime.Now.Date > dateTo.Date && dateFromBool && dateToBool)
                    {
                        e.Row.BackColor = Color.Red;  //expired margin
                    }
                }
                Literal lit = e.Row.Cells[3].FindControl("litIsActive") as Literal;
                if (lit.Text.Equals("Yes"))
                {
                    e.Row.ForeColor = Color.Black; //active margin text
                }
                else if (lit.Text.Equals("No"))
                {
                    e.Row.ForeColor = Color.Gray; //inactive margin text
                }
            }
        }


        /// <summary>
        /// Go to adding a new margin
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void addMarginBtn_ServerClick(object sender, EventArgs e)
        {
            string errorListId = helper.GetValue("ErrorListId");
            string pageUrl = SitecoreFieldsHelper.GetItemFieldValue(Sitecore.Context.Item, "Add and Edit Page Link");
            if (!string.IsNullOrEmpty(errorListId))
            {
                Response.Redirect(pageUrl + "?propertyId=" + propertyId + "&ErrorListId=" + errorListId + "&type=add");
            }
            else
            {
                if (!string.IsNullOrEmpty(pageUrl))
                    Response.Redirect(pageUrl + "?propertyId=" + propertyId + "&type=add");
            }
        }

        /// <summary>
        /// Apply the filters for margin
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlFilters_SelectedIndexChanged(object sender, EventArgs e)
        {
            isactive = ddlFilters.SelectedValue;
            DataSet data = TroikaDataController.GetProviderMargin(propertyId, isactive);
            if (data != null)
            {
                gvMarginData.DataSource = data;
                gvMarginData.DataBind();
            }
        }
    }
}