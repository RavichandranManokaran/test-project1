﻿CREATE TABLE [dbo].[uCommerce_PercentOffOrderLinesAward] (
    [PercentOffOrderLinesAwardId] INT             NOT NULL,
    [PercentOff]                  DECIMAL (18, 2) NOT NULL,
    CONSTRAINT [PK_uCommerce_ProcentOffOrderLinesAward] PRIMARY KEY CLUSTERED ([PercentOffOrderLinesAwardId] ASC)
);

