﻿CREATE TABLE [dbo].[uCommerce_FavoriteHotel](
	[RseenID] [uniqueidentifier] NULL,
	[CustomerID] [int] NULL,
	[HotelID] [varchar](100) NULL,
	[CreatedBy] [varchar](100) NULL,
	[UpdateBy] [varchar](100) NULL
) ON [PRIMARY]

GO