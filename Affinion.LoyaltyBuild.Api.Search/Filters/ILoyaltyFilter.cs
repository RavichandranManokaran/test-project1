﻿using Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi;

namespace Affinion.LoyaltyBuild.Api.Search.Filters
{
    /// <summary>
    /// Interface to get LoyaltyFilter
    /// </summary>
    public interface ILoyaltyFilter
    {
        /// <summary>
        /// SearchCriteria
        /// </summary>
        /// <param name="criteria"></param>
        void BuildSearch(ref SearchCriteria criteria);
    }
}
