﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TopbarLinkArea.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal.TopbarLinkArea" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<div class="header-right-content-nav width-full">
    <div class="container-fluid">
        <div class="navbar navbar-default no-margin-bottom">
            <div class="container-fluid">
                <!--// End of Logo And Search -->
                <!-- Second NavBar -->
                <!-- Secondary Navbar -->
                <div class="nav-container">
                    <!-- Global Navbar -->
                    <div class="navbar-wrapper">
                        <div class="">
                            <div class="navbar navbar-default navbar-fixed-top" role="navigation">
                                <div>
                                    <div class="navbar-header">
                                        <button id="ty-navtrigger" type="button" class="navbar-toggle collapsed" data-toggle="collapse">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- //End Of Global Navbar -->
                    <!-- Logo And Search -->
                    <!--// End of Logo And Search -->
                    <!-- Second NavBar -->
                    <!-- Secondary Navbar -->
                    <div class="navbar navbar-default" role="navigation" id="ty-res-nav">
                        <div class="container-fluid ty-noPadding-left-right">
                            <div class="navbar-header">
                            </div>
                            <div class="navbar-collapse collapse in ty-navbar" id="nav2">
                                <ul class="nav navbar-nav ty-taylor-nav2  ty-nav-back">
                                    <li class="no-padding-right">
                                        <a href="#">Home</a>
                                        <%-- <sc:Placeholder ID="TobbarTextLink" Key="TobbarTextLink" runat="server"/> --%>                                                                               
                                    </li>
                                   <%-- <li>                                        
                                        <a href="#">hotel detail </a>
                                    </li>--%>
                                     <sc:Placeholder ID="TobbarTextLink" Key="TobbarTextLink" runat="server"/> 
                                    <li>
                                        <a href="#">
                                            <div class="basket float-right header-right hidden-xs" id="basket-info">
                                                <div id="basket-ico">
                                                    <p>
                                                        <i class="fa fa-briefcase"></i>
                                                        <span>5</span>
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!--/.nav-collapse -->
                        </div>
                        <!--/.container-fluid -->
                    </div>
                </div>
            </div>
            <!--/.nav-collapse -->
        </div>
        <!--/.container-fluid -->
    </div>
    
</div>


     <div class="sm-visibale">
                     <div class="sm-visibale-inner">
                        <div class="res-icon float-right visible-xs" id="ph-icon">
                           <a href="#"><i class="fa fa-phone"></i></a>
                        </div>
                        <div class="res-icon float-right visible-xs" id="fav-icon">
                           <a href="#"><i class="fa fa-heart"></i></a>
                        </div>
                        <div class="res-icon float-right visible-xs" id="bck-icon">
                           <a href="#">
                              <div id="basket-ico-respodive">
                                 <p><i class="fa fa-briefcase"></i> 
                                    <span>5</span>
                                 </p>
                              </div>
                           </a>
                        </div>
                        <div class="res-icon float-right visible-xs" id="user-icon">
                           <a href="#"><i class="fa fa-user" data-toggle="modal" data-target="#exampleModal" data-whatever="@fat"></i></a>
                        </div>
                     </div>
                  </div>
              