
/****** Object:  StoredProcedure [dbo].[LB_BB_GetMarginForEdit]    Script Date: 7/19/2016 7:26:01 PM ******/

create procedure [dbo].[LB_BB_GetMarginForEdit](@marginId int)
as
begin
select marginPercentage, startDate, endDate, isactive from LB_BB_MarginDetails where MarginID=@marginId
end
GO

