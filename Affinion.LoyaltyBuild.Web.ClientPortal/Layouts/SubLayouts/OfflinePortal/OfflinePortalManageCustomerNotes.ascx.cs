﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Customer;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
using Affinion.LoyaltyBuild.Web.ClientPortal.Helper;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Sitecore.Data.Items;
using Affinion.LoyaltyBuild.Common.CardValidationCheck;
using Affinion.LoyaltyBuild.Api.Booking.Service;
using Affinion.LoyaltyBuild.Model.Product;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    public partial class OfflinePortalManageCustomerNotes : BaseSublayout
    {
        #region Privete member

        private INoteService _noteService;
        private IEnumerable<BookingNotes> _notes;
        private int _orderLineId;
        private string _createdBy = string.Empty;
        #endregion

        #region Protected Method
        protected ValidationResponse ValidationResponse { get; private set; }

        protected void Page_Init(Object sender, EventArgs e)
        {


        }

        protected void Page_Load(object sender, EventArgs e)
        {
            BindSiteoreText();           
            int olid;
            int.TryParse(this.QueryString("olid"), out olid);
            _orderLineId = olid;
            _createdBy = SitecoreUserProfileHelper.GetItem(UserProfileKeys.CreatedBy);
            BookingService booking = new BookingService();
            var bookingDetails = booking.GetBooking(_orderLineId);
            if (bookingDetails != null && bookingDetails.Type == ProductType.BedBanks)
            {
                txtNotetoProvider.Disabled = true;
                btnNotetoProvider.Disabled = true;
            }
            if (!Page.IsPostBack)
            {
                LoadCustomerNotes(_orderLineId);
            }
        }

        protected void AddiInternalNote_Click(object sender, EventArgs e)
        {
            var notes = new BookingNotes();
            notes.TypeId = Convert.ToInt32(NoteType.InternalNote);
            notes.OrderLineId = _orderLineId;
            notes.Note = (CredictCardCheck.CredictCard(txtAddiInternalNote.Value)).Trim();
            notes.CreatedBy = _createdBy;
            SaveCustomerNotes(notes);
            LoadCustomerNotes(_orderLineId);
            txtAddiInternalNote.Value = string.Empty;
        }

        protected void NotetoProvider_Click(object sender, EventArgs e)
        {
            var notes = new BookingNotes();
            notes.TypeId = Convert.ToInt32(NoteType.NoteToProvider);
            notes.OrderLineId = _orderLineId;
            notes.Note = (CredictCardCheck.CredictCard(txtNotetoProvider.Value)).Trim();
            notes.CreatedBy = _createdBy;
            SaveCustomerNotes(notes);
            LoadCustomerNotes(_orderLineId);
        }

        protected void SpecialRequests_Click(object sender, EventArgs e)
        {
            var notes = new BookingNotes();
            notes.TypeId = Convert.ToInt32(NoteType.SpecialRequest);
            notes.OrderLineId = _orderLineId;
            notes.Note = (CredictCardCheck.CredictCard(txtSpecialRequests.Value)).Trim();
            notes.CreatedBy = _createdBy;
            SaveCustomerNotes(notes);
            LoadCustomerNotes(_orderLineId);
        }

        #endregion

        #region Private Method
        private void BindSiteoreText()
        {
            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "Title Text", titleTxt);
            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "Internal Note Label Text", IntrnlNoteTxt);
            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "Add Internal Note Label Text", AddIntNotetxt);
            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "Note to Provider Label Text", ntpTxt);
            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "Special Request Label Text", srTxt);
            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "Add Internal Note Button Text", btnIntlNoteTxt);
            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "Save Provider Note Button Text", btnProNotTxt);
            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "Save Special Request Button Text", btnSrTxt);
            this.internalNote.ErrorMessage = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "DontEnterCardNumber", internalNote.ErrorMessage);
            this.noteToProvider.ErrorMessage = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "DontEnterCardNumber:", noteToProvider.ErrorMessage);
            this.specialRequest.ErrorMessage = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "DontEnterCardNumber:", specialRequest.ErrorMessage);

        }

        private void LoadCustomerNotes(int orderLineID)
        {
            _notes = new List<BookingNotes>();
            _noteService = new NoteService();
            _notes = _noteService.GetReservationNotes(orderLineID);

            if (_notes.Any())
            {
                // dispaly Internal Note
                var internalNotes = _notes
                    .Where(i => i.TypeId.Equals((int)NoteType.InternalNote))
                    .Select(i => i.Note);

                // display Note to Provider
                var noteToProvider = _notes
                    .Where(i => i.TypeId.Equals((int)NoteType.NoteToProvider))
                    .Select(i => i.Note).FirstOrDefault();

                // display Special Request
                var spiecialRequest = _notes
                    .Where(i => i.TypeId.Equals((int)NoteType.SpecialRequest))
                    .Select(i => i.Note).FirstOrDefault();

                txtInternalNote.Value = string.Join("\n", internalNotes);
                txtNotetoProvider.Value = noteToProvider;
                txtSpecialRequests.Value = spiecialRequest;
            }
        }

        //To Save the Note
        private void SaveCustomerNotes(BookingNotes bookingNote)
        {
            try
            {
                _noteService = new NoteService();
                ValidationResponse saveFlag = _noteService.SaveReservationNote(bookingNote);
                ValidationResponse = saveFlag;
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Sets the alert message to the user
        /// </summary>            
        /// <returns></returns> 
        private void SetMessage(string message, bool isError = true)
        {
            if (isError)
                divMsg.Attributes.Add("class", "alert alert-danger");
            else
                divMsg.Attributes.Add("class", "alert alert-success");

            divMsg.InnerText = message;
        }


        //Enum  Notetype
        private enum NoteType
        {
            InternalNote = 1,
            NoteToProvider = 2,
            SpecialRequest = 3
        }


        #endregion
    }
}