﻿using model = Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using service = Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Customer;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Handlers.CS
{
    /// <summary>
    /// Summary description for CustomerQuery
    /// </summary>
    public class AddCustomerQuery : IHttpHandler
    {

        private NameValueCollection _query = null;
        private service.QueryService _queryService;
        private JavaScriptSerializer _serializer;
        
        public void ProcessRequest(HttpContext context)
        {
            _query = HttpUtility.ParseQueryString(context.Request.Url.Query.ToLower());
            _queryService = new service.QueryService();
            _serializer = new JavaScriptSerializer();

            switch (_query["action"])
            {
                case "searchcustomer":
                    SearchCustomer(context);
                    break;
                case "addcustomer":
                    AddCustomer(context);
                    break;
                case "addquery":
                    AddQuery(context);
                    break;
            }
        }

        /// <summary>
        /// Add Query
        /// </summary>
        /// <param name="context"></param>
        private void AddQuery(HttpContext context)
        {
            var note = _serializer.Deserialize<model.CustomerQuery>(GetDocumentContents(context.Request));

            _queryService.SaveCustomerQuery(note);
        }

        /// <summary>
        /// Search customer query
        /// </summary>
        /// <param name="context"></param>
        private void SearchCustomer(HttpContext context)
        {
            var searchQuery = _serializer.Deserialize<model.SearchCustomer>(GetDocumentContents(context.Request));
        }

        /// <summary>
        /// Add customer    
        /// </summary>
        /// <param name="context"></param>
        private void AddCustomer(HttpContext context)
        {
            var customer = _serializer.Deserialize<model.Customer>(GetDocumentContents(context.Request));
        
        }

        /// <summary>
        /// is resuable flag
        /// </summary>
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// get body content
        /// </summary>
        /// <param name="Request"></param>
        /// <returns></returns>
        private string GetDocumentContents(System.Web.HttpRequest Request)
        {
            string documentContents;

            using (Stream receiveStream = Request.InputStream)
            {
                using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
                {
                    documentContents = readStream.ReadToEnd();
                }
            }

            return documentContents;
        }
    }
}