﻿CREATE TABLE [dbo].[uCommerce_ProductDescription] (
    [ProductDescriptionId] INT            IDENTITY (1, 1) NOT NULL,
    [ProductId]            INT            NOT NULL,
    [DisplayName]          NVARCHAR (512) NOT NULL,
    [ShortDescription]     NVARCHAR (MAX) NULL,
    [LongDescription]      NVARCHAR (MAX) NULL,
    [CultureCode]          NVARCHAR (60)  NOT NULL,
    CONSTRAINT [uCommerce_PK_ProductDescription] PRIMARY KEY CLUSTERED ([ProductDescriptionId] ASC),
    CONSTRAINT [uCommerce_FK_ProductDescription_Product] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[uCommerce_Product] ([ProductId]),
    CONSTRAINT [IX_uCommerce_ProductDescription_ProductId_CultureCode] UNIQUE NONCLUSTERED ([ProductId] ASC, [CultureCode] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_uCommerce_ProductDescription_ProductId]
    ON [dbo].[uCommerce_ProductDescription]([ProductId] ASC);

