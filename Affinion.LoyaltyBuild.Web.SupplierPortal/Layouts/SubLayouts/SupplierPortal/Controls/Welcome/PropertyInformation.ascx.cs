﻿using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data;
using Sitecore.Caching;
using Sitecore.Workflows;
using Sitecore.SecurityModel;
using Sitecore.Configuration;
using System.Data;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Sitecore.Data.Fields;
using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Security.Accounts;
using System.Text;
using Sitecore.Text;
using System.Collections.Specialized;



namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Welcome
{
    /// <summary>
    /// class PropertyInformation
    /// </summary>
    public partial class PropertyInformation : System.Web.UI.UserControl
    {

        #region private variables
        private String SelectedHotelID { get; set; }
        private Item SelectedHotelItem { get; set; }
        private String SelectedHotelDisplayName { get; set; }
        private String LoggedInSupplierID { get; set; }
        private String LoggedInSupplierDisplayName { get; set; }
        private Item LoggedInSupplierItem { get; set; }
        private bool isVersionAddedForItem { get; set; }
        #endregion

        List<ContactDetails> listContactDetails = new List<ContactDetails>();
        string currentUserRole = SitecoreUserProfile.GetItem(UserProfileKeys.currentLoggedinUserRole);

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

            if (currentUserRole.ToLower().Contains(Constants.accountManagement) || currentUserRole.ToLower().Contains(Constants.marketing) || currentUserRole.ToLower().Contains(Constants.IT) || currentUserRole.ToLower().Contains(Constants.superUser) || Sitecore.Context.IsAdministrator)
            {
                Submit.Text = "Save Changes";
            }
            if (!IsPostBack)
            {
                //to load hotel info
                LoadHotelDetails();

            }

        }


        /// <summary>
        /// Update button click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Update_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }
            ScriptManager.RegisterStartupScript(UpdatePanel1, UpdatePanel1.GetType(), "javascript", "setflag1();", true);

            Submit.Enabled = false;
            if (currentUserRole.ToLower().Contains(Constants.accountManagement) || currentUserRole.ToLower().Contains(Constants.marketing) || currentUserRole.ToLower().Contains(Constants.IT) || currentUserRole.ToLower().Contains(Constants.superUser))
            {
                updateLblMsg.Visible = true;
                updateLblMsg.Text = "Changes Updated Successfully";
            }
            else
            {
                updateLblMsg.Visible = true;
                updateLblMsg.Text = "Your changes will be approved by one of our Hotel Supply Executives";
            }
            
            var currentSupplierItem = SitecoreUserProfile.GetItem(UserProfileKeys.currentSupplierItemForUpdate);
            var currentContact = SitecoreUserProfile.GetItem(UserProfileKeys.currentContactItemForUpdate);
            int itemCount = 0;

            if (currentSupplierItem != null)
            {
                Item getSupplierItem1 = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(currentSupplierItem));

                UrlString url = new UrlString(); 
                NameValueCollection nvcol = new NameValueCollection();
                                 
                // Get latest version of the item after adding version

                nvcol.Add(string.Format("{0}{1}", Constants.itemId, itemCount), Convert.ToString(getSupplierItem1.ID));

                if (!string.IsNullOrEmpty(txtAddress.Text))
                {
                    nvcol.Add(Constants.addressLine1, txtAddress.Text);
                }
                else
                {
                    nvcol.Add(Constants.addressLine1, string.Empty);
                }

                if (!string.IsNullOrEmpty(txtTelephone.Text))
                {
                    nvcol.Add(Constants.phone, txtTelephone.Text);
                }
                else
                {
                    nvcol.Add(Constants.phone, string.Empty);
                }

                if (!string.IsNullOrEmpty(txtFax.Text))
                {
                    nvcol.Add(Constants.fax, txtFax.Text);
                }
                else
                {
                    nvcol.Add(Constants.fax, string.Empty);
                }

                if (!string.IsNullOrEmpty(txtHotelEmail.Text))
                {
                    nvcol.Add(Constants.emailID1, txtHotelEmail.Text);  
                }
                else
                {
                    nvcol.Add(Constants.emailID1, string.Empty);
                }
                
                if (currentUserRole != null)
                {
                    FetchRepeaterValue(nvcol);                                      
                }

                url.Append(nvcol);
                PropertyHelper propertyHelper = new PropertyHelper();
                propertyHelper.SendPropertyInfoMail(url.ToString());             

            }
        }


        /// <summary>
        ///Cancel Click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            string Url = Request.UrlReferrer.ToString();
            if (Url != null)
                Response.Redirect(Url);
        }

        #region private methods

        /// <summary>
        /// Mapping sitecore field values
        /// </summary>
        /// <param name="item"></param>
        private void MappingValues(Item item)
        {
            if (item != null)
            {
                PropNameLabel.Text = item.Fields[Constants.name] != null ? item.Fields[Constants.name].Value : string.Empty;
                txtAddress.Text = item.Fields[Constants.addressLine1] != null ? item.Fields[Constants.addressLine1].Value : string.Empty;
                txtTelephone.Text = item.Fields[Constants.phone] != null ? item.Fields[Constants.phone].Value : string.Empty;
                txtFax.Text = item.Fields[Constants.fax] != null ? item.Fields[Constants.fax].Value : string.Empty;
                txtHotelEmail.Text = item.Fields[Constants.emailID1] != null ? item.Fields[Constants.emailID1].Value : string.Empty;
                MultilistField multilistField = item.Fields[Constants.contactsList];
                if (multilistField != null)
                {
                    //to map sitecore contact fields
                    MappingContacts(item, multilistField);
                }
            }


        }

        /// <summary>
        ///Load Hotel Details
        /// </summary>     
        private void LoadHotelDetails()
        {
            SelectedHotelID = SitecoreUserProfile.GetItem(UserProfileKeys.dropDownSelectedValue);
            if (!String.IsNullOrWhiteSpace(SelectedHotelID))
            {
                SelectedHotelItem = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(SelectedHotelID));
                SelectedHotelDisplayName =SelectedHotelItem!=null?SitecoreFieldsHelper.GetValue(SelectedHotelItem, "Name"):"";
            }
            LoggedInSupplierID = SitecoreUserProfile.GetItem(UserProfileKeys.loggedInProviderId);
            if (!String.IsNullOrWhiteSpace(LoggedInSupplierID))
            {
                LoggedInSupplierItem = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(LoggedInSupplierID));
                if (LoggedInSupplierItem != null)
                {
                    LoggedInSupplierDisplayName = SitecoreFieldsHelper.GetValue(LoggedInSupplierItem, "Name");
                }
                if (currentUserRole != null && currentUserRole.ToLower().Contains(Constants.grpHotelRole))
                {
                    SetVisibilitytoPropInfoTextBox(false);
                    groupSection.Visible = true;

                    groupHotelNameLabel.Text = LoggedInSupplierDisplayName; //SitecoreUserProfile.GetItem(UserProfileKeys.loggedinSupplierDisplayName);

                    if (!String.IsNullOrWhiteSpace(SelectedHotelID) && !String.IsNullOrWhiteSpace(SelectedHotelDisplayName))
                    {
                        FillPropInfo(SelectedHotelID, SelectedHotelDisplayName);
                    }
                }
                else if (currentUserRole != null && (currentUserRole.ToLower().Contains(Constants.hotelRole) || currentUserRole.ToLower().Contains(Constants.hotelSupervisor)))
                {
                    propInfoContent.Visible = true;
                    FillPropInfo(LoggedInSupplierID, LoggedInSupplierDisplayName);

                    if (currentUserRole.ToLower().Contains(Constants.hotelRole))
                    {
                    }
                    else
                    {
                        SetVisibilitytoPropInfoTextBox(false);
                    }
                }
            }

            else if (currentUserRole != null && (currentUserRole.ToLower().Contains(Constants.lbAdminRole) || currentUserRole.ToLower().Contains(Constants.superUser) || currentUserRole.ToLower().Contains(Constants.accountManagement) || currentUserRole.ToLower().Contains(Constants.marketing) || currentUserRole.ToLower().Contains(Constants.IT) || currentUserRole.ToLower().Contains(Constants.finance)))
            {
                if (currentUserRole.ToLower().Contains(Constants.finance))
                {
                }
                else
                {
                    SetVisibilitytoPropInfoTextBox(false);
                }
                var TypeDropDownSelectedValue = SitecoreUserProfile.GetItem(UserProfileKeys.typeDropDownSelectedValue);
                if (TypeDropDownSelectedValue != null && TypeDropDownSelectedValue.Equals("Group"))
                {
                    groupSection.Visible = true;
                    groupHotelNameLabel.Text = SitecoreUserProfile.GetItem(UserProfileKeys.groupListBoxSelectedText);

                    if (!String.IsNullOrWhiteSpace(SelectedHotelID) && !String.IsNullOrWhiteSpace(SelectedHotelDisplayName))
                    {
                        propInfoContent.Visible = true;
                        FillPropInfo(SelectedHotelID, SelectedHotelDisplayName);

                    }
                }

                else if (TypeDropDownSelectedValue != null && TypeDropDownSelectedValue.Equals("Independent"))
                {
                    var selectedSupplier = SitecoreUserProfile.GetItem(UserProfileKeys.listBoxSelectedValue);
                    if (!String.IsNullOrWhiteSpace(selectedSupplier))
                    {

                        var selectedSupplierDisplayName = SitecoreUserProfile.GetItem(UserProfileKeys.listBoxSelectedText);
                        propInfoContent.Visible = true;
                        FillPropInfo(selectedSupplier, selectedSupplierDisplayName);
                    }


                }
            }

        }

        /// <summary>
        /// Mapping contacts
        /// </summary>
        /// <param name="item"></param>
        /// <param name="multilistField"></param>
        private void MappingContacts(Item item, MultilistField multilistField)
        {
            if (multilistField.TargetIDs != null)
            {
                if (currentUserRole != null)
                {
                    if (currentUserRole.ToLower().Equals(Constants.hotelRole) || currentUserRole.ToLower().Equals(Constants.hotelSupervisor))
                    {
                        foreach (ID id in multilistField.TargetIDs)
                        {
                            Item targetItem = Sitecore.Context.Database.Items[id];
                            var contactNameID = SitecoreUserProfile.GetItem(UserProfileKeys.loggedInContactID);

                            if (targetItem != null && !String.IsNullOrWhiteSpace(contactNameID) && targetItem.ID.ToString().Equals(contactNameID, StringComparison.InvariantCultureIgnoreCase))
                            {
                                string sessionHotel = Convert.ToString(targetItem.ID);
                                SitecoreUserProfile.AddItem(sessionHotel, UserProfileKeys.currentContactItemForUpdate);
                                ContactFieldsMapping(targetItem);
                                break;

                            }
                        }

                        if (listContactDetails != null)
                        {
                            if (currentUserRole.ToLower().Equals(Constants.hotelRole))
                            {
                                contactRepeater.DataSource = listContactDetails;
                                contactRepeater.DataBind();
                            }
                            else if (currentUserRole.ToLower().Equals(Constants.hotelSupervisor))
                            {
                                contactRepeater.DataSource = listContactDetails;
                                contactRepeater.DataBind();
                                SetVisibilitytoContactTextBox(false);
                            }
                        }

                    }
                    else if (currentUserRole.ToLower().Equals(Constants.grpHotelRole) || currentUserRole.ToLower().Equals(Constants.lbAdminRole) || currentUserRole.ToLower().Contains(Constants.superUser) || currentUserRole.ToLower().Contains(Constants.accountManagement) || currentUserRole.ToLower().Contains(Constants.marketing) || currentUserRole.ToLower().Contains(Constants.IT) || currentUserRole.ToLower().Contains(Constants.finance))
                    {
                        if (multilistField.TargetIDs.Count() > 0)
                        {
                            List<ID> myList = new List<ID>(multilistField.TargetIDs);

                            List<ID> firstFiveItems = multilistField.TargetIDs.ToList();
                            if (multilistField.TargetIDs.Count() > 5)
                            {
                                firstFiveItems.Take(6);
                                GroupContactMapping(firstFiveItems);

                            }
                            else
                            {
                                GroupContactMapping(firstFiveItems);
                            }

                        }
                    }
                }
            }
        }

        /// <summary>
        /// Add Version And UpdateField
        /// </summary>
        /// <param name="item"></param>
        /// <param name="fieldToUpdate"></param>
        /// <param name="valueToUpdate"></param>
        /// <returns></returns>
        private Item AddVersionAndUpdateField(Item item, string fieldToUpdate, string valueToUpdate)
        {
            if (!this.isVersionAddedForItem)
            {
                AssignWorkFlow.AddVersionToItem(item);
                //Get the item with updated version.
                item = Sitecore.Context.Database.GetItem(item.ID);
                this.isVersionAddedForItem = true;
            }

            item.Editing.BeginEdit();
            item.Fields[fieldToUpdate].Value = valueToUpdate;
            return item;
        }

        /// <summary>
        /// Fill Prop Info
        /// </summary>
        /// <param name="hotelId"></param>
        /// <param name="hotelDisplayName"></param>
        private void FillPropInfo(string hotelId, string hotelDisplayName)
        {
            HotelName.Visible = true;
            HotelNameLabel.Visible = true;
            HotelName.Text = hotelDisplayName;
            if (Sitecore.Context.Database.SelectItems(Constants.hotelPath) != null)
            {
                Item Hotels = Sitecore.Context.Database.SelectItems(Constants.hotelPath).Where(x => x.ID.ToString().Equals(hotelId, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                if (Hotels != null)
                {
                    string sessionHotel = Convert.ToString(Hotels.ID);
                    SitecoreUserProfile.AddItem(sessionHotel, UserProfileKeys.currentSupplierItemForUpdate);
                    propInfoContent.Visible = true;
                    MappingValues(Hotels);
                }
            }
        }

        /// <summary>
        /// Fetch Repeater Value
        /// </summary> 
        private void FetchRepeaterValue(NameValueCollection nvcol)
        {
            
            int count=0;
            foreach (RepeaterItem item in contactRepeater.Items)
            {
                var contactNameText = item.FindControl("txtContactName") as TextBox;
                string contactNameInitialValue = contactNameText.Attributes["initialText"] != null ? contactNameText.Attributes["initialText"] : string.Empty;
                string contactNameUpdatedvalue = contactNameText.Text != null ? contactNameText.Text : string.Empty;
                
                var contactJobTitleText = item.FindControl("txtContactJobTitle") as TextBox;
                string contactJobInitialValue = contactJobTitleText.Attributes["initialText"] != null ? contactJobTitleText.Attributes["initialText"] : string.Empty;
                string contactJobUpdatedvalue = contactJobTitleText.Text != null ? contactJobTitleText.Text : string.Empty;
                
                var contactEmailText = item.FindControl("txtContactEmail") as TextBox;
                string contactEmailTextInitialValue = contactEmailText.Attributes["initialText"] != null ? contactEmailText.Attributes["initialText"] : string.Empty;
                string contactEmailTextUpdatedvalue = contactEmailText.Text != null ? contactEmailText.Text : string.Empty;
                
                var contactDirectPhoneText = item.FindControl("txtContactDirectPhone") as TextBox;
                string contactDirectPhoneInitialValue = contactDirectPhoneText.Attributes["initialText"] != null ? contactDirectPhoneText.Attributes["initialText"] : string.Empty;
                string contactDirectPhoneUpdatedvalue = contactDirectPhoneText.Text != null ? contactDirectPhoneText.Text : string.Empty;
               
                var ItemID = item.FindControl("ItemId") as Label;
                Item contactItemID = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(ItemID.Text));
                
                nvcol.Add(string.Format("{0}{1}", Constants.itemId, ++count), Convert.ToString(contactItemID.ID));
                nvcol.Add(Constants.firstName, contactNameUpdatedvalue);
                nvcol.Add(Constants.jobTitle, contactJobUpdatedvalue);
                nvcol.Add(Constants.emailAddress, contactEmailTextUpdatedvalue);
                nvcol.Add(Constants.directPhone, contactDirectPhoneUpdatedvalue);                              
            }
        }


        /// <summary>
        /// Group Contact Mapping
        /// </summary>
        /// <param name="Ids"></param>
        private void GroupContactMapping(List<ID> Ids)
        {
            foreach (var id in Ids)
            {
                Item targetItem = Sitecore.Context.Database.Items[id];
                ContactFieldsMapping(targetItem);

            }
            if (listContactDetails != null)
            {
                contactRepeater.DataSource = listContactDetails;
                contactRepeater.DataBind();
                if (currentUserRole.ToLower().Equals(Constants.finance))
                {

                }
                else
                {
                    SetVisibilitytoContactTextBox(false);
                }
            }
        }

        /// <summary>
        /// Contact Fields Mapping
        /// </summary>
        /// <param name="targetItem"></param>
        private void ContactFieldsMapping(Item targetItem)
        {
            //List<ContactDetails> listContactDetails = new List<ContactDetails>();
            if (targetItem != null)
            {
                ContactDetails contactDetails = new ContactDetails();
                contactDetails.ItemId = targetItem.ID.ToString();
                contactDetails.ContactName = targetItem.Fields[Constants.firstName] != null ? targetItem.Fields[Constants.firstName].Value : string.Empty;
                contactDetails.JobTitle = targetItem.Fields[Constants.jobTitle] != null ? targetItem.Fields[Constants.jobTitle].Value : string.Empty;
                contactDetails.ContactEmail = targetItem.Fields[Constants.emailAddress] != null ? targetItem.Fields[Constants.emailAddress].Value : string.Empty;
                contactDetails.ContactDirectPhone = targetItem.Fields[Constants.directPhone] != null ? targetItem.Fields[Constants.directPhone].Value : string.Empty;
                listContactDetails.Add(contactDetails);
            }

        }

        /// <summary>
        /// Set Visibility to Prop Info TextBox
        /// </summary>
        /// <param name="IsReadonly"></param>
        private void SetVisibilitytoPropInfoTextBox(bool IsReadonly)
        {
            if (!IsReadonly)
            {
                Submit.Visible = true;
            }
            PropNameLabel.ReadOnly = IsReadonly;
            txtAddress.ReadOnly = IsReadonly;
            txtTelephone.ReadOnly = IsReadonly;
            txtFax.ReadOnly = IsReadonly;
            txtHotelEmail.ReadOnly = IsReadonly;

        }

        /// <summary>
        /// Set Visibility to contact Info TextBox
        /// </summary>
        /// <param name="IsReadonly"></param>
        private void SetVisibilitytoContactTextBox(bool IsReadonly)
        {
            if (!IsReadonly)
            {
                Submit.Visible = true;
            }
            foreach (RepeaterItem item in contactRepeater.Items)
            {
                var contactNameText = item.FindControl("txtContactName") as TextBox;
                contactNameText.ReadOnly = IsReadonly;
                var contactJobTitleText = item.FindControl("txtContactJobTitle") as TextBox;
                contactJobTitleText.ReadOnly = IsReadonly;
                var contactEmailText = item.FindControl("txtContactEmail") as TextBox;
                contactEmailText.ReadOnly = IsReadonly;
                var contactDirectPhoneText = item.FindControl("txtContactDirectPhone") as TextBox;
                contactDirectPhoneText.ReadOnly = IsReadonly;
            }

        }
        #endregion

    }


}








