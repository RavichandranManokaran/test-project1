﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalForgotPasswordSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalForgotPasswordSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="body-container">
    <div class="container">
        <div class="form-outer">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <strong>
                                <sc:Text Field="FormTitle" runat="server" />
                            </strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="alert alert-success" runat="server" id="divsuccess" visible="false">
                            <asp:Literal ID="literalSuccess" runat="server" />
                        </div>
                        <div runat="server" class="alert alert-danger" id="divError" visible="false">
                            <asp:Literal ID="literalMessage" runat="server" />
                        </div>
                        <asp:Panel runat="server" ID="PanelEdit">
                            <div class="form-group">
                                <sc:Text Field="UserName" runat="server" />
                                <asp:TextBox ID="TextBoxUser" runat="server" class="form-control" placeholder="Username" required="" ViewStateMode="Disabled" />
                            </div>
                            <asp:Button ID="ButtonForgot" Text="Submit" OnClick="ButtonForgot_Click" runat="server" class="btn btn-sm btn-default" />
                            <button type="cancel" value="cancel" onclick="window.history.back();" class="btn btn-sm btn-default">Cancel</button>
                        </asp:Panel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
