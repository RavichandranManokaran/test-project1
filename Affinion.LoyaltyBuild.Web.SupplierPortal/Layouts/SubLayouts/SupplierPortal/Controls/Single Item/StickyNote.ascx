﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StickyNote.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Single_Item.StickyNote" %>

<asp:Panel runat="server" ID="pnlStickyNote">
 <h2><sc:FieldRenderer ID="frTitle" runat="server" FieldName="Title" /></h2>
 <sc:FieldRenderer ID="frBody" runat="server" FieldName="Message" />
</asp:Panel>