﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientPortalSliderSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortalSliderSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Sitecore.Data.Items" %>

<script type="text/javascript">
    jQuery(function () {
        jQuery("#collapse-button").click(function () {
            jQuery("#show-hide").toggle();
        });
        <%--var sliderinterval =<%=this.interval%>;--%>
        var sliderinterval =<%=this.Timeinterval%>;
        jQuery('.bxslider').bxSlider({ auto: true, autoControls: true, pause:sliderinterval});
    })
</script>

    <!-- Jumbotron -->
    <div class="row slider">
        <div class="col-md-12">
            <div class="slider-top">
                <div id="bb1" class="LBranding">
                    <div class="container container-inner-relative">
                        <div class="book-top">

                            <sc:Placeholder ID="stocksearch" Key="stocksearch" runat="server" />
                        </div>
                    </div>
                    <div class="sliderOuter">
                        <ul class="bxslider">
                            <asp:Repeater ID="ImageSlider" runat="server">
                                <ItemTemplate>
                                    <li>
                                        <sc:Image Field="Image" CssClass="img-responsive img-enlarge" Item="<%#(Item)Container.DataItem %>" runat="server" />
                                        <div class="slider-detail">
                                            <sc:FieldRenderer  FieldName="BannerContent" Item="<%#(Item)Container.DataItem %>" runat="server" />
                                        </div>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>



