﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Model.Provider
{
    /// <summary>
    /// Enum to return the type of provider type
    /// </summary>
    [Serializable]
    public enum ProviderType
    {
        LoyaltyBuild,
        BedBanks,
        Mixed
    }
}
