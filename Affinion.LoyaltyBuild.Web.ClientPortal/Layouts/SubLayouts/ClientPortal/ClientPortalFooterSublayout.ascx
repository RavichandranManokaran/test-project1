﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientPortalFooterSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal.ClientPortalFooterSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Sitecore.Data.Items" %>

<!-- Site footer -->

<div class="row footer-bottom">
    <div class="footer">
        <div class="container-outer-inner width-full footer-bottom-bar">
            <div class="container">

                <%-- <div class="col-sm-4 footer-left-area-top">
                    <div class="footer-left-area">
                        <h2 class="no-margin">Information</h2>
                        <ul class="no-padding">
                            <li><a href="#"><i class="fa fa-arrow-right"></i>HOW TO BOOK </a></li>
                            <li><a href="#"><i class="fa fa-arrow-right"></i>FAQ'S </a></li>
                            <li><a href="#"><i class="fa fa-arrow-right"></i>MY BOOKINGS  </a></li>
                            <li><a href="#"><i class="fa fa-arrow-right"></i>CONTACT US  </a></li>
                            <li><a href="#"><i class="fa fa-arrow-right"></i>TERMS AND CONDITIONS</a></li>
                        </ul>
                    </div>
                </div>--%>

                <sc:Placeholder ID="Placeholder1" Key="footerlinkblock" runat="server" />

                <%--<div class="col-sm-4">
                    <div class="footer-middle-area">
                        <h2 class="no-margin">
                            <asp:Literal ID="PlaceholderText" Text="Set the datasource: Required Fields: Title, Body, Items" runat="server" />
                            <sc:Text Field="Title" ID="Title" runat="server" />
                        </h2>
                        <sc:FieldRenderer FieldName="Body" ID="Body" runat="server" />
                        <asp:Repeater ID="LinksBlock" runat="server">

                            <HeaderTemplate>
                                <ul class="no-padding">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li>                                    
                                        <sc:Link Field="Link" ID="Link" Item="<%#(Item)Container.DataItem %>" runat="server" />
                                </li>
                            </ItemTemplate>
                            <FooterTemplate>
                                </ul>
                            </FooterTemplate>

                        </asp:Repeater>
                    </div>
                </div>--%>

                <%-- <div class="col-sm-4">
                    <div class="footer-middle-area">
                       <h2 class="no-margin">Follow Us</h2>
                        <div class="social-footer">
                            <a href="#" class="float-left "><span class="fa fa-facebook"></span></a>
                            <a href="#" class="float-left "><span class="fa fa-twitter"></span></a>
                            <a href="#" class="float-left "><span class="fa fa-google-plus"></span></a>
                            <a href="#" class="float-left no-margin"><span class="fa fa-skype"></span></a>
                        </div>
                    </div>
                </div>--%>
            </div>
        </div>
    </div>
</div>


<div class="container-outer-inner width-full pay-icon">
    <div class="container">
        <div class="col-sm-12">
            <div class="clearfix">
                <div class="payment-icon">
                    <sc:Placeholder ID="paylogo" Key="paylogo" runat="server" CssClass="img-responsive" />
                    <%-- <img src="images/pay-logo.jpg" alt=""  class="img-responsive" />--%>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-outer-inner width-full copyright-bar">
    <div class="container">
        <div class="copyright-text">
            <p>
                <sc:Placeholder ID="copyrightblock" Key="copyrightblock" runat="server" CssClass="img-responsive" />
            </p>
        </div>
    </div>
</div>

<div class="scroll-top-wrapper ">
    <span class="scroll-top-inner"></span>
</div>
