﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using FluentNHibernate.Conventions.Inspections;
using FluentNHibernate.Conventions.Instances;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Event;
using NHibernate.Tool.hbm2ddl;
using UCommerce.EntitiesV2;
using UCommerce.EntitiesV2.Listeners;
using UCommerce.Extensions;
using UCommerce.Infrastructure;
using UCommerce.Infrastructure.Configuration;
using UCommerce.Security;
using ForeignKey = FluentNHibernate.Conventions.Helpers.ForeignKey;
using ISession = NHibernate.ISession;
using PrimaryKey = FluentNHibernate.Conventions.Helpers.PrimaryKey;
using Table = FluentNHibernate.Conventions.Helpers.Table;
using UCommerce.EntitiesV2;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    /// <summary>
    /// Provides session management plumbing for NHibernate.
    /// </summary>
    public class SessionProvider : ISessionProvider, IDisposable
    {
        private readonly IUserService _userService;
        private readonly IEnumerable<IContainsNHibernateMappingsTag> _mapAssemblyTags;
        private CommerceConfigurationProvider CommerceConfigurationProvider { get; set; }
        internal ISession _session;
        internal IStatelessSession _statelessSession;
        private static bool _rebuildSchema;
        private static object _padLock = new object();
        internal static ISessionFactory _factory;
        internal static bool _inTest;

        public SessionProvider()
            : this(
                ObjectFactory.Instance.Resolve<CommerceConfigurationProvider>(),
                ObjectFactory.Instance.Resolve<IUserService>(),
                ObjectFactory.Instance.ResolveAll<IContainsNHibernateMappingsTag>())
        { }

        public SessionProvider(
            CommerceConfigurationProvider commerceConfiguration,
            IUserService userService,
            IEnumerable<IContainsNHibernateMappingsTag> mapAssemblyTags)
        {
            _userService = userService;
            _mapAssemblyTags = mapAssemblyTags;
            CommerceConfigurationProvider = commerceConfiguration;

            if (_mapAssemblyTags == null) _mapAssemblyTags = new List<IContainsNHibernateMappingsTag>();
        }

        #region Implementation of ISessionProvider

        public ISession GetSession()
        {
            if (_factory == null)
                lock (_padLock)
                {
                    if (_factory == null) // double check variable in case blocked calls were waiting to get in
                        _factory = CreateSessionFactory(
                            CommerceConfigurationProvider.GetRuntimeConfiguration().EnableCache,
                            CommerceConfigurationProvider.GetRuntimeConfiguration().CacheProvider);
                }

            if (_session == null || !_session.IsOpen)
            {
                lock (_padLock)
                {
                    if (_session == null || !_session.IsOpen)
                    {
                        _session = _factory.OpenSession();
                    }
                }
            }

            return _session;
        }

        public IStatelessSession GetStatelessSession()
        {
            if (_factory == null)
            {
                lock (_padLock)
                {
                    if (_factory == null)
                    {
                        _factory = CreateSessionFactory(
                            CommerceConfigurationProvider.GetRuntimeConfiguration().EnableCache,
                            CommerceConfigurationProvider.GetRuntimeConfiguration().CacheProvider);
                    }
                }
            }

            if (_statelessSession == null || !_statelessSession.IsOpen)
            {
                lock (_padLock)
                {
                    if (_statelessSession == null || !_statelessSession.IsOpen)
                    {
                        _statelessSession = _factory.OpenStatelessSession();
                    }
                }
            }

            return _statelessSession;
        }

        #endregion

        internal ISessionFactory CreateSessionFactory(bool enableCache, string cacheProvider)
        {
            try
            {
                var factory = Fluently.Configure()
                    .Database(MsSqlConfiguration.MsSql2008
                                .Raw("connection.isolation", "ReadUncommitted")
                                .ConnectionString(CommerceConfigurationProvider.GetRuntimeConfiguration().ConnectionString)
                                .UseReflectionOptimizer() // Will not work in medium trust environments
                                .AdoNetBatchSize(200))
                    .Cache(c =>
                    {
                        if (enableCache)
                            c
                                .UseQueryCache()
                                .UseSecondLevelCache()
                                .ProviderClass(cacheProvider);
                    })
                    .Mappings(m =>
                    {
                        m.FluentMappings
                            .AddFromTaggedAssemblies(_mapAssemblyTags.Reverse()) // Reverse the list to add user defined maps first.
                            .Conventions.Add(Table.Is(x => "uCommerce_" + x.EntityType.Name))
                            .Conventions.Add(PrimaryKey.Name.Is(FormatPrimaryKey))
                            .Conventions.Add(ForeignKey.Format(FormatForeignKey))
                            .Conventions.AddFromAssemblyOf<IdPropertyConvention>();
                        m.HbmMappings.AddFromAssemblyOf<UCommerce.EntitiesV2.SessionProvider>();

                        // TSD :  This adds the Audit table mappings to NHibernate 
                        // TSD : Not required if Auditing is going to be stored elsewhere.
                    }
                            )
                    .ExposeConfiguration(BuildSchema)
                    .ExposeConfiguration(c =>
                    {
                        // http://ronaldrosiernet.azurewebsites.net/Blog/2013/04/20/timeout_in_nhibernate_batched_sessions

                        // This will set the command_timeout property on factory-level
                        c.SetProperty(NHibernate.Cfg.Environment.CommandTimeout, "6000");
                        // This will set the command_timeout property on system-level
                        NHibernate.Cfg.Environment.Properties.Add(NHibernate.Cfg.Environment.CommandTimeout, "6000");
                    })
                    // TSD : Hook up custom auditing listeners
                    .ExposeConfiguration(AddAuditing)
                    .BuildSessionFactory();

                return factory;
            }
            catch (Exception)
            {
                throw;
            }
        }

        // TSD : Add auditing listeners
        private void AddAuditing(Configuration config)
        {
            // TSD : You will need to create listeners for each event you are interested in
            config.SetListener(ListenerType.PostUpdate, new AuditUpdateListener(_userService));
            config.SetListener(ListenerType.PostInsert, new AuditInsertListener(_userService));
            config.SetListener(ListenerType.PostDelete, new AuditDeleteListener(_userService));
        }
        private static string FormatPrimaryKey(IIdentityInspector arg)
        {
            var result = arg.EntityType.Name + "Id";
            return result;
        }

        private static string FormatForeignKey(FluentNHibernate.Member property, Type type)
        {
            var result = type.Name + "Id";
            return result;
        }

        private void BuildSchema(Configuration obj)
        {
            ApplyEventListeners(obj);

            if (!_rebuildSchema)
                return;

            Console.WriteLine("Rebuilding schema...");

            new SchemaExport(obj)
                .Create(false, true);

            Console.WriteLine("Done rebuilding schema!");
        }

        internal void ApplyEventListeners(Configuration obj)
        {
            obj.SetListener(ListenerType.Delete, new DeleteEventListenerAggragator(
                                                            new List<IEntityDeleteEventListener>
                                                                {
                                                                    new SoftDeleteEventListener(),
                                                                    new ProductDeleteEventListener(),
                                                                    new OrderLineDeleteEventListener()
                                                                }
                                                            ));

            obj.SetListeners(ListenerType.PreInsert, new IPreInsertEventListener[]
            {
                new AuditEventListener(_userService),
                new GuidEventListener()
            });

            obj.SetListener(ListenerType.PreUpdate, GetInsertUpdateEventListener());
        }

        private AuditEventListener GetInsertUpdateEventListener()
        {
            if (_inTest)
                return new TestableAuditEventListener();

            return new AuditEventListener(_userService);
        }

        #region Implementation of IDisposable

        public void Dispose()
        {
            if (_session != null)
                _session.Dispose();

            if (_statelessSession != null)
                _statelessSession.Dispose();
        }

        #endregion
    }

    /// <summary>
    /// Adding Generated.Insert() and ReadOnly() to all properties which names ends with "Id" and is mapped with "Map".
    /// This tells NHibernate that all these properties values is generated on insert, so they now automatically be updated on save.
    /// </summary>
    public class IdPropertyConvention : IPropertyConvention, IPropertyConventionAcceptance
    {
        public void Accept(IAcceptanceCriteria<IPropertyInspector> criteria)
        {
            criteria.Expect(x => x.Name.EndsWith("Id", false, null));
        }

        public void Apply(IPropertyInstance instance)
        {
            instance.Generated.Insert();
        }
    }

    /// <summary>
    /// Makes sure that soft deleted entities in one to many relationships are not loaded.
    /// </summary>
    public class HasManyConvention : IHasManyConvention
    {
        public void Apply(IOneToManyCollectionInstance instance)
        {
            if (instance.ChildType.GetInterfaces().Where(x => x == typeof(ISoftDeletableEntity)).Count() == 1)
                instance.Where("Deleted = 0");
        }
    }

    /// <summary>
    /// Makes sure that soft deleted entities in many to many relationships are not loaded.
    /// </summary>
    public class HasManyToManyConvention : IHasManyToManyConvention
    {
        public void Apply(IManyToManyCollectionInstance instance)
        {
            if (instance.ChildType.GetInterfaces().Where(x => x == typeof(ISoftDeletableEntity)).Count() == 1)
                instance.Relationship.Where("Deleted = 0");
        }
    }
}