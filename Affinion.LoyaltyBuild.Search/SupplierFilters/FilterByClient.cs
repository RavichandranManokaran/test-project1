﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           FilterByClient.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Search (VS project name)
/// Description:           Filter search results by client assignment
/// </summary>
namespace Affinion.LoyaltyBuild.Search.SupplierFilters
{

    #region Using Statements

    using Affinion.LoyaltyBuild.Common.Utilities;

    #endregion

    public class FilterByClient : ISupplierFilter
    {
        #region Public Methods

        /// <summary>
        /// Returns true to include the supplier in search results
        /// </summary>
        /// <param name="filterData">The filter data</param>
        /// <returns>Returns true to include the supplier in search results</returns>
        public bool IncludeSupplier(SupplierFilterData filterData)
        {
            if (filterData == null)
            {
                return false;
            }

            return SitecoreFieldsHelper.ContainsIn(filterData.Client, "Name", filterData.ResultsItem.Clients);
        }
        
        #endregion
    }
}
