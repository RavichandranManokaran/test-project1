namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.PrimaryLayouts
{
    //<summary>
    /// � 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
    /// PROPRIETARY INFORMATION The information contained herein (the 
    /// 'Proprietary Information') is highly confidential and proprietary to and 
    /// constitutes trade secrets of Affinion International. The Proprietary Information 
    /// is for Affinion International use only and shall not be published, 
    /// communicated, disclosed or divulged to any person, firm, corporation or 
    /// other legal entity, directly or indirectly, without the prior written 
    /// consent of Affinion International.
    ///
    /// Source File:           clientPortalPrimaryLayout.cs
    /// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal
    /// Description:           
    /// </summary>
    #region Using Directives
    using Affinion.LoyaltyBuild.Common.BaseLayouts;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    #endregion

    public partial class ClientPortalPrimaryLayout : BasePrimaryLayout
    {
        /// <summary>
        /// Gets the main form
        /// </summary>
        public override HtmlForm MainForm
        {
            get
            {
                return null;
            }
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
        }
    }
}
