﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sitecore.Data.Items;

namespace Affinion.LoyaltyBuild.BedBanks.General
{
    public class BedBanksCurrency
    {
        /// <summary>
        /// Bed Bank Currency item
        /// </summary>
        public Item BedBankCurrencyItem { get; set; }
        
        /// <summary>
        /// Name of the currency
        /// </summary>
        public string CurrencyName { get; set; }

        /// <summary>
        /// Bed Banks Id for the currency
        /// </summary>
        public string BedBanksCurrencyId { get; set; }

        /// <summary>
        /// uCommerce Id for the Currency
        /// </summary>
        public string uCommerceCurrencyId { get; set; }

        /// <summary>
        /// MaxPriceForHotels 
        /// </summary>
        public string MaxPriceForHotels { get; set; }
    }
}
