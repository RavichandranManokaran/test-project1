﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class ArrivalDateTargetMap : SubclassMap<ArrivalDateTarget>
    {
        // Methods
        public ArrivalDateTargetMap()
        {
            base.Table("LB_ArrivalDateTarget");
            base.KeyColumn("ArrivalDateTargetId");
            base.Map(x => x.ArrivalDateTargetId).ReadOnly();
            base.Map(x => x.ArrivalFromDate).Not.Nullable();
            base.Map(x => x.ArrivalToDate).Not.Nullable();
        }

    }

}
