﻿<%@ control language="C#" autoeventwireup="true" codebehind="OfflinePortalAddCustQuery.ascx.cs" inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalAddCustQuery" %>
<%@ register tagprefix="sc" namespace="Sitecore.Web.UI.WebControls" assembly="Sitecore.Kernel" %>

<div class="panel-group" id="accordion">
    <div class="panel panel-default">
        <div class="panel-heading bg-primary">
            <h4 class="panel-title">Customer Enquiry Detail
            </h4>
        </div>
        <div id="collapse0" class="panel-collapse collapse in">
            <div class="panel-body alert-info">

                <div class="col-sm-12 no-padding-m">
                    <div class="content_bg_inner_box alert-info">
                        <div class="col-sm-6 no-padding-m">

                            <div class="row margin-row">
                                <div class="col-sm-5">
                                    <span class="bask-form-titl font-size-12">Query Type:<span class="accent58-f"> *</span>
                                    </span>
                                </div>
                                <div class="col-sm-7">
                                    <%-- Query Type:--%>
                                    <asp:dropdownlist id="ddlQueryType" cssclass="form-control font-size-12" runat="server">
                                    </asp:dropdownlist>
                                </div>
                            </div>

                            <div class="row margin-row">
                                <div class="col-sm-5">
                                    <span class="bask-form-titl font-size-12">Partner:<span class="accent58-f">*</span></span>
                                </div>
                                <div class="col-sm-7">
                                    <asp:dropdownlist id="ddlPartner" runat="server" cssclass="form-control font-size-12"></asp:dropdownlist>
                                </div>
                            </div>

                            <div class="row margin-row">
                                <div class="col-sm-5"><span class="bask-form-titl font-size-12">Compaign Group:</span></div>
                                <div class="col-sm-7">
                                    <asp:dropdownlist clientidmode="Static" cssclass="form-control font-size-12" id="ddlCampaignGroup" runat="server"></asp:dropdownlist>
                                </div>
                            </div>

                            <div class="row margin-row">
                                <div class="col-sm-5"><span class="bask-form-titl font-size-12">Provider:</span></div>
                                <div class="col-sm-7">
                                    <asp:dropdownlist clientidmode="Static" cssclass="form-control font-size-12" id="ddlProvider" runat="server"></asp:dropdownlist>
                                </div>
                            </div>

                            <div class="row margin-row">
                                <div class="col-sm-5"><span class="bask-form-titl font-size-12">Is Resolved:</span><span class="accent58-f"> *</span></div>
                                <div class="col-sm-7">
                                    <asp:radiobuttonlist id="IsSolved" cssclass="font-size-10" runat="server" width="100" cellpadding="0" borderstyle="None" repeatdirection="Horizontal">
                                        <asp:ListItem Text=" Yes " Value="Yes"></asp:ListItem>
                                        <asp:ListItem Text=" No " Value="No" Selected="True"></asp:ListItem>
                                    </asp:radiobuttonlist>
                                </div>
                            </div>

                            <div class="row margin-row pull-right col-sm-6" style="padding: 0;">
                                <div class="col-sm-6">
                                    <asp:button runat="server" id="btnAddNotes"  text="Insert" class="btn btn-default add-break pull-right width-full search-btn"  />
                                </div>
                                <div class="col-sm-6">
                                    <button class="btn btn-default add-break pull-right width-full" type="reset">Reset</button>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 no-padding-m">
                            <div class="row margin-row">
                                <div class="col-sm-3">
                                    <span class="bask-form-titl font-size-12">Notes:<span class="accent58-f"> *</span></span>
                                </div>
                                <div class="col-sm-9">
                                    <textarea id="Notes" runat="server" class="form-control font-size-12" style="min-height: 100px; min-width: 200px"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel-group" id="searchDiv">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">Customer Validation</h4>
        </div>
        <div class="panel-body alert-info">
            <div class="col-sm-12 no-padding-m">
                <div class="content_bg_inner_box alert-info">
                    <div class="col-sm-6 no-padding-m">
                        <div class="row margin-row">
                            <div class="col-sm-5">
                                <span class="bask-form-titl font-size-12">Client Firstname:<span class="accent58-f"> *</span></span>
                            </div>
                            <div class="col-sm-7">
                                <input type="text" id="txtFirstName" class="form-control font-size-12" placeholder="First Name">
                            </div>
                        </div>
                        <div class="row margin-row">
                            <div class="col-sm-5">
                                <span class="bask-form-titl font-size-12">Client Lastname:<span class="accent58-f"> * </span></span>
                            </div>
                            <div class="col-sm-7">
                                <input type="text" id="txtLastName" class="form-control font-size-12" placeholder="Last Name">
                            </div>
                        </div>
                        <div class="row margin-row">
                            <div class="col-sm-5"><span class="bask-form-titl font-size-12">County:</span></div>
                            <div class="col-sm-7">
                                <asp:dropdownlist id="ddlCountry" runat="server" cssclass="form-control font-size-12"></asp:dropdownlist>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 no-padding-m">
                        <div class="row margin-row">
                            <div class="col-sm-5"><span class="bask-form-titl font-size-12">Address Line1:</span></div>
                            <div class="col-sm-7">
                                <input type="text" id="txtxAddress1" class="form-control font-size-12" placeholder="Address Line1" />
                            </div>
                        </div>
                        <div class="row margin-row">
                            <div class="col-sm-5"><span class="bask-form-titl font-size-12">Address Line2:</span></div>
                            <div class="col-sm-7">
                                <input type="text"  id="txtAddress2" class="form-control font-size-12" placeholder="Address Line2" />
                            </div>
                        </div>
                        <div class="row margin-row">
                            <div class="col-sm-5"><span class="bask-form-titl font-size-12">Primary Phone:</span></div>
                            <div class="col-sm-7">
                                <input type="text" id="txtPrimaryPhone" class="form-control font-size-12" placeholder="Phone">
                            </div>
                        </div>
                        <div class="row margin-row">
                            <div class="col-sm-5">
                                <span class="bask-form-titl font-size-12">Locations:</span>
                            </div>
                            <div class="col-sm-7">
                                <asp:dropdownlist id="txtLocations" runat="server" cssclass="form-control font-size-12"></asp:dropdownlist>
                            </div>
                        </div>

                        <div class="row margin-row pull-right col-sm-12" style="padding: 0;">
                            <div class="col-sm-4">
                                <asp:button id="ButtonSearch" runat="server" onclick="ButtonSearch_Click" validationgroup="Search" text="Search" class="btn btn-default add-break pull-right width-full search-btn" type="submit"></asp:button>
                            </div>
                            <div class="col-sm-4">
                                <asp:button id="AddClient" runat="server" onclientclick="return ShowAddClient();" causesvalidation="false" text="Add Client" class="btn btn-default add-break pull-right width-full search-btn" />
                            </div>
                            <div class="col-sm-4">
                                <input type="reset" class="btn btn-default add-break pull-right width-full" value="Reset"></input>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
