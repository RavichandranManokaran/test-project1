﻿CREATE TABLE [dbo].[Shadows] (
    [ID]        UNIQUEIDENTIFIER CONSTRAINT [DF_Shadows_ID] DEFAULT (newid()) NOT NULL,
    [ProxyID]   UNIQUEIDENTIFIER NOT NULL,
    [TargetID]  UNIQUEIDENTIFIER NOT NULL,
    [ShadowID]  UNIQUEIDENTIFIER NOT NULL,
    [DAC_Index] INT              NULL
);


GO
CREATE CLUSTERED INDEX [DAC_Clustered]
    ON [dbo].[Shadows]([DAC_Index] ASC);


GO
CREATE NONCLUSTERED INDEX [ndxProxyTarget]
    ON [dbo].[Shadows]([ProxyID] ASC, [TargetID] ASC);


GO
CREATE NONCLUSTERED INDEX [ndxTargetID]
    ON [dbo].[Shadows]([TargetID] ASC);


GO
CREATE NONCLUSTERED INDEX [ndxShadowID]
    ON [dbo].[Shadows]([ShadowID] ASC);

