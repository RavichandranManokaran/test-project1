﻿using System.Collections.Generic;
using Affinion.Loyaltybuild.BedBanks.CAPI.Model;
using System;

namespace Affinion.Loyaltybuild.BedBanks.CAPI.Message
{
    /// <summary>
    /// This is a class to hold the search Request Parameters.
    /// It will be passed to the API controller to perform the actual request.
    /// </summary>
    [Serializable]
    public class CapiSearchResponse
    {
        public string SessionId;
        public List<Hotel> HotelsList;

        public CapiSearchResponse(string pSessionid, List<Hotel> pHotelslist)
        {
            SessionId = pSessionid;
            HotelsList = pHotelslist;
        }
    }
}