﻿

DROP PROCEDURE dbo.[affinion_EmailToCallCenterAgent]
GO
CREATE PROCEDURE dbo.[affinion_EmailToCallCenterAgent]
AS
BEGIN

     DECLARE 
	        @currentDate as Datetime=CAST(DATEADD(D, -1,GETDATE()) as date)
     SELECT RR.OrderLineid as [Booking Id],
	        RR.NewOrderReference as [Booking Reference],
			RR.Bookingthrough as [Booking Method],
			CH.[From] as [ProviderEmail],
			CH.[To] as [Customer Email],
			RR.CreatedDate as [ReservationDate],
			RR.CheckIndate as[Arrival Date],
			CT.[Name] as [Audience]
			

	 From 
	      LB_uCommerce_RoomReservation RR
	      INNER JOIN uCommerce_CommunicationHistory CH ON CH.Orderlineid=RR.OrderLineId
		  INNER JOIN uCommerce_CommunicationTypes CT ON CT.CommunicationTypeId=CH.CommunicationTypeId
	 WHERE 
	      CH.OrderLineId=RR.OrderLineId AND
	      CAST(CH.CreatedDate as date)=@currentDate AND 
		  CAST (RR.createddate as date)=@currentDate AND
		  CH.[status]=0 AND
		  CH.CommunicationTypeId<>3 AND
		  CH.CommunicationTypeId<>4  AND RR.IsCancelled=0
		  
END
GO