﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    /// <summary>
    /// Attachment Detail
    /// </summary>
    public class CustomerQueryAttachment
    {
        /// <summary>
        /// File Data in Byte
        /// </summary>
        public byte[] File;

        /// <summary>
        /// AttachmentId
        /// </summary>
        public int AttachmentId { get; set; }

        /// <summary>
        /// QueryId
        /// </summary>
        public int QueryId { get; set; }

        /// <summary>
        /// FileType
        /// </summary>
        public string FileType { get; set; }

        /// <summary>
        /// FileName
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// FileContent
        /// </summary>
        public string FileContent { get; set; }
    }
}
