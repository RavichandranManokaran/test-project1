﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model
{
    public class OraclePayment
    {
        public int OrderId { get; set; }
        public string NominalCode { get; set; }
        public int CampaignAccountingID { get; set; }
        public int CurrencyID { get; set; }
        public string CurrencyCode { get; set; }
        public decimal EuroRate { get; set; }
        public int CustomerID { get; set; }
        public decimal SuggestedAmount { get; set; }
        public decimal PostedAmount { get; set; }
        public bool IsPaidToAccounts { get; set; }
        public DateTime DatePaidToAccounts { get; set; }
        public string OracleID { get; set; }
        public string OracleFileID { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedBy { get; set; }
        public int LineNumber { get; set; }
        public int IsDeleted { get; set; }
        public bool IsChecked { get; set; }
        public string ProviderID { get; set; }
        public int OrderLineID { get; set; }
        public string ClientID { get; set; }
    }
}
