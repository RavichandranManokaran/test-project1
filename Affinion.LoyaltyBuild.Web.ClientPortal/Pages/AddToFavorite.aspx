﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddToFavorite.aspx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Pages.AddToFavorite" %>


<input type="hidden" id="hidAddToFavoriteSuccess" class="hidAddToFavoriteSuccess" runat="server" />
<div class="newListWrapper">
    <div class="newListData">
        <input id="chkList<%= listId %>" type="checkbox" checked="checked" value="<%= listId %>"/>
        <asp:Label ID="lblList" runat="server" />
    </div>
</div>
