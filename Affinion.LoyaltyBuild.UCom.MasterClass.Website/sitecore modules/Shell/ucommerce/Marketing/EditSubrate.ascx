﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditSubrate.ascx.cs" Inherits="Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.Ucommerce.Marketing.EditSubrate" %>
<style>
    .rate_bg {
        width: 74.8%;
        float: left;
        background: #f0f0f0;
        font-family: Arial,sans-serif;
        font-size: 12px;
        color: #000;
    }
    .wid100 {
        width:100%;
    }

    .rate_tab {
        width: 100%;
        float: left;
    }

    .cl_rate {
        width: 30%;
        border-right: 1px dotted #999;
        float: left;
        margin-top: 2%;
        margin-bottom: 2%;
        padding-right: 3%;
        min-height: 435px;
    }

    .rate_title {
        padding-left: 5%;
    }

    .cl_tabl {
        border: 1px solid #d6d6d6 !important;
        table-layout: fixed;
        border-collapse: collapse;
        width: 95%;
        margin-left: 5%;
        text-align: left;
    }

    .clrate_thead {
        background: #969696;
        padding: 5px;
        color: #fff;
    }

        .clrate_thead th {
            padding: 15px;
        }

    .cl_tabl td {
        padding: 15px;
        border: 1px solid #d6d6d6;
    }

    .cl_tabl tbody tr:nth-child(2n+1), .sub_rate tbody tr:nth-child(2n+1) {
        background: #fff;
    }

    .cl_tabl tbody tr:nth-child(2n), .sub_rate tbody tr:nth-child(2n) {
        background: #e3e3e3;
    }

    .sub_rate {
        width: 56%;
        float: left;
        margin-top: 2%;
        margin-bottom: 2%;
        padding-right: 3%;
    }

    .subrte_form {
        float: left;
        margin-bottom: 2%;
        font-size: 13px;
        line-height: 36px;
        width: 100%;
    }

        .subrte_form label {
            float: left;
            clear: left;
            margin-left: 9%;
        }

    .frm_val {
        font-weight: 700;
        margin-left: 22%;
        float: left;
        width: 21%;
    }

    .frm_select {
        float: left;
        border: 1px solid #ccc;
        padding: 5px;
        font-size: 12px;
        font-family: arial;
        margin-bottom: 4px;
    }

    .clientname {
        margin: 0;
        margin-bottom: 4px !important;
    }

    .clearboth {
        clear: both;
    }

    .vat_value {
        border: 1px solid #999;
        font-family: arial;
        font-size: 12px;
        padding: 4px;
        width: 82%;
    }

    .ofr_btns {
        float: right;
        clear: right;
        margin-top: 2%;
    }

    .btn_save {
        background-color: #207da2 !important;
        background-image: linear-gradient(to bottom, #289bc8 0px, #207da2 100%) !important;
        border: 1px solid #207da2 !important;
        border-radius: 6px;
        box-shadow: 0 1px #5dbadf inset;
        color: #fff !important;
        cursor: pointer;
        font-family: Arial,Helvetica,sans-serif !important;
        font-size: 12px !important;
        height: 35px;
        line-height: 1.42857;
        min-width: 80px;
    }

    .subrate.clrate_thead {
        background: #5e5e5e;
        padding: 5px;
        color: #fff;
    }

    .cl_tabl th {
        background: #5e5e5e;
        padding: 5px;
        color: #fff;
    }

    .subrate.clrate_thead th {
        padding: 15px;
        border-right: 1px solid #fff;
    }

        .subrate.clrate_thead th:last-child {
            border-right: none;
        }

    .subrte_cancl {
        -moz-user-select: none;
        background-image: linear-gradient(to bottom, #f0f0f0 0%, #d9d9d9 100%);
        background-repeat: repeat-x;
        border: 1px solid #bdbdbd;
        border-radius: 6px;
        box-shadow: 0 1px #ffffff inset;
        cursor: pointer;
        display: inline-block;
        font-family: Arial,Helvetica,sans-serif;
        font-size: 12px;
        font-weight: normal;
        height: 36px;
        line-height: 1.42857;
        margin-bottom: 0;
        margin-left: 10px;
        min-width: 80px;
        outline: medium none;
        padding: 8px 12px;
        text-align: center;
        text-shadow: none;
        vertical-align: middle;
        white-space: nowrap;
    }

    .subrate-pad-25 {
        padding: 25px;
    }
    .width50 {
        width:50px;
    }
</style>
<asp:UpdatePanel ID="updSubrate" runat="server">
    <ContentTemplate>
        <div>
            <div class="row">
                <div class="col-md-12">
                    <asp:Label runat="server" ID="lblMessage" CssClass="alert-success"></asp:Label>
                    <asp:Label runat="server" ID="lblError" CssClass="alert-danger"></asp:Label>
                </div>
            </div>
            <div class="rate_tab">
                <div class="cl_rate">
                    <h3 class="rate_title">Client List</h3>
                    <asp:GridView ID="gvClientList" runat="server" AutoGenerateColumns="false" CssClass="cl_tabl" OnRowCommand="gvClientList_RowCommand" HeaderStyle-CssClass="clrate_thead">
                        <Columns>
                            <asp:BoundField ReadOnly="true" HeaderText="Client Name" DataField="Name" SortExpression="ItemName" />
                            <asp:TemplateField HeaderText="Edit" ItemStyle-Width="60px">
                                <ItemTemplate>
                                    <asp:Button runat="server" ID="btnEdit" CausesValidation="false" CommandName="edittemplate" CommandArgument='<%# Eval("ClientGUID") + "," + Eval("Name") %>' CssClass="btn btn-info" Text="Edit"></asp:Button>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <div class="sub_rate" id="dvEditRate" runat="server">
                    <asp:HiddenField runat="server" ID="ItemID" />
                    <asp:HiddenField runat="server" ID="HiddenClientID" />
                    <h3 class="rate_title">Subrate Settings</h3>
                    <form class="subrte_form">
                        <div class="clearboth">

                            <label class="frm_val">Client Name</label>
                            <asp:Label ID="lblClientName" Text="" runat="server" CssClass="frm_val clientname"></asp:Label>
                        </div>
                        <div class="clearboth">
                            <label class="frm_val">Subrate Template</label>
                            <asp:DropDownList class="frm_val" style="width:200px" ID="drpTemplateList" runat="server" OnSelectedIndexChanged="drpTemplateList_SelectedIndexChanged" AutoPostBack="true" CssClass="frm_select">
                            </asp:DropDownList>
                        </div>
                        <div class="clearboth">
                            <label class="frm_val">Currency</label>
                            <asp:DropDownList class="frm_val" style="width:100px" ID="drpCurrency" runat="server" OnSelectedIndexChanged="drpCurrency_SelectedIndexChanged" AutoPostBack="true" CssClass="frm_select">
                            </asp:DropDownList>
                        </div>
                    </form>
                    <asp:Repeater ID="rptSubrates" runat="server" OnItemDataBound="rptSubrates_ItemDataBound">
                        <HeaderTemplate>
                            <table class="cl_tabl" id="sub_tab" aria-describedby="orderTable_info">
                                <thead class="clrate_thead">
                                    <tr>
                                        <th>SubrateItem</th>
                                        <th>Code</th>
                                        <th>Ex.Vat</th>
                                        <th>Ex.Vat(%)</th>
                                        <th>VAT%</th>
                                        <th>VAT Amount</th>
                                        <th>Total Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><%#Eval("ItemName")%>                                       
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblSRCode" Text='<%#Eval("SubrateItemCode")%>'></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox CssClass="vat_value" runat="server" ID="txtAmount" onkeyup="ReCalculateVat(this)" onkeypress="return isNumberKey(event)" MaxLength="3"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox CssClass="vat_value" runat="server" ID="txtPercentageAmount" onkeyup="ReCalculateVat(this)" onkeypress="return isNumberKey(event)" MaxLength="3"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:DropDownList CssClass="vat_value width50" runat="server" ID="ddlVatPercentage" onchange="ReCalculateVat(this)"></asp:DropDownList>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblVatAmount" ></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblTotalAmount"></asp:Label>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                                 </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <div class="ofr_btns" id="divCommand" runat="server">
                        <asp:Button runat="server" CssClass="btn_save" ID="btnSave" OnClick="btnSave_Click" Text="Save Client Subrate" />
                        <asp:Button runat="server" CausesValidation="false" CssClass="subrte_cancl" ID="btnCancle" OnClick="btnCancle_Click" Text="Cancel" />
                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<script>
    function ReCalculateVat(obj) {
        var controlId = obj.id;
        if (controlId.indexOf("ddlVatPercentage") > -1) {
            controlId = controlId.replace("ddlVatPercentage", "##ChangeID##")
        }
        else if (controlId.indexOf("txtAmount") > -1) {
            controlId = controlId.replace("txtAmount", "##ChangeID##")
        }
        else if (controlId.indexOf("txtPercentageAmount") > -1) {
            controlId = controlId.replace("txtPercentageAmount", "##ChangeID##")
        }

        //        txtPercentageAmount
        var vat = $("#" + controlId.replace("##ChangeID##", "ddlVatPercentage")).val();
        var price = $("#" + controlId.replace("##ChangeID##", "txtAmount")).val();
        var percentage = $("#" + controlId.replace("##ChangeID##", "txtPercentageAmount")).val();
        if (obj.id.indexOf("txtPercentageAmount") > -1 && percentage != '') {
            $("#" + controlId.replace("##ChangeID##", "txtAmount")).attr("oldprice", price);
            $("#" + controlId.replace("##ChangeID##", "txtAmount")).val('');
            price = '';
        }
        else if (obj.id.indexOf("txtAmount") > -1 && price != '') {
            $("#" + controlId.replace("##ChangeID##", "txtPercentageAmount")).attr("oldprice", price);
            $("#" + controlId.replace("##ChangeID##", "txtPercentageAmount")).val('');
            percentage = '';
        }
        if (percentage != '') {
            var vatmsg = (isNaN(vat) ? "0" : vat) + " % " + "Of Ex.Vat (%) Amount";
            var totalmsg = (isNaN(percentage) ? "0" : percentage) + " % Of Room Price" + " & " + vatmsg;
            $("#" + controlId.replace("##ChangeID##", "lblVatAmount")).html(vatmsg);
            $("#" + controlId.replace("##ChangeID##", "lblTotalAmount")).html(totalmsg);
            

        }
        else {
            var vatAmount = roundToTwo(((parseFloat(price) * parseFloat(vat)) / 100));
            var totalAmount = roundToTwo(parseFloat(price) + vatAmount);
            $("#" + controlId.replace("##ChangeID##", "lblVatAmount")).html(isNaN(vatAmount) ? "0" : vatAmount)
            $("#" + controlId.replace("##ChangeID##", "lblTotalAmount")).html(isNaN(totalAmount) ? "0" : totalAmount)
        }
    }
    function roundToTwo(num) {
        return +(Math.round(num + "e+2") + "e-2");
    }
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
</script>
