CREATE PROCEDURE [dbo].[LB_ProviderAvailablityAlertSummary]
@StartDate DATETIME=NULL,
@EndDate DATETIME=NULL,
@SupplierType VARCHAR(MAX)=NULL,
@Provider VARCHAR(MAX)=NULL,
@OccupancyType VARCHAR(MAX)=NULL
AS
BEGIN

DECLARE  @ProviderAvailablity TABLE
(
 ProviderID VARCHAR(MAX),
 ProductID INT,
 ProductName  VARCHAR(MAX),  
 RoomType  VARCHAR(MAX),  
 SupplierType  VARCHAR(MAX),
 ProviderDate  DATETIME,
 ParentProductId INT
)
    INSERT INTO @ProviderAvailablity
	SELECT DISTINCT
			NULL [ProviderID],
			NULL [ProductID],
			NULL [ProductName],
			PPD.RoomType [RoomType],
			NULL [SupplierType],
			(CAST('1/1/1990' AS Datetime) + RA.DateCounter) [ProviderDate],
			P.ParentProductId
	FROM dbo.LB_RoomAvailability RA
		INNER JOIN Ucommerce_product P ON RA.VariantSku = P.VariantSku 
		INNER JOIN (
		   SELECT ProductId,RoomType
			FROM 
			(
			  SELECT ProductId,PD.Name [key], PP.value
			  FROM uCommerce_ProductDefinitionField PD 
			  INNER JOIN uCommerce_ProductProperty PP ON PD.ProductDefinitionFieldId=PP.ProductDefinitionFieldId
			  WHERE PD.Name IN ('RoomType')
			) src
			pivot
			(
			  MAX([value])
			  FOR [key] in (RoomType)
			) piv
		) PPD ON PPD.ProductId=P.ProductId AND PPD.RoomType IS NOT NULL AND CONVERT(VARCHAR,PPD.RoomType)!=''
	WHERE
	((RA.NumberAllocated >= 1 and (RA.NumberAllocated-RA.NumberBooked) <= 1) or (RA.NumberAllocated = 1 and (RA.NumberAllocated-ra.NumberBooked) = 0)) 

	UPDATE  PA SET PA.ProductID=P.ProductId,PA.ProductName=P.Name, PA.ProviderID=PP.SiteCoreItemGuid,PA.SupplierType=PP.SupplierType FROM	@ProviderAvailablity PA 
	INNER JOIN 	uCommerce_Product P ON P.ProductId=PA.ParentProductId AND P.ParentProductId IS NULL AND P.VariantSku IS NULL
	INNER JOIN (
		   SELECT ProductId,SupplierType,SiteCoreItemGuid
			FROM 
			(
			  SELECT ProductId,PD.Name [key], PP.value
			  FROM uCommerce_ProductDefinitionField PD 
			  INNER JOIN uCommerce_ProductProperty PP ON PD.ProductDefinitionFieldId=PP.ProductDefinitionFieldId
			  WHERE PD.Name IN ('SupplierType','SiteCoreItemGuid')
			) src
			pivot
			(
			  MAX([value])
			  FOR [key] in (SupplierType,SiteCoreItemGuid)
			) piv
		) PP ON PP.ProductId=P.ProductId 


		SELECT DISTINCT * FROM @ProviderAvailablity
		WHERE SupplierType IS NOT NULL AND
		(@SupplierType IS NULL OR  SupplierType LIKE  '%' + @SupplierType + '%') AND
		(@Provider IS NULL OR   ProviderID = @Provider) AND
		(@OccupancyType IS NULL OR  RoomType LIKE '%' + @OccupancyType + '%' ) AND
		(@StartDate IS NOT NULL AND CAST(ProviderDate AS DATE) <= CAST(@EndDate AS DATE)  AND  CAST(ProviderDate AS DATE) >= CAST(@StartDate AS DATE)) 
			
END


