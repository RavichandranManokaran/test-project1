﻿CREATE TABLE [dbo].[uCommerce_ProductCatalogPriceGroupRelation] (
    [ProductCatalogPriceGroupRelationId] INT IDENTITY (1, 1) NOT NULL,
    [ProductCatalogId]                   INT NOT NULL,
    [PriceGroupId]                       INT NOT NULL,
    [SortOrder]                          INT CONSTRAINT [DF_uCommerce_ProductCatalogPriceGroupRelation_SortOrder] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_uCommerce_ProductCatalogPriceGroupRelation] PRIMARY KEY CLUSTERED ([ProductCatalogPriceGroupRelationId] ASC),
    CONSTRAINT [FK_uCommerce_ProductCatalogPriceGroupRelation_uCommerce_PriceGroup] FOREIGN KEY ([PriceGroupId]) REFERENCES [dbo].[uCommerce_PriceGroup] ([PriceGroupId]),
    CONSTRAINT [FK_uCommerce_ProductCatalogPriceGroupRelation_uCommerce_ProductCatalog] FOREIGN KEY ([ProductCatalogId]) REFERENCES [dbo].[uCommerce_ProductCatalog] ([ProductCatalogId])
);

