﻿using Affinion.LoyaltyBuild.Api.Booking.Data.Basket;
using Affinion.LoyaltyBuild.Model.Common;
using Affinion.LoyaltyBuild.Model.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using data = Affinion.LoyaltyBuild.Api.Booking.Data;

namespace Affinion.LoyaltyBuild.Api.Booking.Service
{
    /// <summary>
    /// Booking service interface
    /// </summary>
    public interface IBookingService
    {
        /// <summary>
        /// Get basket
        /// </summary>
        /// <returns></returns>
        data.BasketInfo GetBasket();

        /// <summary>
        /// Get basket
        /// </summary>
        /// <returns></returns>
        data.BasketInfo GetPurchaseOrder(int orderId);

        /// <summary>
        /// Get booking by orderlineid
        /// </summary>
        /// <param name="orderLineId"></param>
        /// <returns></returns>
        data.Booking GetBooking(int orderLineId);

        /// <summary>
        /// get booking by orderline nad product type
        /// </summary>
        /// <param name="orderLineId"></param>
        /// <param name="productType"></param>
        /// <returns></returns>
        data.Booking GetBooking(int orderLineId, ProductType productType);


        /// <summary>
        /// Validate Cancel Booking
        /// </summary>
        /// <param name="orderLineId"></param>
        /// <returns></returns>
        data.PreCancelBookingResonse ValidateCancelBooking(int orderLineId);
        
        /// <summary>
        /// Cancel Booking
        /// </summary>
        /// <param name="cancelRequest"></param>
        /// <returns></returns>
        ValidationResult CancelBooking(ICancelRequest cancelRequest);

        /// <summary>
        /// Transfer Booking
        /// </summary>
        /// <param name="transferRequest"></param>
        /// <returns></returns>
        ValidationResult TransferBooking(ITransferRequest transferRequest);

        /// <summary>
        /// Get the transfer history details for the item
        /// </summary>
        /// <param name="orderLineId">order line id</param>
        /// <returns>list of transfer history</returns>
        List<data.TransferHistoryItem> GetBookingTransferHistory(int orderLineId);

        /// <summary>
        /// Get if is free transfer
        /// </summary>
        /// <param name="orderLineId"></param>
        /// <returns></returns>
        bool GetIsFreeTransfer(int orderLineId);

        /// <summary>
        /// Booking status
        /// </summary>
        /// <param name="orderLineId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        bool UpdateStatus(int orderLineId, data.BookingStatus status);

        /// <summary>
        /// Booking status
        /// </summary>
        /// <param name="orderLineId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        bool UpdateBookingReference(int orderLineId, string bookingReference);
    }
}
