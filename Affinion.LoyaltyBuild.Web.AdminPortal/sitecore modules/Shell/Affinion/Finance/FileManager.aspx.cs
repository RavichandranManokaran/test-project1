﻿using Affinion.LoyaltyBuild.BedBanks.General;
using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.AdminPortal.sitecore_modules.Shell.Affinion.Finance
{
    public partial class FileManager : System.Web.UI.Page
    {
        IReportsService iReportsService = new ReportsService();
        SearchCriteria objSearchCriteria = new SearchCriteria();
        Item[] providers = null;
        Item oracleConfiguration = null;
        string ProviderPath = Constants.ProviderPath;
        Sitecore.Data.Database sitecoreDatabase = Sitecore.Configuration.Factory.GetDatabase("master");
        BedBanksSettings bedBankData = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetOracleFileInformation();
            }
            this.lblMessage.Text = string.Empty;
        }

        private void GetOracleFileInformation()
        {
            try
            {
                iReportsService = new ReportsService();

                List<OracleFileInfo> lstOracleFileInfo = iReportsService.GetOracleFileInfo();

                if (lstOracleFileInfo == null || lstOracleFileInfo.Count == 0)
                    lstOracleFileInfo = new List<OracleFileInfo>();

                this.ResultGridView.DataSource = lstOracleFileInfo;
                this.ResultGridView.DataBind();

            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.AdminPortal, ex, null);
                throw;
            }
        }

        private DataTable GetOracleInvoiceData(int oracleFileID,string requestedFrom)
        {
            IReportsService iReportsService = new ReportsService();
            List<OracleInvoiceData> lstOracleInvoiceData = null;
            lstOracleInvoiceData = iReportsService.GetOracleInvoiceData(oracleFileID, requestedFrom);
            DataTable dtOracleInvoiceData = GetDataTable(requestedFrom);
            if (lstOracleInvoiceData != null && lstOracleInvoiceData.Count > 0)
            {
                foreach (var objOracleInvoiceData in lstOracleInvoiceData)
                {
                    string PaymentTerms = string.Empty;
                    string RecepitTerms = string.Empty;
                    int LineNumber = 0;
                    string TaxCode = string.Empty;
                    if (!string.IsNullOrEmpty(objOracleInvoiceData.ProviderID))
                    {
                        providers = sitecoreDatabase.SelectItems(ProviderPath);
                        oracleConfiguration = providers.Where(x => Guid.Parse(x.ID.ToString()).ToString().Equals(Guid.Parse(objOracleInvoiceData.ProviderID.ToString()).ToString(), StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();

                        if (oracleConfiguration != null)
                        {
                            if (requestedFrom == "AP")
                            {
                                PaymentTerms = oracleConfiguration.Fields["AP Payment Terms"].Value.ToString();
                                TaxCode = oracleConfiguration.Fields["AP Tax Code"].Value.ToString();
                                if (!string.IsNullOrEmpty(oracleConfiguration.Fields["AP Line Number"].Value))
                                    LineNumber = int.Parse(oracleConfiguration.Fields["AP Line Number"].Value.ToString());
                                objOracleInvoiceData.PaymentTerms = PaymentTerms;
                                objOracleInvoiceData.LineNumber = LineNumber;
                                objOracleInvoiceData.TaxCode = TaxCode;
                            }
                            else if (requestedFrom == "AR")
                            {
                                RecepitTerms = oracleConfiguration.Fields["AR Receipt Terms"].Value.ToString();
                                TaxCode = oracleConfiguration.Fields["AR Tax Code"].Value.ToString();
                                objOracleInvoiceData.RecepitTerms = RecepitTerms;
                                objOracleInvoiceData.TaxCode = TaxCode;
                            }

                        }
                    }
                    else
                    {
                        oracleConfiguration = sitecoreDatabase.GetItem(Sitecore.Data.ID.Parse(objOracleInvoiceData.ClientID));
                        if (oracleConfiguration != null)
                        {
                            bedBankData = new BedBanksSettings(oracleConfiguration);
                            if (bedBankData != null && bedBankData.BedBanksOracleConfigurations != null)
                            {
                                if (requestedFrom == "AP")
                                {
                                    PaymentTerms = bedBankData.BedBanksOracleConfigurations.ApPaymentTerms;
                                    TaxCode = bedBankData.BedBanksOracleConfigurations.ApTaxCode;
                                    if (!string.IsNullOrEmpty(bedBankData.BedBanksOracleConfigurations.ApLineNumber))
                                        LineNumber = int.Parse(bedBankData.BedBanksOracleConfigurations.ApLineNumber);
                                    objOracleInvoiceData.PaymentTerms = PaymentTerms;
                                    objOracleInvoiceData.LineNumber = LineNumber;
                                    objOracleInvoiceData.TaxCode = TaxCode;
                                }
                                else if (requestedFrom == "AR")
                                {
                                    RecepitTerms = bedBankData.BedBanksOracleConfigurations.ArReceiptTerms;
                                    TaxCode = bedBankData.BedBanksOracleConfigurations.ArTaxCode;
                                    objOracleInvoiceData.RecepitTerms = RecepitTerms;
                                    objOracleInvoiceData.TaxCode = TaxCode;
                                }
                            }
                        }
                    }


                    if (requestedFrom == "AP")
                    {
                        dtOracleInvoiceData.Rows.Add
                            (
                              objOracleInvoiceData.InvoiceID,
                              objOracleInvoiceData.InvoiceDate.Date.ToString("d"),
                              objOracleInvoiceData.VendorNumber,
                              objOracleInvoiceData.VendorSiteCode,
                              objOracleInvoiceData.CurrencyCode,
                              objOracleInvoiceData.Amount.ToString("0.00"),
                              objOracleInvoiceData.PaymentTerms,
                              objOracleInvoiceData.LineNumber,
                              objOracleInvoiceData.TaxCode,
                              objOracleInvoiceData.Description
                            );
                    }
                    else if (requestedFrom == "AR")
                    {
                        dtOracleInvoiceData.Rows.Add
                           (
                             objOracleInvoiceData.InvoiceID,
                             objOracleInvoiceData.InvoiceDate.Date.ToString("d"),
                             objOracleInvoiceData.CustomerId,
                             objOracleInvoiceData.CustomerAddressCode,
                             objOracleInvoiceData.CurrencyCode,
                             objOracleInvoiceData.Amount.ToString("0.00"),
                             objOracleInvoiceData.RecepitTerms,
                             objOracleInvoiceData.LineNumber,
                             objOracleInvoiceData.TaxCode,
                             objOracleInvoiceData.Description,
                             objOracleInvoiceData.SourceInvoiceLineId
                           );
                    }
                }
            }
            return dtOracleInvoiceData;
        }

        private DataTable GetDataTable(string reqFrom)
        {
            DataTable dataTable = new DataTable();
            if (reqFrom == "AP")
            {
                dataTable.Columns.Add("INVOICE_ID");
                dataTable.Columns.Add("INVOICE_DATE");
                dataTable.Columns.Add("VENDOR_NUMBER");
                dataTable.Columns.Add("VENDOR_SITE_CODE");
                dataTable.Columns.Add("CURRENCY_CODE");
                dataTable.Columns.Add("AMOUNT");
                dataTable.Columns.Add("PAYMENT_TERMS");
                dataTable.Columns.Add("LINE_NUMBER");
                dataTable.Columns.Add("TAX_CODE");
                dataTable.Columns.Add("DESCRIPTION");
            }
            else
            {
                dataTable.Columns.Add("INVOICE_ID");
                dataTable.Columns.Add("INVOICE_DATE");
                dataTable.Columns.Add("CUSTOMER_NUMBER");
                dataTable.Columns.Add("CUSTOMER_ADDR_CODE");
                dataTable.Columns.Add("CURRENCY_CODE");
                dataTable.Columns.Add("AMOUNT");
                dataTable.Columns.Add("RECEIPT_TERMS");
                dataTable.Columns.Add("LINE_NUMBER");
                dataTable.Columns.Add("TAX_CODE");
                dataTable.Columns.Add("DESCRIPTION");
                dataTable.Columns.Add("SOURCE_INV_LINE_ID");
            }
            return dataTable;
        }

        protected void lnkDownload_Click(object sender, EventArgs e)
        {
            try
            {
                GridViewRow grvrow = (GridViewRow)((LinkButton)sender).NamingContainer;
                Label lblOracleFileName = (Label)grvrow.FindControl("lblOracleFileName");
                Label lblSequenceNo = (Label)grvrow.FindControl("lblSequenceNo");
                Label lblOracleFileType = (Label)grvrow.FindControl("lblOracleFileType");


                DataTable dtOracleFileData = GetOracleInvoiceData(int.Parse(lblSequenceNo.Text), lblOracleFileType.Text);

                if (dtOracleFileData != null && dtOracleFileData.Rows.Count > 0)
                {

                    string csv = string.Empty;

                    foreach (DataColumn column in dtOracleFileData.Columns)
                    {
                        //Add the Header row for CSV file.
                        csv += column.ColumnName + ',';
                    }

                    //Add new line.
                    csv += "\r\n";

                    foreach (DataRow row in dtOracleFileData.Rows)
                    {
                        foreach (DataColumn column in dtOracleFileData.Columns)
                        {
                            //Add the Data rows.
                            csv += row[column.ColumnName].ToString().Replace(",", ";") + ',';
                        }

                        //Add new line.
                        csv += "\r\n";
                    }

                    //Download the CSV file.
                    Encoding encoding = Encoding.UTF8;
                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=" + lblOracleFileName.Text + ".csv");
                    Response.Charset = encoding.EncodingName;
                    Response.ContentEncoding = Encoding.UTF8;  
                    Response.ContentType = "application/text";
                    Response.Output.Write(csv);
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    this.lblMessage.Text = "Demands are not posted for this file";
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.AdminPortal, ex, null);
                throw;
            }

        }

        protected void ResultGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblOracleFileStatus = (Label)e.Row.FindControl("lblOracleFileStatus");

                if (lblOracleFileStatus.Text == "0")
                {
                    lblOracleFileStatus.Text = "Pending Post";
                }
                else if (lblOracleFileStatus.Text == "1")
                {
                    lblOracleFileStatus.Text = "Complete in Oracle";
                }
            }

        }

        protected void ResultGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ResultGridView.PageIndex = e.NewPageIndex;
            GetOracleFileInformation();

        }
    }
}