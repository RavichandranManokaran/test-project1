﻿using Affinion.LoyaltyBuild.Api.Search.Data;
using Affinion.LoyaltyBuild.BedBanks.General;
using Sitecore.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Api.Search.Filters
{
    public class ThemesFilter: ILoyaltyFilter, ISearchFilter
    {
        private readonly SearchKey _key;

        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="key"></param>
        public ThemesFilter(SearchKey key)
        {
            _key = key;
        }

        public void BuildSearch(ref UCom.MasterClass.ExtendedApi.SearchCriteria criteria)
        {
            if (_key.Themes != null)
            {
                criteria.Themes = _key.Themes.Replace(",","|");
               

            }
        }       
    }
}
