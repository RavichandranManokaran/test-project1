﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Model
{
    public class PackageDetails
    {
        /// <summary>
        /// Package Name
        /// </summary>
        public string PackageName { get; set; }

        /// <summary>
        /// Package Price
        /// </summary>
        public string RoomPrice { get; set; }

        /// <summary>
        /// Add On Price
        /// </summary>
        public string AddOnPrice { get; set; }

        /// <summary>
        /// Total
        /// </summary>
        public string Total { get; set; }
    }
}