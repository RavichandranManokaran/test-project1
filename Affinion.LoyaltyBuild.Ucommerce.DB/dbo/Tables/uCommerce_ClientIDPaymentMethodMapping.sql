﻿CREATE TABLE [dbo].[uCommerce_ClientIDPaymentMethodMapping]
(
	[KeyId] [int] IDENTITY(1,1) NOT NULL,
	[PaymentMethodId] [int] NOT NULL,
	[Description] [nvarchar](256) NOT NULL,
	[ClientId] [nvarchar](256) NOT NULL,
	[ShowInList] [bit] NOT NULL
) 
