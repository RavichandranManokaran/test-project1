﻿CREATE TABLE [dbo].[LB_OrderLineSubrates] (
    [OrderLineSubrateID]    INT           IDENTITY (1, 1) NOT NULL,
    [OrderLineID]           INT           NOT NULL,
    [SubrateItemCode]       NVARCHAR (50) NOT NULL,
    [Amount]                MONEY         NOT NULL,
    [Discount]              MONEY         NULL,
    [IsIncludedInUnitPrice] BIT           NOT NULL,
    PRIMARY KEY CLUSTERED ([OrderLineSubrateID] ASC)
);

