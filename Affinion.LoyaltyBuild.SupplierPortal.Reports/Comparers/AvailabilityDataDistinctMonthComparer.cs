﻿using Affinion.LoyaltyBuild.SupplierPortal.Reports.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.SupplierPortal.Reports.Comparers
{
    internal class AvailabilityDataDistinctMonthComparer : IEqualityComparer<AvailabilityData>
    {

        public bool Equals(AvailabilityData x, AvailabilityData y)
        {
            return (x.HotelId == y.HotelId) && (x.Date.AddDays(-x.Date.Day) == y.Date.AddDays(-y.Date.Day)) && (x.RoomType == y.RoomType);
        }

        public int GetHashCode(AvailabilityData obj)
        {
            return obj.HotelId + (obj.Date.Day * obj.Date.Month * (obj.Date.Year - 2000));
        }
    }
}
