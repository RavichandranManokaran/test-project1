﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           JsonReader.cs
/// Sub-system/Module:     Client Setup(VS project name)
/// Description:           code will be used to list of currency objects
/// </summary>


#region Using Directives
using Newtonsoft.Json;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using System.Collections.ObjectModel;
#endregion


namespace Affinion.LoyaltyBuild.SupplierSetup.CurrencyConverter
{
    public class JsonReader
    {
        public CurrencyBase getResponse { get; set; }

        #region Public Methods
        /// <summary>
        /// Read sitecore Item and retrive Json data
        /// Format the Json dat and create list of objects 
        /// </summary>
        public JsonReader()
        {
            try
            {
                //get master data base
                Database db = Factory.GetDatabase("master");

                //access sitecore item
                Item item = db.Items["/sitecore/content/DemoStore/CurrencyData"];

                //Get json values from sitecore item
                string fieldValue = item["ExchangeRates"];

                //Deserialize Json data 
                JavaScriptSerializer ser = new JavaScriptSerializer();
                var JSONObject = JsonConvert.DeserializeObject<RootObject>(fieldValue);

                var CurrencyBase = new CurrencyBase();
                // Currency list 
                var CurrencyList = new Collection<JsonCurrency>();

                //Loop through lines get base currency value
                foreach (var lineItem in JSONObject.Lines)
                {
                    //Loop through lines get Quote Currency and rate
                    foreach (var columnItem in lineItem.Columns)
                    {
                        JsonCurrency jsonCurrency = new JsonCurrency()
                        {
                            Base = lineItem.BaseCurrency,
                            Quote = columnItem.QuoteCurrency,
                            Value = columnItem.Rate
                        };

                        CurrencyList.Add(jsonCurrency);
                    }
                }

                //add to list
                CurrencyBase.Currencylist = CurrencyList;
                //add list of objects
                getResponse = CurrencyBase;

            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.SupplierSetup, ex, this);
                throw;
            }

        }
        #endregion


    }
}
