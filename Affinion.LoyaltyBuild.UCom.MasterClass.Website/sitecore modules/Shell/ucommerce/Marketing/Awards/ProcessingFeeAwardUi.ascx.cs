﻿using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.EntitiesV2;
using UCommerce.Presentation.Views.Marketing;
using UCommerce.Presentation.Web;
using UCommerce.Presentation.Web.Controls;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.Ucommerce.Marketing.Awards
{
    public partial class ProcessingFeeAwardUi : ViewEnabledControl<IEditCampaignItemView>, IConfigurable
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ProcessingFeeAward dataSource = (ProcessingFeeAward)this.DataSource;
            this.ProcessingFeeLabel.Text = string.Format("Discount On Processing Fee : {0}", dataSource.DiscountAmount.ToString() + " " + (dataSource.IsPercentage ? "Percentage" : "Unit"));
            this.DiscountAmount.Text = dataSource.DiscountAmount.ToString();
            chkIsPercentage.Checked = dataSource.IsPercentage;
            this.DecimalValidator.ErrorMessage = string.Format("{0}", "Please Enter Only Number");
            this.RequiredValidator.ErrorMessage = string.Format("{0}", "Please Enter Only Number");
            this.DeleteButton.OnClientClick = string.Format("return confirm('{0}');", "Are you want to delete award");
        }

        // Methods
        protected void DeleteButton_Click(object sender, ImageClickEventArgs e)
        {
            if (this.AwardId != 0)
            {
                (from x in UCommerce.EntitiesV2.Award.All()
                 where x.AwardId == this.AwardId
                 select x).SingleOrDefault<UCommerce.EntitiesV2.Award>().Delete();
            }
            this.Page.Response.Redirect(this.Page.Request.Url.ToString(), true);
        }

        protected void EditButton_Click(object sender, ImageClickEventArgs e)
        {
            this.ToggleEditor();
        }

        public object New()
        {
            this.DataSource = new ProcessingFeeAward();
            ((ProcessingFeeAward)this.DataSource).CampaignItem = base.View.CampaignItem;
            if (((ProcessingFeeAward)this.DataSource).DiscountAmount < 1M)
            {
                ((ProcessingFeeAward)this.DataSource).DiscountAmount = 0M;
            }
            if (string.IsNullOrEmpty(((ProcessingFeeAward)this.DataSource).Name))
            {
                ((ProcessingFeeAward)this.DataSource).Name = "Discount On Processing Fee";
            }
            ((ProcessingFeeAward)this.DataSource).DiscountAmount = 0;
            ((ProcessingFeeAward)this.DataSource).OnOrderLine = false;
            ((ProcessingFeeAward)this.DataSource).IsPercentage = false;
            ((ProcessingFeeAward)this.DataSource).Save();
            return this.DataSource;
        }

        protected void SaveButton_Click(object sender, ImageClickEventArgs e)
        {
            ProcessingFeeAward award = (this.DataSource == null) ? new ProcessingFeeAward() : ((ProcessingFeeAward)this.DataSource);
            CampaignItem campaignItem = base.View.CampaignItem;
            award.DiscountAmount = decimal.Parse(this.DiscountAmount.Text);
            award.OnOrderLine = false;
            award.IsPercentage = chkIsPercentage.Checked;
            award.CampaignItem = campaignItem;
            award.Save();
            this.ProcessingFeeLabel.Text = string.Format("Discount On Processing Fee : {0}", award.DiscountAmount.ToString() + " " + (award.IsPercentage ? "Percentage" : "Unit"));
            this.ToggleEditor();
        }

        private void ToggleEditor()
        {
            this.NonEditModePlaceHolder.Visible = !this.NonEditModePlaceHolder.Visible;
            this.EditButton.Visible = !this.EditButton.Visible;
            this.EditModePlaceHolder.Visible = !this.EditModePlaceHolder.Visible;
            this.SaveButton.Visible = !this.SaveButton.Visible;
        }

        // Properties
        public int AwardId
        {
            get
            {
                if (this.DataSource == null)
                {
                    return 0;
                }
                return ((UCommerce.EntitiesV2.Award)this.DataSource).AwardId;
            }
        }

        public int CampaignItemId
        {
            get
            {
                return base.View.CampaignItem.CampaignItemId;
            }
        }

        public object DataSource { get; set; }

    }

}