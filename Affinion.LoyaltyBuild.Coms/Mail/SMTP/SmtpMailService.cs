﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Comms.Mail.SMTP
{
    public class SmtpMailService : MailService
    {
        SmtpClient _client;
       

        /// <summary>
        /// Constructor
        /// </summary>
        public SmtpMailService()
        {
            _client = new SmtpClient();
            //System.Net.NetworkCredential credentials = new System.Net.NetworkCredential("keerthikas@virtusapolaris.com", "92@Keerthi");
            //_client.Credentials = credentials;
           
        }

        /// <summary>
        /// Method to Send SMTP Mail
        /// </summary>
        /// <param name="mail">IMail</param>
        public override void SendMail(IMail mail)
        {
            var smtpMail = mail as SmtpMail;

            using (var message = new MailMessage())
            {
                message.From = new MailAddress(mail.FromAddress);

                message.To.Add(string.Join(",", mail.Recepients.Select(i => i.EmailId)));
                message.Subject = mail.Subject;
                message.Body = mail.Body;
                message.IsBodyHtml = true;
               
                _client.Send(message);
            }
        }

    }
}
