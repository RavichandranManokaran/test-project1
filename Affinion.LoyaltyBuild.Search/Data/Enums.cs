﻿
///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           Enums.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Search
/// Description:           Code is used to store enums.
/// </summary>

using System;
namespace Affinion.LoyaltyBuild.Search.Data
{

    #region Enums

    /// <summary>
    /// Enum for sort modes
    /// </summary>
    [Serializable]
    public enum SortMode
    {
        None = 0,
        SupplierName,
        Price,
        Rate,
        Stars,
        Recommend,
        DistanceFromCityCentre
    }

    /// <summary>
    /// Enum for sort modes
    /// </summary>
    [Serializable]
    public enum SearchResultsMode
    {
        None = 0,
        SupplierName,
        Location,
        Mixed
    }

    /// <summary>
    /// Enum for Page Modes
    /// </summary>
    [Serializable]
    public enum PageMode
    {
        Locations,
        Hotels,
        LocationFinder
    }

    [Serializable]
    public enum OfferSearchFailedFilter
    {
        Default,
        AdultAndChildCount

    }
    #endregion
}
