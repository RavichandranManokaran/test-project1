﻿CREATE TABLE [dbo].[uCommerce_PaymentMethodFee] (
    [PaymentMethodFeeId] INT   IDENTITY (1, 1) NOT NULL,
    [PaymentMethodId]    INT   NOT NULL,
    [CurrencyId]         INT   NOT NULL,
    [PriceGroupId]       INT   NOT NULL,
    [Fee]                MONEY NOT NULL,
    CONSTRAINT [uCommerce_PK_PaymentMethodFee] PRIMARY KEY CLUSTERED ([PaymentMethodFeeId] ASC),
    CONSTRAINT [uCommerce_FK_PaymentMethodFee_Currency] FOREIGN KEY ([CurrencyId]) REFERENCES [dbo].[uCommerce_Currency] ([CurrencyId]),
    CONSTRAINT [uCommerce_FK_PaymentMethodFee_PaymentMethod] FOREIGN KEY ([PaymentMethodId]) REFERENCES [dbo].[uCommerce_PaymentMethod] ([PaymentMethodId]),
    CONSTRAINT [uCommerce_FK_PaymentMethodFee_PriceGroup] FOREIGN KEY ([PriceGroupId]) REFERENCES [dbo].[uCommerce_PriceGroup] ([PriceGroupId])
);

