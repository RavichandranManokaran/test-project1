﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    public class OfferDetail
    {
        public string OfferName { get; set; }

        public decimal BookingReceived { get; set; }

        public int FutureBooking { get; set; }

        public decimal RevenueGenerated { get; set; }

        public decimal FutureArrival { get; set; }

        public decimal HotelRate { get; set; }

        public decimal HotelDiscountedRate { get; set; }
    }
}
