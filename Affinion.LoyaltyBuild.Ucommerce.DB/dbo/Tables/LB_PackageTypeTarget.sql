﻿CREATE TABLE [dbo].[LB_PackageTypeTarget](
	[PackageTypeTargetId] [int] NOT NULL,
	[PackageType] [varchar](250) NOT NULL,
 CONSTRAINT [PK_uCommerce_PackageTypeTarget] PRIMARY KEY CLUSTERED 
(
	[PackageTypeTargetId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[LB_PackageTypeTarget]  WITH CHECK ADD  CONSTRAINT [FK_uCommerce_PackageTypeTarget_uCommerce_Target] FOREIGN KEY([PackageTypeTargetId])
REFERENCES [dbo].[uCommerce_Target] ([TargetId])
GO

ALTER TABLE [dbo].[LB_PackageTypeTarget] CHECK CONSTRAINT [FK_uCommerce_PackageTypeTarget_uCommerce_Target]
GO


