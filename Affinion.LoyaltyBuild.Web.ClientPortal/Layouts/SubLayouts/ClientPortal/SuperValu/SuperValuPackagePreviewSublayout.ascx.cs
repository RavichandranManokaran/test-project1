﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SuperValuPackagePreviewSublayout.ascx.cs
/// Sub-system/Module:     Web.ClientPortal(VS project name)
/// Description:           Sublayout for promotional packages preview box
/// </summary>
namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts
{
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Sitecore.Data.Items;
    using System;
    using System.Web.UI.WebControls;

    /// <summary>
    /// Sublayout for promotional packages preview box
    /// </summary>
    public partial class SuperValuPackagePreviewSublayout : BaseSublayout
    {
        /// <summary>
        /// This function includes the code which needs to run on page load
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The parameter</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Item item = this.GetDataSource();

                if (item != null)
                {
                    this.PlaceholderText.Visible = false;
                    this.DesktopImage.Item = this.Title.Item = this.Subtitle.Item = item;
                    this.PackageLinks.DataSource = SitecoreFieldsHelper.GetMutiListItems(item, "Packages");
                    this.PackageLinks.ItemDataBound += PackageLinks_ItemDataBound;
                    this.PackageLinks.DataBind();
                }
            }
            catch (Exception ex)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, ex.Message);
                throw;
            }
        }

        void PackageLinks_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            Item item = (Item)e.Item.DataItem;
            HyperLink link = (HyperLink)e.Item.FindControl("PackageLink");
            Literal text = (Literal)e.Item.FindControl("PackageLinkText");

            if(item != null && link != null && text != null)
            {
                AnchorTagHelperData linkData = SitecoreFieldsHelper.GetUrlOfItem(item);
                link.NavigateUrl = linkData.Url;
                link.ToolTip = linkData.AlterTag;
                text.Text = SitecoreFieldsHelper.GetValue(item, "Title");
            }
        }
    }
}