﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.Presentation.Views.Marketing;
using UCommerce.Presentation.Web.Controls;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.Ucommerce.Marketing
{
    public class ClientRate
    {
        public string RateName { get; set; }

        public decimal Amount { get; set; }

        public int Id { get; set; }

        //public int Id { get; set; }
    }

    public partial class EditClientSubRate :  ViewEnabledControl<IEditCampaignItemView>
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                drpClient.Items.Add(new ListItem() { Text = "Client1", Value = "1" });
                drpClient.Items.Add(new ListItem() { Text = "Client2", Value = "2" });
                drpClient.Items.Add(new ListItem() { Text = "Client3", Value = "3" });
            }
        }

        protected void drpClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            //lstclientRate.Items.Clear();
            //List<SubrateItem> subrates = SubrateItem.All().ToList();
            //if (subrates != null && subrates.Any())
            //{
            //    List<Subrate> clientSubRate = Subrate.All().Where(x => x.ClientId == 1 && x.CampaignItem.CampaignItemId == View.CampaignItem.Id).ToList();
            //    foreach (var item in subrates)
            //    {
                    
            //    }
            //}

            
            lstclientRate.Items.Add(new ListItem() { Text = "test", Value = "test1" });
            lstclientRate.Items.Add(new ListItem() { Text = drpClient.SelectedItem.Text, Value = drpClient.SelectedItem.Value });
        }
    }
}