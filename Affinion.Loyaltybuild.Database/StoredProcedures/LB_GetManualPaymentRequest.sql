CREATE PROCEDURE [dbo].[LB_GetManualPaymentRequest]
@BookingRefNo VARCHAR(MAX)=NULL
AS
BEGIN
DECLARE @OrderLineID BIGINT
DECLARE @OrderID BIGINT
DECLARE @PaymentRequestStatus TINYINT
DECLARE @PaymentRequestDate DATETIME
DECLARE @PaymentPaidDate DATETIME
DECLARE @PaymentSatus TINYINT
DECLARE @SuggestedAmount DECIMAL
DECLARE @RequestWithinSixMonth TINYINT

SET @OrderLineID=(SELECT OrderLineId FROM LB_RoomReservation WHERE OrderReference=@BookingRefNo)
SET @OrderID=(SELECT TOP 1 OrderId FROM uCommerce_OrderLine WHERE OrderLineId=ISNULL(@OrderLineID,0))

IF EXISTS(SELECT OrderId FROM LB_Supplier_Payments WHERE OrderId=ISNULL(@OrderID,0))
BEGIN
	SET @PaymentRequestStatus= 1
END
ELSE
BEGIN
	SET @PaymentRequestStatus= 0
END

IF EXISTS(
          SELECT * FROM LB_RoomReservation RR
		  INNER JOIN ucommerce_OrderLine OL ON RR.OrderLineId=OL.OrderLineId 
		  WHERE OrderId=@OrderID AND DATEDIFF(mm, RR.CheckOutDate,GETDATE())<=6
		 )
BEGIN
         SET @RequestWithinSixMonth=1
END
ELSE
BEGIN
         SET @RequestWithinSixMonth=0
END

SET @PaymentSatus=(SELECT ISNULL(IsPaidToAccounts,0) FROM LB_Supplier_Payments WHERE OrderId=ISNULL(@OrderID,0))
SET @PaymentRequestDate=(SELECT DateCreated FROM LB_Supplier_Payments WHERE OrderId=ISNULL(@OrderID,0))
SET @SuggestedAmount =(SELECT ISNULL(SuggestedAmount,0) FROM LB_Supplier_Payments WHERE OrderId=ISNULL(@OrderID,0))

IF @PaymentSatus >0
BEGIN
    SET @PaymentPaidDate=(SELECT DatePostedToOracle FROM LB_Supplier_Payments WHERE OrderId=ISNULL(@OrderID,0))
END

		SELECT 
		CAT.Guid [ProviderID],
		OL.OrderId,
		OL.OrderLineId,
		CI.Name [Offername],
		RR.CheckInDate,
		RR.CheckOutDate,
		PO.CreatedDate [BookingDate],
		(C.FirstName+' '+C.LastName) [CustomerName],
		CU.ISOCode [Currency],
		CU.CurrencyId [CurrencyId],
		P.Name [AccommodationType],
		CAT.Name [HotelName],
		(CASE WHEN @SuggestedAmount IS NULL THEN OLD.Amount ELSE @SuggestedAmount END)  [SuggestedAmount] ,
		(CASE WHEN CI.Name IS NULL THEN '' ELSE 'Offer Based' END)  [OfferBased],
		PO.CustomerId,
		ISNULL(@PaymentSatus,0) [PaymentStatus],
		@PaymentRequestStatus [PaymentRequestStatus],
		@PaymentPaidDate [PaymentPaidDate],
		@PaymentRequestDate [PaymentRequestedDate],
		@RequestWithinSixMonth [RequestWithinSixMonth]
FROM LB_RoomReservation RR
		INNER JOIN ucommerce_OrderLine OL ON RR.OrderLineId=OL.OrderLineId
		INNER JOIN LB_OrderLineDue OLD ON OL.OrderLineId=OLD.OrderLineId 
		INNER JOIN LB_DueTypes DT ON DT.DueTypeId=OLD.DueTypeId AND DT.DueTypeId=1 AND IsActive=1
		INNER JOIN uCommerce_OrderProperty OP ON OLD.OrderLineId=OP.OrderLineId AND OL.OrderId=OP.OrderId 
		INNER JOIN uCommerce_OrderLineStatus OLS ON OL.OrderLineId=OLS.OrderLineID
		INNER JOIN uCommerce_OrderStatus OS ON OLS.StatusId=OS.OrderStatusId AND OS.OrderStatusId =6
		INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId=PO.OrderId
		INNER JOIN LB_CustomerAttribute CA ON PO.CustomerId=CA.CustomerId
		INNER JOIN uCommerce_Customer C ON CA.CustomerId=C.CustomerId
		INNER JOIN uCommerce_Currency CU ON PO.CurrencyId=CU.CurrencyId
		INNER JOIN Ucommerce_product P ON OL.Sku = P.Sku
		INNER JOIN uCommerce_CategoryProductRelation CPR ON P.Productid = CPR.Productid
		INNER JOIN uCommerce_Category CAT ON CPR.Categoryid = CAT.Categoryid	
		LEFT JOIN uCommerce_OrderLineCampaignRelation OLCR ON OL.OrderLineId=OLCR.OrderLineId  
		LEFT JOIN ucommerce_campaignitem CI ON OLCR.CampaignItemId=CI.CampaignItemId
WHERE 
	    RR.OrderReference IS NOT NULL AND
	    PO.CustomerId IS NOT NULL AND
		(OP.Value='Full' OR OP.Value='Partial') AND
		RR.CheckOutDate <= DATEADD(mm,6,RR.CheckOutDate) AND
		RR.OrderReference= @BookingRefNo

END