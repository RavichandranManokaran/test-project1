﻿ALTER TABLE ucommerce_OrderLinePaymentDues ADD 
	DueTypeId INT
GO


UPDATE olpd SET olpd.DueTypeID = old.DueTypeId
FROM uCommerce_OrderLinePaymentDues olpd INNER JOIN uCommerce_OrderLineDue old
ON olpd.OrderLineDueId = old.OrderLineDueId
WHERE olpd.DueTypeId IS NULL
GO

UPDATE old SET old.PaymentMethodId = olp.PaymentMethodId
FROM uCommerce_OrderLineDue old INNER JOIN uCommerce_OrderLinePayment olp
ON old.OrderLinePaymentID = olp.OrderLinePaymentID
GO