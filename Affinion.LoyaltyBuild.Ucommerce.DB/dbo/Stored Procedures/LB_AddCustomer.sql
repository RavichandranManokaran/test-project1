﻿Create PROCEDURE [dbo].[LB_AddCustomer](
	@FirstName nvarchar(512) = null,
	@LastName nvarchar(512)  = null,
	@EmailAddress nvarchar(255)  = null,
	@Phone nvarchar(50)=Null,
	@MobilePhone nvarchar(50)  = null,
	@LanguageName nvarchar(255)=null,
	@ClientId varchar(50)  = null,
	@Title varchar(50)=Null,
	@AddressLine1 nvarchar(50)  = null,
	@AddressLine2 nvarchar(50)=Null,
	@AddressLine3 nvarchar(50)=Null,
	@CountryId int,
	@LocationId VARCHAR(50),
	@BranchId VARCHAR(50)=Null, 
	@Subscribe int=0, 
	@BookingReferenceId varchar(50)=Null, 
	@Active int=1,
	@SubscribeType varchar(50)=null,
	@Store varchar(50)=Null,
    @TermsandCondition bit=1,
	@NewCustomerId int = 0)

AS
BEGIN TRANSACTION

IF @NewCustomerId = 0 
Begin
	--DECLARE @NewCustomerId int
	INSERT INTO [dbo].uCommerce_Customer  
		(FirstName,
		LastName,
		EmailAddress,
		PhoneNumber,
		MobilePhoneNumber,
		LanguageName
	)
	 VALUES
	  (@FirstName,
	  @LastName,
	  @EmailAddress,
	  @Phone,
	  @MobilePhone,
	  @LanguageName
	  )
 
     /*If an Error Occurs Rollback Add*/

	If @@ERROR<>0
      BEGIN
	  	ROLLBACK
		RETURN
	  END
	select @NewCustomerId = SCOPE_IDENTITY()

End

	Update uCommerce_Customer set  LanguageName = @LanguageName Where CustomerId = @NewCustomerId

	INSERT Into LB_CustomerProperty ([CustomerId] , [Key] , [Value]) Values (@NewCustomerId , '_CustomerId' ,@NewCustomerId)

	INSERT Into LB_CustomerProperty ([CustomerId] , [Key] , [Value]) Values (@NewCustomerId , '_ClientId' , @ClientId)

	INSERT Into LB_CustomerProperty ([CustomerId] , [Key] , [Value]) Values (@NewCustomerId , '_Title' , @Title)

	INSERT Into LB_CustomerProperty ([CustomerId] , [Key] , [Value]) Values (@NewCustomerId , '_AddressLine1' , @AddressLine1)

	INSERT Into LB_CustomerProperty ([CustomerId] , [Key] , [Value]) Values (@NewCustomerId , '_AddressLine2' , @AddressLine2)

	INSERT Into LB_CustomerProperty ([CustomerId] , [Key] , [Value]) Values (@NewCustomerId , '_AddressLine3' , @AddressLine3)

	INSERT Into LB_CustomerProperty ([CustomerId] , [Key] , [Value]) Values (@NewCustomerId , '_CountryId' , @CountryId)

	INSERT Into LB_CustomerProperty ([CustomerId] , [Key] , [Value]) Values (@NewCustomerId , '_LocationId' , @LocationId)

	INSERT Into LB_CustomerProperty ([CustomerId] , [Key] , [Value]) Values (@NewCustomerId , '_BranchId' , @BranchId)

	INSERT Into LB_CustomerProperty ([CustomerId] , [Key] , [Value]) Values (@NewCustomerId , '_Subscribe' , @Subscribe)

	INSERT Into LB_CustomerProperty ([CustomerId] , [Key] , [Value]) Values (@NewCustomerId , '_BookingReference' , @BookingReferenceId)

	INSERT Into LB_CustomerProperty ([CustomerId] , [Key] , [Value]) Values (@NewCustomerId , '_Active' , @Active)

	INSERT Into LB_CustomerProperty ([CustomerId] , [Key] , [Value]) Values (@NewCustomerId , '_SubscribeType' , @SubscribeType)

	INSERT Into LB_CustomerProperty ([CustomerId] , [Key] , [Value]) Values (@NewCustomerId , '_Store' , @Store)

	INSERT Into LB_CustomerProperty ([CustomerId] , [Key] , [Value]) Values (@NewCustomerId , '_TermsandCondition' , @TermsandCondition)

	INSERT Into LB_CustomerProperty ([CustomerId] , [Key] , [Value]) Values (@NewCustomerId , '_LanguageName' , @LanguageName)

  /*If an Error Occurs Rollback Add*/

  If @@ERROR<>0
   BEGIN
	ROLLBACK
	RETURN
   END
COMMIT
SELECT @NewCustomerId
