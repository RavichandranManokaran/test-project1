﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sitecore.Configuration;
using Sitecore.Data;
using System.Runtime.CompilerServices;
using UCommerce.Sitecore.Extensions;
using UCommerce.Sitecore.SitecoreDataProvider;
using UCommerce.Sitecore;


namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Component
{
    public class SitecoreContext : ISitecoreContext 
    {
        public string BackendDomainName
        {
            get
            {
                return JustDecompileGenerated_get_BackendDomainName();
            }
            set
            {
                JustDecompileGenerated_set_BackendDomainName(value);
            }
        }

        private string JustDecompileGenerated_BackendDomainName_k__BackingField;

        public string JustDecompileGenerated_get_BackendDomainName()
        {
            return this.JustDecompileGenerated_BackendDomainName_k__BackingField;
        }

        private void JustDecompileGenerated_set_BackendDomainName(string value)
        {
            this.JustDecompileGenerated_BackendDomainName_k__BackingField = value;
        }

        public virtual Database DatabaseForContent
        {
            get
            {
                return Factory.GetDatabase(this.NameOfContentDatabase);
            }
        }

        public DataProviderMasterDatabase DataProviderMaster
        {
            get
            {
                if (this.MasterDatabase == null)
                {
                    return null;
                }
                return this.MasterDatabase.GetUcommerceDataProvider();
            }
        }

        public virtual Database MasterDatabase
        {
            get
            {
                return Factory.GetDatabase(this.NameOfContentDatabase);
            }
        }

        private string NameOfContentDatabase
        {
            get;
            set;
        }

        public bool ShouldPullTemplatesFromSitecore
        {
            get
            {
                return JustDecompileGenerated_get_ShouldPullTemplatesFromSitecore();
            }
            set
            {
                JustDecompileGenerated_set_ShouldPullTemplatesFromSitecore(value);
            }
        }

        private bool JustDecompileGenerated_ShouldPullTemplatesFromSitecore_k__BackingField;

        public bool JustDecompileGenerated_get_ShouldPullTemplatesFromSitecore()
        {
            return this.JustDecompileGenerated_ShouldPullTemplatesFromSitecore_k__BackingField;
        }

        private void JustDecompileGenerated_set_ShouldPullTemplatesFromSitecore(bool value)
        {
            this.JustDecompileGenerated_ShouldPullTemplatesFromSitecore_k__BackingField = value;
        }

        public SitecoreContext(string backEndDomainName, string nameOfContentDatabase, bool shouldPullTemplatesFromSitecore)
        {
            this.BackendDomainName = backEndDomainName;
            this.NameOfContentDatabase = nameOfContentDatabase;
            this.ShouldPullTemplatesFromSitecore = shouldPullTemplatesFromSitecore;
        }
    }
}
