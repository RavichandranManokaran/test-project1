﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           ResponseStatus.cs
/// Sub-system/Module:     Communications(VS project name)
/// Description:           Response Status Class
/// </summary>

namespace Affinion.LoyaltyBuild.Communications
{
    public enum ResponseStatus
    {
        SuccessAll,
        SuccessWithErrors,
        Failed
    }
}
