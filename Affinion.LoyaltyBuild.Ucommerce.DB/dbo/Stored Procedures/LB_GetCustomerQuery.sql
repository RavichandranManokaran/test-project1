﻿-- =============================================
--	Name		: GetCustomerQuery
--	Purpose		: To get the GetCustomerQuery
--	Author		: Antonysamy
--	Date		:	11/02/2016
-- =============================================

CREATE PROCEDURE [dbo].[LB_GetCustomerQuery]
	(
	@OrderLineId	INT
	)
AS

BEGIN
SELECT   Q.QueryId
		,Q.QueryTypeId
		,Q.OrderLineId
		,Q.CustomerId
		,Q.Content
		,ISNULL(Q.IsSolved,0) IsSolved
		,ISNULL(PartnerId,0)
		,ISNULL(CampaignId,0)
		,ISNULL(ProviderId,0)
		,Q.CreatedDate
		,Q.CreatedBy
		,Q.UpdatedDate
		,Q.UpdatedBy
FROM LB_CustomerQuery Q 
WHERE OrderLineId = @OrderLineId
Order by Q.QueryId desc
END






