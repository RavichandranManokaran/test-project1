﻿DECLARE 
	@DEFENITIONTYPEID INT,
	@DEFENITIONID INT 

IF NOT EXISTS (SELECT 1 FROM uCommerce_DataType WHERE DefinitionName = 'SiteCoreClientMulti')
BEGIN
	INSERT INTO [dbo].[uCommerce_DataType]
			([TypeName]
			,[Nullable]
			,[ValidationExpression]
			,[BuiltIn]
			,[DefinitionName]
			,[Deleted]
			,[Guid]
			,[CreatedOn]
			,[CreatedBy]
			,[ModifiedOn]
			,[ModifiedBy])
	VALUES
			('ClientMultiSelect'
			,1
			,''
			,0
			,'SiteCoreClientMulti'
			,0
			,NEWID()
			,GETDATE()
			,'Script'
			,GETDATE()
			,'Script')
END
IF NOT EXISTS (SELECT 1 FROM uCommerce_DataType WHERE DefinitionName = 'SiteCorePortalTypeMulti')
BEGIN
	INSERT INTO [dbo].[uCommerce_DataType]
			([TypeName]
			,[Nullable]
			,[ValidationExpression]
			,[BuiltIn]
			,[DefinitionName]
			,[Deleted]
			,[Guid]
			,[CreatedOn]
			,[CreatedBy]
			,[ModifiedOn]
			,[ModifiedBy])
	VALUES
			('PortalTypeMultiSelect'
			,1
			,''
			,0
			,'SiteCorePortalTypeMulti'
			,0
			,NEWID()
			,GETDATE()
			,'Script'
			,GETDATE()
			,'Script')
END

DECLARE @CLIENT INT, @PORTALTYPE INT
SELECT @PORTALTYPE = DataTypeId FROM uCommerce_DataType WHERE DefinitionName = 'SiteCorePortalTypeMulti'
SELECT @CLIENT = DataTypeId FROM uCommerce_DataType WHERE DefinitionName = 'SiteCoreClientMulti'

IF NOT EXISTS (SELECT 1 FROM uCommerce_Definition WHERE name = 'AffinionPayment')
BEGIN
	SELECT TOP 1 @DEFENITIONTYPEID = DefinitionTypeId from uCommerce_DefinitionType WHERE Name = 'PaymentMethod'
	
	INSERT INTO [dbo].[uCommerce_Definition]
    ([Name]
    ,[DefinitionTypeId]
    ,[Description]
    ,[Deleted]
    ,[SortOrder]
    ,[Guid]
    ,[BuiltIn]
    ,[CreatedOn]
    ,[CreatedBy]
    ,[ModifiedOn]
    ,[ModifiedBy])
    VALUES
    ('AffinionPayment'
    ,@DEFENITIONTYPEID
    ,''
    ,0
    ,0
    ,NEWID()
    ,0
    ,GETDATE()
    ,'Script'
    ,GETDATE()
    ,'Script')
END

SELECT @DEFENITIONID = DefinitionId FROM uCommerce_Definition WHERE name = 'AffinionPayment'

IF NOT EXISTS (SELECT 1 FROM uCommerce_DefinitionField WHERE DataTypeId = @CLIENT AND DefinitionId = @DEFENITIONID)
BEGIN
	INSERT INTO [dbo].[uCommerce_DefinitionField]
		([DataTypeId]
		,[DefinitionId]
		,[Name]
		,[DisplayOnSite]
		,[Multilingual]
		,[RenderInEditor]
		,[Searchable]
		,[SortOrder]
		,[Deleted]
		,[BuiltIn]
		,[DefaultValue]
		,[Guid])
	SELECT
		@CLIENT
		,@DEFENITIONID
		,'Client Id'
		,1
		,0
		,1
		,0
		,0
		,0
		,0
		,NULL
		,NEWID()
	UNION
	SELECT
		@PORTALTYPE
		,@DEFENITIONID
		,'Portal Type'
		,1
		,0
		,1
		,0
		,0
		,0
		,0
		,NULL
		,NEWID()
END
IF EXISTS (SELECT 1 FROM uCommerce_Definition WHERE name = 'Realex')
BEGIN
	SELECT @DEFENITIONID = DefinitionId FROM uCommerce_Definition WHERE name = 'Realex'

	IF NOT EXISTS (SELECT 1 FROM uCommerce_DefinitionField WHERE DataTypeId = @CLIENT AND DefinitionId = @DEFENITIONID)
	BEGIN
		INSERT INTO [dbo].[uCommerce_DefinitionField]
		([DataTypeId]
		,[DefinitionId]
		,[Name]
		,[DisplayOnSite]
		,[Multilingual]
		,[RenderInEditor]
		,[Searchable]
		,[SortOrder]
		,[Deleted]
		,[BuiltIn]
		,[DefaultValue]
		,[Guid])
		SELECT
			@CLIENT
			,@DEFENITIONID
			,'Client Id'
			,1
			,0
			,1
			,0
			,0
			,0
			,0
			,NULL
			,NEWID()
		UNION
		SELECT
			@PORTALTYPE
			,@DEFENITIONID
			,'Portal Type'
			,1
			,0
			,1
			,0
			,0
			,0
			,0
			,NULL
			,NEWID()
	END
END
GO