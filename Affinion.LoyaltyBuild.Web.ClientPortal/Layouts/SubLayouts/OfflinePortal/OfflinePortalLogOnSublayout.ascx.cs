﻿//<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           LoginSublayout.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal
/// Description:           Used to Bind data to LoginSublayout subLayout
/// </summary>

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    #region Using directives
    using Affinion.Loyaltybuild.Security;
    using Affinion.LoyaltyBuild.Common;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.DataAccess;
    using Sitecore.Data.Items;
    using Sitecore.Security.AccessControl;
    using Sitecore.Security.Accounts;
    using Sitecore.Security.Authentication;
    using Sitecore.Text;
    using Sitecore.Web;
    using System;
    using System.Collections.Specialized;
    using System.Security.Authentication;
    using System.Web.Security;
    using Affinion.LoyaltyBuild.Web.ClientPortal.Helper;
    using UCommerce.Api;
    using Affinion.LoyaltyBuild.SupplierPortal.DataAccess;
    using Affinion.LoyaltyBuild.SupplierPortal.DataAccess.Service;
    #endregion

    public partial class OfflinePortalLogOnSublayout : System.Web.UI.UserControl
    {
        #region Private Variables
        private string returnURL;
        private string ChangePasswordPageUrl { get; set; }
        private string WelcomePage { get; set; }
        private string ErrorAccountLockout { get; set; }
        private string ErrorInvalidAttempt { get; set; }
        private string ErrorPasswordExpired { get; set; }
        #endregion

        protected readonly ILoginService loginSvc;
        public OfflinePortalLogOnSublayout()
        {
            this.loginSvc = new LoginService();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //initialize page
            InitPage();

            //error message
            errormessage.Visible = false;
            MembershipUser membershipUser = Membership.GetUser(Sitecore.Context.User.Name);

            //set the return url
            if (Request.QueryString.HasKey("returnUrl") && !Request.QueryString.HasKey("returnUrl").Equals(string.Empty))
            {
                returnURL = Request.QueryString["returnUrl"];

            }
            else if (!Sitecore.Context.PageMode.IsPageEditor && Sitecore.Context.User.IsAuthenticated && membershipUser != null && !HasPasswordExpired(membershipUser))
            {
                //No redirection in page editor mode
                Response.Redirect(WelcomePage);
            }            
           
            if (!string.IsNullOrEmpty(TextBoxUser.Text))
            {
                //handle account lockout 
                if (IsLockedOutUser)
                {
                    literalMessage.Text = ErrorAccountLockout;
                    errormessage.Visible = true;
                }
            }
        }

        protected void ButtonLogOn_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                try
                {
                    MembershipUser user = DomainUser;

                    if (user == null)
                    {
                        throw new ArgumentException("Username");
                    }
                    else if (!IsAuthorizedUser(User.FromName(user.UserName, false)))
                    {
                        //User is not authorized to access the site
                        throw new ArgumentException("Unauthorized User");
                    }

                    if (SitecoreAccess.LogOn(user.UserName, TextBoxPassword.Text, false))
                    {
                        TransactionLibrary.ClearBasket();
                        //login success
                        string[] currentRole = System.Web.Security.Roles.GetRolesForUser(user.UserName);
                        string allRoles = string.Join(",", currentRole);
                        SitecoreUserProfileHelper.AddItem(allRoles, UserProfileKeys.LoggedInUserRoles);
                        string[] userArray = user.UserName.Split('\\');
                        string sessionUserName = (userArray.Length == 1) ? userArray[0] : userArray[1];
                        //SitecoreUserProfileHelper.AddItem(sessionUserName, UserProfileKeys.CreatedBy);
                        LogOnSuccess(user);
                    }
                    else
                    {
                        //handle account lockout 
                        if (IsLockedOutUser)
                        {
                            ErrorMessage(ErrorAccountLockout);
                            return;
                        }
                        else
                        {
                            throw new AuthenticationException();
                        }
                    }
                }
                catch (AuthenticationException)
                {
                    ErrorMessage(ErrorInvalidAttempt);
                }
                catch (ArgumentException)
                {
                    ErrorMessage(ErrorInvalidAttempt);
                }
                catch (Exception ex)
                {
                    //Log 
                    Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                    throw;
                }
            }
        }


        #region Private Methods

        /// <summary>
        /// Error messages
        /// </summary>
        /// <param name="message"></param>
        private void ErrorMessage(string message)
        {
            literalMessage.Text = message;
            errormessage.Visible = true;
        }

        /// <summary>
        /// logOn Success handler
        /// </summary>
        /// <param name="user">MembershipUser</param>
        private void LogOnSuccess(MembershipUser user)
        {
            PasswordHistoryDataAccess passwordHistory = new PasswordHistoryDataAccess();
            var r = new Random();
            string uniqueNumber = loginSvc.GenerateRandomNumbers(r);
            loginSvc.InsertChangePasswordHistory(uniqueNumber, user.UserName);

            //Check if the given password available in password history records

            if (!passwordHistory.Validate(user.UserName, TextBoxPassword.Text, true))
            {
                //set query string params
                UrlString url = new UrlString(ChangePasswordPageUrl);
                url.Append("id", uniqueNumber);
                WebUtil.Redirect(url.GetUrl());
            }
            //commented due to LWD-1046
            //else if (!string.IsNullOrEmpty(returnURL))
            //{
            //    SitecoreUserProfileHelper.AddItem( "0","IsUserPasswordChanged");
            //    WebUtil.Redirect(returnURL);
            //}
            else
            {
                SitecoreUserProfileHelper.AddItem(user.UserName, UserProfileKeys.CreatedBy);
                SitecoreUserProfileHelper.AddItem("0", "IsUserPasswordChanged");
                Response.Redirect(WelcomePage);
            }
        }

        /// <summary>
        /// Domain User
        /// </summary>
        private MembershipUser DomainUser
        {
            get
            {
                try
                {
                    string username = TextBoxUser.Text;

                    if (!string.IsNullOrEmpty(username))
                    {
                        string domainUser = GetDomainUserName(username);

                        if (User.Exists(domainUser))
                        {
                            return Membership.GetUser(domainUser);
                        }
                    }

                    return null;
                }
                catch (ArgumentException ex)
                {
                    //Log
                    Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                    return null;
                }
                catch (Exception ex)
                {
                    //Log
                    Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                    throw;
                }
            }

        }

        /// <summary>
        /// Check User Authrization
        /// </summary>
        /// <returns></returns>
        private static bool IsAuthorizedUser(User user)
        {
            using (new UserSwitcher(ItemHelper.ContentEditingUser))
            {
                return SitecoreAccess.IsAllowed(user, AccessRight.ItemRead, Sitecore.Context.Site.RootPath, Sitecore.Context.Database.ToString());
            }
        }

        /// <summary>
        /// Get the fully qualified user name 
        /// </summary>
        /// <param name="username">user name</param>
        /// <returns>qualified user name</returns>
        private string GetDomainUserName(string username)
        {
            try
            {
                string[] userarr = username.Split('\\');

                if (userarr.Length == 2 && userarr[0].ToLower().Equals("sitecore"))
                {
                    return username;
                }
                else
                {
                    Sitecore.Security.Domains.Domain domain = Sitecore.Context.Domain;
                    return domain.Name + @"\" + username;
                }
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Check user lock out
        /// </summary>
        private bool IsLockedOutUser
        {
            get
            {
                //get domain user
                var user = DomainUser;
                return (user != null && user.IsLockedOut);
            }

        }

        /// <summary>
        /// Get url from link field
        /// </summary>
        /// <param name="item">Item</param>
        /// <param name="fieldName">Field name</param>
        /// <returns>Url</returns>
        private static string GetLink(Item item, string fieldName)
        {
            var link = SitecoreFieldsHelper.GetUrl(item, fieldName);
            return link.Url;
        }


        /// <summary>
        /// Page Settings
        /// </summary>
        private void SetSettings()
        {
            try
            {
                using (new UserSwitcher(ItemHelper.ContentEditingUser))
                {
                    string itemPath = Constants.OfflinePortalSettings;
                    Item settings = Sitecore.Context.Database.GetItem(itemPath);
                    if (settings != null)
                    {
                        //Links
                        this.WelcomePage = GetLink(settings, "Welcome");
                        this.HyperLinkForgotPassword.NavigateUrl = GetLink(settings, "ForgotPassword");
                        this.ChangePasswordPageUrl = GetLink(settings, "ChangePassword");
                    }
                }

            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }

        }

        /// <summary>
        /// Check password expireation
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private bool HasPasswordExpired(MembershipUser username)
        {

            return username.LastPasswordChangedDate.ToLocalTime().Add(new TimeSpan(90, 0, 0, 0)) <= DateTime.Now;
        }

        /// <summary>
        /// initialize the page
        /// </summary>
        private void InitPage()
        {
            try
            {
                //Buttons
                this.ButtonLogOn.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Login", "Log In");

                //Messages
                this.ErrorAccountLockout = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "AccountLockOut", "Your account has been locked out because of too many invalid login attempts.");
                this.ErrorInvalidAttempt = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "InvalidUsernameorPassword", "Invalid username or password. Too many invalid login attempts cause account locked out.");


                SetSettings();
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }
        #endregion
    }
}