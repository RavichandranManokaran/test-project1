﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           ChangePassword.aspx.cs
/// Sub-system/Module:     Common(VS project name)
/// Description:           Change the user password
/// </summary>

using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using System;
using System.Web;
using System.Web.Security;

namespace Affinion.LoyaltyBuild.Web.Common.Layouts.ExtendedLayouts
{
    /// <summary>
    /// Page to handle change password
    /// </summary>
    public partial class ChangePassword : System.Web.UI.Page
    {
        private const string redirectTo = "~/sitecore";

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                QueryStringHelper stringHelper = new QueryStringHelper(HttpContext.Current.Request);
                var user = stringHelper.GetValue("user");
                if (string.IsNullOrEmpty(user))
                {
                    Server.TransferRequest(redirectTo);
                }

                UserChangePasswordController.UserName = user;

            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                Server.TransferRequest(redirectTo);
            }
        }

    }
}