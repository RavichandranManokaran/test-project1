﻿using Affinion.LoyaltyBuild.SupplierPortal.DataAccess;
using Affinion.LoyaltyBuild.SupplierPortal.DataAccess.Data;
using Affinion.LoyaltyBuild.SupplierPortal.DataAccess.Test;
using Affinion.LoyaltyBuild.SupplierPortal.Reports;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration.SupplierPortal.User;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration.SupplierPortal.Reports
{
    /// <summary>
    /// base user control for the report pages
    /// </summary>
    public class ReportBase : UserControl
    {
        protected readonly IProviderService _providerService;
        protected readonly IReportService _reportService;

        public ReportBase(IProviderService providerService, IReportService reportService)
        {
            _providerService = providerService;
            _reportService = reportService;
        }
        
        /// <summary>
        /// Get the list of provider for the provider drop down
        /// </summary>
        public List<Provider> ProviderList
        {
            get
            {
                string sku = SitecoreUserProfile.SelectedSKU;

                //get the provider list based on logged in user role
                switch(UserHelper.CurrentUserRole)
                {
                    case UserRole.LoyaltyAdmin:
                        return _providerService.GetAllProviders();
                    case UserRole.SupplierGroupAdmin:                        
                        //need to do actual implementation
                        string loggedInProvider= SitecoreUserProfile.GetItem(UserProfileKeys.loggedInProviderId);
                        return 
                            !string.IsNullOrWhiteSpace(loggedInProvider)? _providerService.GetProvidersByHotelGroup(loggedInProvider) : null;
                       
                    case UserRole.Supplier:
                        var list = new List<Provider>();
                        //need to do actual implementation
                      list.Add(_providerService.GetProviderById(SitecoreUserProfile.SelectedSKU));
                       return list;
                      //  return null;
                }

                return null;
            }
        }

        /// <summary>
        /// Bind drop down list
        /// </summary>
        /// <param name="ddlControl"></param>
        /// <param name="text"></param>
        /// <param name="value"></param>
        public void BindDropdown(DropDownList ddlControl, object dataSource, string text, string value)
        {
            ddlControl.DataSource = dataSource;
            ddlControl.DataTextField = text;
            ddlControl.DataValueField = value;
            ddlControl.DataBind();
        }
        
        /// <summary>
        /// cast string to datetime
        /// </summary>
        /// <param name="value">string text</param>
        /// <param name="deflt">default value</param>
        /// <returns></returns>
        public DateTime? GetDateValue(string value, DateTime? deflt = null)
        {
            DateTime? output = deflt;
            DateTime temp;

            if (DateTime.TryParse(value, out temp))
                output = temp;

            return output;
           
        }

        /// <summary>
        /// cast string to int
        /// </summary>
        /// <param name="value">string text</param>
        /// <param name="deflt">default value</param>
        /// <returns></returns>
        public int? GetIntValue(string value, int? deflt = null)
        {
            int? output = deflt;
            int temp;

            if (int.TryParse(value, out temp))
                output = temp;

            return output;

        }

        public string GetStringValue(string value)
        {
            return string.IsNullOrWhiteSpace(value) ? null : value.Trim();
        }
        
    }

    
}