﻿namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    #region Using Statements
    using Affinion.Loyaltybuild.BusinessLogic.Helper;
    using Affinion.LoyaltyBuild.Common;
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.DataAccess.Models;
    using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
    using Affinion.LoyaltyBuild.Search.Helpers;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Linq;
    using System.Web.UI.WebControls;
    using Affinion.LoyaltyBuild.Common.SessionHelper;
    using UCommerce.Api;
    using Sitecore.Data.Managers;
    #endregion

    public partial class OfflinePortalCustomerSearchNew : BaseSublayout
    {
        //private const string ClientListQuery = @"fast:/sitecore/content/#admin-portal#/#client-setup#/*[@Active='1']";
        //private const string LanguageListQuery = @"fast:/sitecore/system/Languages/*";
        private string SelectText = "Please Select";
        static int customerIdd;
        #region Page Load
        private void Page_Load(object sender, EventArgs e)
        {
            string customerEmail = Request.QueryString["Email"];
            DataSet customerDataSet = new DataSet();
            Page.MaintainScrollPositionOnPostBack = false;
            PanelPayment.Visible = false;
            if (!IsPostBack)
            {
                BindSitecoreText();
                FetchLanguageDropDown();
                InitPage();
                if (!string.IsNullOrWhiteSpace(customerEmail))
                {
                    customerDataSet = uCommerceConnectionFactory.GetCustomerDetailByEmail(customerEmail);
                    if (customerDataSet.Tables[0].Rows.Count > 0)
                    {
                        this.LoadCustomerDetailesByEmail(customerDataSet);
                        int.TryParse(customerDataSet.Tables[0].Rows[0][0].ToString(), out customerIdd);
                    }
                    else
                    {
                        Email.Text = customerEmail;
                        Email.ReadOnly = true;
                        Search.Enabled = false;
                        var order = TransactionLibrary.GetBasket(false).PurchaseOrder;
                        string clientId = order["clientId"].ToString().Replace("{", "").Replace("}", "").ToLower();
                        if (!string.IsNullOrWhiteSpace(clientId))
                        {
                            ClientDropDown.Items.FindByValue(clientId).Selected = true;
                            ClientDropDown.Enabled = false;
                        }

                    }
                }

            }
        }
        #endregion

        #region protected Methods
        protected void Search_Click(object sender, EventArgs e)
        {
            successmessage.Visible = false;

            if (Page.IsValid)
            {
                //ResetEditForm(false);

                //id column 
                ResultGridView.Columns[0].Visible = true;

                DataSet result = GetCustomerDetailes();

                ResultGridView.AutoGenerateColumns = false;

                ResultGridView.DataSource = result;
                ResultGridView.DataBind();
                ResultGridView.SelectedIndex = -1;

                //hide id column 
                ResultGridView.Columns[0].Visible = false;

                if (result.Tables[0].Rows.Count > 0)
                {
                    ResultDiv.Visible = true;
                    errormessage.Visible = false;

                    //Focus Search Result Grid
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "CallFocusSearchResultGrid", "FocusSearchResultGrid()", true);
                }
                else
                {
                    ErrorMessageLiteral.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "NoResultsFound", "No Results Found");
                    errormessage.Visible = true;
                    ResultDiv.Visible = false;
                }
            }
        }

        protected void ResultGridView_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Get the currently selected row using the SelectedRow property.
            GridViewRow row = ResultGridView.SelectedRow;

            customerIdd = 0;

            if (int.TryParse(row.Cells[0].Text, out customerIdd))
            {
                LoadCustomerDetailes(customerIdd);
                AddAndSave.Enabled = false;
                EditAndSave.Enabled = true;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "CallExpanEditForm", "ExpanEditForm()", true);
            }
            PanelPayment.Visible = true;
        }

        protected void SearchReset_Click(object sender, EventArgs e)
        {
            //ResetRearchForm();
        }

        protected void Reset_Click(object sender, EventArgs e)
        {
            ResetEditForm(true);
        }

        protected void AddAndSave_Click(object sender, EventArgs e)
        {
            errormessage.Visible = false;
            ResultDiv.Visible = false;

            if (Page.IsValid)
            {
                this.RecordCustomer();
            }
            Search.Enabled = true;
        }

        protected void ResultGridView_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        protected void EditAndSave_Click(object sender, EventArgs e)
        {
            errormessage.Visible = false;
            ResultDiv.Visible = false;

            if (Page.IsValid)
            {
                this.RecordCustomer(true);
            }
            Search.Enabled = true;
            //recently added by kumaran
            AddAndSave.Enabled = true;
        }

        protected void Subscribe_Click(object sender, EventArgs e)
        {
            string message = string.Empty;

            if (Subscribe(DropDownClientListSubscribe.SelectedValue, EmailSubscribe.Text.Trim(), ref message))
            {
                successmessage.Visible = true;
                errormessage.Visible = false;
                //ResetEditForm(true);
            }
            else
            {
                //if email exist
                if (string.IsNullOrEmpty(message))
                {
                    SubscribeMessage.Text = ErrorMessageLiteral.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Failed", "Operation Failed");
                }
                else
                {
                    SubscribeMessage.Text = ErrorMessageLiteral.Text = message;
                }

                successmessage.Visible = false;
                errormessage.Visible = true;

            }
        }

        protected void SubscribeReset_Click(object sender, EventArgs e)
        {
            ResetSubscribeForm(true);
        }

        protected void Email_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Email.Text) && !string.IsNullOrWhiteSpace(Email.Text))
            {
                SubscribeCheckBox.Enabled = true;
            }

            ValidateFields();
        }

        protected void ClientDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ClientDropDown.SelectedIndex != 0)
            {
                StoreDropDown.Enabled = true;
                Database currentDB = Sitecore.Context.Database;
                Item clientItem = currentDB.GetItem(ClientDropDown.SelectedValue);

                if (clientItem != null)
                {
                    this.LoadClientSpecificDropDown("Branches", "BranchLocationName", "BranchLocationName", StoreDropDown, clientItem.Name);
                }

                ValidateFields();
            }
        }

        protected void UnSubscribe_Click(object sender, EventArgs e)
        {
            string message = string.Empty;

            if (Unsubscribe(DropDownClientListSubscribe.SelectedValue, EmailSubscribe.Text.Trim(), ref message))
            {
                successmessage.Visible = true;
                errormessage.Visible = false;
            }
            else
            {
                //if email exist
                if (string.IsNullOrEmpty(message))
                {
                    SubscribeMessage.Text = ErrorMessageLiteral.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Failed", "Operation Failed");
                }
                else
                {
                    SubscribeMessage.Text = ErrorMessageLiteral.Text = message;
                }

                successmessage.Visible = false;
                errormessage.Visible = true;

            }

        }
        #endregion

        #region Private Methods
        /// <summary>
        /// initialize the page
        /// </summary>
        private void InitPage()
        {
            try
            {
                //Load Drop downs
                this.LoadDropDowns();

                //Load Regular Expression Validate
                this.LoadSitecoreFieldData();

                //Bind Table Headers
                this.BindGridViewHeaders(ResultGridView);

            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Bind Table Header
        /// </summary>
        private void BindGridViewHeaders(GridView gridView)
        {

            string fullname = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "FullName", "Full Name");
            string fullAddress = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "FullAddress", "Full Address");
            string emailAddress = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "EmailAddress", "Email Address");
            string mobilePhoneNumber = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "MobilePhoneNumber", "Mobile Phone Number");

            BindBoundField("CustomerId", "CustomerId", gridView);
            BindBoundField(fullname, "FullName", gridView);
            BindBoundField(fullAddress, "FullAddress", gridView);
            BindBoundField(emailAddress, "EmailAddress", gridView);
            BindBoundField(mobilePhoneNumber, "MobilePhoneNumber", gridView);

            BindCommandField(gridView);

        }

        /// <summary>
        /// Bind Bound Field to grid
        /// </summary>
        /// <param name="headerText">Header Text</param>
        /// <param name="dataField"><Data Field/param>
        /// <param name="grid">Grid</param>
        private void BindBoundField(string headerText, string dataField, GridView grid)
        {
            BoundField boundField = new BoundField();
            boundField.HeaderText = headerText;
            boundField.DataField = dataField;
            grid.Columns.Add(boundField);
        }

        /// <summary>
        /// Bind Command Field
        /// </summary>
        /// <param name="grid">Grid</param>
        private void BindCommandField(GridView grid)
        {
            CommandField commandField = new CommandField();
            commandField.ShowSelectButton = true;
            commandField.SelectText = "<span class=\"btn btn-default add-break pull-right font-size-12\">Select Customer</span>";
            grid.Columns.Add(commandField);

        }

        /// <summary>
        /// Load Drop Downs
        /// </summary>
        private void LoadDropDowns()
        {
            try
            {
                //this.LoadSearchLocationDropDown();
                //this.LoadSearchCountryDropDown();
                this.LoadCountryDropDown();
                this.LoadLocationDropDown();
                this.LoadTitles();
                this.LoadClientDropDown();
                this.LoadClientDropDownSubscription();
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }

        }

        /// <summary>
        /// LoadRegular Expression Validate
        /// </summary>
        private void LoadSitecoreFieldData()
        {
            try
            {
                this.Search.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Search", "Search");
                //this.SearchReset.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Reset", "Reset");
                this.AddAndSave.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "AddAndSave", "Add & Save");
                this.EditAndSave.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "EditAndSave", "Edit Details");
                this.Reset.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Reset", "Reset");
                this.SuccessLiteral.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Success", "Operation Succeeded");
                this.SelectText = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "PleaseSelectText", "Please Select");
                this.ButtonSubscribe.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Subscribe", "Subscribe");
                this.UnSubscribe.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Unsubscribe", "Unsubscribe");
                this.SubscribeReset.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Reset", "Reset");

                string invalidPhoneNumberText = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "InvalidPhoneNumber", "Invalid Phone Number");

                this.RegularExpressionValidatorPhone.ValidationExpression = Constants.PhoneNumberValidationRegex;
                //this.RegularExpressionValidatorPhoneSearch.ValidationExpression = Constants.PhoneNumberValidationRegex;
                this.RegularExpressionValidatorSecondaryPhone.ValidationExpression = Constants.PhoneNumberValidationRegex;
                this.RegularExpressionValidatorEmail.ValidationExpression = Constants.EmailValidationRegex;
                this.RegularExpressionValidatorEmailSubscription.ValidationExpression = Constants.EmailValidationRegex;
                //this.RegularExpressionValidatorEmailUnsubscription.ValidationExpression = Constants.EmailValidationRegex;

                this.RegularExpressionValidatorPhone.ErrorMessage = invalidPhoneNumberText;
                //this.RegularExpressionValidatorPhoneSearch.ErrorMessage = invalidPhoneNumberText;
                this.RegularExpressionValidatorSecondaryPhone.ErrorMessage = invalidPhoneNumberText;
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }

        }

        /// <summary>
        /// Fire validate
        /// </summary>
        private void ValidateFields()
        {
            this.RegularExpressionValidatorPhone.Validate();
            this.RegularExpressionValidatorSecondaryPhone.Validate();
            this.RegularExpressionValidatorEmail.Validate();
            this.RegularExpressionValidatorEmailSubscription.Validate();
            this.RegularExpressionValidatorPhone.Validate();
            this.RegularExpressionValidatorSecondaryPhone.Validate();
        }
        /// <summary>
        /// Unsubscribe
        /// </summary>
        /// <param name="email">email</param>
        /// <param name="clientItem">Item</param>
        /// <param name="path">Item path</param>
        /// <param name="massage">message</param>
        /// <returns></returns>
        private bool Unsubscribe(string clientId, string email, ref string massage)
        {
            try
            {
                if (!string.IsNullOrEmpty(clientId) && !string.IsNullOrEmpty(email))
                {
                    Item clientSetupItem = Sitecore.Context.Database.GetItem(clientId);
                    Item clientItem = Sitecore.Context.Database.GetItem(@"/sitecore/content/client-portal/" + clientSetupItem.Name);

                    return SubscribeHelper.DeleteSubscription(email, clientItem, Constants.FolderName);
                }
                return false;
            }
            catch (ArgumentException ex)
            {
                massage = ex.Message;
                return false;
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Get Customer details by id
        /// </summary>
        /// <param name="customerId">customer Id</param>
        private void LoadCustomerDetailes(int customerId)
        {
            try
            {
                DataSet customerDataSet = this.GetCustomerDetailesById(customerId);

                var dataRow = customerDataSet.Tables[0].Rows[0];
                bool active = false;
                bool subscribe = false;

                CustomerId.Value = dataRow["CustomerId"].ToString();
                FirstName.Text = dataRow["FirstName"].ToString();
                LastName.Text = dataRow["LastName"].ToString();
                AddressLine1.Text = dataRow["AddressLine1"].ToString();
                AddressLine2.Text = dataRow["AddressLine2"].ToString();
                AddressLine3.Text = dataRow["AddressLine3"].ToString();
                PrimaryPhone.Text = dataRow["MobilePhoneNumber"].ToString();
                if (dataRow["LanguageName"] != null)
                    LanguageDropDown.SelectedValue = dataRow["LanguageName"].ToString();
                SecondaryPhone.Text = dataRow["PhoneNumber"].ToString();
                Email.Text = dataRow["EmailAddress"].ToString();

                TitleDropDown.SelectedValue = dataRow["Title"].ToString();
                CountryDropDown.SelectedValue = dataRow["CountryId"].ToString();
                LocationDropDown.SelectedValue = dataRow["LocationId"].ToString();
                ClientDropDown.SelectedValue = dataRow["ClientId"].ToString();

                if (!string.IsNullOrEmpty(dataRow["BranchId"].ToString()))
                {
                    StoreDropDown.Enabled = true;
                    StoreDropDown.SelectedValue = dataRow["BranchId"].ToString();
                }

                if (bool.TryParse(dataRow["Active"].ToString(), out active))
                {
                    ActiveCheckBox.Checked = active;
                }

                if (bool.TryParse(dataRow["Subscribe"].ToString(), out subscribe))
                {
                    SubscribeCheckBox.Checked = subscribe;
                }

                this.ClientDropDown_SelectedIndexChanged(null, null);

                this.Email_TextChanged(null, null);


            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
            }
        }

        private void LoadCustomerDetailesByEmail(DataSet customerInfo)
        {
            try
            {

                var dataRow = customerInfo.Tables[0].Rows.Count > 0 ? customerInfo.Tables[0].Rows[0] : null;
                bool active = false;
                bool subscribe = false;
                CustomerId.Value = dataRow["CustomerId"].ToString();
                FirstName.Text = dataRow["FirstName"].ToString();
                LastName.Text = dataRow["LastName"].ToString();
                AddressLine1.Text = dataRow["AddressLine1"].ToString();
                AddressLine2.Text = dataRow["AddressLine2"].ToString();
                AddressLine3.Text = dataRow["AddressLine3"].ToString();
                PrimaryPhone.Text = dataRow["MobilePhoneNumber"].ToString();
                SecondaryPhone.Text = dataRow["PhoneNumber"].ToString();

                if (dataRow["LanguageName"] != null)
                    SetLanguageDropDownValue(dataRow["LanguageName"].ToString());

                Email.Text = dataRow["EmailAddress"].ToString();
                TitleDropDown.SelectedValue = dataRow["Title"].ToString();
                CountryDropDown.SelectedValue = dataRow["CountryId"].ToString();
                LocationDropDown.SelectedValue = dataRow["LocationId"].ToString();
                ClientDropDown.SelectedValue = dataRow["ClientId"].ToString();
                ClientDropDown.Enabled = false;
                if (!string.IsNullOrEmpty(dataRow["BranchId"].ToString()))
                {
                    StoreDropDown.Enabled = true;
                    StoreDropDown.SelectedValue = dataRow["BranchId"].ToString();
                }

                if (bool.TryParse(dataRow["Active"].ToString(), out active))
                {
                    ActiveCheckBox.Checked = active;
                }

                if (bool.TryParse(dataRow["Subscribe"].ToString(), out subscribe))
                {
                    SubscribeCheckBox.Checked = subscribe;
                }
                this.ClientDropDown_SelectedIndexChanged(null, null);
                this.Email_TextChanged(null, null);
                Search.Enabled = false;
                Email.ReadOnly = true;
                AddAndSave.Enabled = false;
                EditAndSave.Enabled = true;
                this.PanelPayment.Visible = true;

            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
            }
        }

        private void SetLanguageDropDownValue(string languageCode)
        {
            try
            {
                ListItem item = LanguageDropDown.Items.FindByValue(languageCode);
                if (item != null)
                    LanguageDropDown.SelectedValue = languageCode;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Search Result
        /// </summary>
        /// <param name="countyId">Country Id</param>
        /// <param name="locationId">Location Id</param>
        /// <returns>SearchResult</returns>
        //private DataSet GetSearchResult(int countyId, int locationId)
        //{
        //    try
        //    {

        //        if (countyId > 0 && locationId > 0)
        //        {
        //            return this.GetCustomerDetailes(countyId, locationId);
        //        }
        //        else if (countyId > 0)
        //        {
        //            return this.GetCustomerDetailes(countyId, null);
        //        }
        //        else if (locationId > 0)
        //        {
        //            return this.GetCustomerDetailes(null, locationId);
        //        }
        //        else
        //        {
        //            return this.GetCustomerDetailes(null, null);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        //Log 
        //        Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
        //        throw;
        //    }
        //}

        /// <summary>
        /// Populate Search Location DropDown
        /// </summary>
        //private void LoadSearchLocationDropDown()
        //{
        //    try
        //    {
        //        //Bind locationList data to the drop-down
        //        SearchLocationDropDown.DataSource = uCommerceConnectionFactory.GetLocationList();
        //        SearchLocationDropDown.DataTextField = "Location";
        //        SearchLocationDropDown.DataValueField = "LocationTargetId";
        //        SearchLocationDropDown.DataBind();
        //        ListItem selectLocation = new ListItem(SelectText, string.Empty);
        //        SearchLocationDropDown.Items.Insert(0, selectLocation);
        //    }
        //    catch (Exception ex)
        //    {
        //        //Log 
        //        Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
        //        throw;
        //    }

        //}

        /// <summary>
        ///  Populate Search Country DropDown
        /// </summary>
        //private void LoadSearchCountryDropDown()
        //{
        //    try
        //    {
        //        //Get Ucommerce Country list
        //        var countryList = UCommerce.Api.TransactionLibrary.GetCountries();

        //        //Bind countryList data to the drop down
        //        SearchCountryDropDown.DataSource = UCommerceHelper.GetDropdownDataSource(countryList != null ? countryList.ToList() : null);
        //        SearchCountryDropDown.DataTextField = "Text";
        //        SearchCountryDropDown.DataValueField = "Value";
        //        SearchCountryDropDown.DataBind();
        //        ListItem selectCountry = new ListItem(SelectText, string.Empty);
        //        SearchCountryDropDown.Items.Insert(0, selectCountry);
        //    }
        //    catch (Exception ex)
        //    {
        //        //Log 
        //        Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
        //        throw;
        //    }
        //}

        /// <summary>
        ///  Populate Search Country DropDown
        /// </summary>


        /// <summary>
        /// Populate Location DropDown
        /// </summary>
        private void LoadLocationDropDown()
        {
            try
            {
                //Bind locationList data to the drop down
                LocationDropDown.DataSource = uCommerceConnectionFactory.GetLocationList();
                LocationDropDown.DataTextField = "Location";
                LocationDropDown.DataValueField = "LocationTargetId";
                LocationDropDown.DataBind();
                ListItem selectLocation = new ListItem(SelectText, string.Empty);
                LocationDropDown.Items.Insert(0, selectLocation);
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }

        }

        /// <summary>
        ///  Populate Country DropDown
        /// </summary>
        private void LoadCountryDropDown()
        {
            try
            {
                //Get Ucommerce Country list
                var countryList = UCommerce.Api.TransactionLibrary.GetCountries();

                //Bind countryList data to the drop down
                CountryDropDown.DataSource = UCommerceHelper.GetDropdownDataSource(countryList != null ? countryList.ToList() : null);
                CountryDropDown.DataTextField = "Text";
                CountryDropDown.DataValueField = "Value";
                CountryDropDown.DataBind();
                ListItem selectCountry = new ListItem(SelectText, string.Empty);
                CountryDropDown.Items.Insert(0, selectCountry);
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Load Client Specific Drop Down
        /// </summary>
        private void LoadClientSpecificDropDown(string dropdownFieldName, string targetItemsTextField, string targetItemsValueField, ListControl dropdown, string client)
        {
            try
            {
                //query for the client item in admin portal
                Item clientItem = Sitecore.Context.Database.GetItem("/sitecore/content/admin-portal/client-setup/" + client + "");
                if (clientItem != null)
                {
                    //Bind data to drop-down
                    Collection<Item> items = SitecoreFieldsHelper.GetMutiListItems(clientItem, dropdownFieldName);
                    dropdown.DataSource = SitecoreFieldsHelper.GetDropdownDataSource(items, targetItemsTextField, targetItemsValueField);
                    dropdown.DataTextField = "Text";
                    dropdown.DataValueField = "Value";
                    dropdown.DataBind();
                    //Set the default list item
                    ListItem select = new ListItem(SelectText, string.Empty);
                    dropdown.Items.Insert(0, select);
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }

        }

        /// <summary>
        /// Populate Titles
        /// </summary>
        private void LoadTitles()
        {
            Sitecore.Data.Database currentDb = Sitecore.Context.Database;

            //Load location data to a locationList
            Item titleList = currentDb.GetItem(Constants.TitleListPath);
            TitleDropDown.AppendDataBoundItems = true;
            if (titleList != null)
            {
                var titles = (from loc in titleList.Children
                              orderby loc.DisplayName
                              select loc.DisplayName);

                //Bind locationList data to the drop down
                //TitleDropDown.SelectedIndex = 0;
                TitleDropDown.DataSource = titles != null ? titles.ToList() : null;
                TitleDropDown.DataBind();
            }
            ListItem selectTitle = new ListItem(SelectText, string.Empty);
            TitleDropDown.Items.Insert(0, selectTitle);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private IList<string> GetClientList()
        {
            try
            {
                /// Get Client items
                //Sitecore.Data.Database currentDb = Sitecore.Context.Database;
                var clientItems = SearchHelper.GetUserSpecificClientItems(); //currentDb.SelectItems(ClientListQuery);

                if (clientItems != null)
                {
                    /// Extract Clients Filtered Items
                    var clientIds = (from client in clientItems
                                     orderby client.DisplayName
                                     select client.ID.Guid.ToString());
                    return clientIds != null ? clientIds.ToList() : null;
                }

                return null;
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Subscribe
        /// </summary>
        /// <param name="clientItemId">Client Item Id</param>
        /// <param name="email">Email</param>
        private bool Subscribe(string clientItemId, string email, ref string message)
        {
            try
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "CallExpanSubscribForm", "ExpanSubscribForm()", true);

                if (!string.IsNullOrEmpty(clientItemId))
                {
                    Item clientSetupItem = Sitecore.Context.Database.GetItem(clientItemId);
                    Item clientItem = Sitecore.Context.Database.GetItem(@"/sitecore/content/client-portal/" + clientSetupItem.Name);

                    return SubscribeHelper.CreateSubscription(email, clientItem, Constants.FolderName);

                }
                return false;

            }
            catch (ArgumentException ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                //Email already exist
                message = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "EmailAlreadyExist", "Email Already Exist"); ;

                return false;
            }

            catch (Exception ex)
            {
                //SubscribeMessage.Text = ex.Message;
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;

            }
        }

        /// <summary>
        /// Load Client Drop Down
        /// </summary>
        private void LoadClientDropDown()
        {
            /// Get Client items
            //Sitecore.Data.Database currentDb = Sitecore.Context.Database;
            var clientItems = SearchHelper.GetUserSpecificClientItems();//currentDb.SelectItems(ClientListQuery);

            if (clientItems != null)
            {
                /// Extract Clients Filtered Items
                var clientFilterList = (from client in clientItems
                                        orderby client.DisplayName
                                        select new { DiplayName = client.DisplayName, Id = client.ID.Guid });

                /// Load Client List Drop down
                this.ClientDropDown.DataSource = clientFilterList != null ? clientFilterList.ToList() : null;

                /// Set data text and data value fields
                ClientDropDown.DataTextField = "DiplayName";
                ClientDropDown.DataValueField = "Id";
                this.ClientDropDown.DataBind();

            }

            ListItem selectClient = new ListItem(SelectText, string.Empty);
            ClientDropDown.Items.Insert(0, selectClient);

        }

        ///// <summary>
        ///// Load Client Drop Down
        ///// </summary>
        //private void LoadBranchDropDown()
        //{
        //    /// Get Client items
        //    Sitecore.Data.Database currentDb = Sitecore.Context.Database;
        //    var clientItems = currentDb.SelectItems();

        //    if (clientItems != null)
        //    {
        //        /// Extract Clients Filtered Items
        //        var clientFilterList = (from client in clientItems
        //                                orderby client.DisplayName
        //                                select new { DiplayName = client.DisplayName, Id = client.ID });

        //        /// Load Client List Drop down
        //        this.ClientDropDown.DataSource = clientFilterList != null ? clientFilterList.ToList() : null;

        //        /// Set data text and data value fields
        //        ClientDropDown.DataTextField = "DiplayName";
        //        ClientDropDown.DataValueField = "Id";

        //        this.ClientDropDown.DataBind();
        //    }

        //    ListItem selectClient = new ListItem(SelectText, string.Empty);
        //    ClientDropDown.Items.Insert(0, selectClient);

        //}

        /// <summary>
        /// Load Client Drop Down Subscription
        /// </summary>
        private void LoadClientDropDownSubscription()
        {
            /// Get Client items
            //Sitecore.Data.Database currentDb = Sitecore.Context.Database;
            var clientItems = SearchHelper.GetUserSpecificClientItems();//currentDb.SelectItems(ClientListQuery);

            if (clientItems != null)
            {
                /// Extract Clients Filtered Items
                var clientFilterList = (from client in clientItems
                                        orderby client.DisplayName
                                        select new { DiplayName = client.DisplayName, Id = client.ID.Guid });

                /// Load Client List Drop down
                this.DropDownClientListSubscribe.DataSource = clientFilterList != null ? clientFilterList.ToList() : null;

                /// Set data text and data value fields
                DropDownClientListSubscribe.DataTextField = "DiplayName";
                DropDownClientListSubscribe.DataValueField = "Id";

                this.DropDownClientListSubscribe.DataBind();
            }


            ListItem selectClient = new ListItem(SelectText, string.Empty);
            DropDownClientListSubscribe.Items.Insert(0, selectClient);

        }

        ///// <summary>
        ///// Load Language Drop Down
        ///// </summary>
        //private void LoadLanguageDropDown()
        //{
        //    /// Get Client items
        //    Sitecore.Data.Database currentDb = Sitecore.Context.Database;

        //    //Load location data to a locationList
        //    var LanguageList = currentDb.SelectItems(LanguageListQuery);

        //    if (LanguageList != null)
        //    {
        //        /// Extract Clients Filtered Items
        //        var languageFilterList = (from language in LanguageList
        //                                  select new { DiplayName = language.DisplayName, Id = language.ID });

        //        /// Load Client List Drop down
        //        LanguageDropDown.DataSource = languageFilterList != null ? languageFilterList.ToList() : null;

        //        /// Set data text and data value fields
        //        LanguageDropDown.DataTextField = "DiplayName";
        //        LanguageDropDown.DataValueField = "Id";
        //        LanguageDropDown.DataBind();
        //    }

        //    ListItem selectLanguage = new ListItem(SelectText, string.Empty);
        //    LanguageDropDown.Items.Insert(0, selectLanguage);
        //}

        /// <summary>
        /// GetCustomerDetailes
        /// </summary>
        /// <param name="countyId">country id</param>
        /// <param name="locationId"></param>
        /// <returns></returns>
        //private DataSet GetCustomerDetailes(int? countyId, int? locationId)
        //{
        //    try
        //    {

        //        return uCommerceConnectionFactory.GetCustomerDetailes(
        //            this.FirstName.Text,
        //            this.LastName.Text,
        //            this.AddressLine1.Text,
        //            this.PrimaryPhone.Text,
        //            countyId,
        //            locationId,
        //            GetClientIdListString());
        //    }
        //    catch (Exception ex)
        //    {
        //        //Log 
        //        Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
        //        throw;
        //    }

        //}

        private DataSet GetCustomerDetailes()
        {
            try
            {
                return uCommerceConnectionFactory.GetCustomerDetailes(CreateCustomerObj(), GetClientIdListString());
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }

        }


        /// <summary>
        /// Convert string list to comma separated string
        /// Prepare string list for SQL IN query
        /// </summary>
        /// <returns>comma separated string id list</returns>
        private string GetClientIdListString()
        {
            var clientList = GetClientList();
            string clientIdList = "{0}";//Dummy

            if (clientList != null)
            {
                //Convert string list to comma separated string as "{6C464DB8},{6451DB8}"
                clientIdList = string.Join(",", clientList);
            }

            return clientIdList;
        }

        /// <summary>
        /// Get Customer Details By Id
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <returns></returns>
        private DataSet GetCustomerDetailesById(int customerId)
        {
            try
            {
                return uCommerceConnectionFactory.GetCustomerDetailesById(customerId);

            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }

        }

        /// <summary>
        /// Write customer records to db
        /// </summary>
        private void RecordCustomer(bool isAnUpdate = false)
        {
            try
            {
                int result;
                bool success = false;
                string message = string.Empty;

                Customer customer = CreateCustomerObj();

                if (isAnUpdate)
                {
                    result = uCommerceConnectionFactory.UpdateCustomer(customer);
                }
                else
                {
                    result = uCommerceConnectionFactory.AddCustomer(customer);
                }

                if (result > 0)
                {
                    success = SubscriptionMessageHandling(ref message);

                    //Ignore subscription response
                    success = true;
                    //Alter massage if email subscription failed
                    //if (!success && message.Equals(Constants.EmailExist))
                    //{
                    //    message = string.Concat("Customer Data Saved, Subscription Failed - ", message);
                    //}
                    //ResetRearchForm();
                }
                if (success)
                {
                    successmessage.Visible = true;
                    errormessage.Visible = false;
                    // ResetEditForm();
                }
                else
                {
                    ErrorMessageLiteral.Text = message;

                    if (string.IsNullOrEmpty(message))
                    {
                        ErrorMessageLiteral.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Failed", "Operation Failed");
                    }

                    errormessage.Visible = true;
                    successmessage.Visible = false;
                }

            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Subscription Message Handling
        /// </summary>
        /// <param name="message">Message</param>
        /// <returns>True if success</returns>
        private bool SubscriptionMessageHandling(ref string message)
        {
            bool success = false;
            string email = Email.Text;
            string client = ClientDropDown.SelectedValue;

            //Subscription 
            if (!string.IsNullOrEmpty(email))
            {
                if (SubscribeCheckBox.Checked)
                {
                    if (this.Subscribe(client, email.Trim(), ref message))
                    {
                        success = true;
                    }
                    else
                    {
                        success = false;
                    }

                }
                else
                {
                    if (this.Unsubscribe(client, email.Trim(), ref message))
                    {
                        success = true;
                    }
                    else
                    {
                        //Ignore unsubscribe error
                        success = true;
                    }
                }
            }

            return success;
        }

        /// <summary>
        /// Create Customer Obj
        /// </summary>
        /// <returns>Customer</returns>
        private Customer CreateCustomerObj()
        {
            try
            {
                string firstName = FirstName.Text;
                string lastName = LastName.Text;

                int countryId;
                int locationId;

                //id column 
                ResultGridView.Columns[0].Visible = true;

                Customer customer = new Customer();
                customer.CustomerId = string.IsNullOrEmpty(CustomerId.Value) ? 0 : int.Parse(CustomerId.Value);
                customer.Title = TitleDropDown.SelectedValue;
                customer.FirstName = firstName.Length > 1 ? firstName.First().ToString().ToUpper().Trim() + firstName.Substring(1) : firstName.ToUpper().Trim();
                customer.LastName = lastName.Length > 1 ? lastName.First().ToString().ToUpper().Trim() + lastName.Substring(1) : lastName.ToUpper().Trim();
                customer.AddressLine1 = string.IsNullOrEmpty(AddressLine1.Text) ? null : AddressLine1.Text.Trim();
                customer.AddressLine2 = string.IsNullOrEmpty(AddressLine2.Text) ? null : AddressLine2.Text.Trim();
                customer.AddressLine3 = string.IsNullOrEmpty(AddressLine3.Text) ? null : AddressLine3.Text.Trim();
                customer.Phone = string.IsNullOrEmpty(SecondaryPhone.Text) ? null : SecondaryPhone.Text.Trim();
                customer.MobilePhone = string.IsNullOrEmpty(PrimaryPhone.Text) ? null : PrimaryPhone.Text.Trim();
                customer.Language = LanguageDropDown.SelectedValue;
                customer.EmailAddress = string.IsNullOrEmpty(Email.Text) ? null : Email.Text.Trim();
                customer.ClientId = ClientDropDown.SelectedValue;
                customer.BranchId = StoreDropDown.SelectedValue;
                customer.Active = ActiveCheckBox.Checked ? 1 : 0;
                customer.BookingReferenceId = string.Empty;
                customer.Subscribe = SubscribeCheckBox.Checked ? 1 : 0;

                if (int.TryParse(CountryDropDown.SelectedValue, out countryId))
                {
                    customer.CountryId = countryId;
                }

                if (int.TryParse(LocationDropDown.SelectedValue, out locationId))
                {
                    customer.LocationId = locationId.ToString();
                }

                return customer;
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Reset Search Form
        /// </summary>
        //private void ResetRearchForm()
        //{
        //    this.SearchFirstName.Text = string.Empty;
        //    this.SearchLastName.Text = string.Empty;
        //    this.SearchAddressLine1.Text = string.Empty;
        //    this.SearchPrimaryPhone.Text = string.Empty;
        //    this.LoadSearchLocationDropDown();
        //    this.LoadSearchCountryDropDown();
        //    this.divResult.Visible = false;
        //}

        /// <summary>
        /// Reset Edit form
        /// </summary>
        private void ResetEditForm(bool clearMessages = false)
        {
            FirstName.Text = string.Empty;
            LastName.Text = string.Empty;
            AddressLine1.Text = string.Empty;
            AddressLine2.Text = string.Empty;
            AddressLine3.Text = string.Empty;
            PrimaryPhone.Text = string.Empty;
            SecondaryPhone.Text = string.Empty;
            LanguageDropDown.SelectedIndex = -1;
            Email.Text = string.Empty;
            Email.ReadOnly = false;
            AddAndSave.Enabled = true;
            Search.Enabled = true;
            CountryDropDown.ClearSelection();
            LocationDropDown.ClearSelection();
            TitleDropDown.ClearSelection();
            ClientDropDown.ClearSelection();
            this.LoadCountryDropDown();
            this.LoadLocationDropDown();
            this.LoadTitles();
            this.LoadClientDropDown();
            this.StoreDropDown.ClearSelection();
            this.StoreDropDown.Enabled = false;
            SubscribeCheckBox.Checked = false;
            SubscribeCheckBox.Enabled = false;

            EditAndSave.Enabled = false;
            AddAndSave.Enabled = true;

            if (clearMessages)
            {
                successmessage.Visible = false;
                errormessage.Visible = false;
                ResultDiv.Visible = false;
            }

        }

        private void ResetSubscribeForm(bool expanSubscribeForm)
        {

            this.LoadClientDropDownSubscription();
            this.EmailSubscribe.Text = string.Empty;
            this.SubscribeMessage.Text = string.Empty;

            if (expanSubscribeForm)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "CallExpanSubscribForm", "ExpanSubscribForm()", true);
            }
        }

        #endregion

        protected void btnPaymentGateway_Click(object sender, EventArgs e)
        {
            string productName = BasketHelper.GetInvalidReservation();
            Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "REALEX: invalid offline product: " + productName + "");
            if (string.IsNullOrEmpty(productName))
            {
                UpdateOrderLineStatus();
                string redirectUrl = Request.Url.ToString().Replace("http:", "https:");
                redirectUrl = redirectUrl.Replace(Request.RawUrl, "/paymentgateway");
                Response.Redirect(redirectUrl, false);
            }
            else
            {
                customerSearchWrapper.Visible = false;
                PanelPayment.Visible = false;
                pnlInvalidProduct.Visible = true;
                return;
            }
        }

        private void UpdateOrderLineStatus()
        {
            int OrderId = BasketHelper.GetBasketOrderId();
            uCommerceConnectionFactory.UpdatePurchaseOrder(OrderId, customerIdd);
            AddPersonalDetailsToOrder();
        }

        private void AddPersonalDetailsToOrder()
        {
            PersonalDetails details = new PersonalDetails();
            details.FistName = FirstName.Text;
            details.LastName = LastName.Text;
            details.Address1 = AddressLine1.Text.Trim();
            details.Address2 = AddressLine2.Text.Trim();
            details.Address3 = AddressLine3.Text.Trim();
            details.CountyRegion = LocationDropDown.SelectedValue;
            details.Country = CountryDropDown.SelectedItem.Text;
            details.MobilePhone = SecondaryPhone.Text;
            details.Phone = PrimaryPhone.Text;
            details.Email = Email.Text;
            details.Store = StoreDropDown.SelectedValue;
            details.SendEmail = SubscribeCheckBox.Checked;
            details.TermsAndConditions = true;
            details.OrderId = BasketHelper.GetBasketOrderId();
            BasketHelper.AddPersonalDetails(details);
            BasketHelper.SetBookingReferenceBooking(customerIdd);
        }

        /// <summary>
        /// Load Language Drop Down
        /// </summary>
        private void FetchLanguageDropDown()
        {
            try
            {
                Item itemName = Sitecore.Context.Item;
                if (itemName != null)
                {
                    var langNames = new ListItemCollection();
                    var languages = ItemManager.GetContentLanguages(itemName);
                    if (languages != null)
                    {
                        foreach (var language in languages)
                        {
                            ListItem listItems = new ListItem();
                            listItems.Text = language.Title;
                            listItems.Value = language.Name;
                            langNames.Add(listItems);
                            LanguageDropDown.Visible = true;
                        }
                        BindDropdown(LanguageDropDown, langNames, "Text", "Value");
                        LanguageDropDown.Items.Insert(0, new ListItem { Text = "Please Select", Value = "0" });
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Bind DropDownList
        /// </summary>

        public void BindDropdown(DropDownList ddlControl, object dataSource, string text, string value)
        {
            try
            {
                ddlControl.DataSource = dataSource;
                ddlControl.DataTextField = text;
                ddlControl.DataValueField = value;
                ddlControl.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void BindSitecoreText()
        {
            if (Sitecore.Context.Item != null)
            {
                this.EditAndSave.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "EditAndSave", "EditAndSave");
                this.Search.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Search", "Search");
                this.AddAndSave.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "AddAndSave", "AddAndSave");
                this.EditAndSave.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "EditAndSave", "EditAndSave");
                this.RequiredFieldValidator8.ErrorMessage = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Mandatory Field Message", RequiredFieldValidator8.ErrorMessage);
                this.FirstNameRequiredFieldValidatorEdit.ErrorMessage = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Mandatory Field Message", FirstNameRequiredFieldValidatorEdit.ErrorMessage);
                this.FirstNameRequiredFieldValidatorSearch.ErrorMessage = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Mandatory Field Message", FirstNameRequiredFieldValidatorSearch.ErrorMessage);
                this.LastNameRequiredFieldValidatorEdit.ErrorMessage = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Mandatory Field Message", LastNameRequiredFieldValidatorEdit.ErrorMessage);
                this.LastNameRequiredFieldValidatorSearch.ErrorMessage = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Mandatory Field Message", LastNameRequiredFieldValidatorSearch.ErrorMessage);
                this.AddressLine1RequiredFieldValidator3.ErrorMessage = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Mandatory Field Message", AddressLine1RequiredFieldValidator3.ErrorMessage);
                this.CountryRequiredFieldValidator.ErrorMessage = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Mandatory Field Message", CountryRequiredFieldValidator.ErrorMessage);
                this.LocationRequiredFieldValidator.ErrorMessage = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Mandatory Field Message", LocationRequiredFieldValidator.ErrorMessage);
                this.PrimaryPhoneRequiredFieldValidator.ErrorMessage = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Mandatory Field Message", PrimaryPhoneRequiredFieldValidator.ErrorMessage);
                this.RegularExpressionValidatorEmail.ErrorMessage = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Mandatory Field Message", RegularExpressionValidatorEmail.ErrorMessage);                              
                this.RegularExpressionValidatorSecondaryPhone.ErrorMessage = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "InvalidPhoneNumber", RegularExpressionValidatorSecondaryPhone.ErrorMessage);
                this.RegularExpressionValidatorPhone.ErrorMessage = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "InvalidPhoneNumber", RegularExpressionValidatorPhone.ErrorMessage);
                this.RequiredFieldValidatorClientSubscribe.ErrorMessage = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Mandatory Field Message", RequiredFieldValidatorClientSubscribe.ErrorMessage);
                this.RequiredFieldValidator10.ErrorMessage = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Mandatory Field Message", RequiredFieldValidator10.ErrorMessage);
                this.RegularExpressionValidatorEmailSubscription.ErrorMessage = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Mandatory Field Message", RegularExpressionValidatorEmailSubscription.ErrorMessage);                                         
            }
        }
    }
}
