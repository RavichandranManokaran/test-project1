﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           OfflinePortalFilterSearchSublayout.ascx.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal
/// Description:           SubLayout for offline portal Filter Search
/// </summary>

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    #region Using Directives

    using Affinion.LoyaltyBuild.Common;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    using System.Web.UI.WebControls;

    #endregion

    public partial class OfflinePortalFilterSearchSublayout : BaseSublayout
    {
        #region Protected Methods

        /// <summary>
        /// Executes on page pre load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            LoadTextFromSitecore();

            if (!IsPostBack)
            {
                FiltersDataBinding(Constants.StarRankingsPath, StarRankingRepeater);
                FiltersDataBinding(Constants.FacilitiesPath, FacilitiesRepeater);
                FiltersDataBinding(Constants.ThemesPath, ThemesRepeater);
                FiltersDataBinding(Constants.ExperiencesPath, ExperienceRepeater);

                UpdateControlerState("StarRanking", StarRankingRepeater, "starCheckBox");
                UpdateControlerState("Facilities", FacilitiesRepeater, "facilitiesCheckBox");
                UpdateControlerState("Themes", ThemesRepeater, "themesCheckBox");
                UpdateControlerState("Experiences", ExperienceRepeater, "experienceCheckBox");

                ShowHideFilter("StarRanking", StarRankingRepeater);
                ShowHideFilter("Facilities", FacilitiesRepeater);
                ShowHideFilter("Themes", ThemesRepeater);
                ShowHideFilter("Experience", ExperienceRepeater);

                this.filterButton.UseSubmitBehavior = false;
            }
        }

        /// <summary>
        /// Button click event for filter button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ButtonFilter_Click(object sender, EventArgs e)
        {            
            QueryStringHelper helper = new QueryStringHelper(Request.Url);

            ///Append the selected index value to query string and return            
            if (StarRankingRepeater.Visible)
            helper.SetValue("StarRanking", Server.UrlEncode(SetFilterParameter(StarRankingRepeater, "starCheckBox")));

            if (FacilitiesRepeater.Visible)
            helper.SetValue("Facilities", Server.UrlEncode(SetFilterParameter(FacilitiesRepeater, "facilitiesCheckBox")));

            if (ThemesRepeater.Visible)
            helper.SetValue("Themes", Server.UrlEncode(SetFilterParameter(ThemesRepeater, "themesCheckBox")));

            if (ExperienceRepeater.Visible)
            helper.SetValue("Experiences", Server.UrlEncode(SetFilterParameter(ExperienceRepeater, "experienceCheckBox")));

            helper.SetValue("PageId", "1");
            Response.Redirect(helper.GetUrl().ToString(), true);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// bind data value repeates
        /// </summary>
        /// <param name="path">sitecore folder path</param>
        /// <param name="filterRepeater">repeater Id</param>
        private void FiltersDataBinding(string path, Repeater filterRepeater)
        {
            try
            {
                var filterHomeItem = Sitecore.Context.Database.GetItem(path);
                List<Item> filterItemList = new List<Item>();

                if (filterHomeItem == null)
                    return;

                filterRepeater.DataSource = filterHomeItem.Children;
                filterRepeater.DataBind();
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Save checkbox state according to parameter of url 
        /// </summary>
        /// <param name="urlParameter"></param>
        /// <param name="repeaterName"></param>
        /// <param name="checkBoxId"></param>
        private void UpdateControlerState(string urlParameter, Repeater repeaterName, string checkBoxId)
        {
            if (!repeaterName.Visible)
                return;

            QueryStringHelper helper = new QueryStringHelper(Request.Url);
            string urlParameterValue = null;
            //urlParameterValue = helper.GetValue(urlParameter);

            urlParameterValue = Server.UrlDecode(helper.GetValue(urlParameter));

            if (!string.IsNullOrEmpty(urlParameterValue))
            {
                foreach (var value in urlParameterValue.Split(','))
                {
                    if (!string.IsNullOrEmpty(value))
                    {
                        CheckBox repeaterCheckBox = null;
                        foreach (RepeaterItem item in repeaterName.Items)
                        {
                            if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                            {
                                repeaterCheckBox = (CheckBox)item.FindControl(checkBoxId);
                                if (repeaterCheckBox.Text == value)
                                {
                                    repeaterCheckBox.Checked = true;
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Get checked values from common method
        /// </summary>
        /// <returns></returns>
        private string SetFilterParameter(Repeater filterRepeater, string filterCheckBoxId)
        {
            List<string> parameterList = new List<string>();
            string filterParameter = null;
            CheckBox filterCheckBox = null;
            foreach (RepeaterItem item in filterRepeater.Items)
            {
                if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                {
                    filterCheckBox = (CheckBox)item.FindControl(filterCheckBoxId);
                    if (filterCheckBox != null)
                    {
                        if (filterCheckBox.Checked)
                        {
                            parameterList.Add(filterCheckBox.Text);
                        }
                    }
                }
            }

            filterParameter = string.Join(",", parameterList.ToArray());
            return filterParameter;
        }

        private void ShowHideFilter(string filterName, System.Web.UI.Control control)
        {
            if (this.GetParameter(filterName) != "1")
            {
                control.Visible = false;
            }
        }

        #endregion

        /// <summary>
        /// LoadTextFromSitecore
        /// </summary>

        private void LoadTextFromSitecore()
        {
            filterButton.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "FilterButton", "");           
        }
    }
}