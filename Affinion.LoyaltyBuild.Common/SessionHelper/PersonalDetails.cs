﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           PersonalDetails.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Code is used to store the personal details
/// </summary>

namespace Affinion.LoyaltyBuild.Common.SessionHelper
{
    /// <summary>
    /// Class which includes personal details related properties
    /// </summary>
    public class PersonalDetails
    {
        public string FistName { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string CountyRegion { get; set; }
        public string Country { get; set; }
        public string MobilePhone { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Store { get; set; }
        public bool SendEmail { get; set; }
        public bool TermsAndConditions { get; set; }
        public int OrderId { get; set; }
    }
}
