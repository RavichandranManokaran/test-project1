﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    public class LBHotelDetail
    {
        public LBHotelDetail()
        {
            this.SKU = string.Empty;
            this.SiteCoreItemId = string.Empty;
            this.HotelPath = string.Empty;
            this.HotelName = string.Empty;
            this.HotelType = string.Empty;
            this.HotelRating = string.Empty;
            this.DisplayName = string.Empty;
            this.Region = string.Empty;
            this.Client = string.Empty;
            this.GeoCoOrdinate = string.Empty;
            this.HotelExperience = string.Empty;
            this.Latitude = string.Empty;
            this.Longitude = string.Empty;
            this.Theme = string.Empty;
            this.Groups = string.Empty;
            this.SuperGroups = string.Empty;
            this.HotelFacility = string.Empty;
        }

        public string SKU { get; set; }

        public bool WeekendSerchRestriction { get; set; }

        public string SiteCoreItemId { get; set; }

        public string HotelPath { get; set; }

        public string HotelName { get; set; }

        public string HotelType { get; set; }

        public string Location { get; set; }

        public string GeoCoOrdinate { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string HotelRating { get; set; }

        public string DisplayName { get; set; }

        public int UComId { get; set; }

        public List<int> CatalogId { get; set; }

        public string Country { get; set; }

        public string Region { get; set; }
        public string Client { get; set; }
        public string HotelExperience { get; set; }
        public string HotelFacility { get; set; }
        public string Theme { get; set; }
        public string Groups { get; set; }
        public string SuperGroups { get; set; }

        public void ConvertToLower()
        {
            this.SKU = this.SKU.ToLower();
            this.SiteCoreItemId = this.SiteCoreItemId.ToLower();
            this.HotelPath = this.HotelPath.ToLower();
            this.HotelName = this.HotelName.ToLower();
            this.HotelType = this.HotelType.ToLower();
            this.HotelRating = this.HotelRating.ToLower();
            this.DisplayName = this.DisplayName.ToLower();
            this.Region = this.Region.ToLower();
            this.Client = this.Client.ToLower();
            this.Location = this.Location.ToLower();
            this.Country = this.Country.ToLower();
            this.HotelExperience = this.HotelExperience.ToLower();
            this.HotelFacility = this.HotelFacility.ToLower();
            this.Theme = this.Theme.ToLower();
            this.Groups = this.Groups.ToLower();
            this.SuperGroups = this.SuperGroups.ToLower();
            this.GeoCoOrdinate = this.GeoCoOrdinate.ToLower();
            this.Latitude = this.Latitude.ToLower();
            this.Longitude = this.Longitude.ToLower();
        }
    }

    public class CountryDetail
    {
        public string CountryName { get; set; }

        public string CultureCode { get; set; }
    }

    public class LBAssignSupplier
    {
        public LBAssignSupplier()
        {
            this.AddedHotel = new List<int>();
            this.RemoveHotel = new List<int>();
        }

        public int CatalogId { get; set; }

        public string ClientName { get; set; }

        public List<int> AddedHotel { get; set; }

        public List<int> RemoveHotel { get; set; }
    }

    public class LBProductInfo
    {
        public string SKU { get; set; }

        public string VSKU { get; set; }

        public int ProductId { get; set; }
    }

    public class LBProductDetail
    {
        public LBProductDetail()
        {
            Name = string.Empty;

            RoomType = string.Empty;

            FoodType = string.Empty;

            PackageType = string.Empty;

            Description = string.Empty;

            ParentSKU = string.Empty;

            RelatedSku = string.Empty;

            RelatedAddOnSku = string.Empty;

            SKU = string.Empty;


        }

        public string Name { get; set; }

        public string RoomType { get; set; }

        public string FoodType { get; set; }

        public string PackageType { get; set; }

        public string Description { get; set; }

        public string ParentSKU { get; set; }

        public string RelatedSku { get; set; }

        public string RelatedAddOnSku { get; set; }

        public bool IsUnlimited { get; set; }

        public bool IsSellableStaandalone { get; set; }

        public int Ranking { get; set; }

        public int MaxNumberOfAdults { get; set; }

        public int MaxNumberOfChildren { get; set; }

        public int MaxNumberOfPeople { get; set; }

        public int MaxNumberOfInfrant { get; set; }

        public string SKU { get; set; }

        public int UcommerceId { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsActive { get; set; }

        public void ConvertToLower()
        {
            this.Name = this.Name.ToLower();
            this.RoomType = this.RoomType.ToLower();
            this.FoodType = this.FoodType.ToLower();
            this.PackageType = this.PackageType.ToLower();
            this.Description = this.Description.ToLower();
            this.ParentSKU = this.ParentSKU.ToLower();
            this.RelatedSku = this.RelatedSku.ToLower();
            this.RelatedAddOnSku = this.RelatedAddOnSku.ToLower();
            this.SKU = this.SKU.ToLower();
            
        }
    }
}
