﻿CREATE TABLE [dbo].[uCommerce_CampaignItemProperty] (
    [CampaignItemPropertyId] INT            IDENTITY (1, 1) NOT NULL,
    [Value]                  NVARCHAR (MAX) NOT NULL,
    [DefinitionFieldId]      INT            NOT NULL,
    [CultureCode]            NVARCHAR (60)  NULL,
    [CampaignItemId]         INT            NOT NULL,
    CONSTRAINT [PK_uCommerce_CampaignItemProperty] PRIMARY KEY CLUSTERED ([CampaignItemPropertyId] ASC),
    CONSTRAINT [FK_uCommerce_CampaignItemProperty_uCommerce_CampaignItem] FOREIGN KEY ([CampaignItemId]) REFERENCES [dbo].[uCommerce_CampaignItem] ([CampaignItemId]),
    CONSTRAINT [FK_uCommerce_CampaignItemProperty_uCommerce_DefinitionField] FOREIGN KEY ([DefinitionFieldId]) REFERENCES [dbo].[uCommerce_DefinitionField] ([DefinitionFieldId])
);

