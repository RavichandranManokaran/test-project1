﻿namespace Affinion.LoyaltyBuild.AdminPortal.DataExtraction
{
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using Sitecore.Security.Accounts;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for importing external data to sitecore
    /// </summary>
    public interface ISitecoreExternalDataProvider
    {
        /// <summary>
        /// Gets the database which data needs to be import
        /// </summary>
        /// <returns></returns>
        Database GetDatabase();

        /// <summary>
        /// Get the sitecore template to import data as sitecore items
        /// </summary>
        /// <returns></returns>
        TemplateItem GetTemplate();

        /// <summary>
        /// Get the root folder item to decide where to add imported data
        /// </summary>
        /// <returns></returns>
        Item GetRootFolder();

        /// <summary>
        /// Gives the external party to decide on item name
        /// </summary>
        /// <returns></returns>
        string GetContextItemName();

        /// <summary>
        /// Presents the external data as a collection of SitecoreFieldData
        /// </summary>
        /// <returns></returns>
        IEnumerable<SitecoreFieldData> GetData();

        /// <summary>
        /// Gets the external content version to decide weather the content is already being imported
        /// </summary>
        /// <returns></returns>
        string GetExternalVersion();

        /// <summary>
        /// Get the external Version field name
        /// </summary>
        /// <returns></returns>
        string GetExternalVersionFieldName();
    }
}
