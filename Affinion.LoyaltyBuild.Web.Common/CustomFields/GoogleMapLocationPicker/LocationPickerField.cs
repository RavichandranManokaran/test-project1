﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           LocationPickerField.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.Common
/// Description:           Google map location picker custom field implementation
/// </summary>

using Sitecore;
using Sitecore.Shell.Applications.ContentEditor;
using Sitecore.Web.UI.Sheer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Affinion.LoyaltyBuild.Web.Common.CustomFields.GoogleMapLocationPicker
{
    public class LocationPickerField : Text, IContentField
    {
        public string GetValue()
        {
            return Value;
        }

        public void SetValue(string value)
        {
            Value = value;
        }

        /// <summary>
        /// Handles click event of the hyperlinks on top of the custom field
        /// </summary>
        /// <param name="message"></param>
        public override void HandleMessage(Message message)
        {
            base.HandleMessage(message);

            if (message["id"] != ID || string.IsNullOrWhiteSpace(message.Name)) return;

            switch (message.Name)
            {
                case "customGmap:SetLocation": //It defined in core database
                    Sitecore.Context.ClientPage.Start(this, "SetLocation");
                    return;
                case "customGmap:ClearLocation": //it also defined in core database
                    Sitecore.Context.ClientPage.Start(this, "ClearLocation");
                    return;
            }

            if (Value.Length > 0) SetModified();

            Value = string.Empty;
        }

        protected void SetLocation(ClientPipelineArgs args)
        {
            //Check if popup windows is postback
            if (args.IsPostBack)
            {
                //check whether popup windows has selected value
                if (args.HasResult && Value.Equals(args.Result) == false)
                {
                    //tell content editor that value in field is modified
                    SetModified();

                    //set current field value with selected value from popup window
                    SetValue(args.Result);
                }
            }
            else
            {
                //show popup
                //get popup control that named GmapLocationPickerDialog
                var url = UIUtil.GetUri("control:GmapLocationPickerDialog");

                //Try to get Current Value (if it previously has a value)
                var value = GetValue();
                if (!string.IsNullOrEmpty(value))
                {
                    //passing current value to querystring so that it could read by our popup window
                    url = string.Format("{0}&value={1}",
                        url, value);
                }

                //Show our popup dialog
                SheerResponse.ShowModalDialog(url, "800", "600", string.Empty, true);

                //Wait popup dialog for postback
                args.WaitForPostBack();
            }
        }

        protected void ClearLocation(ClientPipelineArgs args)
        {
            //set empty value
            SetValue(string.Empty);

            //tell content editor that value in field is modified
            SetModified();
        }
    }
}