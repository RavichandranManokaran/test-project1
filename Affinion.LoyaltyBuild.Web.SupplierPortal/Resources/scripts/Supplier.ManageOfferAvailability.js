﻿
function disableButton() {
    var roleBasedDisableButtons = $(".roleBasedDisableButtons").val();
    if (roleBasedDisableButtons == "true") {
        $("div.cal-table-top-right button").each(function () {
            $(this).attr('disabled', true);
        });
        $("div.tit_pric button.view_btn").attr('disabled', false);
        $("div.tit_pric button#btnbackavilability").attr('disabled', false);
        $("div.tabale-top input[type=text]").attr('disabled', true);
        $("div.offers ul li a.edit i").css("display", "none");
    }
}


Supplier.ManageOfferAvailability = new function () {
    var showallactiveoffer = false;
    var showallinactiveoffer = false;
    var changeOfferStatus = true;
    var hashtable = {};
    var jsonObj = [];
    var limitClass = "limits";
    var closeOutClass = "closeOut";
    var unAllocateClass = "unallocate";
    this.ChangeAvailability = function (ele, displayalert) {
        var oldvalue;
        var newvalue;
        var month = $(ele).attr("month");
        if (displayalert) {
            //$("#btn_" + month).removeAttr("disabled");
            $("#btn_0").removeAttr("disabled");
            $("#btn_1").removeAttr("disabled");
        }
        var weekendClass = $(ele).attr("weekend") == "True" ? "weekend" : "";
        if ($(ele).attr("maxsellableroom") != null && $(ele).attr("minsellableroom") != null) {
            oldvalue = $(ele).attr("offerallocatedRoom");
            newvalue = $(ele).val();
            if ($(ele).val() == '') {
                $(ele).val($(ele).attr("offerallocatedRoom"));
                $("#dvShowEmptyMsg").modal();
            }
            if (displayalert && parseInt($(ele).val()) > parseInt($(ele).attr("maxsellableroom"))) {
                $(ele).val(parseInt($(ele).attr("maxsellableroom")));
                $("#offerallocated").hide();
                $("#roommsg").show();
                $("#spofferallocated").hide();
                $("#sproommsg").show();
                $("#dvmoreallocated").modal();
            }
            else if (displayalert && parseInt($(ele).val()) < parseInt($(ele).attr("minsellableroom"))) {
                $(ele).val(parseInt($(ele).attr("minsellableroom")));
                $("#dvLessRoomAllocationMsg").modal();

            }
            if ((parseInt($(ele).val()) == 0 && parseInt($(ele).attr("offerallocatedRoom")) == 0) ||
                parseInt(oldvalue) == parseInt($(ele).val())
                ) {
                if (hashtable.hasOwnProperty($(ele).attr("datecounter"))) {
                    delete hashtable[$(ele).attr("datecounter")];
                    $(ele).removeClass("limits" + weekendClass);
                    $(ele).addClass($(ele).attr("defaultclass"));
                    $(ele).removeClass("changestyle");
                }
                return;
            }
            else {
                if (!$(ele).hasClass("closeOut" + weekendClass)) {
                    if ($(ele).hasClass("unallocate" + weekendClass)) {
                        $(ele).removeClass("unallocate" + weekendClass)
                    }
                    $(ele).addClass("limits" + weekendClass);
                }
                $(ele).addClass("changestyle");
            }
            //Supplier.ManageOfferAvailability.ModifiedHashTable(ele);
            Supplier.ManageOfferAvailability.SaveClosedDataInHashTable(ele);

        }
    }

    this.UpdateChanges = function (month) {
        jsonObj = [];
        for (var k in hashtable) {

            // use hasOwnProperty to filter out keys from the Object.prototype
            if (month == 0 || parseInt(hashtable[k][5]) == parseInt(month)) {
                var item = {};
                item["CombineSKU"] = hashtable[k][2];
                item["DateCounter"] = hashtable[k][3];
                item["AvailableRoom"] = hashtable[k][1];
                item["CampaignItemId"] = hashtable[k][4];
                jsonObj.push(item);
            }
        }
        if (jsonObj != null && jsonObj.length > 0)
            Supplier.ManageOfferAvailability.SaveUpdateChanges(month);
    }

    //Hash table method
    this.SaveClosedDataInHashTable = function (ele) {
        var room = $(ele).attr('productsku');
        var oldavailbleroom = $(ele).attr('offerallocatedRoom');
        var availbleroom = $(ele).val();
        var dateCounter = $(ele).attr("datecounter");
        var hashId = $(ele).attr('hashid');;
        hashtable[hashId] = [parseInt(oldavailbleroom), parseInt(availbleroom), room, parseInt(dateCounter), parseInt($(ele).attr("offerid")), parseInt($(ele).attr("month"))];
        if (parseInt(oldavailbleroom) == parseInt(availbleroom)) {
            delete hashtable[hashId];
        }
    }
    this.ClearChanges = function (month) {
        Supplier.ManageOfferAvailability.ValidationBoxForClearChanges(month);

    }
    //Json Call
    this.SaveUpdateChanges = function (month) {
        var url = "/handler/Offer/OfferRoomAvailibilityData.ashx";
        $.ajax({
            url: url,
            type: "POST",
            data: JSON.stringify(jsonObj),
            contentType: "application/json",
            success: function (data) {
                if (data == "SUCCESS");
                {
                    for (var k in hashtable) {
                        // use hasOwnProperty to filter out keys from the Object.prototype
                        if (month == 0 || parseInt(hashtable[k][5]) == parseInt(month)) {
                            delete hashtable[k];
                        }
                    }
                    if (month == 0) {
                        Supplier.ManageOfferAvailability.ViewByDateRange(0);
                    }
                    else {
                        Supplier.ManageOfferAvailability.LoadRoomAvailibilityData(month);
                    }
                    $("#messageModal").modal();
                    disableButton();
                }
            },
            error: function (data, status, jqXHR) {
            }
        });
    }
    this.LoadRoomAvailibilityData = function (mon) {
        Supplier.ManageOfferAvailability.ViewByDateRange(mon);
    }
    this.ViewByDateRange = function (mon) {
        if (mon == 0) {
            for (var k in hashtable) {
                Supplier.ManageOfferAvailability.ValidationBoxForClearChanges(0);
                return;
            }
        }
        if (changeOfferStatus) {
            showallinactiveoffer = false;
            showallactiveoffer = false;
        }
        else {
            changeOfferStatus = true;
        }
        Supplier.ManageOfferAvailability.ViewOfferByDateRange(0);
        var sDate = $('#datepickerFrom').val();
        var eDate = $('#datepickerTo').val();
        var url = "/handler/Offer/OfferRoomAvailibilityByDateRange.ashx";
        myApp.showPleaseWait();
        $.ajax({
            url: url,
            type: "GET",
            data: { "startDate": sDate, "endDate": eDate, "month": mon, "OfferId": $("#hdOfferId").val() },
            success: function (data) {
                myApp.hidePleaseWait();
                for (var k in hashtable) {
                    // use hasOwnProperty to filter out keys from the Object.prototype
                    if (mon == 0 || parseInt(hashtable[k][5]) == parseInt(mon)) {
                        delete hashtable[k];
                    }
                }
                var dataLength = $("<div>").append(data).find("input").filter("[datecounter]").length;
                if (dataLength > 0) {
                    $(".btnpanel").show();
                }
                else {
                    $(".btnpanel").hide();
                }
                //alert($("<div>").append(data).find("input").filter("[datecounter]").length);
                //alert($("<div>").append(data).find("input").length);
                //alert($("<div>").append(data).find("input").filter("[datecounter]").length);
                if (mon == 0) {
                    $('#dvContent').html('');
                    $('#dvContent').html(data);
                    Supplier.ManageOfferAvailability.BindOnlyNumber();
                }
                else {
                    $("#dv_" + mon).html('');
                    $("#dv_" + mon).html(data);
                    Supplier.ManageOfferAvailability.BindOnlyNumber();
                }
                var offerName = '';
                if ($("#cmp_" + $("#hdOfferId").val()).length > 0) {
                    offerName = $("#cmp_" + $("#hdOfferId").val()).attr("offername");
                }
                $("#offername").html(offerName);
                disableButton();
            },
            error: function (data, status, jqXHR) {
                myApp.hidePleaseWait();
                $(".btnpanel").hide();

            }
        });
    }

    this.ViewOfferByDateRange = function (mon, displaymessage) {

        var sDate = $('#datepickerFrom').val();
        var eDate = $('#datepickerTo').val();
        var url = "/handler/OffersParticipating.ashx";
        myApp.showPleaseWait();
        $.ajax({
            url: url,
            type: "GET",
            cache: false,
            data: { "startDate": sDate, "endDate": eDate, "month": mon, "OfferId": $("#hdOfferId").val() },
            success: function (data) {
                myApp.hidePleaseWait();
                $('#dvOfferContent').html('');
                $('#dvOfferContent').html(data);
                $("div.tooltip").remove();
                $("#tooltipcontainer a[title]").tooltips();
                $("#offerspart").on("hide.bs.collapse", function () {
                    $(".offerpart").html('<span class="glyphicon glyphicon-plus pull-right"></span>');
                });
                $("#offerspart").on("show.bs.collapse", function () {
                    $(".offerpart").html('<span class="glyphicon glyphicon-minus pull-right"></span>');
                });

                $("#offernotpart").on("hide.bs.collapse", function () {
                    $(".offernotpart").html('<span class="glyphicon glyphicon-plus pull-right"></span>');
                });
                $("#offernotpart").on("show.bs.collapse", function () {
                    $(".offernotpart").html('<span class="glyphicon glyphicon-minus pull-right"></span>');
                });

                if (showallinactiveoffer) {
                    Supplier.ManageOfferAvailability.ShowInActiveOfferTrue();
                }
                if (showallactiveoffer) {
                    Supplier.ManageOfferAvailability.ShowActiveOfferTrue();
                }
                if (typeof displaymessage !== "undefined" && displaymessage) {
                    $("#roommsg").hide();
                    $("#offerallocated").show();
                    $("#sproommsg").hide();
                    $("#spofferallocated").show();
                    $("#dvmoreallocated").modal();
                }
            },
            error: function (data, status, jqXHR) {
                myApp.hidePleaseWait();
            }
        });
    }

    this.SubscribeForNewOffer = function (offerId, supplierInviteId) {
        $("#subscribeheading").html('Subscription of promotion');
        $("#btnsubscribecontent").html('Subscribe for Promotion');
        $('#subscribeOfferId').val('0');
        $('#hdcontenttype').val('0');
        $('#subscribeSupplierInviteId').val('0');

        $('#subscribeOfferId').val(offerId);
        $('#subscribeSupplierInviteId').val(supplierInviteId);

        // $("#subscribeOfferContent").html(data);
        $("#dvSubscribePromotion").modal();
        var url = "/handler/Offer/SubscribeForNewOffer.ashx";
        $.ajax({
            url: url,
            data: { "offerId": offerId, "supplierInviteId": supplierInviteId },
            success: function (data) {
                if (data != "");
                {
                    CKEDITOR.instances['txtContent'].setData(data);
                    /* $('#subscribeOfferId').val(offerId);
                     $('#subscribeSupplierInviteId').val(supplierInviteId);*/


                }

            },
            error: function (data, status, jqXHR) {
            }
        });
    }

    this.EditOfferContent = function (offerId, supplierInviteId) {
        $("#subscribeheading").html('Update Offer information');
        $("#btnsubscribecontent").html('Save Offer Information');
        $('#subscribeOfferId').val('0');
        $('#subscribeSupplierInviteId').val('0');
        $('#hdcontenttype').val('1');
        $('#subscribeOfferId').val(offerId);
        $('#subscribeSupplierInviteId').val(supplierInviteId);

        // $("#subscribeOfferContent").html(data);
        $("#dvSubscribePromotion").modal();
        var url = "/handler/Offer/SubscribeForNewOffer.ashx";
        $.ajax({
            url: url,
            data: { "offerId": offerId, "supplierInviteId": supplierInviteId },
            success: function (data) {
                if (data != "");
                {
                    CKEDITOR.instances['txtContent'].setData(data);
                }
            },
            error: function (data, status, jqXHR) {
            }
        });
    }

    this.ChangeCampaignSubscribe = function () {
        var supplierId = $('#subscribeSupplierInviteId').val();
        var isSubscribe = true;
        var url = "/handler/ChangeCampaignSubscribe.ashx";
        myApp.showPleaseWait();
        $.ajax({
            url: url,
            type: "POST",
            data: { "supplierId": supplierId, "isSubscribe": isSubscribe, "OfferDetail": CKEDITOR.instances['txtContent'].getData() },
            success: function (data) {
                myApp.hidePleaseWait();
                $("#btncancelsubscribe").click();
                if ($('#hdcontenttype').val() == "0") {
                    $('#dvoffersubscribe').modal();
                    Supplier.ManageOfferAvailability.ViewOfferByDateRange(0, isSubscribe);
                    //Supplier.ManageAvailability.ViewOfferByDateRange(0);
                }
                else {
                    $('#dvOfferContentalert').modal();
                }
            },
            error: function (data, status, jqXHR) {
                myApp.hidePleaseWait();
                $("#btncancelsubscribe").click();
            }
        });
    }
    
    this.alertmessage = function () {
        var hasdata = false;
        for (var k in hashtable) {
            hasdata = true;
        }
        return hasdata;
    }

    this.ValidationBoxForClearChanges = function (mon) {
        $("#btnNoModalPopup").unbind("click");
        $("#btnUpdateModalPopup").unbind("click");
        var hasdata = false;
        for (var k in hashtable) {
            if (parseInt(mon) == 0 || parseInt(hashtable[k][5]) == parseInt(mon)) {
                hasdata = true;
                break;
            }
        }
        if (hasdata) {
            $("#btnUpdateModalPopup").click(function () { Supplier.ManageOfferAvailability.UpdateChanges(mon); });
            $("#btnNoModalPopup").click(function () {
                if (mon == 0) {
                    hashtable = {};
                }
                Supplier.ManageOfferAvailability.ViewByDateRange(mon);
            });
            $("#myModal").modal();
        }

    }

    this.SaveAllChanges = function () {

        Supplier.ManageOfferAvailability.UpdateChanges(0);
    }

    this.ShowHideOffer = function (obj, classname, spanname) {


        if ($(obj).attr('isshow') == 'true') {
            $(obj).attr('isshow', 'false');
            $('.' + classname).css('display', 'none');
            $(obj).text('');
            $(obj).html("<span  class=\"glyphicon glyphicon-circle-arrow-down\"></span> Show More ");
            if (classname == "inactiveofferli") {
                showallinactiveoffer = false;
            }
            else {
                showallactiveoffer = false;
            }
        }
        else {
            $(obj).attr('isshow', 'true');
            $('.' + classname).css('display', 'block');
            $(obj).text('');
            $(obj).html("<span class=\"glyphicon glyphicon-circle-arrow-up\"></span> Hide More ");
            if (classname == "inactiveofferli") {
                showallinactiveoffer = true
            }
            else {
                showallactiveoffer = true;
            }

        }
    }

    this.ShowActiveOfferTrue = function () {

        $('#btnactiveoffer').attr('isshow', 'true');
        $('.activeofferli').css('display', 'block');
        $('#btnactiveoffer').text('');
        $('#btnactiveoffer').html("<span  class=\"glyphicon glyphicon-circle-arrow-up\"></span> Hide More ");
    }

    this.ShowInActiveOfferTrue = function () {

        $('#btninactiveoffer').attr('isshow', 'true');
        $('.inactiveofferli').css('display', 'block');
        $('#btninactiveoffer').text('');
        $('#btninactiveoffer').html("<span  class=\"glyphicon glyphicon-circle-arrow-up\"></span> Hide More ");
    }
    this.printDiv = function (id) {

        var html = "";

        $('link').each(function () { // find all <link tags that have
            if ($(this).attr('rel').indexOf('stylesheet') != -1) { // rel="stylesheet"
                if (!($(this).attr("href").indexOf("simple-sidebar") > -1))
                    html += '<link rel="stylesheet" href="' + $(this).attr("href") + '" />';
            }
        });
        var sDate = $('#datepickerFrom').val();
        var eDate = $('#datepickerTo').val();
        var d = new Date(),
		minutes = d.getMinutes().toString().length == 1 ? '0' + d.getMinutes() : d.getMinutes(),
		hours = d.getHours().toString().length == 1 ? '0' + d.getHours() : d.getHours(),
		ampm = d.getHours() >= 12 ? ' pm' : ' am',
	 months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        var displaystring = "Printed On : " + days[d.getDay()] + ' ' + months[d.getMonth()] + ' ' + d.getDate() + ' ' + d.getFullYear() + ' ' + hours + ':' + minutes + ampm;
        var header = ' <div id="header" style=”background-color:White;text-align:center;”>' + "Offer Availability Report : from " + sDate + ' ' + hours + ':' + minutes + ampm + " To " + eDate + ' ' + hours + ':' + minutes + ampm + '</div><br>';
        var footer = '<br><br><div id="footer" style="background-color:White;text-align:center;">' + displaystring + '</div>';
        html += '<body onload="window.focus(); window.print()" >' + header + $("#" + id).html() + footer + '</body>';
        var w = window.open("", "print");
        if (w) { w.document.write(html); w.document.close() }

    };
    this.BindOnlyNumber = function () {
        $('.offerinput input[type="text"]').keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .

            var code = e.keyCode || e.which;
            // if (code != 8 && code != 0 && (code < 48 || code > 57)) {
            // return false;
            // }
            if (code == 13) { e.preventDefault(); }
            if ($.inArray(code, [46, 8, 9, 27, 13]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                (code == 65 && (e.ctrlKey === true || e.metaKey === true))
                // Allow: home, end, left, right, down, up
                 ) {
                // let it happen, don't do anything
                return;
            }

            //// Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (code < 48 || code > 57)) && (code < 96 || code > 105)) {
                e.preventDefault();
            }
        }).on('drop', function (event) {
            event.preventDefault();
        }).on('keyup', function (event) {
            var month = parseInt($(this).attr("month"));

            if (parseInt($(this).val()) != parseInt($(this).attr("offerallocatedRoom"))) {
                //$("#btn_" + month).attr("disabled", "disabled");
                $("#btn_0").attr("disabled", "disabled");
                $("#btn_1").attr("disabled", "disabled");
            }
            else {
                //$("#btn_" + month).removeAttr("disabled");
                $("#btn_0").removeAttr("disabled");
                $("#btn_1").removeAttr("disabled");
            }
            if ($(this).val() != $(this).val().replace(/[^0-9]/g, '')) {
                $(this).val($(this).val().replace(/[^0-9]/g, ''));
            }
            if (event.keyCode == 13) { event.preventDefault(); }
        });;
    };
    this.LoadCampaignData = function (campaignItemId) {
        $("#hdOfferId").val(campaignItemId);
        changeOfferStatus = false;
        Supplier.ManageOfferAvailability.ViewByDateRange(0);

    }

    this.RemoveOffer = function (offerId, supplierInviteId) {
        $('#removeOfferId').val('0');
        $('#removeSupplierInviteId').val('0');

        var url = "/handler/Offer/RemoveOffer.ashx";
        $.ajax({
            url: url,
            data: { "offerId": offerId, "supplierInviteId": supplierInviteId },
            success: function (data) {
                if (data != "");
                {
                    $('#removeOfferId').val(offerId);
                    $('#removeSupplierInviteId').val(supplierInviteId);
                    $("#removeOfferContent").html(data);
                    $("#removal-offer").modal();
                }

            },
            error: function (data, status, jqXHR) {
            }
        });

    }
    this.GetReport = function () {
        var url = "/handler/Offer/AnnualReportHandler.ashx";
        $.ajax({
            url: url,
            data: { "offerId": $("#hdOfferId").val() },
            success: function (data) {
                if (data != "");
                {
                    $('#dvanualcontainer').html('');
                    $("#dvanualcontainer").html(data);
                    $("#myanualModal").modal();
                }

            },
            error: function (data, status, jqXHR) {
            }
        });
    }

    this.ViewPrice = function (offerId) {
        var offerId = $("#hdOfferId").val();
        if (offerId != "0" && offerId != "") {
            var url = "/handler/Offer/ViewPrice.ashx";
            $.ajax({
                url: url,
                data: { "offerId": offerId },
                cache: false,
                success: function (data) {
                    if (data != "");
                    {
                        $("#dvviewpricecontent").html('');
                        $("#dvviewpricecontent").html(data);
                        $("#dvviewprice").modal();
                    }
                },
                error: function (data, status, jqXHR) {
                }
            });
        }
        else {
            $("#dvviewpricecontent").html("No Offer Selected");
            $("#dvviewprice").modal();
        }
    }

    this.UnSubScribeOffer = function () {
        var offerId = $('#removeOfferId').val();
        var supplierInviteId = $('#removeSupplierInviteId').val();;
        var reason = $("#drpReasonList").val();
        if (reason == '') {
            $("#dvOfferUnsubscribe").modal();
            return;
        }
        var url = "/handler/Offer/UnsubscribeOffer.ashx";
        $.ajax({
            url: url,
            type: "POST",
            data: { "offerId": offerId, "supplierInviteId": supplierInviteId, "Reason": reason },
            success: function (data) {
                if (data == "SUCCESS") {
                    $("#btncancelremoval").click();
                    ///  $('#removal-offer').modal('hide');
                    Supplier.ManageOfferAvailability.ViewByDateRange(0);
                }
            },
            error: function (data, status, jqXHR) {
            }
        });

    }

    this.ResizeWindow = function (setdefault) {
        if (!setdefault) {
            $("#leftbar").removeClass("col-md-3").addClass("col-md-2");
            $("#page-content-wrapper1").removeClass("col-md-9").addClass("col-md-10");
        }
        else {
            $("#leftbar").removeClass("col-md-2").addClass("col-md-3");
            $("#page-content-wrapper1").removeClass("col-md-10").addClass("col-md-9");
        }
    }

    this.Init = function () {
        $.ajaxSetup({ cache: false });
        $('.container').addClass("fullwidth");
        //disabled enter form submit
        $("form").bind("keypress", function (e) {
            if (e.keyCode == 13) {
                return false;
            }
        });
        if ($(window).width() > 1400) {
            Supplier.ManageOfferAvailability.ResizeWindow(false);
        }
        $(window).resize(function () {
            Supplier.ManageOfferAvailability.ResizeWindow(!($(window).width() > 1400));
        });
        var minDate = new Date($.datepicker.parseDate(Rgen.Config.DatePicker.format, $("#serverdate").val())).addDays(-90);
        var maxDate = new Date($.datepicker.parseDate(Rgen.Config.DatePicker.format, $("#serverdate").val()));
        maxDate.setFullYear(maxDate.getFullYear() + 1);
        $("#datepickerFrom").datepicker({
            dateFormat: Rgen.Config.DatePicker.format,
            showOn: "both",
            buttonText: Rgen.Config.DatePicker.Text,
            minDate: minDate,
            maxDate: maxDate,
            onSelect: function (dateStr) {
                var date = new Date($.datepicker.parseDate(Rgen.Config.DatePicker.format, dateStr));
                $("#datepickerTo").datepicker("option", { minDate: date });
            }
        });
        $("#datepickerTo").datepicker({
            dateFormat: Rgen.Config.DatePicker.format,
            showOn: "both",
            buttonText: Rgen.Config.DatePicker.Text,
            minDate: minDate,
            maxDate: maxDate,
        });

        Supplier.ManageOfferAvailability.ViewByDateRange(0);

        $(window).bind("beforeunload", function (event) {
            if (Supplier.ManageOfferAvailability.alertmessage()) {
                return "You have unsaved changes";
            }
        });

        $("#menu-toggle").click(function (e) {
            e.preventDefault();
            $("#leftbar").toggleClass("toggled");
            if ($("#leftbar").hasClass('toggled')) {
                $("#page-content-wrapper1").addClass("offerfullwidth");
            }
            else {
                $("#page-content-wrapper1").removeClass("offerfullwidth");
            }
        });


        $("#printofferavailability").on('click', function () {
            Supplier.ManageOfferAvailability.printDiv("offerprintable");

        });
    }
    this.RedirectToBacktoAvailability = function () {
        var url = $("#offerurl").val();
        var sDate = $('#datepickerFrom').val();
        var eDate = $('#datepickerTo').val();

        url = url + "?fromdate=" + sDate + "&todate=" + eDate;
        window.location.href = url;

    }
};
$(function () {
    if ($("#pagetype").val() == "offeravailability") {
        Supplier.ManageOfferAvailability.Init();
    }
})

