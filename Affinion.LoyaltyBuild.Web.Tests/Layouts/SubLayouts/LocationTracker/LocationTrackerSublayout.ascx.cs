﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           LocationTrackerSublayout.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.Tests.Layouts.SubLayouts
/// Description:           Contains the code behind of the LocationTracker sublayout. 
/// 

namespace Affinion.LoyaltyBuild.Web.Tests.Layouts.SubLayouts.LocationTracker
{
    #region Directives
    using Affinion.LoyaltyBuild.ExternalServices.LocationService.Services;    
    using System;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    #endregion Directives

    /// <summary>
    /// Partial class for LocationTracker sublayout
    /// </summary>
    public partial class LocationTrackerSublayout : System.Web.UI.UserControl
    {

        #region Constants

        private const string LocationName = "LocationName";

        #endregion Constants


        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">argument</param>
        private void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //call set location to load values to dropdown list
                this.SetLocation();
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ExternalServices, exception, this);
                ErrorLabel.Text = exception.Message;                
            }
        }

        /// <summary>
        /// Get the locations to the dropdown list
        /// </summary>
        private void SetLocation()
        {
            GoogleLocationService service = new GoogleLocationService();
            //Call GetLocation service class to get the list of locations.
            LocationsDropdownList.DataSource = service.GetLocations();
            LocationsDropdownList.DataTextField = LocationName;
            LocationsDropdownList.DataBind();

        }


    }
}