﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SimpleLinkSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.Common.Layouts.SubLayouts.Common.SimpleLinkSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<asp:Literal ID="Prefix" runat="server" />
<p id="PlaceholderText" runat="server">Set the datasource: Required Fields: Link</p>
<sc:Link Field="Link" ID="TextLink" runat="server"></sc:Link>
<asp:Literal ID="Suffix" runat="server" />