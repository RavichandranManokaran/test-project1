﻿using Affinion.LoyaltyBuild.Api.Booking.Data;
using Affinion.LoyaltyBuild.Api.Booking.Data.Basket;
using Affinion.LoyaltyBuild.BedBanks.General;
using Affinion.LoyaltyBuild.BedBanks.Helper;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Message;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Model.Room;
using Affinion.LoyaltyBuild.BedBanks.Troika;
using Affinion.LoyaltyBuild.Common.Exceptions;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.Model.Product;
using Affinion.LoyaltyBuild.Model.Provider;
using Affinion.LoyaltyBuild.UCom.Common.Extension;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Availability;
using Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi;
using Newtonsoft.Json;
using Sitecore.Data;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using UCommerce;
using common = Affinion.LoyaltyBuild.Common;
using model = Affinion.LoyaltyBuild.Model.Pricing;

namespace Affinion.LoyaltyBuild.Api.Booking.Extension
{
    /// <summary>
    /// Pre process booking class
    /// </summary>
    static class PreProcessor
    {
        /// <summary>
        /// Pre processing booking
        /// </summary>
        /// <param name="product"></param>
        /// <param name="properties"></param>
        /// <returns></returns>
        public static PreProcessResult PreProcessBooking(this IProduct product, Dictionary<string, string> properties)
        {
            PreProcessResult result = null;

            switch (product.Type)
            {
                case ProductType.Room:
                case ProductType.Package:
                    result = PreProcessRoomBooking(product, properties);
                    break;
                case ProductType.BedBanks:
                    result = PreProcessBedBanksBooking(product, properties);
                    break;
            }

            return result;
        }



        #region Private methods

        /// <summary>
        /// Reverse map currency
        /// </summary>
        /// <param name="properties"></param>
        /// <returns></returns>
        private static BedBanksCurrency ReverseMapCurrency(Dictionary<string, string> properties)
        {
            string currencyId = properties.Where(x => x.Key == Constants.BBCurrencyId).Select(y => y.Value).FirstOrDefault();
            string itemPath = Constants.CurrItemPath;
            Item[] currItems = Sitecore.Context.Database.SelectItems(itemPath);
            Item currItem = currItems.Where(x => x.Fields["Bed Banks Currency Id"].Value.Equals(currencyId)).Select(j => (Item)j).FirstOrDefault();
            BedBanksCurrency curr = new BedBanksCurrency();
            curr.BedBankCurrencyItem = currItem;
            curr.BedBanksCurrencyId = currencyId;
            curr.CurrencyName = SitecoreFieldsHelper.GetItemFieldValue(currItem, "Currency Name");
            curr.uCommerceCurrencyId = SitecoreFieldsHelper.GetItemFieldValue(currItem, "uCommerce Currency Id");
            return curr;
        }

        /// <summary>
        /// Get margin
        /// </summary>
        /// <param name="providerId"></param>
        /// <param name="arrivalDate"></param>
        /// <returns></returns>
        public static string GetMargin(string providerId, string arrivalDate)
        {
            DataTable marginData = TroikaDataController.GetMarginForEAchBBHotel(providerId, arrivalDate.ToString());
            string marginId = marginData.Rows[0][0].ToString();
            string marginPercentage = marginData.Rows[0][1].ToString();
            return marginId + "," + marginPercentage;
        }

        #endregion

        #region Preprocess Booking

        /// <summary>
        /// Pre processing booking
        /// </summary>
        /// <param name="product"></param>
        /// <param name="properties"></param>
        /// <returns></returns>
        private static PreProcessResult PreProcessBedBanksBooking(IProduct product, Dictionary<string, string> properties)
        {
            var result = new PreProcessResult();
            JtPreBookResponse propertyResponse = null;

            decimal priceWithMargin;
            decimal tempPercent = 0;

            //Create bedbank request
            AddBedBanksRequest bedBankRequest = properties.ToBasketRequest<AddBedBanksRequest>();
            var client = Sitecore.Context.Database.GetItem(new ID(bedBankRequest.ClientId));
            BedBanksSettings bbs = new BedBanksSettings(client);
            var acutalCurr = ReverseMapCurrency(properties);
            //set bed banks currency
            if (acutalCurr != null && !string.IsNullOrEmpty(acutalCurr.BedBanksCurrencyId))
            {
                bbs.BedBanksActualCurrency = acutalCurr;
            }

            //pre book request
            JacTravelApiWrapper jac = new JacTravelApiWrapper();
            propertyResponse = jac.PreBook(bedBankRequest.PropertyID.ToString(), bedBankRequest.CheckinDate, bedBankRequest.NoofNights.ToString(), bedBankRequest.PreBookingToken, bedBankRequest.MealBasisID, bedBankRequest.NoOfRooms.ToString(), bedBankRequest.NoOfAdults.ToString(), bedBankRequest.NoOfChildren.ToString(), bedBankRequest.ChildrenAges, bbs);
            //TODO handle prebook failure

            if (propertyResponse != null)
            {
                //Calculate margin amount  
                decimal.TryParse(properties.Where(x => x.Key == Constants.MarginPercentage).Select(x => x.Value).FirstOrDefault(), out tempPercent);
                decimal marginAmount = (tempPercent / 100) * ((decimal)propertyResponse.TotalPrice);

                //create Cancel criteria 
                var list = new List<BedBankCancelCriteria>();
                for (int i = 0; i < propertyResponse.Cancellations.Length; i++)
                {
                    DateTime startDate, endDate, arrivalDate;

                    if (DateTime.TryParse(propertyResponse.Cancellations[i].StartDate, out startDate)
                        && DateTime.TryParse(propertyResponse.Cancellations[i].EndDate, out endDate))
                    {
                        DateTime.TryParse(properties.Where(x=>x.Key==Constants.CheckinDate).Select(y=>y.Value).FirstOrDefault(),out arrivalDate);                        
                        list.Add(new BedBankCancelCriteria
                        {
                            StartDate = DateTime.Parse(propertyResponse.Cancellations[i].StartDate),
                            EndDate = DateTime.Parse(propertyResponse.Cancellations[i].EndDate) > arrivalDate ? arrivalDate : DateTime.Parse(propertyResponse.Cancellations[i].EndDate),
                            Amount = Math.Round((decimal)propertyResponse.Cancellations[i].Penalty + marginAmount, 2),
                        });
                    }
                }

                priceWithMargin = ((decimal)propertyResponse.TotalPrice) + marginAmount;

                //Add the values to property result
                result[Constants.CancelCriteria] = JsonConvert.SerializeObject(list);
                result[Constants.RoomToken] = propertyResponse.PreBookingToken; // Prebook response token
                result[Constants.PaymentMethod] = common.Constants.PaymentMethod.FullPayment;
                result[Constants.ProviderType] = Affinion.LoyaltyBuild.Common.Constants.ProviderType.BedBanks;
                int uCommerceCurrId = 0;
                int.TryParse(properties.Where(x => x.Key.Equals(Constants.CurrencyId)).Select(x => x.Value).FirstOrDefault(), out uCommerceCurrId);
                result[Constants.Price] = priceWithMargin.ToString();
                if (uCommerceCurrId != 0)
                    result.UnitPrice = new Money(priceWithMargin, UCommerce.EntitiesV2.Currency.FirstOrDefault(x => x.CurrencyId == uCommerceCurrId));

                result[Constants.ProcessingFee] = "0.00";
                result[Constants.CommissionAmount] = marginAmount.ToString();
                result[Constants.ProviderAmount] = ((decimal)propertyResponse.TotalPrice).ToString();
                if (bbs.BedBanksActualCurrency != null)
                {
                    result[Constants.CalculatedProcessingFee] = bbs.BedBanksProcessingFees.FirstOrDefault(x => x != null && x.BedBanksCurrencyItem.ID == bbs.BedBanksActualCurrency.BedBankCurrencyItem.ID).ProccesingFee.ToString();
                }
                else
                    result[Constants.CalculatedProcessingFee] = bbs.BedBanksProcessingFees.FirstOrDefault(x => x != null && x.BedBanksCurrencyItem.ID == bbs.BedBanksDefaultCurrency.BedBankCurrencyItem.ID).ProccesingFee.ToString();
            }
            return result;
        }

        /// <summary>
        /// Pre process room booking
        /// </summary>
        /// <param name="product"></param>
        /// <param name="properties"></param>
        /// <returns></returns>
        private static PreProcessResult PreProcessRoomBooking(IProduct product, Dictionary<string, string> properties)
        {
            var result = new PreProcessResult();
            List<model.ProductPrice> productList = null;

            AddRoomRequest roomRequest = properties.ToBasketRequest<AddRoomRequest>();

            RoomAvailabilityService roomAvailabilityService = new RoomAvailabilityService();

            //check room is available before adding to basket
            var availability = LoyaltyBuildCatalogLibrary.GetRoomAvailabilityWithPrice(new List<string> { product.VariantSku },
                roomRequest.CheckinDate.ConvetDatetoDateCounter(),
                roomRequest.CheckOutDate.ConvetDatetoDateCounter() - 1);
            if (availability.Any(i => i.AvailableRoom < 1))
                throw new AffinionException("Not enough items available");


            productList = roomAvailabilityService.GetProductPriceByDateRange(product.Sku, product.VariantSku, roomRequest.CheckinDate, roomRequest.CheckOutDate.AddDays(-1));
            if (productList != null)
            {
                decimal totalPrice = 0;
                decimal totalCommission = 0;
                decimal processingFee = 0;
                int count = 0;
                foreach (var productItem in productList)
                {
                    count++;
                    totalPrice += productItem.PriceAmount;

                    if (productItem.Commision.ProcessingFee > processingFee)
                        processingFee = productItem.Commision.ProcessingFee;
                    switch (count)
                    {
                        case 1:
                            totalCommission += productItem.Commision.Day1;
                            break;
                        case 2:
                            totalCommission += productItem.Commision.Day2;
                            break;
                        case 3:
                            totalCommission += productItem.Commision.Day3;
                            break;
                        case 4:
                            totalCommission += productItem.Commision.Day4;
                            break;
                        case 5:
                            totalCommission += productItem.Commision.Day5;
                            break;
                        case 6:
                            totalCommission += productItem.Commision.Day6;
                            break;
                        case 7:
                            totalCommission += productItem.Commision.Day7;
                            break;
                        default:
                            totalCommission += productItem.Commision.Common;
                            break;
                    }
                }
                int currencyId = roomRequest.CurrencyId;

                result.UnitPrice = new Money(totalPrice, UCommerce.EntitiesV2.Currency.FirstOrDefault(x => x.CurrencyId == currencyId));
                result.Vat = new Money(0, UCommerce.EntitiesV2.Currency.FirstOrDefault(x => x.CurrencyId == currencyId));
                result[Constants.CommissionAmount] = Math.Round(totalCommission, 2).ToString();
                result[Constants.CalculatedProcessingFee] = Math.Round(processingFee, 2).ToString();
                result[Constants.ProviderType] = Affinion.LoyaltyBuild.Common.Constants.ProviderType.Hotel;
                result[Constants.ProviderAmount] = Math.Round((totalPrice - totalCommission), 2).ToString();
            }
            return result;
        }


        #endregion

    }
}
