﻿/* Procedure for fetching Transfer History details based on @OrderLineId */

CREATE PROCEDURE [dbo].[LB_GetTransferHistoryDetail]
(	 
	@OrderLineId int=NULL	
)
AS 
BEGIN
	IF (@OrderLineId IS NOT NULL)
	BEGIN		
		SELECT OL.OrderLineId [OrderLineId],			
			C.Name [SupplierName],
			RR.CheckInDate [ArrivalDate],
			RR.CheckOutDate [DepartureDate],
			1 [NoOfRooms],
			CASE WHEN DATEDIFF(day, RR.CheckInDate,RR.checkOutdate) IS NULL THEN 0 ELSE DATEDIFF(day, RR.CheckInDate,RR.checkOutdate) END as [NoOfNights],	
			CASE WHEN RR.NoOfGuest IS NULL THEN 0 ELSE RR.NoOfGuest END as [NoOfPeople], 		
			OL.CreatedOn [ReservationDate]
		FROM [dbo].uCommerce_OrderLine OL 			
			INNER JOIN [dbo].LB_RoomReservation RR ON OL.OrderLineId = RR.OrderLineId 
			INNER JOIN [dbo].uCommerce_Product P ON OL.Sku = P.Sku
			INNER JOIN [dbo].uCommerce_CategoryProductRelation CPR ON P.ProductId = CPR.ProductId 
			INNER JOIN [dbo].uCommerce_Category C ON CPR.CategoryId = C.CategoryId			
		WHERE 				
			OL.OrderLineId in (SELECT OrderLineId from [dbo].[LB_FnRelatedOrderLines](@OrderLineId))
			AND OL.OrderLineId != @OrderLineId
		order by OL.CreatedOn asc;			
	END
END
