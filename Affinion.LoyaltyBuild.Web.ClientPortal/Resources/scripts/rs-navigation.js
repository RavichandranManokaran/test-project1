/*jslint browser: true*/
/*global $, jQuery*/

/*
Custom ResponsiveMenu 
*/
// Anonymous Function Wrapper

(function () {
    "use strict";
    //Responsive Menu Handler
    //Read State 
    function pushMenu() {
        var menuRight, body;
        menuRight = $('#rs-res-nav');
        body = $("body");
        $(body).toggleClass("cbp-spmenu-push-toleft");
        $(menuRight).toggleClass("cbp-spmenu-open");
        // body...
    }
    //No Push Menu
    function noPushMenu() {
        var body = $("body");
        $("#rs-res-nav").removeClass("cbp-spmenu-open");
        $(body).removeClass("cbp-spmenu-push-toleft");
    }
    /*Enable Responsive Menu*/
    function enableResMenu() {

        $(".cbp-spmenu-push").css({
            "right": "240px"
        });
        $("#rs-res-nav").removeClass();
        $("#rs-res-nav").addClass("cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right rs-margin-top-navi");
        $(".rs-nav-back").removeClass("rs-nav-back");
        $("#nav2").removeClass("rs-navbar");
        $(".rs-noPadding-left-right").addClass("rs-5padding");
        $(".rs-social-icons").prependTo('#rs-topleft-links');
        
        //Menu Replacement
        $("#rs-nav-user").appendTo("#nav2"); // Login user
        $("#lang-globe").appendTo("#nav2"); // Language selector
        /*Global Menu Re-Structure Enable On Responsive */
        $(".rs-topleft-links-home").appendTo("#nav2");

        $(".rs-topleft-links-home").show();
        $("#lang-globe").addClass("rs-nav2 rs-nav-responsive");
        $("#rs-nav-user").removeClass();
        $("#rs-nav-user").addClass("nav navbar-nav rs-nav2 rs-nav-responsive");
        $("#rs-nav-user").show();
        //        init();

    }
    /*Disable Responsive Menu*/
    function disableResMenu() {

        $(".cbp-spmenu-push").css({
            "right": "0"
        });
        $("#nav2").addClass("rs-navbar");
        $("#rs-res-nav").removeClass();
        $("#rs-res-nav").addClass("navbar navbar-default");
        $(".rs-nav2").addClass("rs-nav-back");
        $("#rs-nav-user").appendTo("#rs-nav-Main");
        $("#rs-nav-user").removeClass();
        $(".rs-5padding").addClass("rs-noPadding-left-right");
        $(".rs-noPadding-left-right").removeClass("rs-5padding");
        $("#rs-nav-user").addClass("nav navbar-nav navbar-right");
        $("#lang-globe").appendTo(".lang-globe-holder-nav1");
        /*Global Menu Re-Structure Disable On Responsive */
        $(".rs-topleft-links-home").appendTo("#rs-topleft-links");

        $("#lang-globe").removeClass();
        $("#lang-globe").addClass('nav navbar-nav');
        $(".rs-social-icons").prependTo('.rs-social-icons-holder');
    }

    $(document).on('click', '*', function (evt) {
        var resNav = $('#rs-res-nav'),
            trigger = $('#rs-navtrigger');
        if ($(this).is(trigger)) {
            evt.stopPropagation(); // stop the event propagating to ancestral elements
            trigger.css("background", "transparent");
            pushMenu();
        } else if (!$(this).closest(resNav).length) {
            //<-- on click outside, hide menu   
            trigger.css("background", "#0093d0");
            noPushMenu();
        }
    });
    //Document Ready Event Handler for ResMenu
    $(document).ready(function () {
        var width, height;
        width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
        height = (window.innerHeight > 0) ? window.innerHeight : screen.height;
        if (width <= 767) {
            enableResMenu();
            $("#nav2").css("max-height", (height - 60));
        } else {
            disableResMenu();
        }
    });
    //On Resize CrosBrowser Handler
    $(window).on('resize', function () {
        noPushMenu();
        var width, height;
        width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
        height = (window.innerHeight > 0) ? window.innerHeight : screen.height;
        if (width <= 767) {
            enableResMenu();
            $("#nav2").css("max-height", (height - 60));
        } else {
            disableResMenu();
        }
    });
}());
//End of Wrapper Annon