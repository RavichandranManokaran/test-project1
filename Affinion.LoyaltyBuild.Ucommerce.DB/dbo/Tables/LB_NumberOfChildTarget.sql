﻿CREATE TABLE [dbo].[LB_NumberOfChildTarget] (
    [NumberOfChildTargetId] INT NOT NULL,
    [NumberOfChild]         INT NOT NULL,
    CONSTRAINT [PK_uCommerce_NumberOfChild] PRIMARY KEY CLUSTERED ([NumberOfChildTargetId] ASC),
    CONSTRAINT [FK_uCommerce_NumberOfChildTarget_uCommerce_Target] FOREIGN KEY ([NumberOfChildTargetId]) REFERENCES [dbo].[uCommerce_Target] ([TargetId])
);

