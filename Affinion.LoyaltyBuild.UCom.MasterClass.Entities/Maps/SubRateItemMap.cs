﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2.Maps;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Maps
{
    public class SubRateItemMap : BaseClassMap<SubRateItem>
    {
        // Methods
        public SubRateItemMap()
        {
            base.Table("LB_SubrateItem");
            Id(x => x.SubRateItemCode).Column("SubRateItemCode");
            base.Map(x => x.SubRateItemCode).Not.Nullable();
            base.Map(x => x.DisplayName).Not.Nullable();
        }
    }
}
