﻿
IF not EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[uCommerce_OrderAddress]') 
         AND name = 'Line3'
)
ALTER TABLE [dbo].[uCommerce_OrderAddress]
ADD [Line3] [nvarchar](512) NULL


IF not EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[uCommerce_OrderAddress]') 
         AND name = 'County'
)
ALTER TABLE [dbo].[uCommerce_OrderAddress]
ADD [County] [nvarchar](512) NULL

IF not EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[uCommerce_OrderAddress]') 
         AND name = 'Branch'
)
ALTER TABLE [dbo].[uCommerce_OrderAddress]
ADD [Branch] [nvarchar](512) NULL


IF not EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[uCommerce_OrderAddress]') 
         AND name = 'IsEmail'
)
ALTER TABLE [dbo].[uCommerce_OrderAddress]
ADD [IsEmail]  [bit]  NULL


IF not EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[uCommerce_OrderAddress]') 
         AND name = 'IsTerms'
)
ALTER TABLE [dbo].[uCommerce_OrderAddress]
ADD [IsTerms]  [bit]  NULL