﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion Inernational. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           HtmlHelper.cs
/// Sub-system/Module:     ClientSetup(VS project name)
/// Description:           HtmlHelper Class use to populate html wizard tags
/// </summary>


#region Using Directives
using Affinion.LoyaltyBuild.Common.Exceptions;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 
#endregion

namespace Affinion.LoyaltyBuild.ClientSetup.Wizard
{
    /// <summary>
    /// This is a helper class use to manuplate XML files
    /// </summary>
    public static class HtmlHelper
    {
        /// <summary>
        /// local variable which use to store html tags
        /// </summary>
        public static string RenderHtml { get; set; }

        #region Public functions 

        /// <summary>
        /// use to populate the Wizard content
        /// </summary>
        public static void PopulateWizardContent()
        {
            
            // add the returning list to Iterator degign pattern
            IWizardAggregate<WizardDialog> aggregate = null;
            string str = string.Empty;

            try
            {
                aggregate = new ConcreteAggregate<WizardDialog>();
                List<WizardDialog> list = XMLParsers.ParseByXDocument();
                
                ///This will add trace information to the log
                Diagnostics.Trace(DiagnosticsCategory.ClientSetup, "wizard module, xml helper started");

                foreach (var item in list)
                {
                    aggregate.AddItem(item);
                }

                foreach (WizardDialog i in aggregate.GetAll())
                {
                    //str += "<fieldset> <h2 class=\"fs-title\">" + i.SampleDescription + "</h2> <h3 class=\"fs-subtitle\">This is step 1</h3>";
                    str += string.Concat("<fieldset> <h2 class=\"fs-title\">", i.SampleDescription + "</h2> <h3 class=\"fs-subtitle\">This is step 1</h3>");

                    foreach (var item in i.HtmlElements.ListElements)
                    {
                        //<input type="button" name="next" class="next action-button" value="Next" />
                        str +=  string.Concat( "<input type = \"" , item.Type , "\"  name = \"" , item.Name , "\" " , "placeholder= \"" , item.Placeholder , "\"" , "class= \"" , item.Class , "\"" , "value= \"" + item.Value , "\" ");
                        str += "> </input>";
                    }

                    // type="password" name="pass" placeholder="Password" 
                    //str += @"<asp:TextBox id=" + "\"tb1 \"" + "runat=" + "\"server\"" + "type=" + "\"pass\""  + " placeholder= "+ "\"Password\""  + " />";
                    str += "</fieldset>";
                }

                Diagnostics.Trace(DiagnosticsCategory.ClientSetup, "wizard module creating xml tags completed");
            }

            catch (ArgumentException ex)
            {
                ///Catch argument exceptions
                ///First parameter : Module
                ///Second parameter: Exception
                ///Third parameter : Object logging
                Diagnostics.WriteException(DiagnosticsCategory.ClientSetup, ex, aggregate);
                throw;
            }
            catch (AffinionException ex)
            {
                ///Catch affinion exceptions
                Diagnostics.WriteException(DiagnosticsCategory.ClientSetup, ex, aggregate);
                throw;
            }
            catch (Exception ex)
            {
                ///Catch general exceptions
                Diagnostics.WriteException(DiagnosticsCategory.ClientSetup, ex, aggregate);
                throw;
            }

            //<asp:TextBox id="tb1" runat="server" />
            RenderHtml = str;
        } 

        #endregion
    }
}