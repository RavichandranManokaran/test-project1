﻿CREATE PROCEDURE [dbo].[LB_DeleteDue]
(
	@OrderLineDueId INT,
	@Reason VARCHAR(MAX)
)
AS
BEGIN
	UPDATE orderlinedue
	SET orderlinedue.IsDeleted = 1, Note = (ISNULL(Note,'') + ISNULL(@Reason,''))
	FROM LB_OrderLineDue orderlinedue
		INNER JOIN LB_DueTypes duetypes ON duetypes.DueTypeId = orderlinedue.DueTypeId
	WHERE orderlinedue.OrderLineDueId = @OrderLineDueId 
		AND duetypes.CanBeDeleted = 1


	DECLARE @OrderLineId INT
	SELECT @OrderLineId = OrderLineId FROM LB_OrderLineDue orderlinedue
		INNER JOIN LB_DueTypes duetypes ON duetypes.DueTypeId = orderlinedue.DueTypeId
	WHERE orderlinedue.OrderLineDueId = @OrderLineDueId 
		AND duetypes.CanBeDeleted = 1

	IF(@OrderLineId IS NOT NULL)
		EXEC [LB_UpdateStatusOnDueAndPay] @OrderLineId = @OrderLineId, @CreatedBy = ''
END
