﻿using System;
using System.Xml.Serialization;

namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Model
{
    [Serializable()]
    [XmlType(TypeName = "Cancellation")]
    public class JtCancellation
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public double Penalty { get; set; }
    }
}
