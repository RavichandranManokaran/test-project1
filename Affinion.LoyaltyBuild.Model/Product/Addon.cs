﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Model.Product
{
    /// <summary>
    /// Add on product
    /// </summary>
    public class Addon : IProduct
    {
        /// <summary>
        /// Sku
        /// </summary>
        public string Sku { get; set; }

        /// <summary>
        /// Variant Sku
        /// </summary>
        public string VariantSku { get; set; }

        /// <summary>
        /// Product type
        /// </summary>
        public ProductType Type { get; set; }

        /// <summary>
        /// Ucommerce currency id
        /// </summary>
        public int CurrencyId { get; set; }

        // <summary>
        /// Gets or sets the Offer Price
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }
    }
}
