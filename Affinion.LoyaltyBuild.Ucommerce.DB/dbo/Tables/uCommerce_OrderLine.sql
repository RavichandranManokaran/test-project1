﻿CREATE TABLE [dbo].[uCommerce_OrderLine] (
    [OrderLineId]  INT            IDENTITY (1, 1) NOT NULL,
    [OrderId]      INT            NOT NULL,
    [Sku]          NVARCHAR (512) NOT NULL,
    [ProductName]  NVARCHAR (512) NOT NULL,
    [Price]        MONEY          NOT NULL,
    [Quantity]     INT            NOT NULL,
    [CreatedOn]    DATETIME       CONSTRAINT [uCommerce_DF_OrderLine_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [Discount]     MONEY          CONSTRAINT [uCommerce_DF_OrderLine_Rebate] DEFAULT ((0)) NOT NULL,
    [VAT]          MONEY          CONSTRAINT [uCommerce_DF_OrderLine_Vat] DEFAULT ((0)) NOT NULL,
    [Total]        MONEY          NULL,
    [VATRate]      MONEY          NOT NULL,
    [VariantSku]   NVARCHAR (512) NULL,
    [ShipmentId]   INT            NULL,
    [UnitDiscount] MONEY          NULL,
    [CreatedBy]    NVARCHAR (255) NULL,
    [PaidInFull]   BIT            NULL,
    CONSTRAINT [uCommerce_PK_OrderLine] PRIMARY KEY CLUSTERED ([OrderLineId] ASC),
    CONSTRAINT [FK_OrderLine_Shipment] FOREIGN KEY ([ShipmentId]) REFERENCES [dbo].[uCommerce_Shipment] ([ShipmentId]),
    CONSTRAINT [uCommerce_FK_OrderLine_Order] FOREIGN KEY ([OrderId]) REFERENCES [dbo].[uCommerce_PurchaseOrder] ([OrderId])
);


GO
CREATE NONCLUSTERED INDEX [IX_uCommerce_OrderLine_OrderId]
    ON [dbo].[uCommerce_OrderLine]([OrderId] ASC)
    INCLUDE([OrderLineId], [Sku], [ProductName], [Price], [Quantity], [CreatedOn], [Discount], [VAT], [Total], [VATRate], [VariantSku], [ShipmentId], [UnitDiscount]);


GO
CREATE NONCLUSTERED INDEX [IX_uCommerce_OrderLine_ShipmentId]
    ON [dbo].[uCommerce_OrderLine]([ShipmentId] ASC)
    INCLUDE([OrderLineId], [OrderId], [Sku], [ProductName], [Price], [Quantity], [CreatedOn], [Discount], [VAT], [Total], [VATRate], [VariantSku], [UnitDiscount]);

