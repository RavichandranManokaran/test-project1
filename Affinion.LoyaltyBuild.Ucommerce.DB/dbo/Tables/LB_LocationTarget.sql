﻿CREATE TABLE [dbo].[LB_LocationTarget] (
    [LocationTargetId] INT            NOT NULL,
    [Location]         NVARCHAR (500) NOT NULL,
    CONSTRAINT [PK_uCommerce_LocationTarget] PRIMARY KEY CLUSTERED ([LocationTargetId] ASC),
    CONSTRAINT [FK_uCommerce_LocationTarget_uCommerce_Target] FOREIGN KEY ([LocationTargetId]) REFERENCES [dbo].[uCommerce_Target] ([TargetId])
);

