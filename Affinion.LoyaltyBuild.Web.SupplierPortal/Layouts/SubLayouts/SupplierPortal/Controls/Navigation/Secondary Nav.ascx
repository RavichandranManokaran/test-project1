﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Secondary Nav.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Navigation.Secondary_Nav" %>

<div class="side-nav sidebar-block" id="menuWrapper" runat="server">
 <h2><sc:FieldRenderer ID="MenuHeader" runat="server" FieldName="Menu Title" /></h2>
 <asp:Repeater ID="rptTree" runat="server" OnItemDataBound="rptTree_ItemDataBound">
  <HeaderTemplate><ul></HeaderTemplate>
  <ItemTemplate>
   <li>
    <asp:Hyperlink ID="MenuLink" runat="server">
     <sc:FieldRenderer ID="MenuText" runat="server" FieldName="Menu Title" />
    </asp:Hyperlink>
    <asp:PlaceHolder ID="phSubTree" runat="server" />
   </li>
  </ItemTemplate>
  <FooterTemplate></ul></FooterTemplate> 
 </asp:Repeater>
</div>


