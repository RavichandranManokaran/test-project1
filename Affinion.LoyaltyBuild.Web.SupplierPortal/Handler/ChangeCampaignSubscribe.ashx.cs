﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;
//using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Handler
{
    /// <summary>
    /// Summary description for ChangeCampaignSubscribe
    /// </summary>
    public class ChangeCampaignSubscribe : IHttpHandler, System.Web.SessionState.IRequiresSessionState 
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                string supplierId = context.Request.Form["supplierId"];
                string isSubscribe = context.Request.Form["isSubscribe"];
                string offerInfo = context.Request.Form["OfferDetail"];
                context.Response.Write(LoadOffers(supplierId, isSubscribe, offerInfo, context));
            }
            catch (Exception er)
            {
                context.Response.Write("ERROR" + er.Message.ToString());
                Sitecore.Diagnostics.Log.Error(er.Message, new object());
            }
        }
        protected static string LoadOffers(string supplierId, string isSubscribe, string offerInfo, HttpContext context)
        {
            StringBuilder s = new StringBuilder();
            AvailabilityHelper dataForOffers = new AvailabilityHelper(context);
            bool res = dataForOffers.ChangeCampaignSubscribe(Convert.ToInt32(supplierId), Convert.ToBoolean(isSubscribe.ToString()),offerInfo);
            return res.ToString();
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}