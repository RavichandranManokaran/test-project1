﻿CREATE VIEW [dbo].[LB_APDemandsRequestView]
AS
SELECT DISTINCT
		P.ProductId [HotelID],
		SPR.DateCreated [DemandDate],
		'REF='+BI.BookingNo [BookingRefNo],
		'ID = '+CONVERT(VARCHAR,SPR.OrderId) AS BookingID, 
		SPR.OrderId [OrderID],
		SPR.OrderLineId,
		(C.FirstName+' '+C.LastName) [CustomerName],
		BI.CheckInDate,
		BI.CheckOutDate,
		ISNULL(SPR.IsPaidToAccounts,0) [PaymentStatus],
		SPR.SuggestedAmount [SuggestedAmount],
		SPR.DatePostedToOracle [DatePostedToOracle],
		SPR.OrderCurrencyIsoCode [Currency],
		'AP'+CONVERT(VARCHAR,SPR.OracleFileId) [OracleFieID],
		SPR.SupplierPaymentId [SupplierPaymentId],
		SPR.OrderDate [BookingDate]
FROM LB_BookingInfo BI
		INNER JOIN ucommerce_OrderLine OL ON BI.OrderLineId=OL.OrderLineId 
		INNER JOIN LB_Supplier_Payments SPR ON  OL.OrderId=SPR.OrderId AND SPR.OrderLineId=OL.OrderLineId
		INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId=PO.OrderId
		INNER JOIN LB_CustomerProperty CP ON PO.CustomerId=CP.CustomerId
		INNER JOIN uCommerce_Customer C ON CP.CustomerId=C.CustomerId
		INNER JOIN Ucommerce_product P ON OL.Sku = P.Sku AND P.ParentProductId IS NULL AND P.VariantSku IS NULL
		INNER JOIN uCommerce_ProductDefinition PD ON P.ProductDefinitionId=PD.ProductDefinitionId AND PD.Name='Hotel'
		 
WHERE 
	    BI.BookingNo IS NOT NULL AND
	    PO.CustomerId IS NOT NULL 


