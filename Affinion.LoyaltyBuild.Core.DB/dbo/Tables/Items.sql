﻿CREATE TABLE [dbo].[Items] (
    [ID]         UNIQUEIDENTIFIER NOT NULL,
    [Name]       NVARCHAR (256)   NOT NULL,
    [TemplateID] UNIQUEIDENTIFIER NOT NULL,
    [MasterID]   UNIQUEIDENTIFIER NOT NULL,
    [ParentID]   UNIQUEIDENTIFIER NOT NULL,
    [Created]    DATETIME         NOT NULL,
    [Updated]    DATETIME         NOT NULL,
    [DAC_Index]  INT              NULL
);


GO
CREATE CLUSTERED INDEX [DAC_Clustered]
    ON [dbo].[Items]([DAC_Index] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [ndxID]
    ON [dbo].[Items]([ID] ASC);


GO
CREATE NONCLUSTERED INDEX [ndxName]
    ON [dbo].[Items]([Name] ASC);


GO
CREATE NONCLUSTERED INDEX [ndxParentID]
    ON [dbo].[Items]([ParentID] ASC);


GO
CREATE NONCLUSTERED INDEX [ndxTemplateID]
    ON [dbo].[Items]([TemplateID] ASC);

