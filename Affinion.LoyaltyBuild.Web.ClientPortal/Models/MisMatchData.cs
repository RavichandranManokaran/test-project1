﻿using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml.Linq;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Models
{
    public class MisMatchData
    {
        public DataTable realexData { get; set; }
        public DataTable reportTable { get; set; }
        private int rowCount { get; set; }
        public void ReadFile(string filepath)
        {
            var table = new DataTable();
            var fileStream = new FileStream(@filepath, FileMode.Open, FileAccess.Read);
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
            {
                XDocument doc = XDocument.Load(streamReader);
                var record = doc.Descendants("recordset").First();
                int count;
                int.TryParse(record.Attribute("rowCount").Value, out count);
                var names = record.Attribute("fieldNames").Value.Split(',');
                var fields = record.Elements("field");

                foreach (var item in names)
                {
                    table.Columns.Add(item);
                }
                for (var index = 0; index < count; index++)
                {
                    var row = table.NewRow();
                    foreach (var item in names)
                    {
                        row[item] = fields.Where(i => i.Attribute("name").Value == item).Elements().ElementAt(index).Value;
                    }
                    table.Rows.Add(row);
                }
            }
            table.DefaultView.Sort = "ORDERID";
            this.realexData = table;
            //uCommerceConnectionFactory.StoreRealexReport(this.realexData);
        }

        private DataTable GetLBpaymentData()
        {
            DataSet lbPaymentData = uCommerceConnectionFactory.paymentData();
            return lbPaymentData.Tables[0];
        }

        public DataTable CompareData()
        {

            DataTable lbData = GetLBpaymentData();
            DataTable report = new DataTable();
            report.Columns.Add(new DataColumn("S No."));
            report.Columns.Add(new DataColumn("Amount"));
            report.Columns.Add(new DataColumn("Currency"));
            report.Columns.Add(new DataColumn("Narrative"));
            report.Columns.Add(new DataColumn("Note"));
            report.Columns.Add(new DataColumn("OrderId"));
            report.Columns.Add(new DataColumn("SearchuCommerceUsing"));
            this.rowCount = 0;
            //DataTable rrr = (from rxOrderId in this.realexData.AsEnumerable() join lbOrderId in lbData.AsEnumerable() on rxOrderId["ORDERID"].ToString() equals lbOrderId["OrderGuid"] into report1 where report1.Count() > 0 select rxOrderId).CopyToDataTable();
            for (int i = 0; i < this.realexData.Rows.Count; i++)
            {
                int flag = 0;
                for (int j = 0; j < lbData.Rows.Count; j++)
                {
                    if ((lbData.Rows[j]["OrderGuid"].ToString().Equals(realexData.Rows[i]["ORDERID"].ToString(), StringComparison.InvariantCultureIgnoreCase)))
                    {
                        double lbAmt, rxAmt;
                        double.TryParse(lbData.Rows[j]["SubTotal"].ToString(), out lbAmt);
                        double.TryParse(realexData.Rows[i]["AMOUNT"].ToString(), out rxAmt);
                        if (lbAmt != rxAmt)
                        {
                            //if(!(lbData.Rows[j]["SubTotal"].ToString().Equals(realexData.Rows[i]["AMOUNT"].ToString())))
                            report.Rows.Add(++this.rowCount, realexData.Rows[i]["AMOUNT"].ToString(), realexData.Rows[i]["CURRENCY"].ToString(), "Amount Mismatch Realex: " + rxAmt + " uCommerce: " + lbAmt, lbData.Rows[j]["Booking Method"], realexData.Rows[i]["ORDERID"].ToString(), lbData.Rows[j]["OrderGuid"].ToString().ToUpper());
                        }
                        if (!(lbData.Rows[j]["Currency"].ToString().Equals(realexData.Rows[i]["CURRENCY"].ToString())))
                        {
                            report.Rows.Add(++this.rowCount, realexData.Rows[i]["AMOUNT"].ToString(), realexData.Rows[i]["CURRENCY"].ToString(), "Currency Mismatch Realex: " + realexData.Rows[i]["CURRENCY"].ToString() + " uCommerce: " + lbData.Rows[j]["Currency"].ToString(), lbData.Rows[j]["Booking Method"], realexData.Rows[i]["ORDERID"].ToString(), lbData.Rows[j]["OrderGuid"].ToString().ToUpper());
                        }
                        if (lbData.Rows[j]["Order Status"].ToString().Equals("Paid") && !(realexData.Rows[i]["STATUS"].ToString().Equals("Batched")))
                        {
                            report.Rows.Add(++this.rowCount, realexData.Rows[i]["AMOUNT"].ToString(), realexData.Rows[i]["CURRENCY"].ToString(), "Status Mismatch Realex: " + realexData.Rows[i]["STATUS"].ToString() + " uCommerce: " + lbData.Rows[j]["Order Status"].ToString(), lbData.Rows[j]["Booking Method"], realexData.Rows[i]["ORDERID"].ToString(), lbData.Rows[j]["OrderGuid"].ToString().ToUpper());
                        }
                        flag = 1;
                    }
                }
                if (flag != 1)
                {
                    report.Rows.Add(++this.rowCount, realexData.Rows[i]["AMOUNT"].ToString(), realexData.Rows[i]["CURRENCY"].ToString(), "Transaction Not in uCommerce", "[empty string]", realexData.Rows[i]["ORDERID"].ToString(), "No uCommerce Reference");
                }
            }
            return report;
        }
    }
}