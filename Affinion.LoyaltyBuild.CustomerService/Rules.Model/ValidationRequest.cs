﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Rules.Model
{
    public class ValidationRequest
    {
        public ValidationRequest()
        {
            RuleName = string.Empty;
            InputToValidate = string.Empty;
        }
        /// <summary>
        /// Validate Rule Set for Against The Client
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// Input That Needs to be validated with Various Logics
        /// </summary>
        public string InputToValidate { get; set; }

        /// <summary>
        /// Application From Which Validation is Requested
        /// </summary>
        public Application RequestingApplication { get; set; }

        /// <summary>
        /// Rule Category Used From the Const List of RuleCategory
        /// </summary>
        public String RuleCategory { get; set; }

        public string RuleName { get; set; }
    }

    //public enum RuleCategory
    //{
    //    /// <summary>
    //    /// Base Rule Set to Check Whether Enabled Or Not
    //    /// </summary>
    //    [Description("partner-validation")]
    //    PartnerValidation,
    //    /// <summary>
    //    /// Base Rule Set to check with the Given Input
    //    /// </summary>
    //    [Description("content")]
    //    Content
    //}

    public enum Application
    {
        /// <summary>
        /// Online Client Portal
        /// </summary>
        Online,
        /// <summary>
        /// Offline Portal
        /// </summary>
        Offline
    }
    /// <summary>
    /// To Specify the Validator and its Required Comparison
    /// </summary>
    public class Validators
    {
        /// <summary>
        /// Category of Rule Set To Validate With List of Rules
        /// </summary>
        public String Category { get; set; }
        /// <summary>
        /// What Validation Needs to be performed
        /// </summary>
        public ValidatorOperator OperatorToPerform { get; set; }
        /// <summary>
        /// To Compare With Check Value
        /// </summary>
        public string CompareWith { get; set; }

        /// <summary>
        /// To Return the Message When the Validation Fails
        /// </summary>
        public string ErrorMessage { get; set; }

        public string RuleName { get; set; }
    }

    public enum ValidatorOperator
    {
        /// <summary>
        /// Equal Operator
        /// </summary>
        Equal,
        /// <summary>
        /// To Validate On Right Side
        /// </summary>
        Right,
        /// <summary>
        /// To Validate on Left Side
        /// </summary>
        Left,
        /// <summary>
        /// To Check Whether Greater Than provided Value
        /// </summary>
        GreaterThan,
        /// <summary>
        /// To Check Whether Greater Than Or Equal To provided Value
        /// </summary>
        GreaterThanEqual,
        /// <summary>
        /// To Check Whether Lesser Than provided Value
        /// </summary>
        LessThan,
        /// <summary>
        /// To Check Whether Lesser Than or Equal to provided Value
        /// </summary>
        LessThanEqual,
        /// <summary>
        /// To check whether the input Contains given Value
        /// </summary>
        Contains,
        /// <summary>
        /// To Check The String Length is Less Than Given value
        /// </summary>
        MaximumLength,
        /// <summary>
        /// To Check The String Length is Max Than Given value
        /// </summary>
        MinimumLength
    }

    public class ValidationResponse
    {
        public ValidationResponse(bool HasPassed)
        {
            IsValidationSuccess = HasPassed;
        }
        public bool IsValidationSuccess { get; set; }
        public string ErrorMessage { get; set; }

        public Dictionary<String, bool> PartnerValidation { get; set; }
    }

    /// <summary>
    /// Rule Category Defined in sitecore/content/#admin-portal#/global/#_supporting-content#/#rule-categories#
    /// Add Rules Whenever new Rules are Defined
    /// </summary>
    public class RuleCategory
    {
        public const string PartnerValidation = "Partner-Validation";
        public const string Content = "Content";
    }
}