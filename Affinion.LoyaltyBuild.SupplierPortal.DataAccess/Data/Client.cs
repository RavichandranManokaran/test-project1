﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.SupplierPortal.DataAccess.Data
{
    public class Client
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}
