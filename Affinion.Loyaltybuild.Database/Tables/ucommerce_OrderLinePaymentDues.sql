﻿CREATE TABLE ucommerce_OrderLinePaymentDues
(
	PaymentDueId BIGINT IDENTITY(1,1),
	OrderLinePaymentId INT,
	OrderLineDueId INT,
	Amount MONEY
)
