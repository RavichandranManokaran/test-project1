﻿CREATE PROCEDURE [dbo].[LB_GetProvisionalBookings]
(
	@datefrom Date=Null,
	@dateto Date=Null,	
	@partner nvarchar(50)=Null,
	@partialpaymentprovisional int = Null, 
	@reportandorderby nvarchar(50)= Null
)
AS 
BEGIN
	--SET NOCOUNT ON
    IF ISNULL(@ReportandOrderby,'') =''
    BEGIN
       SELECT @ReportandOrderby = (ISNULL(@ReportandOrderby,'ArrivalDate'))
    END
	      
	IF ISNULL(@partialpaymentprovisional,'') <> ''
    BEGIN
       SELECT @datefrom = CONVERT(DATE,GETDATE())
	   SELECT @dateto = CONVERT(DATE,DATEADD(DAY,@partialpaymentprovisional,GETDATE()))
    END

	SELECT Distinct BI.BookingNo [BookingReference],
	C.Name [ProviderName],			
	BI.CreatedDate [ReservationDate],
	BI.CheckIndate [ArrivalDate],
	CONCAT(Cus.FirstName,'',CUS.LastName)[CustomerName],
	Cus.MobilePhoneNumber [MobileNumber],
	Cus.PhoneNumber [PhoneNumber],
	OS.Name [BookingStatus],			
	Cus.EmailAddress [Emailid]
	FROM  [dbo].uCommerce_OrderLine OL
	INNER JOIN [dbo].uCommerce_OrderAddress OA ON OA.OrderId=OL.OrderId
	INNER JOIN [dbo].uCommerce_PurchaseOrder PO ON OL.OrderId = PO.OrderId
	INNER JOIN [dbo].LB_BookingInfo BI ON OL.OrderLineId = BI.OrderLineId 
	INNER JOIN [dbo].uCommerce_Product P ON OL.Sku = P.Sku
	INNER JOIN [dbo].uCommerce_CategoryProductRelation CPR ON P.ProductId = CPR.ProductId 
	INNER JOIN [dbo].uCommerce_Category C ON CPR.CategoryId = C.CategoryId
	INNER JOIN [dbo].uCommerce_Customer CUS ON PO.CustomerId = CUS.CustomerId 
	INNER JOIN [dbo].LB_CustomerAttribute CA ON PO.CustomerId = CA.CustomerId 
	INNER JOIN [dbo].LB_OrderLineStatusAudit OLS ON BI.StatusID = OLS.StatusId
	INNER JOIN [dbo].uCommerce_OrderStatus OS ON OLS.StatusId = OS.OrderStatusId
	INNER JOIN [dbo].uCommerce_OrderProperty OP ON OL.OrderLineId = OP.OrderLineId                 
	WHERE 	
	case when @reportandorderby='ArrivalDate' then CONVERT(date,RR.CheckInDate) else CONVERT(date,BI.CreatedDate) end between @datefrom and @dateto
	AND (OS.Name='Provisional' or OS.Name='Partial' AND OS.OrderStatusId=5)	
END
