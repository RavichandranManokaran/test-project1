﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class CampaignItemSubrate : IEntity
    {
        private int? _hashCode;

        // Methods
        public static IQueryable<CampaignItemSubrate> All()
        {
            return GetRepo().Select();
        }

        public virtual void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<CampaignItemSubrate, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            CampaignItemSubrate objA = obj as CampaignItemSubrate;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<CampaignItemSubrate, bool>> expression)
        {
            return All().Any<CampaignItemSubrate>(expression);
        }

        public static IList<CampaignItemSubrate> Find(Expression<Func<CampaignItemSubrate, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<CampaignItemSubrate>();
        }

        public static CampaignItemSubrate FirstOrDefault(Expression<Func<CampaignItemSubrate, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<CampaignItemSubrate>();
        }

        public static CampaignItemSubrate Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<CampaignItemSubrate> GetRepo()
        {
           
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<CampaignItemSubrate>>();
        }

        public virtual void Save()
        {
            try
            {
                GetRepo().Save(this);
            }
            catch (Exception)
            {

            }
        }

        public static CampaignItemSubrate SingleOrDefault(Expression<Func<CampaignItemSubrate, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties

        public virtual int CampaignSubRateId { get; protected set; }

        public virtual int Id
        {
            get
            {
                return this.CampaignSubRateId;
            }
        }

        public virtual CampaignItem CampaignItem { get; set; }

        public virtual SubRate SubRate { get; set; }

        public virtual int ClientId { get; set; }

        public virtual decimal Price { get; set; }

        public virtual bool IsPercentageAmount { get; set; }

    }
}
