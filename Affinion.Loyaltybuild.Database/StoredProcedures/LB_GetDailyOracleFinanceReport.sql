CREATE PROCEDURE [dbo].[LB_GetDailyOracleFinanceReport]
@StartDate DATETIME=NULL
AS
BEGIN
			DECLARE @DailyOraceFinanceReport TABLE
			(
			 OrderLineId INT,
			 Currency VARCHAR(100),
			 Company VARCHAR(100),
			 MerchantIdentificationNo BIGINT,
			 ClientName VARCHAR(100),
			 Department VARCHAR(100),
			 ProductCode VARCHAR(100),
			 GlCode VARCHAR(100),
			 ClientCode VARCHAR(100),
			 Project VARCHAR(100),
			 Spare VARCHAR(100),
			 AmountCredit DECIMAL,
			 AmountDebit DECIMAL,
			 Description VARCHAR(100),
			 SequenceNo INT
			)

            INSERT INTO @DailyOraceFinanceReport  
			SELECT 
			        OL.OrderLineId,
                    C.ISOCode AS CurrencyName,
					FC.Company,
					FC.MerchantIdentificationNo,
					FC.ClientName,
					FC.Department,
					FC.ProductCode,
					FC.GlCode,
					FC.ClientCode,
					'00000' AS Project,
					'0000' AS Spare,
					OLP.Amount AS AmountCredit,
					0 AS AmountDebit,
					FC.Description,
					'1'					
				FROM [dbo].uCommerce_OrderLine OL 
				INNER JOIN [dbo].LB_OrderLinePayment OLP ON OL.OrderLineId =OLP.OrderLineId
				INNER JOIN [dbo].LB_DueTypes DT ON OLP.DueTypeId =DT.DueTypeId AND OLP.DueTypeId IS NOT NULL
				INNER JOIN [dbo].uCommerce_Currency C ON OLP.CurrencyTypeId=C.CurrencyId
				INNER JOIN [dbo].uCommerce_FinanceCode FC ON OL.OrderLineId=FC.OrderLineId
				WHERE 
				   CAST(OL.CreatedOn AS DATE) = CAST(@StartDate AS DATE) --AND
				   --DT.DueTypeId IN (1,2,3,4,5,8)

		    INSERT INTO @DailyOraceFinanceReport  
			SELECT 
			        OL.OrderLineId,
                    C.ISOCode AS CurrencyName,
					FC.Company,
					FC.MerchantIdentificationNo,
					FC.ClientName,
					'0000' AS Department,
					'0000' AS ProductCode,
					FC.GlCode,
					'000000' AS ClientCode,
					'00000' AS Project,
					'0000' AS Spare,
					0 AS AmountCredit,
					SUM(OLP.Amount) AS AmountDebit,
					FC.Description,
					'2'					
				FROM [dbo].uCommerce_OrderLine OL 
				INNER JOIN [dbo].LB_OrderLinePayment OLP ON OL.OrderLineId =OLP.OrderLineId
				INNER JOIN [dbo].LB_DueTypes DT ON OLP.DueTypeId =DT.DueTypeId AND OLP.DueTypeId IS NOT NULL
				INNER JOIN [dbo].uCommerce_Currency C ON OLP.CurrencyTypeId=C.CurrencyId
				INNER JOIN [dbo].uCommerce_FinanceCode FC ON OL.OrderLineId=FC.OrderLineId
				WHERE 
				   CAST(OL.CreatedOn AS DATE) = CAST(@StartDate AS DATE) --AND
				   --DT.DueTypeId IN (6,7)
				GROUP BY 
                    C.ISOCode,
					FC.Company,
					FC.MerchantIdentificationNo,
					FC.ClientName,FC.GlCode,FC.Description,OL.OrderLineId

				SELECT * FROM @DailyOraceFinanceReport order by OrderLineId,SequenceNo ASC 
END  
