﻿
--********************************************************************************************************************************************************************************
--CREATE TABLE SCRIPTS For uCommerce_FreeNightAward
--********************************************************************************************************************************************************************************
CREATE TABLE [dbo].[uCommerce_FreeNightAward](
	[FreeNightAwardId] [int] NOT NULL,
	[NoOfNight] [int] NOT NULL,
 CONSTRAINT [PK_uCommerce_FreeNightAward] PRIMARY KEY CLUSTERED 
(
	[FreeNightAwardId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[uCommerce_FreeNightAward]  WITH CHECK ADD  CONSTRAINT [FK_uCommerce_FreeNightAward_uCommerce_Award] FOREIGN KEY([FreeNightAwardId])
REFERENCES [dbo].[uCommerce_Award] ([AwardId])
GO

ALTER TABLE [dbo].[uCommerce_FreeNightAward] CHECK CONSTRAINT [FK_uCommerce_FreeNightAward_uCommerce_Award]
GO

--********************************************************************************************************************************************************************************
--CREATE TABLE SCRIPTS For uCommerce_OrdelinePaymentRequest
--********************************************************************************************************************************************************************************
CREATE TABLE [dbo].[uCommerce_OrdelinePaymentRequest](
	[OrdelinePaymentRequestID] [int] IDENTITY(1,1) NOT NULL,
	[OrderLinePaymentID] [int] NOT NULL,
	[ISProviderPaymentRequested] [bit] NULL,
	[DateProviderPaymentRequested] [datetime] NULL,
	[SuggestedProviderPaymentAmount] [money] NULL,
	[ActualProviderPaymentAmount] [money] NULL,
	[ProviderPaymentRequestedBy] [nvarchar](50) NULL,
	[DateProviderPaid] [datetime] NULL,
	[IsProviderPaid] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[OrdelinePaymentRequestID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[uCommerce_OrdelinePaymentRequest]  WITH CHECK ADD  CONSTRAINT [fk_OrderLinePaymentID] FOREIGN KEY([OrderLinePaymentID])
REFERENCES [dbo].[uCommerce_OrderLinePayment] ([OrderLinePaymentID])
GO

ALTER TABLE [dbo].[uCommerce_OrdelinePaymentRequest] CHECK CONSTRAINT [fk_OrderLinePaymentID]
GO

--********************************************************************************************************************************************************************************
--CREATE TABLE SCRIPTS For uCommerce_SeasonFixPriceAward
--********************************************************************************************************************************************************************************

CREATE TABLE [dbo].[uCommerce_SeasonFixPriceAward](
	[SeasonFixPriceAwardId] [int] NOT NULL,
	[Content] [varchar](50) NULL,
 CONSTRAINT [PK_uCommerce_SeasonFixPriceAward] PRIMARY KEY CLUSTERED 
(
	[SeasonFixPriceAwardId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[uCommerce_SeasonFixPriceAward]  WITH CHECK ADD  CONSTRAINT [FK_uCommerce_SeasonFixPriceAward_uCommerce_Award] FOREIGN KEY([SeasonFixPriceAwardId])
REFERENCES [dbo].[uCommerce_Award] ([AwardId])
GO

ALTER TABLE [dbo].[uCommerce_SeasonFixPriceAward] CHECK CONSTRAINT [FK_uCommerce_SeasonFixPriceAward_uCommerce_Award]
GO

--********************************************************************************************************************************************************************************
--CREATE TABLE SCRIPTS For uCommerce_SeasonFixPriceAwardDetail
--********************************************************************************************************************************************************************************

CREATE TABLE [dbo].[uCommerce_SeasonFixPriceAwardDetail](
	[SeasonFixPriceAwardDetailId] [int] IDENTITY(1,1) NOT NULL,
	[SeasonFixPriceAwardId] [int] NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[AmountOff] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK_uCommerce_SeasonFixPriceAwardDetail] PRIMARY KEY CLUSTERED 
(
	[SeasonFixPriceAwardDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[uCommerce_SeasonFixPriceAwardDetail]  WITH CHECK ADD  CONSTRAINT [FK_uCommerce_SeasonFixPriceAwardDetail_uCommerce_SeasonFixPriceAward] FOREIGN KEY([SeasonFixPriceAwardId])
REFERENCES [dbo].[uCommerce_SeasonFixPriceAward] ([SeasonFixPriceAwardId])
GO

ALTER TABLE [dbo].[uCommerce_SeasonFixPriceAwardDetail] CHECK CONSTRAINT [FK_uCommerce_SeasonFixPriceAwardDetail_uCommerce_SeasonFixPriceAward]
GO

--********************************************************************************************************************************************************************************
--CREATE TABLE SCRIPTS For uCommerce_SeasonUnitDiscountAward
--********************************************************************************************************************************************************************************

CREATE TABLE [dbo].[uCommerce_SeasonUnitDiscountAward](
	[SeasonUnitDiscountAwardId] [int] NOT NULL,
	[Content] [varchar](50) NULL,
 CONSTRAINT [PK_uCommerce_SeasonUnitDiscountAward] PRIMARY KEY CLUSTERED 
(
	[SeasonUnitDiscountAwardId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[uCommerce_SeasonUnitDiscountAward]  WITH CHECK ADD  CONSTRAINT [FK_uCommerce_SeasonUnitDiscountAward_uCommerce_Award] FOREIGN KEY([SeasonUnitDiscountAwardId])
REFERENCES [dbo].[uCommerce_Award] ([AwardId])
GO

ALTER TABLE [dbo].[uCommerce_SeasonUnitDiscountAward] CHECK CONSTRAINT [FK_uCommerce_SeasonUnitDiscountAward_uCommerce_Award]
GO

--********************************************************************************************************************************************************************************
--CREATE TABLE SCRIPTS For uCommerce_SeasonUnitDiscountAwardDetail
--********************************************************************************************************************************************************************************

CREATE TABLE [dbo].[uCommerce_SeasonUnitDiscountAwardDetail](
	[SeasonUnitDiscountAwardDetailId] [int] IDENTITY(1,1) NOT NULL,
	[SeasonUnitDiscountAwardId] [int] NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[AmountOff] [decimal](18, 0) NOT NULL,
	[IsPercentage] [bit] NOT NULL,
	[OnOrderLine] [bit] NOT NULL,
 CONSTRAINT [PK_uCommerce_SeasonUnitDiscountAwardDetail] PRIMARY KEY CLUSTERED 
(
	[SeasonUnitDiscountAwardDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[uCommerce_SeasonUnitDiscountAwardDetail]  WITH CHECK ADD  CONSTRAINT [FK_uCommerce_SeasonUnitDiscountAwardDetail_uCommerce_SeasonUnitDiscountAward] FOREIGN KEY([SeasonUnitDiscountAwardId])
REFERENCES [dbo].[uCommerce_SeasonUnitDiscountAward] ([SeasonUnitDiscountAwardId])
GO

ALTER TABLE [dbo].[uCommerce_SeasonUnitDiscountAwardDetail] CHECK CONSTRAINT [FK_uCommerce_SeasonUnitDiscountAwardDetail_uCommerce_SeasonUnitDiscountAward]
GO

 --********************************************************************************************************************************************************************************
--Alter Table Scripts for uCommerce_RoomReservation
--********************************************************************************************************************************************************************************

  ALTER TABLE [uCommerce_RoomReservation] ADD
  CustomerReference VARCHAR(50)

  Go
  
--********************************************************************************************************************************************************************************
--INSERT SCRIPTS For uCommerce_DataType, uCommerce_DefinitionField, uCommerce_DefinitionFieldDescription
--********************************************************************************************************************************************************************************

declare @datatypeid int = 0
declare @fieldid int = 0

select @datatypeid = DataTypeId from uCommerce_DataType where DefinitionName= 'SiteCorePaymentMethod'
if(@datatypeid = 0)
begin
	insert into uCommerce_DataType (TypeName,Nullable,ValidationExpression,BuiltIn,DefinitionName,Deleted,Guid,CreatedOn,
	CreatedBy,ModifiedOn,ModifiedBy)
	values ('PaymentMethod',1,'',0,'SiteCorePaymentMethod',0,NEWID(),GETDATE(),'admin',GETDATE(),'admin')
	
	set @datatypeid=@@identity 
	
	insert into uCommerce_DefinitionField(DataTypeId,DefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,BuiltIn,DefaultValue,Guid)
	values (@datatypeid , 2,'PaymentMethod',1,0,1,0,0,0,0,'',NEWID())
	
	set @fieldid=@@identity 
	
	insert into [uCommerce_DefinitionFieldDescription] ([CultureCode],[DisplayName],[Description],[DefinitionFieldId])
	values ('en','PaymentMethod','',@fieldid)
 END
 go

--********************************************************************************************************************************************************************************
--INSERT SCRIPTS For ucommerce_entityui, uCommerce_EntityUiDescription
--********************************************************************************************************************************************************************************
  declare @id int
insert into ucommerce_entityui values('Affinion.LoyaltyBuild.UCom.MasterClass.Entities.SeasonFixPriceAward, Affinion.LoyaltyBuild.UCom.MasterClass.Entities','Awards/SeasonFixPriceAwardUi.ascx',1)
set @id = @@identity
insert into uCommerce_EntityUiDescription values(@id,'Season FixPrice Award','Season FixPrice Award','en')

insert into ucommerce_entityui values('Affinion.LoyaltyBuild.UCom.MasterClass.Entities.SeasonUnitDiscountAward, Affinion.LoyaltyBuild.UCom.MasterClass.Entities','Awards/SeasonUnitDiscountAwardUi.ascx',1)
set @id = @@identity
insert into uCommerce_EntityUiDescription values(@id,'Season Unit Discount Award','Season Unit Discount Award','en')

insert into ucommerce_entityui values('Affinion.LoyaltyBuild.UCom.MasterClass.Entities.FreeNightAward, Affinion.LoyaltyBuild.UCom.MasterClass.Entities','Awards/FreeNightAwardUi.ascx',1)
set @id = @@identity
insert into uCommerce_EntityUiDescription values(@id,'Free Night(s) Discount','Free Night(s) Discount','en')

GO

--********************************************************************************************************************************************************************************
--Stored Procedure for affinion_GetFinanceOverallRevenueReport
--********************************************************************************************************************************************************************************
 IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[affinion_GetFinanceOverallRevenueReport]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[affinion_GetFinanceOverallRevenueReport]
GO 

 CREATE PROCEDURE [dbo].[affinion_GetFinanceOverallRevenueReport]
@StartDate DATETIME=NULL,
@EndDate DATETIME=NULL,
@BookingMethodID INT=0,
@SubTypeReportID INT=0
AS
BEGIN
 
 DECLARE @bookingThrough VARCHAR(20)

 DECLARE @BookingThroguh table
(
    BookingMethod varchar(100)
)

DELETE FROM @BookingThroguh
  
 IF(@BookingMethodID=1)
 BEGIN
	  INSERT INTO @BookingThroguh VALUES('Online')
 END
 ELSE IF(@BookingMethodID=2) 
 BEGIN 
	  INSERT INTO @BookingThroguh VALUES('Offline')
 END 
 ELSE IF(@BookingMethodID=3)
 BEGIN 
	  INSERT INTO @BookingThroguh VALUES('Online')
	  INSERT INTO @BookingThroguh VALUES('Offline')
 END  
	  
 IF(@SubTypeReportID=1)
 BEGIN
      
	   SELECT 
                    OL.CreatedOn,
					COUNT(OL.CreatedOn) AS 'Bookings',
					OL.ProductName AS ClientOffer,
					CA.ClientId AS ClientID,
					C.ISOCode AS CurrencyName,
					SUM(OL.Total)+SUM(OLS.Amount)  AS TotalPrice,
					OL.VAT,
					SUM(OL.Total) AS LoyaltyBuildRevenue	
				FROM uCommerce_roomreservation RS
					INNER JOIN uCommerce_OrderLine OL on RS.OrderLineId=OL.OrderLineId
					INNER JOIN uCommerce_OrderLineSubrates OLS ON OL.OrderLineID=OLS.OrderLineID
					INNER JOIN uCommerce_OrderLinePayment OLP ON OLS.OrderLineID=OLP.OrderLineID
					INNER JOIN uCommerce_Currency C ON OLP.CurrencyTypeId=C.CurrencyId
					INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId=PO.OrderId AND PO.CustomerId IS NOT NULL
					INNER JOIN uCommerce_Customer CU ON PO.CustomerId=CU.CustomerId
					INNER JOIN uCommerce_CustomerAttribute CA ON CU.CustomerId=CA.CustomerId
				WHERE 
				   RS.BookingThrough IS NOT NULL AND
				   RS.BookingThrough IN (SELECT BookingMethod FROM @BookingThroguh) AND
				   OLS.SubrateItemCode='CCPROFEE' AND
				   CAST(OL.CreatedOn AS DATE) >= CAST(@StartDate AS DATE) AND  CAST(OL.CreatedOn AS DATE) <= CAST(@EndDate AS DATE) AND
				   OL.VAT IS NOT NULL
				GROUP BY 
				   OL.ProductName,C.ISOCode,CA.ClientId,OL.VAT,OL.CreatedOn ORDER BY OL.CreatedOn DESC 
       
 END
 ELSE IF(@SubTypeReportID=2)
 BEGIN 
      SELECT 
                    OL.CreatedOn,
					COUNT(OL.CreatedOn) AS 'Bookings',
					OL.ProductName AS ClientOffer,
					CA.ClientId AS ClientID,
					C.ISOCode AS CurrencyName,
					SUM(OL.Total) AS TotalPrice,
					OL.VAT,
					SUM(OL.Total) AS LoyaltyBuildRevenue	
				FROM uCommerce_roomreservation RS
					INNER JOIN uCommerce_OrderLine OL on RS.OrderLineId=OL.OrderLineId
					INNER JOIN uCommerce_OrderLineSubrates OLS ON OL.OrderLineID=OLS.OrderLineID
					INNER JOIN uCommerce_OrderLinePayment OLP ON OLS.OrderLineID=OLP.OrderLineID
					INNER JOIN uCommerce_Currency C ON OLP.CurrencyTypeId=C.CurrencyId
					INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId=PO.OrderId AND PO.CustomerId IS NOT NULL
					INNER JOIN uCommerce_Customer CU ON PO.CustomerId=CU.CustomerId
					INNER JOIN uCommerce_CustomerAttribute CA ON CU.CustomerId=CA.CustomerId
				WHERE 
				   RS.BookingThrough IS NOT NULL AND
				   RS.BookingThrough IN (SELECT BookingMethod FROM @BookingThroguh) AND
				   OLS.SubrateItemCode <> 'CCPROFEE' AND
				   CAST(OL.CreatedOn AS DATE) >= CAST(@StartDate AS DATE) AND  CAST(OL.CreatedOn AS DATE) <= CAST(@EndDate AS DATE) AND
				   OL.VAT IS NOT NULL
				 GROUP BY 
				   OL.ProductName,C.ISOCode,CA.ClientId,OL.VAT,OL.CreatedOn ORDER BY OL.CreatedOn DESC 
 END 
 
END  
	 
GO

--********************************************************************************************************************************************************************************
--Stored Procedure for affinion_ProcessOLDueAndOLPayments
--********************************************************************************************************************************************************************************
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[affinion_ProcessOLDueAndOLPayments]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[affinion_ProcessOLDueAndOLPayments]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
/*
*/ 
CREATE PROCEDURE [dbo].[affinion_ProcessOLDueAndOLPayments]
(	 
	@OrderId INT=NULL,
	@IsPartialPayment BIT = 1	 
)
AS 
BEGIN
	IF (@OrderId IS NOT NULL)
	BEGIN	
		--Inserting data into ucommerce_orderlinedue based ON 'LBCOM'					
		INSERT INTO ucommerce_orderlinedue 
		(
			OrderLineID, 
			DueTypeId, 
			CurrencyTypeId, 
			Amount, 
			CommissionAmount, 
			TransferAmount,
			ClientAmount, 
			ProviderAmount, 
			CollectionByProviderAmount,
			Note, 
			OrderLinePaymentID, 
			IsDeleted, 
			CreatedBy, 
			CreatedDate, 
			UpdatedBy, 
			UpdatedDate,
			CancellationAmount, SubrateItemCode
		)
		SELECT 
			OLS.OrderLineID AS [OrderLineID],
			DT.DueTypeId AS [DueTypeId], 
			PO.CurrencyId AS [CurrencyTypeId],
			OLS.Amount-ISNULL(OLS.Discount,0) AS [Amount],
			0 AS [CommissionAmount],
			0 AS [TransferAmount],
			0 AS [ClientAmount],
			0 AS [ProviderAmount],
			0 AS [CollectionByProviderAmount],
			'' AS [Note],
			NULL AS [OrderLinePaymentID],
			0 AS [IsDeleted],
			OL.CreatedBy AS [CreatedBy],
			OL.CreatedOn AS [CreatedDate],
			'' AS [UpdatedBy],
			GETDATE() AS [UpdatedDate], 
			0 AS [CancellationAmount],
			OLS.SubrateItemCode AS [SubrateItemCode]
		FROM 
			uCommerce_OrderLineSubrates OLS
			INNER JOIN ucommerce_duetypes DT ON ols.SubrateItemCode = DT.SubrateItemCode  
			INNER JOIN ucommerce_orderline OL ON ol.OrderLineId = OLS.OrderLineID		
			INNER JOIN uCommerce_PurchaseOrder PO ON po.OrderId = OL.OrderId
		WHERE 
			OLS.SubrateItemCode = 'LBCOM' 
			AND Po.OrderId  = @OrderId

		--this is a full payment (param name need to be reverted
		
		BEGIN
			INSERT INTO ucommerce_orderlinedue 
			(
				OrderLineID, 
				DueTypeId, 
				CurrencyTypeId, 
				Amount, 
				CommissionAmount, 
				TransferAmount,
				ClientAmount, 
				ProviderAmount, 
				CollectionByProviderAmount,
				Note, 
				OrderLinePaymentID, 
				IsDeleted, 
				CreatedBy, 
				CreatedDate, 
				UpdatedBy, 
				UpdatedDate,
				CancellationAmount, SubrateItemCode
			)
			SELECT 
				OL.OrderLineID AS [OrderLineID],
				1 AS [DueTypeId], 
				PO.CurrencyId AS [CurrencyTypeId],
				OL.Total-OLS.Amount+ISNULL(OLS.Discount,0)  AS [Amount],
				0 AS [CommissionAmount],
				0 AS [TransferAmount],
				0 AS [ClientAmount],
				0 AS [ProviderAmount],
				0 AS [CollectionByProviderAmount],
				'' AS [Note],
				NULL AS [OrderLinePaymentID],
				0 AS [IsDeleted],
				OL.CreatedBy AS [CreatedBy],
				OL.CreatedOn AS [CreatedDate],
				'' AS [UpdatedBy],
				GETDATE() AS [UpdatedDate], 
				0 AS [CancellationAmount],
				'' AS [SubrateItemCode]
			FROM 
				uCommerce_OrderLineSubrates OLS
				INNER JOIN ucommerce_duetypes DT ON ols.SubrateItemCode = DT.SubrateItemCode  
				INNER JOIN ucommerce_orderline OL ON ol.OrderLineId = OLS.OrderLineID		
				INNER JOIN uCommerce_PurchaseOrder PO ON po.OrderId = OL.OrderId
				INNER JOIN uCommerce_OrderProperty OP ON OL.OrderLineId=OP.OrderLineId
			WHERE 
				OLS.SubrateItemCode = 'LBCOM' 
				AND Po.OrderId  = @OrderId
				AND OP.[Key]='PaymentMethod'
				AND OP.Value IN ('Full','Partial')
		END

		--Inserting data into ucommerce_orderlinedue based ON 'CCPROFEE'
		INSERT INTO ucommerce_orderlinedue 
		(
			OrderLineID, 
			DueTypeId, 
			CurrencyTypeId, 
			Amount, 
			CommissionAmount, 
			TransferAmount,
			ClientAmount, 
			ProviderAmount, 
			CollectionByProviderAmount,
			Note, 
			OrderLinePaymentID, 
			IsDeleted, 
			CreatedBy, 
			CreatedDate, 
			UpdatedBy, 
			UpdatedDate,
			CancellationAmount, SubrateItemCode
		)
		SELECT TOP 1 
			OLS.OrderLineID AS [OrderLineID],
			DT.DueTypeId AS [DueTypeId], 
			PO.CurrencyId AS [CurrencyTypeId],
			OLS.Amount AS [Amount],
			0 AS [CommissionAmount],
			0 AS [TransferAmount],
			0 AS [ClientAmount],
			0 AS [ProviderAmount],
			0 AS [CollectionByProviderAmount],
			'' AS [Note],
			NULL AS [OrderLinePaymentID],
			0 AS [IsDeleted],
			OL.CreatedBy AS [CreatedBy],
			OL.CreatedOn AS [CreatedDate],
			'' AS [UpdatedBy],
			GETDATE() AS [UpdatedDate], 
			0 AS [CancellationAmount],
			OLS.SubrateItemCode AS [SubrateItemCode]
		FROM 
			uCommerce_OrderLineSubrates OLS
			INNER JOIN ucommerce_duetypes DT ON ols.SubrateItemCode = DT.SubrateItemCode  
			INNER JOIN ucommerce_orderline OL ON ol.OrderLineId = OLS.OrderLineID		
			INNER JOIN uCommerce_PurchaseOrder PO ON po.OrderId = OL.OrderId
		WHERE  
			OLS.SubrateItemCode = 'CCPROFEE'  AND 
			OLS.OrderLineID in (Select orderlineid from ucommerce_orderline where orderid = @OrderId)
		ORDER BY ols.Amount DESC
		
		--Inserting data into ucommerce_orderlinepayment 
		INSERT INTO ucommerce_orderlinepayment
		(
			OrderLineID, 
			CurrencyTypeId, 
			PaymentMethodID, 
			Amount, 
			OrderPaymentID, 
			CreatedDate, 
			ReferenceID, 
			CreatedBy,
			DueTypeId
		)
		SELECT 
			ol.orderlineid AS [OrderLineID], 
			po.CurrencyId AS [CurrencyTypeId],
			p.PaymentMethodId AS [PaymentMethodID],
			CASE WHEN DueTypeId=1 THEN OP.Value
			ELSE old.Amount 
			END AS [Amount],
			p.PaymentId AS [OrderPaymentID],
			p.Created AS [CreatedDate],
			convert(nvarchar(100), po.OrderGuid) as [ReferenceID],
			--p.ReferenceId AS [ReferenceID], 
			ol.CreatedBy  AS [CreatedBy],
			old.DueTypeId
		FROM  uCommerce_OrderLineDue old 
			INNER JOIN ucommerce_orderline ol ON old.orderlineid = ol.orderlineid 			
			INNER JOIN uCommerce_PurchaseOrder po ON po.OrderId = ol.OrderId
			INNER JOIN ucommerce_payment p ON p.orderid = po.orderid
			LEFT JOIN uCommerce_OrderProperty OP ON ol.OrderLineId = OP.OrderLineId 
		WHERE
			PO.orderid = @OrderId
			AND OP.[Key]='ProviderAmountPaidCheckOut'
			

		UPDATE oldue SET oldue.PaymentMethodId = olp.PaymentMethodID
		FROM ucommerce_orderlinedue oldue, ucommerce_orderlinepayment olp
		WHERE oldue.orderlineid = olp.OrderLineID 
			AND oldue.OrderLineID in (Select orderlineid from ucommerce_orderline where orderid = @OrderId)
			AND oldue.OrderLinePaymentID is null

		--Orderline status date update
		declare @orderlineids table(orderlineid int, isupdated bit)
		insert into @orderlineids select orderlineid, 0 from uCommerce_OrderLine where orderid = @OrderId
		declare @currentorderlineid int
		while(select COUNT(1) from @orderlineids where isupdated = 0) > 0
		begin
			select top 1 @currentorderlineid = orderlineid from @orderlineids where isupdated = 0
			--call update status sp here
			exec [affinion_AddBookingStatus] @currentorderlineid, null, ''
		update @orderlineids set isupdated = 1 where orderlineid = @currentorderlineid
		end

		--update entry for special request in reservation notes
		insert into uCommerce_ReservationNotes
			select 3 as NoteTypeId, rr.OrderLineId, rr.SpecialRequest, getdate() as CreatedDate, '' as CreatedBy from uCommerce_RoomReservation rr
			inner join ucommerce_orderline ol on ol.orderlineid = rr.orderlineid
			inner join ucommerce_purchaseorder po on po.orderid = ol.OrderId 
			where po.orderid = @OrderId
			and ISNULL(rr.specialrequest,'') <> '' 
			and not exists (select 1 from uCommerce_ReservationNotes where OrderLineId = rr.OrderLineId and notetypeid = 3)
	END
END

GO

--********************************************************************************************************************************************************************************
--Stored Procedure for affinion_TransferBooking
--********************************************************************************************************************************************************************************

/****** Object:  StoredProcedure [dbo].[[affinion_TransferBooking]]    Script Date: 2/12/2016 3:06:06 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[affinion_TransferBooking]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[affinion_TransferBooking]
GO

/****** Object:  StoredProcedure [dbo].[[affinion_TransferBooking]]    Script Date: 2/12/2016 3:06:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[affinion_TransferBooking] 
	-- Add the parameters for the stored procedure here
	@Note NVARCHAR(MAX), 
	@OrderLineId INT,
	@NoOfAdults INT,
	@NoOfChildren INT,
	@ChildrenAgeInfo VARCHAR(50),
	@CartId VARCHAR(50),
	@CreatedBy VARCHAR(50),
	@NewOrderLineId INT OUT,
	@OrderReference VARCHAR(50)
AS
BEGIN
	
	--DECLARE @OrderReference VARCHAR(50)
	DECLARE @NewOrderId INT
	DECLARE @OrderId INT
	DECLARE @CustomerId INT

	SELECT @OrderId = PO.OrderId, @CustomerId = PO.CustomerId 
	FROM uCommerce_OrderLine OL
		INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId = PO.OrderId
	WHERE OrderLineId = @OrderLineId

	--get order reference
	--SELECT @OrderReference = ISNULL(BookingReferenceId,'') + '-' + CAST((ISNULL(LastSequence,0)+1) AS VARCHAR(5))
	--	FROM uCommerce_CustomerAttribute
	--	WHERE CustomerId = @CustomerId
	--UPDATE uCommerce_CustomerAttribute SET LastSequence = (ISNULL(LastSequence,0)+1) WHERE CustomerId = @CustomerId

	SELECT TOP 1  
		@NewOrderLineId =  RR.OrderLineId,
		@NewOrderId = PO.OrderId
	FROM
		uCommerce_RoomReservation RR 
		INNER JOIN uCommerce_OrderLine OL ON RR.OrderLineId = OL.OrderLineId
		INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId = PO.OrderId
	WHERE PO.BasketId = @CartId
	
	--Update no of adults and child and order reference
	UPDATE uCommerce_RoomReservation SET OrderReference = @OrderReference, NoOfAdult = @NoOfAdults, NoOfChild = @NoOfChildren WHERE OrderLineId = @NewOrderLineId


	-- Insert statements for procedure here	 
	INSERT INTO uCommerce_ReservationNotes 
		(
		NoteTypeId,
		OrderLineId,
		Note,
		CreatedBy,
		CreatedDate
		) 
	VALUES 
		(
		1,
		@OrderLineId,
		@Note,
		@CreatedBy,
		GETDATE()
		);

	--Update Customer
	UPDATE uCommerce_PurchaseOrder SET CustomerId = @CustomerId WHERE OrderId = @NewOrderId

	--insert order address
	INSERT INTO [dbo].[uCommerce_OrderAddress]
		([FirstName]
		,[LastName]
		,[EmailAddress]
		,[PhoneNumber]
		,[MobilePhoneNumber]
		,[Line1]
		,[Line2]
		,[PostalCode]
		,[City]
		,[State]
		,[CountryId]
		,[Attention]
		,[CompanyName]
		,[AddressName]
		,[OrderId]
		,[Line3]
		,[County]
		,[Branch]
		,[IsEmail]
		,[IsTerms])
	SELECT [FirstName]
		,[LastName]
		,[EmailAddress]
		,[PhoneNumber]
		,[MobilePhoneNumber]
		,[Line1]
		,[Line2]
		,[PostalCode]
		,[City]
		,[State]
		,[CountryId]
		,[Attention]
		,[CompanyName]
		,[AddressName]
		,@NewOrderId
		,[Line3]
		,[County]
		,[Branch]
		,[IsEmail]
		,[IsTerms]
	FROM [dbo].[uCommerce_OrderAddress]
	WHERE OrderId = @OrderId

			
	UPDATE uCommerce_RoomReservation 
	SET isCancelled = 1, NewOrderReference = @OrderReference 
	WHERE OrderLineId = @OrderlineId


	IF(@ChildrenAgeInfo IS NOT NULL and @ChildrenAgeInfo <> '')
	BEGIN
		DECLARE @xml as xml
		SET @xml = cast(('<X>'+replace(@ChildrenAgeInfo,',' ,'</X><X>')+'</X>') as xml)
		
		INSERT INTO [dbo].[uCommerce_RoomReservationAgeInfo] ([RoomReservationId], [OrderLineId], [Age])
		SELECT RR.RoomReservationId, RR.OrderLineId, N.value('.', 'varchar(10)') as value FROM @xml.nodes('X') as T(n)
		CROSS JOIN uCommerce_RoomReservation RR
		WHERE RR.OrderLineId = @NewOrderLineId
	END

	--Cancelling for existing OrderLineId
	EXEC [affinion_UpdateRoomAvailability] @OrderLineId, 1

	--Adding for new OrderLineId
	EXEC [affinion_UpdateRoomAvailability] @NewOrderLineId, 0

	UPDATE uCommerce_PurchaseOrder
	SET BasketId = '00000000-0000-0000-0000-000000000000'
	WHERE BasketId = @CartId
END

GO

--********************************************************************************************************************************************************************************
--Stored Procedure for GetExistingBookings
--********************************************************************************************************************************************************************************

/*<summary>
Description: Stored procedure to retrieve Existing Booking details for the given search criteria
<param name="@clientIds">Client Id or ',' seperated Id list</param>
<param name="@firstname">Customer First Name</param>
<param name="@lastname">Customer Last Name</param>
<param name="@providerName">Hotel Name</param>
<param name="@reservationDate">Booking Created Date</param>
<param name="@arrivalDate">Customer Arrival Date</param>
<param name="@reservationDateFrom">Customer Arrival Date From date</param>
<param name="@reservationDateTo">Customer Arrival Date To date</param>
<param name="@bookingReference">Customer Booking reference Id</param>
<param name="@bookingMethod">Booked through Online or Offline</param>
<param name="@orderStatusId">Current Order Status Id</param>
</summary>*/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetExistingBookings]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[GetExistingBookings]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetExistingBookings]

( @clientIds nvarchar(512), 
	   @firstname nvarchar(512)=NULL, 
       @lastname nvarchar(512)=NULL, 
       @providerName nvarchar(50)=NULL, 
	   @reservationDate Date=NULL, 
       @arrivalDate Date=NULL,
	   @reservationDateFrom Date=NULL, 
       @reservationDateTo Date=NULL, 
       @bookingReference nvarchar(50)=NULL,
	   	   @EmailValidation varchar(50)=NULL,
	   @NumericValidation varchar(50)=NULL,
	   @bookingMethod varchar(50)=NULL,
	   	   @orderStatusId int=NULL)
		   AS 
		   		   	   /*Client Ids are Mandatory*/
					   /* Select the bookings which are associated with Offers */
SELECT OL.OrderLineId,
OL.OrderId,
PO.OrderGuid,
CI.Name as Campaign, 
CONCAT(CUS.FirstName, ' ', CUS.LastName) as FullName , 
RR.OrderReference as BookingReferenceId,
CONCAT( Replace(Convert(varchar(11), RR.CheckInDate,103),' ','/'), '   ', Replace(Convert(varchar(11), RR.CheckOutDate,103),' ','/')) as CheckInCheckOutDates, 
RR.NoOfAdult, 
RR.NoOfChild, 
CONVERT(varchar(20),OL.CreatedOn,103) as CreatedOn,
RR.BookingThrough, 
OS.Name as BookingStatus, 
CONCAT(C.Name,' ', P.Name) as Accommodation
FROM [dbo].uCommerce_OrderLine OL 
INNER JOIN [dbo].uCommerce_PurchaseOrder PO ON OL.OrderId = PO.OrderId
INNER JOIN [dbo].uCommerce_RoomReservation RR ON OL.OrderLineId = RR.OrderLineId 
INNER JOIN [dbo].uCommerce_Product P ON OL.Sku = P.Sku
INNER JOIN [dbo].uCommerce_CategoryProductRelation CPR ON P.ProductId = CPR.ProductId 
INNER JOIN [dbo].uCommerce_Category C ON CPR.CategoryId = C.CategoryId
INNER JOIN [dbo].uCommerce_Customer CUS ON PO.CustomerId = CUS.CustomerId 
INNER JOIN [dbo].uCommerce_CustomerAttribute CA ON PO.CustomerId = CA.CustomerId 
INNER JOIN [dbo].uCommerce_OrderLineStatus OLS ON RR.OrderLineStatusID = OLS.OrderLineStatusId
INNER JOIN [dbo].uCommerce_OrderStatus OS ON OLS.StatusId = OS.OrderStatusId
INNER JOIN [dbo].uCommerce_OrderProperty OP ON OL.OrderLineId = OP.OrderLineId
INNER JOIN [dbo].uCommerce_CampaignItem CI ON CI.CampaignItemId = Convert(INT,Value)
WHERE (@firstname IS NULL OR CUS.FirstName Like @firstname + '%')
AND (@lastname IS NULL OR CUS.LastName Like @lastname + '%') 
AND CHARINDEX(CA.ClientId, @clientIds) > 0 
AND (@providerName IS NULL OR C.Name Like @providerName + '%')
AND ((@ArrivalDate IS NULL) OR (CONVERT(date,RR.CheckInDate)=@ArrivalDate))
AND (@bookingReference IS NULL OR RR.OrderReference like @bookingReference + '%') 
AND(@EmailValidation  IS NULL OR RR.CustomerReference like (@EmailValidation + '%') OR CUS.EmailAddress like (@EmailValidation + '%'))
AND(@NumericValidation IS NULL OR RR.CustomerReference like @NumericValidation + '%')
AND RR.BookingThrough = ISNULL(@bookingMethod,RR.BookingThrough)  
AND OLS.StatusId=ISNULL(@orderStatusId,OLS.StatusId) 
AND ((@reservationDate IS NULL) OR (CONVERT(date,OL.CreatedOn)=@reservationDate))
AND OL.CreatedOn >= COALESCE(@reservationDateFrom,OL.CreatedOn) 
AND OL.CreatedOn <= COALESCE(@reservationDateTo,OL.CreatedOn) 
AND OP.[Key] = '_appliedcampaign'

UNION 
/* Select the bookings which are NOT associated with Offers */
SELECT distinct OL.OrderLineId,
OL.OrderId,
PO.OrderGuid,
'' as Campaign, 
CONCAT(CUS.FirstName, ' ', CUS.LastName) as FullName , 
RR.OrderReference as BookingReferenceId,
CONCAT( Replace(Convert(varchar(11), RR.CheckInDate,103),' ','/'), '   ', Replace(Convert(varchar(11), RR.CheckOutDate,103),' ','/')) as CheckInCheckOutDates, 
RR.NoOfAdult, 
RR.NoOfChild, 
CONVERT(varchar(20),OL.CreatedOn,103) as CreatedOn,
RR.BookingThrough, 
OS.Name as BookingStatus, 
CONCAT(C.Name,' ', P.Name) as Accommodation
FROM [dbo].uCommerce_OrderLine OL 
INNER JOIN [dbo].uCommerce_PurchaseOrder PO ON OL.OrderId = PO.OrderId
INNER JOIN [dbo].uCommerce_RoomReservation RR ON OL.OrderLineId = RR.OrderLineId 
INNER JOIN [dbo].uCommerce_Product P ON OL.Sku = P.Sku
INNER JOIN [dbo].uCommerce_CategoryProductRelation CPR ON P.ProductId = CPR.ProductId 
INNER JOIN [dbo].uCommerce_Category C ON CPR.CategoryId = C.CategoryId
INNER JOIN [dbo].uCommerce_Customer CUS ON PO.CustomerId = CUS.CustomerId 
INNER JOIN [dbo].uCommerce_CustomerAttribute CA ON PO.CustomerId = CA.CustomerId 
INNER JOIN [dbo].uCommerce_OrderLineStatus OLS ON RR.OrderLineStatusID = OLS.OrderLineStatusId
INNER JOIN [dbo].uCommerce_OrderStatus OS ON OLS.StatusId = OS.OrderStatusId
INNER JOIN [dbo].uCommerce_OrderProperty OP ON OL.OrderLineId = OP.OrderLineId
WHERE (@firstname IS NULL OR CUS.FirstName Like @firstname + '%')
AND (@lastname IS NULL OR CUS.LastName Like @lastname + '%') 
AND CHARINDEX(CA.ClientId, @clientIds) > 0 
AND (@providerName IS NULL OR C.Name Like @providerName + '%')
AND ((@ArrivalDate IS NULL) OR (CONVERT(date,RR.CheckInDate)=@ArrivalDate))
AND (@bookingReference IS NULL OR RR.OrderReference like @bookingReference + '%') 
AND(@EmailValidation  IS NULL OR RR.CustomerReference like (@EmailValidation + '%') OR CUS.EmailAddress like (@EmailValidation + '%'))
AND(@NumericValidation IS NULL OR RR.CustomerReference like @NumericValidation + '%')
AND RR.BookingThrough = ISNULL(@bookingMethod,RR.BookingThrough)  
AND OLS.StatusId=ISNULL(@orderStatusId,OLS.StatusId) 
AND ((@reservationDate IS NULL) OR (CONVERT(date,OL.CreatedOn)=@reservationDate))
AND OL.CreatedOn >= COALESCE(@reservationDateFrom,OL.CreatedOn) 
AND OL.CreatedOn <= COALESCE(@reservationDateTo,OL.CreatedOn) 
AND (1>(Select count(value) from [dbo].uCommerce_OrderProperty 
Where [Key] = '_appliedcampaign' AND OrderLineId = OL.OrderLineId))
ORDER BY CreatedOn DESC;

GO

--********************************************************************************************************************************************************************************
--Stored Procedure for SetBookingReferenceNumber
--********************************************************************************************************************************************************************************
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SetBookingReferenceNumber]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[SetBookingReferenceNumber]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE [dbo].[SetBookingReferenceNumber]
	@OrderLineId as int,
	@OrderReference as varchar(50),
	@CustomerRefernce as varchar(50)

AS
BEGIN

UPDATE [dbo].[uCommerce_RoomReservation]
SET [OrderReference] = @OrderReference,[CustomerReference]=@CustomerRefernce
WHERE OrderLineId = @OrderLineId

END

GO

--********************************************************************************************************************************************************************************
--Stored Procedure for uCommerce_GetTransfer
--********************************************************************************************************************************************************************************

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[uCommerce_GetTransfer]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[uCommerce_GetTransfer]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uCommerce_GetTransfer]
	
@OrderlineId int

as

begin 

select orderlineid, ol.createdon, c.categoryid providerid, c.[guid] providerguid from ucommerce_orderline ol inner join ucommerce_product p on ol.sku = p.sku

inner join ucommerce_categoryproductrelation pcr on p.productid = pcr.productid

inner join ucommerce_category c on pcr.categoryid = c.categoryid

where ol.orderlineid in (SELECT * FROM  affinion_relatedorderlines(@OrderlineId)) 

order by ol.createdon

end

GO

--********************************************************************************************************************************************************************************
--Stored Procedure for UnClaimedReceiptsReport
--********************************************************************************************************************************************************************************

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UnClaimedReceiptsReport]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[UnClaimedReceiptsReport]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Procedure UnClaimedReceiptsReport
(
	@ReceiptDate datetime
)
AS
Begin
	
	Select C.CategoryId, C.Name ,OLP.CurrencyTypeId ,CU.ISOCode, 
	SUM(OLP.Amount) Amount,
	MONTH(MAX(OL.CreatedOn)) [Month], 
	YEAR(MAX(OL.CreatedOn)) [Year],
	COUNT(1) [Count]
	FROM 
		uCommerce_OrderLinePayment OLP
	INNER JOIN 	[dbo].uCommerce_OrderLine OL  ON OLP.OrderLineID = OL.OrderLineId
	INNER JOIN [dbo].uCommerce_PurchaseOrder PO ON OL.OrderId = PO.OrderId
	INNER JOIN [dbo].uCommerce_RoomReservation RR ON OL.OrderLineId = RR.OrderLineId 
	INNER JOIN [dbo].uCommerce_Product P ON OL.Sku = P.Sku
	INNER JOIN [dbo].uCommerce_CategoryProductRelation CPR ON P.ProductId = CPR.ProductId 
	INNER JOIN [dbo].uCommerce_Category C ON CPR.CategoryId = C.CategoryId
	INNER JOIN [dbo].uCommerce_Customer CUS ON PO.CustomerId = CUS.CustomerId 
	INNER JOIN [dbo].uCommerce_CustomerAttribute CA ON PO.CustomerId = CA.CustomerId 
	INNER JOIN [dbo].uCommerce_OrderLineStatus OLS ON RR.OrderLineStatusID = OLS.OrderLineStatusId
	INNER JOIN [dbo].uCommerce_OrderStatus OS ON OLS.StatusId = OS.OrderStatusId
	INNER JOIN [dbo].uCommerce_Currency CU ON CU.CurrencyId = OLP.CurrencyTypeId
	INNER JOIN [dbo].uCommerce_OrdelinePaymentRequest OLR  ON  OLR.OrderLinePaymentID = OLP.OrderLinePaymentID
	WHERE OLP.DueTypeId =1 and OLP.CreatedDate >= @ReceiptDate and isnull(OLR.IsProviderPaid,0) = 0
	GROUP BY C.CategoryId, C.Name,OLP.CurrencyTypeId,CU.ISOCode
end

GO

--********************************************************************************************************************************************************************************
--Funtions for affinion_IsPaidInFull
--********************************************************************************************************************************************************************************
IF EXISTS (
    SELECT * FROM sysobjects WHERE id = object_id(N'affinion_IsPaidInFull') 
    AND xtype IN (N'FN', N'IF', N'TF')
)
BEGIN
	DROP FUNCTION [dbo].[affinion_IsPaidInFull]
END
GO

CREATE FUNCTION [dbo].[affinion_IsPaidInFull]
(
	@OrderLineId int
)
RETURNS BIT
AS
BEGIN
	DECLARE @IspaidInFull BIT
	SET @IspaidInFull = 0
	SELECT TOP 1 @IspaidInFull = 1 FROM uCommerce_OrderProperty OP
		INNER JOIN uCommerce_OrderLine OL ON OL.OrderId = OP.OrderId
	WHERE  
		OL.OrderLineId = @OrderLineId
		AND OP.[Key]='PaymentMethod'
		AND OP.Value IN ('Full','Partial')

	RETURN @IspaidInFull
END

GO

--********************************************************************************************************************************************************************************
--TRIGGER for trUnclaimedReportInsert
--********************************************************************************************************************************************************************************

IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[trUnclaimedReportInsert]'))
DROP TRIGGER [dbo].[trUnclaimedReportInsert]
GO

CREATE TRIGGER [dbo].[trUnclaimedReportInsert] 
   ON  [dbo].[uCommerce_OrderlinePayment] 
   AFTER INSERT 
AS 
    BEGIN 
         
        INSERT INTO uCommerce_OrdelinePaymentRequest 
        ( 
            OrderLinePaymentID, 
            ActualProviderPaymentAmount 
        ) 
      
                SELECT OrderLinePaymentID, 
                        Amount 
                FROM 
                        INSERTED 
                WHERE 
                        DueTypeId = 1 
    END 
GO










