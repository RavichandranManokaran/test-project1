﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Main Nav.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Navigation.Main_Nav" %>

<div class="navbar navbar-default" id="navbar" runat="server">
 <div class="container-fluid ty-noPadding-left-right">
        <div class="navbar-header">
        </div>
  <div class="navbar-collapse collapse in ty-navbar">
    <ul class="nav navbar-nav ty-taylor-nav2  ty-nav-back" style="list-style-position:inside">
     <asp:Repeater ID="rptDropDownMenu" runat="server" OnItemDataBound="rptDropDownMenu_ItemDataBound">
      <ItemTemplate>
       <li id="MenuLi" runat="server">
                            <asp:HyperLink ID="MenuLink" runat="server">
                                <asp:Literal ID="MenuText" runat="server" /></asp:HyperLink><asp:PlaceHolder ID="phSubMenu" runat="server" />
       </li>
      </ItemTemplate>
     </asp:Repeater>
    </ul>
    <%--<div class="navbar-search pull-right">
     <asp:TextBox class="search-query span2" placeholder="Search" runat="server" ID="txtSearch" AutoPostBack="true" OnTextChanged="txt_Search_OnTextChanged" ClientIDMode="Static" />
    </div>--%>
   </div>
  </div>
 </div>
