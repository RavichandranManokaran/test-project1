﻿
using Affinion.LoyaltyBuild.UCom.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;
using UCommerce.Marketing.Targets;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class NumberOfAdultTarget : Target, IOrderLineTarget, IEntity, ITarget
    {
        private int? _hashCode;

        public virtual bool IsSatisfiedBy(OrderLine orderLine)
        {
            OrderProperty searchIgnor = orderLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.IgnorCampaignSearchCriteria);
            if (searchIgnor != null)
                return true;

            OrderProperty adultProperty = orderLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.NoOfAdultKey);
            if (adultProperty != null)
            {
                if (adultProperty.Value == this.NumberOfAdult.ToString())
                    return true;
            }
            return false;
        }



        internal static IRepository<NumberOfAdultTarget> _repo = null;
        private static object lockItem = 0;

        // Methods
        public new static IQueryable<NumberOfAdultTarget> All()
        {
            return GetRepo().Select();
        }

        public override void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<NumberOfAdultTarget, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            NumberOfAdultTarget objA = obj as NumberOfAdultTarget;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<NumberOfAdultTarget, bool>> expression)
        {
            return All().Any<NumberOfAdultTarget>(expression);
        }

        public static IList<NumberOfAdultTarget> Find(Expression<Func<NumberOfAdultTarget, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<NumberOfAdultTarget>();
        }

        public static NumberOfAdultTarget FirstOrDefault(Expression<Func<NumberOfAdultTarget, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<NumberOfAdultTarget>();
        }

        public new static NumberOfAdultTarget Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<NumberOfAdultTarget> GetRepo()
        {
            //if (_repo != null)
            //{
            //    return _repo;
            //}
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<NumberOfAdultTarget>>();
            
        }


        public override void Save()
        {
            GetRepo().Save(this);
        }

        public static NumberOfAdultTarget SingleOrDefault(Expression<Func<NumberOfAdultTarget, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties
        public override int Id
        {
            get
            {
                return this.NumberOfAdultTargetId;
            }
        }

        public virtual int NumberOfAdultTargetId { get; protected set; }

        public virtual int NumberOfAdult { get; set; }



    }
}
