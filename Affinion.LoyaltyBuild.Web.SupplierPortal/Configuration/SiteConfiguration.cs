﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;
using Sitecore.Data;
using Sitecore.Globalization;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration.SiteUI.Search;
using Sitecore.ContentSearch;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Sitecore.Data.Fields;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration
{
    /// <summary>
    /// A collection of methods to centralize common tasks throughout the site.  This allows them to be managed 
    /// in one place and be easily reused.  Many of these methods look to Sitecore items for values.
    /// </summary>
    public static class SiteConfiguration
    {
        /// <summary>
        /// Launch Sitecore stores the prerquisite articles for each article.  In order to get a list of items that 
        /// you have fulfilled the prerequisites for, we need to run a query.  This method returns the query to use.        
        /// </summary>
        /// <param name="ItemId">The Item Id of the current article.</param>
        /// <returns>A string holding the query to use.</returns>
        public static string GetFurtherReadingArticlesQuery(string ItemId)
        {
            // Use Fast Query
            return String.Format("fast://sitecore/content//*[@Prerequisite Articles = '%{0}%']", ItemId);
        }

        /// <summary>
        /// Since each Article has one or more Contributors, we need the ability to retreive the articles by a specific 
        /// author.  This method returns the query to use.       
        /// </summary>
        /// <param name="ContributorItemId">The Item Id of the Contributor.</param>
        /// <returns>A string holding the query to use.</returns>
        public static string GetArticlesByContributor(string ContributorItemId)
        {
            return String.Format(".//*[@@templatekey='article' and contains(@Contributors, '{0}')]", ContributorItemId);
        }

        /// <summary>
        /// Retreives all of the the articles in the site.  This method returns the query to use.        
        /// </summary>       
        /// <returns>A string holding the query to use.</returns>
        public static string GetArticles()
        {
            return ".//*[@@templatekey='article']";
        }

        /// <summary>
        /// All of the site settings items are stored beneath the Configuration item for the current site.        
        /// </summary>       
        /// <returns>The root configuration item.</returns>
        public static Item GetSiteSettingsItem()
        {
            return Sitecore.Context.Database.GetItem(String.Format("{0}/Configuration/site-settings", GetHomeItem().Paths.FullPath));
        }

        /// <summary>
        /// All of the site settings items are stored beneath the Configuration item for the current site.        
        /// </summary>       
        /// <returns>The root configuration item.</returns>
        public static Item GetSiteSettingsItem(Item temp)
        {
            return Sitecore.Context.Database.GetItem(String.Format("{0}/Configuration/site-settings", GetHomeItem(temp).Paths.FullPath));
        }

        /// <summary>
        /// All of the site settings items are stored beneath the Configuration item for the current site.        
        /// </summary>       
        /// <returns>The root configuration item.</returns>
        public static Item GetSearchItem()
        {
            return Sitecore.Context.Database.GetItem(String.Format("{0}/Search", GetHomeItem().Paths.FullPath));
        }

        /// <summary>
        /// All of the site settings items are stored beneath the Configuration item for the current site. This gets the presentation item below it.
        /// </summary>       
        /// <returns>The root configuration item.</returns>
        public static Item GetPresentationSettingsItem()
        {
            return Sitecore.Context.Database.GetItem(String.Format("{0}/configuration/presentation-settings", GetHomeItem().Paths.FullPath));
        }

        /// <summary>
        /// All of the site settings items are stored beneath the Configuration item for the current site.  This get the version info item beneath it.
        /// </summary>       
        /// <returns>The root configuration item.</returns>
        public static Item GetVersionInformationItem()
        {
            return Sitecore.Context.Database.GetItem(String.Format("{0}/Configuration/Version Information", GetHomeItem().Paths.FullPath));
        }

        /// <summary>
        /// All of the site settings items are stored beneath the Configuration item for the current site.  This gets the footer links item beneath it.
        /// </summary>       
        /// <returns>The root configuration item.</returns>
        public static Item GetFooterLinksItem()
        {
            return Sitecore.Context.Database.GetItem(String.Format("{0}/Configuration/footer-links", GetHomeItem().Paths.FullPath));
        }

        /// <summary>
        /// All of the site settings items are stored beneath the Configuration item for the current site.  This gets the external sites item beneath it.
        /// </summary>       
        /// <returns>The root configuration item.</returns>
        public static Item GetExternalSitesItem()
        {
            return Sitecore.Context.Database.GetItem("/sitecore/content/Global/settings/external-sites");
        }

        /// <summary>
        /// All of the global settings items are stored beneath the Global item.
        /// </summary>       
        /// <returns>The root configuration item.</returns>
        public static Item GetGlobalItem()
        {
            return Sitecore.Context.Database.GetItem("/sitecore/content/Global");
        }

        /// <summary>
        /// Quick access to the home node for the site.        
        /// </summary>       
        /// <returns>The home item.</returns>
        public static Item GetHomeItem()
        {
            // Since we want to support multi-site for evaluation purposes and do not create site nodes in the site section of 
            // the web.config, we will just go up the tree until we get to the content node.
            Item temp = Sitecore.Context.Item;
            Item contentNode = Sitecore.Context.Database.GetItem("/sitecore/content/supplierportal");
            while (temp.Parent != null && temp.ParentID != contentNode.ID)
            {
                temp = temp.Parent;
            }
            return temp;

            // This is the best way to get the home node, but it only works if there is a site definition in the web.config
            //return Sitecore.Context.Database.GetItem(Sitecore.Context.Site.StartPath);

            // These options are also ways to get tot he home node.
            //return Sitecore.Context.Item.Axes.SelectSingleItem("ancestor-or-self::*[@@templatekey='home']");
            //return Sitecore.Context.Database.GetItem("/sitecore/content/home");

            // There are multiple ways to retrieve the home node.  You can reference the path, but this is bad for multisite installs.  
            // You can also check by template if all items share the same template type, but my favorite way is to use the StartPath 
            // which is on the site definition node in the config file.
        }

        /// <summary>
        /// Quick access to the home node for the site.        
        /// </summary>       
        /// <returns>The home item.</returns>
        public static Item GetHomeItem(Item temp)
        {
            // Since we want to support multi-site for evaluation purposes and do not create site nodes in the site section of 
            // the web.config, we will just go up the tree until we get to the content node.          
            Item contentNode = Sitecore.Context.Database.GetItem("/sitecore/content");
            while (temp.Parent != null && temp.ParentID != contentNode.ID)
            {
                temp = temp.Parent;
            }
            return temp;
        }

        /// <summary>
        /// Quick access to the team node for the site.
        /// </summary>       
        /// <returns>The Team item.</returns>
        public static Item GetTeamItem()
        {
            return Sitecore.Context.Database.GetItem("/sitecore/content/home/team");
        }

        /// <summary>
        /// Quick access to the Offer Item.
        /// </summary>       
        /// <returns>The Team item.</returns>
        public static Item GetOfferItem()
        {
            return Sitecore.Context.Database.GetItem("/sitecore/content/supplierportal/welcome-page/offer-availability");
        }

        /// <summary>
        /// Quick access to the Manage Availability Item.
        /// </summary>       
        /// <returns>The Team item.</returns>
        public static Item GetManageAvailabilityItem()
        {
            return Sitecore.Context.Database.GetItem("/sitecore/content/supplierportal/welcome-page/manage-availablity");
        }

        public static string GetPageDescripton(Item item)
        {
            string description = item["Body"];
            // Most items have an abstract
            if (item["Abstract"] != String.Empty) description = item["Abstract"];
            // Terms do not have an abstract, so just use the Definition
            if (item["Definition"] != String.Empty) description = item["Definition"];

            description = HtmlRemoval.StripTagsCharArray(description);
            if (description.Length > 160) description = String.Format("{0}...", description.Substring(0, 160));

            return description.Replace("\"", "'");
        }

        public static Item GetPageEditorAlertsRootItem()
        {
            return
              Sitecore.Context.Database.GetItem(
                "/sitecore/system/Modules/Launch Sitecore/Page Editor Alerts");
        }

        public static bool DoesItemExistInCurrentLanguage(Item i)
        {
            // standard way of checking
            if (i.Versions.Count != 0) return true;

            return false;
        }

        public static bool IsMicrosite()
        {
            Item home = GetHomeItem();
            if (home != null && home.Template.Key == "microsite home") return true;

            return false;
        }

        /// <summary>
        /// Returns a search context    
        /// </summary>
        /// <param name="key">The dictionary key you are requesting.</param>
        /// <returns>A search context.</returns>
        public static IProviderSearchContext GetSearchContext(Item item)
        {
            // UNDONE: This is broken in the Sitecore 8 Technical Preview, so I am getting the index by name.
            // return ContentSearchManager.CreateSearchContext((SitecoreIndexableItem)(Sitecore.Context.Item))

            if (Sitecore.Context.PageMode.IsNormal || Sitecore.Context.PageMode.IsDebugging)
                return ContentSearchManager.GetIndex("sitecore_web_index").CreateSearchContext();

            return ContentSearchManager.GetIndex("sitecore_master_index").CreateSearchContext();
        }

        //GeneralExtensions
        public static T ValueOrDefault<T>(this T? source) where T : struct
        {
            return source ?? default(T);
        }

        public static Guid ToGuid(this Guid? source)
        {
            return source ?? Guid.Empty;
        }

        public static ID ToId(this Guid? source)
        {
            return source != Guid.Empty ? new ID(source.ToGuid()) : null;
        }

        /// <summary>
        /// Quick access to the vat range.        
        /// </summary>       
        /// <returns>The vat range.</returns>
        public static List<int> GetVatRangeItem()
        {
            Item vatrangetNode = Sitecore.Context.Database.GetItem("/sitecore/content/admin-portal/global/_supporting-content/vat/subratevat");
            int startFrom = Convert.ToInt32(vatrangetNode.Fields["Start"].Value);
            int endat = Convert.ToInt32(vatrangetNode.Fields["End"].Value);
            //int startFrom = 15;
            //int endat = 25;
            List<int> lstvatrange = new List<int>();

            lstvatrange.Add(0);

            for (int i = startFrom; i <= endat; i++)
            {
                lstvatrange.Add(i);
            }

            return lstvatrange;
        }

        public static SupplierInfo GetSupplierDetailById(string supplierId)
        {
            Item supplierItem = Sitecore.Context.Database.GetItem(supplierId);
            SupplierInfo supplierInfo = new SupplierInfo();
            if (supplierItem != null)
            {
                supplierInfo.SupplierName = supplierItem.DisplayName;
                if (supplierItem.Fields["AddressLine1"] != null)
                {
                    supplierInfo.SupplierAddress = supplierItem.Fields["AddressLine1"].Value;
                }
                if (supplierItem.Fields["AddressLine2"] != null && !string.IsNullOrEmpty(supplierItem.Fields["AddressLine2"].Value))
                {
                    supplierInfo.SupplierAddress += string.Format(", {0}", supplierItem.Fields["AddressLine2"].Value);
                }
                if (supplierItem.Fields["AddressLine3"] != null && !string.IsNullOrEmpty(supplierItem.Fields["AddressLine3"].Value))
                {
                    supplierInfo.SupplierAddress += string.Format(", {0}", supplierItem.Fields["AddressLine3"].Value);
                }
                if (supplierItem.Fields["AddressLine4"] != null && !string.IsNullOrEmpty(supplierItem.Fields["AddressLine4"].Value))
                {
                    supplierInfo.SupplierAddress += string.Format(", {0}", supplierItem.Fields["AddressLine4"].Value);
                }
                if (supplierItem.Fields["Country"] != null && ((LookupField)supplierItem.Fields["Country"]).TargetItem != null)
                {
                    supplierInfo.Country = ((LookupField)supplierItem.Fields["Country"]).TargetItem.DisplayName;
                }
                if (supplierItem.Fields["SupplierType"] != null && ((LookupField)supplierItem.Fields["SupplierType"]).TargetItem != null)
                {
                    supplierInfo.SupplierType = ((LookupField)supplierItem.Fields["SupplierType"]).TargetItem.DisplayName;
                }
                if (supplierItem.Fields["StarRanking"] != null && ((LookupField)supplierItem.Fields["StarRanking"]).TargetItem != null)
                {
                    supplierInfo.StarRating = ((LookupField)supplierItem.Fields["StarRanking"]).TargetItem.DisplayName;
                }
                if (supplierItem.Fields["Group"] != null && ((LookupField)supplierItem.Fields["Group"]).TargetItem != null)
                {
                    supplierInfo.Group = ((LookupField)supplierItem.Fields["Group"]).TargetItem.DisplayName;
                }
                if (supplierItem.Fields["SuperGroup"] != null && ((LookupField)supplierItem.Fields["SuperGroup"]).TargetItem != null)
                {
                    supplierInfo.SuperGroup = ((LookupField)supplierItem.Fields["SuperGroup"]).TargetItem.DisplayName;
                }
                if (supplierItem.Fields["MapLocation"] != null && ((LookupField)supplierItem.Fields["MapLocation"]).TargetItem != null)
                {
                    supplierInfo.MapLocation = ((LookupField)supplierItem.Fields["MapLocation"]).TargetItem.DisplayName;
                }
                //MapLocation
                if (supplierItem.Fields["EmailID2"] != null)
                {
                    supplierInfo.Email = (supplierItem.Fields["EmailID2"].Value);
                }

            }
            return supplierInfo;
        }

        /// <summary>
        /// Quick access to the Offer removal reasons.        
        /// </summary>       
        /// <returns>List of reason for offer remove.</returns>
        public static List<string> GetOfferRemoveReason()
        {

            try
            {
                List<string> lstResons = new List<string>();
                Item[] reasons = Sitecore.Context.Database.SelectItems("fast:/sitecore/content/GlobalData/#offer-remove-reason#/*[@@templatename='RemoveOfferReason']");


                foreach (Item reason in reasons)
                {
                    lstResons.Add(reason.Fields["Reason"].Value);
                }

                return lstResons;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 1. availability
        /// 2. For Opted Out
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string GetHelpContentByType(int type)
        {

            try
            {
                string tempName = string.Empty;
                if (type == 1)
                {
                    tempName = "availability";
                }
                else if (type == 2)
                {
                    tempName = "offeravailability";
                }
                else
                {
                    tempName = "bulkupload";
                }
                Item helpItem = Sitecore.Context.Database.GetItem(string.Format("/sitecore/content/globaldata/help/{0}", tempName));

                if (helpItem != null)
                {
                    if (helpItem.Fields["Content"] != null && !string.IsNullOrEmpty(helpItem.Fields["Content"].Value))
                    {
                        return helpItem.Fields["Content"].Value;
                    }
                }
                return string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// 1. For Opted in
        /// 2. For Opted Out
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string GetEmailTemplateHtml(int type)
        {

            try
            {
                string tempName = string.Empty;
                if (type == 1)
                {
                    tempName = "opted-in-campaign";
                }
                else
                {
                    tempName = "opted-out-campaign";
                }
                Item supplierEmailItem = Sitecore.Context.Database.GetItem(string.Format("/sitecore/content/globaldata/email-settings/{0}", tempName));

                if (supplierEmailItem != null)
                {
                    if (supplierEmailItem.Fields["HtmlTemplate"] != null && !string.IsNullOrEmpty(supplierEmailItem.Fields["HtmlTemplate"].Value))
                    {
                        return supplierEmailItem.Fields["HtmlTemplate"].Value;
                    }
                }
                return string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }

        public static EmailSetting GetSupplierEmailConfiguration()
        {
            EmailSetting settings = new EmailSetting();
            try
            {

                Item supplierEmailItem = Sitecore.Context.Database.GetItem("/sitecore/content/globaldata/email-settings/supplier-email-settings");
                if (supplierEmailItem != null)
                {
                    if (supplierEmailItem.Fields["LBAdmin"] != null && !string.IsNullOrEmpty(supplierEmailItem.Fields["LBAdmin"].Value))
                    {
                        settings.LBAdmin = supplierEmailItem.Fields["LBAdmin"].Value;
                    }
                    if (supplierEmailItem.Fields["Password"] != null && !string.IsNullOrEmpty(supplierEmailItem.Fields["Password"].Value))
                    {
                        settings.Password = supplierEmailItem.Fields["Password"].Value;
                    }
                    if (supplierEmailItem.Fields["SMTPPort"] != null && !string.IsNullOrEmpty(supplierEmailItem.Fields["SMTPPort"].Value))
                    {
                        settings.SMTPPort = supplierEmailItem.Fields["SMTPPort"].Value;
                    }
                    if (supplierEmailItem.Fields["SMTPServer"] != null && !string.IsNullOrEmpty(supplierEmailItem.Fields["SMTPServer"].Value))
                    {
                        settings.SMTPServer = supplierEmailItem.Fields["SMTPServer"].Value;
                    }
                    if (supplierEmailItem.Fields["SupplierFrom"] != null && !string.IsNullOrEmpty(supplierEmailItem.Fields["SupplierFrom"].Value))
                    {
                        settings.SupplierFrom = supplierEmailItem.Fields["SupplierFrom"].Value;
                    }
                    if (supplierEmailItem.Fields["SupplierTo"] != null && !string.IsNullOrEmpty(supplierEmailItem.Fields["SupplierTo"].Value))
                    {
                        settings.SupplierTo = supplierEmailItem.Fields["SupplierTo"].Value;
                    }
                    if (supplierEmailItem.Fields["UserName"] != null && !string.IsNullOrEmpty(supplierEmailItem.Fields["UserName"].Value))
                    {
                        settings.UserName = supplierEmailItem.Fields["UserName"].Value;
                    }


                }
                return settings;
            }
            catch
            {
                return null;
            }
        }

        public static List<ClientDetail> GetAllClient()
        {
            Item[] items = Sitecore.Context.Database.SelectItems("fast:/sitecore/content/#admin-portal#/#client-setup#/*[@@templatename='ClientDetails']");
            List<ClientDetail> clientList = new List<ClientDetail>();

            int i = 1;
            foreach (Item client in items)
            {
                clientList.Add(new ClientDetail() { Id = i, Name = client.Fields["Name"].Value, ClientGUID = client.ID.ToString().Replace("{", "").Replace("}", "") });
                i++;
            }
            return clientList;
        }

    }
}