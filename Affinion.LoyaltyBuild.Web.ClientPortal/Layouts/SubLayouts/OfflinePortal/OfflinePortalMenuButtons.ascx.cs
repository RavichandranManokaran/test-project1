﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Web.ClientPortal.Models;
using Sitecore.Links;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Order;
using Affinion.LoyaltyBuild.Web.ClientPortal.Helper;
using Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Email;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic;
using System.Net;
using Affinion.LoyaltyBuild.Api.Booking.Service;
using data = Affinion.LoyaltyBuild.Api.Booking.Data;
using Affinion.LoyaltyBuild.UCom.Common.Extension;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    public partial class OfflinePortalMenuButtons : BaseSublayout
    {
        List<SubMenu> menuButtonList = new List<SubMenu>();
        //variable declaration
        private IOrderService orderService;
        private int orderLineId;
        private IMailService mailService;
        public string status = WebUtility.HtmlEncode("cancelled");
        //response object
        protected ValidationResponse ValidationResponse { get; private set; }
        data.Booking bookingInfo = null;
        #region LBUATR-116
        protected bool ShowTransfer { get; private set; }
        protected string TransferUrl { get; set; }
        #endregion
        protected void page_Init(Object sender, EventArgs e)
        {
            BindSitecoreErrormsgs();
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                Item menuItem = this.GetDataSource();
                BindSitecoreErrormsgs();
                if (menuItem != null)
                {
                    var mainMenuItems = SitecoreFieldsHelper.GetMutiListItems(menuItem, "MenuItems");
                    var litsOfMainMenu = new List<Item>();

                    int olid;
                    int.TryParse(this.QueryString("olid"), out olid);
                    orderLineId = olid;
                    IOrderService orderSvc = ContainerFactory.Instance.GetInstance<IOrderService>();
                    BookingService booking = new BookingService();
                    bookingInfo = booking.GetBooking(orderLineId);
                    foreach (var item in mainMenuItems)
                    {
                        //to map menu bar buttons
                        MenuButtonFieldsMapping(item, menuButtonList);

                    }
                    this.MenuButtons.DataSource = menuButtonList;
                    this.MenuButtons.DataBind();
                    if (bookingInfo.Type == Model.Product.ProductType.BedBanks)
                    {
                        taNoteToProvider.Disabled = true;
                    }
                }
            }


            catch (Exception ex)
            {
                Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Method to load menu buttons
        /// </summary>
        protected void MenuButtonFieldsMapping(Item targetItem, List<SubMenu> list)
        {
            if (targetItem != null)
            {
                SubMenu topBarButtons = new SubMenu();
                topBarButtons.MenuText = targetItem.Fields["Title"] != null ? targetItem.Fields["Title"].Value : string.Empty;
                Sitecore.Data.Fields.LinkField lnkFld = targetItem.Fields["Link"];
                if (targetItem.Name == "cancel-booking")
                {
                    orderService = ContainerFactory.Instance.GetInstance<OrderService>();

                    var booking = orderService.GetBaseBooking(orderLineId);
                    //disable cancel on invalid booking
                    if (booking == null || booking.CheckInDate < DateTime.Now.AddDays(-1))
                    {
                        //ShowError(booking);
                        ShowBookingStatus(booking);
                    }

                    //set booking reference text
                    if (booking != null)
                        lblBookingReference.Text = booking.BookingReference;



                    topBarButtons.CssClass = "cancel";
                    topBarButtons.RedirectUrl = "#";
                }
                else
                {
                    if (lnkFld != null && lnkFld.TargetItem != null)
                    {
                        string lnkUrl = LinkManager.GetItemUrl(lnkFld.TargetItem);
                        if (!String.IsNullOrWhiteSpace(lnkFld.QueryString))
                        {
                            var decodedParameter = HttpUtility.UrlDecode(HttpUtility.UrlDecode(lnkFld.QueryString));
                            //var formattedString= string.Format(decodedParameter, orderLineId);
                            var query = HttpUtility.ParseQueryString(decodedParameter);
                            var queryValues = new List<string>();
                            foreach (var item in query.AllKeys)
                            {
                                queryValues.Add(QueryString(item));
                            }
                            var queryParameters = string.Format(decodedParameter, queryValues.ToArray());

                            var url = string.Concat(lnkUrl + "?", queryParameters);
                            topBarButtons.RedirectUrl = this.GetUrl(url);
                            if (Request.Url.AbsolutePath.Equals(lnkUrl, StringComparison.InvariantCultureIgnoreCase))
                            {
                                topBarButtons.CssClass = "active";
                                topBarButtons.RedirectUrl = "javascript:void(0);";
                            }

                            if (lnkFld.TargetItem.Name == "transferbooking" && bookingInfo.Type != Model.Product.ProductType.BedBanks)
                            {
                                url = string.Empty;
                                topBarButtons.RedirectUrl = string.Empty;
                                topBarButtons.Disabled = true;
                                SetShowTransfer(bookingInfo);
                                if (ShowTransfer)
                                {
                                    topBarButtons.Disabled = false;
                                    var clientName = "loyaltybuild";
                                    var ClientId = string.Empty;
                                    if (!string.IsNullOrEmpty(bookingInfo.ClientId))
                                    {
                                        ClientId = bookingInfo.ClientId;
                                        var client = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(bookingInfo.ClientId));
                                        if (client != null)
                                            clientName = client.Name;
                                    }

                                    var roomBooking = (data.RoomBooking)bookingInfo;

                                    TransferUrl = string.Format("&mode=Transfer&ClientList={5}&CheckinDate={0}&CheckoutDate={1}&NoOfNights={2}&NoOfAdults={3}&NoOfChildren={4}"
                                        , bookingInfo.CheckinDate.ConvertDateToString()
                                        , bookingInfo.CheckoutDate.ConvertDateToString(),
                                       roomBooking.NoOfNights,
                                       roomBooking.NoOfAdults,
                                        roomBooking.NoOfChildren,
                                        clientName
                                        );

                                    url = "/locations?olid=" + bookingInfo.OrderLineId + TransferUrl;
                                    topBarButtons.RedirectUrl = url;
                                }
                            }
                        }
                        else
                        {

                            topBarButtons.RedirectUrl = lnkUrl;
                        }

                        topBarButtons.Target = lnkFld.Target;
                        if (targetItem.HasChildren)
                        {
                            var subList = new List<SubMenu>();
                            foreach (Item item in targetItem.Children)
                                MenuButtonFieldsMapping(item, subList);
                            topBarButtons.Children = subList;
                            if (subList.Any(i => i.CssClass == "active"))
                                topBarButtons.CssClass = "active";
                        }
                    }
                    if (bookingInfo.Type == Model.Product.ProductType.BedBanks)
                    {
                        switch (lnkFld.TargetItem.Name)
                        {
                            case "transferbooking": topBarButtons.Disabled = true;
                                topBarButtons.RedirectUrl = "javascript:void(0);";
                                break;
                            case "transferhistory": topBarButtons.Disabled = true;
                                topBarButtons.CssClass = "disabled";
                                topBarButtons.RedirectUrl = "javascript:void(0);";
                                break;
                            default: topBarButtons.Disabled = false;
                                break;
                        }
                    }
                }
                list.Add(topBarButtons);
            }

        }

        private void ShowBookingStatus(BaseBooking booking)
        {
            taCancelReason.Disabled = true;
            taNoteToProvider.Disabled = true;
            divCancel.Visible = false;
        }

        private void ShowError(BaseBooking booking)
        {
            if (booking == null)
                ValidationResponse.Message = "Invalid booking details";
            else if (booking.IsCancelled)
                ValidationResponse.Message = "Booking already cancelled";
            else if (booking.CheckInDate < DateTime.Now.AddDays(-1))
                ValidationResponse.Message = "Booking past cancellation time";
        }

        /// <summary>
        /// Complete cancel click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CompleteCancel_Click(object sender, EventArgs e)
        {

            string noteToProvider = taNoteToProvider.InnerText;
            string cancelReason = taCancelReason.InnerText;

            ValidationResponse cancelFlag = orderService.CancelBooking(new CancelBookingInfo()
            {
                OrderLineid = orderLineId,
                noteToProvider = taNoteToProvider.InnerText,
                cancelReason = taCancelReason.InnerText,
                paymentDetails = null,
                CreatedBy = SitecoreUserProfileHelper.GetItem(UserProfileKeys.CreatedBy)
            });

            ValidationResponse = cancelFlag;

            if (cancelFlag.IsSuccess)
            {
                Booking booking = orderService.GetBooking(orderLineId);
                SendEmail(booking);

            }
        }

        /// <summary>
        /// Send cancellation email
        /// </summary>
        /// <param name="booking"></param>
        private void SendEmail(Booking booking)
        {
            mailService = new MailService(booking.ClinetId);

            mailService.GenerateEmail(MailTypes.CustomerCancellation, orderLineId);
            mailService.GenerateEmail(MailTypes.ProviderCancellation, orderLineId);
        }


        protected void MenuButtons_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var submenu = (Repeater)e.Item.FindControl("SubButtons");
                submenu.DataSource = ((SubMenu)e.Item.DataItem).Children;
                submenu.DataBind();
            }
        }

        #region binderrormsgs
        private void BindSitecoreErrormsgs()
        {
            Item conditionalMessageItem = this.GetItemFromRenderingParameterPath("Conditional Messages Link");
            string dontentercardFieldMessageFieldName = "Not Enter Card Number Message";
        }
        #endregion

        private void SetShowTransfer(data.Booking booking)
        {
            ShowTransfer = true;

            //if already cancelled
            if (booking == null || booking.Status == data.BookingStatus.Cancelled || booking.CheckinDate < DateTime.Now.AddDays(-1))
                ShowTransfer = false;
        }
    }
}