﻿<%@ Page Title="" Language="C#" MasterPageFile="~/sitecore modules/Shell/Affinion/Finance/MainLayout.Master" AutoEventWireup="true"  CodeBehind="HotelDemands.aspx.cs" Inherits="Affinion.LoyaltyBuild.AdminPortal.sitecore_modules.Shell.Affinion.Finance.HotelDemands" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {

            $('input[id="DateRange"]').daterangepicker();

            $('#DateRange').daterangepicker({
                locale: { cancelLabel: 'Clear' }
            });

            function cb(start, end) {
                $('#DateRange span').html(start + ' - ' + end);
            }
            cb(moment().subtract(29, 'days'), moment());

            $('#DateRange').daterangepicker({
                ranges: {
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    'Last Two Month': [moment().subtract(2, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    'Last Four Month': [moment().subtract(4, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    'Last Six Month': [moment().subtract(6, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            $('#DateRange').on('cancel.daterangepicker', function (ev, picker) {
                //do something, like clearing an input
                $('#DateRange').val('');
                Validation();
            });

            $('#DateRange').on('apply.daterangepicker', function (ev, picker) {
                Validation();
            });

            $("#chkSelectAll").click(function () {
                if ($(this).is(":checked")) {
                    $("#ResultGridView input[id*='chkSelect']:checkbox").attr("checked", function () {
                        if (!$(this).is(":disabled"))
                            this.checked = true;
                    });

                    $("#hdnSelectAll").val("true");
                }
                else {
                    $("#ResultGridView input[id*='chkSelect']:checkbox").attr("checked", function () {
                        this.checked = false;
                    });
                    $("#hdnSelectAll").val("false");
                }
                UpdateCheckUnCheck("SelectAll", $(this).is(":checked"), 0, 0);
                Validation();

            });

            $("#ResultGridView input[id*='chkSelect']:checkbox").click(function () {
                if (!$(this).is(":checked")) {
                    $("#hdnSelectAll").val("false");
                    $("#ResultGridView input[id*='chkSelectAll']:checkbox").attr("checked", function () {
                        this.checked = false;
                    });
                }
                else {
                    $("#hdnSelectAll").val("true");
                }

                var OrderID = 0;
                var OrderLineID = 0;
                var checkedIndex = $('input[type="checkbox"]').index(this);
                var grdResultView = document.getElementById('ResultGridView');
                var hiddenField = grdResultView.rows[checkedIndex].getElementsByTagName("input");
                if (hiddenField != null && hiddenField != "") {
                    for (var i = 0; i < hiddenField.length; i++) {
                        if (hiddenField[i].type == "hidden") {
                            if (hiddenField[i].id == "hdnOrderID") {
                                OrderID = hiddenField[i].value;
                            }
                            if (hiddenField[i].id == "hdnOrderLineID") {
                                OrderLineID = hiddenField[i].value;
                            }
                        }
                    }
                    UpdateCheckUnCheck("Select", $(this).is(":checked"), OrderID, OrderLineID);
                    Validation();
                }

            });

            if ($("#hdnSelectAll").val() == "true") {
                if ($("#hdnSelect").val() == "true") {
                    $("#ResultGridView input[id*='chkSelectAll']:checkbox").attr("checked", function () {
                        this.checked = true;
                    });
                }
                else {
                    $("#hdnSelectAll").val() = "false";
                    $("#ResultGridView input[id*='chkSelectAll']:checkbox").attr("checked", function () {
                        this.checked = false;
                    });

                }
            }

            $("#DateUpTo").change(function () {
                Validation();
            });

            if ($("#hdnDateRange").val() == "false") {
                $('#DateRange').val('');
            }
            Validation();

        });

        function UpdateCheckUnCheck(selectmode, status, orderID, orderLineID) {
            $.ajax(
                {
                    type: "POST",
                    url: "/sitecore modules/Shell/Affinion/Finance/HotelDemands.aspx/UpdateSelectedCheck",
                    data: '{SelectMode: "' + selectmode + '",Status: "' + Boolean(status) + '",OrderID: "' + parseInt(orderID) + '" ,OrderLineID: "' + parseInt(orderLineID) + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        $("#hdnSelect").val("true");
                    },
                    complete: function (response) {
                        $("#hdnSelect").val("true");
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
        }

        function Validation() {
            
            var DateRange = $("#DateRange").val();
             
            if (DateRange != "") {
                $("#Demands").removeAttr('disabled');
                $("#btnGo").removeAttr('disabled');
                $("#hdnDateRange").val("true");
            }
            else
            {
                $("#Demands").attr('disabled', 'disabled');
                $("#btnGo").attr('disabled', 'disabled');
                $("#hdnDateRange").val("false");
            }

            if ($("#hdnSelect").val() == "true" || $("#hdnSelectAll").val() == "true") {
                $("#PostToOracle").removeAttr('disabled');
            }
            else {
                $("#PostToOracle").attr('disabled', 'disabled');
            }
        }
 </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuItems" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Title" runat="server">
    Hotel Demands
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Body" runat="server">
    <div class="row">
			<div class="col-md-12">
                 <div>
                    <h4 class="title-text font-weight-bold"><asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label></h4>
                </div>  
			</div>
		</div>
    <div class="panel-heading bg-primary">
									<h4 class="panel-title">
									<span>Search Oracle Hotel Demands : <asp:Label ID="lblTotalRecords" runat="server" Text="0"></asp:Label> records(s)</span>
									<a data-toggle="collapse" data-parent="#accordion1" href="#collapse0" class="show-hide-list">
									<span class="glyphicon glyphicon-triangle-top pull-right"></span>
									</a>
									</h4>
								</div>
	<div id="collapse0" class="panel-collapse collapse in">
									<div class="panel-body">
										<div class="row">
											<div class="col-xs-12"> 
												<div class="col-xs-12 no-padding">
													<div class="col-xs-3 no-padding-left">
														<div><b>Date Range:</b></div>
														<div>
															<div>
																 <input type="text" id="DateRange" runat="server" clientidmode="Static" class="form-control datepicker font-size-12" tabindex="1" />
															</div>
														</div>
													</div>
													<div class="col-xs-3 no-padding-left">
														<div><b>Currency:</b></div>
														<div>
															<asp:DropDownList ID="Currency" ClientIDMode="Static" runat="server" CssClass="form-control font-size-12 dropdown-field" TabIndex="2">
                                                            </asp:DropDownList> 
														</div>
													</div>
                                                    <div class="col-xs-3 no-padding">
														<div><b>Payment Status:</b></div>
														<div>
															<asp:DropDownList ID="PaymentStatus" ClientIDMode="Static" runat="server" CssClass="form-control font-size-12 dropdown-field" TabIndex="3">
                                                                <asp:ListItem Value="">Select Payment Status</asp:ListItem>
                                                                <asp:ListItem Value="1">Paid</asp:ListItem>
                                                                <asp:ListItem Value="0">Unpaid</asp:ListItem>
                                                                 <asp:ListItem Value="2">Pending</asp:ListItem>
                                                            </asp:DropDownList> 
														</div>
													</div>
													<div class="col-xs-3 no-padding-right"> 
														<div>&nbsp;</div>
														<div><asp:Button type="button" ID="Demands" class="btn btn-primary fullWidth pull-right"  ClientIDMode="Static" Text="Get Demands" runat="server" OnClick="Demands_Click" TabIndex="4"/>
														</div>
													</div>
												</div>
											</div>
											
										</div>
										<div class="row">
											<div class="col-xs-12 marT15"> 
												<div class="col-xs-9 no-padding"> <label class="pull-right"><b>Search:</b></label></div>
												<div class="col-xs-3 no-padding-right">
                                                        <asp:TextBox  ID="SearchBox" clientidmode="Static" runat="server" class="form-control demondsearch" placeholder="search" TabIndex="5" />
                                                        <asp:Button type="button" ID="btnGo" runat="server" ClientIDMode="Static" Text="Go" class="btn btn-primary pull-right mar0" TabIndex="6" OnClick="btnGo_Click"/>
                                   
												</div>
											</div>
											<div class="col-xs-12 marT15">
												<div class="row" id="htmlReportGrid" runat="server">
                                                    <div class="col-sm-12">
                                                        <div>
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading claim_titl">
                                                                    <h4 class="panel-title">Oracle Hotel Demand reports.
                                                                    </h4>
                                                                </div>
                                                                <div id="totalCount" runat="server" class="totalCountpad">Oracle Hotel Demand details reports Total of <span><%= TotalOfRecords %> </span>record(s) found. </div>
                                                                <div class="panel-body alert-info">
                                                                    <%-- Result Grid--%>
                                                                    <div runat="server" id="ResultDiv" clientidmode="Static">
                                                                        <div id="data-grid" class="table-responsive">
                                                                            <asp:GridView ID="ResultGridView" ClientIDMode="Static" runat="server" Width="100%" SelectedRowStyle-Font-Bold="true" class="table table-striped table-bordered" AutoGenerateColumns="False" AllowPaging="true" PageSize="10"
                                                                                PagerSettings-NextPageText=" Next " PagerSettings-PreviousPageText=" Previous " PagerSettings-Mode="NumericFirstLast" PagerSettings-FirstPageText=" First " PagerSettings-LastPageText=" Last " PagerSettings-Position="Bottom" PagerStyle-Font-Bold="true" PagerStyle-CssClass="pagination-grid" OnPageIndexChanging="ResultGridView_PageIndexChanging" OnRowDataBound="ResultGridView_RowDataBound" AllowSorting="True" OnSorting="ResultGridView_Sorting" >
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="Hotel ID" SortExpression="HotelID">
                                                                                        <ItemTemplate>
                                                                                            <table style="width: 100%">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblHotelID" runat="server" Text='<%# Bind("HotelID") %>'></asp:Label></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblSupplierPaymentId" runat="server" Text='<%# Bind("SupplierPaymentId") %>'  style="display:none"></asp:Label></td>
                                                                                                </tr>
                                                                                                 <tr>
                                                                                                    <td>
                                                                                                        <asp:HiddenField ID="hdnOrderID" runat="server" ClientIDMode="Static" Value='<%# Bind("OrderID") %>'></asp:HiddenField></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:HiddenField ID="hdnOrderLineID" runat="server" ClientIDMode="Static" Value='<%# Bind("OrderLineID") %>'></asp:HiddenField></td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Demand Date" SortExpression="DemandDate">
                                                                                        <ItemTemplate>
                                                                                            <table style="width: 100%">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblDemandDate" runat="server" Text='<%# Bind("DemandDate","{0:dd/MM/yyyy}") %>'></asp:Label></td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Booking Identifier" SortExpression="BookingID">
                                                                                        <ItemTemplate>
                                                                                            <table style="width: 100%">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblBookingID" runat="server" Text='<%# Bind("BookingID") %>'></asp:Label></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblBookingRefNo" runat="server" Text='<%# Bind("BookingRefNo") %>'></asp:Label></td>
                                                                                                </tr>
                                                                                               
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Guest Name" SortExpression="CustomerName">
                                                                                        <ItemTemplate>
                                                                                            <table style="width: 100%">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblGuestName" runat="server" Text='<%# Bind("CustomerName") %>'></asp:Label></td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Arrival Date" SortExpression="CheckInDate">
                                                                                        <ItemTemplate>
                                                                                            <table style="width: 100%">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblArrivalDate" runat="server" Text='<%# Bind("CheckInDate","{0:dd/MM/yyyy}") %>'></asp:Label></td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Departure Date" SortExpression="CheckOutDate">
                                                                                        <ItemTemplate>
                                                                                            <table style="width: 100%">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblDepartureDate" runat="server" Text='<%# Bind("CheckOutDate","{0:dd/MM/yyyy}") %>'></asp:Label></td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Paid in Oracle" SortExpression="PaymentStatus">
                                                                                        <ItemTemplate>
                                                                                            <table style="width: 100%">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblPaidInOracle" runat="server" Text='<%# Bind("PaymentStatus") %>'></asp:Label></td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Suggest Amount" SortExpression="SuggestedAmount">
                                                                                        <ItemTemplate>
                                                                                            <table>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblSuggestAmount" runat="server" Text='<%# Bind("SuggestedAmount") %>'></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                 <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblCurrency" runat="server" Text='<%# Bind("Currency") %>'></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                            

                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                     <asp:TemplateField HeaderText="Posted Amount" SortExpression="SuggestedAmount">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblPostedAmount" runat="server" Text='<%# Bind("SuggestedAmount") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                      <asp:TemplateField HeaderText="Date Posted to Oracle" SortExpression="DatePostedToOracle">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblDatePostedToOracle" runat="server" Text='<%# Bind("DatePostedToOracle","{0:dd/MM/yyyy}") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField>
                                                                                        <HeaderTemplate>
                                                                                            <table style="width: 100%">
                                                                                                <tr>
                                                                                                    <td colspan="2">
                                                                                                        <asp:Label ID="lblPost" runat="server" Text="Post"></asp:Label></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblSelectAll" runat="server" Text="Select All:"></asp:Label></td>
                                                                                                    <td>
                                                                                                        <asp:CheckBox ID="chkSelectAll" ClientIDMode="Static" runat="server" />

                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </HeaderTemplate>
                                                                                        <ItemTemplate>
                                                                                            <table style="width: 100%">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:CheckBox ID="chkSelect" ClientIDMode="Static" runat="server" /></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblPostedDate" runat="server" Text='<%# Bind("DatePostedToOracle","{0:dd/MM/yyyy}") %>'></asp:Label></td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                     <asp:TemplateField HeaderText="Oracle File">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblOracleFileID" runat="server" Text='<%# Bind("OracleFieID") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>

                                                                                    <asp:TemplateField>
                                                                                        <ItemTemplate>
                                                                                             <asp:ImageButton ID="btnHotelDemandDelete" runat="server" Width="30" Height="30" ImageUrl="~/ResourcesAdmin/images/delete.jpg" ToolTip="Delete"  OnClick="btnHotelDemandDelete_Click"/>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>

                                                                                <EmptyDataTemplate>
                                                                                    ---No Records---
                                                                                </EmptyDataTemplate>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
											</div>

                                             <div class="col-xs-12">
                                                <asp:Button type="button" ID="PostToOracle" runat="server" ClientIDMode="Static" Text="Post Selected Oracle Demands" class="btn btn-primary pull-right" TabIndex="7" OnClick="PostToOracle_Click" />
                                                 <asp:HiddenField ID="hdnSelectAll" ClientIDMode="Static"  runat="server" />
                                                <asp:HiddenField ID="hdnSelect" ClientIDMode="Static"  runat="server" />
                                                 <asp:HiddenField ID="hdnDateRange" ClientIDMode="Static" Value="true"  runat="server" />
                                            </div>
                                            <div class="col-xs-12 marbot40"></div>
										</div>
                                        
									</div>
								</div>
</asp:Content>
