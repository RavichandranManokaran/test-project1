﻿CREATE TABLE [dbo].[uCommerce_ProductDefinitionFieldDescription] (
    [ProductDefinitionFieldDescriptionId] INT            IDENTITY (1, 1) NOT NULL,
    [CultureCode]                         NVARCHAR (60)  NOT NULL,
    [DisplayName]                         NVARCHAR (255) NOT NULL,
    [ProductDefinitionFieldId]            INT            NOT NULL,
    [Description]                         NVARCHAR (MAX) NULL,
    CONSTRAINT [uCommerce_PK_ProductDefinitionFieldDescription] PRIMARY KEY CLUSTERED ([ProductDefinitionFieldDescriptionId] ASC),
    CONSTRAINT [uCommerce_FK_ProductDefinitionFieldDescription_ProductDefinitionField] FOREIGN KEY ([ProductDefinitionFieldId]) REFERENCES [dbo].[uCommerce_ProductDefinitionField] ([ProductDefinitionFieldId]),
    CONSTRAINT [Unique_ProductDefinitionFieldDescription_CultureCode_ProductDefintionFieldId] UNIQUE NONCLUSTERED ([CultureCode] ASC, [ProductDefinitionFieldId] ASC)
);

