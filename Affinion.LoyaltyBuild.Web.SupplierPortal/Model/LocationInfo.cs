﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Model
{
    public class LocationInfo
    {
        public String Location { get; set; }
        public String MyProperty { get; set; }
    }
}