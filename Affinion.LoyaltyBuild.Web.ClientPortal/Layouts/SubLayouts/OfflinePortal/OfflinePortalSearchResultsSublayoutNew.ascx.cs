﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           OfflinePortalSearchResultsSublayout.ascx.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal
/// Description:           SubLayout for offline portal search Results
/// </summary>

using Affinion.LoyaltyBuild.Api.Booking.Helper;
using Affinion.LoyaltyBuild.Api.Search.Helpers;
using Affinion.LoyaltyBuild.Model.Product;
using Affinion.LoyaltyBuild.Model.Provider;
using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
using Affinion.LoyaltyBuild.Web.ClientPortal.Helper;
using Affinion.LoyaltyBuild.Web.ClientPortal.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using data = Affinion.LoyaltyBuild.Api.Search.Data;

namespace Affinion.Loyaltybuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    public partial class OfflinePortalSearchResultsSublayoutNew : BaseSublayout
    {
        private List<PriceAvailabilityByDate> _availability = null;
        private List<DateTime> _dates = new List<DateTime>();
        private DateTime? checkInDate = null;
        protected string CreatedBy { get; set; }

        #region Properties

        /// <summary>
        /// Checkin date
        /// </summary>
        protected DateTime? CheckInDate
        {
            get
            {
                return checkInDate;
            }
        }

        /// <summary>
        /// No Of Nights
        /// </summary>
        protected int NoOfNights { get; set; }

        /// <summary>
        /// Currency Id 
        /// </summary>
        protected int CurrencyId { get; set; }

        /// <summary>
        /// is checkin date selected
        /// </summary>
        protected bool IsDateSelected { get; set; }
        
        /// <summary>
        /// result size
        /// </summary>
        protected int ResultSize { get; set; }
        
        /// <summary>
        /// page size
        /// </summary>
        protected int PageSize { get; set; }
        
        /// <summary>
        /// current page
        /// </summary>
        protected int CurrentPage { get; set; }

        /// <summary>
        /// Search Mode
        /// </summary>
        protected data.BookingMode SearchMode { get; set; }

        /// <summary>
        /// get previous week url
        /// </summary>
        protected string PreviousWeekUrl
        {
            get
            {
                var searchKey = new data.SearchKey(this.Context);
                //set url
                if(searchKey.IsValid && searchKey.CheckInDate != null)
                {
                    searchKey.Week = searchKey.Week - 1;
                    return string.Format("{0}?{1}", Request.Url.GetLeftPart(UriPartial.Path), searchKey.CreateSearchString());
                }
                return string.Empty;
            }
        }
        
        /// <summary>
        /// get previous week url
        /// </summary>
        protected string NextWeekUrl
        {
            get
            {
                var searchKey = new data.SearchKey(this.Context);
                //set url
                if (searchKey.IsValid && searchKey.CheckInDate != null)
                {
                    searchKey.Week = searchKey.Week + 1;
                    return string.Format("{0}?{1}", Request.Url.GetLeftPart(UriPartial.Path), searchKey.CreateSearchString());
                }
                return string.Empty;
            }
        }

        /// <summary>
        /// get previous week url
        /// </summary>
        protected string BasePageUrl
        {
            get
            {
                var searchKey = new data.SearchKey(this.Context);
                //set url
                if (searchKey.IsValid && searchKey.CheckInDate != null)
                {
                    return string.Format("{0}?{1}", Request.Url.GetLeftPart(UriPartial.Path), searchKey.CreateSearchString());
                }
                return string.Empty;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected string SortText
        {
            get;
            set;
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            var searchKey = new data.SearchKey(this.Context);
            CreatedBy = Sitecore.Context.User.Name;
            SearchMode = searchKey.Mode;

            if (searchKey.IsValid)
            {
                //set search mode properties
                SetSearchModeProperties(SearchMode);

                hdnClientID.Value = searchKey.ClientId.ToString();
                hdnOldOrderLineID.Value = string.Empty;
                if (Request.QueryString["olid"] != null)
                    hdnOldOrderLineID.Value = Request.QueryString["olid"].ToString();

                var search = SearchHelper.SearchHotels(searchKey, GetSearchOptions());

                //get current page
                int currentPage = 1;
                int.TryParse(Request.QueryString["page"], out currentPage);
                //pager config
                if(search.Count > search.PageSize)
                {
                    ResultSize = search.Count;
                    PageSize = search.PageSize;
                    CurrentPage = currentPage;
                }

                var pageData = SearchHelper.GetPageData(search, currentPage);
                //set search result count
                if (searchKey.Location.Type != data.LocationType.Hotel)
                    litSearchText.Text = string.Format("{0} hotel(s) near {1}", search.Count, searchKey.Location.Text);

                //sort visibility
                if (search.Count > 1)
                    divSort.Visible = true;
                //pager visibility
                if (search.TotalPages > 1)
                    divPager.Visible = true;

                //availability and product info if checkin date is not null
                if (searchKey.CheckInDate != null)
                {
                    checkInDate = searchKey.CheckInDate;
                    NoOfNights = searchKey.NoOfNights;
                    //set previous next week nav if checin date is selected
                    if (search.Count > 0)
                        divPreviousNext.Visible = true;

                    IsDateSelected = true;

                    _dates.AddRange(Enumerable.Range(-7, 15)
                        .Select(i => searchKey.CheckInDate.Value.AddDays(7 * searchKey.Week).AddDays(i)));

                    var products = pageData.SelectMany(i => i.Products).ToList();
                    //loyalty availability
                    _availability = HotelSearchHelper.GetAvailabilityAndPriceByDate(
                        products,
                        searchKey,
                        searchKey.CheckInDate.Value.AddDays(7 * searchKey.Week).AddDays(-7),
                        searchKey.CheckInDate.Value.AddDays(7 * searchKey.Week).AddDays(7),
                        searchKey.NoOfNights);

                    //set currency id if the products are availeble
                    if (products.Count > 0)
                        CurrencyId = products.First().CurrencyId;
                }
                
                rptProviders.DataSource = pageData;
                rptProviders.DataBind();
            }
        }

        /// <summary>
        /// GetSearchOption
        /// </summary>
        /// <returns></returns>
        private data.SearchOptions GetSearchOptions()
        {
            if(string.IsNullOrEmpty(Request.QueryString["SortField"]))
            {
                SortText = string.Empty;
                return null;
            }

            data.SortField sortOn = data.SortField.Default;
            data.SortDirection sortDirection = data.SortDirection.Ascending;

            Enum.TryParse(Request.QueryString["SortField"], out sortOn);
            Enum.TryParse(Request.QueryString["SortDirection"], out sortDirection);
            SortText = string.Format("SortField={0}&SortDirection={1}", sortOn.ToString(), sortDirection.ToString());
            return new data.SearchOptions
            {
                SortOn = sortOn,
                SortDirection = sortDirection
            };
        }

        /// <summary>
        /// Set search properties
        /// </summary>
        /// <param name="mode"></param>
        private void SetSearchModeProperties(data.BookingMode mode)
        {
            if(mode == data.BookingMode.Transfer)
            {
                baketText.Text = "Transfer Booking";
                divAddToBasket.Visible = false;
                scCheckOut.Field = "Transfer";
            }
        }

        /// <summary>
        /// Get search text
        /// </summary>
        /// <returns></returns>
        protected string GetSearchText(data.SortField sortOn, data.SortDirection sortDirection)
        {
            return string.Format("SortField={0}&SortDirection={1}", sortOn.ToString(), sortDirection.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Sender"></param>
        /// <param name="e"></param>
        protected void ProviderItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if(e.Item.ItemType == ListItemType.Item || 
                e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var rptRooms = (Repeater)e.Item.FindControl("rptRooms");
                
                if (checkInDate != null)
                {
                    rptRooms.DataSource = ((IProvider)e.Item.DataItem).Products
                        .Where(i => _availability.Any(j => j.VariantSku == i.VariantSku));
                    rptRooms.DataBind();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Sender"></param>
        /// <param name="e"></param>
        protected void PriceItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item ||
                e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var val = (PriceAvailabilityByDate)e.Item.DataItem;
                var spnPastItem = (HtmlGenericControl)e.Item.FindControl("spnPastItem");
                var spnFutureItem = (HtmlGenericControl)e.Item.FindControl("spnFutureItem");
                var spnNoItem = (HtmlGenericControl)e.Item.FindControl("spnNoItem");

                spnNoItem.Visible = (val.Availability <= 0 || val.Price <= 0);
                spnPastItem.Visible = (val.Availability > 0 && val.Price > 0 && checkInDate != null && val.Date < DateTime.Now.Date);
                spnFutureItem.Visible = (val.Availability > 0 && val.Price > 0 && checkInDate != null && val.Date >= DateTime.Now.Date);

                if (spnFutureItem.Visible && val is BBPriceAvailabilityByDate)
                {
                    var valBB = (BBPriceAvailabilityByDate)e.Item.DataItem;
                    spnFutureItem.Attributes.Add("data-PreBookingToken", valBB.PreBookingToken);
                    spnFutureItem.Attributes.Add("data-MealBasisID", valBB.MealBasisId);
                    spnFutureItem.Attributes.Add("data-ProviderID", valBB.ProviderId);
                    spnFutureItem.Attributes.Add("data-PropertyRefId", valBB.PropertyReferenceId);
                    spnFutureItem.Attributes.Add("data-RoomInfo", valBB.RoomInfo);
                    spnFutureItem.Attributes.Add("data-OldPrice", valBB.OldPrice.ToString("#.##"));
                    spnFutureItem.Attributes.Add("data-MarginId", valBB.MarginId.ToString());
                    spnFutureItem.Attributes.Add("data-MarginPercentage", valBB.MarginPercentage.ToString());
                    spnFutureItem.Attributes.Add("data-BBCurrencyId", valBB.BBCurrencyId);
                    spnFutureItem.Attributes.Add("data-IsDirect", valBB.IsDirectStock.ToString());
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected List<DateTime> GetDates()
        {
            return _dates;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        protected IEnumerable<PriceAvailabilityByDate> GetPriceByDate(IProduct product)
        {
            foreach (var date in _dates)
            {
                if (product.Type == ProductType.Room || product.Type == ProductType.Package)
                {
                    if (_availability.Any(i => i.VariantSku == product.VariantSku && i.Date == date))
                        yield return _availability.FirstOrDefault(i => i.VariantSku == product.VariantSku && i.Date == date);
                    else
                        yield return new PriceAvailabilityByDate() { Date = date };
                }
                else if (product.Type == ProductType.BedBanks)
                {
                    var bbProduct = (BedBankProduct)product;
                    var bbAvailability = _availability.Where(i => i.Type == ProductType.BedBanks)
                        .Cast<BBPriceAvailabilityByDate>();
                    if (bbAvailability.Any(i => i.ProviderId == bbProduct.ProviderId && i.Date == date))
                        yield return bbAvailability.FirstOrDefault(i => i.ProviderId == bbProduct.ProviderId && i.Date == date && i.RoomInfo.Equals(bbProduct.Name));
                    else
                        yield return new BBPriceAvailabilityByDate() { Date = date };
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="products"></param>
        /// <returns></returns>
        protected string GetVaraintSkus(IList<IProduct> products)
        {
            return string.Join(",", products.Where(i => i.Type != ProductType.BedBanks)
                .Select(i => i.VariantSku));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="availabilityInfo"></param>
        /// <returns></returns>
        protected string GetDateClass(PriceAvailabilityByDate availabilityInfo)
        {
            if (availabilityInfo.Date == checkInDate)
                return availabilityInfo.Availability > 0 ? "body-text accent54-bg accent36-f" : "availability-zero accent36-f";

            if (availabilityInfo.Date < DateTime.Now.Date || availabilityInfo.Availability <= 0)
                return "disable-box";

            return "body-text";
        }
    }
}