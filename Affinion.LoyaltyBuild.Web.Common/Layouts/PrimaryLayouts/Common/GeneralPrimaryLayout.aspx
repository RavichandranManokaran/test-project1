﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GeneralPrimaryLayout.aspx.cs" Inherits="Affinion.LoyaltyBuild.Web.Common.Layouts.PrimaryLayouts.Common.GeneralPrimaryLayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Register TagPrefix="wc" Namespace="Affinion.LoyaltyBuild.Web.Common.WebControls" Assembly="Affinion.LoyaltyBuild.Web.Common" %>
<%@ OutputCache Location="None" VaryByParam="none" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">                  
  <head>
    <title>
    
    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="CODE_LANGUAGE" content="C#" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="/default.css" rel="stylesheet" />
    <sc:VisitorIdentification runat="server" />
    
    <script src="~/Resources/scripts/jquery.min.js"></script>
    <script src="~/Resources/bootstrap_assets/javascripts/bootstrap.min.js"></script> 

    <link rel="stylesheet" href="~/Resources/styles/bootstrap.min.css">   
    <link rel="stylesheet" href="~/Resources/styles/affinion.min.css">
    <link rel="stylesheet" href="~/Resources/styles/theme2/theme2.css"> 

  </head>
  <body>
  <form method="post" runat="server" id="mainform">

      <div class="container-outer width-full overflow-hidden-att">

            <%--body content goes here. Grid blocks should be added before placing content--%>
            <sc:Placeholder ID="GridBlock" Key="GridBlock" runat="server" />
            <%--<wc:DynamicKeyPlaceholder Key="GridBlock" runat="server" />--%>
            
            <!-- Site footer -->
            <div class="row footer-bottom margin-common">
                <footer class="footer">

                    footer goes here

                </footer>
            </div>
       </div>
  
  </form>
  </body>
</html>

