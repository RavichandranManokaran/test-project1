﻿using Affinion.LoyaltyBuild.Model.Product;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.ViewModels
{
    [Serializable]
    public class BasketRequest
    {
        /// <summary>
        /// Room type
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public ProductType RoomType { get; set; }
    }
}