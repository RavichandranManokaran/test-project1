﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           GoogleLocationData.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.ExternalServices.LocationService
/// Description:           GoogleLocationData code will be used as a data model class to either add or retrieve 
///                        data in a structured manner.


namespace Affinion.LoyaltyBuild.ExternalServices.LocationService.DataObjects
{
    /// <summary>
    /// DTO Class for Google Location data
    /// </summary>
    public class GoogleLocationData
    {

        #region Public Variables 

        /// <summary>
        /// Gets or Sets Location Name
        /// </summary>
        public string LocationName { get; set; }

        /// <summary>
        /// Gets or Sets Latitude
        /// </summary>
        public string Latitude { get; set; }

        /// <summary>
        /// Gets or Sets Longitude
        /// </summary>
        public string Longitude { get; set; }

        /// <summary>
        /// Gets or Sets the Description
        /// </summary>
        public string Description { get; set; } 

        #endregion
    }
}
