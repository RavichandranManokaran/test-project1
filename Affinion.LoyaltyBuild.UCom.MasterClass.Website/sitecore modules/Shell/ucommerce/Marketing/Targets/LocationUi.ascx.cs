﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.EntitiesV2;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using UCommerce.Presentation.Views.Marketing;
using UCommerce.Presentation.Web.Controls;
using UCommerce.Presentation.Web.Marketing;
using Affinion.LoyaltyBuild.UCom.Common.Constants;
using Affinion.LoyaltyBuild.UCom.MasterClass.Website.Configuration;
using Affinion.LoyaltyBuild.Common.Instrumentation;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.ucommerce.Marketing.Targets
{
    public partial class LocationUi : ViewEnabledControl<IEditCampaignItemView>, ITargetUi
    {
        List<string> datatypeList;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (datatypeList == null)
            {
                //DataType Location = DataType.All().Where(x => x.TypeName == DataTypeConstant.Location && !x.Deleted).ToList().FirstOrDefault();
                //if (Location != null)
                //{
                //    datatypeList = Location.DataTypeEnums.ToList();
                //}
                //datatypeList.Insert(0, new DataTypeEnum() { Value = "" });
                datatypeList = SiteConfiguration.GetSiteCoreLocation();
            }
            LocationTarget dataSource = this.DataSource as LocationTarget;
            this.DeleteButton.OnClientClick = string.Format("return confirm('Delete');");
            string[] locations = dataSource.Location.Split('|');
            chklocation.Items.Clear();
            foreach (var item in datatypeList)
            {
                ListItem items = new ListItem();
                items.Text = item.ToString();
                items.Value = (item).ToString();
                items.Selected = locations.Any(x => x.ToString() == (item).ToString());
                chklocation.Items.Add(items);
            }
            this.ReadOnlyMessage.Text = this.TargetToString(dataSource);
        }

        protected void SaveButton_Click(object sender, ImageClickEventArgs e)
        {
            LocationTarget dataSource = this.DataSource as LocationTarget;
            string selectedLocation = string.Empty;
            foreach (ListItem item in chklocation.Items)
            {
                if (item.Selected)
                {
                    selectedLocation += item.Value + "|";
                }
            }
            if (selectedLocation.Length > 0)
            {
                selectedLocation = selectedLocation.Substring(0, selectedLocation.Length - 1);
            }

            dataSource.Location = selectedLocation;
            dataSource.Save();
            this.ReadOnlyMessage.Text = this.TargetToString(dataSource);
            this.ToggleEditor();
        }

        private void ToggleEditor()
        {
            this.NonEditModePlaceHolder.Visible = !this.NonEditModePlaceHolder.Visible;
            this.EditButton.Visible = !this.EditButton.Visible;
            this.EditModePlaceHolder.Visible = !this.EditModePlaceHolder.Visible;
            this.SaveButton.Visible = !this.SaveButton.Visible;
        }

        private string TargetToString(LocationTarget LocationTarget)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendFormat("City ");
            stringBuilder.AppendFormat("{0} ", string.Join("\n", LocationTarget.Location.Split('|')));
            return stringBuilder.ToString();
        }

        protected void DeleteButton_Click(object sender, ImageClickEventArgs e)
        {
            LocationTarget dataSource = this.DataSource as LocationTarget;
            if (dataSource != null)
            {
                dataSource.Delete();
            }
            this.Page.Response.Redirect(this.Page.Request.Url.ToString(), true);
        }

        protected void EditButton_Click(object sender, ImageClickEventArgs e)
        {
            this.ToggleEditor();
        }


        public bool EnabledForApply
        {
            get;
            set;
        }

        public bool EnabledForDisplay
        {
            get;
            set;
        }

        public object DataSource
        {
            get;
            set;

        }

        public object New()
        {
            var LocationTarget = new LocationTarget();
            try
            {
                LocationTarget.CampaignItem = View.CampaignItem;
                LocationTarget.EnabledForApply = true;
                LocationTarget.Location = string.Empty;
                LocationTarget.Save();
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.MasterClassWebsite, ex, this);
                throw;
            }
            this.DataSource = LocationTarget;
            return LocationTarget;
        }
    }
}