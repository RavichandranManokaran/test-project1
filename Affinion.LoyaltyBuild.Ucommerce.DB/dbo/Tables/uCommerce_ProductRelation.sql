﻿CREATE TABLE [dbo].[uCommerce_ProductRelation] (
    [ProductRelationId]     INT IDENTITY (1, 1) NOT NULL,
    [ProductId]             INT NOT NULL,
    [RelatedProductId]      INT NOT NULL,
    [ProductRelationTypeId] INT NOT NULL,
    CONSTRAINT [PK_uCommerce_ProductRelation2] PRIMARY KEY CLUSTERED ([ProductRelationId] ASC),
    CONSTRAINT [FK_uCommerce_ProductRelation_uCommerce_Product2] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[uCommerce_Product] ([ProductId]),
    CONSTRAINT [FK_uCommerce_ProductRelation_uCommerce_Product3] FOREIGN KEY ([RelatedProductId]) REFERENCES [dbo].[uCommerce_Product] ([ProductId]),
    CONSTRAINT [FK_uCommerce_ProductRelation_uCommerce_ProductRelationType] FOREIGN KEY ([ProductRelationTypeId]) REFERENCES [dbo].[uCommerce_ProductRelationType] ([ProductRelationTypeId])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_uCommerce_ProductRelation]
    ON [dbo].[uCommerce_ProductRelation]([ProductId] ASC, [RelatedProductId] ASC, [ProductRelationTypeId] ASC);

