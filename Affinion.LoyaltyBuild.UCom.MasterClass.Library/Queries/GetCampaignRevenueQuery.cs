﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using NHibernate;
using NHibernate.Transform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2.Queries;
using Affinion.LoyaltyBuild.Common.Instrumentation;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Queries
{
    public class GetCampaignRevenueQuery : ICannedQuery<CampaignRevenue>
    {
        /// <summary>
        /// CampaignItemId
        /// </summary>
        private readonly int campaignItemId;

        /// <summary>
        /// CampaignRevenueQuery 
        /// </summary>
        /// <param name="campaignItemId"></param>
        public GetCampaignRevenueQuery(int campaignItemId)
        {
            this.campaignItemId = campaignItemId;
        }

        /// <summary>
        /// IEnumerable type CampaignRevenue
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public IEnumerable<CampaignRevenue> Execute(ISession session)
        {
            try
            {
                return session.CreateSQLQuery("exec LB_GetCampaignRevenue :@campaignItemId").SetParameter("@campaignItemId", campaignItemId)
                .SetResultTransformer(Transformers.AliasToBean<CampaignRevenue>())
                .List<CampaignRevenue>().ToList();
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.MasterClassBusinessLogic, ex, this);
                return new List<CampaignRevenue>().AsEnumerable();
            }

        }


    }
}
