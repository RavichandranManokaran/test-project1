﻿using Affinion.LoyaltyBuild.Model.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.ViewModels
{
    public class PriceAvailabilityByDate
    {
        public string Name { get; set; }

        public string Sku { get; set; }

        public string VariantSku { get; set; }

        public string RefId { get; set; }

        public decimal Price { get; set; }

        public DateTime Date { get; set; }

        public int Availability { get; set; }

        public int NoOfNights { get; set; }

        public int CurrencyId { get; set; }

        public int? NoOfAdults { get; set; }

        public int? NoOfChildren { get; set; }

        public ProductType Type { get; set; }

    }
}