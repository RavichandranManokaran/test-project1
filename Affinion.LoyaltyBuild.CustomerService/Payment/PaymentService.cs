﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model.Payment;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Helpers;
using System.Transactions;
using Affinion.LoyaltyBuild.Common;
using System.Data.SqlClient;
/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/
namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Payment
{
    /// <summary>
    /// Service that provides methods to perform all Payment related actions
    /// </summary>
    public class PaymentService : IPaymentService
    {
        //private string
        private string _connectionString = "Ucommerce";
        
        /// <summary>
        /// Constructor
        /// </summary>
        public PaymentService()
        {
        }

        #region IPaymentService Implementation

        /// <summary>
        /// Gets the OrderlinePayment Information 
        /// </summary>
        /// <param name="orderLineId">orderLineId</param>
        /// <returns>Object of OrderLinePaymentInfo</returns>
        public OrderLinePaymentInfo GetOrderLinePaymentInfo(int orderLineId)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);

            var sql = "LB_GetPaymentHistory";

            //the result object
            OrderLinePaymentInfo result = null;

            //create command
            using (var command = db.GetStoredProcCommand(sql))
            {
                db.AddInParameter(command, "OrderLineId", DbType.Int32, orderLineId);

                //execute reader
                using (var reader = db.ExecuteReader(command))
                {
                    //get booking referece
                    if (reader.Read())
                    {
                        result = new OrderLinePaymentInfo
                        {
                            OrderLineId = orderLineId,
                            BookingReference = reader.Value<string>(1)
                        };

                        //get total due by currency
                        if (reader.NextResult())
                            result.DueByCurrencies = GetDueByCurrencies(reader).ToList();

                        //get due history
                        if (reader.NextResult())
                            result.DueHistory = GetDueHistory(reader).ToList();

                        //get payment history
                        if (reader.NextResult())
                            result.PaymentHistory = GetPaymentHistory(reader).ToList();

                        //get payment history
                        if (reader.NextResult())
                            result.DiscountHistory = GetDiscountHistory(reader).ToList();
                    }

                }
            }
            return result;
        }


        /// <summary>
        /// Saves the due details
        /// </summary>
        /// <param name="dueDetails">dueDetails</param>
        /// <returns>ValidationResponse object with success/failure flag</returns>
        public ValidationResponse SaveDue(DueDetails dueDetails)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "LB_SaveOrderLineDue";

            //the result object
            var result = new ValidationResponse();

            //create command
            
            //create command
            using (var scope = new TransactionScope())
            {
                try
                {
                    var command = db.GetStoredProcCommand(sql);
                    db.AddInParameter(command, "OrderLineDueId", DbType.Int32, dueDetails.OrderLineDueId);
                    db.AddInParameter(command, "OrderLineId", DbType.Int32, dueDetails.OrderLineId);
                    //db.AddInParameter(command, "SubrateItemCode", DbType.String, null);
                    //db.AddInParameter(command, "OrderLinePaymentId", DbType.Int32,null);
                    db.AddInParameter(command, "CurrencyTypeId", DbType.Int32, dueDetails.CurrencyTypeId);
                    db.AddInParameter(command, "DueTypeId", DbType.Int32, dueDetails.DueTypeId);
                    db.AddInParameter(command, "Amount", DbType.Decimal, dueDetails.Amount);
                    db.AddInParameter(command, "CommissionAmount", DbType.Decimal, dueDetails.CommissionAmount);
                    //db.AddInParameter(command, "TransferAmount", DbType.Decimal, dueDetails.TransferAmount);
                   // db.AddInParameter(command, "CancellationAmount", DbType.Decimal, dueDetails.CancellationAmount);
                   // db.AddInParameter(command, "PaymentMethodId", DbType.Int32, dueDetails.PaymentMethodId);
                   // db.AddInParameter(command, "CollectionByProviderAmount", DbType.Decimal, dueDetails.CollectionByProviderAmount);
                   // db.AddInParameter(command, "ClientAmount", DbType.Decimal, dueDetails.ClientAmount);
                    db.AddInParameter(command, "ProviderAmount", DbType.Decimal, dueDetails.ProviderAmount);
                    db.AddInParameter(command, "ProcessingFeeAmount", DbType.Decimal, dueDetails.ProcessingFeeAmount);
                    db.AddInParameter(command, "Note", DbType.String, dueDetails.Note);
                    db.AddInParameter(command, "CreatedDate", DbType.DateTime, DateTime.Now);
                    db.AddInParameter(command, "CreatedBy", DbType.String, dueDetails.CreatedBy);
                    //execute data
                    result.Id = Convert.ToInt32(db.ExecuteScalar(command));

                    var statusCommand = db.GetStoredProcCommand("LB_UpdateStatusOnDueAndPay");
                    db.AddInParameter(statusCommand, "OrderLineId", DbType.Int32, dueDetails.OrderLineId);
                    db.AddInParameter(statusCommand, "CreatedBy", DbType.String, dueDetails.CreatedBy);
                    db.ExecuteNonQuery(statusCommand);

                    result.IsSuccess = true;
                    result.Message = "Due details saved successfully";
                    scope.Complete();
                }
                catch
                {
                    result.Message = "Error saving due";
                }
            }

            //return result
            return result;
        }

        /// <summary>
        /// Deletes a due item with given DueId
        /// </summary>
        /// <param name="dueId">dueId</param>
        /// <returns>ValidationResponse object with success/failure flag</returns>
        public ValidationResponse DeleteDue(int dueId, string reason)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "LB_DeleteDue";

            //the result object
            var result = new ValidationResponse();

            //create command
            using (var scope = new TransactionScope())
            {
                try
                {
                    var command = db.GetStoredProcCommand(sql);
                    db.AddInParameter(command, "OrderLineDueId", DbType.Int32, dueId);
                    db.AddInParameter(command, "Reason", DbType.String, reason);
                    db.ExecuteNonQuery(command);

                    result.IsSuccess = true;
                    result.Message = "Due deleted.";
                    scope.Complete();
                }
                catch
                {
                    result.Message = "Error deleting due";
                }
            }
            
            //return result
            return result;
        }


        /// <summary>
        /// Get the Due Details for the particular DueId
        /// </summary>
        /// <param name="dueId">dueId</param>
        /// <returns>DueDetails object</returns>
        public DueDetails GetDueDetail(int dueId)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            //var db = factory.Create(_connectionString);
            var sql = "LB_GetDueDetail";
            //the result object

            DueDetails result = null;

            //create command
            using (var command = db.GetStoredProcCommand(sql))
            {
               
                db.AddInParameter(command, "DueId", DbType.Int32, dueId);
                //execute reader

                using (var reader = db.ExecuteReader(command))
                {
                    if (reader.Read())
                    {
                        result = new DueDetails
                        {
                            OrderLineDueId = reader.Value<int>("OrderLineDueId"),
                            DueTypeId = reader.Value<Guid>("DueTypeId"),
                            CurrencyTypeId = reader.Value<int>("CurrencyTypeId"),
                            Amount = reader.Value<decimal>("TotalAmount"),
                            CommissionAmount = reader.Value<decimal>("CommissionAmount"),
                           // TransferAmount = reader.Value<decimal>("TransferAmount"),
                           // ClientAmount = reader.Value<decimal>("ClientAmount"),
                            ProviderAmount = reader.Value<decimal>("ProviderAmount"),
                           // CollectionByProviderAmount = reader.Value<decimal>("CollectionByProviderAmount"),
                            PaymentMethodId=reader.Value<int>("PaymentMethodId"),
                            ProcessingFeeAmount=reader.Value<decimal>("ProcessingFeeAmount"),
                            Note = reader.Value<string>("Note")

                        };
                    }

                }
            }

            //return result
            return result;
        }

        /// <summary>
        /// Add a payment 
        /// </summary>
        /// <param name="payment">payment</param>
        /// <returns>ValidationResponse object with success/failure flag</returns>
        public ValidationResponse AddPayment(PaymentDetail payment)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "LB_AddBookingPayment";

            //the result object
            var result = new ValidationResponse();

            //create command
            using (var scope = new TransactionScope())
            {
                try
                {
                    var command = db.GetStoredProcCommand(sql);
                    db.AddInParameter(command, "OrderLineId", DbType.Int32, payment.OrderLineID);
                    db.AddInParameter(command, "CurrencyId", DbType.Int32, payment.CurrencyTypeId);
                    db.AddInParameter(command, "OrderPaymentId", DbType.Int32, payment.OrderLinePaymentID);
                    db.AddInParameter(command, "PaymentMethodId", DbType.Int32, payment.PaymentMethodID);
                    db.AddInParameter(command, "Amount", DbType.Decimal, payment.Amount);
                    db.AddInParameter(command, "ReferenceId", DbType.String, payment.ReferenceID);
                    db.AddInParameter(command, "CreatedBy", DbType.String, payment.CreatedBy);
                    if (payment is CardPaymentDetail)
                    {
                        db.AddInParameter(command, "CommissionAmount", DbType.Decimal, ((CardPaymentDetail)payment).CommissionAmount);
                        db.AddInParameter(command, "ProviderAmount", DbType.Decimal, ((CardPaymentDetail)payment).ProviderAmount);
                        db.AddInParameter(command, "ProcessingFee", DbType.Decimal, ((CardPaymentDetail)payment).ProcessingFee);
                    }
                    db.ExecuteNonQuery(command);
                    
                    var statusCommand = db.GetStoredProcCommand("LB_UpdateStatusOnDueAndPay");
                    db.AddInParameter(statusCommand, "OrderLineId", DbType.Int32, payment.OrderLineID);
                    db.AddInParameter(statusCommand, "CreatedBy", DbType.String, payment.CreatedBy);
                    db.ExecuteNonQuery(statusCommand);

                    result.IsSuccess = true;
                    result.Message = "Your payment is saved Successfully";
                    scope.Complete();
                }
                catch
                {
                    result.IsSuccess = false;
                    result.Message = "Error in saving payment";
                }
            }

            //return result
            return result;
        }

        /// <summary>
        /// Gets the list of payment methods
        /// </summary>
        /// <returns> List of Payment menthods</returns>
        public List<PaymentMethod> GetPaymentMethods(string clientId)
        {
            List<PaymentMethod> paymentInfo = new List<PaymentMethod>();
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "LB_GetPaymentMethodsByClientID";
           
            
            
            using (var command = db.GetStoredProcCommand(sql))
            {
                db.AddInParameter(command, "@ClientId", DbType.String, clientId);
                using (var reader = db.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        var tmpPaymentMethod = new PaymentMethod
                        {
                            Id = reader.GetInt32(0),
                            Name = reader.GetString(1)
                        };
                    paymentInfo.Add(tmpPaymentMethod);
                    }                       
                }
            } 
            return paymentInfo;
        }

        /// <summary>
        /// Gets the list of currency details
        /// </summary>
        /// <returns> List of currency details</returns>
        public List<Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model.CurrencyDetail> GetCurrencyListDetails()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "LB_GetAllCurrencies";

            var res = new List<Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model.CurrencyDetail>();

            using (var command = db.GetStoredProcCommand(sql))
            {
                using (var reader = db.ExecuteReader(command))
                {
                    while (reader.Read())
                        res.Add(new Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model.CurrencyDetail
                            {
                                //CurrencyGUID  = reader.GetString(2),
                                CurrencyID = reader.GetInt32(0),
                                Name = reader.GetString(1)

                            });
                }

                return res;
            }

        }

        /// <summary>
        /// Gets currencyID based on OrderlineID
        /// </summary>
        /// <returns> Gets currencyID based on OrderlineID</returns>
        public List<int> GetCurrencyDetailsbyOrderlineID(int orderlineID)
        {
            List<int> currencydetailList = new List<int>();
            int currencyID=0;
            int exchangerate = 0;
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "LB_GetCurrencyIDByOrderlineID";
            using (var command = db.GetStoredProcCommand(sql))
            {
                db.AddInParameter(command, "OrderLineId", DbType.Int32, orderlineID);
                using (var reader = db.ExecuteReader(command))
                {
                    if (reader.Read())
                    {
                        currencyID = reader.GetInt32(0);
                        exchangerate = reader.GetInt32(1);
                    }                       
                    currencydetailList.Add(currencyID);
                    currencydetailList.Add(exchangerate);
                }
            }                        
            return currencydetailList;
        }

        /// <summary>
        /// Gets order guid based on orderlineid
        /// </summary>
        /// <returns> Gets order guid based on orderlineid</returns>
        public string GetOrderGuidByOrderlineId(int orderlineID)
        {
            string orderGuid = string.Empty ;
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "LB_GetOrderGuidByOrderlineID";
            using (var command = db.GetStoredProcCommand(sql))
            {
                db.AddInParameter(command, "OrderLineId", DbType.Int32, orderlineID);
                using (var reader = db.ExecuteReader(command))
                {
                    if (reader.Read())
                    {
                        orderGuid = reader.GetString(0);
                    }
                }
            }
            return orderGuid;
        }

        /// <summary>
        /// Gets the payment reasons
        /// </summary>
        /// <returns> List of Payment Reasons</returns>
        public List<PaymentReason> GetPaymentReasons()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "LB_GetPaymentReason";

            var res = new List<PaymentReason>();

            using (var command = db.GetStoredProcCommand(sql))
            {
                using (var reader = db.ExecuteReader(command))
                {
                    while (reader.Read())
                        res.Add(new PaymentReason
                            {
                                DueId = reader.GetGuid(0),
                                DueReason = reader.GetString(1)
                            });
                }

                return res;
            }

        }

        /// <summary>
        /// Gets the available payment list for refund
        /// </summary>
        /// <param name="orderLineId">orderLineId</param>
        /// <param name="paymentType">paymentType</param>
        /// <param name="currencyType">currencyType</param>
        /// <returns>List of Payment History Item</returns>
        public List<RefundableItem> GetAvailablePaymentsForRefund(int orderLineId, int paymentType, int currencyType)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            //var db = factory.Create(_connectionString);
            var sql = "LB_GetPaymentsWithoutRefunds";
            //the result object

            var result = new List<RefundableItem>();

            //create command
            using (var command = db.GetStoredProcCommand(sql))
            {
                db.AddInParameter(command, "OrderLineId", DbType.Int32, orderLineId);
                db.AddInParameter(command, "PaymentTypeId", DbType.Int32, paymentType);
                db.AddInParameter(command, "CurrencyTypeId", DbType.Int32, currencyType);
                //execute reader

                using (var reader = db.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        var item = new RefundableItem
                        {
                            ProviderAmount = reader.Value<decimal>("ProviderAmount"),
                            CommissionAmount = reader.Value<decimal>("CommissionAmount"),
                            ProcessingFee = reader.Value<decimal>("ProcessingFee"),
                            Amount = reader.Value<decimal>("Amount"),
                            ReferenceId = reader.Value<string>("ReferenceId"),
                            CreatedDate = reader.Value<DateTime>("Createddate"),
                        };
                        result.Add(item);
                    }
                }
            }

            //return result
            return result;
        }
                   
        #endregion

        #region Private Methods

        /// <summary>
        /// Get Payment History details
        /// </summary>
        /// <param name="reader">reader</param>
        /// <returns>Emumerable - PaymentHistoryItems</returns>
        private IEnumerable<PaymentHistoryItem> GetPaymentHistory(IDataReader reader)
        {
            while (reader.Read())
            {
                var item = new PaymentHistoryItem
                {
                    OrderLinePaymentid = reader.Value<int>("OrderLinePaymentId"),
                    OrderLineId = reader.Value<int>("oderLineID"),
                    PaymentMethod = reader.Value<string>("PaymentMethod"),
                    PaymentReason = reader.Value<string>("PaymentReason"),
                    Amount = reader.Value<decimal>("Amount"),
                    ReferenceId = reader.Value<string>("ReferenceId"),
                    CreatedDate = reader.Value<DateTime>("Createddate"),
                    CreatedBy = reader.Value<string>("Createdby"),
                    CurrencyTypeId = reader.Value<int>("Currency")
                };

                yield return item;
            }
        }

        public List<TotalDueItem> GetDuesByOrderlineID(int orderLineId, int? currencyTypeId)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "LB_GetDuesByOrderLineID";
            
            //the result object
            var result = new List<TotalDueItem>();

            //create command
            using (var command = db.GetStoredProcCommand(sql))
            {
                db.AddInParameter(command, "OrderLineId", DbType.Int32, orderLineId);
                command.Parameters.Add(new SqlParameter{SqlValue=currencyTypeId ?? (object)DBNull.Value,ParameterName="CurrencyTypeId" }  );              

                //execute reader
                using (var reader = db.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        var item = new TotalDueItem
                        {
                            Amount = Math.Round(reader.Value<decimal>(0),2),
                            CurrencyTypeId = reader.Value<int>(1),                            
                            OrderLineID = reader.Value<int>(2)
                        };
                        result.Add(item);
                    }
                }
            }           
            return result;
        }


        /// <summary>
        /// Get Due details by Currency
        /// </summary>
        /// <param name="reader">reader</param>
        /// <returns>Enumerable - Total Due Item</returns>
        private IEnumerable<TotalDueItem> GetDueByCurrencies(IDataReader reader)
        {
            while (reader.Read())
            {
                var item = new TotalDueItem
                {

                    Amount = reader.Value<decimal>("Total"),
                    CurrencyTypeId = reader.Value<int>("Currency")
                };

                yield return item;
            }

        }

        /// <summary>
        /// Get Due History Details
        /// </summary>
        /// <param name="reader">reader</param>
        /// <returns>Enumerable - Due History Item</returns>
        private IEnumerable<DueHistoryItem> GetDueHistory(IDataReader reader)
        {

            while (reader.Read())
            {
                var item = new DueHistoryItem
                {
                    OrderLineDueId = reader.Value<int>("OrderLineDueId"),
                    OrderLineId = reader.Value<int>("OrderLineID"),
                    CurrencyTypeId = reader.Value<int>("Currency"),
                    Amount = reader.Value<decimal>("Amount"),
                    PaymentMethod = reader.Value<string>("PaymentMethod"),
                    Reason = reader.Value<string>("Reason"),
                    CreatedDate = reader.Value<DateTime>("CreatedDate"),
                    CreatedBy = reader.Value<string>("CreatedBy"),
                    CanBeDeleted = reader.Value<bool>("CanBeDeleted")
                };

                yield return item;
            }

        }

        /// <summary>
        /// Get Discount History information
        /// </summary>
        /// <param name="reader">reader</param>
        /// <returns>Enumerable - DiscountHistoryItem</returns>
        private IEnumerable<DiscountHistoryItem> GetDiscountHistory(IDataReader reader)
        {
            while (reader.Read())
            {
                var item = new DiscountHistoryItem
                {
                    DiscountName = reader.Value<string>("Name"),
                    Amount = reader.Value<decimal>("Amount"),
                    CurrencyTypeId = reader.Value<int>("Currency")
                };

                yield return item;
            }
        }
        #endregion


        /// <summary>
        /// Gets the client and provider id based on OrderlineID or orderId
        /// </summary>
        /// <returns> List of currency details</returns>
        public FinanceCode GetClientAndProviderID(int orderId,int orderLineId)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "LB_GetClientAndProviderID";

            var objFinanceCode = new FinanceCode();

            using (var command = db.GetStoredProcCommand(sql))
            {
                db.AddInParameter(command, "OrderId", DbType.Int32, orderId);
                db.AddInParameter(command, "OrderLineId", DbType.Int32, orderLineId);

                using (var reader = db.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        objFinanceCode.ClientId = reader.Value<string>("ClientId");
                        objFinanceCode.ProviderId = reader.Value<string>("ProviderId").ToString();
                        objFinanceCode.Currency = reader.Value<string>("Currency");
                    }
                }
            }
            return objFinanceCode;

        }

        /// <summary>
        /// Insert finance code  based on OrderlineID
        /// </summary>
        /// <returns> bool</returns>
        public ValidationResponse AddFinaceCode(FinanceCode objFinanceCode)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "LB_AddFinanceCode";

            var result = new ValidationResponse();

            using (var scope = new TransactionScope())
            {
                try
                {
                    var command = db.GetStoredProcCommand(sql);
                    db.AddInParameter(command, "OrderId", DbType.Int32, objFinanceCode.OrderId);
                    db.AddInParameter(command, "OrderLineId", DbType.Int32, objFinanceCode.OrderLineId);
                    db.AddInParameter(command, "Company", DbType.Int32, objFinanceCode.Company);
                    db.AddInParameter(command, "MerchantIdentificationNo", DbType.Int32, objFinanceCode.MerchantIdentificationNo);
                    db.AddInParameter(command, "ClientName", DbType.String, objFinanceCode.ClientName);
                    db.AddInParameter(command, "Department", DbType.String, objFinanceCode.Department);
                    db.AddInParameter(command, "ProductCode", DbType.String, objFinanceCode.ProductCode);
                    db.AddInParameter(command, "GlCode", DbType.String, objFinanceCode.GlCode);
                    db.AddInParameter(command, "ClientCode", DbType.String, objFinanceCode.ClientCode);
                    db.AddInParameter(command, "Description", DbType.String, objFinanceCode.Description);
                    db.ExecuteNonQuery(command);

                    result.IsSuccess = true;
                    result.Message = "Finance code details are saved successfully";
                    scope.Complete();
                }
                catch
                {
                    result.IsSuccess = false;
                    result.Message = "Finance code details are not saved successfully";
                }
            }

            return result;
        }
    }
}
