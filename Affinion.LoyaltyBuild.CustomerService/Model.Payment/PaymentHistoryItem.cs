﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/
namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model.Payment
{
    /// <summary>
    /// Entity containing Payment History item related details
    /// </summary>
    public class PaymentHistoryItem
    {
        /// <summary>
        /// Orderlinedueid
        /// </summary>
        public int OrderLineDueId { get; set; }

        /// <summary>
        /// Orderlineid
        /// </summary>
        public int OrderLineId { get; set; }

        /// <summary>
        /// Currency types
        /// </summary>
        public int CurrencyTypeId { get; set; }
        
        /// <summary>
        /// Amount
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// OrderpaymentId
        /// </summary>      
        public int OrderLinePaymentid { get; set; }

        /// <summary>
        /// Payment method
        /// </summary>
        public string PaymentMethod { get; set; }


        /// <summary>
        /// Payment reason
        /// </summary>
        public string PaymentReason { get; set; }


        /// <summary>
        /// payment reason
        /// </summary>
        public string Reason { get; set; }

        /// <summary>
        /// Realeax id
        /// </summary>
        public string ReferenceId { get; set; }

        /// <summary>
        /// IsDeleted Flag
        /// </summary>
        public int IsDeleted { get; set; }

        /// <summary>
        /// payment method
        /// </summary>
        public int PaymnetMethodId { get; set; }

        /// <summary>
        /// Available amount
        /// </summary>
        public decimal AvailableAmount { get; set; }

        /// <summary>
        /// Created By
        /// </summary>
        public string CreatedBy { get; set; }

        /// <summary>
        /// Created Date
        /// </summary>
        public DateTime CreatedDate { get; set; }
    }
}
