﻿namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    #region Using Statements
    using Affinion.Loyaltybuild.BusinessLogic.Helper;
    using Affinion.LoyaltyBuild.Common;
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using LBDataAccess = Affinion.LoyaltyBuild.DataAccess.Models;
    using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
    using Affinion.LoyaltyBuild.Search.Helpers;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Linq;
    using System.Web.UI.WebControls;
    using System.Text.RegularExpressions;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Customer;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Web.ClientPortal.Helper;
    using UCommerce.EntitiesV2;


    #endregion

    public partial class OfflinePortalAddCustomerQuery : BaseSublayout
    {

        #region private constant variable

        /// <summary>
        /// Client item path
        /// </summary>
        private const string ClientItemPath = "/sitecore/content/admin-portal/client-setup/{0}";
        Database contextDb = Sitecore.Context.Database;      

      

        private QueryService _customerQueryService = new QueryService();
        private CustomerQuery _customerQuery;
        private string SelectText = "Please Select";
        private string _createdBy = string.Empty;
        //static int customerId;
        private const string _errorCredirCardMessage = "Customer query content having credit card no.";
        
        #endregion

        #region Page Load       
        private void Page_Load(object sender, EventArgs e)
        {
            Page.MaintainScrollPositionOnPostBack = false;
            BindSitecoreErrormsgs();
            _createdBy = SitecoreUserProfileHelper.GetItem(UserProfileKeys.CreatedBy);

            if (!IsPostBack)
            {
                InitPage();
                ButtonInsert.Enabled = false;
            }

        }
        #endregion

        #region protected Methods
        protected ValidationResponse ValidationResponse { get; private set; }
        protected void Search_Click(object sender, EventArgs e)
        {
            //add.Visible = false;
            //search.Visible = true;
        }

        protected void ButtonInsert_Click(object sender, EventArgs e)
        {

            if (Page.IsValid)
            {
                ValidationResponse saveFlag = new ValidationResponse();

                if (!string.IsNullOrEmpty(CustomerId.Value))
                {
                    if (HasCreditCardNumber(Notes.Value))
                    {
                        saveFlag.IsSuccess = false;
                        saveFlag.Message = _errorCredirCardMessage;
                        ValidationResponse = saveFlag;
                        if (Convert.ToInt32(CustomerId.Value) > 0)
                            ButtonInsert.Visible = true;
                        return;
                    }

                    if (Convert.ToInt32(CustomerId.Value) > 0)
                    {
                        _customerQuery = new CustomerQuery();
                        _customerQuery.QueryTypeId = Convert.ToInt32(QueryType.SelectedValue);
                        _customerQuery.ClientId = ClientDropDownSearch.SelectedValue;
                        _customerQuery.CampaignGroupID = ddlCompaignGroup.SelectedValue;
                        _customerQuery.ProviderId = ddlProvider.SelectedValue;
                        _customerQuery.Content = Notes.Value;
                        _customerQuery.IsSolved = IsSolved.SelectedItem.Value == "Yes" ? true : false;
                        _customerQuery.CreatedBy = _createdBy;
                        _customerQuery.CustomerId = Convert.ToInt32(CustomerId.Value);
                        saveFlag = _customerQueryService.SaveCustomerQuery(_customerQuery);
                        if (saveFlag.IsSuccess)
                            saveFlag.Message = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "CustomerQuerySaveSuccess", "Customer query saved successfully");
                        else
                            saveFlag.Message = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "CustomerQuerySaveFail", "Error occurred while saving Customer query");

                        ValidationResponse = saveFlag;
                        ClearQueryControls();
                    }
                    else
                    {
                        saveFlag.IsSuccess = false;
                        saveFlag.Message = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "InvalidCustomer", "Invalid Customer");
                        ValidationResponse = saveFlag;
                    }
                }
                else
                {
                    saveFlag.IsSuccess = false;
                    saveFlag.Message = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "DefaultMessage", "Search customer for validations");
                    ValidationResponse = saveFlag;
                }
            }
        }

        private void ClearQueryControls()
        {
            ClientDropDownSearch.Enabled = true;
            ClientDropDownSearch.SelectedIndex = 0;
            QueryType.SelectedIndex = 0;
            ddlCompaignGroup.SelectedIndex = 0;
            ddlProvider.SelectedIndex = 0;
            Notes.Value = string.Empty;
            IsSolved.SelectedIndex = 1;
          
        }

        protected void Email_TextChanged(object sender, EventArgs e)
        {
            //if (!string.IsNullOrEmpty(Email.Text) && !string.IsNullOrWhiteSpace(Email.Text))
            //{
            //    SubscribeCheckBox.Enabled = true;
            //}

            ValidateFields();
        }

        
        protected void Reset_Click(object sender, EventArgs e)
        {
            // ResetEditForm(true);
        }

        protected void ButtonSearch_Click(object sender, EventArgs e)
        {
            int countryId;
            int locationId;

            // ValidationResponse search = new ValidationResponse();
            if (Page.IsValid)
            {
                LBDataAccess.Customer customer = new LBDataAccess.Customer();

                customer.FirstName = FirstNameSearch.Value;
                customer.LastName = LastNameSearch.Value;
                customer.AddressLine1 = AddressLine1Search.Value;
                customer.AddressLine2 = AddressLine2Search.Value;
                customer.Phone = PrimaryPhoneSearch.Value;

                if (ClientDropDownSearch.SelectedIndex > 0)
                    customer.ClientId = ClientDropDownSearch.SelectedValue;

                if (int.TryParse(CountryDropDown.SelectedValue, out countryId))
                {
                    customer.CountryId = countryId;
                }

                //if (int.TryParse(LocationDropDown.SelectedValue, out locationId))
                //{
                    customer.LocationId = hdfLocation.Value;
                //}


                //search = _customerQueryService.SearchCustomer(customer);

                BindCustomerGrid(customer);
                // ValidationResponse = search;
                //if (search.IsSuccess)
                //    ButtonInsert.Enabled = true;
                //else
                //    ButtonInsert.Enabled = false;
                //CustomerId.Value = Convert.ToString(search.Id);
            }
        }

        protected void ResultGridView_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        protected void AddAndSave_Click(object sender, EventArgs e)
        {
            //if (Page.IsValid)
            //{
                this.RecordCustomer();
                ResultDiv.InnerHtml = string.Empty;
            //}
        }

        protected void EditAndSave_Click(object sender, EventArgs e)
        {
            //errormessage.Visible = false;

            if (Page.IsValid)
            {
                //   this.RecordCustomer(true);
            }
        }

        protected void ResultGridView_SelectedIndexChanged(object sender, EventArgs e)
        {
            int custId;
            // Get the currently selected row using the SelectedRow property.
            GridViewRow row = ResultGridView.SelectedRow;

            CustomerId.Value = string.Empty;
            ClientDropDownSearch.Enabled = false;
            if (int.TryParse(row.Cells[0].Text, out custId))
            {
                CustomerId.Value = Convert.ToString(custId);
                ButtonInsert.Enabled = true;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "CallExpanEditForm", "ExpanEditForm()", true);
            }
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// initialize the page
        /// </summary>
        private void InitPage()
        {
            try
            {
                ValidationResponse = null;
                ValidationResponse msg = new ValidationResponse();
                msg.IsSuccess = true;
                msg.Message = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "DefaultMessage", "Search customer for validations"); 
                ValidationResponse = msg;

                //Load Drop downs
                this.LoadDropDowns();
                //Load Regular Expression Validate
                this.LoadSitecoreFieldData();
                this.BindGridViewHeaders(ResultGridView);
                //Bind Table Headers

            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Load Drop Downs
        /// </summary>
        private void LoadDropDowns()
        {
            try
            {
                Item dataSourceitem = this.GetDataSource();
                this.LoadCountryDropDownSearch();
                //this.LoadLocationDropDownSearch();
                this.LoadCountryDropDown();
                //this.LoadLocationDropDown();
                this.LoadTitles();
                this.BindQueryType();
                this.BindClientDropDown();
                this.BindCompaignGroup(dataSourceitem);
                this.BindProvider();
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        /// <summary>
        /// LoadRegular Expression Validate
        /// </summary>
        private void LoadSitecoreFieldData()
        {
            try
            {
                this.RegularExpressionValidatorPhone.ValidationExpression = Constants.PhoneNumberValidationRegex;
                //this.RegularExpressionValidatorPhoneSearch.ValidationExpression = Constants.PhoneNumberValidationRegex;
                this.RegularExpressionValidatorSecondaryPhone.ValidationExpression = Constants.PhoneNumberValidationRegex;
                this.RegularExpressionValidatorEmail.ValidationExpression = Constants.EmailValidationRegex;

                //Customer Enquiry Detail
                this.ButtonInsert.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "InsertButton", "Insert");
                //Customer Validation
                this.ButtonSearch.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "SearchButton", "Search");
                this.AddClient.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "AddClientButton", "AddClient");

                //Customer Addition
                this.EditAndSave.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "CAAddClientButton", "Add Client ");
                this.SearchAgain.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "SearchAgainButton", "Search Again ");
                this.GoBack.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "GoBackButton", "Go Back");

            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }

        }

        /// <summary>
        /// Bind Table Header
        /// </summary>
        private void BindGridViewHeaders(GridView gridView)
        {

            string fullname = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "FullName", "Full Name");
            string fullAddress = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "FullAddress", "Full Address");
            string emailAddress = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "EmailAddress", "Email Address");
            string mobilePhoneNumber = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "MobilePhoneNumber", "Mobile Phone Number");

            BindBoundField("CustomerId", "CustomerId", gridView);
            BindBoundField(fullname, "FullName", gridView);
            BindBoundField(fullAddress, "FullAddress", gridView);
            BindBoundField(emailAddress, "EmailAddress", gridView);
            BindBoundField(mobilePhoneNumber, "MobilePhoneNumber", gridView);

            BindCommandField(gridView);

        }

        /// <summary>
        /// Bind Bound Field to grid
        /// </summary>
        /// <param name="headerText">Header Text</param>
        /// <param name="dataField"><Data Field/param>
        /// <param name="grid">Grid</param>
        private void BindBoundField(string headerText, string dataField, GridView grid)
        {
            BoundField boundField = new BoundField();
            boundField.HeaderText = headerText;
            boundField.DataField = dataField;
            grid.Columns.Add(boundField);
        }

        /// <summary>
        /// Bind Command Field
        /// </summary>
        /// <param name="grid">Grid</param>
        private void BindCommandField(GridView grid)
        {
            CommandField commandField = new CommandField();
            commandField.ShowSelectButton = true;
            commandField.SelectText = "<i class='glyphicon glyphicon-edit icons-positionRight'></i>";
            grid.Columns.Add(commandField);

        }
        
        /// <summary>
        /// Fire validate
        /// </summary>
        private void ValidateFields()
        {
            // this.RegularExpressionValidatorSecondaryPhone.Validate();
            // this.RegularExpressionValidatorEmail.Validate();
            // this.RegularExpressionValidatorPhone.Validate();
            // this.RegularExpressionValidatorSecondaryPhone.Validate();
        }

        private void BindCustomerGrid(LBDataAccess.Customer customer)
        {

            ValidationResponse search = new ValidationResponse();
            try
            {
                var result = uCommerceConnectionFactory.GetCustomerList(customer);

                ResultGridView.AutoGenerateColumns = false;
                ResultGridView.DataSource = result;
                ResultGridView.DataBind();
                //ResultGridView.Columns[0].Visible = true;
                //ResultGridView.SelectedIndex = -1;
                if(ResultGridView.Columns.Count>0)
                     //hide id column 
                     ResultGridView.Columns[0].Visible = false;
                if (result.Count > 0)
                {
                    ResultDiv.Visible = true;
                    //Focus Search Result Grid
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "CallFocusSearchResultGrid", "FocusSearchResultGrid()", true);
                }
                else
                {
                    ResultDiv.Visible = false;
                    search.IsSuccess = false;
                    search.Message = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "CustomerNotFound", "No Customer found for this search criteria"); 
                    ValidationResponse = search;
                }
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Write customer records to db
        /// </summary>
        private void RecordCustomer()
        {
            try
            {
                int result;
                ValidationResponse SaveFlag = new ValidationResponse();
                LBDataAccess.Customer customer = CreateCustomerObj();

                var country = UCommerce.EntitiesV2.Country.Get(Convert.ToInt32(CountryDropDown.SelectedValue));
                if (country == null)
                {
                    country = new Country()
                    {
                        Name = CountryDropDown.SelectedItem.Text,
                        Culture = CountryDropDown.SelectedItem.Attributes["data-culture"] == null ? "en-US" : CountryDropDown.SelectedItem.Attributes["data-culture"]
                    };
                }
                var objuComCustomer = UCommerce.EntitiesV2.Customer.Find(f => f.CustomerId == customer.CustomerId).FirstOrDefault();
                if (objuComCustomer == null)
                {

                    Address addressInfo = new Address
                    {
                        FirstName = customer.FirstName,
                        LastName = customer.LastName,
                        EmailAddress = customer.EmailAddress,
                        PhoneNumber = customer.Phone,
                        MobilePhoneNumber = customer.MobilePhone,
                        Line1 = string.IsNullOrEmpty(customer.AddressLine1) ? string.Empty : customer.AddressLine1,
                        Line2 = string.IsNullOrEmpty(customer.AddressLine2) ? string.Empty : customer.AddressLine2,
                        City = string.IsNullOrEmpty(customer.AddressLine3) ? string.Empty : customer.AddressLine3,
                        State = hdfLocation.Value,
                        AddressName = "",
                        Attention = "",
                        CompanyName = "",
                        PostalCode = "",
                        Country = country
                    };


                    objuComCustomer = new UCommerce.EntitiesV2.Customer
                    {
                        FirstName = customer.FirstName,
                        LastName = customer.LastName,
                        EmailAddress = customer.EmailAddress,
                        PhoneNumber = customer.Phone,
                        MobilePhoneNumber = customer.MobilePhone,
                    };



                    objuComCustomer.AddAddress(addressInfo);
                    objuComCustomer.Save();

                    customer.CustomerId = objuComCustomer.CustomerId;

                    result = uCommerceConnectionFactory.AddCustomer(customer);
                }

                else
                {

                    //var objCustomerAddress = objuComCustomer.Addresses.FirstOrDefault();
                    var objCustomerAddress = objuComCustomer.Addresses.FirstOrDefault(p => p.Customer.CustomerId == customer.CustomerId);

                    objuComCustomer.FirstName = customer.FirstName;
                    objuComCustomer.LastName = customer.LastName;
                    objuComCustomer.EmailAddress = customer.EmailAddress;
                    objuComCustomer.PhoneNumber = customer.Phone;
                    objuComCustomer.MobilePhoneNumber = customer.MobilePhone;

                    if (objCustomerAddress == null)
                    {
                        objCustomerAddress = new UCommerce.EntitiesV2.Address();
                        objuComCustomer.AddAddress(objCustomerAddress);
                    }
                    objCustomerAddress.FirstName = customer.FirstName;
                    objCustomerAddress.LastName = customer.LastName;
                    objCustomerAddress.EmailAddress = customer.EmailAddress;
                    objCustomerAddress.PhoneNumber = customer.Phone;
                    objCustomerAddress.MobilePhoneNumber = customer.MobilePhone;
                    objCustomerAddress.Line1 = string.IsNullOrEmpty(customer.AddressLine1) ? string.Empty : customer.AddressLine1;
                    objCustomerAddress.Line2 = string.IsNullOrEmpty(customer.AddressLine2) ? string.Empty : customer.AddressLine2;
                    objCustomerAddress.City = string.IsNullOrEmpty(customer.AddressLine3) ? string.Empty : customer.AddressLine3;
                    objCustomerAddress.State = hdfLocation.Value;
                    objCustomerAddress.AddressName = "";
                    objCustomerAddress.Attention = "";
                    objCustomerAddress.CompanyName = "";
                    objCustomerAddress.PostalCode = "";
                    if (country != null)
                        objCustomerAddress.Country = country;


                    //if (objuComCustomer.Addresses.FirstOrDefault(p => p.EmailAddress == customer.EmailAddress) == null)
                    //    objuComCustomer.Addresses.Add(objCustomerAddress);
                    //else
                    //    objCustomerAddress.Save();
                    objuComCustomer.Save();
                    customer.CustomerId = objuComCustomer.CustomerId;

                    result = uCommerceConnectionFactory.UpdateCustomer(customer);
                }
               // result = uCommerceConnectionFactory.AddCustomer(customer);
                if (result > 0)
                {

                    CustomerId.Value = Convert.ToString(result);
                    SaveFlag.IsSuccess = true;
                    SaveFlag.Message = customer.FirstName + " " + customer.LastName + " " + SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "SuccessMsgForCustomerSaving", "  successfully added as customer"); 
                    ClearControls();
                    ClientDropDownSearch.Enabled = false;
                }
                else
                {
                    SaveFlag.IsSuccess = false;
                    SaveFlag.Message = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "CustomerQuerySaveFail", "Error occurred when customer saving");
                }
                ValidationResponse = null;
                ValidationResponse = SaveFlag;

                if (SaveFlag.IsSuccess)
                    ButtonInsert.Enabled = true;
                else
                    ButtonInsert.Enabled = false;
                // CustomerId.Value = Convert.ToString(SaveFlag.Id); 

            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }


        private void ClearControls()
        {
            try
            {
                TitleDropDown.SelectedIndex = 0;
                FirstName.Text = string.Empty;
                LastName.Text = string.Empty;
                AddressLine1.Text = string.Empty;
                AddressLine2.Text = string.Empty;
                AddressLine3.Text = string.Empty;
                SecondaryPhone.Text = string.Empty;
                PrimaryPhone.Text = string.Empty;
                Email.Text = string.Empty;
                CountryDropDown.SelectedIndex = 0;
                LocationDropDown.SelectedIndex = 0;
            }
            catch { }
        }

        /// <summary>
        /// Populate Location DropDown
        /// </summary>
        private void LoadLocationDropDownSearch()
        {
            try
            {
                //Bind locationList data to the drop down
                LocationDropDownSearch.DataSource = uCommerceConnectionFactory.GetLocationList();
                LocationDropDownSearch.DataTextField = "Location";
                LocationDropDownSearch.DataValueField = "LocationTargetId";
                LocationDropDownSearch.DataBind();
                ListItem selectLocation = new ListItem(SelectText, string.Empty);
                LocationDropDownSearch.Items.Insert(0, selectLocation);
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }

        }

        /// <summary>
        /// <summary>
        ///  Populate Country DropDown
        /// </summary>
        private void LoadCountryDropDownSearch()
        {

            CountryDropDownSearch.Items.Clear();
            ListItem allLocation = new ListItem("Please Select", "0");
            LocationDropDown.Items.Insert(0, allLocation);
            ListItem allCountry = new ListItem("Please Select", "0");
            CountryDropDownSearch.Items.Insert(0, allCountry);

            Database contextDb = Sitecore.Context.Database;
            dynamic items = contextDb.GetItem(Constants.Countries) != null ? contextDb.GetItem(Constants.Countries).GetChildren() : null;
            if (items != null)
            {
                foreach (var item in items)
                {
                    ListItem liBox = new ListItem();
                    liBox.Text = SitecoreFieldsHelper.GetValue(item, "CountryName");
                    liBox.Value = SitecoreFieldsHelper.GetValue(item, "UCommerceCountryID");
                    liBox.Attributes.Add("data-itemid", item.ID.ToString());
                    liBox.Attributes.Add("data-culture", SitecoreFieldsHelper.GetValue(item, "Culture"));
                    CountryDropDownSearch.Items.Add(liBox);
                }
            }
        }

        /// <summary>
        /// Load Client Specific Drop Down
        /// </summary>
        private void LoadClientSpecificDropDown(string dropdownFieldName, string targetItemsTextField, string targetItemsValueField, ListControl dropdown, string client)
        {
            try
            {
                //query for the client item in admin portal
                Item clientItem = Sitecore.Context.Database.GetItem("/sitecore/content/admin-portal/client-setup/" + client + "");
                if (clientItem != null)
                {
                    //Bind data to drop-down
                    Collection<Item> items = SitecoreFieldsHelper.GetMutiListItems(clientItem, dropdownFieldName);
                    dropdown.DataSource = SitecoreFieldsHelper.GetDropdownDataSource(items, targetItemsTextField, targetItemsValueField);
                    dropdown.DataTextField = "Text";
                    dropdown.DataValueField = "Value";
                    dropdown.DataBind();
                    //Set the default list item
                    ListItem select = new ListItem(SelectText, string.Empty);
                    dropdown.Items.Insert(0, select);
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Populate Titles
        ///// </summary>
        private void LoadTitles()
        {
            Sitecore.Data.Database currentDb = Sitecore.Context.Database;

            //Load location data to a locationList
            Item titleList = currentDb.GetItem(Constants.TitleListPath);

            if (titleList != null)
            {
                var titles = (from loc in titleList.Children
                              orderby loc.DisplayName
                              select loc.DisplayName);

                //Bind locationList data to the drop down
                TitleDropDown.DataSource = titles != null ? titles.ToList() : null;
                TitleDropDown.DataBind();
            }

            ListItem selectTitle = new ListItem(SelectText, string.Empty);
            TitleDropDown.Items.Insert(0, selectTitle);
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        private IList<string> GetClientList()
        {
            try
            {
                /// Get Client items
                //Sitecore.Data.Database currentDb = Sitecore.Context.Database;
                var clientItems = SearchHelper.GetUserSpecificClientItems(); //currentDb.SelectItems(ClientListQuery);

                if (clientItems != null)
                {
                    /// Extract Clients Filtered Items
                    var clientIds = (from client in clientItems
                                     orderby client.DisplayName
                                     select client.ID.Guid.ToString());
                    return clientIds != null ? clientIds.ToList() : null;
                }

                return null;
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }


        private DataSet GetCustomerDetailes()
        {
            try
            {


                return uCommerceConnectionFactory.GetCustomerDetailes(CreateCustomerObj(), GetClientIdListString());
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }

        }
       

        /// <summary>
        /// Convert string list to comma separated string
        /// Prepare string list for SQL IN query
        /// </summary>
        /// <returns>comma separated string id list</returns>
        private string GetClientIdListString()
        {
            var clientList = GetClientList();
            string clientIdList = "{0}";//Dummy

            if (clientList != null)
            {
                //Convert string list to comma separated string as "{6C464DB8},{6451DB8}"
                clientIdList = string.Join(",", clientList);
            }

            return clientIdList;
        }


        /// <summary>
        /// Create Customer Obj
        /// </summary>
        /// <returns>Customer</returns>
        private LBDataAccess.Customer CreateCustomerObj()
        {
            try
            {
                string firstName = FirstName.Text;
                string lastName = LastName.Text;

                int countryId;
                int locationId;

                //id column 
                LBDataAccess.Customer customer = new LBDataAccess.Customer();
                customer.CustomerId = 0;
                customer.Title = TitleDropDown.SelectedValue;
                customer.FirstName = firstName.Length > 1 ? firstName.First().ToString().ToUpper().Trim() + firstName.Substring(1) : firstName.ToUpper().Trim();
                customer.LastName = lastName.Length > 1 ? lastName.First().ToString().ToUpper().Trim() + lastName.Substring(1) : lastName.ToUpper().Trim();
                customer.AddressLine1 = string.IsNullOrEmpty(AddressLine1.Text) ? null : AddressLine1.Text.Trim();
                customer.AddressLine2 = string.IsNullOrEmpty(AddressLine2.Text) ? null : AddressLine2.Text.Trim();
                customer.AddressLine3 = string.IsNullOrEmpty(AddressLine3.Text) ? null : AddressLine3.Text.Trim();
                customer.Phone = string.IsNullOrEmpty(SecondaryPhone.Text) ? null : SecondaryPhone.Text.Trim();
                customer.MobilePhone = string.IsNullOrEmpty(PrimaryPhone.Text) ? null : PrimaryPhone.Text.Trim();
                customer.EmailAddress = string.IsNullOrEmpty(Email.Text) ? null : Email.Text.Trim();
                customer.ClientId = ClientDropDownSearch.SelectedValue;
                //customer.BranchId = StoreDropDown.SelectedValue;
                customer.Active = 1;
                customer.BookingReferenceId = string.Empty;

                if (int.TryParse(CountryDropDown.SelectedValue, out countryId))
                {
                    customer.CountryId = countryId;
                }

                //if (int.TryParse(LocationDropDown.SelectedValue, out locationId))
                //{
                customer.LocationId = hdfLocation.Value;
                //}

                return customer;
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Check the credit card no in the text
        /// </summary>
        private bool HasCreditCardNumber(string inputCardNumber)
        {
            bool result = false;

            var input = inputCardNumber.Replace("-", string.Empty).Replace(" ", string.Empty);
            Regex regex = new Regex(@"\d{16}");
            Match match = regex.Match(input);
            if (match.Success)
            {
                return true;
            }
            return result;
        }

      
        #endregion

        #region "Add Client"
        /// <summary>
        ///  Populate Search Country DropDown
        /// </summary>


        /// <summary>
        /// Populate Location DropDown
        /// </summary>
        private void LoadLocationDropDown()
        {
            try
            {
                //Bind locationList data to the drop down
                LocationDropDown.DataSource = uCommerceConnectionFactory.GetLocationList();
                LocationDropDown.DataTextField = "Location";
                LocationDropDown.DataValueField = "LocationTargetId";
                LocationDropDown.DataBind();
                ListItem selectLocation = new ListItem(SelectText, string.Empty);
                LocationDropDown.Items.Insert(0, selectLocation);
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }

        }

        /// <summary>
        ///  Populate Country DropDown
        /// </summary>
        private void LoadCountryDropDown()
        {

            CountryDropDown.Items.Clear();
            ListItem allLocation = new ListItem("Please Select", "0");
            LocationDropDown.Items.Insert(0, allLocation);
            ListItem allCountry = new ListItem("Please Select", "0");
            CountryDropDown.Items.Insert(0, allCountry);

            Database contextDb = Sitecore.Context.Database;
            dynamic items = contextDb.GetItem(Constants.Countries) != null ? contextDb.GetItem(Constants.Countries).GetChildren() : null;
            if (items != null)
            {
                foreach (var item in items)
                {
                    ListItem liBox = new ListItem();
                    liBox.Text = SitecoreFieldsHelper.GetValue(item, "CountryName");
                    liBox.Value = SitecoreFieldsHelper.GetValue(item, "UCommerceCountryID");
                    liBox.Attributes.Add("data-itemid", item.ID.ToString());
                    liBox.Attributes.Add("data-culture", SitecoreFieldsHelper.GetValue(item, "Culture"));
                    CountryDropDown.Items.Add(liBox);
                }
            }
        }

        #endregion

        #region "DropDown  Member"
        private void BindData(Item dataSourceitem)
        {

        }

        //To bind the Customer Query type  
        private void BindQueryType()
        {
            DataTable customerQueryTypes = null;
            customerQueryTypes = _customerQueryService.GetQueryTypes();
            QueryType.DataSource = customerQueryTypes;
            QueryType.DataValueField = "QueryTypeId";
            QueryType.DataTextField = "QueryType";
            QueryType.DataBind();
            //Set the default list item
            ListItem select = new ListItem(SelectText, string.Empty);
            QueryType.Items.Insert(0, select);
        }

        //<summary>
        //Bind Client data to drop downs
        //</summary>
        /// <summary>
        /// Bind Client data to drop downs
        /// </summary>
        private void BindClientDropDown()
        {
            try
            {
                if (SearchHelper.GetUserSpecificClientItems() != null)
                {
                    /// Extract Clients Filtered Items
                    var clientFilterList = (from client in SearchHelper.GetUserSpecificClientItems()
                                            orderby client.DisplayName
                                            select new { DiplayName = client.DisplayName, ItemName = client.ID.Guid.ToString() }).ToList();

                    /// Load Client List Dropdown
                    this.ClientDropDownSearch.DataSource = clientFilterList;

                    /// Set data text and data value fields
                    ClientDropDownSearch.DataTextField = "DiplayName";
                    ClientDropDownSearch.DataValueField = "ItemName";

                    this.ClientDropDownSearch.DataBind();
                }

                this.ClientDropDownSearch.Items.Insert(0, Constants.PleaseSelectText);

                /// Assign respective client images to dropdown
                //foreach (ListItem clientListItem in ClientDropDownSearch.Items)
                //{
                //    if (!string.Equals(clientListItem.Text, Constants.PleaseSelectText))
                //    {
                //        Item clientItem = contextDb.GetItem(string.Format(ClientItemPath, clientListItem.Value));
                //        clientListItem.Attributes.Add("data-image", GetImageURL(clientItem, "Customer Service Image"));
                //    }
                //}
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, null);
                throw;
            }
        }



        private void BindCompaignGroup(Item dataSourceitem)
        {
            if (dataSourceitem != null)
            {
                Dictionary<string, string> campaignDatasource = new Dictionary<string, string>();
                string showAllText = SitecoreFieldsHelper.GetValue(dataSourceitem, "ShowAllLabel");
                if (SitecoreFieldsHelper.GetBoolean(dataSourceitem, "DisplayShowAllLabel"))
                {
                    ListItem showAllItem = new ListItem(showAllText, "");
                    this.ddlCompaignGroup.AppendDataBoundItems = true;
                    this.ddlCompaignGroup.Items.Insert(0, showAllItem);
                }

                Sitecore.Data.Fields.MultilistField offerGroupField = dataSourceitem.Fields["OfferGroups"];
                List<Sitecore.Data.ID> multiListTargetIds = (offerGroupField != null && !string.IsNullOrWhiteSpace(offerGroupField.Value)) ? offerGroupField.TargetIDs.ToList<Sitecore.Data.ID>() : null;
                foreach (var id in multiListTargetIds)
                {
                    Item campaignItem = Sitecore.Context.Database.GetItem(id);
                    if (campaignItem != null)
                    {
                        campaignDatasource.Add(campaignItem.ID.ToString(), campaignItem.Fields["Title"].Value);
                    }
                }

                this.ddlCompaignGroup.DataSource = campaignDatasource;
                this.ddlCompaignGroup.DataTextField = "Value";
                this.ddlCompaignGroup.DataValueField = "Key";
                this.ddlCompaignGroup.DataBind();
            }
        }

        private void BindProvider()
        {
            Dictionary<string, string> providerDatasource = new Dictionary<string, string>();

            ListItem showAllItem = new ListItem("Select a Provider", "");
            this.ddlProvider.AppendDataBoundItems = true;
            this.ddlProvider.Items.Insert(0, showAllItem);

            Item[] hotels = Sitecore.Context.Database.SelectItems(Constants.BasketPageQueryPath);
            foreach (var hotel in hotels)
            {
                if (hotel != null)
                {
                    providerDatasource.Add(hotel.ID.ToString(), hotel.Fields["Name"].Value);
                }
            }

            this.ddlProvider.DataSource = providerDatasource;
            this.ddlProvider.DataTextField = "Value";
            this.ddlProvider.DataValueField = "Key";
            this.ddlProvider.DataBind();
        }

        /// <summary>
        /// Method used to Get Image URL of Sitecore Image field
        /// </summary>
        /// <param name="currentItem"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        private string GetImageURL(Item currentItem, string fieldName)
        {
            string imageURL = string.Empty;
            Sitecore.Data.Fields.ImageField imageField = currentItem.Fields[fieldName];
            if (imageField != null && imageField.MediaItem != null)
            {
                Sitecore.Data.Items.MediaItem image = new Sitecore.Data.Items.MediaItem(imageField.MediaItem);
                imageURL = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image));
            }
            return imageURL;
        }
        #endregion

        #region binderrormsgs
        private void BindSitecoreErrormsgs()
        {
            Item currentPageItem = Sitecore.Context.Item;
            if (currentPageItem != null)
            {

                CustomerEnquiryHeaderText.Item = currentPageItem;
                QueryTypeText.Item = currentPageItem;
                PartnerText.Item = currentPageItem;
                CampaignGroupText.Item = currentPageItem;
                ProviderText.Item = currentPageItem;
                IsResolvedText.Item = currentPageItem;
                ResetButtonText.Item = currentPageItem;
                NotesText.Item = currentPageItem;
                CustomerValidationHeaderText.Item = currentPageItem;
                ClientFirstnameText.Item = currentPageItem;
                ClientLastnameText.Item = currentPageItem;
                CountyText.Item = currentPageItem;
                ClientAddressLine1Text.Item = currentPageItem;
                ClientAddressLine2Text.Item = currentPageItem;
                ClientPrimaryPhoneText.Item = currentPageItem;
                LocationsText.Item = currentPageItem;
                btnCVResetText.Item = currentPageItem;
                CustomerTitleText.Item = currentPageItem;
                FirstNameText.Item = currentPageItem;
                LastNameText.Item = currentPageItem;
                AddressLine1Text.Item = currentPageItem;
                AddressLine2Text.Item = currentPageItem;
                AddressLine3Text.Item = currentPageItem;
                LocationText.Item = currentPageItem;
                SecondaryPhoneText.Item = currentPageItem;
                EmailText.Item = currentPageItem;

            }

            Item conditionalMessageItem = this.GetItemFromRenderingParameterPath("Conditional Messages Link");            
            if (conditionalMessageItem != null)
            {
                FirstNameRequired.Item = conditionalMessageItem;
                DontEnterCardNumber.Item = conditionalMessageItem;
                QueryTypeRequired.Item = conditionalMessageItem;
                PartnerRequired1.Item = conditionalMessageItem;
                PartnerRequired2.Item = conditionalMessageItem;
                PartnerRequired3.Item = conditionalMessageItem;
                NotesRequired.Item = conditionalMessageItem;
                LastNameRequired.Item = conditionalMessageItem;
                FirstNameRequiredValEdit.Item = conditionalMessageItem;
                AddressLine1Required.Item = conditionalMessageItem;
                CountryRequired.Item = conditionalMessageItem;
                LocationRequired.Item = conditionalMessageItem;
                PrimaryPhoneRequired1.Item = conditionalMessageItem;
                PrimaryPhoneRequired2.Item = conditionalMessageItem;
                SecondaryPhoneRequired.Item = conditionalMessageItem;
                EnterCorrectEmailFormat.Item = conditionalMessageItem;
                EmailRequired.Item = conditionalMessageItem;
            }
        }
        #endregion

    }
}
