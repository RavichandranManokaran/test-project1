﻿namespace Affinion.LoyaltyBuild.DataAccess
{
    using Affinion.LoyaltyBuild.DataAccess.DatabaseScripts;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Data class for reading data from database
    /// </summary>
    public class DataObject
    {
        public string Command { get; set; }

        public CommandType CommandType { get; set; }

        public DataTable Data { get; set; }

        public Exception Exception { get; set; }

        public Dictionary<string, object> Parameters { get; set; }

        public SqlDataAdapter DataAdapter { get; set; }

        public SqlTransaction Transaction { get; set; }

        public IAffinionDatabaseScript DatabaseScript { get; set; }

        public DataObject()
        {
            this.DataAdapter = new SqlDataAdapter();
            this.Parameters = new Dictionary<string, object>();
            this.CommandType = System.Data.CommandType.StoredProcedure;
            this.Data = new DataTable();
            this.Exception = null;
        }
    }
}
