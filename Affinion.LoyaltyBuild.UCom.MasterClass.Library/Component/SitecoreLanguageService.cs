﻿using Sitecore.Collections;
using Sitecore.Data.Items;
using Sitecore.Globalization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using UCommerce.Infrastructure.Globalization;
using UCommerce.Sitecore;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Component
{
    public class SitecoreLanguageService : UCommerce.Infrastructure.Globalization.ILanguageService
    {
        // Fields
        private readonly ISitecoreContext _context;

        // Methods
        public SitecoreLanguageService(ISitecoreContext context)
        {
            this._context = context;
        }

        public IList<UCommerce.Infrastructure.Globalization.Language> GetAllLanguages()
        {
            ChildList installedSitecoreLanguageItems = this.GetInstalledSitecoreLanguageItems();
            IList<UCommerce.Infrastructure.Globalization.Language> source = new List<UCommerce.Infrastructure.Globalization.Language>();
            try
            {
                IEnumerator enumerator = installedSitecoreLanguageItems.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    Func<UCommerce.Infrastructure.Globalization.Language, bool> predicate = null;
                    Sitecore.Data.Items.Item languageItem = (Sitecore.Data.Items.Item)enumerator.Current;
                    string name = this.TryGetDisplayNameWithFallback(languageItem);
                    if (predicate == null)
                    {
                        predicate = x => x.CultureCode != languageItem.Name;
                    }
                    if (source.All<UCommerce.Infrastructure.Globalization.Language>(predicate))
                    {
                        source.Add(new UCommerce.Infrastructure.Globalization.Language(name, languageItem.Name));
                    }
                }
            }
            catch (Exception er)
            {
                throw er;
            }
            return source;
        }

        private ChildList GetInstalledSitecoreLanguageItems()
        {
            //List<UCommerce.Infrastructure.Globalization.Language> list = LanguageManager.GetLanguages(this._context.MasterDatabase).Distinct<UCommerce.Infrastructure.Globalization.Language>().ToList<UCommerce.Infrastructure.Globalization.Language>();
            //UCommerce.Infrastructure.Globalization.Language language = list.First<UCommerce.Infrastructure.Globalization.Language>();
            //Item item = this._context.MasterDatabase.GetItem(language.get_Origin().get_ItemId());
            //return item.get_Parent().GetChildren();

            //var installedSitecoreLanguages = LanguageManager.GetLanguages(_context.MasterDatabase).Distinct().ToList();
            var installedSitecoreLanguages = Sitecore.Data.Managers.LanguageManager.GetLanguages(_context.DatabaseForContent).Distinct().ToList();

            // LanguageService does not observe sort order
            // of languages from Sitecore. We have to get the
            // parent of the languages and use that to load
            // the children to get the proper sort order.
            var randomLangauge = installedSitecoreLanguages.First();
            Item randomLanguageItem = _context.MasterDatabase.GetItem(randomLangauge.Origin.ItemId);

            // Item.Name is validated by Sitecore as a valid culture
            // so it's safe to new up a CultureInfo and use that
            // for DisplayName.
            return randomLanguageItem.Parent.GetChildren();
        }
        private string TryGetDisplayNameWithFallback(Item languageItem)
        {
            try
            {
                return new CultureInfo(languageItem.Name).DisplayName;
            }
            catch (CultureNotFoundException)
            {
                return languageItem.Name;
            }
        }
    }
}

