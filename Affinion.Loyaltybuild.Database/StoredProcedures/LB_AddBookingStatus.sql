﻿/****** Object:  StoredProcedure [dbo].[[LB_AddBookingStatus]]    Script Date: 2/12/2016 11:42:28 AM ******/

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LB_AddBookingStatus]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[LB_AddBookingStatus]
GO

/****** Object:  StoredProcedure [dbo].[LB_AddBookingStatus]    Script Date: 2/12/2016 11:42:28 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Prasanna SR
-- Create date: Feb 12, 2016
-- Description:	Add booking status
-- =============================================
CREATE PROCEDURE [dbo].[LB_AddBookingStatus]
	@OrderLineId INT,
	@Status INT = NULL,
	@CreatedBy VARCHAR(50)
AS
BEGIN
	DECLARE @OrderLineStatusId INT, @NewOrderRef VARCHAR(50)
	
	IF (@Status IS NULL)
	BEGIN
		--Confirmed
		SET @Status = 6
		--Cancelled
		SELECT TOP 1 @Status = 7, @NewOrderRef = NewOrderReference FROM LB_RoomReservation WHERE IsCancelled = 1 AND OrderLineId = @OrderLineId
		--Set Provisional
		IF @NewOrderRef IS NULL AND EXISTS (SELECT SUM([Total]) [Total], [Currency] FROM
					(SELECT SUM(OLD.Amount) [Total],
							OLD.CurrencyTypeId [Currency]
					FROM uCommerce_OrderLine  OL
						INNER JOIN LB_OrderLineDue OLD ON OLD.OrderLineID = OL.OrderLineId 
					WHERE
						ISNULL(OLD.IsDeleted,0) = 0 AND
						OLD.OrderLineID IN (SELECT OrderLineID FROM [dbo].[LB_FnRelatedOrderLines](@OrderLineId))
					GROUP BY 
						OLD.CurrencyTypeId
					UNION ALL
					SELECT -SUM(OLP.Amount) [Total],
							OLP.CurrencyTypeId [Currency]
					FROM uCommerce_OrderLine  OL
						INNER JOIN LB_OrderLinePayment OLP ON OLP.OrderLineID = OL.OrderLineId 
					WHERE
						OLP.OrderLineID IN (SELECT OrderLineID FROM [dbo].[LB_FnRelatedOrderLines](@OrderLineId))
					GROUP BY 
						OLP.CurrencyTypeId) P
				GROUP BY [Currency]
				HAVING
					SUM([Total]) > 0)
		BEGIN
			SET @Status = 5
		END
	END
	
	--add status log
	INSERT INTO [dbo].[uCommerce_OrderLineStatus]
        ([OrderLineID]
		,[StatusId]
        ,[CreatedBy]
        ,[CreatedDate])
     VALUES
        (@OrderLineId
		,@Status
        ,@CreatedBy
        ,GETDATE())

	SET @OrderLineStatusId = SCOPE_IDENTITY()

	--update current status
	UPDATE LB_RoomReservation
	SET OrderLineStatusID = @OrderLineStatusId 
	WHERE OrderLineId = @OrderLineId
END
GO



