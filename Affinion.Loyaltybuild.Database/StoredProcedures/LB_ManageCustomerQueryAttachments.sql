
CREATE PROCEDURE [dbo].[LB_ManageCustomerQueryAttachments]
(
	@QueryId int,
	@FileType nvarchar(50),
	@FileName nvarchar(100),
	@FileContent varbinary(MAX)
)	
AS 
BEGIN 
	DELETE FROM [dbo].[LB_CustomerQuery_Attachments] WHERE QueryId = @QueryId
	INSERT INTO [dbo].[LB_CustomerQuery_Attachments](
		 QueryId
		,FileType
		,[FileName]
		,FileContent)
	VALUES(@QueryId,@FileType,@FileName,@FileContent)
END


GO





