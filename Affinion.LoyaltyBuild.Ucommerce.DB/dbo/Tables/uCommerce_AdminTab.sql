﻿CREATE TABLE [dbo].[uCommerce_AdminTab] (
    [AdminTabId]      INT            IDENTITY (1, 1) NOT NULL,
    [VirtualPath]     NVARCHAR (512) NOT NULL,
    [AdminPageId]     INT            NOT NULL,
    [SortOrder]       INT            NOT NULL,
    [MultiLingual]    BIT            CONSTRAINT [uCommerce_DF_AdminTab_MultiLingual] DEFAULT ((0)) NOT NULL,
    [ResouceKey]      NVARCHAR (256) NULL,
    [HasSaveButton]   BIT            CONSTRAINT [uCommerce_DF_AdminTab_HasSaveButton] DEFAULT ((1)) NOT NULL,
    [HasDeleteButton] BIT            CONSTRAINT [uCommerce_DF_AdminTab_HasDeleteButton] DEFAULT ((0)) NOT NULL,
    [Enabled]         BIT            NOT NULL,
    CONSTRAINT [uCommerce_PK_AdminTab] PRIMARY KEY CLUSTERED ([AdminTabId] ASC),
    CONSTRAINT [uCommerce_FK_AdminTab_AdminPage] FOREIGN KEY ([AdminPageId]) REFERENCES [dbo].[uCommerce_AdminPage] ([AdminPageId])
);

