﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalTransferBookingSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.TransferBooking" %>

<div class="row">
    <%if (Booking != null) { %>
    <div class="col-sm-12">
        <div class="accordian-outer">
            <div class="panel-group" id="accordion1">
                <div class="panel panel-default">
                    <div class="panel-heading bg-primary">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion1" href="#collapse0" class="show-hide-list">
                                <sc:text id="TextBookingDetails" field="BookingStatusText" runat="server" />
                            </a>
                        </h4>
                    </div>
                    <div id="collapse0" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="content_bg_inner_box summary-detail alert-info">
                                        <div id="divPrint" runat="server" style="font-family: Segoe UI; box-sizing: border-box; display: inline-block; margin: 8px 0; padding: 15px; width: 100%; background-color: rgba(3, 129, 203, 0.05); border: 1px solid rgba(3, 129, 203, 0.2); color: #0380c9;">
                                            <div class="col-sm-6 col-xs-12 no-padding-m">
                                                <div class="col-sm-12 no-padding-m" id="divDates" runat="server">
                                                    <div class="col-sm-12">
                                                        <h2 class="no-margin">
                                                            <sc:text id="TextDates" field="DatesText" runat="server" />
                                                        </h2>
                                                    </div>
                                                    <div class="row margin-row">
                                                        <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                            <sc:text id="TextArrival" field="ArrivalText" runat="server" />
                                                        </span></div>
                                                        <div class="col-sm-6 font-size-12"><span>
                                                            <b><asp:Literal runat="server" ID="litArrivalDate"></asp:Literal></b></span></div>
                                                    </div>
                                                    <div class="row margin-row">
                                                        <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                            <sc:text id="TextDeparture" field="DepartureText" runat="server" />
                                                        </span></div>
                                                        <div class="col-sm-6 font-size-12"><span>
                                                            <b><asp:Literal runat="server" ID="litDepartureDate" /></b></span></div>
                                                    </div>
                                                    <div class="row margin-row">
                                                        <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                            <sc:text id="TextConfirmation" field="ConfirmationText" runat="server" />
                                                        </span></div>
                                                        <div class="col-sm-6 font-size-12"><span>
                                                            <asp:Literal runat="server" ID="litConfirmationDate" /></span></div>
                                                    </div>
                                                    <div class="row margin-row">
                                                        <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                            <sc:text id="TextReservation" field="ReservationText" runat="server" />
                                                        </span></div>
                                                        <div class="col-sm-6 font-size-12"><span>
                                                            <asp:Literal runat="server" ID="litReservationDate" /></span></div>
                                                    </div>
                                                    <div class="row margin-row">
                                                        <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                            <sc:text id="TextStatusDate" field="StatusDateText" runat="server" />
                                                        </span></div>
                                                        <div class="col-sm-6 font-size-12"><span>
                                                            <asp:Literal runat="server" ID="litStatusDate" /></span></div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-xs-12 no-padding-m" id="divCustomer" runat="server">
                                                    <div class="col-sm-12">
                                                        <h2 class="no-margin">
                                                            <sc:text id="TextCustomer" field="CustomerText" runat="server" />
                                                        </h2>
                                                    </div>
                                                    <div class="row margin-row">
                                                        <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                            <sc:text id="TextCustomerName" field="NameText" runat="server" />
                                                        </span></div>
                                                        <div class="col-sm-6 font-size-12"><span>
                                                            <asp:Literal runat="server" ID="litCustomerName" /></span></div>
                                                    </div>
                                                    <div class="row margin-row">
                                                        <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                            <sc:text id="TextCustomerAddress" field="AddressText" runat="server" />
                                                        </span></div>
                                                        <div class="col-sm-6 font-size-12"><span>
                                                            <asp:Literal runat="server" ID="litCustomerAddress" /></span></div>
                                                    </div>
                                                    <div class="row margin-row">
                                                        <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                            <sc:text id="TextCustomerPhone" field="PhoneText" runat="server" />
                                                        </span></div>
                                                        <div class="col-sm-6 font-size-12"><span>
                                                            <asp:Literal runat="server" ID="litCustomerPhone" /></span></div>
                                                    </div>
                                                    <div class="row margin-row">
                                                        <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                            <sc:text id="TextCustomerEmail" field="EmailText" runat="server" />
                                                        </span></div>
                                                        <div class="col-sm-6 font-size-12"><span>
                                                            <asp:Literal runat="server" ID="litCustomerEmail" /></span></div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 no-padding-m" id="divPayments" runat="server">
                                                    <div class="col-sm-12">
                                                        <h2 class="no-margin">
                                                            <sc:text id="TextPaymentDetails" field="PaymentDetailsText" runat="server" />
                                                        </h2>
                                                    </div>
                                                    <div class="row margin-row">
                                                        <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                            <sc:text id="TextTotalPrice" field="TotalPriceText" runat="server" />
                                                        </span></div>
                                                        <div class="col-sm-6 font-size-12"><span>
                                                            <asp:Literal runat="server" ID="litPrice" /></span></div>
                                                    </div>
                                                    <div class="row margin-row">
                                                        <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                            <sc:text id="TextDeposit" field="DepositText" runat="server" />
                                                        </span></div>
                                                        <div class="col-sm-6 font-size-12"><span>
                                                            <asp:Literal runat="server" ID="litDeposit" /></span></div>
                                                    </div>
                                                    <div class="row margin-row">
                                                        <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                            <sc:text id="TextProcesingFee" field="ProcesingFeeText" runat="server" />
                                                        </span></div>
                                                        <div class="col-sm-6 font-size-12">
                                                            <asp:Literal runat="server" ID="litProcesingFee" /><span></span></div>
                                                    </div>
                                                    <div class="row margin-row" style="display:none;">
                                                        <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                            <sc:text id="TextDueAtCheckout" field="DueAtCheckoutText" runat="server" />
                                                        </span></div>
                                                        <div class="col-sm-6 font-size-12"><span>
                                                            <asp:Literal runat="server" ID="litCheckout" /></span></div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-xs-12 no-padding-m">
                                                <div class="col-sm-12 no-padding-m" id="divAccomodationInfo" runat="server">
                                                    <div class="col-sm-12">
                                                        <h2 class="no-margin">
                                                            <sc:text id="TextAccommodation" field="AccommodationText" runat="server" />
                                                        </h2>
                                                    </div>
                                                    <div class="row margin-row">
                                                        <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                            <sc:text id="TextAccommodationName" field="NameText" runat="server" />
                                                        </span></div>
                                                        <div class="col-sm-6 font-size-12"><span>
                                                            <asp:Literal runat="server" ID="litAccommodationName" /></span></div>
                                                    </div>
                                                    <div class="row margin-row">
                                                        <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                            <sc:text id="TextAccAddress" field="AddressText" runat="server" />
                                                        </span></div>
                                                        <div class="col-sm-6 font-size-12"><span>
                                                            <asp:Literal runat="server" ID="litAccommodationAddress" /></span></div>
                                                    </div>
                                                    <div class="row margin-row">
                                                        <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                            <sc:text id="TextAccPhone" field="PhoneText" runat="server" />
                                                        </span></div>
                                                        <div class="col-sm-6 font-size-12"><span>
                                                            <asp:Literal runat="server" ID="litAccommodationPhone" /></span></div>
                                                    </div>
                                                    <div class="row margin-row">
                                                        <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                            <sc:text id="TextAccEmail" field="EmailText" runat="server" />
                                                        </span></div>
                                                        <div class="col-sm-6 font-size-12"><span>
                                                            <asp:Literal runat="server" ID="litAccommodationEmail" /></span></div>
                                                    </div>
                                                    <div class="row margin-row">
                                                        <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                            <sc:text id="TextNoofRooms" field="NoOfRoomsText" runat="server" />
                                                        </span></div>
                                                        <div class="col-sm-6 font-size-12"><span>1</span></div>
                                                    </div>
                                                    <div class="row margin-row">
                                                        <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                            <sc:text id="TextNoofAdults" field="NoOfAdultsText" runat="server" />
                                                        </span></div>
                                                        <div class="col-sm-6 font-size-12"><span>
                                                            <asp:Literal runat="server" ID="litNoOfAdults" /></span></div>
                                                    </div>
                                                    <div class="row margin-row">
                                                        <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                            <sc:text id="TextNoofChildren" field="NoOfChildrenText" runat="server" />
                                                        </span></div>
                                                        <div class="col-sm-6 font-size-12"><span>
                                                            <asp:Literal runat="server" ID="litNoOfChildren" /></span></div>
                                                    </div>
                                                    <div class="row margin-row">
                                                        <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                            <sc:text id="TextNoOfNights" field="NoOfNightsText" runat="server" />
                                                        </span></div>
                                                        <div class="col-sm-6 font-size-12"><span>
                                                            <asp:Literal runat="server" ID="litNoOfNights" /></span></div>
                                                    </div>
                                                    <div class="row margin-row">
                                                        <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                            <sc:text id="TextOccupancyType" field="OccupancyTypeText" runat="server" />
                                                        </span></div>
                                                        <div class="col-sm-6 font-size-12"><span>
                                                            <asp:Literal runat="server" ID="litOccupancyType" /></span></div>
                                                    </div>
                                                    <div class="row margin-row" style="display:none;">
                                                        <div class="col-sm-6 font-size-12"><span class="accent33-f">Cost</span></div>
                                                        <div class="col-sm-6 font-size-12"><span>
                                                            <asp:Literal runat="server" ID="litCost" /></span></div>
                                                    </div>
                                                    <div class="row margin-row">
                                                        <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                            <sc:text id="TextHotelDue" field="HotelDueText" runat="server" />
                                                        </span></div>
                                                        <div class="col-sm-6 font-size-12"><span>
                                                            <asp:Literal runat="server" ID="litHotelDue" /></span></div>
                                                    </div>
                                                    <div class="row margin-row">
                                                        <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                            <sc:text id="TextOutstanding" field="OutstandingText" runat="server" />
                                                        </span></div>
                                                        <div class="col-sm-6 font-size-12"><span>
                                                            <asp:Literal runat="server" ID="litOutstanding" /></span></div>
                                                    </div>

                                                </div>
                                                <div class="col-sm-12 no-padding-m" id="divAdditionalInfo" runat="server">
                                                    <div class="col-sm-12">
                                                        <h2 class="no-margin">
                                                            <sc:text id="TextAdditionalInformation" field="AdditionalInformationText" runat="server" />
                                                        </h2>
                                                    </div>
                                                    <div class="row margin-row">
                                                        <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                            <sc:text id="TextCreatedBy" field="CreatedByText" runat="server" />
                                                        </span></div>
                                                        <div class="col-sm-6 font-size-12"><span>
                                                            <asp:Literal runat="server" ID="litCreatedBy" /></span></div>
                                                    </div>
                                                    <div class="row margin-row">
                                                        <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                            <sc:text id="TextMethod" field="MethodText" runat="server" />
                                                        </span></div>
                                                        <div class="col-sm-6 font-size-12"><span>
                                                            <asp:Literal runat="server" ID="litBookingMethod" /></span></div>
                                                    </div>
                                                    <div class="row margin-row">
                                                        <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                            <sc:text id="TextLastUpdate" field="LastUpdateText" runat="server" />
                                                        </span></div>
                                                        <div class="col-sm-6 font-size-12"><span>
                                                            <asp:Literal runat="server" ID="litLastUpdate" /></span></div>
                                                    </div>
                                                    <div class="row margin-row">
                                                        <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                            <sc:text id="TextBookingRef" field="BookingRefText" runat="server" />
                                                        </span></div>
                                                        <div class="col-sm-6 font-size-12"><span>
                                                            <asp:Literal runat="server" ID="litBookingRef" /></span></div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <% if(!IsTransferConfirm)  { %>
                                        <div class="col-sm-12 no-padding-m">
                                            <div class="col-sm-4 col-xm-12">
                                                <a href="<%=GetUrl("/bookingdetails?olid="+QueryString("olid"))%>" class="btn btn-default add-break pull-right margin-row width-full">
                                                    <sc:text id="TextCancel" field="CancelText" runat="server" />
                                                </a>
                                            </div>
                                            <% if(ShowTransfer)  { %>
                                            <div class="col-sm-4 col-xm-12">
                                                <a id="transferBooking" href="<%="/locations?olid=" + QueryString("olid") + TransferUrl%>" class="btn btn-default add-break pull-right margin-row width-full">
                                                    <sc:text id="TextTransfer" field="TransferText" runat="server" />
                                                </a>
                                            </div>
                                            <%} %>
                                            <div class="col-sm-4 col-xm-12">
                                                <button type="submit" id="btnPrint" runat="server" onserverclick="btnPrint_ServerClick" class="btn btn-default add-break pull-right margin-row width-full">
                                                    <sc:text id="TextPrint" field="PrintText" runat="server" />
                                                </button>
                                            </div>
                                        </div>
                                        <% } %>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%} %>
    <% if(NewBooking !=null ) { %>
    <div class="col-sm-12">
        <div class="accordian-outer">
            <div class="panel-group" id="accordion2">
                <div class="panel panel-default">
                    <div class="panel-heading bg-primary">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion2" href="#collapse1" class="show-hide-list">
                                <sc:text id="TextTransferDetail" field="TransferDetailText" runat="server" />
                            </a>
                        </h4>
                    </div>
                    <div id="collapse1" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="content_bg_inner_box summary-detail alert-info">
                                        <div class="col-sm-6 col-xs-12 no-padding-m">
                                            <div class="col-sm-12 no-padding-m">
                                                <div class="col-sm-12">
                                                    <h2 class="no-margin">
                                                        <sc:text id="TextNewBookingDates" field="NewBookingDatesText" runat="server" />
                                                    </h2>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                        <sc:text id="TextNewArrival" field="ArrivalText" runat="server" />
                                                    </span></div>
                                                    <div class="col-sm-6 font-size-12"><span><asp:Literal runat="server" ID="litNewBookingArrival" /></span></div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                        <sc:text id="TextNewDeparture" field="DepartureText" runat="server" />
                                                    </span></div>
                                                    <div class="col-sm-6 font-size-12"><span><asp:Literal runat="server" ID="litNewBookingDeparture" /></span></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 no-padding-m">
                                                <div class="col-sm-12">
                                                    <h2 class="no-margin">
                                                        <sc:text id="TextNoteToProvider" field="NoteToProviderText" runat="server" />
                                                    </h2>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-12 font-size-12">
                                                        <textarea id="NoteToProvider" runat="server" class="form-control" rows="5"></textarea>
                                                        <asp:CustomValidator ID="noteToProviderText" ControlToValidate="NoteToProvider" ClientValidationFunction="ClientValidate" Display="Dynamic" ErrorMessage="*Dont Enter Card Number!" ForeColor="Red" Font-Name="verdana" Font-Size="10pt" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 no-padding-m">
                                                <div class="col-sm-12">
                                                    <h2 class="no-margin">
                                                        <sc:text id="TextNewNoOfAdults" field="NoOfAdultsText" runat="server" />
                                                    </h2>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-12 font-size-12">
                                                        <asp:DropDownList ID="ddlAdultCount" runat="server">
                                                            <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                            <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                            <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                            <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                            <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                            <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 no-padding-m">
                                                <div class="col-sm-12">
                                                    <h2 class="no-margin">
                                                        <sc:text id="TextNewNoOfChildren" field="NoOfChildrenText" runat="server" />
                                                    </h2>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-12 font-size-12">
                                                        <asp:DropDownList ID="ddlChildrenCount" runat="server" ClientIDMode="static">
                                                            <asp:ListItem Text="0" Value="0"></asp:ListItem>
                                                            <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                            <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                            <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                            <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                            <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                            <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <input type="hidden" id="hdnChildrenAge" runat="server" clientidmode="static" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 no-padding-m" id="sectionChildrenAge" style="display: none;">
                                                <div class="col-sm-12">
                                                    <h2 class="no-margin">
                                                        <sc:text id="TextAgeOfChildren" field="AgeOfChildrenText" runat="server" />
                                                    </h2>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-12 font-size-12" id="containerChildrenAge">
                                                    </div>
                                                </div>
                                                <div id="ErrorPanelAge" class="col-xs-12 error-msg" style="display: none;">
                                                    <sc:text id="TextPleseSelectChildrenAge" visible="false" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12 no-padding-m">

                                            <div class="col-sm-12 no-padding-m">
                                                <div class="col-sm-12">
                                                    <h2 class="no-margin">
                                                        <sc:text id="TextNewAccommodation" field="NewAccommodationText" runat="server" />
                                                    </h2>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                        <sc:text id="TextNewName" field="NameText" runat="server" />
                                                    </span></div>
                                                    <div class="col-sm-6 font-size-12"><span><asp:Literal runat="server" ID="litNewAccomodationName" /></span></div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                        <sc:text id="TextNewAddress" field="AddressText" runat="server" />
                                                    </span></div>
                                                    <div class="col-sm-6 font-size-12"><span><asp:Literal runat="server" ID="litNewAccomodationAddress" /></span></div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                        <sc:text id="TextNewNoOfRooms" field="NoOfRoomsText" runat="server" />
                                                    </span></div>
                                                    <div class="col-sm-6 font-size-12"><span><asp:Literal runat="server" ID="litNewBookingNumberOfRooms" /> </span></div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                        <sc:text id="TextNewNoOfNights" field="NoOfNightsText" runat="server" />
                                                    </span></div>
                                                    <div class="col-sm-6 font-size-12"><span><asp:Literal runat="server" ID="litNewBookingNumberOfNights" /></span></div>
                                                </div>
                                                <%--<div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12"><span class="accent33-f">Adults:</span></div>
                                                    <div class="col-sm-6 font-size-12"><span><%=NewBooking.NumberOfAdults %></span></div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12"><span class="accent33-f">Children:</span></div>
                                                    <div class="col-sm-6 font-size-12"><span><%=NewBooking.NumberOfChildren %></span></div>
                                                </div>--%>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                        <sc:text id="TextNewOccupancyType" field="OccupancyTypeText" runat="server" />
                                                    </span></div>
                                                    <div class="col-sm-6 font-size-12"><span><asp:Literal runat="server" ID="litNewBookingOccupancyType" /></span></div>
                                                </div>

                                            </div>
                                            <div class="col-sm-12 no-padding-m">
                                                <div class="col-sm-12">
                                                    <h2 class="no-margin">
                                                        <sc:text id="TextPaymentDetailsHeading" field="PaymentDetailsText" runat="server" />
                                                    </h2>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                        <sc:text id="NewTextTotalPrice" field="TotalPriceText" runat="server" />
                                                    </span></div>
                                                    <div class="col-sm-6 font-size-12 no-padding"><span><asp:Literal runat="server" ID="litNewBookingTotalPrice" /></span></div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                        <sc:text id="NewTextDeposit" field="DepositText" runat="server" />
                                                    </span></div>
                                                    <div class="col-sm-6 font-size-12 no-padding"><span><asp:Literal runat="server" ID="litNewBookingDeposit" /></span></div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12"><span class="accent33-f">
                                                        <sc:text id="NewTextProcessingFee" field="ProcesingFeeText" runat="server" />
                                                    </span></div>
                                                    <div class="col-sm-6 font-size-12 no-padding"><span><asp:Literal runat="server" ID="litNewBookingProcessingFee" /></span></div>
                                                </div>
                                                 <div class="row margin-row" style="display:none;">
                                                        <div class="col-sm-6 font-size-12" style="display:none;"><span class="accent33-f">
                                                            <sc:text id="TexNewtDueAtCheckout" field="DueAtCheckoutText" runat="server" />
                                                        </span></div>
                                                        <div class="col-sm-6 font-size-12"><span>
                                                            <asp:Literal runat="server" ID="litNewCheckout" /></span></div>
                                                    </div>
                                            </div>
                                            <% if(IsTransferConfirm) { %>
                                            <div class="row margin-row">
                                                <div class="col-sm-12">
                                                    <%--<asp:Button ID="confirmTransaction" runat="server" onclick="return SetChildrenAge();" OnClick="ButtonConfirmTransaction_Click" Enabled="true" Text="Confirm Transfer" class="btn btn-default add-break pull-right margin-row width-full" />--%>
                                                    <button id="confirmTransaction" clientidmode="static" onclick="return SetChildrenAge(); " runat="server" onserverclick="ButtonConfirmTransaction_Click" enabled="true" class="btn btn-default add-break pull-right margin-row width-full">
                                                        <sc:text id="TextConfirmTransfer" field="ConfirmTransferText" runat="server" />
                                                    </button>

                                                </div>
                                                <div class="col-sm-12">
                                                    <a href="<%=GetUrl("/bookingdetails?olid=" + QueryString("olid")) %>" class="btn btn-default add-break pull-right margin-row width-full">
                                                        <sc:text id="TextCancelTransfer" field="CancelTransferText" runat="server" />
                                                    </a>
                                                </div>
                                            </div>
                                            <% } %>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%} %>
</div>

<script type="text/javascript">

    function SetChildrenAge() {
        var success = true;
        jQuery("div#ErrorPanelAge").hide();
        var age = "";
        jQuery("#containerChildrenAge select").each(function () {
            age += jQuery(this).val() + ",";
            if (jQuery(this).val() === "Select") {
                success = false;
            }
        });

        // alert(age);
        if (success) {
            jQuery("div#ErrorPanelAge").hide();
            __doPostBack('maincontentofflineportal_0$searchresultsofflineportal_0$confirmTransaction', '');
        }
        else {
            jQuery("div#ErrorPanelAge").show();
        }
        return success;
    }



    jQuery(document).ready(function () {
        jQuery("#transferBooking").click(function () {
            jQuery.get("/handlers/cs/baskethelper.ashx?action=reset");
        })

        jQuery("#ddlChildrenCount").change(function () {
            var noOfChildren = parseInt(jQuery(this).val());

            if (noOfChildren === 0) {
                jQuery("div#sectionChildrenAge").hide()
                jQuery("div#containerChildrenAge").html("");
            }
            else {
                jQuery("div#sectionChildrenAge").show()
                var items = "";
                for (i = 0; i < noOfChildren; i++) {
                    items += "<select ><option value='Select'>Select</option><option value='0'>0-12 Months</option><option value='1'>1</option><option value='2'>2</option>" +
                        "<option value='3'>3</option><option value='4'>4</option><option value='5'>5</option><option value='6'>6</option>" +
                    "<option value='7'>7</option><option value='8'>8</option><option value='9'>9</option><option value='10'>10</option>" +
                    "<option value='11'>11</option><option value='12'>12</option></select>"
                }
                jQuery("div#containerChildrenAge").html(items);
            }
        });
    });

<%if(ValidationResponse != null){%>
    jQuery(document).ready(function () {
        CS.Common.ShowAlert('<%= ValidationResponse.Message %>', '', null, {
            "Ok": function () {
                 <%if(ValidationResponse.IsSuccess && HasPendingPayments){%>
                window.location = '<%= GetUrl(((outstandingPayments[0].Amount > 0) ? "/paymenthistory" : "/managecustomerquerys") + "?olid=" + QueryString("olid")) %>'
                <%}else{%>
                $(this).dialog("close");
                <%}%>
            }
        });
    });
    <%}%>
</script>
