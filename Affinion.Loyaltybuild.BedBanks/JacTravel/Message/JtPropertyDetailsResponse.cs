﻿using System;
using System.Xml.Serialization;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Model;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Message.Interfaces;

namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Message
{
    [Serializable]
    public class JtPropertyDetailsResponse : JtResponse, IJtResponse
    { 
        [XmlElement(ElementName = "PropertyID")]
        public string PropertyId { get; set; }
        [XmlElement(ElementName="PropertyReferenceID")]
        public string PropertyReferenceId { get; set; }
        public string PropertyName { get; set; }
        public string PropertyTypeId { get; set; }

        public string GeographyLevel1Id { get; set; }
        public string GeographyLevel2Id { get; set; }
        public string GeographyLevel3Id { get; set; }

        public string Country { get; set; }
        public string Region { get; set; }
        public string Resort { get; set; }
        public string Rating { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string TownCity { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }

        public string MinAdults { get; set; }
        public string MaxAdults { get; set; }
        public string MaxChildren { get; set; }

        public string Strapline { get; set; }
        public string Description { get; set; }

        public string CMSBaseURL { get; set; }
        public string MainImage { get; set; }
        public JtImage[] Images { get; set; }

        public JtFacility[] Facilities { get; set; }
        public string SitecoreHotelId { get; set; }
        //Custom Fields
        public string FullAddress
        {
            get
            {
                string fullAddress = "";

                if (!String.IsNullOrEmpty(Address1)) { fullAddress += Address1 + ", "; }
                if (!String.IsNullOrEmpty(Address2)) { fullAddress += Address2 + ", "; }
                if (!String.IsNullOrEmpty(TownCity)) { fullAddress += TownCity + ", "; }
                if (!String.IsNullOrEmpty(County)) { fullAddress += County + ", "; }
                if (!String.IsNullOrEmpty(Country)) { fullAddress += Country + ", "; }
                if (!String.IsNullOrEmpty(Postcode)) { fullAddress += Postcode; }

                return fullAddress;
            }
        }
        
        public DataLocation DataOrigin { get; set; }
    }
}
