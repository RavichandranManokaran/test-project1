﻿CREATE PROCEDURE [dbo].[LB_GetOfferName]
(
@orderlineid int
)
AS
BEGIN
SELECT [CampaignItemName] [OfferName]
	FROM uCommerce_OrderLine OL
		INNER JOIN [dbo].[uCommerce_PurchaseOrder] PO ON PO.OrderId = OL.OrderId
		INNER JOIN [dbo].[uCommerce_OrderLineDiscountRelation] OLD ON OL.OrderLineId = OLD.OrderLineId
		INNER JOIN [dbo].[uCommerce_Discount] D ON OLD.DiscountId = D.DiscountId
	WHERE
		OL.OrderLineId=@orderlineid
END
