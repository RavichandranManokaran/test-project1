﻿/****** Object:  Table [dbo].[LoginAudit]    Script Date: 4/20/2016 10:40:46 AM ******/


CREATE TABLE [dbo].[LoginAudit](
    [ID] [INT] IDENTITY NOT NULL,
	[UserName] [nvarchar](128) NOT NULL,
	[IsLoginSucess] BIT NOT NULL,
	[LoggedInDate] [datetime] NOT NULL,
 CONSTRAINT [PK_dbo.LoginAudit] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
	
))
