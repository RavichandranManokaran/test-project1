﻿CREATE PROCEDURE [dbo].[LB_GetCommunicationHistorybyHistoryId] 
	@historyId INT 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT CH.[HistoryId]
      ,CT.[Name]
      ,CH.[OrderLineId]
      ,CH.[CreatedDate]
	  ,CH.[From]
	  ,CH.[To]
	  ,CH.[EmailSubject]
      ,CH.[EmailText]
      ,CH.[EmailHtml]
	FROM [dbo].[uCommerce_CommunicationHistory] CH
		INNER JOIN [dbo].[uCommerce_CommunicationTypes] CT ON CT.CommunicationTypeId=CH.CommunicationTypeId
	WHERE CH.HistoryId=@historyId
END
