﻿CREATE TABLE [dbo].[uCommerce_PaymentProperty] (
    [PaymentPropertyId] INT            IDENTITY (1, 1) NOT NULL,
    [PaymentId]         INT            NOT NULL,
    [Key]               NVARCHAR (255) NOT NULL,
    [Value]             NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_uCommerce_PaymentProperty] PRIMARY KEY CLUSTERED ([PaymentPropertyId] ASC),
    CONSTRAINT [FK_uCommerce_PaymentProperty_uCommerce_Payment] FOREIGN KEY ([PaymentId]) REFERENCES [dbo].[uCommerce_Payment] ([PaymentId])
);

