﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
///Source File:           uCommerceConnectionFactory.cs
///Sub-system/Module:     Affinion.LoyaltyBuild.Common.uCommerceUtility
///Description:           Entity class for Country
///</summary>
#region usingDirectives
using Affinion.LoyaltyBuild.Common.Exceptions;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.SessionHelper;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

#endregion

namespace Affinion.LoyaltyBuild.Common.uCommerceUtility
{
    public class uCommerceConnectionFactory
    {
        public static string GetuCommerceConnetionString()
        {
            try
            {
                return (ConfigurationManager.ConnectionStrings["UCommerce"].ConnectionString.Length != 0) ? ConfigurationManager.ConnectionStrings["UCommerce"].ConnectionString : string.Empty;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(null, exception.InnerException);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="basketInfo"></param>
        public static void InsertCustomOrderLineData(BasketInfo basketInfo)
        {
            try
            {
                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        string sql = "UpdateCustomOrderDetails";

                        using (SqlCommand command = new SqlCommand(sql, con))
                        {                           

                            DateTime checkout, checkin;
                            DateTime.TryParse(basketInfo.CheckoutDate, out checkout);
                            DateTime.TryParse(basketInfo.CheckinDate, out checkin);

                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@CheckinDate", SqlDbType.DateTime)).Value = checkout;
                            command.Parameters.Add(new SqlParameter("@CheckOutDate", SqlDbType.DateTime)).Value = checkin;
                            command.Parameters.Add(new SqlParameter("@NofAdults", SqlDbType.Int)).Value = 1; // Int32.Parse(basketInfo.NoOfAdults);
                            command.Parameters.Add(new SqlParameter("@NoofChilderen", SqlDbType.Int)).Value = 1; //Int32.Parse(basketInfo.NoOfChildren);
                            con.Open();
                            command.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(null, exception.InnerException);
            }
        }

        /// <summary>
        /// UpdateCustomOrderLineData
        /// </summary>
        /// <param name="basketInfo"></param>
        /// <param name="id"></param>
        public static void UpdateCustomOrderLineData(BasketInfo basketInfo , int id)
        {
            try
            {
                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        string sql = "UpdateCustomOrderDetails";

                        using (SqlCommand command = new SqlCommand(sql, con))
                        {

                            DateTime checkout, checkin;
                            DateTime.TryParse(basketInfo.CheckoutDate, out checkout);
                            DateTime.TryParse(basketInfo.CheckinDate, out checkin);

                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@CheckinDate", SqlDbType.DateTime)).Value = checkin;
                            command.Parameters.Add(new SqlParameter("@CheckOutDate", SqlDbType.DateTime)).Value = checkout;
                            command.Parameters.Add(new SqlParameter("@NofAdults", SqlDbType.Int)).Value = 1; // Int32.Parse(basketInfo.NoOfAdults);
                            command.Parameters.Add(new SqlParameter("@NoofChilderen", SqlDbType.Int)).Value = 1; //Int32.Parse(basketInfo.NoOfChildren);
                            command.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int)).Value = id; //Int32.Parse(basketInfo.NoOfChildren);
                            command.Parameters.Add(new SqlParameter("@Sku", SqlDbType.VarChar)).Value = basketInfo.Sku; //Int32.Parse(basketInfo.NoOfChildren);
                            con.Open();
                            command.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(null, exception.InnerException);
            }
        }
        
        /// <summary>
        /// GetPurchaseOrderLines
        /// </summary>
        /// <param name="id"></param>
        public static DataSet GetPurchaseOrderLines(int id)
        {
            try
            {
                DataSet ds = new DataSet();

                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        string sql = "GetPurchaseOrderDetails";
                        
                        using (SqlCommand command = new SqlCommand(sql, con))
                        {                           
                            command.CommandType = CommandType.StoredProcedure;                            
                            command.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int)).Value = id; //Int32.Parse(basketInfo.NoOfChildren);
                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                        }
                    }
                }

                return ds;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(null, exception.InnerException);
            }
        }
    }
}
