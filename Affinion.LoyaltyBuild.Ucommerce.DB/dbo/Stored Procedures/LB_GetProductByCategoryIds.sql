﻿--exec GetProductByCategoryIds 'Ar_1122,as_1125','9671','9672'

CREATE procedure [dbo].[GetProductByCategoryIds]

@productid nvarchar(2000),

@startdate int,

@enddate int



As



DECLARE @tblproduct AS TABLE (	[productid] [int],	[SKU] nvarchar(30) , ProductSKU nvarchar(30) )

INSERT INTO @tblproduct

SELECT up.ProductId, up.Sku ,up.VariantSku  FROM  uCommerce_product up 

INNER JOIN uCommerce_ParseArrayToTable(@productid,',',0) tmp on up.SKu = tmp.stringvalue



select ucp.ProductId , ucp.SKU , ucp.VariantSku as VSKU, ucp.Name , (case when ISNULL(ucp.VariantSku,'') <> '' then 'true' else 'false' end ) as IsVariant

from @tblproduct uc

inner join uCommerce_Product ucp on uc.productid = ucp.productid

--and ucp.allowordering = 1 and ucp.DisplayOnSite = 1





select uc.SKU as SKU ,  uc.ProductSKU as VSKU , ucpp.ProductId, ucpdf.Name, ucpp.Value from @tblproduct uc

inner join uCommerce_ProductProperty ucpp on uc.ProductId = ucpp.productid

inner join uCommerce_ProductDefinitionField ucpdf on ucpp.ProductDefinitionFieldId  = ucpdf.ProductDefinitionFieldId and ucpdf.Deleted = 0 

where ISNULL(uc.ProductSKU,'' ) = ''

 

select uc.SKU as SKU ,  uc.ProductSKU as VSKU , ucpp.ProductId, ucpdf.Name, ucpp.Value from @tblproduct uc

inner join uCommerce_ProductProperty ucpp on uc.ProductId = ucpp.productid

inner join uCommerce_ProductDefinitionField ucpdf on ucpp.ProductDefinitionFieldId  = ucpdf.ProductDefinitionFieldId and ucpdf.Deleted = 0 

where ISNULL(uc.ProductSKU,'' ) <> ''





select tc.sku as SKU,  (oa.NumberAllocated-oa.NumberBooked) as Availability, oa.DateCounter , 0 as OfferId, oa.VariantSku as VSKU , case when oa.PackagePrice > 0 then oa.PackagePrice else  oa.price end  as price , oa.PriceBand as PriceBand

from  LB_RoomAvailability oa

INNER JOIN @tblproduct tc  ON oa.Sku = tc.SKU and oa.variantsku = tc.ProductSKU

where  ISNULL(oa.IsCloseOut,0) = 0 and ISNULL(oa.IsStopSellOn,0) = 0 and DateCounter>=@startdate and DateCounter<=@enddate





select tc.sku as SKU , (oa.NumberAllocated-oa.NumberBooked) as Availability, oa.DateCounter , oa.CampaignItem as OfferId, oa.VariantSku as VSKU 

 from  @tblproduct tc  

 INNER JOIN LB_SupplierInvite si  ON si.SKU = tc.SKU

inner join  LB_OfferAvailability oa on si.CampaignItem = oa.CampaignItem

where oa.DateCounter>=@startdate and oa.DateCounter<=@enddate and si.IsSubscribe = 1




