﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;


namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Helper
{
    public class SessionHelper : IRequiresSessionState
    {

        /// <summary>
        /// Add a new Item to session
        /// </summary>
        /// <param name="objectToSession"></param>
        /// <param name="sessionName"></param>
        public static void AddItem(object objectToSession, string sessionName)
        {
            HttpContext.Current.Session[sessionName] = objectToSession;
        }        
        
        /// <summary>
        /// Retrive cached item
        /// </summary>
        /// <typeparam name="T">Type of Session Item</typeparam>
        /// <param name="sessionName">Name of Session Item</param>
        /// <returns>Session Item as type</returns>
        public static T GetItem<T>(string sessionName) where T : class
        {
            try
            {
                return (T)HttpContext.Current.Session[sessionName];
            }
            catch
            {
                return null;
            }
        }
        /// <summary>
        /// Remove Item from the Session.
        /// </summary>       
        /// <param name="sessionName"></param>
        public static void RemoveItem(string sessionName)
        {
            HttpContext.Current.Session.Remove(sessionName);
        }
        
        /// <summary>
        /// Check for the session existance.
        /// </summary>
        /// <param name="sessionName"></param>      
        public static bool IsExisting(string sessionName)
        {
            if (HttpContext.Current.Session[sessionName] != null)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Clear session
        /// </summary>
        public static void SessionClear()
        {
            HttpContext.Current.Session.Clear();
        }
    }

    public abstract class SessionKeys
    {

        public const string loggedInSupplier = "supplierName";
        public const string loggedInUserName = "UserName";
        public const string loggedInContactName = "contactName";
        public const string GroupHotelName = "grpHotelName";
        public const string lastLoggedInDate = "LastLoggedInDate";
        public const string currentLoggedinUserRole = "LoggedInUserRole";
        public const string currentSupplierItemForUpdate = "CurrentItemForUpdate";
        public const string dropDownSelectedValue = "DropDownSelectedValue";
        public const string currentContactItemForUpdate = "CurrentContactItemForUpdate";
        public const string loggedInContactDisplayName = "LoggedInContactDisplayName";
        public const string loggedinSupplierDisplayName = "loggedinSupplierDisplayName";
        public const string dropDownSelectedItem = "dropDownSelectedItem ";
        public const string loggedInSupplier_ucom = "UCommerceCategoryID ";
        public const string listBoxSelectedValue = "listBoxSelectedValue";
        public const string supplierTypeDropDown = "supplierTypeDropDown";
        public const string typeDropDownSelectedValue = "typeDropDownSelectedValue";
        public const string AvailabilityStartDate = "AvailabilityStartDate";
        public const string AvailabilityEndDate = "AvailabilityEndDate";
        public const string listBoxSelectedText = "listBoxSelectedText";
        public const string groupListBoxSelectedText = "groupListBoxSelectedText";
        public const string loggedInUserAdmin = "loggedInUserAdmin";

    }

}