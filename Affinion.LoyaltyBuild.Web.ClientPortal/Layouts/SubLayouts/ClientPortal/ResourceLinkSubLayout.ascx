﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResourceLinkSubLayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal.ResourceLinkSubLayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Sitecore.Data.Items" %>

<h3>
    <sc:Text Field="Res_MainTitle" runat="server" />
</h3>
<h4>
    <sc:Text Field="Res_SubTitle" runat="server" />
</h4>

<div class="row-resource">
    <asp:Repeater ID="ResourceRepeater" runat="server">
        <ItemTemplate>
            <div class="group inline-block width-full <%# this.GetStyleClass(Container.ItemIndex) %>">
                <div class="col-md-3 col-xs-3">
                    <div class="row-resource-row-icon">
                        <a href="#">
                            <sc:Image Field="Res_Image" Item="<%#(Item)Container.DataItem %>" alt="" class="img-responsive" runat="server" />
                        </a>
                    </div>
                </div>

                <div class="col-md-9 col-xs-9">
                    <div class="row-resource-row">
                        <a href="#">
                            <sc:Text Field="Res_Href" Item="<%#(Item)Container.DataItem %>" runat="server" />
                        </a>
                        <p></p>
                        <h6><sc:Text Field="Res_Text" Item="<%#(Item)Container.DataItem %>" runat="server" /></h6>
                        <p></p>
                    </div>

                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
</div>
