﻿namespace Affinion.LoyaltyBuild.Web.Tests.Layouts.SubLayouts.ClientSetup.Wizard
{
    using Affinion.LoyaltyBuild.Common.Wizard;
    using System;

    public partial class Wizard : System.Web.UI.UserControl
    {
        private void Page_Load(object sender, EventArgs e)
        {
            //Session["Username"] = "txtUserName.Text.Trim()";
            HtmlHelper.PopulateWizardContent();
            DivWizard.InnerHtml = HtmlHelper.RenderHtml;
        }
    }
}