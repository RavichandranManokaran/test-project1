﻿
declare @datatypeid int = 0
declare @fieldid int = 0

select @datatypeid = DataTypeId from uCommerce_DataType where DefinitionName= 'SiteCorePaymentMethod'
if(@datatypeid = 0)
begin
	insert into uCommerce_DataType (TypeName,Nullable,ValidationExpression,BuiltIn,DefinitionName,Deleted,Guid,CreatedOn,
	CreatedBy,ModifiedOn,ModifiedBy)
	values ('PaymentMethod',1,'',0,'SiteCorePaymentMethod',0,NEWID(),GETDATE(),'admin',GETDATE(),'admin')
	
	set @datatypeid=@@identity 
	
	insert into uCommerce_DefinitionField(DataTypeId,DefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,BuiltIn,DefaultValue,Guid)
	values (@datatypeid , 2,'PaymentMethod',1,0,1,0,0,0,0,'',NEWID())
	
	set @fieldid=@@identity 
	
	insert into [uCommerce_DefinitionFieldDescription] ([CultureCode],[DisplayName],[Description],[DefinitionFieldId])
	values ('en','PaymentMethod','',@fieldid)
 END
 go
