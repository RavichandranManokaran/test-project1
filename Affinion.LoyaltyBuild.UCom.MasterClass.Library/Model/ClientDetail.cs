﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    public class ClientDetail
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string ClientGUID { get; set; }

        public bool HasSubRate { get; set; }
    }
}
