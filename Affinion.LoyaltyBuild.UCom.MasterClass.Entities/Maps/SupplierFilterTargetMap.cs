﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2.Maps;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Maps
{
    public class SupplierFilterTargetMap : BaseClassMap<SupplierFilter>
    {
        // Methods
        public SupplierFilterTargetMap()
        {
            base.Table("uCommerce_SupplierFilter");
            base.Id(x => x.SupplierFilterId, "SupplierFilterId").GeneratedBy.Identity();
            Map(x => x.SearchFilter).Not.Nullable();
            Map(x => x.CampaignItem).Not.Nullable();      
        }
    }
}

