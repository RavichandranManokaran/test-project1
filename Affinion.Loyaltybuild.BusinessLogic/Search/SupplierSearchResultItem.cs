﻿//<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SupplierSearchResultItem.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.BusinessLogic.Search
/// Description:           Represents search result item
/// </summary>
namespace Affinion.LoyaltyBuild.BusinessLogic.Search
{
    #region Using Statements

    using Affinion.Loyaltybuild.BusinessLogic.Helper;
    using Affinion.LoyaltyBuild.BusinessLogic.Search.OfferFilters;
    using Affinion.LoyaltyBuild.Common;
    using Affinion.LoyaltyBuild.Common.Exceptions;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Search;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.DataAccess.Helpers;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Availability;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
    using Newtonsoft.Json;
    using Sitecore.ContentSearch;
    using Sitecore.ContentSearch.SearchTypes;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Reflection;
    using UCommerce.EntitiesV2;
    using Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi;

    #endregion

    public class SupplierSearchResultItem : SearchResultItem
    {
        public SupplierSearchResultItem()
        {
            Reason = OfferSearchFailedFilter.Default;
        }

        #region Properties

        [IndexField("Name")]
        public string SupplierName { get; set; }

        [IndexField("AddressLine1")]
        public string AddressLine1 { get; set; }

        [IndexField("AddressLine2")]
        public string AddressLine2 { get; set; }

        [IndexField("AddressLine3")]
        public string AddressLine3 { get; set; }

        [IndexField("AddressLine4")]
        public string AddressLine4 { get; set; }

        [IndexField("Location")]
        public string Location { get; set; }

        [IndexField("Town")]
        public string Town { get; set; }

        [IndexField("Country")]
        public string Country { get; set; }

        [IndexField("StarRanking")]
        public string StarRanking { get; set; }

        [IndexField("LocalRanking")]
        public string LocalRanking { get; set; }

        [IndexField("Clients")]
        public string Clients { get; set; }

        [IndexField("ClientOffers")]
        public string ClientOffers { get; set; }

        [IndexField("OfferGroups")]
        public string OfferGroups { get; set; }

        [IndexField("LowestPrice")]
        public string LowestPrice { get; set; }

        [IndexField("ProductPrices")]
        public string ProductPricesValue { get; set; }

        public OfferSearchFailedFilter Reason { get; set; }


        /// <summary>
        /// Gets the product prices
        /// </summary>
        public Collection<ProductPrice> ProductPrices
        {
            get
            {
                Collection<ProductPrice> prices = JsonConvert.DeserializeObject<Collection<ProductPrice>>(this.ProductPricesValue);

                if (prices == null)
                {
                    return null;
                }

                return prices;
            }
        }

        #endregion

        #region Public Functions
        /// <summary>
        /// Gets the processed list of subscribed offers from current supplier
        /// </summary>
        /// <returns>Returns a collection of offers</returns>
        public SupplierData GetOffers(Item clientItem, Item supplierItem, SearchKey keywords)
        {
            try
            {
                int maxSupplierPeopleCount = 0;
                int maxSupplierAdultCount = 0;
                decimal lowestPrice = 0;
                string lowestClientPrice = string.Empty;
                string lowestPriceOccupancyId = string.Empty;

                if (clientItem == null || supplierItem == null || keywords == null || keywords.CheckinDate == DateTime.MinValue || keywords.CheckoutDate == DateTime.MinValue)
                {
                    return new SupplierData();
                }

                //SupplierData supplierOffers = GetAllOffers(clientItem, ClientOffers);
                SupplierData supplier = StoreHelper.GetProducts(supplierItem, clientItem);
                Collection<OfferDataItem> filteredOffersList = new Collection<OfferDataItem>();

                //SupplierData testSupplier = RoomHelper.GetOffers(supplierItem, clientItem, keywords.CheckinDate, keywords.CheckoutDate);

                if (supplier == null)
                {
                    return new SupplierData();
                }

                supplier.AddonProducts = StoreHelper.GetAddons(supplierItem, clientItem);

                for (int i = 0; i < supplier.Offers.Count; i++)
                {
                    OfferDataItem offer = supplier.Offers[i];

                    // check availability
                    offer.Availability = GetAvailability(supplierItem, offer, keywords);

                    if (SkipOffer(offer, keywords, supplierItem, clientItem))
                    {
                        continue;
                    }

                    if (lowestPrice == 0 || offer.Price < lowestPrice)
                    {
                        lowestPrice = offer.Price;
                        lowestClientPrice = offer.PriceWithCurrency;
                        lowestPriceOccupancyId = offer.OccupancyTypeId;
                    }

                    offer.Tip = GetAvailabilityAlert(clientItem, offer);

                    maxSupplierPeopleCount = maxSupplierPeopleCount + (offer.OccupancyType.MaxPeople * offer.Availability);

                    maxSupplierAdultCount = maxSupplierAdultCount + (offer.OccupancyType.MaxAdults * offer.Availability);

                    filteredOffersList.Add(offer);
                }


                // Set filtered offers list to dataset
                supplier.Offers = filteredOffersList;
                supplier.LowestPrice = lowestPrice;
                supplier.LowestPriceWithCurrency = lowestClientPrice;
                supplier.LowestPriceOccupancyTypeId = lowestPriceOccupancyId;

                supplier = skipSupplier(supplier, clientItem, keywords, maxSupplierPeopleCount, maxSupplierAdultCount);

                return supplier;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, typeof(SearchResultItem));
                return new SupplierData();
            }
        }

        /// <summary>
        /// Get lowest product price from available occupancy types
        /// </summary>
        /// <param name="clientId">Current client's sitecore id</param>
        /// <param name="availableOccupancyTypeIds">List of available occupancy type ids of the current supplier</param>
        /// <returns>Returns the lowest available price from the client</returns>
        public decimal GetLowestProductPrices(string clientId, Collection<string> availableOccupancyTypeIds)
        {
            try
            {
                var prices = this.ProductPrices.Where(p => p.ClientId == clientId && availableOccupancyTypeIds.Contains(p.OccupancyTypeId)).OrderBy(p => p.Price).FirstOrDefault();
                return prices.Price;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, typeof(SearchResultItem));
                return 0;
            }
        }

        /// <summary>
        /// Get the lowest price for a iven client
        /// </summary>
        /// <param name="client">the client</param>
        /// <param name="ignoreValue">ignore value to be set incase no value found for the product price</param>
        /// <returns>returns the decimal price value</returns>
        public decimal GetLowestPrice(Item client, decimal ignoreValue)
        {
            try
            {
                if (string.IsNullOrEmpty(this.LowestPrice))
                {
                    return ignoreValue;
                }

                string clientId = client.ID.ToString();
                string clientPrice = this.LowestPrice.Split(',').Where(s => s.Contains(clientId)).First().ToString().Replace(clientId + "=", "");
                decimal value = decimal.Parse(clientPrice);

                if (value == 0)
                {
                    return ignoreValue;
                }

                return value;
            }
            catch (NullReferenceException ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.BusinessLogic, ex, null);
                return ignoreValue;
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Skip supplier
        /// </summary>
        /// <param name="supplier">Supplier data</param>
        /// <param name="keywords">Search keywords</param>
        /// <param name="maxSupplierPeopleCount">Max people count supplier can facilitate</param>
        /// <returns>Supplier data</returns>
        private SupplierData skipSupplier(SupplierData supplier, Item clientItem, SearchKey keywords, int maxSupplierPeopleCount = 0, int maxSupplierAdultCount = 0)
        {

            if (supplier == null || supplier.Offers == null || !supplier.Offers.Any())
            {
                return supplier;
            }

            if (keywords == null)
            {
                throw new ArgumentNullException("keywords");
            }

            if (clientItem == null)
            {
                throw new ArgumentNullException("clientItem");
            }

            string clientCountry = SitecoreFieldsHelper.GetValue(clientItem, Constants.CountryFieldName);

            int noOfPeopleToConsider = keywords.NumberOfPeople;

            ///Ireland related validations
            if (clientCountry.ToLower().Equals(Constants.Ireland.ToLower()))
            {

                if (keywords.LessThanTwoChildrenCount >= 1)
                {
                    int childrenNotconsidered = keywords.LessThanTwoChildrenCount;

                    ///Number equal to room count is ignored, assuming one room can ignore one child
                    if (childrenNotconsidered > keywords.NumberOfRooms)
                    {
                        childrenNotconsidered = keywords.NumberOfRooms;
                    }

                    noOfPeopleToConsider = noOfPeopleToConsider - childrenNotconsidered;
                }
            }

            if (maxSupplierPeopleCount < noOfPeopleToConsider)
            {
                supplier.Offers = new Collection<OfferDataItem>();
                //supplier.Reason = Affinion.LoyaltyBuild.Common.Search.SupplierData.FailedFilter.AdultAndChildCount;
                Reason = OfferSearchFailedFilter.AdultAndChildCount;

            }
            else if (maxSupplierAdultCount < keywords.NumberOfAdults)
            {
                supplier.Offers = new Collection<OfferDataItem>();

                Reason = OfferSearchFailedFilter.AdultAndChildCount;
            }

            return supplier;
        }

        /// <summary>
        /// Convert the json string to a Collection<OffersDataSet> and finds the relevant data set to the given client and returns it
        /// </summary>
        /// <param name="clientItem">The current client item</param>
        /// <param name="offersString">The client offers string returned by lucene search</param>
        /// <returns>Returns the OffersDataSet object of the current client</returns>
        private static SupplierData GetAllOffers(Item clientItem, string offersString)
        {
            if (clientItem == null || string.IsNullOrEmpty(offersString))
            {
                throw new AffinionException("SupplierSearhcResultsItem - GetAllOffers - Arguments can not be null.");
            }

            Collection<SupplierData> offersDataSetCollection = JsonConvert.DeserializeObject<Collection<SupplierData>>(offersString);
            SupplierData offersDataSet = new SupplierData();

            try
            {
                offersDataSet = offersDataSetCollection.Where(offer => offer.ClientId == clientItem.ID.ToString()).First();
            }
            catch (NullReferenceException ex)
            {
                // no offers found for the client
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, typeof(SupplierData));
            }

            return offersDataSet;
        }

        /// <summary>
        /// Returns true to skip the offer
        /// </summary>
        /// <param name="product">Current offer</param>
        /// <param name="searchKey">Search term</param>
        /// <param name="supplier">Current supplier</param>
        /// <param name="client">Current Client</param>
        /// <returns>Returns true to include the offer</returns>
        private static bool SkipOffer(OfferDataItem product, SearchKey searchKey, Item supplier, Item client)
        {
            bool skip = false;
            OfferFilterData filterData = new OfferFilterData();

            filterData.Client = client;
            filterData.Offer = product;
            filterData.SearchTerm = searchKey;
            filterData.Supplier = supplier;

            foreach (Type type in GetOfferFilters())
            {
                IOfferFilter instance = (IOfferFilter)Activator.CreateInstance(type);
                skip = !instance.IncludeOffer(filterData);

                if (skip)
                {
                    break;
                }
            }

            return skip;
        }

        /// <summary>
        /// Gets Implementations of IOfferFilter in the "Affinion.LoyaltyBuild.Common.Search.OfferFilters" namespace
        /// </summary>
        /// <returns>Returns an array of types</returns>
        private static Type[] GetOfferFilters()
        {
            return Assembly.GetExecutingAssembly().GetTypes().
                Where(t =>
                    String.Equals(t.Namespace, "Affinion.LoyaltyBuild.BusinessLogic.Search.OfferFilters", StringComparison.Ordinal) &&
                    Type.Equals(t.GetInterface("IOfferFilter"), typeof(IOfferFilter))
                ).ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="supplier"></param>
        /// <param name="offer"></param>
        /// <returns></returns>
        public int GetAvailability(Item supplier, OfferDataItem offer, SearchKey searchTerm)
        {
            DateTime julianFixed = new DateTime(1990, 1, 1);
            int minimumAvailableProductCount = 0;

            ///Get check-in date in julian format
            //int checkInDateCount = (int)(searchTerm.CheckinDate - julianFixed).TotalDays;
            int checkInDateCount = DateTimeHelper.GetJulianDateCount(searchTerm.CheckinDate);

            ///Get check-out date in julian format
            //int checkOutDateCount = (int)(searchTerm.CheckoutDate - julianFixed).TotalDays;
            int checkOutDateCount = DateTimeHelper.GetJulianDateCount(searchTerm.CheckoutDate);


            int productSku;

            if (int.TryParse(offer.OfferSku, out productSku))
            {
                ///Track which dates are available
                Dictionary<int, bool> availableDates = Enumerable.Range(checkInDateCount, checkOutDateCount - checkInDateCount).ToDictionary(x => x, x => false);

                //IList<dynamic> results = DataAccessSearchHelper.GetAvailableProductList(productSku, checkInDateCount, checkOutDateCount);
                IList<dynamic> results = new List<dynamic>();

                Category uCommerceCategory = Category.FirstOrDefault(x => x.CategoryId == SitecoreFieldsHelper.GetInteger(supplier, "UCommerceCategoryID", 0));

                RoomAvailabilityService roomAvailabilityService = new RoomAvailabilityService();
                List<RoomAvailabilityDetail> roomAvailabilityDetails = roomAvailabilityService.FindRoomAvailability(Convert.ToString(uCommerceCategory.Guid), offer.OfferSku, searchTerm.CheckinDate, searchTerm.CheckoutDate, false);

                foreach (RoomAvailabilityDetail roomAvailability in roomAvailabilityDetails)
                {
                    ///Get available room count to be booked
                    //int availableProductCount = result.NumberAllocated - result.NumberBooked;
                    if (roomAvailability.AvailableRoom == 0)
                    {
                        return roomAvailability.AvailableRoom;
                    }
                    else if (minimumAvailableProductCount == 0 || roomAvailability.AvailableRoom < minimumAvailableProductCount)
                    {
                        ///Set minimum available product count for selected period.
                        minimumAvailableProductCount = roomAvailability.AvailableRoom;
                    }

                    KeyValuePair<int, bool> availability = availableDates.Single(a => a.Key == roomAvailability.AvailabilityDate.ConvetDatetoDateCounter());

                    availableDates[availability.Key] = true;
                }

                ///Room is not available for all selected dates
                if (availableDates.Any(a => a.Value == false))
                {
                    return 0;
                }
                else
                {
                    return minimumAvailableProductCount;
                }
            }
            else
            {
                return 0;
            }

        }

        /// <summary>
        /// Get the Availability alert message
        /// </summary>
        /// <param name="clientItem">client item</param>
        /// <param name="offer">current offer item</param>
        /// <returns>returns the availability alert</returns>
        private string GetAvailabilityAlert(Item clientItem, OfferDataItem offer)
        {
            Collection<Item> alerts = SitecoreFieldsHelper.GetMutiListItems(clientItem, "Alerts");
            string message = (from alert in alerts
                              where alert.Fields["Alert"].Value == offer.Availability.ToString()
                              select alert.Fields["Message"].Value).FirstOrDefault();

            return message;
        }

        #endregion

    }
}
