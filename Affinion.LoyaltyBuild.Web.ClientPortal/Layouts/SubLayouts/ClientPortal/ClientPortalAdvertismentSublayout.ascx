﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientPortalAdvertismentSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal.ClientPortalAdvertismentSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>


<div class="col-xs-12 no-padding">
    <div class="col-md-12 no-padding">
        <div class="adv-banner-area">
            <div class="col-xs-12">
                <div class="adv-banner">
                    <sc:Placeholder ID="Placeholder2" Key="advertisment" runat="server" />
                </div>
            </div>
        </div>
    </div>
</div>
