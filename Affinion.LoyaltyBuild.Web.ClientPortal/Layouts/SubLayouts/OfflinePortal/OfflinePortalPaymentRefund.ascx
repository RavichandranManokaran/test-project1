﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalPaymentRefund.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalPayments" %>
<div class="col-sm-12">
    <div class="panel panel-default">
        <div id="divErrorMsg" runat="server" class="alert alert-danger" visible ="false">
        <p>Sorry. An error occured while processing your refund.</p>
        </div>
        <div id="divTotalAmt" runat="server" class="alert alert-danger" visible ="false">
        <p>Sum of entered amounts should match with total amounts.</p>
        </div>
         <div id="cardPanelSelectDiv" runat="server" class="alert alert-danger" visible ="false">
        <p>Please select Realex ID for processing refund.</p>
        </div>
        <div class="panel-heading">
            <h4 class="panel-title">Refund</h4>
        </div>
        <asp:HiddenField ID="HiddenFieldPaymentID" runat="server" />
        <asp:HiddenField ID="HiddenPaymentAmount" runat="server" />
        <div class="panel-body alert-info">
            <div class="panel-body alert-info">

                <div class="col-sm-12 no-padding-m">
                    <div class="content_bg_inner_box alert-info">
                        <div class="col-sm-6 no-padding-m">
                            <div class="row margin-row">
                                <div class="col-sm-5"><span class="bask-form-titl font-size-12">Currency:<span class="accent58-f"></span></span></div>
                                <div class="col-sm-7">
                                    <asp:DropDownList  ID="ddlCurrency" runat="server" Enabled="False" CssClass="form-control font-size-12" />
                                </div>
                            </div>
                            <div class="row margin-row">
                                <div class="col-sm-5"><span class="bask-form-titl font-size-12">Payment Method:<span class="accent58-f"> </span></span></div>
                                <div class="col-sm-7">
                                    <asp:DropDownList  ID="ddlPaymentMethod" OnSelectedIndexChanged="ddlPaymentMethod_SelectedIndexChanged" runat="server" AutoPostBack="true"   CssClass="form-control font-size-12" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-12">
    <div class="panel panel-default" id="refundbycheque" runat="server">
                <div class="panel-heading">
            <h4 class="panel-title">Refund by Cheque</h4>
        </div>
        <div class="panel-body alert-info">
            <div class="col-sm-6 no-padding-m">
                <div class="row margin-row">
                    <div class="col-sm-5"><span class="bask-form-titl font-size-12">Total Amount:</span></div>
                    <div class="col-sm-7">
                        <asp:TextBox ID="ChequeTotalAmountText" runat="server" Enabled="false" CssClass="form-control font-size-12" />
                    </div>
                </div>
            </div>
            <div class="row margin-row">
                <div class="col-sm-6">
                    <asp:Button ID="BtnChequeRefund" OnClick="BtnChequeRefund_Click" class="btn btn-default pro-check" runat="server" Text="Refund" />
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-12">
    <div class="panel panel-default" id="refundbycard" runat="server">
        <div class="panel-heading">
            <h4 class="panel-title">Refund by Credit Card</h4>
        </div>
        <div class="panel-body alert-info">

            <div class="col-sm-12 no-padding-m">
                <div class="content_bg_inner_box alert-info">
                    <div class="col-sm-6 no-padding-m">
                        <div class="row margin-row">
                            <div class="col-sm-5"><span class="bask-form-titl font-size-12">Total Amount:</span></div>
                            <div class="col-sm-7">
                                <asp:TextBox ID="CreditCardTotalAmountTxt" runat="server" Enabled="false" />
                            </div>
                        </div>
                         <div class="row margin-row">
                            <div class="col-sm-5"><span class="bask-form-titl font-size-12">Commission Amount:</span></div>
                            <div class="col-sm-7">
                                 <asp:DropDownList  ID="DropDownListCommissionAmount" runat="server"  CssClass="form-control font-size-12" OnSelectedIndexChanged="DropDownListCommissionAmount_SelectedIndexChanged" runat="server">
                                 <asp:ListItem Selected="True" Text="Ordinary Refund-Default" Value="0"></asp:ListItem> 
                                     <asp:ListItem Text="Mismatch" Value="1"></asp:ListItem>  
                                     <asp:ListItem  Text="Double Booking" Value="2"></asp:ListItem>
                                     <asp:ListItem Text="Hotel Overbooked" Value="3"></asp:ListItem>
                                     <asp:ListItem  Text="24HR Rule/Med Cert" Value="4"></asp:ListItem> 
                                     <asp:ListItem Text="Voucher" Value="5"></asp:ListItem>
                                     <asp:ListItem  Text="CustomerBooked Wrongbreak" Value="6"></asp:ListItem>
                                     <asp:ListItem Text="CustomerService Issue" Value="7"></asp:ListItem>   
                                 </asp:DropDownList>
                            </div>
                        </div>

                        <div class="row margin-row" id="ProviderAmountDiv" runat="server">
                            <div class="col-sm-5"><span class="bask-form-titl font-size-12">Provider Amount:</span></div>
                            <div class="col-sm-7">
                                <asp:TextBox ID="ProviderAmountText" runat="server" CssClass="form-control font-size-12" Visible="False" />
                            </div>
                        </div>
                        <div class="row margin-row" id="CommissionAmountDiv" runat="server">
                            <div class="col-sm-5"><span class="bask-form-titl font-size-12">Commission Amount:</span></div>
                            <div class="col-sm-7">
                                <asp:TextBox ID="CommissionAmountText" runat="server" CssClass="form-control font-size-12"  Visible="False"/>
                            </div>
                        </div>
                        <div class="row margin-row" id="ProcessingFeeAmountDiv" runat="server">
                            <div class="col-sm-5"><span class="bask-form-titl font-size-12">Processing Fee Amount:</span></div>
                            <div class="col-sm-7">
                                <asp:TextBox ID="ProcessingFeeAmountText" runat="server" CssClass="form-control font-size-12" Visible="False" />
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="row margin-row">
                            <div class="table-responsive">
                                <asp:GridView ID="PaymentGridView" AutoGenerateColumns="false" OnSelectedIndexChanged="PaymentGridView_SelectedIndexChanged" class="table table-striped table-bordered" SelectedRowStyle-Font-Bold="true" SelectedRowStyle-ForeColor="#026199" SelectedRowStyle-BackColor="#381cb" Width="100%" AllowPaging="true" PageSize="10" runat="server" PagerSettings-NextPageText=" Next " PagerSettings-PreviousPageText=" Previous " PagerSettings-Mode="NumericFirstLast" PagerSettings-FirstPageText=" First " PagerSettings-LastPageText=" Last " PagerSettings-Position="Bottom" PagerStyle-Font-Bold="true" PagerStyle-CssClass="pagination-grid">
                                    <Columns>
                                         <asp:CommandField ButtonType="Button" ShowSelectButton="True" ControlStyle-CssClass="btn btn-default"></asp:CommandField>
                                       <asp:BoundField DataField="CreatedDate" HeaderText="Date Created" />
                                       <asp:BoundField DataField="ReferenceId" HeaderText="Realex ID" />
                                       <asp:BoundField DataField="AvailableAmount" HeaderText="Refundable" />
                                       <asp:BoundField DataField="Amount" HeaderText="Total Amount" />                                       
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="col-sm-6"></div>

                        <div class="col-sm-6">
                            <div class="row margin-row">
                                <div class="col-sm-6">
                                    <asp:Button ID="BtnCreditCardRefund" OnClick="BtnCreditCardRefund_Click" class="btn btn-default pro-check" runat="server" Text="Refund" />
                                </div>
                                <div class="col-sm-6">
                                    <asp:Button ID="BtnCreditCardReset" OnClick="BtnCreditCardReset_Click" class="btn btn-default pro-check" runat="server" Text="Reset" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
