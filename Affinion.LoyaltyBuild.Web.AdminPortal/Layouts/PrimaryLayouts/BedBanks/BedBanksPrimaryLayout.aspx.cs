﻿using Affinion.LoyaltyBuild.Common.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.AdminPortal.Layouts.PrimaryLayouts.BedBanks
{
    public partial class BedBanksPrimaryLayout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Sitecore.Data.Fields.ImageField imgField = Sitecore.Context.Item.Fields["Logo Image"];
            string imgUrl = ((imgField.MediaItem) == null) ? string.Empty : Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField.MediaItem);
            imgLogo.Src = imgUrl;
            string link = SitecoreFieldsHelper.GetItemFieldValue(Sitecore.Context.Item, "Base Margin Search Link");
            searchLink.NavigateUrl = (string.IsNullOrEmpty(link)) ? string.Empty : link;
            link = SitecoreFieldsHelper.GetItemFieldValue(Sitecore.Context.Item, "Base Margin Edit Link");
            errorLink.NavigateUrl = (string.IsNullOrEmpty(link)) ? string.Empty : link;
            litTitle.Text = Sitecore.Context.Item.DisplayName;
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }

        protected void linkImage_ServerClick(object sender, EventArgs e)
        {
            Response.Redirect("sitecore/content/admin-portal/bed-bank-setup/marginmanagement/margin-management-search/");
        }

        protected void linkDashBoard_ServerClick(object sender, EventArgs e)
        {
            string link = SitecoreFieldsHelper.GetItemFieldValue(Sitecore.Context.Item, "Base Margin Search Link");
            if(!string.IsNullOrEmpty(link))
            Response.Redirect(link);
        }
    }
}