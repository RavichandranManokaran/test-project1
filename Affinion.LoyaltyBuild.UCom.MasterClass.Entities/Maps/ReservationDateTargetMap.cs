﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class ReservationDateTargetMap : SubclassMap<ReservationDateTarget>
    {
        // Methods
        public ReservationDateTargetMap()
        {
            base.Table("LB_ReservationDateTarget");
            base.KeyColumn("ReservationDateTargetId");
            base.Map(x => x.ReservationDateTargetId).ReadOnly();
            base.Map(x => x.ReservationFromDate).Not.Nullable();
            base.Map(x => x.ReservationToDate).Not.Nullable();
        }

    }

}
