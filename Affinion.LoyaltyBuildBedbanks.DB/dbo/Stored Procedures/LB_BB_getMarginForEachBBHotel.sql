﻿Create procedure LB_BB_getMarginForEachBBHotel(@providerID varchar(max), @arrivalDate date)
AS
BEGIN
DECLARE @ProviderList table(ProviderId nvarchar(max))
 INSERT INTO @ProviderList (ProviderId) select * from SplitString(@providerID,',')
--IF EXISTS (Select MarginID, MarginPercentage from LB_BB_MarginDetails where ProviderID IN (select * from @ProviderList) AND (@arrivalDate BETWEEN StartDate AND ENDDATE))
--BEGIN
Select md.MarginID, md.MarginPercentage, md.ProviderID from LB_BB_MarginDetails md 
inner join @providerList pl on md.ProviderID = pl.ProviderID
where (@arrivalDate BETWEEN md.StartDate AND md.EndDate) and md.IsActive=1
UNION
Select MarginID, MarginPercentage, MarginType from LB_BB_MarginDetails where MarginType='Default Margin'
--END
END