﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.Controls
{
	public partial class Order : System.Web.UI.UserControl
	{
		public UCommerce.EntitiesV2.PurchaseOrder PurchaseOrder { get; set; }
		public bool IsEditable { get; set; }
		protected void Page_Load(object sender, EventArgs e)
		{
			OrderLinesRepeater.DataSource = PurchaseOrder.OrderLines;
			OrderLinesRepeater.DataBind();
		}

		protected void UpdateCart_OnClick(object sender, EventArgs e)
		{
			
		}
	}
}