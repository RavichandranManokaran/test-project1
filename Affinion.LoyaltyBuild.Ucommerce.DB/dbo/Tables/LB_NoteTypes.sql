﻿CREATE TABLE [dbo].[LB_NoteTypes] (
    [NoteTypeId]  INT           IDENTITY (1, 1) NOT NULL,
    [Description] NVARCHAR (50) NULL,
    PRIMARY KEY CLUSTERED ([NoteTypeId] ASC)
);

