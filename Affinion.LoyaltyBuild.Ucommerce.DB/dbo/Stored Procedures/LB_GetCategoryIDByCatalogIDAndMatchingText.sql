﻿CREATE PROCEDURE [dbo].[LB_GetCategoryIDByCatalogIDAndMatchingText]
	@CatalogID INT,
	@MatchingText varchar(100) 
AS
BEGIN
DECLARE @CategoryId varchar(100)
	select @CategoryId = min(CategoryId) from uCommerce_Category where lower(Name) like '%' + lower(@MatchingText)+ '%' and ProductCatalogId = @CatalogID
	if (@CategoryId is null)
	begin
		select @CategoryId = min(CategoryId) from uCommerce_Category where ProductCatalogId = @CatalogID and lower(Name) in (select rtrim(ltrim(lower(item))) from dbo.SplitString(@MatchingText, ','))
	end	
	if (@CategoryId is null)
	begin
		select @CategoryId = min(CategoryId) from uCommerce_Category where ProductCatalogId = @CatalogID and lower(Name) in (select rtrim(ltrim(lower(item))) from dbo.SplitString(@MatchingText, ' '))
	end	
	select case when @categoryId is null then 0 else @categoryId end as 'CategoryId'
END

GO

