﻿CREATE TABLE [dbo].[uCommerce_Role] (
    [RoleId]                INT            IDENTITY (1, 1) NOT NULL,
    [Name]                  NVARCHAR (255) NULL,
    [ProductCatalogGroupId] INT            NULL,
    [ProductCatalogId]      INT            NULL,
    [CultureCode]           NVARCHAR (10)  NULL,
    [PriceGroupId]          INT            NULL,
    [RoleType]              INT            NOT NULL,
    [ParentRoleId]          INT            NULL,
    CONSTRAINT [uCommerce_PK_Role] PRIMARY KEY CLUSTERED ([RoleId] ASC),
    FOREIGN KEY ([ParentRoleId]) REFERENCES [dbo].[uCommerce_Role] ([RoleId]),
    FOREIGN KEY ([PriceGroupId]) REFERENCES [dbo].[uCommerce_PriceGroup] ([PriceGroupId]),
    FOREIGN KEY ([ProductCatalogGroupId]) REFERENCES [dbo].[uCommerce_ProductCatalogGroup] ([ProductCatalogGroupId]),
    FOREIGN KEY ([ProductCatalogId]) REFERENCES [dbo].[uCommerce_ProductCatalog] ([ProductCatalogId])
);

