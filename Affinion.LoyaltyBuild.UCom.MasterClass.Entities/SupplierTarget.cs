﻿using Affinion.LoyaltyBuild.UCom.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;
using UCommerce.Marketing;
using UCommerce.Marketing.Targets;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{

    public class SupplierTarget : Target, IOrderLineTarget, ITarget, IEntity, IOrderLineQualifier
    {
        public virtual bool IsSatisfiedBy(OrderLine orderLine)
        {
            OrderProperty searchIgnor = orderLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.IgnorCampaignSearchCriteria);
            if (searchIgnor != null)
                return true;

            if (this._containgSkus == null)
            {
                this._containgSkus = new HashSet<string>();
                List<string> category = SupplierInvite.All().Where(x => x.CampaignItem == CampaignItem.CampaignItemId && x.IsSubscribe).Select(x => x.Sku).ToList();// this.SupplierId.Split('|').Where(y => !string.IsNullOrEmpty(y)).ToList();

                List<string> productSku = (from x in CategoryProductRelation.All()
                                           where (category.Contains(x.Category.Guid.ToString()) && !x.Category.Deleted)
                                           select x.Product.Sku).ToList();
                foreach (string str in productSku)
                {
                    this._containgSkus.Add(str);
                }
            }
            return this._containgSkus.Contains(orderLine.Sku);
        }
        // Fields
        private HashSet<string> _containgSkus;
        private int? _hashCode;

        // Methods
        public new static IQueryable<SupplierTarget> All()
        {
            return GetRepo().Select();
        }

        public override void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<SupplierTarget, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            SupplierTarget objA = obj as SupplierTarget;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<SupplierTarget, bool>> expression)
        {
            return All().Any<SupplierTarget>(expression);
        }

        public static IList<SupplierTarget> Find(Expression<Func<SupplierTarget, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<SupplierTarget>();
        }

        public static SupplierTarget FirstOrDefault(Expression<Func<SupplierTarget, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<SupplierTarget>();
        }

        public new static SupplierTarget Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<SupplierTarget> GetRepo()
        {
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<SupplierTarget>>();
            
        }

        public override void Save()
        {
            GetRepo().Save(this);
        }

        public static SupplierTarget SingleOrDefault(Expression<Func<SupplierTarget, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties
        public virtual string SupplierId { get; set; }

        public virtual int SupplierTargetId { get; protected set; }

        public override int Id
        {
            get
            {
                return this.SupplierTargetId;
            }
        }

        public virtual string Name { get; set; }
    }



}

