﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.EntitiesV2;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using UCommerce.Presentation.Views.Marketing;
using UCommerce.Presentation.Web.Controls;
using UCommerce.Presentation.Web.Marketing;
using Affinion.LoyaltyBuild.UCom.Common.Constants;
using Affinion.LoyaltyBuild.UCom.MasterClass.Website.CustomSiteCoreDataType;
using Affinion.LoyaltyBuild.UCom.MasterClass.Website.Configuration;
using Affinion.LoyaltyBuild.Common.Instrumentation;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.ucommerce.Marketing.Targets
{
    public partial class HotelRatingUi : ViewEnabledControl<IEditCampaignItemView>, ITargetUi
    {
        List<string> datatypeList;
        /// <summary>
        /// page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (datatypeList == null)
            //{
            //    DataType hotelRating = DataType.All().Where(x => !x.Deleted && x.TypeName == DataTypeConstant.HotelRating).ToList().FirstOrDefault();
            //    if (hotelRating != null)
            //    {
            //        datatypeList = hotelRating.DataTypeEnums.ToList();
            //    }
            //    datatypeList.Insert(0, new DataTypeEnum() { Value = "" });
            //}
            datatypeList = SiteConfiguration.GetSiteCoreItem(CustomDataTypeConstant.HotelRanking);
            drpHotelRating.Items.Clear();
            if (datatypeList != null)
            {
                foreach (var item in datatypeList)
                {
                    drpHotelRating.Items.Add(new ListItem() { Text = item , Value = item });
                }
            }
            HotelRatingTarget hotelRatingData = this.DataSource as HotelRatingTarget;
            this.DeleteButton.OnClientClick = string.Format("return confirm('Delete');");
            if (datatypeList.Any(x => x == hotelRatingData.HotelRating))
            {
                drpHotelRating.SelectedValue = hotelRatingData.HotelRating;
            }
            this.ReadOnlyMessage.Text = this.TargetToString(hotelRatingData);
        }

        /// <summary>
        /// Save changes for hotel rating type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveButton_Click(object sender, ImageClickEventArgs e)
        {
            HotelRatingTarget hotelRatingData = this.DataSource as HotelRatingTarget;
            hotelRatingData.HotelRating = drpHotelRating.SelectedValue;
            hotelRatingData.Save();
            this.ReadOnlyMessage.Text = this.TargetToString(hotelRatingData);
            this.ToggleEditor();
        }

        /// <summary>
        ///  enable edit mode view in page
        /// </summary>
        private void ToggleEditor()
        {
            this.NonEditModePlaceHolder.Visible = !this.NonEditModePlaceHolder.Visible;
            this.EditButton.Visible = !this.EditButton.Visible;
            this.EditModePlaceHolder.Visible = !this.EditModePlaceHolder.Visible;
            this.SaveButton.Visible = !this.SaveButton.Visible;
        }

        /// <summary>
        /// Apply hotel rating type value to View
        /// </summary>
        /// <param name="HotelRatingTarget"></param>
        /// <returns></returns>
        private string TargetToString(HotelRatingTarget HotelRatingTarget)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendFormat("Hotel Rating ");
            stringBuilder.AppendFormat("{0} ", HotelRatingTarget.HotelRating);
            return stringBuilder.ToString();
        }

        /// <summary>
        /// Delete hotel rating type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteButton_Click(object sender, ImageClickEventArgs e)
        {
            HotelRatingTarget hotelRatingData = this.DataSource as HotelRatingTarget;
            if (hotelRatingData != null)
            {
                hotelRatingData.Delete();
            }
            this.Page.Response.Redirect(this.Page.Request.Url.ToString(), true);
        }

        /// <summary>
        /// Edit hotel rating type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditButton_Click(object sender, ImageClickEventArgs e)
        {
            this.ToggleEditor();
        }

        /// <summary>
        /// get set for apply
        /// </summary>
        public bool EnabledForApply
        {
            get;
            set;
        }

        /// <summary>
        /// get set for display
        /// </summary>
        public bool EnabledForDisplay
        {
            get;
            set;
        }

        /// <summary>
        /// Get set for datasource
        /// </summary>
        public object DataSource
        {
            get;
            set;

        }

        /// <summary>
        /// Create new hotel rating Type
        /// </summary>
        /// <returns></returns>
        public object New()
        {
            var HotelRatingTarget = new HotelRatingTarget();
            try
            {
                HotelRatingTarget.CampaignItem = View.CampaignItem;
                HotelRatingTarget.EnabledForApply = true;
                HotelRatingTarget.HotelRating = string.Empty;
                HotelRatingTarget.Save();
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.MasterClassWebsite, ex, this);
                throw;
            }
            this.DataSource = HotelRatingTarget;
            return HotelRatingTarget;
        }
    }
}