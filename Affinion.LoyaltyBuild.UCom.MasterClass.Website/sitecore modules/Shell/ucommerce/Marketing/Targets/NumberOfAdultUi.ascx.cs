﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.EntitiesV2;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using UCommerce.Presentation.Views.Marketing;
using UCommerce.Presentation.Web.Controls;
using UCommerce.Presentation.Web.Marketing;
using Affinion.LoyaltyBuild.Common.Instrumentation;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.ucommerce.Marketing.Targets
{
    public partial class NumberOfAdultUi : ViewEnabledControl<IEditCampaignItemView>, ITargetUi
    {
        //List<DataTypeEnum> datatypeList;

        protected void Page_Load(object sender, EventArgs e)
        {
            NumberOfAdultTarget dataSource = this.DataSource as NumberOfAdultTarget;
            this.DeleteButton.OnClientClick = string.Format("return confirm('Delete');");
            NumberOfAdult.Text = dataSource.NumberOfAdult.ToString();
            this.ReadOnlyMessage.Text = this.TargetToString(dataSource);
        }

        protected void SaveButton_Click(object sender, ImageClickEventArgs e)
        {
            NumberOfAdultTarget dataSource = this.DataSource as NumberOfAdultTarget;
            dataSource.NumberOfAdult = Convert.ToInt32(NumberOfAdult.Text);
            dataSource.Save();
            this.ReadOnlyMessage.Text = this.TargetToString(dataSource);
            this.ToggleEditor();
        }

        private void ToggleEditor()
        {
            this.NonEditModePlaceHolder.Visible = !this.NonEditModePlaceHolder.Visible;
            this.EditButton.Visible = !this.EditButton.Visible;
            this.EditModePlaceHolder.Visible = !this.EditModePlaceHolder.Visible;
            this.SaveButton.Visible = !this.SaveButton.Visible;
        }

        private string TargetToString(NumberOfAdultTarget NumberOfAdultTarget)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendFormat("Number Of Adults: ");
            stringBuilder.AppendFormat("{0} ", NumberOfAdultTarget.NumberOfAdult);
            return stringBuilder.ToString();
        }

        protected void DeleteButton_Click(object sender, ImageClickEventArgs e)
        {
            NumberOfAdultTarget dataSource = this.DataSource as NumberOfAdultTarget;
            if (dataSource != null)
            {
                dataSource.Delete();
            }
            this.Page.Response.Redirect(this.Page.Request.Url.ToString(), true);
        }

        protected void EditButton_Click(object sender, ImageClickEventArgs e)
        {
            this.ToggleEditor();
        }


        public bool EnabledForApply
        {
            get;
            set;
        }

        public bool EnabledForDisplay
        {
            get;
            set;
        }

        public object DataSource
        {
            get;
            set;

        }

        public object New()
        {
            var NumberOfAdultTarget = new NumberOfAdultTarget();
            try
            {
                NumberOfAdultTarget.CampaignItem = View.CampaignItem;
                NumberOfAdultTarget.EnabledForApply = true;
                NumberOfAdultTarget.NumberOfAdult = 0;
                NumberOfAdultTarget.Save();
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.MasterClassWebsite, ex, this);
                throw;
            }
            this.DataSource = NumberOfAdultTarget;
            return NumberOfAdultTarget;
        }
    }
}