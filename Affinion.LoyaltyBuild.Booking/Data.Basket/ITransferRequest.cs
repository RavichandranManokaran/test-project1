﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Api.Booking.Data.Basket
{
    /// <summary>
    /// Transfer booking request
    /// </summary>
    public interface ITransferRequest
    {
        /// <summary>
        /// OrderLine id
        /// </summary>
        int OrderLineid { get; set; }

        /// <summary>
        /// Note to Provider
        /// </summary>
        string NoteToProvider { get; set; }

        /// <summary>
        /// no of adults
        /// </summary>
        int NoOdAdult { get; set; }

        /// <summary>
        /// no of children
        /// </summary>
        int NoOfChildren { get; set; }

        /// <summary>
        /// Childern Age Info
        /// </summary>
        string ChildrenAgeInfo { get; set; }

        /// <summary>
        /// Basket id
        /// </summary>
        string BasketId { get; set; }

        ///<summary>
        /// Createdby userlogin
        /// </summary>
        string CreatedBy { get; set; }

        /// <summary>
        /// Set if this is a free transfer
        /// </summary>
        bool IsFreeTransfer { get; set; }

        /// <summary>
        /// Booking reference
        /// </summary>
        string BookingReferenceNumber { get; set; }
    }
}
