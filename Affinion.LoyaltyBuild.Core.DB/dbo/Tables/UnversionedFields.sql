﻿CREATE TABLE [dbo].[UnversionedFields] (
    [Id]        UNIQUEIDENTIFIER CONSTRAINT [DF_UnversionedFields_Id] DEFAULT (newid()) NOT NULL,
    [ItemId]    UNIQUEIDENTIFIER NOT NULL,
    [Language]  NVARCHAR (50)    NOT NULL,
    [FieldId]   UNIQUEIDENTIFIER NOT NULL,
    [Value]     NVARCHAR (MAX)   NOT NULL,
    [Created]   DATETIME         NOT NULL,
    [Updated]   DATETIME         NOT NULL,
    [DAC_Index] INT              NULL
);


GO
CREATE CLUSTERED INDEX [DAC_Clustered]
    ON [dbo].[UnversionedFields]([DAC_Index] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [ndxUnique]
    ON [dbo].[UnversionedFields]([ItemId] ASC, [Language] ASC, [FieldId] ASC);

