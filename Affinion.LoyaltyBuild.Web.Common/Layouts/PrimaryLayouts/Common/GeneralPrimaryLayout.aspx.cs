﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           GeneralPrimaryLayout.aspx.cs
/// Sub-system/Module:     Web.Common(VS project name)
/// Description:           Primary layout for general pages
/// </summary>
namespace Affinion.LoyaltyBuild.Web.Common.Layouts.PrimaryLayouts.Common
{
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Sitecore.Web.UI.WebControls;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;

    /// <summary>
    /// Primary layout for general pages
    /// </summary>
    public partial class GeneralPrimaryLayout : BasePrimaryLayout
    {
        /// <summary>
        /// Gets the main form
        /// </summary>
        public override HtmlForm MainForm
        {
            get
            {
                return this.mainform;
            }
        }

        /// <summary>
        /// Gets the grid block placeholder
        /// </summary>
        //public Placeholder GridBlockPlacHolder
        //{
        //    get
        //    {
        //        return this.gridBlock;
        //    }
        //}

        /// <summary>
        /// This function includes the code which needs to run on page load
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The parameter</param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}