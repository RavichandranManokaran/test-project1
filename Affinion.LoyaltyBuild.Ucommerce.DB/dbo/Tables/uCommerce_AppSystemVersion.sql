﻿CREATE TABLE [dbo].[uCommerce_AppSystemVersion] (
    [AppSystemVersionId] INT            IDENTITY (1, 1) NOT NULL,
    [SchemaVersion]      INT            NOT NULL,
    [MigrationName]      NVARCHAR (512) NULL
);

