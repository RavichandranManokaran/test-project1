﻿CREATE PROCEDURE [dbo].[LB_UpdateCommunicationHistory] 
	-- Add the parameters for the stored procedure here
	@orderlineId int,
	@CommunicationTypeId int, 
	@status bit 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE LB_CommunicationHistory SET [status]=@status WHERE OrderLineId=@orderlineId and CommunicationTypeId=@CommunicationTypeId
END

