﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           FilterByPackage.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Search (VS project name)
/// Description:           Filter by package
/// </summary>
namespace Affinion.LoyaltyBuild.Search.SupplierFilters
{
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Search.Data;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;

    public class FilterByPackage : ISupplierFilter
    {
        /// <summary>
        /// Returns true to include the supplier in search results
        /// </summary>
        /// <param name="filterData">The filter data</param>
        /// <returns>Returns true to include the supplier in search results</returns>
        public bool IncludeSupplier(SupplierFilterData filterData)
        {
            if (filterData == null)
            {
                return false;
            }

            if (string.IsNullOrEmpty(filterData.SearchTerm.OfferGroup) || filterData.SearchTerm.OfferGroup == "all")
            {
                return true;
            }

            bool campaignApplicable = false;
            Collection<CampaignOfferData> campaignOffers = filterData.ResultsItem.ApplicableCampaigns;
            if (campaignOffers != null && campaignOffers.Count > 0)
            {
                var filteredCampaignOffers = campaignOffers.Where(o => o.Enabled && o.StartDate <= filterData.SearchTerm.CheckinDate && o.EndDate >= filterData.SearchTerm.CheckoutDate);
                foreach (CampaignOfferData campaignOffer in filteredCampaignOffers)
                {
                    if (campaignOffer.Name.ToLowerInvariant().IndexOf(filterData.SearchTerm.OfferGroup.ToLowerInvariant()) >= 0)
                    {
                        if (campaignOffer.ArrivalDates.Count > 0)
                        {
                            foreach (CampaginItemDateRange dateRange in campaignOffer.ArrivalDates)
                            {
                                DateTime startDate = DateTimeHelper.ParseDate(dateRange.StartDate);
                                DateTime endDate = DateTimeHelper.ParseDate(dateRange.EndDate);
                                if (startDate <= filterData.SearchTerm.CheckinDate && endDate >= filterData.SearchTerm.CheckoutDate)
                                {
                                    campaignApplicable = true;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            campaignApplicable = true;
                            break;
                        }
                    }
                }
            }

            return campaignApplicable;
        }
    }
}
