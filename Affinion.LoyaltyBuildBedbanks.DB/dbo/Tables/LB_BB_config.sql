/****** Object:  Table [dbo].[LB_BB_config]    Script Date: 7/19/2016 7:58:18 PM ******/
CREATE TABLE [dbo].[LB_BB_config](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Param] [varchar](50) NOT NULL,
	[Value] [varchar](50) NULL,
 CONSTRAINT [PK__config] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

