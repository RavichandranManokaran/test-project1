
declare @definationid int = 0
select @definationid = DefinitionId from uCommerce_Definition
where Name = 'Default Campaign Item'
Go

declare @datatypeid int = 0
select @datatypeid = DataTypeId from uCommerce_DataType where DefinitionName= 'SiteCoreOfferType'
if(@datatypeid <> 0)
begin
insert into uCommerce_DefinitionField(DataTypeId,DefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,BuiltIn,DefaultValue,Guid)
values (@datatypeid , @definationid,'OfferType',1,0,1,0,0,0,0,'',NEWID())
end
Go