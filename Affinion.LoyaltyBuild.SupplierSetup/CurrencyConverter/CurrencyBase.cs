﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           CurrencyBase.cs
/// Sub-system/Module:     Suplier Setup(VS project name)
/// Description:           Currency base
/// </summary>
#region Using Directives
using System.Collections.Generic;
using System.Collections.ObjectModel;
#endregion

namespace Affinion.LoyaltyBuild.SupplierSetup.CurrencyConverter
{
    /// <summary>
    /// 
    /// </summary>
    public class CurrencyBase
    {
        private string Outcome { get; set; }
        private object Message { get; set; }
        private string Identity { get; set; }
        private double Delay { get; set; }
        public Collection<JsonCurrency> Currencylist { get; set; }
    }
}
