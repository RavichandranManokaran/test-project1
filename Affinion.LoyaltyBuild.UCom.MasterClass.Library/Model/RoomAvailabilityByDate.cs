﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    public class ChangeRoomAvailabilityByDate
    {
        public int DateCounter { get; set; }

        public int AvailableRoom { get; set; }

        public string CombineSKU { get; set; }

        public string ProductSku
        {
            get
            {
                if (!string.IsNullOrEmpty(CombineSKU) && CombineSKU.IndexOf('@') > -1)
                    return CombineSKU.Split('@')[0];
                return string.Empty;
            }
            set { }
        }

        public bool IsCloseOut { get; set; }

        public bool RecordUpdate { get; set; }

        public bool RemoveOffer { get; set; }

        public string PriceBand { get; set; }

        public decimal Price { get; set; }

        public int ProductType { get; set; }

        public decimal PackagePrice { get; set; }

        public decimal AddOnPrice { get; set; }

        public string VariantSku
        {
            get
            {
                if (!string.IsNullOrEmpty(CombineSKU) && CombineSKU.IndexOf('@') > -1)
                    return CombineSKU.Split('@')[1];
                return string.Empty;
            }
            set { }
        }
    }

    public class ChangeOfferRoomAvailabilityByDate
    {

        public int DateCounter { get; set; }

        public int AvailableRoom { get; set; }

        //public string ProductSku { get; set; }

        public int CampaignItemId { get; set; }

        public bool RecordUpdate { get; set; }

        public string VariantSku
        {
            get
            {
                if (!string.IsNullOrEmpty(CombineSKU) && CombineSKU.IndexOf('@') > -1)
                    return CombineSKU.Split('@')[1];
                return string.Empty;
            }
            set { }
        }
        public string CombineSKU { get; set; }

        public string ProductSku
        {
            get
            {
                if (!string.IsNullOrEmpty(CombineSKU) && CombineSKU.IndexOf('@') > -1)
                    return CombineSKU.Split('@')[0];
                return string.Empty;
            }
            set { }
        }
    }

    public class PriceBand
    {
        public string Name { get; set; }

        public decimal Price { get; set; }

        public string ItemId { get; set; }
    }

    public class RoomAvailabilityByDate
    {
        public RoomAvailabilityByDate()
        {
            this.ddPrice = new List<PriceBand>();
        }

        public string Id { get; set; }

        public DateTime AvialabilityDate { get; set; }

        public int BlockedRoom { get; set; }

        public int AvailabelRoom { get; set; }

        public int OfferBlockedRoom { get; set; }

        public int OfferAvailabelRoom { get; set; }

        public string ProductSku { get; set; }

        public bool IsCloseOut { get; set; }

        public bool HasOffer { get; set; }

        public bool IsNewRecord { get; set; }

        public bool IsNewOfferRecord { get; set; }

        public bool IsNonEditable { get; set; }

        public int DateCounter { get; set; }

        public int OfferId { get; set; }

        public decimal Price { get; set; }

        public string PriceBand { get; set; }

        public decimal PackagePrice { get; set; }

        //public decimal AddonPrice { get; set; }

        public int flag { get; set; }

        public List<PriceBand> ddPrice { get; set; }

        public int TotalUnSoldRoom
        {
            get
            {
                return (this.AvailabelRoom - this.BlockedRoom);
            }
        }
    }


}
