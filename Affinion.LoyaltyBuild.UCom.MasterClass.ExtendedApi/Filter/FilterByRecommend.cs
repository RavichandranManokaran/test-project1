﻿using Affinion.LoyaltyBuild.UCom.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.Search.Facets;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi.Filter
{
    public class FilterByRecommend : IHotelFilter
    {
        public bool IncludeHotel(UCommerce.Documents.Product product, SearchCriteria criteria)
        {
            if (string.IsNullOrEmpty(criteria.LocalRanking))
            {
                return true;
            }
            if (criteria.LocalRanking == "all")
            {
                return true;
            }
            if (!string.IsNullOrEmpty(criteria.LocalRanking))
            {
                KeyValuePair<string, Dictionary<string, object>> experienceProperty = product.Properties.FirstOrDefault(x => x.Value.Any(y => y.Key == ProductDefinationConstant.Recommended));
                if (experienceProperty.Value != null)
                {
                    foreach (var item in criteria.LocalRanking.Split(','))
                    {
                        bool found = experienceProperty.Value.Any(x => x.Value.ToString().Contains(item));
                        if (found)
                        {
                            return true;
                        }
                    }

                }
            }
            return false;
        }


        public UCommerce.Search.Facets.Facet FilterRecordFacet(SearchCriteria criteria)
        {
            if (criteria.LocalRanking != "all" && !string.IsNullOrEmpty(criteria.LocalRanking))
            {
                var facet = new Facet();
                facet.FacetValues = new List<FacetValue>();
                facet.Name = ProductDefinationConstant.Recommended; // keeping as star ranking until local ranking adds to hotel definition

                //In this example selected values are seperated by a '|' in the querystring value.
                foreach (var value in criteria.LocalRanking.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    facet.FacetValues.Add(new FacetValue() { Value = value });
                }
                return facet;
            }
            return null;
        }
    }
}
