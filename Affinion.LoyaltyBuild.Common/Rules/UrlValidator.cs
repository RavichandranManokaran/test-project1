﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
///Source File:           UrlValidator.cs
///Sub-system/Module:     Affinion.LoyaltyBuild.Common.Rules
///Description:           Class for Field  Validator rules
///</summary>
#region Using Directives
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Sitecore.Data.Items;
using Sitecore.Data.Validators;
using System;
using System.Runtime.Serialization;
using System.Text.RegularExpressions; 
#endregion

namespace Affinion.LoyaltyBuild.Common.Rules
{
    /// <summary>
    /// Url validation class
    /// </summary>
    [Serializable]
    public class UrlValidator : StandardValidator
    {
        #region constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public UrlValidator()
        {

        }

        public UrlValidator(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {

        }
        #endregion

        /// <summary>
        /// ValidatorResult for given field
        /// </summary>
        /// <returns></returns>
        protected override ValidatorResult Evaluate()
        {
            try
            {
                Item myItem = base.GetItem();

                //Get the Parameter value
                string urlString = base.GetControlValidationValue();

                if (urlString.Length != 0)
                {
                    if (!IsUrlValid(urlString))
                    {
                        Text = String.Format("Please enter a valid " + myItem.Fields[this.FieldID].DisplayName);
                        return ValidatorResult.FatalError;
                    } 
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, this);
                throw;
            }

            return ValidatorResult.Valid;
        }

        /// <summary>
        /// ValidatorResult of a field
        /// </summary>
        /// <returns></returns>
        protected override ValidatorResult GetMaxValidatorResult()
        {
            return GetFailedResult(ValidatorResult.FatalError);
        }

        /// <summary>
        /// overide the name
        /// </summary>
        public override string Name
        {
            get { return "URL format mismatch"; }
        }

        /// <summary>
        /// Validate the given url string against the pattern match
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private bool IsUrlValid(string url)
        {
            string pattern = @"^(http|https|ftp|)\://|[a-zA-Z0-9\-\.]+\.[a-zA-Z](:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*[^\.\,\)\(\s]$";
            Regex reg = new Regex(pattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);
            return reg.IsMatch(url);
        }
    }
    
}
