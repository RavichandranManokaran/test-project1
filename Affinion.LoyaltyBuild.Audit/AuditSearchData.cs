﻿namespace Affinion.LoyaltyBuild.Audit
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Store parameterss related to Audit record retrieval
    /// </summary>
    public class AuditSearchData
    {
        /// <summary>
        /// Gets or sets the record ID. Never set the ID manually.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the Audit name
        /// For each audit type, relevent name should be defined in enum
        /// </summary>
        public AuditNames AuditName { get; set; }

        /// <summary>
        /// Gets or sets the Operation
        /// </summary>
        public AuditOperations Operation { get; set; }

        /// <summary>
        /// Gets or sets the ObjectType
        /// </summary>
        public AuditObjectType ObjectType { get; set; }

        /// <summary>
        /// Gets or sets the ObjectId
        /// </summary>
        public string ObjectId { get; set; }

        /// <summary>
        /// Gets or sets the Section
        /// </summary>
        public AuditSection Section { get; set; }

        /// <summary>
        /// Gets or sets the TextValue. this will search on oldvalue, newvalue, additionaldata and remarks fields
        /// </summary>
        public string TextValue { get; set; }

        /// <summary>
        /// Gets or sets the Username
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the Time
        /// </summary>
        public DateTime StartDateTime { get; set; }

        /// <summary>
        /// Gets or sets the end Time
        /// </summary>
        public DateTime EndDateTime { get; set; }

        /// <summary>
        /// Constructs the object
        /// </summary>
        public AuditSearchData()
        {
            // SQL datetime starts from 1753 and min value should be greater than that
            this.StartDateTime = new DateTime(1970, 1, 1);
            this.EndDateTime = DateTime.MaxValue;

            this.AuditName = AuditNames.Undefined;
            this.Operation = AuditOperations.Undefined;
            this.ObjectType = AuditObjectType.Undefined;
            this.Section = AuditSection.Undefined;
        }
    }
}
