﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/
namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.CategoryImport
{

    /// <summary>
    /// Interface to define the ISiteCoreHotel related actions
    /// </summary>
    public interface ISiteCoreHotel
    {

        int CreateCataLog(string name);

        /// <summary>
        /// SaveSiteCoreHotel 
        /// </summary>
        /// <param name="hotelDetail">hotelDetail</param>
        /// <returns>save hotel details</returns>
        LBProductInfo SaveSiteCoreHotel(LBHotelDetail hotelDetail);

        /// <summary>
        /// SaveSiteCountry 
        /// </summary>
        /// <param name="countryDetail">countryDetail</param>
        /// <returns>save country id</returns>
        int SaveSiteCountry(CountryDetail countryDetail);

        /// <summary>
        /// GetHotelDetailByGUID 
        /// </summary>
        /// <param name="categoryId">categoryId</param>
        /// <returns>gets the hotel information by guid</returns>
        LBHotelDetail GetHotelDetailByGUID(string categoryId);

        /// <summary>
        /// GetHotelDetailByGUID 
        /// </summary>
        /// <param name="sitecoreItemId">sitecoreItemId</param>
        /// <returns>gets the hotel information by guid</returns>
        bool DeleteCategory(string sitecoreItemId);

        /// <summary>
        /// UpdateSupplierClientDetail 
        /// </summary>
        /// <param name="supplier">supplier</param>
        /// <returns>update the supllier and client details</returns>
        bool UpdateSupplierClientDetail(LBAssignSupplier supplier);

        LBProductInfo SaveProducVariant(LBProductDetail detail);
    }
}
