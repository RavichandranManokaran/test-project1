﻿using Affinion.LoyaltyBuild.AdminPortal.DataExtraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.AdminPortal.sitecore_modules.Shell.Affinion.SupplierInformationUdate
{
    public partial class SupplierInformationAcceptance1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string ResultErrorMessage = string.Empty;
            if (!User.Identity.IsAuthenticated)
            {
                Response.Redirect("/sitecore/login?returnUrl=" + HttpUtility.UrlEncode(this.Request.Url.PathAndQuery));
            }

            bool sucess = true;
            var itemUrls = ItemUrls(RemoveIntegersInItemIds(this.Request.Url.ToString()));

            string ParentContextId = string.Empty;

            foreach (var item in itemUrls)
            {
                //Loop starts
                SupplierInformationAcceptanceHttpSitecoreDataProvider dataProvider = new SupplierInformationAcceptanceHttpSitecoreDataProvider();
                dataProvider.SetData(new System.Uri(item));
                SitecoreExternalDataExtractionResult result = SitecoreExternalDataExtraction.SaveToDraftState(dataProvider);

                if (sucess)      //check values not overwritten
                    sucess = result.ErrorType != SitecoreExternalDataExtractionErrorType.Failed;

                if (sucess)
                {
                    if (string.IsNullOrEmpty(ParentContextId))
                        ParentContextId = result.ContextItem.ID.ToString(); //ParentContextId
                }
                else
                {
                    ResultErrorMessage = result.Exception.Message;
                    break;
                }
                //Loop ends
            }
            if (sucess)
            {
                this.Response.Redirect(string.Format("http://{0}:{1}/sitecore/shell/Applications/Content%20Editor?id={2}&fo={2}&sc_bw=1&sc_lang=en", this.Request.Url.Host, this.Request.Url.Port, ParentContextId));
            }
            else
            {
                this.ErrorMessage.InnerText = ResultErrorMessage;
            }
        }

        private List<string> ItemUrls(string urls)
        {
            List<string> ResultUrls = new List<string>();
            string[] stringSeparators = new string[] { "itemId" };

            string[] differentItems = urls.Split(stringSeparators, StringSplitOptions.None);
            for (int i = 1; i < differentItems.Length; i++)
            {
                ResultUrls.Add(differentItems[0] + "itemId" + differentItems[i]);
            }
            return ResultUrls;
        }

        private string RemoveIntegersInItemIds(string url)
        {
            return url.Replace("itemId0", "itemId")
                .Replace("itemId1", "itemId")
                .Replace("itemId2", "itemId")
                .Replace("itemId4", "itemId")
                .Replace("itemId5", "itemId")
                .Replace("itemId6", "itemId")
                .Replace("itemId7", "itemId")
                .Replace("itemId8", "itemId");

        }
    }
}