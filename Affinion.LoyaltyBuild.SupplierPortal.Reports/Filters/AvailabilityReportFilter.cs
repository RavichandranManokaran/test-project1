﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.SupplierPortal.Reports.Filters
{
    /// <summary>
    /// Contains all the filters of Availability report
    /// </summary>
    public class AvailabilityReportFilter
    {
        /// <summary>
        /// From date
        /// </summary>
        public DateTime? DateFrom { get; set; }
        /// <summary>
        /// To date
        /// </summary>
        public DateTime? DateTo { get; set; }
        /// <summary>
        /// Provider id
        /// </summary>
        public int? ProviderId { get; set; }

    }
}
