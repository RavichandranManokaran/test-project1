﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           PostBreakEmailScheduler.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal
/// Description:           Handling Post Break Email scheduler
/// </summary>

using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Data;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Affinion.Loyaltybuild.BusinessLogic.Helper;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.EmailScheduler
{
    public class PostBreakEmailScheduler
    {
        #region Class variables
        public Item PostBreakEmailItem { get; set; }
        #endregion

        /// <summary>
        /// Run the Email scheduler
        /// </summary>
        public void Run()
        {
            try
            {

                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("POSTBREAK: Service Start"));
                Sitecore.Context.Job.Status.LogInfo("Scheduler started");

                Database currentDB = Sitecore.Configuration.Factory.GetDatabase("web");
                
                List<Item> clients = currentDB.GetItem("/sitecore/content/client-portal").Children.Where(i => i.TemplateName == "ClientPortalRoot").ToList<Item>();
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("GetItem From Web: Service Start"));
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("POSTBREAK: GetItem From Web"));
                foreach(Item c in clients)
                {
                    string clientItem=c.Name;
                    string postBreakEmail = string.Concat("/sitecore/content/client-portal/",clientItem,"/global/_supporting-content/email");
                    PostBreakEmailItem = currentDB.GetItem(postBreakEmail).Children.FirstOrDefault(i => i.TemplateName == "PostBreakEmail");
                    Item client = SitecoreFieldsHelper.GetLookupFieldTargetItem(c, Constants.ClientDetailsFieldName);
                    string clientId = client.ID.Guid.ToString();
       
                string noOfDays = SitecoreFieldsHelper.GetValue(PostBreakEmailItem, "NumberOfDaysAfter");

                DataSet ds = EmailHelper.GetPostBreakEmailDetails(clientId);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        string receiver = (!string.IsNullOrWhiteSpace(row["EmailAddress"].ToString())) ? row["EmailAddress"].ToString() : string.Empty;

                        string receiverName = (!string.IsNullOrWhiteSpace(row["FirstName"].ToString())) ? row["FirstName"].ToString() : string.Empty;

                        string hotelName = (!string.IsNullOrWhiteSpace(row["Name"].ToString())) ? row["Name"].ToString() : string.Empty;

                        if (string.IsNullOrEmpty(receiver))
                            continue;

                        DateTime checkout;
                        DateTime.TryParse(row["CheckOutDate"].ToString(), out checkout);
                        Double nodays;
                        Double.TryParse(noOfDays, out nodays);
                        if ((DateTime.Now - checkout).Days == nodays)
                        {
                            mailGenarate(receiver, receiverName, hotelName);
                        }
                    }
                }
                }
                Sitecore.Context.Job.Status.LogInfo("Scheduler finished");
            }
            catch (Exception exception)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("POSTBREAK: Exception-{0}", exception.ToString()));
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
            }

        }

        /// <summary>
        /// Generate Post Break Email 
        /// </summary>
        private void mailGenarate(string receiverId, string receiverName, string provider)
        {
            try
            {
                Affinion.LoyaltyBuild.Communications.IEmailService emailService = new Affinion.LoyaltyBuild.Communications.Services.EmailService();

                string senderID = !string.IsNullOrWhiteSpace(SitecoreFieldsHelper.GetValue(PostBreakEmailItem, "senderEmail")) ? SitecoreFieldsHelper.GetValue(PostBreakEmailItem, "senderEmail") : string.Empty;

                if (string.IsNullOrEmpty(senderID))
                    return;

                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("POSTBREAK: senderId-{0}", senderID));

                //DataSet ds = EmailHelper.GetPostBreakEmailDetails();
                //if (ds != null && ds.Tables.Count > 0)
                //{
                //foreach (DataRow row in ds.Tables[0].Rows)
                //{
                //  string receiver = row["EmailAddress"].ToString();

                //  if (string.IsNullOrEmpty(receiver))
                //      continue;

                string mailSubject = !string.IsNullOrWhiteSpace(SitecoreFieldsHelper.GetValue(PostBreakEmailItem, "Subject")) ? SitecoreFieldsHelper.GetValue(PostBreakEmailItem, "Subject") : string.Empty;

                string msgBodyTemplate = !string.IsNullOrWhiteSpace(SitecoreFieldsHelper.GetValue(PostBreakEmailItem, "EmailBody")) ? SitecoreFieldsHelper.GetValue(PostBreakEmailItem, "EmailBody") : string.Empty;
                string mailBody = ReplaceMailBodyText(msgBodyTemplate, receiverName, provider);
                emailService.Send(senderID, receiverId, mailSubject, mailBody, true);
                //}              
                //}
            }
            catch (Exception exception)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("POSTBREAK: Exception-{0}", exception.ToString()));
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
            } 
        }

        /// <summary>
        /// Mail Body Part
        /// </summary>
        /// <param name="bodyText"></param>
        /// <returns></returns>
        public string ReplaceMailBodyText(string bodyText, string guestName , string providerName)
        {
            bodyText = bodyText.Replace("{Guest Name}", guestName);
            bodyText = bodyText.Replace("{Provider Name}", providerName);
            return bodyText;
        }
    }
}