﻿CREATE TABLE [dbo].[SharedFields] (
    [Id]        UNIQUEIDENTIFIER CONSTRAINT [DF_SharedFields_Id] DEFAULT (newid()) NOT NULL,
    [ItemId]    UNIQUEIDENTIFIER NOT NULL,
    [FieldId]   UNIQUEIDENTIFIER NOT NULL,
    [Value]     NVARCHAR (MAX)   NOT NULL,
    [Created]   DATETIME         NOT NULL,
    [Updated]   DATETIME         NOT NULL,
    [DAC_Index] INT              NULL
);


GO
CREATE CLUSTERED INDEX [DAC_Clustered]
    ON [dbo].[SharedFields]([DAC_Index] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [ndxUnique]
    ON [dbo].[SharedFields]([ItemId] ASC, [FieldId] ASC);

