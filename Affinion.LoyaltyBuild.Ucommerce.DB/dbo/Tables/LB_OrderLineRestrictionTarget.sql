﻿CREATE TABLE [dbo].[LB_OrderLineRestrictionTarget](
	[OrderLineRestrictionTargetId] [int] NOT NULL,
	[NoOfOrderLine] [varchar](250) NOT NULL,
 CONSTRAINT [PK_uCommerce_OrderLineRestrictionTarget] PRIMARY KEY CLUSTERED 
(
	[OrderLineRestrictionTargetId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


ALTER TABLE [dbo].[LB_OrderLineRestrictionTarget]  WITH CHECK ADD  CONSTRAINT [FK_uCommerce_OrderLineRestrictionTarget_uCommerce_Target] FOREIGN KEY([OrderLineRestrictionTargetId])
REFERENCES [dbo].[uCommerce_Target] ([TargetId])
GO

ALTER TABLE [dbo].[LB_OrderLineRestrictionTarget] CHECK CONSTRAINT [FK_uCommerce_OrderLineRestrictionTarget_uCommerce_Target]
GO


