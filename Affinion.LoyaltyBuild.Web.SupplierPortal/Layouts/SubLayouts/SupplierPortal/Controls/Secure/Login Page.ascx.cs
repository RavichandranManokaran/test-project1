﻿using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
using Affinion.LoyaltyBuild.SupplierPortal.DataAccess;
using Affinion.LoyaltyBuild.SupplierPortal.DataAccess.Service;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;
using Sitecore.Data.Items;
using Sitecore.Links;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.Security;
using Affinion.LoyaltyBuild.Communications;
using Affinion.LoyaltyBuild.Communications.Services;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts
{
    /// <summary>
    /// Login Page
    /// </summary>
    public partial class Login_Page : BaseSublayout
    {

        #region private variables


        private Dictionary<string, string> _sitecoreMessages;

        #endregion


        #region protected variables

        protected String SCChangePasswordPageUrl { get; set; }
        protected String SCPasswordGuideUrl { get; set; }
        protected String SCContactUsUrl { get; set; }
        protected String SCContactUsMessage { get; set; }
        protected String SCContactUsHeader { get; set; }
        protected readonly IEmailService emailService;
        protected readonly ILoginService loginSvc;
        protected String DomainUser { get; set; }

        #endregion


        /// <summary>
        /// Login Page Constructor
        /// </summary>
        public Login_Page()
        {
            this.emailService = new EmailService();
            this.loginSvc = new LoginService();
            this._sitecoreMessages = new Dictionary<string, string>();
        }

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Load(object sender, EventArgs e)
        {
            SetVisibilityToDiv(true);
            //bind sitecore text
            BindSitecoreTexts();


        }

        /// <summary>
        /// LoginSubmit Click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LoginSubmit_Click(object sender, EventArgs e)
        {

            var userName = txtUsername.Text.Trim();
            string passWord = Password1.Text.Trim();
            if (!String.IsNullOrEmpty(userName) && !String.IsNullOrEmpty(passWord))
            {
                try
                {
                    var domain = Sitecore.Security.Domains.Domain.GetDomain(Constants.suppliers);
                    if (domain != null)
                    {
                        
                        
                        DomainUser = domain.Name + @"\" + userName;
                        if (Sitecore.Security.Authentication.AuthenticationManager.Login(DomainUser, passWord, false))
                        {
                            //check expired
                            if (HasPasswordExpired(DomainUser))
                            {
                                lblMessage.Text = "Password has been expired";
                                resetPassword.Visible = true;

                            }
                            else
                            {

                                Response.Redirect(Constants.redirectWelcomePage);
                            }
                        }
                        else if (Membership.GetUser(DomainUser) != null && Membership.GetUser(DomainUser).IsLockedOut)
                        {
                            //user locked message
                            lblMessage.Text = GetMessage(Constants.UserLocked);
                        }
                        else
                        {
                            lblMessage.Text = GetMessage(Constants.AuthenticationFailed);
                        }

                    }
                    else
                    {
                        throw new System.Security.Authentication.AuthenticationException(GetMessage(Constants.InvalidCredentials));
                    }
                }
                catch (Exception ex)
                {
                    Diagnostics.WriteException(DiagnosticsCategory.SupplierPortal, ex, this);
                    throw;

                }
            }



        }


        /// <summary>
        /// Forgot User name click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ForgotUsername_Click(object sender, EventArgs e)
        {
            ForgotUserNameSection.Visible = true;
            SetVisibilityToDiv(false);
        }

        /// <summary>
        /// Forgot UserName submit click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ForgotUserNameSubmit_Click(object sender, EventArgs e)
        {
            SetVisibilityToDiv(false);
            try
            {
                //emailService = new Affinion.LoyaltyBuild.Communications.Services.EmailService();
                if (string.IsNullOrEmpty(emailBox.Text))
                {
                    EmailLabel.Text = GetMessage(Constants.EmailShouldNotEmpty);
                }
                else
                {
                    var username = System.Web.Security.Membership.FindUsersByEmail(emailBox.Text);
                    if (username != null)
                    {
                        if (username.Count > 1)
                        {
                            forgotUserNameLableMsg.Text = GetMessage(Constants.MultipleUserMessage);

                        }
                        else if (username.Count == 1)
                        {
                            IEnumerator enumerator = username.GetEnumerator();

                            while (enumerator.MoveNext())
                            {
                                object item = enumerator.Current;

                                if (!String.IsNullOrWhiteSpace(GetMessage(Constants.SenderID)) && emailBox.Text != null)
                                {
                                    emailService.Send(GetMessage(Constants.SenderID), emailBox.Text, GetMessage(Constants.ForgotUserNameSubject), string.Format(GetMessage(Constants.ForgotUserNameMailBody), item.ToString()), true);
                                    forgotUserNameLableMsg.Text = Constants.msgUserNameDetails;
                                }
                            }
                        }
                        else
                        {
                            forgotUserNameLableMsg.Text = GetMessage(Constants.EmailIdNotFound);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                forgotUserNameLableMsg.Text = GetMessage(Constants.SomethingWentWrong);
                Diagnostics.WriteException(DiagnosticsCategory.SupplierPortal, ex, this);
            }




        }

        /// <summary>
        /// Forgot or change password click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Forgot_ChangePassword_Click(object sender, EventArgs e)
        {
            ForgotPasswordSection.Visible = true;
            SetVisibilityToDiv(false);
        }

        /// <summary>
        /// Fotgot password submit click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void FotgotPasswordSubmit_Click(object sender, System.EventArgs e)
        {
            SetVisibilityToDiv(false);
            var domain = Sitecore.Security.Domains.Domain.GetDomain(Constants.suppliers);

            string domainUser = domain.Name + @"\" + usernameText.Text;
            try
            {
                if (Sitecore.Security.Accounts.User.Exists(domainUser))
                {
                    Sitecore.Security.Accounts.User user = Sitecore.Security.Accounts.User.FromName(domainUser, true);
                    Sitecore.Security.UserProfile profile = user.Profile;
                    MembershipUser user1 = Membership.GetUser(domainUser);
                    if (user1.IsLockedOut)
                    {
                        user1.UnlockUser();
                    }
                    string resetPassword = user1.ResetPassword();
                    var r = new Random();

                    string UniqueNumber = loginSvc.GenerateRandomNumbers(r);

                    loginSvc.InsertChangePasswordHistory(UniqueNumber, domainUser);
                    string userEmail = profile.Email;
                    if (!String.IsNullOrWhiteSpace(GetMessage(Constants.SenderID)) && userEmail != null)
                    {
                        string nav = string.Empty;

                        nav = Sitecore.Context.Site.HostName + SCChangePasswordPageUrl + "?id=" + UniqueNumber;
                        emailService.Send(GetMessage(Constants.SenderID), userEmail, GetMessage(Constants.ChangePaswordSubject), string.Format(GetMessage(Constants.ChangePaswordMailBody), resetPassword, nav), true);
                        changePwdLabel.Text = GetMessage(Constants.ReceiveAnEmailShortly);
                    }
                    else
                    {
                        changePwdLabel.Text = GetMessage(Constants.EmailDoesntExist);

                    }
                }
                else
                {
                    changePwdLabel.Text = domainUser + GetMessage(Constants.DoesNotExist);
                }
            }
            catch (Exception ex)
            {
                changePwdLabel.Text = GetMessage(Constants.SomethingWentWrong);
                Diagnostics.WriteException(DiagnosticsCategory.Security, ex, this);
            }
        }

        /// <summary>
        ///Back button click event 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnBack_Click(object sender, System.EventArgs e)
        {
            string prevPage = Request.UrlReferrer.ToString();

            Response.Redirect(prevPage);
        }


        #region Common Methods

        /// <summary>
        /// to bind sitecore fields using rendering parameters and current page item
        /// </summary>
        private void BindSitecoreTexts()
        {
            Item currentPageItem = Sitecore.Context.Item;
            _sitecoreMessages = new Dictionary<string, string>();
            if (currentPageItem != null)
            {
                SCPasswordGuideUrl = FetchPdfHelper.GetFileUrl(currentPageItem, Constants.PasswordGuideFieldName);
                SCContactUsUrl = FetchPdfHelper.GetFileUrl(currentPageItem, Constants.ContactUS);
                SCContactUsMessage = SitecoreFieldsHelper.GetValue(currentPageItem, Constants.ContactUsMessage);
                SCContactUsHeader = SitecoreFieldsHelper.GetValue(currentPageItem, Constants.ContactUsTitle);
                Sitecore.Data.Fields.LinkField lnkFld = currentPageItem.Fields[Constants.ChangePasswordUrlField];
                SCChangePasswordPageUrl = lnkFld != null ? LinkManager.GetItemUrl(lnkFld.TargetItem) : string.Empty;

                SCChangePasswordPageUrl = lnkFld != null ? LinkManager.GetItemUrl(lnkFld.TargetItem) : string.Empty;
                _sitecoreMessages.Add(Constants.SenderID, SitecoreFieldsHelper.GetValue(currentPageItem, Constants.SenderID));
                _sitecoreMessages.Add(Constants.ForgotUserNameSubject, SitecoreFieldsHelper.GetValue(currentPageItem, Constants.ForgotUserNameSubject));
                _sitecoreMessages.Add(Constants.ChangePaswordSubject, SitecoreFieldsHelper.GetValue(currentPageItem, Constants.ChangePaswordSubject));
                _sitecoreMessages.Add(Constants.ForgotUserNameMailBody, SitecoreFieldsHelper.GetValue(currentPageItem, Constants.ForgotUserNameMailBody));
                _sitecoreMessages.Add(Constants.ChangePaswordMailBody, SitecoreFieldsHelper.GetValue(currentPageItem, Constants.ChangePaswordMailBody));
                _sitecoreMessages.Add(Constants.ReceiveAnEmailShortly, SitecoreFieldsHelper.GetValue(currentPageItem, Constants.ReceiveAnEmailShortly));
                _sitecoreMessages.Add(Constants.EmailDoesntExist, SitecoreFieldsHelper.GetValue(currentPageItem, Constants.EmailDoesntExist));
                _sitecoreMessages.Add(Constants.MultipleUserMessage, SitecoreFieldsHelper.GetValue(currentPageItem, Constants.MultipleUserMessage));

            }

            Item conditionalMessageItem = this.GetItemFromRenderingParameterPath("Conditional Messages Link");
            if (conditionalMessageItem != null)
            {
                _sitecoreMessages.Add(Constants.AuthenticationFailed, SitecoreFieldsHelper.GetValue(conditionalMessageItem, Constants.AuthenticationFailed));
                _sitecoreMessages.Add(Constants.InvalidCredentials, SitecoreFieldsHelper.GetValue(conditionalMessageItem, Constants.InvalidCredentials));
                _sitecoreMessages.Add(Constants.EmailShouldNotEmpty, SitecoreFieldsHelper.GetValue(conditionalMessageItem, Constants.EmailShouldNotEmpty));
                _sitecoreMessages.Add(Constants.DoesNotExist, SitecoreFieldsHelper.GetValue(conditionalMessageItem, Constants.DoesNotExist));
                _sitecoreMessages.Add(Constants.EmailIdNotFound, SitecoreFieldsHelper.GetValue(conditionalMessageItem, Constants.EmailIdNotFound));
                _sitecoreMessages.Add(Constants.UserInactive, SitecoreFieldsHelper.GetValue(conditionalMessageItem, Constants.UserInactive));
                _sitecoreMessages.Add(Constants.UserNotFound, SitecoreFieldsHelper.GetValue(conditionalMessageItem, Constants.UserNotFound));
                _sitecoreMessages.Add(Constants.UserNamePasswordIncorrectMsg, SitecoreFieldsHelper.GetValue(conditionalMessageItem, Constants.UserNamePasswordIncorrectMsg));
                _sitecoreMessages.Add(Constants.UserLocked, SitecoreFieldsHelper.GetValue(conditionalMessageItem, Constants.UserLocked));
                _sitecoreMessages.Add(Constants.SomethingWentWrong, SitecoreFieldsHelper.GetValue(conditionalMessageItem, Constants.SomethingWentWrong));

            }
        }

        /// <summary>
        /// Check wheather password expired for current MembershipUser.
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        private bool HasPasswordExpired(string username)
        {
            var currentUser = Membership.GetUser(username);
            return currentUser.LastPasswordChangedDate.ToLocalTime().Add(new TimeSpan(90, 0, 0, 0)) <= DateTime.Now;
        }

        /// <summary>
        /// Set Visibility to div
        /// </summary>
        /// <param name="isVisble"></param>
        private void SetVisibilityToDiv(bool isVisble)
        {
            LoginSection.Visible = isVisble;
            emostitle.Visible = isVisble;
            help.Visible = isVisble;

        }

        /// <summary>
        /// Get Message
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private string GetMessage(string key)
        {
            if (_sitecoreMessages.ContainsKey(key))
                return _sitecoreMessages[key];

            return string.Empty;
        }

        #endregion

    }
}