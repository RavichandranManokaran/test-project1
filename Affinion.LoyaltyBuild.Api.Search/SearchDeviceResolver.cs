﻿using Sitecore;
using Sitecore.Data.Items;
using Sitecore.Pipelines.HttpRequest;
using System.Web;

namespace Affinion.LoyaltyBuild.Api.Search
{
    /// <summary>
    /// resolver class to set the device based on ajax request for search
    /// </summary>
    public class SearchDeviceResolver : HttpRequestProcessor
    {
        /// <summary>
        /// override process method
        /// </summary>
        /// <param name="args"></param>
        public override void Process(HttpRequestArgs args)
        {
            HttpContext currentHttpContext = HttpContext.Current;

            if (currentHttpContext == null || Context.Database == null)
                return;
            
            //if it is a ajax request set the device as Ajax
            if(IsSearchAjaxRequest())
            {
                DeviceItem device = Context.Database.Resources.Devices["Ajax"];
                Context.Device = device;
            }
        }
        
        /// <summary>
        /// is Ajax request
        /// </summary>
        /// <returns></returns>
        public bool IsSearchAjaxRequest()
        {
            var request = HttpContext.Current.Request;

            return ((request.Headers != null) && (request.Headers["x-search-type"] == "ajax"));
        }
    }
}
