

/****** Object:  StoredProcedure [dbo].[LB_BB_insertProviderDetails]    Script Date: 7/19/2016 7:27:57 PM ******/



---- ====================================================
---- Description:	Inserts the property Details response from the Bed Banks API
---- Written: Adam
---- Created: 2016.05.16
---- ====================================================
CREATE PROCEDURE [dbo].[LB_BB_insertProviderDetails]
	
	-- Input parameters 
	@pi_providerid int = 1,
	@pi_availabilityProviderID int = 1,
	@pi_xmlResponse varchar(max),
	@pi_expiryTime datetime,
	
	--Return parameters
	@po_returnstatus INT OUTPUT,
	@po_returnmsg VARCHAR(200) OUTPUT

AS
set nocount on;
BEGIN
	--Assume transaction will fail, set the return variable to -1.
	SET @po_returnstatus = -1;
	SET @po_returnmsg = null;
	
	BEGIN TRY
        INSERT INTO LB_BB_providerDetails (
			_ProviderID
			,availabilityProviderID
			,xmlResponse
			,ExpiryTime
		)
		VALUES
		(
			@pi_providerid
			,@pi_availabilityProviderID
			,@pi_xmlResponse
			,DATEADD(day, 30, GETDATE())
		)      
    END TRY	
	BEGIN CATCH	  
		SET @po_returnstatus = error_number();
		SET @po_returnmsg = error_message();
		RETURN;
	END CATCH
	
	SET @po_returnstatus = 0;
	SET @po_returnmsg = 'Completed successfully.';	
END





GO

