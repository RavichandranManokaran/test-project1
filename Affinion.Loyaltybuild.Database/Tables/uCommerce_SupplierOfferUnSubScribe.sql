﻿CREATE TABLE [dbo].[uCommerce_SupplierOfferUnSubScribe](
	[SupplierOfferUnSubScribeId] [int] IDENTITY(1,1) NOT NULL,
	[Reason] [nvarchar](500) NOT NULL,
	[CategoryGuid] [nvarchar](500) NOT NULL,
	[CampaignItem] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](100) NULL,
 CONSTRAINT [PK_uCommerce_SupplierOfferUnSubScribe] PRIMARY KEY CLUSTERED 
(
	[SupplierOfferUnSubScribeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO