

/****** Object:  StoredProcedure [dbo].[LB_BB_getBedBankRegionsForClient]    Script Date: 7/19/2016 7:25:29 PM ******/
---- ====================================================
---- Description:	Tii Sp to return all countries for a voucher client
---- Written: Noreen
---- Created: 2011.08.30
---- ====================================================
CREATE PROCEDURE [dbo].[LB_BB_getBedBankRegionsForClient]
	
	-- Input parameters 
	@pi_ClientID int = 68001,
	
	--Return parameters
	@po_returnstatus INT OUTPUT,
	@po_returnmsg VARCHAR(200) OUTPUT,
	@po_scopeid NUMERIC OUTPUT
	
AS
set nocount on;
BEGIN

	--Assume transaction will fail, set the return variable to -1.
	SET @po_returnstatus = -1;
	SET @po_returnmsg = null;
	SET @po_scopeid = 0;	
	BEGIN TRY
	
		DECLARE @MAPLOCATIONID INT,@MAPLOCATIONNAME varchar(500),@COUNTRYNAME varchar(500), @COUNTER INT = 0, @DATA int
		Declare myCur cursor for
		Select distinct  ml.maplocationid,maplocationName,countryname, (Select Top 1 RegionID from LB_BB_Location where Region = ml.MapLocationName) as data from
		syn_emosmaplocation ml inner join syn_emosProvider p on ml.MapLocationID = p.MapLocationID
		and p.isActive = 1
		inner join syn_emosLocation l  on p.LocationID = l.LocationID
		inner join syn_emosCountryLingual CL on CL.CountryID = l.CountryID
		and CL.LanguageID = 1
		and ml.maplocationid not in (Select maptorecordid from LB_BB_mappings where mapsto = 'maplocation' and mapstocolumn='maplocationid')
		and ml.status = 1
		and (p.providertypeid in (1,2,3,4) or p.providertypeid > 22)

		--where (Select Top 1 Region from Location where Region = ml.MapLocationName) is null

		Open myCur
		Fetch next from myCur into @MaplocationID,@MaplocationName,@COUNTRYNAME,@DATA

		While @@FETCH_STATUS = 0
		BEGIN

			IF @DATA IS NULL
			BEGIN
				SET @COUNTER = @COUNTER - 1
				INSERT INTO LB_BB_Location(Country,RegionID,Region,ResortID,Resort,isMatched)
				Values (@COUNTRYNAME,@COUNTER,@MAPLOCATIONNAME,-1,'LB Holding Resort',1)
			
				INSERT INTO LB_BB_mappings
				Values ('Location',@COUNTER,'Region','Maplocation',@MaplocationID,'MaplocationID')
			END
			ELSE
			BEGIN
			
				UPDATE LB_BB_Location set isMatched = 1
				where RegionID = @DATA
				INSERT INTO LB_BB_mappings
				Values ('Location',@DATA,'Region','Maplocation',@MaplocationID,'MaplocationID')
			END

		Fetch next from myCur into @MaplocationID,@MaplocationName,@COUNTRYNAME,@DATA
		END
		CLOSE myCur
		DEALLOCATE myCur
	
	IF @pi_ClientID = 68045
	BEGIN
		SELECT DISTINCT Country,RegionID, REGION FROM Bedbank.dbo.LB_BB_Location
			where Country in ('England','Scotland','Wales','Republic of Ireland', 'Northern Ireland', 'United Kingdom')
			Order by Region asc,Country desc
			
		/*SELECT DISTINCT Country,RegionID, REGION FROM Bedbank.dbo.Locatio
		where RegionID in (
		279
		,4958
		,186
		,390
		,3767
		,150
		,3765
		,4906
		,507
		,249
		,4907
		,633
		,444
		,3417
		,622
		,4911
		,226
		,60
		,14
		,118
		)
			Order by Country asc, Region asc*/
	END 
	ELSE
	BEGIN
			SELECT DISTINCT Country,RegionID, REGION FROM Bedbank.dbo.LB_BB_Location
			where Country like '%Ireland%'
			and regionid <> 4971
			Order by Region asc
	END
	

	
	
    END TRY	
	BEGIN CATCH	  
		SET @po_returnstatus = error_number();
		SET @po_returnmsg = error_message();
		RETURN;
	END CATCH
	
	SET @po_returnstatus = 0;
	SET @po_returnmsg = 'Completed successfully.';	
END











GO

