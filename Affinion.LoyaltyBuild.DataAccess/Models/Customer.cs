﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           Customer.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.DataAccess
/// Description:           Customer data model.
/// </summary>


namespace Affinion.LoyaltyBuild.DataAccess.Models
{
    public class Customer
    {
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string Phone { get; set; }
        public string MobilePhone { get; set; }
        public string MobilePhoneNumber { get; set; }
        public string ClientId { get; set; }
        public string Title { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string AddressLine4 { get; set; }    //TO DO: Remove. Not required.
        public string Language { get; set; }    //TO DO: Remove. Not required.
        public int CountryId { get; set; }
        public string LocationId { get; set; }
        public string LocationName{ get; set; }
        public string BranchId { get; set; }
        public string BookingReferenceId { get; set; }
        public int Subscribe { get; set; }
        public int Active { get; set; }
        public string SubscribeType { get; set; }
        public int Store { get; set; }
        public int TermsandCondition { get; set; }
        public string FullName { get; set; }
        public string FullAddress { get; set; }
        public string GuestName { get; set; }
        public string Notes { get; set; }
        public int OrderLineId { get; set; }
    }
}
