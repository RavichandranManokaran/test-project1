﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalAddCustomerQuery.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalAddCustomerQuery" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<script type="text/javascript">

    function ShowAddClient() {
        document.getElementById("addDiv").style.display = "block";
        document.getElementById("searchDiv").style.display = "none";
        var resultDiv = document.getElementById("ResultDiv");
        if (resultDiv)
        { resultDiv.style.display = "none"; }
        return false;
    }

    function ShowSearch() {
        document.getElementById("addDiv").style.display = 'none';
        document.getElementById("searchDiv").style.display = 'block';
        var resultDiv = document.getElementById("ResultDiv");
        if (resultDiv)
        { resultDiv.style.display = "block"; }
        return false;
    }

    function ResetControls() {       
        var locationDropDown = document.getElementById("LocationDropDownSearch")
        locationDropDown.selectedIndex = 0;
        var countryDropDown = document.getElementById("CountryDropDownSearch")
        countryDropDown.selectedIndex = 0;
        document.getElementById("FirstNameSearch").value = '';
        document.getElementById("LastNameSearch").value = '';
        document.getElementById("AddressLine1Search").value = '';
        document.getElementById("AddressLine2Search").value = '';
        document.getElementById("PrimaryPhoneSearch").value = '';
        document.getElementById("ButtonInsert").disabled = true;
      
        var resultDiv = document.getElementById("ResultDiv");
        if (resultDiv)
        { resultDiv.innerHTML=''; }
        return false;
    }
    function ResetCustomerValidationControls() {
        var titleDropDown = document.getElementById("maincontentofflineportal_0_TitleDropDown")
        titleDropDown.selectedIndex = 0;
        var countryDropDown = document.getElementById("maincontentofflineportal_0_CountryDropDown")
        countryDropDown.selectedIndex = 0;
        var locationDropDown = document.getElementById("maincontentofflineportal_0_LocationDropDown")
        locationDropDown.selectedIndex = 0;
        document.getElementById("maincontentofflineportal_0_FirstName").value = '';
        document.getElementById("maincontentofflineportal_0_LastName").value = '';
        document.getElementById("maincontentofflineportal_0_AddressLine1").value = '';
        document.getElementById("maincontentofflineportal_0_AddressLine2").value = '';
        document.getElementById("maincontentofflineportal_0_AddressLine3").value = '';
        document.getElementById("maincontentofflineportal_0_PrimaryPhone").value = '';
        document.getElementById("maincontentofflineportal_0_SecondaryPhone").value = '';
        document.getElementById("maincontentofflineportal_0_Email").value = '';
        document.getElementById("maincontentofflineportal_0_ButtonInsert").disabled = true;

        var resultDiv = document.getElementById("ResultDiv");
        if (resultDiv)
        { resultDiv.innerHTML = ''; }
        return false;
    }

    function ResetCustomerQuery()
    {
        var clientDropDown = document.getElementById("ClientDropDownSearch");
        clientDropDown.disabled = false;
        clientDropDown.selectedIndex = 0;
        var querytype = document.getElementById("QueryType");
        querytype.selectedIndex = 0;
        var compaignGroup = document.getElementById("ddlCompaignGroup");
        compaignGroup.selectedIndex = 0;
        var provider = document.getElementById("ddlProvider");
        provider.selectedIndex = 0;
        document.getElementById("Notes").value = '';
        IsSolved_1.checked = "checked";
        return false;
    }

    function FocusSearchResultGrid() {
        document.getElementById('data-grid').scrollIntoView();
    }
</script>

<%if (ValidationResponse != null)
  {%>
<div class="col-sm-12 alert alert-<%= ValidationResponse.IsSuccess ? "success" : "danger" %>">
    <%= ValidationResponse.Message %>
</div>
<%} %>

<div id="divMsg" runat="server">
</div>
<div class="container booking-detail-info">

    <div class="content">

        <%--Customer Enquiry Detail--%>
        <div class="row">
            <div class="col-sm-12">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading bg-primary">
                            <h4 class="panel-title"><sc:text id="CustomerEnquiryHeaderText" field="CustomerEnquiryHeader" runat="server" />
                            </h4>
                        </div>
                        <div id="collapse0" class="panel-collapse collapse in">
                            <div class="panel-body alert-info">

                                <div class="col-sm-12 no-padding-m">
                                    <div class="content_bg_inner_box alert-info">
                                        <div class="col-sm-6 no-padding-m">

                                            <div class="row margin-row">
                                                <div class="col-sm-5">
                                                    <span class="bask-form-titl font-size-12"> <sc:text id="QueryTypeText" field="QueryType" runat="server" /> <span class="accent58-f"> *
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValQueryType" ValidationGroup="Insert" runat="server" ControlToValidate="QueryType"><span class='fa fa-warning'><sc:text id="QueryTypeRequired" field="QueryTypeRequired" runat="server" /></span></asp:RequiredFieldValidator>
                                                    </span>
                                                    </span>
                                                </div>
                                                <div class="col-sm-7">
                                                    <%-- Query Type:--%>
                                                    <asp:DropDownList ID="QueryType" ClientIDMode="Static" CssClass="form-control font-size-12" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="row margin-row">
                                                <div class="col-sm-5">
                                                    <span class="bask-form-titl font-size-12"><sc:text id="PartnerText" field="Partner" runat="server" />  </span><span class="accent58-f">*
                                                        <asp:RequiredFieldValidator ID="RequiredFieldPartner" Display="Dynamic" runat="server" InitialValue="Please Select" ValidationGroup="Insert" ControlToValidate="ClientDropDownSearch"><span class='fa fa-warning'><sc:text id="PartnerRequired1" field="PartnerRequired" runat="server" /></span></asp:RequiredFieldValidator>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValPartner" Display="Dynamic" runat="server" InitialValue="Please Select" ValidationGroup="Search" ControlToValidate="ClientDropDownSearch"><span class='fa fa-warning'><sc:text id="PartnerRequired2" field="PartnerRequired" runat="server" /></span></asp:RequiredFieldValidator>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" InitialValue="Please Select" ValidationGroup="Add" ControlToValidate="ClientDropDownSearch"><span class='fa fa-warning'><sc:text id="PartnerRequired3" field="PartnerRequired" runat="server" /></span></asp:RequiredFieldValidator>
                                                    </span>
                                                </div>
                                                <div class="col-sm-7">
                                                    <asp:DropDownList ID="ClientDropDownSearch" ClientIDMode="Static" runat="server" CssClass="form-control font-size-12"></asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="row margin-row">
                                                <div class="col-sm-5"><span class="bask-form-titl font-size-12"><sc:text id="CampaignGroupText" field="CampaignGroup" runat="server" /> </span></div>
                                                <div class="col-sm-7">
                                                    <asp:DropDownList ClientIDMode="Static"  CssClass="form-control font-size-12" ID="ddlCompaignGroup" runat="server"></asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="row margin-row">
                                                <div class="col-sm-5"><span class="bask-form-titl font-size-12"><sc:text id="ProviderText" field="Provider" runat="server" /></span></div>
                                                <div class="col-sm-7">
                                                    <asp:DropDownList ClientIDMode="Static" CssClass="form-control font-size-12" ID="ddlProvider" runat="server"></asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="row margin-row">
                                                <div class="col-sm-5"><span class="bask-form-titl font-size-12"><sc:text id="IsResolvedText" field="IsResolved" runat="server" /></span><span class="accent58-f"> *</span></div>
                                                <div class="col-sm-7">
                                                    <asp:RadioButtonList ID="IsSolved" ClientIDMode="Static" CssClass="font-size-10" runat="server" Width="100" CellPadding="0" BorderStyle="None" RepeatDirection="Horizontal">
                                                        <asp:ListItem Text=" Yes " Value="Yes"></asp:ListItem>
                                                        <asp:ListItem Text=" No " Value="No" Selected="True"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>

                                            <div class="row margin-row pull-right col-sm-6" style="padding: 0;">
                                                <div class="col-sm-6 no-padding">
                                                    <asp:Button runat="server" ID="ButtonInsert" ValidationGroup="Insert" Text="Insert" class="btn btn-default add-break pull-right width-full search-btn" OnClick="ButtonInsert_Click " />
                                                </div>
                                                <div class="col-sm-6 no-padding-left">
                                                   <button id="btnCQReset"  onclick=" return ResetCustomerQuery();"  class="btn btn-default add-break pull-right width-full" causesvalidation="false"  style="width: 100px;float:left" type="button">  <sc:text id="ResetButtonText" field="ResetButton" runat="server" /> </button>  
                                                    <%--<input type="button" onclick=" return ResetCustomerQuery();" causesvalidation="false" class="btn btn-default add-break pull-right width-full" value="Reset"></input>--%>
                                                    <%--<button class="btn btn-default add-break pull-right width-full" onclick=" return ResetCustomerQuery();" type="reset">Reset</button>--%>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6 no-padding-m">
                                            <div class="row margin-row">
                                                <div class="col-sm-3">
                                                    <span class="bask-form-titl font-size-12"><sc:text id="NotesText" field="Notes" runat="server" /><span class="accent58-f"> *
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValNotes" ValidationGroup="Insert" runat="server" ControlToValidate="Notes"><span class='fa fa-warning'><sc:text id="NotesRequired" field="NotesRequired" runat="server" /></asp:RequiredFieldValidator>
                                                    </span>
                                                    </span>
                                                </div>
                                                <div class="col-sm-9">
                                                    <textarea id="Notes" runat="server" class="form-control font-size-12" style="min-height: 100px; min-width: 200px"></textarea>
                                                    <asp:CustomValidator id="CVnotes" ControlToValidate="Notes" ClientValidationFunction="ClientValidate" Display="Dynamic" ForeColor="Red" Font-Name="verdana"  Font-Size="10pt" runat="server"><sc:text id="DontEnterCardNumber" field="DontEnterCardNumber" runat="server"/></asp:CustomValidator>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="panel-group" id="searchDiv">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><sc:text id="CustomerValidationHeaderText" field="CustomerValidationHeader" runat="server" /></h4>
                        </div>
                        <div class="panel-body alert-info">

                            <div class="col-sm-12 no-padding-m">
                                <div class="content_bg_inner_box alert-info">
                                    <div class="col-sm-6 no-padding-m">
                                        <div class="row margin-row">
                                            <div class="col-sm-5">
                                                <span class="bask-form-titl font-size-12"> <sc:text id="ClientFirstnameText" field="ClientFirstname" runat="server" /> <span class="accent58-f"> *
                                                <asp:RequiredFieldValidator ID="FirstNameRequiredFieldValSearch" ValidationGroup="Search" runat="server" ControlToValidate="FirstNameSearch"><span class='fa fa-warning'><sc:text id="FirstNameRequired" field="FirstNameRequired" runat="server" /></span></asp:RequiredFieldValidator>
                                                </span></span>
                                            </div>
                                            <div class="col-sm-7">
                                                <input type="text" id="FirstNameSearch" clientidmode="Static" runat="server" class="form-control font-size-12" placeholder="First Name">
                                            </div>
                                        </div>
                                        <div class="row margin-row">
                                            <div class="col-sm-5">
                                                <span class="bask-form-titl font-size-12"> <sc:text id="ClientLastnameText" field="ClientLastname" runat="server" /> <span class="accent58-f"> * 
                                                <asp:RequiredFieldValidator ID="LastNameRequiredFieldValSearch" ValidationGroup="Search" runat="server" ControlToValidate="LastNameSearch"><span class='fa fa-warning'><sc:text id="LastNameRequired" field="LastNameRequired" runat="server" /></span></asp:RequiredFieldValidator>
                                                </span></span>
                                            </div>
                                            <div class="col-sm-7">
                                                <input type="text" id="LastNameSearch" clientidmode="Static" runat="server" class="form-control font-size-12" placeholder="Last Name">
                                            </div>
                                        </div>
                                        <div class="row margin-row">
                                            <div class="col-sm-5"><span class="bask-form-titl font-size-12"><sc:text id="CountyText" field="County" runat="server" /> </span></div>
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="CountryDropDownSearch" ClientIDMode="Static" runat="server" CssClass="form-control font-size-12"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 no-padding-m">
                                        <div class="row margin-row">
                                            <div class="col-sm-5"><span class="bask-form-titl font-size-12"> <sc:text id="ClientAddressLine1Text" field="AddressLine1" runat="server" /> </span></div>
                                            <div class="col-sm-7">
                                                <input type="text" runat="server" id="AddressLine1Search" clientidmode="Static" class="form-control font-size-12" placeholder="Address Line1" />
                                            </div>
                                        </div>
                                        <div class="row margin-row">
                                            <div class="col-sm-5"><span class="bask-form-titl font-size-12"> <sc:text id="ClientAddressLine2Text" field="AddressLine2" runat="server" /> </span></div>
                                            <div class="col-sm-7">
                                                <input type="text" runat="server" id="AddressLine2Search" clientidmode="Static" class="form-control font-size-12" placeholder="Address Line2" />
                                            </div>
                                        </div>
                                        <div class="row margin-row">
                                            <div class="col-sm-5"><span class="bask-form-titl font-size-12"> <sc:text id="ClientPrimaryPhoneText" field="PrimaryPhone" runat="server" /> </span></div>
                                            <div class="col-sm-7">
                                                <input type="text" runat="server" id="PrimaryPhoneSearch" clientidmode="Static" class="form-control font-size-12" placeholder="Phone">
                                            </div>
                                        </div>
                                        <div class="row margin-row">
                                            <div class="col-sm-5">
                                                <span class="bask-form-titl font-size-12">  <sc:text id="LocationsText" field="Locations" runat="server" /> </span>
                                            </div>
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="LocationDropDownSearch" ClientIDMode="Static" runat="server" CssClass="form-control font-size-12"></asp:DropDownList>
                                               <asp:HiddenField ID="hdfLocationSearch" ClientIDMode="Static" runat="server" />
                                            </div>
                                        </div>

                                        <div class="row margin-row pull-right col-sm-12" style="padding: 0;">
                                            <div class="col-sm-12">
                                                 <button id="btnSearchReset"  onclick=" return ResetControls();"  class="btn btn-default add-break pull-right" causesvalidation="false"  style="width: 100px;float:left" type="button">  <sc:text id="btnCVResetText" field="CVResetButton" runat="server" /> </button>
                                                <asp:Button ID="AddClient" runat="server" OnClientClick="return ShowAddClient();" CausesValidation="false" Text="Add Client" class="btn btn-default add-break pull-right search-btn marR5" />
                                                <asp:Button ID="ButtonSearch" runat="server" OnClick="ButtonSearch_Click" ValidationGroup="Search" Text="Search" type="submit" class="btn btn-default add-break pull-right search-btn marR5 ButtonSearch" ></asp:Button>
                                            
                                                
                                           
                                               
                                               <%-- <input type="reset" onclick=" return ResetControls();" causesvalidation="false" class="btn btn-default add-break pull-right width-full" value="Reset"></input>--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>




        <%-- Result Grid--%>
        <div runat="server" id="ResultDiv" clientidmode="Static" visible="false">
            <div id="data-grid" class="table-responsive">
                <asp:GridView ID="ResultGridView" runat="server" OnSelectedIndexChanged="ResultGridView_SelectedIndexChanged" SelectedRowStyle-Font-Bold="true" CssClass="table tbl-calendar" OnRowEditing="ResultGridView_RowEditing"
                    ShowHeaderWhenEmpty="true" >
                    <Columns>
                      <%-- <asp:BoundField DataField="CustomerId" />
                        <asp:BoundField DataField="FullName" HeaderText="Customer" />
                        <asp:BoundField DataField="FullAddress" HeaderText="Address" />
                        <asp:BoundField DataField="EmailAddress" HeaderText="Email Address" />
                        <asp:BoundField DataField="MobilePhoneNumber" HeaderText="Mobile Phone No" />
                        <asp:CommandField ShowSelectButton="True" SelectText="<i class='glyphicon glyphicon-edit icons-positionRight'></i>" />--%>
                    </Columns>
                </asp:GridView>
            </div>
        </div>




        <%--Add Customer--%>
        <div class="row">
            <div class="col-sm-12">
                <div id="addDiv" style="display: none">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"> <sc:text id="CustomerAdditionTitleText" field="CustomerAdditionTitle" runat="server" />
                            </h4>
                        </div>

                        <div class="panel-body alert-info">

                            <div class="col-sm-12 no-padding-m">
                                <div class="content_bg_inner_box alert-info">
                                    <div class="col-sm-6 no-padding-m">
                                        <div class="row margin-row">
                                            <div class="col-sm-5">
                                                <span class="bask-form-titl font-size-12">
                                                    <asp:HiddenField ID="CustomerId" runat="server" Value="0" />
                                                    <span class="bask-form-titl font-size-12"><sc:text id="CustomerTitleText" field="CustomerTitle" runat="server" /></span>
                                                </span>
                                            </div>
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="TitleDropDown" runat="server" CssClass="form-control font-size-12"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row margin-row">
                                            <div class="col-sm-5">
                                                <span class="bask-form-titl font-size-12">
                                                    <%--       First Name:--%>
                                                    <span class="bask-form-titl font-size-12"> <sc:text id="FirstNameText" field="FirstName" runat="server" />
                                                <span class="accent58-f">*
                                                       
                                               
                                                <asp:RequiredFieldValidator ID="FirstNameRequiredFieldValidatorEdit" ValidationGroup="Add" runat="server" ControlToValidate="FirstName"><span class='fa fa-warning'><sc:text id="FirstNameRequiredValEdit" field="FirstNameRequired" runat="server" /></span></asp:RequiredFieldValidator>
                                                </span></span></span>
                                            </div>
                                            <div class="col-sm-7">

                                                <asp:TextBox ID="FirstName" class="form-control font-size-12" placeholder="First Name" runat="server"> </asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row margin-row">
                                            <div class="col-sm-5">
                                                <span class="bask-form-titl font-size-12">
                                                    <%--          Last Name:--%>
                                                    <span class="bask-form-titl font-size-12"> <sc:text id="LastNameText" field="LastName" runat="server" />
                                                <span class="accent58-f">*
                                                    <asp:RequiredFieldValidator ID="LastNameRequiredFieldValidatorEdit" runat="server" ValidationGroup="Add" ControlToValidate="LastName"><span class='fa fa-warning'><sc:text id="LastNameRequiredValEdit" field="LastNameRequired" runat="server" /></span></asp:RequiredFieldValidator>
                                                </span></span></span>
                                            </div>
                                            <div class="col-sm-7">

                                                <asp:TextBox ID="LastName" class="form-control font-size-12" placeholder="Last Name" runat="server"> </asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row margin-row">
                                            <div class="col-sm-5">
                                                <span class="bask-form-titl font-size-12">
                                                    <%-- Address Line1:--%>
                                                    <span class="bask-form-titl font-size-12"> <sc:text id="AddressLine1Text" field="CAAddressLine1" runat="server" /> 
                                                <span class="accent58-f">*
                                                    <asp:RequiredFieldValidator ID="AddressLine1RequiredFieldValidator3" ValidationGroup="Add" runat="server" ControlToValidate="AddressLine1"><span class='fa fa-warning'><sc:text id="AddressLine1Required" field="AddressLine1Required" runat="server" /></span></asp:RequiredFieldValidator>
                                                </span></span></span>
                                            </div>
                                            <div class="col-sm-7">

                                                <asp:TextBox ID="AddressLine1" ValidationGroup="Add" class="form-control font-size-12" placeholder="Address Line 1" runat="server"> </asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row margin-row">
                                            <div class="col-sm-5">
                                                <span class="bask-form-titl font-size-12">
                                                    <%-- Address Line2:--%>
                                                    <span class="bask-form-titl font-size-12"> <sc:text id="AddressLine2Text" field="CAAddressLine2" runat="server" />
                                                    </span></span>
                                            </div>
                                            <div class="col-sm-7">
                                                <asp:TextBox ID="AddressLine2" class="form-control font-size-12" placeholder="Address Line 2" runat="server"> </asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row margin-row">
                                            <div class="col-sm-5">
                                                <span class="bask-form-titl font-size-12"> <sc:text id="AddressLine3Text" field="CAAddressLine3" runat="server" /> 
                                                <%--Address Line3:--%>
                                                </span>
                                            </div>
                                            <div class="col-sm-7">
                                                <asp:TextBox ID="AddressLine3" class="form-control font-size-12" placeholder="Address Line 3" runat="server"> </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 no-padding-m">
                                        <div class="row margin-row">
                                            <div class="col-sm-5">
                                                <span class="bask-form-titl font-size-12"> <sc:text id="CountryText" field="CACountry" runat="server" /> 
                                                <%--Country:--%>
                                                    <span class="accent58-f">*
                                                        <asp:RequiredFieldValidator ID="CountryRequiredFieldValidator" runat="server" ValidationGroup="Add" ControlToValidate="CountryDropDown" InitialValue="0"><span class='fa fa-warning'><sc:text id="CountryRequired" field="CountryRequired" runat="server" /></span></asp:RequiredFieldValidator>
                                                    </span>
                                                </span>
                                            </div>
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="CountryDropDown"  ValidationGroup="Add" runat="server" CssClass="form-control font-size-12"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row margin-row">
                                            <div class="col-sm-5">
                                                <span class="bask-form-titl font-size-12"> <sc:text id="LocationText" field="CALocation" runat="server" /> 
                                                <%--Location:--%>
                                                    <span class="accent58-f">*
                                                        <asp:RequiredFieldValidator ID="LocationRequiredFieldValidator" ValidationGroup="Add" runat="server" InitialValue="0" ControlToValidate="LocationDropDown"><span class='fa fa-warning'><sc:text id="LocationRequired" field="LocationRequired" runat="server" /></span></asp:RequiredFieldValidator>
                                                    </span>
                                                </span>
                                            </div>
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="LocationDropDown" ValidationGroup="Add" runat="server" CssClass="form-control font-size-12"></asp:DropDownList>
                                                <asp:HiddenField ID="hdfLocation" ClientIDMode="Static" runat="server" />
                                            </div>
                                        </div>

                                        <div class="row margin-row">
                                            <div class="col-sm-5">
                                                <span class="bask-form-titl font-size-12"> <sc:text id="PrimaryPhoneText" field="CAPrimaryPhone" runat="server" /> <span class="accent58-f">*
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidatorPhone" Display="Dynamic" runat="server" ValidationGroup="Add" ControlToValidate="PrimaryPhone"><span class='fa fa-warning'><sc:text id="PrimaryPhoneRequired1" field="PrimaryPhoneRequired" runat="server" /></span></asp:RegularExpressionValidator>
                                                    <asp:RequiredFieldValidator ID="PrimaryPhoneRequiredFieldValidator" Display="Dynamic" runat="server" ValidationGroup="Add" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="PrimaryPhone"><span class='fa fa-warning'><sc:text id="PrimaryPhoneRequired2" field="PrimaryPhoneRequired" runat="server" /></span></asp:RequiredFieldValidator>
                                                </span></span>
                                            </div>
                                            <div class="col-sm-7">
                                                <asp:TextBox ID="PrimaryPhone" ValidationGroup="Add" class="form-control font-size-12" placeholder="Phone" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row margin-row">
                                            <div class="col-sm-5">
                                                <span class="bask-form-titl font-size-12"> <sc:text id="SecondaryPhoneText" field="CASecondaryPhone" runat="server" />
                                                <%--Secondary Phone:--%>
                                                    <span class="accent58-f">
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorSecondaryPhone" runat="server" ValidationGroup="Add" ControlToValidate="SecondaryPhone"><span class='fa fa-warning'><sc:text id="SecondaryPhoneRequired" field="SecondaryPhoneRequired" runat="server" /></span></asp:RegularExpressionValidator>
                                                    </span>
                                                </span>
                                            </div>
                                            <div class="col-sm-7">
                                                <asp:TextBox ID="SecondaryPhone" class="form-control font-size-12" placeholder="Phone" runat="server"> </asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row margin-row">
                                            <div class="col-sm-5">
                                                <span class="bask-form-titl font-size-12"> <sc:text id="EmailText" field="Email" runat="server" />
                                                <%--Email:--%>
                                                    <span class="accent58-f">*
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorEmail" Display="Dynamic" runat="server" ValidationGroup="Add" ControlToValidate="Email"><span class='fa fa-warning'><sc:text id="EnterCorrectEmailFormat" field="EnterCorrectEmailFormat" runat="server" /></span></asp:RegularExpressionValidator>
                                                        <asp:RequiredFieldValidator ID="EmailRequiredFieldValidator" Display="Dynamic" runat="server" ValidationGroup="Add" ControlToValidate="Email"><span class='fa fa-warning'><sc:text id="EmailRequired" field="EmailRequired" runat="server" /></span></asp:RequiredFieldValidator>
                                                    </span>
                                                </span>
                                            </div>
                                            <div class="col-sm-7">
                                                <asp:TextBox ID="Email" class="form-control font-size-12 validateCreditCard" placeholder="Email" runat="server" OnTextChanged="Email_TextChanged" AutoPostBack="false"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="row margin-row">
                                            <div class="col-sm-12">
                                                <asp:Button ID="GoBack" runat="server" OnClientClick="return ShowSearch();" ClientIDMode="Static" Text="Go Back" class="btn btn-default add-break pull-right " CausesValidation="false" />
                                                <button id="btnReset" onclick=" return ResetCustomerValidationControls();" class="btn btn-default add-break pull-right marR5 " causesvalidation="false"  style="width: 100px;float:left" type="button">  <sc:text id="ResetText" field="CAResetButton" runat="server" /> </button>
                                                 <asp:Button ID="SearchAgain" runat="server" Text="Search Again " Width="100" OnClientClick="return ShowSearch();" ClientIDMode="Static" class="btn btn-default add-break pull-right marR5 " ValidationGroup="Add" OnClick="Search_Click" />
                                                <asp:Button ID="EditAndSave" runat="server" ClientIDMode="Static" Text="Add Client " ValidationGroup="Add" class="btn btn-default add-break pull-right marR5 EditAndSave" OnClick="AddAndSave_Click" />
                                            
                                               
                                            
                                                 
                                              <%--  <input type="reset" class="btn btn-default add-break pull-right width-full" causesvalidation="false" value="Reset"></input>--%>
                                                <%--<asp:Button ID="Reset" runat="server" Text="Reset" class="btn btn-default add-break pull-right width-full" />--%>
                                            
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>
</div>

<script>

    $(".ButtonSearch,.EditAndSave").click(function () {
        $('html, body').animate({
            scrollTop: $("#maincontentofflineportal_0_RequiredFieldValPartner").position().top
        }, 1000);
    });


    $(function () {
        $('#ButtonInsert').addClass('btn btn-default add-break pull-right');
        jQuery("#CountryDropDownSearch").change(function () {
            var countryVal = jQuery("#CountryDropDownSearch option:selected").html();
            $.get("/handlers/cs/ServerSideEventHandler.ashx?action=LoadLocationByCountry&value=" + countryVal, function (data) {
                if (data) {
                    var jsonData = JSON.parse(data); //This converts the string to json
                    $('#LocationDropDownSearch').find('option').not(':first').remove();
                    for (var i = 0; i < jsonData.length; i++) //The json object has lenght
                    {
                        var object = jsonData[i]; //You are in the current object
                        var objItems = object.split(",");
                        $('#LocationDropDownSearch').append('<option value=' + objItems[0] + '>' + objItems[1] + '</option>'); //now you access the property.

                    }
                }
            });
        });

        jQuery("#LocationDropDownSearch").change(function () {
            $("#hdfLocationSearch").val(jQuery("#LocationDropDownSearch :selected").text());
        });

        jQuery("#maincontentofflineportal_0_CountryDropDown").change(function () {
            var countryVal = jQuery("#maincontentofflineportal_0_CountryDropDown option:selected").html();
            $.get("/handlers/cs/ServerSideEventHandler.ashx?action=LoadLocationByCountry&value=" + countryVal, function (data) {
                if (data) {
                    var jsonData = JSON.parse(data); //This converts the string to json
                    $('#maincontentofflineportal_0_LocationDropDown').find('option').not(':first').remove();
                    for (var i = 0; i < jsonData.length; i++) //The json object has lenght
                    {
                        var object = jsonData[i]; //You are in the current object
                        var objItems = object.split(",");
                        $('#maincontentofflineportal_0_LocationDropDown').append('<option value=' + objItems[0] + '>' + objItems[1] + '</option>'); //now you access the property.

                    }
                }
            });
        });

        jQuery("#maincontentofflineportal_0_LocationDropDown").change(function () {
            $("#hdfLocation").val(jQuery("#maincontentofflineportal_0_LocationDropDown :selected").val());
        });
    });
   
</script>

<script type="text/javascript">
    function Cardvalidate(source, arguments) {
        var pattern = /(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})/;
        arguments.IsValid = !pattern.test(arguments.Value);
    }

    </script>
