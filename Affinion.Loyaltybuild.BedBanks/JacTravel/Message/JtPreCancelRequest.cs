﻿using Affinion.LoyaltyBuild.BedBanks.JacTravel.Message.Interfaces;
using System;
using System.IO;
using System.Text;
using System.Diagnostics;
using System.Xml.Serialization;
using Affinion.LoyaltyBuild.BedBanks.Troika;
using Affinion.LoyaltyBuild.BedBanks;
using Affinion.LoyaltyBuild.BedBanks.General;
using System.Collections.Specialized;
namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Message
{
    [Serializable()]
    [XmlType(TypeName = "PreCancelRequest")]
    public class JtPreCancelRequest : JtRequest, IJtRequest
    {
        public string BookingReference { get; set; }
        
        //Parameterless Constructor - Needed for XML Serialization
        private JtPreCancelRequest() { }
        BedBanksSettings bbsettings;
        //The real constructor
        int currId = 0;
        public JtPreCancelRequest(string pBookingReference, BedBanksSettings bbs)
        {
            this.bbsettings = bbs;
            if (bbs.BedBanksActualCurrency != null)
            {
                int.TryParse(bbs.BedBanksActualCurrency.BedBanksCurrencyId, out currId);
                LoginDetails = JtController.GenerateLoginDetails(currId, bbs.BedBanksUserName, bbs.BedBanksPassword);
            }
            else
            {
                int.TryParse(bbs.BedBanksDefaultCurrency.BedBanksCurrencyId, out currId);
                LoginDetails = JtController.GenerateLoginDetails(bbs.BedBanksUserName, bbs.BedBanksPassword);
            }
                BookingReference = pBookingReference;
        }

        public IJtResponse Fire()
        {
            Stopwatch apiTimer = Stopwatch.StartNew();
            string response = JtController.PerformApiRequest(this);
            apiTimer.Stop();

            //READ THE RESULTS
            XmlRootAttribute xRoot = new XmlRootAttribute
            {
                ElementName = "PreCancelResponse",
                IsNullable = true
            };

            XmlSerializer serializer = new XmlSerializer(typeof(JtPreCancelResponse), xRoot);

            JtPreCancelResponse pcRes;

            try
            {
                MemoryStream memStream = new MemoryStream(Encoding.UTF8.GetBytes(response));
                pcRes = (JtPreCancelResponse)serializer.Deserialize(memStream);
            }
            catch (Exception fnfExc)
            {
                NameValueCollection exception = new NameValueCollection();
                exception.Add(Constants.ExceptionMessage, fnfExc.Message);
                exception.Add(Constants.ExceptionFlow, Constants.PrecancelRequest);
                SendExceptionMail(exception, bbsettings.ClientItemId);
                throw new Exception(fnfExc.Message);
            }

            pcRes.ResponseTime = apiTimer.ElapsedMilliseconds;
            TroikaDataController.InsertApiResponseTime(1, "PreCancel", pcRes.ResponseTime);

            return pcRes;
        }
    }
}
