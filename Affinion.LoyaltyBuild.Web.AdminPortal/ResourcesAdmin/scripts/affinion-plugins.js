/*global window: false */
/*global jQuery: false */
/*global Rgen: false */
/*global console: false */
/*global $: false */
/*
 Rgen-Plugins
 Version : 0.2.2
 */


Rgen.Plugins.Add({

    Modal: function (title, content) {
        'use strict';
        var props = {
                popupHtml: '<div class="modal fade" id="taylorPopup" tabindex="-1" role="dialog" aria-labelledby="taylorPopupTitle" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><span class="ty-close close pull-right" title="Close" data-dismiss="modal" aria-hidden="true"></span><h4 class="ty-page-heading" id="taylorPopupTitle"></h4></div><div class="modal-body"></div><div class="modal-footer"></div></div></div></div>',
                popupContainer: 'body',
                popup: null,
                popupInner: null
            },
            classes = {
                id: '#taylortaylorPopup',
                title: '.modal-header',
                body: '.modal-body',
                footer: '.modal-footer'
            };


        function privateBaseInit(opt) {

            var jqObjCache, tables, jQobj, tmbody, outerHolder, innerHolder;
            if (!Rgen.Global.isDefaultModalAvailable) {
                $(props.popupContainer).append(props.popupHtml);
                Rgen.Global.Add({
                    isDefaultModalAvailable: true
                });
            }


            props.popup = $(classes.id);
            props.popupInner = props.popup.children('div').children('div');

            if (content !== undefined) {
                props.popupInner.children(classes.title).css('border-bottom-style', 'solid').children('h4').fadeIn(Rgen.Config.Animation.Fast).text(title);
            } else {
                content = title;
                props.popupInner.children(classes.title).children('h4').fadeOut(Rgen.Config.Animation.Fast).parent().css('border-bottom-style', 'none');
            }

            if (props.popup.is(':visible')) {

                props.popupInner.children(classes.body).slideUp(Rgen.Config.Animation.Fast, function () {
                    $(this).html('').append($(content).children('.tm-body').clone(true)).slideDown(Rgen.Config.Animation.Fast);
                    props.popupInner.children(classes.footer).fadeOut(Rgen.Config.Animation.Fast, function () {
                        $(this).html('').append($(content).children('.tm-footer').clone(true)).fadeIn(Rgen.Config.Animation.Fast);
                    });
                });

            } else {

                props.popupInner.children(classes.body).html('').append($(content).children('.tm-body').clone(true));
                props.popupInner.children(classes.footer).html('').append($(content).children('.tm-footer').clone(true));


                $(classes.id).on('hidden.bs.modal', function () {
                    //Modal Close Event Fired !
                    //  collapseMoreInfo();
                    Rgen.Plugins.CollapseMoreTableInfo();

                    if (opt && opt.OuterHolder !== undefined) {

                        outerHolder = opt.OuterHolder;
                        $(jqObjCache).appendTo("." + outerHolder);
                        $("." + innerHolder).empty();

                    } else {
                        $(jqObjCache).appendTo(".ty-PopTable-outer-holder");
                        $(".ty-PopTable-inner-holder").empty();
                    }


                });

            }

            //Pop Table Setup
            if ((opt && opt.PopTable !== "") || null) {


                tmbody = $(".tm-body");
                tables = opt.PopTable;


                if (tables instanceof jQuery) {
                    jQobj = $(tables);
                } else if (tables.indexOf('#') > -1 || tables.indexOf('.') > -1) {
                    jQobj = $(tables);
                } else if (typeof tables === 'string' || tables instanceof String) {
                    jQobj = $("#" + tables);
                }
                jqObjCache = jQobj;

                if (opt && opt.InnerHolder !== undefined) {

                    innerHolder = opt.InnerHolder;
                    jQobj.appendTo("." + innerHolder);
                    jQobj.show();

                } else {

                    jQobj.appendTo(".ty-PopTable-inner-holder");
                    jQobj.show();

                }

            }

            props.popup.modal(opt);

        }


        return {
            Init: function (options) {
                privateBaseInit(options);
            }
        };
    },

    /*Basic Popup*/
    Popup: function (modal, options) {
        "use strict";

        var modalelm;

        if (modal !== undefined && modal !== "") {

            modalelm = $(modal);
            try {


                if (options === undefined || options.backclick === undefined) {
                    modalelm.modal();

                } else {

                    if (options.backclick === true) {
                        modalelm.modal();

                    }

                    if (options.backclick === false) {
                        modalelm.modal({
                            backdrop: "static",
                            keyboard: false
                        });

                    }

                    //Callback
                    if (options.callback !== undefined) {

                        options.callback();
                    }


                }


            } catch (e) {
                Rgen.Util.log("Exception:" + e, "Error");
            }


        } else {
            Rgen.Util.log("Popup:: Please Define the modal parameter!", "error");
        }

    },

    /*Browser Detection Plugin*/
    GetBrowser: function () {
        "use strict";
        var browser = {},
            sUsrAg = navigator.userAgent;

        if (sUsrAg.indexOf("Chrome") > -1) {
            browser.name = "Chrome";
            browser.version = navigator.appVersion;
        } else if (sUsrAg.indexOf("Safari") > -1) {
            browser.name = "Safari";
            browser.version = navigator.appVersion;
        } else if (sUsrAg.indexOf("Opera") > -1) {
            browser.name = "Opera";
            browser.version = navigator.appVersion;
        } else if (sUsrAg.indexOf("Firefox") > -1) {
            browser.name = "Firefox";
            browser.version = navigator.appVersion;
        } else if (sUsrAg.indexOf("MSIE") > -1) {
            browser.name = "IE";
        }

        var ie9 = document.createElement("ie9"),
            ie8 = document.createElement("ie8"),
            ie7 = document.createElement("ie7"),
            ie10 = document.createElement("ie10"),
            ie11 = document.createElement("ie11");

        ie7.innerHTML = "<!--[if IE 7]><i></i><![endif]-->";
        ie8.innerHTML = "<!--[if IE 8]><i></i><![endif]-->";
        ie9.innerHTML = "<!--[if IE 9]><i></i><![endif]-->";
        ie10.innerHTML = "<!--[if IE 10]><i></i><![endif]-->";
        ie11.innerHTML = "<!--[if IE 11]><i></i><![endif]-->";

        var isIe7 = (ie7.getElementsByTagName("i").length == 1);
        var isIe8 = (ie8.getElementsByTagName("i").length == 1);
        var isIe9 = (ie9.getElementsByTagName("i").length == 1);
        var isIe10 = (ie10.getElementsByTagName("i").length == 1);
        var isIe11 = (ie11.getElementsByTagName("i").length == 1);

        if (isIe7) {
            browser.version = "IE7";
        }
        if (isIe8) {
            browser.version = "IE8";
        }
        if (isIe9) {
            browser.version = "IE9";
        }
        if (isIe10) {
            browser.version = "IE10";
        }
        if (isIe11) {
            browser.version = "IE11";
        }

        return browser;


    },

    /*Extended jQuery UI DatePicker Plugin*/
    DatePicker: {

        ScriptLoad: 'static',

        /*Initialize DatePicker On Page Load*/
        Init: function () {

            //Get Default Language From Browser
            var browserLanguage = window.navigator.userLanguage || window.navigator.language,
                CurrentLanguage;

            if (Rgen.Plugins.DatePicker.ScriptLoad == "ajax") {

                var localeScript = "DatePicker/languages/datepicker-" + browserLanguage + ".js";
                //load the localized js datepicker script
                $.ajax({
                    type: "GET",
                    url: localeScript,
                    dataType: "script",
                    success: function (data, textStatus, jqXHR) {
                        //Default Date Based on Browser Local Language
                        if (typeof jQuery.ui !== undefined) {
                            $.datepicker.setDefaults($.extend($.datepicker.regional[browserLanguage]));
                            Rgen.Plugins.DatePicker.After();
                        } else {

                            Rgen.Util.log('DatePicker:: jQuery UI Is Not Available', 'warning');

                        }
                    }
                });

            } else {

                if (typeof jQuery.ui !== undefined) {
                    $.datepicker.setDefaults($.extend($.datepicker.regional[browserLanguage]));


                    Rgen.Plugins.DatePicker.After();


                } else {

                    Rgen.Util.log('DatePicker:: jQuery UI Is Not Available', 'warning');
                }

            }
        },

        SetLanguage: function (lang) {

            if (Rgen.Plugins.DatePicker.ScriptLoad == "ajax") {
                var localeScript = "DatePicker/languages/datepicker-" + lang + ".js";
                //load the localized js datepicker script
                $.ajax({
                    type: "GET",
                    url: localeScript,
                    dataType: "script",
                    success: function (data, textStatus, jqXHR) {
                        //Default Date Based on Browser Local Language
                        if (typeof jQuery.ui !== 'undefined') {
                            $.datepicker.setDefaults($.extend($.datepicker.regional[lang]));
                            Rgen.Plugins.DatePicker.After();
                        }
                    }
                });

            } else {
                if (typeof jQuery.ui !== 'undefined') {
                    $.datepicker.setDefaults($.extend($.datepicker.regional[lang]));
                    Rgen.Plugins.DatePicker.After();
                } else {

                    Rgen.Util.log('DatePicker:: jQuery UI Is Not Available', 'warning');
                }
            }


        },
        After: function () {
            var format = "dd/mm/yyyy";
            if (typeof jQuery.ui !== 'undefined') {

                var y2format = 'yy',
                    y4format = 'yyyy',
                    matchY2 = new RegExp('\\b' + y2format + '\\b'),
                    matchY4 = new RegExp('\\b' + y4format + '\\b');

                if (matchY2.test(format)) {
                    var newFormat = format.split("yy").join("y");

                    $(".hasDatepicker").attr("placeholder", format).datepicker();
                    $(".hasDatepicker").datepicker("option", "dateFormat", newFormat);
                } else if (matchY4.test(format)) {
                    var newFormat = format.split("yyyy").join("yy");

                    $(".hasDatepicker").attr("placeholder", format).datepicker();
                    $(".hasDatepicker").datepicker("option", "dateFormat", newFormat);
                } else {
                    $(".hasDatepicker").attr("placeholder", format).datepicker();
                    $(".hasDatepicker").datepicker("option", "dateFormat", format);
                }

            }
        },
        GetDateFormat: function () {
            "use strict";
            var dateFormat = "YYYY-MM-DD";
            var lang = window.navigator.userLanguage || window.navigator.language;

            if (lang != undefined) {
                try {
                    dateFormat = $.datepicker.regional[lang].dateFormat;
                } catch (e) {

                    Rgen.Util.log('Localization Files not available !', 'warning');
                }
            }

            return dateFormat;

        }

    },

    /*Responsive Table Plugin*/
    ResponsiveTable: function (wrapper, table) {

        "use strict";

        var tableMain = $(table);

        function privateInit() {

            $(window).load(function () {
                ResTable(table);
                setTimeout(function () {
                    ResTable(table);
                }, 500);

            });

            $(window).resize(function () {
                ResTable(table);
                setTimeout(function () {
                    ResTable(table);
                }, 500);


            });

            $(document).ajaxComplete(function () {
                ResTable(table);
                setTimeout(function () {
                    ResTable(table);
                }, 500);

            });

        }

        function ResTable(table) {


            if (table != "" && table != undefined && wrapper != "" && wrapper != undefined) {

                var tableCached = $(table),
                    height = $(window).height(),
                    width = $(window).width(),
                    footablewidth = $(table).width();

                if (width < footablewidth) {

                    var dynwidth = width - 70;

                    $(wrapper).css("width", dynwidth);
                    $(wrapper).css("overflow-x", "auto");

                } else {

                    $(wrapper).css("width", "100%");


                    $(wrapper).css("overflow-x", "auto");
                }


            } else {
                Rgen.Util.log('ResponsiveTable:: Please check the wrapper and table', 'warning');
            }


        }

        return {

            Init: function () {

                privateInit();
            },

            Fire: function (wrapper, table) {


                ResTable(table);


            }
        };


    },


});