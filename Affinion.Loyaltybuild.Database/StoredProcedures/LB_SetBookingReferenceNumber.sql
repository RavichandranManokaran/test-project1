﻿CREATE PROCEDURE [dbo].[LB_SetBookingReferenceNumber]
	@OrderLineId as int,
	@OrderReference as varchar(50),
	@CustomerRefernce as varchar(50)
AS
BEGIN
	UPDATE [dbo].[LB_RoomReservation]
	SET [OrderReference] = @OrderReference,[CustomerReference]=@CustomerRefernce
	WHERE OrderLineId = @OrderLineId
END