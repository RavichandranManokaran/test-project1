

/****** Object:  StoredProcedure [dbo].[LB_BB_getBedBankPropertyMarginForCache]    Script Date: 7/19/2016 7:24:55 PM ******/

---- ====================================================
---- Description: RETURNS MARGIN FOR A PROPERTY BASED ON AVAILABILITY SOURCE
---- Written: RICHARD
---- Created: 2016.04.19
---- ====================================================
CREATE PROCEDURE [dbo].[LB_BB_getBedBankPropertyMarginForCache]
      
      
      --Return parameters
      
      @po_returnstatus INT OUTPUT,
      @po_returnmsg VARCHAR(200) OUTPUT

     
AS
set nocount on;
BEGIN

      --Assume transaction will fail, set the return variable to -1.
      SET @po_returnstatus = -1;
      SET @po_returnmsg = null;

      
      
      BEGIN TRY
      SELECT availabilityProviderRecordID PropertyID, LB_BB_provider.ID ProviderID, LB_BB_provider.lbProviderID LBProviderID, LB_BB_providerMargin.ID as MarginID, LB_BB_providerMargin.marginPercentage,DateFrom as DateFromCounter,Dateto DatetoCounter,DATEADD(d,datefrom,'01/01/1990') as DateFrom, DATEADD(d,dateto,'01/01/1990') DateTo
      FROM dbo.LB_BB_provider
      INNER JOIN LB_BB_providerMargin ON LB_BB_provider.ID = LB_BB_providerMargin._providerid
      WHERE availabilityProviderID = 1    
      AND DATEDIFF(d,'01/01/1990',GETDATE()) between datefrom and dateto
      AND isactive = 1
      Order by availabilityProviderRecordID,DateFrom,DateTo,marginPercentage
            
    END TRY 
      BEGIN CATCH   
            SET @po_returnstatus = error_number();
            SET @po_returnmsg = error_message();
            RETURN;
      END CATCH
      
      SET @po_returnstatus = 0;
      SET @po_returnmsg = 'Completed successfully.';  
END








 
	
		








GO

