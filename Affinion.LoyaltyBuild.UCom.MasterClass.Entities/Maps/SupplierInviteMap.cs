﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce;
using UCommerce.EntitiesV2.Maps;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{

    public class SupplierInviteTargetMap : BaseClassMap<SupplierInvite>
    {
        // Methods
        public SupplierInviteTargetMap()
        {
            base.Table("LB_SupplierInvite");
            base.Id(x => x.SupplierInviteId, "SupplierInviteId").GeneratedBy.Identity();
            Map(x => x.Name).Not.Nullable();
           // Map(x => x.CategoryGuid).Not.Nullable();            
            Map(x => x.IsSubscribe).Not.Nullable();
            Map(x => x.CampaignItem).Not.Nullable();
            Map(x => x.Sku).Not.Nullable();
            Map(x => x.Offerinformation).Nullable();
        }
    }
}

