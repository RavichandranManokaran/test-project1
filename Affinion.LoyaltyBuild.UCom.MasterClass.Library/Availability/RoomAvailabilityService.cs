﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using model = Affinion.LoyaltyBuild.Model.Pricing;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Availability;
using Affinion.LoyaltyBuild.UCom.Common.Constants;
using Affinion.LoyaltyBuild.UCom.Common.Extension;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Sitecore.Data.Items;
using NHibernate.Linq;
using Newtonsoft.Json.Linq;
using Affinion.LoyaltyBuild.Audit;
using Affinion.LoyaltyBuild.Common;
using Sitecore.Data;
using Affinion.LoyaltyBuild.Common.Utilities;
using Newtonsoft.Json;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using Affinion.LoyaltyBuild.Common.Exceptions;


/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Availability
{
    public class DateRange
    {
        #region Property
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        #endregion
    }

    /// <summary>
    /// Service that provides methods to perform all RoomAvailabilityService
    /// </summary>
    public class RoomAvailabilityService : IRoomAvailabilityService
    {

        public static string GetuCommerceConnetionString()
        {
            try
            {
                return (ConfigurationManager.ConnectionStrings["UCommerce"].ConnectionString.Length != 0) ? ConfigurationManager.ConnectionStrings["UCommerce"].ConnectionString : string.Empty;
                //DataConnection dataConnection = new DataConnection("ClientPortal_uCommerce");
                //return dataConnection.ConnectionString;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// FindRoomAvailability 
        /// </summary>
        /// <param name="providerId">providerId</param>
        ///<param name="productSku">productSku</param>
        ///<param name="startDate">startDate</param>
        ///<param name="endDate">endDate</param>
        ///<param name="includeOfferDetail">includeOfferDetail</param>
        ///<param name="campaignItemId">campaignItemId</param>
        /// <returns><list>RoomAvailabilityDetail</list>Object of RoomAvailabilityDetail</returns>
        public List<Model.ProviderAvialbilityData> FindRoomAndOfferAvailabiltyByProvider(string providerId, int campaignItemId, DateTime startDate, DateTime endDate)
        {
            List<Model.ProviderAvialbilityData> manageAvailability = new List<Model.ProviderAvialbilityData>();
            // Category providerDetail = Category.All().Where(x => x.Guid == Guid.Parse(providerId)).FirstOrDefault();
            Product providerDetail = Product.All().Where(x => x.Sku == providerId).FirstOrDefault();
            if (providerDetail != null)
            {
                List<Product> providerProduct = providerDetail.Variants.Where(x => !x.ProductProperties.Any(y => y.ProductDefinitionField.Name.Equals(ProductDefinationVairantConstant.RelatedSKU) && y.Value == "True")
                    && !x.ProductProperties.Any(y => y.ProductDefinitionField.Name.Equals(ProductDefinationVairantConstant.IsUnlimited) && y.Value == "True")).ToList();
                //string categoryId = providerDetail.Guid.ToString();
                string categoryId = providerDetail.Sku;
                List<RoomAvailability> availability = RoomAvailabilityByProvider(categoryId, startDate.ConvetDatetoDateCounter(), endDate.ConvetDatetoDateCounter(), string.Empty);
                List<OfferRoomAvailability> offerAvailability = OfferRoomAvailabilityByProvider(categoryId, campaignItemId, startDate.ConvetDatetoDateCounter(), endDate.ConvetDatetoDateCounter(), string.Empty);
                if (campaignItemId != 0)
                {
                    offerAvailability = offerAvailability.Where(x => x.CampaignItem == campaignItemId).ToList();
                }
                List<Product> finalProduct = new List<Product>();
                finalProduct.AddRange(providerProduct);
                Campaign cDetail = null;
                if (campaignItemId != 0)
                {
                    CampaignItem cItem = CampaignItem.All().Where(x => x.CampaignItemId == campaignItemId).ToList().FirstOrDefault();
                    if (cItem != null)
                    {
                        cDetail = cItem.Campaign;
                        RoomTypeTarget roomTarget = RoomTypeTarget.All().Where(x => x.CampaignItem.CampaignItemId == cItem.CampaignItemId).FirstOrDefault();
                        if (roomTarget != null)
                        {
                            finalProduct = finalProduct.Where(x => x.ProductProperties != null && x.ProductProperties.Any(y => y.ProductDefinitionField.Name.Equals(ProductDefinationConstant.RoomType, StringComparison.InvariantCultureIgnoreCase) && y.Value.Equals(roomTarget.RoomType, StringComparison.InvariantCultureIgnoreCase))).ToList();
                            if (finalProduct == null || finalProduct.Count == 0)
                                return new List<ProviderAvialbilityData>();
                        }

                    }

                }
                List<DateRange> dateRange = this.CrateDateRange(startDate, endDate);
                List<ProductDetail> tmpDetail = new List<ProductDetail>();
                List<ProductDetail> pDetail = new List<ProductDetail>();
                foreach (var item in finalProduct)
                {
                    ProductDetail room = new ProductDetail();
                    room.ProductId = item.ProductId;
                    room.ProductSKU = item.Sku;
                    room.VariantSku = item.VariantSku;
                    room.ProductName = item.Name;
                    room.ProductType = GetProductType(item, true);

                    ProductProperty adultProperty = item.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name.Equals(ProductDefinationConstant.Adult));
                    int selectValue = 0;
                    if (adultProperty != null && int.TryParse(adultProperty.Value, out selectValue))
                    {
                        room.NumberOfAdult = Convert.ToInt32(adultProperty.Value);
                    }
                    ProductProperty childProperty = item.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name.Equals(ProductDefinationConstant.Child));
                    if (childProperty != null && int.TryParse(adultProperty.Value, out selectValue))
                    {
                        room.NumberOfChild = Convert.ToInt32(childProperty.Value);
                    }
                    if (room.ProductType == 1)
                    {
                        room.PriceBand = GetPriceBand(item.VariantSku, providerId);
                    }
                    //Fetch Add On Price
                    else if (room.ProductType == 2 || room.ProductType == -1)
                    {
                        List<decimal> data = GetAddOnPrice(item.VariantSku, item.Sku);
                        room.ProductPrice = data[0];
                        room.PackagePrice = data[1];
                    }
                    //Fetch Package Standalone Price
                    else if (room.ProductType == 3)
                    {
                        ProductProperty relatedSKU = item.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name.Equals(ProductDefinationVairantConstant.RelatedSKU));
                        ProductProperty relatedAddOnSKU = item.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name.Equals(ProductDefinationVairantConstant.RelatedAddOnSKU));
                        if (relatedSKU != null && !string.IsNullOrEmpty(relatedSKU.Value))
                        {
                            room.RelatedSKU = relatedSKU.Value;
                        }
                        if (relatedAddOnSKU != null && !string.IsNullOrEmpty(relatedAddOnSKU.Value))
                        {
                            room.RelatedAddOnSKU = relatedAddOnSKU.Value;
                        }

                    }
                    tmpDetail.Add(room);
                }

                if (tmpDetail != null && tmpDetail.Any())
                {
                    if (tmpDetail.Any(x => x.ProductType == 1))
                    {
                        pDetail.AddRange(tmpDetail.Where(x => x.ProductType == 1));
                    }
                    if (tmpDetail.Any(x => x.ProductType == 3))
                    {
                        pDetail.AddRange(tmpDetail.Where(x => x.ProductType == 3));
                    }
                    if (tmpDetail.Any(x => x.ProductType == 2))
                    {
                        pDetail.AddRange(tmpDetail.Where(x => x.ProductType == 2));
                    }
                }


                foreach (var item in dateRange)
                {
                    ProviderAvialbilityData monthavailability = new ProviderAvialbilityData();
                    monthavailability.StatDate = item.StartDate;
                    monthavailability.EndDate = item.EndDate;
                    monthavailability.Month = item.StartDate.GetMonthFromJulianFixed();
                    // string Addn= null;
                    foreach (var rooms in pDetail)
                    {
                        bool isSetData = false;
                        if (!string.IsNullOrEmpty(rooms.VariantSku))
                        {
                            decimal AddonPrice = 0;
                            RoomDetail room = new RoomDetail();
                            room.Name = rooms.ProductName;
                            room.ProductId = rooms.ProductId.ToString();
                            room.Month = item.StartDate.Month;
                            room.ProductSku = string.Format("{0}@{1}", providerDetail.Sku, rooms.VariantSku);
                            room.ProductTye = rooms.ProductType;
                            room.NumberOfAdult = rooms.NumberOfAdult;
                            room.NumberOfChild = rooms.NumberOfChild;
                            if (rooms.ProductType == 1)
                            {
                                string dependsOn = string.Empty;
                                foreach (var dSku in tmpDetail.Where(x => !string.IsNullOrEmpty(x.RelatedSKU)))
                                {
                                    List<string> allSKU = dSku.RelatedSKU.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Where(x => !string.IsNullOrWhiteSpace(x)).Select(s => s.Trim()).ToList();
                                    if (allSKU.Any(x => x == rooms.VariantSku))
                                    {
                                        dependsOn += "," + string.Format("{0}@{1}", dSku.ProductSKU, dSku.VariantSku);
                                    }
                                }
                                room.DependsOn = string.Join(",", dependsOn.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Where(x => !string.IsNullOrWhiteSpace(x)).Select(s => s.Trim()));
                            }
                            else if (rooms.ProductType == 3)
                            {
                                List<string> allSKU = rooms.RelatedAddOnSKU.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Where(x => !string.IsNullOrWhiteSpace(x)).Select(s => s.Trim()).ToList();
                                foreach (var sku in allSKU)
                                {

                                    ProductDetail data = tmpDetail.FirstOrDefault(x => x.VariantSku == sku);
                                    if (data != null)
                                    {
                                        AddonPrice += data.PackagePrice;
                                    }
                                }
                                List<string> allRoomSKU = rooms.RelatedSKU.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Where(x => !string.IsNullOrWhiteSpace(x)).Select(s => s.Trim()).ToList();
                                string dependsOn = string.Empty;
                                foreach (var dSku in allRoomSKU)
                                {
                                    ProductDetail data = tmpDetail.FirstOrDefault(x => x.VariantSku == dSku && x.ProductType == 1);
                                    if (data != null)
                                    {
                                        dependsOn += "," + string.Format("{0}@{1}", data.ProductSKU, data.VariantSku);
                                    }
                                }
                                room.DependsOn = string.Join(",", dependsOn.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Where(x => !string.IsNullOrWhiteSpace(x)).Select(s => s.Trim()));
                            }

                            room.AdditionalPrice = AddonPrice;
                            List<Model.RoomAvailabilityByDate> daywiseAvailability = new List<Model.RoomAvailabilityByDate>();
                            try
                            {
                                //GetPackagePrice(rooms.VariantSku.ToString(), providerId);

                                for (int i = 0; i <= (item.EndDate.Day - item.StartDate.Day); i++)
                                {
                                    bool isNonEditable = false;
                                    if (cDetail != null)
                                    {
                                        isNonEditable = (!(item.StartDate.AddDays(i) >= cDetail.StartsOn));
                                        if (!isNonEditable)
                                        {
                                            isNonEditable = (!(item.StartDate.AddDays(i) <= cDetail.EndsOn));
                                        }
                                    }
                                    if (!isNonEditable)
                                    {
                                        isNonEditable = (!(item.StartDate.AddDays(i) >= startDate && item.StartDate.AddDays(i) <= endDate));
                                    }

                                    RoomAvailability dataAvailability = availability.FirstOrDefault(x => x.DateCounter == item.StartDate.AddDays(i).ConvetDatetoDateCounter() && x.VariantSku.Trim() == rooms.VariantSku) ?? new RoomAvailability();
                                    OfferRoomAvailability offerAvailabilityData = offerAvailability.FirstOrDefault(x => x.VariantSku.Trim() == rooms.VariantSku.Trim() && x.DateCounter == item.StartDate.AddDays(i).ConvetDatetoDateCounter()) ?? new OfferRoomAvailability();
                                    //Ignore is restricted !x.IsRestrictedForSale && 

                                    bool hasOffer = offerAvailability.Any(x => !x.IsRestrictedForSale && x.NumberAllocated > 0 && x.VariantSku.Trim() == rooms.VariantSku.Trim() && x.DateCounter == item.StartDate.AddDays(i).ConvetDatetoDateCounter());
                                    daywiseAvailability.Add(new Model.RoomAvailabilityByDate()
                                    {
                                        flag = room.ProductTye,
                                        Price = rooms.ProductType == 2 ? rooms.ProductPrice : dataAvailability.Price,
                                        // Addons=Addn,
                                        //AddonPrice = AddonPrice,
                                        PackagePrice = dataAvailability.PackagePrice,
                                        PriceBand = dataAvailability.PriceBand,
                                        ddPrice = rooms.PriceBand ?? new List<PriceBand>(),
                                        AvailabelRoom = dataAvailability.NumberAllocated,
                                        BlockedRoom = dataAvailability.NumberBooked,
                                        AvialabilityDate = dataAvailability.RoomAvailibilityID == default(Guid) ? (item.StartDate.AddDays(i).ConvetDatetoDateCounter().GetDateFromDateCount()) : dataAvailability.DateCounter.GetDateFromDateCount(),
                                        Id = dataAvailability.RoomAvailibilityID.ToString(),
                                        DateCounter = item.StartDate.AddDays(i).ConvetDatetoDateCounter(),
                                        IsNewRecord = dataAvailability.RoomAvailibilityID == default(Guid),
                                        IsNewOfferRecord = offerAvailabilityData.OfferAvailibilityID == default(Guid),
                                        OfferId = campaignItemId,
                                        ProductSku = room.ProductSku, // !string.IsNullOrEmpty(dataAvailability.VariantSku) ? dataAvailability.VariantSku.Trim() : string.Empty,
                                        HasOffer = hasOffer,
                                        OfferAvailabelRoom = offerAvailabilityData.NumberAllocated,
                                        OfferBlockedRoom = offerAvailabilityData.NumberBooked,
                                        IsCloseOut = dataAvailability.IsCloseOut,
                                        IsNonEditable = isNonEditable
                                    });

                                }
                                room.AvailabilityData = daywiseAvailability;
                                monthavailability.RoomAvailability.Add(room);
                            }
                            catch (Exception er)
                            {

                            }

                        }
                    }
                    manageAvailability.Add(monthavailability);
                }
            }
            return manageAvailability;
            //  throw new NotImplementedException();
        }

        /// <summary>
        /// Return Add ON Price on  0 Position (Product Price)  , 1 Position (Package Price)
        /// </summary>
        /// <param name="variantSKU"></param>
        /// <param name="SKU"></param>
        /// <returns></returns>
        public List<decimal> GetAddOnPrice(string variantSKU, string SKU)
        {
            List<decimal> plist = new List<decimal>() { 0, 0 };
            Item[] addOn = Sitecore.Context.Database.SelectItems(string.Format("fast:/sitecore/content/#admin-portal#/#supplier-setup#//*[@@TemplateName='Supplier Addon' and @SKU='{0}']", variantSKU));
            if (addOn != null && addOn.Length > 0)
            {
                decimal parseValue = 0;
                if (addOn[0].Fields["Price"] != null && decimal.TryParse(addOn[0].Fields["Price"].Value, out parseValue))
                {
                    parseValue = Math.Round(parseValue, 2);
                    plist[0] = parseValue;
                }
                if (addOn[0].Fields["PackagePrice"] != null && decimal.TryParse(addOn[0].Fields["PackagePrice"].Value, out parseValue))
                {
                    parseValue = Math.Round(parseValue, 2);
                    plist[1] = parseValue;
                }
            }
            return plist;
        }

        /// <summary>
        /// Get Package Additional Price
        /// </summary>
        /// <param name="variantSKU"></param>
        /// <param name="SKU"></param>
        /// <returns></returns>
        public decimal GetPackageAdditionalPrice(string variantSKU, string SKU)
        {
            List<PriceBand> plist = new List<PriceBand>();
           // plist.Add(new PriceBand() { Price = 0, Name = " " });

            Item[] hotels = Sitecore.Context.Database.SelectItems(string.Format("fast:/sitecore/content/#admin-portal#/#supplier-setup#//*[@@TemplateName='SupplierDetails' and @SKU='{0}']", SKU));
            if (hotels != null && hotels.Length > 0)
            {

                Item[] Rooms = Sitecore.Context.Database.SelectItems(string.Format("fast:/sitecore/content/#admin-portal#/#supplier-setup#//*[@@TemplateName='Supplier Room' and @SKU='{0}']", variantSKU));
                if (Rooms != null && Rooms.Length > 0)
                {

                }
            }
            return 0;
            //string pPrice = null;
            //Item[] hotels = Sitecore.Context.Database.SelectItems(string.Format("fast:/sitecore/content/#admin-portal#/#supplier-setup#//*[@@TemplateName='SupplierDetails']"));
            //if (hotels != null && hotels.Length > 0)
            //{
            //    foreach (var h in hotels)
            //    {
            //        if (h.Fields["SKU"].Value == providerId)
            //        {
            //            Item[] package = Sitecore.Context.Database.SelectItems(string.Format("fast:/sitecore/content/#admin-portal#/#supplier-setup#//*[@@TemplateName='Package Folder']"));
            //            if (package != null && package.Length > 0)
            //            {
            //                foreach (Item p in package)
            //                {
            //                    if (h.ID == p.ParentID)
            //                    {
            //                        var roomtype = p.Children.InnerChildren;
            //                        foreach (Item r in roomtype)
            //                        {
            //                            var addon = (r.Fields["Addon"].Value).Split('|');
            //                            Item[] Addon = Sitecore.Context.Database.SelectItems(string.Format("fast:/sitecore/content/#admin-portal#/#supplier-setup#//*[@@TemplateName='Addon Folder']"));
            //                            if (Addon != null && Addon.Length > 0)
            //                            {
            //                                foreach (var a in addon)
            //                                {
            //                                    Item adon = Sitecore.Context.Database.GetItem(a);
            //                                    if (adon.Fields["SKU"].Value == RoomType)
            //                                    {
            //                                        pPrice = adon.Fields["PackagePrice"].Value;
            //                                    }
            //                                }
            //                            }
            //                        }
            //                        //var AddonChild = add.Children.InnerChildren;
            //                        //foreach (Item a_child in AddonChild)
            //                        //{
            //                        //    if (addon == a_child.ID.ToString())
            //                        //    {
            //                        //        pPrice = a_child.Fields["PackagePrice"].Value;
            //                        //    }
            //                        //}
            //                    }
            //                }
            //            }
            //        }
            //    }
            //}
            ////pPrice = 0;
            //return pPrice;
        }

        /// <summary>
        /// Get Price Band
        /// </summary>
        /// <param name="variantSKU"></param>
        /// <param name="SKU"></param>
        /// <returns></returns>
        public List<PriceBand> GetPriceBand(string variantSKU, string SKU)
        {
            List<PriceBand> plist = new List<PriceBand>();
            // plist.Add(new PriceBand() { Price = 0, Name = " " });

            Item[] hotels = Sitecore.Context.Database.SelectItems(string.Format("fast:/sitecore/content/#admin-portal#/#supplier-setup#//*[@@TemplateName='SupplierDetails' and @SKU='{0}']", SKU));
            if (hotels != null && hotels.Length > 0)
            {

                Item[] Rooms = Sitecore.Context.Database.SelectItems(string.Format("fast:/sitecore/content/#admin-portal#/#supplier-setup#//*[@@TemplateName='Supplier Room' and @SKU='{0}']", variantSKU));
                if (Rooms != null && Rooms.Length > 0)
                {
                    foreach (Item i in Rooms)
                    {
                        if (i.Fields["SKU"].Value == variantSKU)
                        {
                            if (i.Fields["PriceBand"] != null && !string.IsNullOrEmpty((i.Fields["PriceBand"].Value)))
                            {
                                var PBand = (i.Fields["PriceBand"].Value).Split('|');
                                foreach (var pb in PBand)
                                {
                                    decimal productprice = 0;
                                    Item band = Sitecore.Context.Database.GetItem(pb);
                                    if (!string.IsNullOrWhiteSpace(SitecoreFieldsHelper.GetItemFieldValue(band, "Rate")))
                                    {
                                        Decimal.TryParse(SitecoreFieldsHelper.GetItemFieldValue(band, "Rate"), out productprice);
                                        var commision = CalculateCommisionForRoom(band, productprice);
                                        plist.Add(new PriceBand() { Name = band.DisplayName, Commision = commision.Day1, Price = Math.Round(productprice, 2), Rate = Math.Round(productprice) - commision.Day1, ItemId = band.ID.Guid.ToString() });
                                    }
                                }
                            }
                        }
                    }


                }
            }

            return plist;
        }

        /// <summary>
        /// Get Product By Category
        /// </summary>
        /// <param name="providerDetail"></param>
        /// <param name="ignoreAddOn"></param>
        /// <returns></returns>
        private List<Product> GetProductByCategory(Category providerDetail, bool ignoreAddOn = true)
        {
            List<Product> providerProduct = Product.GetForCategory(providerDetail).ToList();
            if (ignoreAddOn)
            {
                providerProduct = providerProduct.Where(x => x.ProductDefinition.Name.Equals(ProductDefinationConstant.HotelRoom)).ToList();
            }
            return providerProduct;
        }

        /// <summary>
        /// Crate Date Range
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        private List<DateRange> CrateDateRange(DateTime fromDate, DateTime toDate)
        {
            List<DateRange> dateRanges = new List<DateRange>();

            int startMonth = fromDate.Month;
            int endMonth = toDate.Month;
            int startYear = fromDate.Year;
            dateRanges = new List<DateRange>();
            if (startMonth == endMonth && fromDate.Year == toDate.Year)
            {
                DateTime startDate = new DateTime(fromDate.Year, fromDate.Month, 1);
                DateTime endDate = startDate.AddMonths(1).AddDays(-1);
                dateRanges.Add(new DateRange() { StartDate = startDate.ConvertDateToString().ParseFormatDateTime(), EndDate = endDate.ConvertDateToString().ParseFormatDateTime() });
            }
            else
            {
                int i = 0;
                int monthdiff = ((toDate.Year - fromDate.Year) * 12) + toDate.Month - fromDate.Month;
                DateRange rangs;
                DateTime startUpDate = new DateTime(startYear, fromDate.Month, 1);
                do
                {
                    rangs = new DateRange();
                    DateTime startDate = startUpDate.AddMonths(i);
                    DateTime endDate = startDate.AddMonths(1).AddDays(-1);
                    //if (startDate < fromDate)
                    //{
                    //    rangs.StartDate = fromDate;
                    //}
                    //else
                    {
                        rangs.StartDate = startDate;
                    }
                    //if (endDate > toDate)
                    //{
                    //    rangs.EndDate = toDate;
                    //}
                    //else
                    {
                        rangs.EndDate = endDate;
                    }
                    dateRanges.Add(rangs);
                    i++;
                } while (i <= monthdiff);
            }
            return dateRanges;

        }

        /// <summary>
        /// Get RoomAvailability By Provider
        /// </summary>
        /// <param name="providerId"></param>
        /// <param name="startDateCounter"></param>
        /// <param name="endDateCounter"></param>
        /// <param name="productSku"></param>
        /// <returns></returns>
        public List<RoomAvailabilityByDate> GetRoomAvailabilityByProvider(string providerId, int startDateCounter, int endDateCounter, string productSku)
        {
            List<RoomAvailability> data = RoomAvailabilityByProvider(providerId, startDateCounter, endDateCounter, string.Empty);
            List<RoomAvailabilityByDate> roomData = new List<RoomAvailabilityByDate>();
            foreach (var item in data)
            {
                roomData.Add(new RoomAvailabilityByDate()
                {
                    AvailabelRoom = item.NumberAllocated,
                    BlockedRoom = item.NumberBooked,
                    AvialabilityDate = item.DateCounter.GetDateFromDateCount(),
                    DateCounter = item.DateCounter,
                    PriceBand = item.PriceBand,
                    ProductSku = item.VariantSku
                });
            }
            return roomData;
        }

        /// <summary>
        /// Room Availability ByProvider
        /// </summary>
        /// <param name="providerId"></param>
        /// <param name="startDateCounter"></param>
        /// <param name="endDateCounter"></param>
        /// <param name="productSku"></param>
        /// <returns></returns>
        private List<RoomAvailability> RoomAvailabilityByProvider(string providerId, int startDateCounter, int endDateCounter, string productSku)
        {
            return RoomAvailability.All().Where(x => x.Sku == providerId && x.DateCounter >= startDateCounter && x.DateCounter <= endDateCounter && x.VariantSku.Equals(string.IsNullOrEmpty(productSku) ? x.VariantSku.Trim() : productSku.Trim())).ToList();
        }

        /// <summary>
        /// Offer Room Availability By Provider
        /// </summary>
        /// <param name="providerId"></param>
        /// <param name="campaignItemId"></param>
        /// <param name="startDateCounter"></param>
        /// <param name="endDateCounter"></param>
        /// <param name="productSku"></param>
        /// <returns></returns>
        private List<OfferRoomAvailability> OfferRoomAvailabilityByProvider(string providerId, int campaignItemId, int startDateCounter, int endDateCounter, string productSku)
        {
            List<OfferRoomAvailability> offerRoomAvailability = OfferRoomAvailability.All().Where(x => x.Sku == providerId && x.DateCounter >= startDateCounter && x.DateCounter <= endDateCounter && x.VariantSku.Equals(string.IsNullOrEmpty(productSku) ? x.VariantSku.Trim() : productSku.Trim())).ToList();
            if (campaignItemId != 0)
            {
                offerRoomAvailability = offerRoomAvailability.Where(x => x.CampaignItem == campaignItemId).ToList();
            }
            return offerRoomAvailability;
        }

        /// <summary>
        /// SaveProviderRoomAvailability 
        /// </summary>
        /// <param name="providerId">providerId</param>
        ///<param name="roomAvailability"><list>ChangeRoomAvailabilityByDate</list>roomAvailability</param>
        /// <returns><list>ChangeRoomAvailabilityByDate</list>Object of ChangeRoomAvailabilityByDate</returns>
        public List<ChangeRoomAvailabilityByDate> SaveProviderRoomAvailability(string providerId, List<ChangeRoomAvailabilityByDate> roomAvailability)
        {
            DateTime startDate = roomAvailability.Min(x => x.DateCounter).GetDateFromDateCount();
            DateTime endDate = roomAvailability.Max(x => x.DateCounter).GetDateFromDateCount();
            List<Model.ProviderAvialbilityData> manageAvailability = new List<Model.ProviderAvialbilityData>();
            Product providerDetail = Product.All().Where(x => x.Sku == providerId).FirstOrDefault();

            if (providerDetail != null)
            {
                List<RoomAvailability> availabilityData = this.RoomAvailabilityByProvider(providerDetail.Sku.ToString(), startDate.ConvetDatetoDateCounter(), endDate.ConvetDatetoDateCounter(), string.Empty);
                List<OfferRoomAvailability> offerAvaialbilty = new List<OfferRoomAvailability>();
                if (roomAvailability.Any(x => x.RemoveOffer))
                {
                    offerAvaialbilty = this.OfferRoomAvailabilityByProvider(providerDetail.Sku.ToString(), 0, startDate.ConvetDatetoDateCounter(), endDate.ConvetDatetoDateCounter(), string.Empty);

                }
                foreach (var item in roomAvailability)
                {
                    try
                    {

                        RoomAvailability datewiseAvailability = availabilityData.FirstOrDefault(x => x.VariantSku.Trim().Equals(item.VariantSku.Trim()) && x.DateCounter == item.DateCounter) ?? new RoomAvailability();

                        if (item.AvailableRoom >= datewiseAvailability.NumberBooked)
                        {
                            if (item.RemoveOffer)
                            {
                                foreach (var offer in offerAvaialbilty.Where(x => x.VariantSku.Trim() == item.VariantSku.Trim() && x.DateCounter == item.DateCounter))
                                {
                                    offer.NumberAllocated = offer.NumberBooked;
                                    offer.IsRestrictedForSale = true;
                                    offer.DateLastUpdate = DateTime.Now;
                                    offer.Save();
                                }

                            }

                            datewiseAvailability.Price = item.Price;
                            datewiseAvailability.PackagePrice = item.PackagePrice;
                            datewiseAvailability.PriceBand = string.IsNullOrEmpty(item.PriceBand) ? default(Guid).ToString() : item.PriceBand;
                            datewiseAvailability.NumberAllocated = item.AvailableRoom;
                            datewiseAvailability.Sku = providerDetail.Sku.ToString();
                            datewiseAvailability.VariantSku = item.VariantSku.Trim();
                            datewiseAvailability.IsCloseOut = item.IsCloseOut;
                            datewiseAvailability.IsStopSellOn = false; //Need to confirm for it.
                            datewiseAvailability.DateLastUpdate = DateTime.Now;
                            datewiseAvailability.DateCounter = item.DateCounter;
                            if (string.IsNullOrEmpty(datewiseAvailability.RoomAvailibilityID.ToString()))
                            {
                                datewiseAvailability.DateCreated = DateTime.Now;
                            }
                            datewiseAvailability.DateCreated = DateTime.Now;
                            datewiseAvailability.Save();
                            //update package availability logic goes here
                            UpdatePackageAvailabilityBasedOnRoomAvailability(availabilityData, item, datewiseAvailability);

                            item.RecordUpdate = true;
                        }
                        else
                        {
                            item.RecordUpdate = false;
                        }
                        if (item.RecordUpdate)
                        {

                            //Audit Log
                            RoomAvailabilityAuditLog(datewiseAvailability);

                        }

                    }
                    catch (Exception ex)
                    {
                        item.RecordUpdate = false;
                        Diagnostics.WriteException(DiagnosticsCategory.MasterClassBusinessLogic, ex, this);
                    }
                }
            }
            //Get Updated Record and group by variantsku
            var updatedRecords = roomAvailability.Where(x => x.RecordUpdate);
            var dateCounter = string.Join(",", updatedRecords.Select(y => y.DateCounter));
            var variantSkus = updatedRecords.GroupBy(i => i.VariantSku).Select(y => y.First().VariantSku);
            if (variantSkus != null && !string.IsNullOrWhiteSpace(dateCounter) && variantSkus.Count() > 0)
            {
                foreach (var variantsku in variantSkus)
                {
                    //update related package availability based on availability comparison with room and add ons
                    bool isPackageAvailabilityUpdated = UpdateRelatedPackageAvailability(variantsku, dateCounter);

                }
            }


            return roomAvailability.Where(x => !x.RecordUpdate).ToList();
        }

        /// <summary>
        /// Update Package Availability Based On Room Availability
        /// </summary>
        /// <param name="availabilityData"></param>
        /// <param name="item"></param>
        /// <param name="datewiseAvailability"></param>
        private void UpdatePackageAvailabilityBasedOnRoomAvailability(List<RoomAvailability> availabilityData, ChangeRoomAvailabilityByDate item, RoomAvailability datewiseAvailability)
        {
            var packageProducts = Product.All().Where(i => i.ProductProperties.Any(x => x.ProductDefinitionField.Name.Equals(ProductDefinationVairantConstant.RelatedSKU) && x.Value == item.VariantSku)).ToList();
            if (packageProducts != null)
            {
                foreach (var package in packageProducts)
                {
                    RoomAvailability datewiseAvailabilityForPackage = availabilityData.FirstOrDefault(x => x.VariantSku.Trim().Equals(package.VariantSku.Trim()) && x.DateCounter == item.DateCounter) ?? new RoomAvailability();
                    if (datewiseAvailabilityForPackage != null)
                    {
                        if (item.AvailableRoom >= datewiseAvailabilityForPackage.NumberBooked)
                        {
                            #region add on price calculation
                            // Add On Price calculation
                            decimal addOnPrice = 0;

                            if (datewiseAvailabilityForPackage.RoomAvailibilityID != null && datewiseAvailabilityForPackage.RoomAvailibilityID.Equals(Guid.Empty))
                            {
                                var relatedProducts = new List<string>();
                                var relatedAddOnSkus = package[ProductDefinationVairantConstant.RelatedAddOnSKU];
                                if (relatedAddOnSkus != null && !string.IsNullOrEmpty(relatedAddOnSkus.Value))
                                    relatedProducts.AddRange(relatedAddOnSkus.Value.Split(','));
                                if (relatedProducts != null && relatedProducts.Count > 0)
                                {
                                    foreach (var rProduct in relatedProducts)
                                    {
                                        addOnPrice += GetAddOnPrice(rProduct, item.ProductSku)[1];
                                    }
                                }
                            }
                            else
                            {
                                addOnPrice = datewiseAvailabilityForPackage.Price - datewiseAvailabilityForPackage.PackagePrice;
                            }
                            #endregion

                            datewiseAvailabilityForPackage.Price = datewiseAvailability.Price;
                            datewiseAvailabilityForPackage.PackagePrice = datewiseAvailability.Price + addOnPrice;
                            datewiseAvailabilityForPackage.NumberAllocated = datewiseAvailability.NumberAllocated;
                            datewiseAvailabilityForPackage.Sku = package.Sku.ToString();
                            datewiseAvailabilityForPackage.VariantSku = package.VariantSku.Trim();
                            datewiseAvailabilityForPackage.IsCloseOut = datewiseAvailability.IsCloseOut;
                            datewiseAvailabilityForPackage.IsStopSellOn = false;
                            datewiseAvailabilityForPackage.DateLastUpdate = DateTime.Now;
                            datewiseAvailabilityForPackage.DateCounter = datewiseAvailability.DateCounter;
                            datewiseAvailabilityForPackage.UpdateBy = (Sitecore.Context.User) != null ? Sitecore.Context.User.Name : string.Empty;
                            datewiseAvailabilityForPackage.DateCreated = DateTime.Now;
                            datewiseAvailabilityForPackage.Save();

                        }
                    }
                }
            }
        }

        /// <summary>
        /// SaveProviderOfferAvailability 
        /// </summary>
        /// <param name="providerId">providerId</param>
        ///<param name="ChangeOfferRoomAvailabilityByDate"><list>ChangeOfferRoomAvailabilityByDate</list>ChangeOfferRoomAvailabilityByDate</param>
        /// <returns><list>ChangeOfferRoomAvailabilityByDate</list>Object of ChangeOfferRoomAvailabilityByDate</returns>
        public List<ChangeOfferRoomAvailabilityByDate> SaveProviderOfferAvailability(string providerId, List<ChangeOfferRoomAvailabilityByDate> roomAvailability)
        {
            DateTime startDate = roomAvailability.Min(x => x.DateCounter).GetDateFromDateCount();
            DateTime endDate = roomAvailability.Max(x => x.DateCounter).GetDateFromDateCount();
            List<RoomAvailability> manageAvailability = new List<RoomAvailability>();
            Product providerDetail = Product.All().Where(x => x.Sku == providerId).FirstOrDefault();
            //  Category providerDetail = Category.All().Where(x => x.Guid == Guid.Parse(providerId)).FirstOrDefault();
            if (providerDetail != null && roomAvailability != null && roomAvailability.Any())
            {
                List<OfferRoomAvailability> availabilityData = this.OfferRoomAvailabilityByProvider(providerDetail.Sku.ToString(), roomAvailability.FirstOrDefault().CampaignItemId, startDate.ConvetDatetoDateCounter(), endDate.ConvetDatetoDateCounter(), string.Empty);
                List<ChangeOfferRoomAvailabilityByDate> offerAvaialbilty = new List<ChangeOfferRoomAvailabilityByDate>();
                manageAvailability = this.RoomAvailabilityByProvider(providerId, startDate.ConvetDatetoDateCounter(), endDate.ConvetDatetoDateCounter(), string.Empty);
                foreach (var item in roomAvailability)
                {
                    try
                    {
                        item.ProductSku = item.ProductSku.Trim();
                        OfferRoomAvailability datewiseOfferAvailability = availabilityData.FirstOrDefault(x => x.VariantSku.Trim().Equals(item.VariantSku.Trim()) && x.DateCounter == item.DateCounter) ?? new OfferRoomAvailability();
                        RoomAvailability roomAvailabilityData = manageAvailability.FirstOrDefault(x => x.VariantSku.Trim().Equals(item.VariantSku.Trim()) && x.DateCounter == item.DateCounter) ?? new RoomAvailability();
                        if (
                            ((roomAvailabilityData.NumberAllocated - roomAvailabilityData.NumberBooked) >= (item.AvailableRoom - datewiseOfferAvailability.NumberBooked))
                            && item.AvailableRoom >= datewiseOfferAvailability.NumberBooked)
                        {

                            datewiseOfferAvailability.NumberAllocated = item.AvailableRoom;
                            datewiseOfferAvailability.Sku = providerDetail.Sku.ToString();
                            datewiseOfferAvailability.VariantSku = item.VariantSku.Trim();
                            datewiseOfferAvailability.CampaignItem = item.CampaignItemId;
                            datewiseOfferAvailability.DateLastUpdate = DateTime.Now;
                            datewiseOfferAvailability.DateCounter = item.DateCounter;
                            datewiseOfferAvailability.IsRestrictedForSale = false;
                            if (string.IsNullOrEmpty(datewiseOfferAvailability.OfferAvailibilityID.ToString()) || datewiseOfferAvailability.OfferAvailibilityID == default(Guid))
                            {
                                datewiseOfferAvailability.DateCreated = DateTime.Now;
                            }
                            datewiseOfferAvailability.Save();
                            item.RecordUpdate = true;
                        }
                        else
                        {
                            item.RecordUpdate = false;
                        }
                        if (item.RecordUpdate)
                        {
                            //audit Log
                            OfferAvailabilityAuditLog(datewiseOfferAvailability);
                        }
                    }
                    catch (Exception)
                    {
                        item.RecordUpdate = false;
                    }
                }

            }
            return roomAvailability;
        }


        /// <summary>
        /// GetHotelRoomAndOfferAvailabilityByProvider 
        /// </summary>
        /// <param name="providerId">providerId</param>
        /// <param name="startDate">startDate</param>
        ///  <param name="endDate">endDate</param>
        /// <returns> <list>RoomAndOfferAvaialbityShortage</list>GetHotelRoomAndOfferAvailabilityByProvider</returns>
        public bool ChangeRoomRestriction(string providerId, int campaignItemId, bool isRestricted)
        {
            try
            {
                Product providerDetail = Product.All().Where(x => x.Sku == providerId).FirstOrDefault();
                // Category providerDetail = Category.All().Where(x => x.Guid == Guid.Parse(providerId)).FirstOrDefault();
                List<OfferRoomAvailability> availabilityData = this.OfferRoomAvailabilityByProvider(providerDetail.Sku.ToString(), campaignItemId, 0, Int32.MaxValue, string.Empty);

                if (providerDetail != null && availabilityData != null && availabilityData.Any())
                {
                    foreach (var item in availabilityData)
                    {
                        item.NumberAllocated = item.NumberBooked;
                        item.IsRestrictedForSale = isRestricted;
                        item.Save();
                    }
                    return true;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.MasterClassBusinessLogic, ex, this);
                return false;
            }
        }


        /// <summary>
        /// FindRoomAvailability 
        /// </summary>
        /// <param name="providerId">providerId</param>
        ///<param name="productSku">productSku</param>
        ///<param name="startDate">startDate</param>
        ///<param name="endDate">endDate</param>
        ///<param name="includeOfferDetail">includeOfferDetail</param>
        ///<param name="campaignItemId">campaignItemId</param>
        /// <returns><list>RoomAvailabilityDetail</list>Object of RoomAvailabilityDetail</returns>
        public List<RoomAvailabilityDetail> FindRoomAvailability(string providerId, string productSku, DateTime startDate, DateTime endDate, bool includeOfferDetail = false, int campaignItemId = 0)
        {
            Product providerDetail = Product.All().Where(x => x.Sku == providerId).FirstOrDefault();
            //Category providerDetail = Category.All().Where(x => x.Guid == Guid.Parse(providerId)).FirstOrDefault();
            if (providerDetail != null)
            {
                List<RoomAvailabilityDetail> roomAvailability = new List<RoomAvailabilityDetail>();
                List<OfferRoomAvailability> availabilityData = new List<OfferRoomAvailability>();
                List<RoomAvailability> roomavailabilityData = this.RoomAvailabilityByProvider(providerDetail.Sku.ToString(), startDate.ConvetDatetoDateCounter(), endDate.ConvetDatetoDateCounter(), productSku);
                if (includeOfferDetail)
                    availabilityData = this.OfferRoomAvailabilityByProvider(providerDetail.Sku.ToString(), campaignItemId, startDate.ConvetDatetoDateCounter(), endDate.ConvetDatetoDateCounter(), productSku);
                int numberOfDay = endDate.ConvetDatetoDateCounter() - startDate.ConvetDatetoDateCounter();
                for (int i = 0; i < numberOfDay; i++)
                {
                    RoomAvailability roomAvailabilityData = roomavailabilityData.FirstOrDefault(x => x.DateCounter == startDate.AddDays(i).ConvetDatetoDateCounter());
                    if (roomAvailability == null)
                    {
                        roomAvailability.Add(new RoomAvailabilityDetail() { AvailabilityDate = startDate.AddDays(i), AvailableRoom = 0, OfferAvailability = null, SKU = productSku, DateCounter = startDate.AddDays(i).ConvetDatetoDateCounter() });
                    }
                    else
                    {
                        List<OfferAvailabilityDetail> offerAvailabilityDetail = new List<OfferAvailabilityDetail>();
                        foreach (var item in availabilityData.Where(x => !x.IsRestrictedForSale && x.DateCounter == startDate.AddDays(i).ConvetDatetoDateCounter()))
                        {
                            offerAvailabilityDetail.Add(new OfferAvailabilityDetail() { Id = item.CampaignItem, Name = "", OfferAvailableRoom = (item.NumberAllocated - item.NumberBooked) });
                        }
                        roomAvailability.Add(new RoomAvailabilityDetail()
                        {
                            AvailabilityDate = startDate.AddDays(i),
                            AvailableRoom = roomAvailabilityData == null ? 0 : (roomAvailabilityData.IsCloseOut ? -1 : (roomAvailabilityData.NumberAllocated - roomAvailabilityData.NumberBooked)),
                            OfferAvailability = offerAvailabilityDetail,
                            DateCounter = startDate.AddDays(i).ConvetDatetoDateCounter(),
                            SKU = productSku
                        });
                    }
                }
                return roomAvailability;

            }
            return null;
        }

        /// <summary>
        /// FindOfferAvailability 
        /// </summary>
        /// <param name="providerId">providerId</param>
        ///<param name="productSku">productSku</param>
        ///<param name="startDate">startDate</param>
        ///<param name="endDate">endDate</param>
        ///<param name="campaignItemId">campaignItemId</param>
        /// <returns><list>OfferAvailability</list>Object of OfferAvailability</returns>
        public List<OfferAvailability> FindOfferAvailability(string providerId, string productSku, DateTime startDate, DateTime endDate, int campaignItemId)
        {
            Product providerDetail = Product.All().Where(x => x.Sku == providerId).FirstOrDefault();
            //Category providerDetail = Category.All().Where(x => x.Guid == Guid.Parse(providerId)).FirstOrDefault();
            if (providerDetail != null)
            {
                List<OfferAvailability> offerAvailability = new List<OfferAvailability>();
                List<OfferRoomAvailability> availabilityData = new List<OfferRoomAvailability>();
                List<RoomAvailability> roomavailabilityData = this.RoomAvailabilityByProvider(providerDetail.Sku.ToString(), startDate.ConvetDatetoDateCounter(), endDate.ConvetDatetoDateCounter(), productSku);
                //if (campaignItemId != 0) // Commented to get all room offer
                availabilityData = this.OfferRoomAvailabilityByProvider(providerDetail.Sku.ToString(), campaignItemId, startDate.ConvetDatetoDateCounter(), endDate.ConvetDatetoDateCounter(), productSku);
                int numberOfDay = endDate.ConvetDatetoDateCounter() - startDate.ConvetDatetoDateCounter();
                for (int i = 0; i <= numberOfDay; i++)
                {
                    RoomAvailability roomAvailabilityData = roomavailabilityData.FirstOrDefault(x => x.DateCounter == startDate.AddDays(i).ConvetDatetoDateCounter());
                    OfferRoomAvailability offerRoomAvailability = availabilityData.FirstOrDefault(x => !x.IsRestrictedForSale && x.DateCounter == startDate.AddDays(i).ConvetDatetoDateCounter());
                    if (roomAvailabilityData == null || offerRoomAvailability == null)
                    {
                        offerAvailability.Add(new OfferAvailability() { Id = campaignItemId, Name = string.Empty, AvailabilityDate = startDate.AddDays(i), OfferAvailableRoom = 0, SKU = productSku, DateCounter = startDate.AddDays(i).ConvetDatetoDateCounter() });
                    }
                    else
                    {
                        if (roomAvailabilityData != null && offerRoomAvailability != null)
                        {
                            int sellableRoom = (roomAvailabilityData.NumberAllocated - roomAvailabilityData.NumberBooked);
                            int offerSellableRoom = (offerRoomAvailability.NumberAllocated - offerRoomAvailability.NumberBooked);
                            if (offerSellableRoom > sellableRoom)
                            {
                                offerSellableRoom = sellableRoom;
                            }
                            offerAvailability.Add(new OfferAvailability()
                            {
                                AvailabilityDate = startDate.AddDays(i),
                                DateCounter = startDate.AddDays(i).ConvetDatetoDateCounter(),
                                OfferAvailableRoom = (roomAvailabilityData.IsCloseOut ? -1 : offerSellableRoom),
                                SKU = productSku,
                                Id = offerRoomAvailability.CampaignItem,
                                Name = string.Empty
                            });
                        }
                    }
                }
                return offerAvailability;
            }
            return null;
        }


        /// <summary>
        /// GetHotelRoomAndOfferAvailabilityByProvider 
        /// </summary>
        /// <param name="providerId">providerId</param>
        /// <param name="startDate">startDate</param>
        ///  <param name="endDate">endDate</param>
        /// <returns> <list>RoomAndOfferAvaialbityShortage</list>GetHotelRoomAndOfferAvailabilityByProvider</returns>
        public List<RoomAndOfferAvaialbityShortage> GetHotelRoomAndOfferAvailabilityByProvider(string providerId, DateTime startDate, DateTime endDate)
        {
            Product providerDetail = Product.All().Where(x => x.Sku == providerId).FirstOrDefault();
            //Category providerDetail = Category.All().Where(x => x.Guid == Guid.Parse(providerId)).FirstOrDefault();
            if (providerDetail != null)
            {
                ICampaignSearch campaignDetail = new CampaignSearch();
                List<SupplierInviteDetail> supplierInvite = campaignDetail.GetCampaignDetailBySupplier(providerId, startDate, endDate);
                List<Product> providerProduct = providerDetail.Variants.ToList();
                List<RoomAndOfferAvaialbityShortage> shortageDetail = new List<RoomAndOfferAvaialbityShortage>();
                List<RoomAvailability> roomAvailabilityDetail = RoomAvailabilityByProvider(providerId, startDate.ConvetDatetoDateCounter(), endDate.ConvetDatetoDateCounter(), string.Empty);
                List<OfferRoomAvailability> offerAvailabilityDetail = OfferRoomAvailabilityByProvider(providerId, 0, startDate.ConvetDatetoDateCounter(), endDate.ConvetDatetoDateCounter(), string.Empty);
                List<RoomAndOfferAvaialbityShortage> roomOfferShortageDeatils = new List<RoomAndOfferAvaialbityShortage>();
                for (int i = 0; i < (endDate.ConvetDatetoDateCounter() - startDate.ConvetDatetoDateCounter()); i++)
                {
                    RoomAndOfferAvaialbityShortage roomofferShortage = new RoomAndOfferAvaialbityShortage();
                    roomofferShortage.Date = startDate.AddDays(i);
                    foreach (var item in providerProduct)
                    {
                        //get product type
                        int type = GetProductType(item, true);
                        var roomAvailability = roomAvailabilityDetail.FirstOrDefault(x => x.DateCounter == startDate.AddDays(i).ConvetDatetoDateCounter() && x.VariantSku.Trim().Equals(item.VariantSku.Trim()));
                        //add availability details, only if product type is 1. i.e: room
                        if (type == 1)
                        {
                            if (roomAvailability != null)
                            {
                                roomofferShortage.RoomShortage.Add(new RoomShortage() { AvailableRoom = roomAvailability.NumberAllocated, BookedRoom = roomAvailability.NumberBooked, DataExist = true, ProductSku = item.VariantSku, RoomName = item.Name, IsCloseOut = roomAvailability.IsCloseOut, IsSoldOut = (roomAvailability.NumberAllocated - roomAvailability.NumberBooked == 0) });
                            }
                            else
                            {
                                roomofferShortage.RoomShortage.Add(new RoomShortage() { AvailableRoom = 0, BookedRoom = 0, DataExist = false, ProductSku = item.VariantSku, RoomName = item.Name });
                            }
                        }
                        foreach (var supplierDetail in supplierInvite.Where(x => x.IsSubscibe))
                        {
                            var offerAvailability = offerAvailabilityDetail.FirstOrDefault(x => x.DateCounter == startDate.AddDays(i).ConvetDatetoDateCounter() && x.CampaignItem == supplierDetail.CampaignItemId && x.VariantSku.Trim().Equals(item.Sku.Trim()));
                            if (offerAvailability != null)
                            {
                                roomofferShortage.OfferShortage.Add(new OfferShortage() { AvailableRoom = offerAvailability.NumberAllocated, CampaignItemId = supplierDetail.CampaignItemId, OfferName = supplierDetail.CampaignName, BookedRoom = offerAvailability.NumberBooked, DataExist = true, ProductSku = item.Sku, RoomName = item.Name });
                            }
                            else
                            {
                                roomofferShortage.OfferShortage.Add(new OfferShortage() { AvailableRoom = 0, BookedRoom = 0, CampaignItemId = supplierDetail.CampaignItemId, OfferName = supplierDetail.CampaignName, DataExist = false, ProductSku = item.Sku, RoomName = item.Name });
                            }
                        }
                    }
                    roomOfferShortageDeatils.Add(roomofferShortage);


                }
                return roomOfferShortageDeatils;
            }
            return new List<RoomAndOfferAvaialbityShortage>();
        }

        /// <summary>
        /// GetOfferBookingByDateRange 
        /// </summary>
        /// <param name="providerId">providerId</param>
        /// <param name="campaignItemId">campaignItemId</param>
        /// <returns> <list>GetOfferBookingByDateRange</list>GetOfferBookingByDateRange</returns>
        public List<int> GetOfferBookingByDateRange(string providerId, int campaignItemId)
        {
            List<int> bookingDetail = new List<int>();
            bookingDetail.Add(0);
            bookingDetail.Add(0);
            Product providerDetail = Product.All().Where(x => x.Sku == providerId).FirstOrDefault();
            //Category providerDetail = Category.All().Where(x => x.Guid == Guid.Parse(providerId)).FirstOrDefault();
            if (providerDetail != null)
            {
                List<OfferRoomAvailability> offerAvailability = this.OfferRoomAvailabilityByProvider(providerId, campaignItemId, DateTime.Now.AddYears(-1).ConvetDatetoDateCounter(), DateTime.Now.ConvetDatetoDateCounter(), string.Empty);
                if (offerAvailability != null && offerAvailability.Any())
                {
                    bookingDetail[0] = offerAvailability.Sum(x => x.NumberBooked);
                }
                List<OfferRoomAvailability> futureofferAvailability = this.OfferRoomAvailabilityByProvider(providerId, campaignItemId, DateTime.Now.AddDays(1).ConvetDatetoDateCounter(), DateTime.MaxValue.ConvetDatetoDateCounter(), string.Empty);
                if (futureofferAvailability != null && futureofferAvailability.Any())
                {
                    bookingDetail[1] = futureofferAvailability.Sum(x => x.NumberBooked);
                }

            }
            return bookingDetail;
        }

        /// <summary>
        /// GetProductDetailByProviderId 
        /// </summary>
        /// <param name="providerId">providerId</param>
        /// <returns> <list>GetProductDetailByProviderId</list>GetProductDetailByProviderId</returns>
        public List<ProductDetail> GetProductDetailByProviderId(string providerId)
        {
            List<ProductDetail> products = new List<ProductDetail>();
            List<ProductDetail> tmpproducts = new List<ProductDetail>();
            Product providerDetail = Product.All().Where(x => x.Sku == providerId).FirstOrDefault();

            if (providerDetail != null)
            {
                List<Product> providerProduct = providerDetail.Variants
                    .Where(x => !x.ProductProperties.Any(y => y.ProductDefinitionField.Name.Equals(ProductDefinationVairantConstant.RelatedSKU) && y.Value == "True")
                    && !x.ProductProperties.Any(y => y.ProductDefinitionField.Name.Equals(ProductDefinationVairantConstant.IsUnlimited) && y.Value == "True")).ToList();
                foreach (var item in providerProduct)
                {
                    int type = GetProductType(item, true);
                    if (type == -1)
                        continue;
                    tmpproducts.Add(new ProductDetail() { ProductType = type, CombineSKU = string.Format("{0}@{1}", providerId, item.VariantSku), ProductId = item.ProductId, ProductSKU = item.VariantSku, ProductName = item.Name, PriceBand = GetPriceBand(item.VariantSku, providerId) });
                }
            }
            if (tmpproducts != null && tmpproducts.Any())
            {
                if (tmpproducts.Any(x => x.ProductType == 1))
                {
                    products.AddRange(tmpproducts.Where(x => x.ProductType == 1));
                }
                if (tmpproducts.Any(x => x.ProductType == 3))
                {
                    products.AddRange(tmpproducts.Where(x => x.ProductType == 3));
                }
                if (tmpproducts.Any(x => x.ProductType == 2))
                {
                    products.AddRange(tmpproducts.Where(x => x.ProductType == 2));
                }
            }
            return products;
        }

        private int GetProductType(Product item, bool ignoreNotSellable = false)
        {
            int type = -1;
            if (item != null)
            {
                ProductProperty addOn = item[ProductDefinationVairantConstant.FoodType];
                ProductProperty packagType = item[ProductDefinationVairantConstant.PackageType];
                ProductProperty relatedSKU = item[ProductDefinationVairantConstant.RelatedSKU];
                ProductProperty roomType = item[ProductDefinationVairantConstant.RoomType];
                if (packagType != null && !string.IsNullOrEmpty(packagType.Value))
                {
                    //Package
                    type = 3;
                }
                else if (roomType != null && !string.IsNullOrEmpty(roomType.Value))
                {
                    //Room
                    type = 1;
                }
                else
                {
                    ProductProperty sellableAddon = item[ProductDefinationVairantConstant.IsSellableStandAlone];
                    bool sellable = false;
                    if (sellableAddon != null && !string.IsNullOrEmpty(sellableAddon.Value))
                    {
                        sellable = Convert.ToBoolean(sellableAddon.Value);
                    }
                    if (!sellable)
                    {
                        if (ignoreNotSellable)
                            type = -1;
                    }
                    else
                    {
                        //Add on
                        type = 2;
                    }
                }
            }
            return type;
        }


        /// <summary>
        /// Audit trial log for offer availability
        /// </summary>
        /// <param name="datewiseOfferAvailability"></param>
        private void OfferAvailabilityAuditLog(OfferRoomAvailability datewiseOfferAvailability)
        {
            //Map Data
            AuditData auditData = new AuditData();

            //mapping values from OfferRoomAvailability
            auditData.AddAdditionalData("OfferAvailibilityID", datewiseOfferAvailability.OfferAvailibilityID);
            auditData.AddAdditionalData("NumberAllocated", datewiseOfferAvailability.NumberAllocated);
            auditData.AddAdditionalData("NumberBooked", datewiseOfferAvailability.NumberBooked);
            auditData.AddAdditionalData("Sku", datewiseOfferAvailability.Sku);
            auditData.AddAdditionalData("CampaignItem", datewiseOfferAvailability.CampaignItem);
            auditData.AddAdditionalData("VariantSku", datewiseOfferAvailability.VariantSku);
            auditData.AddAdditionalData("DateCounter", datewiseOfferAvailability.DateCounter);
            auditData.AddAdditionalData("DateCreated", datewiseOfferAvailability.DateCreated);
            auditData.AddAdditionalData("CreatedBy", datewiseOfferAvailability.CreatedBy);
            auditData.AddAdditionalData("IsRestrictedForSale", datewiseOfferAvailability.IsRestrictedForSale);
            auditData.AddAdditionalData("DateLastUpdate", datewiseOfferAvailability.DateLastUpdate);
            auditData.AddAdditionalData("UpdateBy", datewiseOfferAvailability.UpdateBy);

            auditData.AuditName = AuditNames.ChangesInAvailability;
            auditData.Message = "LB_OfferAvailability";
            auditData.Operation = AuditOperations.Edit;
            auditData.ObjectType = AuditObjectType.CustomTableInUcommerce;
            auditData.ObjectId = datewiseOfferAvailability.OfferAvailibilityID != null ?
                datewiseOfferAvailability.OfferAvailibilityID.ToString() : string.Empty;
            auditData.Section = AuditSection.SupplierPortal;
            auditData.Username = (Sitecore.Context.User) != null ? Sitecore.Context.User.Name : string.Empty;
            auditData.Date = datewiseOfferAvailability.DateLastUpdate;

            //Log Data in Audit Trial                    
            AuditTrail.Instance.Log(auditData);

        }
        /// <summary>
        /// Audit trial log for offer availability
        /// </summary>
        /// <param name="datewiseAvailability"></param>
        private void RoomAvailabilityAuditLog(RoomAvailability datewiseAvailability)
        {
            //Map Data
            AuditData auditData = new AuditData();


            //mapping property values from RoomAvailability 
            auditData.AddAdditionalData("RoomAvailibilityID", datewiseAvailability.RoomAvailibilityID);
            auditData.AddAdditionalData("VariantSku", datewiseAvailability.VariantSku);
            auditData.AddAdditionalData("NumberAllocated", datewiseAvailability.NumberAllocated);
            auditData.AddAdditionalData("NumberBooked", datewiseAvailability.NumberBooked);
            auditData.AddAdditionalData("DateCounter", datewiseAvailability.DateCounter);
            auditData.AddAdditionalData("CreatedBy", datewiseAvailability.CreatedBy);
            auditData.AddAdditionalData("UpdateBy", datewiseAvailability.UpdateBy);
            auditData.AddAdditionalData("DateCreated", datewiseAvailability.DateCreated);
            auditData.AddAdditionalData("DateLastUpdate", datewiseAvailability.DateLastUpdate);
            auditData.AddAdditionalData("IsStopSellOn", datewiseAvailability.IsStopSellOn);
            auditData.AddAdditionalData("IsCloseOut", datewiseAvailability.IsCloseOut);
            auditData.AddAdditionalData("IsStopSellOn", datewiseAvailability.IsStopSellOn);
            auditData.AddAdditionalData("Price", datewiseAvailability.Price);
            auditData.AddAdditionalData("PackagePrice", datewiseAvailability.PackagePrice);
            auditData.AddAdditionalData("PriceBand", datewiseAvailability.PriceBand);
            auditData.AddAdditionalData("Sku", datewiseAvailability.Sku);

            auditData.AuditName = AuditNames.ChangesInAvailability;
            auditData.Message = "LB_RoomAvailability";
            auditData.Operation = AuditOperations.Edit;
            auditData.ObjectType = AuditObjectType.CustomTableInUcommerce;
            auditData.ObjectId = datewiseAvailability.RoomAvailibilityID != null ?
                datewiseAvailability.RoomAvailibilityID.ToString() : string.Empty;
            auditData.Section = AuditSection.SupplierPortal;
            auditData.Username = (Sitecore.Context.User) != null ? Sitecore.Context.User.Name : string.Empty;
            auditData.Date = datewiseAvailability.DateLastUpdate;

            //Log Data in Audit Trial                    
            AuditTrail.Instance.Log(auditData);
        }

        /// <summary>
        /// Get price by daterange
        /// </summary>
        /// <param name="sku"></param>
        /// <param name="variantSku"></param>
        /// <param name="startDateCounter"></param>
        /// <param name="endDateCounter"></param>
        /// <returns></returns>
        public List<model.ProductPrice> GetProductPriceByDateRange(string sku, string variantSku, DateTime startDate, DateTime endDate)
        {
            var results = new List<model.ProductPrice>();

            for (var dateDiff = startDate; dateDiff <= endDate; dateDiff = dateDiff.AddDays(1))
            {
                results.Add(GetProductPriceByDate(sku, variantSku, dateDiff));
            }

            return results;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sku"></param>
        /// <param name="variantSku"></param>
        /// <param name="date"></param>
        /// <param name="isSubItem"></param>
        /// <returns></returns>
        public model.ProductPrice GetProductPriceByDate(string sku, string variantSku, DateTime date, bool isSubItem = false)
        {
            Sitecore.Data.Database workingdb = Sitecore.Context.Database;
            var roomavailability = RoomAvailability.FirstOrDefault(x => x.Sku == sku && x.VariantSku == variantSku && x.DateCounter == date.ConvetDatetoDateCounter());
            var product = Product.FirstOrDefault(i => i.Sku == sku && i.VariantSku == variantSku);

            Item priceband = null;
            if (roomavailability != null && !string.IsNullOrEmpty(roomavailability.PriceBand))
                priceband = workingdb.GetItem(Sitecore.Data.ID.Parse(roomavailability.PriceBand));
            var price = new model.ProductPrice();
            if (product != null)
            {
                var type = GetProductType(product);
                var relatedSkus = product[ProductDefinationVairantConstant.RelatedSKU];
                var relatedAddOnSkus = product[ProductDefinationVairantConstant.RelatedAddOnSKU];
                var relatedProducts = new List<string>();
                if (relatedSkus != null && !string.IsNullOrEmpty(relatedSkus.Value))
                    relatedProducts.AddRange(relatedSkus.Value.Split(','));
                if (relatedAddOnSkus != null && !string.IsNullOrEmpty(relatedAddOnSkus.Value))
                    relatedProducts.AddRange(relatedAddOnSkus.Value.Split(','));

                price.Sku = sku;
                price.VariantSku = variantSku;
                price.Name = product.Name;
                if (roomavailability != null)
                {
                    price.Date = roomavailability.DateCounter.GetDateFromDateCount();
                }
                if (type != null)
                {
                    //if product type is room, price calculation logic goes here
                    if (type == 1)
                    {
                        decimal prodPrice = 0;
                        Decimal.TryParse(SitecoreFieldsHelper.GetItemFieldValue(priceband, "Rate"), out prodPrice);
                        price.PriceAmount = Math.Round(prodPrice,2);
                        price.Commision = CalculateCommisionForRoom(priceband, price.PriceAmount);
                    }
                    //if product type is add on, price calculation logic goes here
                    else if (type == 2)
                    {
                        var addOnPriceDetails = GetAddOnPriceFromSC(variantSku);

                        var priceKey = isSubItem ? "PackagePrice" : "Price";
                        var commissionKey = isSubItem ? "LBShare_PKG" : "LBShare";
                        price.Commision = new model.Commision();
                        price.PriceAmount = AddOnPrice(priceKey, addOnPriceDetails);
                        var commisionVal = AddOnPrice(commissionKey, addOnPriceDetails);
                        price.Commision.Common =
                        price.Commision.Day1 =
                        price.Commision.Day2 =
                        price.Commision.Day3 =
                        price.Commision.Day4 =
                        price.Commision.Day5 =
                        price.Commision.Day6 =
                        price.Commision.Day7 = commisionVal;

                    }

                    //if package,price calculation logic goes here
                    if (relatedProducts.Count > 0)
                    {
                        price.RelatedPrice = relatedProducts.Select(i => GetProductPriceByDate(sku, i, date, true)).ToList();
                        price.PriceAmount = price.RelatedPrice.Sum(i => i.PriceAmount);
                        price.Commision = new model.Commision();
                        price.Commision.Common = Math.Round(price.RelatedPrice.Sum(i => i.Commision.Common));
                        price.Commision.Day1 = Math.Round(price.RelatedPrice.Sum(i => i.Commision.Day1));
                        price.Commision.Day2 = Math.Round(price.RelatedPrice.Sum(i => i.Commision.Day2));
                        price.Commision.Day3 = Math.Round(price.RelatedPrice.Sum(i => i.Commision.Day3));
                        price.Commision.Day4 = Math.Round(price.RelatedPrice.Sum(i => i.Commision.Day4));
                        price.Commision.Day5 = Math.Round(price.RelatedPrice.Sum(i => i.Commision.Day5));
                        price.Commision.Day6 = Math.Round(price.RelatedPrice.Sum(i => i.Commision.Day6));
                        price.Commision.Day7 = Math.Round(price.RelatedPrice.Sum(i => i.Commision.Day7));
                        price.Commision.ProcessingFee = Math.Round(price.RelatedPrice.Max(i => i.Commision.ProcessingFee));
                    }
                }
            }
            return price;

        }

        /// <summary>
        /// Calculate Commision for room
        /// </summary>
        /// <param name="PriceBand"></param>
        /// <param name="productprice"></param>
        /// <returns></returns>
        private model.Commision CalculateCommisionForRoom(Item PriceBand, decimal productprice)
        {

            productprice = Math.Round(productprice, 2);
            var commision = new model.Commision();
            List<OrderLinePriceBand> commissionlist = new List<OrderLinePriceBand>();

            //Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
            if (PriceBand != null)
            {
                Item selectedItem = SitecoreFieldsHelper.GetDropLinkFieldTargetItem(PriceBand, "Commission");
                if (selectedItem != null)
                {
                    var processFeeField = "Processing Fee";
                    var processFees = new List<decimal>();
                    commision.Common = Commision(selectedItem, "common", productprice);
                    processFees.Add(Commision(selectedItem, "common", productprice, processFeeField));
                    commision.Day1 = Commision(selectedItem, "1", productprice);
                    commision.Day1 = commision.Day1 == 0 ? commision.Common : commision.Day1;
                    processFees.Add(Commision(selectedItem, "1", productprice, processFeeField));
                    commision.Day2 = Commision(selectedItem, "2", productprice);
                    commision.Day2 = commision.Day1 == 0 ? commision.Common : commision.Day2;
                    processFees.Add(Commision(selectedItem, "2", productprice, processFeeField));
                    commision.Day3 = Commision(selectedItem, "3", productprice);
                    commision.Day3 = commision.Day1 == 0 ? commision.Common : commision.Day3;
                    processFees.Add(Commision(selectedItem, "3", productprice, processFeeField));
                    commision.Day4 = Commision(selectedItem, "4", productprice);
                    commision.Day4 = commision.Day1 == 0 ? commision.Common : commision.Day4;
                    processFees.Add(Commision(selectedItem, "4", productprice, processFeeField));
                    commision.Day5 = Commision(selectedItem, "5", productprice);
                    commision.Day5 = commision.Day1 == 0 ? commision.Common : commision.Day5;
                    processFees.Add(Commision(selectedItem, "5", productprice, processFeeField));
                    commision.Day6 = Commision(selectedItem, "6", productprice);
                    commision.Day6 = commision.Day1 == 0 ? commision.Common : commision.Day6;
                    processFees.Add(Commision(selectedItem, "6", productprice, processFeeField));
                    commision.Day7 = Commision(selectedItem, "7", productprice);
                    commision.Day7 = commision.Day1 == 0 ? commision.Common : commision.Day7;
                    processFees.Add(Commision(selectedItem, "7", productprice, processFeeField));
                    commision.ProcessingFee = Math.Round(processFees.Max(), 2);
                }
            }

            return commision;
        }


        /// <summary>
        /// Get Fields WithIn Section
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="sectionName"></param>
        /// <returns></returns>
        private Dictionary<string, string> GetFieldsWithInSection(Item itemId, string sectionName)
        {
            Dictionary<string, string> FieldsWithInSection = new Dictionary<string, string>();

            if (itemId != null)
            {
                FieldsWithInSection = itemId.Fields.Where(name => name.Section == sectionName).ToDictionary(field => field.Name, field => field.Value);
            }

            return FieldsWithInSection;
        }

        /// <summary>
        /// Commision amount
        /// </summary>
        /// <param name="itemID"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>

        private decimal Commision(Item itemID, string fieldName, decimal productprice, string field = SiteCoreCommsionConstant.LBCommision)
        {
            decimal price = 0;

            Item selectedItem = SitecoreFieldsHelper.GetDropLinkFieldTargetItem(itemID, fieldName);
            if (selectedItem != null)
            {
                Dictionary<string, string> fieldsInFeesPercentageScetion = GetFieldsWithInSection(selectedItem, "Fees Percentage");
                Dictionary<string, string> fieldsInFeeScetion = GetFieldsWithInSection(selectedItem, "Fees");
                var feePercentage = fieldsInFeesPercentageScetion.Where(x => x.Key.Equals(field)).FirstOrDefault();
                var fee = fieldsInFeeScetion.Where(x => x.Key.Equals(field)).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(fee.Value))
                {
                    Decimal.TryParse(fee.Value, out price);
                    price = Math.Round(price, 2);
                }
                else
                {
                    Decimal.TryParse(feePercentage.Value, out price);
                    price = productprice * price / 100;
                    price = Math.Round(price, 2);
                }

            }

            return price;
        }

        /// <summary>
        /// Add On Price from sitecore
        /// </summary>
        /// <param name="variantsku"></param>
        /// <returns></returns>
        private Dictionary<string, string> GetAddOnPriceFromSC(string variantsku)
        {
            var priceDetails = new Dictionary<string, string>();
            Item addOn = Sitecore.Context.Database.SelectItems(string.Format("fast:/sitecore/content/#admin-portal#/#supplier-setup#//*[@@TemplateName='Supplier Addon' and @SKU='{0}']", variantsku)).FirstOrDefault();
            if (addOn != null)
            {
                string price = SitecoreFieldsHelper.GetItemFieldValue(addOn, "Price");
                string packagePrice = SitecoreFieldsHelper.GetItemFieldValue(addOn, "PackagePrice");
                string lbShare = SitecoreFieldsHelper.GetItemFieldValue(addOn, "LBShare");
                string lbPKGShare = SitecoreFieldsHelper.GetItemFieldValue(addOn, "LBShare_PKG");
                priceDetails.Add("Price", price);
                priceDetails.Add("PackagePrice", packagePrice);
                priceDetails.Add("LBShare", lbShare);
                priceDetails.Add("LBShare_PKG", lbPKGShare);
            }
            return priceDetails;
        }

        /// <summary>
        /// Add On Price
        /// </summary>
        /// <param name="key"></param>
        /// <param name="addOnPrice"></param>
        /// <returns></returns>
        private decimal AddOnPrice(string key, Dictionary<string, string> addOnPrice)
        {
            decimal price = 0;

            string priceAmt = string.Empty;
            if (addOnPrice.ContainsKey(key))
            {
                addOnPrice.TryGetValue(key, out priceAmt);
                Decimal.TryParse(priceAmt, out price);
                price = Math.Round(price, 2);
            }
            return price;
        }


        /// <summary>
        /// Update Related Package Availability
        /// </summary>
        /// <param name="variantSku"></param>
        /// <returns>updated or not</returns>
        public bool UpdateRelatedPackageAvailability(string variantSku, string dateCounter)
        {
            bool recordUpdated = false;
            try
            {
                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand("LB_UpdatePackageAvailability", con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@VariantSku", SqlDbType.NVarChar)).Value = variantSku;
                            command.Parameters.Add(new SqlParameter("@DateCounter", SqlDbType.NVarChar)).Value = dateCounter;
                            if (con.State != ConnectionState.Open)
                                con.Open();
                            command.ExecuteNonQuery();
                            con.Close();
                            recordUpdated = true;

                        }
                    }
                }

            }
            catch (Exception exception)
            {

                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
            return recordUpdated;
        }
    }
}
