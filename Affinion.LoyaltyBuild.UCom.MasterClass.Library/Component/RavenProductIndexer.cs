﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce;
using UCommerce.Content;
using UCommerce.EntitiesV2;
using UCommerce.EntitiesV2.Definitions;
using UCommerce.Infrastructure.Globalization;
using UCommerce.Search;
using UCommerce.Search.Indexers;
using UCommerce.Search.Indexers.PropertyValueConverters;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Component
{
    public class LBRavenProductIndexer : IIndexer<IEnumerable<Product>>, IDeindexer<IEnumerable<Product>>, IIndexer<Product>, IDeindexer<Product>
    {
        // Fields
        private readonly IImageService _imageService;
        private readonly ILanguageService _languageService;
        private readonly IList<IPropertyValueConverter> _propertyValueConverters;
        private readonly IRepository _ravenDbRepository;

        // Methods
        public LBRavenProductIndexer(IRepository ravenDbRepository, ILanguageService languageService, IImageService imageService, IList<IPropertyValueConverter> propertyValueConverters)
        {
            this._ravenDbRepository = ravenDbRepository;
            this._languageService = languageService;
            this._imageService = imageService;
            this._propertyValueConverters = propertyValueConverters;
        }

        protected virtual object ConvertValue(IProperty property)
        {
            IPropertyValueConverter converter = this._propertyValueConverters.FirstOrDefault<IPropertyValueConverter>(x => x.Supports(property)) ?? new DefaultPropertyValueConverter();
            return converter.Convert(property);
        }

        public virtual void Deindex(IEnumerable<Product> subjects)
        {
            List<string> list = (from x in subjects select string.Format("products/{0}", x.ProductId)).ToList<string>();
            this._ravenDbRepository.Delete<Product>((IEnumerable<object>)list);
        }

        public virtual void Deindex(Product subjects)
        {
            this.Deindex(new List<Product> { subjects });
        }

        public virtual void Index(Product subjects)
        {
            this.Index(new List<Product> { subjects });
        }

        public virtual void Index(IEnumerable<Product> subjects)
        {
            try
            {
                List<UCommerce.Documents.Product> items = new List<UCommerce.Documents.Product>();
                foreach (Product product in subjects)
                {
                    items.Add(this.MapProductToRavenProduct(product));
                }
                this._ravenDbRepository.SaveMany<UCommerce.Documents.Product>(items);
            }
            catch (Exception er)
            { 
            
            }
        }


        protected virtual UCommerce.Documents.Product MapProductToRavenProduct(Product product)
        {
            UCommerce.Documents.Product product4 = new UCommerce.Documents.Product
            {
                PrimaryImageUrl = this._imageService.GetImage(product.PrimaryImageMediaId).Url,
                ThumbnailImageUrl = this._imageService.GetImage(product.ThumbnailImageMediaId).Url,
                AllowOrdering = product.AllowOrdering,
                CreatedBy = product.CreatedBy,
                CreatedOn = product.CreatedOn,
                DisplayOnSite = product.DisplayOnSite,
                Id = product.ProductId,
                ModifiedBy = product.ModifiedBy,
                ModifiedOn = product.ModifiedOn,
                Name = product.Name
            };
            double? rating = product.Rating;
            product4.Rating = rating.HasValue ? rating.GetValueOrDefault() : 0.0;
            product4.Sku = product.Sku;
            product4.VariantSku = product.VariantSku;
            product4.Weight = product.Weight;
            product4.ProductDefinition = "definitions/" + product.ProductDefinition.Guid;
            UCommerce.Documents.Product product2 = product4;
            foreach (Product product3 in product.Variants)
            {
                product2.Variants.Add(this.MapProductToRavenProduct(product3));
            }
            foreach (ProductRelation relation in product.ProductRelations)
            {
                product2.RelatedProductIds.Add(relation.RelatedProduct.ProductId.ToString());
            }
            ICollection<IProperty> properties = product.GetProperties();
            foreach (IGrouping<string, IProperty> grouping in from x in properties
                                                              where x.GetDefinitionField().Multilingual
                                                              group x by x.CultureCode)
            {
                string cultureCode = !string.IsNullOrWhiteSpace(grouping.Key) ? grouping.Key : Constants.Search.InvariantCulture;
                foreach (IProperty property in grouping)
                {
                    product2.Properties.AddProperty(cultureCode, property.GetDefinitionField().Name, this.ConvertValue(property));
                }
            }
            foreach (IProperty property2 in from x in properties
                                            where !x.GetDefinitionField().Multilingual
                                            select x)
            {
                string name = property2.GetDefinitionField().Name;
                object obj2 = this.ConvertValue(property2);
                product2.Properties.AddProperty(name, obj2);
            }
            using (IEnumerator<Language> enumerator6 = this._languageService.GetAllLanguages().GetEnumerator())
            {
                while (enumerator6.MoveNext())
                {
                    Func<ProductDescription, bool> predicate = null;
                    Language language = enumerator6.Current;
                    if (predicate == null)
                    {
                        predicate = x => x.CultureCode == language.CultureCode;
                    }
                    ProductDescription description = product.ProductDescriptions.SingleOrDefault<ProductDescription>(predicate);
                    if (description != null)
                    {
                        string key = !string.IsNullOrWhiteSpace(language.CultureCode) ? language.CultureCode : Constants.Search.InvariantCulture;
                        if (!product2.Properties.ContainsKey(key))
                        {
                            product2.Properties.Add(key, new Dictionary<string, object>());
                        }
                        product2.Properties.AddProperty(key, "DisplayName", description.DisplayName);
                        product2.Properties.AddProperty(key, "ShortDescription", description.ShortDescription);
                        product2.Properties.AddProperty(key, "LongDescription", description.LongDescription);
                    }
                }
            }
            foreach (PriceGroupPrice price in from x in product.PriceGroupPrices
                                              where !x.PriceGroup.Deleted
                                              select x)
            {
                if (price.Price.HasValue)
                {
                    product2.Prices.Add(price.PriceGroup.Name, price.Price.Value);
                }
            }
            product2.CategoryIds = (from x in product.GetCategories() select x.CategoryId).ToList<int>();
            return product2;
        }
    }
}