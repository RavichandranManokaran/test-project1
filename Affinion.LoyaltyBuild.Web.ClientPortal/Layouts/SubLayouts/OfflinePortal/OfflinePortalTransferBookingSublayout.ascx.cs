﻿using System;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using order = Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Order;
using payment = Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Payment;
using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
using basket = Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Basket;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.Loyaltybuild.BusinessLogic.Helper;
using Affinion.LoyaltyBuild.Web.ClientPortal.Helper;
using mail = Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Email;
using Affinion.LoyaltyBuild.UCom.Common.Extension;
using System.Collections.Generic;
using Affinion.LoyaltyBuild.Common.SessionHelper;
using System.IO;
using System.Web.UI;
using SelectPdf;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Order;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Basket;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Payment;
using Affinion.LoyaltyBuild.Common;
using Sitecore.Data.Items;
using Affinion.LoyaltyBuild.Common.CardValidationCheck;
using apiBookingService=Affinion.LoyaltyBuild.Api.Booking.Service;
using data = Affinion.LoyaltyBuild.Api.Booking.Data;
using bookinghelper = Affinion.LoyaltyBuild.Api.Booking.Helper;
using Affinion.LoyaltyBuild.Model.Provider;
using Affinion.LoyaltyBuild.Api.Booking.Data.Basket;
using Affinion.LoyaltyBuild.Web.ClientPortal.Models;
using Affinion.LoyaltyBuild.Model.Common;
using UCommerce.Infrastructure;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    public partial class TransferBooking : BaseSublayout
    {
        private mail.IMailService _mailService;
        private apiBookingService.IPaymentService _paymentService;
        private int _orderLineId;
        private int _newOrderlineId;

        protected data.Booking Booking { get; private set; }
        protected data.Booking NewBooking { get; private set; }
        protected string TransferUrl { get; set; }
        protected Customer Customer { get; private set; }
        protected IAccomodation Accomodation { get; private set; }
        protected IAccomodation NewAccomodation { get; private set; }
        protected bool IsTransferConfirm { get; private set; }
        protected bool ShowTransfer { get; private set; }
        protected bool HasPendingPayments { get; private set; }
        protected ValidationResult ValidationResponse { get; private set; }
        private bool startConversion = false;
        apiBookingService.IBookingService bookingService = null;
        protected List<data.Payment.TotalDueItem> outstandingPayments = null;
        /// <summary>
        /// page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

            bookingService = ObjectFactory.Instance.Resolve<apiBookingService.IBookingService>();

            int olid;
            int.TryParse(this.QueryString("olid"), out olid);
            _orderLineId = olid;

            //get booking
            this.Booking = bookingService.GetBooking(olid);

            LoadDates(this.Booking);
            LoadCustomer(this.Booking);
            LoadPayments(this.Booking);
            LoadAccomodation(this.Booking);
            LoadOtherDetails(this.Booking);

           //set show transfer
            SetShowTransfer(this.Booking);

            if (this.Booking != null)
            {
                //if action is confirmed
                if (this.QueryString("action").Equals("confirm"))
                    ConfirmedTransfer(this.Booking);

                //check if it is confirm transfer
                if (ShowTransfer && this.QueryString("action") == "transfer" && UCommerce.Api.TransactionLibrary.HasBasket())
                {
                    IsTransferConfirm = true;
                    var basketItem = bookinghelper.BasketHelper.GetBasket();
                    if (basketItem != null)
                    {
                        NewBooking = (data.Booking)basketItem.Bookings[0];
                        _newOrderlineId = NewBooking.OrderLineId;
                        LoadNewBookingDetails(NewBooking);
                    }
                }
                
                //build transfer string
                if (ShowTransfer)
                {
                    var clientName = "loyaltybuild";
                    var ClientId = string.Empty;
                    if (!string.IsNullOrEmpty(this.Booking.ClientId))
                    {
                        ClientId = this.Booking.ClientId;
                        var client = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(Booking.ClientId));
                        if (client != null)
                            clientName = client.Name;
                    }
                    
                    TransferUrl = string.Format("&mode=Transfer&ClientList={5}&CheckinDate={0}&CheckoutDate={1}&NoOfNights={2}&NoOfAdults={3}&NoOfChildren={4}"
                        , Booking.CheckinDate.ConvertDateToString()
                        , Booking.CheckoutDate.ConvertDateToString(),
                        litNoOfNights.Text,
                        litNoOfAdults.Text,
                        litNoOfChildren.Text,
                        clientName
                        );
                }
                startConversion = true;
            }

            ShowMessage(this.Booking);

        }

        private void LoadDates(data.Booking booking)
        {
            var dateFormat = "dd/MM/yyyy-ddd";

            if (booking.CheckinDate != DateTime.MinValue)
                litArrivalDate.Text = booking.CheckinDate.ToString(dateFormat);
            if (booking.CheckoutDate != DateTime.MinValue)
                litDepartureDate.Text = booking.CheckoutDate.ToString(dateFormat);
            if (booking.CurrentStatusDate != null && booking.CurrentStatusDate != DateTime.MinValue)
                litStatusDate.Text = booking.CheckinDate.ToString(dateFormat);
            if (booking.ConfirmedStatusDate != null && booking.ConfirmedStatusDate != DateTime.MinValue)
                litConfirmationDate.Text = booking.CheckinDate.ToString(dateFormat);
            if (booking.OrderLine.CreatedOn != null && booking.OrderLine.CreatedOn != DateTime.MinValue)
                litReservationDate.Text = booking.OrderLine.CreatedOn.ToString(dateFormat);
        }

        private void LoadCustomer(data.Booking booking)
        {
            var order = UCommerce.EntitiesV2.PurchaseOrder.Get(booking.OrderId);

            if (order == null || order.Customer == null)
            {
                divCustomer.Visible = false;
                return;
            }

            var customer = order.Customer;
            litCustomerName.Text = string.Format("{0} {1}", customer.FirstName, customer.LastName);
            litCustomerPhone.Text = customer.PhoneNumber;
            litCustomerEmail.Text = customer.EmailAddress;
            var address = new List<string>();
            if (customer.Addresses != null && customer.Addresses.Count > 0)
            {
                var firstAddress = customer.Addresses[0];
                if (!string.IsNullOrEmpty(firstAddress.Line1))
                    address.Add(firstAddress.Line2);
                if (!string.IsNullOrEmpty(firstAddress.City))
                    address.Add(firstAddress.City);
                if (!string.IsNullOrEmpty(firstAddress.State))
                    address.Add(firstAddress.State);
                if (firstAddress.Country != null)
                    address.Add(firstAddress.Country.Name);
            }
            litCustomerAddress.Text = string.Join(",", address);
        }

        private void LoadPayments(data.Booking booking)
        {
            litPrice.Text = CurrencyHelper.FormatCurrency(booking.Price + booking.ProcessingFee, booking.CurrencyId);
            litDeposit.Text = CurrencyHelper.FormatCurrency(booking.CommissionAmount, booking.CurrencyId);
            litProcesingFee.Text = CurrencyHelper.FormatCurrency(booking.ProcessingFee, booking.CurrencyId);
            var checkout = booking.ProviderAmountPaidOnCheckOut + booking.CommissionAmount + booking.ProcessingFee;
            litCheckout.Text = CurrencyHelper.FormatCurrency(checkout, booking.CurrencyId);
        }

        private void LoadAccomodation(data.Booking booking)
        {
            var provider = booking.GetProvider();
            if (provider != null)
            {
                var genericProvider = (IGenericProvider)provider;

                // litAccName.Text = Site
                litAccommodationName.Text = provider.Name;
                litAccommodationAddress.Text = genericProvider.Address;
                litAccommodationPhone.Text = genericProvider.Email;
                litAccommodationEmail.Text = genericProvider.Phone;
            }

            switch (booking.Type)
            {
                case Affinion.LoyaltyBuild.Model.Product.ProductType.BedBanks:
                    var bbBooking = (data.BedBanksBooking)booking;
                    litNoOfAdults.Text = bbBooking.NoOfAdults.ToString();
                    litNoOfChildren.Text = bbBooking.NoOfChildren.ToString();
                    litNoOfNights.Text = bbBooking.Duration;
                    break;
                case Affinion.LoyaltyBuild.Model.Product.ProductType.Room:
                case Affinion.LoyaltyBuild.Model.Product.ProductType.Package:
                    var roomBooking = (data.RoomBooking)booking;
                    litNoOfAdults.Text = roomBooking.NoOfAdults.ToString();
                    litNoOfChildren.Text = roomBooking.NoOfChildren.ToString();
                    litNoOfNights.Text = roomBooking.NoOfNights.ToString();
                    break;
                case Affinion.LoyaltyBuild.Model.Product.ProductType.Addon:
                    break;
                default:
                    break;
            }

            var product = booking.GetProduct();
            if (product != null)
            {
                litOccupancyType.Text = product.Name;
            }

            litHotelDue.Text = CurrencyHelper.FormatCurrency((booking.ProviderAmount - booking.ProviderAmountPaidOnCheckOut), booking.CurrencyId);
            litCost.Text = CurrencyHelper.FormatCurrency(booking.ProviderAmount, booking.CurrencyId);

            //get outstanding
            _paymentService = ObjectFactory.Instance.Resolve<apiBookingService.IPaymentService>();
            outstandingPayments = _paymentService.GetDuesByOrderlineID(booking.OrderLineId, null);
            var payments = new List<string>();
            if (outstandingPayments != null && outstandingPayments.Count >0)
            {
                HasPendingPayments=true;
                foreach (var item in outstandingPayments)
                {
                    payments.Add(CurrencyHelper.FormatCurrency(item.Amount, item.CurrencyTypeId));
                }
            }
            litOutstanding.Text = string.Join(", ", payments);
        }

        private void LoadNewBookingDetails(data.Booking booking)
        {
             var dateFormat = "dd/MM/yyyy-ddd";
             switch (booking.Type)
             {
                 case Affinion.LoyaltyBuild.Model.Product.ProductType.BedBanks:
                     var bbBooking = (data.BedBanksBooking)booking;
                     litNewBookingNumberOfNights.Text = bbBooking.Duration.ToString();
                     break;
                 case Affinion.LoyaltyBuild.Model.Product.ProductType.Room:
                 case Affinion.LoyaltyBuild.Model.Product.ProductType.Package:
                      var roomBooking = (data.RoomBooking)booking;
                      litNewBookingNumberOfNights.Text = roomBooking.NoOfNights.ToString();
                     break;
                 case Affinion.LoyaltyBuild.Model.Product.ProductType.Addon:
                     break;
                 default:
                     break;
             }
             var provider = booking.GetProvider();
             if (provider != null)
             {
                 var genericProvider = (IGenericProvider)provider;

                 litNewAccomodationName.Text = provider.Name;
                 litNewAccomodationAddress.Text = genericProvider.Address;
             }
            var product = booking.GetProduct();

            if (booking.CheckinDate != DateTime.MinValue)
                litNewBookingArrival.Text = booking.CheckinDate.ToString(dateFormat);
            if (booking.CheckoutDate != DateTime.MinValue)
                litNewBookingDeparture.Text = booking.CheckoutDate.ToString(dateFormat);
            
            litNewBookingNumberOfRooms.Text = "1";
            litNewBookingOccupancyType.Text = product.Name;
            litNewBookingTotalPrice.Text = CurrencyHelper.FormatCurrency(booking.Price + booking.ProcessingFee, booking.CurrencyId);
            litNewBookingDeposit.Text = CurrencyHelper.FormatCurrency(booking.CommissionAmount, booking.CurrencyId);
            litNewBookingProcessingFee.Text = CurrencyHelper.FormatCurrency(booking.ProcessingFee, booking.CurrencyId);
            var checkout = booking.ProviderAmountPaidOnCheckOut + booking.CommissionAmount + booking.ProcessingFee;
            litNewCheckout.Text = CurrencyHelper.FormatCurrency(checkout, booking.CurrencyId);
        }

        

        private void LoadOtherDetails(data.Booking booking)
        {
            litCreatedBy.Text = booking.CreatedBy;
            litBookingMethod.Text = booking.BookingThrough;
            litLastUpdate.Text = booking.UpdateDate.ToString("dd/MM/yyyy-ddd"); ;
            litBookingRef.Text = booking.BookingReferenceNo;
        }

        /// <summary>
        /// 
        /// </summary>
        private void BindSitecoreTexts()
        {
            Item currentItem = Sitecore.Context.Item;
            TextPrint.Item = currentItem;
        }

        

        /// <summary>
        /// transfer click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ButtonConfirmTransaction_Click(object sender, EventArgs e)
        {
            string noteToProvider = CredictCardCheck.CredictCard(NoteToProvider.Value);

            var roomReservationAgeInfo = new List<string>();
            foreach (var item in hdnChildrenAge.Value.Trim().Split(','))
            {
                int age = 0;
                if (int.TryParse(item, out age))
                    roomReservationAgeInfo.Add(age.ToString());
            }

            bookingService = new apiBookingService.BookingService();
            ITransferRequest objTransferRequest = new TransferBookingDetails();
            objTransferRequest.OrderLineid = _orderLineId;
            objTransferRequest.BasketId = Request.Cookies["basketid"].Value;
            objTransferRequest.NoteToProvider = NoteToProvider.Value;
            objTransferRequest.CreatedBy = SitecoreUserProfileHelper.GetItem(UserProfileKeys.CreatedBy);
            objTransferRequest.IsFreeTransfer = false;
            objTransferRequest.BookingReferenceNumber = Booking.BookingReferenceNo;
            objTransferRequest.NoOfChildren = int.Parse(ddlChildrenCount.SelectedValue);
            objTransferRequest.NoOdAdult = int.Parse(ddlAdultCount.SelectedValue);
            objTransferRequest.ChildrenAgeInfo = string.Join(",", roomReservationAgeInfo);

            ValidationResult valresult = bookingService.TransferBooking(objTransferRequest);

            if (valresult.IsSuccess)
            {
                SendMail();
                BasketHelper.ClearBasket();

                Response.Redirect(string.Format("/transferbooking?action=confirm&olid={0}", _newOrderlineId));


            }
            else
                this.ValidationResponse = valresult;

        }

        /// <summary>
        /// action for confirmed transfer
        /// </summary>
        /// <param name="booking"></param>
        private void ConfirmedTransfer(data.Booking booking)
        {
            ShowTransfer = false;

            /*if (booking.DueByCurrencies != null && booking.DueByCurrencies.Count > 0)
                HasPendingPayments = true;*/
        }

        /// <summary>
        /// Set visibility for show transfer
        /// </summary>
        /// <param name="booking"></param>
        private void SetShowTransfer(data.Booking booking)
        {
            ShowTransfer = true;

            //if already cancelled
            if (this.Booking == null || this.Booking.Status == data.BookingStatus.Cancelled || this.Booking.CheckinDate < DateTime.Now.AddDays(-1))
                ShowTransfer = false;
        }

        /// <summary>
        /// Show message
        /// </summary>
        /// <param name="booking"></param>
        private void ShowMessage(data.Booking booking)
        {
            if (booking == null)
            {
                ValidationResponse = new ValidationResult();
                this.ValidationResponse.Message = "Invalid booking details!";
            }
            else if (booking.Status == data.BookingStatus.Cancelled)
            {
                ValidationResponse = new ValidationResult();
                this.ValidationResponse.Message = "Booking already cancelled!";
            }
            else if (this.Booking.CheckinDate < DateTime.Now.AddDays(-1))
            {
                ValidationResponse = new ValidationResult();
                this.ValidationResponse.Message = "Booking past transfer time!";
            }
            else if (this.QueryString("action") == "confirm")
            {
                ValidationResponse = new ValidationResult();
                ValidationResponse.IsSuccess = true;
                ValidationResponse.Message = "Booking successfuly transferred!";
            }
        }

        /// <summary>
        /// Send email
        /// </summary>
        private void SendMail()
        {
            try
            {
                _mailService = new mail.MailService(Booking.ClientId);


                _mailService.GenerateEmail(mail.MailTypes.CustomerCancellation, _orderLineId);
                _mailService.GenerateEmail(mail.MailTypes.ProviderCancellation, _orderLineId);

                _paymentService = ObjectFactory.Instance.Resolve<apiBookingService.IPaymentService>();

                //check for payment dues
                if (_paymentService.GetDuesByOrderlineID(_orderLineId, null).Count > 0)
                {

                    _mailService.GenerateEmail(mail.MailTypes.ProviderProvisionalConfirmation, _newOrderlineId);
                    _mailService.GenerateEmail(mail.MailTypes.ClientProvisionalConfirmation, _newOrderlineId);

                }
                else
                {
                    _mailService.GenerateEmail(mail.MailTypes.ProviderConfirmation, _newOrderlineId);
                    _mailService.GenerateEmail(mail.MailTypes.CustomerConfirmation, _newOrderlineId);
                }
            }
            catch
            {
 
            }
        }
    

        protected void btnPrint_ServerClick(object sender, EventArgs e)
        {
            if (startConversion)
            {
                /// get html of the page
                TextWriter myWriter = new StringWriter();
                HtmlTextWriter htmlWriter = new HtmlTextWriter(myWriter);
                //this.RenderControl(htmlWriter);
                divPrint.RenderControl(htmlWriter);
                // instantiate a html to pdf converter object
                HtmlToPdf converter = new HtmlToPdf();

                // create a new pdf document converting the html string of the page
                PdfDocument doc = converter.ConvertHtmlString(myWriter.ToString(), Request.Url.AbsoluteUri);

                // save pdf document
                doc.Save(Response, true, "Your Booking Confirmation.pdf");

                // close pdf document
                doc.Close();
            }
        }
    }
}