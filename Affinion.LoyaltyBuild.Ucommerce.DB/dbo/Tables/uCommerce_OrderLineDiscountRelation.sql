﻿CREATE TABLE [dbo].[uCommerce_OrderLineDiscountRelation] (
    [OrderLineDiscountRelationId] INT IDENTITY (1, 1) NOT NULL,
    [DiscountId]                  INT NOT NULL,
    [OrderLineId]                 INT NOT NULL,
    CONSTRAINT [PK_uCommerce_OrderLineDiscountRelation] PRIMARY KEY CLUSTERED ([OrderLineDiscountRelationId] ASC),
    CONSTRAINT [FK_uCommerce_OrderLineDiscountRelation_uCommerce_Discount] FOREIGN KEY ([DiscountId]) REFERENCES [dbo].[uCommerce_Discount] ([DiscountId]),
    CONSTRAINT [FK_uCommerce_OrderLineDiscountRelation_uCommerce_OrderLine] FOREIGN KEY ([OrderLineId]) REFERENCES [dbo].[uCommerce_OrderLine] ([OrderLineId])
);

