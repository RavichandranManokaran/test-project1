﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SuperValuTopCallNowSublayout.ascx.cs
/// Sub-system/Module:     Web.ClientPortal(VS project name)
/// Description:           Sublayout for top bar call now link
/// </summary>
namespace Affinion.LoyaltyBuild.Web.Common.Pages
{
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    /// <summary>
    /// Error page for general errors
    /// </summary>
    public partial class GenericErrorPage : System.Web.UI.Page
    {
        /// <summary>
        /// declare variable for themess
        /// </summary>
        public string SelectedTheme { get; set; }

        /// <summary>
        /// Gets or sets the last exception
        /// </summary>
        public Exception LastException { get; set; }

        /// <summary>
        /// Gets or sets the exception message
        /// </summary>
        public string ExceptionMessage { get; set; }

        /// <summary>
        /// Code to exwcute on page load
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The parameters</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //Load the selected theme
            SelectedTheme = ThemeSelector.LoadSelectedTheme();

            this.LastException = Server.GetLastError();
            this.ExceptionMessage = string.Concat(this.LastException.Message, "-", this.LastException.InnerException,"-",this.LastException.StackTrace);
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_PreInit(Object sender, EventArgs e)
        {
           
        }

        //protected void ReturnHome_Click(object sender, EventArgs e)
        //{
        //    Item currentItem = Sitecore.Context.Item;
        //    Item root = ItemHelper.RootItem;
        //    string rootPath = root.Paths.ContentPath;
        //    Response.Redirect("/");
        //}

     
    }
}