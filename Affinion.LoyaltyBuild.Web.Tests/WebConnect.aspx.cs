namespace Affinion.LoyaltyBuild.Web.Tests
{
    #region Using Directives
    using Affinion.LoyaltyBuild.Common.Wizard;
    using Affinion.LoyaltyBuild.Common.Exceptions;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using System;
    using System.Web.Services;
    using System.Web.UI; 
    #endregion

    public partial class WebConnect : Page
    {
        private void Page_Load(object sender, System.EventArgs e)
        {
        }
        /// <summary>
        /// its a web method which will call from sublayout via ajax call
        /// </summary>
        /// <param name="jsonData"></param>
        /// <returns>return string to caller</returns>
        [WebMethod]
        public static string ProcessAjax(string jsonData)
        {

            // create a singleton object
            DataTransfer dataTransfer = DataTransfer.Instance;
            string result = string.Empty;
            //set the Json string property with request data
            dataTransfer.JSonString = jsonData;

            try
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientSetup, "wizard module Ajax call from sublayout to webconnect started");

                if (jsonData != string.Empty)
                {
                    //{string[3]}
                    //[0]: "twitter:ttt"
                    //[1]: "facebook:fff"
                    //[2]: "gplus:ggg"

                    // Split the string using "," and will return set of key value pair
                    var KeyValPair = jsonData.Replace('{', ' ').Replace('}', ' ').Replace("'", " ").Replace(" ", "").Split(',');
                    foreach (var item in KeyValPair)
                    {
                        // split the item using ":" to get key and value and store them into DictionaryData      

                        if (dataTransfer.DictionaryData.ContainsKey(item.Split(':')[0]))
                        {
                            dataTransfer.DictionaryData[item.Split(':')[0]] = item.Split(':')[1];
                            result = "Value Updated";
                        }
                        else
                        {
                            dataTransfer.DictionaryData.Add(item.Split(':')[0], item.Split(':')[1]); 
                            result = "Success";
                        }

                        Diagnostics.Trace(DiagnosticsCategory.ClientSetup, "wizard module Ajax call from sublayout to webconnect completed");
                    }
                }
                else
                    result = "Fail";

            }
            catch (ArgumentException ex)
            {
                ///Catch argument exceptions
                ///First parameter : Module
                ///Second parameter: Exception
                ///Third parameter : Object logging
                Diagnostics.WriteException(DiagnosticsCategory.ClientSetup, ex, dataTransfer);
            }
            catch (AffinionException ex)
            {
                ///Catch affinion exceptions
                Diagnostics.WriteException(DiagnosticsCategory.ClientSetup, ex, dataTransfer);
            }
            catch (Exception ex)
            {
                ///Catch general exceptions
                Diagnostics.WriteException(DiagnosticsCategory.ClientSetup, ex, dataTransfer);
            }
            return result;
        }
    }
}