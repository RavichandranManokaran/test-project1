﻿CREATE TABLE [dbo].[uCommerce_PercentOffOrderTotalAward] (
    [PercentOffOrderTotalAwardId] INT             NOT NULL,
    [PercentOff]                  DECIMAL (18, 2) NOT NULL,
    CONSTRAINT [PK_PercentOffOrderTotalAward] PRIMARY KEY CLUSTERED ([PercentOffOrderTotalAwardId] ASC)
);

