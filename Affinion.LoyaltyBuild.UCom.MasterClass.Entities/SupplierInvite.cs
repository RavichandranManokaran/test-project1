﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class SupplierInvite : IEntity
    {
        private int? _hashCode;

        // Methods
        public static IQueryable<SupplierInvite> All()
        {
            return GetRepo().Select();
        }

        public virtual void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<SupplierInvite, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            SupplierInvite objA = obj as SupplierInvite;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<SupplierInvite, bool>> expression)
        {
            return All().Any<SupplierInvite>(expression);
        }

        public static IList<SupplierInvite> Find(Expression<Func<SupplierInvite, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<SupplierInvite>();
        }

        public static SupplierInvite FirstOrDefault(Expression<Func<SupplierInvite, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<SupplierInvite>();
        }

        public static SupplierInvite Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<SupplierInvite> GetRepo()
        {
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<SupplierInvite>>();
            
        }

        public virtual void Save()
        {
            try
            {
                GetRepo().Save(this);
            }
            catch (Exception ex )
            {
                throw ex;
            }
        }

        public static SupplierInvite SingleOrDefault(Expression<Func<SupplierInvite, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties

        public virtual int SupplierInviteId { get; protected set; }

        public virtual int Id
        {
            get
            {
                return this.SupplierInviteId;
            }
        }

        public virtual string Name { get; set; }

        public virtual string Sku { get; set; }

        public virtual bool IsSubscribe { get; set; }

        public virtual int CampaignItem { get; set; }

        public virtual string Offerinformation { get; set; }
    }
}
