﻿CREATE TABLE [dbo].[uCommerce_OrderLineCampaignRelation] (
    [OrderLineCampaignRelationId] INT IDENTITY (1, 1) NOT NULL,
    [OrderId]                     INT NOT NULL,
    [OrderLineId]                 INT NULL,
    [DiscountId]                  INT NOT NULL,
    [CampaignItemId]              INT NOT NULL,
    CONSTRAINT [PK_uCommerce_OrderLineCampaign] PRIMARY KEY CLUSTERED ([OrderLineCampaignRelationId] ASC)
);

