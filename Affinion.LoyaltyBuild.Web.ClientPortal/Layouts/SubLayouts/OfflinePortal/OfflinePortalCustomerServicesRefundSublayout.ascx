﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalCustomerServicesRefundSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalCustomerServiceRefundSublayout" %>
<html>
    <head>
        <script src="scripts/jquery.min.js"></script>
      <script src="bootstrap_assets/javascripts/bootstrap.min.js"></script>
      <script src="scripts/jquery-1.10.2.js"></script>
      <script src="scripts/jquery-ui.js"></script>   
      <link rel="stylesheet" href="styles/bootstrap.min.css">
      <link href="styles/responsiveNav.css" rel="stylesheet" type="text/css" />
      <link rel="stylesheet" href="styles/affinion.min.css">
      <link rel="stylesheet" href="styles/customerService.min.css">   
      <link rel="stylesheet" href="styles/theme1/theme1.css">
      <link rel="stylesheet" href="styles/fonts.css">
      <link rel="stylesheet" href="styles/font-awesome.min.css">
      <link rel="stylesheet" href="styles/img-gllary.css">
      <link rel="stylesheet" href="styles/jquery-ui.css">    
      <script src="scripts/script.js"></script>
      <script src="scripts/fotorama.js"></script>
      <script src="scripts/images.js"></script>
      <script type="text/javascript" src="scripts/jssor.slider-20.mini.js"></script>
      <script type="text/javascript" src="scripts/detail-gallary.js"></script>
       <link rel="stylesheet" href="styles/bootstrap-social.css">   
      <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
	  
	 <link href = "http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet" />
      <script src = "http://code.jquery.com/jquery-1.10.2.js"></script>
      <script src = "http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script> 
	  
	  
	    <style>
         #tabs-1{font-size: 14px;}
         .ui-widget-header {
            background:none;
            border: none;
            color: #FFFFFF;
            font-weight: bold;
			padding:0;
         }
		 .pric-eu
		 {
		   float:left;
		   width:50%;
		 }
		 .pay_optn
		 {
		   float:right;
		   width:50%;
		   padding-left:8px;
		   border-left:1px solid;
		 }
		 .panel-heading.pay_bg
		 {
		   background:#f5f5f5;
		   color:#0d7ec1;
		 }
		 .ui-tabs .ui-tabs-panel,
		 .ui-widget-content
		 {
		   background:none;
		   border:none;
		 }
		 .ui-tabs .ui-tabs-panel
		 {
		    border:1px solid #ccc;
			background:#f2f9fc;
		 }
		 .ui-tabs,.ui-tabs .ui-tabs-nav
		 {
		   padding:0;
		 }
		 .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active
		 {
		   border:1px solid #ccc !important;
		   border-bottom:none !important;
		   text-decoration:none;
		   background:#f2f9fc !important;
		   }
		 .ui-state-active a, .ui-state-active a:link, .ui-state-active a:visited
		 {
		   color:#1c94c4 !important;
		   text-decoration:none;
		   outline:none;
		 }
		 .ui-state-hover, .ui-widget-content .ui-state-hover, .ui-widget-header .ui-state-hover, .ui-state-focus, .ui-widget-content .ui-state-focus, .ui-widget-header .ui-state-focus
		 {
		   background:#026199;
		   border:1px solid #ccc !important;
		   outline:none;
		 }
		 #Customerservicerefund{
		    margin-top:20px;
		 }
	    </style>
	  
      </head>
      <body>
<div class="container-outer width-full overflow-hidden-att">
        <div class="container-outer-inner width-full header-top-bar">
    <div class="container"> 
            <!-- The justified navigation menu is meant for single line per list item.
                  Multiple lines will require custom code not provided by Bootstrap. -->
            <div class="row coomon-outer-padding">
                  <div class="col-md-6 col-xs-6">
                     <div class="logo">
                        <a href="home.html"><img src="images/LBLogo-home.png"  class="img-responsive" alt="logo" ></a>
                     </div>
                  </div>
                  <div class="col-md-6 col-xs-6">
                     
                  </div>
                  
               </div>
          </div>
  </div>
      </div>
<div class="body-container">
        <div class="container">
        <div class="form-outer">
		
		<div class="col-md-12">
    <div class="panel panel-default" style="border:none;">
		<div>
			<p>Current Balance(s) for Booking Ref 9826165500002447398</p>
		</div>
	     <div class="panel-heading pay_bg"><span class="pric-eu"><strong>Total Amount </strong> </span>
		 <span class="pay_optn">Payment Method</div>	
		 <div class="panel-heading"><span class="pric-eu"><strong>&#8364; 0.00 </strong> </span>
		 <span class="pay_optn"><u>Pay Now</u></div>
  <div class="panel-body">

<div id ="tabs-1">
         <ul>
            <li><a href = "#tabs-2">Payment Information</a></li>
            <!--<li><a href = "#tabs-3">Booking Due(e)</a></li>-->
            <li><a href = "#tabs-4">Booking Due</a></li>
			<li><a href = "#tabs-5">Payments</a></li>
         </ul>
			
         <div id = "tabs-2">
            <p>Neque porro quisquam est qui dolorem ipsum quia dolor sit 
               amet, consectetur, adipisci velit... </p>
         </div>
			
        <!-- <div id = "tabs-3">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
               sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
               Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
               nisi ut aliquip ex ea commodo consequat. </p>
         </div> -->
			
        <div id="tabs-4">
			<!-- jayapriya start code-->
			<div class="row margin-row">
				<div class="col-sm-5"><span class="bask-form-titl font-size-12">Currency:<span class="accent58-f"> *</span></span></div>
				<div class="col-sm-7"> 
					<select class="form-control font-size-12">
						<option>Euro</option>
						<option>Dollar</option>
						<option>Rupee</option>
						<option>4</option>
					</select>
				</div>
			</div>
			<div class="row margin-row">
				<div class="col-sm-5"><span class="bask-form-titl font-size-12">Payment Method:<span class="accent58-f"> *</span></span></div>
				<div class="col-sm-7">
					<select class="form-control font-size-12">
						<option>Credit</option>
						<option>Laser</option>
						<option>Debit Card</option>
					</select>
				</div>
			</div>
			<div class="row margin-row">
				<div class="col-sm-5"><span class="bask-form-titl font-size-12">Payment Reason:<span class="accent58-f"> *</span></span></div>
				<div class="col-sm-7">
					<select class="form-control font-size-12">
						<option>No Show Fee</option>
						<option>&nbsp;</option>
					</select>
				</div>
			</div>
			<div class="row margin-row">
				<div class="col-sm-5"><span class="bask-form-titl font-size-12">Total Amount:</span></div>
				<div class="col-sm-7"><input type="text" placeholder="100.00" class="form-control font-size-12"></div>
			</div>
			<div class="row margin-row">
				<div class="col-sm-5"><span class="bask-form-titl font-size-12">Relevant commission Amount:</span></div>
				<div class="col-sm-7"><input type="text" placeholder="" class="form-control font-size-12"></div>
			</div>
			<div class="row margin-row">
				<div class="col-sm-5"><span class="bask-form-titl font-size-12">Relevant Transfer Amount:</span></div>
				<div class="col-sm-7"><input type="text" placeholder="" class="form-control font-size-12"></div>
			</div>
			<div class="row margin-row">
				<div class="col-sm-5"><span class="bask-form-titl font-size-12">Relevant Cancellation Amount:</span></div>
				<div class="col-sm-7"><input type="text" placeholder="" class="form-control font-size-12"></div>
			</div>
			<div class="row margin-row">
				<div class="col-sm-5"><span class="bask-form-titl font-size-12">Client Amount:</span></div>
				<div class="col-sm-7"><input type="text" placeholder="" class="form-control font-size-12"></div>
			</div>
			<div class="row margin-row">
				<div class="col-sm-5"><span class="bask-form-titl font-size-12">Provider Amount:</span></div>
				<div class="col-sm-7"><input type="text" placeholder="" class="form-control font-size-12"></div>
			</div>
			<div class="row margin-row">
				<div class="col-sm-5"><span class="bask-form-titl font-size-12">For collection By Provider Amount:</span></div>
				<div class="col-sm-7"><input type="text" placeholder="" class="form-control font-size-12"></div>
			</div>
			<div class="row margin-row">
				<div class="col-sm-5"><span class="bask-form-titl font-size-12">Note:</span></div>
				<div class="col-sm-7"><input type="text" placeholder="" class="form-control font-size-12"></div>
			</div>
			<div class="row margin-row">
			<div class="col-sm-3"></div>
			<div class="col-sm-9">
				<button type="submit" class="btn btn-default add-break font-size-12 pull-right"><span class="glyphicon glyphicon-refresh"></span> Reset</button> 
				<button type="submit" class="btn btn-default add-break font-size-12 pull-right"><span class="glyphicon glyphicon-plus"></span> Add Due</button> 
				<button type="submit" class="btn btn-default add-break font-size-12 pull-right"><span class="glyphicon glyphicon-info-sign"></span> Pricing Info</button> 	
			</div>
			</div>
			<!-- jayapriya End code-->
	    </div>

		<div id="tabs-5">
			<div class="row margin-row">
				<div class="col-sm-5"><span class="bask-form-titl font-size-12">Currency:<span class="accent58-f"> *</span></span></div>
				<div class="col-sm-7"> 
					<select class="form-control font-size-12">
						<option>Euro</option>
						<option>Dollar</option>
						<option>Rupee</option>
						<option>4</option>
					</select>
				</div>
			</div>
			<div class="row margin-row">
				<div class="col-sm-5"><span class="bask-form-titl font-size-12">Payment Method:<span class="accent58-f"> *</span></span></div>
				<div class="col-sm-7">
					<select class="form-control font-size-12">
						<option>Cheque</option>
						<option>DD</option>
						<option>Cash</option>
						<option>4</option>
					</select>
				</div>
			</div>
			<div class="row margin-row">
				<div class="col-sm-5"><span class="bask-form-titl font-size-12">Total Amount:</span></div>
				<div class="col-sm-7"><input type="text" placeholder="100.00" class="form-control font-size-12"></div>
			</div>
			<div class="row margin-row">
				<div class="col-sm-5"></div>
				<div class="col-sm-7">
					<button type="submit" class="btn btn-default add-break font-size-12 pull-right"><span class="glyphicon glyphicon-credit-card"></span>&nbsp;&nbsp;Submit</button> 
					<!--<button type="submit" class="btn btn-default add-break font-size-12"><span class="glyphicon glyphicon-refresh"></span> Reset</button> -->
				</div>
			</div>
			<div id="Customerservicerefund">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Refund by Cheque</h4>
					</div>
					<div class="panel-body alert-info">  
						<div class="col-sm-12 no-padding-m">
							<div class="content_bg_inner_box alert-info">
								<div class="col-sm-6 no-padding-m">
									<div class="row margin-row">
										<div class="col-sm-5"><span class="bask-form-titl font-size-12">Currency:<span class="accent58-f"> *</span></span></div>
										<div class="col-sm-7"> 
											<select class="form-control font-size-12" >
												<option>Euro</option>
												<option>2</option>
												<option>3</option>
												<option>4</option>
											</select>
										</div>
									</div>
									<div class="row margin-row">
										<div class="col-sm-5"><span class="bask-form-titl font-size-12">Payment Method:<span class="accent58-f"> *</span></span></div>
										<div class="col-sm-7">
											<select class="form-control font-size-12" >
												<option>Cheque</option>
												<option>2</option>
												<option>3</option>
												<option>4</option>
											</select>
										</div>
									</div>
								</div>
							<div class="col-sm-6 no-padding-m">
								<div class="row margin-row">
									<div class="col-sm-5"><span class="bask-form-titl font-size-12">Total Amount:</span></div>
									<div class="col-sm-7"><input type="text"  class="form-control font-size-12"  ></div>
								</div>
								<div class="row margin-row">
									<div class="col-sm-6"></div>
									<div class="col-sm-6">
										<button class="btn btn-default add-break pull-right" type="submit"><span class="glyphicon glyphicon-refresh"></span>&nbsp;&nbsp;Reset</button>
										<button class="btn btn-default add-break pull-right search-btn" type="submit"><span class="glyphicon glyphicon-credit-card"></span>&nbsp;&nbsp;Pay Now</button>
									</div>
								</div>
							</div>
							</div>
						</div>
					</div>           
				</div>
				
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Refund by Credit Card</h4>
					</div>
					<div class="panel-body alert-info">  
						<div class="col-sm-12 no-padding-m">
							<div class="content_bg_inner_box alert-info">
								<div class="col-sm-6 no-padding-m">
									<div class="row margin-row">
										<div class="col-sm-5"><span class="bask-form-titl font-size-12">Currency:<span class="accent58-f"> *</span></span></div>
										<div class="col-sm-7"> 
											<select class="form-control font-size-12" >
												<option>Euro</option>
												<option>2</option>
												<option>3</option>
												<option>4</option>
											</select>
										</div>
									</div>
									<div class="row margin-row">
										<div class="col-sm-5"><span class="bask-form-titl font-size-12">Payment Method:<span class="accent58-f"> *</span></span></div>
										<div class="col-sm-7">
											<select class="form-control font-size-12" >
												<option>Cheque</option>
												<option>2</option>
												<option>3</option>
												<option>4</option>
											</select>
										</div>
									</div>
									<div class="row margin-row">
										<div class="col-sm-5"><span class="bask-form-titl font-size-12">Commission Amount:</span></div>
										<div class="col-sm-7">
											<select class="form-control font-size-12" >
												<option>Double Booking</option>
												<option>2</option>
												<option>3</option>
												<option>4</option>
											</select>
										</div>
									</div>
								</div>
							
								<div class="col-sm-6 no-padding-m">
									<div class="row margin-row">
										<div class="col-sm-5"><span class="bask-form-titl font-size-12">Provider Amount:</span></div>
										<div class="col-sm-7">
											<input type="text"  class="form-control font-size-12" >
										</div>
									</div>
									<div class="row margin-row">
										<div class="col-sm-5"><span class="bask-form-titl font-size-12">Commission Amount:</span></div>
										<div class="col-sm-7">
											<input type="text"  class="form-control font-size-12" >
										</div>
									</div>
									<div class="row margin-row">
										<div class="col-sm-5"><span class="bask-form-titl font-size-12">Processing Fee Amount:</span></div>
										<div class="col-sm-7">
											<input type="text"  class="form-control font-size-12"  >
										</div>
									</div>
								</div>
							
								<div class="col-sm-12">
									<div class="row margin-row"> 
										<div class="table-responsive">
											<table class="table tbl-calendar">
												<thead>
													<tr>
														<th>Date Created</th>
														<th>Realex ID</th>
														<th>Refundable</th>
														<th>Total Amount</th> 
													</tr>
												</thead>
												<tbody>
													<tr style="background-color: rgb(255, 255, 255);">
														<td><span>02/12/2015</span></td>
														<td><span><a href="#">adfyvb123-ewufi-458-sdh23</a></span></td>
														<td><span>$22.56</span></td>
														<td><span>$18.66</span></td> 
													</tr>
												</tbody>
											</table>
										</div> 
									</div> 
									<div class="col-sm-6"></div>               
									<div class="col-sm-6">
										<div class="row margin-row">
											<div class="col-sm-6"></div>
											<div class="col-sm-6">
												<button class="btn btn-default add-break pull-right font-size-12" type="submit"><span class="glyphicon glyphicon-refresh"></span>&nbsp;&nbsp;Reset</button> 
												<button class="btn btn-default add-break pull-right font-size-12" type="submit"><span class="glyphicon glyphicon-credit-card"></span>&nbsp;&nbsp;Refund</button> 
											</div>
										</div>           
									</div>   
								</div>      
							</div>
						</div>
					</div>           
				</div>
			</div>
		</div>

		  
      </div>
	  
	</div>  
	  
</div>
</div>	  
	  
</div>



       </div>
	   
	   
	   
	   
    </div>
<!-- Site footer -->





       
       

    
</body>


  <script>
      jQuery(document).ready(function () {
          jQuery("table tr:even").css({ "background-color": "#fafefe", "border-top": "solid 1px #cff2f6", "border-bottom": "solid 1px #cff2f6" });
          jQuery("table tr:odd").css("background-color", "#fff");

      });
      $(function () {
          $("#check-in-date").datepicker();
      });
      $(function () {
          $("#check-out-date").datepicker();
      });
      </script>
	  
	  <script>
	      $(function () {
	          $("#tabs-1").tabs();
	      });
      </script>
</html>

