﻿--********************************************************************************************************************************************************************************
--Create procedure script For PasswordHistories
--********************************************************************************************************************************************************************************

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LB_SelectLoginAudit]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[LB_SelectLoginAudit]
GO


CREATE PROCEDURE [dbo].[LB_SelectLoginAudit]
       @UserName    varchar(200)       
AS
BEGIN 
     SET NOCOUNT ON 

     SELECT TOP 6 [IsLoginSucess]           
     FROM   [dbo].[LoginAudit]   
     WHERE  
            UserName = @UserName  Order By [LoggedInDate] DESC

END 

GO