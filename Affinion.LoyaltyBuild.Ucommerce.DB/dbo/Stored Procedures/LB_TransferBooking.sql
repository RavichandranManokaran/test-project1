﻿CREATE PROCEDURE [dbo].[LB_TransferBooking] 
	-- Add the parameters for the stored procedure here
	@Note NVARCHAR(MAX)=NULL, 
	@OrderLineId INT=NULL,
	@NoOfAdults INT=NULL,
	@NoOfChildren INT=NULL,
	@ChildrenAgeInfo VARCHAR(50)=NULL,
	@CartId VARCHAR(50)=NULL,
	@CreatedBy VARCHAR(50)=NULL,
	@NewOrderLineId INT OUT,
	@BookingNo VARCHAR(50)=NULL,
	@NewProviderAmount Money=NULL,
	@Paymentid int=NULL
AS
BEGIN
	
	DECLARE @OrderReference VARCHAR(50)
	DECLARE @NewOrderId INT
	DECLARE @OrderId INT
	DECLARE @CustomerId INT

	DECLARE @OldBookingId UNIQUEIDENTIFIER
	SET @OldBookingId=(SELECT TOP 1 BookingId FROM LB_BookingInfo WHERE BookingNo=@BookingNo)
	

	SELECT @OrderId = PO.OrderId, @CustomerId = PO.CustomerId 
	FROM uCommerce_OrderLine OL
		INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId = PO.OrderId
	WHERE OrderLineId = @OrderLineId

	--get order reference
	--SELECT @OrderReference = ISNULL(BookingReferenceId,'') + '-' + CAST((ISNULL(LastSequence,0)+1) AS VARCHAR(5))
	--	FROM LB_CustomerAttribute
	--	WHERE CustomerId = @CustomerId
	--UPDATE LB_CustomerAttribute SET LastSequence = (ISNULL(LastSequence,0)+1) WHERE CustomerId = @CustomerId

	SELECT TOP 1  
		@NewOrderLineId =  BI.OrderLineId,
		@NewOrderId = PO.OrderId
	FROM
		LB_BookingInfo BI 
		INNER JOIN uCommerce_OrderLine OL ON BI.OrderLineId = OL.OrderLineId
		INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId = PO.OrderId
	WHERE PO.BasketId = @CartId
	
	--Update no of adults and child and order reference
	UPDATE LB_BookingInfo SET BookingNo = @BookingNo ,BookingId = @OldBookingId WHERE OrderLineId = @NewOrderLineId
	
	--update no of adults
	IF EXISTS (SELECT 1 FROM uCommerce_OrderProperty where [Key]='_NoOfAdult' and OrderLineId=@NewOrderLineId)
		Update Ucommerce_OrderProperty Set Value=@NoOfAdults where [Key]='_NoOfAdult' and OrderLineId=@NewOrderLineId
	ELSE
		INSERT INTO Ucommerce_OrderProperty (OrderId, OrderLineId, [Key], [Value]) VALUES (@NewOrderId, @NewOrderLineId, '_NoOfAdult', @NoOfAdults)
	--update no of children
	IF EXISTS (SELECT 1 FROM uCommerce_OrderProperty where [Key]='_NoOfChildren' and OrderLineId=@NewOrderLineId)
		Update Ucommerce_OrderProperty Set Value=@NoOfChildren where [Key]='_NoOfChildren' and OrderLineId=@NewOrderLineId
	ELSE
		INSERT INTO Ucommerce_OrderProperty (OrderId, OrderLineId, [Key], [Value]) VALUES (@NewOrderId, @NewOrderLineId, '_NoOfChildren', @NoOfChildren)
	--update age
	IF(@ChildrenAgeInfo IS NOT NULL and @ChildrenAgeInfo <> '')
	BEGIN
		IF EXISTS (SELECT 1 FROM uCommerce_OrderProperty where [Key]='_ChildrenAge' and OrderLineId=@NewOrderLineId)
			Update Ucommerce_OrderProperty Set Value=@ChildrenAgeInfo where [Key]='_ChildrenAge' and OrderLineId=@NewOrderLineId
		ELSE
			INSERT INTO Ucommerce_OrderProperty (OrderId, OrderLineId, [Key], [Value]) VALUES (@NewOrderId, @NewOrderLineId, '_ChildrenAge', @ChildrenAgeInfo) 
	END

	--Taking BookingID from LB_BookingInfo table against particular orderLineID
    Declare @BookingID VARCHAR(MAX)
	 SELECT @BookingID=bookingid from LB_BookingInfo where OrderLineId=@OrderLineId
	-- Insert statements for procedure here	 
	INSERT INTO LB_ReservationNotes 
		(
		NoteTypeId,
		OrderLineId,
		Note,
		CreatedBy,
		CreatedDate,
		BookingId
		) 
	VALUES 
		(
		1, --Note TypeID 1 for InternalProvider
		@NewOrderLineId,
		@Note,
		@CreatedBy,
		GETDATE(),
		@Bookingid
		);

	--Update Customer
	UPDATE uCommerce_PurchaseOrder SET CustomerId = @CustomerId WHERE OrderId = @NewOrderId

	--insert order address
	INSERT INTO [dbo].[uCommerce_OrderAddress]
		([FirstName]
		,[LastName]
		,[EmailAddress]
		,[PhoneNumber]
		,[MobilePhoneNumber]
		,[Line1]
		,[Line2]
		,[PostalCode]
		,[City]
		,[State]
		,[CountryId]
		,[Attention]
		,[CompanyName]
		,[AddressName]
		,[OrderId]
		,[Line3]
		,[County]
		,[Branch]
		,[IsEmail]
		,[IsTerms])
	SELECT [FirstName]
		,[LastName]
		,[EmailAddress]
		,[PhoneNumber]
		,[MobilePhoneNumber]
		,[Line1]
		,[Line2]
		,[PostalCode]
		,[City]
		,[State]
		,[CountryId]
		,[Attention]
		,[CompanyName]
		,[AddressName]
		,@NewOrderId
		,[Line3]
		,[County]
		,[Branch]
		,[IsEmail]
		,[IsTerms]
	FROM [dbo].[uCommerce_OrderAddress]
	WHERE OrderId = @OrderId

			
	UPDATE LB_BookingInfo 
	SET BookingNo = @BookingNo 
	WHERE OrderLineId = @OrderlineId


	
	

	--Cancelling for existing OrderLineId
	EXEC [LB_UpdateAvailability] @OrderLineId = @OrderLineId, @IsCancel = 1

	--Adding for new OrderLineId
	EXEC [LB_UpdateAvailability] @OrderLineId = @NewOrderLineId

	UPDATE uCommerce_PurchaseOrder
	SET BasketId = '00000000-0000-0000-0000-000000000000'
	WHERE BasketId = @CartId

	--Insert payment method for new order line id
	DECLARE @PaymentMethod VARCHAR(50)

	SET @PaymentMethod=(SELECT Value FROM uCommerce_OrderProperty WHERE OrderLineID = @OrderLineId AND [KEY]='PaymentMethod')

	UPDATE uCommerce_OrderProperty SET Value=@PaymentMethod
	WHERE 
	OrderId=@NewOrderId AND 
	OrderLineID = @NewOrderLineId AND
	[KEY]='PaymentMethod'
	
END

