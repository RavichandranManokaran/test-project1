﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[LB_GetCommunicationHistorybyHistoryId] 
	@historyId INT 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT CH.[HistoryId]
      ,CT.[Name]
      ,CH.[OrderLineId]
      ,CH.[CreatedDate]
	  ,CH.[From]
	  ,CH.[To]
	  ,CH.[EmailSubject]
      ,CH.[EmailText]
      ,CH.[EmailHtml]
	FROM [dbo].[LB_CommunicationHistory] CH
		INNER JOIN [dbo].[LB_CommunicationTypes] CT ON CT.CommunicationTypeId=CH.CommunicationTypeId
	WHERE CH.HistoryId=@historyId
END

