﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           GridBlockSublayout.ascx.cs
/// Sub-system/Module:     Web.Common(VS project name)
/// Description:           Sublayout for common grid block which supports the bootstrap grid system
/// </summary>
namespace Affinion.LoyaltyBuild.Web.Common.Layouts.SubLayouts.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    /// <summary>
    /// Sublayout for common grid block which supports the bootstrap grid system
    /// </summary>
    public partial class GridBlockSublayout : System.Web.UI.UserControl
    {
        /// <summary>
        /// This function includes the code which needs to run on page load
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The parameter</param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}