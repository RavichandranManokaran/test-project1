﻿
CREATE PROCEDURE [dbo].[uCommerce_GetTotalSales]
(
	@StartDate DATETIME, -- NULLABLE
	@EndDate DATETIME, -- NULLABLE
	@ProductCatalogGroupId INT -- NULLABLE
)
AS
	SET NOCOUNT ON
	SELECT 
		dbo.uCommerce_ProductCatalogGroup.Name,
		SUM(ISNULL(dbo.uCommerce_PurchaseOrder.OrderTotal, 0)) Revenue,
		SUM(ISNULL(dbo.uCommerce_PurchaseOrder.VAT, 0)) [VATTotal],
		SUM(ISNULL(dbo.uCommerce_PurchaseOrder.TaxTotal, 0)) [TaxTotal],
		SUM(ISNULL(dbo.uCommerce_PurchaseOrder.ShippingTotal, 0)) [ShippingTotal],
		ISNULL(dbo.uCommerce_Currency.ISOCode, '-') Currency
	FROM
		dbo.uCommerce_PurchaseOrder
		JOIN dbo.uCommerce_Currency ON dbo.uCommerce_Currency.CurrencyId = dbo.uCommerce_PurchaseOrder.CurrencyId
		RIGHT JOIN dbo.uCommerce_ProductCatalogGroup ON dbo.uCommerce_ProductCatalogGroup.ProductCatalogGroupId = dbo.uCommerce_PurchaseOrder.ProductCatalogGroupId
	WHERE
		(
			dbo.uCommerce_PurchaseOrder.CreatedDate BETWEEN @StartDate AND @EndDate
			OR
			(
				@StartDate IS NULL
				AND
				@EndDate IS NULL
			)
			OR
			dbo.uCommerce_PurchaseOrder.CreatedDate IS NULL
		)
		AND
		(
			dbo.uCommerce_ProductCatalogGroup.ProductCatalogGroupId = @ProductCatalogGroupId
			OR
			@ProductCatalogGroupId IS NULL
		)
		AND
		(
			NOT dbo.uCommerce_PurchaseOrder.OrderStatusId IN (1, 4, 7) -- Basket, -- Cancelled Order, -- Cancelled
			OR
			dbo.uCommerce_PurchaseOrder.OrderStatusId IS NULL
		)
	GROUP BY
		dbo.uCommerce_ProductCatalogGroup.Name,
		dbo.uCommerce_Currency.ISOCode

