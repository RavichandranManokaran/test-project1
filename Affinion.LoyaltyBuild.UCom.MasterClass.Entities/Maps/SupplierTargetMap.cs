﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Maps
{

    public class SupplierTargetMap : SubclassMap<SupplierTarget>
    {
        // Methods
        public SupplierTargetMap()
        {
            base.Table("LB_SupplierTarget");
            base.KeyColumn("SupplierTargetId");
            base.Map(x => x.SupplierTargetId).ReadOnly();
            base.Map(x => x.Name).Not.Nullable();
            base.Map(x => x.SupplierId).Nullable().Generated.Never();
        }
    }
}

