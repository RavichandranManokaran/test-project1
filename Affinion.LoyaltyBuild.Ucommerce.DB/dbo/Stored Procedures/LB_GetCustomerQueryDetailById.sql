﻿CREATE PROCEDURE [dbo].[LB_GetCustomerQueryDetailById]
(
	@QueryId int
) 
AS 
BEGIN

SELECT  top 1 isnull(cq.OrderLineId,0) OrderLineId ,cq.PartnerId,cq.ProviderId,cq.QueryId,cq.querytypeid,cq.CampaignId,cq.Content,cq.CreatedBy,cq.CreatedDate,cq.CustomerId,ISNULL(cq.issolved,0) as issolved,
 case when ISNULL(cqa.AttachmentId,0) > 0 then 'true' else 'false' end as HasAttachment ,
ISNULL(CP.Value,'') BookingReference
FROM  LB_CustomerQuery cq
LEFT JOIN [dbo].LB_CustomerProperty CP ON cq.CustomerId = CP.CustomerId And CP.[Key] = '_BookingReference'
LEFT join [dbo].[LB_CustomerQuery_Attachments] cqa on  cq.QueryId = cqa.QueryId
WHERE cq.QueryId = @QueryId

END
