﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    public class RoomDetail
    {
        public RoomDetail()
        {
            AvailabilityData = new List<RoomAvailabilityByDate>();
        }

        public string Name { get; set; }
        public List<RoomAvailabilityByDate> AvailabilityData { get; set; }
        
        public int Month { get; set; }

        public string ProductId { get; set; }

        public string ProductSku { get; set; }

        public int NumberOfAdult { get; set; }

        public int NumberOfChild { get; set; }


        public decimal AdditionalPrice { get; set; }

        public int ProductTye { get; set; }
        
        public string Addons { get; set; }

        public string pkgroom { get; set; }

        public string DependsOn { get; set; }
    }


}
