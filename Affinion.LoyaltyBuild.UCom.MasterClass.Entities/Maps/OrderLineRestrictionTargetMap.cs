﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Maps
{
    public class OrderLineRestrictionTargetMap : SubclassMap<OrderLineRestrictionTarget>
    {
        public OrderLineRestrictionTargetMap()
        {
            base.Table("LB_OrderLineRestrictionTarget");
            base.KeyColumn("OrderLineRestrictionTargetId");
            base.Map(x => x.OrderLineRestrictionTargetId).ReadOnly();
            base.Map(x => x.NoOfOrderLine).Not.Nullable();
        }
    }

}
