﻿CREATE TABLE [dbo].[ArchivedItems] (
    [RowId]      UNIQUEIDENTIFIER NOT NULL,
    [ArchivalId] UNIQUEIDENTIFIER NOT NULL,
    [ItemId]     UNIQUEIDENTIFIER NOT NULL,
    [Name]       NVARCHAR (256)   NOT NULL,
    [TemplateID] UNIQUEIDENTIFIER NOT NULL,
    [MasterID]   UNIQUEIDENTIFIER NOT NULL,
    [ParentID]   UNIQUEIDENTIFIER NOT NULL,
    [Created]    DATETIME         NOT NULL,
    [Updated]    DATETIME         NOT NULL,
    [DAC_Index]  INT              NULL
);


GO
CREATE CLUSTERED INDEX [DAC_Clustered]
    ON [dbo].[ArchivedItems]([DAC_Index] ASC);


GO
CREATE NONCLUSTERED INDEX [ndx_ArchivalId]
    ON [dbo].[ArchivedItems]([ArchivalId] ASC);

