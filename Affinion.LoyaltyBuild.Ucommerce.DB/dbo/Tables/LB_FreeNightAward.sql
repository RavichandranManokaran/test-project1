﻿CREATE TABLE [dbo].[LB_FreeNightAward](
	[FreeNightAwardId] [int] NOT NULL,
	[NoOfNight] [int] NOT NULL,
 CONSTRAINT [PK_LB_FreeNightAward] PRIMARY KEY CLUSTERED 
(
	[FreeNightAwardId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[LB_FreeNightAward]  WITH CHECK ADD  CONSTRAINT [FK_LB_FreeNightAward_uCommerce_Award] FOREIGN KEY([FreeNightAwardId])
REFERENCES [dbo].[uCommerce_Award] ([AwardId])
GO

ALTER TABLE [dbo].[LB_FreeNightAward] CHECK CONSTRAINT [FK_LB_FreeNightAward_uCommerce_Award]
GO


