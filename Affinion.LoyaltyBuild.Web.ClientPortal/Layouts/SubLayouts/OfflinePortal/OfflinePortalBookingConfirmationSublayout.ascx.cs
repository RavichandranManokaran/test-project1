﻿//<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           BookingConfirmationSublayout.ascx.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal
/// Description:           SubLayout for BookingConfirmationSublayout generation
/// </summary>


namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    #region Using Directives
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Affinion.Loyaltybuild.BusinessLogic.Helper;
    using Affinion.LoyaltyBuild.Common;
    using Affinion.LoyaltyBuild.Common.Exceptions;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.SessionHelper;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Communications;
    using Affinion.LoyaltyBuild.Communications.Services;
    using Affinion.LoyaltyBuild.Search.Helpers;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Email;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model.Payment;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Payment;
    //using UCommerce.EntitiesV2;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Subrate;
    using Sitecore.Data.Items;
    using Sitecore.Resources.Media;
    using UcomPaymentMethod = UCommerce.EntitiesV2.PaymentMethod;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic;
    using Affinion.LoyaltyBuild.Web.ClientPortal.Helper;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Rules;
    using UCommerce.EntitiesV2;
    using Affinion.LoyaltyBuild.UCom.Common.Constants;
    using model = Affinion.LoyaltyBuild.Api.Booking.Data;
    using Affinion.LoyaltyBuild.Model.Provider;
    using Affinion.LoyaltyBuild.Model.Product;
    using Affinion.LoyaltyBuild.Api.Booking.Data;
    using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
    using apiBasket = Affinion.LoyaltyBuild.Api.Booking.Helper;
    using Affinion.LoyaltyBuild.BedBanks.JacTravel.Message;
    using System.Collections.Specialized;

    #endregion

    /// <summary>
    /// SubLayout for OfflinePortalBookingConfirmation generation
    /// </summary>
    public partial class OfflinePortalBookingConfirmationSublayout : System.Web.UI.UserControl
    {
        List<Item> hotels = null;
        //List<BasketInfo> listBasketInfo = null;
        List<OrderLineSubrate> OrderLineSubrates = null;
        List<Affinion.LoyaltyBuild.Common.SessionHelper.BasketInfo> listBasketInfo = null;
        List<JtBookResponse> book = new List<JtBookResponse>();
        public static string ProviderEmailID = string.Empty;

        public IPaymentService _paymentService;

        private int _orderLineId = 0;
        public MailService mail;

        public string Secret { get; set; } // Hold the shared secret of the Merchant
        public string ResponseResult { get; set; }
        public string ResponseMerchantId { get; set; }
        public string ResponseOrderId { get; set; }
        public string ResponseAmount { get; set; }
        public string ResponseMessage { get; set; }
        public string ResponsePasRef { get; set; }
        public string ResponseAuthCode { get; set; }
        public string ResponseTimestamp { get; set; }
        public string ResponseSha1Hash { get; set; }
        public string ResponseOther { get; set; }
        public string CurrentSiteUrl { get; set; }
        public string CurrencySymbol { get; set; }
        public string CurrencyCode { get; set; }
        public string redirectString { get; set; }
        public string OutOrderLineID { get; set; }
        public string cOrderlineID { get; set; }
        public string pageTitle { get; set; }
        public Item MailItemSource
        {
            get { return Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(ClientID)); }
        }

        public string SenderEmail
        {
            get { return MailItemSource["SenderEmail"]; }
        }
        public string CustomerConfirmationSubject
        {
            get { return MailItemSource["CustomerConfirmationSubject"]; }
        }
        public string ProviderConfirmationSubject
        {
            get { return MailItemSource["ProviderConfirmationSubject"]; }
        }

        public bool IgnoreHash { get; set; }

        public int orderId { get; set; }

        #region LWP-873
        private static bool IsPageLoaded = false;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "REALEX: Confirmation page");
                CurrentSiteUrl = "https://" + Sitecore.Context.Site.HostName;
                Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "REALEX: cur site url in sub: " + CurrentSiteUrl);
                pageTitle = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "BreadcrumbTitle");
                siteurlfield.Value = CurrentSiteUrl;
                Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "REALEX: hiddenfield value: " + siteurlfield.Value);
                ///Read response POST data
                ResponseResult = Request.Form["RESULT"];

                this.SetSession();
                if (ResponseResult == null)
                {
                    successDiv.Visible = false;
                    return;
                }
                ///Initialize properties using the response parameters
                InitializeResponseProperties();

                Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "REALEX: anything_else: orderid: " + ResponseOther + "");
                ///Validate returning hash code for security purpose
                ///
                if (!IgnoreHash)
                {
                    ValidateHash();
                }
                if (string.Equals(ResponseResult, "00"))
                {
                    Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "REALEX: " + ResponseResult + "");
                    successDiv.Visible = true;
                    dangerDiv.Visible = false;

                    

                    if (IsTransferSubPayment())
                    {
                        fullPymtPanel.Visible = false;
                        partialPymtPanel.Visible = true;


                        string[] strArr = null;
                        strArr = ResponseOther.Split('_');
                        int orderLineID = Convert.ToInt32(strArr[1]);
                        string orderGuid = strArr[2];
                        Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "REALEX: TransferSubPayment - before UpdateOrderLineTable method call - orderlineid, guid " + strArr[1] + "--" + orderGuid);
                        cOrderlineID = string.Format("javascript:window.parent.location.href = \"http://{0}/paymenthistory?olid={1}\"", Sitecore.Context.Site.HostName, orderLineID.ToString());
                        UpdateOrderLineTable(orderLineID, orderGuid);
                        Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "REALEX: TransferSubPayment - completed UpdateOrderLineTable method ");
                    }
                    else
                    {
                         fullPymtPanel.Visible = true;
                        partialPymtPanel.Visible = false;
                        ///Show booking summary
                        
                        Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, string.Format("REALEX: OrderId:{0}", ResponseOther));
                        orderId = Convert.ToInt32(ResponseOther);

                        //Send Bedbanks Book Request
                        if (!IsPostBack)
                        {
                            apiBasket.BbBookRequestHelper bookHelper = new apiBasket.BbBookRequestHelper();
                            book = bookHelper.GetBedBanksBookResponse(orderId);
                        }

                        ///Set currency labels (symbol or sign) in ascx.
                        Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "REALEX: Setting currency");
                        SetCurrency(orderId);
                       
                        Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "REALEX: Show summary");
                        ShowSummary(orderId);
                        ///Update Availability table
                        Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "REALEX: update availability");
                        UpdateAvailability(orderId);
                        //update subrates

                        //UpdateOrderlineSubrates(orderId);
                        //update dues and payments
                        Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "REALEX: update dues");
                        UpdateDuesAndPayments(orderId);
                       
                        ///Update status of the order
                        Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "REALEX: update status");
                        UpdateOrderStatus(orderId);
                        //send confirmation mail
                        
                        Affinion.LoyaltyBuild.Api.Booking.Service.BookingService bs = new Api.Booking.Service.BookingService();
                        Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "REALEX: Get order");
                        Affinion.LoyaltyBuild.Api.Booking.Data.BasketInfo bi = bs.GetPurchaseOrder(orderId);
                        var ClientId = bi.Bookings.First().ClientId;
                        Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "ClientId " + ClientId + "");
                        //var ClientId = Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.GetBasket().Bookings.FirstOrDefault().ClientId;//BasketHelper.GetOrderClientId(orderId);

                        //Affinion.LoyaltyBuild.Api.Booking.Data.BasketInfo  newBasketData =  Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.GetBasket();
                        //mail.GenerateEmail(MailTypes.CustomerCancellation, _orderLineId);
                        if (!mailGenarate(orderId, ClientId))
                        {
                            MailErrorLiteral.Text = "Mail sending failure !!";
                        }
                        #region LWP-873
                        if (Session["IsOfflinePortalLoaded"] == null)
                        {
                            AddFinanceCode(orderId, 0);
                            Session["IsOfflinePortalLoaded"] = "true";
                        }
                        #endregion
                    }

                }
                else
                {
                    if (!IsTransferSubPayment())
                    {
                        Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, string.Format("REALEX: get response order id: ", ResponseOther));
                        orderId = Convert.ToInt32(ResponseOther);
                        BasketHelper.ClearBasket(orderId);
                    }
                    ///Show error div
                    dangerDiv.Visible = true;
                    ///Show success div
                    successDiv.Visible = false;
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, this);
                /// Throw a custom exception with a custom message
                throw new AffinionException("Error is occured in Page_Load " + exception.Message, exception.InnerException);
            }
        }

        /// <summary>
        /// Set currencyID from database
        /// </summary>
        /// <param name="orderId">Current order id</param>
        private List<int> GetCurrencyDetailsbyOrderlineID(int orderLineId)
        {
            _paymentService = ContainerFactory.Instance.GetInstance<IPaymentService>();
            return (_paymentService.GetCurrencyDetailsbyOrderlineID(orderLineId));
        }

        /// <summary>
        /// Update availability table
        /// </summary>
        private void UpdateOrderLineTable(int orderLineId, string orderGuid)
        {
            Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "REALEX: TransferSubPayment - inside UpdateOrderLineTable method ");
            _paymentService = ContainerFactory.Instance.GetInstance<IPaymentService>();
            CardPaymentDetail pymtdetails = new CardPaymentDetail();
            pymtdetails.OrderLineID = orderLineId;


            List<int> currencydetailList = new List<int>();
            Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "REALEX: TransferSubPayment - inside UpdateOrderLineTable method, before making GetCurrencyDetailsbyOrderlineID call");
            currencydetailList = GetCurrencyDetailsbyOrderlineID(orderLineId);
            Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "REALEX: TransferSubPayment - inside UpdateOrderLineTable method, after making GetCurrencyDetailsbyOrderlineID call");
            int currencyIDvalue = 0;
            int exchangeRate = 1;
            if (currencydetailList.Count != 0)
            {
                currencyIDvalue = currencydetailList[0];
                exchangeRate = currencydetailList[1];
            }
            if (exchangeRate == 0)
            {
                exchangeRate = 1;
            }
            pymtdetails.CurrencyTypeId = currencyIDvalue;
            Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "REALEX: TransferSubPayment - inside UpdateOrderLineTable method, before exchangerate processing");
            pymtdetails.Amount = Convert.ToDecimal(Convert.ToDecimal(ResponseAmount) / Convert.ToDecimal(exchangeRate));
            pymtdetails.ProviderAmount = 0;
            pymtdetails.CommissionAmount = 0;
            pymtdetails.ProcessingFee = 0;

            pymtdetails.PaymentMethodID = 9; //For RealexOffline
            var orderPayment = OrderLine.FirstOrDefault(i => i.OrderLineId == orderLineId).PurchaseOrder.Payments.FirstOrDefault(i => i.PaymentProperties.Any(x => x.Key == "paymentGuid" && x.Value == orderGuid));
            if(orderPayment != null)
            {
                pymtdetails.PaymentMethodID = orderPayment.PaymentMethod.PaymentMethodId;
                pymtdetails.OrderPaymentID = orderPayment.PaymentId;
            }
            pymtdetails.ReferenceID = orderGuid;
          
            pymtdetails.CreatedBy = SitecoreUserProfileHelper.GetItem(UserProfileKeys.CreatedBy);
           
            Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "REALEX: TransferSubPayment - inside UpdateOrderLineTable method, before addpayment call");
            ValidationResponse result = _paymentService.AddPayment(pymtdetails);
            Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "REALEX: TransferSubPayment - inside UpdateOrderLineTable method, after addpayment call");
            if (result.IsSuccess)
            {


                Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "REALEX: TransferSubPayment - inside UpdateOrderLineTable method, inside result.issuccess");
                partPymtSuccessDiv.Visible = true;
                partPymtRejectDiv.Visible = false;
            }
            else
            {
                Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "REALEX: TransferSubPayment - inside UpdateOrderLineTable method, inside result.failure");
                Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "REALEX: TransferSubPayment - inside UpdateOrderLineTable method, after addpayment call");
                partPymtSuccessDiv.Visible = false;
                partPymtRejectDiv.Visible = true;
            }
            Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "REALEX: TransferSubPayment - inside UpdateOrderLineTable method, before redirect string formation");
            redirectString = CurrentSiteUrl + "/PaymentHistory?olid=" + Convert.ToString(orderLineId);
            Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "REALEX: TransferSubPayment - inside UpdateOrderLineTable method, after redirect string formation");
        }

        private void UpdateOrderlineSubrates(int orderId)
        {
            BasketHelper.UpdateOrderLineSubrate(OrderLineSubrates, orderId);
        }

        /// <summary>
        /// Set currency in ascx
        /// </summary>
        /// <param name="orderId">Current order id</param>
        private void SetCurrency(int orderId)
        {
            string currency = UCommerce.EntitiesV2.PurchaseOrder.Get(orderId).BillingCurrency.ISOCode;

            string symbol = StoreHelper.GetCurrencySymbol(currency);

            CurrencySymbol = string.IsNullOrEmpty(symbol) ? string.Empty : string.Format("{0} ", symbol);

            ///Set the currency code if a symbol is not available
            if (string.Equals(symbol, currency))
            {
                CurrencyCode = string.Format(" {0}", currency);
                CurrencySymbol = string.Empty;
            }
        }

        /// <summary>
        /// Initialize response properties
        /// </summary>
        private void InitializeResponseProperties()
        {
            ResponseMerchantId = Request.Form["MERCHANT_ID"];
            ResponseOrderId = Request.Form["ORDER_ID"];
            ResponseAmount = Request.Form["AMOUNT"];
            ResponseMessage = Request.Form["MESSAGE"];
            ResponsePasRef = Request.Form["PASREF"];
            ResponseAuthCode = Request.Form["AUTHCODE"];
            ResponseTimestamp = Request.Form["TIMESTAMP"];
            ResponseSha1Hash = Request.Form["SHA1HASH"];
            ResponseOther = Request.Form["ANYTHING ELSE"];
            SetSession();
        }

        private void SetSession()
        {
            if (Request.QueryString["islocal"] != null)
            {
                ResponseOrderId = Session["ResponseOrderId"].ToString();
                ResponseResult = "00";
                ResponseAmount = Session["ResponseAmount"].ToString();
                ResponseOther = Session["ResponseOther"].ToString();
                IgnoreHash = true;
            }
        }
        private bool IsTransferSubPayment()
        {
            Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "REALEX: InsideIsTransferSubPayment Method ");
            int tmp;
            if (ResponseOther != null)
            {
                bool IsNumber = int.TryParse(ResponseOther, out tmp);
                if (!IsNumber)
                {
                    if (ResponseOther.Contains("TransferSubPayment"))
                    {
                        Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "REALEX: InsideIsTransferSubPayment Method - valid response for partial payment ");
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Update availability table
        /// </summary>
        private void UpdateAvailability(int orderId)
        {
            Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "REALEX: inside update availability method");
            BasketHelper.UpdateAvailability(orderId);
        }

        /// <summary>
        /// Update availability table
        /// </summary>
        private void UpdateDuesAndPayments(int orderId)
        {
            Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "REALEX: inside update dues and payments method");
            BasketHelper.UpdateDuesAndPayments(orderId);
        }



        /// <summary>
        /// Update status of the purchase order
        /// </summary>
        private void UpdateOrderStatus(int orderId)
        {
            try
            {
                Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "REALEX: inside updateorderstatus method");
                var currentOrder = BasketHelper.GetPurchaseOrder(orderId);
                BasketHelper.UpdateOrderStatus(currentOrder, 6); //6 is the order status ID of "Paid" status

            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException("Error ocuured in a method(UpdateOrderStatus()). More Details" + exception, exception.InnerException);
            }
        }

        /// <summary>
        /// Validate the response hash
        /// </summary>
        private void ValidateHash()
        {
            UcomPaymentMethod paymentMethod = UcomPaymentMethod.SingleOrDefault(x => x.Name == Constants.OfflinePortalPaymentType);
            var paymentMethodProperties = paymentMethod.PaymentMethodProperties;
            Secret = paymentMethodProperties.FirstOrDefault(x => x.DefinitionField.Name == "Secret").Value;

            ///Generate the hash using SHA1 algorithm
            var sha1Hash = PaymentGatewayHelper.GetHash(string.Format("{0}.{1}", PaymentGatewayHelper.GetHash(string.Format("{0}.{1}.{2}.{3}.{4}.{5}.{6}", ResponseTimestamp, ResponseMerchantId, ResponseOrderId, ResponseResult, ResponseMessage, ResponsePasRef, ResponseAuthCode)), Secret));

            ///Validate the response HASH
            if (!string.Equals(ResponseSha1Hash, sha1Hash))
            {
                throw new AffinionException("Invalid payment request. Response hash not matched");//TODO: Exception or any other solution ?
            }
        }
        
        /// <summary>
        /// Show booking summary
        /// </summary>
        private void ShowSummary(int orderId)
        {
            var basketService = new Affinion.LoyaltyBuild.Api.Booking.Service.BookingService();
            var order = basketService.GetPurchaseOrder(orderId);

            //listBasketInfo = BasketHelper.GetPopulateConfirmationListFromDB(orderId);
            PanelRepeater.DataSource = order.Bookings;

            PanelRepeater.DataBind();
        }

        /// <summary>
        /// bind repeater 
        /// </summary>
        protected void BookingsBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var booking = (model.Booking)e.Item.DataItem;
                var customer = UCommerce.EntitiesV2.PurchaseOrder.Get(booking.OrderId).Customer;
                var provider = booking.GetProvider();
                
                var product = booking.GetProduct();
                Image hotelImage = e.Item.FindControl("hotelImage") as Image;

                ((Literal)e.Item.FindControl("litHotelName")).Text = provider.Name;

                var starRepeater = ((Repeater)e.Item.FindControl("StarRepeater"));
                starRepeater.DataSource = System.Linq.Enumerable.Range(1, ((IGenericProvider)provider).StarRanking);
                starRepeater.DataBind();


                var lstConfirmation = uCommerceConnectionFactory.GetConfirmationInfo(booking.OrderId);
                

                switch (booking.Type)
                {
                    case ProductType.Room:
                    case ProductType.Package:
                        var loyaltyProvider = (Hotel)provider;
                        var roomBooking = (RoomBooking)booking;
                        Sitecore.Data.Fields.FileField fileField = loyaltyProvider.SitecoreItem.Fields["IntroductionImage"];
                        MediaUrlOptions muo = new MediaUrlOptions();
                        muo.AlwaysIncludeServerUrl = true;
                        if (fileField.MediaItem != null)
                        {
                            string url = MediaManager.GetMediaUrl(fileField.MediaItem, muo);
                            hotelImage.ImageUrl = url;
                        }                   
                        ((Panel)e.Item.FindControl("pnlRoom")).Visible = true;
                        ((Literal)e.Item.FindControl("litCheckIn")).Text = string.Format("{0}", roomBooking.CheckinDate.ToString("dddd, MMMM dd, yyyy"), loyaltyProvider.CheckInTime);
                        ((Literal)e.Item.FindControl("litCheckOut")).Text = string.Format("{0}", roomBooking.CheckoutDate.ToString("dddd, MMMM dd, yyyy"), loyaltyProvider.CheckOutTime);
                        ((Literal)e.Item.FindControl("litGuestName")).Text = lstConfirmation.FirstOrDefault(p => p.OrderLineId == roomBooking.OrderLineId).GuestName;
                        ((Literal)e.Item.FindControl("litPhone")).Text = loyaltyProvider.SitecoreItem["Phone"];
                        ((Literal)e.Item.FindControl("litEmail")).Text = customer.EmailAddress;//roomBooking.GetProvider().Name;//loyaltyProvider.SitecoreItem["EmailID1"];
                        ((Literal)e.Item.FindControl("BookingNo")).Text =booking.BookingReferenceNo;

                        ((Literal)e.Item.FindControl("litHotelAddress")).Text =loyaltyProvider.Address;
                        ((Literal)e.Item.FindControl("litEmail1")).Text = loyaltyProvider.SitecoreItem["EmailID1"];
                        ((Literal)e.Item.FindControl("litSpecialRequest")).Text = lstConfirmation.FirstOrDefault(p => p.OrderLineId == roomBooking.OrderLineId).Notes;
                        ((Literal)e.Item.FindControl("litNoOfPeople")).Text = string.Format("{0} Adults, {1} Children", roomBooking.NoOfAdults, roomBooking.NoOfChildren);
                        ((Literal)e.Item.FindControl("litRoomDetail")).Text = product.Name;
                        ((Literal)e.Item.FindControl("PriceAccommodation")).Text = CurrencyHelper.FormatCurrency((decimal)(roomBooking.ProviderAmount - roomBooking.ProviderAmountPaidOnCheckOut), roomBooking.CurrencyId);
                        ((Literal)e.Item.FindControl("ProcessingFee")).Text = CurrencyHelper.FormatCurrency(roomBooking.ProcessingFee, roomBooking.CurrencyId);
                        ((Literal)e.Item.FindControl("DepositPayableToday")).Text = CurrencyHelper.FormatCurrency(roomBooking.CommissionAmount + roomBooking.ProcessingFee + roomBooking.ProviderAmountPaidOnCheckOut, roomBooking.CurrencyId);

                        break;
                    case ProductType.BedBanks:
                        var bedbankProvider = (BedBankHotel)provider;
                        var bbRoombooking = (BedBanksBooking)booking;
                        hotelImage.ImageUrl = bedbankProvider.CMSBaseUrl + bedbankProvider.IntroductionImage;

                        ((Panel)e.Item.FindControl("pnlBedBanks")).Visible = true;
                        ((Literal)e.Item.FindControl("litBBCheckIn")).Text = string.Format("{0}", bbRoombooking.CheckinDate.ToString("dddd,MMMM dd,yyyy"));
                        ((Literal)e.Item.FindControl("litBBCheckOut")).Text = string.Format("{0}", bbRoombooking.CheckoutDate.ToString("dddd, MMMM dd, yyyy"));
                        ((Literal)e.Item.FindControl("litBBGuestName")).Text = lstConfirmation.FirstOrDefault(p => p.OrderLineId == bbRoombooking.OrderLineId).GuestName;
                        ((Literal)e.Item.FindControl("litPhone")).Text = bedbankProvider.Phone;
                        ((Literal)e.Item.FindControl("litEmail")).Text = customer.EmailAddress;
                        ((Literal)e.Item.FindControl("BookingNo")).Text =booking.BookingReferenceNo;

                        ((Literal)e.Item.FindControl("litHotelAddress")).Text = bedbankProvider.AddressLine1 + bedbankProvider.AddressLine2;
                        ((Literal)e.Item.FindControl("litEmail1")).Text = bedbankProvider.Email;
                        ((Literal)e.Item.FindControl("litBBSpecialRequest")).Text = lstConfirmation.FirstOrDefault(p => p.OrderLineId == bbRoombooking.OrderLineId).Notes;
                        ((Literal)e.Item.FindControl("litBBNoOfPeople")).Text = string.Format("{0} Adults, {1} Children", bbRoombooking.NoOfAdults, bbRoombooking.NoOfChildren);
                        ((Literal)e.Item.FindControl("litBBRoomDetail")).Text = bbRoombooking.RoomInfo;
                        ((Literal)e.Item.FindControl("litBBPriceAccommodation")).Text = CurrencyHelper.FormatCurrency((decimal)(bbRoombooking.ProviderAmount - bbRoombooking.ProviderAmountPaidOnCheckOut), bbRoombooking.CurrencyId);
                        ((Literal)e.Item.FindControl("litBBProcessingFee")).Text = CurrencyHelper.FormatCurrency(bbRoombooking.ProcessingFee, bbRoombooking.CurrencyId);
                        ((Literal)e.Item.FindControl("litBBDepositPayableToday")).Text = CurrencyHelper.FormatCurrency(bbRoombooking.CommissionAmount + bbRoombooking.ProcessingFee + bbRoombooking.ProviderAmountPaidOnCheckOut, bbRoombooking.CurrencyId);

                        break;
                }
            }
        }

        //protected void PanelRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //    {
        //        Repeater starRepeater = e.Item.FindControl("StarRepeater") as Repeater;
        //        Image hotelImage = e.Item.FindControl("hotelImage") as Image;
        //        BasketInfo dataItem = e.Item.DataItem as BasketInfo;
        //        var dataItemSitecore = dataItem.SitecoreItem;
        //        MediaUrlOptions muo = new MediaUrlOptions();
        //        muo.AlwaysIncludeServerUrl = true;
        //        Sitecore.Data.Fields.FileField fileField = dataItemSitecore.Fields["IntroductionImage"];
        //        if (fileField.MediaItem != null)
        //        {
        //            string url = MediaManager.GetMediaUrl(fileField.MediaItem, muo);
        //            hotelImage.ImageUrl = url;
        //        }

        //        string BillingCurrency = UCommerce.EntitiesV2.PurchaseOrder.Get(orderId).BillingCurrency.ISOCode;
        //        OrderLine orderLine = UCommerce.EntitiesV2.OrderLine.FirstOrDefault(i => i.OrderLineId == int.Parse(dataItem.OrderLineId));
        //        if (orderLine != null)
        //        {

        //            if (orderLine[OrderPropertyConstants.PaymentMethod] == Constants.PaymentMethod.PartialPayment)
        //            {
        //                string daysBefore = orderLine[OrderPropertyConstants.DaysBefore];

        //                Literal pricePartial = e.Item.FindControl("PriceAccommodation") as Literal;
        //                pricePartial.Text = string.Format("Total Payable {0} Days Prior to Arrival:", daysBefore);
        //                pricePartial.Text = CurrencyHelper.GetCurrency(BillingCurrency) + dataItem.PayableAtAccommodation;

        //            }
        //            else
        //            {
        //                Literal priceTotalLiteral = e.Item.FindControl("PriceAccommodation") as Literal;

        //                UCommerce.EntitiesV2.PurchaseOrder order = UCommerce.EntitiesV2.PurchaseOrder.Get(orderId);

        //                priceTotalLiteral.Text = StoreHelper.GetPriceWithCurrency(Convert.ToDecimal(dataItem.PayableAtAccommodation), null, BillingCurrency);

        //                Literal processingPriceLiteral = e.Item.FindControl("DepositPayableToday") as Literal;
        //                processingPriceLiteral.Text = StoreHelper.GetPriceWithCurrency(Convert.ToDecimal(dataItem.DepositPayableToday), null, BillingCurrency);

        //                Literal ProcessingFeeLiteral = e.Item.FindControl("ProcessingFee") as Literal;
        //                ProcessingFeeLiteral.Text = StoreHelper.GetPriceWithCurrency(Convert.ToDecimal(dataItem.ProcessingFee), null, BillingCurrency);
        //            }
        //        }
        //        string ranking = SitecoreFieldsHelper.GetDropLinkFieldValue(dataItemSitecore, "StarRanking", "Name");
        //        if (!string.IsNullOrWhiteSpace(ranking))
        //        {
        //            int starRank;
        //            bool result = Int32.TryParse(ranking, out starRank);
        //            if (result)
        //            {
        //                List<int> listOfStars = Enumerable.Repeat(starRank, starRank).ToList();
        //                starRepeater.DataSource = listOfStars;
        //                starRepeater.DataBind();
        //            }
        //            else
        //            {
        //                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal,
        //                new AffinionException("Unable to parse star rank:" + dataItemSitecore.DisplayName), this);
        //            }
        //        }
        //    }
        //}

        private string GetImageURL(Item currentItem, string fieldName)
        {
            string imageURL = string.Empty;
            Sitecore.Data.Fields.ImageField imageField = currentItem.Fields[fieldName];
            if (imageField != null && imageField.MediaItem != null)
            {
                Sitecore.Data.Items.MediaItem image = new Sitecore.Data.Items.MediaItem(imageField.MediaItem);
                imageURL = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image));
            }
            return imageURL;
        }

        /// <summary>
        /// Send Booking Confirmation Email
        /// </summary>
        private bool mailGenarate(int orderID, string ClientId)
        {
            try
            {
                int bbOrderLineId = 0;
                Affinion.LoyaltyBuild.Api.Booking.Service.BookingService bs = new Api.Booking.Service.BookingService();
                Affinion.LoyaltyBuild.Api.Booking.Data.BasketInfo bi = bs.GetPurchaseOrder(orderId);
                var pType = bi.Bookings.Where(x => x.Type == ProductType.BedBanks);

                MailService mailService = new MailService(ClientId);
                int iOrderLineId=0;
                DataSet orderLineDetails = BasketHelper.GetOrderLines(orderId);
                for (int i = 0; i < orderLineDetails.Tables[0].Rows.Count; i++)
                {
                    string orderlineid = orderLineDetails.Tables[0].Rows[i][0].ToString();
                    if (!string.IsNullOrWhiteSpace(orderlineid))
                    {
                        
                        if (int.TryParse(orderlineid, out iOrderLineId))
                        {
                           bbOrderLineId = pType.Where(x => x.OrderLineId.Equals(iOrderLineId)).Select(y => y.OrderLineId).FirstOrDefault();
                            if(bbOrderLineId>0)
                            {
                                NameValueCollection addInfo = new NameValueCollection();
                                addInfo[Constants.ConditionalMessageItemId] = "{056B5DFF-ED1E-4C1D-96B2-014F7621C1AE}";
                                mailService.GenerateEmail(MailTypes.BedBanksCustomerConfirmation, iOrderLineId, addInfo);
                            }
                            else
                            {
                                mailService.GenerateEmail(MailTypes.CustomerConfirmation, iOrderLineId);
                                mailService.GenerateEmail(MailTypes.ProviderConfirmation, iOrderLineId);
                            }
                        }
                    }
                }
                if (book != null && book.Count > 0)
                {
                    foreach (JtBookResponse response in book)
                    {
                        if (!response.ReturnStatus.Success.ToLower().Equals("true"))
                        {
                            //TODO: handle the code to send the emails
                            if (response.ReturnStatus.Success.ToLower().Equals("false") && response.ReturnStatus.Exception != null)
                            {
                                mailService.GenerateEmail(MailTypes.BedBanksConfirmationIssue, iOrderLineId);
                                //API Exception
                            }
                            else
                            {
                                mailService.GenerateEmail(MailTypes.BedBanksConfirmationIssue, iOrderLineId);
                                //Booking failure
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                return false;
            }
            //SendCustomerConfirmationEmail();
        }

        #region LWP-873
        private void AddFinanceCode(int orderID, int orderLineID)
        {
            try
            {
                _paymentService = ContainerFactory.Instance.GetInstance<IPaymentService>();

                FinanceCode objFinanceCode = _paymentService.GetClientAndProviderID(orderID, orderLineID);

                string ProviderPath = Constants.ProviderPath;
                string productCode = string.Empty;
                long fourDigitMerchantID = 0;

                Item ClientConfigItems = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(objFinanceCode.ClientId));
                Item[] providers = Sitecore.Context.Database.SelectItems(ProviderPath);
                Item providersItem = providers.Where(x => Guid.Parse(x.ID.ToString()).ToString().Equals(objFinanceCode.ProviderId, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                Item providertype = SitecoreFieldsHelper.GetDropLinkFieldTargetItem(providersItem, "SupplierType");
                Dictionary<string, string> FieldsWithInSection = new Dictionary<string, string>();
                objFinanceCode.OrderId = orderID;
                objFinanceCode.OrderLineId = orderLineID;
                if (ClientConfigItems != null && ClientConfigItems.Fields != null)
                {
                    if (ClientConfigItems.Fields["Company"] != null && ClientConfigItems.Fields["Company"].Value != string.Empty)
                        objFinanceCode.Company = ClientConfigItems.Fields["Company"].Value;
                    else
                        objFinanceCode.Company = "0";

                    var currencySection = ClientConfigItems.Fields.Where(name => name.Section == objFinanceCode.Currency).ToDictionary(field => field.Name, field => field.Value);
                    if (currencySection != null && currencySection.Count > 0 && !string.IsNullOrEmpty(currencySection["Offline MID Value"]))
                        objFinanceCode.MerchantIdentificationNo = Int32.Parse(currencySection["Offline MID Value"]);
                    else
                        objFinanceCode.MerchantIdentificationNo = 0;

                    if (ClientConfigItems.Fields["Department"] != null && ClientConfigItems.Fields["Department"].Value != string.Empty)
                        objFinanceCode.Department = ClientConfigItems.Fields["Department"].Value;
                    else
                        objFinanceCode.Department = "0";

                    if (ClientConfigItems.Fields["Client Code"] != null && ClientConfigItems.Fields["Client Code"].Value != string.Empty)
                        objFinanceCode.ClientCode = ClientConfigItems.Fields["Client Code"].Value;
                    else
                        objFinanceCode.ClientCode = string.Empty;

                    if (ClientConfigItems.Fields["Processing Fee"] != null && ClientConfigItems.Fields["Processing Fee"].Value != string.Empty)
                        objFinanceCode.GlCode = ClientConfigItems.Fields["Processing Fee"].Value;
                    else
                        objFinanceCode.GlCode = string.Empty;

                    if (providertype != null)
                    {
                        if (providertype.Fields != null && providertype.Fields["Product"].Value != string.Empty)
                        {
                            productCode = providertype.Fields["Product"].Value;
                        }
                    }
                }

                objFinanceCode.ClientName = ClientConfigItems.DisplayName;
                objFinanceCode.ProductCode = productCode;

                fourDigitMerchantID = objFinanceCode.MerchantIdentificationNo;
                objFinanceCode.Description = "EMSXXX" + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + "XXXX" + fourDigitMerchantID.ToString().Substring(fourDigitMerchantID.ToString().Length - 4);

                ValidationResponse financeCodeResult = _paymentService.AddFinaceCode(objFinanceCode);
                IsPageLoaded = true;

            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.AdminPortal, ex, this);

            }
        }

        #endregion

      
    }
}