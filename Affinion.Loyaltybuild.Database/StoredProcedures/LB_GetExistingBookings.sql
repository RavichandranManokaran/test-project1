﻿
CREATE PROCEDURE [dbo].[LB_GetExistingBookings]

(	   @clientIds nvarchar(512), 
	   @firstname nvarchar(512)=NULL, 
       @lastname nvarchar(512)=NULL, 
       @providerName nvarchar(50)=NULL, 
	   @reservationDate Date=NULL, 
       @arrivalDate Date=NULL,
	   @reservationDateFrom Date=NULL, 
       @reservationDateTo Date=NULL, 
       @bookingReference nvarchar(50)=NULL,
	   @EmailValidation varchar(50)=NULL,
	   @NumericValidation varchar(50)=NULL,
	   @bookingMethod varchar(50)=NULL,
	   @orderStatusId int=NULL,
	   @CreatedByUser varchar(100) = NULL)
 AS 
         /*Client Ids are Mandatory*/		
	    /* Select the bookings which are associated with Offers */
       IF (@CreatedByUser = '' OR @CreatedByUser IS NULL)
	   BEGIN
	   SELECT 
	            OL.OrderLineId,
                OL.OrderId,			      
                PO.OrderGuid,
                CI.Name as [OfferName],
                CONCAT(oa.FirstName, ' ', oa.LastName) as [FullName], 
                rr.BookingNo as [BookingReferenceId],
                CONCAT( Replace(Convert(varchar(11), RR.CheckInDate,103),' ','/'), '   ', Replace(Convert(varchar(11), RR.CheckOutDate,103),' ','/')) as [CheckInCheckOutDates], 
                op.NoOfAdult, 
                op.NoOfChild, 
                CONCAT(CONVERT(varchar(20),OL.CreatedOn,103), ' ', CONVERT(VARCHAR(8), OL.CreatedOn, 108)) as 'CreatedOn',
			   op.BookingThrough as [Booking Method], 
               OS.Name as [BookingStatus], 
               CONCAT(ppd.HotelName,' ', PPD.RoomType,'',PPD.AdOnsType) as [Accommodation]
	     
		FROM [dbo].uCommerce_OrderLine OL 
	    INNER JOIN
	    uCommerce_PurchaseOrder PO on po.OrderId=ol.OrderId
		INNER JOIN 
		uCommerce_OrderAddress oa on oa.OrderId=po.OrderId
		INNER JOIN 
		LB_BookingInfo rr on rr.OrderLineId =ol.OrderLineId
		INNER JOIN 
		Ucommerce_product P ON OL.Sku = P.Sku AND P.ParentProductId IS NULL 
		INNER JOIN
			  (
			    /* select the noofadult,children,bookingthrough */
			    SELECT OrderLineId,OrderId, _NoOfAdult [NoOfAdult], _NoOfChild [NoOfChild],_BookingThrough [BookingThrough]
				 FROM 
			 (
				 SELECT OrderLineId,OrderId, [key], [value]
				FROM uCommerce_OrderProperty 
			 ) src
				pivot
			(
				MAX([value])
				FOR [key] in (_NoOfAdult, _NoOfChild,_BookingThrough)
			) piv
			) OP ON op.OrderLineId=ol.OrderLineId
	 INNER JOIN
	 uCommerce_ProductDefinition PD ON P.ProductDefinitionId=PD.ProductDefinitionId AND PD.Name='Hotel'
	 INNER JOIN 
			 (
			     /* select the provider name,room type and addons*/
				SELECT ProductId, RoomType, HotelName,AdOnsType
				 FROM 
			(
				SELECT ProductId,PD.Name [key], PP.value
				FROM uCommerce_ProductDefinitionField PD 
				INNER JOIN 
				uCommerce_ProductProperty PP ON PD.ProductDefinitionFieldId=PP.ProductDefinitionFieldId
				WHERE PD.Name IN ('RoomType','HotelName','AdOnsType')
			) src
			pivot
		   (
			MAX([value])
			FOR [key] in (RoomType, HotelName)
			) piv
		    ) PPD ON PPD.ProductId=P.ProductId 
	 LEFT JOIN 
	 uCommerce_OrderLineCampaignRelation OLCR ON OL.OrderLineId=OLCR.OrderLineId  
	LEFT JOIN 
	 ucommerce_campaignitem CI ON OLCR.CampaignItemId=CI.CampaignItemId	
     INNER JOIN
			 (
				 SELECT CustomerId,_ClientId [ClientId]
				 FROM 
		    (
			   SELECT CustomerId, [key], [value]
				FROM LB_CustomerProperty 
			 ) src
			  pivot
			 (
			    MAX([value])
			    FOR [key] in (_ClientId) 
			  ) piv
		      ) CP ON PO.CustomerId=CP.CustomerId 
     INNER JOIN 
     uCommerce_OrderStatus OS on OS.OrderStatusId=BI.StatusId
     WHERE
                (@firstname IS NULL OR oa.FirstName Like @firstname + '%')
			AND (@lastname IS NULL OR oa.LastName Like @lastname + '%') 
			AND (op.ClientId= @clientIds) 
			AND (@providerName IS NULL OR p.Name Like @providerName + '%')
			AND ((@ArrivalDate IS NULL) OR (CONVERT(date,RR.CheckInDate)=@ArrivalDate))
			AND (@bookingReference IS NULL OR RR.OrderReference like @bookingReference + '%') 
			AND ( oa.EmailAddress like (@EmailValidation + '%'))
			--AND(@NumericValidation IS NULL OR RR.CustomerReference like @NumericValidation + '%')
			--AND RR.BookingThrough = ISNULL(@bookingMethod,RR.BookingThrough)  
			AND op.BookingThrough=@bookingMethod
			AND RR.StatusId=ISNULL(@orderStatusId,OS.OrderStatusId) 
			AND ((@reservationDate IS NULL) OR (CONVERT(date,OL.CreatedOn)=@reservationDate))
			AND OL.CreatedOn >= COALESCE(@reservationDateFrom,OL.CreatedOn) 
			AND cast(OL.CreatedOn as date) <= COALESCE(@reservationDateTo,OL.CreatedOn) 
			AND OP.[Key] = '_appliedcampaign'

	UNION
	       SELECT 
	            OL.OrderLineId,
                OL.OrderId,			      
                PO.OrderGuid,
                '' as [OfferName],
                CONCAT(oa.FirstName, ' ', oa.LastName) as [FullName], 
                rr.BookingNo as [BookingReferenceId],
                CONCAT( Replace(Convert(varchar(11), RR.CheckInDate,103),' ','/'), '   ', Replace(Convert(varchar(11), RR.CheckOutDate,103),' ','/')) as [CheckInCheckOutDates], 
                op.NoOfAdult, 
                op.NoOfChild, 
                CONCAT(CONVERT(varchar(20),OL.CreatedOn,103), ' ', CONVERT(VARCHAR(8), OL.CreatedOn, 108)) as 'CreatedOn',
			   op.BookingThrough as [Booking Method], 
               OS.Name as [BookingStatus], 
               CONCAT(ppd.HotelName,' ', PPD.RoomType,'',PPD.AdOnsType) as [Accommodation]

			   	FROM [dbo].uCommerce_OrderLine OL 
	    INNER JOIN
	    uCommerce_PurchaseOrder PO on po.OrderId=ol.OrderId
		INNER JOIN 
		uCommerce_OrderAddress oa on oa.OrderId=po.OrderId
		INNER JOIN 
		LB_BookingInfo rr on rr.OrderLineId =ol.OrderLineId
		INNER JOIN 
		Ucommerce_product P ON OL.Sku = P.Sku AND P.ParentProductId IS NULL 
		INNER JOIN
			  (
			    SELECT OrderLineId,OrderId, _NoOfAdult [NoOfAdult], _NoOfChild [NoOfChild],_BookingThrough [BookingThrough]
				 FROM 
			 (
				 SELECT OrderLineId,OrderId, [key], [value]
				FROM uCommerce_OrderProperty  
			 ) src
				pivot
			(
				MAX([value])
				FOR [key] in (_NoOfAdult, _NoOfChild,_BookingThrough)
			) piv
			) OP ON op.OrderLineId=ol.OrderLineId
	 INNER JOIN
	 uCommerce_ProductDefinition PD ON P.ProductDefinitionId=PD.ProductDefinitionId AND PD.Name='Hotel'
	 INNER JOIN 
			 (
				SELECT ProductId, RoomType, HotelName,AdOnsType
				 FROM 
			(
				SELECT ProductId,PD.Name [key], PP.value
				FROM uCommerce_ProductDefinitionField PD 
				INNER JOIN 
				uCommerce_ProductProperty PP ON PD.ProductDefinitionFieldId=PP.ProductDefinitionFieldId
				WHERE PD.Name IN ('RoomType','HotelName','AdOnsType')
			) src
			pivot
		   (
			MAX([value])
			FOR [key] in (RoomType, HotelName,AdOnsType)
			) piv
		    ) PPD ON PPD.ProductId=P.ProductId 
	 LEFT JOIN 
	 uCommerce_OrderLineCampaignRelation OLCR ON OL.OrderLineId=OLCR.OrderLineId  
	LEFT JOIN 
	 ucommerce_campaignitem CI ON OLCR.CampaignItemId=CI.CampaignItemId	
     INNER JOIN
			 (
				 SELECT CustomerId,_ClientId [ClientId]
				 FROM 
		    (
			   SELECT CustomerId, [key], [value]
				FROM LB_CustomerProperty 
			 ) src
			  pivot
			 (
			    MAX([value])
			    FOR [key] in (_ClientId) 
			  ) piv
		      ) CP ON PO.CustomerId=CP.CustomerId 
     INNER JOIN 
     uCommerce_OrderStatus OS on OS.OrderStatusId=BI.StatusId

	 WHERE
                (@firstname IS NULL OR oa.FirstName Like @firstname + '%')
			AND (@lastname IS NULL OR oa.LastName Like @lastname + '%') 
			AND (op.ClientId= @clientIds) 
			AND (@providerName IS NULL OR p.Name Like @providerName + '%')
			AND ((@ArrivalDate IS NULL) OR (CONVERT(date,RR.CheckInDate)=@ArrivalDate))
			AND (@bookingReference IS NULL OR RR.OrderReference like @bookingReference + '%') 
			AND ( oa.EmailAddress like (@EmailValidation + '%'))
			--AND(@NumericValidation IS NULL OR RR.CustomerReference like @NumericValidation + '%')
			--AND RR.BookingThrough = ISNULL(@bookingMethod,RR.BookingThrough)  
			AND op.BookingThrough=@bookingMethod
			AND RR.StatusId=ISNULL(@orderStatusId,OS.OrderStatusId) 
			AND ((@reservationDate IS NULL) OR (CONVERT(date,OL.CreatedOn)=@reservationDate))
			AND OL.CreatedOn >= COALESCE(@reservationDateFrom,OL.CreatedOn) 
			AND cast(OL.CreatedOn as date) <= COALESCE(@reservationDateTo,OL.CreatedOn) 
			AND (1>(Select count(value) from [dbo].uCommerce_OrderProperty 
			Where [Key] = '_appliedcampaign' AND OrderLineId = OL.OrderLineId))
			ORDER BY CreatedOn DESC
	END
		Else
	BEGIN
	   SELECT    OL.OrderLineId,
                OL.OrderId,			      
                PO.OrderGuid,
                CI.Name as [OfferName],
                CONCAT(oa.FirstName, ' ', oa.LastName) as [FullName], 
                rr.BookingNo as [BookingReferenceId],
                CONCAT( Replace(Convert(varchar(11), RR.CheckInDate,103),' ','/'), '   ', Replace(Convert(varchar(11), RR.CheckOutDate,103),' ','/')) as [CheckInCheckOutDates], 
                op.NoOfAdult, 
                op.NoOfChild, 
                CONCAT(CONVERT(varchar(20),OL.CreatedOn,103), ' ', CONVERT(VARCHAR(8), OL.CreatedOn, 108)) as 'CreatedOn',
			   op.BookingThrough as [Booking Method], 
               OS.Name as [BookingStatus], 
               CONCAT(ppd.HotelName,' ', PPD.RoomType,'',PPD.AdOnsType) as [Accommodation]

	 	FROM [dbo].uCommerce_OrderLine OL 
	    INNER JOIN
	    uCommerce_PurchaseOrder PO on po.OrderId=ol.OrderId
		INNER JOIN 
		uCommerce_OrderAddress oa on oa.OrderId=po.OrderId
		INNER JOIN 
		LB_BookingInfo rr on rr.OrderLineId =ol.OrderLineId
		INNER JOIN 
		Ucommerce_product P ON OL.Sku = P.Sku AND P.ParentProductId IS NULL 
		INNER JOIN
			  (
			    SELECT OrderLineId,OrderId, _NoOfAdult [NoOfAdult], _NoOfChild [NoOfChild],_BookingThrough [BookingThrough]
				 FROM 
			 (
				 SELECT OrderLineId,OrderId, [key], [value]
				FROM uCommerce_OrderProperty  where uCommerce_OrderProperty.[Value]='Offline'
			 ) src
				pivot
			(
				MAX([value])
				FOR [key] in (_NoOfAdult, _NoOfChild,_BookingThrough)
			) piv
			) OP ON op.OrderLineId=ol.OrderLineId
	 INNER JOIN
	 uCommerce_ProductDefinition PD ON P.ProductDefinitionId=PD.ProductDefinitionId AND PD.Name='Hotel'
	 INNER JOIN 
			 (
				SELECT ProductId, RoomType, HotelName,AdOnsType
				 FROM 
			(
				SELECT ProductId,PD.Name [key], PP.value
				FROM uCommerce_ProductDefinitionField PD 
				INNER JOIN 
				uCommerce_ProductProperty PP ON PD.ProductDefinitionFieldId=PP.ProductDefinitionFieldId
				WHERE PD.Name IN ('RoomType','HotelName','AdOnsType')
			) src
			pivot
		   (
			MAX([value])
			FOR [key] in (RoomType, HotelName,AdOnsType)
			) piv
		    ) PPD ON PPD.ProductId=P.ProductId 
	 LEFT JOIN 
	 uCommerce_OrderLineCampaignRelation OLCR ON OL.OrderLineId=OLCR.OrderLineId  
	LEFT JOIN 
	 ucommerce_campaignitem CI ON OLCR.CampaignItemId=CI.CampaignItemId	
     INNER JOIN
			 (
				 SELECT CustomerId,_ClientId [ClientId]
				 FROM 
		    (
			   SELECT CustomerId, [key], [value]
				FROM LB_CustomerProperty 
			 ) src
			  pivot
			 (
			    MAX([value])
			    FOR [key] in (_ClientId) 
			  ) piv
		      ) CP ON PO.CustomerId=CP.CustomerId 
     INNER JOIN 
     uCommerce_OrderStatus OS on OS.OrderStatusId=BI.StatusId
	 WHERE
			( CA.ClientId= @clientIds)
			AND ((@reservationDate IS NULL) OR (CONVERT(date,OL.CreatedOn)=@reservationDate))
			AND RR.CreatedBy = @CreatedByUser 
			AND (rr.StatusId in (5,6,7))  --(OS.OrderStatusId=5 or OS.OrderStatusId=7 or OS.OrderStatusId=6)
			AND (1>(Select count(value) from [dbo].uCommerce_OrderProperty 
			Where [Key] = '_appliedcampaign' AND OrderLineId = OL.OrderLineId))
			ORDER BY CreatedOn DESC
	
	UNION
		 SELECT OL.OrderLineId,
                OL.OrderId,			      
                PO.OrderGuid,
                '' as [OfferName],
                CONCAT(oa.FirstName, ' ', oa.LastName) as [FullName], 
                rr.BookingNo as [BookingReferenceId],
                CONCAT( Replace(Convert(varchar(11), RR.CheckInDate,103),' ','/'), '   ', Replace(Convert(varchar(11), RR.CheckOutDate,103),' ','/')) as [CheckInCheckOutDates], 
                op.NoOfAdult, 
                op.NoOfChild, 
                CONCAT(CONVERT(varchar(20),OL.CreatedOn,103), ' ', CONVERT(VARCHAR(8), OL.CreatedOn, 108)) as 'CreatedOn',
			   op.BookingThrough as [Booking Method], 
               OS.Name as [BookingStatus], 
               CONCAT(ppd.HotelName,' ', PPD.RoomType,'',PPD.AdOnsType) as [Accommodation]

	    FROM [dbo].uCommerce_OrderLine OL 
	    INNER JOIN
	    uCommerce_PurchaseOrder PO on po.OrderId=ol.OrderId
		INNER JOIN 
		uCommerce_OrderAddress oa on oa.OrderId=po.OrderId
		INNER JOIN 
		LB_BookingInfo rr on rr.OrderLineId =ol.OrderLineId
		INNER JOIN 
		Ucommerce_product P ON OL.Sku = P.Sku AND P.ParentProductId IS NULL 
		INNER JOIN
			  (
			    SELECT OrderLineId,OrderId, _NoOfAdult [NoOfAdult], _NoOfChild [NoOfChild],_BookingThrough [BookingThrough]
				 FROM 
			 (
				 SELECT OrderLineId,OrderId, [key], [value]
				FROM uCommerce_OrderProperty  
			 ) src
				pivot
			(
				MAX([value])
				FOR [key] in (_NoOfAdult, _NoOfChild,_BookingThrough)
			) piv
			) OP ON op.OrderLineId=ol.OrderLineId
	 INNER JOIN
	 uCommerce_ProductDefinition PD ON P.ProductDefinitionId=PD.ProductDefinitionId AND PD.Name='Hotel'
	 INNER JOIN 
			 (
				SELECT ProductId, RoomType, HotelName,AdOnsType
				 FROM 
			(
				SELECT ProductId,PD.Name [key], PP.value
				FROM uCommerce_ProductDefinitionField PD 
				INNER JOIN 
				uCommerce_ProductProperty PP ON PD.ProductDefinitionFieldId=PP.ProductDefinitionFieldId
				WHERE PD.Name IN ('RoomType','HotelName','AdOnsType')
			) src
			pivot
		   (
			MAX([value])
			FOR [key] in (RoomType, HotelName)
			) piv
		    ) PPD ON PPD.ProductId=P.ProductId 
	 LEFT JOIN 
	 uCommerce_OrderLineCampaignRelation OLCR ON OL.OrderLineId=OLCR.OrderLineId  
	LEFT JOIN 
	 ucommerce_campaignitem CI ON OLCR.CampaignItemId=CI.CampaignItemId	
     INNER JOIN
			 (
				 SELECT CustomerId,_ClientId [ClientId]
				 FROM 
		    (
			   SELECT CustomerId, [key], [value]
				FROM LB_CustomerProperty 
			 ) src
			  pivot
			 (
			    MAX([value])
			    FOR [key] in (_ClientId) 
			  ) piv
		      ) CP ON PO.CustomerId=CP.CustomerId 
     INNER JOIN 
     uCommerce_OrderStatus OS on OS.OrderStatusId=BI.StatusId
	 WHERE 
			 (CA.ClientID = @clientIds) 
			AND ((@reservationDate IS NULL) OR (CONVERT(date,OL.CreatedOn)=@reservationDate))
			AND RR.CreatedBy = @CreatedByUser 
			
			AND (RR.StatusId in (5,6,7))  --(OS.OrderStatusId=5 or OS.OrderStatusId=7 or OS.OrderStatusId=6)
			AND (1>(Select count(value) from [dbo].uCommerce_OrderProperty 
			Where [Key] = '_appliedcampaign' AND OrderLineId = OL.OrderLineId))
			ORDER BY CreatedOn DESC
	END
	
	
