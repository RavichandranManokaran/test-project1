﻿create PROCEDURE [dbo].[LB_GetCustomerList]( 
	@ClientIds varchar(500)=NULL,
	@FirstName nvarchar(512)=NULL,
	@LastName nvarchar(512)=NULL,
	@EmailAddress nvarchar(255)=NULL,
	@Phone nvarchar(50)=NULL,
	@MobilePhone nvarchar(50)=NULL,
	@Title varchar(50)=NULL,
	@AddressLine1 nvarchar(50)=NULL,
	@AddressLine2 nvarchar(50)=NULL,
	@AddressLine3 nvarchar(50)=NULL,
	@CountryId int=NULL,
	@LocationId VARCHAR(50)=NULL,
	@BranchId VARCHAR(50)=NULL)
AS 
/*ClientIds, First Name and Last Name are mandatory Fields. Search excluded Acive and Subscribe fields since they have free defined values*/
BEGIN

	SELECT 
			  C.CustomerId,
              CONCAT(C.FirstName, ' ', C.LastName) as FullName , 
              RTRIM(concat( ISNULL(OP.AddressLine1,UA.Line1), IIF(ISNULL(OP.AddressLine2,UA.Line2) IS NULL OR ISNULL(OP.AddressLine2,UA.Line2)='','',concat(', ',ISNULL(OP.AddressLine2,UA.Line2))), IIF(ISNULL(OP.AddressLine3,UA.City) IS NULL OR ISNULL(OP.AddressLine3,UA.City)='','',concat(', ', ISNULL(OP.AddressLine3,UA.City))))) as FullAddress,
			  C.EmailAddress,
              C.MobilePhoneNumber
	FROM 
              uCommerce_Customer C
              INNER JOIN 
              (
								SELECT  
									CustomerId,
									_ClientId [ClientId],
									_Title [Title],
									_AddressLine1 [AddressLine1],
									_AddressLine2 [AddressLine2], 
									_AddressLine3 [AddressLine3],
									_CountryId [CountryId],
									_LocationId [LocationId],
									_BranchId [BranchId],
									_Subscribe [Subscribe],
									_BookingReference [BookingReference],
									_Active [Active]
                                  FROM 
                                  (
                                    SELECT CustomerId, [key], [value]
                                    FROM LB_CustomerProperty 
                                  ) src
                                  pivot
                                  (
                                    MAX([value])
                                    FOR [key] in (_ClientId,_Title,
									_AddressLine1,_AddressLine2,_AddressLine3,
									_CountryId,_LocationId,_BranchId,_Subscribe,_BookingReference,_Active)
                                  ) piv
              ) OP ON OP.CustomerId=C.CustomerId 
			  Left Outer Join
			  uCommerce_Address UA
			  ON
			  UA.CustomerId = C.CustomerId
			WHERE 
				C.FirstName Like '%'+ @FirstName + '%' 
			AND C.LastName Like '%'+ @LastName + '%' 
			AND (@ClientIds IS NULL OR OP.ClientId=@ClientIds)
			AND (@EmailAddress IS NULL OR C.EmailAddress=@EmailAddress)  
			AND (@Phone IS NULL OR C.PhoneNumber = @Phone) 
			AND (@MobilePhone IS NULL OR C.MobilePhoneNumber =@MobilePhone) 
			AND (@Title IS NULL OR OP.Title=@Title) 
			AND (@addressLine1 IS NULL OR OP.AddressLine1 Like '%' + @AddressLine1 + '%') 
			AND (@addressLine2 IS NULL OR OP.AddressLine2 Like '%' + @AddressLine2 + '%') 
			AND (@addressLine3 IS NULL OR OP.AddressLine3 Like '%' + @AddressLine3 + '%') 
			AND (@CountryId IS NULL  OR OP.CountryId=@CountryId)  
			AND (@LocationId IS NULL OR OP.LocationId=@LocationId)  
			AND (@BranchId IS NULL OR OP.BranchId=@BranchId)

END