﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           CurrencyHelper.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Code is used to get the Currency symbol
/// </summary>

#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#endregion

namespace Affinion.LoyaltyBuild.Common.Utilities
{
    public static class CurrencyHelper
    {

        #region Public Methods
        
        /// <summary>
        /// Gets the currency symbol
        /// </summary>
        /// <param name="currencyType">CurrencyType</param>
        /// <returns>Currency Symbol</returns>
        public static string GetCurrency(string currencyType)
        {
            if (Currencies != null && Currencies.Any(i => i.CurrencyCode == currencyType))
                return Currencies.First(i => i.CurrencyCode == currencyType).Symbol;

            return string.Empty;
        }
        
        /// <summary>
        /// get currency symbol based on Id
        /// </summary>
        /// <param name="currencyId"></param>
        /// <returns></returns>
        public static string GetCurrency(int currencyId)
        {
            if (Currencies != null && Currencies.Any(i => i.CurrencyId == currencyId))
                return Currencies.First(i => i.CurrencyId == currencyId).Symbol;

            return string.Empty;
        }

        /// <summary>
        /// Gets the currency symbol
        /// </summary>
        /// <param name="currencyType">CurrencyType</param>
        /// <returns>Currency Symbol</returns>
        public static string FormatCurrency(this decimal value, string currencyType)
        {
            return string.Format("{0} {1}", GetCurrency(currencyType), value.ToString("F"));
        }

        /// <summary>
        /// get currency symbol based on Id
        /// </summary>
        /// <param name="currencyId"></param>
        /// <returns></returns>
        public static string FormatCurrency(this decimal value, int currencyId)
        {
            return string.Format("{0} {1}", GetCurrency(currencyId), value.ToString("F"));
        }

        #endregion

        private static Object key = new Object();
        private static List<LoyaltyCurrency> _currencies = null;

        /// <summary>
        /// Get Currencies
        /// </summary>
        /// <returns></returns>
        private static List<LoyaltyCurrency> Currencies
        {
            get
            {
                if (_currencies == null)
                {
                    lock (key)
                    {
                        _currencies = GetCurrencies();
                    }
                }

                return _currencies;
            }
        }

        /// <summary>
        /// get currencies from sitecore
        /// </summary>
        /// <returns></returns>
        private static List<LoyaltyCurrency> GetCurrencies()
        {
            var currencies = new List<LoyaltyCurrency>();
            var currencyPath = @"fast:/sitecore/content/#admin-portal#/global/#_supporting-content#/currency/*[@@templatename='Currency']";
            var currencyItems = Sitecore.Context.Database.SelectItems(currencyPath);

            if(currencyItems != null )
            {
                foreach (var item in currencyItems)
                {
                    int currencyId = 0;
                    int.TryParse(item["UCommerceId"], out currencyId);
                    if(currencyId > 0)
                    {
                        currencies.Add(new LoyaltyCurrency
                        {
                            CurrencyId = currencyId,
                            CurrencyCode = item["Currency Name"],
                            Symbol = item["CurrentSymbol"]
                        });
                    }
                }
            }

            return currencies;
        }
    }

    /// <summary>
    /// Currency
    /// </summary>
    internal class LoyaltyCurrency
    {
        /// <summary>
        /// currency id
        /// </summary>
        public int CurrencyId { get; set; }

        /// <summary>
        /// currency code
        /// </summary>
        public string CurrencyCode { get; set; }

        /// <summary>
        /// Symbol
        /// </summary>
        public string Symbol { get; set; }
    }
}
