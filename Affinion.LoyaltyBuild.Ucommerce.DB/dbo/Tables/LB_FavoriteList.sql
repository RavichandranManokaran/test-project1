﻿CREATE TABLE [dbo].[LB_FavoriteList] (
    [ListID]          INT            IDENTITY (100, 1) NOT NULL,
    [CustomerID]      INT            NULL,
    [ListName]        VARCHAR (50)   NULL,
    [HotelID]         VARCHAR (1000) NULL,
    [CreatedBy]       VARCHAR (50)   NULL,
    [UpdatedBy]       VARCHAR (50)   NULL,
    [DateCreated]     DATETIME       NULL,
    [DateLastUpdated] DATETIME       NULL,
    CONSTRAINT [PK_uCommerce_FavoriteList] PRIMARY KEY CLUSTERED ([ListID] ASC)
);

