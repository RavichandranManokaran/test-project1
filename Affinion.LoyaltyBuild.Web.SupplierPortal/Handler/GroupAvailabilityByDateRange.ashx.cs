﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.Availability;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.Availability.SupplierPortal.Controls;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;
using Affinion.LoyaltyBuild.UCom.Common.Extension;
using System;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Availability;


namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Handler
{
    /// <summary>
    /// Summary description for GroupAvailabilityByDateRange
    /// </summary>
    public class GroupAvailabilityByDateRange : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                String loggedInProvider_ucom = SitecoreUserProfile.GetItem(SessionKeys.loggedInSupplier_ucom);
                Guid ucomCategoryId;
                bool validGuid = Guid.TryParse(loggedInProvider_ucom,out ucomCategoryId);
                if (validGuid)
                {
                    context.Response.ContentType = "text/plain";
                    string sDate = context.Request["startDate"];
                    string eDate = context.Request["endDate"];
                    string month = context.Request["month"];
                    if (!String.IsNullOrEmpty(sDate) && !String.IsNullOrEmpty(eDate))
                    {
                        DateTime startDate = sDate.ParseFormatDateTime();
                        DateTime endDate = eDate.ParseFormatDateTime(); 
                        context.Response.Write(LoadUserControl(startDate, endDate, Convert.ToInt32(month), context));
                        if (Convert.ToInt32(month) == 0)
                        {
                            SessionHelper.AddItem(startDate, SessionKeys.AvailabilityStartDate);
                            SessionHelper.AddItem(endDate, SessionKeys.AvailabilityEndDate);
                        }
                    }
                }
                else
                {
                    context.Response.Write("Please configure hotel in sitecore to manage room availability ");
                }

            }
            catch (Exception ex)
            {
                context.Response.Write("Error loading calendar View");
                Sitecore.Diagnostics.Log.Error(ex.Message, new object());
                string data = ex.Message;
            }
        }

        protected static string LoadUserControl(DateTime startDate, DateTime endDate, int month, HttpContext context)
        {
            using (Page page = new Page())
            {
                StringBuilder s = new StringBuilder();
                string hotelName=string.Empty;
                List<ProviderAvialbilityData> avialbilityData = new List<ProviderAvialbilityData>();
                AvailabilityHelper staticDataForRoom = new AvailabilityHelper(context);

                avialbilityData = staticDataForRoom.RoomAvailibilityByDateRange(startDate, endDate);
                PlaceHolder f = new PlaceHolder();
                SupplierInfo data = staticDataForRoom.GetLoggedInSupplierDetailById();
                if (data != null)
                {
                    hotelName = string.Format("{0}{1}{2}{3}{4}{5}", data.SupplierName,
                        !string.IsNullOrEmpty(data.Group) ? string.Format(" /{0}", data.Group) : string.Empty,
                        !string.IsNullOrEmpty(data.SuperGroup) ? string.Format(" /{0}", data.SuperGroup) : string.Empty,
                        !string.IsNullOrEmpty(data.MapLocation) ? string.Format(", {0}", data.MapLocation) : string.Empty,
                        !string.IsNullOrEmpty(data.Country) ? string.Format(", {0}", data.Country) : string.Empty,
                        !string.IsNullOrEmpty(data.StarRating) ? string.Format(", Rating: {0}", data.StarRating) : string.Empty);
                }
                foreach (var item in avialbilityData)
                {
                    if (month != 0 && item.StatDate.GetMonthFromJulianFixed() != month)
                    {
                        continue;
                    }
                    using (StringWriter writer = new StringWriter(CultureInfo.CurrentCulture))
                    {
                        GroupAvailabilityCalendarView gavp = (GroupAvailabilityCalendarView)page.LoadControl("~/Layouts/SubLayouts/SupplierPortal/Controls/Availability/GroupAvailabilityCalendarView.ascx");
                        gavp.currenItem = item;
                        gavp.hotelName = hotelName;
                        f.Controls.Add(gavp);
                        page.Controls.Add(f);
                        HttpContext.Current.Server.Execute(page, writer, false);
                        s.Clear();
                        s.Append(writer.ToString());
                    }
                }
                return s.ToString();
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

    }
}