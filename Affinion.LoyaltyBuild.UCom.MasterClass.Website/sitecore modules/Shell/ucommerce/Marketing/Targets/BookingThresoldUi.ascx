﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BookingThresoldUi.ascx.cs" Inherits="Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.ucommerce.Marketing.Targets.BookingThresoldUi" %>
<table cellpadding="0" cellspacing="0" style="width: 100%;">
    <tr>
        <td>
            <asp:PlaceHolder runat="server" ID="NonEditModePlaceHolder">
                <asp:Literal runat="server" ID="ReadOnlyMessage"></asp:Literal>
            </asp:PlaceHolder>

            <asp:PlaceHolder runat="server" ID="EditModePlaceHolder" Visible="false">
                <p>
                    Min Hours :<asp:TextBox runat="server" ValidationGroup="compareValidate" ID="txtMinHours"></asp:TextBox><br />
                    Max Hours :<asp:TextBox runat="server" ValidationGroup="compareValidate" ID="txtMaxHours"></asp:TextBox>
                </p>
                <br />

                <asp:RegularExpressionValidator ID="rfvMinHours" runat="server"
                    ControlToValidate="txtMinHours" ErrorMessage="Invalid value." 
                    ValidationExpression="^(0|[1-9][0-9]*)$" Display="Dynamic"></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator ID="rfvMaxHours" runat="server"
                    ControlToValidate="txtMaxHours" ErrorMessage="Invalid value."
                    ValidationExpression="^(0|[1-9][0-9]*)$" Display="Dynamic"></asp:RegularExpressionValidator>

             <asp:CustomValidator ID="rfvValidateHours" runat="server" OnServerValidate="ValidateHours" ControlToValidate="txtMinHours" ValidationGroup="compareValidate" ForeColor="Red" ErrorMessage="Min hours should be less than max hours." Display="Dynamic" />
            </asp:PlaceHolder>
        </td>
        <td style="width: 50px; text-align: right; vertical-align: top;">
            <asp:ImageButton ID="EditButton" runat="server" ImageUrl="../../Images/pencil.png" meta:resourcekey="Edit" OnClick="EditButton_Click" />
            <asp:ImageButton ID="SaveButton" runat="server" ImageUrl="../../Images/save.gif" meta:resourcekey="Save" Visible="false" ValidationGroup="compareValidate" OnClick="SaveButton_Click" />
            <asp:ImageButton ID="DeleteButton" runat="server" ImageUrl="../../Images/ui/cross.png" meta:resourcekey="Delete" OnClick="DeleteButton_Click" /><br />
        </td>
    </tr>
</table>



