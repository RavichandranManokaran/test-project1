﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SuperValuPromoBannerSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.SuperValuPromoBannerSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="col-xs-12">
    <div id="four-great-reasons">
        <asp:Literal ID="PlaceholderText" Text="Set the datasource: Required Fields: Link, DesktopImage, PromotionalText" runat="server" />
        <sc:Link Field="Link" ID="Link" runat="server">
            <sc:Image Field="DesktopImage" CssClass="visible-sm visible-md visible-lg img-responsive img-enlarge"  ID="Image" runat="server" />
        </sc:Link>
    </div>
    <div id="exclusions">
        <sc:Text Field="PromotionalText1" ID="PromotionalText" runat="server" />
    </div>
</div>
