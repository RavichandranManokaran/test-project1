﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class RoomAvailability : IEntity
    {
        private int? _hashCode;


        // Methods
        public static IQueryable<RoomAvailability> All()
        {
            return GetRepo().Select();
        }

        public virtual void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<RoomAvailability, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            RoomAvailability objA = obj as RoomAvailability;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<RoomAvailability, bool>> expression)
        {
            return All().Any<RoomAvailability>(expression);
        }

        public static IList<RoomAvailability> Find(Expression<Func<RoomAvailability, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<RoomAvailability>();
        }

        public static RoomAvailability FirstOrDefault(Expression<Func<RoomAvailability, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<RoomAvailability>();
        }

        public static RoomAvailability Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<RoomAvailability> GetRepo()
        {
            
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<RoomAvailability>>();
            
        }


        public virtual void Save()
        {
            GetRepo().Save(this);
        }

        public static RoomAvailability SingleOrDefault(Expression<Func<RoomAvailability, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties
        public virtual int Id
        {
            get
            {
                return 0;
            }
        }

        public virtual Guid RoomAvailibilityID { get; set; }
        public virtual string Sku { get; set; }
        public virtual string VariantSku { get; set; }
        public virtual int NumberAllocated { get; set; }
        public virtual int NumberBooked { get; set; }
        public virtual int DateCounter { get; set; }
        public virtual string CreatedBy { get; set; }
        public virtual string UpdateBy { get; set; }
        public virtual DateTime DateCreated { get; set; }
        public virtual DateTime DateLastUpdate { get; set; }
        public virtual bool IsStopSellOn { get; set; }
        public virtual bool IsCloseOut { get; set; }
        public virtual decimal Price { get; set; }
        public virtual decimal PackagePrice { get; set; }
        public virtual string PriceBand { get; set; }  

    }
}
