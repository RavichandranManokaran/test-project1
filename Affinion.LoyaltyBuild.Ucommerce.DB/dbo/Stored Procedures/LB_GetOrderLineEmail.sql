﻿CREATE Procedure [dbo].[LB_GetOrderLineEmail]
(
@Email nvarchar(100),
@LastName nvarchar(100)
)
As
Begin
	select OL.orderlineid, 
		   CA._ClientId [ClientId] 
	from ucommerce_orderline OL
    inner join LB_BookingInfo BI on OL.OrderLineId = BI.OrderLineId
    inner join ucommerce_purchaseorder PO on OL.OrderId = PO.OrderId
    inner join uCommerce_Customer C on PO.CustomerId = C.CustomerId
	inner join (
					Select CustomerId, _ClientId
					from
					(
						Select CustomerId, [KEY], [Value]
						from LB_CustomerProperty
					) Src
					pivot
					(
						max([Value])
						for [Key] in (_ClientId)
					) Piv
			   ) CA on C.CustomerId= CA.CustomerId
    
	where C.EmailAddress = @Email and C.LastName=@LastName and  BI.CheckInDate>= getdate()
End
