﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           OfferSubrateFilter.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Search (VS project name)
/// Description:           Checks the subrate settings of offers
/// </summary>
namespace Affinion.LoyaltyBuild.Search.OfferFilters
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class OfferSubrateFilter : IOfferFilter
    {
        /// <summary>
        /// Returns true if the offer is valid and fine to show on client page
        /// </summary>
        /// <param name="filterData">Filter meta data</param>
        /// <returns>Returns true to include the offer</returns>
        public bool IncludeOffer(OfferFilterData filterData)
        {
            if (filterData != null && filterData.Offer != null)
            {
                return filterData.Offer.SubrateApplied;
            }

            return false;
        }
    }
}
