

/****** Object:  StoredProcedure [dbo].[LB_BB_getRegionAssigments]    Script Date: 7/19/2016 7:26:46 PM ******/
---- ====================================================
---- Description:	RETURNS MARGIN FOR A PROPERTY BASED ON AVAILABILITY SOURCE
---- Written: RICHARD
---- Created: 2016.04.19
---- ====================================================

CREATE PROCEDURE [dbo].[LB_BB_getRegionAssigments]
	
	-- Input parameters 
	@pi_regionid int,
	--Return parameters
	@po_returnstatus INT OUTPUT,
	@po_returnmsg VARCHAR(200) OUTPUT

	
AS
set nocount on;
BEGIN

	--Assume transaction will fail, set the return variable to -1.
	SET @po_returnstatus = -1;
	SET @po_returnmsg = null;

	
	
	BEGIN TRY
	
	Select Distinct CountryName, mapFromRecordID RegionID,l.Region,SEMl.MaplocationID,MaplocationName from LB_BB_mappings
	inner join syn_emosMaplocation SEML on SEML.maplocationid = LB_BB_mappings.maptoRecordID
	inner join syn_emosProvider SEP on SEP.MaplocationID = SEML.MaplocationID
	inner join syn_emosLocation SEL on SEL.locationID = SEP.locationID
	inner join syn_emosCountryLingual CL on CL.Countryid = SEL.Countryid
	and Cl.languageid = 1
	inner join LB_BB_Location l on l.RegionID = LB_BB_mappings.mapFromRecordID
	where mapFrom = 'Location'
	and mapsFromColumn = 'Region'
	and mapsTo = 'Maplocation'
	and mapsToColumn = 'Maplocationid'
	and mapFromRecordID = @pi_regionid
	and countryname <> 'Vatican City State'
	Order by CountryName, MaplocationName,mapFromRecordID,l.Region,SEMl.MaplocationID
	
	
	Select Distinct CountryName,SEML.MaplocationID,MaplocationName from syn_emosMaplocation SEML
	inner join syn_emosProvider SEP on SEP.MaplocationID = SEML.MaplocationID
	inner join syn_emosLocation SEL on SEL.locationID = SEP.locationID
	inner join syn_emosCountryLingual CL on CL.Countryid = SEL.Countryid
	and Cl.languageid = 1
	where status = 1 and SEML.maplocationid not in (
	Select maplocationid from LB_BB_mappings
	inner join syn_emosMaplocation SEML on SEML.maplocationid = LB_BB_mappings.maptoRecordID
	inner join LB_BB_Location l on l.RegionID = LB_BB_mappings.mapFromRecordID
	where mapFrom = 'Location'
	and mapsFromColumn = 'Region'
	and mapsTo = 'Maplocation'
	and mapsToColumn = 'Maplocationid'
	and mapFromRecordID = @pi_regionid)
	and countryname <> 'Vatican City State'
	Order by CountryName,MaplocationName 
      
    END TRY	
	BEGIN CATCH	  
		SET @po_returnstatus = error_number();
		SET @po_returnmsg = error_message();
		RETURN;
	END CATCH
	
	SET @po_returnstatus = 0;
	SET @po_returnmsg = 'Completed successfully.';	
END













GO

