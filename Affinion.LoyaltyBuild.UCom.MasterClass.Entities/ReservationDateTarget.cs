﻿using Affinion.LoyaltyBuild.UCom.Common.Constants;
using Affinion.LoyaltyBuild.UCom.Common.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;
using UCommerce.Marketing.Targets;



namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class ReservationDateTarget : Target, IOrderLineTarget, IEntity, ITarget
    {
        private int? _hashCode;

        public virtual bool IsSatisfiedBy(OrderLine orderLine)
        {
            OrderProperty searchIgnor = orderLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.IgnorCampaignSearchCriteria);
            if (searchIgnor != null)
                return true;
            OrderProperty reservationDateProperty = orderLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.ReservationDateKey);
            if (reservationDateProperty != null)
            {
                DateTime reservationDate = reservationDateProperty.Value.ParseFormatDateTime();
                if (reservationDate >= this.ReservationFromDate && reservationDate <= this.ReservationToDate)
                {
                    return true;
                }
            }
            return false;

        }



        // Methods
        public new static IQueryable<ReservationDateTarget> All()
        {
            return GetRepo().Select();
        }

        public override void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<ReservationDateTarget, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            ReservationDateTarget objA = obj as ReservationDateTarget;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<ReservationDateTarget, bool>> expression)
        {
            return All().Any<ReservationDateTarget>(expression);
        }

        public static IList<ReservationDateTarget> Find(Expression<Func<ReservationDateTarget, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<ReservationDateTarget>();
        }

        public static ReservationDateTarget FirstOrDefault(Expression<Func<ReservationDateTarget, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<ReservationDateTarget>();
        }

        public new static ReservationDateTarget Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<ReservationDateTarget> GetRepo()
        {
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<ReservationDateTarget>>();
            
        }


        public override void Save()
        {
            GetRepo().Save(this);
        }

        public static ReservationDateTarget SingleOrDefault(Expression<Func<ReservationDateTarget, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties
        public override int Id
        {
            get
            {
                return this.ReservationDateTargetId;
            }
        }

        public virtual int ReservationDateTargetId { get; protected set; }

        public virtual DateTime ReservationFromDate { get; set; }

        public virtual DateTime ReservationToDate { get; set; }

    }
}
