﻿using System;

namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Model.Room
{
    [Serializable]
    public class Erratum
    {
        public string Subject { get; set; }

        public string Description { get; set; }
    }
}
