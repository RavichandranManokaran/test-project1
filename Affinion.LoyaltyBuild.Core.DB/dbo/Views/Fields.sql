﻿CREATE VIEW dbo.Fields
AS
SELECT     Id, ItemId, '' AS Language, FieldId, Value, Created, Updated
FROM         dbo.SharedFields
UNION ALL
SELECT     Id, ItemId, Language, FieldId, Value, Created, Updated
FROM         dbo.UnversionedFields
UNION ALL
SELECT     dbo.VersionedFields.Id, dbo.VersionedFields.ItemId, dbo.VersionedFields.Language, dbo.VersionedFields.FieldId, dbo.VersionedFields.Value, 
                      dbo.VersionedFields.Created, dbo.VersionedFields.Updated
FROM         dbo.VersionedFields INNER JOIN
                          (SELECT     ItemId, Language, MAX(Version) AS Version
                            FROM          dbo.VersionedFields AS VersionedFields_1
                            GROUP BY ItemId, Language) AS b ON dbo.VersionedFields.ItemId = b.ItemId AND dbo.VersionedFields.Language = b.Language AND 
                      dbo.VersionedFields.Version = b.Version
