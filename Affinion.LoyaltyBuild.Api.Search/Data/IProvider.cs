﻿using Affinion.LoyaltyBuild.Model.Provider;

namespace Affinion.LoyaltyBuild.Api.Search.Data
{
    /// <summary>
    /// Base Provider interface
    /// </summary>
    public interface IProvider
    {
        /// <summary>
        /// Provider Id
        /// </summary>
        string ProviderId { get; set; }

        /// <summary>
        /// SitecoreItemId
        /// </summary>
        string SitecoreItemId { get; set; }

        /// <summary>
        /// provider type
        /// </summary>
        ProviderType ProviderType { get; set; }
    }
}
