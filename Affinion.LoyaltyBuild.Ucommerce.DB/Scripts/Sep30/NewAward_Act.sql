declare @id int = 0

insert into ucommerce_entityui values('Affinion.LoyaltyBuild.UCom.MasterClass.Entities.FreeNightAward, Affinion.LoyaltyBuild.UCom.MasterClass.Entities','Awards/FreeNightAwardUi.ascx',1)
set @id = @@identity
insert into uCommerce_EntityUiDescription values(@id,'Free Night(s) Discount','Free Night(s) Discount','en')

insert into ucommerce_entityui values('Affinion.LoyaltyBuild.UCom.MasterClass.Entities.CommissionAward, Affinion.LoyaltyBuild.UCom.MasterClass.Entities','Awards/CommissionAwardUi.ascx',1)
set @id = @@identity
insert into uCommerce_EntityUiDescription values(@id,'Discount On Commision','Discount On Commision','en')

insert into ucommerce_entityui values('Affinion.LoyaltyBuild.UCom.MasterClass.Entities.HotelBreakAward, Affinion.LoyaltyBuild.UCom.MasterClass.Entities','Awards/HotelBreakAwardUi.ascx',1)
set @id = @@identity
insert into uCommerce_EntityUiDescription values(@id,'Discount On Hotel Break','Discount On Hotel Break','en')


insert into ucommerce_entityui values('Affinion.LoyaltyBuild.UCom.MasterClass.Entities.ProcessingFeeAward, Affinion.LoyaltyBuild.UCom.MasterClass.Entities','Awards/ProcessingFeeAwardUi.ascx',1)
set @id = @@identity
insert into uCommerce_EntityUiDescription values(@id,'Discount On Processing Fee','Discount On Processing Fee','en')

insert into ucommerce_entityui values('Affinion.LoyaltyBuild.UCom.MasterClass.Entities.BookingDateThresoldTarget, Affinion.LoyaltyBuild.UCom.MasterClass.Entities','Targets/BookingThresoldUi.ascx',1)
set @id = @@identity
insert into uCommerce_EntityUiDescription values(@id,'Time Limited Offer Saving','Time Limited Offer Saving','en')

insert into ucommerce_entityui values('Affinion.LoyaltyBuild.UCom.MasterClass.Entities.PackageTypeTarget, Affinion.LoyaltyBuild.UCom.MasterClass.Entities','Targets/PackageTypeUi.ascx',1)
set @id = @@identity
insert into uCommerce_EntityUiDescription values(@id,'Package type','Package type','en')

insert into ucommerce_entityui values('Affinion.LoyaltyBuild.UCom.MasterClass.Entities.PortalTypeTarget, Affinion.LoyaltyBuild.UCom.MasterClass.Entities','Targets/PortalTypeUi.ascx',1)
set @id = @@identity
insert into uCommerce_EntityUiDescription values(@id,'PortalType','PortalType','en')

insert into ucommerce_entityui values('Affinion.LoyaltyBuild.UCom.MasterClass.Entities.OrderLineRestrictionTarget, Affinion.LoyaltyBuild.UCom.MasterClass.Entities','Targets/OrderLineRestrictionUi.ascx',1)
set @id = @@identity
insert into uCommerce_EntityUiDescription values(@id,'OrderLine Restriction','OrderLine Restriction','en')

Go
Declare @datatypeid int, @definitionid int
declare @definationfieldid int = 0

select @definitionid=ProductDefinitionId from uCommerce_ProductDefinition where Name='Hotel'
select @datatypeid = DataTypeId from uCommerce_DataType where DefinitionName= 'Boolean'
if(@datatypeid <> 0)
begin
if not exists(select 1 from uCommerce_ProductDefinitionField where ProductDefinitionId = @definitionid and name = 'WeekendSerchRestriction')
begin
	   insert into uCommerce_ProductDefinitionField(DataTypeId,ProductDefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,Facet,Guid,IsVariantProperty)
	   values(@datatypeid,@definitionid,'WeekendSerchRestriction',1,0,1,0,0,0,1,NEWID(),0)
	   set @definationfieldid = @@Identity
	   insert into uCommerce_ProductDefinitionFieldDescription(CultureCode,DisplayName,Description,ProductDefinitionFieldId)
	   values ('en' , 'WeekendSerchRestriction','',@definationfieldid)
end
end
--Procedure Update : [GetProductByCategoryIds]
--Table Added : LB_CommissionAward , LB_ProcessingFeeAward ,LB_HotelBreakAward,LB_FreeNightAward
-- New table Added -- [LB_OrderLineRestrictionTarget],[LB_PackageTypeTarget],[LB_PortalTypeTarget]

--Alter Table

--Alter table LB_NightTarget
--add LastArrivalDay varchar(50) 

--Alter table LB_SupplierInvite
--add Offerinformation varchar(MAX) 