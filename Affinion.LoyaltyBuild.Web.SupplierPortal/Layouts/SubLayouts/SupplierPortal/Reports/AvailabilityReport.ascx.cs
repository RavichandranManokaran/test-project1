﻿using Affinion.LoyaltyBuild.SupplierPortal.Reports.Filters;
using Affinion.LoyaltyBuild.SupplierPortal.Reports.Service;
using Affinion.LoyaltyBuild.SupplierPortal.Reports.Test;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration.SupplierPortal.Reports;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration.SupplierPortal.User;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Affinion.LoyaltyBuild.SupplierPortal.DataAccess.Service;
using System.Security;
using System.Security.Permissions;
using Sitecore.Data.Items;
using Affinion.LoyaltyBuild.Common.Utilities;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Reports
{
    /// <summary>
    /// Availability report user control
    /// </summary>
    public partial class AvailabilityReport : ReportBase
    {
        public AvailabilityReport()
            : base(new ProviderService(), new ReportService())
        {

        }

        /// <summary>
        /// page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            BindSitecoreText();
            dateFromCalendarExtender.StartDate = DateTime.Now.AddMonths(-12);
            dateToCalendarExtender.EndDate = DateTime.Now.AddMonths(12);
            if (!IsPostBack)
            {
                BindDropdown(ddlProvider, ProviderList, "Name", "Id");
            }
            if (UserHelper.CurrentUserRole == UserRole.Supplier)
            {
                providerLabel.Visible = false;
                providerDropDown.Visible = false;
            }

        }

        /// <summary>
        /// display reports click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Display_Click(object sender, EventArgs e)
        {
            DateTime Datefrom = Convert.ToDateTime(txtDatefrom.Text);
            DateTime DateTo = Convert.ToDateTime(txtDateto.Text);


            if (Datefrom <= DateTo)
            {
                TimeSpan daycount = Convert.ToDateTime(DateTo).Subtract(Convert.ToDateTime(Datefrom));
                int dacount1 = Convert.ToInt32(daycount.Days);
                if ((dacount1 < 366))
                {
                    ShowAvailabiltyReport();
                }

                else
                {
                    cusValDateTo.IsValid = false;
                    
                }
            }
        }

        private void ShowAvailabiltyReport()
        {
            //get the report assembly to load report definition of rdlc files
            Assembly assembly = Assembly.GetAssembly(typeof(Affinion.LoyaltyBuild.SupplierPortal.Reports.IReportService));

            var dateFrom = GetDateValue(txtDatefrom.Text);
            //get date to
            var dateTo = GetDateValue(txtDateto.Text);

            int? providerId = null;
            string showProvider;
            if (UserHelper.CurrentUserRole == UserRole.Supplier)
            {
                
                var sku = SitecoreUserProfile.SelectedSKU;
                if (!String.IsNullOrWhiteSpace(sku))
                {
                    var provider = _providerService.GetProviderById(sku);
                    if (provider != null)
                        providerId = provider.Id;
                }
                showProvider = "false";
            }
            else
            {
                int tempProvider = 0;
                if (int.TryParse(ddlProvider.SelectedValue, out tempProvider))
                    providerId = tempProvider;
                showProvider = "true";
            }


            //load report
            using (var stream = assembly.GetManifestResourceStream("Affinion.LoyaltyBuild.SupplierPortal.Reports.Reports.AvailabilityReport.rdlc"))
            {
                var objShowRecord = _reportService.GetAvailability(new AvailabilityReportFilter()
                {
                    DateFrom = dateFrom,
                    DateTo = dateTo,
                    ProviderId = providerId
                });
                this.rvDisplayrecord.KeepSessionAlive = false;
                this.rvDisplayrecord.AsyncRendering = false;
                rvDisplayrecord.ProcessingMode = ProcessingMode.Local;
                rvDisplayrecord.LocalReport.SetBasePermissionsForSandboxAppDomain(new PermissionSet(PermissionState.Unrestricted));
                rvDisplayrecord.LocalReport.LoadReportDefinition(stream);
                var datasource = new ReportDataSource("DataSetAvailability", objShowRecord);
                rvDisplayrecord.LocalReport.SetParameters(new ReportParameter("ShowProvider", showProvider));
                rvDisplayrecord.LocalReport.DataSources.Clear();
                rvDisplayrecord.LocalReport.DataSources.Add(datasource);
            }
        }
        public void BindSitecoreText()
        {
            Item currentPageItem = Sitecore.Context.Item;
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Title", AvailabityReportTitle);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "AvailabityReportIformation", AvailabityReportInformation);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "DateFrom", datefromText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "DateTo", dateToText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Provider", ProviderText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Submit", SubmitText);
        }
    }

}