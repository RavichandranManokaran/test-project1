﻿using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Helper
{
    public class AssignWorkFlow
    {
        public static void AwaitingForApproval(Item itemID)
        {

            itemID.Fields[Sitecore.Data.ID.Parse(Constants.workFlowFieldID)].Value = Constants.newWorkFlowFieldID;
            itemID.Fields[Sitecore.Data.ID.Parse(Constants.workflowStateFieldID)].Value = Constants.newWorkflowStateFieldID;
        }
        public static void Approved(Item itemID)
        {

            itemID.Fields[Sitecore.Data.ID.Parse(Constants.workFlowFieldID)].Value = Constants.newWorkFlowFieldID;
            itemID.Fields[Sitecore.Data.ID.Parse(Constants.workflowStateFieldID)].Value = Constants.newWorkFlowApprovedStateID;
        }

        /// <summary>
        /// Method to add version to the item if the item is in Approved state or the workflow state is blank
        /// </summary>
        /// <param name="item">Item for which version is to be added</param>
        /// <returns>The item after adding the version</returns>
        public static Item AddVersionToItem(Item item)
        {
            string workflowstate = item.Fields[Sitecore.Data.ID.Parse(Constants.workflowStateFieldID)].Value;
            if (string.IsNullOrWhiteSpace(workflowstate) || workflowstate.Equals(Constants.newWorkFlowApprovedStateID, StringComparison.CurrentCultureIgnoreCase))
            {
                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    Item Latest = item.Versions.AddVersion();
                }
            }
            return item;
        }
    }
}