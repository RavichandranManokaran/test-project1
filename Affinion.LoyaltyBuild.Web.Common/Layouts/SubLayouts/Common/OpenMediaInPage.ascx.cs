﻿
namespace Affinion.LoyaltyBuild.Web.Common.Layouts.SubLayouts.Common
{
    using System;
    using System.Net;
    using Sitecore.Data.Items;
    using Sitecore.Resources.Media;

    public partial class OpenMediaInPage : System.Web.UI.UserControl
    {
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="e">Event Arguments</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.OpenMedia();
        }

        /// <summary>
        /// Method to Open Media Item in the current page.
        /// </summary>
        private void OpenMedia()
        {
            string fileid = Request.QueryString["fileId"];
            if (string.IsNullOrWhiteSpace(fileid))
            {
                ErrorMessage.Text = "File not found";
                ErrorMessage.Visible = true;
                return;
            }

            // Get the Sitecore Item
            Sitecore.Data.ID fileItemId = new Sitecore.Data.ID();
            Sitecore.Data.ID.TryParse(fileid, out fileItemId);
            Item fileItem = (!fileItemId.IsNull) ? Sitecore.Context.Database.GetItem(fileItemId) : null;
            if (fileItem == null)
            {
                ErrorMessage.Text = "File not found";
                ErrorMessage.Visible = true;
                return;
            }
            else
            {
                // Get the Media Item through Sitecore Item
                Media mediaItem = MediaManager.GetMedia(fileItem);
                string mediaName = fileItem.Name + "." + mediaItem.Extension;

                try
                {
                    Response.Clear();
                    Response.ContentType = mediaItem.MimeType;
                    Response.AppendHeader("Content-Disposition", string.Format("inline;filename=\"{0}\"", mediaName));
                    Response.StatusCode = (int)HttpStatusCode.OK;
                    Response.BufferOutput = true;
                    // Copy the media stream to the response output stream
                    mediaItem.GetStream().CopyTo(Response.OutputStream);
                    // As momma always said: Always remember to flush
                    Response.Flush();
                    Response.End();
                }
                catch
                {
                    ErrorMessage.Text = string.Format("Unable to Open file: {0}", mediaName);
                    ErrorMessage.Visible = true;
                }
            }
        }
    }
}