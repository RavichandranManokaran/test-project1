﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           TripAdvisorService.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.ExternalServices.Services.TripAdvisorService
/// Description:           Handle the custom affinion cache

using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.Common.Caching;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using System;
using System.IO;
using System.Net;
using System.Web.Helpers;

namespace Affinion.LoyaltyBuild.ExternalServices.Services
{
    /// <summary>
    /// Trip advisor service class
    /// </summary>
    public static class TripAdvisorService
    {

        #region Private Fields

        //private static string tripAdvisorKey = "48ddaf1ebd4c458bb163a16af2c25379";

        #endregion

        /// <summary>
        /// Get hotel details
        /// </summary>
        /// <param name="tripAdvisorId">TripAdvisor id</param>
        /// <returns>Hotel details</returns>
        public static dynamic GetHotelDetails(string tripAdvisorId)
        {
            try
            {
                Uri requestUrl = CreateHotelDetailRequestUrl(tripAdvisorId);

                HttpWebRequest request = WebRequest.Create(requestUrl) as HttpWebRequest;
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        throw new Exception(String.Format("Server error (HTTP {0}: {1}).", response.StatusCode, response.StatusDescription));
                    }

                    //using (Stream stream = response.GetResponseStream())
                    //{
                    using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
                    {
                        var json = streamReader.ReadToEnd();

                        ///Decode the json string
                        dynamic jsonResponse = Json.Decode(json);

                        if (jsonResponse == null)
                        {
                            Diagnostics.Trace(DiagnosticsCategory.Common, "jsonResponse or jsonResponse.data is null ");
                            return null;
                        }

                        return jsonResponse;

                    }
                    //}

                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                throw;
            }
        }

        /// <summary>
        /// Return object contains trip-advisor ids for matched hotels
        /// </summary>
        /// <param name="latitudeAndLongitude">Comma separated latitude and longitude </param>
        /// <param name="hotelName">Name of the hotel</param>
        /// <returns>Mapped hotels</returns>
        public static dynamic GetMappedHotels(string latitudeAndLongitude, string hotelName)
        {
            try
            {
                ///URL of the request
                Uri requestUrl = CreateHotelMapperRequestUrl(latitudeAndLongitude, hotelName);

                HttpWebRequest request = WebRequest.Create(requestUrl.AbsoluteUri) as HttpWebRequest;

                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        throw new Exception(String.Format("Server error (HTTP {0}: {1}).", response.StatusCode, response.StatusDescription));
                    }

                    //using (Stream stream = response.GetResponseStream())
                    //{
                    using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
                    {
                        var json = streamReader.ReadToEnd();

                        ///Decode the json string
                        dynamic jsonResponse = Json.Decode(json);

                        if (jsonResponse == null || jsonResponse.data == null)
                        {
                            Diagnostics.Trace(DiagnosticsCategory.Common, "jsonResponse or jsonResponse.data is null ");
                            return null;
                        }

                        ///Check whether only one record returned
                        if (jsonResponse.data.Length == 1)
                        {
                            return jsonResponse.data[0];

                        }
                        ///Notify when more than one hotel found
                        else if (jsonResponse.data.Length > 1)
                        {
                            //TODO: Implement notification
                            Diagnostics.Trace(DiagnosticsCategory.Common, "Multiple locations matched for " + requestUrl);
                            return null;
                        }
                    }
                    //}

                    Diagnostics.Trace(DiagnosticsCategory.Common, "No locations matched for " + requestUrl);
                    return null;

                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                throw;
            }
        }

        /// <summary>
        /// Get hotel mapper request url
        /// </summary>
        /// <param name="latitudeAndLongitude"></param>
        /// <param name="hotelName"></param>
        /// <returns></returns>
        private static Uri CreateHotelMapperRequestUrl(string latitudeAndLongitude, string hotelName)
        {
            string url = LanguageReader.GetText("HotelMapperUrl", "http://api.tripadvisor.com/api/partner/2.0/location_mapper/{0}?key={1}-mapper&category=hotels&q={2}");

            Uri requestUrl = new Uri(string.Format("http://api.tripadvisor.com/api/partner/2.0/location_mapper/{0}?key={1}-mapper&category=hotels&q={2}", latitudeAndLongitude, GetTripAdvisorKey(), hotelName));

            return requestUrl;
        }

        /// <summary>
        /// Get hotel detail request url
        /// </summary>
        /// <param name="locationId"></param>
        /// <returns></returns>
        private static Uri CreateHotelDetailRequestUrl(string locationId)
        {

            string url = LanguageReader.GetText("HotelDetailsUrl", "http://api.tripadvisor.com/api/partner/2.0/location/{0}?key={1}");

            Uri requestUrl = new Uri(string.Format(url, locationId, GetTripAdvisorKey()));
            return requestUrl;
        }

        /// <summary>
        /// Get TripAdvisor key
        /// </summary>
        /// <returns>Key</returns>
        private static string GetTripAdvisorKey()
        {
            string key = string.Empty;

            ///Read key from cache
            key = CacheManager.GetCache(Constants.TripAdvisorKeyCacheKey);

            if (!string.IsNullOrWhiteSpace(key))
            {
                return key;
            }
            else
            {
                ///Read key from config file
                key = Sitecore.Configuration.Settings.GetSetting(Constants.TripAdvisorKeyConfigKey);

                ///Set cache value
                CacheManager.SetCache(Constants.TripAdvisorKeyCacheKey, key);

                return key;
            }

        }

    }
}
