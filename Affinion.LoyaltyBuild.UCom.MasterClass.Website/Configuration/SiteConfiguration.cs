﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;
using Sitecore.Data;
using Sitecore.Globalization;
using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Sitecore.Data.Fields;
using Sitecore.Diagnostics;
using System.Globalization;
using Affinion.LoyaltyBuild.UCom.MasterClass.Website.CustomSiteCoreDataType;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.UCom.MasterClass.Model;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.Configuration
{
    /// <summary>
    /// A collection of methods to centralize common tasks throughout the site.  This allows them to be managed 
    /// in one place and be easily reused.  Many of these methods look to Sitecore items for values.
    /// </summary>
    public static class SiteConfiguration
    {
        public static void LogError(string message, object owner)
        {
            try
            {
                Sitecore.Diagnostics.Log.Error(message, owner);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Quick access to the vat range.        
        /// </summary>       
        /// <returns>The vat range.</returns>
        public static List<PromotionUser> GetPromotionUser()
        {
            List<PromotionUser> promotionusers = new List<PromotionUser>();
            try
            {
                Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
                Item[] salesTeam = workingdb.SelectItems("fast:/sitecore/content/#admin-portal#/global/#_supporting-content#/#sales-persons#/*[@@templatename='Sales Person']");
                if (salesTeam != null && salesTeam.Length > 0)
                {

                    foreach (var item in salesTeam)
                    {
                        if (item.Fields["IsActive"] != null && item.Fields["IsActive"].Value == "1")
                        {
                            PromotionUser promotionuser = new PromotionUser();
                            if (item.Fields["Email"] != null)
                            {
                                promotionuser.EmailAddress = item.Fields["Email"].Value;
                            }
                            if (item.Fields["Name"] != null)
                            {
                                promotionuser.Name = item.Fields["Name"].Value;
                            }
                            if (item.Fields["SitecoreUser"] != null)
                            {
                                promotionuser.UserName = item.Fields["SitecoreUser"].Value.Replace("sitecore\\", "");
                            }
                            promotionusers.Add(promotionuser);
                        }
                    }
                }
            }
            catch (Exception er)
            { }
            return promotionusers;
        }

        /// <summary>
        /// Quick access to the vat range.        
        /// </summary>       
        /// <returns>The vat range.</returns>
        public static List<int> GetVatRangeItem()
        {
            Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
            Item vatrangetNode = workingdb.GetItem("/sitecore/content/admin-portal/global/_supporting-content/vat/subratevat");
            int startFrom = Convert.ToInt32(vatrangetNode.Fields["Start"].Value);
            int endat = Convert.ToInt32(vatrangetNode.Fields["End"].Value);
            List<int> lstvatrange = new List<int>();

            lstvatrange.Add(0);

            for (int i = startFrom; i <= endat; i++)
            {
                lstvatrange.Add(i);
            }

            return lstvatrange;
        }

        /// <summary>
        /// Get Site Core Data Basesd on Type.        
        /// </summary>       
        /// <returns>The vat range.</returns>
        public static List<string> GetSiteCoreItem(string type)
        {
            List<string> dicClientlist = new List<string>();
            switch (type)
            {
                case CustomDataTypeConstant.Client:
                case CustomDataTypeConstant.SingleClient:
                    dicClientlist = SiteConfiguration.GetSiteCoreClient();
                    break;

                case CustomDataTypeConstant.Country:
                    dicClientlist = SiteConfiguration.GetSiteCoreCountry();
                    break;

                case CustomDataTypeConstant.Experience:
                    dicClientlist = SiteConfiguration.GetSiteCoreHotelExperience();
                    break;

                case CustomDataTypeConstant.Facility:
                    dicClientlist = SiteConfiguration.GetSiteCoreHotelFacility();
                    break;
                case CustomDataTypeConstant.HotelRanking:
                    dicClientlist = SiteConfiguration.GetSiteCoreHotelRanking();
                    break;

                case CustomDataTypeConstant.Recommended:
                    dicClientlist = SiteConfiguration.GetSiteCoreLocalRanking();
                    break;

                case CustomDataTypeConstant.HotelTheme:
                    dicClientlist = SiteConfiguration.GetSiteCoreHotelTheme();
                    break;

                case CustomDataTypeConstant.Location:
                    dicClientlist = SiteConfiguration.GetSiteCoreLocation();
                    break;

                case CustomDataTypeConstant.OccupancyTypes:
                    dicClientlist = SiteConfiguration.GetSiteCoreOccupancyTypes();
                    break;

                case CustomDataTypeConstant.Region:
                    dicClientlist = SiteConfiguration.GetSiteCoreRegion();
                    break;

                case CustomDataTypeConstant.SupplierType:
                    dicClientlist = SiteConfiguration.GetSiteCoreSupplierType();
                    break;

                case CustomDataTypeConstant.Groups:
                    dicClientlist = SiteConfiguration.GetSiteCoreGroups();
                    break;

                case CustomDataTypeConstant.SuperGroups:
                    dicClientlist = SiteConfiguration.GetSiteCoreSuperGroups();
                    break;

                case CustomDataTypeConstant.OfferType:
                    dicClientlist = SiteConfiguration.GetSiteCoreOfferType();
                    break;

                case CustomDataTypeConstant.AddOn:
                case CustomDataTypeConstant.AddOnMultiSelect:
                    dicClientlist = SiteConfiguration.GetSiteCoreAddOn();
                    break;

                case CustomDataTypeConstant.Hotel:
                    dicClientlist = SiteConfiguration.GetSiteCoreHotel();
                    break;

                case CustomDataTypeConstant.PaymentMethod:
                    dicClientlist = SiteConfiguration.GetSitecorePaymentMethod();
                    break;

                case CustomDataTypeConstant.PackageType:
                    dicClientlist = SiteConfiguration.GetSiteCorePackage();
                    break;

                case CustomDataTypeConstant.Commission:
                    dicClientlist = SiteConfiguration.GetSiteCoreCommission();
                    break;

                case CustomDataTypeConstant.PortalTypeMulti:
                case CustomDataTypeConstant.PortalType:
                    dicClientlist = SiteConfiguration.GetSiteCorePortalType();
                    break;

                case CustomDataTypeConstant.Currency:
                    dicClientlist = SiteConfiguration.GetSiteCoreCurrency();
                    break;
            }
            return dicClientlist;
        }

        private static List<string> GetSiteCoreCurrency()
        {
            Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
            List<string> dicHotel = new List<string>();
            try
            {
                Item[] hotels = workingdb.SelectItems(string.Format("fast:{0}", Sitecore.Configuration.Settings.GetSetting("SiteCoreCurrency")));
                foreach (Item reason in hotels)
                {
                    dicHotel.Add(string.Format("{0} ({1})", reason.Fields["Description"].Value, reason.Fields["CurrencyCode"].Value));
                }
                dicHotel = dicHotel.OrderBy(x => x).ToList();
            }
            catch (Exception er)
            {
                SiteConfiguration.LogError(er.Message, new object());
            }
            return dicHotel;
        }

        // <summary>
        /// Get Site Core Data Basesd on Type.        
        /// </summary>       
        /// <returns>The vat range.</returns>
        public static List<KeyValuePair<string, string>> GetSiteCoreItemWithId(string type)
        {
            List<KeyValuePair<string, string>> dicClientlist = new List<KeyValuePair<string, string>>();
            switch (type)
            {
                case CustomDataTypeConstant.ClientMulti:
                    dicClientlist = SiteConfiguration.GetSiteCoreClientWithId();
                    break;
            }
            return dicClientlist;
        }

        private static List<KeyValuePair<string, string>> GetSiteCoreClientWithId()
        {

            Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
            List<KeyValuePair<string, string>> dicClient = new List<KeyValuePair<string, string>>();
            try
            {
                Item[] hotels = workingdb.SelectItems(string.Format("fast:{0}", Sitecore.Configuration.Settings.GetSetting("SiteCoreClient")));
                foreach (Item reason in hotels)
                {
                    dicClient.Add(new KeyValuePair<string, string>(reason.ID.Guid.ToString("D"), reason.Fields["Name"].Value));
                }
                dicClient = dicClient.OrderBy(x => x.Key).ToList();
            }
            catch (Exception er)
            {
                SiteConfiguration.LogError(er.Message, new object());
            }
            return dicClient;
        }


        private static List<string> GetSiteCorePackage()
        {

            Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
            List<string> dicHotel = new List<string>();
            try
            {
                Item[] hotels = workingdb.SelectItems(string.Format("fast:{0}", Sitecore.Configuration.Settings.GetSetting("SiteCorePackageType")));
                foreach (Item reason in hotels)
                {
                    dicHotel.Add(reason.Fields["Title"].Value);
                }
                dicHotel = dicHotel.OrderBy(x => x).ToList();
            }
            catch (Exception er)
            {
                SiteConfiguration.LogError(er.Message, new object());
            }
            return dicHotel;
        }

        private static List<string> GetSiteCoreHotel()
        {

            Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
            List<string> dicHotel = new List<string>();
            try
            {
                Item[] hotels = workingdb.SelectItems(string.Format("fast:{0}", Sitecore.Configuration.Settings.GetSetting("SiteCoreHotel")));
                foreach (Item reason in hotels)
                {
                    dicHotel.Add(reason.Fields["Name"].Value);
                }
                dicHotel = dicHotel.OrderBy(x => x).ToList();
            }
            catch (Exception er)
            {
                SiteConfiguration.LogError(er.Message, new object());
            }
            return dicHotel;
        }

        private static List<string> GetSitecorePaymentMethod()
        {


            Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
            List<string> paymentRule = new List<string>();
            try
            {
                Item[] paymentrules = workingdb.SelectItems(Constants.PaymentMethodPath);
                foreach (Item rule in paymentrules)
                {
                    paymentRule.Add(rule.Fields["Title"].Value);
                }
                paymentRule = paymentRule.OrderBy(x => x).ToList();
            }
            catch (Exception er)
            {
                SiteConfiguration.LogError(er.Message, new object());
            }
            return paymentRule;

        }


        private static List<string> GetSiteCoreAddOn()
        {
            Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
            List<string> dicAddOn = new List<string>();
            try
            {
                Item[] siteCoreAddOn = workingdb.SelectItems(string.Format("fast:{0}", Sitecore.Configuration.Settings.GetSetting("SiteCoreAddOn")));
                // Item[] siteCoreAddOn = workingdb.SelectItems("fast:/sitecore/content/#admin-portal#/#supplier-setup#/global/#_supporting-content#/#supplier-types#//*[@@templatename='SupplierType']");
                foreach (Item reason in siteCoreAddOn)
                {
                    dicAddOn.Add(reason.Fields["Name"].Value);
                }
            }
            catch (Exception er)
            {
                SiteConfiguration.LogError(er.Message, new object());
            }
            return dicAddOn;
        }

        private static List<string> GetSiteCorePortalType()
        {
            Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
            List<string> dicAddOn = new List<string>();
            try
            {
                //  Item[] siteCoreAddOn = workingdb.SelectItems(string.Format("fast:{0}", Sitecore.Configuration.Settings.GetSetting("SiteCorePortalType")));
                // Item[] siteCoreAddOn = workingdb.SelectItems("fast:/sitecore/content/#admin-portal#/#supplier-setup#/global/#_supporting-content#/#supplier-types#//*[@@templatename='SupplierType']");
                dicAddOn.Add("Online");
                dicAddOn.Add("Offline");
                //foreach (Item reason in siteCoreAddOn)
                //{
                //    dicAddOn.Add(reason.Fields["Name"].Value);
                //}
            }
            catch (Exception er)
            {
                SiteConfiguration.LogError(er.Message, new object());
            }
            return dicAddOn;
        }

        private static List<string> GetSiteCoreOfferType()
        {
            //TODO : Need to update path for Offer Type
            Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
            List<string> dicOfferType = new List<string>();
            try
            {
                Item[] offerType = workingdb.SelectItems(string.Format("fast:{0}", Sitecore.Configuration.Settings.GetSetting("SiteCoreOfferType")));
                /// Item[] offerType = workingdb.SelectItems("fast:/sitecore/content/#admin-portal#/#supplier-setup#/global/#_supporting-content#/#supplier-types#/*[@@templatename='SupplierType']");
                foreach (Item reason in offerType)
                {
                    dicOfferType.Add(reason.Fields["Title"].Value);
                }
            }
            catch (Exception er)
            {
                SiteConfiguration.LogError(er.Message, new object());
            }
            return dicOfferType;
        }

        private static List<string> GetSiteCoreSuperGroups()
        {
            Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
            List<string> dicSuperGroup = new List<string>();
            try
            {
                //SupplierSuperGroup
                Item[] superGroups = workingdb.SelectItems(string.Format("fast:{0}", Sitecore.Configuration.Settings.GetSetting("SiteCoreSupplierSuperGroup")));
                // Item[] superGroups = workingdb.SelectItems("fast:/sitecore/content/#admin-portal#/#supplier-setup#/global/#_supporting-content#/#super-groups#/*[@@templatename='SupplierSuperGroup']");
                foreach (Item reason in superGroups)
                {
                    dicSuperGroup.Add(reason.Fields["Name"].Value);
                }
            }
            catch (Exception er)
            {
                SiteConfiguration.LogError(er.Message, new object());
            }
            return dicSuperGroup;
        }

        private static List<string> GetSiteCoreGroups()
        {
            Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
            List<string> dicsupplierGroups = new List<string>();
            try
            {
                Item[] supplierGroups = workingdb.SelectItems(string.Format("fast:{0}", Sitecore.Configuration.Settings.GetSetting("SiteCoreSupplierGroup")));
                //Item[] locations = workingdb.SelectItems("fast:");
                foreach (Item reason in supplierGroups)
                {
                    try
                    {
                        dicsupplierGroups.Add(reason.Fields["Name"].Value);
                    }
                    catch (Exception er)
                    {
                        SiteConfiguration.LogError(er.Message, new object());
                    }
                }
            }
            catch (Exception er)
            {
                SiteConfiguration.LogError(er.Message, new object());
            }
            return dicsupplierGroups;
        }

        private static List<string> GetSiteCoreSupplierType()
        {
            Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
            List<string> dicSupplierType = new List<string>();
            try
            {

                //SiteCoreSupplierType
                Item[] supplierType = workingdb.SelectItems(string.Format("fast:{0}", Sitecore.Configuration.Settings.GetSetting("SiteCoreSupplierType")));
                //  Item[] supplierType = workingdb.SelectItems("fast:/sitecore/content/#admin-portal#/#supplier-setup#/global/#_supporting-content#/#supplier-types#/*[@@templatename='SupplierType']");
                foreach (Item reason in supplierType)
                {
                    dicSupplierType.Add(reason.Fields["Name"].Value);
                }
            }
            catch (Exception er)
            {
                SiteConfiguration.LogError(er.Message, new object());
            }
            return dicSupplierType;
        }

        private static List<string> GetSiteCoreRegion()
        {
            Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
            List<string> dicRegion = new List<string>();
            try
            {
                Item[] regions = workingdb.SelectItems(string.Format("fast:{0}", Sitecore.Configuration.Settings.GetSetting("SiteCoreRegion")));
                //Item[] regions = workingdb.SelectItems("fast://sitecore/content/#admin-portal#/global/#_supporting-content#/regions/*[@@templatename='Region']");
                foreach (Item reason in regions)
                {
                    dicRegion.Add(reason.Fields["Name"].Value);
                }
            }
            catch (Exception er)
            {
                SiteConfiguration.LogError(er.Message, new object());
            }
            return dicRegion;
        }

        private static List<string> GetSiteCoreOccupancyTypes()
        {
            Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
            List<string> dicRoomType = new List<string>();
            try
            {
                Item[] roomType = workingdb.SelectItems(string.Format("fast:{0}", Sitecore.Configuration.Settings.GetSetting("SiteCoreOccupancyType")));
                //  Item[] roomType = workingdb.SelectItems("fast:/sitecore/content/#admin-portal#/#supplier-setup#/global/#_supporting-content#/#occupancy-types#/*[@@templatename='OccupancyType']");
                foreach (Item reason in roomType)
                {
                    dicRoomType.Add(reason.Fields["Name"].Value);
                }
            }
            catch (Exception er)
            {
                SiteConfiguration.LogError(er.Message, new object());
            }
            return dicRoomType;
        }

        private static List<string> GetSiteCoreClient()
        {
            Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
            List<string> dicClient = new List<string>();
            try
            {
                Item[] items = workingdb.SelectItems(string.Format("fast:{0}", Sitecore.Configuration.Settings.GetSetting("SiteCoreClient")));
                //  Item[] items = workingdb.SelectItems("fast:/sitecore/content/#admin-portal#/#client-setup#/*[@@templatename='ClientDetails']");
                foreach (Item reason in items)
                {
                    dicClient.Add(reason.Fields["Name"].Value);
                }
            }
            catch (Exception er)
            {
                SiteConfiguration.LogError(er.Message, new object());
            }
            return dicClient;
        }

        private static List<string> GetSiteCoreHotelTheme()
        {
            Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
            List<string> dicTheme = new List<string>();
            try
            {
                Item[] themes = workingdb.SelectItems(string.Format("fast:{0}", Sitecore.Configuration.Settings.GetSetting("SiteCoreTheme")));
                //  Item[] themes = workingdb.SelectItems("fast:/sitecore/content/#admin-portal#/#supplier-setup#/global/#_supporting-content#/themes/*[@@templatename='Theme']");
                foreach (Item reason in themes)
                {
                    dicTheme.Add(reason.Fields["Name"].Value);
                }
            }
            catch (Exception er)
            {
                SiteConfiguration.LogError(er.Message, new object());
            }
            return dicTheme;
        }

        private static List<string> GetSiteCoreLocalRanking()
        {
            Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
            List<string> dicRanking = new List<string>();
            try
            {
                Item[] hotelRanking = workingdb.SelectItems(string.Format("fast:{0}", Sitecore.Configuration.Settings.GetSetting("SiteCoreLocalRanking")));
                //Item[] hotelRanking = workingdb.SelectItems("fast:/sitecore/content/#admin-portal#/#supplier-setup#/global/#_supporting-content#/#star-rankings#/*[@@templatename='Ranking']");
                foreach (Item reason in hotelRanking)
                {
                    dicRanking.Add(reason.Fields["Name"].Value);
                }
            }
            catch (Exception er)
            {
                SiteConfiguration.LogError(er.Message, new object());
            }
            return dicRanking;
        }

        private static List<string> GetSiteCoreHotelRanking()
        {
            Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
            List<string> dicRanking = new List<string>();
            try
            {
                Item[] hotelRanking = workingdb.SelectItems(string.Format("fast:{0}", Sitecore.Configuration.Settings.GetSetting("SiteCoreRanking")));
                //Item[] hotelRanking = workingdb.SelectItems("fast:/sitecore/content/#admin-portal#/#supplier-setup#/global/#_supporting-content#/#star-rankings#/*[@@templatename='Ranking']");
                foreach (Item reason in hotelRanking)
                {
                    dicRanking.Add(reason.Fields["Name"].Value);
                }
            }
            catch (Exception er)
            {
                SiteConfiguration.LogError(er.Message, new object());
            }
            return dicRanking;
        }

        private static List<string> GetSiteCoreHotelExperience()
        {
            Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
            List<string> dicFacility = new List<string>();
            try
            {
                Item[] facility = workingdb.SelectItems(string.Format("fast:{0}", Sitecore.Configuration.Settings.GetSetting("SiteCoreExperience")));
                //Item[] facility = workingdb.SelectItems("fast:/sitecore/content/#admin-portal#/#supplier-setup#/global/#_supporting-content#/facilities/*[@@templatename='Facility']");
                foreach (Item reason in facility)
                {
                    dicFacility.Add(reason.Fields["Name"].Value);
                }
            }
            catch (Exception er)
            {
                SiteConfiguration.LogError(er.Message, new object());
            }
            return dicFacility;
        }

        private static List<string> GetSiteCoreHotelFacility()
        {
            Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
            List<string> dicFacility = new List<string>();
            try
            {
                Item[] facility = workingdb.SelectItems(string.Format("fast:{0}", Sitecore.Configuration.Settings.GetSetting("SiteCoreFacility")));
                //Item[] facility = workingdb.SelectItems("fast:/sitecore/content/#admin-portal#/#supplier-setup#/global/#_supporting-content#/facilities/*[@@templatename='Facility']");
                foreach (Item reason in facility)
                {
                    dicFacility.Add(reason.Fields["Name"].Value);
                }
            }
            catch (Exception er)
            {
                SiteConfiguration.LogError(er.Message, new object());
            }
            return dicFacility;
        }

        private static List<string> GetSiteCoreCountry()
        {
            Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
            List<string> dicCountry = new List<string>();
            try
            {
                Item[] country = workingdb.SelectItems(string.Format("fast:{0}", Sitecore.Configuration.Settings.GetSetting("SiteCoreCountry")));
                //  Item[] country = workingdb.SelectItems("fast:/sitecore/content/#admin-portal#/global/#_supporting-content#/countries/*[@@templatename='Country']");
                foreach (Item reason in country)
                {
                    dicCountry.Add(reason.Fields["CountryName"].Value);
                }
            }
            catch (Exception er)
            {
                SiteConfiguration.LogError(er.Message, new object());
            }
            return dicCountry;
        }

        /// <summary>
        /// Quick access to the location       
        /// </summary>       
        /// <returns>The vat range.</returns>
        /// 
        public static List<string> GetSiteCoreLocation()
        {
            Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
            List<string> dicLocation = new List<string>();
            try
            {
                Item[] locations = workingdb.SelectItems(string.Format("fast:{0}", Sitecore.Configuration.Settings.GetSetting("SiteCoreLocation")));
                // Item[] locations = workingdb.SelectItems("fast:/sitecore/content/#admin-portal#/global/#_supporting-content#/locations/*[@@templatename='Location']");
                foreach (Item reason in locations)
                {
                    dicLocation.Add(reason.Fields["Name"].Value);
                }
                // GetSiteCoreCommission();
            }
            catch (Exception er)
            {
                SiteConfiguration.LogError(er.Message, new object());
            }
            return dicLocation;
        }

        public static List<string> GetSiteCoreCommission()
        {
            Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
            List<string> dicCommission = new List<string>();

            try
            {
                Item[] commission = workingdb.SelectItems(string.Format("fast:{0}", Sitecore.Configuration.Settings.GetSetting("SiteCoreCommission")));
                //Item[] commission = workingdb.SelectItems("fast:/sitecore/content/#admin-portal#/global/#_supporting-content#/commission/*[@@templatename='Comission']");

                foreach (Item Comm in commission)
                {
                    string Name = Comm.Name + "|" + Comm.ID;
                    dicCommission.Add(Name);
                }
            }
            catch (Exception er)
            {
                SiteConfiguration.LogError(er.Message, new object());
            }
            return dicCommission;
        }


        /// <summary>
        /// 1. For Opted in
        /// 2. For Opted Out
        /// 3. 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string GetEmailTemplateHtml(int type)
        {

            try
            {
                string tempName = string.Empty;
                if (type == 1)
                {
                    tempName = "opted-in-campaign";
                }
                else if (type == 2)
                {
                    tempName = "opted-out-campaign";
                }
                else if (type == 3)
                {
                    tempName = "supplierinvite";
                }
                else if (type == 4)
                {
                    tempName = "new-promotion-alert";
                }
                else if (type == 5)
                {
                    tempName = "approve-promotion-alert";
                }
                else if (type == 6)
                {
                    tempName = "reject-promotion-alert";
                }
                Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
                Item supplierEmailItem = workingdb.GetItem(string.Format("/sitecore/content/globaldata/email-settings/{0}", tempName));
                if (supplierEmailItem != null)
                {
                    if (supplierEmailItem.Fields["HtmlTemplate"] != null && !string.IsNullOrEmpty(supplierEmailItem.Fields["HtmlTemplate"].Value))
                    {
                        return supplierEmailItem.Fields["HtmlTemplate"].Value;
                    }
                }
                return string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Quick access to the Offer removal reasons.        
        /// </summary>       
        /// <returns>List of reason for offer remove.</returns>
        public static void GetSupplierDetail(List<string> ucomId, string campaignName)
        {

            try
            {
                List<string> lstResons = new List<string>();
                string template = GetEmailTemplateHtml(3);
                Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
                Item[] hotelData = workingdb.SelectItems("/sitecore/content/#admin-portal#/#supplier-setup#//*[@@templateid='{A7235C94-2EF5-4A3B-87DB-0D4DCF10B4DB}']");
                Item[] selecetdCatgory = hotelData.Where(x => x.Fields["SKU"] != null && ucomId.Any(y => y.Equals(x.Fields["SKU"].Value, StringComparison.CurrentCultureIgnoreCase))).ToArray();
                List<SupplierInfo> supplierDetail = new List<SupplierInfo>();
                foreach (Item supplierItem in selecetdCatgory)
                {
                    SupplierInfo supplierInfo = new SupplierInfo();
                    if (supplierItem != null)
                    {
                        try
                        {
                            supplierInfo.SupplierName = supplierItem.DisplayName;
                            if (supplierItem.Fields["AddressLine1"] != null)
                            {
                                supplierInfo.SupplierAddress = supplierItem.Fields["AddressLine1"].Value;
                            }
                            if (supplierItem.Fields["AddressLine2"] != null && !string.IsNullOrEmpty(supplierItem.Fields["AddressLine2"].Value))
                            {
                                supplierInfo.SupplierAddress += string.Format(", {0}", supplierItem.Fields["AddressLine2"].Value);
                            }
                            if (supplierItem.Fields["AddressLine3"] != null && !string.IsNullOrEmpty(supplierItem.Fields["AddressLine3"].Value))
                            {
                                supplierInfo.SupplierAddress += string.Format(", {0}", supplierItem.Fields["AddressLine3"].Value);
                            }
                            if (supplierItem.Fields["AddressLine4"] != null && !string.IsNullOrEmpty(supplierItem.Fields["AddressLine4"].Value))
                            {
                                supplierInfo.SupplierAddress += string.Format(", {0}", supplierItem.Fields["AddressLine4"].Value);
                            }
                            if (supplierItem.Fields["Country"] != null && ((LookupField)supplierItem.Fields["Country"]).TargetItem != null)
                            {
                                supplierInfo.Country = ((LookupField)supplierItem.Fields["Country"]).TargetItem.DisplayName;
                            }
                            if (supplierItem.Fields["SupplierType"] != null && ((LookupField)supplierItem.Fields["SupplierType"]).TargetItem != null)
                            {
                                supplierInfo.SupplierType = ((LookupField)supplierItem.Fields["SupplierType"]).TargetItem.DisplayName;
                            }
                            if (supplierItem.Fields["StarRanking"] != null && ((LookupField)supplierItem.Fields["StarRanking"]).TargetItem != null)
                            {
                                supplierInfo.StarRating = ((LookupField)supplierItem.Fields["StarRanking"]).TargetItem.DisplayName;
                            }
                            if (supplierItem.Fields["EmailID2"] != null)
                            {
                                supplierInfo.Email = (supplierItem.Fields["EmailID2"].Value);
                            }
                            //EmailID2
                            supplierDetail.Add(supplierInfo);
                        }
                        catch (Exception er)
                        {
                            SiteConfiguration.LogError(er.Message, new object());  // handle 
                        }
                    }
                }
                if (supplierDetail != null && supplierDetail.Any())
                {
                    Affinion.LoyaltyBuild.Communications.IEmailService emailService = new Affinion.LoyaltyBuild.Communications.Services.EmailService();
                    string emailtemplate = template;
                    foreach (var item in supplierDetail.Where(x => !string.IsNullOrEmpty(x.Email)))
                    {
                        try
                        {
                            string msg = emailtemplate.Replace("@@suppliername@@", item.SupplierName).Replace("@@campaignname@@", campaignName);
                            emailService.Send("info@loyaltybuild.com", item.Email, "Campaign Invitation", msg, true);
                        }
                        catch (Exception er)
                        {
                            SiteConfiguration.LogError(er.Message, new object());  // handle 
                        }
                    }
                }
            }
            catch (Exception er)
            {
                SiteConfiguration.LogError(er.Message, new object());  // handle 
            }
        }
    }
}