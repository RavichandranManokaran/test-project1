﻿using Affinion.LoyaltyBuild.BedBanks.Troika.Model.Coldfusion;

namespace Affinion.LoyaltyBuild.BedBanks.Troika.Model
{
    public class AvailabilityProvider
    {
        public int Available;
        public int AvailabilitySourceId;
        public int BaseCurrencyId;
        public string BasedOnArrivalDate;
        public string CheckedOnDate;
        public int ClientOfferId;
        public string ExtraDescription;
        public int HasFeaturedTheme; //bit??
        public ColdFusionBit IgnoreProvider; //bit??
        public string InfoBoxText;
        public int IsCore; //bit??
        public int IsHiddenGem; //bit??
        public string ItemDescriptor;
        public string ItemId;
        public string ItemName;
        public int ItemStarRanking;
        public string ItemTypeId;
        public ColdFusionVarChar Latitude;
        public int LocalRanking;
        public string LocationName;
        public int LoipId;
        public ColdFusionVarChar Longitude;
        public int MapLocationId;
        public string MapLocationName;
        public int MaxNumberOfAdults;
        public int MaxNumberOfChildren;
        public int MaxNumberTotal;
        public string NightsStay;
        public int NominalHotelRateAtClientOfferToProviderLevel; //Double??
        public int NominalHotelRateAtProviderToItemLevel; //Double?
        public int NumReviews;
        public int PercentageHotelRateAtClientOfferToProviderLevel; //Double??
        public int PercentageHotelRateAtProviderToItemLevel; //Double?
        public string PictureFileName;
        public int ProviderCountryId;
        public string ProviderEmail;
        public int LbProviderId;
        public string ProviderId;
        public int ProviderLanguageId;
        public string ProviderName;
        public string RatingImage;
        public int Savings;
        public int StarRanking;
        public int TimeLimitedOfferNoCancelation;
        public string Value;
        public string WebIntroduction;
        public string FeatureListing;
        public string IsDirect;
        public string RoomToken;
        public string MealBasisId;
        public string ErratumText;
    }
}
