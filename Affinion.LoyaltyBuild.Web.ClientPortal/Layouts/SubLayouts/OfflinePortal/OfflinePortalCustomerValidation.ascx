﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalCustomerValidation.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalCustomerValidation" %>
<%@ register tagprefix="sc" namespace="Sitecore.Web.UI.WebControls" assembly="Sitecore.Kernel" %>
 
<div class="body-container">
    <div class="container booking-detail-info">
        <div class="content">

            <div class="content-body">

                <div>
                    <div class="row hide">
                        <div class="col-sm-12 no-padding-m">
                            <div class="content_bg_inner_box alert-info">
                                <div class="col-sm-12 margin-row">
                                    <span>SuperValu Northern Ireland customers,</span> <a href="#">Click Here.</a><span> to book on the correct website. </span>
                                </div>
                                <div class="col-sm-12 margin-row">
                                    <span>IF YOU ARE HAVING ISSUES ENTERING YOUR FOB NUMBER, PLEASE CALL 0818 220088</span>
                                </div>
                                <div class="col-sm-12 margin-row">
                                    <span>To book your SuperValu Getaway Break, please enter your details below:</span>
                                </div>

                                <div class="col-sm-12 margin-row">
                                    <div class="col-sm-4">
                                        <label>
                                             <sc:Text ID="Text1" Field="Real Rewards Member Number" runat="server" />
                                            <input type="radio" name="optradio">
                                            Real Rewards Member Number</label>
                                    </div>
                                    <div class="col-sm-5">
                                        <input type="text" placeholder="Enter the last digit on your Real Rewards here" class="form-control">
                                    </div>
                                    <div class="col-sm-3"><span>Not Member ?  <a href="#">Click Here</a></span></div>
                                </div>
                                <div class="col-sm-12 margin-row">
                                    <div class="col-sm-4">
                                        <label>
                                             <sc:Text ID="Text2" Field="Collect Card Customer" runat="server" />
                                            <input type="radio" name="optradio">
                                            Collect Card Customer</label>
                                        <span>(Inside the front cover of your collector card)</span>
                                    </div>
                                    <div class="col-sm-5">
                                        <input type="text" placeholder="" class="form-control">
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>
                                <div class="col-sm-12 margin-row">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-5">
                                        <asp:Button ID="btnToPersonalDetail1" runat="server" CssClass="btn btn-default add-break pull-right validate" Text="Continue to Personal Detail" />
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row hide">
                        <div class="col-sm-12 no-padding-m">
                            <div class="content_bg_inner_box alert-info">
                                <div class="col-sm-12 margin-row">
                                    <h2>Kupp validation Offline process </h2>
                                    <sc:Text ID="Text4" Field="Kupp validation" runat="server" />
                                </div>
                                <div class="col-sm-12 margin-row">
                                    <span>IF YOU ARE HAVING ISSUES ENTERING YOUR FOB NUMBER, PLEASE CALL 0818 220088</span>
                                </div>


                                <div class="col-sm-12 margin-row">
                                    <div class="col-sm-4">
                                        <label>Validate Number <span class="accent58-f">*</span></label>
                                        <sc:Text ID="Text3" Field="validate number" runat="server" />
                                    </div>
                                    <div class="col-sm-5">
                                        <input type="text" placeholder="Validate Number" class="form-control">
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>

                                <div class="col-sm-12 margin-row">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-5">
                                        <button type="submit" class="btn btn-default add-break pull-right">Cancel</button>
                                        <button type="submit" class="btn btn-default add-break pull-right validate">Validate Number </button>
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a name="atgvalidation"></a>
                    <div class="row">
                        <div class="col-sm-12 no-padding-m">
                            <div class="content_bg_inner_box alert-info">
                                <div class="col-sm-6">
                                <h3>
                                    <sc:Text ID="EmailValidationTitleText" Field="EmailValidationTitle" runat="server" />
                                </h3>
                            </div>
                    <div id="FirstDescriptionDiv" class="col-sm-12 margin-row" runat="server">
                            <span>
                                <sc:Text ID="FirstDescription" Field="FirstDescription" runat="server" />
                            </span>
                        </div>
                        <div id="SecondDescriptionDiv" class="col-sm-12 margin-row" runat="server">
                            <span>
                                <sc:Text ID="SecondDescription" Field="SecondDescription" runat="server" />
                            </span>
                        </div>
                        <div id="ThirdDescriptionDiv" class="col-sm-12 margin-row" runat="server">
                            <span>
                                <sc:Text ID="ThirdDescription" Field="ThirdDescription" runat="server" />
                            </span>
                     </div>
                                <div class="col-sm-12 margin-row">
                                    <div class="col-sm-4">
                                        <label><sc:text id="email" runat="server" field="email" /> <span class="accent58-f">*</span></label>
                                        
                                    </div>
                                    <div class="col-sm-5">
                                       <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" ToolTip="Enter Email Address"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1"  placeholder="Enter Email Address" runat="server" ForeColor="Red"  ControlToValidate="txtEmail" ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$" Display="Dynamic" ValidationGroup="emailValidation"><sc:text id="txtvalidEmailText" runat="server" /></asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter Email Address" ForeColor="Red" ControlToValidate="txtEmail" Display="Static" ValidationGroup="emailValidation"><sc:text id="txtEmailRequiredText" runat="server" /></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>
                                <div class="col-sm-12 margin-row">
                                    <div class="col-sm-4">
                                        <label><sc:text id="confirmemail" runat="server" field="confirmemail" />   <span class="accent58-f">*</span></label>
                                        
                                    </div>
                                    <div class="col-sm-5">
                                        <asp:TextBox ID="txtConfirmEmail" runat="server" CssClass="form-control" ToolTip="Confirm Email Address"  ></asp:TextBox>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" placeholder="Confirm Email Address" ControlToCompare="txtEmail" ControlToValidate="txtConfirmEmail" ForeColor="Red" ValidationGroup="emailValidation"><sc:text id="txtEmailMismatchText" runat="server" /></asp:CompareValidator>
                                        <asp:RequiredFieldValidator ID="cnfrmEmailValidator" runat="server" Display="Static" ControlToValidate="txtConfirmEmail" ForeColor="Red" ValidationGroup="emailValidation"><sc:text id="txtConfirmEmailText" runat="server" /></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>
                          <div id="EmailListConsent1Div" class="col-sm-12 margin-row" runat="server">
                            <div class="col-sm-9">

                                <sc:Text ID="EmailList1ConsentLabel" Field="EmailList1Consent" runat="server" />
                            </div>
                            <div class="col-sm-2">

                                <asp:CheckBox ID="EmailList1ConsentCheckBox" runat="server" />
                            </div>
                        </div>
                        <div id="EmailListConsent2Div" class="col-sm-12 margin-row" runat="server">
                            <div class="col-sm-9">

                                <sc:Text ID="EmailList2ConsentLabel" Field="EmailList2Consent" runat="server" />
                            </div>
                            <div class="col-sm-2">
                                <asp:CheckBox ID="EmailList2ConsentCheckBox" runat="server" />
                            </div>
                        </div>
                       <div class="col-sm-12 margin-row">
                            <div class="col-sm-9">
                                <span>
                                    <sc:Text ID="InstructionText" Field="InstructionText" runat="server" />
                                </span>
                            </div>
                            <div class="col-sm-2">
                            </div>
                        </div>
                                <div class="col-sm-12 margin-row">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-5">
                                       <!-- <asp:Button ID="btnToPersonalDetail" runat="server" CssClass="btn btn-default add-break pull-right validate" Text="Continue to Personal Detail" OnClick="btnToPersonalDetail_Click" ValidationGroup="emailValidation" />-->
                                        <button type="button" cCssClass="btn btn-default add-break pull-right validate "  OnClick="btnToPersonalDetail_Click"  runat="server" validationgroup="emailValidation">
                                <sc:text id="btnpersonaldetails" runat="server" />
                            </button>
                                        
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row hide">
                        <div class="col-sm-12 no-padding-m">
                            <div class="content_bg_inner_box alert-info">
                                <div class="col-sm-12 margin-row">
                                    <h2>Premie validation</h2>
                                    <sc:Text ID="Text5" Field="Premie validation" runat="server" />
                                </div>
                                <div class="col-sm-12 margin-row">
                                    <span>IF YOU ARE HAVING ISSUES ENTERING YOUR FOB NUMBER, PLEASE CALL 0818 220088</span>
                                </div>


                                <div class="col-sm-12 margin-row">
                                    <div class="col-sm-4">
                                        <label>Validate Number <span class="accent58-f">*</span></label>
                                        <sc:Text ID="Text6" Field="Validate Number" runat="server" />
                                    </div>
                                    <div class="col-sm-5">
                                        <input type="text" placeholder="Validate Number" class="form-control">
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>
                                <div class="col-sm-12 margin-row">
                                    <div class="col-sm-4">
                                        <label>Date of Birth</label>
                                        <sc:Text ID="Text7" Field="Date of Birth" runat="server" />
                                    </div>
                                    <div class="col-sm-5">

                                        <div class="input-group">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default glyphicon glyphicon-calendar calander-icon height-icon" type="button"></button>
                                            </span>
                                            <input type="text" class="form-control" placeholder="Date of Birth" id="datepicker">
                                        </div>
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>
                                <div class="col-sm-12 margin-row">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-5">
                                        <button type="submit" class="btn btn-default add-break pull-right">Cancel</button>
                                        <button type="submit" class="btn btn-default add-break pull-right validate">Validate Number </button>
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="hide-div">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2 class="panel-title">Personal Details</h2>
                                sc:Text ID="Text7" Field="Personal Details" runat="server" />
                            </div>
                            <div class="row hide">
                                <div class="panel-body">
                                    <div class="col-sm-12">
                                        <div class="content_bg_inner_box alert-info">
                                            <div class="col-sm-6">
                                                <div class="row margin-row">
                                                    <div class="col-sm-5"><span class="bask-form-titl font-size-12">First Name:</span>
                                                        sc:Text ID="Text10" Field="First Name" runat="server" />
                                                    </div>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control font-size-12" placeholder="First Name">
                                                    </div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-5"><span class="bask-form-titl font-size-12">Last Name:</span>
                                                        <sc:Text ID="Text11" Field="Last Name" runat="server" />
                                                    </div>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control font-size-12" placeholder="Last Name">
                                                    </div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-5"><span class="bask-form-titl font-size-12">Address:</span>
                                                        <sc:Text ID="Text12" Field="Address" runat="server" />
                                                    </div>
                                                    <div class="col-sm-7">
                                                        <textarea class="form-control font-size-12" rows="5" id="comment"></textarea>
                                                    </div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-5"><span class="bask-form-titl font-size-12">City / Town:</span>
                                                        <sc:Text ID="Text13" Field="City Town" runat="server" />
                                                    </div>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control font-size-12" placeholder="City">
                                                    </div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-5"><span class="bask-form-titl font-size-12">County/Region:</span>
                                                        <sc:Text ID="Text14" Field="County Region" runat="server" />
                                                    </div>
                                                    <div class="col-sm-7">
                                                        <select class="form-control font-size-12">
                                                            <option>Select Location</option>
                                                            <option>2</option>
                                                            <option>3</option>
                                                            <option>4</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="row margin-row">
                                                    <div class="col-sm-5"><span class="bask-form-titl font-size-12">Country:</span>
                                                        <sc:Text ID="Text15" Field="Country" runat="server" />
                                                    </div>
                                                    <div class="col-sm-7">
                                                        <select class="form-control font-size-12">
                                                            <option>Select Location</option>
                                                            <option>2</option>
                                                            <option>3</option>
                                                            <option>4</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-5"><span class="bask-form-titl font-size-12">Mobile Phone:</span>
                                                        <sc:Text ID="Text16" Field="Mobile Phone" runat="server" />
                                                    </div>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control font-size-12" placeholder="Mobile Phone">
                                                    </div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-5"><span class="bask-form-titl font-size-12">Phone:</span>
                                                        <sc:Text ID="Text17" Field="Phone" runat="server" />
                                                    </div>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control font-size-12" placeholder="Phone">
                                                    </div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-5"><span class="bask-form-titl font-size-12">E-mail:</span></div>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control font-size-12" placeholder="E-mail">
                                                    </div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-5"><span class="bask-form-titl font-size-12">Reconfirm E-mail Address:</span></div>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control font-size-12" placeholder="Reconfirm E-mail Address">
                                                    </div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-5"><span class="bask-form-titl font-size-12">SuperValu Store:</span>
                                                        <sc:Text ID="Text18" Field="SuperValu Store" runat="server" />
                                                    </div>
                                                    <div class="col-sm-7">
                                                        <select class="form-control font-size-12">
                                                            <option>Please Select Your local store</option>
                                                            <option>2</option>
                                                            <option>3</option>
                                                            <option>4</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row margin-row">
                                                    <div class="col-sm-12 pay-chek">
                                                        <div class="checkbox font-size-12">
                                                            <label>
                                                                <input type="checkbox">
                                                                Please send me emails on SuperValu Special Offers and Sales.
                                                            </label>
                                                            <label>
                                                                <input type="checkbox">
                                                                I have read and accept the Terms & Conditions and Privacy Statement 
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-12">
                                                        <button class="btn btn-default add-break pull-right" type="submit">Continue to Secure Payment</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
