﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Abstract Box.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Single_Item.Abstract_Box" %>

<asp:Panel ID="pnlAbstract" runat="server">
 <div class="grey-box abstract-box">
   <h2><sc:FieldRenderer ID="frTitle" runat="server" FieldName="Title" /></h2>
    <p><sc:FieldRenderer ID="frIcon" runat="server" FieldName="Icon" Parameters="MaxWidth=32" /></p>
    <sc:FieldRenderer ID="frAbstract" runat="server" FieldName="Abstract" />     
    <asp:HyperLink ID="LinkTo" runat="server" class="btn btn-primary" />
  </div>
</asp:Panel>



 
