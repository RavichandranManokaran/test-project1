﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           OccupancyType.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common.Search
/// Description:           Occupancy type
/// </summary>

namespace Affinion.LoyaltyBuild.Common.Search
{
    #region Using directives
    using Affinion.Loyaltybuild.BusinessLogic.Helper;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Sitecore.Data.Items;
    using System;
    using UCommerce.Api;
    using UCommerce.EntitiesV2; 
    #endregion

    /// <summary>
    /// Data class for occupancy type
    /// </summary>
    public class OccupancyType
    {
        public Item Item { get; set; }

        public string Name { get; set; }

        /// <summary>
        /// Max adult count
        /// </summary>
        public int MaxAdults { get; set; }

        /// <summary>
        /// Max children count
        /// </summary>
        public int MaxChildren { get; set; }

        /// <summary>
        /// Max People count
        /// </summary>
        public int MaxPeople { get; set; }

        /// <summary>
        /// Max infant count
        /// </summary>
        public int MaxInfants { get; set; }

        /// <summary>
        /// Ranking
        /// </summary>
        public int Ranking { get; set; }

        /// <summary>
        /// Whether occupancy type is active
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Create an instance of occupancy type
        /// </summary>
        /// <param name="productSku">Sitecore item id of the occupancy type item</param>
        public OccupancyType(string productSku)
        {
            try
            {
                Product Product = string.IsNullOrEmpty(productSku) ? null : CatalogLibrary.GetProduct(productSku);

                this.Name = Product.Name;
                this.MaxAdults = UCommerceHelper.GetIntergerProductFieldValue(Product, "MaxNumberOfAdults", 1);
                this.MaxChildren = UCommerceHelper.GetIntergerProductFieldValue(Product, "MaxNumberOfChildren", 0);
                this.MaxPeople = UCommerceHelper.GetIntergerProductFieldValue(Product, "MaxNumberOfPeople", 1);
                this.MaxInfants = UCommerceHelper.GetIntergerProductFieldValue(Product, "MaxNumberOfInfants", 0);
                this.Ranking = UCommerceHelper.GetIntergerProductFieldValue(Product, "Ranking", 1);
                this.IsActive = true;
            }
            catch(Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, typeof(OccupancyType));
                throw;
            }
        }
    }
}
