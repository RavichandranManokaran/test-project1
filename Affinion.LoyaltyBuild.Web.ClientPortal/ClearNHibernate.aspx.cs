﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;

namespace Affinion.LoyaltyBuild.Web.ClientPortal
{
    public partial class ClearNHibernate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ObjectFactory.Instance.Resolve<ICacheProvider>().ClearCache();
        }
    }
}