﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.EntitiesV2;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using UCommerce.Presentation.Views.Marketing;
using UCommerce.Presentation.Web.Controls;
using UCommerce.Presentation.Web.Marketing;
using Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi;
using Affinion.LoyaltyBuild.UCom.Common.Extension;
using System.Text.RegularExpressions;
using System.Globalization;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.ucommerce.Marketing.Targets
{
    public partial class BookingThresoldUi : ViewEnabledControl<IEditCampaignItemView>, ITargetUi
    {
        /// <summary>
        /// Load method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

            BookingDateThresoldTarget dataSource = this.DataSource as BookingDateThresoldTarget;
            this.DeleteButton.OnClientClick = string.Format("return confirm('Delete');");
            if (dataSource.MinHours != null)
                this.txtMinHours.Text = dataSource.MinHours.ToString();
            if (dataSource.MaxHours != null)
                this.txtMaxHours.Text = dataSource.MaxHours.ToString();
            this.ReadOnlyMessage.Text = this.TargetToString(dataSource);

        }

        /// <summary>
        /// save ac
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveButton_Click(object sender, ImageClickEventArgs e)
        {
            if (Page.IsValid)
            {
                BookingDateThresoldTarget dataSource = this.DataSource as BookingDateThresoldTarget;

                int minHours = 0, maxHours = 0;
                if (int.TryParse(txtMinHours.Text, out minHours))
                    dataSource.MinHours = minHours;
                if (int.TryParse(txtMaxHours.Text, out maxHours))
                    dataSource.MaxHours = maxHours;

                dataSource.Save();
                this.ReadOnlyMessage.Text = this.TargetToString(dataSource);
                this.ToggleEditor();
            }
        }

        protected void ValidateHours(object sender, ServerValidateEventArgs e)
        {
            e.IsValid = false;
            int minHours = 0, maxHours = 0;

            int.TryParse(txtMinHours.Text, out minHours);
            int.TryParse(txtMaxHours.Text, out maxHours);

            if (minHours == 0 && maxHours == 0)
            {
                rfvValidateHours.ErrorMessage = "MinHours and MaxHours should not be Zero";
            }
            else if ((maxHours < minHours) && maxHours != 0)
            {
                rfvValidateHours.ErrorMessage = "MaxHours should be greater than MinHours";
            }
            else if (maxHours == minHours)
            {
                rfvValidateHours.ErrorMessage = "MinHours and MaxHours should not be same";
            }
            else if (minHours == 0)
            {
                rfvValidateHours.ErrorMessage = "MinHours should not be Zero";
            }
            else if ((maxHours > minHours) || (minHours > 0 && maxHours == 0))
            {
                e.IsValid = true;

            }
        }

        private void ToggleEditor()
        {
            this.NonEditModePlaceHolder.Visible = !this.NonEditModePlaceHolder.Visible;
            this.EditButton.Visible = !this.EditButton.Visible;
            this.EditModePlaceHolder.Visible = !this.EditModePlaceHolder.Visible;
            this.SaveButton.Visible = !this.SaveButton.Visible;
        }

        private string TargetToString(BookingDateThresoldTarget bookingDateThresoldTarget)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendFormat("Thresold Hours ");
            if (bookingDateThresoldTarget.MinHours.HasValue)
                stringBuilder.AppendFormat("Min hours {0} ", bookingDateThresoldTarget.MinHours.ToString());
            if (bookingDateThresoldTarget.MaxHours.HasValue)
                stringBuilder.AppendFormat("Max hours {0} ", bookingDateThresoldTarget.MaxHours.ToString());
            return stringBuilder.ToString();

        }

        protected void DeleteButton_Click(object sender, ImageClickEventArgs e)
        {
            BookingDateThresoldTarget dataSource = this.DataSource as BookingDateThresoldTarget;
            if (dataSource != null)
            {
                dataSource.Delete();
            }
            this.Page.Response.Redirect(this.Page.Request.Url.ToString(), true);
        }

        protected void EditButton_Click(object sender, ImageClickEventArgs e)
        {
            this.ToggleEditor();
        }


        public bool EnabledForApply
        {
            get;
            set;
        }

        public bool EnabledForDisplay
        {
            get;
            set;
        }

        public object DataSource
        {
            get;
            set;

        }

        public object New()
        {
            try
            {
                var BookingDateThresoldTarget = new BookingDateThresoldTarget();

                BookingDateThresoldTarget.CampaignItem = View.CampaignItem;
                BookingDateThresoldTarget.EnabledForApply = true;
                BookingDateThresoldTarget.Save();

                this.DataSource = BookingDateThresoldTarget;
                return BookingDateThresoldTarget;
            }
            catch (Exception)
            {

            }
            return null;
        }
    }
}