﻿CREATE TABLE [dbo].[uCommerce_EmailProfile] (
    [EmailProfileId] INT              IDENTITY (1, 1) NOT NULL,
    [Name]           NVARCHAR (128)   NOT NULL,
    [ModifiedOn]     DATETIME         NOT NULL,
    [ModifiedBy]     NVARCHAR (50)    NOT NULL,
    [CreatedOn]      DATETIME         NOT NULL,
    [CreatedBy]      NVARCHAR (50)    NOT NULL,
    [Deleted]        BIT              CONSTRAINT [uCommerce_DF_EmailProfile_Deleted] DEFAULT ((0)) NOT NULL,
    [Guid]           UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    CONSTRAINT [uCommerce_PK_EmailProfile] PRIMARY KEY CLUSTERED ([EmailProfileId] ASC)
);

