﻿using System;

namespace Affinion.LoyaltyBuild.BedBanks.General
{
    public static class CurrencyController
    {
        public static Tuple<decimal, decimal> GetRoundedValues(decimal pFullTotal, decimal pCommissionTotal)
        {
            //Get the mod of the full total
            decimal placeValue = pFullTotal % 1;

            //Now round up the place value (eventually we will bring currency into this to cater for different currencies
            decimal roundedPlaceValue = placeValue;
            if (placeValue <= 0.5M)
            {
                roundedPlaceValue = 0.5M;
            }
            else
            {
                roundedPlaceValue = 1.0M;
            }
            decimal fullTotalRounded = pFullTotal - placeValue + roundedPlaceValue;

            //Now work our Delta
            decimal roundedDelta = fullTotalRounded - pFullTotal;

            //Add the delta to our commission
            decimal commissionTotalIncDelta = pCommissionTotal + roundedDelta;

            //Return as a tuple
            return Tuple.Create(fullTotalRounded, commissionTotalIncDelta);
        }
    }
}
