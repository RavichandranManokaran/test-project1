﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Title and Body for Footer.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Single_Item.Title_and_Body_for_Footer" %>

 <h4><sc:FieldRenderer ID="frTitle" runat="server" FieldName="Title" /></h4>
 <sc:FieldRenderer ID="frBody" runat="server" FieldName="Body" />