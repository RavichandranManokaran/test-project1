﻿/****** Object:  StoredProcedure [dbo].[LB_GetDueDetail]    Script Date: 2/12/2016 11:40:56 AM ******/
DROP PROCEDURE [dbo].[LB_GetDueDetail]
GO

/****** Object:  StoredProcedure [dbo].[LB_GetDueDetail]    Script Date: 2/12/2016 11:40:56 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[LB_GetDueDetail]
(
	@DueId INT
) 
AS 
BEGIN
	SELECT [OrderLineDueId]
		,[OrderLineID]
		,[SubrateItemCode]
		,[DueTypeId]
		,[CurrencyTypeId]
		,[Amount]
		,[CommissionAmount]
		,[TransferAmount]
		,[ClientAmount]
		,[ProviderAmount]
		,[CollectionByProviderAmount]
		,[Note]
		,[OrderLinePaymentID]
		,[IsDeleted]
		,[CreatedBy]
		,[CreatedDate]
		,[UpdatedBy]
		,[UpdatedDate]
		,[CancellationAmount]
		,[PaymentMethodId]
	FROM 
		[dbo].[LB_OrderLineDue]
	WHERE 
		[OrderLineDueId] = @DueId
END


GO

