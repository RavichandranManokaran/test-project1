﻿CREATE TABLE [dbo].[uCommerce_Campaign] (
    [CampaignId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]       NVARCHAR (512) NULL,
    [StartsOn]   DATETIME       NOT NULL,
    [EndsOn]     DATETIME       NOT NULL,
    [Enabled]    BIT            NOT NULL,
    [Priority]   INT            NULL,
    [Deleted]    BIT            NOT NULL,
    [CreatedBy]  NVARCHAR (50)  NULL,
    [ModifiedBy] NVARCHAR (50)  NULL,
    [CreatedOn]  DATETIME       NOT NULL,
    [ModifiedOn] DATETIME       NOT NULL,
    CONSTRAINT [PK_uCommerce_Campaign] PRIMARY KEY CLUSTERED ([CampaignId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_uCommerce_Campaign]
    ON [dbo].[uCommerce_Campaign]([CampaignId] ASC);

