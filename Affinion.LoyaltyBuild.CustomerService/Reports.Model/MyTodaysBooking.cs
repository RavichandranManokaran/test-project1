﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model
{
   public  class MyTodaysBooking
    {
       /// <summary>
       /// TimeFrame
       /// </summary>
       public string TimeFrame { get; set; }
       /// <summary>
       /// Current User
       /// </summary>
       public int CurrentUser { get; set; }

       /// <summary>
       /// Other Users
       /// </summary>
       public int OtherUsers { get; set; }

       /// <summary>
       /// Total
       /// </summary>
       public int Total { get; set; }
              
    }
}
