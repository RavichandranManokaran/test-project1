﻿<%@ control language="C#" autoeventwireup="true" codebehind="OfflinePortalBasketSublayoutNew.ascx.cs" inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalBasketSublayoutNew" %>
<div class="content">
    <div class="content-body">
        <div class="col-md-12">
            <div class="alert alert-danger msg-margin-top" id="divEmptyBasket" clientidmode="static" runat="server" style="display: none;">
                <asp:label id="EmptyBasket" runat="server" text=""></asp:label>
            </div>
            <div id="divBookings" runat="server" clientidmode="static">
                <div class="accordian-outer">
                    <div class="panel-group" id="accordion1" style="margin-bottom: 0">
                        <asp:repeater id="rptProviders" runat="server" onitemdatabound="ProvidersBound">
                            <ItemTemplate>
                                <div class="panel panel-default">
                                    <div class="panel-heading bg-primary">
                                        <a data-toggle="collapse" data-parent="#accordion1" href="#booking<%#Eval("Key") %>" class="show-hide-list">
                                            <div class="star-rank pull-left padding-left-small padding-top-small">
                                                <asp:Repeater runat="server" ID="rptStar">
                                                    <ItemTemplate>
                                                        <span class="fa fa-star"></span>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <span class="panel-title">
                                                    <span>
                                                        <asp:Literal ID="litHotelNameHeader" runat="server"></asp:Literal></span>
                                                </span>
                                            </div>
                                        </a>
                                    </div>
                                     
                                    <div id="booking<%#Eval("Key") %>" class="panel-collapse collapse in">
                                        <div class="panel-group tbl-accordion" id="accordion">
                                        <div class="panel-body">
                                            <div>
                                                <asp:Repeater ID="rptBookings" runat="server" OnItemDataBound="BookingsBound">
                                                    <ItemTemplate>
                                                        <div class="row" data-name='booking' id="booking-<%#Eval("OrderLineId") %>">
                                                            <div class="col-xs-12 martop10">
                                                                <div class="col-xs-12 col-md-8 no-padding">
                                                                    <asp:Panel ID="pnlRoom" runat="server" CssClass="content_bg_inner_box alert-info" Visible="false">
                                                                        <div class="col-md-12">
                                                                            <div class="title">
                                                                                <h2 class="no-margin">Booking Detail</h2>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12">
                                                                            <div class="col-md-3 col-xs-12 no-padding">
                                                                                <span class="tit accent33-f">Check-in :</span>
                                                                            </div>
                                                                            <div class="col-md-9 col-xs-12 no-padding">
                                                                                <span>
                                                                                    <asp:Literal runat="server" ID="litRoomCheckInDate"></asp:Literal></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12">
                                                                            <div class="col-md-3 col-xs-12 no-padding">
                                                                                <span class="tit accent33-f">Check-out :</span>
                                                                            </div>
                                                                            <div class="col-md-9 col-xs-12 no-padding">
                                                                                <span>
                                                                                    <asp:Literal runat="server" ID="litRoomCheckOutDate"></asp:Literal></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12">
                                                                            <div class="col-md-3 col-xs-12 no-padding">
                                                                                <span class="tit accent33-f">Nights :</span>
                                                                            </div>
                                                                            <div class="col-md-9 col-xs-12 no-padding">
                                                                                <span>
                                                                                    <asp:Literal runat="server" ID="litRoomNights"></asp:Literal></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12">
                                                                            <div class="col-md-3 col-xs-12 no-padding">
                                                                                <span class="tit accent33-f">Room Type :</span>
                                                                            </div>
                                                                            <div class="col-md-9 col-xs-12 no-padding">
                                                                                <span>
                                                                                    <asp:Literal runat="server" ID="litRoomName"></asp:Literal></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12">
                                                                            <div class="col-md-3 col-xs-12 no-padding">
                                                                                <span class="tit accent33-f">Rooms :</span>
                                                                            </div>
                                                                            <div class="col-md-9 col-xs-12 no-padding">
                                                                                <span>
                                                                                    <asp:Literal runat="server" ID="litRoomQuantity"></asp:Literal></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12">
                                                                            <div class="col-md-3 col-xs-12 no-padding">
                                                                                <span class="tit accent33-f">Price :</span>
                                                                            </div>
                                                                            <div class="col-md-9 col-xs-12 no-padding">
                                                                                <span>
                                                                                    <asp:Literal runat="server" ID="litRoomPrice"></asp:Literal></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12">
                                                                            <div class="col-md-6 col-xs-6 no-padding">
                                                                                <button id="ButtonSetAccomodation" type="button" onclick="OpenPopup('<%#Eval("OrderLineId") %>' , '1')" data-olid='<%#Eval("OrderLineId") %>' class="btn btn-default add-break margin-3 buttonaccomodation"><span class="fa fa-unlink"></span><%=SetAccommodationText%> </button>
                                                                            </div>
                                                                            <div class="col-md-6 col-xs-6 no-padding">
                                                                                <h5 class="pull-right"><a class="accent58-f" href="javascript:void(0);" data-remove="<%#Eval("OrderLineId") %>"><span class="glyphicon glyphicon-trash"></span>Remove Item</a></h5>
                                                                                <input runat="server" id="chkValidateAcc" type="checkbox" data-validate="accomodation" data-olid='<%#Eval("OrderLineId") %>' style="display: none;" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12">
                                                                            <div class="col-md-12 col-xs-12 no-padding">
                                                                                <span style="display: none;" class="required martop10 accmdtmsg" id="spanAccomodationValidation" data-olid="<%#Eval("OrderLineId") %>"><%=AccomodationDetailsRequiredMsg%></span>
                                                                            </div>
                                                                        </div>
                                                                    </asp:Panel>
                                                                    <asp:Panel ID="pnlBedBanks" runat="server" CssClass="content_bg_inner_box alert-info" Visible="false">
                                                                        <div class="col-md-12">
                                                                            <div class="title">
                                                                                <h2 class="no-margin">Booking Detail</h2>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12">
                                                                            <div class="col-md-3 col-xs-12 no-padding">
                                                                                <span class="tit accent33-f">Check-in :</span>
                                                                            </div>
                                                                            <div class="col-md-9 col-xs-12 no-padding">
                                                                                <%--<span>Thursday, October 22, 2015 from 14:00</span>--%>
                                                                                <span>
                                                                                    <asp:Literal runat="server" ID="bblitRoomCheckInDate"></asp:Literal>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12">
                                                                            <div class="col-md-3 col-xs-12 no-padding">
                                                                                <span class="tit accent33-f">Check-out :</span>
                                                                            </div>
                                                                            <div class="col-md-9 col-xs-12 no-padding">
                                                                                <%--<span>Thursday, October 22, 2015 from 14:00</span>--%>
                                                                                <span>
                                                                                    <asp:Literal runat="server" ID="bblitRoomCheckOutDate"></asp:Literal></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12">
                                                                            <div class="col-md-3 col-xs-12 no-padding">
                                                                                <span class="tit accent33-f">Nights :</span>
                                                                            </div>
                                                                            <div class="col-md-9 col-xs-12 no-padding">
                                                                                <%--<span>1</span>--%>
                                                                                <span>
                                                                                    <asp:Literal runat="server" ID="bblitRoomNights"></asp:Literal></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12">
                                                                            <div class="col-md-3 col-xs-12 no-padding">
                                                                                <span class="tit accent33-f">Room Type :</span>
                                                                            </div>
                                                                            <div class="col-md-9 col-xs-12 no-padding">
                                                                                <%--<span>4* Family 1 Child (Breakfast)</span>--%>
                                                                                <span>
                                                                                    <asp:Literal runat="server" ID="bblitRoomName"></asp:Literal></span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12">
                                                                            <div class="col-md-3 col-xs-12 no-padding">
                                                                                <span class="tit accent33-f">Rooms :</span>
                                                                            </div>
                                                                            <div class="col-md-9 col-xs-12 no-padding">
                                                                                <%--<span>1</span>--%>
                                                                                <span class="col-sm-12 col-md-12 no-padding font11">
                                                                                    <asp:Literal runat="server" ID="bblitRoomQuantity"></asp:Literal></span>
                                                                            </div>
                                                                        </div>
                                                                        <%--<div class="col-xs-12">
                                                                <div class="col-md-3 col-xs-12 no-padding">
                                                                    <span class="tit accent33-f">People :</span>
                                                                </div>
                                                                <div class="col-md-9 col-xs-12 no-padding">
                                                                    <span>2 Adults</span>
                                                                </div>
                                                            </div>--%>
                                                                        <div class="col-xs-12">
                                                                            <div class="col-md-3 col-xs-12 no-padding">
                                                                                <span class="tit accent33-f">Price :</span>
                                                                            </div>
                                                                            <div class="col-md-9 col-xs-12 no-padding">
                                                                                <%--<span>€ 85</span>--%>
                                                                                <div id="PriceDiv" runat="server" class="col-md-2 no-padding">
                                                                                    <span>
                                                                                        <asp:Literal runat="server" ID="bblitRoomPrice"></asp:Literal>
                                                                                    </span>
                                                                                </div>
                                                                                <div class="col-md-10 no-padding"><asp:Literal runat="server" ID="bblitNewRoomPrice"></asp:Literal></div>
                                                                                <div class="col-md-12 no-padding font11"><asp:Literal runat="server" ID="litBBPriceInfo"></asp:Literal></div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12">
                                                                            <div class="col-md-6 col-xs-6 no-padding">
                                                                                <button id="ButtonSetAccomodationBB" type="button" onclick="OpenPopup('<%#Eval("OrderLineId") %>' , '1')" data-olid='<%#Eval("OrderLineId") %>' class="btn btn-default add-break margin-3 buttonaccomodation"><span class="fa fa-unlink"></span><%=SetAccommodationText%> </button>
                                                                            </div>
                                                                            <div class="col-md-6 col-xs-6 no-padding">
                                                                                <h5 class="pull-right"><a class="accent58-f" href="javascript:void(0);" data-remove="<%#Eval("OrderLineId") %>"><span class="glyphicon glyphicon-trash"></span>Remove Item</a></h5>
                                                                                <input runat="server" id="chkValidateAccBB" type="checkbox" data-validate="accomodation" data-olid='<%#Eval("OrderLineId") %>' style="display: none;" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12">
                                                                            <div class="col-md-12 col-xs-12 no-padding">
                                                                                <span style="display: none;" class="required martop10 accmdtmsg" id="spanAccomodationValidation" data-olid="<%#Eval("OrderLineId") %>"><%=AccomodationDetailsRequiredMsg%></span>
                                                                            </div>
                                                                        </div>
                                                                    </asp:Panel>
                                                                </div>
                                                                <%--Errata feed start--%>
                                                                <div class="col-xs-12" id="errataInfoDiv" runat="server">
                                                                    <div class="row">
                                                                        <div class="content_bg_inner_box alert-warning">
                                                                            <div class="col-md-12">
                                                                                <asp:Repeater ID="ErrataRepeater" runat="server" OnItemDataBound="ErrataBound">
                                                                                    <ItemTemplate>
                                                                                        <div class="col-md-3 no-padding">
                                                                                            <asp:Literal ID="BBerrataSubject" runat="server"></asp:Literal></div>
                                                                                        <div class="col-md-9 no-padding">
                                                                                            <asp:Literal ID="BBerrataDescription" runat="server"></asp:Literal></div>
                                                                                    </ItemTemplate>
                                                                                </asp:Repeater>
                                                                            </div>
                                                                            <div class="col-md-12">
                                                                                <div class="col-md-3 no-padding"><span>
                                                                                    <asp:Literal ID="cancellationHeader" runat="server"></asp:Literal></span></div>
                                                                                <div class="col-md-9 no-padding">
                                                                                    <asp:Repeater ID="CancellationCriteriaRepeater" runat="server" OnItemDataBound="CancellationCriteriaBound">
                                                                                        <ItemTemplate>
                                                                                            <span>
                                                                                                <asp:Literal ID="cancelCriteria" runat="server"></asp:Literal></span>
                                                                                        </ItemTemplate>
                                                                                    </asp:Repeater>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12 required">
                                                                                <asp:Literal ID="nonTransferableText" runat="server"></asp:Literal></div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <%--Errata feed End--%>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </ItemTemplate>
                        </asp:repeater>
                        <div class="col-xs-12 no-padding martop10" id="btn-continue" style="margin-bottom: 10px;">
                            <%--<asp:Button ID="ButtonAddAnotherBreak" OnClick="ButtonAddAnotherBreak_Click" class="btn btn-default pro-check" runat="server" CausesValidation="False" />--%>
                            <button type="button" onserverclick="ButtonAddAnotherBreak_Click" class="btn btn-default pro-check" runat="server">
                                <sc:text id="Continuetobooking" field="AddToBreak" runat="server" />
                            </button>
                        </div>
                        <asp:textbox id="hdnValidateAcc" style="display: none;" runat="server" />
                        <asp:customvalidator id="GuestName" validateemptytext="true" controltovalidate="hdnValidateAcc" validationgroup="PaymentValidation" clientvalidationfunction="IsValidAccomation" display="None" errormessage="" runat="server" />
                    </div>
                </div>
            </div>
            <div class="accordian-outer">
            <div class="panel-group" id="accordion2" style="margin-bottom: 0">
                <div class="panel panel-default">
                    <div class="panel-heading bg-primary">
                        <a id="paymentsection" data-toggle="collapse" data-parent="#accordion2" href="#booking" class="show-hide-list">
                            <div class="star-rank pull-left padding-left-small padding-top-small">
                                <span class="panel-title">
                                    <span>Payment Section </span>
                                </span>
                            </div>
                        </a>
                    </div>
					
                    <div id="booking" class="panel-collapse collapse in">
                        <div class="panel-body" >
                        <div class="row" id="divFees" runat="server" clientidmode="static">
                            <div class="col-sm-5 no-padding-m" >
                                <div class="content_bg_inner_box no-margin alert-info">
                                    <div class="col-xs-12">
                                        <div class="col-xs-8 no-padding">
                                            <span class="accent33-f" id="spnProFeeLabel">Processing Fee :</span>
                                        </div>
                                        <div class="col-xs-4 no-padding">
                                            <span class="pull-right" id="spnProFeeValue">
                                                <asp:literal id="litProFee" runat="server"></asp:literal>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="col-xs-8 no-padding">
                                            <span class="accent33-f" id="spnPayableTodayLabel">Total Payable Today :</span>
                                        </div>
                                        <div class="col-xs-4 no-padding pull-right">
                                            <span class="pull-right" id="spnPayableTodayValue">
                                                <asp:literal id="litPayableToday" runat="server"></asp:literal>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="col-xs-8 no-padding">
                                            <span class="accent33-f" id="spnPayableAccomodationLabel">Total Payable at Accommodation :</span>
                                        </div>
                                        <div class="col-xs-4 no-padding">
                                            <span class="pull-right" id="spnPayableAccomodationValue">
                                                <asp:literal id="litPayableAccomodation" runat="server"></asp:literal>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="col-xs-8 no-padding">
                                            <span class="accent33-f" id="spnTotalAmountLabel">Total Amount :</span>
                                        </div>
                                        <div class="col-xs-4 no-padding">
                                            <span class="pull-right" id="spnTotalAmountValue">
                                                <asp:literal id="litTotalAmount" runat="server"></asp:literal>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-7 no-padding-m" id="PaymentSection" runat="server" visible="false">
                                <div class="col-xs-12 no-padding">
                                    <span id="spnPaymentError" class="required"></span>
                                </div>
                                <div class="col-xs-12 no-padding" id="divPaymentPortions">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">Payment Portion</h4>
                                        </div>
                                        <div class="panel-body  alert-info">
                                            <div class="paymentPortion">
                                                <label class="checkbox-inline font-size-12">
                                                    <asp:checkbox id="fullCheckbox" runat="server" value="Full" text="Full" />
                                                </label>
                                            </div>
                                            <div class="paymentPortion">
                                                <label class="checkbox-inline font-size-12">
                                                    <asp:checkbox id="depositCheckBox" runat="server" value="Partial" text="Deposit" />
                                                </label>
                                            </div>
                                            <div class="paymentPortion">
                                                <label class="checkbox-inline font-size-12">
                                                    <asp:checkbox id="partialCheckBox" runat="server" value="Deposit" text="Partial" />
                                                    <asp:literal id="LitInst" runat="server" />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 no-padding" id="divPaymentTypes">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">Payment Method</h4>
                                        </div>
                                        <div class="panel-body  alert-info">
                                            <div class="paymentMethod">
                                                <label class="checkbox-inline font-size-12">
                                                    <asp:checkbox id="creditDebitCheckBox" runat="server" value="CreditCard" text="Credit/Laser/Debit Card" />
                                                </label>
                                            </div>
                                            <div class="paymentMethod">
                                                <label class="checkbox-inline font-size-12">
                                                    <asp:checkbox id="chequeCheckBox" runat="server" value="Cheque" text="Cheque" />
                                                </label>
                                            </div>
                                            <div class="paymentMethod">
                                                <label class="checkbox-inline font-size-12">
                                                    <asp:checkbox id="cashCheckBox" runat="server" value="Cash" text="Cash" />
                                                </label>
                                            </div>
                                            <div class="paymentMethod">
                                                <label class="checkbox-inline font-size-12">
                                                    <asp:checkbox id="poCheckBox" runat="server" value="PostOffice" text="Post Office / Bank Transfer" />
                                                </label>
                                            </div>
                                            <div class="paymentMethod">
                                                <label class="checkbox-inline font-size-12">
                                                    <asp:checkbox id="voucherCheckBox" runat="server" value="Voucher" text="Voucher & Other" />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 no-padding">
                                    <asp:button validationgroup="PaymentValidation" runat="server" clientidmode="Static" type="submit" onclick="ProceedToPayment" id="paymentButton" class="btn btn-default add-break pull-right margin-row font-size-12 marR5" text="Continue To Payment"></asp:button>
                                </div>
                            </div>
                        </div>
                    </div>
					</div>
				</div>
            </div>
           </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function OpenPopup(OrderlineId, NoOfRooms) {

        window.open("/set-accommodation?Rooms=" + NoOfRooms + "&GuestRange=" + '1' + "&orderid=" + OrderlineId + "+", "List", "toolbar=no, location=no,status=yes,menubar=no,scrollbars=yes,resizable=no, width=900,height=600,left=430,top=100");
        return false;
    }
    jQuery(function ($) {
        $("#divPaymentPortions input").change(function (e) {
            if ($(this).is(':checked')) {
                $("#divPaymentPortions input").not(this).removeAttr('checked');
                UpdatePayment();
            }
            e.preventDefault();
            e.stopPropagation();
        })
        $("#divPaymentTypes input").change(function (e) {
            if ($(this).is(':checked')) {
                $("#divPaymentTypes input").not(this).removeAttr('checked');
                UpdatePayment();
            }
            e.preventDefault();
            e.stopPropagation();
        })
        $("a[data-remove]").click(function () {
            olid = $(this).data("remove");
            RemoveBasketItem(olid);
        });


    });

    function CheckAvailability() {
        $.get("/handlers/cs/baskethelper.ashx?action=checkavailability", function (data) {
            if (!results.IsSuccess) {
                CS.Common.ShowResult(results); event.preventDefault(); return false;
            }
        });
    }
    function RemoveBasketItem(olid) {
        $.get("/handlers/cs/baskethelper.ashx?action=removeitem&olid=" + olid, function (data) {
            if (data.Success) {
                RemoveItem();
                UpdatePayment();
            }
        });
    }
    function RemoveItem() {
        if ($("div[data-name='booking']").length == 1) {
            $("#divBookings").empty();
            $("#divFees").empty();
            $("#divEmptyBasket").css("display", "block");
            $(".booking-detail-info").empty();
        }
        else if ($("#booking-" + olid).siblings("div[data-name='booking']").length == 0) {
            var panel = $("#booking-" + olid).closest('.panel');
            panel.hide("fast", function () {
                panel.empty();
                panel.remove();
            });
        }
        else {
            var booking = $("#booking-" + olid);
            booking.hide("fast", function () {
                booking.empty();
                booking.remove();
            });
        }
        UpdateBasketCount(-1);
        CS.Common.ShowAlert("Item removed from basket.")
    }
    function UpdatePayment() {
        $("#paymentButton").attr("disabled", true);
        var paymentMethod = ($("#divPaymentPortions input:checked").val() != undefined) ? "&method=" + $("#divPaymentPortions input:checked").val() : "";
        var paymentType = ($("#divPaymentTypes input:checked").val() != undefined) ? "&type=" + $("#divPaymentTypes input:checked").val() : "";
        $.get("/handlers/cs/baskethelper.ashx?action=calculatepayment" + paymentMethod + paymentType, function (data) {
            if (data != null) {
                $("#spnProFeeValue").html(data.ProcessingFee);
                $("#spnPayableTodayValue").html(data.TotalPayableToday);
                $("#spnPayableAccomodationValue").html(data.TotalPayableAtAccommodation);
                $("#spnTotalAmountValue").html(data.TotalPrice);
                $("#paymentButton").attr("disabled", false);
            }
        });
    }
    function IsValidAccomation(oSrc, args) {
        args.IsValid = ValidatePaymentSelection();
    }
    function ValidatePaymentSelection() {
        var paymentMethodMsg = "Please select a payment portion";
        var paymentPortionMsg = "Please select a payment method";

        var paymentMethodcheckboxes = $('#divPaymentPortions input[type="checkbox"]');
        var paymentPortioncheckboxes = $('#divPaymentTypes input[type="checkbox"]');
        var countCheckedboxPaymentmethod = paymentMethodcheckboxes.filter(':checked').length;
        var countCheckedboxPaymentPortion = paymentPortioncheckboxes.filter(':checked').length;
        if (countCheckedboxPaymentmethod < 1)
            $('#spnPaymentError').html(paymentMethodMsg);
        else if (countCheckedboxPaymentPortion < 1)
            $('#spnPaymentError').html(paymentPortionMsg);
        else
            $('#spnPaymentError').html("");

        return (SetAccomodationValidation() && countCheckedboxPaymentmethod == 1 && countCheckedboxPaymentPortion == 1);
    }
    function SetAccomodationValidation() {
        var checkedCount = 0;
        $('input[data-olid]').each(function () {
            var olid = $(this).data("olid");
            if (!$(this).prop("checked")) {
                checkedCount++;
                $("span[data-olid='" + olid + "']").css("display", "block");
            }
        });
        return checkedCount == 0;
    }

    $(document).ready(function () {
        $("input[id*='chkValidateAcc']:checked,input[id*='chkValidateAccBB']:checked").each(function () {
            var CurrentOlid = $(this).data("olid");
            $(this).parent().prev().children("button").css("background-color", "#428243");
            $("span[data-olid='" + CurrentOlid + "']").css("display", "none");
        });

        $('#paymentsection').click(function () {
            if ($("#booking").hasClass("in"))
                $("#booking").collapse('hide');
            else
                $("#booking").collapse('show');
        });
    });


</script>
