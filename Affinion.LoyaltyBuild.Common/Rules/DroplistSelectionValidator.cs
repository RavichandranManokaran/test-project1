﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
///Source File:           DroplistSelectionValidator.cs
///Sub-system/Module:     Affinion.LoyaltyBuild.Common.Rules
///Description:           Class for Field  Validator rules
///</summary>
#region Using Directives
using Sitecore.Data.Items;
using Sitecore.Data.Validators;
using System;
using System.Runtime.Serialization;
using Sitecore.Data.Fields;
using Affinion.LoyaltyBuild.Common.Instrumentation;
#endregion

namespace Affinion.LoyaltyBuild.Common.Rules
{
    /// <summary>
    /// Class to validate the dropdown selection
    /// </summary>
    [Serializable]
    public class DroplistSelectionValidator : StandardValidator
    {   
        #region constructors
        public DroplistSelectionValidator()
        {

        }

        public DroplistSelectionValidator(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {

        }
        #endregion

        /// <summary>
        /// Evaluate the field value
        /// </summary>
        /// <returns></returns>
        protected override ValidatorResult Evaluate()
        {
            try
            {
                Item myItem = base.GetItem();

                //GroupedDroplistField field = new GroupedDroplistField(myItem.Fields[base.FieldID]);
                var value = base.GetControlValidationValue();

                //Get the Parameter value
                if (value.Contains("Please"))
                {
                    Text = String.Format("Please Select a valid " + myItem.Fields[this.FieldID].DisplayName);
                    return ValidatorResult.Error;
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, this);
                throw;
            }
            
            return ValidatorResult.Valid;
        }

        /// <summary>
        /// ValidatorResult of a field
        /// </summary>
        /// <returns></returns>
        protected override ValidatorResult GetMaxValidatorResult()
        {
            return GetFailedResult(ValidatorResult.Error);
        }

        /// <summary>
        /// overide the name
        /// </summary>
        public override string Name
        {
            get { return "Dropdown Selection Validator"; }
        }
    }
}
