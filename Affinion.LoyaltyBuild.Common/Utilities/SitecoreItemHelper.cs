﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SitecoreItemHelper.cs
/// Sub-system/Module:     Common(VS project name)
/// Description:           Contains helper methods related to 
/// </summary>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Common.Utilities
{
    public static class SitecoreItemHelper
    {
        #region Public methods
        /// <summary>
        /// Remove special charactors from item name and append dash (-) for spaces
        /// </summary>
        /// <param name="name">Item name</param>
        /// <returns>Formatted item name</returns>
        public static string SetItemName(string name)
        {
            return Regex.Replace(name, @"\(*\)*", string.Empty).Replace("&", "and").Replace("+", "and").Replace(' ', '-').Replace("Ø", "oe").Replace("Æ", "ae").Replace("Å", "au").Replace("Ä", "ae").Replace("Ö", "oe").Replace("æ", "ae").Replace("ø", "oe").Replace("å", "au").Replace("ä", "ae").Replace("ö", "oe");
        } 

        /// <summary>
        /// Replace item name by any given character 
        /// </summary>
        /// <param name="name">Item Name</param>
        /// <param name="oldValue">Value need to replace</param>
        /// <param name="newValue">Value should appear in new formatted name</param>
        /// <returns>Formatted item name</returns>
        public static string ResetItemName(string name, string oldValue, string newValue)
        {
            return Regex.Replace(name, oldValue, newValue);
        }
        #endregion
    }
}
