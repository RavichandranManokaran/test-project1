﻿CREATE PROCEDURE [dbo].[LB_GetDueDetail]
(
	@DueId INT
) 
AS 
BEGIN
	SELECT [OrderLineDueId]
		,[OrderLineID]
		,BookingId	
		,[DueTypeId]
		,[CurrencyTypeId]
		,[TotalAmount]
		,[CommissionAmount]
		,[ProviderAmount]
		,[ProcessingFeeAmount]
		,[Note]
		,[IsDeleted]
		,[CreatedBy]
		,[CreatedDate]
		,[UpdatedBy]
		,[UpdatedDate]
		,[PaymentMethodId]
	FROM 
		[dbo].[LB_OrderLineDue]
	WHERE 
		[OrderLineDueId] = @DueId
END
