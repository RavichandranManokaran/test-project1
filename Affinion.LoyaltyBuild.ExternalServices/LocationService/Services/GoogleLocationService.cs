﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           GoogleLocationService.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.ExternalServices.LocationService
/// Description:           GoogleLocationService code will be used to retrived data from sitecore end and expose them
///                        to web service. Contains methods for filtering data and couple of helper methods. 


#region Directives

using Affinion.LoyaltyBuild.Common.Exceptions;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.ExternalServices.LocationService.DataObjects;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;

#endregion Directives

namespace Affinion.LoyaltyBuild.ExternalServices.LocationService.Services
{
    /// <summary>
    /// Google Location Service class: Contains service methods to invoke data related to google map geo locations
    /// </summary>   
    public class GoogleLocationService : ILocationService<GoogleLocationData>
    {
        #region Constants

        /// <summary>
        /// Constant Value of Latitude
        /// </summary>
        private const string Latitude = "Latitude";

        /// <summary>
        /// Constant Value of Longitude
        /// </summary>
        private const string Longitude = "Longitude";


        /// <summary>
        /// Constant value fo Description
        /// </summary>
        private const string Description = "Description";

        /// <summary>
        /// Contant value of GetLactions Field Name
        /// </summary>
        private const string GetLocationFieldName = "GetLocations";

        /// <summary>
        /// Constant value of SubLocations Field Name
        /// </summary>
        private const string SubLocationsFieldName = "SubLocations";


        /// <summary>
        /// Constant value of Center location Name
        /// </summary>
        private const string CenterLocationName = "CenterLocation";

        /// <summary>
        /// Constant value for exception message
        /// </summary>
        private const string ExceptionMessage = "is missing";


        #endregion Constants

        #region Private Properties

        /// <summary>
        /// Gets the current Item
        /// </summary>
        private Item GetCurrentItem
        {
            get
            {
                Item CurrentlocationItem = Sitecore.Context.Database.GetItem(new ID("{6FE6E265-E673-481B-97BF-DD9704CE14B1}"));

                if (CurrentlocationItem != null)
                {
                    return CurrentlocationItem;
                }
                else
                {
                    return CurrentlocationItem;
                }
            }
        }

        /// <summary>
        /// Gets the SubLocation Multi List Field
        /// </summary>
        //private MultilistField GetSubLocationMultilistField
        //{
        //    get
        //    {
        //        Item SublocationItem = this.GetCurrentItem;
        //        if (SublocationItem != null)
        //        {
        //            //Get the multilist field
        //            MultilistField locationMultiListField = SublocationItem.Fields[SubLocationsFieldName];
        //            if (locationMultiListField != null)
        //            {
        //                return locationMultiListField;
        //            }
        //            else
        //            {
        //                return locationMultiListField;
        //            }
        //        }
        //        else
        //        {
        //            return null;
        //        }
        //    }
        //}

        /// <summary>
        /// Returns of GetLocaions MulitilistField
        /// </summary>
        //private MultilistField GetLocationsMultiListField
        //{
        //    get
        //    {
        //        Item ReturnLocationItem = this.GetCurrentItem;
        //        if (ReturnLocationItem != null)
        //        {
        //            //Get the multilist field
        //            MultilistField locationMultiListField = ReturnLocationItem.Fields[GetLocationFieldName];
        //            if (locationMultiListField != null)
        //            {
        //                return locationMultiListField;
        //            }
        //            else
        //            {
        //                return locationMultiListField;
        //            }
        //        }
        //        else
        //        {
        //            return null;
        //        }
        //    }
        //}

        #endregion Private Properties


        #region Private Methods

        /// <summary>
        /// Get MultiList Field in an item
        /// </summary>
        /// <param name="item">item</param>
        /// <param name="locationFieldName">Field name</param>
        /// <returns>Returns multi list field</returns>
        private MultilistField GetMultiListField(Item item, string locationFieldName)
        {
            item = this.GetCurrentItem;
            
            //Get the multilist field
            MultilistField locationMultiListField = item.Fields[locationFieldName];
            if (locationMultiListField != null)
            {
                return locationMultiListField;
            }
            else
            {
                return locationMultiListField;
            }
            
        }


        /// <summary>
        /// Get the sub location list as a generic object list
        /// </summary>
        /// <param name="centerLocationName">Center Location Name</param>
        /// <returns>List of GoogleLocationDTO objects</returns>
        private List<GoogleLocationData> GetSubLocationList(string centerLocationName)
        {
            //Get the sublocation multilist field
            //MultilistField subLocationsField = this.GetSubLocationMultilistField;
            MultilistField subLocationsField = this.GetMultiListField(this.GetCurrentItem, SubLocationsFieldName);

            if (subLocationsField != null)
            {
                List<GoogleLocationData> sublocationList = new List<GoogleLocationData>();

                //Iterate through each item
                foreach (Item item in subLocationsField.GetItems())
                {
                    if (item.Fields[CenterLocationName] != null)
                    {
                        //Checks the selected centerlized location is similar to each item center location
                        if (item.Fields[CenterLocationName].Value.Equals(centerLocationName))
                        {
                            GoogleLocationData sublocation = new GoogleLocationData();
                            sublocation = AddListItemToDTOType(item, sublocation);
                            sublocationList.Add(sublocation);
                        }
                    }
                    else
                    {
                        throw new AffinionException(string.Format("{0} {1}", CenterLocationName, ExceptionMessage));
                    }
                }

                return sublocationList;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Add sitecore item values to a DTO object type
        /// </summary>
        /// <param name="item">Item: multi list item</param>
        /// <param name="locationDTO">GoogleLocationDTO object type</param>
        /// <returns>returns GoogleLocationDTO object Type</returns>
        private static GoogleLocationData AddListItemToDTOType(Item item, GoogleLocationData locationDTO)
        {
            locationDTO.LocationName = item.Name;
            locationDTO.Latitude = item.Fields[Latitude].Value;
            locationDTO.Longitude = item.Fields[Longitude].Value;
            locationDTO.Description = item.Fields[Description].Value;
            return locationDTO;
        }

        #endregion Private Methods

        #region Public Methods

        /// <summary>
        /// Get the list of Centrelized locations
        /// </summary>
        /// <returns>An instance of GoogleLocationData containing Latitude,Longitude and the location name</returns>
        public List<GoogleLocationData> GetLocations()
        {
            List<GoogleLocationData> locationList = new List<GoogleLocationData>();
            try
            {
                //Get th location Multilistfield
                //MultilistField locationMultiListField = this.GetLocationsMultiListField;
                MultilistField locationMultiListField = this.GetMultiListField(this.GetCurrentItem, GetLocationFieldName);

                if (locationMultiListField != null)
                {
                    //If location Multilist field is not null
                    if (locationMultiListField != null)
                    {
                        foreach (Item item in locationMultiListField.GetItems())
                        {
                            GoogleLocationData location = new GoogleLocationData();
                            location = AddListItemToDTOType(item, location);
                            locationList.Add(location);
                        }
                    }
                }
                return locationList;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ExternalServices, exception, this);
                throw;
            }
        }

        /// <summary>
        /// Get the Location Details based on the user selected centralized location 
        /// </summary>
        /// <param name="location">selected location name</param>
        /// <returns>returns of location details based of the selected location.(Location Name, Latitude and Longitude)</returns>
        public List<GoogleLocationData> GetLocations(string location)
        {
            //Filterd Location List
            List<GoogleLocationData> filterdLocationList = new List<GoogleLocationData>();

            try
            {
                if (!string.IsNullOrEmpty(location))
                {
                    //Get all the location data
                    List<GoogleLocationData> locationList = this.GetLocations();

                    //Sub locaiton list
                    List<GoogleLocationData> subLocationList = new List<GoogleLocationData>();

                    //Iterate through all the locations to get the selected location details
                    foreach (GoogleLocationData GoogleLocationItem in locationList)
                    {
                        if (GoogleLocationItem != null)
                        {
                            //If the location name is equal to selected location name
                            if (GoogleLocationItem.LocationName.Equals(location))
                            {
                                GoogleLocationData currentLocationData = new GoogleLocationData();
                                currentLocationData.LocationName = GoogleLocationItem.LocationName;
                                currentLocationData.Latitude = GoogleLocationItem.Latitude;
                                currentLocationData.Longitude = GoogleLocationItem.Longitude;
                                currentLocationData.Description = GoogleLocationItem.Description;
                                filterdLocationList.Add(currentLocationData);

                                //Get the sub location list 
                                subLocationList = this.GetSubLocationList(location);

                                if (subLocationList != null)
                                {
                                    //Add to filtered location list
                                    filterdLocationList.AddRange(subLocationList);
                                }
                            }
                        }
                    }

                    //Returns of the list locations based on the selected value.
                    return filterdLocationList;
                }
                else
                {
                    return filterdLocationList;
                }
            }
            catch (AffinionException exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ExternalServices, exception, this);
                throw;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ExternalServices, exception, this);
                throw;
            }
        }

        #endregion Public Methods
    }
}
