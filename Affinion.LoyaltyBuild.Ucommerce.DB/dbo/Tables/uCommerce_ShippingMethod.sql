﻿CREATE TABLE [dbo].[uCommerce_ShippingMethod] (
    [ShippingMethodId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]             NVARCHAR (128) NOT NULL,
    [ImageMediaId]     NVARCHAR (255) NULL,
    [PaymentMethodId]  INT            NULL,
    [ServiceName]      NVARCHAR (128) NULL,
    [Deleted]          BIT            CONSTRAINT [uCommerce_DF_ShippingMethod_Deleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [uCommerce_PK_ShippingMethod] PRIMARY KEY CLUSTERED ([ShippingMethodId] ASC),
    CONSTRAINT [uCommerce_FK_ShippingMethod_PaymentMethod] FOREIGN KEY ([PaymentMethodId]) REFERENCES [dbo].[uCommerce_PaymentMethod] ([PaymentMethodId])
);

