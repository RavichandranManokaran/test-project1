﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           DiagnosticsCategory.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Used to log diagnostic data
/// </summary>

namespace Affinion.LoyaltyBuild.Common.Instrumentation
{
    /// <summary>
    /// Provides enumeration for Diagnostic Category.
    /// </summary>
    public enum DiagnosticsCategory
    {

        /// <summary>
        /// Write diagnostics category for branding offers module
        /// </summary>
        Promotions,

        /// <summary>
        /// Write diagnostics category for bookings module
        /// </summary>
        Bookings,

        /// <summary>
        /// Write diagnostics category for vouchers module
        /// </summary>
        Vouchers,

        /// <summary>
        /// Write diagnostics category for common module
        /// </summary>
        Common,

        /// <summary>
        /// Write diagnostics category for client Portal module
        /// </summary>
        ClientPortal,

        /// <summary>
        /// Write diagnostics category for client setup module
        /// </summary>
        ClientSetup,

        /// <summary>
        /// Write diagnostics category for Supplier setup module
        /// </summary>
        SupplierSetup,

        /// <summary>
        ///  Write diagnostics category for communication module
        /// </summary>
        Communications,

        /// <summary>
        ///  Write diagnostics category for ExternalServices module
        /// </summary>
        ExternalServices,

        /// <summary>
        /// Write diagnostics category for DataAccess module
        /// </summary>
        DataAccess,

        /// <summary>
        /// Write diagnostics category for Security module
        /// </summary>
        Security,

        /// <summary>
        /// Write diagnostics category for search project
        /// </summary>
        Search,

        /// <summary>
        /// Write diagnostics category for Offline module
        /// </summary>
        OfflinePortal,

        /// <summary>
        /// Write diagnostics category for business logic
        /// </summary>
        BusinessLogic,

        /// <summary>
        /// Write diagnostics category for supplier portal
        /// </summary>
        SupplierPortal,

        MasterClassBusinessLogic,


        MasterClassWebsite,
        /// <summary>
        /// Sync UComm data
        /// </summary>
        SyncData,
        AdminPortal,

        /// <summary>
        /// Diagnostics category for Audit Trail
        /// </summary>
        AuditTrail

    }


}
