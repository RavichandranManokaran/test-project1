﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           GenericHelpers.cs
/// Sub-system/Module:     ClientSetup(VS project name)
/// Description:           GenericHelpers for uCommerce Initalization
/// </summary>
 

#region Using Directives
using System;
using UCommerce.Infrastructure;
using UCommerce.Infrastructure.Globalization; 
#endregion

namespace Affinion.LoyaltyBuild.ClientSetup.ClientSetup
{
    /// <summary>
    /// Helpler class
    /// </summary>
    public static class GenericHelpers
    {
        public static void DoForEachCulture(Action<string> toDo)
        {
            if (toDo != null)
            {
                foreach (Language language in ObjectFactory.Instance.Resolve<ILanguageService>().GetAllLanguages())
                {
                    toDo(language.CultureCode);
                } 
            }
        }
    }
}
