﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    /// <summary>
    /// CampaignDetails
    /// </summary>
    public class CampaignDetail
    {
        /// <summary>
        /// Offer Name
        /// </summary>
        public string CampaignName { get; set; }
    }
}
