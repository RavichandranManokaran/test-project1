﻿



CREATE PROCEDURE [dbo].[OLD_affinion_GetProvisionalHilidayHomesBookings] 

	   @clientId as varchar(50) = null

AS

Begin

Select	   OL.OrderLineId,

		   RR.OrderReference,

		   CUS.FirstName +' ' + CUS.LastName CustomerName,

		   OA.EmailAddress,

		   RR.GuestName,

		   C.CategoryId,

		   C.Name ProviderName,

		   PO.CurrencyId,

		   CU.ISOCode,

		   RR.CheckInDate,

		    isnull((select top 1  ISNULL(Total,0) from affinion_fnGetDuesByOrderLineID(OL.OrderLineId,null)),0) AmountDue

		   

	FROM 	

	[dbo].uCommerce_OrderLine OL 

	INNER JOIN [dbo].uCommerce_PurchaseOrder PO ON OL.OrderId = PO.OrderId

	INNER JOIN [dbo].uCommerce_OrderAddress  OA ON OA.OrderId = PO.OrderId 

	INNER JOIN [dbo].LB_RoomReservation RR ON OL.OrderLineId = RR.OrderLineId 

	INNER JOIN [dbo].uCommerce_Product P ON OL.Sku = P.Sku

	INNER JOIN [dbo].uCommerce_CategoryProductRelation CPR ON P.ProductId = CPR.ProductId 

	INNER JOIN [dbo].uCommerce_Category C ON CPR.CategoryId = C.CategoryId

	INNER JOIN [dbo].uCommerce_Customer CUS ON PO.CustomerId = CUS.CustomerId 

	INNER JOIN [dbo].uCommerce_CustomerAttribute CA ON PO.CustomerId = CA.CustomerId 

	INNER JOIN [dbo].uCommerce_OrderLineStatus OLS ON RR.OrderLineStatusID = OLS.OrderLineStatusId

	INNER JOIN [dbo].uCommerce_OrderStatus OS ON OLS.StatusId = OS.OrderStatusId

	INNER JOIN [dbo].uCommerce_Currency CU ON CU.CurrencyId = PO.CurrencyId

	

	WHERE 

	OS.Name = 'Provisional'
	 and DateDiff(dd,getdate(),RR.CheckInDate) >0 
	 and CA.ClientId = @clientId

end







