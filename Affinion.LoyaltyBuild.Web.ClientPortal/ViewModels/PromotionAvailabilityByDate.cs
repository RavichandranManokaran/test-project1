﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.ViewModels
{
    public class PromotionAvailabilityByDate : PriceAvailabilityByDate
    {
        /// <summary>
        /// Promotion Id
        /// </summary>
        public int PromotionId { get; set; }
    }
}