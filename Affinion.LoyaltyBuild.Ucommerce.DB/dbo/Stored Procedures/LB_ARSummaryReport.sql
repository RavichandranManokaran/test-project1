CREATE PROCEDURE [dbo].[LB_ARSummaryReport]
@OracleFileID INT=NULL,
@DateRange DATETIME=NULL,
@DateUpTo DATETIME=NULL,
@Provider VARCHAR(MAX)=NULL,
@Partner VARCHAR(MAX)=NULL
AS
BEGIN

	SELECT P.Name [HotelName],COUNT(P.Name) [NumberOfSales],SUM(LVP.NetAmount) [ProviderAmount],CU.ISOCode [CurrencyCode]

	FROM LB_Vat_Payments LVP
		INNER JOIN uCommerce_OrderLine OL ON LVP.OrderLineId=OL.OrderLineId AND LVP.OrderId=OL.OrderId
		INNER JOIN LB_BookingInfo BI ON OL.OrderLineId=BI.OrderLineId
		INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId=PO.OrderId
		INNER JOIN uCommerce_Currency CU ON PO.CurrencyId=CU.CurrencyId
		INNER JOIN (
		   SELECT CustomerId,_ClientId [ClientId]
			FROM 
			(
			  SELECT CustomerId, [key], [value]
			  FROM LB_CustomerProperty 
			) src
			pivot
			(
			  MAX([value])
			  FOR [key] in (_ClientId)
			) piv
		) CP ON PO.CustomerId=CP.CustomerId 
		INNER JOIN uCommerce_Customer C ON CP.CustomerId=C.CustomerId
		INNER JOIN Ucommerce_product P ON OL.Sku = P.Sku
		INNER JOIN (
		   SELECT ProductId, SiteCoreItemGuid
			FROM 
			(
			  SELECT ProductId,PD.Name [key], PP.value
			  FROM uCommerce_ProductDefinitionField PD 
			  INNER JOIN uCommerce_ProductProperty PP ON PD.ProductDefinitionFieldId=PP.ProductDefinitionFieldId
			  WHERE PD.Name IN ('SiteCoreItemGuid')
			) src
			pivot
			(
			  MAX([value])
			  FOR [key] in (SiteCoreItemGuid)
			) piv
		) PPD ON PPD.ProductId=P.ProductId 

	WHERE

		(@OracleFileID IS NULL OR  LVP.OracleFileId=@OracleFileID) AND 
		LVP.PostedVatAmount IS NOT NULL AND
		(@Partner IS NULL OR   CP.ClientId = @Partner) AND
		(@Provider IS NULL OR  PPD.SiteCoreItemGuid = @Provider) AND
		(@DateRange IS NULL OR (CAST(BI.CheckInDate AS DATE) <= CAST(@DateUpTo AS DATE)  AND  CAST(BI.CheckOutDate AS DATE) >= CAST(@DateRange AS DATE)))

	GROUP BY
		CU.ISOCode,
		P.Name
			

END


