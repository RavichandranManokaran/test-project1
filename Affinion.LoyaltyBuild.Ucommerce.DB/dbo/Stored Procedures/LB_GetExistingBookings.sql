﻿CREATE PROCEDURE [dbo].[LB_GetExistingBookings]

(	   @clientIds nvarchar(512)=NULL,
	   @firstname nvarchar(512)=NULL, 
       @lastname nvarchar(512)=NULL, 
       @providerName nvarchar(50)=NULL, 
	   @reservationDate Date=NULL, 
       @arrivalDate Date=NULL,
	   @reservationDateFrom Date=NULL, 
       @reservationDateTo Date=NULL, 
       @bookingReference nvarchar(50)=NULL,
	   @EmailValidation varchar(50)=NULL,
	   @NumericValidation varchar(50)=NULL,
	   @bookingMethod varchar(50)=NULL,
	   @orderStatusId int=NULL,
	   @CreatedByUser varchar(100) = NULL)
 AS 
 BEGIN
	WITH CTE_Items (OrderLineId, OrderId, OrderGuid, Campaign, FullName, BookingReferenceId, CheckInCheckOutDates,CreatedOn, BookingStatus, Accommodation)
	AS
	(
		SELECT 
			OL.OrderLineId,
			OL.OrderId,			      
			PO.OrderGuid,
			CI.Name as [Campaign],
			CONCAT(OA.FirstName, ' ', OA.LastName) as [FullName], 
			RR.BookingNo as [BookingReferenceId],
			CONCAT( Replace(Convert(varchar(11), RR.CheckInDate,103),' ','/'), '   ', Replace(Convert(varchar(11), RR.CheckOutDate,103),' ','/')) as [CheckInCheckOutDates], 
			CONCAT(CONVERT(varchar(20),OL.CreatedOn,103), ' ', CONVERT(VARCHAR(8), OL.CreatedOn, 108)) as 'CreatedOn',
			OS.Name as [BookingStatus], 
			OL.ProductName as [Accommodation]
		FROM [dbo].uCommerce_OrderLine OL 
			INNER JOIN 	uCommerce_PurchaseOrder PO on PO.OrderId=OL.OrderId
			INNER JOIN 	uCommerce_OrderAddress OA on OA.OrderId=PO.OrderId
			INNER JOIN 	LB_BookingInfo RR on RR.OrderLineId =OL.OrderLineId AND RR.StatusId IN (5,6,7)
			INNER JOIN  Ucommerce_product P ON OL.Sku = P.Sku AND P.ParentProductId IS NULL 
			INNER JOIN	LB_CustomerProperty CP ON PO.CustomerId=CP.CustomerId AND CP.[Key]='_ClientId'
			INNER JOIN  uCommerce_OrderStatus OS on OS.OrderStatusId=RR.StatusId
			LEFT JOIN 	uCommerce_OrderLineCampaignRelation OLCR ON OL.OrderLineId=OLCR.OrderLineId  
			LEFT JOIN 	uCommerce_CampaignItem CI ON OLCR.CampaignItemId=CI.CampaignItemId	
		WHERE
			(@firstname IS NULL OR oa.FirstName Like @firstname + '%')
			AND (@lastname IS NULL OR oa.LastName Like @lastname + '%') 
			AND (@clientIds IS NULL OR CP.Value IN (SELECT item FROM dbo.SplitString(@clientIds, ','))) 
			AND (@providerName IS NULL OR p.Name Like @providerName + '%')
			AND (@ArrivalDate IS NULL OR (CONVERT(date,RR.CheckInDate)=@ArrivalDate))
			AND (@bookingReference IS NULL OR RR.BookingNo like @bookingReference + '%') 
			AND (@EmailValidation IS NULL OR oa.EmailAddress like @EmailValidation + '%')
			AND (@orderStatusId IS NULL OR RR.StatusId=@orderStatusId) 
			AND (@reservationDate IS NULL OR (CONVERT(date,OL.CreatedOn)=@reservationDate))
			AND (@reservationDateFrom IS NULL OR OL.CreatedOn >= COALESCE(@reservationDateFrom,OL.CreatedOn)) 
			AND (@reservationDateTo IS NULL OR cast(OL.CreatedOn as date) <= COALESCE(@reservationDateTo,OL.CreatedOn))
	)
	SELECT 
		OrderLineId, 
		OrderId, 
		OrderGuid, 
		Campaign, 
		FullName, 
		BookingReferenceId, 
		CheckInCheckOutDates,
		CreatedOn, 
		BookingStatus, 
		Accommodation,
		ISNULL(_NoOfAdult,0) [NoOfAdult],
		ISNULL(_NoOfChildren,0) [NoOfChild],
		_BookingThrough [BookingThrough]
	FROM 
		(SELECT CTE.*, [key], [value] 
		FROM uCommerce_OrderProperty CP 
		INNER JOIN CTE_Items CTE ON CTE.OrderId = CP.OrderId AND CTE.OrderLineId = CP.OrderLineId
		) src
	PIVOT
		(MAX([value]) FOR [key] in (_NoOfAdult, _NoOfChildren,_BookingThrough)) piv
	WHERE
	 (@bookingMethod IS NULL OR _BookingThrough=@bookingMethod) 

		
 END



