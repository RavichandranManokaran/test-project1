﻿namespace Affinion.LoyaltyBuild.Audit
{
    using Newtonsoft.Json.Linq;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Stores the data and related functions for audit records
    /// </summary>
    public class AuditData
    {
        private JObject additionalData;

        /// <summary>
        /// Gets or sets the record ID. Never set the ID manually.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the Audit name
        /// For each audit type, relevent name should be defined in enum
        /// </summary>
        public AuditNames AuditName { get; set; }

        /// <summary>
        /// Gets or sets the message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the Operation
        /// </summary>
        public AuditOperations Operation { get; set; }

        /// <summary>
        /// Gets or sets the ObjectType
        /// </summary>
        public AuditObjectType ObjectType { get; set; }

        /// <summary>
        /// Gets or sets the ObjectId
        /// </summary>
        public string ObjectId { get; set; }

        /// <summary>
        /// Gets or sets the Section
        /// </summary>
        public AuditSection Section { get; set; }

        /// <summary>
        /// Gets or sets the OldValue
        /// </summary>
        public string OldValue { get; set; }

        /// <summary>
        /// Gets or sets the NewValue
        /// </summary>
        public string NewValue { get; set; }

        /// <summary>
        /// Gets or sets the Username
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the Date
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Gets or sets the Time
        /// </summary>
        public DateTime Time { get; set; }

        /// <summary>
        /// Gets or sets Additional Data. Use setter function to add values. Do not add directly
        /// </summary>
        public JObject AdditionalData 
        { 
            get
            {
                return additionalData;
            }
            set
            {
                additionalData = value;
            }
        }

        /// <summary>
        /// Gets or sets Remarks
        /// </summary>
        public string Remarks { get; set; }

        /// <summary>
        /// Constructs the object
        /// </summary>
        public AuditData()
        {
            additionalData = new JObject();
            this.Date = DateTime.Today;
            this.Time = DateTime.Now;

            this.AuditName = AuditNames.Undefined;
            this.Operation = AuditOperations.Undefined;
            this.ObjectType = AuditObjectType.Undefined;
            this.Section = AuditSection.Undefined;
        }

        /// <summary>
        /// Sets the additional data
        /// </summary>
        /// <param name="key">Key name</param>
        /// <param name="values">Values as objects</param>
        public void AddAdditionalData(string key, params object[] values)
        {
            additionalData[key] = new JArray(values.Where(o => o != null).Select(r => r.ToString()));
        }

        /// <summary>
        /// Sets the additional data
        /// </summary>
        /// <param name="key">Key name</param>
        /// <param name="values">Values as strings</param>
        public void AddAdditionalData(string key, params string[] values)
        {
            additionalData[key] = new JArray(values);
        }

        /// <summary>
        /// Sets the additional data
        /// </summary>
        /// <param name="key">Key name</param>
        /// <param name="values">Values as a JArray object</param>
        public void AddAdditionalDataArray(string key, JArray values)
        {
            additionalData[key] = values;
        }
    }
}
