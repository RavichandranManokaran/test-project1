﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BedBanksPrimaryLayout.aspx.cs" Inherits="Affinion.LoyaltyBuild.AdminPortal.Layouts.PrimaryLayouts.BedBanks.BedBanksPrimaryLayout" %>

<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><asp:Literal ID="litTitle" runat="server"></asp:Literal></title>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Bootstrap, a sleek, intuitive, and powerful mobile first front-end framework for faster and easier web development.">
    <meta name="keywords" content="HTML, CSS, JS, JavaScript, framework, bootstrap, front-end, frontend, web development">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">


    <link href="~/ResourcesAdmin/datePicker/jquery-ui.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="~/ResourcesAdmin/styles/bootstrap.min.css" rel="stylesheet"/>
    <link href="/ResourcesAdmin/styles/bootstrap-datetimepicker.min.css" rel="stylesheet"/>
    <%--<link href="../../../ResourcesAdmin/styles/jquery.dataTables.min.css" rel="stylesheet">--%>
    
    <script type="text/javascript" src="/ResourcesAdmin/scripts/jquery-1.8.3.min.js" charset="UTF-8"></script>
    <script type="text/javascript" src="/ResourcesAdmin/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="/ResourcesAdmin/scripts/bootstrap-datetimepicker.js" charset="UTF-8"></script>
    <link href="/ResourcesAdmin/styles/responsiveNav.css" rel="stylesheet" type="text/css" />
    <link href="/ResourcesAdmin/styles/affinion.min.css" rel="stylesheet" />
    <link href="/ResourcesAdmin/styles/affinion-admin.min.css" />
    <link href="/ResourcesAdmin/styles/affinion-bedbank.css" rel="stylesheet" />
    <link href="/ResourcesAdmin/styles/theme1/theme1.css" rel="stylesheet" />
    <link href="/ResourcesAdmin/styles/font-awesome.min.css" rel="stylesheet" />
    <link href="/ResourcesAdmin/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <style>
		.list-group{
			margin-bottom:0px;
		}
		.list-group.inner li{
			border:none;
		}
		.list-group-item{
			padding:0px;
		}
		.list-group .list-group-item a{
			background-color:#00BCD4;
			color:#ffffff;
			font-weight:bold;
		}
		.inner .list-group-item a{
			background-color:#ffffff;
			border-top:1px solid #cccccc;
			color:#5a5a5a;
			font-weight:normal;
		}
		.list-group-item a{
			display:block;
			padding:10px 15px;
			border-bottom:1px solid #f7f7f7;
		}
		.list-group-submenu{
			margin-left:-40px;
		}
		.list-group-submenu .list-group-item a{
			padding-left:40px;
		}
		
		</style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container-outer width-full overflow-hidden-att">
            <div class="container-outer-inner width-full bedbank-header-top-bar">
                <div class="container-fluid">
                    <div class="row coomon-outer-padding">
                        <div class="col-md-6 col-xs-6">
                            <div class="logo">
                                <a href="" runat="server" id="linkImage" onserverclick="linkImage_ServerClick">
                                    <img runat="server" id="imgLogo" class="img-responsive" alt="logo"></a>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-6">
                            <div class="navbar navbar-default">
                                <div class="container-fluid">
                                </div>
                            </div>
                        </div>
                        <div class="sm-visibale">

                            <div class="sm-visibale-inner">
                                <div class="res-icon float-right visible-xs" id="fav-icon">
                                    <a href="#"><i class="fa fa-heart"></i></a>
                                </div>
                                <div class="res-icon float-right visible-xs" id="bck-icon">
                                    <a href="#">
                                        <div id="basket-ico-respodive">
                                            <p>
                                                <i class="fa fa-briefcase"></i>
                                                <span>5</span>
                                            </p>
                                        </div>
                                    </a>
                                </div>
                                <div class="res-icon float-right visible-xs" id="user-icon">
                                    <a href="#"><i class="fa fa-user" data-toggle="modal" data-target="#exampleModal" data-whatever="@fat"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="body-container">
            <div class="container-fluid">
                <div class="content">
                    <div class="content-body">
                        <div class="row no-margin">
                            <div class="col-sm-2 no-padding bedbank-leftpanel">
                                <div class="navbar navbar-default" role="navigation">
                                    <!-- Collect the nav links, forms, and other content for toggling -->
                                    <div class="collapse navbar-collapse navbar-ex1-collapse">
                            <ul class="list-group">
							<li class="list-group-item"><a href="#">Dash board</a>
								<ul class="list-group inner">
									<li class="list-group-item"><asp:HyperLink ID="searchLink" runat="server"><sc:Text id="Text1" runat="server" field="Margin Search Link Text"/></asp:HyperLink>
										<ul class="list-group-submenu">
											<li class="list-group-item"><asp:HyperLink ID="errorLink" ForeColor="#3399ff" runat="server"><sc:Text id="Text2" runat="server" field="Margin Edit Link Text"/></asp:HyperLink></li></li>
										</ul>
									</li>
								</ul>
							</li>
						</ul>
                                    </div>
                                    <!-- /.navbar-collapse -->
                                </div>
                            </div>
                            <div class="col-sm-10 no-padding bedbank-rightpanel">

                                <sc:placeholder id="maincontent" key="maincontent" runat="server" />

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Site footer -->
        <div class="container-outer-inner width-full copyright-bar">
            <div class="container">
                <div class="copyright-text">
                    <sc:Text id="footer" runat="server" field="Footer Text"/>
                </div>
            </div>
        </div>


        <%--<script src="../../../ResourcesAdmin/scripts/jquery.min.js"></script>--%>
        <script src="../../../ResourcesAdmin/scripts/jquery-1.10.2.js"></script>
        <%--<script src="../../../ResourcesAdmin/scripts/jquery-1.12.3.js"></script>
		<script src="../../../ResourcesAdmin/scripts/jquery.dataTables.min.js"></script>--%>
        <script src="../../../ResourcesAdmin/scripts/bootstrap.min.js"></script>

        <script>
            $(window).on('load', function () {
                target = $('.bedbank-rightpanel').height();
                $('.bedbank-leftpanel').height(target);
            });

            $(document).ready(function () {
                $('#myTable').dataTable();
            });
        </script>
    </form>
</body>
</html>
