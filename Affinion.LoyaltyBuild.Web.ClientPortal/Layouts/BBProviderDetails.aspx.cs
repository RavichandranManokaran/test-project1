﻿using Affinion.LoyaltyBuild.BedBanks.General;
using Affinion.LoyaltyBuild.BedBanks.JacTravel;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Message;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Model;
using Affinion.LoyaltyBuild.Common.Exceptions;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using data = Affinion.LoyaltyBuild.Api.Search.Data;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts
{
    public partial class BBProviderDetails : System.Web.UI.Page
    {
        public string latitude { get; set; }
        public string longitude { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            QueryStringHelper helper = new QueryStringHelper(Request.Url);
            string providerId=helper.GetValue("supplierid");
            if (!string.IsNullOrEmpty(providerId))
            {
                LoadBBProviderDetails(providerId);
            }
        }

        private void LoadBBProviderDetails(string providerId)
        {
            JtPropertyDetailsResponse propertyDetailsRes = JtController.GetSinglePropertyDetails(providerId);
            if (propertyDetailsRes == null)
                return;
            var address = new List<string>();
            if (!string.IsNullOrWhiteSpace(propertyDetailsRes.Address1))
                address.Add(propertyDetailsRes.Address1.Trim());
            if (!string.IsNullOrWhiteSpace(propertyDetailsRes.Address2))
                address.Add(propertyDetailsRes.Address2.Trim());
            if (!string.IsNullOrWhiteSpace(propertyDetailsRes.Region))
                address.Add(propertyDetailsRes.Region.Trim());
            if (!string.IsNullOrWhiteSpace(propertyDetailsRes.Country))
                address.Add(propertyDetailsRes.Country.Trim());
            imageIntroduction.ImageUrl = propertyDetailsRes.CMSBaseURL + propertyDetailsRes.MainImage;
            litProviderNameHead.Text = propertyDetailsRes.PropertyName;
            litProviderName.Text = propertyDetailsRes.PropertyName;
            //litTitle.Text = propertyDetailsRes.PropertyName;
            litAddress.Text = string.Join(", ", address);
            litOverview.Text = propertyDetailsRes.Strapline;
            litFacility.Text = string.Join(", ", propertyDetailsRes.Facilities.Select(x => x.Facility).ToList<string>());
            BindBBStarRankings(propertyDetailsRes.Rating);
            List<Item> Facilities = MapBedBanksFacilities(propertyDetailsRes.Facilities);
            rptBBFacilities.DataSource = Facilities;
            rptBBFacilities.DataBind();
            this.latitude = propertyDetailsRes.Latitude;
            this.longitude = propertyDetailsRes.Longitude;
        }

        private List<Item> MapBedBanksFacilities(JtFacility[] facilitites)
        {
            Item clientItem;
            var searchKey = new data.SearchKey(HttpContext.Current);
            clientItem = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(searchKey.ClientId));
            if (clientItem == null)
            {
                return null;
            }
            List<Item> facilityItem = new List<Item>();
            BedBanksSettings bedBankData = new BedBanksSettings(clientItem);
            List<BedBanksFacility> bedBanksFacility = bedBankData.BedBanksFacility;
            if ((facilitites != null) && (bedBanksFacility != null))
            {
                List<BedBanksFacility> matchingFacilities = bedBanksFacility.Where(bbf => facilitites.Any(fac => int.Parse(bbf.FacilityId) == fac.FacilityID)).ToList<BedBanksFacility>();
                if (matchingFacilities != null)
                {
                    foreach (var matchingfacility in matchingFacilities)
                    {
                        facilityItem.Add(matchingfacility.SitecoreFacilityItem);
                    }
                }
            }
            return facilityItem;
        }


        protected void rptBBFacilities_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            Item fac = e.Item.DataItem as Item;
            ListItemType itemType = e.Item.ItemType;

            if (itemType == ListItemType.Item || itemType == ListItemType.AlternatingItem)
            {
                string url = string.Empty;
                if (fac != null)
                {
                    url = fac.Fields["Icon"].Value;
                }
                Image facImage = e.Item.FindControl("imgFac") as Image;
                if (!string.IsNullOrEmpty(url))
                {
                    facImage.ImageUrl = url;
                }
            }
        }
        private void BindBBStarRankings(string ranking)
        {
            ///Bind star rank
            if (!string.IsNullOrWhiteSpace(ranking))
            {
                decimal starRank;

                bool result = decimal.TryParse(ranking, out starRank);

                if (result)
                {
                    int count = Convert.ToInt32(starRank);
                    List<int> listOfStars = Enumerable.Repeat(count, count).ToList();
                    this.rptBBStarRepeater.DataSource = listOfStars;
                    this.rptBBStarRepeater.DataBind();
                }
                else
                {
                    Diagnostics.WriteException(DiagnosticsCategory.ClientPortal,
                        new AffinionException("Unable to parse star rank: BB star Rating"), this);
                }
            }
        }
    }
}