﻿using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Data.Items;
using Sitecore.Security.Accounts;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Profile;
using System.Web.Security;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Exceptions;
using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
using System.Web;


namespace Affinion.Loyaltybuild.Security.MemberShip
{
    internal class RoleHelper
    {
        private static Affinion.LoyaltyBuild.Communications.IEmailService emailService;

        public static bool ValidateSupplierPortalRole(string userName, string roleName, bool isValidUser)
        {

            return AuthenticationBasedOnRole(userName, roleName, isValidUser);
        }


        #region Supplier Portal


        /// <summary>
        /// authenticate supplier portal based on role
        /// </summary>
        private static bool AuthenticationBasedOnRole(string userName, string currentRole, bool isValidUser)
        {
            bool isSuccess = false;


            Sitecore.Data.Database context = Sitecore.Context.Database;


            switch (currentRole.ToLower().Trim())
            {
                case Constants.SupplierPortal.hotelRole:
                case Constants.SupplierPortal.hotelSupervisor:
                case Constants.SupplierPortal.grpHotelRole:
                    isSuccess = CheckProviderAuthentication(userName, currentRole, isValidUser);
                    break;
                case Constants.SupplierPortal.lbAdminRole:
                case Constants.SupplierPortal.superUser:
                case Constants.SupplierPortal.accountManagement:
                case Constants.SupplierPortal.marketing:
                case Constants.SupplierPortal.finance:
                case Constants.SupplierPortal.IT:
                    isSuccess = isValidUser && CheckAdminUserAuthentication(userName);
                    break;
                default:
                    break;
            }


            if (isSuccess)
            {
                string[] splitDomainUser;

                splitDomainUser = userName.Split('\\');
                AddItem(userName, splitDomainUser[1], UserProfileKeys.loggedInUserName);
                AddItem(userName, userName, UserProfileKeys.loggedinDomainUser);
                System.Web.Security.MembershipUser membershipUser = System.Web.Security.Membership.GetUser(userName);
                if (membershipUser != null)
                {
                    DateTime lastLoginDate = membershipUser.LastLoginDate;
                    string lastLoginDateTime = lastLoginDate.ToString(Constants.SupplierPortal.DateMonthFormat);
                    AddItem(userName, lastLoginDateTime, UserProfileKeys.lastLoggedInDate);

                }

                AddItem(userName, currentRole, UserProfileKeys.currentLoggedinUserRole);

            }
            return isSuccess;
        }



        /// <summary>
        /// check authentication to supplier portal for admin user
        /// </summary>
        private static bool CheckAdminUserAuthentication(string username)
        {
            AddItem(username, username, UserProfileKeys.loggedInUserAdmin);
            return true;
        }
        #endregion


        ///// <summary>
        ///// check authentication to supplier portal for hotel user
        ///// </summary>
        private static bool CheckProviderAuthentication(string username, string role, bool isValidUser)
        {
            Item parent = null;
            string[] splitDomainUser;

            splitDomainUser = username.Split('\\');
            bool isSuccess = false;
            Sitecore.Data.Database currentDb = Sitecore.Context.Database;
            String contactQuery = @"fast:/sitecore/content/#admin-portal#/#supplier-setup#//*[@@templateid='{75EEF5CF-7C81-45C2-AEDA-95FBF1E02768}']";
            Item[] contactItems = currentDb.SelectItems(contactQuery);
            if (splitDomainUser[1] != null)
            {
                Item selectedContact = contactItems
                    .Where(item => item.Fields["UserName"].Value != "" && item.Fields["UserName"].Value.ToLower().Equals(splitDomainUser[1], StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                if (selectedContact != null)
                {
                    Sitecore.Data.Fields.CheckboxField checkbox = selectedContact.Fields[Constants.SupplierPortal.isActive];
                    if (checkbox != null && checkbox.Checked)
                    {
                        if (role.Equals(Constants.SupplierPortal.grpHotelRole, StringComparison.InvariantCultureIgnoreCase))
                        {
                            parent = selectedContact.Parent.Parent;
                        }
                        else
                        {
                            parent = selectedContact.Parent.Parent.Parent;
                        }
                        if (parent != null)
                        {
                            Sitecore.Data.Fields.MultilistField multilistField = parent.Fields[Constants.SupplierPortal.contactsList];

                            if (multilistField != null && multilistField.TargetIDs.Contains(selectedContact.ID))
                            {

                                isSuccess = isValidUser;

                                if (isSuccess)
                                {

                                    AddItem(username, parent.DisplayName, UserProfileKeys.loggedInSupplier);
                                    AddItem(username, selectedContact.DisplayName, UserProfileKeys.loggedInContactDisplayName);
                                    AddItem(username, parent.ID.ToString(), UserProfileKeys.loggedInProviderId);
                                    AddItem(username, parent.DisplayName, UserProfileKeys.loggedinSupplierDisplayName);
                                    AddItem(username, selectedContact.ID.ToString(), UserProfileKeys.loggedInContactID);


                                }
                            }


                            if (!isSuccess)
                            {

                                SendEmail(parent, username);

                            }
                        }
                    }
                }
            }
            return isSuccess;

        }
        ///// <summary>
        ///// send email to provider
        ///// </summary>
        private static void SendEmail(Item parent, string username)
        {


            emailService = new Affinion.LoyaltyBuild.Communications.Services.EmailService();
            Item emailItem = Sitecore.Context.Database.SelectSingleItem("/sitecore/content/admin-portal/global/_supporting-content/provider-email/provider-email");
            string providerName = SitecoreFieldsHelper.GetValue(parent, "Name");
            string prividerId = SitecoreFieldsHelper.GetValue(parent, "ID");
            string prividerLocation = SitecoreFieldsHelper.GetDropLinkFieldValue(parent, "Location", "__display name");

            string emailTitle = SitecoreFieldsHelper.GetValue(emailItem, "Mail Title");
            string emailBody = SitecoreFieldsHelper.GetValue(emailItem, "Mail Body");
            string emailSenderID = SitecoreFieldsHelper.GetValue(emailItem, "Sender ID");
            string emailReceiverID = SitecoreFieldsHelper.GetValue(emailItem, "Receiver ID");
            //string lockedMailBody = SitecoreFieldsHelper.GetValue(emailItem, "User Locked Mail Body");
            try
            {
                //List<bool> loginSuccess = uCommerceConnectionFactory.SelectLoginAuditHistory(username);
                //if (providerName != "" && prividerId != "" && prividerLocation != "")
                //{
                if (!String.IsNullOrWhiteSpace(emailTitle) && !String.IsNullOrWhiteSpace(emailBody) && !String.IsNullOrWhiteSpace(emailSenderID) && !String.IsNullOrWhiteSpace(emailReceiverID))
                {
                    string invalidAttemptCount = GetInvalidLoginAttempts(username).ToString();



                    emailBody = emailBody.Replace("{ProviderName}", providerName);
                    emailBody = emailBody.Replace("{ProviderID}", prividerId);
                    emailBody = emailBody.Replace("{Provider Location}", prividerLocation);


                    if (invalidAttemptCount.Equals("1"))
                    {
                        AddItem(username, GetInvalidLoginAttemptTime(username, invalidAttemptCount).ToString("HH:mm"), UserProfileKeys.InvalidAttemptTime1);
                    }
                    else if (invalidAttemptCount.Equals("2"))
                    {
                        AddItem(username, GetInvalidLoginAttemptTime(username, invalidAttemptCount).ToString("HH:mm"), UserProfileKeys.InvalidAttemptTime2);
                    }
                    else if (invalidAttemptCount.Equals("3"))
                    {
                        string invalidLoginAttemptTime1 = GetItem(username, UserProfileKeys.InvalidAttemptTime1);
                        string invalidLoginAttemptTime2 = GetItem(username, UserProfileKeys.InvalidAttemptTime2);
                        string invalidLoginAttemptTime3 = GetInvalidLoginAttemptTime(username, invalidAttemptCount).ToString("HH:mm");

                        emailBody = emailBody.Replace("{1}", invalidLoginAttemptTime1);
                        emailBody = emailBody.Replace("{2}", invalidLoginAttemptTime2);
                        emailBody = emailBody.Replace("{3}", invalidLoginAttemptTime3);
                        emailService.Send(emailSenderID, emailReceiverID, emailTitle, emailBody, true);
                        ClearCustomProperty(username,UserProfileKeys.InvalidAttemptTime1);
                        ClearCustomProperty(username, UserProfileKeys.InvalidAttemptTime2);                      

                    }

                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.SupplierPortal, ex, username);
            }

        }


        ///// <summary>
        ///// failed login attempt count
        ///// </summary>
        private static int GetInvalidLoginAttempts(string username)
        {
            try
            {
                int invalidLoginAttemptCount = 0;
                MembershipUser userInfo = Membership.GetUser(username);
                if (userInfo != null && userInfo.ProviderUserKey != null)
                {

                    string userId = userInfo.ProviderUserKey.ToString();
                    invalidLoginAttemptCount = uCommerceConnectionFactory.GetInvalidPasswordAttemptCount(userId);
                    return invalidLoginAttemptCount;
                }
                return invalidLoginAttemptCount;

            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        private static DateTime GetInvalidLoginAttemptTime(string username, string count)
        {
            try
            {
                DateTime invalidLoginAttemptTime = DateTime.Now;
                MembershipUser userInfo = Membership.GetUser(username);
                if (userInfo != null && userInfo.ProviderUserKey != null)
                {

                    string userId = userInfo.ProviderUserKey.ToString();
                    invalidLoginAttemptTime = uCommerceConnectionFactory.GetInvalidPasswordAttemptTime(userId, count);
                    return invalidLoginAttemptTime;
                }
                return invalidLoginAttemptTime;

            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }

        }


        /// <summary>
        /// add objects to sitecore user profile
        /// </summary>
        private static void AddItem(string userName, string objectToSession, string profileName)
        {
            User user = User.FromName(userName, true);
            user.Profile.SetCustomProperty(profileName, objectToSession);
            user.Profile.Save();

        }
        /// <summary>
        /// GetItem
        /// </summary>
        /// <param name="profileName"></param>
        /// <returns></returns>
        public static string GetItem(string userName, string profileName)
        {
            try
            {
                User user = User.FromName(userName, true);
                return user.Profile.GetCustomProperty(profileName);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Clear all custom property
        /// </summary>
        public static void ClearCustomProperty(string userName, string profileName)
        {
            User user = User.FromName(userName, true);
            user.Profile.RemoveCustomProperty(profileName);
            user.Profile.Save();
        }
    }




    public abstract class UserProfileKeys
    {

        public const string loggedInSupplier = "supplierName";
        public const string loggedInUserName = "UserName";
        public const string lastLoggedInDate = "LastLoggedInDate";
        public const string currentLoggedinUserRole = "LoggedInUserRole";
        public const string loggedInContactDisplayName = "LoggedInContactDisplayName";
        public const string loggedinSupplierDisplayName = "loggedinSupplierDisplayName";
        public const string loggedinDomainUser = "loggedinDomainUser";
        public const string loggedInUserAdmin = "loggedInUserAdmin";
        public const string loggedInProviderId = "loggedInProviderId";
        public const string loggedInContactID = "loggedInContactID";
        public const string InvalidAttemptTime1 = "InvalidAttemptTime1";
        public const string InvalidAttemptTime2 = "InvalidAttemptTime2";


    }
}
