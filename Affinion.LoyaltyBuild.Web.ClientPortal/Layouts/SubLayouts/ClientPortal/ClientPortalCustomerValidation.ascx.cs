﻿namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal
{
    using System;
    using Affinion.Loyaltybuild.BusinessLogic.Helper;
    using Affinion.LoyaltyBuild.Common;
    using Affinion.LoyaltyBuild.Common.Exceptions;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.SessionHelper;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Communications.ExceptionServices;
    using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Web.Security;
    using Affinion.LoyaltyBuild.UCom.Common.Constants;
    using helper = Affinion.Loyaltybuild.BusinessLogic.Helper;

    public partial class ClientPortalCustomerValidation : BaseSublayout
    {
        ExceptionMailHelper ES = new ExceptionMailHelper();
        Sitecore.Data.Database currentDb = Sitecore.Context.Database;


        //regular expressions
        Regex emailValidator = new Regex(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
        private Item clientItem = SitecoreFieldsHelper.GetLookupFieldTargetItem(ItemHelper.RootItem, "ClientDetails");

        protected string RequiredFieldMessage { get; set; }
        // protected string EmailValidationFieldMessage { get; set; }
        // protected string EmailMismatchMessage { get; set; }
        public Item ValidateContinueToDetailItem { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            BindSitecoreErrorMessages();

            // Put user code to initialize the page here
            try
            {
                //  LabelAccomodationValidation.Text = string.Empty;
                Item item = this.GetDataSource();

                if (!UCommerce.Api.TransactionLibrary.HasBasket())
                {

                    ATGValidation.Visible = false;
                }

                InitializeFields(item);
            }
            catch (Exception exception)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, exception.Message);
                throw;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ContinueToDetailButton_Click(object sender, EventArgs e)
        {
            Item item = this.GetDataSource();
            try
            {

                ValidateFields(item);

            }
            catch (Exception exception)
            {
                //Log 
                mailGenarate();
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, exception, this);
                throw new AffinionException("error in continuing to personal details", exception.InnerException);

            }
        }



        /// <summary>
        /// Generate Email on validation 
        /// </summary>
        private void mailGenarate()
        {
            try
            {
                //Database currentDB = Sitecore.Context.Database;
                string ValidateContinueToDetailItemPath = string.Concat("/sitecore/content/client-portal/", clientItem.Name, "/global/_supporting-content/validation-email");
                ValidateContinueToDetailItem = currentDb.GetItem(ValidateContinueToDetailItemPath).Children.FirstOrDefault(i => i.TemplateName == "GeneralEmail");
                Affinion.LoyaltyBuild.Communications.IEmailService emailService = new Affinion.LoyaltyBuild.Communications.Services.EmailService();

                string senderID = SitecoreFieldsHelper.GetValue(ValidateContinueToDetailItem, "SenderEmail");
                string mailSubject = SitecoreFieldsHelper.GetValue(ValidateContinueToDetailItem, "Subject");
                string msgBody = SitecoreFieldsHelper.GetValue(ValidateContinueToDetailItem, "EmailBody");
                string receiver = SitecoreFieldsHelper.GetValue(ValidateContinueToDetailItem, "ReceiverEmail");
                if (senderID == null || receiver == null)
                {
                    return;
                }
                emailService.Send(senderID, receiver, mailSubject, msgBody, true);
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, exception, this);
            }

        }
        /// <summary>
        /// Validate fields 
        /// </summary>
        /// <param name="item">datasource item</param>
        private void ValidateFields(Item item)
        {
            Item conditionalMessageItem = this.GetItemFromRenderingParameterPath("Conditional Messages Link");
            string email = EmailAddress.Text;
            string confirmEmail = ConfirmEmailAddress.Text;

            if (Page.IsValid)
            {
                EmailAddress.CssClass = "form-control";
                ConfirmEmailAddress.CssClass = "form-control";
                Sitecore.Events.Event.RaiseEvent("lb:ClientValidationSuccess", email);
                bool isATG = SitecoreFieldsHelper.GetBoolean(item, Constants.IsATGFieldName);

                //if (isATG)
                //{
                //    HandleMailLists(email);
                //}

            }

        }

        /// <summary>
        /// Validate the accomodation page data
        /// </summary>
        /// <returns>return true if validation successfull</returns>
        private bool ValidateAccomodation(Item item)
        {
            try
            {
                int orderID = BasketHelper.GetBasketOrderId();
                DataSet ordeLineIDs = uCommerceConnectionFactory.GetOrderLines(orderID);

                for (int i = 0; i < ordeLineIDs.Tables[0].Rows.Count; i++)
                {
                    string orderLineID = ordeLineIDs.Tables[0].Rows[i]["OrderLineId"].ToString();
                    DataSet reservationCount = uCommerceConnectionFactory.GetAccomodationDetailsByOrderLineId(orderLineID);
                    string guestName = reservationCount.Tables[0].Rows[0]["GuestName"].ToString();
                    if (guestName.Length <= 0)
                    {
                        //LabelAccomodationValidation.Text = (item != null && item.Fields["Accommodation Details Required Message"] != null) ? item.Fields["Accommodation Details Required Message"].Value : string.Empty;
                        return false;
                    }
                }
                return true;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException("Error in validating accomadation", exception.InnerException);
            }
        }
        private void BindSitecoreErrorMessages()
        {
            Item item = this.GetItemFromRenderingParameterPath("Conditional Messages Link");


            Item currentPageItem = Sitecore.Context.Item;
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "TextValidator", ValidatorText);
            SitecoreFieldsHelper.BindSitecoreText(item, "Required Field Message", RequiredFielsMessage1);
            SitecoreFieldsHelper.BindSitecoreText(item, "Required Field Message", RequiredFielsMessage2);
            SitecoreFieldsHelper.BindSitecoreText(item, "Email Mismatch Message", EmailMismatchMessage);
            SitecoreFieldsHelper.BindSitecoreText(item, "Email Validation Message", EmailValidationFieldMessage);



        }


        private void HandleMailLists(string email)
        {
            Item rootItem = Sitecore.Context.Database.GetItem(Sitecore.Context.Site.RootPath);

            try
            {
                if (EmailList1ConsentCheckBox.Checked && EmailList2ConsentCheckBox.Checked)
                {
                    uCommerceConnectionFactory.AddSubscribedCustomer(GetClientId(), CustomerId(), EmailAddress.Text, Constants.ServiceAndPromotional);
                }

                else if (EmailList1ConsentCheckBox.Checked)
                {
                    uCommerceConnectionFactory.AddSubscribedCustomer(GetClientId(), CustomerId(), EmailAddress.Text, Constants.PromotionalMail);
                }
                else if (EmailList2ConsentCheckBox.Checked)
                {
                    uCommerceConnectionFactory.AddSubscribedCustomer(GetClientId(), CustomerId(), EmailAddress.Text, Constants.ServiceImprovement);
                }
            }
            catch (ArgumentException exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, exception, this);
            }
        }


        /// <summary>
        /// Get the ClientId
        /// </summary>
        private string GetClientId()
        {
            Item clientItem = SitecoreFieldsHelper.GetLookupFieldTargetItem(ItemHelper.RootItem, Constants.ClientDetailsFieldName);
            return clientItem.ID.Guid.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static int CustomerId()
        {
            int UserId = 68;
            return UserId;
        }



        private void InitializeFields(Item item)
        {
            if (item != null)
            {

                this.EmailTitle.Item = item;
                this.ConfirmEmailTitle.Item = item;

                this.ContinueToDetailButton.Text = item.Fields["ButtonTitle"].Value;


                this.RequiredFieldIndicator.Item = item;

                bool isATG = SitecoreFieldsHelper.GetBoolean(item, Constants.IsATGFieldName);

                if (isATG)
                {
                    this.EmailList1ConsentLabel.Item = item;
                    this.EmailList2ConsentLabel.Item = item;
                    this.InstructionText.Item = item;
                    this.EmailValidationTitleText.Item = item;
                }
                else
                {
                    this.FirstDescription.Item = item;
                    this.SecondDescription.Item = item;
                    this.ThirdDescription.Item = item;
                }

                SetControlVisibility(isATG);


            }
        }

        /// <summary>
        /// Set control visibility according to configurations
        /// </summary>
        /// <param name="isATG">Whether the client is ATG</param>
        private void SetControlVisibility(bool isATG)
        {
            if (isATG)
            {
                FirstDescriptionDiv.Visible = false;
                SecondDescriptionDiv.Visible = false;
                ThirdDescriptionDiv.Visible = false;
            }
            else
            {
                EmailList1ConsentCheckBox.Visible = false;
                EmailList2ConsentCheckBox.Visible = false;
                EmailListConsent1Div.Visible = false;
                EmailListConsent2Div.Visible = false;
            }


        }

    }
}
