﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditClientSubRate.ascx.cs" Inherits="Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.Ucommerce.Marketing.EditClientSubRate" %>

<asp:Panel runat="server" ID="PropertyPanel">

    <div>
        <asp:ScriptManager runat="server" ID="smClientRate">
        </asp:ScriptManager>
        <asp:DropDownList ID="drpClient" runat="server" OnSelectedIndexChanged="drpClient_SelectedIndexChanged" AutoPostBack="true">
        </asp:DropDownList>

        <asp:UpdatePanel ID="clientRate" runat="server">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="drpClient" EventName="SelectedIndexChanged" />
            </Triggers>
            <ContentTemplate>
                <asp:ListBox ID="lstclientRate" runat="server"></asp:ListBox>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

</asp:Panel>
