﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           LocationTrackerWebService.asmx.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.Tests.WebServices
/// Description:           Contains the web methods and script methods.
/// 

#region Directives

//using Affinion.LoyaltyBuild.Common.Wizard;
using Affinion.LoyaltyBuild.Common.Exceptions;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.ExternalServices.LocationService.DataObjects;
using Affinion.LoyaltyBuild.ExternalServices.LocationService.Services;
using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;

#endregion Directives

namespace Affinion.LoyaltyBuild.Web.Tests.WebServices
{
    /// <summary>
    /// Summary description for LocationTrackerWebService
    /// </summary>
    [WebService(Namespace = "http://AffinionTesting/WebServices/LocationTrackerWebService.asmx")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    [Serializable]
    public class LocationTrackerWebService : System.Web.Services.WebService
    {
        /// <summary>
        /// Get the geo locations of the user selected location
        /// </summary>
        /// <param name="location">location</param>
        /// <returns>serialized string</returns>
        [WebMethod, ScriptMethod]
        public string GetSelectedLocation(string location)
        {
            //Initializing 
            GoogleLocationService googleLocationService = new GoogleLocationService();
            try
            {
                List<GoogleLocationData> locationList = googleLocationService.GetLocations(location);
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                //Serialize the locationlist
                string serializedList = serializer.Serialize(locationList);
                return serializedList;
            }

            catch (AffinionException exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ExternalServices, exception, this);
                throw;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ExternalServices, exception, this);
                throw;
            }

        }

        /*
        /// <summary>
        /// store the wizard data to DictionaryData via ajax call
        /// </summary>
        /// <param name="jsonData"></param>
        /// <returns>return string message to caller to get the status (updated, failed. success)</returns>
        [WebMethod]
        public static string ProcessAjax(string jsonData)
        {
            // create a singleton object
            DataTransfer dataTransfer = DataTransfer.Instance;
            string result = string.Empty;
            //set the Json string property with request data
            dataTransfer.JSonString = jsonData;

            try
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientSetup, "wizard module Ajax call from sublayout to webconnect started");

                if (jsonData != string.Empty)
                {
                    //{string[3]}
                    //[0]: "twitter:ttt"
                    //[1]: "facebook:fff"
                    //[2]: "gplus:ggg"

                    // Split the string using "," and will return set of key value pair
                    var KeyValPair = jsonData.Replace('{', ' ').Replace('}', ' ').Replace("'", " ").Replace(" ", "").Split(',');
                    foreach (var item in KeyValPair)
                    {
                        // split the item using ":" to get key and value and store them into DictionaryData      

                        if (dataTransfer.DictionaryData.ContainsKey(item.Split(':')[0]))
                        {
                            dataTransfer.DictionaryData[item.Split(':')[0]] = item.Split(':')[1];
                            result = "Value Updated";
                        }
                        else
                        {
                            dataTransfer.DictionaryData.Add(item.Split(':')[0], item.Split(':')[1]);
                            result = "Success";
                        }

                        Diagnostics.Trace(DiagnosticsCategory.ClientSetup, "wizard module Ajax call from sublayout to webconnect completed");
                    }
                }
                else
                    result = "Fail";

            }
            catch (ArgumentException ex)
            {
                ///Catch argument exceptions
                ///First parameter : Module
                ///Second parameter: Exception
                ///Third parameter : Object logging
                Diagnostics.WriteException(DiagnosticsCategory.ClientSetup, ex, dataTransfer);
            }
            catch (AffinionException ex)
            {
                ///Catch affinion exceptions
                Diagnostics.WriteException(DiagnosticsCategory.ClientSetup, ex, dataTransfer);
            }
            catch (Exception ex)
            {
                ///Catch general exceptions
                Diagnostics.WriteException(DiagnosticsCategory.ClientSetup, ex, dataTransfer);
            }
            return result;
        }

        */
    }
}
