﻿CREATE TABLE [dbo].[uCommerce_PaymentMethodDescription] (
    [PaymentMethodDescriptionId] INT            IDENTITY (1, 1) NOT NULL,
    [PaymentMethodId]            INT            NOT NULL,
    [CultureCode]                NVARCHAR (60)  NOT NULL,
    [DisplayName]                NVARCHAR (512) NOT NULL,
    [Description]                NTEXT          NULL,
    CONSTRAINT [uCommerce_PK_PaymentMethodDescription] PRIMARY KEY CLUSTERED ([PaymentMethodDescriptionId] ASC),
    CONSTRAINT [uCommerce_FK_PaymentMethodDescription_PaymentMethod] FOREIGN KEY ([PaymentMethodId]) REFERENCES [dbo].[uCommerce_PaymentMethod] ([PaymentMethodId])
);

