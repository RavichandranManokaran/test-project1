﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SearchHelper.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common.Search
/// Description:           Stores the data for offers
/// </summary>
namespace Affinion.LoyaltyBuild.Common.Search
{
    #region Using Statements

    using System.Collections.ObjectModel;

    #endregion

    /// <summary>
    /// Stores the data for offers
    /// </summary>
    public class SupplierData
    {
        #region Properties



        public string ClientId { get; set; }

        public string ClientName { get; set; }

        public Collection<OfferDataItem> Offers { get; set; }

        public Collection<AddonProduct> AddonProducts { get; set; }

        public decimal LowestPrice { get; set; }

        public string LowestPriceWithCurrency { get; set; }

        public int LowestPriceCurrencyId { get; set; }

        public string LowestPriceOccupancyTypeId { get; set; }

        public string LowestPriceOccupancyTypeName { get; set; }

       

        #endregion

        public SupplierData()
        {
            this.Offers = new Collection<OfferDataItem>();
            this.AddonProducts = new Collection<AddonProduct>();
        }

        
    }
}
