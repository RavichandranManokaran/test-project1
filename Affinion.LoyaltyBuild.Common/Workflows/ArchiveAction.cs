﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           ArchiveAction.cs
/// Sub-system/Module:     Common(VS project name)
/// Description:           ArchiveAction for uCommerce Initalization
/// </summary>

#region Using Directives
using Sitecore.Data;
using System;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
#endregion

namespace Affinion.LoyaltyBuild.Common.Workflows
{
    /// <summary>
    /// Use to archive selected item via WF
    /// </summary>
    public class ArchiveAction
    {
        /// <summary>
        /// Archive the item
        /// </summary>
        /// <param name="args">WorkflowPipelineArgs object</param>
        public void Process(Sitecore.Workflows.Simple.WorkflowPipelineArgs args)
        {
            try
            {
                //Sitecore.Data.Items.Item item = Sitecore.Context.Item;

                Sitecore.Diagnostics.Assert.IsNotNull(args.DataItem, "item");

                // Get the archive from master database
                Sitecore.Data.Archiving.Archive archive = Sitecore.Data.Archiving.ArchiveManager.GetArchive("archive", args.DataItem.Database);

                using (new Sitecore.Security.Accounts.UserSwitcher(ItemHelper.ContentEditingUser))
                {

                    if (archive != null)
                    {
                        // archive the item
                        // to archive an individual version instead: archive.ArchiveVersion(child);
                        archive.ArchiveItem(args.DataItem);
                    }
                    else
                    {
                        // recycle the item
                        // no need to check settings and existence of archive
                        args.DataItem.Recycle();
                    } 
                }

            }
            catch (Exception e)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, e, null);
                throw;
            }
        }
    }
}
