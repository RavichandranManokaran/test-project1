﻿<%@ control language="C#" autoeventwireup="true" codebehind="OfflinePortalCancelBookingSublayout.ascx.cs" inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalCancelBookingSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<div class="col-md-12">
    <div class="accordian-outer">
        <div class="panel-group" id="accordion">
            <div class="panel panel-default">
                <div class="panel-heading bg-primary">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse0" class="show-hide-list"><sc:text id="CancelBookingText" runat="server"/></a>
                    </h4>
                </div>
                <div id="collapse0" class="panel-collapse collapse in">
                    <div class="panel-body alert-info">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel panel-default">
                                    <div>
                                        <div class="panel-heading bg-primary">
                                            <h4 class="panel-title"><sc:text id="BookingIdText" runat="server"/> 
                                                <asp:label runat="server" id="lblBookingReference"></asp:label>
                                                <asp:label runat="server" id="lbCancelToken"></asp:label>
                                            </h4>
                                        </div>
                                        <div class="panel-body alert-info">
                                            <div class="margin-row">
                                                <h4><sc:text id="txtCancellationCost" runat="server"/>
                                                <asp:label runat="server" id="lblCancellationCost"></asp:label>
                                                    </h4>
                                            </div>
                                            <div class="margin-row">
                                                <h4><sc:text id="txtRefundAmount" runat="server"/>
                                                <asp:label runat="server" id="lblRefundAmount"></asp:label>
                                                    </h4>
                                            </div>
                                            <div class="margin-row">
                                                <h4><sc:text id="NoteToProviderText" runat="server"/>
                                                <textarea id="ta_NoteToProvider" runat="server" class="form-control" rows="5"></textarea>
                                                     <asp:CustomValidator id="noteToProvider" ControlToValidate="ta_NoteToProvider" ClientValidationFunction="Cardvalidate" Display="Dynamic"  ForeColor="Red" Font-Name="verdana"  Font-Size="10pt" runat="server"><sc:text id="txtnoteToProviderText" runat="server" /></asp:CustomValidator>
                                                    </h4>
                                            </div>
                                            <div class="margin-row">
                                                <h4><sc:text id="InternalNoteText" runat="server"/></h4>
                                                <textarea id="ta_CancelReason" runat="server" class="form-control" rows="5"></textarea>
                                                <asp:CustomValidator id="cancelReason" ControlToValidate="ta_CancelReason" ClientValidationFunction="Cardvalidate" Display="Dynamic" ForeColor="Red" Font-Name="verdana"  Font-Size="10pt" runat="server"><sc:text id="txtcancelreasonText" runat="server" /></asp:CustomValidator>

                                            </div>
                                            <div class="margin-row">
                                             <%--  <asp:button type="button" cssclass="btn btn-default" data-dismiss="modal" id="CompleteCancel" runat="server" onclick="CompleteCancel_Click"> </asp:button>--%>
                                                 <button id="CompleteCancel" runat="server" OnServerclick="CompleteCancel_Click" class="btn btn-default"  data-dismiss="modal">
                                                      <sc:text id="CompleteCancellationText"  runat="server"/>
                                                      </button>
                                              
                                              
                                                <a class="btn btn-default" href="<%=GetUrl("/existingbooking")%>"> <sc:text id="CancelCancellationText"  runat="server"/></a>

                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="divErrorMsg" runat="server" class="alert alert-danger" visible ="false">
    <p>Dont Give Personnel Information</p>
</div>
<script type="text/javascript">
    function Cardvalidate(source, arguments) {
        var pattern = /(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})/;
        arguments.IsValid = !pattern.test(arguments.Value);
    }
<%if(ValidationResponse != null){%> 
    jQuery(document).ready(function () {
        CS.Common.ShowAlert('<%= ValidationResponse.Message %>', '', null, {
            "Ok": function () {
                <%if(ValidationResponse.IsSuccess){%>
                window.location = '<%= GetUrl("/bookingdetails?olid=" + QueryString("olid")) %>'
                <%}else{%>
                $(this).dialog("close");
                <%}%>
            }
        });
    });
<%}%>
</script>
