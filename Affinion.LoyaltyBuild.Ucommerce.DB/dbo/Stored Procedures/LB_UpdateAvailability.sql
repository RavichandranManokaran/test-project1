﻿CREATE PROCEDURE [dbo].[LB_UpdateAvailability]
(@OrderLineId INT,
@CheckInDateCount INT=0, 
@CheckOutDateCount INT=0,
@Sku NVARCHAR(512)=NULL,
@VariantSku NVARCHAR(512)=NULL,
@IsCancel BIT = 0)
AS		
		
		DECLARE @Count INT
		DECLARE @CampaignItemID INT
		SET @Count = CASE WHEN @IsCancel= 0 THEN +1 ELSE -1 END

		IF @CheckInDateCount = 0
		BEGIN
			SELECT 	
				@CheckInDateCount = dbo.GetCounterByDate(CheckInDate),
				@CheckOutDateCount = dbo.GetCounterByDate(CheckOutDate) 
			FROM 
				LB_BookingInfo 
			WHERE
				OrderLineId = @OrderLineId

			SELECT 	
				@Sku = Sku,
				@VariantSku = VariantSku 
			FROM 
				uCommerce_OrderLine 
			WHERE
				OrderLineId = @OrderLineId
		END

		SELECT TOP 1 @CampaignItemID = CampaignItemId from ucommerce_orderlinecampaignrelation where OrderLineId = @OrderLineId
		
		declare @temprelated as table(varaiantsku1 varchar(100), varaiantsku2 varchar(100))

		insert into @temprelated
		SELECT A.VariantSku,  
			Split.a.value('.', 'VARCHAR(100)') AS Data  
		FROM  
		(
		select p.VariantSku,  CAST ('<M>' + REPLACE(pp.Value, ',', '</M><M>') + '</M>' AS XML) Data from 
		uCommerce_Product p 
		inner join ucommerce_productproperty pp on p.ProductId = pp.ProductId
		inner join uCommerce_ProductDefinitionField pdf on pp.ProductDefinitionFieldId = pdf.ProductDefinitionFieldId
		where pdf.Name  in ('RelatedAddOnSKU','RelatedSKU') and isnull(pp.Value,'') <> ''
		and p.Sku = (select sku from uCommerce_OrderLine where OrderLineId = @OrderLineId) 
		) AS A CROSS APPLY Data.nodes ('/M') AS Split(a); 
		
	WHILE (@CheckInDateCount < @CheckOutDateCount)
	BEGIN

	UPDATE [LB_RoomAvailability] SET NumberBooked = NumberBooked + @Count WHERE DateCounter = @CheckInDateCount AND Sku = @Sku AND VariantSku = @VariantSku

		UPDATE [LB_RoomAvailability] SET NumberBooked = NumberBooked + @Count WHERE DateCounter = @CheckInDateCount AND Sku = @Sku AND VariantSku in 
		(
			select varaiantsku1 from @temprelated
			where varaiantsku2 = @variantsku
			union 			
			select varaiantsku2 from @temprelated
			where varaiantsku1 = @variantsku
		)

			IF @CampaignItemID IS NOT NULL
			BEGIN
				--Update Offer Availability table 			
				UPDATE LB_OfferAvailability SET NumberBooked = NumberBooked + @Count WHERE DateCounter = @CheckInDateCount AND Sku = @Sku AND VariantSku = @VariantSku

				UPDATE LB_OfferAvailability SET NumberBooked = NumberBooked + @Count WHERE DateCounter = @CheckInDateCount AND Sku = @Sku AND VariantSku in 
				(
					select varaiantsku1 from @temprelated
					where varaiantsku2 = @variantsku
					union 			
					select varaiantsku2 from @temprelated
					where varaiantsku1 = @variantsku
				)
			END	

		SET @CheckInDateCount = @CheckInDateCount + 1
	END
