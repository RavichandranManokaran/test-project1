﻿CREATE PROCEDURE [dbo].[uCommerce_BookingCounter]
as

begin

-- for today booking(s)

select ca.clientid as ClientId,count(ol.OrderLineId)as 'Booking(s)' from uCommerce_CustomerAttribute as ca

inner join

uCommerce_Customer as c on ca.CustomerId=c.CustomerId

inner join 

uCommerce_PurchaseOrder as po on c.CustomerId=po.CustomerId

inner join

uCommerce_OrderLine as ol on po.OrderId=ol.OrderId 

where CAST(ol.CreatedOn AS DATE)=CAST(getdate()as date)

group by ca.clientid

-- for yesterday booking(s)

select ca.clientid as ClientId,count(ol.OrderLineId)as 'Booking(s)' from uCommerce_CustomerAttribute as ca

inner join

uCommerce_Customer as c on ca.CustomerId=c.CustomerId

inner join 

uCommerce_PurchaseOrder as po on c.CustomerId=po.CustomerId

inner join

uCommerce_OrderLine as ol on po.OrderId=ol.OrderId 

where CAST(ol.CreatedOn AS DATE)=CAST(getdate()-1 as date)

group by ca.clientid

-- for week booking(s)

select ca.clientid as ClientId,count(ol.OrderLineId)as 'Booking(s)' from uCommerce_CustomerAttribute as ca

inner join

uCommerce_Customer as c on ca.CustomerId=c.CustomerId

inner join 

uCommerce_PurchaseOrder as po on c.CustomerId=po.CustomerId

inner join

uCommerce_OrderLine as ol on po.OrderId=ol.OrderId 

where CAST(ol.CreatedOn AS DATE) between DATEADD(wk, DATEDIFF(wk, 6, GETDATE()), 6) and getdate()

group by ca.clientid

-- for month booking(s)

select ca.clientid as ClientId,count(ol.OrderLineId)as 'Booking(s)' from uCommerce_CustomerAttribute as ca

inner join

uCommerce_Customer as c on ca.CustomerId=c.CustomerId

inner join 

uCommerce_PurchaseOrder as po on c.CustomerId=po.CustomerId

inner join

uCommerce_OrderLine as ol on po.OrderId=ol.OrderId 

where CAST(ol.CreatedOn AS DATE) between DATEADD(DAY, 1-DAY(GETDATE()), DATEDIFF(DAY, 0, GETDATE()))

and getdate()

group by ca.clientid

-- for year booking(s)

select ca.clientid as ClientId,count(ol.OrderLineId)as 'Booking(s)' from uCommerce_CustomerAttribute as ca

inner join

uCommerce_Customer as c on ca.CustomerId=c.CustomerId

inner join 

uCommerce_PurchaseOrder as po on c.CustomerId=po.CustomerId

inner join

uCommerce_OrderLine as ol on po.OrderId=ol.OrderId 

where CAST(ol.CreatedOn AS DATE) between   DATEADD(yy, DATEDIFF(yy,0,getdate()), 0) 

and DATEADD(yy, DATEDIFF(yy,0,getdate()) + 1, -1) 

group by ca.clientid

end

