﻿Create PROCEDURE [dbo].[LB_AddGuestDetails]
	@OrderLineId as int,
	@NoOfGuest as int,
	@NoOfAdult as int,
	@NoOfChildren as int,
	@GuestName as varchar(50),
	@SpecialRequest as varchar(max)

AS
BEGIN
IF EXISTS (SELECT 1 FROM dbo.uCommerce_OrderProperty WHERE OrderLineId = @OrderLineId) --and RoomReservationId = @RoomReservationId)
	BEGIN

	  Declare @OrderId int,
			  @BookingId varchar(max),
			  @NoOfChild int,
			  @GuestNumber varchar(50),
			  @AdultNumber varchar(50),
			  @Name varchar(50),
			  @SpecialNote varchar(max)

	  
	  Select @OrderId = (select OrderId from uCommerce_OrderLine where OrderLineId=@OrderLineId)
	  select @BookingId = (select BookingId from LB_BookingInfo where OrderLineId=@OrderLineId)

	  select @GuestNumber= ( select [Value] from uCommerce_OrderProperty where OrderLineId=@OrderLineId and [Key]='_NoOfGuest')
	  select @AdultNumber= ( select [Value] from uCommerce_OrderProperty where OrderLineId=@OrderLineId and [Key]='_NoOfAdult')
	  select @Name= ( select [Value] from uCommerce_OrderProperty where OrderLineId=@OrderLineId and [Key]='_GuestName')
	  select @SpecialNote = ( select Note from LB_ReservationNotes where OrderLineId=@OrderLineId and NoteTypeId=3)
	  

	  if(@GuestNumber is null)
		Begin
			Insert into [uCommerce_OrderProperty] ( OrderId ,OrderLineId ,[Key], [Value]) values ( @OrderId , @OrderLineId ,'_NoOfGuest' , @NoOfGuest)
		End
	  Else
		Begin
			Update [uCommerce_OrderProperty] set [value]=@NoOfGuest where OrderLineId=@OrderLineId and [key]='_NoOfGuest'
		End

	  if(@AdultNumber is null)
		Begin
			Insert into [uCommerce_OrderProperty] ( OrderId ,OrderLineId ,[Key], [Value]) values ( @OrderId , @OrderLineId ,'_NoOfAdult' , @NoOfAdult)
		End
	  Else
		Begin
			Update [uCommerce_OrderProperty] set [value]=@NoOfAdult where OrderLineId=@OrderLineId and [key]='_NoOfAdult'
		End

	  if(@Name is null)
		Begin
			Insert into [uCommerce_OrderProperty] ( OrderId ,OrderLineId ,[Key], [Value]) values ( @OrderId , @OrderLineId ,'_GuestName' , @GuestName)
		End
	  Else
		Begin
			Update [uCommerce_OrderProperty] set [value]=@GuestName where OrderLineId=@OrderLineId and [key]='_GuestName'
		End
	  
	  Update [uCommerce_OrderProperty] set [value]=@NoOfChildren where OrderLineId=@OrderLineId and [key]='_NoOfChildren' 

	  if(@SpecialNote is null)
		Begin
			Insert into [LB_ReservationNotes] ( NoteTypeId , OrderLineId, BookingId, Note , CreatedDate, CreatedBy) values (3, @OrderLineId , @BookingId , @SpecialRequest, GETDATE() , '' )
		END
	  Else
		Begin
			Update LB_ReservationNotes set Note=@SpecialRequest where OrderLineId=@OrderLineId and NoteTypeId=3
		END
	END
END