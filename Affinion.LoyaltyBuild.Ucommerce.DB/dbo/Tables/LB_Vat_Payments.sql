CREATE TABLE [dbo].[LB_Vat_Payments](
	[SupplierVatPaymentId] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [int] NOT NULL,
	[OrderLineId] [int] NOT NULL,
	[OrderDate] [datetime] NOT NULL,
	[SupplierId] [int] NULL,
	[CampaignId] [varchar](50) NULL,
	[ClientId] [varchar](50) NOT NULL,
	[NominalCode] [varchar](50) NULL,
	[CampaignAccountingId] [varchar](20) NULL,
	[OrderCurrencyId] [int] NOT NULL,
	[OrderCurrencyIsoCode] [varchar](5) NOT NULL,
	[EuroRate] [decimal](11, 2) NULL,
	[CustomerId] [int] NOT NULL,
	[GrossAmount] [decimal](11, 2) NOT NULL,
	[SuggestedVatAmount] [decimal](11, 2) NOT NULL,
	[NetAmount] [decimal](11, 2) NOT NULL,
	[PostedVatAmount] [decimal](11, 2) NULL,
	[IsPaidToAccounts] [tinyint] NULL,
	[DatePostedToOracle] [datetime] NULL,
	[OracleIdForVat] [varchar](50) NULL,
	[OracleFileId] [int] NULL,
	[CustomerAddressCode] [nvarchar](80) NULL,
	[InvoiceId] [int] NULL,
	[LineNumber] [int] NULL,
	[CreatedBy] [varchar](80) NOT NULL,
	[UpdatedBy] [varchar](80) NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateUpdated] [datetime] NULL,
 CONSTRAINT [uCommerce_PK_SupplierVatPaymentId] PRIMARY KEY CLUSTERED 
(
	[SupplierVatPaymentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


ALTER TABLE [dbo].[LB_Vat_Payments] ADD  CONSTRAINT [DF_LB_Vat_Payments_PostedVatAmount]  DEFAULT (NULL) FOR [PostedVatAmount]
GO


