﻿/* Procedure for fetching [LB_GetOrderGuidByOrderlineID] */
CREATE PROCEDURE [dbo].[LB_GetOrderGuidByOrderlineID]
(              
   @OrderLineId int=NULL                
)
AS 
BEGIN
   IF (@OrderLineId IS NOT NULL)
   BEGIN                   
	select OrderReference from LB_RoomReservation where OrderLineId = @OrderLineId                                                                                
   END       
END
