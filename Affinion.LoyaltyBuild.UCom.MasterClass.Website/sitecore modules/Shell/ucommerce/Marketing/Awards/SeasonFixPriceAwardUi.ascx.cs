﻿using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.EntitiesV2;
using UCommerce.Presentation.Views.Marketing;
using UCommerce.Presentation.Web;
using UCommerce.Presentation.Web.Controls;
using Affinion.LoyaltyBuild.UCom.Common.Extension;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.Ucommerce.Marketing.Awards
{
    public partial class SeasonFixPriceAwardUi : ViewEnabledControl<IEditCampaignItemView>, IConfigurable
    {


        protected void Page_Load(object sender, EventArgs e)
        {
            lblError.Text = string.Empty;
            SeasonFixPriceAward dataSource = (SeasonFixPriceAward)this.DataSource;
            BindData();
            this.DeleteButton.OnClientClick = string.Format("return confirm('{0}');", "Are you want to delete award");
        }

        #region Grid Event
        protected void EditSeasonFixPriceDetail(object sender, GridViewEditEventArgs e)
        {
            GrdSeasonFixPrice.EditIndex = e.NewEditIndex;
            ViewState["EditIndex"] = e.NewEditIndex;
            BindData();
        }

        protected void CancelEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GrdSeasonFixPrice.EditIndex = -1;
            ViewState["EditIndex"] = -1;
            BindData();
        }


        protected void AddNewAwardDetail(object sender, EventArgs e)
        {
            string startDate = ((TextBox)GrdSeasonFixPrice.FooterRow.FindControl("txtStartDate")).Text;
            string endDate = ((TextBox)GrdSeasonFixPrice.FooterRow.FindControl("txtEndDate")).Text;
            string amountOff = ((TextBox)GrdSeasonFixPrice.FooterRow.FindControl("txtAmountOff")).Text;
            bool isValid;
            string msg;
            ValidateGrid(startDate, endDate, amountOff, out isValid, out msg);
            if (isValid)
            {
                List<SeasonFixPriceAwardDetail> awrddetails = ((SeasonFixPriceAward)this.DataSource).Details.ToList();
                if (awrddetails != null && awrddetails.Any(x =>
                    (startDate.ParseFormatDateTime() >= x.StartDate && startDate.ParseFormatDateTime() <= x.EndDate)
                    ||
                    (endDate.ParseFormatDateTime() >= x.StartDate && endDate.ParseFormatDateTime() <= x.EndDate)
                    ))
                {
                    lblError.Text = "Date Selection is invalid";

                }
                else
                {
                    try
                    {
                        SeasonFixPriceAwardDetail awardDetail = new SeasonFixPriceAwardDetail();
                        awardDetail.StartDate = startDate.ParseFormatDateTime();
                        awardDetail.EndDate = endDate.ParseFormatDateTime();
                        awardDetail.AmountOff = Convert.ToInt32(amountOff);
                        awardDetail.SeasonFixPriceAward = ((SeasonFixPriceAward)this.DataSource);
                        awardDetail.Save();
                        ((SeasonFixPriceAward)this.DataSource).Details.Add(awardDetail);
                        //((SeasonFixPriceAward)this.DataSource).Save();
                    }
                    catch (Exception er)
                    {

                    }
                    //awardDetail.Save();
                    BindData();
                }
            }
            else
            {
                lblError.Text = msg;
            }
        }


        protected void UpdateAwardDetail(object sender, GridViewUpdateEventArgs e)
        {

            string detailId = ((HiddenField)GrdSeasonFixPrice.Rows[e.RowIndex].FindControl("hdDetailId")).Value;
            string startDate = ((TextBox)GrdSeasonFixPrice.Rows[e.RowIndex].FindControl("txtStartDate")).Text;
            string endDate = ((TextBox)GrdSeasonFixPrice.Rows[e.RowIndex].FindControl("txtEndDate")).Text;
            string amountOff = ((TextBox)GrdSeasonFixPrice.Rows[e.RowIndex].FindControl("txtAmountOff")).Text;
            int awarddetailId = 0;
            bool isValid;
            string msg;
            ValidateGrid(startDate, endDate, amountOff, out isValid, out msg);
            if (isValid)
            {
                if (int.TryParse(detailId, out awarddetailId))
                {
                    List<SeasonFixPriceAwardDetail> awrddetails = ((SeasonFixPriceAward)this.DataSource).Details.ToList();
                    if (awrddetails != null && awrddetails.Any(x => x.SeasonFixPriceAwardDetailId != awarddetailId &&
                        ((startDate.ParseFormatDateTime() >= x.StartDate && startDate.ParseFormatDateTime() <= x.EndDate)
                        ||
                        (endDate.ParseFormatDateTime() >= x.StartDate && endDate.ParseFormatDateTime() <= x.EndDate)
                        )))
                    {
                        lblError.Text = "Date Selection is invalid";
                    }
                    else
                    {
                        SeasonFixPriceAwardDetail awardDetail = SeasonFixPriceAwardDetail.Get(awarddetailId);
                        awardDetail.StartDate = startDate.ParseFormatDateTime();
                        awardDetail.EndDate = endDate.ParseFormatDateTime();
                        awardDetail.AmountOff = Convert.ToInt32(amountOff);
                        awardDetail.SeasonFixPriceAward = ((SeasonFixPriceAward)this.DataSource);
                        awardDetail.Save();
                        GrdSeasonFixPrice.EditIndex = -1;
                        ViewState["EditIndex"] = -1;
                        BindData();
                    }
                }

            }
            else
            {
                lblError.Text = msg;
            }

        }

        private void ValidateGrid(string startDate, string endDate, string amountOff, out bool isValid, out string msg)
        {
            lblError.Text = string.Empty;
            isValid = true;
            msg = string.Empty;
            if (!startDate.ValidDate())
            {
                isValid = false;
                msg += "Start Date Invaid. ";
            }
            if (!endDate.ValidDate())
            {
                isValid = false;
                msg += "End Date Invaid. ";
            }
            if (string.IsNullOrEmpty(amountOff))
            {
                isValid = false;
                msg += "Price Required. ";
            }
            int amount;
            if (!int.TryParse(amountOff, out amount))
            {
                isValid = false;
                msg += "Price is invalid. ";
            }
            if (isValid)
            {
                DateTime dtStartDate = startDate.ParseFormatDateTime();
                DateTime dtEndDate = endDate.ParseFormatDateTime();
                if (dtStartDate.Date > dtEndDate.Date)
                {
                    isValid = false;
                    msg += "StartDate is less then or equal to end date";
                }
            }
        }

        private void BindData(int currentPageIndex = 0)
        {
            //            SeasonFixPriceAward dataSource = SeasonFixPriceAward.Get(this.AwardId); // (SeasonFixPriceAward)this.DataSource;
            SeasonFixPriceAward dataSource = (SeasonFixPriceAward)this.DataSource;
            if (dataSource != null && dataSource.Details != null && dataSource.Details.Any())
            {
                //dataSource.Details = new List<SeasonFixPriceAwardDetail>();
                //dataSource.Details.Add(new SeasonFixPriceAwardDetail() { AmountOff = 0, StartDate = DateTime.Now, EndDate = DateTime.Now, SeasonFixPriceAwardDetailId = -1 });
                GrdSeasonFixPrice.DataSource = dataSource.Details;
                GrdSeasonFixPrice.PageIndex = currentPageIndex;
                GrdSeasonFixPrice.DataBind();
            }

            else
            {
                List<SeasonFixPriceAwardDetail> data = new List<SeasonFixPriceAwardDetail>();
                data.Add(new SeasonFixPriceAwardDetail() { AmountOff = 0, StartDate = DateTime.Now, EndDate = DateTime.Now, SeasonFixPriceAwardDetailId = -1 });
                GrdSeasonFixPrice.DataSource = data;
                GrdSeasonFixPrice.PageIndex = currentPageIndex;
                GrdSeasonFixPrice.DataBind();
            }

        }

        protected void ValidateDate(object sender, ServerValidateEventArgs e)
        {
            string controlName = ((BaseValidator)sender).ControlToValidate;
            var control = FindControl(controlName) as TextBox;
            try
            {
                e.Value.ParseFormatDateTime();
                e.IsValid = true;
            }
            catch
            {
                e.IsValid = false;
            }
        }


        protected void DeleteAwardDetail(object sender, EventArgs e)
        {
            LinkButton lnkRemove = (LinkButton)sender;
            int detailId = 0;
            if (int.TryParse(lnkRemove.CommandArgument, out detailId))
            {
                SeasonFixPriceAward dataSource = (SeasonFixPriceAward)this.DataSource;
                if (dataSource != null && dataSource.Details != null)
                {
                    SeasonFixPriceAwardDetail removeeDetail = dataSource.Details.FirstOrDefault(x => x.SeasonFixPriceAwardDetailId == detailId);
                    if (removeeDetail != null)
                    {
                        dataSource.Details.Remove(removeeDetail);
                        removeeDetail.Delete();
                        dataSource.Save();
                    }
                    BindData();
                }
            }
        }



        protected void OnPaging(object sender, GridViewPageEventArgs e)
        {
            BindData(e.NewPageIndex);
        }

        #endregion



        // Methods
        protected void DeleteButton_Click(object sender, ImageClickEventArgs e)
        {
            if (this.AwardId != 0)
            {
                try
                {
                    (from x in UCommerce.EntitiesV2.Award.All()
                     where x.AwardId == this.AwardId
                     select x).SingleOrDefault<UCommerce.EntitiesV2.Award>().Delete();
                }
                catch (Exception er)
                {

                }
            }
            this.Page.Response.Redirect(this.Page.Request.Url.ToString(), true);
        }

        public object New()
        {
            SeasonFixPriceAward priceAward = SeasonFixPriceAward.All().FirstOrDefault(x => x.CampaignItem.CampaignItemId == View.CampaignItem.CampaignItemId) ?? new SeasonFixPriceAward();
            priceAward.CampaignItem = base.View.CampaignItem;
            priceAward.Content = base.View.CampaignItem.Name;
            priceAward.Name = "Season Fix Price";
            //priceAward.Details.Add(new SeasonFixPriceAwardDetail() { SeasonFixPriceAward = priceAward, AmountOff = 0, StartDate = new DateTime(2016, 1, 1), EndDate = new DateTime(2016, 4, 1) });
            //priceAward.Details.Add(new SeasonFixPriceAwardDetail() { SeasonFixPriceAward = priceAward, AmountOff = 0, StartDate = new DateTime(2016, 4, 1), EndDate = new DateTime(2016, 6, 1) });
            //priceAward.Details.Add(new SeasonFixPriceAwardDetail() { SeasonFixPriceAward = priceAward, AmountOff = 0, StartDate = new DateTime(2016, 7, 1), EndDate = new DateTime(2016, 9, 1) });
            //priceAward.Details.Add(new SeasonFixPriceAwardDetail() { SeasonFixPriceAward = priceAward, AmountOff = 0, StartDate = new DateTime(2016, 10, 1), EndDate = new DateTime(2017, 1, 1) });
            this.DataSource = priceAward;
            ((SeasonFixPriceAward)this.DataSource).Save();
            return this.DataSource;
        }

        public int AwardId
        {
            get
            {
                if (this.DataSource == null)
                {
                    return 0;
                }
                return ((UCommerce.EntitiesV2.Award)this.DataSource).AwardId;
            }
        }

        public int CampaignItemId
        {
            get
            {
                return base.View.CampaignItem.CampaignItemId;
            }
        }

        public object DataSource { get; set; }

        protected void GrdSeasonFixPrice_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null && ((SeasonFixPriceAwardDetail)e.Row.DataItem).SeasonFixPriceAwardDetailId == -1)
            {
                e.Row.Visible = false;
            }
        }

    }



}