﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ArrivalReport.ascx.cs"  Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Reports.ArrivalReport" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" TagPrefix="asp" %>

<script language="javascript" type="text/javascript">
    //No.of Days Difference
    function DateDifference(sender, args) {
        var startDate = Date.parse(document.getElementById('tbStartDate').value);
        var endDate = Date.parse(document.getElementById('tbEndDate').value);
        var timeDiff = endDate - startDate;

        daysDiff = Math.floor(timeDiff / (1000 * 60 * 60 * 24));

        if (daysDiff > 366) {
            args.IsValid = false;
        }
        else {
            args.IsValid = true;
        }
    }
</script>

<style type="text/css">

     #maincontent_0_rvShowRecord_ctl09
       {

           max-height:350px;

       }     


</style>


<div class="supplier-report">


    <div>
        <h3 class="title-text font-weight-bold"> <sc:Text id="ArrivalReportText" runat="server" /></h3>
    </div>
    <div>
         <sc:Text id="ArrivalReportInformation" runat="server" />
        <asp:Label ID="lblshow" runat="server" ></asp:Label>
       
    </div>
    <div class="col-xs-12">
        <asp:ValidationSummary ID="valsum" CssClass="alert alert-danger validation-summary" runat="server" ShowValidationErrors="true" EnableClientScript="true"  DisplayMode="BulletList" ShowSummary="true" />
    </div>
    <div>
        <asp:UpdatePanel ID="uplFilters" runat="server">
            <ContentTemplate>
                <ajaxtoolkit:calendarextender id="dateFromCalendarExtender" runat="server" animated="false"
                    targetcontrolid="txtDatefrom"
                    popupposition="BottomLeft"
                    format="dd/MM/yyyy"
                    popupbuttonid="btnDateFrom" />
                <ajaxtoolkit:calendarextender id="dateToCalendarExtender" runat="server" animated="false"
                    targetcontrolid="txtDateTo"
                    popupposition="BottomLeft"
                    format="dd/MM/yyyy"
                    popupbuttonid="btnDateTo" />
                <div class="row report-filter">
                    <div class="col-xs-11">
                        <div class="row">
                            <div class="col-xs-2">
                                 <sc:Text id="BookingReferenceText" runat="server" />
                                <label></label>
                            </div>
                            <div class="col-xs-3">
                                <asp:TextBox CssClass="form-control" ID="txtbookingInfor" runat="server" MaxLength="50"></asp:TextBox>
                            </div>
                            <div class="col-xs-2">
                                <label></label>
                                 <sc:Text id="ClientNameText" runat="server" />

                            </div>
                            <div class="col-xs-3">
                                <asp:TextBox CssClass="form-control" ID="txtClientName" runat="server" placeholder="First Name or Last Name" MaxLength="50"></asp:TextBox>
                                
                            </div>
							<div class="col-xs-2">
								<span style="font-size: 12px"> <sc:Text id="FirstNameOrLastText" runat="server" />
</span>
							</div>
                        </div>
                        <div class="row">
                            <div class="col-xs-2">
                                <label></label>
                                 <sc:Text id="DateFromText" runat="server" />

                                <span class="required">*</span>
                            </div>
                            <div class="col-xs-3">
                                <div class="input-group">
                                    <asp:RequiredFieldValidator ID="datefromRequiredFieldValidator" ErrorMessage="Date from is required" CssClass="highligh-error" ControlToValidate="txtDatefrom" runat="server" Display="None" />
                                    <asp:TextBox CssClass="form-control" ID="txtDatefrom" runat="server"></asp:TextBox>
                                    <div class="input-group-addon">
                                        <asp:LinkButton ID="btnDateFrom" runat="server" CssClass="calendar glyphicon glyphicon-calendar" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-2">
                                <label></label>
                                 <sc:Text id="OfferNameText" runat="server" />

                            </div>
                            <div class="col-xs-3">
                                <asp:DropDownList CssClass="form-control" ID="ddlOfferName" runat="server" AppendDataBoundItems="false">
									<asp:ListItem Selected="True" Text="-All-" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-2">
                                <label></label>
                                 <sc:Text id="DateToText" runat="server" />

                                <span class="required">*</span>
                            </div>
                            <div class="col-xs-3">
                                <div class="input-group">
                                    <asp:CompareValidator ID="cmpVal1" ControlToCompare="txtDatefrom"  CssClass="highligh-error"
									 ControlToValidate="txtDateTo" Type="Date" Operator="GreaterThanEqual"  
									 ErrorMessage="Start date should be greater than or equal to end date" runat="server"></asp:CompareValidator>
									 <asp:RequiredFieldValidator ID="datetoRequiredFieldValidator" ErrorMessage="Date to is required" CssClass="highligh-error"  ControlToValidate="txtDateTo" runat="server" Display="None" />
                                    <asp:CustomValidator ID="cusValDateTo" ErrorMessage="Report can only be viewed for 12 months at a time" CssClass="highligh-error" runat="server" ControlToValidate="txtDateTo" Display="None"></asp:CustomValidator>
                                    <asp:TextBox CssClass="form-control" ID="txtDateTo" runat="server" CausesValidation="true"></asp:TextBox>
                                    <div class="input-group-addon">
                                        <asp:LinkButton ID="btnDateTo" runat="server" CssClass="calendar glyphicon glyphicon-calendar" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-2">
                                <label></label>
                                 <sc:Text id="ProviderText" runat="server" />

                                <span class="required">*</span>
                            </div>
                            <div class="col-xs-3">
                                <asp:RequiredFieldValidator ID="providerrequiredfieldvalidator" ErrorMessage="Provider is required" CssClass="highligh-error" ControlToValidate="ddlProvider" InitialValue="0" runat="server" Display="None" />
                                <asp:DropDownList CssClass="form-control" ID="ddlProvider" AutoPostBack="true" runat="server" AppendDataBoundItems="false" OnSelectedIndexChanged="ddlProvider_SelectedIndexChanged">
                                    <asp:ListItem Text="-Select-" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-2">
                                <label></label>
                                 <sc:Text id="ReportAndOrderByText" runat="server" />

                            </div>
                            <div class="col-xs-3">
                                <asp:DropDownList CssClass="form-control" ID="ddlReportOrder" runat="server">
                                    <asp:ListItem>Arrival Date</asp:ListItem>
                                    <asp:ListItem>Reservation Date</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                              <div class="col-xs-2">
                                <label></label>
                                   <sc:Text id="BookingStatusText" runat="server" />

                            </div>
                            <div class="col-xs-3">
                                <asp:DropDownList CssClass="form-control" ID="ddlBookingStatus" AutoPostBack="true" runat="server" AppendDataBoundItems="false">
                                    <asp:ListItem Selected="True" Text="-Select-" Value="0"></asp:ListItem>
                                    <asp:ListItem Text ="Provisional" Value="5"></asp:ListItem>
                                    <asp:ListItem  Text="Confirmed" Value="6"></asp:ListItem>
                                    <asp:ListItem  Text="Cancelled" Value="7"></asp:ListItem>
                                </asp:DropDownList>
                                
                            </div>
                            <div class="col-xs-7">
                               <%-- <asp:Button ID="btnList" runat="server" Text="List Booking" CssClass="btn btn-default" OnClick="btnList_Click" />--%>
                                <button id="btnList" runat="server" CssClass="btn btn-default" OnClick="btnList_Click">
                                 <sc:Text id="btnListText" runat="server" />
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnList" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <div>
        <b><asp:Label Id="lblHotelname" runat="server"></asp:Label></b>
    </div>
    <div>
        <ul class="color-codes">
            <li class="color-never-tranferred"><sc:Text id="NeverTransferredText" runat="server" /></li>
            <li class="color-tranferred-in"><sc:Text id="TransferredInText" runat="server" /></li>
            <li class="color-tranferred-out"><sc:Text id="TransferredOutText" runat="server" /></li>
        </ul>
        <div class="clearfix"></div>
    </div>
    
    <div>
        <rsweb:reportviewer id="rvShowRecord" runat="server" width="100%" asyncrendering="false" ShowPrintButton="true"></rsweb:reportviewer>
    </div>
</div>


