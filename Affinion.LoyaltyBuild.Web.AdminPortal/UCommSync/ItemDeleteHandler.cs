﻿using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.CategoryImport;
using Sitecore.Data.Items;
using Sitecore.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Affinion.LoyaltyBuild.AdminPortal.UCommSync
{
    public class ItemDeleteHandler
    {
        public void OnItemDeleteing(object sender, EventArgs args)
        {
            try
            {
                Item savedItem = Event.ExtractParameter(args, 0) as Item;
                ISiteCoreHotel dataImport = new SiteCoreHotel();
                // Allow only non null items and allow only items from the master database
                if (savedItem != null)
                {
                    if (savedItem.TemplateName.Equals("SupplierDetails", StringComparison.InvariantCultureIgnoreCase))
                    {
                        dataImport.DeleteCategory(savedItem.ID.Guid.ToString());
                    }
                }
            }
            catch (Exception er)
            {
                Diagnostics.WriteException(DiagnosticsCategory.SyncData, er, null);
            }
        }
    }
}