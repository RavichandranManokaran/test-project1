﻿using UCommerce.EntitiesV2;
using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Rules.Model;
using Sitecore.Security.Accounts;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using UCommerce.Infrastructure;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Queries;
using System.Globalization;
using Affinion.LoyaltyBuild.UCom.Common.Extension;
using Affinion.LoyaltyBuild.UCom.Common.Constants;
/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Rules
{
    /// <summary>
    /// Service that provides methods to get payment rule
    /// </summary>
    public class SitecorePaymentService : ISitecorePaymentService
    {
        private IRepository<OrderLineCampaignRelation> relationRepository = ObjectFactory.Instance.Resolve<IRepository<OrderLineCampaignRelation>>();

        /// <summary>
        /// Gets the Payment Rule Information 
        /// </summary>
        /// <param name="Clientid">Clientid</param>
        /// <returns>Object of PaymentRuleModel list</returns>
        public List<PaymentRule> GetPaymentRule(string clientId)
        {
            List<PaymentRule> listPaymentRule = new List<PaymentRule>();

            using (new UserSwitcher(ItemHelper.ContentEditingUser))
            {
                if (!string.IsNullOrEmpty(clientId))
                {
                    Item clientItem = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(clientId));
                    if (clientItem != null)
                    {
                       
                        var childItems = clientItem.Axes.GetDescendants().Where(x =>x.TemplateID.ToString().Equals(Constants.paymentRuleTemplate, StringComparison.InvariantCultureIgnoreCase)).ToList();
                        if (childItems != null)
                        {
                            foreach (Item childItem in childItems)
                            {
                                PaymentRule paymentRule = new PaymentRule();
                                paymentRule.ProviderType = SitecoreFieldsHelper.GetDropLinkFieldValue(childItem, "Provider Type","__display name");
                                paymentRule.Portal = SitecoreFieldsHelper.GetDropLinkFieldValue(childItem, "Portal","__display name");
                                paymentRule.PaymentType = SitecoreFieldsHelper.GetDropLinkFieldValue(childItem, "Payment Type", "__display name");
                                paymentRule.PaymentMethod = SitecoreFieldsHelper.GetDropLinkFieldValue(childItem, "Payment Method", "__display name");
                                paymentRule.OfferOverrides = SitecoreFieldsHelper.CheckBoxChecked(childItem, "OfferOverrides");

                                listPaymentRule.Add(paymentRule);
                            }
                            return listPaymentRule;
                        }
                    }
                }
                return null;
            }
        }

        /// <summary>
        /// Update the payment method 
        /// </summary>
        /// <param name="Clientid">Clientid</param>
        /// <param name="orderId">orderId</param>
        /// <param name="portal">portal</param>
        /// <param name="paymentType">paymentType</param>
        /// <param name="orderPaymentMethod">orderPaymentMethod</param>
        /// <returns></returns>
        public void UpdatePaymentMethod(string clientId, int orderId, string portal, string paymentType = null, string orderPaymentMethod = null)
        {
            var methods = GetRulePaymentMethod(clientId, orderId, portal, paymentType, orderPaymentMethod);
            //update partial to full if not all items are partial
            if (methods.Count != methods.Count(i => i.Paymentmethod == Constants.PaymentMethod.PartialPayment))
            {
                methods.ForEach(i =>
                    {
                        if (i.Paymentmethod == Constants.PaymentMethod.PartialPayment)
                            i.Paymentmethod = Constants.PaymentMethod.FullPayment;
                    });
            }

            foreach (var item in methods)
            {
                var orderLine = OrderLine.All().FirstOrDefault(x => x.OrderLineId == item.OrderLineId);
                if (orderLine != null)
                {
                    orderLine[OrderPropertyConstants.PaymentMethod] = item.Paymentmethod;
                    orderLine.Save();
                }
            }
        }

        /// <summary>
        /// get rule Paymentmethod
        /// </summary>
        /// <param name="Clientid">clientId</param>
        ///<param name="orderId">orderId</param>
        ///<param name="portal">portal</param>
        ///<param name="paymentType">paymentType</param>
        /// <returns>Object of update Paymentmethod list</returns>
        public List<OrderLinePaymentmethod> GetRulePaymentMethod(string clientId, int orderId, string portal, string paymentType = null, string orderPaymentMethod = null)
        {
            List<PaymentRule> rules = null;
            if (clientId != null && !string.IsNullOrWhiteSpace(clientId))
            {
                rules = GetPaymentRule(clientId);
            }
            else
            {
                Affinion.LoyaltyBuild.Api.Booking.Service.BookingService bs = new Api.Booking.Service.BookingService();
                Affinion.LoyaltyBuild.Api.Booking.Data.BasketInfo bi = bs.GetPurchaseOrder(orderId);
                clientId = bi.Bookings.First().ClientId;
                rules = GetPaymentRule(clientId);
            }
            var result = new List<OrderLinePaymentmethod>();

            List<OrderLine> orderLineList = OrderLine.All().Where(x => x.PurchaseOrder.OrderId == orderId).ToList();
            List<OrderLineCampaignRelation> orderLineCampaignMapping = relationRepository.Select(new GetOrderLineSubrateQuery(orderId)).ToList();

            foreach (var item in orderLineList)
            {
                if (string.IsNullOrEmpty(orderPaymentMethod))
                {
                    var product = UCommerce.EntitiesV2.Product.FirstOrDefault(x => x.Sku == item.Sku && x.VariantSku == null);

                    var providerType = product.ProductProperties.FirstOrDefault(i => i.ProductDefinitionField.Name == ProductDefinationConstant.SupplierType).Value;

                    int campaingItemId = 0;
                    var campaingInfo = orderLineCampaignMapping.FirstOrDefault(x => x.OrderLineId == item.OrderLineId);
                    if (campaingInfo != null)
                    {
                        campaingItemId = campaingInfo.CampaignItemId;
                    }
                    var property = UCommerce.EntitiesV2.CampaignItemProperty.FirstOrDefault(i => i.CampaignItem.CampaignItemId == campaingItemId && i.DefinitionField.Name == DefinationConstant.PaymentMethod);
                    string offerPaymentMethod = string.Empty;
                    if (property != null)
                    {
                        offerPaymentMethod = property.Value;
                    }


                    foreach (var rule in rules)
                    {
                        var match = (string.IsNullOrEmpty(rule.PaymentType) || rule.PaymentType == paymentType) &&
                          (string.IsNullOrEmpty(rule.Portal) || rule.Portal == portal) &&
                          (string.IsNullOrEmpty(rule.ProviderType) || (!string.IsNullOrEmpty(rule.ProviderType) && rule.ProviderType.ToLower() == providerType.ToLower()));

                        if (match)
                        {
                            var method = (!rule.OfferOverrides || string.IsNullOrEmpty(offerPaymentMethod)) ? rule.PaymentMethod : offerPaymentMethod;

                            if (method.Equals(Constants.PaymentMethod.PartialPayment))
                            {
                                int percentage = 0, daysBefore = 0;
                                DateTime checkInDate, currentDate;
                                checkInDate = currentDate = DateTime.Now;

                                int.TryParse(item[OrderPropertyConstants.PercentageOfAmount], out percentage);
                                int.TryParse(item[OrderPropertyConstants.DaysBefore], out daysBefore);
                                if (item[OrderPropertyConstants.CheckInDate].ValidDate())
                                    checkInDate = item[OrderPropertyConstants.CheckInDate].ParseFormatDateTime();
                                if (percentage == 0 || daysBefore == 0 || (checkInDate - currentDate).TotalDays < daysBefore)
                                {
                                    method = Constants.PaymentMethod.FullPayment;
                                }
                            }

                            result.Add(new OrderLinePaymentmethod{
                                OrderLineId = item.OrderLineId,
                                Paymentmethod = method
                            });
                            break;
                        }
                    }
                }
                else
                {
                    result.Add(new OrderLinePaymentmethod
                    {
                        OrderLineId = item.OrderLineId,
                        Paymentmethod = orderPaymentMethod
                    });
                }
            }

            return result;
        }


        /// <summary>
        /// Gets the OrderLine Payment method Information as List
        /// </summary>
        /// <param name="orderId">orderId</param>
        /// <returns>Object of PaymentRuleModel list</returns>
        public List<OrderLinePaymentmethod> GetPaymentMethod(int orderId)
        {
            List<OrderLinePaymentmethod> orderLinePayment = new List<OrderLinePaymentmethod>();
            List<OrderLine> orderLineList = OrderLine.All().Where(x => x.PurchaseOrder.OrderId == orderId).ToList();

            foreach (var item in orderLineList)
            {
                orderLinePayment.Add(new OrderLinePaymentmethod
                {
                    OrderLineId = item.OrderLineId,
                    Paymentmethod = item["PaymentMethod"]
                });
            }

            return orderLinePayment;
        }
    }
}


