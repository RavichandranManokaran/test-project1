﻿using Affinion.LoyaltyBuild.UCom.Common.Extension;
using Affinion.LoyaltyBuild.UCom.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi
{
    public class SearchCriteria
    {
        public Category SearchCategory { get; set; }

        /// <summary>
        /// if searching for a hotel
        /// </summary>
        public int? SearchHotel { get; set; }

        /// <summary>
        /// Get or Set a Themes
        /// </summary>
        public string Themes { get; set; }

        /// <summary>
        /// Get or Set a Country
        /// </summary>
        public string Country { get; set; }


        /// <summary>
        /// Get or Set a Facility
        /// </summary>
        public string Facility { get; set; }

        public string LocalRanking { get; set; }

        public string StarRanking { get; set; }

        public string Experience { get; set; }

        /// <summary>
        /// Set Room Type Based On Occupancy
        /// </summary>
        public string RoomType { get; set; }

        public SearchCriteria() { Pageno = 1; PageSize = 10; }

        public List<string> categoryGuid { get; set; }

        public bool SortByPrice { get; set; }

        public int Pageno { get; set; }

        public int PageSize { get; set; }

        public string ClientName { get; set; }

        public string Location { get; set; }

        public string Region { get; set; }

        public string PackageType { get; set; }

        public int Night { get; set; }

        public DateTime ArrivalDate { get; set; }

        public int NumberOfChild { get; set; }

        public int NumberOfAdult { get; set; }

        public PriceGroup PriceGroup { get; set; }

        public Guid ClientId { get; set; }

        public Guid CurrencyId { get; set; }

        public List<OrderProperty> CreateOrderLineProperty()
        {
            List<OrderProperty> orderProperties = new List<OrderProperty>();
            OrderProperty orderProperty = new OrderProperty();
            orderProperty.Key = OrderPropertyConstants.NightKey;
            orderProperty.Value = this.Night.ToString();
            orderProperties.Add(orderProperty);

            orderProperty = new OrderProperty();
            orderProperty.Key = OrderPropertyConstants.ArrivalDateKey;
            orderProperty.Value = this.ArrivalDate.ConvertDateToString(true);
            orderProperties.Add(orderProperty);

            orderProperty = new OrderProperty();
            orderProperty.Key = OrderPropertyConstants.NoOfChildKey;
            orderProperty.Value = this.NumberOfChild.ToString();
            orderProperties.Add(orderProperty);

            orderProperty = new OrderProperty();
            orderProperty.Key = OrderPropertyConstants.NoOfAdultKey;
            orderProperty.Value = this.NumberOfAdult.ToString();
            orderProperties.Add(orderProperty);

            orderProperty = new OrderProperty();
            orderProperty.Key = OrderPropertyConstants.ReservationDateKey;
            orderProperty.Value = DateTime.Now.ConvertDateToString(true);
            orderProperties.Add(orderProperty);



            return orderProperties;
        }
    }
}
