
/****** Object:  Table [dbo].[LB_BB_Provider_Reference]    Script Date: 7/19/2016 8:02:20 PM ******/

CREATE TABLE [dbo].[LB_BB_Provider_Reference](
	[ProviderId] [int] NULL,
	[ProviderName] [varchar](50) NULL,
	[Latitude] [decimal](10, 5) NULL,
	[Longitude] [decimal](10, 5) NULL
) ON [PRIMARY]

GO

