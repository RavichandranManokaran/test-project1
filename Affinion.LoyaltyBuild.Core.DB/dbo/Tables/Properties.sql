﻿CREATE TABLE [dbo].[Properties] (
    [ID]        UNIQUEIDENTIFIER CONSTRAINT [DF_Properties_ID] DEFAULT (newid()) NOT NULL,
    [Key]       NVARCHAR (256)   NOT NULL,
    [Value]     NTEXT            NOT NULL,
    [DAC_Index] INT              NULL
);


GO
CREATE CLUSTERED INDEX [DAC_Clustered]
    ON [dbo].[Properties]([DAC_Index] ASC);


GO
CREATE NONCLUSTERED INDEX [ndxKey]
    ON [dbo].[Properties]([Key] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [ndxID]
    ON [dbo].[Properties]([ID] ASC);

