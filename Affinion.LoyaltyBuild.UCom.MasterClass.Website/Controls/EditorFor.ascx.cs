﻿using System;
using System.Linq;
using UCommerce.EntitiesV2;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.Controls
{
	public partial class EditorFor : System.Web.UI.UserControl
	{
		public OrderAddress OrderAddress { get; set; }
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				Header.InnerText = OrderAddress.AddressName;
				FistNameTextBox.Text = OrderAddress.FirstName;
				LastNameTextBox.Text = OrderAddress.LastName;
				EmailTextBox.Text = OrderAddress.EmailAddress;
				CompanyTextBox.Text = OrderAddress.CompanyName;
				AttentionTextBox.Text = OrderAddress.Attention;
				StreetTextBox.Text = OrderAddress.Line1;
				StreetTwoTextBox.Text = OrderAddress.Line2;
				CityTextBox.Text = OrderAddress.City;
				PostalCodeTextBox.Text = OrderAddress.PostalCode;
				PhoneTextBox.Text = OrderAddress.PhoneNumber;
				MobilePhoneTextBox.Text = OrderAddress.MobilePhoneNumber;

				CountryDropDownList.DataSource = Country.All().ToList();
				CountryDropDownList.DataBind();
				CountryDropDownList.SelectedValue = (OrderAddress.Country ?? new Country()).CountryId.ToString();
			}
		}

		public OrderAddress EditedAddress
		{
			get
			{
				var editedAddress = new OrderAddress();
				editedAddress.AddressName = Header.InnerText;
				editedAddress.FirstName = FistNameTextBox.Text;
				editedAddress.LastName = LastNameTextBox.Text;
				editedAddress.EmailAddress = EmailTextBox.Text;
				editedAddress.CompanyName = CompanyTextBox.Text;
				editedAddress.Attention = AttentionTextBox.Text;
				editedAddress.Line1 = StreetTextBox.Text;
				editedAddress.Line2 = StreetTwoTextBox.Text;
				editedAddress.City = CityTextBox.Text;
				editedAddress.PostalCode = PostalCodeTextBox.Text;
				editedAddress.PhoneNumber = PhoneTextBox.Text;
				editedAddress.MobilePhoneNumber = MobilePhoneTextBox.Text;
				editedAddress.Country = Country.Get(Convert.ToInt32(CountryDropDownList.SelectedValue));

				return editedAddress;
			}
		}
	}
}