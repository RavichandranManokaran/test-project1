﻿CREATE TABLE [dbo].[uCommerce_Savedforlater](
       [SavedlaterId] [int] IDENTITY(1,1) NOT NULL,
       [customerID] [int] NOT NULL,
       [ProviderID] [uniqueidentifier] NOT NULL,
       [CreatedBy] [nchar](10) NULL,
       [Updatedby] [nchar](10) NULL,
       [Createddate] [datetime] NULL,
       [updatedate] [datetime] NULL,
CONSTRAINT [PK_uCommerce_Savedforlater] PRIMARY KEY CLUSTERED 
(
       [SavedlaterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
