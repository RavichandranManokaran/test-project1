﻿//<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           ChangePasswordSublayout.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal
/// Description:           Used to Bind data to ChangePasswordSublayout subLayout
/// </summary>

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    #region Using Directives
    using Affinion.Loyaltybuild.Security;
    using Affinion.LoyaltyBuild.Common;
    using Affinion.LoyaltyBuild.Common.Exceptions;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.SupplierPortal.DataAccess;
    using Affinion.LoyaltyBuild.SupplierPortal.DataAccess.Service;
    using Affinion.LoyaltyBuild.Web.ClientPortal.Helper;
    using Sitecore.Data.Items;
    using Sitecore.Links;
    using Sitecore.Security.Authentication;
    using System;
    using System.Security.Authentication;
    using System.Web.Security;
    #endregion

    public partial class OfflinePortalChangePasswordSublayout : System.Web.UI.UserControl
    {
        #region Private Properties
        private TimeSpan timeSpanToExpirePassword;
        private string LogOnPage { get; set; }
        private string SuccessMessage { get; set; }
        private string FailierMessage { get; set; }
        private string IncorrectCurrentPassword { get; set; }
        private string PasswordInHistory { get; set; }
        private string PasswordNotStrong { get; set; }
        private string PasswordAlreadyChanged { get; set; }
        #endregion

        protected readonly ILoginService loginSvc;
        public OfflinePortalChangePasswordSublayout()
        {
            this.loginSvc = new LoginService();

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //initialyze page
            InitPage();
            //message labels 
            errorMessage.Visible = false;
            PanelPasswordExpiered.Visible = false;

            //Expiered password message
            string uniqueID = Request.QueryString["id"];
            if (!String.IsNullOrWhiteSpace(uniqueID))
            {

                string message = string.Empty;
                string username = loginSvc.SelectChangePasswordHistory(uniqueID);
                if (!String.IsNullOrWhiteSpace(username))
                {
                    MembershipUser userProfile = Membership.GetUser(username);
                    var user = userProfile;

                    if (user != null && HasPasswordExpired(user))
                    {
                        PanelPasswordExpiered.Visible = true;
                    }
                }
            }

        }


        protected void ButtonEditPassword_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                try
                {
                    string message = string.Empty;
                    string username = string.Empty;
                    if (Request.QueryString.Count > 0)
                    {
                        string uniqueID = Request.QueryString["id"];
                        if (!String.IsNullOrWhiteSpace(uniqueID))
                        {
                            username = loginSvc.SelectChangePasswordHistory(uniqueID);

                            message = ChangePasswordBasedOnUser(message, username);
                        }
                    }
                    else
                    {
                        username = Sitecore.Context.User.Name;
                        message = ChangePasswordBasedOnUser(message, username);

                    }
                }
                catch (Exception ex)
                {
                    //Log 
                    Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                    literalMessagePassword.Text = ex.Message;
                    throw;

                }
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ButtonCancel_Click(object sender, EventArgs e)
        {
            if (Request.QueryString.HasKey("id"))
            {
                Response.Redirect("/logout");
            }
            else
            {
                Response.Redirect("/");

            }

        }

        private string ChangePasswordBasedOnUser(string message, string username)
        {
            if (!String.IsNullOrWhiteSpace(username))
            {
                string oldPassword = TextBoxPasswordOld.Text;
                string newPassword = TextBoxPasswordNew.Text;
                bool updated = ChangePassword(oldPassword, newPassword, ref message, username);

                if (updated)
                {
                    loginSvc.DeleteChangePasswordHistory(username);
                    PanelPasswordExpiered.Visible = false;
                    PanelSuccess.Visible = true;
                    PanelEdit.Visible = false;
                    HyperLinkContinue.Text = SuccessMessage;
                    HyperLinkContinue.Visible = true;
                    errorMessage.Visible = true;
                    HyperLinkContinue.NavigateUrl = LogOnPage;
                    literalMessagePassword.Text = string.Empty;

                    //Logoff the user
                    LogOffUser();

                    //Login user with new 
                    SitecoreAccess.LogOn(username, newPassword, false);
                    SitecoreUserProfileHelper.AddItem("0", "IsUserPasswordChanged");

                }
                else
                {
                    HyperLinkContinue.Visible = false;
                    errorMessage.Visible = true;
                    literalMessagePassword.Text = message;
                }


            }
            else
            {
                HyperLinkContinue.Visible = false;
                errorMessage.Visible = true;
                literalMessagePassword.Text = PasswordAlreadyChanged;
            }
            return message;
        }




        /// <summary>
        /// Update password
        /// </summary>
        /// <param name="oldPassword">old password</param>
        /// <param name="newPassword">new password</param>
        /// <param name="message">Ref Message</param>
        /// <returns></returns>
        private bool ChangePassword(string oldPassword, string newPassword, ref string message, string username)
        {
            try
            {
                //Change Password
                if (SitecoreAccess.UpdatePassword(username, oldPassword, newPassword))
                {
                    message = SuccessMessage;
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (AuthenticationException)
            {
                message = FailierMessage;
            }
            catch (ArgumentException)
            {
                message = PasswordNotStrong;
            }
            catch (PasswordInHistoryException)
            {
                message = PasswordInHistory;
            }
            catch (Exception ex)
            {
                message = FailierMessage;

                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
            return false;
        }

        /// <summary>
        /// Check password expireation
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private bool HasPasswordExpired(MembershipUser username)
        {

            return username.LastPasswordChangedDate.ToLocalTime().Add(new TimeSpan(90, 0, 0, 0)) <= DateTime.Now;
        }

        /// <summary>
        /// Domain User
        /// </summary>
        //private static MembershipUser DomainUser
        //{
        //    get
        //    {
        //        if (Sitecore.Context.User != null)
        //        {
        //            return Membership.GetUser(Sitecore.Context.User.Name);
        //        }
        //        return null;
        //    }
        //}


        /// <summary>
        /// Logout the user
        /// </summary>
        private static void LogOffUser()
        {
            SitecoreUserProfileHelper.ClearCustomProperty();
            AuthenticationManager.Logout();

        }

        /// <summary>
        /// Initialize Page
        /// </summary>
        private void InitPage()
        {

            try
            {
                Item currentItem = Sitecore.Context.Item;
                //label
                this.HyperLinkContinue.Text = SitecoreFieldsHelper.GetValue(currentItem, "Success", "Submit");

                //Buttons
                this.ButtonEditPassword.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Submit", "Submit");

                //Messages
                this.SuccessMessage = SitecoreFieldsHelper.GetValue(currentItem, "Success", "Password Has Been Changed. Click to Continue");
                this.FailierMessage = SitecoreFieldsHelper.GetValue(currentItem, "Incorrect Current Password", "Old Password Is Incorrect");
                this.PasswordInHistory = SitecoreFieldsHelper.GetValue(currentItem, "PasswordInHistory", "New Password is in Password History Records");
                this.RegularExpressionValidatorPassword.ErrorMessage = PasswordNotStrong = SitecoreFieldsHelper.GetValue(currentItem, "password Not Match Regular Expression", RegularExpressionValidatorPassword.ErrorMessage);
                this.CompareValidatorRePassword.ErrorMessage = SitecoreFieldsHelper.GetValue(currentItem, "RePassword Not Match", CompareValidatorRePassword.ErrorMessage);
                this.PasswordAlreadyChanged = SitecoreFieldsHelper.GetValue(currentItem, "AlreadyChanged");
                //Links
                string redirectUrl = string.Empty;

                Item itm = Sitecore.Context.Database.GetItem(Sitecore.Context.Site.StartPath);
                if (itm != null)
                    redirectUrl = LinkManager.GetItemUrl(itm);
                this.LogOnPage = redirectUrl;

                //Set the strong password Regex
                string passwordLength = @"{" + Membership.MinRequiredPasswordLength.ToString() + @",}$";
                string strongPasswordRegex = Membership.PasswordStrengthRegularExpression.Replace(@"*$", passwordLength);

                if (!string.IsNullOrEmpty(strongPasswordRegex))
                {
                    //Set Regex
                    this.RegularExpressionValidatorPassword.ValidationExpression = strongPasswordRegex;
                }
                else
                {
                    this.RegularExpressionValidatorPassword.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }

        }
    }
}