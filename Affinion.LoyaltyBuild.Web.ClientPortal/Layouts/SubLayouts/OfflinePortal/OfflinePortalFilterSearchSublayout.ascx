﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalFilterSearchSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalFilterSearchSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Sitecore.Data.Items" %>
<%@ Import Namespace="Affinion.LoyaltyBuild.Common.Utilities" %>

<script>

    function activate_accordion(course_id) {
        var active_item;
        if (course_id == false) {
            active_item = false;
        } else {
            active_item = $('.accordion div[id="course-' + course_id + '"]').attr('active');
        }

        $(".accordion").accordion({
            collapsible: true,
            active: parseInt(active_item),
            heightStyle: "content",
            icons: {
                "header": "ui-icon-plus",
                "activeHeader": "ui-icon-minus"
            }
        });

        $('.tbl-calendar thead tr').removeAttr("style");
    }

</script>

<style type="text/css">
    .noText label 
{
    display: none;
}
</style>

<div class="col-xs-12 no-padding">

    <div class="advancedSearchLink margin-common-inner-box">
        <div class="accordion">
            <div id="course-25"><sc:text field="FilterByThemesFacilities" id="FilterByThemesFacilitiesLabel" runat="server" /></div>

            <div class="theme-facility-inner alert alert-info ">

                <%--Themes Section--%>
                <asp:Repeater ID="ThemesRepeater" runat="server">
                    <HeaderTemplate>
                        <div class="theme-common">
                        <div class="col-btn-list-fs">
                            <button type="button" class="btn theme width-full text-left btn-common" data-toggle="collapse" data-target="#tf-icon1">
                               <sc:text field="Themes" id="ThemesLabel" runat="server" />
                            </button>
                        </div>
                        <div id="tf-icon1" class="collapse in">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="row">
                            <div class="col-xs-9">
                                <label class="checkbox-inline">
                                    <asp:CheckBox ID="themesCheckBox" runat="server"  EnableViewState="true"   AutoPostBack="false" Text ='<%#SitecoreFieldsHelper.GetValue((Item)Container.DataItem, "Name") %>' CssClass="noText"/> 
                                    <sc:Text Field="Name"  Item="<%#(Item)Container.DataItem %>" runat="server" />
                                </label>
                            </div>
                        </div>
                    </ItemTemplate>
                    <FooterTemplate>
                        </div>
                        </div>
                    </FooterTemplate>
                </asp:Repeater>
                
                <%--Facilities Section--%>                
                <asp:Repeater ID="FacilitiesRepeater" runat="server">
                    <HeaderTemplate>
                        <div class="theme-common">
                        <div class="col-btn-list-fs">
                            <button type="button" class="btn facilities width-full text-left btn-common" data-toggle="collapse" data-target="#tf-icon2">
                               <sc:text field="Facilities" id="FacilitiesLabel" runat="server" />
                            </button>
                        </div>
                        <div id="tf-icon2" class="collapse in">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="row">
                            <div class="col-xs-9">
                                <label class="checkbox-inline">
                                        <asp:CheckBox ID="facilitiesCheckBox" runat="server"  EnableViewState="true"  AutoPostBack="false" Text ='<%#SitecoreFieldsHelper.GetValue((Item)Container.DataItem, "Name") %>' CssClass="noText"  />
                                    <sc:Text Field="Name"  Item="<%#(Item)Container.DataItem %>" runat="server" />
                                </label>
                            </div>
                        </div>
                    </ItemTemplate>
                    <FooterTemplate>
                        </div>
                        </div>
                    </FooterTemplate>
                </asp:Repeater>


                <%--Experiences Section--%>
                <asp:Repeater ID="ExperienceRepeater" runat="server">
                    <HeaderTemplate>
                        <div class="theme-common">
                        <div class="col-btn-list-fs">
                            <button type="button" class="btn facilities width-full text-left btn-common" data-toggle="collapse" data-target="#tf-icon2">
                                <sc:text field="Experience" id="ExperienceLabel" runat="server" />
                            </button>
                        </div>
                        <div id="tf-icon2" class="collapse in">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="row">
                            <div class="col-xs-9">
                                <label class="checkbox-inline">
                                        <asp:CheckBox ID="experienceCheckBox" runat="server"  EnableViewState="true"  AutoPostBack="false" Text ='<%#SitecoreFieldsHelper.GetValue((Item)Container.DataItem, "Name") %>' CssClass="noText"  />
                                    <sc:Text Field="Name"  Item="<%#(Item)Container.DataItem %>" runat="server" />
                                </label>
                            </div>
                        </div>
                    </ItemTemplate>
                        <FooterTemplate>
                        </div>
                        </div>
                    </FooterTemplate>
                </asp:Repeater>


                <%--Star Ranking Section--%>
                <asp:Repeater ID="StarRankingRepeater" runat="server">
                    <HeaderTemplate>
                        <div class="theme-common">
                        <div class="col-btn-list-fs">
                            <button type="button" class="btn facilities width-full text-left btn-common" data-toggle="collapse" data-target="#tf-icon4">
                                <sc:text field="StarRanking" id="StarRankingLabel" runat="server" />
                            </button>
                        </div>
                        <div id="tf-icon4" class="collapse in">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="row">
                            <div class="col-xs-9">
                                <label class="checkbox-inline">
                                    <asp:CheckBox ID="starCheckBox" runat="server" EnableViewState="true"  AutoPostBack="false" Text ='<%#Eval("Name") %>' CssClass="noText" />
                                    <sc:Text Field="Name"  Item="<%#(Item)Container.DataItem %>" runat="server" />
                                       
                                </label>
                            </div>
                        </div>
                    </ItemTemplate>
                    <FooterTemplate>
                        </div>
                        </div>
                    </FooterTemplate>
                </asp:Repeater>

                
                <div class="col-xs-12">
                    <asp:Button id="filterButton" CssClass="btn btn-default add-break" runat="server" OnClick="ButtonFilter_Click"  />                    
                </div>

            </div>

        </div>
    </div>
</div>

<script src="scripts/navigation.js" type="text/javaScript"></script>
<script type="text/javascript">
    $(document).ready(function () {

        activate_accordion(40);
        jQuery("table tr:even").css({ "background-color": "#fafefe", "border-top": "solid 1px #cff2f6", "border-bottom": "solid 1px #cff2f6" });
        jQuery("table tr:odd").css("background-color", "#fff");

    });
</script>