﻿using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using NHibernate;
using NHibernate.Transform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2.Queries;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Queries
{
    public class AvailabilityByDateQuery : ICannedQuery<LBAvailaiblity>
    {
        /// <summary>
        /// StartDate
        /// </summary>
        private readonly int _startDate;

        /// <summary>
        /// EndDate
        /// </summary>
        private readonly int _endDate;

        /// <summary>
        /// Variant SKU
        /// </summary>
        private readonly string _variantSKU;

        /// <summary>
        /// AvailabilityByDateQuery
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        public AvailabilityByDateQuery(string variantSKU , int startDate, int endDate)
        {
            _startDate = startDate;
            _endDate = endDate;
            _variantSKU = variantSKU;
        }

        /// <summary>
        /// IEnumerable type CampaignItem
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public IEnumerable<LBAvailaiblity> Execute(ISession session)
        {
            try
            {
                return session.CreateSQLQuery("exec GetRoomAvaialibility :@VariantSKU, :@sdate , :@edate").SetParameter("@sdate", _startDate).SetParameter("@edate", _endDate).SetParameter("@VariantSKU", _variantSKU)
                .SetResultTransformer(Transformers.AliasToBean<LBAvailaiblity>())
                .List<LBAvailaiblity>().ToList();
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.MasterClassBusinessLogic, ex, this);
                return new List<LBAvailaiblity>().AsEnumerable();
            }

            return new List<LBAvailaiblity>();
        }
    }
}
