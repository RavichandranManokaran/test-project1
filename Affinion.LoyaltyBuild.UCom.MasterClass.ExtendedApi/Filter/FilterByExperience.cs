﻿using Affinion.LoyaltyBuild.UCom.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.Search.Facets;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi.Filter
{
    public class FilterByExperience : IHotelFilter
    {
        public bool IncludeHotel(UCommerce.Documents.Product product, SearchCriteria criteria)
        {
            if (string.IsNullOrEmpty(criteria.Experience))
            {
                return true;
            }
            if (criteria.Experience == "all")
            {
                return true;
            }
            if (!string.IsNullOrEmpty(criteria.Experience))
            {
                KeyValuePair<string, Dictionary<string, object>> experienceProperty = product.Properties.FirstOrDefault(x => x.Value.Any(y => y.Key == ProductDefinationConstant.Experience));
                if (experienceProperty.Value != null)
                {
                    foreach (var item in criteria.Experience.Split(','))
                    {
                        bool found = experienceProperty.Value.Any(x => x.Value.ToString().Contains(item));
                        if (!found)
                        {
                            return false;
                        }
                    }

                }
            }
            return false;
        }


        public UCommerce.Search.Facets.Facet FilterRecordFacet(SearchCriteria criteria)
        {
            if (criteria.Experience != "all" && !string.IsNullOrEmpty(criteria.Experience))
            {
                var facet = new Facet();
                facet.FacetValues = new List<FacetValue>();
                facet.Name = ProductDefinationConstant.Experience;

                foreach (var value in criteria.Experience.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    facet.FacetValues.Add(new FacetValue() { Value = value });
                }
                return facet;
            }
            return null;
        }
    }
}
