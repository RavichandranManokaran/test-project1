﻿using Affinion.LoyaltyBuild.Api.Search.Data;
using Affinion.LoyaltyBuild.BedBanks.General;
using Sitecore.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Affinion.LoyaltyBuild.Api.Search.Filters
{
    public class StarRankingFilter : ILoyaltyFilter, ISearchFilter, IBedBankFilter
    {
        private readonly SearchKey _key;

        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="key"></param>
        public StarRankingFilter(SearchKey key)
        {
            _key = key;
        }

        public void BuildSearch(ref UCom.MasterClass.ExtendedApi.SearchCriteria criteria)
        {
            if (!string.IsNullOrWhiteSpace(_key.StarRanking))
            {
                criteria.StarRanking = _key.StarRanking.Replace(',', '|');
            }
        }

        public void BuildbbSearch(ref BBsearchCriteria criteria)
        {
            if (!string.IsNullOrWhiteSpace(_key.StarRanking))
            {
                criteria.StarRanking = HttpUtility.UrlDecode(_key.StarRanking).Replace(',', '|');
            }
        }
    }
}
