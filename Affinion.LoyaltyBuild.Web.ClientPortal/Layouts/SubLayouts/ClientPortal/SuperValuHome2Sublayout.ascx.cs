﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SuperValuHome2Sublayout.aspx.cs
/// Sub-system/Module:     Web.ClientPortal(VS project name)
/// Description:           sub layout of the home 2
/// </summary>
namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal
{
    using System;

    public partial class SuperValuHome2Sublayout : System.Web.UI.UserControl
    {
        /// <summary>
        /// Page load
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
        }
    }
}