﻿using Affinion.LoyaltyBuild.BedBanks.Troika;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Model;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Message.Interfaces;
using System;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Diagnostics;
using Affinion.LoyaltyBuild.BedBanks.General;
using System.Collections.Specialized;

namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Message
{
    [Serializable()]
    [XmlType(TypeName = "PreBookRequest")]
    public class JtPreBookRequest : JtRequest, IJtRequest
    {
        public JtBookingDetailsPreBook BookingDetails { get; set; }

        //Parameterless Constructor - Needed for XML Serialization
        private JtPreBookRequest() { }
        BedBanksSettings bbsettings;
        //The real constructor
        public JtPreBookRequest(string pPropertyID, string pArrivalDate, string pDuration, string pRoomToken, bool pIsDirect, string pMealBasisID, string pRoomInfo, BedBanksSettings bbs)
        {
            this.bbsettings = bbs;
            JtBookingDetailsPreBook bookingDetails = new JtBookingDetailsPreBook
            {
                PropertyID = pPropertyID,
                ArrivalDate = pArrivalDate,
                Duration = pDuration
            };

            //Split the rooms info down to an array of rooms
            string[] roomsInfoArray = pRoomInfo.Split(',');

            string numberAdults = roomsInfoArray[0];
            string numberChildren = roomsInfoArray[1];
            string numberInfants = roomsInfoArray[2].TrimEnd('|');

            List<JtChildAge> childrenAges = new List<JtChildAge>();

            if (int.Parse(numberChildren) > 0)
            {
                string[] strChildrenAges = roomsInfoArray[3].Split('/');

                foreach (string ca in strChildrenAges)
                {
                    JtChildAge childAge = new JtChildAge
                    {
                        Age = ca
                    };
                    childrenAges.Add(childAge);
                }
            }

            List<RoomBooking> roomBookings = new List<RoomBooking>();
            RoomBooking roomOne = new RoomBooking
            {
                MealBasisID = pMealBasisID,
                Adults = roomsInfoArray[0],
                Children = roomsInfoArray[1],
                Infants = roomsInfoArray[2],
                ChildAges = childrenAges
            };

            if (pIsDirect)
            {
                roomOne.PropertyRoomTypeID = pRoomToken;
            }
            else {
                roomOne.BookingToken = pRoomToken;
            }

            roomBookings.Add(roomOne);
            bookingDetails.RoomBookings = roomBookings;

            int curr;
            if (bbs.BedBanksActualCurrency != null)
            {
                int.TryParse(bbs.BedBanksActualCurrency.BedBanksCurrencyId, out curr);
                LoginDetails = JtController.GenerateLoginDetails(curr, bbs.BedBanksUserName, bbs.BedBanksPassword);
            }
            else
            {
                int.TryParse(bbs.BedBanksDefaultCurrency.BedBanksCurrencyId, out curr);
                LoginDetails = JtController.GenerateLoginDetails(curr, bbs.BedBanksUserName, bbs.BedBanksPassword);
            }
            BookingDetails = bookingDetails;
        }

        public IJtResponse Fire()
        {
            Stopwatch apiTimer = Stopwatch.StartNew();
            string response = JtController.PerformApiRequest(this);
            apiTimer.Stop();

            //READ THE RESULTS
            XmlRootAttribute xRoot = new XmlRootAttribute
            {
                ElementName = "PreBookResponse",
                IsNullable = true
            };

            XmlSerializer serializer = new XmlSerializer(typeof(JtPreBookResponse), xRoot);

            JtPreBookResponse pbRes;

            try
            {
                MemoryStream memStream = new MemoryStream(Encoding.UTF8.GetBytes(response));
                pbRes = (JtPreBookResponse)serializer.Deserialize(memStream);
            }
            catch (Exception fnfExc)
            {
                NameValueCollection exception = new NameValueCollection();
                exception.Add(Constants.ExceptionMessage, fnfExc.Message);
                exception.Add(Constants.ExceptionFlow, Constants.PrebookRequest);
                SendExceptionMail(exception, bbsettings.ClientItemId);
                throw new Exception(fnfExc.Message);
            }

            pbRes.ResponseTime = apiTimer.ElapsedMilliseconds;
            TroikaDataController.InsertApiResponseTime(1, "PreBook", pbRes.ResponseTime);

            return pbRes;
        }
    }
}
