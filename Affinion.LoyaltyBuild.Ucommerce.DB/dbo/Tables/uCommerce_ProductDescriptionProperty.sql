﻿CREATE TABLE [dbo].[uCommerce_ProductDescriptionProperty] (
    [ProductDescriptionPropertyId] INT            IDENTITY (1, 1) NOT NULL,
    [ProductDescriptionId]         INT            NOT NULL,
    [ProductDefinitionFieldId]     INT            NOT NULL,
    [Value]                        NVARCHAR (MAX) NULL,
    CONSTRAINT [uCommerce_PK_ProductDescriptionProperty] PRIMARY KEY CLUSTERED ([ProductDescriptionPropertyId] ASC),
    CONSTRAINT [uCommerce_FK_ProductDescriptionProperty_ProductDefinitionField] FOREIGN KEY ([ProductDefinitionFieldId]) REFERENCES [dbo].[uCommerce_ProductDefinitionField] ([ProductDefinitionFieldId]),
    CONSTRAINT [uCommerce_FK_ProductDescriptionProperty_ProductDescription] FOREIGN KEY ([ProductDescriptionId]) REFERENCES [dbo].[uCommerce_ProductDescription] ([ProductDescriptionId]),
    CONSTRAINT [ProductDescriptionProperty_Unique_ProductDescriptionId_ProductDefinitionFieldId] UNIQUE NONCLUSTERED ([ProductDescriptionId] ASC, [ProductDefinitionFieldId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [uCommerce_NonClusteredIndex_ProductDescriptionId]
    ON [dbo].[uCommerce_ProductDescriptionProperty]([ProductDescriptionId] ASC);

