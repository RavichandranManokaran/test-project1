﻿<%@ control language="C#" autoeventwireup="true" codebehind="FAQDetails.ascx.cs" inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.FAQs.FAQDetails" %>
<%@ import namespace="Sitecore.Links" %>
<%@ import namespace="Sitecore.Data.Items" %>


<div class="body-container">
    <div class="container">
        <div class="form-outer">
            <div class="col-md-12">
                <div class="panel panel-default" style="border: none;">
                    <div class="container">
                        <h3 class="title-text font-weight-bold"><%=Title%></asp:Label></h3>
                        <%if(FaqList!=null){ %>
                        <%foreach(var item in FaqList) {%>
                        <div class="content_bg_inner_box alert-warning">
                            <div class="title-text font-weight-bold"><%=item.Question %></div>
                            <div><%=item.Answer%></div>
                        </div>
                        <%}%>
                        <%} %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
