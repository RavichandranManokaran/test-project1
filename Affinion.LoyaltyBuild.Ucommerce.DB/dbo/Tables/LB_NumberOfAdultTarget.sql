﻿CREATE TABLE [dbo].[LB_NumberOfAdultTarget] (
    [NumberOfAdultTargetId] INT NOT NULL,
    [NumberOfAdult]         INT NOT NULL,
    CONSTRAINT [PK_uCommerce_NumberOfAdult] PRIMARY KEY CLUSTERED ([NumberOfAdultTargetId] ASC),
    CONSTRAINT [FK_uCommerce_NumberOfAdultTarget_uCommerce_Target] FOREIGN KEY ([NumberOfAdultTargetId]) REFERENCES [dbo].[uCommerce_Target] ([TargetId])
);

