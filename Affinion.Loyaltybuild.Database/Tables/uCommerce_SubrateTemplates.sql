﻿
CREATE TABLE [dbo].[uCommerce_SubrateTemplates](
	[SubrateTemplateID] [int] IDENTITY(1,1) NOT NULL,
	[SubrateTemplateCode] [nvarchar](50) NOT NULL,
	[SubrateTemplate] [nvarchar](250) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdateOn] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_uCommerce_SubrateTemplates] PRIMARY KEY CLUSTERED 
(
	[SubrateTemplateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[uCommerce_SubrateTemplates] ADD  CONSTRAINT [DF_uCommerce_SubrateTemplates_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO

ALTER TABLE [dbo].[uCommerce_SubrateTemplates] ADD  CONSTRAINT [DF_uCommerce_SubrateTemplates_UpdateOn]  DEFAULT (getdate()) FOR [UpdateOn]
GO