﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SuperValuTopBarSublayout.ascx.cs
/// Sub-system/Module:     Web.ClientPortal(VS project name)
/// Description:           Sublayout for top bar containing logo, top links, etc.
/// </summary>
namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts
{
    using System;

    /// <summary>
    /// Sublayout for top bar containing logo, top links, etc.
    /// </summary>
    public partial class SuperValuTopBarSublayout : System.Web.UI.UserControl
    {
        /// <summary>
        /// This function includes the code which needs to run on page load
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}