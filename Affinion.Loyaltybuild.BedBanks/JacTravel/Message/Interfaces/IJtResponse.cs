﻿using Affinion.LoyaltyBuild.BedBanks.JacTravel.Model;

namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Message.Interfaces
{
    public interface IJtResponse
    {
        ReturnStatus ReturnStatus { get; set; }
    }
}
