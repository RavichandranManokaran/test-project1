﻿CREATE TABLE [dbo].[uCommerce_OrderLine](
	[OrderLineId] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [int] NOT NULL,
	[Sku] [nvarchar](512) NOT NULL,
	[ProductName] [nvarchar](512) NOT NULL,
	[Price] [money] NOT NULL,
	[Quantity] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[Discount] [money] NOT NULL,
	[VAT] [money] NOT NULL,
	[Total] [money] NULL,
	[VATRate] [money] NOT NULL,
	[VariantSku] [nvarchar](512) NULL,
	[ShipmentId] [int] NULL,
	[UnitDiscount] [money] NULL,
	[CreatedBy] [nvarchar](255) NULL,
	CONSTRAINT [uCommerce_PK_OrderLine] PRIMARY KEY CLUSTERED ([OrderLineId] ASC)
)