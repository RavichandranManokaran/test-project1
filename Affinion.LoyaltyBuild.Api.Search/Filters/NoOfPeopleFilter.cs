﻿using Affinion.LoyaltyBuild.Api.Search.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Api.Search.Filters
{
    class NoOfPeopleFilter : ISearchFilter, ILoyaltyFilter, IBedBankFilter
    {
        private readonly SearchKey _key;


        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="key"></param>
        public NoOfPeopleFilter(SearchKey key)
        {
            _key = key;
        }

        /// <summary>
        /// Build LoyaltyBuild search criteria
        /// </summary>
        /// <param name="criteria"></param>
        public void BuildSearch(ref UCom.MasterClass.ExtendedApi.SearchCriteria criteria)
        {
            if (_key.NoOfAdults != null)
            {
                criteria.NumberOfAdult = (int)_key.NoOfAdults;
            }
            if (_key.NoOfChildren != null)
            {
                criteria.NumberOfChild = (int)_key.NoOfChildren;
            }
        }

        /// <summary>
        /// Build Bed Bank Search Criteria
        /// </summary>
        /// <param name="criteria"></param>
        public void BuildbbSearch(ref BedBanks.General.BBsearchCriteria criteria)
        {
            if (_key.NoOfRooms > 0)
            {
                int noOfRooms = (int)_key.NoOfRooms;
                int noOfAdults = (int)(_key.NoOfAdults ?? 0);
                int noOfChildren = (int)(_key.NoOfChildren ?? 0);

                string[] childAge = !string.IsNullOrEmpty(_key.ChildAge) ? _key.ChildAge.Split(',').ToArray() : null;
                var roomInfo = new List<string>();

                int childCount = 0;
                
                if (noOfRooms > 0 && noOfAdults > 0)
                {
                    for (int roomCount = noOfRooms; roomCount > 0; roomCount--)
                    {
                        int roomAdultValue;
                        int.TryParse(Math.Ceiling(decimal.Parse((noOfAdults / roomCount).ToString())).ToString(), out roomAdultValue);
                        int roomChildValue = 0;
                        string childAges = string.Empty;

                        if (noOfChildren > 0 && childAge != null)
                        {
                            int.TryParse(Math.Ceiling(decimal.Parse((noOfChildren / roomCount).ToString())).ToString(), out roomChildValue);
                            childAges = "," + string.Join("/", childAge.Skip(childCount).Take(roomChildValue));
                            childCount += roomChildValue;
                        }
                        roomInfo.Add(string.Format("{0},{1},0{2}", roomAdultValue.ToString(), roomChildValue.ToString(), childAges));

                        noOfAdults = noOfAdults - roomAdultValue;
                        noOfChildren = noOfChildren - roomChildValue;
                    }
                }

                criteria.RoomInfo = string.Join("|", roomInfo);
            }
        }
    }
}
