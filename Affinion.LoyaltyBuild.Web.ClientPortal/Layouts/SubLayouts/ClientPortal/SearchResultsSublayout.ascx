﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchResultsSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal.SearchResultsSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Sitecore.Data.Items" %>
<%@ Import Namespace="Affinion.LoyaltyBuild.Search" %>
<%@ Import Namespace="Affinion.LoyaltyBuild.Search.Data" %>
<div class="col-md-12 no-padding">
    <div class="search-result-top">
        <div class="search-result-top">        
             <p>
            <asp:Literal ID="litSearchText" runat="server" /></p>
    </div>
    </div>
</div>
<div class="col-md-12 no-padding" id="SortSection" runat="server">
    <div class="county-filter col-xs-12 alert alert-info accent6-bg" id="CountySection" runat="server">
        <div class="col-xs-12 col-md-6">
            <h4 class="details-header no-margin padding-top5">
                <sc:text id="CountryFilterText" runat="server" />
            </h4>
        </div>
        <div class="col-xs-12 col-md-6">
            <div class="select-loc" aria-label="..." role="group">
                <asp:DropDownList CssClass="form-control" ID="CountyDropDown" ClientIDMode="Static" runat="server" OnSelectedIndexChanged="CountyDropDown_SelectedIndexChanged" AutoPostBack="true" EnableViewState="true" />
            </div>
        </div>
    </div>
    <div class="search-nav">
        <ul class="nav nav-tabs">
            <li class="sort-by">Sort by</li>
            <li role="presentation" class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="<%=this.SortLink(SortMode.SupplierName, "0") %>" role="button" aria-haspopup="true" aria-expanded="false" id="SupplierName">
                    <sc:text field="SortByHotelNameLabel" runat="server" />
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="<%=this.SortLink(SortMode.SupplierName, "0") %>" id="SupplierNameAsc">
                            <sc:text id="AscendingText" runat="server" />
                        </a>
                    </li>
                    <li>
                        <a href="<%=this.SortLink(SortMode.SupplierName, "1") %>" id="SupplierNameDes">
                            <sc:text id="DescendingText" runat="server" />
                        </a>
                    </li>
                </ul>
            </li>
            <li role="presentation" class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="<%=this.SortLink(SortMode.Price, "0") %>" role="button" aria-haspopup="true" aria-expanded="false" id="Price">
                    <sc:text field="SortByPriceLable" runat="server" />
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="<%=this.SortLink(SortMode.Price, "0") %>" id="PriceAsc">
                            <sc:text id="PriceLowToHighText" runat="server" />
                        </a>
                    </li>
                    <li>
                        <a href="<%=this.SortLink(SortMode.Price, "1") %>" id="PriceDes">
                            <sc:text id="PriceHighToLowText" runat="server" />
                        </a>
                    </li>
                </ul>
            </li>

            <li role="presentation" class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="<%=this.SortLink(SortMode.Rate, "0") %>" role="button" aria-haspopup="true" aria-expanded="false" id="Rate">
                    <sc:text field="SortByRateLable" runat="server" />
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="<%=this.SortLink(SortMode.Rate, "0") %>" id="RateAsc">
                            <sc:text id="RateLowToHighText" runat="server" />
                        </a>
                    </li>
                    <li>
                        <a href="<%=this.SortLink(SortMode.Rate, "1") %>" id="RateDes">
                            <sc:text id="RateHighToLowText" runat="server" />
                        </a>
                    </li>
                </ul>
            </li>
            <li role="presentation" class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="<%=this.SortLink(SortMode.Recommend, "0") %>" role="button" aria-haspopup="true" aria-expanded="false" id="Recommend">
                    <sc:text field="SortByRecommendLabel" runat="server" />
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="<%=this.SortLink(SortMode.Recommend, "0") %>" id="RecommendAsc">
                            <sc:text id="RecommendLowToHighText" runat="server" />
                        </a>
                    </li>
                    <li>
                        <a href="<%=this.SortLink(SortMode.Recommend, "1") %>" id="RecommendDes">
                            <sc:text id="RecommendHighToLowText" runat="server" />
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<asp:Literal ID="Literal1test" runat="server" />
<asp:Repeater ID="SearchResultsRepeater" runat="server">
    <ItemTemplate>
            <div class="col-md-12 no-padding">
                <div class="search-info">
                    <div class="col-xs-12 col-md-4 no-padding-left no-padding-right no-padding-m">
                        <div class="search-pho">
                            <a href="<%#this.SupplierInformationPageLink((HotelSearchResultItem)Container.DataItem) %>">
                                <div id="LBImageDiv" runat="server">
                                    <sc:image field="IntroductionImage" item="<%#((HotelSearchResultItem)Container.DataItem).SitecoreItem %>" cssclass="img-responsive img-enlarge" parameters="mw=580" runat="server" />
                                </div>
                                <div id="BBImageDiv" runat="server">
                                    <asp:Image ID="imgBBIntroImage" CssClass="img-responsive img-enlarge" parameters="mw=580" runat="server" />
                                </div>
                            </a>
                        </div>
                        <div class="price" id="LowestPriceContainer" runat="server">
                            <p class="no-margin">
                                <asp:Literal ID="LowestOccupancyType" runat="server" />
                            </p>
                            <h4 class="no-margin">
                                <asp:Literal ID="StartingPrice" runat="server" />
                            </h4>
                        </div>
                        <div class="search-info-paymentdetail" id="FullPaymentDiv" runat="server">
                            <p><asp:literal ID="litFullPayment" runat="server"></asp:literal></p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-8 no-padding-m">
                        <div class="search-info-detail">
                            <div class="search-info-detail-top">
                                <div class="col-xs-12 no-padding-m">
                                    <a href="<%#this.SupplierInformationPageLink((HotelSearchResultItem)Container.DataItem) %>" class="title-list display-block-val">
                                        <div id="LBNameDiv" runat="server">
                                            <sc:text field="Name" item="<%#((HotelSearchResultItem)Container.DataItem).SitecoreItem %>" runat="server" />
                                        </div>
                                        <div id="BBNameDiv" runat="server">
                                            <asp:Literal ID="BBName" runat="server" />
                                        </div>
                                    </a>
                                    <div class="star-rank pull-left width-full">
                                        <asp:Repeater ID="StarRepeater" runat="server">
                                            <ItemTemplate>
                                                <span class="fa fa-star"></span>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                    <a href="#" class="pull-left" data-toggle="modal" data-target="#dialog-mapB" data-coordinates="<%#this.SupplierMapLocation((HotelSearchResultItem)Container.DataItem) %>" onclick="loadMap(this);"><i class="fa fa-map-marker icon-padding"></i></a>
                                    <h5 class="no-margin">
                                        <div id="LBTownDiv" runat="server">
                                            <sc:text field="Town" id="Town" item="<%#((HotelSearchResultItem)Container.DataItem).SitecoreItem %>" runat="server" />
                                            ,                                   
                                  <asp:Literal ID="CountryName" runat="server" />
                                        </div>
                                        <div id="BBTownDiv" runat="server">
                                            <asp:Literal ID="BBTown" runat="server" />
                                            <asp:Literal ID="CountryNameTxt" runat="server" />
                                        </div>
                                    </h5>
                                    </br>
                           <div class="search-info-description pull-left">
                               <p>
                                   <div id="LBOverviewDiv" runat="server">
                                       <sc:text field="Overview" item="<%#((HotelSearchResultItem)Container.DataItem).SitecoreItem %>" runat="server" />
                                   </div>
                                   <div id="BBOverviewDiv" runat="server">
                                       <asp:Literal ID="BBOverview" runat="server" />
                                   </div>
                               </p>
                           </div>
                                </div>
                                <div class="col-xs-12 col-md-8">
                                    <asp:Repeater ID="HotelFacilities" runat="server">
                                        <HeaderTemplate>
                                            <ul class="event">
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <li>
                                                <div id="LBFacilityDiv" runat="server">
                                                    <sc:link field="Link" id="FacilityLink1" runat="server" item="<%#(Item)Container.DataItem %>">
                                        <sc:Image Field="Icon" ID="FacilityImage1" runat="server" Item="<%#(Item)Container.DataItem %>" />
                                        </sc:link>
                                                </div>
                                                <div id="Div2" runat="server">
                                                    <asp:LinkButton ID="FacilityLink" runat="server" />
                                                    <asp:Image field="Icon" CssClass="img-responsive img-enlarge" parameters="mw=580" runat="server" />
                                                </div>
                                            </li>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </ul>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                                <div class="col-xs-12 col-md-4">
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="book-detail-hr">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="col-btn-list" id="SelectRooms" runat="server">
                                        <button type="button" class="btn btn-primary collapsed pull-right" data-toggle="collapse" data-target="#OfferDetails<%#Container.ItemIndex %>">
                                            <sc:text field="SelectRoomLabel" runat="server" />
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 hide-list-room no-padding">
                        <div id="OfferDetails<%#Container.ItemIndex %>" class="collapse">
                            <asp:Repeater ID="OffersList" runat="server" OnItemDataBound="OffersList_ItemDataBound">
                                <HeaderTemplate>
                                    <div class="show-price-list">
                                        <table class="room-type">
                                            <tbody>
                                                <tr style="border-top-style: solid; border-top-width: 1px; border-top-color: rgb(207, 242, 246); border-bottom-style: solid; border-bottom-width: 1px; border-bottom-color: rgb(207, 242, 246); background-color: rgb(250, 254, 254);">
                                                    <th>
                                                        <sc:text id="RoomTypesLabel" field="RoomTypesLabel" runat="server" />
                                                    </th>
                                                    <th>&nbsp;</th>
                                                    <th></th>
                                                    <th>
                                                        <sc:text id="PricePerBrakeLabel" field="PricePerBrakeLabel" runat="server" />
                                                    </th>
                                                    <th>
                                                        <sc:text id="RoomsLabel" field="RoomsLabel" runat="server" />
                                                    </th>
                                                    <th></th>
                                                </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr style="background-color: rgb(255, 255, 255);">
                                        <td><span class="type-room"><%#GetRoomName((ProviderInfo)Container.DataItem)%></span></td>
                                        <td><span class="text-red"></span></td>
                                        <td><span class="text-red"><%#GetTipText((ProviderInfo)Container.DataItem) %></span></td>
                                        <td><span><%#this.GetRoomPrice((ProviderInfo)Container.DataItem) %>
                                            <sc:text field="OfferPriceSuffixLabel" runat="server" />
                                        </span></td>
                                        <td>
                                            <div aria-label="..." role="group">
                                                <asp:DropDownList ID="drpAvailability" runat="server" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </td>
                                        <div>
                                               <asp:Label ID="btnvalidation" runat="server" Class="dummy"  />
                                        </div>
                                        <td>
                                            <asp:Button ID="ButtonAddToBasket1"   OnClick="ButtonAddToBasket_Click" CommandArgument='<%#GetRoomInformation((ProviderInfo)Container.DataItem) %>' class="btn btn-primary booking-margin-top btn-bg pull-left" runat="server" Text='<%#Affinion.LoyaltyBuild.Common.Utilities.SitecoreFieldsHelper.GetValue(Sitecore.Context.Item ,"AddToBasketLabel") %>' Visible="false" />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <tr style="background-color: rgb(255, 255, 255);">
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>

                                        <td>
                                            <asp:Button ID="ButtonAddToBasketOneClick"  OnClick="ButtonAddToBasketOneClick_Click" class="btn btn-primary booking-margin-top btn-bg pull-left width-full btn-basket" runat="server" Text='<%#Affinion.LoyaltyBuild.Common.Utilities.SitecoreFieldsHelper.GetValue(Sitecore.Context.Item ,"AddToBasketLabel") %>' />
                                        </td>
                                        <td></td>

                                        <%--<td></td>
                           <td></td>
                           <td></td>--%>
                                        <%--     <asp:Button ID="ButtonAddToBasket" OnClick="ButtonAddToBasket_Click" CommandArgument = '<%#Eval("OfferSku")+","+ Eval("UcommerceCurrencyDetails.CurrencyId")%>'  class="btn btn-primary booking-margin-top btn-bg pull-left" runat="server" Text="Add to Basket" />                                           --%>
                                        <%-- <button id="btnCart<%#Container.ItemIndex %>" class="btn btn-primary booking-margin-top btn-bg pull-left" type="button" onclick="ButtonAddToBasket('<%#Container.DataItem%>')">Add to Basket</button>--%>
                                    </tr>
                                    </tbody>
                        </table>
                        </div>
                        </FooterTemplate>
                     </asp:repeater>
                           
</div>
               </div>
               <%--<div class="row">
                   <div class="col-xs-12" id="AvailableAlert" runat="server">
                       <div id="availabilityMessage" runat="server" class="alert alert-danger msg-margin-top">                           
                       </div>
                   </div>
               </div>--%>
</div>
         </div>
</ItemTemplate>
</asp:Repeater>
<div class="col-xs-12 no-padding">
                    <div runat="server" id="divPager" visible="false">
                    </div>
                </div>
<script type="text/javascript" language="javascript">
    function ButtonAddToBasket(o) {
        alert('helloo');
        var btn = $('#btnCart');
        alert(btn.parentNode);
    }


    // Load the sort filter styles based on the selection
    jQuery(document).ready(function () {
        console.log("Loading filter style!");

        if (location.search.split('sortmode=').length > 1) {
            SortMode = location.search.split('sortmode=')[1].split('&')[0];
            isAscndingOrder = location.search.split('sortmode=')[1].split('&')[1].split('=')[1]

            if (SortMode != null) {
                //Supplier
                if ((SortMode.split('&')[0] == "SupplierName") && location.search.split('sortValue=')[1] == "0") {
                    jQuery("#SupplierNameAsc").addClass("active-price");
                    jQuery("#SupplierName").addClass("active-price");
                }

                if ((SortMode.split('&')[0] == "SupplierName") && location.search.split('sortValue=')[1] == "1") {
                    jQuery("#SupplierNameDes").addClass("active-price");
                    jQuery("#SupplierName").addClass("active-price");
                }

                // Price
                if ((SortMode.split('&')[0] == "Price") && location.search.split('sortValue=')[1] == "0") {
                    jQuery("#PriceAsc").addClass("active-price");
                    jQuery("#Price").addClass("active-price");
                }

                if ((SortMode.split('&')[0] == "Price") && location.search.split('sortValue=')[1] == "1") {
                    jQuery("#PriceDes").addClass("active-price");
                    jQuery("#Price").addClass("active-price");
                }


                // Rate
                if ((SortMode.split('&')[0] == "Stars") && location.search.split('sortValue=')[1] == "0") {
                    jQuery("#RateAsc").addClass("active-price");
                    jQuery("#Rate").addClass("active-price");
                }

                if ((SortMode.split('&')[0] == "Stars") && location.search.split('sortValue=')[1] == "1") {
                    jQuery("#RateDes").addClass("active-price");
                    jQuery("#Rate").addClass("active-price");
                }
                //Recommend
                if ((SortMode.split('&')[0] == "Recommend") && location.search.split('sortValue=')[1] == "0") {
                    jQuery("#RecommendAsc").addClass("active-price");
                    jQuery("#Recommend").addClass("active-price");
                }

                if ((SortMode.split('&')[0] == "Recommend") && location.search.split('sortValue=')[1] == "1") {
                    jQuery("#RecommendDes").addClass("active-price");
                    jQuery("#Recommend").addClass("active-price");
                }

                console.log("SortMode: " + SortMode + "  isAscndingOrder " + isAscndingOrder);
            }
        }

    });

</script>
<div class="modal fade" id="dialog-mapB" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><span class="fa fa-map-marker"></span>Map</h4>
            </div>
            <div class="modal-body">
                <div id="map_canvasB" class="large-gmap"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .dummy
    {
        display:none;
    }
</style>
<script type="text/javascript">

    $(function () {
        $("#<%=divPager.ClientID%>").pagination({
             hrefTextPrefix: "<%=BasePageUrl%>page=",
            items: <%=ResultSize.ToString()%>,
            itemsOnPage: <%=PageSize.ToString()%>,
            currentPage: <%=CurrentPage.ToString()%>,
            cssStyle: 'compact-theme'
        });        
    });

    //Store reference to the google map object
    var map;
    var storedCoordinates;

    function loadMap(obj) {
        if (obj) {
            var lati = 0;
            var long = 0;
            var coordinates = obj.getAttribute('data-coordinates');
            storedCoordinates = coordinates.split(',');

            lati = storedCoordinates[0];
            long = storedCoordinates[1];
            SupplierId = storedCoordinates[2];
            var mapOptions = {
                center: new google.maps.LatLng(lati, long),
                //center: coords,
                zoom: 6,
            };

            map = new google.maps.Map(document.getElementById("map_canvasB"), mapOptions);
            var marker;
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(lati, long),
                url: "/hotelinformation?id=" + SupplierId,
                map: map
            });

            google.maps.event.addListener(marker, 'click', function () {
                window.location.href = marker.url;
            });
        }
        function btnvalidation(){
            alert('hi');
            $('.dummy').cssStyle("display:block")
        }
        jQuery(document).ready(function () {

            jQuery('#dialog-mapB').on('shown.bs.modal', function () {

                if (navigator.geolocation) {
                    //navigator.geolocation.getCurrentPosition(initializeMap);
                    google.maps.event.trigger(map, "resize");
                    map.setCenter(new google.maps.LatLng(lati, long));
                    //loadMap();
                }
                else {
                    alert("Sorry, your browser does not support geolocation services.");
                }

            });
        });
    };


   
    <%--    $(document).ready(function () {
        $('.dummy').click(function () {
            Boolean setva=<%=btnvalidation()%>
            alert('Thank you');
        })
    });
</script>--%>

</script>
