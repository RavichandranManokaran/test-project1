﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.Availability;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.Availability.SupplierPortal.Controls;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;
using Affinion.LoyaltyBuild.UCom.Common.Extension;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Handler
{
    /// <summary>
    /// Summary description for RoomAvailibilityByDateRange
    /// </summary>
    public class RoomAvailibilityByDateRange : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                
                if (!string.IsNullOrWhiteSpace(SitecoreUserProfile.SelectedSKU))
                {
                    context.Response.ContentType = "text/plain";
                    string sDate = context.Request["startDate"];
                    string eDate = context.Request["endDate"];
                    string month = context.Request["month"];
                    if (!String.IsNullOrEmpty(sDate) && !String.IsNullOrEmpty(eDate))
                    {
                        DateTime startDate = sDate.ParseFormatDateTime();
                        DateTime endDate = eDate.ParseFormatDateTime();
                        context.Response.Write(LoadUserControl(startDate, endDate, Convert.ToInt32(month), context));
                        if (Convert.ToInt32(month) == 0)
                        {
                            SessionHelper.AddItem(startDate, SessionKeys.AvailabilityStartDate);
                            SessionHelper.AddItem(endDate, SessionKeys.AvailabilityEndDate);
                        }
                    }
                }
                else
                {
                    context.Response.Write("Please configure hotel in sitecore to manage room availability ");
                }

            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, new object());
                string data = ex.Message;
            }
        }



        protected static string LoadUserControl(DateTime startDate, DateTime endDate, int month, HttpContext context)
        {
            using (Page page = new Page())
            {
                StringBuilder s = new StringBuilder();

                List<ProviderAvialbilityData> avialbilityData = new List<ProviderAvialbilityData>();
                AvailabilityHelper staticDataForRoom = new AvailabilityHelper(context);
                avialbilityData = staticDataForRoom.RoomAvailibilityByDateRange(startDate, endDate);
                PlaceHolder f = new PlaceHolder();

                foreach (var item in avialbilityData)
                {
                    if (month != 0 && item.StatDate.GetMonthFromJulianFixed() != month)
                    {
                        continue;
                    }
                    using (StringWriter writer = new StringWriter(CultureInfo.CurrentCulture))
                    {
                        AvailabilityCalendarView avp = (AvailabilityCalendarView)page.LoadControl("~/Layouts/SubLayouts/SupplierPortal/Controls/Availability/AvailabilityCalendarView.ascx");
                        avp.currenItem = item;
                        f.Controls.Add(avp);
                        page.Controls.Add(f);
                        HttpContext.Current.Server.Execute(page, writer, false);
                        s.Clear();
                        s.Append(writer.ToString());
                    }
                }

                return s.ToString();
            }

        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}