﻿CREATE TABLE [dbo].[uCommerce_ProductReview] (
    [ProductReviewId]       INT            IDENTITY (1, 1) NOT NULL,
    [Rating]                INT            NULL,
    [CustomerId]            INT            NULL,
    [ProductCatalogGroupId] INT            NOT NULL,
    [CreatedOn]             DATETIME       NOT NULL,
    [ModifiedOn]            DATETIME       NOT NULL,
    [ModifiedBy]            NVARCHAR (50)  NULL,
    [CultureCode]           NVARCHAR (60)  NULL,
    [ReviewHeadline]        NVARCHAR (512) NULL,
    [ReviewText]            NVARCHAR (MAX) NULL,
    [ProductId]             INT            NOT NULL,
    [Ip]                    NVARCHAR (50)  NOT NULL,
    [ProductReviewStatusId] INT            NOT NULL,
    CONSTRAINT [PK_uCommerce_ProductReview] PRIMARY KEY CLUSTERED ([ProductReviewId] ASC),
    CONSTRAINT [FK_uCommerce_ProductReview_uCommerce_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[uCommerce_Customer] ([CustomerId]),
    CONSTRAINT [FK_uCommerce_ProductReview_uCommerce_Product] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[uCommerce_Product] ([ProductId]),
    CONSTRAINT [FK_uCommerce_ProductReview_uCommerce_ProductCatalogGroup] FOREIGN KEY ([ProductCatalogGroupId]) REFERENCES [dbo].[uCommerce_ProductCatalogGroup] ([ProductCatalogGroupId]),
    CONSTRAINT [FK_uCommerce_ProductReview_uCommerce_ProductReviewStatus] FOREIGN KEY ([ProductReviewStatusId]) REFERENCES [dbo].[uCommerce_ProductReviewStatus] ([ProductReviewStatusId])
);


GO
CREATE NONCLUSTERED INDEX [IX_uCommerce_ProductReview]
    ON [dbo].[uCommerce_ProductReview]([ProductId] ASC);

