﻿namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Affinion.LoyaltyBuild.Common;
    using Sitecore.Data.Items;
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.Loyaltybuild.BusinessLogic.Helper;
    using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
    using Sitecore.Data;

    public partial class OfflinePortalCustomerValidation : BaseSublayout
    {
        bool isATG = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            Item item = this.GetDataSource();
            this.isATG = SitecoreFieldsHelper.GetBoolean(item, Constants.IsATGFieldName);
            if (this.isATG)
            {
                this.EmailList1ConsentLabel.Item = item;
                this.EmailList2ConsentLabel.Item = item;
                this.InstructionText.Item = item;
                this.EmailValidationTitleText.Item = item;
            }
            else
            {
                this.FirstDescription.Item = item;
                this.SecondDescription.Item = item;
                this.ThirdDescription.Item = item;
            }
            SetControlVisibility(this.isATG);
            BindSitecoreData();
            BindSitecoreTexts();
        }

        protected void btnToPersonalDetail_Click(object sender, EventArgs e)
        {
            Item item = this.GetDataSource();
            bool isATG = SitecoreFieldsHelper.GetBoolean(item, Constants.IsATGFieldName);
            if (this.isATG)
            {
                //HandleMailLists(txtConfirmEmail.Text);
            }
            //TODO: replace hardcoded URL with template search of item and then redirect
            Response.Redirect("/customersearch?Email=" + txtConfirmEmail.Text);
        }

        /// <summary>
        /// Get ClientId 
        /// </summary>
        /// <returns></returns>
        private static string GetClientID()
        {
            int? orderId = BasketHelper.GetBasketOrderId();
            var clientId = BasketHelper.GetOrderClientId(orderId.Value);
            return clientId;
        }

        /// <summary>
        /// Get LoggedIn Customer ID
        /// </summary>
        /// <returns></returns>
        private static int CustomerID()
        {
            int UserId = 68;
            return UserId;
        }

        private void HandleMailLists(string email)
        {
            Item rootItem = Sitecore.Context.Database.GetItem(Sitecore.Context.Site.RootPath);

            try
            {
                if (EmailList1ConsentCheckBox.Checked && EmailList2ConsentCheckBox.Checked)
                {
                    uCommerceConnectionFactory.AddSubscribedCustomer(GetClientID(), CustomerID(), txtConfirmEmail.Text, Constants.ServiceAndPromotional);
                }

                else if (EmailList1ConsentCheckBox.Checked)
                {
                    //SubscribeHelper.CreateSubscription(email, rootItem, Constants.PromotionalMailfolder);
                    uCommerceConnectionFactory.AddSubscribedCustomer(GetClientID(), CustomerID(), txtConfirmEmail.Text, Constants.PromotionalMail);
                }
                else if (EmailList2ConsentCheckBox.Checked)
                {
                    //SubscribeHelper.CreateSubscription(email, rootItem, Constants.ServiceImprovementMailFolder);
                    uCommerceConnectionFactory.AddSubscribedCustomer(GetClientID(), CustomerID(), txtConfirmEmail.Text, Constants.ServiceImprovement);
                }
            }
            catch (ArgumentException exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, exception, this);
            }
        }

        private void SetControlVisibility(bool isATG)
        {
            if (isATG)
            {
                FirstDescriptionDiv.Visible = false;
                SecondDescriptionDiv.Visible = false;
                ThirdDescriptionDiv.Visible = false;
            }
            else
            {
                EmailList1ConsentCheckBox.Visible = false;
                EmailList2ConsentCheckBox.Visible = false;
                EmailListConsent1Div.Visible = false;
                EmailListConsent2Div.Visible = false;
            }
        }

        private void BindSitecoreData()
        {
            Item currentpageitem = Sitecore.Context.Item;
            SitecoreFieldsHelper.BindSitecoreText(currentpageitem, "email", email);
            SitecoreFieldsHelper.BindSitecoreText(currentpageitem, "confirmemail", confirmemail);
            SitecoreFieldsHelper.BindSitecoreText(currentpageitem, "btnpersonaldetails", btnpersonaldetails);
        }


        private void BindSitecoreTexts()
        {
            Item conditionalMessageItem = this.GetItemFromRenderingParameterPath("Conditional Messages Link");
            string emailisRequiredMessageFieldName = "Email is Required Message";
            string emailValidationMessageFieldName = "Email Validation Message";
            string emailMismatchMessageFieldName = "Email Mismatch Message";
            string emailconfirmMessageFieldName = "Confirm Email Message";
            SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, emailisRequiredMessageFieldName, txtEmailRequiredText);
            SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, emailValidationMessageFieldName, txtvalidEmailText);
            SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, emailMismatchMessageFieldName, txtEmailMismatchText);
            SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, emailconfirmMessageFieldName, txtConfirmEmailText);
        }
    }
}