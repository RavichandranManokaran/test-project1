﻿CREATE TABLE [dbo].[WorkflowHistory] (
    [ID]        UNIQUEIDENTIFIER CONSTRAINT [DF_WorkflowHistory_ID] DEFAULT (newid()) NOT NULL,
    [ItemID]    UNIQUEIDENTIFIER NOT NULL,
    [Language]  NVARCHAR (20)    NOT NULL,
    [Version]   INT              NOT NULL,
    [OldState]  NVARCHAR (200)   NOT NULL,
    [NewState]  NVARCHAR (200)   NOT NULL,
    [Text]      NVARCHAR (2000)  NOT NULL,
    [User]      NVARCHAR (100)   NOT NULL,
    [Date]      DATETIME         NOT NULL,
    [Sequence]  INT              IDENTITY (1, 1) NOT NULL,
    [DAC_Index] INT              NULL
);


GO
CREATE CLUSTERED INDEX [DAC_Clustered]
    ON [dbo].[WorkflowHistory]([DAC_Index] ASC);


GO
CREATE NONCLUSTERED INDEX [ndxItemID]
    ON [dbo].[WorkflowHistory]([ItemID] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [ndxID]
    ON [dbo].[WorkflowHistory]([ID] ASC);

