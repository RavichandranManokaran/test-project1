﻿using Affinion.LoyaltyBuild.Audit;
using NHibernate;
using NHibernate.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Marketing.Awards;
using UCommerce.Marketing.Targets;
using UCommerce.Security;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class AuditInsertListener : IPostInsertEventListener
    {
        private const string _noValueString = "*No Value*";

        public IUserService UserService { get; set; }

        public AuditInsertListener(IUserService userService)
        {
            this.UserService = userService;
        }


        public void OnPostInsert(PostInsertEvent insertEvent)
        {
            if (insertEvent.Entity is ITarget || insertEvent.Entity is IAward || insertEvent.Entity is CampaignItem || insertEvent.Entity is CampaignItemProperty)
            {
                StringBuilder stroldValue = new StringBuilder();
                StringBuilder strnewValue = new StringBuilder();


                var entityFullName = insertEvent.Entity.GetType().FullName;

                var propertyNames = insertEvent.Persister.PropertyNames;

                var session = insertEvent.Session.GetSession(EntityMode.Poco);

                foreach (var propName in propertyNames)
                {
                    stroldValue.Append(string.Format("\"{0}\":\"{1}\",", propName, _noValueString));
                    strnewValue.Append(string.Format("\"{0}\":\"{1}\",", propName, insertEvent.Persister.GetPropertyValue(insertEvent.Entity, propName, EntityMode.Poco)));
                }
                AuditTrail.Instance.Log(new AuditData()
                {
                    AuditName = AuditNames.Promotion,
                    Message = string.Empty,
                    NewValue = string.Format("{{{0}}}", strnewValue.ToString()),
                    ObjectId = insertEvent.Id.ToString(),
                    ObjectType = AuditObjectType.Ucommerce,
                    Operation = AuditOperations.Add,
                    OldValue = string.Format("{{{0}}}", stroldValue.ToString()),
                    Remarks = strnewValue.ToString(),
                    Username = this.UserService.GetCurrentUserName(),
                    Section = AuditSection.AdminPortalUcommerce

                });
                session.Flush();
            }

        }
    }
}
