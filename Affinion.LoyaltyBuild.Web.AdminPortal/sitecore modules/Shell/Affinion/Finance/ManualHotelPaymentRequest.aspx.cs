﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using System.Globalization;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Sitecore.Data.Items;
using Affinion.LoyaltyBuild.Common;
using Sitecore.Data;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.BedBanks.General;

namespace Affinion.LoyaltyBuild.AdminPortal.sitecore_modules.Shell.Affinion.Finance
{
    public partial class ManualHotelPaymentRequest : System.Web.UI.Page
    {
        IReportsService iReportsService = new ReportsService();
        SearchCriteria objSearchCriteria = new SearchCriteria();
        static APInvoice objAPInvoice = null;
        APOraclePayment objAPOraclePayment = null;
        Item[] providers = null;
        Item oracleConfiguration = null;
        string ProviderPath = Constants.ProviderPath;
        Sitecore.Data.Database sitecoreDatabase = Sitecore.Configuration.Factory.GetDatabase("master");
        BedBanksSettings bedBankData = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                divRetrieveBookingDetails.Visible = false;
            }
            this.lblMessage.Text = string.Empty;
        }

        protected void RetrieveBooking_Click(object sender, EventArgs e)
        {
            RetrieveBookingDetails();
        }

        protected void ManualPaymentRequest_Click(object sender, EventArgs e)
        {
            try
            {
                iReportsService = new ReportsService();
                objAPOraclePayment = new APOraclePayment();
                List<APOraclePayment> lstAPOraclePayment = new List<APOraclePayment>();

                if (objAPInvoice != null)
                {
                    objAPOraclePayment.ProviderID = objAPInvoice.ProviderID;
                    objAPOraclePayment.OrderId = objAPInvoice.OrderID;
                    objAPOraclePayment.OrderLineID = objAPInvoice.OrderLineID;
                    objAPOraclePayment.OrderDate = objAPInvoice.BookingDate;
                    objAPOraclePayment.CurrencyCode = objAPInvoice.Currency;
                    objAPOraclePayment.CurrencyID = objAPInvoice.CurrencyID;
                    objAPOraclePayment.EuroRate = 0;
                    objAPOraclePayment.CustomerID = objAPInvoice.CustomerID;
                    objAPOraclePayment.SuggestedAmount = objAPInvoice.SuggestedAmount;
                    objAPOraclePayment.PostedAmount = objAPInvoice.SuggestedAmount;
                    objAPOraclePayment.ClientID = objAPInvoice.ClientID;
                    objAPOraclePayment.CreatedBy = "Admin";

                    if (!string.IsNullOrEmpty(objAPOraclePayment.ProviderID))
                    {
                        providers = sitecoreDatabase.SelectItems(ProviderPath);

                        oracleConfiguration = providers.Where(x => Guid.Parse(x.ID.ToString()).ToString().Equals(Guid.Parse(objAPOraclePayment.ProviderID.ToString()).ToString(), StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();

                        objAPOraclePayment.NominalCode = oracleConfiguration.Fields["AP Nominal Code"].Value.ToString();
                        if (!string.IsNullOrEmpty(oracleConfiguration.Fields["AP Campaign Accounting ID"].Value))
                            objAPOraclePayment.CampaignAccountingID = int.Parse(oracleConfiguration.Fields["AP Campaign Accounting ID"].Value.ToString());
                        objAPOraclePayment.OracleID = oracleConfiguration.Fields["OracleID"].Value.ToString();
                        objAPOraclePayment.SupplierAddrCode = oracleConfiguration.Fields["Supplier Address Code"].Value.ToString();
                        if (!string.IsNullOrEmpty(oracleConfiguration.Fields["AP Line Number"].Value))
                            objAPOraclePayment.LineNumber = int.Parse(oracleConfiguration.Fields["AP Line Number"].Value.ToString());
                    }
                    else
                    {
                        oracleConfiguration = sitecoreDatabase.GetItem(Sitecore.Data.ID.Parse(objAPOraclePayment.ClientID));
                        if (oracleConfiguration != null)
                        {
                            bedBankData = new BedBanksSettings(oracleConfiguration);
                            if (bedBankData != null && bedBankData.BedBanksOracleConfigurations != null)
                            {
                                objAPOraclePayment.NominalCode = bedBankData.BedBanksOracleConfigurations.ApNominalCode;
                                if (!string.IsNullOrEmpty(bedBankData.BedBanksOracleConfigurations.ApCampaignAccountingId))
                                    objAPOraclePayment.CampaignAccountingID = int.Parse(bedBankData.BedBanksOracleConfigurations.ApCampaignAccountingId);
                                objAPOraclePayment.OracleID = bedBankData.BedBanksOracleConfigurations.OracleId;
                                objAPOraclePayment.SupplierAddrCode = bedBankData.BedBanksOracleConfigurations.SupplierAddresCode;
                                if (!string.IsNullOrEmpty(bedBankData.BedBanksOracleConfigurations.ApLineNumber))
                                    objAPOraclePayment.LineNumber = int.Parse(bedBankData.BedBanksOracleConfigurations.ApLineNumber);
                            }
                        }
                    }

                    lstAPOraclePayment.Add(objAPOraclePayment);
                }


                if (lstAPOraclePayment != null && lstAPOraclePayment.Count > 0)
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = string.Empty;
                    ValidationResponse objValidationResponse = iReportsService.APPaymentRequest(lstAPOraclePayment,"ManualPaymentRequest");
                    if (objValidationResponse.IsSuccess)
                    {
                        this.txtBookingRefNo.Text = string.Empty;
                        this.txtBookingSeq.Text = string.Empty;
                        this.divRetrieveBookingDetails.Visible = false;
                        this.RetrieveBooking.Enabled = false;
                    }
                    lblMessage.Text = "Manual Hotel payment request" + objValidationResponse.Message.ToString();
                }

            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.AdminPortal, ex, null);
                throw;
            }
        }

        private DateTime InputDateFormatting(string date)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(date))
                {
                    return default(DateTime);
                }
                else
                {
                    DateTime tmpDateTime;
                    try
                    {
                        tmpDateTime = DateTime.Parse(date);
                        return tmpDateTime;
                    }
                    catch
                    {
                        string[] words = date.Split('/');
                        string newstring = words[1] + "/" + words[0] + "/" + words[2];
                        DateTime dt = Convert.ToDateTime(newstring);
                        return dt;
                    }
                }
            }
            catch
            {
                return default(DateTime);
            }

        }

        private void RetrieveBookingDetails()
        {
            try
            {
                iReportsService = new ReportsService();
                objSearchCriteria = new SearchCriteria();

                if (!string.IsNullOrEmpty(this.txtBookingSeq.Text))
                    objSearchCriteria.BookingRefNumber = this.txtBookingRefNo.Text.Trim() + "-" + this.txtBookingSeq.Text.Trim();
                else
                    objSearchCriteria.BookingRefNumber = this.txtBookingRefNo.Text.Trim();

                objAPInvoice = iReportsService.GetManualPaymentRequest(objSearchCriteria);

                if (objAPInvoice != null)
                {
                    lblArrivalDate.Text = objAPInvoice.CheckInDate.Date.ToString("dd/MM/yyyy");
                    lblDepartureDate.Text = objAPInvoice.CheckOutDate.Date.ToString("dd/MM/yyyy");
                    string[] customerName = objAPInvoice.CustomerName.Split(' ');
                    lblFirstName.Text = customerName[0];
                    lblLastName.Text = customerName[1];
                    lblProviderName.Text = objAPInvoice.HotelName;
                    lblItem.Text = objAPInvoice.AccommodationType;
                    lblFirstConfirmed.Text = objAPInvoice.BookingDate.Date.ToString("dd/MM/yyyy");
                    lblSuggestedAmount.Text = objAPInvoice.SuggestedAmount.ToString("0.00");
                    lblTotalAmount.Text = objAPInvoice.SuggestedAmount.ToString("0.00") +" " + objAPInvoice.Currency;

                    ManualPaymentRequest.Enabled = true;
                    PaymentPaidDate.Visible = false;
                    PaymentRequestedDate.Visible = false;
                    paymentrequestwitinmonth.Visible = false;
                    Paymenteligible.InnerText = "Payment is eligible to be requested";
                    if (objAPInvoice.PaymentStatus == 1)
                    {
                        ManualPaymentRequest.Enabled = false;
                        this.lblPaidDate.Text = objAPInvoice.PaymentPaidDate.Date.ToString("dd/MM/yyyy");
                        PaymentPaidDate.Visible = true;
                        Paymenteligible.InnerText = string.Empty;
                        Paymenteligible.InnerText = "Payment is not eligible to be requested";
                    }
                    else if (objAPInvoice.PaymentRequestStatus == 1)
                    {
                        ManualPaymentRequest.Enabled = false;
                        lblPaymentRequestedDate.Text = objAPInvoice.PaymentRequestedDate.Date.ToString("dd/MM/yyyy");
                        PaymentRequestedDate.Visible = true;
                        Paymenteligible.InnerText = string.Empty;
                        Paymenteligible.InnerText = "Payment is not eligible to be requested";
                    }
                    else if (objAPInvoice.RequestWithinSixMonth == 0)
                    {
                        ManualPaymentRequest.Enabled = false;
                        paymentrequestwitinmonth.Visible = true;
                        Paymenteligible.InnerText = string.Empty;
                        Paymenteligible.InnerText = "Payment is not eligible to be requested";
                    }

                    providers = sitecoreDatabase.SelectItems(ProviderPath);

                    //oracleConfiguration = providers.Where(x => Guid.Parse(x.ID.ToString()).ToString().Equals(Guid.Parse(objAPInvoice.ProviderID.ToString()).ToString(), StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                    bool isOracleConfiguration = true;
                    if (!string.IsNullOrEmpty(objAPInvoice.ProviderID))
                    {
                        oracleConfiguration = providers.Where(x => Guid.Parse(x.ID.ToString()).ToString().Equals(Guid.Parse(objAPInvoice.ProviderID.ToString()).ToString(), StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                        if (oracleConfiguration == null || string.IsNullOrEmpty(oracleConfiguration.Fields["OracleID"].Value.ToString()))
                        {
                            isOracleConfiguration = false;
                        }
                    }
                    else
                    {
                        oracleConfiguration = sitecoreDatabase.GetItem(Sitecore.Data.ID.Parse(objAPInvoice.ClientID));
                        if (oracleConfiguration != null)
                        {
                            bedBankData = new BedBanksSettings(oracleConfiguration);
                            if (bedBankData != null && (bedBankData.BedBanksOracleConfigurations == null || string.IsNullOrEmpty(bedBankData.BedBanksOracleConfigurations.OracleId)))
                            {
                                isOracleConfiguration = false;
                            }
                        }
                        else
                        {
                            isOracleConfiguration = false;
                        }
                    }

                    if (!isOracleConfiguration)
                    {
                        ManualPaymentRequest.Enabled = false;
                        lblMessage.Text = "Oracle id is missing";
                    }

                    divRetrieveBookingDetails.Visible = true;
                }
                else
                {
                    divRetrieveBookingDetails.Visible = false;
                    lblMessage.Text = "There is no booking details for " + objSearchCriteria.BookingRefNumber;
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.AdminPortal, ex, null);
                throw;
            }

        }
    }
}