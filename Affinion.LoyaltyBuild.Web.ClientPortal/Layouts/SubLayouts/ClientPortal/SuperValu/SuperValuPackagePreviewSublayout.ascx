﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SuperValuPackagePreviewSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.SuperValuPackagePreviewSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="col-md-6 col-xs-12">
    <div id="families">
        <asp:Literal ID="PlaceholderText" Text="Set the datasource: Required Fields: Title, Subtitle, Packages" runat="server" />
        <sc:Image Field="DesktopImage" CssClass="img-responsive img-enlarge"  ID="DesktopImage" runat="server" />
        <div class="box-inner-text">
            <h3><sc:Text Field="Title" ID="Title" runat="server" /></h3>
            <p><sc:Text Field="Subtitle" ID="Subtitle" runat="server" /></p>
        </div>
    </div>
    <div class="home-links">
        <asp:Repeater ID="PackageLinks" runat="server">

            <ItemTemplate>
                <asp:HyperLink ID="PackageLink" runat="server">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <asp:Literal ID="PackageLinkText" runat="server" />
                </asp:HyperLink>
            </ItemTemplate>

        </asp:Repeater>
    </div>
</div>
