/****** Object:  StoredProcedure [dbo].[NewList]    Script Date: 1/27/2016 3:55:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[NewList] (@UserEmail varchar(50),
						  @ListName varchar(100),
						  @date datetime)
AS
BEGIN
	INSERT INTO [dbo].[uCommerce_FavoriteList]
     VALUES
           ((select CustomerID from uCommerce_Customer where EmailAddress=@UserEmail),
			@ListName,NULL,
			(select FirstName from uCommerce_Customer where EmailAddress=@UserEmail),
     	    (select FirstName from uCommerce_Customer where EmailAddress=@UserEmail),
			 @date,
		     @date)

END


GO

