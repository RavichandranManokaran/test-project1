﻿CREATE TABLE [dbo].[uCommerce_NoteTypes]
(
	[NoteTypeId] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Description] NVARCHAR(50) NULL
)
