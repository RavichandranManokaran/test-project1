﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Helpers;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Affinion.LoyaltyBuild.Common.Instrumentation;

/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Customer
{
    /// <summary>
    /// Service that provides methods to perform Communication related actions
    /// </summary>  
    public class CommunicationHistoryService:ICommunicationHistoryService
    {
        //private variables
        private string _connectionString = "uCommerce";


        /// <summary>
        /// constructor
        /// </summary>
        public CommunicationHistoryService()
        { }


        #region ICommunicationHistoryService Implementation

        /// <summary>
        /// To get the list of Communication history info by providing OrderlineID
        /// </summary>
        /// <param name="OrderLineID">OrderLineID</param>
        /// <returns> List of Communication History info  </returns>
        public List<CommunicationHistoryInfo> GetCommunicationHistoryByOrderLineID(int orderLineID)
        {
            List<CommunicationHistoryInfo> communicationHistoryInfo = new List<CommunicationHistoryInfo>();

            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);

            var sql = "LB_GetCommunicationHistorybyOrderlineId";

            using (var command = db.GetStoredProcCommand(sql))
            {
                db.AddInParameter(command, "@OrderLineId", DbType.Int32, orderLineID);
                using (var reader = db.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        var historyInfo = new CommunicationHistoryInfo
                        {
                            HistoryID = reader.Value<int>("HistoryId"),
                            CommunicationType = reader.Value<string>("Name"),
                            EmailHtmlText = reader.Value<string>("EmailHtml"),
                            EmailPlainText = reader.Value<string>("EmailText"),
                            CreatedDate =reader.Value<DateTime>("CreatedDate"),
                            OrderlineId = reader.Value<int>("OrderLineId"),
                            From = reader.Value<string>("From"),
                            To = reader.Value<string>("To"),
                            Subject =reader.Value<string>("EmailSubject")
                        };
                        communicationHistoryInfo.Add(historyInfo);
                    }
                }
            }

            return communicationHistoryInfo;
        }

        /// <summary>
        /// Add communication histroy info
        /// </summary>
        /// <param name="communicationHistoryInfo">communicationHistoryInfo</param>        
        /// <returns>Insert success flag</returns>
        public ValidationResponse InsertCommunicationHistory(CommunicationHistoryInfo communicationHistoryInfo)
        {
            var result = new ValidationResponse();

            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "LB_AddCommunicationHistory";

            using (var scope = new TransactionScope())
            {
                try
                {
                    var insertCommand = db.GetStoredProcCommand(sql);
                    db.AddInParameter(insertCommand, "OrderLineId", DbType.Int32, communicationHistoryInfo.OrderlineId);
                    db.AddInParameter(insertCommand, "CommunicationTypeId", DbType.Int32, communicationHistoryInfo.CommunicationTypeID);                   
                    db.AddInParameter(insertCommand, "EmailText", DbType.String, communicationHistoryInfo.EmailPlainText);
                    db.AddInParameter(insertCommand, "EmailHtml", DbType.String, communicationHistoryInfo.EmailHtmlText);
                    db.AddInParameter(insertCommand, "EmailSubject", DbType.String, communicationHistoryInfo.Subject);
                    db.AddInParameter(insertCommand, "From", DbType.String, communicationHistoryInfo.From);
                    db.AddInParameter(insertCommand, "To", DbType.String, communicationHistoryInfo.To);
                    db.ExecuteNonQuery(insertCommand); 
                    result.IsSuccess = true;
                    result.Message = "Communication history Successfully Inserted";
                    scope.Complete();
                }
                catch
                {
                    result.IsSuccess = false;
                    result.Message = "Error.. Could not add the Communication History Details";
                }
            }

            return result;
        }

        /// <summary>
        /// To get the Communication history info by providing HistoryID
        /// </summary>
        /// <param name="communicationHistoryID">history id</param>
        /// <returns>Communication history info</returns>
        public CommunicationHistoryInfo GetCommucationHistoryByHistoryID(int communicationHistoryID)
        {
            CommunicationHistoryInfo historyInfo = null;
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "LB_GetCommunicationHistorybyHistoryId";
            using (var command = db.GetStoredProcCommand(sql))
            {
                db.AddInParameter(command, "@HistoryId", DbType.Int32, communicationHistoryID);

                using (var reader = db.ExecuteReader(command))
                {
                    if (reader.Read())
                    {
                        historyInfo = new CommunicationHistoryInfo
                        {
                            HistoryID = reader.Value<int>("HistoryId"),
                            CommunicationType = reader.Value<string>("Name"),
                            EmailHtmlText = reader.Value<string>("EmailHtml"),
                            EmailPlainText = reader.Value<string>("EmailText"),
                            CreatedDate = reader.Value<DateTime>("CreatedDate"),
                            OrderlineId = reader.Value<int>("OrderLineId"),
                            From = reader.Value<string>("From"),
                            To = reader.Value<string>("To"),
                            Subject = reader.Value<string>("EmailSubject")
                        };                       
                    }
                }
            }

            return historyInfo;
        }

        /// <summary>
        /// Update the Mail status
        /// </summary>
        /// <param name="orderlineId">Orderline id</param>
        /// <param name="status">status</param>
        /// <returns>Mail status flag</returns>
        public ValidationResponse UpdateMailSentStatus(int orderlineId,int communicationTypeId, bool status)
        {
            var result = new ValidationResponse();

            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "LB_UpdateCommunicationHistory";

            using (var scope = new TransactionScope())
            {
                try
                {
                    var insertCommand = db.GetStoredProcCommand(sql);
                    db.AddInParameter(insertCommand, "OrderlineId", DbType.Int32, orderlineId);
                    db.AddInParameter(insertCommand, "CommunicationTypeId", DbType.Int32, communicationTypeId);
                    db.AddInParameter(insertCommand, "status", DbType.Boolean, status);
                    db.ExecuteNonQuery(insertCommand);
                    result.IsSuccess = true;
                    result.Message = "Mail sent successfully";
                    scope.Complete();
                }
                catch(Exception e)
                {
                    result.IsSuccess = false;
                    result.Message = "Error.. Could not send mail";
                    Diagnostics.WriteException(DiagnosticsCategory.BusinessLogic, e, this);
                }
            }
            return result;
        }

        #endregion

    }
}
