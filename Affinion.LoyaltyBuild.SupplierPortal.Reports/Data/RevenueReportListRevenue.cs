﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.SupplierPortal.Reports.Data
{
    /// <summary>
    /// Contains details for the items of revenue report item
    /// </summary>
    public class RevenueReportListRevenue:RevenueBase
    {
        /// <summary>
        /// Provider Name
        /// </summary>
        public string ProviderName { get; set; }
    }

    /// <summary>
    /// Collection to manage Revenue Report
    /// </summary>

    public class RevenueReportListRevenueCollection : IEnumerable<RevenueReportListRevenue>,IList<RevenueReportListRevenue>
    {
        //Private variables declaration
        private List<RevenueReportListRevenue> _items;

        /// <summary>
        /// Constructor
        /// </summary>
        public RevenueReportListRevenueCollection()
        {
            _items = new List<RevenueReportListRevenue>();
        }

        /// <summary>
        /// Add method to add item
        /// </summary>
        /// <param name="item"></param>
        public void Add(RevenueReportListRevenue item)
        {
            _items.Add(item);
        }

        /// <summary>
        /// Override method IEnumenrable class
        /// </summary>
        /// <returns></returns>
        public IEnumerator<RevenueReportListRevenue> GetEnumerator()
        {
            foreach (var item in _items)
                yield return item;
        }

        /// <summary>
        /// Override method IEnumenrable class
        /// </summary>
        /// <returns></returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            foreach (var item in _items)
                yield return item;
        }

        public int IndexOf(RevenueReportListRevenue item)
        {
            throw new NotImplementedException();
        }

        public void Insert(int index, RevenueReportListRevenue item)
        {
            throw new NotImplementedException();
        }

        public void RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        public RevenueReportListRevenue this[int index]
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }


        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(RevenueReportListRevenue item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(RevenueReportListRevenue[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public int Count
        {
            get { return _items.Count; }
        }

        public bool IsReadOnly
        {
            get { throw new NotImplementedException(); }
        }

        public bool Remove(RevenueReportListRevenue item)
        {
            throw new NotImplementedException();
        }
    }
}
