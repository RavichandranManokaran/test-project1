﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion Inernational. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           OccupancyTypeControlFactory.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.SupplierSetup.CustomDataTypes
/// Description:           Adding custom data type OccupancyType 
/// </summary>

#region Using Derectives
using Sitecore.Configuration;
using Sitecore.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.EntitiesV2;
using UCommerce.EntitiesV2.Definitions;
using UCommerce.Presentation.Web.Controls;
#endregion

namespace Affinion.LoyaltyBuild.SupplierSetup.CustomDataTypes
{
    class OccupancyTypeControlFactory : IControlFactory
    {
        private readonly DataTypeDefinition _occupancyTypeDefinition = new DataTypeDefinition("OccupancyType");
        public bool Supports(DataType dataType)
        {
            return dataType.DefinitionName == _occupancyTypeDefinition.Name;
        }

        public Control GetControl(IProperty property)
        {
            //This is the drop down list we'll return to use in the UI.
            var dropDownList = new SafeDropDownList();
            
            Database masterDB = Factory.GetDatabase("web");
            var occupancyTypes = masterDB.GetItem("{8246AE06-7E92-4239-B394-99DB87931574}");
            var occupancyTypesList = occupancyTypes.Children.ToList();

            foreach (var occupancyType in occupancyTypesList)
            {
                var listItem = new ListItem();

                listItem.Text = occupancyType.Name;

                //We'll mark this item as selected if the value of the property is equal to the price group id.
                listItem.Selected = property.GetValue().ToString() == occupancyType.Name.ToString();

                dropDownList.Items.Add(listItem);
            }
            return dropDownList;
        }
    }
}
