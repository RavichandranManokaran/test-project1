﻿<%@ control language="C#" autoeventwireup="true" codebehind="SearchResult.ascx.cs" inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal.SearchResult" %>
<%@ import namespace=" Affinion.LoyaltyBuild.Model.Provider" %>
<asp:repeater runat="server" id="rptSearchResult">
    <itemtemplate>
        <asp:placeholder runat="server" visible="<%#!String.IsNullOrEmpty(((IProvider)Container.DataItem).ProviderId) %>">
            <div class="col-md-12">
                <div class="search-info">
                    <div class="col-xs-12 col-md-4 no-padding-left no-padding-right no-padding-m">
                      <div class="search-pho">
                         <a href="#">
                            <sc:image field="IntroductionImage" item="<%#Sitecore.Context.Database.GetItem(((IProvider)Container.DataItem).ProviderId) %>" cssclass="img-responsive img-enlarge" parameters="mw=580" runat="server" />
                         </a>
                      </div>
                      <div class="price" id="LowestPriceContainer" runat="server">
                         <p class="no-margin">
                            <asp:Literal ID="LowestOccupancyType" runat="server" />
                         </p>
                         <h4 class="no-margin">
                            <asp:Literal ID="StartingPrice" runat="server" />
                         </h4>
                      </div>
                   </div>
                    <div class="col-xs-12 col-md-8 no-padding-m">
                  <div class="search-info-detail">
                     <div class="search-info-detail-top">
                        <div class="col-xs-12 no-padding-m">
                           <a href="#" class="title-list display-block-val">
                              <sc:text field="Name" item="<%#Sitecore.Context.Database.GetItem(((IProvider)Container.DataItem).ProviderId) %>" runat="server" />
                           </a>
                           <div class="star-rank pull-left width-full">
                              <asp:Repeater ID="StarRepeater" runat="server">
                                 <ItemTemplate>
                                    <span class="fa fa-star"></span>
                                 </ItemTemplate>
                              </asp:Repeater>
                           </div>
                           <a href="#" class="pull-left" data-toggle="modal" data-target="#dialog-mapB" data-coordinates="231" onclick="loadMap(this);"><i class="fa fa-map-marker icon-padding"></i></a>
                           <h5 class="no-margin">
                              <sc:text field="Town" ID="Town" item="<%#Sitecore.Context.Database.GetItem(((IProvider)Container.DataItem).ProviderId) %>" runat="server" />
                              , 
                              <asp:Literal ID="CountryName" runat="server" />
                           </h5>
                           </br>
                           <div class="search-info-description pull-left">
                              <p>
                                 <sc:text field="Overview" item="<%#Sitecore.Context.Database.GetItem(((IProvider)Container.DataItem).ProviderId)%>" runat="server" />
                              </p>
                           </div>
                        </div>
                        <div class="col-xs-12 col-md-8">
                           <%--<asp:Repeater ID="HotelFacilities" runat="server">
                              <HeaderTemplate>
                                 <ul class="event">
                              </HeaderTemplate>
                              <ItemTemplate>
                              <li>
                              <sc:link field="Link" id="FacilityLink" runat="server" item="<%#(Item)Container.DataItem %>">
                              <sc:Image Field="Icon" ID="FacilityImage" runat="server" Item="<%#(Item)Container.DataItem %>" />
                              </sc:link>
                              </li>
                              </ItemTemplate>
                              <FooterTemplate>
                              </ul>
                              </FooterTemplate>
                           </asp:Repeater>--%>
                        </div>
                        <div class="col-xs-12 col-md-4">
                        </div>
                        <div class="col-xs-12 col-md-6">
                           <div class="book-detail-hr">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
                </div>
            </div>
        </asp:placeholder>
        <asp:Repeater id="test" DataSource="<%#((IProvider)Container.DataItem).Products%>" runat="server">
            <ItemTemplate>
                <div><%#Eval("Sku") %></div>
                <div><%#Eval("VariantSku") %></div>
            </ItemTemplate>
          </asp:Repeater>
    </itemtemplate>
</asp:repeater>
