﻿CREATE FUNCTION [dbo].[LB_FnRelatedOrderLines] 
(	
	@OrderLineId INT
)
RETURNS @RelatedOrderLinesIDTable TABLE
(
	OrderLineId INT
) 
AS
BEGIN
	DECLARE @OrderRef VARCHAR(50)
	--DECLARE @NewOrderRef VARCHAR(50)
	--DECLARE @LatestOrderLineId AS INT

	SELECT @OrderRef = BookingNo FROM LB_BookingInfo where OrderLineId=@OrderLineId -- and StatusId=7 --@NewOrderRef = NewOrderReference FROM LB_RoomReservation WHERE IsCancelled = 1 AND OrderLineId = @OrderLineId
    -- select max(orderlineid) from LB_BookingInfo where OrderLineId=@OrderLineId and StatusId=7
	INSERT INTO @RelatedOrderLinesIDTable VALUES (@OrderLineId)
	
	insert into @RelatedOrderLinesIDTable select orderlineid from LB_BookingInfo where bookingno = @orderref
	RETURN
END

