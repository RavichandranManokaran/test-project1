﻿
Create PROCEDURE ManageBooking 
	-- Add the parameters for the stored procedure here
	@BookingNumber int 
	  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT PurchaseOrder.OrderTotal [Price], OrderLine.CheckInDate[CheckIn Date],OrderLine.CheckOutDate[CheckOut Date],Category.[Guid] [uCommerceCategoryId] 
	from uCommerce_PurchaseOrder PurchaseOrder 
	inner join uCommerce_OrderLine OrderLine On PurchaseOrder.OrderId=OrderLine.OrderId
	inner join uCommerce_Product Product on OrderLine.Sku=Product.Sku
	inner join uCommerce_CategoryProductRelation CategoryProductRelation on Product.ProductId=CategoryProductRelation.ProductId
	inner join uCommerce_Category Category on CategoryProductRelation.CategoryId=Category.CategoryId
	where @BookingNumber= PurchaseOrder.OrderId;   

END

