﻿//<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           AutoComplete.asmx.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal
/// Description:           Auto complete
/// </summary>


using Affinion.LoyaltyBuild.Api.Search.Data;
using Affinion.LoyaltyBuild.Api.Search.Helpers;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using helpers = Affinion.LoyaltyBuild.Search.Helpers;
using Sitecore.Data;
using Sitecore.Data.Items;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.WebServices
{
    /// <summary>
    /// Summary description for AutoComplete
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    // [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class AutoComplete : System.Web.Services.WebService
    {

        [WebMethod, ScriptMethod]
        public string GetSuggestions(string clientId, string term)
        {

            try
            {
                Diagnostics.Trace(DiagnosticsCategory.Search, string.Concat("term: ", term, " clientName: ", clientId));


                if (!helpers.SearchHelper.ValidateKeywords(term))
                {
                    return string.Empty;
                }

                //List<string> results = GetSuggestionsByClientPortalId(clientId, term).ToList();
                IEnumerable<SearchLocation> results = this.GetSearchLocations(clientId, term);

                var serializer = new JavaScriptSerializer();

                return serializer.Serialize(results.Take(5));
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, this);
                throw;
            }
        }

        [WebMethod, ScriptMethod]
        public string GetSuggestionsOfflinePortal(string clientSetupId, string term)
        {

            try
            {
                Diagnostics.Trace(DiagnosticsCategory.Search, string.Concat("term: ", term, " clientName: ", clientSetupId));

                if (!helpers.SearchHelper.ValidateKeywords(term))
                {
                    return string.Empty;
                }


                Item clientSetupItem = Sitecore.Context.Database.GetItem(new ID(clientSetupId));
                // List<string> results = helpers.SearchHelper.GetSuggestions(clientSetupItem, term).ToList();
                var results = this.GetSearchLocations(clientSetupId, term);

                var serializer = new JavaScriptSerializer();

                return serializer.Serialize(results.Take(5));
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, this);
                throw;
            }
        }

        private static List<string> GetSuggestionsByClientPortalId(string clientPoratlId, string term)
        {
            List<string> results = new List<string>();

            if (!string.IsNullOrWhiteSpace(clientPoratlId))
            {
                clientPoratlId = clientPoratlId.ToUpperInvariant();

                Item clientPortalItem = Sitecore.Context.Database.GetItem(new ID(clientPoratlId));

                Item clientSetupItem = SitecoreFieldsHelper.GetDropLinkFieldTargetItem(clientPortalItem, "ClientDetails");

                results = helpers.SearchHelper.GetSuggestions(clientSetupItem, term).ToList();

            }
            return results;
        }

        private IEnumerable<SearchLocation> GetSearchLocations(string clientId, string term)
        {
            if (!helpers.SearchHelper.ValidateKeywords(term))
            {
                return null;
            }

            ID clientItemId;
            if(!ID.TryParse(clientId, out clientItemId))
            {
                return null;
            }

            Item clientPortalItem = Sitecore.Context.Database.GetItem(clientItemId);
            Item clientSetupItem = SitecoreFieldsHelper.GetDropLinkFieldTargetItem(clientPortalItem, "ClientDetails");
            Item uCommerceItem = Sitecore.Context.Database.GetItemByKey("UcommerceSettings");
            string hotelDefinition = SitecoreFieldsHelper.GetValue(uCommerceItem, "HotelDefinitionName");
            string cityDefinition = SitecoreFieldsHelper.GetValue(uCommerceItem, "CityDefinitionName");
            string countryDefinition = SitecoreFieldsHelper.GetValue(uCommerceItem, "CountryDefinitionName");
            int catalogId = SitecoreFieldsHelper.GetInteger(clientSetupItem, "ProductCatalogID", 0);

            IEnumerable<SearchLocation> results = Affinion.LoyaltyBuild.Api.Search.Helpers.SearchHelper.GetSuggestion(catalogId, term, hotelDefinition, cityDefinition, countryDefinition);
            return results;
        }

        /// <summary>
        /// Gets the supplier search index configuration name
        /// </summary>
        public static string SupplierSearchConfig
        {
            get
            {
                return "affinion_suppliers_index";
            }
        }

        //private List<string> GetResults(string term, LinkField linkField)
        //{
        //    List<string> results = linkField.TargetItem.Axes.GetDescendants().
        //        Where(i => this.SearchText(i, term)).
        //        Select(i => i.Fields["Text"].Value).
        //        Take(5).
        //        ToList();
        //    return results;
        //}



        ///// <summary>
        ///// Search the items
        ///// </summary>
        ///// <param name="item">The current item</param>
        ///// <param name="keyword">The keyword</param>
        ///// <returns></returns>
        //private bool SearchText(Item item, string keyword)
        //{
        //    bool result = false;

        //    if (item != null && item.Fields["Text"] != null && item.Fields["Text"].Value.ToLowerInvariant().IndexOf(keyword.ToLowerInvariant()) >= 0)
        //    {
        //        result = true;
        //    }

        //    return result;
        //}
    }
}
