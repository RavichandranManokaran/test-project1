﻿CREATE TABLE [dbo].[uCommerce_Payment] (
    [PaymentId]         INT             IDENTITY (1, 1) NOT NULL,
    [TransactionId]     NVARCHAR (MAX)  NULL,
    [PaymentMethodName] NVARCHAR (50)   NOT NULL,
    [Created]           DATETIME        CONSTRAINT [uCommerce_DF_Payment_Created] DEFAULT (getdate()) NOT NULL,
    [PaymentMethodId]   INT             NOT NULL,
    [Fee]               MONEY           CONSTRAINT [uCommerce_DF_Payment_Fee] DEFAULT ((0)) NOT NULL,
    [FeePercentage]     DECIMAL (18, 4) CONSTRAINT [uCommerce_DF_Payment_FeePercentage] DEFAULT ((0)) NOT NULL,
    [PaymentStatusId]   INT             NOT NULL,
    [Amount]            MONEY           NOT NULL,
    [OrderId]           INT             NOT NULL,
    [FeeTotal]          MONEY           DEFAULT ((0)) NULL,
    [ReferenceId]       NVARCHAR (MAX)  NULL,
    CONSTRAINT [uCommerce_PK_Payment] PRIMARY KEY CLUSTERED ([PaymentId] ASC),
    CONSTRAINT [uCommerce_FK_Payment_Order] FOREIGN KEY ([OrderId]) REFERENCES [dbo].[uCommerce_PurchaseOrder] ([OrderId]),
    CONSTRAINT [uCommerce_FK_Payment_PaymentMethod] FOREIGN KEY ([PaymentMethodId]) REFERENCES [dbo].[uCommerce_PaymentMethod] ([PaymentMethodId]),
    CONSTRAINT [uCommerce_FK_Payment_PaymentStatus] FOREIGN KEY ([PaymentStatusId]) REFERENCES [dbo].[uCommerce_PaymentStatus] ([PaymentStatusId])
);

