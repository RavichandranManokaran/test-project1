
CREATE PROCEDURE [dbo].[LB_SearchBooking]
(
	@OrderReference VARCHAR(50),
	@GuestName NVARCHAR(100),
	@CheckInDate DATE
)
AS
BEGIN
DECLARE @orderLineID INT
    SET @orderLineID = (SELECT OrderLineId 
		FROM LB_RoomReservation 
		WHERE OrderReference LIKE @OrderReference 
		AND  CAST([checkInDate] AS DATE) = CAST(@CheckInDate AS DATE)
		AND @GuestName = (SELECT [LastName] FROM [dbo].[uCommerce_Customer]
							WHERE [CustomerId] = (SELECT [CustomerId] FROM [dbo].[uCommerce_PurchaseOrder]
							WHERE [OrderId] = (SELECT [OrderId] FROM [dbo].[uCommerce_OrderLine]
							WHERE [OrderLineId] = (SELECT [OrderLineId] From [dbo].[LB_RoomReservation]
							WHERE [OrderReference] LIKE @OrderReference))))
		)	
	RETURN @orderLineID
END
