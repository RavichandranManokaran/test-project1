﻿/****** Object:  StoredProcedure [dbo].[LB_DeleteDue]    Script Date: 2/12/2016 11:41:12 AM ******/
DROP PROCEDURE [dbo].[LB_DeleteDue]
GO

/****** Object:  StoredProcedure [dbo].[LB_DeleteDue]    Script Date: 2/12/2016 11:41:12 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LB_DeleteDue]
(
	@OrderLineDueId INT,
	@Reason VARCHAR(MAX)
)
AS
BEGIN
	UPDATE orderlinedue
	SET orderlinedue.IsDeleted = 1, Note = (ISNULL(Note,'') + ISNULL(@Reason,''))
	FROM LB_OrderLineDue orderlinedue
		INNER JOIN LB_DueTypes duetypes ON duetypes.DueTypeId = orderlinedue.DueTypeId
	WHERE orderlinedue.OrderLineDueId = @OrderLineDueId 
		AND duetypes.CanBeDeleted = 1


	DECLARE @OrderLineId INT
	SELECT @OrderLineId = OrderLineId FROM LB_OrderLineDue orderlinedue
		INNER JOIN LB_DueTypes duetypes ON duetypes.DueTypeId = orderlinedue.DueTypeId
	WHERE orderlinedue.OrderLineDueId = @OrderLineDueId 
		AND duetypes.CanBeDeleted = 1

	IF(@OrderLineId IS NOT NULL)
		EXEC [LB_UpdateStatusOnDueAndPay] @OrderLineId = @OrderLineId, @CreatedBy = ''
END


GO

