﻿DROP PROCEDURE [dbo].[LB_BB_InsertErrataFeedForOrderLine]
GO

/****** Object:  StoredProcedure [dbo].[LB_BB_InsertErrataFeedForOrderLine]    Script Date: 8/14/2016 6:52:38 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LB_BB_InsertErrataFeedForOrderLine] 
	-- Add the parameters for the stored procedure here
	@PropertyId int , 
	@PropertyRoomTypeId varchar(max),
	@OrderLineId int,
	@Errata varchar(max) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Insert into LB_BB_ErrataInfo values (@PropertyId,@PropertyRoomTypeId,@OrderLineId,@Errata)
END
GO