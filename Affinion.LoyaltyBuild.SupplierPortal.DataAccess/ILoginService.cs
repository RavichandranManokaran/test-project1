﻿using Affinion.LoyaltyBuild.SupplierPortal.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.SupplierPortal.DataAccess
{
    /// <summary>
    /// Interface Login Service
    /// </summary>
    public interface ILoginService
    {
        /// <summary>
        /// insert chage password history
        /// </summary>
        /// <returns></returns>
        void InsertChangePasswordHistory(string uniqueid, string username);

        /// <summary>
        /// select chage password history
        /// </summary>
        /// <returns>username</returns>
        string SelectChangePasswordHistory(string uniqueid);

        /// <summary>
        /// delete chage password history
        /// </summary>
        /// <returns></returns>
        void DeleteChangePasswordHistory(string username);

        /// <summary>
        /// Generate random numbers
        /// </summary>
        /// <param name="r">Random number</param>
         string GenerateRandomNumbers(Random r);
        
    }
}
