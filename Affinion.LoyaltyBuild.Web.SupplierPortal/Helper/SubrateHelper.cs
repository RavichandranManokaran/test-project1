﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Subrate;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Helper
{
    /// <summary>
    /// Subrate helper class. This needs to be removed later
    /// </summary>
    [Obsolete("This class is obsolete as subrate is removed")]
    public class SubrateHelper
    {
        /// <summary>
        /// Provider id
        /// </summary>
        public string ProviderId { get; set; }

        /// <summary>
        /// Client id
        /// </summary>
        public string ClientId { get; set; }

        private ISubrate _subrateServices;
        Sitecore.Data.Database context = Sitecore.Context.Database;

        /// <summary>
        /// Constructor
        /// </summary>
        public SubrateHelper()
        {
            this._subrateServices = new SubrateService();
        }

        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="currentcontext"></param>
        public SubrateHelper(HttpContext currentcontext)
        {
            String loggedInProvider_ucom = Convert.ToString(currentcontext.Session[SessionKeys.loggedInSupplier_ucom]);

            if (!String.IsNullOrEmpty(loggedInProvider_ucom))
            {
                if (context.SelectItems(Constants.hotelPath) != null)
                {
                    this.ProviderId = loggedInProvider_ucom;

                }
            }
            this._subrateServices = new SubrateService();
        }

        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="currentcontext"></param>
        public SubrateHelper(string providerId, string clientId)
        {
            this.ProviderId = providerId;
            this.ClientId = clientId;
        }

        /// <summary>
        /// Get all subrates
        /// </summary>
        /// <returns></returns>
        public List<SubrateTemplatesDetail> GetAllSubrateTemplate()
        {
            List<SubrateTemplatesDetail> subrateView = new List<SubrateTemplatesDetail>();
            subrateView = _subrateServices.GetAllSubRateTemplate();
            return subrateView;
        }

        /// <summary>
        /// Get all subrate items
        /// </summary>
        /// <returns></returns>
        public List<SubrateItemDetail> GetAllSubRateItems()
        {
            List<SubrateItemDetail> subrateView = new List<SubrateItemDetail>();
            subrateView = _subrateServices.GetAllSubRateItems();
            return subrateView;
        }

        /// <summary>
        /// Get subrate items
        /// </summary>
        /// <param name="ItemID"></param>
        /// <returns></returns>
        public List<SubratesDetail> GetSubratesBySintelItem(string ItemID)
        {
            return _subrateServices.GetSubRateByItemId(ItemID);
        }

        /// <summary>
        /// Save subrate
        /// </summary>
        /// <param name="objSubrates"></param>
        /// <param name="ItemID"></param>
        /// <returns></returns>
        public int SaveSubrates(List<SubratesDetail> objSubrates, string ItemID)
        {
            return _subrateServices.SaveSubrates(objSubrates, ItemID);
        }

        /// <summary>
        /// Delete subrate
        /// </summary>
        /// <param name="ItemID"></param>
        /// <param name="ClientID"></param>
        /// <returns></returns>
        public int DeleteSubrate(string ItemID, string ClientID = "Admin")
        {
            return _subrateServices.DeleteSubrate(ItemID, ClientID);
        }

        /// <summary>
        /// Save subrate
        /// </summary>
        /// <param name="objsrtemplate"></param>
        /// <returns></returns>
        public string SaveTemplate(SubrateTemplatesDetail objsrtemplate)
        {
            return _subrateServices.SaveTemplate(objsrtemplate);
        }

        /// <summary>
        /// Delete subrate
        /// </summary>
        /// <param name="SubrateTemplateID"></param>
        /// <returns></returns>
        public int DeleteTemplate(string SubrateTemplateID)
        {
            return _subrateServices.DeleteTemplate(SubrateTemplateID);
        }
    }
}