﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sitecore.Data.Items;

namespace Affinion.LoyaltyBuild.BedBanks.General
{
    public class BedBanksProcessingFee
    {
        /// <summary>
        /// Bed Banks Currency Item
        /// </summary>
        public Item BedBanksCurrencyItem { get; set; }

        /// <summary>
        /// Processing Fee for the corresponding currency
        /// </summary>
        public float ProccesingFee { get; set; }
    }
}
