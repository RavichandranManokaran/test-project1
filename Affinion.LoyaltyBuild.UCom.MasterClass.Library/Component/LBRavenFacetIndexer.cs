﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Infrastructure.Globalization;
using UCommerce.Search;
using UCommerce.Search.Facets;
using LBRaven = Raven.Abstractions.Data;
namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Component
{
    public class LBRavenFacetIndexer : IIndexer<Facet>, IIndexer<IEnumerable<Facet>>, IDeindexer<Facet>, IDeindexer<IEnumerable<Facet>>
    {
        // Fields
        private readonly ILanguageService _languageService;
        private readonly IRepository _repository;

        // Methods
        public LBRavenFacetIndexer(IRepository documentRepository, ILanguageService languageService)
        {
            this._repository = documentRepository;
            this._languageService = languageService;
        }

        public void Deindex(IEnumerable<Facet> subjects)
        {
            Guard.Against.NullArgument<IEnumerable<Facet>>(subjects);
            List<LBRaven.FacetSetup> modifiedFacetSetups = new List<LBRaven.FacetSetup>();
            foreach (Language language in this._languageService.GetAllLanguages())
            {
                string facetSetupName = this.GetFacetSetupName(language);
                foreach (Facet facet in subjects)
                {
                    LBRaven.FacetSetup facetSetup = this._repository.Get<LBRaven.FacetSetup>(facetSetupName);
                    if (facetSetup != null)
                    {
                        this.RemoveDeindexedFacet(facetSetup, facet, modifiedFacetSetups);
                    }
                }
            }
            this.UpdateModifiedFacetSetups(modifiedFacetSetups);
        }

        public void Deindex(Facet subjects)
        {
            this.Deindex(new List<Facet> { subjects });
        }

        private string GetFacetSetupName(Language language)
        {
            return string.Format("facets/ProductFacets/{0}", language.CultureCode);
        }

        public void Index(IEnumerable<Facet> subjects)
        {
            List<LBRaven.FacetSetup> source = new List<LBRaven.FacetSetup>();
            foreach (Language language in this._languageService.GetAllLanguages())
            {
                string id = this.GetFacetSetupName(language);
                LBRaven.FacetSetup item = (source.FirstOrDefault<LBRaven.FacetSetup>(x => (x.Id == id)) ?? this._repository.Get<LBRaven.FacetSetup>(id)) ?? new LBRaven.FacetSetup();
                foreach (Facet facet in subjects)
                {
                    LBRaven.Facet rfacet;
                    if (facet.CultureCode == language.CultureCode)
                    {
                        rfacet = new LBRaven.Facet
                        {
                            Mode = Raven.Abstractions.Data.FacetMode.Default,
                            Name = facet.Name,
                            DisplayName = facet.DisplayName
                        };
                        item.Id = id;
                        if (item.Facets.All<LBRaven.Facet>(x => x.Name != facet.Name))
                        {
                            item.Facets.Add(rfacet);
                        }
                        if (!source.Contains(item))
                        {
                            source.Add(item);
                        }
                    }
                }
            }
            this._repository.SaveMany<LBRaven.FacetSetup>(source);
        }

        public void Index(Facet subjects)
        {
            this.Index(new List<Facet> { subjects });
        }

        private void RemoveDeindexedFacet(LBRaven.FacetSetup facetSetup, Facet subject, List<LBRaven.FacetSetup> modifiedFacetSetups)
        {
            Func<LBRaven.Facet, bool> predicate = null;
            if (facetSetup.Facets.Any<LBRaven.Facet>(x => x.Name == subject.Name))
            {
                if (predicate == null)
                {
                    predicate = x => x.Name == subject.Name;
                }
                facetSetup.Facets.Remove(facetSetup.Facets.First<LBRaven.Facet>(predicate));
                modifiedFacetSetups.Add(facetSetup);
            }
        }

        private void UpdateModifiedFacetSetups(IEnumerable<LBRaven.FacetSetup> modifiedFacetSetups)
        {
            foreach (LBRaven.FacetSetup setup in modifiedFacetSetups)
            {
                if (setup.Facets.Any<LBRaven.Facet>())
                {
                    this._repository.Save<LBRaven.FacetSetup>(setup);
                }
                else
                {
                    LBRaven.FacetSetup entity = this._repository.Get<LBRaven.FacetSetup>(setup.Id);
                    this._repository.Delete<LBRaven.FacetSetup>(entity);
                }
            }
        }
    }

}
