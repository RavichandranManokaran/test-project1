﻿CREATE TABLE [dbo].[LB_DueTypes] (
    [DueTypeId]        UNIQUEIDENTIFIER  NOT NULL,
    [Description]      VARCHAR (MAX) NULL,
    [IsActive]         BIT           NOT NULL,
    [CanBeDeleted]     BIT           NOT NULL,
    [ShowInList]       BIT           NOT NULL,
    [Code] VARCHAR(50)           NULL,
    PRIMARY KEY CLUSTERED ([DueTypeId] ASC)
);

