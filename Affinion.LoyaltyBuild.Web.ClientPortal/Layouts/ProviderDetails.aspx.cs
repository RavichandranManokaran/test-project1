﻿using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.Common.Exceptions;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts
{
    public partial class ProviderDetails : System.Web.UI.Page
    {
        #region Properties

        public string latitude { get; set; }
        public string lognitude { get; set; }

        #endregion

        #region Fields

        private string providerItemId;        

        #endregion

        #region Protected Methods

        /// <summary>
        /// Code to execute on page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                QueryStringHelper helper = new QueryStringHelper(Request.Url);
                providerItemId = helper.GetValue("supplierid");
                Item item = Sitecore.Context.Database.GetItem(providerItemId);
                string SKU = item.Fields["SKU"].Value;

                LoadProviderDetails(SKU);
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, null);
                throw;
            }            
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Load hotel related details
        /// </summary>
        /// <param name="itemID">Item id of the hotel</param>
        private void LoadProviderDetails(string providerSku)
        {
            //Item item = Sitecore.Context.Database.GetItem(itemID);
            //string ProviderSku = item.Fields["SKU"].Value;

            if (providerSku == null)
            {
                return;
            }

            Item hotels = Sitecore.Context.Database.SelectItems(Constants.BasketPageQueryPath).Where(x => x.Fields["SKU"].Value == providerSku).FirstOrDefault();
            ///Bind hotel name
            
            ProviderTitle.Item = hotels;
            ProviderNameHead.Item = hotels;
            ProviderName.Item = hotels;
            ProviderIamge.Item = hotels;

            BindStarRankings(hotels);
            BindAddressFields(hotels);            

            ///Bind facilities
            FacilitiesRepeater.DataSource = SitecoreFieldsHelper.GetMutiListItems(hotels, "Facilities");
            FacilitiesRepeater.DataBind();

            string latLong = hotels.Fields["GeoCoordinates"].Value;
            if (!string.IsNullOrWhiteSpace(latLong))
            {
                List<string> latLongList = latLong.Split(',').ToList<string>();
                latitude = latLongList[0];
                lognitude = latLongList[1];
            }
            else
            {
                latitude = "0";
                lognitude = "0";
            }

            ///Other info binding
            Overview.Item = hotels;
            Facilities.Item = hotels;
            GoodToKnow.Item = hotels;
            BindTimeFields(hotels);
        }

        /// <summary>
        /// Bind ranking related fields
        /// </summary>
        /// <param name="item">Sitecore item</param>
        private void BindStarRankings(Item item)
        {
            ///Bind star rank
            string ranking = SitecoreFieldsHelper.GetDropLinkFieldValue(item, "StarRanking", "Name");

            if (!string.IsNullOrWhiteSpace(ranking))
            {
                int starRank;

                bool result = Int32.TryParse(ranking, out starRank);

                if (result)
                {
                    //starRank = int.Parse(ranking);
                    List<int> listOfStars = Enumerable.Repeat(starRank, starRank).ToList();
                    this.StarRepeater.DataSource = listOfStars;
                    this.StarRepeater.DataBind();
                }
                else
                {
                    Diagnostics.WriteException(DiagnosticsCategory.ClientPortal,
                        new AffinionException("Unable to parse star rank:" + item.DisplayName), this);
                }
            }
        }

        /// <summary>
        /// Bind address related fields
        /// </summary>
        /// <param name="item">Sitecore item</param>
        private void BindAddressFields(Item item)
        {            
            ///Address binding
            AddressLine1.Item = item;
            AddressLine2.Item = item;
            AddressLine3.Item = item;
            AddressLine4.Item = item;
            Town.Item = item;

            Item country = SitecoreFieldsHelper.GetDropLinkFieldTargetItem(item, "Country");
            if (country != null)
            {
                CountryName.Item = country;
            }
        }

        /// <summary>
        /// Bind time related fields
        /// </summary>
        /// <param name="item">Sitecore item</param>
        private void BindTimeFields(Item item)
        {
            CheckinTime.Item = item;
            CheckoutTime.Item = item;
        }

        #endregion
    }
}