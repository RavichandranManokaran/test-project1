﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalTwoColumnSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalTwoColumnSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<!-- Example row of columns -->
<div class="row">
    <div class="col-md-12">

        <div class="col-xs-12 col-md-4 leftpanel">
            <sc:Placeholder ID="searchboxofflineportal" Key="searchboxofflineportal" runat="server" />
            <div class="col-xs-12 no-padding"></div>
            <sc:Placeholder ID="LocationSelectorOfflinePortal" Key="LocationSelectorOfflinePortal" runat="server" />
            <sc:Placeholder ID="MapLocatorOfflinePortal" Key="MapLocatorOfflinePortal" runat="server" />
            <sc:Placeholder ID="FilterSearchOfflinePortal" Key="FilterSearchOfflinePortal" runat="server" />
        </div>

        <div class="col-xs-12 col-md-8 rightpanel">
            <sc:Placeholder ID="searchresultsofflineportal" Key="searchresultsofflineportal" runat="server" />
        </div>

    </div>
</div>
