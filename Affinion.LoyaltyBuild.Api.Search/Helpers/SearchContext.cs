﻿using Affinion.LoyaltyBuild.Api.Search.Data;
using Affinion.LoyaltyBuild.Api.Search.Filters;
using Affinion.LoyaltyBuild.Api.Search.ServiceProviders;
using Affinion.LoyaltyBuild.Api.Search.Store;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Model.Provider;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI.WebControls;
using UCommerce.EntitiesV2;
using api = Affinion.LoyaltyBuild.Api.Search.ServiceProviders;

namespace Affinion.LoyaltyBuild.Api.Search.Helpers
{
    /// <summary>
    /// Search context class
    /// </summary>
    internal sealed class SearchContext
    {
        private readonly string _clientId = string.Empty;
        private readonly ISearchStore _store = null;
        private readonly IProviderService _providerService = null;

        /// <summary>
        /// private constructor to avoid outside initializing
        /// </summary>
        public SearchContext(IProviderService providerService, ISearchStore store)
        {
            _store = store;
            _providerService = providerService;
        }
                
        /// <summary>
        /// Search hotels by search key
        /// </summary>
        /// <param name="key"></param>
        /// <param name="options"></param>
        public PagedProviderCollection SearchHotels(SearchKey key, SearchOptions options = null)
        {
            //return null if the key is null
            if (key == null)
                return null;
                        
            //get current search key from session
            var currentKey = _store.GetSearchKey();
            IList<SearchResult> mergedItems = null;
            
            //if not the search is same
            if (!key.Equals(currentKey))
            {
                //get data dynamically
                var providers = new List<ProviderCollection>();
                var filters = GetFilters(key);

                foreach (api.IServiceProvider service in _providerService.GetServiceProvidersByClient(key.ClientId))
                    providers.Add(service.SearchProviders(filters));

                //Task.WaitAll(providers.ToArray());

                mergedItems = ProviderMerger.MergeResults(providers, key.ClientId);
            }
            else
            {
                //get data from store
                mergedItems = _store.GetSearchResult();
            }

            //sort the result set
            PagedProviderCollection result = SortResult(mergedItems, options);
            
            //store the search key and result
            _store.SetSearchKey(key);
            _store.SetSearchRestult(result);

            //return response
            return result;
        }

        /// <summary>
        /// Get Particular Provider Details with its Product
        /// </summary>
        /// <param name="key"></param>
        /// <param name="providerSearch"></param>
        /// <returns></returns>
        public IProvider GetProviderInfo(SearchKey key, ProviderSearch providerSearch)
        {
            var service = _providerService.GetServiceProvider(providerSearch.ProviderType);

            var filters = GetFilters(key);
            var provider = service.GetProviderWithProduct(providerSearch);

            return provider;
        }


        /// <summary>
        /// Get provider data for the page
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="currentPage"></param>
        /// <returns></returns>
        public IList<IProvider> GetPageData(PagedProviderCollection collection, int currentPage)
        {
            if (collection == null)
                return null;
            
            //get the recors to show for this page
            var recordsToSkip = collection.PageSize * (currentPage-1);
            var itemsToShow = collection.Skip(recordsToSkip).Take(collection.PageSize);

            var providers = new List<IProvider>();
            foreach (var item in itemsToShow)
            {
                IProvider provider = null;
                try
                {
                    provider = _providerService
                    .GetServiceProvider(item.ProviderType)
                    .GetProviderDetails(item);

                    if (provider != null)
                    {
                        provider.Products = item.Products;
                        providers.Add(provider);
                    }
                    else
                    {
                        Diagnostics.Trace(DiagnosticsCategory.Common, string.Format("Error ProviderId : {0} , PropertyRefId:{1} , Name:{2}",item.ProviderId,item.PropertyReferenceId,item.Name));
                    }
                }
                catch(Exception ex)
                {
                    Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                }
            }

            //return current page data
            return providers;
        }

        /// <summary>
        /// Get suggestions
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SearchLocation> GetSuggestion(int catalogId, string term, string hotelDefinitionName, string cityDefinitionName, string countryDefinitionName)
        {
            List<SearchLocation> suggestions = new List<SearchLocation>();

            int count = 0;

            // search ucommerce hotel names
            IEnumerable<Product> hotels = LoyaltyBuildCatalogLibrary.GetProducts(term, hotelDefinitionName, catalogId).Take(5);
            suggestions.AddRange(this.GetSearchLocations<Product>(hotels, LocationType.Hotel, countryDefinitionName, cityDefinitionName));
            count = hotels.Count();

            if (count < 5)
            {
                // search ucommerce cities
                IEnumerable<Category> cities = LoyaltyBuildCatalogLibrary.GetCategories(term, cityDefinitionName, catalogId).Take(5 - count);
                suggestions.AddRange(this.GetSearchLocations<Category>(cities, LocationType.City, countryDefinitionName, cityDefinitionName));
                count += cities.Count();
            }
 
            // search ucommerce countries and cities
            if (count < 5)
            {
                IEnumerable<Category> countries = LoyaltyBuildCatalogLibrary.GetCategories(term, countryDefinitionName).Take(5 - count);
                suggestions.AddRange(this.GetSearchLocations<Category>(countries, LocationType.Country, countryDefinitionName, cityDefinitionName));
            }

            return suggestions.Take(5);
        }

        /// <summary>
        /// Create querystring based on current search
        /// </summary>
        /// <returns></returns>
        public string CreateCurrentSearchString()
        {
            var searchKey = _store.GetSearchKey();

            //return querystring if search exists
            if (searchKey != null)
                return searchKey.CreateSearchString();

            return null;
        }


        #region Private Helper Methods
        
        /// <summary>
        /// Get all search filters based on the key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private IList<ISearchFilter> GetFilters(SearchKey key)
        {
            var filters = new List<ISearchFilter>();

            //get all types inherited from Isearchfilter
            var filterTypes = Assembly.GetExecutingAssembly()
                .GetTypes()
                .Where(i => (typeof(ISearchFilter)).IsAssignableFrom(i) && i.IsClass);

            filters.AddRange(filterTypes.Select(i => (ISearchFilter)System.Activator.CreateInstance(i, key)));

            return filters;
        }

        /// <summary>
        /// Sort data and store the sorted result
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        private PagedProviderCollection SortResult(IList<SearchResult> collection, SearchOptions options)
        {
            IEnumerable<SearchResult> sortedResult = collection;

            if (options != null)
            {
                switch (options.SortOn)
                {
                    case SortField.Name:
                        sortedResult = collection.OrderBy(i => i.Name);
                        break;
                    case SortField.Price:
                        sortedResult = collection.OrderBy(i => GetLowestPrice(i.Products, options.SortDirection));
                        break;
                    case SortField.Rate:
                        sortedResult = collection.OrderBy(i => i.StarRanking);
                        break;
                    case SortField.Recommended:
                        sortedResult = collection.OrderBy(i => i.LocalRanking);
                        break;
                    default:
                        sortedResult = collection.OrderBy(i => i.LocalRanking).ThenBy(i => i.Name);
                        break;
                }

                if (options.SortDirection == Data.SortDirection.Descending)
                    sortedResult = sortedResult.Reverse();
            }

            var result = new PagedProviderCollection();
            result.PageSize = 10;
            result.AddRange(sortedResult.ToList());

            return result;
        }

        private decimal GetLowestPrice(IList<Affinion.LoyaltyBuild.Model.Product.IProduct> products, Affinion.LoyaltyBuild.Api.Search.Data.SortDirection sortDirection)
        {
            if(products == null || products.Count <= 0)
            {
                // push the hotels witout products to last
                if (sortDirection == Data.SortDirection.Ascending)
                    return decimal.MinValue;
                else
                    return decimal.MaxValue;
            }

            return products.Min(p => p.Price);
        }

        public static PagedDataSource GetPagedDataSource(IList<SearchResult> list, int pageNumber, int itemsPerPage)
        {
            if (list == null || pageNumber == 0 || itemsPerPage == 0)
            {
                return null;
            }

            PagedDataSource pagedDataSource = new PagedDataSource();

            pagedDataSource.DataSource = list;
            pagedDataSource.AllowPaging = true;
            pagedDataSource.CurrentPageIndex = pageNumber > 0 ? pageNumber - 1 : 0;
            pagedDataSource.PageSize = itemsPerPage;

            return pagedDataSource;
        }

        /// <summary>
        /// Get search location objects from a list of products or categories
        /// </summary>
        /// <typeparam name="T">Object type. Can be ucommerce product or lb category</typeparam>
        /// <param name="records">List of records to process</param>
        /// <param name="locationType">Location type of the entry list</param>
        /// <param name="countryDefinitionName">Category definition id of countries</param>
        /// <param name="cityDefinitionName">Category definition of cities</param>
        /// <returns>Returns a collection of SearchLocation objects</returns>
        private IEnumerable<SearchLocation> GetSearchLocations<T>(IEnumerable<T> records, LocationType locationType, string countryDefinitionName = null, string cityDefinitionName = null)
        {
            List<SearchLocation> suggestions = new List<SearchLocation>();

            try
            {
                if (records == null)
                    return suggestions;

                foreach (T record in records)
                {
                    string hotelName = string.Empty;
                    Category city = null;
                    Category country = null;
                    Product product = null;
                    string text = string.Empty;

                    if (record is Product)
                    {
                        product = record as Product;
                        hotelName = product.Name;
                        city = LoyaltyBuildCatalogLibrary.GetParentCategory(product, cityDefinitionName);
                        country = LoyaltyBuildCatalogLibrary.GetParentCategory(city, countryDefinitionName);

                        if (!string.IsNullOrEmpty(hotelName) && city != null && !string.IsNullOrEmpty(city.Name) && country != null && !string.IsNullOrEmpty(country.Name))
                            text = string.Format("{0}, {1}, {2}", hotelName, city.Name, country.Name);
                    }
                    else if(record is Category)
                    {
                        city = locationType == LocationType.City ? record as Category : null;
                        country = locationType == LocationType.City ? LoyaltyBuildCatalogLibrary.GetParentCategory(city, countryDefinitionName) : record as Category;

                        if (locationType == LocationType.City && city != null && !string.IsNullOrEmpty(city.Name) && country != null && !string.IsNullOrEmpty(country.Name))
                            text = string.Format("{0}, {1}", city.Name, country.Name);

                        if (locationType == LocationType.Country && country != null && !string.IsNullOrEmpty(country.Name))
                            text = string.Format("{0}", country.Name);
                    }
                    else
                    {
                        return suggestions;
                    }

                    if (!string.IsNullOrEmpty(text))
                    {
                        string categoryId = null;
                        
                        switch(locationType)
                        {
                            case LocationType.City:
                                categoryId = city.Id.ToString();
                                break;
                            case LocationType.Country:
                                categoryId = country.Id.ToString();
                                break;
                            case LocationType.Hotel:
                                categoryId =  string.Format("{0},{1}", product.Id.ToString(), city.Id.ToString());
                                break;
                        }

                        suggestions.Add(new SearchLocation()
                        {
                            Text = text,
                            ItemId = categoryId,
                            Type = locationType
                        });
                    }
                }

                return suggestions;
            }
            catch(Exception ex)
            {
                return suggestions;
            }
        }

        #endregion
    }
}
