﻿using Affinion.LoyaltyBuild.SupplierPortal.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.SupplierPortal.DataAccess
{
    public interface IOfferService
    {
        List<Offer> GetAllOffers();

        List<Offer> GetParticipatingOffersByProvider(string productSku);

       

        List<Offer> GetNotParticipatingOffersByProvider(int providerId);
    }
}
