﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalTopbarSublayout.ascx.cs" Inherits="Affinion.Loyaltybuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalTopbarSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="container-outer width-full">
    <div class="container-outer-inner width-full header-top-bar">
        <div class="container position-relative">
            <!-- The justified navigation menu is meant for single line per list item.
                  Multiple lines will require custom code not provided by Bootstrap. -->

            <div class="row coomon-outer-padding">
                <div class="col-md-3">
                    <div class="logo">
                        <sc:Placeholder ID="logoofflineportal" Key="logoofflineportal" runat="server" />
                    </div>
                </div>

                <div class="col-md-9">
                     <sc:Placeholder ID="menuofflineportal" Key="menuofflineportal" runat="server" />
                </div>               
            </div>

        </div>
    </div>
</div>




