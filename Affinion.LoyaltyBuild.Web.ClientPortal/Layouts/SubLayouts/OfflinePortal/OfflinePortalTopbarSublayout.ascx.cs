﻿//<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           OfflinePortalTopbarSublayout.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal
/// Description:           Used to Bind data to OfflinePortalTopbarSublayout subLayout
/// </summary>


namespace Affinion.Loyaltybuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    using Affinion.LoyaltyBuild.Common;
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Sitecore.Data.Items;
    using Sitecore.Security.Authentication;
    using Sitecore.Web;
    using System;

    public partial class OfflinePortalTopbarSublayout : BaseSublayout
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            Item TopbarItem = this.GetDataSource();           

        }
    }
}