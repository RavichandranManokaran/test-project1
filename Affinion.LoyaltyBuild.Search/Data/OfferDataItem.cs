﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SearchHelper.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Search
/// Description:           Stores information related to individual offers
/// </summary>
namespace Affinion.LoyaltyBuild.Search.Data
{

    using Affinion.LoyaltyBuild.Common.Exceptions;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Search.Helpers;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Component;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Subrate;
    using Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using UCommerce.Api;
    using UCommerce.EntitiesV2;
    using UCommerce.Infrastructure;
    using UCommerce.Marketing.Awards;
    using UCommerce.Marketing.Awards.AwardResolvers;
    using UCommerce.Marketing.TargetingContextAggregators;
    using UCommerce.Marketing.Targets.TargetResolvers;

    /// <summary>
    /// Stores information related to individual offers
    /// </summary>
    public class OfferDataItem : ICloneable
    {
        //private Item occupancyItem;
        //private Item offerGroupItem;
        //private string occupancyName;
       // private string offerGroupName;
        private CurrencyInfo currencyInfo;

        /// <summary>
        /// Gets or sets the offer name
        /// </summary>
        public string OfferName { get; set; }

        /// <summary>
        /// Gets or sets the offer sku
        /// </summary>
        public string OfferSku { get; set; }

        /// <summary>
        /// Gets or sets the occupancy type id
        /// </summary>
        public string OccupancyTypeId { get; set; }

        /// <summary>
        /// Gets or sets the occupancy type
        /// </summary>
        public OccupancyType OccupancyType { get; set; }

        /// <summary>
        /// Gets or sets the offer group id
        /// </summary>
        public string OfferGroupId { get; set; }

        /// <summary>
        /// Gets or sets the offer group name
        /// </summary>
        public string OfferGroupName { get; set; }

        /// <summary>
        /// Gets or sets the Offer Price object
        /// </summary>
        public LBPriceCalculation PriceDetails { get; set; }

        /// <summary>
        /// Gets or sets the Offer Price
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Gets or sets the Offer Price
        /// </summary>
        public decimal DiscountedPrice { get; set; }

        /// <summary>
        /// Gets or sets value indicating weather the price is discounted
        /// </summary>
        public bool IsDiscounted { get; set; }

        /// <summary>
        /// Gets or sets the currency
        /// </summary>
        public Currency UcommerceCurrencyDetails { get; set; }

        /// <summary>
        /// Gets or sets the currency details
        /// </summary>
        public CurrencyInfo CurrencyDetails 
        { 
            get
            {
                return this.currencyInfo;
            }
            set
            {
                this.currencyInfo = value;
                if (this.currencyInfo != null && this.currencyInfo.Errors == null)
                {
                    PriceGroup priceGroup = this.GetPriceGroup(this.currencyInfo.PriceGroup);
                    if(priceGroup == null)
                    {
                        // sitecore settings are wrong. You got an invalid price group
                        this.currencyInfo = null;
                    }
                    else
                    {
                        this.UcommerceCurrencyDetails = priceGroup.Currency;
                    }
                }
                else
                {
                    // not without errors
                    this.currencyInfo = null;
                }
            }
        }

        /// <summary>
        /// Gets or sets the offer's occupancy type availability
        /// </summary>
        public int Availability { get; set; }

        /// <summary>
        /// Gets or sets the offer tip
        /// </summary>
        public string Tip { get; set; }

        /// <summary>
        /// Gets or sets the supplier ID
        /// </summary>
        public string SupplierId { get; set; }

        /// <summary>
        /// Constructs an empty offer object
        /// </summary>
        public OfferDataItem() { }

        public List<RoomAvailabilityDetail> RoomAvailability { get; set; }

        public OfferTypes OfferType { get; set; }

        public int CampaignItemId { get; set; }

        public bool SubrateApplied { get; set; }

        public Guid ClientId { get; set; }

        /// <summary>
        /// Constructs a offer item
        /// </summary>
        /// <param name="sku">Product sku</param>
        /// <param name="priceGroup">Price group name</param>
        /// <param name="supplier">Supplier sitecore item</param>
        public OfferDataItem(Category category, string sku, string priceGroup, Item supplier,SearchKey searchTerm)
        {
            Product product;

            try
            {
                product = CatalogLibrary.GetProduct(sku);
            }
            catch (ArgumentException ex )
            {
                Diagnostics.WriteException(DiagnosticsCategory.Search,  ex, typeof(OfferDataItem));
                return;
            }

            UpdateProductInformation(category, product, priceGroup,searchTerm);
            this.SupplierId = supplier.ID.ToString();
        }

        /// <summary>
        /// Constructs a offer item
        /// </summary>
        /// <param name="product">UCommerce product</param>
        /// <param name="priceGroup">Price group name</param>
        /// <param name="supplier">Supplier sitecore item</param>
        public OfferDataItem(Category category, Product product, string priceGroup, Item supplier,SearchKey searchTerm)
        {
            UpdateProductInformation(category, product, priceGroup, searchTerm);
            this.SupplierId = supplier.ID.ToString();
        }

        /// <summary>
        /// Get collection of related offers to the current offer
        /// </summary>
        /// <returns>Returns collection of related offers</returns>
        public Collection<OfferDataItem> GetRelatedOffers()
        {
            Collection<OfferDataItem> relatedOffers = new Collection<OfferDataItem>();

            if(this.PriceDetails != null && this.PriceDetails.ApplicableItem != null)
            {
                List<ProductCampaignDetail> offers = this.PriceDetails.ApplicableItem.CampaignDetail;

                foreach (ProductCampaignDetail campaignItem in offers)
                {
                    OfferDataItem temp = (OfferDataItem)this.Clone();
                    temp.OfferName = campaignItem.CampaignName;
                    temp.Price = campaignItem.TotalPrice;
                    temp.Availability = campaignItem.OfferAvailability;
                    temp.CampaignItemId = campaignItem.CampaignItemId;
                    temp.OfferType = OfferTypes.UnhandledAward;

                    // get award type information
                    // currently supports only one award per campaign item
                    LBMarketingService marketingService = new LBMarketingService(ObjectFactory.Instance.Resolve<IRepository<CampaignItem>>(), ObjectFactory.Instance.Resolve<ITargetingContextAggregator>(), ObjectFactory.Instance.Resolve<ITargetAggregator>(), ObjectFactory.Instance.Resolve<IAwardAggregator>());
                    // CampaignItem item = marketingService.GetActiveCampaignItems().Where(i => i.CampaignItemId == campaignItem.CampaignItemId).FirstOrDefault();
                    CampaignItem item = CampaignItem.Get(campaignItem.CampaignItemId);
                    IAward award = item == null ? null : marketingService.GetAwards(item).FirstOrDefault();

                    if(award != null)
                    {
                        switch (award.GetType().Name)
                        {
                            case "AmountOffOrderLinesAward":
                                temp.OfferType = OfferTypes.AmountOffFromOrderLine;
                                break;
                            case "AmountOffOrderTotalAward":
                                temp.OfferType = OfferTypes.AmountOffFromTotal;
                                break;
                            case "AmountOffUnitAward":
                                temp.OfferType = OfferTypes.AmountOffFromUnitPrice;
                                break;
                            case "DiscountSpecificOrderLineAward":
                                temp.OfferType = OfferTypes.DiscountSpecificOrderLine;
                                break;
                            case "FreeGiftAward":
                                temp.OfferType = OfferTypes.FreeGift;
                                break;
                            case "FreeSkuAward":
                                temp.OfferType = OfferTypes.FreeSku;
                                break;
                            case "PercentOffOrderTotalAward":
                                temp.OfferType = OfferTypes.PercentageOffFromTotal;
                                break;
                            case "PercentOffOrderLinesAward":
                                temp.OfferType = OfferTypes.PercentageOffFromOrderLine;
                                break;
                            case "PercentOffShippingTotalAward":
                                temp.OfferType = OfferTypes.PercentageOffFromShipping;
                                break;
                            case "FixPriceAward":
                                temp.OfferType = OfferTypes.SpecialPrice;
                                break;
                            case "UpgradeProductSkuAward":
                                temp.OfferType = OfferTypes.UpgradeRoomSku;
                                break;
                            default:
                                temp.OfferType = OfferTypes.UnhandledAward;
                                break;
                        }
                    }

                    this.UpdateSubrateInformation(temp.CampaignItemId.ToString(), temp);

                    relatedOffers.Add(temp);
                }
            }

            return relatedOffers;
        }

        /// <summary>
        /// Get the price with currency details
        /// </summary>
        /// <param name="price">The final price (price - discount)</param>
        /// <returns>Returns a formatted string</returns>
        public string GetPriceWithCurrency(decimal price)
        {
            if (this.CurrencyDetails == null && this.UcommerceCurrencyDetails != null)
            {
                return StoreHelper.GetPriceWithCurrency(price, null, this.UcommerceCurrencyDetails.ISOCode);
            }
            else if (this.CurrencyDetails == null && this.UcommerceCurrencyDetails == null)
            {
                return string.Empty;
            }

            return this.CurrencyDetails.GetPriceWithCurrency(price);
        }

        /// <summary>
        /// Updates the occupancy type information
        /// </summary>
        public void UpdateOccupancyType()
        {
            this.OccupancyType = new OccupancyType(this.OccupancyTypeId);
        }

        /// <summary>
        /// Get the price group object from price group name
        /// </summary>
        /// <param name="priceGroupName">The price group name</param>
        /// <returns>Returns the price group object</returns>
        public PriceGroup GetPriceGroup(string priceGroupName)
        {
            try
            {
                List<PriceGroup> priceGroupList = PriceGroup.All().ToList();
                var priceList = priceGroupList.Where(p => IsClientDefaultPriceGroup(p, priceGroupName));
                return priceList.First();
            }
            catch(Exception ex)
            {
                string errorMessage = "Price group " + priceGroupName + " not found.";
                Diagnostics.WriteException(DiagnosticsCategory.Search, new AffinionException(errorMessage, ex), typeof(OfferDataItem));
                return null;
            }
        }

        /// <summary>
        /// Clones the current object
        /// </summary>
        /// <returns>Returns a fresh copy with current values</returns>
        public object Clone()
        {
            return this.MemberwiseClone();
        }

        /// <summary>
        /// Update ucommerce product information to the offer item
        /// </summary>
        /// <param name="item">The offer sitecore item</param>
        /// <param name="priceGroupName">Current client name</param>
        private void UpdateProductInformation(Category category, Product product, string priceGroupName, SearchKey searchTerm)
        {

            if (product != null)
            {
                this.OfferType = OfferTypes.BaseProduct;
                this.ClientId = searchTerm.Client.ID.Guid;
                SearchCriteria criteria = new SearchCriteria();
                criteria.ArrivalDate = searchTerm.CheckinDate;
                criteria.Night = searchTerm.NoOfNights;
                criteria.NumberOfAdult = searchTerm.NumberOfAdults;
                criteria.NumberOfChild = searchTerm.NumberOfChildren;

                if(!string.IsNullOrEmpty(searchTerm.CurrencyCode))
                {
                    this.CurrencyDetails = new CurrencyInfo(searchTerm.CurrencyCode);
                    if(this.CurrencyDetails.Errors == null)
                    {
                        criteria.PriceGroup = this.GetPriceGroup(this.CurrencyDetails.PriceGroup);
                    }
                }
               // LoyaltyBuildCatalogLibrary.GetProductByCategory(category, criteria, category.ProductCatalog);
                LBPriceCalculation price = LoyaltyBuildCatalogLibrary.CalculatePrice(product, criteria, category.ProductCatalog);

                if (price != null)
                {
                    this.PriceDetails = price;
                    this.IsDiscounted = price.IsDiscounted;

                    if (price.YourPrice != null && price.Discount != null)
                    {
                        this.Price = price.YourPrice.Amount.Value;
                        this.DiscountedPrice = price.Discount.Amount.Value;
                        this.UcommerceCurrencyDetails = price.YourPrice.Amount.Currency;
                    }
                }

                this.CurrencyDetails = string.IsNullOrEmpty(searchTerm.CurrencyCode) ? null : new CurrencyInfo(searchTerm.CurrencyCode);
            }

            this.OfferSku = product.Sku;
            this.OccupancyTypeId = product.Sku;
            this.OfferGroupId = string.Empty;

            this.UpdateOccupancyType();
            this.UpdateSubrateInformation(product.Sku, this);            
        }

        /// <summary>
        /// update subrate information
        /// </summary>
        /// <param name="product">the product</param>
        /// <param name="searchTerm">the search term</param>
        public void UpdateSubrateInformation(string subrateItemId, OfferDataItem offer)
        {
            try
            {
                // when indexing happens context database is empty
                Database database = Sitecore.Context.Database == null ? Sitecore.Data.Database.GetDatabase("web") : Sitecore.Context.Database;
                SubrateService subrateService = new SubrateService();
                List<CurrencyDetail> currencyList = subrateService.GetAllCurrency(database);

                if(offer.UcommerceCurrencyDetails == null)
                {
                    offer.SubrateApplied = false;
                    return;
                }

                string currency = offer.UcommerceCurrencyDetails.Name;
                string currencyId = string.Empty;
                if (currencyList != null && currencyList.Any(x => x.Name.Equals(currency, StringComparison.InvariantCultureIgnoreCase)))
                {
                    currencyId = currencyList.FirstOrDefault(x => x.Name.Equals(currency, StringComparison.InvariantCultureIgnoreCase)).CurrencyGUID;
                }

                List<SubratesDetail> subrateDetails = subrateService.GetSubRateByClientAndItemId(subrateItemId, offer.ClientId.ToString(), currencyId);
                offer.SubrateApplied = subrateDetails.Count() > 0;

                if (offer.SubrateApplied)
                {
                    SubratesDetail lbCommision = subrateDetails.Where(sub => sub.SubrateItemCode == "LBCOM").FirstOrDefault();
                    offer.SubrateApplied = lbCommision != null && lbCommision.TotalAmount > 0;
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Error occured in reading subrate information.";
                Diagnostics.WriteException(DiagnosticsCategory.Search, new AffinionException(errorMessage, ex), typeof(OfferDataItem));
                offer.SubrateApplied = false;
            }
        }

        /// <summary>
        /// Checks the default price group of the client
        /// </summary>
        /// <param name="priceGroup">Current price group from the ucommerce price group list</param>
        /// <param name="clientName">Current client name</param>
        /// <returns>Returns true if the given price group is the default price group of the given client</returns>
        private static bool IsClientDefaultPriceGroup(PriceGroup priceGroup, string clientName)
        {
            string priceGroupName = priceGroup.Name.ToLowerInvariant();
            clientName = clientName.ToLowerInvariant();
            return priceGroupName.IndexOf(clientName) >= 0;
        }
    }

    /// <summary>
    /// Defines the offer types
    /// (Keeping space to add more offer types in future)
    /// </summary>
    public enum OfferTypes
    {
        BaseProduct=1,
        FreeSku,
        UpgradeRoomSku,
        FreeGift,
        SpecialPrice,
        AmountOffFromTotal,
        AmountOffFromOrderLine,
        AmountOffFromUnitPrice,
        PercentageOffFromOrderLine,
        PercentageOffFromTotal,
        PercentageOffFromShipping,
        DiscountSpecificOrderLine,
        UnhandledAward
    }
}
