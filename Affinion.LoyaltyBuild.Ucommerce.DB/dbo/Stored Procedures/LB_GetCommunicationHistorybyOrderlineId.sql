﻿CREATE PROCEDURE [dbo].[LB_GetCommunicationHistorybyOrderlineId] 
	@orderlineId INT 	  
AS
BEGIN
	SET NOCOUNT ON;
	SELECT CH.[HistoryId]
      ,CT.[Name]
      ,CH.[OrderLineId]
      ,CH.[CreatedDate]
	  ,CH.[From]
	  ,CH.[To]
	  ,CH.[EmailSubject]
      ,CH.[EmailText]
      ,CH.[EmailHtml]
	FROM [dbo].[LB_CommunicationHistory] CH
		INNER JOIN [dbo].[LB_CommunicationTypes] CT ON CT.CommunicationTypeId=CH.CommunicationTypeId
	WHERE CH.OrderLineId IN (SELECT OrderLineID FROM [dbo].[LB_FnRelatedOrderLines](@OrderLineId))  
END
