namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.PrimaryLayouts.ClientPortal
{
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Sitecore.Web.UI.HtmlControls;
    using System;
    using System.Web;
    using System.Web.UI;
    using Affinion.LoyaltyBuild.Common.Utilities;

    public partial class ClientPortalEmptyPrimaryLayout : BasePrimaryLayout
    {
        public string CurrentSiteUrl { get; set; }

        //Gets and sets the theme for primary layout
        public string selectedTheme { get; set; }

        private void Page_Load(object sender, System.EventArgs e)
        {
            //load the selected theme
            selectedTheme = ThemeSelector.LoadSelectedTheme();

            CurrentSiteUrl = System.Web.HttpContext.Current.Request.Url.ToString().Replace(Request.RawUrl, "").Replace("http:","https:");
            Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("REALEX: cur site url in primary layout: {0}", CurrentSiteUrl));

            AddCss(CurrentSiteUrl + "/Resources/styles/bootstrap.min.css");
            AddCss(CurrentSiteUrl + "/Resources/styles/responsiveNav.css");
            AddCss(CurrentSiteUrl + "/Resources/styles/affinion.min.css");
            AddCss(CurrentSiteUrl + string.Format("/Resources/styles/{0}/theme.css", selectedTheme));
            AddCss(CurrentSiteUrl + "/Resources/styles/affinion.custom.css");
            AddCss(CurrentSiteUrl + "/Resources/styles/font-awesome.min.css");
            AddCss(CurrentSiteUrl + "/Resources/styles/jquery.bxslider.css");
            AddCss(CurrentSiteUrl + "/Resources/styles/bootstrap-social.css");

            AddJquery(CurrentSiteUrl + "/Resources/scripts/jquery.min.js");
        }

        private void AddCss(string path)
        {
            Literal cssFile = new Literal() { Text = @"<link href=""" + path + @""" type=""text/css"" rel=""stylesheet"" />" };
            this.Page.Header.Controls.Add(cssFile);
        }

        private void AddJquery(string path)
        {
            Literal jqueryFile = new Literal() { Text = @"<script src=""" + path + @""" type=""text/javascript"" ></script>" };
            this.Page.Header.Controls.Add(jqueryFile);
        }


        public override System.Web.UI.HtmlControls.HtmlForm MainForm
        {
            get
            {
                return this.mainform;
            }
        }
    }
}
