﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class SeasonFixPriceAwardDetail : IEntity
    {
        // Fields
        private int? _hashCode;

        // Methods
        public new static IQueryable<SeasonFixPriceAwardDetail> All()
        {
            return GetRepo().Select();
        }

        public virtual void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<SeasonFixPriceAwardDetail, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            SeasonFixPriceAwardDetail objA = obj as SeasonFixPriceAwardDetail;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<SeasonFixPriceAwardDetail, bool>> expression)
        {
            return All().Any<SeasonFixPriceAwardDetail>(expression);
        }

        public static IList<SeasonFixPriceAwardDetail> Find(Expression<Func<SeasonFixPriceAwardDetail, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<SeasonFixPriceAwardDetail>();
        }

        public static SeasonFixPriceAwardDetail FirstOrDefault(Expression<Func<SeasonFixPriceAwardDetail, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<SeasonFixPriceAwardDetail>();
        }

        public new static SeasonFixPriceAwardDetail Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<SeasonFixPriceAwardDetail> GetRepo()
        {
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<SeasonFixPriceAwardDetail>>();

        }


        public virtual void Save()
        {
            GetRepo().Save(this);
        }

        public static SeasonFixPriceAwardDetail SingleOrDefault(Expression<Func<SeasonFixPriceAwardDetail, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties

        public virtual DateTime StartDate { get; set; }

        public virtual DateTime EndDate { get; set; }

        public virtual decimal AmountOff { get; set; }

        public virtual SeasonFixPriceAward SeasonFixPriceAward { get; set; }

        public virtual int SeasonFixPriceAwardDetailId { get; set; }

        

        public virtual int Id
        {
            get
            {
                return this.SeasonFixPriceAwardDetailId;
            }
        }
    }
}
