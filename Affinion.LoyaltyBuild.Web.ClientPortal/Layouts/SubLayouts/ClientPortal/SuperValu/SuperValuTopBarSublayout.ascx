﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SuperValuTopBarSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.SuperValuTopBarSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="container-outer-inner width-full header-top-bar" id="header-wrapper">
    <div class="container">
        <!-- The justified navigation menu is meant for single line per list item.
           Multiple lines will require custom code not provided by Bootstrap. -->
        <div class="row coomon-outer-padding">

            <div class="col-md-6 col-xs-6">
                <div class="logo">
                    <sc:Placeholder ID="logo" Key="logo" runat="server" />
                </div>
            </div>

            <div class="col-md-6 col-xs-6">
                <div class="header-right-content-nav width-full hidden-xs">
                    <div class="container-fluid">
                        <nav class="navbar navbar-default no-margin-bottom ">
                            <div class="container-fluid">
                                <div class="navbar-header">
                                    <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>
                                <div class="navbar-collapse collapse" id="navbar" aria-expanded="false" style="height: 1px;">
                                    <ul class="nav navbar-nav">
                                        <sc:Placeholder ID="toplinks" Key="toplinks" runat="server" />
                                    </ul>
                                </div>
                                <!--/.nav-collapse -->
                            </div>
                            <!--/.container-fluid -->
                        </nav>
                    </div>
                </div>
                
                <sc:Placeholder ID="topcallnowlink" Key="topcallnowlink" runat="server" />

                <sc:Placeholder ID="topcartlink" Key="topcartlink" runat="server" />

            </div>

        </div>

    </div>
</div>