﻿using System;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Model;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Message.Interfaces;
using System.Collections.Generic;

namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Message
{
    [Serializable]
    public class JtSearchResponse : JtResponse, IJtResponse
    {
        public string SearchURL { get; set; }
        public List<PropertyResult> PropertyResults { get; set; }
    }
}
