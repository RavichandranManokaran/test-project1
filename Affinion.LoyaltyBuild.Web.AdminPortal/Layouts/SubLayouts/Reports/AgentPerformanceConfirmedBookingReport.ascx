﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AgentPerformanceConfirmedBookingReport.ascx.cs" Inherits="Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.Reports.AgentPerformanceConfirmedBookingReport" %>
<%@ register tagprefix="sc" namespace="Sitecore.Web.UI.WebControls" assembly="Sitecore.Kernel" %>
<%@ import namespace="Sitecore.Data.Items" %>
    <div class="martop10 Agent Performance Report">
        <div>
           <sc:text id="PleaseSelectLabel" runat="server" />
        </div>
    </div>
<div>
   <div class="row report-filter container-fluid">
      <div class="col-xs-12 martop10">
         <div class="row">
            <div class="col-md-3">
               <sc:text id="DateFromLabel" runat="server" />
            </div>
            <div class="col-md-3 ">
               <div class="input-group date">
                  <asp:TextBox class="form-control txtDateFrom" ID="txtDateFrom" runat="server" MaxLength="50"/>
                  <asp:RequiredFieldValidator ID="dateFromRequiredField" runat="server" style="color:red" ControlToValidate="txtDateFrom" Display="Dynamic" validationgroup="ReportValidationGroup">
                     <sc:text id="DateFromRequiredLabel" runat="server" />
                  </asp:RequiredFieldValidator>
               </div>
            </div>
            <div class="col-md-3">
               <sc:text id="PartnerLabel" runat="server" />
            </div>
            <div class="col-md-3">
               <asp:DropDownList ID="ddlPartner" runat="server" CssClass="form-control" AppendDataBoundItems="false" AutoPostBack="true" OnSelectedIndexChanged="ddlPartner_OnSelectedIndex">
               </asp:DropDownList>
               <asp:RequiredFieldValidator ID="partnerRequiredField" runat="server" style="color:red" ControlToValidate="ddlPartner" Display="Dynamic"  InitialValue="0" validationgroup="ReportValidationGroup">
                  <sc:text id="PartnerRequiredLabel" runat="server" />
               </asp:RequiredFieldValidator>
            </div>
         </div>
      </div>
      <div class="col-xs-12  martop10">
         <div class="row">
            <div class="col-md-3">
               <sc:text id="DateToLabel" runat="server" />
            </div>
            <div class="col-md-3 marT5">
               <div class="input-group date">
                  <asp:TextBox class="form-control txtDateTo" ID="txtDateTo" runat="server" MaxLength="50"/>
                  <asp:RequiredFieldValidator ID="dateToRequiredField" runat="server" style="color:red" ControlToValidate="txtDateTo" Display="Dynamic" validationgroup="ReportValidationGroup">
                     <sc:text id="ToDateRequiredLabel" runat="server" />
                  </asp:RequiredFieldValidator>
               </div>
            </div>
            <div class="col-md-3">
               <sc:text id="AgentLabel" runat="server" Text="Agent Name" />
            </div>
            <div class="col-md-3">
               <asp:DropDownList ID="ddlAgentDropDown" runat="server" CssClass="form-control" AppendDataBoundItems="false" AutoPostBack="true" />

            </div>
            <div class="col-md-3">
               <asp:CompareValidator ID="compareDatesField" ControlToCompare="txtDateFrom"  style="color:red" ControlToValidate="txtDateTo" Type="Date" Operator="GreaterThanEqual" Display="Dynamic" validationgroup="ReportValidationGroup" runat="server">
                  <sc:text id="CompareDatesLabel" runat="server" />
               </asp:CompareValidator>
               <button type="button" class="listAgentPerformance btn btn-primary" validationgroup="ReportValidationGroup" OnServerClick="ListAgentPerformance_Click" runat="server">
                  <sc:text id="ListAgentPerformanceTxt" runat="server" />
               </button>
            </div>
         </div>
      </div>
   </div>
     
       <asp:HiddenField ID="hidGridView" runat="server" />
<div id="printDiv" runat="server">
   <asp:Repeater ID="reportRepeater" runat="server" OnItemDataBound="rpt_ItemDataBound">
      <ItemTemplate>
         <div runat="server" id="ResultDiv" clientidmode="Static" visible="false">
            <div id="data-grid" class="table-responsive agentperformance">
               <asp:GridView ID="ResultGridView" runat="server" Width="100%" SelectedRowStyle-Font-Bold="true" AlternatingRowStyle-BackColor="#d8d8d8"
                   CssClass="table table-striped table-bordered reptab martop10 agentperformance"
                   AutoGenerateColumns="False" ShowFooter="true" EnableViewState="false">
                  <Columns>
                     <asp:TemplateField ItemStyle-Width="300px" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle">
                         <HeaderTemplate>                                        
                                <asp:Label ID="LblAgentNameHeader" runat="server" Text=""/>
                        </HeaderTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField ItemStyle-Width="300px" FooterStyle-CssClass="agentPerformance-RightAlign" FooterStyle-HorizontalAlign="Center">
                        <HeaderTemplate><%=LblDateText%>
                        </HeaderTemplate>
                        <ItemTemplate>
                           <%#Eval("Date")%>
                        </ItemTemplate>
                         <FooterTemplate>
                             <asp:Label ID="lblTotalText" runat="server" Text="" CssClass="agentPerformance-TotalTextFooter"/>
                        </FooterTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderStyle-CssClass="agentperformance-CenterHead" ItemStyle-CssClass="agentPerformance-CenterAlign"  FooterStyle-CssClass="agentPerformance-CenterAlign">
                        <HeaderTemplate><%=LblTotalBookingMade %>
                        </HeaderTemplate>
                        <ItemTemplate>
                           <%#Eval("TotalBookingMade")%>
                        </ItemTemplate>
                        <FooterTemplate>
                             <asp:Label ID="lblTotalBookingMadeCount" runat="server" Text=""/>
                        </FooterTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderStyle-CssClass="agentperformance-CenterHead" ItemStyle-CssClass="agentPerformance-CenterAlign" FooterStyle-CssClass="agentPerformance-CenterAlign">
                        <HeaderTemplate><%=LblTotalBookingConfirmed %>
                        </HeaderTemplate>
                        <ItemTemplate>
                           <%#Eval("TotalBookingConfirmed")%>
                        </ItemTemplate>
                         <FooterTemplate>
                             <asp:Label ID="lblTotalBookingConfCount" runat="server" Text=""/>
                        </FooterTemplate>
                     </asp:TemplateField>
                  </Columns>
                  <EmptyDataTemplate>
                     ---No Records---
                  </EmptyDataTemplate>
               </asp:GridView>
            </div>
         </div>
      </ItemTemplate>

   </asp:Repeater>
</div>
    
 </div>
<div class="col-sm-4 col-xm-12" id="PrintButtonSection" Visible="False" runat="server">
      <button type="button" id="btnPrintReport" class="btnPrintReport btn btn-primary" runat="server"><sc:text id="PrintLabel" runat="server" /> </button>   
     <button type="button" class="btnExportToExcel btn btn-primary" OnServerClick="btnExportIntoExcel_OnClick" runat="server">
         <sc:text id="ExcelLabel" runat="server" />
         </button>
 </div>

                 

    <script lang="javascript" type="text/javascript">
        function AssignExportHTML() {
            document.getElementById("<%= hidGridView.ClientID %>").value = htmlEscape(printDiv.innerHTML);
        }
        function htmlEscape(str) {
            return String(str)
            .replace(/&/g, '&amp;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '&#39;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');
        }
    $(function () {
        $('.txtDateFrom').datepicker({
            defaultDate: new Date(),
            dateFormat: "<%=Affinion.LoyaltyBuild.Common.Utilities.DateTimeHelper.MonthDateFormat%>",
           minDate: '-18m',
           maxDate: 0,
           numberOfMonths: 1,
           showOn: "both",
           buttonImage: "images/calendar.png",
           buttonImageOnly: false,
           buttonClass: "glyphicon-calendar"
       }).next(".ui-datepicker-trigger").addClass("btn btn-default glyphicon glyphicon-calendar calander-icon pull-left");

       $('.txtDateTo').datepicker({
           defaultDate: new Date(),
           dateFormat: "<%=Affinion.LoyaltyBuild.Common.Utilities.DateTimeHelper.MonthDateFormat%>",
          minDate: '-18m',
          maxDate: 0,
          numberOfMonths: 1,
          showOn: "both",
          buttonImage: "images/calendar.png",
          buttonImageOnly: false,
          buttonClass: "glyphicon-calendar"
      }).next(".ui-datepicker-trigger").addClass("btn btn-default glyphicon glyphicon-calendar calander-icon pull-left");

       $('.listAgentPerformance').click(function (e) {

           if (!Page_ClientValidate("ReportValidationGroup")) {
              
               for ( var i = 0; i <= Page_Validators.length; i++) {
                   if (!Page_Validators[i].isvalid) {
                       $("#" + Page_Validators[i].controltovalidate).addClass('has-error-input');
                   }
                   else {

                       if ($("#" + Page_Validators[i].controltovalidate).val().length > 0)
                           $("#" + Page_Validators[i].controltovalidate).removeClass("has-error-input");
                   }
               }
               e.preventDefault();
               return false;
           }
           return true;


       });
       $('.btnPrintReport').click(function (e) {
           var divContents = $("#maincontentadmin_0_printDiv").html();
           var leftPosition, topPosition;
           leftPosition = (window.screen.width / 2) - (410);
           topPosition = (window.screen.height / 2) - (350);
           var printWindow = window.open('', "Agent Performance Report", "status=no,height=600,width=800,resizable=yes,left="
           + leftPosition + ",top=" + topPosition + ",screenX=" + leftPosition + ",screenY="
           + topPosition + ",toolbar=no,menubar=no,scrollbars=yes,location=no,directories=no");

           printWindow.document.write('<html><head><title>Agent Performance Report</title></head><body>');
           printWindow.document.write(divContents);
           printWindow.document.write('</body></html>');

           printWindow.document.close();

           printWindow.print();
       });
   });
</script>