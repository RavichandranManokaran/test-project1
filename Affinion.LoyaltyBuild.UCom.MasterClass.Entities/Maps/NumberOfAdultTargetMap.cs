﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class NumberOfAdultTargetMap : SubclassMap<NumberOfAdultTarget>
    {
        // Methods
        public NumberOfAdultTargetMap()
        {
            base.Table("LB_NumberOfAdultTarget");
            base.KeyColumn("NumberOfAdultTargetId");
            base.Map(x => x.NumberOfAdultTargetId).ReadOnly();
            base.Map(x => x.NumberOfAdult).Not.Nullable();
        }

    }

}
