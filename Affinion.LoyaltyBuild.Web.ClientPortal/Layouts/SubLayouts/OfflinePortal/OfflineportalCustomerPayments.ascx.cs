﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model.Payment;
using service = Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Payment;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Subrate;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
using Affinion.LoyaltyBuild.Web.ClientPortal.Helper;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic;
using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
using Affinion.Loyaltybuild.BusinessLogic;
using Affinion.Loyaltybuild.BusinessLogic.Helper;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Email;
using Affinion.LoyaltyBuild.UCom.Common.Constants;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Payment;
using UCommerce.EntitiesV2;
using Affinion.LoyaltyBuild.Api.Booking.Service;
using UCommerce.Infrastructure;
using System.Globalization;
using UCommerce.Transactions.Payments;
using UCommerce;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    public partial class OfflineportalCustomerPayments : BaseSublayout
    {
        private service.IPaymentService _paymentService;
        public int orderLineId = 0;
        public string orderGuid = string.Empty;
        public int currencyTypeID = 0;
        public decimal paymentAmount = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            _paymentService = ContainerFactory.Instance.GetInstance<service.IPaymentService>();
            int.TryParse(QueryString("olid"), out orderLineId);
            int.TryParse(QueryString("cid"), out currencyTypeID);
            //orderGuid = _paymentService.GetOrderGuidByOrderlineId(orderLineId);
            PopulateDefaultValues();
        }

        public void PopulateDefaultValues()
        {
            _paymentService = ContainerFactory.Instance.GetInstance<service.IPaymentService>();

            if (!IsPostBack)
            {
                IBookingService bookingservice = ObjectFactory.Instance.Resolve<IBookingService>();

                var booking = bookingservice.GetBooking(orderLineId);
                var clientId = booking.ClientId;
                ddlPaymentmethod.DataSource = _paymentService.GetPaymentMethods(clientId);
                ddlPaymentmethod.DataTextField = "Name";
                ddlPaymentmethod.DataValueField = "Id";
                ddlPaymentmethod.DataBind();

                List<Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model.CurrencyDetail> currencyList = new List<Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model.CurrencyDetail>();
                currencyList = _paymentService.GetCurrencyListDetails();
                ddlCurrency.DataSource = currencyList;
                ddlCurrency.DataTextField = "Name";
                ddlCurrency.DataValueField = "CurrencyID";
                ddlCurrency.DataBind();
            }
            List<TotalDueItem> tmpDueItem = new List<TotalDueItem>();
            tmpDueItem = _paymentService.GetDuesByOrderlineID(orderLineId, currencyTypeID);
            if (tmpDueItem != null)
            {
                paymentAmount = tmpDueItem[0].Amount;
                txtAmount.Text = Convert.ToString(paymentAmount);
                currencyTypeID = tmpDueItem[0].CurrencyTypeId;
            }



            ListItem listItem = ddlCurrency.Items.FindByValue(Convert.ToString(currencyTypeID));
            if (listItem != null)
            {
                ddlCurrency.ClearSelection();
                listItem.Selected = true;
            }
        }

        protected void BtnPayNow_Click(object sender, EventArgs e)
        {
            var purchaseOrder = UCommerce.EntitiesV2.OrderLine.FirstOrDefault(i => i.OrderLineId == orderLineId).PurchaseOrder;
            var paymentMethod = UCommerce.EntitiesV2.PaymentMethod.FirstOrDefault(i => i.PaymentMethodId == int.Parse(ddlPaymentmethod.SelectedValue));
            var amount = Convert.ToDecimal(txtAmount.Text);
            var payment = ExecutePaymentMethodService(paymentMethod, purchaseOrder, amount);
                
            //ddlPaymentmethod.SelectedItem.Text = "card";
            if (ddlPaymentmethod.SelectedItem.Text.ToLower().Contains("card") || ddlPaymentmethod.SelectedItem.Text.ToLower().Contains("credit"))
            {
                System.Text.StringBuilder redirectURL = new System.Text.StringBuilder();
                string redirectstring = Request.Url.ToString().Replace("http:", "https:");
                redirectstring = redirectstring.Replace(HttpUtility.UrlDecode(Request.RawUrl), "/TransferPaymentGateway?");

                redirectURL.Append(redirectstring);
                redirectURL.Append("cid=");
                redirectURL.Append(ddlCurrency.SelectedItem.Text);
                redirectURL.Append("&guid=");
                redirectURL.Append(payment["paymentGuid"]);
                redirectURL.Append("&olid=");
                redirectURL.Append(Convert.ToString(orderLineId));
                redirectURL.Append("&amt=");
                redirectURL.Append(txtAmount.Text);
                Response.Redirect(redirectURL.ToString());
            }
            else
            {
                // _paymentService = ContainerFactory.Instance.GetInstance<service.IPaymentService>();
                var pymtdetails = new PaymentDetail();
                pymtdetails.OrderLineID = orderLineId;
                pymtdetails.CurrencyTypeId = Convert.ToInt32(ddlCurrency.SelectedValue);
                pymtdetails.Amount = Convert.ToDecimal(txtAmount.Text);
                pymtdetails.PaymentMethodID = Convert.ToInt32(ddlPaymentmethod.SelectedValue);
                pymtdetails.ReferenceID = string.Empty;
                pymtdetails.OrderPaymentID = payment.PaymentId;
                //pymtdetails.CreatedBy = "test";
                pymtdetails.CreatedBy = SitecoreUserProfileHelper.GetItem(UserProfileKeys.CreatedBy);
                ValidationResponse result = _paymentService.AddPayment(pymtdetails);


                if (result.IsSuccess)
                {
                    SendPartialConfirmationMail(orderLineId);
                    Response.Redirect("/PaymentHistory?olid=" + orderLineId);
                }

            }
        }

        /// <summary>
        /// Execute payment method service to build and load payment gateway
        /// </summary>
        /// <param name="paymentMethod">Selected payment method</param>
        /// <param name="purchaseOrder">Current purchase order</param>
        /// <param name="amount">Amount to be paid</param>
        public Payment ExecutePaymentMethodService(UCommerce.EntitiesV2.PaymentMethod paymentMethod, UCommerce.EntitiesV2.PurchaseOrder purchaseOrder, decimal amount)
        {
            var cultureInfo = new CultureInfo(CultureInfo.InvariantCulture.LCID);

            ///Create a new payment request to 
            var paymentRequest = new PaymentRequest(
                        purchaseOrder,
                        paymentMethod,
                        new Money(amount,
               new CultureInfo(CultureInfo.InvariantCulture.LCID), purchaseOrder.BillingCurrency)

               );

            ///Build and load the payment page
            return ((IPaymentFactory)paymentMethod.GetPaymentMethodService()).CreatePayment(paymentRequest);
        }

        public void SendPartialConfirmationMail(int orderLineID)
        {
            
                List<TotalDueItem> tmpDueItem = new List<TotalDueItem>();
            
                tmpDueItem = _paymentService.GetDuesByOrderlineID(orderLineId, currencyTypeID);

               
            
                if (tmpDueItem.Count == 0)
                {
                    var ClientId = Affinion.Loyaltybuild.BusinessLogic.Helper.BasketHelper.GetOrderLineClientId(orderLineId);
                    MailService mail = new MailService(ClientId);

                    mail.GenerateEmail(MailTypes.PartialConfirmationEmailRemainder, orderLineId);
                }
            }

        }

        /// <summary>
        /// 
        /// </summary>


    }
