﻿CREATE PROCEDURE [dbo].[LB_UpdateCustomOrderDetails] 
	-- Add the parameters for the stored procedure here
	@Id as int,
	@CheckinDate as datetime,
	@CheckOutDate as datetime,
	@NoofChilderen as int,
	@NofAdults as int,
	@Sku as varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Update statements for procedure here
	UPDATE uCommerce_OrderLine
	SET [CheckinDate] = @CheckinDate
      ,[CheckOutDate] = @CheckOutDate
      ,[NoofChilderen] = @NoofChilderen
      ,[NoOfAdults] = @NofAdults
 WHERE [OrderLineId] = @Id and 
	[Sku] = @Sku
    
END