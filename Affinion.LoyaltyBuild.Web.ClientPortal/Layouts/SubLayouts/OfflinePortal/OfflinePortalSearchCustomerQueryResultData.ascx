﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalSearchCustomerQueryResultData.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalCustomerSearchResultData" %>
<%if(SearchQueryResultItems != null && SearchQueryResultItems.Count > 0){ %>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title"><sc:text id="ResultGridTitle"  field="ResultGridTitle" runat="server" /></h4>
    </div>
    <div class="panel-body alert-info">
        <div class="table-responsive">
            <table class="table tbl-calendar">
                <thead>
                    <tr>
                        <th> <sc:text id="ID"           field="ID" runat="server" /> </th>
                         <th> <sc:text id="Note"        field="Note" runat="server" /> </th>  
                         <th> <sc:text id="BookingNo"   field="BookingNo" runat="server" /> </th>    
                         <th> <sc:text id="Customer"    field="Customer" runat="server" /> </th>
                         <th> <sc:text id="Phone"       field="Phone" runat="server" /> </th>
                         <th> <sc:text id="Resolved"    field="Resolved" runat="server" /> </th>
                         <th> <sc:text id="Created"     field="Created" runat="server" /> </th>
                         <th> <sc:text id="Updated"     field="Updated" runat="server" /> </th>
                         <th> <sc:text id="Mail"        field="Mail" runat="server" /> </th>
                         <th> <sc:text id="Attachments" field="Attachments" runat="server" /> </th> 
                         <th> <sc:text id="Edit"        field="Edit" runat="server" /> </th>
                    </tr>
                </thead>
                <tbody>
                    <%foreach(var item in SearchQueryResultItems){ %>
                    <tr style="background-color: rgb(255, 255, 255);">
                        <td><span><%=item.ID%></span></td>
                        <td><span><%=item.Content %></span></td>
                        <td><span><%=item.BookingReference %></span></td>
                        <td><span><%=item.CustomerName %></span></td>
                        <td><span><%=item.Phone %></span></td>
                        <td><span><%=(item.IsResolved?"Yes":"No") %></span></td> 
                        <td><span><%=item.Created %></span></td>
                        <td><span><%=item.Updated %></span></td>
                        <td><a href="mailto:<%=item.Email %>" ><span class="glyphicon glyphicon-envelope"></span></a></td>
                        <td>
                        <%if (item.HasAttachments){%>
                         <a href="javascript:void(0)" onclick="CS.CommunicationService.ViewUploadFile('<%=item.QueryId %>')"> <span class="glyphicon glyphicon-download"></span></a>
                        <%}else{%>
                        &nbsp;
                        <%}%></td>
                        <td><a href="javascript:void(0)" onclick="CS.CommunicationService.LoadCustomerQuery('<%=item.QueryId %>')"> <span class="glyphicon glyphicon-edit"></span></a></td>
                    </tr>

                    <%} %>
                </tbody>
            </table>
        </div>
    </div>
</div>
<%}else {%>
<div class="alert alert-info">
    No matching records found
</div>
<%} %>
