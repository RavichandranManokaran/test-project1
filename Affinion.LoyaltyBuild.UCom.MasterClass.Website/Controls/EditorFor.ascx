﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditorFor.ascx.cs" Inherits="Affinion.LoyaltyBuild.UCom.MasterClass.Website.Controls.EditorFor" %>
<h1 runat="server" id="Header" />
<div>FirstName: <asp:TextBox runat="server" ID="FistNameTextBox"></asp:TextBox></div>
<div>LastName: <asp:TextBox runat="server" ID="LastNameTextBox"></asp:TextBox></div>
<div>Email: <asp:TextBox runat="server" ID="EmailTextBox" /></div>
<div>Company: <asp:TextBox runat="server" ID="CompanyTextBox"></asp:TextBox></div>
<div>Attention: <asp:TextBox runat="server" ID="AttentionTextBox"></asp:TextBox></div>
<div>Street: <asp:TextBox runat="server" ID="StreetTextBox"></asp:TextBox></div>
<div>Street 2: <asp:TextBox runat="server" ID="StreetTwoTextBox"></asp:TextBox></div>
<div>City: <asp:TextBox runat="server" ID="CityTextBox"></asp:TextBox></div>
<div>Postal code: <asp:TextBox runat="server" ID="PostalCodeTextBox"></asp:TextBox></div>
<div>Phone: <asp:TextBox runat="server" ID="PhoneTextBox"></asp:TextBox></div>
<div>Mobile phone: <asp:TextBox runat="server" ID="MobilePhoneTextBox"></asp:TextBox></div>
<div>Country: <asp:DropDownList runat="server" DataTextField="Name" DataValueField="CountryId" ID="CountryDropDownList" /></div>