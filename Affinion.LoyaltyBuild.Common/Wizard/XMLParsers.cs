﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion Inernational. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           XMLParsers.cs
/// Sub-system/Module:     ClientSetup(VS project name)
/// Description:           XMLParsers Class read xml file and return WizardDialog object
/// </summary>


#region Using Directives
using Affinion.LoyaltyBuild.Common.Exceptions;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
#endregion

namespace Affinion.LoyaltyBuild.Common.Wizard
{
    /// <summary>
    /// this is a xml parser class use to cread and format the xml
    /// </summary>
    public static class XMLParsers
    {
        #region Class variables
        private static string xmlUrl = "PATH_OF_XML_FILE"; //@"C:\Affinion SVN\trunk\Research\Shanjeeva\Wizard\Framework\SampleLib\Affinion.LoyaltyBuild.ClientSetup\Wizard\Config\WizardConfig.xml"; 
        #endregion
        
        #region Public Functions
        // Parse the xml using XMLDocument class.
        public static WizardDialog ParseByXMLDocument()
        {
            var wizardDialog = new WizardDialog();

            XmlDocument doc = new XmlDocument();
            doc.Load(xmlUrl);

            XmlNode GeneralInformationNode = doc.SelectSingleNode("/Wizardnformation/GeneralInformation");

            int number;
            bool result = Int32.TryParse(GeneralInformationNode.SelectSingleNode("NumberOfSteps").InnerText, out number);
            if (result)
                wizardDialog.NumberOfSteps = number;

            XmlNode WizardtListNode = doc.SelectSingleNode("/Wizardnformation/WizardDialogLists");

            //wizardDialog.NumberOfSteps = int.Parse(GeneralInformationNode.SelectSingleNode("NumberOfSteps").InnerText);
            //XmlNodeList WizardDialogLists = WizardtListNode.SelectNodes("WizardDialogLists");

            // bool isSave = false;
            using (XmlNodeList WizardDialogLists = WizardtListNode.SelectNodes("WizardDialogLists"))
            {
                foreach (XmlNode node in WizardDialogLists)
                {
                    //WizardDialog wizardDialog = new WizardDialog();
                    wizardDialog.isRequiredToFill = false; //IsSaveTryPass(node.Attributes.GetNamedItem("isRequiredToFill").Value, out isSave);  //bool.TryParse(node.Attributes.GetNamedItem("isRequiredToFill").Value, out isSave);
                    //wizardDialog.isRequiredToFill = bool.Parse(node.Attributes.GetNamedItem("isRequiredToFill").Value);                   
                    wizardDialog.SampleDescription = node.Attributes.GetNamedItem("SampleDescription").Value;
                }
            }

            return wizardDialog;
        }
        #endregion

        #region XMLReader

        /// <summary>
        /// Read the xml file and pupulate the list of WizardDialog
        /// Parse the XML using XDocument class.
        /// </summary>
        /// <returns>WizardDialog</returns>
        public static List<WizardDialog> ParseByXDocument()
        {
            List<WizardDialog> listWizardDialog = null;
            try
            {
                ///This will add trace information to the log
                Diagnostics.Trace(DiagnosticsCategory.ClientSetup, "wizard module started");

                // create an object
                var wizardDialog = new WizardDialog();
                var htmlDictionary = new Dictionary<string, string>();

                XDocument doc = XDocument.Load(xmlUrl);
                XElement generalElement = doc
                        .Element("Wizardnformation")
                        .Element("GeneralInformation");

                wizardDialog.NumberOfSteps = Convert.ToInt32(generalElement.Element("NumberOfSteps").Value);

                //wizardDialog. = generalElement.Element("Department").Value;
                //bool isSave = false;
                // Linq query to fetch data
                listWizardDialog = (from c in doc.Descendants("WizardDialogList")

                                    select new WizardDialog()
                                    {
                                        isRequiredToFill = false, // IsSaveTryPass(c.Attribute("isRequiredToFill").Value, out isSave),     // bool.TryParse(c.Attribute("isRequiredToFill").Value, out isSave),

                                        //isRequiredToFill = bool.Parse(c.Attribute("isRequiredToFill").Value),
                                        SampleDescription = c.Attribute("SampleDescription").Value,

                                        HtmlElements = new HtmlElements()
                                        {
                                            ListElements = new List<Element>(from html in c.Descendants("htmltag")
                                                                             select new Element
                                                                             {
                                                                                 ID = html.Attribute("id").Value,
                                                                                 Name = html.Attribute("Name").Value,
                                                                                 Type = html.Attribute("Type").Value,
                                                                                 Placeholder = html.Attribute("Placeholder").Value,
                                                                                 Class = html.Attribute("Class").Value,
                                                                                 Value = html.Attribute("Value").Value,

                                                                             })
                                        }

                                    }).ToList<WizardDialog>();

                ///This will add trace information to the log
                Diagnostics.Trace(DiagnosticsCategory.ClientSetup, "wizard module xml reading completed");

            }
            catch (ArgumentException ex)
            {
                ///Catch argument exceptions
                ///First parameter : Module
                ///Second parameter: Exception
                ///Third parameter : Object logging
                Diagnostics.WriteException(DiagnosticsCategory.ClientSetup, ex, null);
                throw;
            }
            catch (AffinionException ex)
            {
                ///Catch affinion exceptions
                Diagnostics.WriteException(DiagnosticsCategory.ClientSetup, ex, null);
                throw;
            }
            catch (Exception ex)
            {
                ///Catch general exceptions
                Diagnostics.WriteException(DiagnosticsCategory.ClientSetup, ex, null);
                throw;
            }

            return listWizardDialog;
        }
        #endregion
    }
}
