﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Model
{
    [Serializable()]
    [XmlType(TypeName = "BookingDetails")]
    public class JtBookingDetailsPreBook
    {
        public string PropertyID { get; set; }
        public string ArrivalDate { get; set; }
        public string Duration { get; set; }
        public List<RoomBooking> RoomBookings { get; set; }
    }
}
