﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientPortalRecentlySeen.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal.ClientPortalRecentlySeen" %>
<asp:placeholder runat="server" ID="enableScript">
<script type="text/javascript">
    jQuery(document).ready(function () {
        var recentlySeenText = '<%=recentlySeenText%>';
        jQuery('#btnRecentlySeen').click(function () {
            jQuery("#popupdivRecentlySeen").dialog({
                title: recentlySeenText,
                position: 'center',
                stack: true,
                height: '600',
                width: '500',
                modal: true,
                buttons: {
                    "Close": function () {
                        // jQuery(this).dialog('<%=closeText%>');
                        $(this).dialog("close");
                    }
                }
            });

            jQuery("div").each(function () {
                if (jQuery(this).attr("aria-describedby") == "popupdivRecentlySeen") {
                    var topPos = (jQuery(window).height() - jQuery(this).outerHeight()) / 2;
                    var leftPos = (jQuery(window).width() - jQuery(this).outerWidth()) / 2;
                    if (topPos < 0) {
                        topPos = 0;
                    }
                    if (leftPos < 0) {
                        leftPos = 0;
                    }
                    jQuery(this).css({
                        top: topPos,
                        left: leftPos
                    });
                }
            });

            return false;
        });
    });

</script>
</asp:placeholder>
<a href="#" id="btnRecentlySeen" class="recentlySeenLink float-right header-right hidden-xs padding-left-small" style="color: white; padding-top: 11px;"><span class="glyphicon glyphicon-download"></span><sc:Text id="recentlyseenHotel" runat="server" /></a>
<div id="popupdivRecentlySeen" title="Basic modal dialog" style="display: none">
    <div>
        <asp:Repeater ID="rptRSeenHotelList" runat="server" OnItemDataBound="rptRSeenHotelList_DataBinding">
            <ItemTemplate>
                <div class="content_bg_inner_box summary-detail alert-info recentlySeenHotelDiv">
                    <div class="col-md-5 row col-sm-12">
                        <div class="col-xs-12 col-md-12">

                            <div class="basket-img">
                                <asp:Image ID="imgHotelImage" runat="server" AlternateText="" CssClass="img-responsive img-thumbnail" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7 col-sm-12">
                        <div class="row">
                            <div class="col-sm-12">
                                <span class="accent33-f">
                                    <h2><asp:Hyperlink ID="linkHotelName" runat="server"><strong><u><asp:Label ID="lblHotelName" runat="server"/></u></strong></asp:Hyperlink>
                                        <asp:Repeater ID="rptStarRating" runat="server">
                                            <ItemTemplate>
                                                <span class="glyphicon glyphicon-star accent50-f"></span>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <%--<span class="glyphicon glyphicon-star accent50-f"></span>
                                        <span class="glyphicon glyphicon-star accent50-f"></span>
                                        <span class="glyphicon glyphicon-star accent50-f"></span>
                                        <span class="glyphicon glyphicon-star accent50-f"></span>--%></h2>
                                    
                                    <asp:Label ID="lblLocation" runat="server"></asp:Label><br />
                                    <br />
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>

        <asp:Repeater ID="rptTopSearchHotel" runat="server" OnItemDataBound="rptTopSearchHotel_DataBinding">
            <ItemTemplate>
                <div class="content_bg_inner_box summary-detail alert-info topHotelDiv">
                    <div class="col-md-5 row col-sm-12">
                        <div class="col-xs-12 col-md-12">

                            <div class="basket-img">
                                <asp:Image ID="imgHotelImage" runat="server" AlternateText="" CssClass="img-responsive img-thumbnail" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7 col-sm-12">
                        <div class="row">
                            <div class="col-sm-12">
                                <span class="accent33-f">
                                    <h2>
                                        <asp:Hyperlink ID="linkHotelName" runat="server"><strong><u><asp:Label ID="lblHotelName" runat="server" /></u></strong></asp:Hyperlink>
                                        <asp:Repeater ID="rptStarRating" runat="server">
                                            <ItemTemplate>
                                                <span class="glyphicon glyphicon-star accent50-f"></span>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <%--<span class="glyphicon glyphicon-star accent50-f"></span>
                                        <span class="glyphicon glyphicon-star accent50-f"></span>
                                        <span class="glyphicon glyphicon-star accent50-f"></span>
                                        <span class="glyphicon glyphicon-star accent50-f"></span>--%></h2>

                                    <asp:Label ID="lblLocation" runat="server"></asp:Label><br />
                                    <br />
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>
