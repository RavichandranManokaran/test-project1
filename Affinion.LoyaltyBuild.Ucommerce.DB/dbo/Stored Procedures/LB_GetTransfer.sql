﻿CREATE PROCEDURE [dbo].[LB_GetTransfer]	
@OrderlineId int
as
begin 
	select orderlineid, ol.createdon, c.categoryid providerid, c.[guid] providerguid from ucommerce_orderline ol inner join ucommerce_product p on ol.sku = p.sku
	inner join ucommerce_categoryproductrelation pcr on p.productid = pcr.productid
	inner join ucommerce_category c on pcr.categoryid = c.categoryid
	where ol.orderlineid in (SELECT * FROM  LB_FnRelatedorderlines(@OrderlineId)) 
	order by ol.createdon
end
