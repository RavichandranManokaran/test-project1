﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BBHotelDetailsSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.BBHotelDetailsSublayout" %>
<%@ Import Namespace="Affinion.LoyaltyBuild.Common.Utilities" %>
<%@ Import Namespace="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts" %>
<%@ Import Namespace="Sitecore.Data.Items" %>
<%@ Import Namespace="Affinion.LoyaltyBuild.Search" %>
<%@ Import Namespace="Affinion.LoyaltyBuild.Search.Data" %>
<asp:PlaceHolder ID="plcBBwrapper" runat="server">
<div class="col-xs-12">
    <div class="col-xs-12 alert alert-warning">
        <div class="col-xs-12 col-md-3">
            <span class="display-block-val search-box-margin">
                <strong><sc:text id="CheckinLabel" field="CheckinDateLabel" runat="server" />
                </strong>
                <asp:Label ID="CheckingDateLabel" Text="" runat="server"></asp:Label>
            </span>
        </div>
        <div class="col-xs-12 col-md-3">
            <span class="display-block-val search-box-margin">
                <strong> <asp:Label ID="Label1" Text="Number Of Nights" runat="server"></asp:Label>
                </strong>
                <asp:Label ID="NumberOfNightsLabel" runat="server" Text=""></asp:Label>
            </span>
        </div>
        <div class="col-xs-12 col-md-3">
        </div>
        <div class="col-xs-12 col-md-3">
            <a href="#avilabillity" class="btn btn-secondry pull-right width-full"> 
                <asp:Label ID="BBCheckInDateLabel" Text="Check On Availability" runat="server"></asp:Label>
                <%--<sc:text id="ChangeDateLabel" field="Change Date Button" runat="server" />--%>
            </a>
        </div>
    </div>
</div>
<div class="col-xs-12">
    <ul class="info-top-link list-group">
        <li class="display-block-val"><a href="#facilities" class="list-group-item alert-info"><sc:text id="FacilitiesLabel" field="FacilitiesLabel" runat="server" />
        </a></li>
    </ul>

</div>
<div id="hotelDetailsDiv">

    <!-- Example row of columns -->
    <div class="col-md-12 no-padding">
        <div class="col-xs-12 col-md-8">
            <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; height: 456px; overflow: hidden; background-color: #24262e;">
                <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
                    <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
                    <div style="position: absolute; display: block; background: url('imag/loading.gif') no-repeat center center; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
                </div>
                <div id="imageGallery" data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 800px; height: 356px; overflow: hidden;">

                    <asp:Repeater ID="ImageGalleryRepeater" runat="server">
                        <ItemTemplate>

                            <div>
                                <image data-u="image" src="<%# Eval("Image") %>" />
                                <image data-u="thumb" src="<%# Eval("Image") %>" />
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                </div>

                <!-- Thumbnail Navigator -->
                <div data-u="thumbnavigator" class="jssort01" style="position: absolute; left: 0px; bottom: 0px; width: 800px; height: 100px;" data-autocenter="1">
                    <!-- Thumbnail Item Skin Begin -->
                    <div data-u="slides" style="cursor: default;" id="image_bottom">
                        <div data-u="prototype" class="p">
                            <div class="w">
                                <div data-u="thumbnailtemplate" class="t"></div>
                            </div>
                            <div class="c"></div>
                        </div>
                    </div>
                    <!-- Thumbnail Item Skin End -->
                </div>
                <!-- Arrow Navigator -->
                <span data-u="arrowleft" class="jssora05l" style="top: 158px; left: 8px; width: 40px; height: 40px;"></span>
                <span data-u="arrowright" class="jssora05r" style="top: 158px; right: 8px; width: 40px; height: 40px;"></span>
            </div>
        </div>
        <div class="col-xs-12 col-md-4">
            <div class="hotel-search-box-detail">

                <div class="top-bar float-right">
                </div>


                <div class="col-xs-12 row">
                    <div class="hotel-name-detail">
                        <h2 class="no-margin pull-left">

                            <asp:Label ID="providerName" runat="server"></asp:Label>
                        </h2>
                        <div class="star-rank pull-left padding-left-small padding-top-small">
                            <asp:Repeater ID="BBStarRepeater" runat="server">
                                <ItemTemplate>
                                    <span class="fa fa-star"></span>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        <a href="" id="savelater" runat="server">
                            <!--<sc:image id="Image1" field="SavedForLaterButton" runat="server" />-->
                        </a>
                        <div class="favoriteWrapper" style="display: block;">
                            <a data-placement="bottom" data-toggle="popover" data-title="My List" data-container="form" type="button" data-html="true" href="#" id="login">
                                <!--<sc:image id="btnAddToFav" field="AddToFavoriteButton" runat="server" />-->
                            </a>
                            <div id="popover-content" class="popover-content hide">
                                <div class="existingLitWrapper">
                                    <asp:CheckBoxList ID="chkFavList" runat="server"></asp:CheckBoxList>
                                </div>
                                <div class="NewListCreationWrapper">
                                    <input type="checkbox" id="chkNewList" class="chkNewList" disabled />
                                    <input type="text" id="txtNewList" class="txtNewList" title="newlist" />
                                </div>
                                <div class="createdListContentTemp" style="display: none;"></div>
                            </div>
                        </div>
                        <div class="hotel-name-detail-bottom width-full pull-left">
                            <p>
                                <asp:Label ID="providerAddress" runat="server" />
                            </p>
                            <p>
                                <a href data-toggle="modal" data-target="#dialog-map"><span class="fa fa-map-marker" title="Show map"></span>
                                    <sc:text id="ShowMapLabel" field="ShowMapLabel" runat="server" />
                                </a>
                            </p>
                        </div>
                    </div>
                </div>


            </div>


            <div class="content_bg_inner_box alert-info">
                <p>
                     <sc:text id="Overview" field="Overview" runat="server" />
                </p>
                <asp:Literal ID="litOverview" runat="server"></asp:Literal>

            </div>

        </div>
    </div>
    <div class="col-xs-12">

        <div class="col-xs-12 check-avilability content_bg_inner_box alert-info" id="avilabillity">
            <div>
                <div class="col-xs-12">
                    <h2><sc:text id="AvailabilityLabel" field="AvailabilityLabel" runat="server" />
                    </h2>
                </div>
                <div class="col-xs-12 margin-row">
                    <span><sc:text id="AvailabilityBoxTitleLabel" field="AvailabilityBoxTitleLabel" runat="server" /> <asp:Label ID="providerNameText" runat="server"></asp:Label>
                    </span>
                </div>
                <div class="col-md-4 col-xs-12">

                    <span><sc:text id="CheckinDateLabel" field="CheckinDateLabel" runat="server" />
                    </span>
                    <div class="width-full">
                        <asp:TextBox ID="CheckInDate" CssClass="form-control datepicker width-avilability-textbox pull-right" Text="" ClientIDMode="Static" runat="server" data-provide="datepicker" />
                        <asp:RequiredFieldValidator ID="rfvCheckInDate" runat="server" ForeColor="Red" ControlToValidate="CheckInDate" Display="Dynamic">
                            <span class="pull-left" style="margin-left: 40px;">
                                CheckInDateRequired
                            </span>
                        </asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">

                    <span><sc:text id="CheckoutDateLabel" field="NoOfNightsLabel" runat="server" />
                    </span>
                    <%--<asp:TextBox ID="CheckOutDate" CssClass="form-control" Text="" ClientIDMode="Static" runat="server" />--%>
                    <div role="group" aria-label="...">
                        <asp:DropDownList CssClass="form-control" ID="NoOfNights" ClientIDMode="Static" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvNumberOfNights" runat="server" ForeColor="Red" ControlToValidate="NoOfNights" Display="Dynamic">
                            NumberOfNightsRequired
                        </asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <span></span>
                    <%--<asp:Button CssClass="btn btn-primary booking-margin-top btn-bg pull-left" ID="CheckAvailabilityButton" OnClientClick="javascript:__doPostBack('CheckAvailabilityButton')" runat="server" Text=""/>--%>
                    <a href="" class="btn btn-primary booking-margin-top btn-bg pull-left" id="CheckAvailabilityBtn" runat="server" onclick="javascript:__doPostBack('CheckAvailabilityButton')">CheckAvailability
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 width-full">
                <div id="demo">
                    <asp:Repeater ID="roomTypeBBRepeater" runat="server" OnItemDataBound="roomTypeBBRepeater_ItemDataBound">
                        <HeaderTemplate>

                            <table class="room-type" style="width:100%;">
                                <tbody>

                                    <tr>
                                        <th>Room Types</th>
                                        <th>&nbsp;</th>
                                        <th>Best Rates</th>
                                        <th>No. rooms</th>
                                    </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><span><%# Eval("Name") %>(<%# (int.Parse(Eval("BBMealBasisId").ToString()))>0?Eval("BBMealBasis"):"" %>)</span></td>
                                <td><span class="text-red"></span></td>
                                <td>                                   
                                    <span class="display-block-val"><%#Eval("PriceCurrency") %> per break</span>
                                </td>
                                <td>
                                    <div aria-label="..." role="group">
                                        <asp:DropDownList ID="availabilityDDL" CssClass="form-control" runat="server"></asp:DropDownList>
                                    </div>
                                </td>
                                <td>
                                    <asp:Button ID="ButtonBBAddToBasket1" OnClick="ButtonAddToBasket_Click" CommandArgument='<%#GetVariantSkuAndProviderID((ProviderInfo)Container.DataItem) %>' class="btn btn-primary booking-margin-top btn-bg pull-right" runat="server" Text='<%#Affinion.LoyaltyBuild.Common.Utilities.SitecoreFieldsHelper.GetValue(Sitecore.Context.Item ,"AddToBasketLabel") %>' Visible="false" />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            <tr style="background-color: rgb(255, 255, 255);">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <asp:Button ID="ButtonBBAddToBasketOneClick" OnClick="ButtonBBAddToBasketOneClick_Click" class="btn btn-primary booking-margin-top btn-bg pull-right btn-basket" runat="server" Text="AddtoBasket" />
                                </td>
                            </tr>
                            </tbody>
         </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <asp:Repeater ID="OffersList" runat="server" OnItemDataBound="OffersList_ItemDataBound">
                        <HeaderTemplate>
                            <div class="show-price-list no-padding">
                                <table class="room-type">
                                    <tbody>
                                        <tr style="border-top-style: solid; border-top-width: 1px; border-top-color: rgb(207, 242, 246); border-bottom-style: solid; border-bottom-width: 1px; border-bottom-color: rgb(207, 242, 246); background-color: rgb(250, 254, 254);">
                                            <th>
                                                <sc:text id="RoomTypesLabel" field="RoomTypesLabel" runat="server" />
                                            </th>
                                            <th>&nbsp;</th>
                                            <th>
                                                <sc:text id="PricePerBrakeLabel" field="PricePerBrakeLabel" runat="server" />
                                            </th>
                                            <th>
                                                <sc:text id="RoomsLabel" field="RoomsLabel" runat="server" />
                                            </th>
                                            <th></th>
                                        </tr>
                        </HeaderTemplate>

                        <ItemTemplate>
                            <tr style="background-color: rgb(255, 255, 255);">
                                <td><span><%#GetRoomName((ProviderInfo)Container.DataItem)%></span></td>
                                <%--<td><span class="text-red"><%#((OfferDataItem)Container.DataItem).Tip %></span></td>--%>
                                <td></td>
                                <td><span><%#this.GetRoomPrice((ProviderInfo)Container.DataItem) %>
                                    <sc:text field="OfferPriceSuffixLabel" runat="server" />
                                </span></td>
                                <td>
                                    <div aria-label="..." role="group">
                                        <asp:DropDownList ID="drpAvailability" runat="server" CssClass="form-control">
                                        </asp:DropDownList>

                                    </div>
                                </td>
                                <td>
                                    <asp:Button ID="ButtonAddToBasket1" OnClick="ButtonAddToBasket1_Click" CommandArgument='<%#Eval("Sku")+","+ Eval("PriceCurrency") + "," + Eval("ProviderID")+ "," + Eval("Price")+ "," + Eval("Sku") %>' class="btn btn-primary booking-margin-top btn-bg pull-right" runat="server" Text='<%#Affinion.LoyaltyBuild.Common.Utilities.SitecoreFieldsHelper.GetValue(Sitecore.Context.Item ,"AddToBasketLabel") %>' Visible="false" />
                                </td>
                            </tr>
                        </ItemTemplate>

                        <FooterTemplate>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <asp:Button ID="ButtonAddToBasketOneClick" OnClick="ButtonAddToBasketOneClick_Click" class="btn btn-primary booking-margin-top btn-bg pull-right" runat="server" Text='<%#Affinion.LoyaltyBuild.Common.Utilities.SitecoreFieldsHelper.GetValue(Sitecore.Context.Item ,"AddToBasketLabel") %>' />
                                </td>
                                <td></td>
                            </tr>
                            </tbody>
                                </table>
                            </div>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <div class="alert alert-danger msg-margin-top" id="SelectRoomAlertWrapper">
                    <sc:text id="txtSelectRoomText" runat="server" visible="false" />
                </div>
                <div class="row">
                    <div class="col-xs-12" id="AvailableAlert" runat="server">
                        <div class="alert alert-danger msg-margin-top">
                            <sc:text id="NoOfferFoundMessage" field="NoOfferFoundMessage" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
         <div class="detail-hotel margin-bottom-15">
            <div class="content_bg_inner_box light-box" id="facilities">
                <h2><sc:text id="facilityHeading" field="FacilitiesLabel" runat="server" />
                </h2>
              <asp:Label ID="lblFacilities" runat="server"></asp:Label>
                <!--<sc:text id="Facilities" field="Facility" runat="server" />-->
            </div>
            
        </div>
    </div>
</div>

                </div>
                <%--<div class="alert alert-danger msg-margin-top" id="SelectRoomAlertWrapper">
                    <sc:text id="txtSelectRoomText" runat="server" visible="false" />
                </div>--%>
              <%--  <div class="row">
                    <div class="col-xs-12" id="AvailableAlert" runat="server">
                        <div class="alert alert-danger msg-margin-top">
                            <sc:text id="NoOfferFoundMessage" field="NoOfferFoundMessage" runat="server" />
                        </div>
                    </div>
                </div>--%>
            </div>
        </div>


       


<div class="modal fade" id="dialog-map" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><span class="fa fa-map-marker"></span>
                    <sc:text id="MapLabelText" field="MapLabel" runat="server" />
                </h4>
            </div>
            <div class="modal-body">
                <div id="map_canvas" class="large-gmap"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <sc:text id="MapCloseText" field="MapCloseButton" runat="server" />
                </button>
            </div>
        </div>
    </div>
</div>

<input type="hidden" class="hiddenListName" />

<script type="text/javascript">
    //Store reference to the google map object
    var map;



    //Set the required Geo Information
    var latitude = '<%=latitude%>';
    var lognitude = '<%=lognitude%>';

    var mapOptions = {
        center: new google.maps.LatLng(latitude, lognitude),
        //center: coords,
        zoom: 14,
    };

    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    var marker;
    marker = new google.maps.Marker({
        position: new google.maps.LatLng(latitude, lognitude),
        map: map
    });

    //google.maps.event.addListener(marker, 'click', function () {
    //    window.location.href = marker.url;
    //});

    jQuery(document).ready(function () {
        jQuery('#dialog-map').on('shown.bs.modal', function () {

            if (navigator.geolocation) {
                //navigator.geolocation.getCurrentPosition(initializeMap);
                google.maps.event.trigger(map, "resize");
                map.setCenter(new google.maps.LatLng(latitude, lognitude));
                //loadMap();
            }
            else {
                alert("Sorry, your browser does not support geolocation services.");
            }

        });
    });

    jQuery("#CheckInDate").datepicker({
        defaultDate: "+1w",
        dateFormat: "<%=DateTimeHelper.DateFormatJavaScript%>",
        numberOfMonths: 1,
        minDate: 0,
        showOn: "both",
        buttonImage: "calendar.png",
        buttonImageOnly: false,
        buttonClass: "glyphicon-calendar"
          //onClose: function (selectedDate) {

          //    var checkoutDate = new Date(selectedDate);
          //    if (checkoutDate.toString() != "Invalid Date") {
          //        checkoutDate.setDate(checkoutDate.getDate() + 1);
          //    }

          //    jQuery("#CheckOutDate").datepicker("option", "minDate", checkoutDate);
          //}
      }).next(".ui-datepicker-trigger").addClass("btn btn-default glyphicon glyphicon-calendar calander-icon pull-right");
    //jQuery("#CheckOutDate").datepicker({
    //    defaultDate: "+1w",
    //    numberOfMonths: 1,
    //    minDate: 0,
    //    onClose: function (selectedDate) {
    //        // jQuery("#CheckInDate").datepicker("option", "maxDate", selectedDate);
    //    }
    //});
    jQuery('#CheckInDate').attr('readonly', true);

    jQuery("[data-toggle=popover]").popover({
        html: true,
        content: function () {
            return jQuery('#popover-content').html();
        }
    });

</script>
</asp:PlaceHolder>