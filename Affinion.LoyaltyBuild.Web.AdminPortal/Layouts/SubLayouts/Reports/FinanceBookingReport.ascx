﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FinanceBookingReport.ascx.cs" Inherits="Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.Reports.FinanceBookingReport" %>

<script src="/Resources/scripts/jquery.nanoscroller.js"></script>
<script src="/Resources/scripts/callscroller.js"></script>

<script src="/Resources/scripts/jquery.dd.min.js"></script>
<link href="/Resources/styles/dd.css" rel="stylesheet" />

<style>
    .date-width {
        border: 1px solid;
    }
</style>

<div class="row">
    <div class="col-xs-12">
        <div class="site_map">
            <h4 class="panel-title" style="text-align: center">Finance Booking Report</h4>
            <div class="panel-body">

                <div class="col-sm-12 no-padding-m">
                    <div class="content_bg_inner_box alert-info">
                        <div class="col-sm-6 no-padding-m">
                            <div class="row margin-row">
                                <table style="width: 100%">
                                    <tr>
                                        <td style="width: 100%" colspan="2">Fill in the parameters for your report:</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%" colspan="2">Filter by date:</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 30%; text-align: right">
                                            Start Date:
                                        </td>
                                        <td style="width: 70%">
                                            <input type="text" id="StartDate" runat="server" clientidmode="Static" class="form-control datepicker width-85 pull-right font-size-12 date-width" placeholder="Start date" tabindex="0">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 30%; text-align: right">
                                            End Date:
                                        </td>
                                        <td style="width: 70%">
                                            <input type="text" id="EndDate" runat="server" clientidmode="Static" class="form-control datepicker width-85 pull-right font-size-12 date-width" placeholder="End date" tabindex="1">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 30%"></td>
                                        <td style="width: 70%"></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 30%; text-align: right"></td>
                                        <td style="width: 70%">
                                            <asp:Button ID="ExcelReport" runat="server" Text="Excel Report" TabIndex="2" OnClick="ExcelReport_Click" />
                                            <asp:Button ID="HtmlReport" runat="server" Text="Html Report" TabIndex="3" OnClick="HtmlReport_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row margin-row"></div>
                <div runat="server" id="ResultDiv" clientidmode="Static" visible="false">
                    <div id="data-grid" class="table-responsive">
                        <asp:GridView ID="ResultGridView" runat="server" Width="100%" SelectedRowStyle-Font-Bold="true" CssClass="table tbl-calendar" AutoGenerateColumns="False">
                            <Columns>
                                <asp:TemplateField HeaderText="Bookings">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtBookings" runat="server" Text='<%# Bind("Bookings") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblBookings" runat="server" Text='<%# Bind("Bookings") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Booking Method ID">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtBookingMethodID" runat="server" Text='<%# Bind("BookingMethodID") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblBookingMethodID" runat="server" Text='<%# Bind("BookingMethodID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Booking Method">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtBookingMethod" runat="server" Text='<%# Bind("BookingMethod") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblBookingMethod" runat="server" Text='<%# Bind("BookingMethod") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Partner Name">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtPartnerName" runat="server" Text='<%# Bind("PartnerName") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblPartnerName" runat="server" Text='<%# Bind("PartnerName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>

                            <EmptyDataTemplate>
                                ---No Records---
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    jQuery(function () {
        jQuery("#StartDate").datepicker({
            defaultDate: new Date(),
            dateFormat: "<%=Affinion.LoyaltyBuild.Common.Utilities.DateTimeHelper.DateFormatJavaScript%>",
                minDate: -90,
                maxDate: 0,
                numberOfMonths: 1,
                showOn: "both",
                buttonImage: "images/calendar.png",
                buttonImageOnly: false,
                buttonClass: "glyphicon-calendar"
            }).next(".ui-datepicker-trigger").addClass("btn btn-default glyphicon glyphicon-calendar calander-icon pull-right");

            jQuery("#EndDate").datepicker({
                defaultDate: new Date(),
                dateFormat: "<%=Affinion.LoyaltyBuild.Common.Utilities.DateTimeHelper.DateFormatJavaScript%>",
            minDate: 0,
            maxDate: 0,
            numberOfMonths: 1,
            showOn: "both",
            buttonImage: "images/calendar.png",
            buttonImageOnly: false,
            buttonClass: "glyphicon-calendar"
        }).next(".ui-datepicker-trigger").addClass("btn btn-default glyphicon glyphicon-calendar calander-icon pull-right");
            var msclient = $("#ClientList").msDropdown({ roundedCorner: false });
            $(".hotel-search-box button[type='reset']").click(function () {
                setTimeout(function () { document.getElementById("ClientList").refresh(); }, 500);
            });

        });
</script>
