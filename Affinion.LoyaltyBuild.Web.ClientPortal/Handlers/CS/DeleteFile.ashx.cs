﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Customer;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Handlers.CS
{
    /// <summary>
    /// Summary description for DeleteFile
    /// </summary>
    public class DeleteFile : IHttpHandler
    {
        private NameValueCollection _query = null;
        private QueryService _queryService;

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                _query = HttpUtility.ParseQueryString(context.Request.Url.Query.ToLower());
                int queryId = Convert.ToInt32(_query["queryid"]);
                if (queryId == 0)
                {
                    throw new Exception("invalid request");
                }

                _queryService = new QueryService();
                ValidationResponse response = _queryService.DeleteCustomerQueryAttachments(queryId);
                if (response.IsSuccess)
                {
                    context.Response.Write("1");
                }
                else
                {
                    context.Response.Write("0");
                }

            }
            catch (Exception)
            {
                context.Response.Write("0");
            }
            context.Response.End();

        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}

