﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SubscribeSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.SubscribeSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="col-sm-4">
    <div class="footer-middle-area">
        <div class="subscribe-top">
            <asp:Literal ID="LiteralSubscription" Text="Set the datasource: Required Fields: Message, ContentMessage " runat="server" />
            <h2 class="no-margin">
                <sc:text field="Message" id="message" runat="server" />
            </h2>
            <div class="subscribe">
                <div class="form-inline">
                    <div class="form-group">
                        <asp:TextBox ID="emailTextBox" CssClass="form-control" runat="server" autocomplete="off"></asp:TextBox> 
                    </div>
                    <button id="subscribeButton" runat="server" OnServerclick="SubscribeButton_Click" class="btn btn-default" validationgroup="rtvSubscribeEmail">
                        <sc:text id="txtBtnText" runat="server" />
                    </button>
                </div>
                <div>
                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="emailTextBox" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="Red" Display="Dynamic" ValidationGroup="rtvSubscribeEmail">
                        <sc:text id="txtEmailRegularText" runat="server" />
                    </asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ForeColor="Red" ControlToValidate="emailTextBox" Display="Dynamic" ValidationGroup="rtvSubscribeEmail">
                        <sc:text id="txtEmailRequiredText" runat="server"/>
                    </asp:RequiredFieldValidator>
                </div>
                <asp:Label ID="Subscribelabel" runat="server" Text=""></asp:Label>
            </div>
        </div>
    </div>
</div>

<%--<script type="text/javascript">
    function IsValidEmail(email) {
        var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        //alert("working");
        return expr.test(email)
    };
    function ValidateEmail() {

        
        var email = jQuery("#<%= emailTextBox.ClientID %>").val();
        if (!IsValidEmail(email) || email == "") {
            jQuery("#<%= Subscribelabel.ClientID %>").html("Invalid email address");
            return false;
        }
        else {
            return true;
        }
    }
</script>--%>