﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           Logger.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Used to log diagnostic data
/// </summary>
#region Using Directives

using System;
using System.Diagnostics;
using System.Globalization;

#endregion

namespace Affinion.LoyaltyBuild.Common.Instrumentation
{
    /// <summary>
    /// Implements ILogger interface
    /// </summary>
    public class Logger : ILogger
    {
        #region Public Methods
        /// <summary>
        /// Write the message to logs.
        /// </summary>
        /// <param name="message">The message.</param>       
        /// <param name="owner">Owner</param>
        public void Trace(string message, object owner)
        {
            try
            {
                Sitecore.Diagnostics.Log.Info(message, owner);
            }
            catch (NullReferenceException e)
            {
                Debug.WriteLine(String.Format(CultureInfo.CurrentCulture, "Failed To Log Trace(Null Reference). {0}", e.Message));
            }
            catch (Exception ex)
            {
                Debug.WriteLine(String.Format(CultureInfo.CurrentCulture, "Failed To Log Trace. {0}", ex.Message));
            }
        }

        /// <summary>
        /// Writes the exception to log.
        /// </summary>
        /// <param name="categoryName">Name of the category.</param>
        /// <param name="exception">The exception.</param>       
        /// <param name="owner">Owner</param>        
        public void WriteException(DiagnosticsCategory categoryName, Exception exception, object owner)
        {

            if (exception != null)
            {
                try
                {
                    //Write exception to the event log as critical error
                    Sitecore.Diagnostics.Log.Error(categoryName.ToString(), exception, owner);
                }
                catch (NullReferenceException e)
                {
                    Debug.WriteLine(String.Format(CultureInfo.CurrentCulture, "Failed To Log Event(Null Reference). {0}", e.Message));
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(String.Format(CultureInfo.CurrentCulture, "Failed To Log Event. {0}", ex.Message));
                }
            }
        }

        #endregion

    }
}
