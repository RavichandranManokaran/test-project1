﻿using System;
using System.Collections.Generic;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Pipelines;
using UCommerce.Pipelines.AddToBasket;
using UCommerce.Pipelines.GetProduct;
using UCommerce.Runtime;
using UCommerce.Transactions;
using Affinion.LoyaltyBuild.Model.Product;
using Affinion.LoyaltyBuild.Api.Booking.Service;
using System.Linq;
using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
using Affinion.LoyaltyBuild.BedBanks.Troika;
using Affinion.LoyaltyBuild.BedBanks.General;
using Affinion.LoyaltyBuild.BedBanks.Helper;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Message;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Model.Room;
using Sitecore.Data;
using Affinion.LoyaltyBuild.Model.Provider;
using Newtonsoft.Json;

namespace Affinion.LoyaltyBuild.Api.Booking.Extension
{
    public static class LBTransactionLibrary
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="catalogName"></param>
        /// <param name="quantity"></param>
        /// <param name="sku"></param>
        /// <param name="variantSku"></param>
        /// <param name="addToExistingLine"></param>
        /// <param name="executeBasketPipeline"></param>
        /// <returns></returns>
        public static OrderLine AddToBasket(int quantity, IProduct product, Dictionary<string, string> properties = null,
            bool addToExistingLine = false, bool executeBasketPipeline = true)
        {
            try
            {
                var obj = ObjectFactory.Instance.Resolve<LBTransactionLibraryInternal>();

                return obj.AddToBasket(quantity, product, addToExistingLine, executeBasketPipeline, properties);
            }
            catch 
            {
                throw;
            }
        }

        /// <summary>
        /// Remove an Item From Basket with Order lineId
        /// </summary>
        /// <param name="orderLineId"></param>
        /// <returns></returns>
        public static bool RemoveBasketItem(int orderLineId)
        {
            UCommerce.Api.TransactionLibrary.UpdateLineItem(orderLineId, 0);

            return true;
        }

    }

    public class LBTransactionLibraryInternal
    {
        private readonly TransactionLibraryInternal _transactionLibraryInternal;
        private readonly IBookingService _bookingService;
        private readonly IPipeline<IPipelineArgs<AddToBasketRequest, AddToBasketResponse>> _addToBasketPipeline;
        private readonly IPipeline<IPipelineArgs<GetProductRequest, GetProductResponse>> _getProductPipeline;
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="transactionLibraryInternal"></param>
        /// <param name="addToBasketPipeline"></param>
        public LBTransactionLibraryInternal(TransactionLibraryInternal transactionLibraryInternal, 
            IPipeline<IPipelineArgs<AddToBasketRequest, AddToBasketResponse>> addToBasketPipeline,
            IPipeline<IPipelineArgs<GetProductRequest, GetProductResponse>> getProductPipeline,
            IBookingService bookingService)
        {
            this._addToBasketPipeline = addToBasketPipeline;
            this._transactionLibraryInternal = transactionLibraryInternal;
            this._getProductPipeline = getProductPipeline;
            this._bookingService = bookingService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="catalogName"></param>
        /// <param name="quantity"></param>
        /// <param name="sku"></param>
        /// <param name="variantSku"></param>
        /// <param name="addToExistingLine"></param>
        /// <param name="executeBasketPipeline"></param>
        /// <returns></returns>
        public virtual OrderLine AddToBasket(int quantity, IProduct product, bool addToExistingLine = true, bool executeBasketPipeline = true, Dictionary<string, string> properties = null)
        {
            GetProductResponse productResponse = new GetProductResponse();
            if (this._getProductPipeline.Execute(new GetProductPipelineArgs(new GetProductRequest(new ProductIdentifier(product.Sku, product.VariantSku)), productResponse)) == PipelineExecutionResult.Error)
            {
                throw new ArgumentException(string.Format("Could not find product. {0}", product.Sku));
            }
            
            //get the product to add
            Product uProduct = productResponse.Product;
                        
            //create basket request
            AddToBasketRequest request = new AddToBasketRequest(true)
            {
                PriceGroup = SiteContext.Current.CatalogContext.CurrentPriceGroup,
                Product = uProduct,
                PurchaseOrder = _transactionLibraryInternal.GetBasket(true).PurchaseOrder,
                Quantity = quantity,
                AddToExistingOrderLine = addToExistingLine,
                ExecuteBasketPipeline = true
            };

            //preprocess and set unit price and vat
            var preprocess = product.PreProcessBooking(properties);
            request.UnitPrice = preprocess.UnitPrice;
            request.UnitTax = preprocess.Vat;


            //create and get product response
            AddToBasketResponse response = new AddToBasketResponse();
            if (this._addToBasketPipeline.Execute(new AddToBasketPipelineArgs(request, response)) == PipelineExecutionResult.Error)
            {
                throw new ArgumentException(string.Format("Product is not added. {0}", product.Sku));
            }

            var booking = _bookingService.GetBooking(response.OrderLine.OrderLineId, product.Type);

            if (properties != null)
            {
                foreach (var key in properties.Keys)
                    booking[key] = properties[key];
            }
            if (preprocess != null)
            {
                foreach (var key in preprocess.Keys)
                    booking[key] = preprocess[key];
            }

            booking[Constants.ProductType] = product.Type.ToString();
            booking.Save();

            uCommerceConnectionFactory.AddBookingInfo(response.OrderLine.OrderLineId);

            if (product.Type.Equals(ProductType.BedBanks) && booking.OrderLine.GetOrderProperty(Constants.PropertyReferenceId).Value != null && booking.OrderLineId!=0)
            {
                booking.OrderLine.ProductName = string.Format("{0} {1}", booking.OrderLine.GetOrderProperty(Constants.PropertyName).Value, booking.OrderLine.GetOrderProperty(Constants.RoomInfo).Value); 
                booking.Save();
                InsertErrata(booking);
            }

            if (executeBasketPipeline)
                _transactionLibraryInternal.ExecuteBasketPipeline();

            return response.OrderLine;

        }

        private static void InsertErrata(Data.Booking booking)
        {
            int orderLineId = booking.OrderLineId;

            string bookingToken = booking.OrderLine.GetOrderProperty(Constants.PreBookingToken).Value;
            string propertyReferenceId = booking.OrderLine.GetOrderProperty(Constants.PropertyReferenceId).Value;
            //string arrDate = booking.OrderLine.GetOrderProperty(Constants.CheckinDate).Value;
            string arrDate = booking.OrderLine.GetOrderProperty(Constants.CheckinDate).Value;

            DateTime date;
            if (!string.IsNullOrEmpty(arrDate))
            {
                date = Convert.ToDateTime(arrDate);
                arrDate = date.ToString("yyyy-MM-dd");
            }

            string duration = booking.OrderLine.GetOrderProperty(Constants.Nights).Value;
            string noOfRooms = booking.OrderLine.GetOrderProperty(Constants.NoOfRooms).Value;
            string noOfAdults = booking.OrderLine.GetOrderProperty(Constants.NoOfAdults).Value;
            string noOfChildren = booking.OrderLine.GetOrderProperty(Constants.NoOfChildren).Value;
            string ageOfChildren = string.Empty;
            int children = 0;
            int.TryParse(noOfChildren, out children);
            if (children > 0)
                ageOfChildren = booking.OrderLine.GetOrderProperty(Constants.ChildrenAges).Value;
            string clientId = booking.OrderLine.GetOrderProperty(Constants.ClientId).Value;

            var client = Sitecore.Context.Database.GetItem(new ID(clientId));
            BedBanksSettings bbs = new BedBanksSettings(client);
            List<string> propertyRefIdList = new List<string>();
            propertyRefIdList.Add(propertyReferenceId);

            //Make Api call
            List<Erratum> errataInfo;
            JacTravelApiWrapper jac = new JacTravelApiWrapper();
            JtSearchResponse propertyDetailsSearchResponse = jac.SearchOnPropRef("0", arrDate, duration, noOfRooms, noOfAdults, noOfChildren, ageOfChildren, bbs, propertyRefIdList);
            if (propertyDetailsSearchResponse.PropertyResults.Count > 0)
            {
                var hotelDetails = propertyDetailsSearchResponse.PropertyResults.FirstOrDefault();
                var roomInfo = hotelDetails.RoomTypes.Where(x => x.BookingToken == bookingToken || x.PropertyRoomTypeID == bookingToken).Select(y=>y).FirstOrDefault();
                errataInfo = roomInfo.Errata;

                var list = new List<BedBankErrata>();
                foreach (var errata in errataInfo)
                {
                    list.Add(new BedBankErrata
                    {
                        Subject = errata.Subject,
                        Description = errata.Description
                    });
                }

                string strErrataInfo = JsonConvert.SerializeObject(list);
                int tempPropertyId = 0;
                int.TryParse(hotelDetails.PropertyID, out tempPropertyId);

                //store this in table
                TroikaDataController.InsertErrataInfo(tempPropertyId, roomInfo.PropertyRoomTypeID, booking.OrderLineId, strErrataInfo);
            }
        }
    }
}
