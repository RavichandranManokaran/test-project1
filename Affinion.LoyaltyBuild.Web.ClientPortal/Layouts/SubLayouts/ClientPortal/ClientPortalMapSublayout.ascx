﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientPortalMapSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortalMapSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>



<script type="text/javascript">
    //Store reference to the google map object
    var map;

    function initialize(location) {

        //Set the required Geo Information

        // Define the coordinates as a Google Maps LatLng Object

        if (locationList && locationList.length > 0) {
            var centerArgs = locationList[0].split(",");
            //Map initialization variables
            var mapOptions = {
                center: new google.maps.LatLng(centerArgs[0], centerArgs[1]),
                //center: coords,
                zoom: 6,
            };

            map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

            var marker, i;

            for (i = 0; i < locationList.length; i++) {
                var args = locationList[i].split(",");

                if (args.length == 2) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(args[0], args[1]),
                        map: map
                    });
                }
            }
        }

    }

    $(document).ready(function () {
        if (navigator.geolocation) {
            // Call getCurrentPosition with success and failure callbacks
            //navigator.geolocation.getCurrentPosition(initialize);
            initialize();
        }
        else {
            alert("Sorry, your browser does not support geolocation services.");
        }
    });
</script>

<div class="info-map margin-common-inner-box" id="GoogleMap" runat="server">

    <div id="map_canvas" class="gmap"></div>

</div>
