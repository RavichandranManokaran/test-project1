
CREATE PROCEDURE [dbo].[LB_GetOverAllRevenueForClient] 
@DateFrom DATE,
@DateTo DATE,
@BookingThrough VARCHAR(50),
@ClientId VARCHAR(max)

AS
BEGIN
         SELECT  Count(OL.OrderId)  as [Bookings],
                 OP.BookingThrough as [Booking Method],
                 OP.clientId       as [ParterName]
                   
              FROM uCommerce_OrderLine OL
              inner join LB_BookingInfo BI on BI.OrderLineId=OL.OrderLineId
              inner join 
              (
                 select OrderLineId ,_ClientId [ClientId], _BookingThrough [BookingThrough]
                 from
              (
                 select OrderLineId, [key],[value]
                    from uCommerce_OrderProperty 
                 )op
                 pivot
                 (
                    max([value])
                           for [key] in ( _ClientId, _BookingThrough)
                           
                 )piv
                 )OP on OP.OrderLineId=OL.OrderLineId
                       
         WHERE 
              (BI.CheckInDate between @DateFrom and @DateTo) AND
              (@BookingThrough IS NULL OR OP.BookingThrough LIKE @BookingThrough + '%') AND
              (@ClientId IS NULL OR OP.ClientId LIKE @ClientId + '%')
              group by OP.BookingThrough,OP.ClientId

END

GO

