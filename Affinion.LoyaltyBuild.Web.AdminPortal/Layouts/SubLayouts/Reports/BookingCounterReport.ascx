﻿<%@ control language="C#" autoeventwireup="true" codebehind="BookingCounterReport.ascx.cs" inherits="Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.Reports.BookingCounterReport" %>

<div>
    <div class="booking-heading"><sc:Text id="scTodayBooking" runat="server"/>Today's Booking </div>  
    <div class="booking">
        <h2><sc:Text id="scTodayBookingPartner" runat="server"/><asp:label ID="lblDateTime" runat="server" CssClass="booking-date" ></asp:label></h2>
        <asp:GridView ID="grdvwToday" runat="server" CssClass="table table-striped marTB10" AutoGenerateColumns="False" ShowFooter="true">
            <Columns>
                <asp:BoundField DataField="ClientName" HeaderText="ClientName" FooterText="GrandTotal"></asp:BoundField>
                <asp:BoundField DataField="Bookings" HeaderText="Booking Number" FooterText=""></asp:BoundField>
                <asp:BoundField HeaderText="Budget" FooterText="0,000"></asp:BoundField>
           </Columns>
        </asp:GridView>
        <br />
    </div>

    <div class="booking-heading"><sc:Text id="scYesterdayBooking" runat="server"/></div>
    <div class="booking">
        <h2><sc:Text id="scYesterdayBookingPartner" runat="server"/><asp:label ID="lblDateTimeYesterday" runat="server" CssClass="booking-date" ></asp:label></h2>
        <asp:GridView ID="grdvwYesterday" runat="server" CssClass="table table-striped marTB10" AutoGenerateColumns="False" ShowFooter="true">
            <Columns>
                <asp:BoundField DataField="ClientName" HeaderText="ClientName" FooterText="GrandTotal"></asp:BoundField>
                <asp:BoundField DataField="Bookings" HeaderText="Booking Number" FooterText=""></asp:BoundField>
                <asp:BoundField HeaderText="Budget" FooterText="0,000"></asp:BoundField>
            </Columns>
        </asp:GridView>
    </div>

    <div class="booking-heading"><sc:Text id="scWeek" runat="server"/></div>
    <div class="booking">
        <h2><sc:Text id="scWeekPartner" runat="server"/><asp:label ID="lblDateTimeWeek" runat="server" CssClass="booking-date" ></asp:label></h2>
        <asp:GridView ID="grdvwWeek" runat="server" CssClass="table table-striped marTB10" AutoGenerateColumns="False" ShowFooter="true">
            <Columns>
                <asp:BoundField DataField="ClientName" HeaderText="ClientName" FooterText="GrandTotal"></asp:BoundField>
                <asp:BoundField DataField="Bookings" HeaderText="Booking Number" FooterText=""></asp:BoundField>
                <asp:BoundField HeaderText="Budget" FooterText="0,000"></asp:BoundField>
            </Columns>
        </asp:GridView>
    </div>

    <div class="booking-heading"><sc:Text id="scMonth" runat="server"/></div>
    <div class="booking">
        <h2><sc:Text id="scMonthPartner" runat="server"/><asp:label ID="lblDateTimeMonth" runat="server" CssClass="booking-date" ></asp:label></h2>
        <asp:GridView ID="grdvwMonth" runat="server" CssClass="table table-striped marTB10" AutoGenerateColumns="False" ShowFooter="true">
            <Columns>
                <asp:BoundField DataField="ClientName" HeaderText="ClientName" FooterText="GrandTotal"></asp:BoundField>
                <asp:BoundField DataField="Bookings" HeaderText="Booking Number" FooterText=""></asp:BoundField>
                <asp:BoundField HeaderText="Budget" FooterText="0,000"></asp:BoundField>
                
            </Columns>
        </asp:GridView>
    </div>

    <div class="booking-heading"><sc:Text id="scYear" runat="server"/></div>
    <div class="booking">
        <h2><sc:Text id="scYearPartner" runat="server"/><asp:label ID="lblDateTimeYear" runat="server" CssClass="booking-date" ></asp:label></h2>
        <asp:GridView ID="grdvwYear" runat="server" CssClass="table table-striped marTB10" AutoGenerateColumns="False" ShowFooter="true">
            <Columns>
                <asp:BoundField DataField="ClientName" HeaderText="ClientName" FooterText="GrandTotal"></asp:BoundField>
                <asp:BoundField DataField="Bookings" HeaderText="Booking Number"></asp:BoundField>
                <asp:BoundField HeaderText="Budget" FooterText="0,000"></asp:BoundField>
           </Columns>
        </asp:GridView>
    </div>  
</div>

