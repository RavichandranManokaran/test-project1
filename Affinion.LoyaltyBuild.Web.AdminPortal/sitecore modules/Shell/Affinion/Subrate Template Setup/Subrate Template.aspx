﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Subrate Template.aspx.cs" Inherits="Affinion.LoyaltyBuild.AdminPortal.sitecore_modules.Shell.Affinion.Subrate_Template_Setup.Subrate_Template" %>

<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ OutputCache Location="None" VaryByParam="none" %>

<!DOCTYPE HTML>
<html>
<head id="Head1" runat="server">
    <title>Subrate Template
    </title>

    <meta name="robots" content="follow, index" />
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,crome=1" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <%--Bootstrap--%>
    <link href="/Resources/styles/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/Resources/styles/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />

    <link href="/Resources/styles/affinion-admin.min.css" rel="stylesheet" type="text/css" />
    <link href="/Resources/styles/theme1.css" rel="stylesheet" type="text/css" />
    <link href="/Resources/styles/affinion-supplier.css" rel="stylesheet" type="text/css" />

    <link href="/Resources/datePicker/jquery-ui.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="/Resources/styles/simple-sidebar.css" rel="stylesheet" />
    <link href="/Resources/styles/print.css" rel="stylesheet" />
    <link href="/Resources/styles/CustomeStyle.css" rel="stylesheet" />
    <style>
        #subrate-set table tr td {
            word-wrap: break-word;
        }
    </style>

    <!--[if lte IE 8]>
 <link rel="stylesheet" type="text/css" href="/assets/css/IE-fix.css" />
 <![endif]-->
</head>
<body id="mainbody" class="cbp-spmenu-push">
    <form runat="server" id="frmSubrate">
        <div>
            <div class="col-md-12">
                <asp:Label runat="server" ID="lblMessage" CssClass="alert-success"></asp:Label>
                <asp:Label runat="server" ID="lblError" CssClass="alert-danger"></asp:Label>
            </div>
            <div class="rate_tab" id="subrate-set">
                <div class="cl_rate">
                    <h3 class="rate_title">Templates</h3>

                    <asp:GridView ID="gvSubrateTemplates" runat="server" AutoGenerateColumns="false" CssClass="cl_tabl" OnRowCommand="gvSubrateTemplates_RowCommand">
                        <Columns>
                            <asp:BoundField ReadOnly="true" HeaderText="TemplateName" DataField="TemplateName" SortExpression="ItemName" />
                            <asp:TemplateField HeaderText="Edit">
                                <ItemTemplate>
                                    <asp:Button runat="server" ID="btnEdit" CausesValidation="false" CommandName="edittemplate" CommandArgument='<%# Eval("SubrateTemplateCode") + "," + Eval("TemplateName")  %>' CssClass="btn btn-info" Text="Edit"></asp:Button>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Delete">
                                <ItemTemplate>
                                    <asp:Button runat="server" ID="btnDelete" CausesValidation="false" CommandName="deletetemplate" CommandArgument='<%# Eval("SubrateTemplateID") %>' CssClass="btn btn-danger" Text="Delete"></asp:Button>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <div class="ofr_btns">
                        <asp:Button runat="server" CssClass="btn_save" ID="btnAddNew" OnClick="btnAddNew_Click" Text="Add New Template" />
                    </div>
                </div>
                <div class="sub_rate">
                    <h3 class="rate_title">Subrate Settings</h3>
                    <div class="subrate-pad-25">
                        <h1>Template Name:</h1>
                        <asp:TextBox CssClass="vat_value" runat="server" ID="txtItemName"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ID="reqItemName" ControlToValidate="txtItemName" CssClass="alert-danger" ErrorMessage="Please enter template name!" />
                         <asp:RegularExpressionValidator Display = "Dynamic" ControlToValidate = "txtItemName" ID="RegularExpressionValidator1" ValidationExpression = "^[\s\S]{0,50}$" runat="server" ErrorMessage="Maximum 50 characters allowed." CssClass="alert-danger" ></asp:RegularExpressionValidator>
                        <asp:HiddenField runat="server" ID="ItemID" />
                    </div>
                    <asp:Repeater ID="rptSubrates" runat="server" OnItemDataBound="rptSubrates_ItemDataBound">

                        <HeaderTemplate>
                            <table class="cl_tabl">
                                <thead>
                                    <tr>
                                        <th>SubrateItem</th>
                                        <th>Code</th>
                                        <th>Ex.Vat</th>
                                        <th>Ex.Vat(%)</th>
                                        <th>VAT%</th>
                                        <th>VAT Amount</th>
                                        <th>Total Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <%#Eval("ItemName")%>                                       
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblSRCode" Text='<%#Eval("SubrateItemCode")%>'></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" CssClass="vat_value" ID="txtAmount" onkeyup="ReCalculateVat(this)" onkeypress="return isNumberKey(event)" MaxLength="3"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:TextBox CssClass="vat_value" runat="server" ID="txtPercentageAmount" onkeyup="ReCalculateVat(this)" onkeypress="return isNumberKey(event)" MaxLength="3"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:DropDownList CssClass="vat_value" runat="server" ID="ddlVatPercentage" onchange="ReCalculateVat(this)"></asp:DropDownList>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblVatAmount"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblTotalAmount"></asp:Label>
                                </td>

                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                                 </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <div class="ofr_btns">
                        <asp:Button runat="server" CssClass="btn_save" ID="btnSave" OnClick="btnSave_Click" Text="Save Template" />
                        <asp:Button runat="server" CausesValidation="false" CssClass="subrte_cancl" ID="btnCancle" OnClick="btnCancle_Click" Text="Cancel" />
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script src="/Resources/scripts/jquery.2.1.0.min.js"></script>
    <script src="/Resources/scripts/jquery-ui.custom.min.js" type="text/javaScript"></script>
    <script src="/Resources/bootstrap_assets/javascripts/bootstrap.min.js" type="text/javaScript"></script>

    <script src="/Resources/scripts/affinion-framework.js"></script>
    <script src="/Resources/scripts/affinion-plugins.js"></script>

    <script src="/Resources/scripts/languages/datepicker-zh-CN.js" type="text/javascript"></script>
    <script src="/Resources/scripts/languages/datepicker-fr.js" type="text/javascript"></script>
    <script src="/Resources/scripts/languages/datepicker-en-US.js" type="text/javascript"></script>

    <script type="text/javascript" src="/Resources/scripts/Supplier.js"></script>
    <script type="text/javascript" src="/Resources/scripts/Supplier.ManageAvailability.js"></script>
    <script type="text/javascript" src="/Resources/scripts/Supplier.ManageOfferAvailability.js"></script>
    <script>
        function ReCalculateVat(obj) {
            var controlId = obj.id;
            if (controlId.indexOf("ddlVatPercentage") > -1) {
                controlId = controlId.replace("ddlVatPercentage", "##ChangeID##")
            }
            else if (controlId.indexOf("txtAmount") > -1) {
                controlId = controlId.replace("txtAmount", "##ChangeID##")
            }
            else if (controlId.indexOf("txtPercentageAmount") > -1) {
                controlId = controlId.replace("txtPercentageAmount", "##ChangeID##")
            }

            //        txtPercentageAmount
            var vat = $("#" + controlId.replace("##ChangeID##", "ddlVatPercentage")).val();
            var price = $("#" + controlId.replace("##ChangeID##", "txtAmount")).val();
            var percentage = $("#" + controlId.replace("##ChangeID##", "txtPercentageAmount")).val();
            if (obj.id.indexOf("txtPercentageAmount") > -1 && percentage != '') {
                $("#" + controlId.replace("##ChangeID##", "txtAmount")).attr("oldprice", price);
                $("#" + controlId.replace("##ChangeID##", "txtAmount")).val('');
                price = '';
            }
            else if (obj.id.indexOf("txtAmount") > -1 && price != '') {
                $("#" + controlId.replace("##ChangeID##", "txtPercentageAmount")).attr("oldprice", price);
                $("#" + controlId.replace("##ChangeID##", "txtPercentageAmount")).val('');
                percentage = '';
            }
            if (percentage != '') {
                var vatmsg = (isNaN(vat) ? "0" : vat) + " % " + "Of Ex.Vat (%) Amount";
                var totalmsg = (isNaN(percentage) ? "0" : percentage) + " % Of Room Price" + " & " + vatmsg;
                $("#" + controlId.replace("##ChangeID##", "lblVatAmount")).html(vatmsg);
                $("#" + controlId.replace("##ChangeID##", "lblTotalAmount")).html(totalmsg);


            }
            else {
                var vatAmount = roundToTwo(((parseFloat(price) * parseFloat(vat)) / 100));
                var totalAmount = roundToTwo(parseFloat(price) + vatAmount);
                $("#" + controlId.replace("##ChangeID##", "lblVatAmount")).html(isNaN(vatAmount) ? "0" : vatAmount)
                $("#" + controlId.replace("##ChangeID##", "lblTotalAmount")).html(isNaN(totalAmount) ? "0" : totalAmount)
            }
        }
        function roundToTwo(num) {
            return +(Math.round(num + "e+2") + "e-2");
        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>

</body>
</html>
