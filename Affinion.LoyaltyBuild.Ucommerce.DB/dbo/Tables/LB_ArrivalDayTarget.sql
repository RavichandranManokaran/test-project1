﻿CREATE TABLE [dbo].[LB_ArrivalDayTarget] (
    [ArrivalDayTargetId] INT            NOT NULL,
    [ArrivalDay]         NVARCHAR (500) NOT NULL,
    CONSTRAINT [PK_uCommerce_ArrivalDay] PRIMARY KEY CLUSTERED ([ArrivalDayTargetId] ASC),
    CONSTRAINT [FK_uCommerce_ArrivalDayTarget_uCommerce_Target] FOREIGN KEY ([ArrivalDayTargetId]) REFERENCES [dbo].[uCommerce_Target] ([TargetId])
);

