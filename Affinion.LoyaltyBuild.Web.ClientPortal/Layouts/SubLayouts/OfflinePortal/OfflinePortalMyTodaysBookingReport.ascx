﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalMyTodaysBookingReport.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalMyTodaysBookingReport" %>
<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Sitecore.Data.Items" %>

 <div>
     <sc:text id="ChartTitle" runat="server" />
 </div>
<div class="row no-margin">
    <div class="col-md-12 no-padding">
        <div class="col-md-6 no-padding-right">
            <asp:Chart ID="MyBookingReportChart" runat="server" Width="500px" Height="500px">
            <Series>
                <asp:Series Name="CurrentUser" ChartType="Column" ToolTip="#SERIESNAME #VALX Booking Count is #VAL" 
                    IsValueShownAsLabel="false" ChartArea="MyBookingReportChartArea" MarkerBorderColor="#e6ac00" Color="#e6ac00" />
            <asp:Series Name="OtherUser" ChartType="Column" ToolTip="#SERIESNAME #VALX Booking Count is #VAL" 
                IsValueShownAsLabel="false" ChartArea="MyBookingReportChartArea" MarkerBorderColor="#cc3300" Color="#cc3300" />
            </Series>
            <Titles><asp:Title Docking="Bottom" Text="TimeFrame" /></Titles>
            <Titles><asp:Title Docking="Left" Text="Number of Rooms Booked" Alignment="MiddleCenter" TextOrientation="Rotated90"/></Titles>
            <ChartAreas>
            <asp:ChartArea Name="MyBookingReportChartArea" BorderWidth="0" >
                <Area3DStyle Enable3D="false"/>
                    <AxisX LineColor="Gray">
                        <MajorGrid LineColor="white" />
                        <LabelStyle ForeColor="Black" />
                    </AxisX>
                    <AxisY LineColor="Gray">
                        <MajorGrid Enabled="true" LineColor="#E2E2E20" />
                        <LabelStyle ForeColor="Black" />
                    </AxisY>
            </asp:ChartArea>
            </ChartAreas>
            </asp:Chart>
        </div>
        <div class="col-md-6 no-padding-left">
            <div class="col-md-12 no-padding">
                <span class="currentuser colorguidewidth"></span>
                <div class="pull-left"><sc:text id="CurrentUser" runat="server" /></div>
            </div>
            <div class="col-md-12 no-padding">
                <span class="otheruser colorguidewidth"></span>
                <div class="pull-left"><sc:text id="OtherUser" runat="server" /></div>
            </div>
            <div class="col-md-12 no-padding martop10">
                <asp:GridView ID="MyTodaysBookingGrid" class="table table-striped table-bordered" runat="server"></asp:GridView>
            </div>
            <div class="col-md-12 no-padding martop10">
                 <button type="button" class="listAgentPerformance btn btn-primary"     OnServerClick="RegenerateReportButton_Click" runat="server">
                    <sc:text id="RefreshButton" runat="server" />
                </button>
                 <div><sc:text id="StarMessage" runat="server" /></div>
            </div>
        </div>
    </div>
</div>


<!--border-color:#E2E2E20-->