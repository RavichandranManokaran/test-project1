﻿CREATE PROCEDURE [dbo].[SelectLoginAudit]
       @UserName    varchar(200)       
AS
BEGIN 
     SET NOCOUNT ON 

     SELECT TOP 6 [IsLoginSucess]           
     FROM   [dbo].[LoginAudit]   
     WHERE  
            UserName = @UserName  Order By [LoggedInDate] DESC

END 

