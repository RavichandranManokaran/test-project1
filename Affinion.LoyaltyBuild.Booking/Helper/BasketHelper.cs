﻿using data = Affinion.LoyaltyBuild.Api.Booking.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.Api;
using Affinion.LoyaltyBuild.Api.Booking.Service;
using Affinion.LoyaltyBuild.Model.Product;
using Affinion.LoyaltyBuild.Api.Booking.Extension;
using Affinion.LoyaltyBuild.Api.Booking.Data.Basket;
using UCommerce.Infrastructure;

namespace Affinion.LoyaltyBuild.Api.Booking.Helper
{
    /// <summary>
    /// Basket helper class
    /// </summary>
    public static class BasketHelper
    {
        /// <summary>
        /// Booking service
        /// </summary>
        /// <returns></returns>
        private static IBookingService GetBookingService()
        {
            return ObjectFactory.Instance.Resolve<IBookingService>();
        }

        /// <summary>
        /// Add to basket
        /// </summary>
        /// <param name="product"></param>
        public static void AddToBasket(IProduct product,int quantity,Dictionary<string,string> dicProduct)
        {
            LBTransactionLibrary.AddToBasket(quantity, product, dicProduct, false);
            
            //reset payment method
            ResetPaymentMethod();
            //set processing fee
            SetProcessingFee();
        }

        /// <summary>
        /// Add to basket using request
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="product"></param>
        /// <param name="quantity"></param>
        /// <param name="basketRequest"></param>
        public static void AddToBasket<T>(IProduct product, int quantity, T basketRequest) where T : IAddToBasketRequest
        {
            //call add to basket
            AddToBasket(product, quantity, basketRequest.ToDictionary());
        }

        /// <summary>
        /// Get basket info
        /// </summary>
        /// <returns></returns>
        public static data.BasketInfo GetBasket()
        {
            var bookingService = GetBookingService();

            return bookingService.GetBasket();
        }

        /// <summary>
        /// Is basket exist
        /// </summary>
        public static bool HasBasket
        {
            get
            {
               return TransactionLibrary.HasBasket();
            }
        }

        /// <summary>
        /// Clear Basket
        /// </summary>
        public static void ClearBasket()
        {
            TransactionLibrary.ClearBasket();
        }

        /// <summary>
        /// Rest payment method
        /// </summary>
        public static void ResetPaymentMethod()
        {
            SetPaymentMethod(null, null);
        }

        /// <summary>
        /// Set payment method
        /// </summary>
        /// <param name="paymentType"></param>
        /// <param name="paymentMethod"></param>
        public static  void SetPaymentMethod(string paymentType, string paymentMethod)
        {
            if (TransactionLibrary.HasBasket())
            {
                var bookingService = GetBookingService();

                var basket = bookingService.GetBasket();

                basket.Order[Constants.PaymentMethod] = paymentMethod;
                basket.Order[Constants.PaymentType] = paymentType;

                if (basket.Bookings.Count > 0)
                {
                    var clientId = basket.Bookings.FirstOrDefault().ClientId;
                    var rules = RuleHelper.GetPaymentRules(clientId);

                    foreach (var item in basket.Bookings)
                    {
                        //always full payment for bedbaks
                        if(item[Constants.ProviderType] == Affinion.LoyaltyBuild.Common.Constants.ProviderType.BedBanks)
                        {
                            item[Constants.PaymentMethod] = Affinion.LoyaltyBuild.Common.Constants.PaymentMethod.FullPayment;
                        }
                        else if (paymentMethod != null)
                        {
                            item[Constants.PaymentMethod] = paymentMethod;
                        }
                        else
                        {
                            int? campainId = null;
                            int tempCampain = 0;
                            if (int.TryParse(item[Constants.AppliedCampaign], out tempCampain))
                                campainId = tempCampain;

                            item[Constants.PaymentMethod] = RuleHelper.GetPaymentMethod(rules, item[Constants.BookingThrough], item[Constants.ProviderType], null, campainId);

                        }

                        if (item[Constants.PaymentMethod] == Affinion.LoyaltyBuild.Common.Constants.PaymentMethod.DepositOnly)
                            item.ProviderAmountPaidOnCheckOut = 0;
                        else
                            item.ProviderAmountPaidOnCheckOut = item.ProviderAmount;
                    }
                }

                basket.Order.Save();
            }
        }
        
        /// <summary>
        /// Set payment method
        /// </summary>
        /// <param name="paymentType"></param>
        /// <param name="paymentMethod"></param>
        public static void SetProcessingFee()
        {
            if (TransactionLibrary.HasBasket())
            {
                var bookingService = GetBookingService();

                var basket = bookingService.GetBasket();

                int maxProcessFeeBooking = 0;
                decimal maxProcessFee = 0;
                foreach (var item in basket.Bookings)
                {
                    if (item.CalculatedProcessingFee >= maxProcessFee)
                    {
                        maxProcessFee = item.CalculatedProcessingFee;
                        maxProcessFeeBooking = item.OrderLineId;
                    }
                }

                foreach (var item in basket.Bookings)
                    item.ProcessingFee = item.OrderLineId == maxProcessFeeBooking ? maxProcessFee : 0;

                basket.Order.Save();
            }
        }

        /// <summary>
        /// Get all order payment methods
        /// </summary>
        /// <returns></returns>
        public static List<string> GetAllPaymentMethods()
        {
            var methods = new List<string>();
            var bookingService = GetBookingService();

            if (TransactionLibrary.HasBasket())
            {
                var basket = bookingService.GetBasket();
                foreach (var item in basket.Bookings)
                {
                    methods.Add(item[Constants.PaymentMethod]);
                }
            }

            return methods;
        }

        /// <summary>
        /// Get basket count
        /// </summary>
        public static int BasketCount {
            get
            {
                try
                {

                    if (HasBasket)
                    {
                        var basket = GetBasket();
                        if (basket != null && basket.Bookings != null)
                            return basket.Bookings.Count;
                        else
                            return 0;
                    }
                    else
                        return 0;
                }
                catch (Exception)
                {
                    
                    throw;
                }
               
            }
        }

        /// <summary>
        /// remove item from basket
        /// </summary>
        /// <param name="orderLineId"></param>
        /// <returns></returns>
        public static bool RemoveBasketItem(int orderLineId)
        {
            var result = LBTransactionLibrary.RemoveBasketItem(orderLineId);

            //reset processing fee calculated
            if (result)
                SetProcessingFee();

            return result;
        }

    }
}
