﻿namespace Affinion.Loyaltybuild.BusinessLogic.Schedulers.Finance
{
    using Affinion.LoyaltyBuild.Common;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using Affinion.LoyaltyBuild.Common.Exceptions;

    /// <summary>
    /// Generates payment requests related to Amount receivable and amount payable
    /// </summary>
    public class PaymentRequests
    {
        /// <summary>
        /// Requests payments for suppliers
        /// </summary>
        public void ProcessSupplierPaymentRequests()
        {
            try
            {
                Diagnostics.Trace(DiagnosticsCategory.AdminPortal, "PaymentRequests.ProcessSupplierPaymentRequests() method started");

                if (ExecuteProcess("SupplierPaymentRequestGenerationDay"))
                {

                    ReportsService iReportsService = new ReportsService();
                    ValidationResponse objValidationResponse = iReportsService.GenerateSupplierPaymentRequests();
                    if (objValidationResponse.IsSuccess)
                    {
                        Diagnostics.Trace(DiagnosticsCategory.AdminPortal, "PaymentRequests.ProcessSupplierPaymentRequests() method executed successfully");
                    }
                    else
                    {
                        Diagnostics.Trace(DiagnosticsCategory.AdminPortal, "PaymentRequests.ProcessSupplierPaymentRequests() method failed to execute successfully");
                    }
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.AdminPortal, ex, this);
            }
        }

        /// <summary>
        /// Requests vat payment generation requests
        /// </summary>
        public void ProcessVatPaymentRequests()
        {
            try
            {
                Diagnostics.Trace(DiagnosticsCategory.AdminPortal, "PaymentRequests.ProcessVatPaymentRequests() method started");

                if (ExecuteProcess("VatPaymentRequestGenerationDay"))
                {
                    ReportsService iReportsService = new ReportsService();
                    ValidationResponse objValidationResponse = iReportsService.GenerateVatPaymentRequests();
                    if (objValidationResponse.IsSuccess)
                    {
                        Diagnostics.Trace(DiagnosticsCategory.AdminPortal, "PaymentRequests.ProcessVatPaymentRequests() method executed successfully");
                    }
                    else
                    {
                        Diagnostics.Trace(DiagnosticsCategory.AdminPortal, "PaymentRequests.ProcessVatPaymentRequests() method failed to execute successfully");
                    }
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.AdminPortal, ex, this);
            }
        }

        /// <summary>
        /// Check conditions to decide weather to execute the request depending on the day of the month
        /// </summary>
        /// <param name="settingsFieldName">Field name of the Finance settings item in admin portal global folder</param>
        /// <returns>Returns true if scheduled task to be executed</returns>
        private bool ExecuteProcess(string settingsFieldName)
        {
            try
            {
                Database sitecoreDatabase = Sitecore.Configuration.Factory.GetDatabase("master");
                Item settingsItem = sitecoreDatabase.GetItemByKey("FinanceSettings");
                if (settingsItem == null)
                {
                    throw new AffinionException("Error occured while executing the scheduled job for AR or AP. FinanceSettings key was not found under key path (default: /sitecore/content/settings/guids)");
                }

                int noOfDaysInMonth = DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month);
                int dayToExecute = SitecoreFieldsHelper.GetInteger(settingsItem, settingsFieldName, 100);
                int dayOfToday = DateTime.Today.Day;

                dayToExecute = dayToExecute == 32 ? noOfDaysInMonth : dayToExecute;

                if (dayToExecute == 0 || (dayToExecute <= noOfDaysInMonth && dayToExecute == dayOfToday))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.AdminPortal, ex, this);
                return false;
            }
        }
    }
}
