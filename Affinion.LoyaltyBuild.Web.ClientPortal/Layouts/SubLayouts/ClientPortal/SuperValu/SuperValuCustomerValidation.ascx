﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SuperValuCustomerValidation.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal.SuperValuCustomerValidation" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>


<div class="row" id="SuperValuValidation1" runat="server">
    <div class="">
        <div class="panel-group" id="accordion2">
            <div class="panel panel-default">
                <div class="col-sm-12">
                    <div class="panel-heading bg-primary">
                        <h4 class="panel-title">
                            <span>My Booking</span>
                            <a data-toggle="collapse" data-parent="#SuperValuValidation" href="#SuperValuValidation" class="show-hide-list"></a>
                        </h4>
                    </div>
                    <div id="superValuValidation" class="panel-collapse collapse in" runat="server">
                        <div class="panel-body">
                            <div class="row margin-row">
                                <div class="col-xs-12 col-md-12">
                                    <p class="margin-bottom-15">Northern Ireland Customers, <a href="#">Click here</a> to book on the correct website</p>
                                </div>
                                <div class="col-xs-12 col-md-12">
                                    <p class="margin-bottom-15">IF YOU ARE HAVING ISSUES ENTERING YOUR FOR NUMBER, PLEASE CALL CUSTOMER CARE</p>
                                </div>
                                <div class="col-xs-12 col-md-12">
                                    <p class="margin-bottom-15">To book your Gateway Break, please enter your detail below</p>
                                </div>
                            </div>
                            <div class="row margin-row">
                                <div class="col-xs-12 col-md-12">
                                    <div class="col-xs-12 col-md-4">
                                        <asp:RadioButton ID="SuperValuOption1" runat="server" Text="Real Rewards Member Number" GroupName="Toggle" />
                                        <div class="col-xs-12 col-md-4">
                                            <asp:TextBox ID="TxtCollectorCard" runat="server" placeholder="Enter the last 11 digits on your Real Rewards to be here" Enabled="false"></asp:TextBox>
                                        </div>


                                        <div class="col-xs-12 col-md-4">
                                            <b>Not a Member ?</b><a href="#">Click here</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row margin-row">
                                    <div class="col-xs-12 col-md-12">
                                        <div class="col-xs-12 col-md-4 ">
                                            <div class="col-xs-12 col-md-12 no-padding">
                                                <asp:RadioButton ID="SuperValuOption2" runat="server" Text="Collector Card Customer" GroupName="Toggle" />
                                            </div>
                                            <div class="col-xs-12 col-md-12 no-padding">Inside the front cover of your collector Card</div>
                                        </div>
                                        <div class="col-xs-12 col-md-4">
                                            <asp:TextBox ID="TxtRealRewardsCardId" runat="server" Enabled="false"></asp:TextBox>
                                        </div>
                                        <div class="col-xs-12 col-md-4">&nbsp;</div>
                                    </div>
                                </div>
                                <div class="row margin-row">
                                    <div class="col-sm-4">&nbsp;</div>
                                    <div class="col-sm-8">
                                        <asp:Button OnClick="ContinueToDetailButton_Click" ID="ContinueToDetailButton" CssClass="btn btn-default add-break font-size-12 pull-right" runat="server" Text="Continue To Payment" />
                                    </div>
                                    <div id="ErrorMessageSection" runat="server">
                                    <asp:CustomValidator ID="ClientcustomValidator" runat="server" ClientValidationFunction="Validators.PerformValidation" ForeColor="Red">
                                        Please provide Valid Input to Proceed
                                    </asp:CustomValidator>
                                        <asp:CustomValidator ID="ServerCustomValidator" runat="server" CausesValidation="false"  OnServerValidate="PerformServerValidationOnCard" EnableClientScript="False" ValidationGroup="SuperValuCardValidationGroup" ForeColor="Red">
                                        Invalid Card Input
                                    </asp:CustomValidator>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
<script language="javascript">
    var Validators = {
        PerformValidation: function (source, clientside_arguments) {
            var valid_val = false;
            if ($('#<%= SuperValuOption1.ClientID  %>').is(':checked')) {
                if (document.getElementById("<%= TxtCollectorCard.ClientID %>").value != "") {
                    valid_val = true;
                }
            }
            else if ($('#<%= SuperValuOption2.ClientID  %>').is(':checked')) {
                if (document.getElementById("<%= TxtRealRewardsCardId.ClientID %>").value != "") {
                    valid_val = true;
                }
            }
            var ErroMessageSection = $('#<%= ErrorMessageSection.ClientID %>');
                       
            clientside_arguments.IsValid = valid_val;
        }
    }
</script>
    <script type="text/javascript">
        $(function () {
            $('#<%= SuperValuOption1.ClientID  %>').click(function (e) {

                var CollectorCard = $('#<%= TxtCollectorCard.ClientID %>');
                var RealRewardsCardId = $('#<%= TxtRealRewardsCardId.ClientID %>');

                if (this.checked) {
                    CollectorCard.removeAttr('disabled');
                    RealRewardsCardId.attr('disabled', true);
                    RealRewardsCardId.val("");
                }
            });

            $('#<%= SuperValuOption2.ClientID  %>').click(function (e) {

                var CollectorCard = $('#<%= TxtCollectorCard.ClientID %>');
                var RealRewardsCardId = $('#<%= TxtRealRewardsCardId.ClientID %>');

                if (this.checked) {
                    RealRewardsCardId.removeAttr('disabled');
                    CollectorCard.attr('disabled', true);
                    CollectorCard.val("");
                }
            });            
        });
    </script>
