﻿CREATE TABLE [dbo].[uCommerce_Permission] (
    [PermissionId] INT IDENTITY (1, 1) NOT NULL,
    [UserId]       INT NULL,
    [RoleId]       INT NULL,
    CONSTRAINT [uCommerce_PK_Permission] PRIMARY KEY CLUSTERED ([PermissionId] ASC),
    FOREIGN KEY ([RoleId]) REFERENCES [dbo].[uCommerce_Role] ([RoleId]),
    FOREIGN KEY ([UserId]) REFERENCES [dbo].[uCommerce_User] ([UserId]),
    CONSTRAINT [Unique_Permission_UserId_RoleId] UNIQUE NONCLUSTERED ([UserId] ASC, [RoleId] ASC)
);

