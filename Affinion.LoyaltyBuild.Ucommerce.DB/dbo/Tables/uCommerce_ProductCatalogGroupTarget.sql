﻿CREATE TABLE [dbo].[uCommerce_ProductCatalogGroupTarget] (
    [ProductCatalogGroupTargetId] INT           NOT NULL,
    [Name]                        NVARCHAR (60) NOT NULL,
    CONSTRAINT [PK_uCommerce_ProductCatalogGroupTarget] PRIMARY KEY CLUSTERED ([ProductCatalogGroupTargetId] ASC)
);

