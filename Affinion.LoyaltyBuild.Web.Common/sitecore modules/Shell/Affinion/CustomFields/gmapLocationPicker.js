﻿var map;
var geocoder;
var marker;

//return current position value by using Latitude Longitude Format
function GetCurrentLocation() {
    var currentLoc = $J('input#TextBox_SelectedLocation').val();

    return currentLoc;
}

function initializeMap() {

    var currentLoc = GetCurrentLocation();
    var lat = -34.397;
    var lng = 150.644;

    if (currentLoc != "") {
        var position = currentLoc.split(",");

        if (position.length == 2) {
            lat = parseFloat(position[0]);
            lng = parseFloat(position[1]);
        }
    }

    if (map == null) {
        var mapOptions = {
            zoom: 8,
            center: new google.maps.LatLng(lat, lng),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            panControl: false,
            zoomControl: true,
            scaleControl: true,
            streetViewControl: false
        };

        var mapDiv = $J('div.mapCanvas').get(0);

        map = new google.maps.Map(mapDiv, mapOptions);
        placeMarker(mapOptions.center);

        //add event listener
        google.maps.event.addListener(map, 'click', function (event) {

            if (marker != null) {
                placeMarker(event.latLng);
            }
        });

        google.maps.event.addListener(marker, 'dragend', function (event) {
            map.panTo(event.latLng);

            updateLocation(event.latLng);
        });
    }
}

function updateMapLocation(address) {
    if (address != '') {

        if (geocoder == null) {
            geocoder = new google.maps.Geocoder();
        }

        geocoder.geocode({ 'address': address }, function (result, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                //map.setCenter(result[0].geometry.location);

                placeMarker(result[0].geometry.location);
            } else {
                alert("Geocode was not successfull for the following reason: " + status);
            }
        });
    }
}

function placeMarker(latLng) {
    if (marker == null) {
        marker = new google.maps.Marker({
            map: map,
            draggable: true
        });
    }

    marker.setPosition(latLng);
    marker.setAnimation(google.maps.Animation.DROP);

    map.panTo(latLng);
    updateLocation(latLng);
}

function updateLocation(latLng) {
    //document.getElementById('TextBox_SelectedLocation').value = latLng.Xa.toString() + "," + latLng.Ya.toString();
    $J('input#TextBox_SelectedLocation').val(latLng.lat().toString() + "," + latLng.lng().toString());
}