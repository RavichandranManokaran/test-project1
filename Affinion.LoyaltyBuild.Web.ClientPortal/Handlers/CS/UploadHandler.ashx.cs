﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Customer;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Web;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Handlers.CS
{
    /// <summary>
    /// Summary description for UploadHandler
    /// </summary>
    public class UploadHandler : IHttpHandler
    {
        private NameValueCollection _query = null;
        private QueryService _queryService;

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                if (context.Request.Files.Count > 0)
                {
                    int queryId = 0;
                    if (HttpContext.Current.Request.Params["queryId"] != null)
                    {
                        queryId = Convert.ToInt32(HttpContext.Current.Request.Params["queryId"]);
                    }
                    if (queryId == 0)
                    {
                        throw new Exception("invalid request");
                    }
                    _query = HttpUtility.ParseQueryString(context.Request.Url.Query.ToLower());
                    _queryService = new QueryService();
                    HttpFileCollection files = context.Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFile file = files[i];
                        string fname;
                        if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE" || HttpContext.Current.Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }
                        string extension = System.IO.Path.GetExtension(file.FileName);
                        byte[] fileData = null;
                        using (var binaryReader = new BinaryReader(HttpContext.Current.Request.Files[0].InputStream))
                        {
                            fileData = binaryReader.ReadBytes(HttpContext.Current.Request.Files[0].ContentLength);
                        }
                        CustomerQueryAttachment attachment = new CustomerQueryAttachment();
                        attachment.File = fileData;
                        attachment.QueryId = queryId;
                        attachment.FileName = fname;
                        attachment.FileType = extension;
                        ValidationResponse respose= _queryService.AddCustomerAttachment(attachment);
                        if(respose.IsSuccess)
                        {
                            context.Response.Write("1");
                        }
                        else
                        {
                            context.Response.Write("0");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, this);
                context.Response.Write("0");
            }
            context.Response.End();

        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}

