﻿CREATE procedure [dbo].[LB_BB_GetProvidersInErrorList] (@listID int)
as
BEGIN
select DISTINCT pro.availabilityProviderRecordID [ProviderId], pro.providerName [ProviderName], pro.addressLine5 [country], pro.addressLine4 [location], pro.addressLine3 [maplocation], CONCAT(pro.addressLine1,',', pro.addressLine2) [address] from LB_BB_provider pro
inner join LB_BB_MarginErrorDetail errDetails ON pro.availabilityProviderRecordID=errDetails.ProviderID
inner join LB_BB_MarginErrorList errList ON errDetails.ListID=errList.ListID
where errList.ListID=@listID
END 
GO