﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Api.Booking.Data.Basket
{
    /// <summary>
    /// Add to basket add on request
    /// </summary>
    public class AddOnRequest : IAddToBasketRequest
    {
        /// <summary>
        /// CurrencyID Property
        /// </summary>
        [OrderProperty(Constants.CurrencyId)]
        public int CurrencyId { get; set; }

        /// <summary>
        /// Sku
        /// </summary>
        public string Sku { get; set; }

        /// <summary>
        /// Variant Sku
        /// </summary>
        public string VariantSku { get; set; }

        /// <summary>
        /// NoOfRooms Property
        /// </summary>
        public int NoOfRooms { get; set; }


        public string ClientId { get; set; }

        /// <summary>
        /// Created By
        /// </summary>
        public string CreatedBy { get; set; }
    }
}
