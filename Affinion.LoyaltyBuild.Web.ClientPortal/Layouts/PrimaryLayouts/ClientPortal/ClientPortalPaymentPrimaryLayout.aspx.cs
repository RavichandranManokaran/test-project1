namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.PrimaryLayouts.ClientPortal
{
    using Affinion.Loyaltybuild.BusinessLogic.Helper;
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.Common.Exceptions;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Sitecore.Data.Items;
    using System;
    using System.Globalization;
    using System.Web;
    using System.Web.UI;
    using UCommerce;
    using UCommerce.Runtime;
    using UCommerce.EntitiesV2;
    using UCommerce.Transactions.Payments;
    using AffinionConstants = Affinion.LoyaltyBuild.Common.Constants;

    public partial class ClientPortalPaymentPrimaryLayout : BasePrimaryLayout
    {
        #region Public properties
        /// <summary>
        /// Payment gateway settings
        /// </summary>
        public string GatewayUrl { get; set; }
        public string MerchantId { get; set; }
        public string Secret { get; set; }
        public string CurrentTimeStamp { get; set; }
        public string OrderId { get; set; }
        public string Account { get; set; }
        public string PaymentCurrency { get; set; }
        public string AmountToPay { get; set; }
        public string ShaHash { get; set; }
        public string ResponseUrl { get; set; }
        public string AnythingElse { get; set; }
        #endregion

        #region Protected methods

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ///Load the portal specific payment method. TODO: Read from sitecore configuration
                Item clientPaymentMethodItem = SitecoreFieldsHelper.GetDropLinkFieldTargetItem(ItemHelper.RootItem, "ServiceName");

                if (clientPaymentMethodItem == null)
                {
                    throw new AffinionException("Payment Method Service is not configured for this client. Payment method is not selected for this supplier in Sitecore");
                }

                PaymentMethod paymentMethod = PaymentMethod.SingleOrDefault(x => x.Name == clientPaymentMethodItem.Fields["ServiceName"].Value);

                if (paymentMethod == null)
                {
                    throw new AffinionException("Defined Payment Method Service cannot be found. Cannot find a payment method in UCommerce for the selected service in Sitecore. May be the service names are mismatched in sitecore & ucommerce");
                }

                ///Get the current basket
                Basket basket = UCommerce.Runtime.SiteContext.Current.OrderContext.GetBasket();
                ///Get current purchase order
                var order = basket.PurchaseOrder;
                ///Calculate the amount to pay. TODO: May be discounts and voucher related
                var amount = CalculateAmount(order);
                ///Build the page and load the payment gateway
                ExecutePaymentMethodService(paymentMethod, order, amount);
            }
            catch (Exception exception)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, exception.Message);
                throw new AffinionException("Failure loading Realex payment page", exception);
            }
        }

        #endregion

        #region Public methods
        /// <summary>
        /// Execute payment method service to build and load payment gateway
        /// </summary>
        /// <param name="paymentMethod">Selected payment method</param>
        /// <param name="purchaseOrder">Current purchase order</param>
        /// <param name="amount">Amount to be paid</param>
        public void ExecutePaymentMethodService(PaymentMethod paymentMethod, PurchaseOrder purchaseOrder, decimal amount)
        {
            var cultureInfo = new CultureInfo(CultureInfo.InvariantCulture.LCID);

            ///Create a new payment request to 
            var paymentRequest = new PaymentRequest(
                        purchaseOrder,
                        paymentMethod,
                        new Money(amount,
               new CultureInfo(CultureInfo.InvariantCulture.LCID), purchaseOrder.BillingCurrency));

            ///Build and load the payment page
            Payment payment = paymentMethod
                            .GetPaymentMethodService()
                            .RequestPayment(paymentRequest);
            SetSession(purchaseOrder.OrderGuid.ToString(), purchaseOrder.OrderTotal.ToString(), paymentRequest.PurchaseOrder.OrderId.ToString());
            //SetSession(purchaseOrder.OrderGuid.ToString(),"20", paymentRequest.PurchaseOrder.OrderId.ToString());
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Calculate amount to be paid
        /// </summary>
        /// <param name="paymentMethod">Selected payment method for the current portal</param>
        /// <param name="order">Current purchase order</param>
        /// <returns>Returns the amount to be paid</returns>
        private decimal CalculateAmount(PurchaseOrder order)
        {
            //Item clientItem = SitecoreFieldsHelper.GetLookupFieldTargetItem(ItemHelper.RootItem, "ClientDetails");

            //return BasketHelper.GetTotalPayableToday(order.OrderId, clientItem.ID.Guid.ToString());
            decimal checkoutAmount = 0.00m;

            Basket basket = SiteContext.Current.OrderContext.GetBasket();
            PurchaseOrder purchaseOrder = basket.PurchaseOrder;
            decimal.TryParse(Convert.ToString(purchaseOrder.OrderTotal), out checkoutAmount);
            if (checkoutAmount > 0)
            {
                return checkoutAmount;
            }
            else
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, "Payment Amount not defined.");
                throw new AffinionException("Payment Amount not defined.");
            }
        }
        private void SetSession(string oderId, string amount, string responseOther)
        {
            Session["ResponseOrderId"] = oderId;
            Session["ResponseAmount"] = amount;
            Session["ResponseOther"] = responseOther;
        }

        #endregion

        #region Overridden methods

        public override System.Web.UI.HtmlControls.HtmlForm MainForm
        {
            get
            {
                return this.mainform;
            }
        }

        #endregion
    }
}
