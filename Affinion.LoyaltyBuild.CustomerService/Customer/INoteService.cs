﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/
namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Customer
{
    /// <summary>
    /// Interface to define the Customer Notes related actions
    /// </summary>
    public interface INoteService 
    {
        /// <summary>
        /// To Save Customer Notes
        /// </summary>
        /// <param name="reservationNotes">Booking Notes</param>
        /// <returns> True or False  </returns>
        ValidationResponse SaveReservationNote(BookingNotes reservationNotes);

        /// <summary>
        /// To get the Customer Notes
        /// </summary>
        /// <param name="OrderLineID">OrderLineID</param>
        /// <returns>BookingNotes object List  </returns>
         List<BookingNotes> GetReservationNotes(int orderLineID);
    }
}
