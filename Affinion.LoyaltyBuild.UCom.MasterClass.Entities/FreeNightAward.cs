﻿using Affinion.LoyaltyBuild.UCom.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;
using Affinion.LoyaltyBuild.UCom.Common.Extension;


namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class FreeNightAward : Award, IEntity
    {
        public FreeNightAward()
        {
        }
        // Fields
        private int? _hashCode;

        // Methods

        public override void Apply(PurchaseOrder order, IList<OrderLine> orderLines)
        {
            if (orderLines != null && orderLines.Any())
            {
                foreach (OrderLine line in orderLines)
                {
                    OrderProperty arrivalDateProperty = line.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.ArrivalDateKey);
                    OrderProperty nightProperty = line.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.NightKey);
                    if (arrivalDateProperty != null && nightProperty != null)
                    {
                        DateTime arrivalDate = arrivalDateProperty.Value.ParseFormatDateTime(true);
                        int startDate = arrivalDate.ConvetDatetoDateCounter();
                        int night = 0;
                        int.TryParse(nightProperty.Value, out night);
                        int endDate = startDate + night - 1;
                        RoomAvailability avaialbilityData = RoomAvailability.All().Where(x => x.DateCounter == endDate && x.VariantSku == line.VariantSku).FirstOrDefault();
                        if (avaialbilityData != null)
                        {
                            Discount discount = this.CreateDiscount(order, orderLines);
                            discount.AmountOffTotal = avaialbilityData.Price;
                            order.AddDiscount(discount);
                            discount.AddOrderLine(line);
                            line.AddDiscount(discount);
                        }
                    }
                }
            }
        }

        // Methods
        public new static IQueryable<FreeNightAward> All()
        {
            return GetRepo().Select();
        }

        public override void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<FreeNightAward, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            FreeNightAward objA = obj as FreeNightAward;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<FreeNightAward, bool>> expression)
        {
            return All().Any<FreeNightAward>(expression);
        }

        public static IList<FreeNightAward> Find(Expression<Func<FreeNightAward, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<FreeNightAward>();
        }

        public static FreeNightAward FirstOrDefault(Expression<Func<FreeNightAward, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<FreeNightAward>();
        }

        public new static FreeNightAward Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<FreeNightAward> GetRepo()
        {
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<FreeNightAward>>();

        }


        public override void Save()
        {
            GetRepo().Save(this);
        }

        public static FreeNightAward SingleOrDefault(Expression<Func<FreeNightAward, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties
        public virtual int FreeNightAwardId { get; protected set; }

        public override int Id
        {
            get
            {
                return this.FreeNightAwardId;
            }
        }

        public virtual int NoOfNight { get; set; }
    }
}
