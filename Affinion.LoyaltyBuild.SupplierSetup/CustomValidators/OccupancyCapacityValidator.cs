﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           OccupancyCapacityValidator.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.SupplierSetup.CustomValidators
/// Description:           Handling custom error for occupancy capacity
/// </summary>

using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Data.Validators;
using System;
using System.Runtime.Serialization;
using Affinion.LoyaltyBuild.Common.Instrumentation;

namespace Affinion.LoyaltyBuild.SupplierSetup.CustomValidators
{
    [Serializable]
    public class OccupancyCapacityValidator : StandardValidator
    {
        public OccupancyCapacityValidator()
        {

        }

        public OccupancyCapacityValidator(SerializationInfo info, StreamingContext context): base(info, context)
        {

        }

        protected override ValidatorResult Evaluate()
        {
            try
            {
                Item validatingItem = base.GetItem();

                Field totalOccupancy = validatingItem.Fields["TotalCapacity"];

                int occupancyCapacity = 0;

                if (int.TryParse(totalOccupancy.Value, out occupancyCapacity) && occupancyCapacity >= 10)
                {
                    return ValidatorResult.Valid;
                }
                this.Text = string.Format("Total capacity should be greater than 10");
                return ValidatorResult.Error;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.SupplierSetup, ex, null);
                throw;
            }
        }

        protected override ValidatorResult GetMaxValidatorResult()
        {
            return GetFailedResult(ValidatorResult.Error);
        }

        public override string Name
        {
            get { return "Total capacity should be greater than 10"; }
        }
    }
}
