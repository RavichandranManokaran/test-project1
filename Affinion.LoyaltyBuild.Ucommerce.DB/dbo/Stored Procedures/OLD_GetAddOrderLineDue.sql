﻿CREATE PROCEDURE [dbo].[OLD_GetAddOrderLineDue]
            
				@OrderLineID INT,
				@CurrencyTypeId INT,
				@OrderLinePaymentId INT,
                @DueTypeId INT,
                @Amount MONEY,
                @CommissionAmount MONEY ,
                @TransferAmount MONEY ,
				@CancellationAmount MONEY,
                @ClientAmount MONEY ,
                @ProviderAmount MONEY,
                @CollectionByProviderAmount MONEY ,
                @Note VARCHAR(MAX)
				

As				
BEGIN
INSERT INTO [dbo].LB_OrderLineDue
	(
	OrderLineId,
	CurrencyTypeId,
	OrderLinePaymentId,
	DueTypeId,
	Amount,
	CommissionAmount,
	TransferAmount,
	CancellationAmount,
	ClientAmount,
	ProviderAmount,
	CollectionByProviderAmount,
	Note
	)
VALUES
 (
   @OrderLineID,
   '',
   '',
   '',
   @Amount,
   @CommissionAmount,
   @TransferAmount,
   @CancellationAmount,
   @ClientAmount,
   @ProviderAmount,
   @CollectionByProviderAmount,
   @Note 
 )
 END

