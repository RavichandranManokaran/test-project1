﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model
{
    public class ProvisionalBooking
    {
        /// <summary>
        /// Booking Reference
        /// </summary>
        public string BookingReference { get; set; }
        /// <summary>
        /// Provider Name
        /// </summary>
        public string Provider { get; set; }
        /// <summary>
        /// Reservation Date
        /// </summary>
        public DateTime ReservationDate { get; set; }
        /// <summary>
        /// Arrival Date
        /// </summary>
        public DateTime ArrivalDate { get; set;}
        /// <summary>
        /// FirstName
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Last Name
        /// </summary>
        public string LastName { get; set;}
        /// <summary>
        /// PhoneNumber
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// MobilePhoneNUmber
        /// </summary>
        
        public string MobilePhoneNumber { get; set; }

        ///<summary>
        ///Email
        ///<summary>
        public string EmailId { get; set; }

        /// <summary>
        /// Customer Name
        /// </summary>
        public string CustomerName { get; set; }
    }
}
