﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
using Affinion.LoyaltyBuild.Search.Helpers;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Customer;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Sitecore.Data;
using Sitecore.Data.Items;
using Affinion.LoyaltyBuild.Web.ClientPortal.Helper;


namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    public partial class OfflinePortalSearchCustomerQuery : BaseSublayout
    {
        #region private constant variable

        /// <summary>
        /// Client item path
        /// </summary>
        private const string ClientItemPath = "/sitecore/content/admin-portal/client-setup/{0}";
        Database contextDb = Sitecore.Context.Database;
        #endregion

        //private CustomerQuerySearch _customerQuerySearch;
        private QueryService _customerQueryService = new QueryService();
        //private string _createdBy = "Anto";
        private string _createdBy = SitecoreUserProfileHelper.GetItem(UserProfileKeys.CreatedBy);

        protected void Page_Load(object sender, EventArgs e)
        {
            Item dataSourceitem = this.GetDataSource();
            if (!Page.IsPostBack)
            {
                BindData(dataSourceitem);
            }
        }

        protected void btnSerach_Click(object sender, EventArgs e)
        {
            BindGrid();
        }


        #region "Private Member"
        private void BindData(Item dataSourceitem)
        {
            BindSitecoreFieldData(dataSourceitem);
            BindQueryType();
            BindClientDropDown();
            BindCompaignGroup(dataSourceitem);
            BindProvider();
        }

        private void BindSitecoreFieldData(Item dataSourceitem)
        {
           SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "AddCustomerEnquiryButton", AddCustomerEnquiryButton);
           SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "SearchBoxTitle", SearchBoxTitleText);
           SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "Partner",   PartnerText);
           SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "QueryType", QueryTypeText);
           SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "StartDate", StartDateText);
           SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "EndDate",   EndDateText);
           SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "FirstName", FirstNameText);
           SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "LastName",  LastNameText);
           SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "CampaignGroup", CampaignGroupText);
           SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "Provider",   ProviderText);
           SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "BookingRef", BookingRefText);
           SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "IsResolved", IsResolvedText);
        }

        private void BindGrid()
        {
           // _customerQuerySearch = new CustomerQuerySearch();
           // DateTime checkInDate, checkOutDate;
           // List<SearchQueryResultItem> searchQueryResultItemList = null;
           // _customerQuerySearch.ClientID = ClientList.DataValueField;
           // if (QueryType.SelectedIndex>0)
           // _customerQuerySearch.QueryTypeID = Convert.ToInt32(QueryType.DataValueField);
           // if (DateTime.TryParse(StartDate.Value, out  checkInDate))
           //     _customerQuerySearch.StartDate = checkInDate;
           // if (DateTime.TryParse(EndDate.Value, out  checkOutDate))
           //     _customerQuerySearch.EndDate = checkOutDate;
           // if (txtFirstName.Value.Length > 0)
           //     _customerQuerySearch.FirstName = txtFirstName.Value;
           // if (txtLastName.Value.Length > 0)
           //     _customerQuerySearch.LastName = txtLastName.Value;
           // if (ddlCompaignGroup.SelectedIndex>0)
           //     _customerQuerySearch.CampaignGroupID = Convert.ToInt32(ddlCompaignGroup.SelectedValue);
           // if (ddlProvider.SelectedIndex>0)
           // _customerQuerySearch.ProviderId = ddlProvider.SelectedValue;
           // if (bookingref.Value.Length>0)
           //     _customerQuerySearch.BookingReference = bookingref.Value;
           // if(rbYes.Checked)
           // _customerQuerySearch.IsResolved =true ;
           // if (rbNo.Checked)
           //     _customerQuerySearch.IsResolved = false;
           // if (rbAll.Checked)
           //     _customerQuerySearch.IsResolved = null;

           //searchQueryResultItemList = _customerQueryService.SearchCustomerQuery(_customerQuerySearch);
           
        }

        //To bind the Customer Query type  
        private void BindQueryType()
        {
            DataTable customerQueryTypes = null;
            customerQueryTypes = _customerQueryService.GetQueryTypes();
            QueryType.DataSource = customerQueryTypes;
            QueryType.DataValueField = "QueryTypeId";
            QueryType.DataTextField = "QueryType";
            QueryType.DataBind();
        }

        /// <summary>
        /// Bind Client data to drop downs
        /// </summary>
        private void BindClientDropDown()
        {
            try
            {
                if (SearchHelper.GetUserSpecificClientItems() != null)
                {                    
                    /// Extract Clients Filtered Items
                    var clientFilterList = (from client in SearchHelper.GetUserSpecificClientItems()
                                            orderby client.DisplayName
                                            select new { DiplayName = client.DisplayName, ItemName = client.ID.Guid.ToString() }).ToList();

                    /// Load Client List Dropdown
                    this.ClientList.DataSource = clientFilterList;

                    /// Set data text and data value fields
                    ClientList.DataTextField = "DiplayName";
                    ClientList.DataValueField = "ItemName";

                    this.ClientList.DataBind();
                }

                this.ClientList.Items.Insert(0, Constants.PleaseSelectText);

                /// Assign respective client images to dropdown
                foreach (ListItem clientListItem in ClientList.Items)
                {
                    if (!string.Equals(clientListItem.Text, Constants.PleaseSelectText))
                    {
                        Item clientItem = contextDb.GetItem(string.Format(ClientItemPath, clientListItem.Value));
                        clientListItem.Attributes.Add("data-image", GetImageURL(clientItem, "Customer Service Image"));
                    }
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, null);
                throw;
            }
        }

        private void BindCompaignGroup(Item dataSourceitem)
        {
            if (dataSourceitem != null)
            {
                Dictionary<string, string> campaignDatasource = new Dictionary<string, string>();
                string showAllText = SitecoreFieldsHelper.GetValue(dataSourceitem, "ShowAllLabel");
                if (SitecoreFieldsHelper.GetBoolean(dataSourceitem, "DisplayShowAllLabel"))
                {
                    ListItem showAllItem = new ListItem(showAllText, "");
                    this.ddlCompaignGroup.AppendDataBoundItems = true;
                    this.ddlCompaignGroup.Items.Insert(0, showAllItem);
                }

                Sitecore.Data.Fields.MultilistField offerGroupField = dataSourceitem.Fields["OfferGroups"];
                List<Sitecore.Data.ID> multiListTargetIds = (offerGroupField != null && !string.IsNullOrWhiteSpace(offerGroupField.Value)) ? offerGroupField.TargetIDs.ToList<Sitecore.Data.ID>() : null;
                foreach (var id in multiListTargetIds)
                {
                    Item campaignItem = Sitecore.Context.Database.GetItem(id);
                    if (campaignItem != null)
                    {
                        campaignDatasource.Add(campaignItem.ID.ToString(), campaignItem.Fields["Title"].Value);
                    }
                }

                this.ddlCompaignGroup.DataSource = campaignDatasource;
                this.ddlCompaignGroup.DataTextField = "Value";
                this.ddlCompaignGroup.DataValueField = "Key";
                this.ddlCompaignGroup.DataBind();
            }
        }

        private void BindProvider()
        {
            Dictionary<string, string> providerDatasource = new Dictionary<string, string>();
            
            ListItem showAllItem = new ListItem("Select a Provider", "");
            this.ddlProvider.AppendDataBoundItems = true;
            this.ddlProvider.Items.Insert(0, showAllItem);

            Item[] hotels = Sitecore.Context.Database.SelectItems(Constants.BasketPageQueryPath);
            foreach (var hotel in hotels)
            {
                if (hotel != null)
                {
                    providerDatasource.Add(hotel.ID.ToString(), hotel.Fields["Name"].Value);
                }
            }

            this.ddlProvider.DataSource = providerDatasource;
            this.ddlProvider.DataTextField = "Value";
            this.ddlProvider.DataValueField = "Key";
            this.ddlProvider.DataBind();
        }

        #endregion

        /// <summary>
        /// Method used to Get Image URL of Sitecore Image field
        /// </summary>
        /// <param name="currentItem"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        private string GetImageURL(Item currentItem, string fieldName)
        {
            string imageURL = string.Empty;
            Sitecore.Data.Fields.ImageField imageField = currentItem.Fields[fieldName];
            if (imageField != null && imageField.MediaItem != null)
            {
                Sitecore.Data.Items.MediaItem image = new Sitecore.Data.Items.MediaItem(imageField.MediaItem);
                imageURL = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image));
            }
            return imageURL;
        }
    }
}