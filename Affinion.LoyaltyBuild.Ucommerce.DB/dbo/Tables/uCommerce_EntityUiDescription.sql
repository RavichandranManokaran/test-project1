﻿CREATE TABLE [dbo].[uCommerce_EntityUiDescription] (
    [EntityUiDescriptionId] INT            IDENTITY (1, 1) NOT NULL,
    [EntityUiId]            INT            NOT NULL,
    [DisplayName]           NVARCHAR (512) NOT NULL,
    [Description]           NVARCHAR (MAX) NULL,
    [CultureCode]           NVARCHAR (60)  NOT NULL,
    CONSTRAINT [PK_uCommerce_EntityUiDescription] PRIMARY KEY CLUSTERED ([EntityUiDescriptionId] ASC),
    CONSTRAINT [FK_uCommerce_EntityUiDescription_uCommerce_EntityUi] FOREIGN KEY ([EntityUiId]) REFERENCES [dbo].[uCommerce_EntityUi] ([EntityUiId])
);

