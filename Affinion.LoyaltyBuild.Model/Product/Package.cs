﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Model.Product
{
    /// <summary>
    /// Package product
    /// </summary>
    public class Package : IProduct
    {
        /// <summary>
        /// Sku
        /// </summary>
        public string Sku { get; set; }

        /// <summary>
        /// Variant Sku
        /// </summary>
        public string VariantSku { get; set; }

        /// <summary>
        /// Product type
        /// </summary>
        public ProductType Type { get; set; }

        /// <summary>
        /// List of product the package is made of
        /// </summary>
        IList<IProduct> RelatedProducts { get; set; }

        /// <summary>
        /// Ucommerce currency id
        /// </summary>
        public int CurrencyId { get; set; }

        // <summary>
        /// Gets or sets the Offer Price
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }
    }
}
