﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[LB_AddCommunicationHistory] 
	-- Add the parameters for the stored procedure here
	@orderlineId INT, 
	@communicationTypeId INT,
	@emailText NVARCHAR(MAX),
	@emailHtml NVARCHAR(MAX),
	@from VARCHAR(100),
	@to VARCHAR(100),
	@emailSubject NVARCHAR(250)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   INSERT INTO [dbo].[LB_CommunicationHistory]
           ([CommunicationTypeId]
           ,[OrderLineId]
		   ,[From]
		   ,[To]
	       ,[EmailSubject]
           ,[EmailText]
           ,[EmailHtml]
		   ,[status])
     VALUES
           (@communicationTypeId
           ,@orderlineId
		   ,@from
		   ,@to
		   ,@emailSubject
           ,@emailText
           ,@emailHtml
		   ,0)
END

