﻿DROP PROCEDURE [dbo].[LB_BB_GetErrataInfo]
GO

/****** Object:  StoredProcedure [dbo].[LB_BB_GetErrataInfo]    Script Date: 8/14/2016 6:52:13 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LB_BB_GetErrataInfo] 
	-- Add the parameters for the stored procedure here
	@OrderlineId int

	  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Errata from LB_BB_ErrataInfo where OrderLineId=@OrderlineId 
END
GO