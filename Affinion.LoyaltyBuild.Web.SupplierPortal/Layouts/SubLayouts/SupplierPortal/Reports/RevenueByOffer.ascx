﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RevenueByOffer.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Reports.RevenueByOffer" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" TagPrefix="asp" %>

<div class="supplier-report">
    <div>
        <asp:UpdatePanel ID="uplRevenueByOffer" runat="server">
            <ContentTemplate>
                <ajaxToolkit:CalendarExtender ID="dateFromCalendarExtender" runat="server"
                    TargetControlID="txtDatefrom"
                    PopupPosition="BottomLeft"
                    Format="MMMM d, yyyy"
                    PopupButtonID="btnDateFrom" />
                <ajaxToolkit:CalendarExtender ID="dateToCalendarExtender" runat="server"
                    TargetControlID="txtDateTo"
                    PopupPosition="BottomLeft"
                    Format="MMMM d, yyyy"
                    PopupButtonID="btnDateTo" />
                <asp:Label ID="ReportInstruction" AssociatedControlID="ReportInstruction" runat="server" Text="Please Select the desired option and press list. "></asp:Label><br />
                <br />
                <table class="report-filter">
                    <tr>
                        <td>
                            <asp:Label ID="lblDateFrom" AssociatedControlID="txtDateFrom" runat="server" Text="Date From *"></asp:Label>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ErrorMessage="*" CssClass="highligh-error" ControlToValidate="txtDatefrom" runat="server" />
                            <asp:TextBox ID="txtDateFrom" runat="server"></asp:TextBox>
                            <asp:Button ID="btnDateFrom" runat="server" CssClass="calendar"/>
                        </td>
                        <td>
                            <asp:Label ID="lblDateTo" runat="server" Text="Date To *" AssociatedControlID="txtDateTo"></asp:Label>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ErrorMessage="*" CssClass="highligh-error" ControlToValidate="txtDateTo" runat="server" />
                            <asp:TextBox ID="txtDateTo" runat="server"></asp:TextBox>
                            <asp:Button ID="btnDateTo" runat="server" CssClass="calendar" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblPartner" runat="server" Text="Partner *" AssociatedControlID="ddlPartner"></asp:Label>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ErrorMessage="*" CssClass="highligh-error" ControlToValidate="ddlPartner" runat="server" />
                            <asp:DropDownList ID="ddlPartner" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Label ID="lblOfferName" runat="server" Text="Offer Name" AssociatedControlID="ddlOfferName"></asp:Label>
                        </td>
                        <td>
                          <asp:DropDownList CssClass="form-control" ID="ddlOfferName" runat="server" AppendDataBoundItems="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblProvider" runat="server" Text="Provider" AssociatedControlID="ddlProvider"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList CssClass="form-control" ID="ddlProvider"  AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlProvider_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Label ID="lblProviderGroup" runat="server" Text="Provider Group" AssociatedControlID="ddlProviderGroup"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlProviderGroup" runat="server" Width="325px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblDateType" runat="server" Text="Date Type" AssociatedControlID="ddlDateType"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlDateType" runat="server" Width="200px"></asp:DropDownList>
                        </td>
                        <td>
                            <asp:Label ID="lblCheck" runat="server" Text="Check here to see a" AssociatedControlID="BreakdownCheckbox"></asp:Label>
                            <br />
                            <asp:Label ID="lblBreakdownCampaign" runat="server" Text="breakdown by campaign" AssociatedControlID="BreakdownCheckbox"></asp:Label>
                        </td>
                        <td>
                            <asp:CheckBox ID="BreakdownCheckbox" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblBreakdown" runat="server" Text="Break Down" AssociatedControlID="rbBreakDown"></asp:Label>
                            <br />
                            <asp:Label ID="lblRequired" runat="server" Text="Required" AssociatedControlID="rbBreakDown"></asp:Label>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rbBreakDown" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Text="Overall" Enabled="true" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Monthly" Enabled="true"></asp:ListItem>
                                <asp:ListItem Text="Yearly" Enabled="true"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td>
                            <asp:Button ID="btnListRevenue" runat="server" Height="25px" Text="List Revenue" ValidationGroup="RequiredValidator" Width="120px" OnClick="btnListRevenue_Click" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnListRevenue" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <br />
    <div>
        <rsweb:ReportViewer ID="rvTotalRevenue" runat="server" Width="100%" OnReportError="Viewer_ReportError" AsyncRendering="false"></rsweb:ReportViewer>
    </div>

    <div>
        <rsweb:ReportViewer ID="rvListOfRevenue" runat="server" Width="100%" OnReportError="Viewer_ReportError" AsyncRendering="false"></rsweb:ReportViewer>
    </div>
</div>
