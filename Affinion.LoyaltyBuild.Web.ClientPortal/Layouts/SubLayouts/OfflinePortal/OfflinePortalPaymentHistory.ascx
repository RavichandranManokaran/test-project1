﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalPaymentHistory.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalPaymentHistory" %>
<input type="hidden" class="disableAddDueButton" id="disableAddDueButton" runat="server" />
<div class=""></div>
<div class="col-md-12" id="errormsg" class="<%=(ValidationResponse != null ? "" : "hidden") %>">
    <%if(ValidationResponse != null){%>
    <div class="alert alert-<%= ValidationResponse.IsSuccess ? "success" : "danger" %>">
        <%= ValidationResponse.Message %>
    </div>
    <%} %>
</div>

<%if(PaymentInfo != null) {%>
<div class="col-md-12" id="paymenthistory" data-orderlineid="<%=PaymentInfo.OrderLineId %>">

    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title"><sc:Text id="scPaymentInformation" runat="server"/><%=PaymentInfo.BookingReference %>
                </h4>
            </div>
            <div id="collapse1" class="panel-collapse collapse in" aria-expanded="true">
                <div class="panel-body alert-info">
                    <div style="padding-bottom: 20px" id="duebycurrencies">
                        <div class="panel-heading pay_bg">
                            <span class="pric-eu"><strong><sc:Text id="scPaymentHistoryTotalAmount" runat="server"/></strong></span>
                            <span class="pay_optn"><strong><sc:Text id="scPayment" runat="server"/></strong></span>
                        </div>
                        <%if(PaymentInfo.DueByCurrencies.Count > 0){ %>
                        <%foreach(var item in PaymentInfo.DueByCurrencies) {%>
                        <div class="panel-heading">
                            <span class="pric-eu cur-<%=item.CurrencyTypeId %>"><strong><%= item.Amount.ToString("f") %></strong> </span>
                            <span class="pay_optn">
                                <a href="<%=GetUrl(string.Format((item.Amount>0?"/offlineportalpayment":"/offlineportalrefund") + "?OLID={0}&CID={1}", PaymentInfo.OrderLineId, item.CurrencyTypeId)) %>"><sc:Text id="scNewPayNow" runat="server"/></a>
                            </span>
                        </div>
                        <%} %>
                        <%}else{ %>
                        <div class="panel-heading">
                            <span class="pric-eu"><strong><sc:Text id="scNumeric" runat="server"/></strong> </span>
                            <span class="pay_optn">
                                <a href="javascript:void(0);" onclick="NoPayClick()"><sc:Text id="scOldPayNow" runat="server"/></a>
                            </span>
                        </div>
                        <%} %>
                    </div>
                    <div id="payment-info">
                        <div>
                            <a class="btn btn-default btnAddDue" href="<%=GetUrl(string.Format("/offlineduepayment?olid={0}&ref={1}", PaymentInfo.OrderLineId, PaymentInfo.BookingReference))%>"><sc:Text id="scAddDue" runat="server"/></a>
                            <%if(PaymentInfo.DueHistory!=null){ %>
                            <h4><sc:Text id="scListOfBooking" runat="server"/><%= PaymentInfo.DueHistory.Count %></h4>
                            <div class="table-responsive">
                                <table class="table tbl-calendar">
                                    <thead>
                                        <tr>
                                            <th><sc:Text id="scDate" runat="server"/></th>
                                            <th><sc:Text id="scPaymentMethod" runat="server"/></th>
                                            <th><sc:Text id="scPaymentReason" runat="server"/></th>
                                            <th><sc:Text id="scTotalAmount" runat="server"/></th>
                                            <th style="width: 55px"><sc:Text id="scDelete" runat="server"/></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%foreach(var item in PaymentInfo.DueHistory) { %>
                                        <tr data-dueid="<%= item.OrderLineDueId %>">
                                            <td><%=item.CreatedDate.ToString("dd/MM/yyyy") %></td>
                                            <td><%=item.PaymentMethod %></td>
                                            <%--<td><a  href="/offlineduepayment?olid=<%= item.OrderLineId %>&dueid=<%= item.OrderLineDueId %>"><%=item.Reason%></a></td>--%>
                                            <td>
                                                <%if(item.CanBeDeleted) {%>
                                                <a href="/offlineduepayment?olid=<%= item.OrderLineId %>&dueid=<%= item.OrderLineDueId %>"><%=item.Reason%></a>
                                                <%} %>
                                                <%else {%>
                                                <a href="javascript:void(0);" onclick="CommissionReadOnly()"><%=item.Reason%></a>
                                                <%} %>
                                            </td>
                                            <td><span class=" cur-<%=item.CurrencyTypeId %>"><%=item.Amount.ToString("f")%></span></td>
                                            <td>
                                                <%if(item.CanBeDeleted) {%>
                                                <a class="deletedue" data-dueid="<%= item.OrderLineDueId %>" href="javascript:void(0);"><span class="glyphicon glyphicon-remove-sign"></span></a>
                                                <%} %>
                                                <%else {%>
                                                <a href="javascript:void(0);" onclick="NoDeleteClick()"><span class="glyphicon glyphicon-remove-sign"></span></a>
                                                <%} %>
                                            </td>
                                        </tr>
                                        <%} %>
                                    </tbody>
                                </table>
                            </div>
                            <%} %>
                        </div>

                        <div>
                            <%if(PaymentInfo.PaymentHistory!=null){ %>
                            <h4><sc:Text id="scListPaymentRecords" runat="server"/><%= PaymentInfo.PaymentHistory.Count %></h4>
                            <div class="table-responsive">
                                <table class="table tbl-calendar">
                                    <thead>
                                        <tr>
                                            <th><sc:Text id="scBookingDate" runat="server"/></th>
                                            <th><sc:Text id="scBookingPaymentMethod" runat="server"/></th>
                                            <th><sc:Text id="scBookingPaymentReason" runat="server"/></th>     
                                            <th><sc:Text id="scBookingTotalAmount" runat="server"/></th>
                                            <th><sc:Text id="scRealExID" runat="server"/></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%foreach(var item in PaymentInfo.PaymentHistory) {%>
                                        <tr>
                                            <td><%=item.CreatedDate.ToString("dd/MM/yyyy") %></td>
                                            <td><a href="javascript:void(0);" onclick="ShowPaymentMethod('<%=(string.IsNullOrEmpty(item.ReferenceId)||item.ReferenceId=="")?item.PaymentMethod:item.ReferenceId %>')"><%=(string.IsNullOrEmpty(item.ReferenceId)||item.ReferenceId=="")?item.PaymentMethod:item.ReferenceId %></a></td>
                                            <td><%=item.PaymentReason %></td>
                                            <td><span class=" cur-<%=item.CurrencyTypeId %>"><%=item.Amount.ToString("f")%></span></td>
                                            <td><%=item.ReferenceId %></td>
                                        </tr>
                                        <%} %>
                                    </tbody>
                                </table>
                            </div>
                            <%} %>
                        </div>

                        <div>
                            <%if(PaymentInfo.DiscountHistory!=null){ %>
                            <h4><sc:Text id="scListOfDiscount" runat="server"/><%= PaymentInfo.DiscountHistory.Count %></h4>
                            <div class="table-responsive">
                                <table class="table tbl-calendar">
                                    <thead>
                                        <tr>
                                            <th><sc:Text id="scDescription" runat="server"/></th>
                                            <th><sc:Text id="scDiscAmount" runat="server"/></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%foreach(var item in PaymentInfo.DiscountHistory) {%>
                                        <tr>
                                            <td><%=item.DiscountName %></td>
                                            <td><span class="cur-<%=item.CurrencyTypeId %>"><%=item.Amount.ToString("f")%></span></td>
                                        </tr>
                                        <%} %>
                                    </tbody>
                                </table>
                            </div>
                            <%} %>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%} %>
<script type="text/javascript">
    $(document).ready(function () {
        $("#paymenthistory a.deletedue").click(function () {
            var dueid = $(this).data("dueid")
            $("#dialog-confirm").html("<div>Please provide the delete reason</div><textarea name='duedeletereason' rows='4' cols='23'></textarea>");
            $("#dialog-confirm").dialog({
                title: "Do you want to delete this due?",
                resizable: false,
                height: 260,
                modal: true,
                close: function () {
                    $("#dialog-confirm").html("");
                },
                buttons: {
                    "Yes": function () {
                        CS.PaymentService.DeleteDue(dueid, $("textarea[name='duedeletereason']").val());
                        $(this).dialog("close");
                    },
                    "No": function () {
                        $(this).dialog("close");
                    }
                }
            });
        });

        if ($(".disableAddDueButton").val() == "true") {
            $(".btnAddDue").css("display", "none");
        }
    });
    function NoPayClick() {
        CS.Common.ShowAlert("There is nothing payable/refundable for this transaction!", "");
    }
    function NoDeleteClick() {
        CS.Common.ShowAlert("This due cannot be deleted", "");
    }
    function CommissionReadOnly() {
        CS.Common.ShowAlert("This Due is Read-only", "");
    }
    function ShowPaymentMethod(method) {
        CS.Common.ShowAlert(method, "");
    }
</script>
