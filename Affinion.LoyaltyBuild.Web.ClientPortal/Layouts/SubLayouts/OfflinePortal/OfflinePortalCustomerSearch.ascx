﻿<%@ control language="C#" autoeventwireup="true" codebehind="OfflinePortalCustomerSearch.ascx.cs" inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalCustomerSearchNew" %>
<%@ register tagprefix="sc" namespace="Sitecore.Web.UI.WebControls" assembly="Sitecore.Kernel" %>

<script type="text/javascript">

    jQuery(document).ready(function () {
        //Timer for error messages 
        jQuery('.msg-customer').delay(10000).fadeOut();

    });

    //Change Subscribe CSS Class
    function ChangeSubscribeCss() {

        jQuery("#divSubscribe").addClass("col-sm-12");
    }

    function FocusSearchResultGrid() {
        document.getElementById('data-grid').scrollIntoView();
    }
    //Edit form 
    function ExpanEditForm() {
        jQuery("#collapseCustomerEdit").addClass("panel-collapse collapse in");
        jQuery("#collapseCustomerEditAtag").removeClass();
        jQuery("#collapseCustomerEditAtag").addClass("show-hide-list");

    }

    //Subscribe
    function ExpanSubscribForm() {
        jQuery("#collapseCustomerSubscribe").addClass("panel-collapse collapse in");
        jQuery("#collapseCustomerSubscribeAtag").removeClass();
        jQuery("#collapseCustomerSubscribeAtag").addClass("show-hide-list");
    }

</script>

<asp:panel id="pnlInvalidProduct" runat="server" visible="false">
    <asp:Label ID="invalidProductErrorMessage" runat="server" ForeColor="Red">One or more product in your basket is not available anymore. Please review your basket.</asp:Label>
</asp:panel>

<div class="container booking-detail-info" runat="server" id="customerSearchWrapper">

    <div class="content">

        <%--Add Customer--%>
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <%--Client Addition--%><%--<a id="collapseCustomerEditAtag" data-toggle="collapse" data-parent="#accordion" href="#collapseCustomerEdit" class="show-hide-list collapsed">--%>
                            <sc:text field="ManageTitle" runat="server" />
                            <%--      </a>--%>
                        </h4>
                    </div>

                    <div class="panel-body alert-info">

                        <div class="col-sm-12 no-padding-m">
                            <div class="content_bg_inner_box alert-info">
                                <div class="col-sm-6 no-padding-m">
                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <%-- Select Client:--%>
                                                <sc:text field="Client" runat="server" />
                                                <span class="accent58-f">*
                                                    <asp:requiredfieldvalidator id="RequiredFieldValidator8" Display="Dynamic" ForeColor="Red" Font-Name="verdana" runat="server" validationgroup="Edit" controltovalidate="ClientDropDown"></asp:requiredfieldvalidator>
                                                </span>
                                            </span>
                                        </div>
                                        <div class="col-sm-7">
                                            <asp:dropdownlist id="ClientDropDown" runat="server" cssclass="form-control font-size-12" autopostback="True" onselectedindexchanged="ClientDropDown_SelectedIndexChanged"></asp:dropdownlist>
                                        </div>
                                    </div>
                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <asp:hiddenfield id="CustomerId" runat="server" value="0" />

                                                <sc:text field="CustomerTitle" runat="server" />
                                            </span>
                                        </div>
                                        <div class="col-sm-7">
                                            <asp:dropdownlist id="TitleDropDown" runat="server" cssclass="form-control font-size-12"></asp:dropdownlist>
                                        </div>
                                    </div>
                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <%--       First Name:--%>
                                                <sc:text field="FirstName" runat="server" />
                                                <span class="accent58-f">*
                                                        <asp:requiredfieldvalidator id="FirstNameRequiredFieldValidatorEdit" validationgroup="Edit" runat="server" Display="Dynamic" ForeColor="Red" Font-Name="verdana" controltovalidate="FirstName"></asp:requiredfieldvalidator>
                                                    <asp:requiredfieldvalidator id="FirstNameRequiredFieldValidatorSearch" validationgroup="Search" runat="server" Display="Dynamic" ForeColor="Red" Font-Name="verdana" controltovalidate="FirstName"></asp:requiredfieldvalidator>
                                                </span></span>
                                        </div>
                                        <div class="col-sm-7">
                                            <asp:textbox id="FirstName" class="form-control font-size-12" placeholder="First Name" runat="server"> </asp:textbox>
                                        </div>
                                    </div>
                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <%--          Last Name:--%>
                                                <sc:text field="LastName" runat="server" />
                                                <span class="accent58-f">*
                                                        <asp:requiredfieldvalidator id="LastNameRequiredFieldValidatorEdit" runat="server" validationgroup="Edit" Display="Dynamic" ForeColor="Red" Font-Name="verdana" controltovalidate="LastName"></asp:requiredfieldvalidator>
                                                    <asp:requiredfieldvalidator id="LastNameRequiredFieldValidatorSearch" validationgroup="Search" runat="server" Display="Dynamic" ForeColor="Red" Font-Name="verdana" controltovalidate="LastName"></asp:requiredfieldvalidator>
                                                </span></span>
                                        </div>
                                        <div class="col-sm-7">
                                            <asp:textbox id="LastName" class="form-control font-size-12" placeholder="Last Name" runat="server"> </asp:textbox>
                                        </div>
                                    </div>
                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <%-- Address Line1:--%>
                                                <sc:text field="AddressLine1" runat="server" />
                                                <span class="accent58-f">*
                                                        <asp:requiredfieldvalidator id="AddressLine1RequiredFieldValidator3" validationgroup="Edit" runat="server" Display="Dynamic" ForeColor="Red" Font-Name="verdana" controltovalidate="AddressLine1"></asp:requiredfieldvalidator>
                                                </span></span>
                                        </div>
                                        <div class="col-sm-7">
                                            <asp:textbox id="AddressLine1" validationgroup="Edit" class="form-control font-size-12" placeholder="Address Line 1" runat="server"> </asp:textbox>
                                        </div>
                                    </div>
                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <%-- Address Line2:--%>
                                                <sc:text field="AddressLine2" runat="server" />
                                            </span>
                                        </div>
                                        <div class="col-sm-7">
                                            <asp:textbox id="AddressLine2" class="form-control font-size-12" placeholder="Address Line 2" runat="server"> </asp:textbox>
                                        </div>
                                    </div>
                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <%--Address Line3:--%>
                                                <sc:text field="AddressLine3" runat="server" />
                                            </span>
                                        </div>
                                        <div class="col-sm-7">
                                            <asp:textbox id="AddressLine3" class="form-control font-size-12" placeholder="Address Line 3" runat="server"> </asp:textbox>
                                        </div>
                                    </div>
                                    <%--                                                <div class="row margin-row">
                                                    <div class="col-sm-5">
                                                        <span class="bask-form-titl font-size-12">
                                                            <%--Address Line3:
                                                            <sc:Text Field="AddressLine4" runat="server" />
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-7">
                                                        <asp:TextBox ID="AddressLine4" class="form-control font-size-12" placeholder="Address Line4" runat="server"> </asp:TextBox>
                                                    </div>
                                                </div>--%>
                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <%-- Status:--%>
                                                <sc:text field="Active" runat="server" />
                                            </span>
                                        </div>
                                        <div class="col-sm-7">
                                            <asp:checkbox id="ActiveCheckBox" validationgroup="Edit" runat="server" checked="true" />
                                            <%--<asp:DropDownList ID="StatusDropDownList" ValidationGroup="Edit" runat="server" CssClass="form-control font-size-12"></asp:DropDownList>--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 no-padding-m">
                                    <%--<div class="row margin-row">
                                                    <div class="col-sm-5">
                                                        <span class="bask-form-titl font-size-12">
                                                            <%-- Select Client:
                                                            <sc:Text Field="Client" runat="server" />
                                                            <span class="accent58-f">*
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ValidationGroup="Edit" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="ClientDropDown"></asp:RequiredFieldValidator>
                                                            </span>
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-7">
                                                        <asp:DropDownList ID="ClientDropDown" runat="server" CssClass="form-control font-size-12"></asp:DropDownList>
                                                    </div>
                                                </div>--%>
                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <%--Country:--%>
                                                <sc:text field="Country" runat="server" />
                                                <span class="accent58-f">*
                                                    <asp:requiredfieldvalidator id="CountryRequiredFieldValidator" runat="server" validationgroup="Edit" Display="Dynamic" ForeColor="Red" Font-Name="verdana" controltovalidate="CountryDropDown"></asp:requiredfieldvalidator>
                                                </span>

                                            </span>
                                        </div>
                                        <div class="col-sm-7">
                                            <asp:dropdownlist id="CountryDropDown" validationgroup="Edit" runat="server" cssclass="form-control font-size-12"></asp:dropdownlist>
                                        </div>
                                    </div>
                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <%--Location:--%>
                                                <sc:text field="Location" runat="server" />
                                                <span class="accent58-f">*
                                                    <asp:requiredfieldvalidator id="LocationRequiredFieldValidator" validationgroup="Edit" runat="server" Display="Dynamic" ForeColor="Red" Font-Name="verdana" controltovalidate="LocationDropDown"></asp:requiredfieldvalidator>
                                                </span>
                                            </span>
                                        </div>
                                        <div class="col-sm-7">
                                            <asp:dropdownlist id="LocationDropDown" validationgroup="Edit" runat="server" cssclass="form-control font-size-12"></asp:dropdownlist>
                                        </div>
                                    </div>
                                    <%--                                                <div class="row margin-row">
                                                    <div class="col-sm-5">
                                                        <span class="bask-form-titl font-size-12">
                                                            <%-- Language:
                                                            <sc:Text Field="Language" runat="server" />
                                                            <span class="accent58-f">*
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ValidationGroup="Edit" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="LanguageDropDown"></asp:RequiredFieldValidator>
                                                            </span>
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-7">
                                                        <asp:DropDownList ID="LanguageDropDown" ValidationGroup="Edit" runat="server" CssClass="form-control font-size-12"></asp:DropDownList>

                                                    </div>
                                                </div>--%>

                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <%--Primary Phone:--%>
                                                <sc:text field="PrimaryPhone" runat="server" />
                                                <span class="accent58-f">*      
                                                                 <asp:requiredfieldvalidator id="PrimaryPhoneRequiredFieldValidator" Display="Dynamic" ForeColor="Red" Font-Name="verdana" runat="server" validationgroup="Edit"  controltovalidate="PrimaryPhone"></asp:requiredfieldvalidator>
                                                </span>
                                                <span class="accent58-f display-block-val">
                                                    <asp:regularexpressionvalidator id="RegularExpressionValidatorPhone" Display="Dynamic" ForeColor="Red" Font-Name="verdana" runat="server" validationgroup="Edit"  controltovalidate="PrimaryPhone"></asp:regularexpressionvalidator>
                                                </span>
                                            </span>

                                        </div>
                                        <div class="col-sm-7">
                                            <asp:textbox id="PrimaryPhone" validationgroup="Edit" class="form-control font-size-12" placeholder="Phone" runat="server"></asp:textbox>
                                        </div>
                                    </div>
                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <%--Secondary Phone:--%>
                                                <sc:text field="SecondaryPhone" runat="server" />
                                                <span class="accent58-f display-block-val">
                                                    <asp:regularexpressionvalidator id="RegularExpressionValidatorSecondaryPhone" runat="server" validationgroup="Edit" errormessage="Invalid Entry" controltovalidate="SecondaryPhone"></asp:regularexpressionvalidator>
                                                </span>
                                            </span>
                                        </div>
                                        <div class="col-sm-7">
                                            <asp:textbox id="SecondaryPhone" class="form-control font-size-12" placeholder="Phone" runat="server"> </asp:textbox>
                                        </div>
                                    </div>
                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <sc:text field="Store" runat="server" />
                                                <%--<span class="accent58-f">*      
                                                   <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ValidationGroup="Edit" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="StoreDropDown"></asp:RequiredFieldValidator>
                                                </span>--%>
                                            </span>
                                        </div>
                                        <div class="col-sm-7">
                                            <asp:dropdownlist id="StoreDropDown" validationgroup="Edit" runat="server" cssclass="form-control font-size-12" enabled="False"></asp:dropdownlist>
                                        </div>
                                    </div>
                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <%--Email:--%>
                                                <sc:text field="Email" runat="server" />
                                                <span class="accent58-f">*      
                                                   <asp:requiredfieldvalidator id="EmailRequiredFieldValidator" runat="server" validationgroup="Edit" Display="Dynamic" ForeColor="Red" Font-Name="verdana" controltovalidate="Email"></asp:requiredfieldvalidator>
                                                </span>
                                                <span class="accent58-f">
                                                    <asp:regularexpressionvalidator id="RegularExpressionValidatorEmail" runat="server" validationgroup="Edit" Display="Dynamic" ForeColor="Red" Font-Name="verdana" controltovalidate="Email"></asp:regularexpressionvalidator>
                                                </span>
                                            </span>
                                        </div>
                                        <div class="col-sm-7">
                                            <asp:textbox id="Email" class="form-control font-size-12" placeholder="Email" runat="server" validationgroup="Edit" ontextchanged="Email_TextChanged" autopostback="True"></asp:textbox>
                                        </div>
                                    </div>

                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <%--Language:--%>
                                                <sc:text field="Language" runat="server" />
                                            </span>
                                        </div>
                                        <div class="col-sm-7">
                                            <asp:dropdownlist id="LanguageDropDown" validationgroup="Edit" runat="server" cssclass="form-control font-size-12"></asp:dropdownlist>
                                        </div>
                                    </div>
                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <%-- Subscribe--%>
                                                <sc:text field="Subscribe" runat="server" />
                                            </span>
                                        </div>
                                        <div class="col-sm-7">
                                            <asp:checkbox id="SubscribeCheckBox" validationgroup="Edit" runat="server" enabled="false" />
                                        </div>
                                    </div>
                                    <div class="row margin-row">
                                      
                                        <div class="col-sm-3">
                                            <asp:button id="EditAndSave" runat="server" enabled="false" text="EditAndSave" validationgroup="Edit" class="btn btn-default add-break pull-right width-full font-size-12" onclick="EditAndSave_Click" />
                                           <%--<button id="EditAndSave" runat="server" OnServerclick="EditAndSave_Click" class="btn btn-default add-break pull-right width-full font-size-12"  validationgroup="Edit" enabled="false" >--%>
                                               <%--<sc:text id="EditAndSaveText"  runat="server"/>--%>
                                            <%--</button>--%>
                                            
                                        </div>
                                        <div class="col-sm-3">
                                            <asp:button id="AddAndSave" runat="server" text="AddAndSave" validationgroup="Edit" class="btn btn-default add-break pull-right width-full font-size-12" onclick="AddAndSave_Click" />
                                        </div>
                                        <div class="col-sm-3">
                                            <asp:button id="Search" validationgroup="Search" runat="server" text="Search" class="btn btn-default add-break pull-right width-full search-btn" onclick="Search_Click" />
                                        </div>
                                        <div class="col-sm-3">
                                            <asp:button id="Reset" runat="server" text="Reset" class="btn btn-default add-break pull-right width-full font-size-12" onclick="Reset_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>

        <%--Messages--%>
        <div id="errormessage" class="alert alert-danger msg-customer" runat="server" visible="false" align="center">
            <b>
                <asp:literal id="ErrorMessageLiteral" runat="server" text="No Result Found" />
            </b>
        </div>
        <div class="alert alert-success msg-customer" runat="server" id="successmessage" visible="false" align="center">
            <b>
                <asp:literal id="SuccessLiteral" runat="server" text="New Customer Added Successfully" />
            </b>
        </div>
        <%-- End Messages--%>
        <%-- Result Grid--%>
        <div runat="server" id="ResultDiv" visible="false">

            <%-- <div class="row">--%><%-- <div class="col-sm-12">--%><%--<div class="panel panel-default"> --%><%--      <div class="panel-heading">
                                    <h4 class="panel-title">Search Detail</h4>
                                </div>--%><%--            <div class="panel-body alert-info">--%>
            <div id="data-grid" class="table-responsive">
                <asp:gridview id="ResultGridView" runat="server" onselectedindexchanged="ResultGridView_SelectedIndexChanged" cssclass="table tbl-calendar" onrowediting="ResultGridView_RowEditing">
                    <Columns>
                        <%--  <asp:BoundField DataField="CustomerId" />
                        <asp:BoundField DataField="FullName" HeaderText="Customer" />
                        <asp:BoundField DataField="FullAddress" HeaderText="Address" />
                        <asp:BoundField DataField="EmailAddress" HeaderText="Email Address" />
                        <asp:BoundField DataField="MobilePhoneNumber" HeaderText="Mobile Phone No" />--%>
                        <%-- <asp:CommandField ShowSelectButton="True" SelectText="<i class='glyphicon glyphicon-edit icons-positionRight'></i>" />--%>
                    </Columns>
                </asp:gridview>
            </div>
            <%-- </div>--%><%-- </div>--%>            <%-- </div>--%><%-- </div>--%><%--   </div>--%><%--  End Result Grid--%>
        </div>
        <%--Subscription--%>
        <div class="row">
            <div class="col-sm-12">
                <div class="panel-group" id="accordionSubscription">
                    <div class="panel panel-default">
                        <div class="panel-heading bg-primary">
                            <h4 class="panel-title">
                                <a id="collapseCustomerSubscribeAtag" data-toggle="collapse" data-parent="#accordionSubscription" href="#collapseCustomerSubscribe" class="show-hide-list collapsed">

                                    <sc:text field="SubscribeTitle" runat="server" />
                                </a>
                            </h4>
                        </div>
                        <div id="collapseCustomerSubscribe" class="panel-collapse collapse">
                            <div class="panel-body alert-info">
                                <div class="col-sm-12 no-padding-m">
                                    <div class="content_bg_inner_box alert-info">
                                        <div class="col-sm-6 no-padding-m">

                                            <div class="row margin-row">
                                                <div class="col-sm-5">
                                                    <span class="bask-form-titl font-size-12">

                                                        <sc:text field="Client" runat="server" />
                                                        <span class="accent58-f">*
                                                    <asp:requiredfieldvalidator id="RequiredFieldValidatorClientSubscribe" runat="server" validationgroup="Subscribe" ForeColor="Red" Font-Name="verdana" controltovalidate="DropDownClientListSubscribe"></asp:requiredfieldvalidator>
                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ValidationGroup="Unsubscribe" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="DropDownClientListSubscribe"></asp:RequiredFieldValidator>--%>
                                                        </span>
                                                    </span>
                                                </div>
                                                <div class="col-sm-7">
                                                    <asp:dropdownlist id="DropDownClientListSubscribe" runat="server" cssclass="form-control font-size-12"></asp:dropdownlist>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 no-padding-m">
                                            <div class="row margin-row">
                                                <div class="col-sm-5">
                                                    <span class="bask-form-titl font-size-12">

                                                        <sc:text field="Email" runat="server" />
                                                        <span class="accent58-f">*
                                                                <asp:requiredfieldvalidator id="RequiredFieldValidator10" runat="server" validationgroup="Subscribe" ForeColor="Red" Font-Name="verdana"  controltovalidate="EmailSubscribe"></asp:requiredfieldvalidator>
                                                            <asp:regularexpressionvalidator id="RegularExpressionValidatorEmailSubscription" runat="server" validationgroup="Subscribe" ForeColor="Red" Font-Name="verdana" controltovalidate="EmailSubscribe"></asp:regularexpressionvalidator>
                                                            <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ValidationGroup="Unsubscribe" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="EmailSubscribe"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorEmailUnsubscription" runat="server" ValidationGroup="Unsubscribe" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="EmailSubscribe"></asp:RegularExpressionValidator>--%>
                                                        </span>
                                                        <span class="accent58-f display-block-val">
                                                            <asp:literal id="SubscribeMessage" runat="server" text="" />
                                                        </span>
                                                    </span>
                                                </div>
                                                <div class="col-sm-7">

                                                    <asp:textbox id="EmailSubscribe" class="form-control font-size-12" placeholder="Email" runat="server" validationgroup="Subscribe"></asp:textbox>

                                                </div>
                                            </div>

                                            <div class="row margin-row">
                                                <div class="col-sm-4">
                                                    <asp:button id="ButtonSubscribe" runat="server" text="" validationgroup="Subscribe" class="btn btn-default add-break pull-right width-full" onclick="Subscribe_Click" />
                                                </div>
                                                <div class="col-sm-4">
                                                    <asp:button id="UnSubscribe" runat="server" text="" validationgroup="Subscribe" class="btn btn-default add-break pull-right width-full" onclick="UnSubscribe_Click" />
                                                </div>
                                                <div class="col-sm-4">
                                                    <asp:button id="SubscribeReset" runat="server" text="" class="btn btn-default add-break pull-right width-full" onclick="SubscribeReset_Click" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<asp:panel cssclass="container booking-detail-info" runat="server" id="PanelPayment" visible="false">
    <div class="content">
        <div class="panel-group" id="accordionSubscription">
            <div class="panel panel-default">
                <div class="panel-heading bg-primary">
                    <h4 class="panel-title">Payment Gateway</h4>
                </div>
                <div class="panel-body alert-info">
                    <asp:Button ID="btnPaymentGateway" CssClass="btn btn-default add-break pull-right font-size-12" runat="server" Text="Continue to Payment Gateway" OnClick="btnPaymentGateway_Click"/>
                </div>
            </div>
        </div>
    </div>
</asp:panel>
