﻿using Affinion.LoyaltyBuild.UCom.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.Api;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Infrastructure.Globalization;
using UCommerce.Pipelines;
using UCommerce.Pipelines.AddToBasket;
using UCommerce.Pipelines.GetProduct;
using UCommerce.Pipelines.UpdateLineItem;
using UCommerce.Runtime;
using UCommerce.Transactions;
using UCommerce.Transactions.Payments;
using UCommerce.Xslt;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi
{
    public static class LoyaltyBuildTransactionLibrary
    {
        private static LoyaltyBuildTransactionLibraryInternal LoyaltyBuildTransactionLibraryInternal
        {
            get
            {
                return ObjectFactory.Instance.Resolve<LoyaltyBuildTransactionLibraryInternal>();
            }
        }





        public static void AddToBasket(int quantity, string sku, string variantSku = null, bool addToExistingLine = true, bool executeBasketPipeline = true, int? catalogId = new int?())
        {
            string catalogName = catalogId.HasValue ? catalogId.ToString() : string.Empty;
            LoyaltyBuildTransactionLibraryInternal.AddToBasket(catalogName, quantity, sku, variantSku, addToExistingLine, executeBasketPipeline);
            //   return response2.OrderLine;
        }

        public static OrderLine GetRecentlyAddedOrderLineBySku(string productSku, int orderLineId = 0)
        {
            var basket = SiteContext.Current.OrderContext.GetBasket();
            if (basket == null)
                return null;
            return basket.PurchaseOrder.OrderLines.LastOrDefault(x => x.Sku == productSku && x.OrderLineId == (orderLineId != 0 ? orderLineId : x.OrderLineId));

        }

        public static OrderLine FindExistingOrderLine(string productSku, List<string> orderproperty)
        {
            var basket = SiteContext.Current.OrderContext.GetBasket();
            if (basket == null)
                return null;
            List<OrderLine> orderLineList = basket.PurchaseOrder.OrderLines.ToList();
            return orderLineList.FirstOrDefault(x => x.Sku.Equals(productSku, StringComparison.InvariantCultureIgnoreCase) && (orderproperty.Count == 0 || x.OrderProperties.All(y => orderproperty.Any(z => z.Equals(y.Key)))));
        }

        /// <summary>
        /// Product SKU
        /// </summary>
        /// <param name="productSku"></param>
        /// <param name="orderLineId">Pass 0 to add Recently AddedOrderLine</param>
        public static void AppliedCampaignOnOrderLine(string productSku, int campaignItemId, int orderLineId = 0)
        {
            OrderLine line = GetRecentlyAddedOrderLineBySku(productSku, orderLineId);
            line[OrderPropertyConstants.UserSelectedCampaign] = campaignItemId.ToString();
        }

        /// <summary>
        /// Product SKU
        /// </summary>
        /// <param name="productSku"></param>
        /// <param name="orderLineId">Pass 0 to add Recently AddedOrderLine</param>
        public static void AppliedAwardOnOrderLine(string productSku, string awardType, int orderLineId = 0)
        {
            OrderLine line = GetRecentlyAddedOrderLineBySku(productSku, orderLineId);
            line[OrderPropertyConstants.AppliedAward] = awardType;
        }

        public static void AddAddOnProductToOrderLine(string productSku, int? catalogId, List<Product> addonproduct, SearchCriteria criteria, int orderLineId = 0)
        {
            OrderLine line = GetRecentlyAddedOrderLineBySku(productSku, orderLineId);
            if (line == null)
            {
                throw new Exception("Invalid Order Line");
            }
            List<OrderProperty> orderProperty = line.OrderProperties.ToList();
            if (criteria != null)
            {
                foreach (var item in criteria.CreateOrderLineProperty())
                {
                    int key = orderProperty.FindIndex(x => x.Key == item.Key);
                    if (key == -1)
                    {
                        line.AddOrderProperty(new OrderProperty() { Key = item.Key, Value = item.Value });
                    }
                    else
                    {
                        line.OrderProperties.FirstOrDefault(x => x.Key == item.Key).Value = item.Value;
                    }
                }
            }
            OrderProperty searchProperty = line.OrderProperties.FirstOrDefault(x => x.Key.Equals(OrderPropertyConstants.ForSearch));
            if (searchProperty != null)
            {
                line.OrderProperties.Remove(searchProperty);
            }
            line.Save();
            if (addonproduct != null && addonproduct.Any())
            {
                foreach (var item in addonproduct)
                {
                    List<ProductProperty> productProperty = item.ProductProperties.Where(x => !x.ProductDefinitionField.Deleted && x.ProductDefinitionField.Name.Equals("ProductType", StringComparison.InvariantCultureIgnoreCase)).ToList();
                    if (productProperty != null && productProperty.Any())
                    {
                        foreach (var proerty in productProperty)
                        {
                            line.AddOrderProperty(new OrderProperty() { Key = proerty.Value, Value = item.Sku.ToString() });
                        }
                    }
                }
                line.Save();
                orderLineId = line.OrderLineId;
                foreach (var item in addonproduct)
                {
                    TransactionLibrary.AddToBasket(1, item.Sku, item.VariantSku, null, null, false, false);
                    //TransactionLibrary.AddToBasket(1, item.Sku, item.VariantSku, false, false, catalogId);
                    OrderLine addOnline = GetRecentlyAddedOrderLineBySku(item.Sku);
                    addOnline.AddOrderProperty(new OrderProperty() { Key = OrderPropertyConstants.RefOrderId, Value = orderLineId.ToString() });
                    addOnline.Save();
                }
            }
        }

        public static void DeleteAddOnProduct(int orderLineId)
        {
            OrderLine ordLine = OrderLine.Get(orderLineId);
            OrderProperty property = ordLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.RefOrderId);
            if (property != null)
            {
                OrderLine roomOrderLine = OrderLine.Get(Convert.ToInt32(property.Value));
                OrderProperty skuproperty = roomOrderLine.OrderProperties.FirstOrDefault(x => x.Value == ordLine.Sku);
                roomOrderLine.OrderProperties.Remove(skuproperty);
                roomOrderLine.Save();
            }
            ordLine.Delete();
        }


    }

    public class LoyaltyBuildTransactionLibraryInternal
    {
        private static TransactionLibraryInternal TransactionLibraryInternal
        {
            get
            {
                return ObjectFactory.Instance.Resolve<TransactionLibraryInternal>();
            }
        }




        private readonly IPipeline<IPipelineArgs<AddToBasketRequest, AddToBasketResponse>> _addToBasketPipeline;
        private readonly ICatalogContext _catalogContext;
        private readonly IClientContext _clientContext;
        private readonly IPaymentMethodService _defaultPaymentMethodService;
        private readonly IEmailService _emailService;
        private readonly IPipeline<IPipelineArgs<GetProductRequest, GetProductResponse>> _getProductPipeline;
        private readonly ILocalizationContext _localizationContext;
        private readonly INumberSeriesService _numberSeriesService;
        private readonly IOrderContext _orderContext;
        private readonly IOrderService _orderService;
        private readonly IPipeline<IPipelineArgs<UpdateLineItemRequest, UpdateLineItemResponse>> _updateLineItemPipeline;


        public LoyaltyBuildTransactionLibraryInternal(ILocalizationContext localizationContext, ICatalogContext catalogContext, IClientContext clientContext, IOrderContext orderContext, INumberSeriesService numberSeriesService, IOrderService orderService, IPaymentMethodService defaultPaymentMethodService, IEmailService emailService, IPipeline<IPipelineArgs<GetProductRequest, GetProductResponse>> getProductPipeline, IPipeline<IPipelineArgs<AddToBasketRequest, AddToBasketResponse>> addToBasketPipeline, IPipeline<IPipelineArgs<UpdateLineItemRequest, UpdateLineItemResponse>> updateLineItemPipeline)
        {
            this._localizationContext = localizationContext;
            this._catalogContext = catalogContext;
            this._clientContext = clientContext;
            this._orderContext = orderContext;
            this._numberSeriesService = numberSeriesService;
            this._orderService = orderService;
            this._defaultPaymentMethodService = defaultPaymentMethodService;
            this._emailService = emailService;
            this._getProductPipeline = getProductPipeline;
            this._addToBasketPipeline = addToBasketPipeline;
            this._updateLineItemPipeline = updateLineItemPipeline;
        }



        public virtual OrderLine AddToBasket(string catalogName, int quantity, string sku, string variantSku = null, bool addToExistingLine = true, bool executeBasketPipeline = true)
        {
            GetProductResponse response = new GetProductResponse();
            if (this._getProductPipeline.Execute(new GetProductPipelineArgs(new GetProductRequest(new ProductIdentifier(sku, variantSku)), response)) == PipelineExecutionResult.Error)
            {
                throw new ArgumentException(string.Format("Could not find product. {0}", sku));
            }
            Product product = response.Product;
            AddToBasketRequest request = new AddToBasketRequest(true)
            {
                PriceGroup = SiteContext.Current.CatalogContext.CurrentPriceGroup,
                Product = product,
                PurchaseOrder = TransactionLibraryInternal.GetBasket(true).PurchaseOrder,
                Quantity = quantity,
                AddToExistingOrderLine = addToExistingLine,
                ExecuteBasketPipeline = executeBasketPipeline
            };
            AddToBasketResponse response2 = new AddToBasketResponse();
            if (this._addToBasketPipeline.Execute(new AddToBasketPipelineArgs(request, response2)) == PipelineExecutionResult.Error)
            {
                throw new ArgumentException(string.Format("Product is not added. {0}", product.Sku));
            }

            return response2.OrderLine;
        }











    }
}
