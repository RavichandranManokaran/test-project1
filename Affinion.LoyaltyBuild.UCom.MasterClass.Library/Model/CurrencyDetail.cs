﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    public class CurrencyDetail
    {
        public string CurrencyGUID { get; set; }
        public string Name { get; set; }
        public int CurrencyID { get; set; }

    }
}
