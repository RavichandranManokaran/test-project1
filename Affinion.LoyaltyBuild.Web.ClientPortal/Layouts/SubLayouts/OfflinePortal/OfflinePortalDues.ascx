﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalDues.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalDues" %>

<%if(ValidationResponse != null){%> 
<div class="col-md-12">
    <div class="alert alert-<%= ValidationResponse.IsSuccess ? "success" : "danger" %>"> 
        <%= ValidationResponse.Message %> 
    </div>
</div> 
<%} %>
                <div class="col-md-12" id="paymentadddue">
                    <div class="panel panel-default" >
                        <div class="panel-heading bg-primary">
                            <h4 class="panel-title">
                                <sc:text id="titleTxt" runat="server"/> <%= QueryString("ref") %>
                            </h4>
                        </div>
                      <form id="AddDue" method="get" action=""> 
                        <div class="panel-body">
                            <input type="hidden" id="orderlineid" runat="server" value="" />
                            <input type="hidden" id="orderlinedueid" runat="server" value="" />
                             <!-- jayapriya start code-->
                                    <div class="row margin-row">
                                        <div class="col-sm-4"><span class="bask-form-titl font-size-12"><sc:text id="currencyTxt" runat="server"/><span class="accent58-f"> </span></span></div>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="DrpLstCurrencyId"  CssClass="form-control font-size-12" runat="server" ClientIDMode="Static">
                                               
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row margin-row">
                                        <div class="col-sm-4"><span class="bask-form-titl font-size-12"><sc:text id="paymentMethodTxt" runat="server"/><span class="accent58-f"><sc:text id="pmMandTxt" runat="server" /></span></span></div>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID ="DrpLstPaymentMethod" CssClass="form-control font-size-12" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row margin-row">
                                        <div class="col-sm-4"><span class="bask-form-titl font-size-12"><sc:text id="paymentReasonTxt" runat="server" /><span class="accent58-f"> <sc:text id="payReasonMandTxt" runat="server"/></span></span></div>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="DrpLstPaymentReason" CssClass="form-control font-size-12" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row margin-row">
                                        <div class="col-sm-4"><span class="bask-form-titl font-size-12"><sc:text id="totalAmountTxt" runat="server"/></span></div>
                                        <div class="col-sm-4">
                                            <asp:TextBox required ID="txtTotAmount"  CssClass="form-control font-size-12 numericTexbox" runat="server" ClientIDMode="Static"></asp:TextBox>
                                        </div>
                                         <div class="col-sm-4 accent58-f font-size-12">   
                                             <asp:RequiredFieldValidator  Display="Dynamic" runat="server" 
                                                 ID="FirstNameValidator" CssClass="accent58-f"  
                                                 ControlToValidate="txtTotAmount" ValidationGroup="AmountValidation" ErrorMessage="*" />
                                             <asp:RegularExpressionValidator ID="Regex1" Display="Dynamic" runat="server" ValidationExpression="((\-{0,1})?(\d+)((\.\d{1,2})?))$"
                                                ErrorMessage="Total amount must be sum of commssion,provider and processingfee" ValidationGroup="AmountValidation" 
                                                ControlToValidate="txtTotAmount" />

                                            <asp:CustomValidator id="GuestName" ValidateEmptyText="false" 
                                                ControlToValidate="txtTotAmount" ValidationGroup="AmountValidation" 
                                                ClientValidationFunction="IsValidAmount"  Display="Dynamic" ErrorMessage="Total amount must be sum of commssion,provider and processingfee"  runat="server"/>
                           
            
                                           <%-- <input type="text" id="txtTotAmount" runat="server" ClientIDMode="Static" /></div>--%>
                                       
                                            
                                             </div>
                                    </div>
                                    <div class="row margin-row">
                                        <div class="col-sm-4"><span class="bask-form-titl font-size-12"><sc:text id="commissionAmountTxt" runat="server"/></span></div>
                                        <div class="col-sm-4">
                                            <asp:TextBox required ID="txtCommissionAmount" CssClass="form-control font-size-12 numericTexbox" runat="server" ClientIDMode="Static"></asp:TextBox></div>
                                        <div class="col-sm-4 accent58-f font-size-12">  
                                             <asp:RequiredFieldValidator  Display="Dynamic" runat="server" 
                                                 ID="RequiredFieldValidator1" CssClass="accent58-f"  ErrorMessage="*"
                                                 ControlToValidate="txtCommissionAmount" ValidationGroup="AmountValidation"/>   
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="Dynamic" runat="server" ValidationExpression="((\-{0,1})?(\d+)((\.\d{1,2})?))$"
                                                ErrorMessage="Please enter commission amount"
                                                ControlToValidate="txtCommissionAmount" ValidationGroup="AmountValidation" /> 
                                         <%-- <input type="text" id="CommissionAmount" runat="server" ClientIDMode="Static" /></div>--%>
                                        </div>
                                    </div>
                                    <%--<div class="row margin-row">
                                        <div class="col-sm-5"><span class="bask-form-titl font-size-12"><sc:text id="transferAmtTxt" runat="server" /></span></div>
                                        <div class="col-sm-7">
                                            <asp:TextBox required ID="txtTransferAmount" CssClass="form-control font-size-12 numericTexbox" runat="server" ClientIDMode="Static"></asp:TextBox></div>
                                    </div>--%>
                                    <%--<div class="row margin-row">
                                        <div class="col-sm-5"><span class="bask-form-titl font-size-12"><sc:text id="cancellationAmtTxt" runat="server"/></span></div>
                                        <div class="col-sm-7">
                                            <asp:TextBox required ID="txtCancellationAmount" CssClass="form-control font-size-12 numericTexbox" runat="server" ClientIDMode="Static"></asp:TextBox></div>
                                    </div>--%>
                                    <%--<div class="row margin-row">
                                        <div class="col-sm-5"><span class="bask-form-titl font-size-12"><sc:text id="clientAmtTxt" runat="server" /></span></div>
                                        <div class="col-sm-7">
                                            <asp:TextBox required ID="txtClientAmount" CssClass="form-control font-size-12 numericTexbox" runat="server" ClientIDMode="Static"></asp:TextBox></div>
                                    </div>--%>
                                    <div class="row margin-row">
                                        <div class="col-sm-4"><span class="bask-form-titl font-size-12"><sc:text id="ProviderAmtTxt" runat="server"/></span></div>
                                        <div class="col-sm-4">
                                            <asp:TextBox required ID="txtProviderAmount" CssClass="form-control font-size-12 numericTexbox" runat="server" ClientIDMode="Static"></asp:TextBox></div>
                                        <div class="col-sm-4 accent58-f font-size-12"> 
                                            <asp:RequiredFieldValidator  Display="Dynamic" runat="server" 
                                                 ID="RequiredFieldValidator2" CssClass="accent58-f" ErrorMessage="*" 
                                                 ControlToValidate="txtProviderAmount" ValidationGroup="AmountValidation"/>  
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Display="Dynamic" runat="server" ValidationExpression="((\-{0,1})?(\d+)((\.\d{1,2})?))$"
                                                ErrorMessage="Please enter valid provider amount"
                                                ControlToValidate="txtProviderAmount" /> 
                                          <%--  <input type="text" id="ProviderAmount" runat="server" ClientIDMode="Static"/></div>--%>
                                        </div>
                                    </div>
                                    <%--<div class="row margin-row">
                                        <div class="col-sm-5"><span class="bask-form-titl font-size-12"><sc:text id="CollectionAmtTxt" runat="server"/></span></div>
                                        <div class="col-sm-7">
                                            <asp:TextBox required ID="txtCollectionAmount" CssClass="form-control font-size-12 numericTexbox" runat="server" ClientIDMode="Static"></asp:TextBox></div>
                                    </div>--%>
                            <div class="row margin-row">
                                        <div class="col-sm-4"><span class="bask-form-titl font-size-12"><sc:text id="ProcessingFeeTxt" runat="server"/></span></div>
                                        <div class="col-sm-4">
                                            <asp:TextBox required ID="txtProcessingFee" CssClass="form-control font-size-12 numericTexbox" runat="server" ClientIDMode="Static"></asp:TextBox></div>
                                        <div class="col-sm-4 accent58-f font-size-12"> 
                                             <asp:RequiredFieldValidator  Display="Dynamic" runat="server" 
                                                 ID="RequiredFieldValidator3" CssClass="accent58-f"  ErrorMessage="*"
                                                 ControlToValidate="txtProcessingFee" ValidationGroup="AmountValidation"/>  
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Display="Dynamic" runat="server" ValidationExpression="((\-{0,1})?(\d+)((\.\d{1,2})?))$"
                                                ErrorMessage="Please enter valid  processing fee"
                                                ControlToValidate="txtProcessingFee" /> 
                                            </div>
                                    </div>
                                    <div class="row margin-row">
                                        <div class="col-sm-4"><span class="bask-form-titl font-size-12"><sc:text id="NoteTxt" runat="server"/></span></div>
                                        <div class="col-sm-4">
                                            <asp:TextBox required ID="txtNote" CssClass="form-control font-size-12" runat="server" ClientIDMode="Static"></asp:TextBox></div>
                                    </div>
                                    <div class="row margin-row">`
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <button type="reset" class="btn btn-default add-break font-size-12 pull-right pad5"><span class="glyphicon glyphicon-refresh"></span><sc:text id="btnResetTxt" runat="server"/></button>
                                            <Button ID="btnAddDue" runat="server"  style="margin-right:5px" validationgroup="AmountValidation" class="btn btn-default add-break font-size-12 pull-right pad5" ClientIDMode="Static" onserverclick="btnAddDue_Click"><sc:text id="btnAddDueTxt" runat="server"/></Button>
                                           
                                            <a class="btn btn-default add-break font-size-12 pull-right pad5" style="margin-right:5px" href="<%=GetUrl("/paymenthistory?olid=" + QueryString("olid")) %>"><sc:text id="btnCancelTxt" runat="server"/></a>
                                            <%--<button class="btn btn-default add-break font-size-12 pull-right pad5" style="margin-right:5px"><span class="glyphicon glyphicon-info-sign"></span> Pricing Info</button>--%>
                                        </div>
                                    </div>
                            </form>
                        </div>

                        
                    </div>
                </div>
<script>
    // JQUERY ".Class" SELECTOR.
    $(document).ready(function () {
        $('.numericTexbox').keypress(function (event) {
            return isNumber(event, this)
        });
    });
    // THE SCRIPT THAT CHECKS IF THE KEY PRESSED IS A NUMERIC OR DECIMAL VALUE.
    function isNumber(evt, element) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (
            (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
            (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
            (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    function IsValidAmount(oSrc, args) {
        var comamt = parseFloat($('#txtCommissionAmount').val());
        var provideramt = parseFloat($('#txtProviderAmount').val());
        var processingfee = parseFloat($('#txtProcessingFee').val());
        var totamt = parseFloat($('#txtTotAmount').val());

        args.IsValid = (totamt === (comamt + provideramt + processingfee));
    }

   		</script>