﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Maps
{
    public class HotelBreakAwardMap : SubclassMap<HotelBreakAward>
    {
        // Methods
        public HotelBreakAwardMap()
        {
            base.Table("LB_HotelBreakAward");
            base.KeyColumn("HotelBreakAwardId");
            base.Map(x => x.HotelBreakAwardId).ReadOnly();
            base.Map(x => x.DiscountAmount).Not.Nullable();
            base.Map(x => x.IsPercentage).Not.Nullable();
            base.Map(x => x.OnOrderLine).Not.Nullable();
        }

    }

}
