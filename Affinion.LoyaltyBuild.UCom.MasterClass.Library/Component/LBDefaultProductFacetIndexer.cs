﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.EntitiesV2.Queries.Catalog;
using UCommerce.Infrastructure.Globalization;
using UCommerce.Infrastructure.Logging;
using UCommerce.Search;
using UCommerce.Search.Facets;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Component
{
    public class LBDefaultProductFacetIndexer : IIndexer<ProductDefinitionField>, IDeindexer<ProductDefinitionField>, IIndexer<IEnumerable<ProductDefinitionField>>, IDeindexer<IEnumerable<ProductDefinitionField>>
    {
        // Fields
        private readonly IDeindexer<IEnumerable<Facet>> _facetDeIndexer;
        private readonly IIndexer<IEnumerable<Facet>> _facetIndexer;
        private readonly ILanguageService _languageService;
        private readonly ILoggingService _loggingService;
        private readonly ISessionProvider _sessionProvider;

        // Methods
        public LBDefaultProductFacetIndexer(IIndexer<IEnumerable<Facet>> facetIndexer, IDeindexer<IEnumerable<Facet>> facetDeIndexer, ILanguageService languageService, ISessionProvider sessionProvider, ILoggingService loggingService)
        {
            this._facetIndexer = facetIndexer;
            this._facetDeIndexer = facetDeIndexer;
            this._languageService = languageService;
            this._sessionProvider = sessionProvider;
            this._loggingService = loggingService;
        }

        public void Deindex(IEnumerable<ProductDefinitionField> subjects)
        {
            List<Facet> list = new List<Facet>();
            foreach (ProductDefinitionField field in subjects)
            {
                foreach (Language language in this._languageService.GetAllLanguages())
                {
                    string displayName = field.GetDisplayName(language.CultureCode);
                    Facet item = new Facet
                    {
                        DisplayName = displayName,
                        Name = displayName,
                        CultureCode = language.CultureCode
                    };
                    list.Add(item);
                }
            }
            this._facetDeIndexer.Deindex(list);
        }

        public void Deindex(ProductDefinitionField subjects)
        {
            this.Deindex(new List<ProductDefinitionField> { subjects });
        }

        public virtual bool FacetShouldBeRemoved(Facet facet)
        {
            using (ISession session = this._sessionProvider.GetSession())
            {
                return (new ExistsFacetWithThisNameAndCultureCode(facet.CultureCode, facet.Name).Execute(session).First<ExistsFacetResult>().FacetExists == "False");
            }
        }

        public void Index(ProductDefinitionField subjects)
        {
            this.Index(new List<ProductDefinitionField> { subjects });
        }

        public void Index(IEnumerable<ProductDefinitionField> subjects)
        {
            List<Facet> list = new List<Facet>();
            List<Facet> list2 = new List<Facet>();
            foreach (ProductDefinitionField field in subjects)
            {
                foreach (Language language in this._languageService.GetAllLanguages())
                {
                    string displayName = field.GetDisplayName(language.CultureCode);
                    Facet item = new Facet
                    {
                        DisplayName = displayName,
                        Name = displayName,
                        CultureCode = language.CultureCode
                    };
                    if (field.Facet)
                    {
                        this._loggingService.Log<LBDefaultProductFacetIndexer>(string.Format("Creating facet '{0}' for culture '{1}'", item.DisplayName, item.CultureCode));
                        list.Add(item);
                    }
                    else if (this.FacetShouldBeRemoved(item))
                    {
                        this._loggingService.Log<LBDefaultProductFacetIndexer>(string.Format("Removing facet '{0}' for culture '{1}'", item.DisplayName, item.CultureCode));
                        list2.Add(item);
                    }
                    else
                    {
                        this._loggingService.Log<LBDefaultProductFacetIndexer>(string.Format("Leaving facet '{0}' for culture '{1}'", item.DisplayName, item.CultureCode));
                    }
                }
            }
            this._facetIndexer.Index(list);
            this._facetDeIndexer.Deindex(list2);
        }
    }

}
