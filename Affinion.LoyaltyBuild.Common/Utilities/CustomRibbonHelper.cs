﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           CustomRibbonHelper.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Helper Class to add commands to custom ribbon controls
/// </summary>

#region Using Directives
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Sitecore.Data.Items;
using Sitecore.SecurityModel;
using Sitecore.Shell.Framework.Commands;
using System; 
#endregion

namespace Affinion.LoyaltyBuild.Common.Utilities
{
    public class CustomRibbonHelper : Command
    {
        #region Private Constants
        //Custom message for exception occurs when decrypting
        private const string DecryptExceptionMessage = "Decryption failed for the selected item";
        //Custom message responce for exception
        private const string DecryptExceptionMessageResponce = "Decryption Error";
        //Custom message popup header
        private const string DecryptExceptionMessageHeader = "Internal Error";
        //Field name of the sitecore item
        private const string ItemFieldName = "Value";
        #endregion

        #region Public Methods
        /// <summary>
        /// Execute for the click event of the custom button
        /// </summary>
        /// <param name="context">Current command context</param>
        public override void Execute(CommandContext context)
        {
            try
            {
                //Get the currently editing sitecore item to decrypt
                Sitecore.Data.Items.Item editingItem = context.Items[0];
                //Value of the sitecore item
                string fieldValue = editingItem.Fields[ItemFieldName].Value;
                //Start editing the item
                using (new SecurityDisabler())
                {
                    editingItem.Editing.BeginEdit();
                    //If the current item is the cache, remove it from cache
                    if (Sitecore.Context.Database.Caches.ItemCache.GetItem(editingItem.ID, Sitecore.Globalization.Language.Invariant, Sitecore.Data.Version.First) != null)
                    {
                        //Remove item from the cache
                        Sitecore.Context.Database.Caches.ItemCache.RemoveItem(editingItem.ID);
                    }
                    //Decrypt current value and save
                    editingItem.Fields[ItemFieldName].Value = Cryptography.Decrypt(fieldValue);
                    //Finishing editing and save
                    editingItem.Editing.EndEdit();
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                //Show custom Error message 
                Sitecore.Context.ClientPage.ClientResponse.Alert(DecryptExceptionMessage, DecryptExceptionMessageResponce, DecryptExceptionMessageHeader);
            }
        }

        /// <summary>
        /// Set the state of the custom ribbon control: Enabled, Disabled or Checked (if a check box)
        /// </summary>
        /// <param name="context">Current command context</param>
        /// <returns>Returns the state of the control</returns>
        public override CommandState QueryState(CommandContext context)
        {
            try
            {
                //Get the currently editing sitecore item to decrypt
                Item item = context.Items[0];
                //If the item is not in the cache(means already decrypted and need to disable the button), disable the button
                if (Sitecore.Context.Database.Caches.ItemCache.GetItem(item.ID, Sitecore.Globalization.Language.Invariant, Sitecore.Data.Version.First) == null)
                {
                    //Disable the button
                    return CommandState.Disabled;
                }
                //Enable the button
                return CommandState.Enabled;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                //Disable the button if any error occured
                return CommandState.Disabled;
            }
        } 
        #endregion
    }
}
