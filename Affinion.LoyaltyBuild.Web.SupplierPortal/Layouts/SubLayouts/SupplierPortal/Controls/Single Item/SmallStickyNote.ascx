﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SmallStickyNote.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Single_Item.SmallStickyNote" %>

<asp:Panel runat="server" ID="pnlStickyNote">
 <h3><sc:FieldRenderer ID="frTitle" runat="server" FieldName="Title" /></h3>
 <sc:FieldRenderer ID="frBody" runat="server" FieldName="Message" />
</asp:Panel>
