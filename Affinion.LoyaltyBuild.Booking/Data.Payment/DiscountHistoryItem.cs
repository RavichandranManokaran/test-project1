﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/
namespace Affinion.LoyaltyBuild.Api.Booking.Data.Payment
{
    /// <summary>
    /// Entity related to DiscountHistory Item
    /// </summary>
    public class DiscountHistoryItem
    {
        /// <summary>
        /// Discout name
        /// </summary>
        public string DiscountName { get; set; }
        /// <summary>
        /// Discount Amount
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// Currency typeid
        /// </summary>
        public int CurrencyTypeId { get; set; }
    }
}
