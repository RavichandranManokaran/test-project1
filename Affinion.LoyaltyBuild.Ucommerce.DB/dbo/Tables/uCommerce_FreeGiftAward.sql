﻿CREATE TABLE [dbo].[uCommerce_FreeGiftAward] (
    [FreeGiftAwardId] INT            NOT NULL,
    [Sku]             NVARCHAR (MAX) NOT NULL,
    [VariantSku]      NVARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([FreeGiftAwardId] ASC)
);

