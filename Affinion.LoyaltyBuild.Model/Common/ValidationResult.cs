﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Model.Common
{
    public class ValidationResult
    {
        /// <summary>
        /// Response id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// IsSuccess flag
        /// </summary>
        public bool IsSuccess { get; set; }

        /// <summary>
        /// Message
        /// </summary>
        public string Message { get; set; }
    }
}
