﻿#region Using Directives
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
#endregion

namespace Affinion.LoyaltyBuild.SupplierSetup.PollingManager
{
    /// <summary>
    /// Generate Json files to store the MAC address/Profile name of the voted users
    /// </summary>
    public class JsonCreator
    {
        #region GenerateJsonObject region
        /// <summary>
        /// Generate Json object
        /// </summary>
        /// <param name="itemId">Get the Item ID</param>
        /// <param name="info">Mac address or Profile name</param>
        /// <returns>return the Json Object as string</returns>
        public string GenerateJsonObject(string previousJson, string itemId, string info)
        {
            try
            {
                if (!string.IsNullOrEmpty(itemId) && !string.IsNullOrEmpty(info))
                {
                    Collection<UserDetails> previosDetails = new Collection<UserDetails>();
                    previosDetails = ReadJsonObject(previousJson);

                    previosDetails.Add(new UserDetails()
                    {
                        itemID = itemId,
                        userInfo = info
                    });

                    string json = JsonConvert.SerializeObject(previosDetails, Formatting.Indented);
                    return json;
                }
                else
                {
                    //create a list to store User details
                    List<UserDetails> userDetails = new List<UserDetails>();

                    //Add user details to Json
                    userDetails.Add(new UserDetails()
                    {
                        itemID = itemId,
                        userInfo = info
                    });

                    string json = JsonConvert.SerializeObject(userDetails, Formatting.Indented);
                    return json;
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.SupplierSetup, ex, this);
                throw;
            }
        }
        #endregion

        #region ReadJsonObject region
        /// <summary>
        /// Read the Json data
        /// </summary>
        /// <param name="jsonString">Json data to convert</param>
        /// <returns>Converted Json object as a list of PollingManager_UserDetails type</returns>
        public static Collection<UserDetails> ReadJsonObject(string jsonString)
        {
            //deserialize the jsonn object
            Collection<UserDetails> items = JsonConvert.DeserializeObject<Collection<UserDetails>>(jsonString);
            //PollingManager_UserDetails myDeserializedObj = (PollingManager_UserDetails)JsonConvert.DeserializeObject(jsonString, typeof(PollingManager_UserDetails));
            return items;
        }
        #endregion
    }
}