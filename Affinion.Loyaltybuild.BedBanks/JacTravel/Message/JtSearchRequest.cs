﻿using System.Xml.Serialization;
using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Diagnostics;
using Affinion.LoyaltyBuild.BedBanks.Troika;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Message.Interfaces;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Model;
using System.Linq;
using Affinion.LoyaltyBuild.BedBanks.General;
using Sitecore.Data.Items;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Model;
using System.Collections.Specialized;

namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Message
{
    /// <summary>
    /// This is a class to hold the search Request Parameters.
    /// It will be passed to the API controller to perform the actual request.
    /// </summary>
    [Serializable]
    [XmlType(TypeName = "SearchRequest")]
    public class JtSearchRequest : JtRequest, IJtRequest
    {
        public JtSearchDetails SearchDetails { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        BedBanksSettings bbs;
        //Parameterless Constructor - Needed for XML Serialization
        private JtSearchRequest() { }

        //The real constructor
        public JtSearchRequest(string pRegionId, string pArrivalDate, string pDuration, string pRoomInfo, BedBanksSettings bbs,string minStarRating=null, List<string> propRefIds = null)
        {
            if (!string.IsNullOrEmpty(pRegionId) && !string.IsNullOrEmpty(pDuration) && !string.IsNullOrEmpty(pArrivalDate) && bbs != null)
            {
                this.bbs = bbs;
                JtSearchDetails searchDetails = new JtSearchDetails
                {
                    ArrivalDate = pArrivalDate,
                    Duration = pDuration,
                    RegionID = pRegionId,
                    MealBasisID = "0",
                    MinStarRating = (!string.IsNullOrWhiteSpace(minStarRating))?minStarRating:bbs.BedBanksMinimumStarRating,// Rating is 0 for PropertyReference Search
                };

                List<JtSearchRoomRequest> roomRequests = new List<JtSearchRoomRequest>();
                List<JtPropertyReferenceId> propReferenceId = new List<JtPropertyReferenceId>();
                if (propRefIds != null)
                {
                    foreach (string propRefId in propRefIds)
                    {
                        if (propRefId != null)
                        {
                            JtPropertyReferenceId refToAdd = new JtPropertyReferenceId();
                            refToAdd.PropertyReferenceID = propRefId;
                            propReferenceId.Add(refToAdd);
                        }
                    }

                    searchDetails.PropertyReferenceIDs = propReferenceId;
                }
                string[] roomsArray = pRoomInfo.Split('|');

                foreach (string s in roomsArray)
                {
                    //Split the rooms info down to an array of rooms
                    string[] roomsInfoArray = s.Split(',');

                    string numberAdults = roomsInfoArray[0];
                    string numberChildren = roomsInfoArray[1];
                    string numberInfants = roomsInfoArray[2];

                    List<JtChildAge> childrenAges = new List<JtChildAge>();
                    int noOfChild;
                    int.TryParse(numberChildren, out noOfChild);
                    if (noOfChild > 0)
                    {
                        string[] strChildrenAges = roomsInfoArray[3].Split('/');

                        foreach (string ca in strChildrenAges)
                        {
                            JtChildAge childAge = new JtChildAge
                            {
                                Age = ca
                            };
                            childrenAges.Add(childAge);
                        }
                    }

                    JtSearchRoomRequest room = new JtSearchRoomRequest
                    {
                        Adults = roomsInfoArray[0],
                        Children = roomsInfoArray[1],
                        Infants = roomsInfoArray[2],
                        ChildAges = childrenAges
                    };
                    roomRequests.Add(room);
                }
                searchDetails.RoomRequests = roomRequests;
                int curr;
                if (bbs.BedBanksActualCurrency != null)
                {
                    int.TryParse(bbs.BedBanksActualCurrency.BedBanksCurrencyId, out curr);
                    LoginDetails = JtController.GenerateLoginDetails(curr, bbs.BedBanksUserName, bbs.BedBanksPassword);
                }
                else
                {
                    int.TryParse(bbs.BedBanksDefaultCurrency.BedBanksCurrencyId, out curr);
                    LoginDetails = JtController.GenerateLoginDetails(curr, bbs.BedBanksUserName, bbs.BedBanksPassword);
                }
                SearchDetails = searchDetails;
            }
        }

        public IJtResponse Fire()
        {
            Stopwatch sw = Stopwatch.StartNew();
            string response = JtController.PerformApiRequest(this);
            long apiRT = sw.ElapsedMilliseconds;
            //READ THE RESULTS
            XmlRootAttribute xRoot = new XmlRootAttribute
            {
                ElementName = "SearchResponse",
                IsNullable = true
            };

            XmlSerializer serializer = new XmlSerializer(typeof(JtSearchResponse), xRoot);

            JtSearchResponse sr = null;
            JtSearchResponse initialFilteredSearchResponse = null;
            JtSearchResponse finalFilteredSearchResponse = null;

            try
            {
                MemoryStream memStream = new MemoryStream(Encoding.UTF8.GetBytes(response));
                sr = (JtSearchResponse)serializer.Deserialize(memStream);

                long s1 = sw.ElapsedMilliseconds;
                List<string> missingDetails = new List<string>();

                //perform initial filtering based on min star ranking and max threshold price
                initialFilteredSearchResponse = PerformInitialFilter(sr);
                finalFilteredSearchResponse = RemoveBBHotelOnSuppression(initialFilteredSearchResponse);

                //FIRST STEP - RUN THROUGH THE CACHE
                foreach (PropertyResult pr in finalFilteredSearchResponse.PropertyResults)
                {
                    //First Check cache for Data
                    JtPropertyDetailsResponse pdRes = JtController.GetPropertyDetailsFromCache(pr.PropertyID);

                    if (pdRes == null)
                    {
                        //Add it to the list we need to check the DB for
                        missingDetails.Add(pr.PropertyID);
                    }
                    else
                    {
                        //Retrieve the data from the Cache - cast as JtPropertyDetailsResponse
                        pr.PropertyDetails = pdRes;
                    }
                }

                //SECOND STEP - RUN THROUGH THE DB
                if (missingDetails.Count > 0)
                {
                    string dbList = missingDetails.Aggregate((x, y) => x + "," + y);

                    List<JtPropertyDetailsResponse> dbResults = JtController.GetPropertyDetailsFromDB(dbList);

                    foreach (PropertyResult pr in finalFilteredSearchResponse.PropertyResults)
                    {
                        if (pr.PropertyDetails == null)
                        {
                            foreach (JtPropertyDetailsResponse pdRes in dbResults)
                            {
                                if (pr.PropertyID == pdRes.PropertyId)
                                {
                                    pr.PropertyDetails = pdRes;
                                    missingDetails.Remove(pr.PropertyID);
                                }
                            }
                        }
                    }
                }

                //THIRD STEP - RUN THROUGH THE DB
                List<string> detailsObtained = new List<string>();
                if (missingDetails.Count > 0)
                {
                    string dbList = missingDetails.Aggregate((x, y) => x + "," + y);

                    foreach (string propertyID in missingDetails)
                    {
                        foreach (PropertyResult pr in finalFilteredSearchResponse.PropertyResults)
                        {
                            if (pr.PropertyID == propertyID)
                            {
                                //Perform soundEx Comparison and store in DB property Details
                                JtPropertyDetailsResponse apiResult = JtController.GetPropertyDetailsFromAPI(propertyID, bbs);
                                pr.PropertyDetails = apiResult;
                                detailsObtained.Add(propertyID);
                            }
                        }
                    }
                }

                foreach (string propertyID in detailsObtained)
                {
                    missingDetails.Remove(propertyID);
                }

                //FOURTH STEP REMOVE ANY OTHERS JUST TO BE SAFE (IT WILL CALL A NULL REF ERROR)
                if (missingDetails.Count > 0)
                {
                    foreach (string propertyID in missingDetails)
                    {
                        foreach (PropertyResult pr in finalFilteredSearchResponse.PropertyResults)
                        {
                            if (pr.PropertyID == propertyID)
                            {
                                finalFilteredSearchResponse.PropertyResults.Remove(pr);
                            }
                        }
                    }
                }

                long e1 = sw.ElapsedMilliseconds;
                long t1 = (e1 - s1);
                sr.ExtraInfo += "-----Time spent getting property details: " + t1 + "|";
            }
            catch (Exception exc)
            {
                NameValueCollection exception = new NameValueCollection();
                exception.Add(Constants.ExceptionMessage, exc.Message);
                exception.Add(Constants.ExceptionFlow, Constants.SearchRequest);
                SendExceptionMail(exception, bbs.ClientItemId);//call
                throw new Exception(exc.Message);
                //string s = exc.Message;
            }

            sw.Stop();
            sr.ResponseTime = apiRT;
            sr.ExtraInfo += "-----JT Search.Fire Total Function Time: " + sw.ElapsedMilliseconds + "|";

            TroikaDataController.InsertApiResponseTime(1, "Search", sr.ResponseTime);

            return finalFilteredSearchResponse;
        }

        /// <summary>
        /// Method to remove hotel based on maximum threshold price and minimun star ranking
        /// </summary>
        /// <param name="hotelList"></param>
        /// <returns></returns>
        JtSearchResponse PerformInitialFilter(JtSearchResponse hotelList)
        {
            JtSearchResponse filteredHotels = new JtSearchResponse();
            List<PropertyResult> filteredListOnStar = new List<PropertyResult>();
            List<PropertyResult> finalFiltered = new List<PropertyResult>();
            filteredListOnStar = hotelList.PropertyResults.Where(x => decimal.Parse(x.Rating) >= decimal.Parse(bbs.BedBanksMinimumStarRating)).ToList<PropertyResult>();
            foreach (var hotel in filteredListOnStar)
            {
                int roomCount = 0;
                for (int i = 0; i < hotel.RoomTypes.Length; i++)
                {
                    if (hotel.RoomTypes[i].Total > decimal.Parse(bbs.BedBanksDefaultCurrency.MaxPriceForHotels))
                    {
                        hotel.RoomTypes[i] = null;
                        roomCount++;
                    }
                }
                if (roomCount != hotel.RoomTypes.Length)
                {
                    finalFiltered.Add(hotel);
                }
            }
            filteredHotels.PropertyResults = finalFiltered;
            return filteredHotels;
        }

        /// <summary>
        /// Removing the BB hotels from search based on suppression 
        /// </summary>
        /// <param name="hotelList"></param>
        /// <returns></returns>
        JtSearchResponse RemoveBBHotelOnSuppression(JtSearchResponse hotelList)
        {
            return hotelList; //TODO: Change the code to remove based on suppression
        }
    }
}