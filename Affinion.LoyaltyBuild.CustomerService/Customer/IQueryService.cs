﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Sitecore.Data.Items;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/
namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Customer
{
    public interface IQueryService
    {
        /// <summary>
        /// To Save Customer Query
        /// </summary>
        /// <param name="customerQuery">Booking Query</param>
        /// <returns> ValidationResponse  </returns>
        ValidationResponse SaveCustomerQuery(CustomerQuery customerQuery);

        /// <summary>
        /// To get the Customer query
        /// </summary>
        /// <param name="OrderLineID">OrderLineID</param>
        /// <returns>BookingNotes object List  </returns>
        List<CustomerQuery> GetCustomerQuerys(int orderLineID);

        /// <summary>
        /// To get the Customer query
        /// </summary>
        /// <param name="OrderLineID">AttachmentId</param>
        /// <returns>attachment as bytes</returns>
        byte[] GetCustomerQuerysAttachments(int AttachmentId);

        /// <summary>
        /// To get the Customer Query Types
        /// </summary>
        /// <returns> Retun Qyery Type datatable </returns>
       // DataTable GetCustomerQueryTypes();
        
        /// <summary>
        /// To get the Customer Query Types
        /// </summary>
        /// <returns> Retun Qyery Type from Sitecore </returns>
        DataTable GetQueryTypes(bool IsActive = false);

        /// <summary>
        /// To get the Customer Query based on criteria 
        /// </summary>
        /// <param name="searchDetails">Search Details</param>
        /// <returns> Retun Customer Qyery as datatable </returns>
        List<SearchQueryResultItem> SearchCustomerQuery(CustomerQuerySearch searchDetails);

        /// <summary>
        /// To delete the Customer Query based on criteria 
        /// </summary>
        /// <param name="Customer">Customer</param>
        /// <returns> Retun ValidationResponsee </returns>
        ValidationResponse SearchCustomer(Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model.Customer customer);

       /// <summary>
        /// To delete the Customer Query based on criteria 
        /// </summary>
        /// <param name="queryId">queryId</param>
        /// <returns> Retun ValidationResponsee </returns>
        ValidationResponse DeleteCustomerQuery(int queryId);

        

    }
}
