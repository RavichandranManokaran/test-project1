﻿CREATE TABLE [dbo].[uCommerce_ShippingMethodDescription] (
    [ShippingMethodDescriptionId] INT            IDENTITY (1, 1) NOT NULL,
    [ShippingMethodId]            INT            NOT NULL,
    [DisplayName]                 NVARCHAR (128) NOT NULL,
    [Description]                 NVARCHAR (512) NULL,
    [DeliveryText]                NVARCHAR (512) NULL,
    [CultureCode]                 NVARCHAR (60)  NOT NULL,
    CONSTRAINT [uCommerce_PK_ShippingMethodDescription] PRIMARY KEY CLUSTERED ([ShippingMethodDescriptionId] ASC),
    CONSTRAINT [uCommerce_FK_ShippingMethodDescription_ShippingMethod] FOREIGN KEY ([ShippingMethodId]) REFERENCES [dbo].[uCommerce_ShippingMethod] ([ShippingMethodId])
);

