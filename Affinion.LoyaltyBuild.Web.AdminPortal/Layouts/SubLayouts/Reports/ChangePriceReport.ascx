﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChangePriceReport.ascx.cs" Inherits="Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.Reports.ChangePriceReport" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Sitecore.Data.Items" %>
<div class="container-outer width-full overflow-hidden-att app-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div>
                    <h3 class="title-text font-weight-bold">Change Rates Report - Finance
                    </h3>
                </div>
            </div>
        </div>
        <div class="middle-body-container" style="padding: 0;">
            <div class="row">
                <div class="col-sm-12">
                    <div class="accordian-outer">
                        <div class="panel-group" id="accordion1">
                            <div class="panel panel-default">
                                <div class="panel-heading bg-primary">
                                    <h4 class="panel-title marTM5">
                                        <span>Change Rates Report Search                                           
                                        </span>
                                        <a data-toggle="collapse" data-parent="#accordion1" href="#collapse0" class="show-hide-list"></a>
                                    </h4>
                                </div>
                                <div id="collapse0" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="col-xs-12 no-padding">
                                                    <div class="col-xs-3 no-padding-left">
                                                        DateFrom
                                                      
                                                        <div class="form-group">
                                                            <div class='input-group date' id='datetimepicker1'>
                                                                <input type="text" id="txtFromDate" runat="server" clientidmode="Static" class="form-control datepicker width-85 pull-left font-size-12" />
                                                            </div>

                                                        </div>

                                                    </div>
                                                    <div class="col-xs-3 no-padding-left">
                                                        DateTo
                                                         
                                                      
                                                        <div class="form-group">
                                                            <div class='input-group date' id='datetimepicker2'>
                                                                <input type="text" id="txtToDate" runat="server" clientidmode="Static" class="form-control datepicker width-85 pull-left font-size-12" />
                                                            </div>

                                                        </div>

                                                    </div>
                                                    <div class="col-xs-3 no-padding-left">
                                                        <div>
                                                            Country
                                                        
                                                        </div>
                                                        <div>
                                                            <asp:DropDownList ID="ddlCountry" class="form-control styled-select no-padding" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"></asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-3 no-padding">
                                                        <div>
                                                            Location
                                                          
                                                        </div>
                                                        <div>
                                                            <asp:DropDownList ID="ddlLocation" class="form-control styled-select no-padding" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged"></asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 no-padding">
                                                    <div class="col-xs-3  no-padding-left">
                                                        <div>
                                                            MapLocation
                                                           
                                                        </div>
                                                        <div>
                                                            <asp:DropDownList ID="ddlMapLocation" class="form-control styled-select no-padding" runat="server"></asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-3 no-padding-left">
                                                        <div>
                                                            Theme
                                                           
                                                        </div>
                                                        <div>
                                                            <asp:DropDownList ID="ddlTheme" class="form-control styled-select no-padding" runat="server"></asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-3  no-padding-left">
                                                        <div>
                                                            PriceBandFrom
                                                          
                                                        </div>
                                                        <div>
                                                            <asp:DropDownList ID="ddlPriceBandFrom" class="form-control styled-select no-padding" runat="server"></asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-3 no-padding">
                                                        <div>
                                                            PriceBandTo
                                                          
                                                        </div>
                                                        <div>
                                                            <asp:DropDownList ID="ddlPriceBandTo" class="form-control styled-select no-padding" runat="server"></asp:DropDownList>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-xs-12 no-padding marT15">
                                                    <div class="col-xs-3 no-padding-left">
                                                        <div>
                                                            StarsRating
                                                         
                                                        </div>
                                                        <div>
                                                            <asp:DropDownList ID="ddlStarsRating" class="form-control styled-select no-padding" runat="server"></asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-3 no-padding-left"></div>
                                                    <div class="col-xs-6 no-padding-right">
                                                        <div>&nbsp;</div>

                                                        <asp:Button ID="btnRetrieveData" Text="RetrieveData" clientidmode="Static" class="btn btn-primary pull-right btn_spce" runat="server" OnClick="RetrieveData_Click"></asp:Button>
                                                        <asp:Button ID="btnExport" Text="Export" clientidmode="Static" class="btn btn-primary pull-right btn_spce marR5" runat="server" OnClick="btnExport_Click"></asp:Button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 marT15">
                                                <asp:GridView PagerSettings-NextPageText=" Next " PagerSettings-PreviousPageText=" Previous " PagerSettings-Mode="NumericFirstLast" PagerSettings-FirstPageText=" First " PagerSettings-LastPageText=" Last " PagerSettings-Position="Bottom" PagerStyle-Font-Bold="true" PagerStyle-CssClass="pagination-grid" EmptyDataText="No Row(s) Found" ID="grdvwChangeRates" runat="server" CssClass="table table-striped marTB10" AutoGenerateColumns="False" Width="100%" AllowPaging="True" OnPageIndexChanging="grdvwChangeRates_PageIndexChanging">
                                                    <Columns>
                                                        <asp:BoundField HeaderText="Date" DataFormatString="{0:MM/dd/yyyy}" DataField="DateCreated"></asp:BoundField>
                                                        <asp:BoundField HeaderText="Hotel(Provider)" DataField="ProviderName"></asp:BoundField>
                                                        <asp:BoundField HeaderText="Room-Type" DataField="RoomType"></asp:BoundField>
                                                        <asp:BoundField HeaderText="PriceBand-From" DataField="PriceBandFrom"></asp:BoundField>
                                                        <asp:BoundField HeaderText="PriceBand-To" DataField="PriceBandTo"></asp:BoundField>
                                                        <asp:BoundField HeaderText="Price-From" DataField="PriceFrom"></asp:BoundField>
                                                        <asp:BoundField HeaderText="Price-To" DataField="PriceTo"></asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {

        jQuery("#txtFromDate").datepicker({
            defaultDate: new Date(),
            dateFormat: "mm/dd/yy",
            minDate: -180,
            maxDate: 0,
            numberOfMonths: 1,
            showOn: "both",
            buttonImage: "images/calendar.png",
            buttonImageOnly: false,
            buttonClass: "glyphicon-calendar"
        }).next(".ui-datepicker-trigger").addClass("btn btn-default glyphicon glyphicon-calendar calander-icon pull-left");

        jQuery("#txtToDate").datepicker({
            defaultDate: new Date(),
            dateFormat: "mm/dd/yy",
            minDate: -180,
            maxDate: 0,
            numberOfMonths: 1,
            showOn: "both",
            buttonImage: "images/calendar.png",
            buttonImageOnly: false,
            buttonClass: "glyphicon-calendar"
        }).next(".ui-datepicker-trigger").addClass("btn btn-default glyphicon glyphicon-calendar calander-icon pull-left");
        
        Validation();

    });

    $("#txtFromDate").change(function () {
        Validation();
    });

    $("#txtToDate").change(function () {
        Validation();
    });

    function Validation() {

        var fromDate = $("#txtFromDate").val();
        var toDate = $("#txtToDate").val();

        if (fromDate != "" && toDate != "") {
            $("#btnRetrieveData").removeAttr('disabled');
        }
        else {
            $("#btnRetrieveData").attr('disabled', 'disabled');
            $("#btnExport").attr('disabled', 'disabled');
        }

    }
</script>

