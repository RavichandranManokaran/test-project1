﻿/****** Object:  Table [dbo].[uCommerce_RoomReservation]    Script Date: 1/18/2016 3:19:57 PM ******/
DROP TABLE [dbo].[uCommerce_RoomReservation]
GO

/****** Object:  Table [dbo].[uCommerce_RoomReservation]    Script Date: 1/18/2016 3:19:57 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[uCommerce_RoomReservation](
	[RoomReservationId] [int] IDENTITY(1,1) NOT NULL,
	[OrderLineId] [int] NULL,
	[NoOfGuest] [int] NULL,
	[GuestName] [nvarchar](100) NULL,
	[NoOfAdult] [int] NULL,
	[NoOfChild] [int] NULL,
	[SpecialRequest] [varchar](150) NULL,
 CONSTRAINT [PK_RoomReservation] PRIMARY KEY CLUSTERED 
(
	[RoomReservationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


