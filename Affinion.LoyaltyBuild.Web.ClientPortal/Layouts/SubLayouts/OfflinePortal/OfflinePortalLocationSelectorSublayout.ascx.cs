﻿
//<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           LocationSelectorSublayout.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal
/// Description:           Used to Bind data to HotelPackageSubLayout
/// </summary>

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    #region Using Directives
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using System;
    using System.Linq;
    #endregion

    public partial class OfflinePortalLocationSelectorSublayout : System.Web.UI.UserControl
    {
        /// <summary>
        /// Code to execute on page load
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The parameter</param>
        private void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Database masterDb = Sitecore.Configuration.Factory.GetDatabase("web");
                Sitecore.Data.Database currentDb = Sitecore.Context.Database;

                //Load location data to a locationList
                Item locationList = currentDb.GetItem("/sitecore/content/admin-portal/global/_supporting-content/locations");

                var loactions = (from loc in locationList.Children
                                 orderby loc.DisplayName
                                 select loc.DisplayName).ToList();

                //Bind locationList data to the dropdown
                AvailableLocationDropDown.DataSource = loactions;
                AvailableLocationDropDown.DataBind();
                AvailableLocationDropDown.Items.Insert(0, "Please Select");
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, null);
                throw;
            }
        }
    }
}