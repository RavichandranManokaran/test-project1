﻿
namespace Affinion.LoyaltyBuild.Api.Search.Data
{
    /// <summary>
    /// Properties for SearchOptions used for location search
    /// </summary>
    public class SearchOptions
    {
        /// <summary>
        /// constructor
        /// </summary>
        public SearchOptions()
        {
            SortOn = SortField.Default;
            SortDirection = SortDirection.None;
        }

        /// <summary>
        /// SortOn
        /// </summary>
        public SortField SortOn { get; set; }

        /// <summary>
        /// Sort Direction
        /// </summary>
        public SortDirection SortDirection { get; set; }
    }
}
