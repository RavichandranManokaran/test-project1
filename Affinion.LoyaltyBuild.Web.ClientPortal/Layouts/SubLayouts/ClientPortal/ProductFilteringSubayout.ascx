﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductFilteringSubayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal.ProductFilteringSubayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Sitecore.Data.Items" %>
<%@ Import Namespace="Affinion.LoyaltyBuild.Common.Utilities" %>
<script>

    function activate_accordion(course_id) {
        var active_item;
        if (course_id == false) {
            active_item = false;
        } else {
            active_item = jQuery('.accordion div[id="course-' + course_id + '"]').attr('active');
        }

        jQuery(".accordion").accordion({
            collapsible: true,
            active: parseInt(active_item),
            heightStyle: "content",
            icons: {
                "header": "ui-icon-plus",
                "activeHeader": "ui-icon-minus"
            }
        });


        jQuery('.tbl-calendar thead tr').removeAttr("style");
    }

</script>

<style type="text/css">
    .noText label 
{
    display: none;
}
</style>

   


<div class="col-xs-12 no-padding">

    <div class="advancedSearchLink margin-common-inner-box">
        <div class="accordion">
            <div id="course-25">Filter by: Themes & Facilities</div>
            <div class="theme-facility-inner alert alert-info ">
                
                    <asp:Repeater ID="ThemesRepeater" runat="server">
                        <HeaderTemplate>
                            <div class="theme-common">
                            <div class="col-btn-list-fs">
                                <button type="button" class="btn theme width-full text-left btn-common" data-toggle="collapse" data-target="#tf-icon1">
                                    Themes
                                </button>
                            </div>
                            <div id="tf-icon1" class="collapse in">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <div class="row">
                                <div class="col-xs-9">
                                    <label class="checkbox-inline">
                                            <asp:CheckBox ID="themesCheckBox" runat="server"  EnableViewState="true"   AutoPostBack="false" Text ='<%#SitecoreFieldsHelper.GetValue((Item)Container.DataItem, "Name") %>' CssClass="noText"/> 
                                    <sc:Text Field="Name"  Item="<%#(Item)Container.DataItem %>" runat="server" />
                                    </label>
                                </div>
                            </div>
                        </ItemTemplate>
                        <FooterTemplate>
                            </div>
                            </div>
                        </FooterTemplate>
                </asp:Repeater>
                    


                
                <asp:Repeater ID="FacilitiesRepeater" runat="server">
                    <HeaderTemplate>
                        <div class="theme-common">
                        <div class="col-btn-list-fs">
                            <button type="button" class="btn facilities width-full text-left btn-common" data-toggle="collapse" data-target="#tf-icon2">
                                Facilities
                            </button>
                        </div>
                        <div id="tf-icon2" class="collapse in">
                    </HeaderTemplate>
                <ItemTemplate>
                    <div class="row">
                        <div class="col-xs-9">
                            <label class="checkbox-inline">
                                    <asp:CheckBox ID="facilitiesCheckBox" runat="server"  EnableViewState="true"  AutoPostBack="false" Text ='<%#SitecoreFieldsHelper.GetValue((Item)Container.DataItem, "Name") %>' CssClass="noText"  />
                                <sc:Text Field="Name"  Item="<%#(Item)Container.DataItem %>" runat="server" />
                            </label>
                        </div>
                    </div>
                    </ItemTemplate>
                    <FooterTemplate>
                        </div>
                        </div>
                    </FooterTemplate>
                </asp:Repeater>
                    


                
                <asp:Repeater ID="ExperienceRepeater" runat="server">
                    <HeaderTemplate>
                        <div class="theme-common">
                        <div class="col-btn-list-fs">
                            <button type="button" class="btn facilities width-full text-left btn-common" data-toggle="collapse" data-target="#tf-icon3">
                                Experience
                            </button>
                        </div>
                        <div id="tf-icon3" class="collapse in">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="row">
                            <div class="col-xs-9">
                                <label class="checkbox-inline">
                                        <asp:CheckBox ID="experienceCheckBox" runat="server"  EnableViewState="true"  AutoPostBack="false" Text ='<%#SitecoreFieldsHelper.GetValue((Item)Container.DataItem, "Name") %>' CssClass="noText"  />
                                    <sc:Text Field="Name"  Item="<%#(Item)Container.DataItem %>" runat="server" />
                                </label>
                            </div>
                        </div>
                    </ItemTemplate>
                    <FooterTemplate>
                        </div>
                        </div>
                    </FooterTemplate>
                </asp:Repeater>
                

                <asp:Repeater ID="StarRankingRepeater" runat="server">
                    <HeaderTemplate>
                        <div class="theme-common">
                        <div class="col-btn-list-fs">
                            <button type="button" class="btn facilities width-full text-left btn-common" data-toggle="collapse" data-target="#tf-icon4">
                                Star Ranking
                            </button>
                        </div>
                        <div id="tf-icon4" class="collapse in">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="row">
                            <div class="col-xs-9">
                                <label class="checkbox-inline">
                                        <asp:CheckBox ID="starCheckBox" runat="server" EnableViewState="true"  AutoPostBack="false" Text ='<%#Eval("Name") %>'  CssClass="noText" />
                                    <sc:Text Field="Name"  Item="<%#(Item)Container.DataItem %>" runat="server" />
                                       
                                </label>
                            </div>
                        </div>
                    </ItemTemplate>
                    <FooterTemplate>
                        </div>
                        </div>
                    </FooterTemplate>
                </asp:Repeater>

                 

                <asp:Repeater ID="RecommendedRepeater" runat="server">
                    <HeaderTemplate>
                        <div class="theme-common">
                        <div class="col-btn-list-fs">
                            <button type="button" class="btn facilities width-full text-left btn-common" data-toggle="collapse" data-target="#tf-icon5">
                                Recommended
                            </button>
                        </div>
                        <div id="tf-icon5" class="collapse in">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="row">
                            <div class="col-xs-9">
                                <label class="checkbox-inline">
                                        <asp:CheckBox ID="recommendedCheckBox" runat="server" EnableViewState="true"  AutoPostBack="false" Text ='<%#Eval("Name") %>'  CssClass="noText" />
                                        <sc:Text Field="Name"  Item="<%#(Item)Container.DataItem %>" runat="server" />                                       
                                </label>
                            </div>
                        </div>
                    </ItemTemplate>
                    <FooterTemplate>
                        </div>
                        </div>
                    </FooterTemplate>
                </asp:Repeater>


                <div class="col-xs-12">
                    <asp:Button id="filterButton" Text="Filter"  CssClass="btn btn-default add-break" runat="server" OnClick="ButtonFilter_Click"  />
                </div>
            </div>
        </div>
    </div>



</div>


<script src="scripts/navigation.js" type="text/javaScript"></script>
<!-- ================================================== -->
<script type="text/javascript">
    jQuery(document).ready(function () {

        activate_accordion(40);
        jQuery("table tr:even").css({ "background-color": "#fafefe", "border-top": "solid 1px #cff2f6", "border-bottom": "solid 1px #cff2f6" });
        jQuery("table tr:odd").css("background-color", "#fff");

    });
</script>


