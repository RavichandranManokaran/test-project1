﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model
{
    public class ProviderAvailabilityAlert
    {
        public string ProviderID { get; set; }
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public string RoomTypeName { get; set; }
        public string MapLocationName { get; set; }
        public string StarRanking { get; set; }
        public string CountryName { get; set; }
        public DateTime ProviderDate { get; set; }
        public string ProviderEmail { get; set; }
        public string ProviderTypeName { get; set; }
        public DateTime ReportedDate { get; set; }
        public string Year { get; set; }
        public string Month { get; set; }
        public string Day { get; set; }
        
    }
}
