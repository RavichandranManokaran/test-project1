﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientPortalMyFavorite.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal.ClientPortalMyFavorite" %>

<link href="../../../Resources/styles/bootstrap.min.css" rel="stylesheet" />
<script src="../../../Resources/bootstrap_assets/javascripts/bootstrap.min.js"></script>
<script src="../../../Resources/scripts/new.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        //jQuery('.popover-markup > .trigger').popover({
        //    html: true,
        //    title: function () {
        //        return jQuery(this).parent().find('.head').html();
        //    },
        //    content: function () {
        //        return jQuery(this).parent().find('.content').html();
        //    },
        //    container: 'form',
        //    placement: 'bottom',
        //});

        //jQuery(this).parent().appendTo("form");


        jQuery('.addNewFavHotel').click(function () {
            var URLString = "/Pages/AddToFavorite.aspx";
            var newListName = jQuery('#txtNewList').val();
            jQuery.ajax({
                url: URLString,
                type: 'get',
                data: { listName: newListName },
                contenttype: "application/json;charset=utf-8",
                traditional: true,
                success: function (results) {
                    if (results.indexOf("hidAddToFavoriteSuccess") >= 0) {
                        jQuery('.createdListContentTemp').html(results);
                        var htmlToAppend = jQuery('.createdListContentTemp .newListWrapper').html();
                        jQuery(".existingLitWrapper").append(htmlToAppend);
                        jQuery('#txtNewList').val("");
                        jQuery('.createdListContentTemp').html("");
                    }
                }
            });
        });

    });
</script>
<div class="popover-markup">
    <%--<a href="#" class="trigger">My Favorite
    </a>
    <div class="head hide">
        My Lists
    </div>--%>
    <div class="content">
        <div class="existingLitWrapper">
            <asp:Repeater runat="server" ID="rptList" OnItemDataBound="rptList_ItemDataBound">
            <ItemTemplate>
                <div>
                    <asp:CheckBox ID="chkList" runat="server" />
                    <asp:Label ID="lblList" runat="server" />
                </div>
            </ItemTemplate>
        </asp:Repeater>
        </div>
        <div class="NewListCreationWrapper">
            <input type="checkbox" disabled />
            <input type="text" id="txtNewList" />
            <input type="button" class="addNewFavHotel"/>
            <a id="addNewFavoriteHotel" class="addNewFavoriteHotel" href="#">Click</a>
        </div>
        <div class="createdListContentTemp" style="display:none;"></div>
    </div>
</div>
