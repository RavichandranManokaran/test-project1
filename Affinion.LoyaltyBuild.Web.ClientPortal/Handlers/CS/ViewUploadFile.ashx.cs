﻿using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Customer;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Handlers.CS
{
    /// <summary>
    /// Summary description for ViewUploadFile
    /// </summary>
    public class ViewUploadFile : IHttpHandler
    {
        private NameValueCollection _query = null;
        private QueryService _queryService;
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                _query = HttpUtility.ParseQueryString(context.Request.Url.Query.ToLower());
                int queryId = Convert.ToInt32(_query["queryid"]);
                if (queryId == 0)
                {
                    throw new Exception("invalid request");
                }

                _queryService = new QueryService();
                CustomerQueryAttachment attachment = _queryService.GetQueryAttachment(queryId);
                if (attachment != null)
                {
                    byte[] byteArray = attachment.File;
                    context.Response.AddHeader("content-disposition", string.Format("attachment; filename=download{0}", attachment.FileType));
                    context.Response.AddHeader("Content-Length", byteArray.Length.ToString());
                    switch (attachment.FileType)
                    {
                        case ".doc":
                        case ".docx":
                            context.Response.ContentType = "application/msword";
                            break;
                        case ".pdf":
                            context.Response.ContentType = "application/pdf";
                            break;
                        case ".xls":
                        case ".xlsx":
                            context.Response.ContentType = "application/vnd.ms-excel";
                             break;
                        default :
                            break;
                    }
                    // Set the HTTP MIME type of the output stream
                    context.Response.BinaryWrite(byteArray);
                    context.Response.End();
                }

            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, this);
                throw;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}