﻿CREATE PROCEDURE [dbo].[LB_AddTransferDue] 
@OldOrderLineId INT=NULL,
@NewOrderLineId INT=NULL,
@IsFreeTransfer BIT = 0,
@CreatedBy VARCHAR(50)=NULL
AS
BEGIN
        DECLARE @OldBookingID UNIQUEIDENTIFIER
        DECLARE @BookingID UNIQUEIDENTIFIER
		DECLARE @DueTypeID UNIQUEIDENTIFIER
		DECLARE @PaymentMethod VARCHAR(20)
		DECLARE @SumOfProviderAmount MONEY
		DECLARE @OrderLineDueId INT
		DECLARE @PaymentMethodID INT = 0


		SET @OldBookingID = (SELECT BookingId from LB_BookingInfo where OrderLineId = @OldOrderLineId)
		SET @BookingID = (SELECT BookingId from LB_BookingInfo where OrderLineId = @NewOrderLineId)

		SELECT TOP 1 @PaymentMethodID = P.PaymentMethodId FROM uCommerce_OrderLine OL 
                INNER JOIN uCommerce_Payment P ON OL.OrderId = P.OrderId 
                WHERE OL.OrderLineId IN (SELECT * FROM dbo.LB_FnRelatedOrderLines(@OldOrderLineId)) 

        IF (@IsFreeTransfer = 0)
        BEGIN
		    --INSERT COMMISSION AMOUNT
			SET @DueTypeID = (SELECT DueTypeId FROM LB_DueTypes WHERE Code = 'COMMISSION')
		    
			INSERT INTO LB_OrderLineDue 
			(	
				OrderLineID, BookingId, DueTypeId, CurrencyTypeId, PaymentMethodId, TotalAmount, CommissionAmount, ProviderAmount,
				ProcessingFeeAmount, VatAmount, Note, IsDeleted, CreatedBy, CreatedDate,ParentOrderLineDueId
			)
			
			SELECT @NewOrderLineId,@BookingID [BookingId],@DueTypeID [DueTypeID],_CurrencyID [CurrencyTypeId],
					@PaymentMethodID [PaymentMethodId], _LBCommissionFee [TotalAmount], _LBCommissionFee [CommissionAmount],
					0 [ProviderAmount], 0 [ProcessingFeeAmount],0,'',0,	@CreatedBy,GETDATE(),NULL	
			FROM 
			(
				SELECT OrderLineId, [key], [value] FROM  uCommerce_OrderProperty WHERE OrderLineID = @NewOrderLineId) 
				src
				PIVOT
				(
					MAX([value])
					FOR [key] IN (_CurrencyID,_LBCommissionFee)
			 ) piv 
			
			--INSERT PROCESSING FEE AMOUNT
			SET @DueTypeID = (SELECT DueTypeId FROM LB_DueTypes WHERE Code = 'PROFEE')

		    INSERT INTO LB_OrderLineDue 
			(	
				OrderLineID, BookingId, DueTypeId, CurrencyTypeId, PaymentMethodId, TotalAmount, CommissionAmount, ProviderAmount,
				ProcessingFeeAmount, VatAmount, Note, IsDeleted, CreatedBy, CreatedDate,ParentOrderLineDueId
			)
			SELECT @NewOrderLineId,@BookingID [BookingId],@DueTypeID [DueTypeID],_CurrencyID [CurrencyTypeId],
				   @PaymentMethodID [PaymentMethodId], CalculatedProcessingFee [TotalAmount], 0 [CommissionAmount],
			      0 [ProviderAmount], CalculatedProcessingFee [ProcessingFeeAmount],0,'',0,@CreatedBy,GETDATE(),NULL	
			FROM 
			(
				SELECT OrderLineId, [key], [value] FROM  uCommerce_OrderProperty WHERE OrderLineID = @NewOrderLineId) src
				PIVOT
				(
					MAX([value])
					FOR [key] IN (_CurrencyID, CalculatedProcessingFee)
			 ) piv 
			 WHERE CAST(ISNULL(CalculatedProcessingFee,'0') AS MONEY) > 0
			 
			 --CHNAGE OLD ORDER LINE COMMISSION DUE TYPE TO TRANSFER DUE TYPE
			SET @DueTypeID = (SELECT DueTypeId FROM LB_DueTypes WHERE Code = 'TRANSFERFEE')
			UPDATE LB_OrderLineDue SET DueTypeId=@DueTypeID WHERE CommissionAmount >0 AND OrderLineID = @OldOrderLineId
        END

        SET @PaymentMethod = (SELECT Value FROM uCommerce_OrderProperty WHERE [Key]='PaymentMethod' AND OrderLineId = @NewOrderLineId)

		IF(@PaymentMethod='Full' OR @PaymentMethod='Partial')
		BEGIN
		    DECLARE @OldOrderLineDueID INT
			DECLARE @NewOrderLineDueID INT
		    --INSERT NEGATIVE PROVIDER AMOUNT ENTRY FOR OLD ORDER LINE ID
			SET @DueTypeID = (SELECT TOP 1 DueTypeId FROM LB_DueTypes WHERE Code = 'PROVIDERAMOUNT')

			INSERT INTO LB_OrderLineDue 
			(	
				OrderLineID, BookingId, DueTypeId, CurrencyTypeId, PaymentMethodId, TotalAmount, CommissionAmount, ProviderAmount,
				ProcessingFeeAmount, VatAmount, Note, IsDeleted, CreatedBy, CreatedDate,ParentOrderLineDueId
			)
			SELECT @OldOrderLineId,@OldBookingID [BookingId],@DueTypeID [DueTypeID],_CurrencyID [CurrencyTypeId],
				   @PaymentMethodID [PaymentMethodId], (CONVERT(money,_providerAmount)*-1) [TotalAmount], 0 [CommissionAmount],
				  (CONVERT(money,_providerAmount)*-1) [ProviderAmount], 0 [ProcessingFeeAmount],0,'',0,@CreatedBy,GETDATE(),NULL	
			FROM 
			(
				SELECT OrderLineId, [key], [value] FROM  uCommerce_OrderProperty WHERE OrderLineID = @OldOrderLineId) src
				PIVOT (MAX([value])
					FOR [key] IN (_CurrencyID,_providerAmount)
			) piv 

			SET @OldOrderLineDueID=@@IDENTITY

			--INSERT POSITIVE PROVIDER AMOUNT ENTRY FOR NEW ORDER LINE ID
		    INSERT INTO LB_OrderLineDue 
			(	
				OrderLineID, BookingId, DueTypeId, CurrencyTypeId, PaymentMethodId, TotalAmount, CommissionAmount, ProviderAmount,
				ProcessingFeeAmount, VatAmount, Note, IsDeleted, CreatedBy, CreatedDate,ParentOrderLineDueId
			)
			SELECT @NewOrderLineId,@BookingID [BookingId],@DueTypeID [DueTypeID],_CurrencyID [CurrencyTypeId],
				   @PaymentMethodID [PaymentMethodId], _providerAmount [TotalAmount], 0 [CommissionAmount],
				  _providerAmount [ProviderAmount], 0 [ProcessingFeeAmount],0,'',0,@CreatedBy,GETDATE(),NULL	
			FROM 
			(
				SELECT OrderLineId, [key], [value] FROM  uCommerce_OrderProperty WHERE OrderLineID = @NewOrderLineId) src
				PIVOT
				(
					MAX([value])
					FOR [key] IN (_CurrencyID,_providerAmount)
			 ) piv 

			 SET @NewOrderLineDueID=@@IDENTITY

			 /*IF SUM OF PROVIDER AMOUNT IS NEGATIVE THE ACTION IS REFUND,
			   WE SHOULD UPDATE OLD ORDER LINE PROVIDER AMOUNT 
			   PARENT ORDER LINE DUE ID WITH NEW ORDER LINE PROVIDER AMOUNT DUE  ID*/
			 UPDATE LB_OrderLineDue SET ParentOrderLineDueId = @NewOrderLineDueID WHERE OrderLineDueId = @OldOrderLineDueID

			 SELECT @SumOfProviderAmount = SUM(ProviderAmount) FROM LB_OrderLineDue 
				WHERE OrderLineDueId = @NewOrderLineDueID OR ParentOrderLineDueId = @NewOrderLineDueID
			 

			 IF (@SumOfProviderAmount < 0)
			 BEGIN
			      /*IF OLD ORDER LINE PROVIDER AMOUNT  IS GREATER THAN THE NEW ORDER LINE PROVIDER AMOUNT ,
				   WE SHOULD UPDATE NEW ORDER LINE PROVIDER AMOUNT 
				   PARENT ORDER LINE DUE ID WITH OLD ORDER LINE PROVIDER AMOUNT DUE TYPE ID*/
				  SET @DueTypeID = (SELECT DueTypeId FROM LB_DueTypes WHERE Code = 'REFUND')
				  UPDATE LB_OrderLineDue SET DueTypeId=@DueTypeID WHERE OrderLineDueId = @NewOrderLineDueID
			 END
		END

END

