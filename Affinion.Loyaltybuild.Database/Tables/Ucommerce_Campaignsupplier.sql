﻿CREATE TABLE [dbo].[Ucommerce_Campaignsupplier](
	[ID] [int] NOT NULL,
	[supplierid] [int] NOT NULL,
	[AwardId] [int] NOT NULL,
 CONSTRAINT [PK_Campaign-supplier] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO