﻿using Affinion.LoyaltyBuild.BedBanks.JacTravel.Model;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Message.Interfaces;
using System;

namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Message
{
    [Serializable()]
    public class JtPreCancelResponse : JtResponse, IJtResponse
    {
        public string BookingReference { get; set; }
        public double CancellationCost { get; set; }
        public string CancellationToken { get; set; }
    }
}
