﻿CREATE TABLE [dbo].[uCommerce_CategoryProperty] (
    [CategoryPropertyId] INT            IDENTITY (1, 1) NOT NULL,
    [Value]              NVARCHAR (MAX) NOT NULL,
    [DefinitionFieldId]  INT            NOT NULL,
    [CultureCode]        NVARCHAR (60)  NULL,
    [CategoryId]         INT            NOT NULL,
    CONSTRAINT [PK_uCommerce_CategoryProperty] PRIMARY KEY CLUSTERED ([CategoryPropertyId] ASC),
    CONSTRAINT [FK_uCommerce_CategoryProperty_uCommerce_Category] FOREIGN KEY ([CategoryId]) REFERENCES [dbo].[uCommerce_Category] ([CategoryId]),
    CONSTRAINT [FK_uCommerce_CategoryProperty_uCommerce_DefinitionField] FOREIGN KEY ([DefinitionFieldId]) REFERENCES [dbo].[uCommerce_DefinitionField] ([DefinitionFieldId])
);


GO
CREATE NONCLUSTERED INDEX [IX_uCommerce_CategoryProperty_CategoryId]
    ON [dbo].[uCommerce_CategoryProperty]([CategoryId] ASC)
    INCLUDE([CategoryPropertyId], [Value], [DefinitionFieldId], [CultureCode]);

