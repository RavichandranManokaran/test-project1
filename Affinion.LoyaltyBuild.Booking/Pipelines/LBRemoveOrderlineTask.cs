﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.Pipelines;
using UCommerce.Pipelines.UpdateLineItem;

namespace Affinion.LoyaltyBuild.Api.Booking.Pipelines
{
    /// <summary>
    /// Remove orderline task
    /// </summary>
    public class LBRemoveOrderlineTask: IPipelineTask<IPipelineArgs<UpdateLineItemRequest, UpdateLineItemResponse>>
    {
        /// <summary>
        /// Execute method
        /// </summary>
        /// <param name="subject"></param>
        /// <returns></returns>
        public PipelineExecutionResult Execute(IPipelineArgs<UpdateLineItemRequest, UpdateLineItemResponse> subject)
        {
            //remove item when quentity is 0
            if(subject.Request.Quantity == 0)
            {
                var currentId = subject.Request.OrderLine.OrderLineId;

                //remove all addons associated with this item
                var addons = subject.Request.OrderLine[Constants.ChildItems];
                if (!string.IsNullOrEmpty(addons))
                {
                    var orderLineId = 0;
                    foreach (var item in addons.Split(','))
                    {
                        if (int.TryParse(item, out orderLineId))
                            subject.Request.OrderLine.PurchaseOrder.RemoveOrderLine(subject.Request.OrderLine.PurchaseOrder.OrderLines.FirstOrDefault(i => i.OrderLineId == orderLineId));
                    }
                }

                //remove association on addon removal
                //remove all addons associated with this item
                var parentItem = subject.Request.OrderLine[Constants.ParentItem];
                if (!string.IsNullOrEmpty(parentItem))
                {
                    var orderLineId = 0;
                    if (int.TryParse(parentItem, out orderLineId))
                    {
                        var item = subject.Request.OrderLine.PurchaseOrder.OrderLines.FirstOrDefault(i => i.OrderLineId == orderLineId);
                        if(item != null)
                        {
                            item[Constants.ChildItems] = string.Join(",", item[Constants.ChildItems].Split(',')
                                .SkipWhile(i => i == currentId.ToString()));
                        }

                    }
                    foreach (var item in addons.Split(','))
                    {
                        if (int.TryParse(item, out orderLineId))
                            subject.Request.OrderLine.PurchaseOrder.RemoveOrderLine(subject.Request.OrderLine.PurchaseOrder.OrderLines.FirstOrDefault(i => i.OrderLineId == orderLineId));
                    }
                }

                //remove orderline
                subject.Request.OrderLine.PurchaseOrder.RemoveOrderLine(subject.Request.OrderLine);
            }

            return PipelineExecutionResult.Success;
        }
    }
}
