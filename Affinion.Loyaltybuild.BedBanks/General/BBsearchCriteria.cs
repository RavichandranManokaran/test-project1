﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.BedBanks.General
{
    public class BBsearchCriteria
    {
        /// <summary>
        /// Bed Bank Data
        /// </summary>
        public BedBanksSettings BedBankData {get; set;}

        /// <summary>
        /// Duration (No Of Nights)
        /// </summary>
        public string Duration {get; set;}

        /// <summary>
        /// Arrival Date
        /// </summary>
        public string ArrivalDate {get; set;}

        /// <summary>
        /// Room Information - No.Of Rooms, No.Of Adults, No.Of Children, Age Of Children
        /// </summary>
        public string RoomInfo {get; set;}

        /// <summary>
        /// Region ID (Location ID)
        /// </summary>
        public string RegionId { get; set; }

        /// <summary>
        /// Property Reference ID
        /// </summary>
        public List<string> PropertyReferenceID { get; set; }

        /// <summary>
        /// Star Ranking
        /// </summary>
        public string StarRanking { get; set; }

        /// <summary>
        /// Facility
        /// </summary>
        public string Facility { get; set; }
    }
}
