﻿using System;
using System.Linq;
using Sitecore.Data;
using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Data.Items;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports;
using System.Text;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common;
using System.Web;
using System.Collections.Generic;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.EmailScheduler
{
    /// <summary>
    /// Romm shortage email scheduler which runs once every monday
    /// </summary>
    public class RoomShortageEmailReminder
    {
        //private variable declarations
        private Item _mailItem;
        private Item hotelItem;
        private Item[] providerItem;
        private string _hotalName = string.Empty;
        private string _toEmail = string.Empty;
        private Database _currentDB;
        private string _mailContentPath = "/sitecore/content/admin-portal/global/_supporting-content/room-shortage-email/roomshortageemailreminder";
       // private string _ProvidersPath = @"fast:/sitecore/content/#admin-portal#/#supplier-setup#//*[@@templateid='{A7235C94-2EF5-4A3B-87DB-0D4DCF10B4DB}' and @SKU='{0}']";
       // private string _ProvidersPath = "/sitecore/content/admin-portal/supplier-setup";
        private string _ProvidersPath = "/sitecore/content/#admin-portal#/#supplier-setup#//*[@@templateid='{A7235C94-2EF5-4A3B-87DB-0D4DCF10B4DB}']";        
        
        /// <summary>
        /// Run method for the scheduler job
        /// </summary>
        /// <param name="items"></param>
        /// <param name="command"></param>
        /// <param name="schedule"></param>
        public void Run(Sitecore.Data.Items.Item[] items, Sitecore.Tasks.CommandItem command, Sitecore.Tasks.ScheduleItem schedule)
        {
            //To make sure runs only once per day
            if (schedule.LastRun.Date != DateTime.Now.Date)
            {
                //_currentDB = Sitecore.Configuration.Factory.GetDatabase("web");
                //_mailItem = _currentDB.GetItem(_mailContentPath);
                ExecuteJob();
            }
        }

        /// <summary>
        /// ExecuteJob method for get all provider details 
        /// </summary>
        public void ExecuteJob()
        {
            try
            {
                _currentDB = Sitecore.Configuration.Factory.GetDatabase("web");
                _mailItem = _currentDB.GetItem(_mailContentPath);

                Diagnostics.Trace(DiagnosticsCategory.ExternalServices, "RoomShortageEmailReminder:ExecuteJob() method started  "+DateTime.Now);

                string providerId = string.Empty;
                ReportsService service = new ReportsService();
                var roomShortageList = service.GetShortagelist();
                StringBuilder htmlTable = null;

                string tableHeader = 
                @"<table style='width:100%;display:table;border:1px solid #4c91cd;' cellpadding='0' cellspacing='0'>
                    <tr>
                        <th  style='background:#4c91cd;color:#fff;border-right:1px solid #fff;text-align:center;padding:5px;'>Room Type</th>
                        <th  style='background:#4c91cd;color:#fff;border-right:1px solid #fff;text-align:center;padding:5px;'>Month</th>
                        <th style='background:#4c91cd;color:#fff;border-right:1px solid #fff;text-align:center;padding:5px;'>Stock Shortage Dates</th>
                    </tr>
                    {0}
                </table>";
                string rowStyle = 
                    @"<tr>
                        <td style='background:transparent;color:#4c91cd;border-right:1px solid #ccc;text-align:left;padding:5px;'>{0}</td>
                        <td style='background:transparent;color:#4c91cd;border-right:1px solid #ccc;text-align:left;padding:5px;'>{1}</td>
                        <td style='background:transparent;color:#4c91cd;border-right:1px solid #ccc;text-align:left;padding:5px;'>{2}</td>
                    </tr>";
                string table = string.Empty;               

                Item[] Hotels = _currentDB.SelectItems(_ProvidersPath);            

                if (roomShortageList.Count >= 1)
                {
                    foreach (var item in roomShortageList)
                    {
                        if (providerId != item.SKU)
                        {
                            if (htmlTable != null)
                                PrepareMail(hotelItem, string.Format(tableHeader, htmlTable.ToString()));

                            htmlTable = new StringBuilder();
                            providerId = item.SKU;
                            hotelItem = Hotels.Where(x => x.Fields["SKU"].Value.Equals(providerId, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();

                        }
                        if (hotelItem != null)
                        {
                            IEnumerable<Item> rooms = hotelItem.Axes.GetDescendants().Where(a => a.TemplateName.Equals("Supplier Room"));
                            string roomtype = string.Empty;
                            foreach(Item room in rooms)
                            {
                                roomtype = room.Fields["SKU"].Value == item.RoomType ? room.Fields["Title"].Value : string.Empty;
                                if (!string.IsNullOrEmpty(roomtype))
                                htmlTable.Append(string.Format(rowStyle, roomtype, item.month, item.Day));
                            }
                            
                        }

                    }
                    if (htmlTable != null)
                        PrepareMail(hotelItem, string.Format(tableHeader, htmlTable.ToString()));
                }
            }
            catch (Exception exception)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("HolidayHomeEmailReminder: Exception-{0}", exception.ToString()));
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
            }
            Diagnostics.Trace(DiagnosticsCategory.ExternalServices, "RoomShortageEmailReminder:ExecuteJob() method ended  "+DateTime.Now);

        }           
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="table"></param>
        private void PrepareMail(Item providerItem, string table)
        {
            try
            {

                
                    string FromEmail = SitecoreFieldsHelper.GetValue(_mailItem, "SenderEmail");
                    _toEmail = SitecoreFieldsHelper.GetValue(providerItem, "EmailID2");
                    _hotalName = SitecoreFieldsHelper.GetValue(providerItem, "Name");
                    Sitecore.Data.Fields.CheckboxField subscribe = (Sitecore.Data.Fields.CheckboxField)providerItem.Fields["SubscribeEmail"];

                    string msgBody = SitecoreFieldsHelper.GetValue(_mailItem, "EmailBody");
                    msgBody = PrepareMailBody(msgBody, FromEmail, _hotalName, table);

                    if (!string.IsNullOrEmpty(FromEmail) && !string.IsNullOrEmpty(_toEmail) && subscribe.Checked)
                    {
                        mailGenarate(FromEmail, _toEmail, msgBody, _hotalName);
                    }
                
            }
            catch (Exception exception)
            {

                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("ShortageRoomEmailReminder: Exception-{0}", exception.ToString()));
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
            }
            


        }
      /// <summary>
      ///   PrepareMailBody for update new mail body for particular provider  
      /// </summary>
      /// <param name="bodyText"></param>
      /// <param name="mailId"></param>
      /// <param name="name"></param>
      /// <param name="table"></param>
      /// <returns></returns>
        private string PrepareMailBody(string bodyText,String mailId,string name,string table)
        {

            bodyText = bodyText.Replace("{HotelName}", !string.IsNullOrWhiteSpace(name) ? name : string.Empty);
            bodyText = bodyText.Replace("{HtmlTable}", !string.IsNullOrWhiteSpace(name) ? table : string.Empty);
            bodyText = bodyText.Replace("{FromId}", !string.IsNullOrWhiteSpace(mailId) ? mailId : string.Empty);
            return bodyText;
        }

       /// <summary>
        /// mailGenarate method for send mail to provider
       /// </summary>
       /// <param name="senderId"></param>
       /// <param name="receiverId"></param>
       /// <param name="msgBody"></param>
       /// <param name="hotelName"></param>
        private void mailGenarate(string senderId, string receiverId, string msgBody,string hotelName)
        {
            try
            {
                Affinion.LoyaltyBuild.Communications.IEmailService emailService = new Affinion.LoyaltyBuild.Communications.Services.EmailService();

                string mailSubject = SitecoreFieldsHelper.GetValue(_mailItem, "Subject");
                mailSubject = mailSubject.Replace("{HotelName}", !string.IsNullOrWhiteSpace(hotelName) ? hotelName : string.Empty);
                mailSubject = mailSubject.Replace("{FromDate}", DateTime.Now.ToString("dd/MM/yyyy"));
                mailSubject = mailSubject.Replace("{ToDate}", DateTime.Now.AddDays(30).ToString("dd/MM/yyyy"));

                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("ShortageRoomEmailReminder: SenderId-{0}", senderId));
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("ShortageRoomEmailReminder: ReceiverId-{0}", receiverId));           

                emailService.Send(senderId, receiverId, mailSubject, msgBody, true);
            }
            catch (Exception exception) 
            {

                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("ShortageRoomEmailReminder: Exception-{0}", exception.ToString()));
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
            }
        }
    }
}