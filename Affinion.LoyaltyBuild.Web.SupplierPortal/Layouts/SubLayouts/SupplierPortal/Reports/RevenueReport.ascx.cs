﻿//<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           RevenueReport.ascx.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.SupplierPortal
/// Description:           SubLayout for Revenue report generation
/// </summary>

#region Using directives
using Affinion.LoyaltyBuild.SupplierPortal.DataAccess.Test;
using Microsoft.Reporting.WebForms;
using System;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration.SupplierPortal.Reports;
using Affinion.LoyaltyBuild.SupplierPortal.Reports.Test;
using Affinion.LoyaltyBuild.SupplierPortal.Reports.Filters;
using Affinion.LoyaltyBuild.SupplierPortal.DataAccess;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;
using Affinion.LoyaltyBuild.SupplierPortal.DataAccess.Service;
using Affinion.LoyaltyBuild.SupplierPortal.DataAccess.Data;
using System.Collections.Generic;
using Affinion.LoyaltyBuild.SupplierPortal.Reports.Service;
using System.Security;
using System.Security.Permissions;
using Sitecore.Data.Items;
using Affinion.LoyaltyBuild.Common.Utilities;
#endregion

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Reports
{
    /// <summary>
    /// Revenue report sublayout user control
    /// </summary>
    public partial class RevenueReport : ReportBase
    {
        protected readonly IOfferService _offerService;

        public RevenueReport()
            : base(new ProviderService(), new ReportService())
        {
            _offerService = new OfferService();

        }

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            BindsitecoreText();
            dateFromCalenderExtender.StartDate = DateTime.Now.AddMonths(-3);
            dateToCalenderExtender.EndDate = DateTime.Now.AddMonths(12);
            if (!Page.IsPostBack && !Page.IsCallback)
            {
                BindClient();
                BindProviderGroup();

            }
        }

        /// <summary>
        /// Show report click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnListRevenue_Click(object sender, EventArgs e)
        {
            DateTime Datefrom = Convert.ToDateTime(txtDateFrom.Text);
            DateTime DateTo = Convert.ToDateTime(txtDateTo.Text);


            if (Datefrom <= DateTo)
            {
                TimeSpan daycount = Convert.ToDateTime(DateTo).Subtract(Convert.ToDateTime(Datefrom));
                int dacount1 = Convert.ToInt32(daycount.Days);
                if ((dacount1 < 366))
                {
                    //get the report assembly to load report definition of rdlc files
                    Assembly assembly = Assembly.GetAssembly(typeof(Affinion.LoyaltyBuild.SupplierPortal.Reports.IReportService));
                    //get date from
                    DateTime? dateFrom = GetDateValue(txtDateFrom.Text);
                    //get date to
                    DateTime? dateTo = GetDateValue(txtDateTo.Text);
                    //get offer id
                    int? offerId = GetIntValue(ddlOfferName.SelectedValue);
                    //get provider id
                    int? providerId = GetIntValue(ddlProvider.SelectedValue);
                    //get partner name
                    var partner = GetStringValue(ddlPartner.SelectedValue);
                    List<String> partnersplited = new List<String>(partner.Split("{}".ToCharArray()));
                    string partnersplitednew = partnersplited[1];
                    //get Provider group
                    string providerGroup = GetStringValue(ddlProviderGroup.SelectedValue);
                    //get Date type
                    string dateType = GetStringValue(ddlDateType.SelectedValue);
                    //get Breakdown by Campaign              
                    bool breakdownbyCampaign = BreakdownCheckbox.Checked;
                    //get breakdown
                    string breakdown = GetStringValue(rbBreakDown.SelectedValue);

                    var filter = new RevenueReportFilter()
                        {
                            DateFrom = dateFrom,
                            DateTo = dateTo,
                            Provider = providerId,
                            Partner = partnersplitednew,
                            OfferName = offerId,
                            ProviderGroup = providerGroup,
                            DateType = dateType,
                            BreakdownbyCampaign = breakdownbyCampaign,
                            Breakdown = breakdown
                        };
                    CreateRevenueReport(_reportService.GetRevenueReport(filter), assembly, breakdownbyCampaign, breakdown);
                    CreateRevenueByProviderReport(_reportService.GetRevenueReportbyProvider(filter), assembly, breakdownbyCampaign, breakdown);
                }
                else
                {
                    cusValDateTo.IsValid = false;

                }
            }
        }

        private void CreateRevenueByProviderReport(Affinion.LoyaltyBuild.SupplierPortal.Reports.Data.RevenueReportbyProviderCollection data, Assembly assembly, bool breakdownbyCampaign, string breakdown)
        {
            using (var stream = assembly.GetManifestResourceStream("Affinion.LoyaltyBuild.SupplierPortal.Reports.Reports.RevenueReportListRevenue.rdlc"))
            {
                var objShowRecordbyProvider = data;
                this.rvListOfRevenue.KeepSessionAlive = false;
                this.rvListOfRevenue.AsyncRendering = false;
                rvListOfRevenue.ProcessingMode = ProcessingMode.Local;
                rvListOfRevenue.LocalReport.SetBasePermissionsForSandboxAppDomain(new PermissionSet(PermissionState.Unrestricted));
                rvListOfRevenue.LocalReport.LoadReportDefinition(stream);
                var datasourcebyProvider = new ReportDataSource("DataSetRevenueReportProvider", objShowRecordbyProvider);
                rvListOfRevenue.LocalReport.SetParameters(new ReportParameter("RowCount", objShowRecordbyProvider.Count.ToString()));
                rvListOfRevenue.LocalReport.SetParameters(new ReportParameter("breakdown", breakdown));
                rvListOfRevenue.LocalReport.SetParameters(new ReportParameter("ShowOffer", breakdownbyCampaign.ToString()));
                rvListOfRevenue.LocalReport.DataSources.Clear();
                rvListOfRevenue.LocalReport.DataSources.Add(datasourcebyProvider);
            }
        }

        private void CreateRevenueReport(Affinion.LoyaltyBuild.SupplierPortal.Reports.Data.RevenueReportCollection data, Assembly assembly, bool breakdownbyCampaign, string breakdown)
        {
            //Booking tranferred in report
            using (var stream = assembly.GetManifestResourceStream("Affinion.LoyaltyBuild.SupplierPortal.Reports.Reports.RevenueReportTotalRevenue.rdlc"))
            {
                var objShowRecord = data;
                this.rvTotalRevenue.KeepSessionAlive = false;
                this.rvTotalRevenue.AsyncRendering = false;
                rvTotalRevenue.ProcessingMode = ProcessingMode.Local;
                rvTotalRevenue.LocalReport.SetBasePermissionsForSandboxAppDomain(new PermissionSet(PermissionState.Unrestricted));
                rvTotalRevenue.LocalReport.LoadReportDefinition(stream);
                var datasource = new ReportDataSource("RevenueReportTotalRevenue", objShowRecord);
                rvTotalRevenue.LocalReport.SetParameters(new ReportParameter("RowCount", objShowRecord.Count.ToString()));
                rvTotalRevenue.LocalReport.SetParameters(new ReportParameter("breakdown", breakdown));
                rvTotalRevenue.LocalReport.SetParameters(new ReportParameter("ShowOffer", breakdownbyCampaign.ToString()));
                rvTotalRevenue.LocalReport.DataSources.Clear();
                rvTotalRevenue.LocalReport.DataSources.Add(datasource);
            }
        }

        /// <summary>
        /// Provider dropdown change event to load the offers based on provider
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlProvider_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindOffers();
        }

        protected void ddlProviderGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindProviders();
        }

        #region Private Methods

        /// <summary>
        /// Binds the client list dropdown
        /// </summary>
        private void BindClient()
        {
            var clients = _providerService.GetClientList();
            BindDropdown(ddlPartner, clients, "Name", "Id");
        }

        /// <summary>
        /// Binds the provider group dropdown
        /// </summary>
        private void BindProviderGroup()
        {
            switch (UserHelper.CurrentUserRole)
            {
                case Configuration.SupplierPortal.User.UserRole.LoyaltyAdmin:
                    var groups = _providerService.GetAllProviderGroups();
                    BindDropdown(ddlProviderGroup, groups, "Name", "SitecoreId");
                    ddlProviderGroup.Items.Insert(0, new ListItem { Text = "-All Groups-", Value = "0" });
                    break;
                case Configuration.SupplierPortal.User.UserRole.SupplierGroupAdmin:
                    string loggedInProvider = SitecoreUserProfile.GetItem(UserProfileKeys.loggedInProviderId);
                    
                    if (!string.IsNullOrWhiteSpace(loggedInProvider))
                    {
                        var group = _providerService.GetProviderGroupById(loggedInProvider);
                        ddlProviderGroup.Items.Insert(0, new ListItem { Text = group.Name, Value = group.Id.ToString() });
                    }
                    else
                    {
                        ddlProviderGroup.Items.Insert(0, new ListItem { Text = "-All Groups-", Value = "-1" });
                    }
                    ddlProviderGroup.Enabled = false;
                    break;
                case Configuration.SupplierPortal.User.UserRole.Supplier:
                    ddlProviderGroup.Enabled = false;
                    break;
            }
            BindProviders();
        }

        /// <summary>
        /// Binds the providers drop down
        /// </summary>
        private void BindProviders()
        {
            string loggedInProvider = SitecoreUserProfile.GetItem(UserProfileKeys.loggedInProviderId);
            switch (UserHelper.CurrentUserRole)
            {
                case Configuration.SupplierPortal.User.UserRole.LoyaltyAdmin:
                case Configuration.SupplierPortal.User.UserRole.SupplierGroupAdmin:
                    
                    var groupid = ddlProviderGroup.SelectedValue;
                    List<Provider> providers;
                    if (groupid.Length == 1)
                        providers = _providerService.GetAllProviders();
                    else
                        providers = !string.IsNullOrWhiteSpace(loggedInProvider) ? _providerService.GetProvidersByHotelGroup(loggedInProvider) : null;

                    BindDropdown(ddlProvider, providers, "Name", "SitecoreId");
                    if (!Page.IsPostBack)
                    {
                        ddlProvider.Items.Insert(0, new ListItem { Text = "-All Providers-", Value = "0" });
                    }


                    break;
                case Configuration.SupplierPortal.User.UserRole.Supplier:
                    var sku = SitecoreUserProfile.SelectedSKU;
                    
                    if (!String.IsNullOrWhiteSpace(sku))
                    {
                        var provider = _providerService.GetProviderById(sku);
                        ddlProvider.Items.Insert(0, new ListItem { Text = provider.Name, Value = provider.SitecoreId });
                        ddlProvider.Enabled = false;
                    }
                    break;
            }
             BindOffers();
        }

        /// <summary>
        /// Binds the offers dropdown
        /// </summary>
        private void BindOffers()
        {
           // var revenueoffer = _offerService.GetParticipatingOffersByProvider(int.Parse(ddlProvider.SelectedValue));
            // BindDropdown(ddlOfferName, revenueoffer, "Name", "Id");
            string productSku=string.Empty;
            if (!String.IsNullOrWhiteSpace(ddlProvider.SelectedValue) && ddlProvider.SelectedValue.Length>1)
            {
                Item selectedProviderItem=Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(ddlProvider.SelectedValue));
                if(selectedProviderItem!=null)
                {
                    productSku=SitecoreFieldsHelper.GetItemFieldValue(selectedProviderItem,"SKU");
                }
            }

            if (!string.IsNullOrWhiteSpace(productSku))
            {
                ddlOfferName.DataSource = _offerService.GetParticipatingOffersByProvider(productSku);
                ddlOfferName.DataTextField = "Name";
                ddlOfferName.DataValueField = "Id";
                ddlOfferName.DataBind();
                ddlOfferName.Items.Insert(0, new ListItem { Text = "-All Offers-", Value = "0" });
            }

        }

        public void BindsitecoreText()
        {
            Item currentPageItem = Sitecore.Context.Item;
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Title", RevenueReportTitle);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "ReportIformation", AvailabityReportInformation);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "DateFrom", datefromText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "DateTo", dateToText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "OfferName", OfferNameText);


            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Partner", ParterText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Provider", ProviderText);

            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "ProviderGroup", ProviderGroupText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "DateType", DateTypeText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "CheckBoxCampaign", CampaignText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "BreakDownRequired", breakdownRequired);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Submit", btnLstRevenue);

        }

        #endregion

    }
}