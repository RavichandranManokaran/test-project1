﻿<%@ control language="C#" autoeventwireup="true" codebehind="ChangePassword.ascx.cs" inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Secure.ChangePassword" %>
<%@ import namespace="Sitecore.Configuration" %>
<%@ import namespace="Sitecore.SecurityModel.License" %>

<link rel="stylesheet" href="/Resources/styles/bootstrap.min.css" />
<link rel="stylesheet" href="/Resources/styles/theme1.css" />
<title>Reset Password</title>
<div id="ResetPassword" class="login-form-body"></div>
<div class="container">
    <div class="login-form-body-inner">
        <div class="login-form-body-inner-title">
            <h3>Change Password</h3>
        </div>
        <div class="login-form-body-inner-form" id="changePasswordBody" runat="server">


            <div class="col-md-12" id="OldPassword">
                <span>Old Password :</span>
                <asp:textbox runat="server" id="OldPasswordText" textmode="Password" cssclass="form-control"></asp:textbox>
                <asp:requiredfieldvalidator id="RequiredFieldOldPassword" runat="server" controltovalidate="OldPasswordText" errormessage="*Required" forecolor="Red"></asp:requiredfieldvalidator>

            </div>
            <div class="col-md-12" id="NewPassword">
                <span>New Password :</span>
                <asp:textbox id="NewPasswordText" runat="server" cssclass="form-control" textmode="Password"></asp:textbox>
                <asp:requiredfieldvalidator id="RequiredFieldNewPassword" runat="server" controltovalidate="NewPasswordText" errormessage="*Required" forecolor="Red"></asp:requiredfieldvalidator>

            </div>
            <div class="col-md-12" id="ConfirmPassword">
                <span>Confirm Password : </span>
                <asp:textbox id="ConfirmPasswordText" runat="server" cssclass="form-control" textmode="Password"></asp:textbox>
                <asp:requiredfieldvalidator id="RequiredFieldConfirmPassword" runat="server" controltovalidate="ConfirmPasswordText" errormessage="*Required" forecolor="Red"></asp:requiredfieldvalidator>
            </div>
            <asp:button id="btnSubmit" runat="server" onclick="btnSubmit_Click" text="Submit" cssclass="btn btn-primary btn-bg" style="height: 26px" />
            <asp:label runat="server" id="DisplayLabelMessage" CssClass="err_msg"></asp:label>



        </div>
        <div class="login-form-body-inner-form pwd_succs" id="continueToClickDiv" visible="false" runat="server">
            <a href="<%=SCHomePageUrl%>"><%=SCClickHereToContinue %></a>
        </div>
        <asp:label runat="server" id="ErrorMessage"></asp:label>
        <div class="col-md-12">&nbsp;</div>
    </div>

</div>


