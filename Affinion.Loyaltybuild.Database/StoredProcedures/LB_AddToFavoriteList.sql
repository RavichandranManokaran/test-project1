﻿CREATE PROCEDURE [dbo].[LB_AddToFavoriteList] (@UserEmail varchar(50),
						  @ListName varchar(100),
						  @date datetime)
AS
BEGIN
	INSERT INTO [dbo].[LB_FavoriteList]
     VALUES
           ((select CustomerID from uCommerce_Customer where EmailAddress=@UserEmail),
			@ListName,NULL,
			(select FirstName from uCommerce_Customer where EmailAddress=@UserEmail),
     	    (select FirstName from uCommerce_Customer where EmailAddress=@UserEmail),
			 @date,
		     @date)

END

