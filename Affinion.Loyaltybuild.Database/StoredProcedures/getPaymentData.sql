﻿create procedure [dbo].[getPaymentData]
as
begin
select distinct po.OrderGuid, po.SubTotal, cur.ISOCode [Currency], os.Name [Order Status], rr.BookingThrough [Booking Method] from uCommerce_PurchaseOrder po
inner join uCommerce_OrderLine ol on po.OrderId = ol.OrderId
inner join uCommerce_Currency cur on po.CurrencyId = cur.CurrencyId
inner join uCommerce_OrderStatus os on po.OrderStatusId = os.OrderStatusId
inner join uCommerce_RoomReservation rr on ol.OrderLineId = rr.OrderLineId
where po.OrderStatusId=6
end