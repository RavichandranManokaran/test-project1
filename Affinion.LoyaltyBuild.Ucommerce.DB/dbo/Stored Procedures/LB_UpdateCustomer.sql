﻿CREATE PROCEDURE [dbo].[LB_UpdateCustomer](
	@CustomerId int,
	@FirstName nvarchar(512) = null,
	@LastName nvarchar(512) = null,
	@EmailAddress nvarchar(255) = null,
	@Phone nvarchar(50)=Null,
	@MobilePhone nvarchar(50) = null,
	@LanguageName nvarchar(255)=null,
	@ClientId varchar(50) = null,
	@Title varchar(50)=Null,
	@AddressLine1 nvarchar(50),
	@AddressLine2 nvarchar(50)=Null,
	@AddressLine3 nvarchar(50)=Null,
	@CountryId int,
	@LocationId VARCHAR(50),
	@BranchId VARCHAR(50)=Null, 
	@Subscribe int, 
	@Active int,
	@SubscribeType varchar(50)=null,
	@Store varchar(50)=Null,
    @TermsandCondition bit=1)	
AS
BEGIN
	/*BookingReferenceId should not get updated*/

	BEGIN TRANSACTION
	/*Update uCommerce_Customer table with edited data*/
	UPDATE [dbo].uCommerce_Customer  
		SET FirstName=@FirstName,
		LastName = @LastName,
		EmailAddress = @EmailAddress,
		PhoneNumber = @Phone,
		MobilePhoneNumber = @MobilePhone,
		LanguageName = @LanguageName
		Where CustomerId = @CustomerId

	   /*If an Error Occurs Rollback Add*/
	   If @@ERROR<>0
	   BEGIN
		ROLLBACK
		RETURN
	   END
	
	  /*Update LB_CustomerProperty table with edited data*/

	  Update uCommerce_Customer set  LanguageName = @LanguageName Where CustomerId = @CustomerId
	  UPDATE [dbo].LB_CustomerProperty SET [Value]= @ClientId	  where [Key]='_ClientId'	  AND CustomerId=@CustomerId
	  UPDATE [dbo].LB_CustomerProperty SET [Value]= @Title	      where [Key]='_Title'		  AND CustomerId=@CustomerId
	  UPDATE [dbo].LB_CustomerProperty SET [Value]= @AddressLine1 where [Key]='_AddressLine1' AND CustomerId=@CustomerId
	  UPDATE [dbo].LB_CustomerProperty SET [Value]= @AddressLine2 where [Key]='_AddressLine2' AND CustomerId=@CustomerId
	  UPDATE [dbo].LB_CustomerProperty SET [Value]= @AddressLine3 where [Key]='_AddressLine3' AND CustomerId=@CustomerId
	  UPDATE [dbo].LB_CustomerProperty SET [Value]= @CountryId	  where [Key]='_CountryId'	  AND CustomerId=@CustomerId
	  UPDATE [dbo].LB_CustomerProperty SET [Value]= @LocationId   where [Key]='_LocationId'	  AND CustomerId=@CustomerId
	  UPDATE [dbo].LB_CustomerProperty SET [Value]= @BranchId	  where [Key]='_BranchId'	  AND CustomerId=@CustomerId
	  UPDATE [dbo].LB_CustomerProperty SET [Value]= @Subscribe	  where [Key]='_Subscribe'	  AND CustomerId=@CustomerId
	  UPDATE [dbo].LB_CustomerProperty SET [Value]= @Active		  where [Key]='_Active'		  AND CustomerId=@CustomerId
	  UPDATE [dbo].LB_CustomerProperty SET [Value]= @SubscribeType where [Key]='_SubscribeType'  AND CustomerId=@CustomerId 
      UPDATE [dbo].LB_CustomerProperty SET [Value]= @Store          where [Key]='_Store'          AND CustomerId=@CustomerId
      UPDATE [dbo].LB_CustomerProperty SET [Value]= @TermsandCondition where [Key]='_TermsandCondition' AND CustomerId=@CustomerId
	  UPDATE [dbo].LB_CustomerProperty SET [Value]= @LanguageName where [Key]='_LanguageName' AND CustomerId=@CustomerId


	  /*If an Error Occurs Rollback Add*/
	  If @@ERROR<>0
	   BEGIN
		ROLLBACK
		RETURN
	   END

	COMMIT
END
