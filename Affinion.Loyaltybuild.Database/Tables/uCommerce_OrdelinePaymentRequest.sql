

CREATE TABLE [dbo].[uCommerce_OrdelinePaymentRequest](
	[OrdelinePaymentRequestID] [int] IDENTITY(1,1) NOT NULL,
	[OrderLinePaymentID] [int] NOT NULL,
	[ISProviderPaymentRequested] [bit] NULL,
	[DateProviderPaymentRequested] [datetime] NULL,
	[SuggestedProviderPaymentAmount] [money] NULL,
	[ActualProviderPaymentAmount] [money] NULL,
	[ProviderPaymentRequestedBy] [nvarchar](50) NULL,
	[DateProviderPaid] [datetime] NULL,
	[IsProviderPaid] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[OrdelinePaymentRequestID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[uCommerce_OrdelinePaymentRequest]  WITH CHECK ADD  CONSTRAINT [fk_OrderLinePaymentID] FOREIGN KEY([OrderLinePaymentID])
REFERENCES [dbo].[uCommerce_OrderLinePayment] ([OrderLinePaymentID])
GO

ALTER TABLE [dbo].[uCommerce_OrdelinePaymentRequest] CHECK CONSTRAINT [fk_OrderLinePaymentID]
GO


