

/****** Object:  StoredProcedure [dbo].[LB_BB_GetProviderDetails]    Script Date: 7/19/2016 7:26:22 PM ******/

CREATE procedure [dbo].[LB_BB_GetProviderDetails] (@country varchar(max), @location varchar(max), @maplocation varchar(max), @providerName varchar(max))
as
begin
select DISTINCT availabilityProviderRecordID [propertyId], providerName [ProviderName], concat(addressline1, ',', addressline2) [address], addressline3 [mapLocation], addressline4 [location], addressline5 [country] from LB_BB_provider 
where 
(@country='' OR addressline5 like '%'+@country +'%' )
AND (@location='' OR addressline4 like '%'+@location+'%') 
AND (@maplocation='' OR addressline3 like '%'+@maplocation+'%')
AND (@providerName='' OR providerName like '%'+@providerName+'%')
end



GO

