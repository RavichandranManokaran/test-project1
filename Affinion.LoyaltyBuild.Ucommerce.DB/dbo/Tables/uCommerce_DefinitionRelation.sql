﻿CREATE TABLE [dbo].[uCommerce_DefinitionRelation] (
    [DefinitionRelationId] INT IDENTITY (1, 1) NOT NULL,
    [DefinitionId]         INT NOT NULL,
    [ParentDefinitionId]   INT NOT NULL,
    [SortOrder]            INT CONSTRAINT [DF_uCommerce_DefinitionRelation_SortOrder] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_uCommerce_DefinitionRelation] PRIMARY KEY CLUSTERED ([DefinitionRelationId] ASC)
);

