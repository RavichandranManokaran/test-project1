﻿//<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion Inernational. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           ClientPortalTopbarSublayout.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal
/// Description:           Used to Load Hotel related details to Basket Page
/// </summary>
namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal
{
    #region Using Directives
    using Affinion.Loyaltybuild.BusinessLogic.Helper;
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using System;
    #endregion

    public partial class ClientPortalTopbarSublayout : BaseSublayout
    {
        public string BasketPageNavigation
        {
            get
            {
                if (BasketHelper.GetBasketCount() > 0)
                    return "/Basket";
                else
                    return "#";
            }
        }

        private void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            try
            {
                Uri pageUri = Request.Url;
                // Hide basket count when payment page loads
                if (pageUri.ToString().Contains("paymentgateway") || pageUri.ToString().Contains("BookingConfirmation"))
                {
                    basketico.Visible = false;
                    basketicorespodive.Visible = false;
                    DivSignInPay.Visible = false;
                }
                else
                {
                    basketico.Visible = true;
                    basketicorespodive.Visible = true;
                    DivSignInPay.Visible = true;
                }

                if (Session["listBasketInfo"] != null)
                {                  

                    int basketCountcount = BasketHelper.GetBasketCount();                    

                    if (basketCountcount > 0)
                    {
                        this.basketCount.Text = basketCountcount.ToString();
                        this.basketCountMobile.Text = basketCountcount.ToString();
                        DivBasketMessage.Visible = false;
                    }
                    else
                    {
                        DisplayEmptyBasketMessage();
                        this.basketCount.Text = "0";
                        this.basketCountMobile.Text = "0";
                    }
                }
                else
                {
                    // clear the cart when session time out
                    BasketHelper.ClearBasket();
                    this.basketCount.Text = "0";
                    this.basketCountMobile.Text = "0";
                    DisplayEmptyBasketMessage();
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, this);
                throw;
            }
        }

        private void DisplayEmptyBasketMessage()
        {
            BasketEmptyMsg.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "EmptyBasketMessage");
        }

        /// <summary>
        /// Login button event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void ButtonLogin_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if ((TextUsername.Text.Trim().Length != 0)
        //            && Sitecore.Security.Authentication.AuthenticationManager.Login(
        //            string.Concat(Sitecore.Context.Domain.Name,
        //            "\\",
        //            TextUsername.Text.Trim()),
        //            TextPassword.Text.Trim()))
        //        {
        //            // success!
        //            //Sitecore.Web.WebUtil.Redirect("/");
        //            DivWelcome.Visible = true;
        //            SignIn.Visible = false;
        //            DivLogOff.Visible = true;
        //            DivWelcome.InnerHtml = string.Concat(@"<i class=\"" fa fa-user \""></i> Welcome ", TextUsername.Text);
        //        }
        //        else
        //        {
        //            throw new System.Security.Authentication.AuthenticationException(
        //            "Invalid username or password.");
        //        }
        //    }
        //    catch (Exception exception)
        //    {
        //        Diagnostics.WriteException(DiagnosticsCategory.Common, exception, this);
        //        throw;
        //    }
        //}

        //protected void LblLogOut_Click(object sender, EventArgs e)
        //{
        //    Sitecore.Context.Logout();
        //    Response.Redirect(Request.Url.AbsoluteUri);
        //}
    }
}