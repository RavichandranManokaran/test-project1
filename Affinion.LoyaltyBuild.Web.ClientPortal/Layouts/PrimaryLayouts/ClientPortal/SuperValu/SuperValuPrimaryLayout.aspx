﻿<%@ Page Language="c#" CodePage="65001" AutoEventWireup="true" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.PrimaryLayouts.SuperValuPrimaryLayout" CodeBehind="SuperValuPrimaryLayout.aspx.cs" %>

<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ OutputCache Location="None" VaryByParam="none" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="CODE_LANGUAGE" content="C#" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <sc:VisitorIdentification runat="server" />

    <script src="/Resources/scripts/jquery.min.js" type="text/javascript"></script>
    <script src="/Resources/bootstrap_assets/javascripts/bootstrap.min.js" type="text/javascript"></script>

    <link rel="stylesheet" href="/Resources/styles/bootstrap.min.css"/>
    <link rel="stylesheet" href="/Resources/styles/affinion.min.css"/>
    <link rel="stylesheet" href="/Resources/styles/theme2/theme2.css"/>
    <link rel="stylesheet" href="/Resources/styles/affinion.custom.css"/>
    <link rel="stylesheet" href="/Resources/styles/sitecore-custom.css"/>
</head>
<body>
    <form method="post" runat="server" id="mainform">

        <div class="container-outer width-full overflow-hidden-att">

            <sc:Placeholder ID="topbar" Key="topbar" runat="server" />

            <sc:Placeholder ID="topnavigation" Key="topnavigation" runat="server" />

            <sc:Placeholder ID="bookingbar" Key="bookingbar" runat="server" />

            <sc:Placeholder ID="slider" Key="slider" runat="server" />

            <sc:Placeholder ID="content" Key="content" runat="server" />

            <!-- Site footer -->
            <div class="row footer-bottom margin-common">
                <footer class="footer">

                    <sc:Placeholder ID="footer" Key="footer" runat="server" />

                </footer>
            </div>
        </div>

    </form>
</body>
</html>
