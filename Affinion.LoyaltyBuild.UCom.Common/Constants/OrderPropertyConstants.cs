﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.Common.Constants
{
    public static class OrderPropertyConstants
    {
        public const string NightKey = "_Nights";
        public const string NoOfAdultKey = "_NoOfAdult";
        public const string NoOfChildKey = "_NoOfChildren";
        public const string ReservationDateKey = "ReservationDate";
        public const string ArrivalDateKey = "_CheckinDate";
        public const string RefOrderId = "RefOrderId";

        public const string FreeSku = "_freesku";
        public const string FreeSkuApplied = "_freeskuApplied";

        public const string UpgradeAvailable = "_upgradeavaiable";
        public const string UpgradeMessage = "_upgrademessage";
        public const string UpgradeSku = "_upgradesku";

        public const string IgnorInSearch = "_ignorsearch";

        public const string ForSearch = "_forsearch";

        public const string AppliedCampaign = "_appliedcampaign";

        public const string UserSelectedCampaign = "_userselectedcampaign";

        public const string OrderLinRestrictionCampaign = "_restrictedOrderLine";

        public const string IsPaidInFull = "_isPaidInFull";

        public const string AppliedAward = "_appliedaward";

        /// <summary>
        /// If campaign is valid return true for all act
        /// </summary>
        public const string IgnorCampaignSearchCriteria = "_ignorcampaignsearchcriteria";

        public const string SubrateAward = "_subrateAwrd";

       static public readonly string  PaymentMethod = "PaymentMethod";
      static public readonly string PaymentType = "PaymentType";
        static public readonly string CheckInDate = "CheckInDate";
        static public readonly string ProviderAmountPaidCheckOut = "ProviderAmountPaidOnCheckOut";
        static public readonly string IsProcessingFeeCharged = "IsProcessingFeeCharged";
        static public readonly string CommissionFee = "_LBCommissionFee";
        static public readonly string CancellationFee = "_CancellationFee";
        #region partial payment info
        static public readonly string PercentageOfAmount = "PercentageOfAmount";
        static public readonly string DaysBefore = "DaysBefore";
        static public readonly string ProcessingFee = "ProcessingFee";
        

        #endregion

        public const string ClientId = "ClientId";

        public const string HotelType = "HotelType";
        public const string HotelRating = "HotelRating";
        public const string Location = "HotelLocation";
        public const string RoomType = "RoomType";
        static public readonly string AddonType = "AddOn";

        public const string PackageType = "PackageType";

    }
}
