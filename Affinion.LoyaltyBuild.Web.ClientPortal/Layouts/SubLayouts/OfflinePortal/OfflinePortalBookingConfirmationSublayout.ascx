﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalBookingConfirmationSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalBookingConfirmationSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Sitecore.Data.Items" %>
<%@ Import Namespace="Affinion.LoyaltyBuild.Common.SessionHelper" %>
<script>
    jQuery(document).ready(function () {
        jQuery("table tr:even").css({ "background-color": "#fafefe", "border-top": "solid 1px #cff2f6", "border-bottom": "solid 1px #cff2f6" });
        jQuery("table tr:odd").css("background-color", "#fff");
        jQuery(".sec-payment").click(function () {
            jQuery(".hide-div").show();
            jQuery(".vau-personal-card").hide();
        });

        // window.parent.document.title = '<%=pageTitle%>';
        // $("ol.breadcrumb li.active", window.parent.document).html('<%=pageTitle %>');
        document.title = '<%=pageTitle%>';
        $("li.active").text(<%=pageTitle %>);
    });

    $(".btnprint").live("click", function () {
        var divContents = $(".booking-confirmation").html();
        var printWindow = window.open();
        printWindow.document.write('<html><head><title>Booking Confirmation</title>');
        printWindow.document.write('</head><body >');
        printWindow.document.write(divContents);
        printWindow.document.write('</body></html>');
        printWindow.document.close();
        printWindow.print();
    });

    <%--$(".btnViewPymt").live("click", function () {        
        window.parent.location.href = '<%= this.redirectString %>';        
    });--%>
       
    
  

</script>
<asp:HiddenField ID="siteurlfield" runat="server"></asp:HiddenField>
<input type="hidden" id="hfBreadcrumb" runat="server" class="hfBreadcrumb" value="">

<asp:Panel ID="fullPymtPanel" runat="server" Visible="true" CssClass="stockSearchPanel">
    <div style="visibility: hidden; margin-top: 60px"></div>
    <div class="alert alert-danger" id="dangerDiv" runat="server">
        <strong>Sorry!</strong> Your booking is rejected.
    <br />
        <div id="backbuttonDiv" class="alert col-md-2">
            <input type="button" class="btn btn-primary booking-margin-top btn-bg pull-left" onclick="goback()" value="Go Back" />
        </div>
    </div>
    <div id="successDiv" runat="server">
         <%-- <div class="alert alert-success">
          <strong>Thank you!</strong> Your booking is now confirmed.
        </div>
        <div class="alert col-md-2">
            <button type="button" class="btnprint btn btn-primary booking-margin-top btn-bg pull-left" onclick="javascript:window.print();">Print</button>
        </div>
        <div class="alert col-md-10">
            Please print this confirmation and present it upon check-in your accommodation. If you have provided us with your e-mail address you will receive an e-mail confirmation within the next few minutes.
        </div>--%>
        <div class="content booking-confirmation">

            <div class="content-body">
                <div class="row">
                    <div class="col-sm-12 vau-personal-card">
                    </div>
                </div>
                <div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="accordian-outer">
                                <div class="panel-group" id="accordion">
                                    <div class="panel panel-default">
                                        <div class="panel-heading bg-primary">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="show-hide-list">Booking Summary</a>
                                            </h4>
                                        </div>
                                        <div id="collapse1" class="panel-collapse collapse in">
                                            <div class="panel-body">
                                                <asp:Repeater ID="PanelRepeater" runat="server" OnItemDataBound="BookingsBound">
                                                    <ItemTemplate>
                                                        <div>
                                                            <div class="content_bg_inner_box summary-detail alert-info">
                                                                <div class="col-sm-12 bask-title">
                                                                    <div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 col-md-12">
                                                                    <div class="hotel-name-detail ">
                                                                        <h2 class="no-margin accent54-f">Booking Reference No :<asp:Literal ID="BookingNo" runat="server"></asp:Literal><%--<%#Eval("BookingReferenceNo") %>--%></h2>
                                                                        <div class="hotel-name-detail-bottom">
                                                                            <p class="no-margin accent54-f "><span>e-mail :
                                                                                <asp:Literal ID="litEmail" runat="server"></asp:Literal></span></p>
                                                                            <p class="accent50-f margin-bottom-15"><span>Please quote your booking reference Number in any contact with your accommodation provider </span></p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12 row col-sm-12">
                                                                    <div class="col-xs-12 col-md-12">
                                                                        <div class="hotel-name-detail">
                                                                            <h2 class="no-margin">
                                                                                <asp:Literal ID="litHotelName" runat="server"></asp:Literal>
                                                                                <asp:Repeater ID="StarRepeater" runat="server">
                                                                                    <ItemTemplate>
                                                                                        <span class="fa fa-star"></span>
                                                                                    </ItemTemplate>
                                                                                </asp:Repeater>
                                                                            </h2>
                                                                            <div class="hotel-name-detail-bottom">
                                                                                <p class="no-margin">
                                                                                    <asp:Literal ID="litHotelAddress" runat="server"></asp:Literal>
                                                                                </p>
                                                                                <p class="no-margin">
                                                                                    <span>Phone :
                                                                                        <asp:Literal ID="litPhone" runat="server"></asp:Literal></span>
                                                                                </p>
                                                                                <p class="no-margin">
                                                                                    <span>e-mail :
                                                                                        <asp:Literal ID="litEmail1" runat="server"></asp:Literal></span>
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="basket-img margin-common-top">
                                                                            <asp:Image ID="hotelImage" runat="server" CssClass="img-responsive img-thumbnail" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <asp:Panel ID="pnlRoom" runat="server" CssClass="content_bg_inner_box alert-info no-padding" Visible="false">
                                                                    <div class="col-md-12 row col-sm-12 no-padding-right">
                                                                        <div class="col-sm-12 no-padding-right">
                                                                            <h2 class="no-margin">Reservation Details</h2>
                                                                        </div>
                                                                        <div class="row margin-row">
                                                                            <div class="col-sm-7"><span class="accent33-f">Room Description:</span></div>
                                                                            <div class="col-sm-5 no-padding"><span>
                                                                                <asp:Literal ID="litRoomDetail" runat="server" /></span></div>
                                                                        </div>
                                                                        <div class="row margin-row">
                                                                            <div class="col-sm-7"><span class="accent33-f">Guest Name:</span></div>
                                                                            <div class="col-sm-5 no-padding"><span>
                                                                                <asp:Literal ID="litGuestName" runat="server" /></span></div>
                                                                        </div>
                                                                        <div class="row margin-row">
                                                                            <div class="col-sm-7"><span class="accent33-f">No of People traveling:</span></div>
                                                                            <div class="col-sm-5 no-padding"><span>
                                                                                <asp:Literal ID="litNoOfPeople" runat="server" /></span></div>
                                                                        </div>
                                                                        <div class="row margin-row">
                                                                            <div class="col-sm-7"><span class="accent33-f">Check-in:</span></div>
                                                                            <div class="col-sm-5 no-padding"><span><asp:Literal ID="litCheckIn" runat="server" /></span></div>
                                                                        </div>
                                                                        <div class="row margin-row">
                                                                            <div class="col-sm-7"><span class="accent33-f">Check-out:</span></div>
                                                                            <div class="col-sm-5 no-padding"><span>
                                                                                <asp:Literal ID="litCheckOut" runat="server" /></span></div>
                                                                        </div>
                                                                        <div class="row margin-row">
                                                                            <div class="col-sm-7"><span class="accent33-f">Special Requests:</span></div>
                                                                            <div class="col-sm-5 no-padding"><span>
                                                                                <asp:Literal ID="litSpecialRequest" runat="server" /></span></div>
                                                                        </div>
                                                                        <div class="row margin-row">
                                                                            <div class="col-sm-7"><span class="accent33-f">Total Payable at Accommodation:</span></div>
                                                                            <div class="col-sm-5 no-padding"><span>
                                                                                <asp:Literal ID="PriceAccommodation" runat="server" /></span></div>
                                                                        </div>
                                                                        <div class="row margin-row">
                                                                            <div class="col-sm-7">
                                                                                <span class="accent33-f">Paid Today including processing fee of
                                                                                    <asp:Literal ID="ProcessingFee" runat="server" />:</span>
                                                                            </div>
                                                                            <div class="col-sm-5 no-padding">
                                                                                <span>
                                                                                    <asp:Literal ID="DepositPayableToday" runat="server" /></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </asp:Panel>
                                                                <asp:Panel ID="pnlBedBanks" runat="server" CssClass="content_bg_inner_box alert-info no-padding" Visible="false">
                                                                    <div class="col-md-7 row col-sm-12 no-padding-right">
                                                                        <div class="col-sm-12 no-padding-right">
                                                                            <h2 class="no-margin">Reservation Details</h2>
                                                                        </div>
                                                                        <div class="row margin-row">
                                                                            <div class="col-sm-7"><span class="accent33-f">Room Description:</span></div>
                                                                            <div class="col-sm-5 no-padding"><span>
                                                                                <asp:Literal ID="litBBRoomDetail" runat="server" /></span></div>
                                                                        </div>
                                                                        <div class="row margin-row">
                                                                            <div class="col-sm-7"><span class="accent33-f">Guest Name:</span></div>
                                                                            <div class="col-sm-5 no-padding"><span>
                                                                                <asp:Literal ID="litBBGuestName" runat="server" /></span></div>
                                                                        </div>
                                                                        <div class="row margin-row">
                                                                            <div class="col-sm-7"><span class="accent33-f">No of People traveling:</span></div>
                                                                            <div class="col-sm-5 no-padding"><span>
                                                                                <asp:Literal ID="litBBNoOfPeople" runat="server" /></span></div>
                                                                        </div>
                                                                        <div class="row margin-row">
                                                                            <div class="col-sm-7"><span class="accent33-f">Check-in:</span></div>
                                                                            <div class="col-sm-5 no-padding"><span><asp:Literal ID="litBBCheckIn" runat="server" /></span></div>
                                                                        </div>
                                                                        <div class="row margin-row">
                                                                            <div class="col-sm-7"><span class="accent33-f">Check-out:</span></div>
                                                                            <div class="col-sm-5 no-padding"><span>
                                                                                <asp:Literal ID="litBBCheckOut" runat="server" /></span></div>
                                                                        </div>
                                                                        <div class="row margin-row">
                                                                            <div class="col-sm-7"><span class="accent33-f">Special Requests:</span></div>
                                                                            <div class="col-sm-5 no-padding"><span>
                                                                                <asp:Literal ID="litBBSpecialRequest" runat="server" /></span></div>
                                                                        </div>
                                                                        <div class="row margin-row">
                                                                            <div class="col-sm-7"><span class="accent33-f">Total Payable at Accommodation:</span></div>
                                                                            <div class="col-sm-5 no-padding"><span>
                                                                                <asp:Literal ID="litBBPriceAccommodation" runat="server" /></span></div>
                                                                        </div>
                                                                        <div class="row margin-row">
                                                                            <div class="col-sm-7">
                                                                                <span class="accent33-f">Paid Today including processing fee of
                                                                                    <asp:Literal ID="litBBProcessingFee" runat="server" />:</span>
                                                                            </div>
                                                                            <div class="col-sm-5 no-padding">
                                                                                <span>
                                                                                    <asp:Literal ID="litBBDepositPayableToday" runat="server" /></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </asp:Panel>
                                                            </div>
                                                        </div>

                                                    </ItemTemplate>
                                                </asp:Repeater>

                                            </div>
                                            <div class="alert alert-danger" id="MailErrorDiv" runat="server" visible="false">
                                                <asp:Literal runat="server" ID="MailErrorLiteral"></asp:Literal>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Panel>

<asp:Panel ID="partialPymtPanel" runat="server" Visible="false" CssClass="stockSearchPanel">
    <div style="visibility: hidden; margin-top: 60px"></div>
    <div class="alert alert-danger" id="partPymtRejectDiv" runat="server" visible="false">
        <strong>Sorry!</strong> Your payment is rejected.
    </div>
    <div id="partPymtSuccessDiv" runat="server" visible="false">
        <div class="alert alert-success">
            <strong>Thank you!</strong> Your payment is confirmed.
        </div>
    </div>
    <div id="PymtHstybuttonDiv" class="alert col-md-2">
        <input type="button" class="btn btn-primary booking-margin-top btn-bg pull-left" onclick='<%= cOrderlineID %>' value="View Payment History" />
    </div>
</asp:Panel>
<script>
    function goback() {
        var url = document.getElementById('<%= siteurlfield.ClientID %>').value;
        var ClientUrl = url;
        window.parent.location.href = ClientUrl + "/basket";
    }

</script>
