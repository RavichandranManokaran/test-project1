﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DisplayOffer.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.Availability.SupplierPortal.Controls.OffersView" %>


<input type="hidden" class="manageOfferDisableButtons" id="manageOfferDisableButtons" runat="server" />
<div id="tooltipcontainer">
    <div class="offers-top" id="offercontent">

        <div class="offers" id="offerspart">
            <h2 class="no-margin">
                <div class="sidebar-title">Promotions Participating On</div>
                <a data-toggle="collapse" href="#offerParticipating" class="offerpart">
                    <span class="glyphicon glyphicon-minus pull-right"></span>
                </a>
            </h2>
            <div id="offerParticipating" class="collapse in">
                <% if(currentOffers.Count(x => x.IsSubscibe)>0){ %>
                <div class=" padding-15 ">
                    <button type="button" class="btn btn-info Manage_btn" onclick="Supplier.ManageAvailability.RedirectToOffer('<%= currentOffers.FirstOrDefault(x=>x.IsSubscibe).CampaignItemId%>')">Manage Promotions </button>
                </div>
                <% } %>

                <ul class="offers-det" id="offerMore1">
                    <% 
                var count = 1;
                foreach (var item in currentOffers.Where(x => x.IsSubscibe))
                { %>

                    <li class="<%=  (count > 5) ? "activeofferli" : ""  %>">
                        <i class="glyphicon glyphicon-ok-sign eye-ic"></i>
                        <a href="javascript:void(0)"  onclick="Supplier.ManageAvailability.RedirectToOffer('<%= item.CampaignItemId %>')" title="<%=item.OfferDescription %>" offername="<%= item.CampaignName %>" id="cmp_<%= item.CampaignItemId%>" clientidmode="Static"><%= item.CampaignName %></a>
                        <a href="javascript:void(0)" class="edit no-margin">
                            <i class="glyphicon glyphicon-edit" onclick="Supplier.ManageAvailability.EditOfferContent(<%= item.CampaignItemId %>,<%= item.SupplierInviteId %>)"></i>
                        </a>
                        <%if(item.ManageBySupplier){ %>
                        <a href="javascript:void(0)" class="edit no-margin">
                            <i class="glyphicon glyphicon-remove-circle" onclick="Supplier.ManageAvailability.RemoveOffer(<%= item.CampaignItemId %>,<%= item.SupplierInviteId %>)"></i>
                        </a>
                        <%} %>
                        
                    </li>

                    <%
                  count++;
                } %>
                </ul>
                <% if (currentOffers.Count(x => x.IsSubscibe) > 5)
           {%>
                <div class=" padding-15  float-right">
                    <button id="btnactiveoffer" type="button" class="btn btnOffer btn-info" isshow="false" onclick="Supplier.ManageAvailability.ShowHideOffer(this,'activeofferli','spanactiveoffer')"><span id="spanactiveoffer" class="glyphicon glyphicon-circle-arrow-down"></span>Show More </button>
                </div>
                <% } %>
                <% if (currentOffers.Count(x => x.IsSubscibe) == 0)
           {%>
                <span>No Promotion available</span>
                <%} %>
            </div>
        </div>
    </div>
    <div class="offers-top">
        <div class="offers" id="offernotpart">
            <h2 class="no-margin">
                <div class="sidebar-title">Promotions <span style="color: red;">NOT</span> Participating On</div>
                <a data-toggle="collapse" href="#offernotParticipating" class="offernotpart">
                    <span class="glyphicon glyphicon-minus pull-right"></span>
                </a>
            </h2>
            <div id="offernotParticipating" class="collapse in">
                <ul class="offers-det no-offer" id="no-offerMore1">
                    <%
                var count1 = 1;
                string data = string.Empty;
                foreach (var item in currentOffers.Where(x => !x.IsSubscibe))
                { 
                    data = string.Empty;
                    if(item.OfferPrice!= null)
                    {
                    data = "<table border='2' cellspacing='0' cellpadding='0' width='100%'>";
                    data += "<tr>";
                    data += "<th>Res. Date From</th>";
                    data += "<th>Res. Date To</th>";
                    data += "<th>Arr. Date From</th>";
                    data += "<th>Arr. Date To</th>";
                        data += "<th>Room Name</th>";
                    data += "<th>Price</th>";
                    data += "</tr>";
                    for (int i = 0; i < item.OfferPrice.Count; i++)
                    {
                        data += "<tr>";
                        data += string.Format("<td>{0}</td>", item.OfferPrice[i].ReservationDate.StartDate);
                        data += string.Format("<td>{0}</td>", item.OfferPrice[i].ReservationDate.EndDate);
                        data += string.Format("<td>{0}</td>", item.OfferPrice[i].ArrivalDate.StartDate);
                        data += string.Format("<td>{0}</td>", item.OfferPrice[i].ArrivalDate.EndDate);
                        data += string.Format("<td>{0}</td>", item.OfferPrice[i].RoomName);
                        data += string.Format("<td>{0}</td>", item.OfferPrice[i].price == -1 ? "NA" : item.OfferPrice[i].price.ToString("00"));
                        data += "</tr>";
                    }
                    data += "</table>";
                    }
                    %>
                    <li class="<%=  (count1 > 5) ? "inactiveofferli" : ""  %>">

                        <a id="A1" clientidmode="Static" href="javascript:void(0)" title="<%=data %>"><%= item.CampaignName %></a>
                        <%if(item.ManageBySupplier){ %>
                        <a href="javascript:void(0)" class="edit">
                          <%--  <i class="glyphicon glyphicon-plus-sign icons-positionRight" onclick="Supplier.ManageAvailability.ChangeCampaignSubscribe(<%= item.SupplierInviteId %>,true)"></i>--%>
                              <i class="glyphicon glyphicon-plus-sign" onclick="Supplier.ManageAvailability.SubscribeForNewOffer(<%= item.CampaignItemId %>,<%= item.SupplierInviteId %>)"></i>

                        </a>
                        <% } %>

                    </li>

                    <%
                    count1++;
                } %>
                </ul>

                <%--<ul class="offers-det no-offer collapse" id="no-offerMore2">
            <li><span>Start BandB City Complex</span><a href="#" class="edit"><i class="glyphicon glyphicon-plus-sign icons-positionRight"></i></a></li>
            <li><span>Start BandB City Complex</span><a href="#" class="edit"><i class="glyphicon glyphicon-plus-sign icons-positionRight"></i></a></li>
            <li><span>Start BandB City Complex</span><a href="#" class="edit"><i class="glyphicon glyphicon-plus-sign icons-positionRight"></i></a></li>
            <li><span>Start BandB City Complex</span><a href="#" class="edit"><i class="glyphicon glyphicon-plus-sign icons-positionRight"></i></a></li>
        </ul>--%>
                <% if (currentOffers.Count(x => !x.IsSubscibe) > 5)
           {%>
                <div class=" padding-15  float-right">
                    <button id="btninactiveoffer" type="button" class="btn no-btnOffer btn-info" isshow="false" onclick="Supplier.ManageAvailability.ShowHideOffer(this,'inactiveofferli','spaninactiveoffer')"><span id="spaninactiveoffer" class="glyphicon glyphicon-circle-arrow-down"></span>Show More </button>
                </div>
                <% } %>
                <% if (currentOffers.Count(x => !x.IsSubscibe) == 0)
           {%>
                <span>No Promotion available</span>
                <%} %>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {

    });
</script>
