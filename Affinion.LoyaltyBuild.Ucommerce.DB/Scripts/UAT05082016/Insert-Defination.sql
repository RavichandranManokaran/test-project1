﻿Declare @datatypeid int, @definitionid int
declare @definationfieldid int = 0
select @datatypeid=DataTypeId from uCommerce_DataType where [DefinitionName]='SiteCoreLocation'
select @definitionid=ProductDefinitionId from uCommerce_ProductDefinition where Name='Hotel'
if (@datatypeid <> 0 and @definitionid <> 0 and not exists (select 1 from uCommerce_ProductDefinitionField where ProductDefinitionId = @definitionid and DataTypeId = @datatypeid and name = 'Location'))
begin
       insert into uCommerce_ProductDefinitionField(DataTypeId,ProductDefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,Facet,Guid,IsVariantProperty)
       values(@datatypeid,@definitionid,'Location',1,0,1,0,0,0,1,NEWID(),0)
	   set @definationfieldid = @@Identity
		insert into uCommerce_productdefinitionFieldDescription(CultureCode,DisplayName,Description,ProductDefinitionFieldId)
		values ('en' , 'Location','',@definationfieldid)
end

if not exists (select 1 from uCommerce_DataType where DefinitionName = 'SiteCoreCommission' )
begin
       insert into uCommerce_DataType (TypeName,Nullable,ValidationExpression,BuiltIn,DefinitionName,Deleted,Guid,CreatedOn,
       CreatedBy,ModifiedOn,ModifiedBy)
       values ('Commission',1,'',0,'SiteCoreCommission',0,NEWID(),GETDATE(),'admin',GETDATE(),'admin')
end


select @datatypeid = DataTypeId from uCommerce_DataType where DefinitionName= 'Boolean'
select @definitionid = DefinitionId from uCommerce_Definition
where Name = 'Default Campaign Item'

if(@datatypeid <> 0)
begin
if not exists(select 1 from uCommerce_DefinitionField where DefinitionId = @definitionid and name = 'isapprove')
begin
insert into uCommerce_DefinitionField(DataTypeId,DefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,BuiltIn,DefaultValue,Guid)
values (@datatypeid , @definitionid,'isapprove',1,0,0,0,0,0,0,'',NEWID())
set @definationfieldid = @@Identity
insert into uCommerce_DefinitionFieldDescription(CultureCode,DisplayName,Description,DefinitionFieldId)
values ('en' , 'isapprove','',@definationfieldid)
end

end

if(@datatypeid <> 0)
begin
if not exists(select 1 from uCommerce_DefinitionField where DefinitionId = @definitionid and name = 'isinvitesend')
begin
insert into uCommerce_DefinitionField(DataTypeId,DefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,BuiltIn,DefaultValue,Guid)
values (@datatypeid , @definitionid,'isinvitesend',1,0,0,0,0,0,0,'',NEWID())
set @definationfieldid = @@Identity
insert into uCommerce_DefinitionFieldDescription(CultureCode,DisplayName,Description,DefinitionFieldId)
values ('en' , 'isinvitesend','',@definationfieldid)
end
end

if not exists(select 1 from uCommerce_DataType where DefinitionName = 'SiteCoreSupplierType')
begin
insert into uCommerce_DataType (TypeName,Nullable,ValidationExpression,BuiltIn,DefinitionName,Deleted,Guid,CreatedOn,
CreatedBy,ModifiedOn,ModifiedBy)
values ('HotelType',1,'',0,'SiteCoreSupplierType',0,NEWID(),GETDATE(),'admin',GETDATE(),'admin')

end
select @definitionid=ProductDefinitionId from uCommerce_ProductDefinition where Name='Hotel'
select @datatypeid = DataTypeId from uCommerce_DataType where DefinitionName= 'SiteCoreSupplierType'
if(@datatypeid <> 0)
begin
if not exists(select 1 from uCommerce_ProductDefinitionField where ProductDefinitionId = @definitionid and name = 'SupplierType')
begin
	   insert into uCommerce_ProductDefinitionField(DataTypeId,ProductDefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,Facet,Guid,IsVariantProperty)
	   values(@datatypeid,@definitionid,'SupplierType',1,0,1,0,0,0,1,NEWID(),0)
	   set @definationfieldid = @@Identity
		insert into uCommerce_ProductDefinitionFieldDescription(CultureCode,DisplayName,Description,ProductDefinitionFieldId)
	    values ('en' , 'SupplierType','',@definationfieldid)
end
end
select @datatypeid = DataTypeId from uCommerce_DataType where DefinitionName= 'ShortText'
if(@datatypeid <> 0)
begin
if not exists(select 1 from uCommerce_ProductDefinitionField where ProductDefinitionId = @definitionid and name = 'GeoCoordinate')
begin
insert into uCommerce_ProductDefinitionField(DataTypeId,ProductDefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,Facet,Guid,IsVariantProperty)
	   values(@datatypeid,@definitionid,'GeoCoordinate',1,0,1,0,0,0,1,NEWID(),0)
	   set @definationfieldid = @@Identity
		insert into uCommerce_ProductDefinitionFieldDescription(CultureCode,DisplayName,Description,ProductDefinitionFieldId)
	    values ('en' , 'GeoCoordinate','',@definationfieldid)
end
end

if(@datatypeid <> 0)
begin
if not exists(select 1 from uCommerce_ProductDefinitionField where ProductDefinitionId = @definitionid and name = 'Latitude')
begin
	   insert into uCommerce_ProductDefinitionField(DataTypeId,ProductDefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,Facet,Guid,IsVariantProperty)
	   values(@datatypeid,@definitionid,'Latitude',1,0,1,0,0,0,1,NEWID(),0)
	   set @definationfieldid = @@Identity
		insert into uCommerce_ProductDefinitionFieldDescription(CultureCode,DisplayName,Description,ProductDefinitionFieldId)
	    values ('en' , 'Latitude','',@definationfieldid)
end
end
if(@datatypeid <> 0)
begin
if not exists(select 1 from uCommerce_ProductDefinitionField where ProductDefinitionId = @definitionid and name = 'Longitude')
begin
	   insert into uCommerce_ProductDefinitionField(DataTypeId,ProductDefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,Facet,Guid,IsVariantProperty)
	   values(@datatypeid,@definitionid,'Longitude',1,0,1,0,0,0,1,NEWID(),0)
	   set @definationfieldid = @@Identity
		insert into uCommerce_ProductDefinitionFieldDescription(CultureCode,DisplayName,Description,ProductDefinitionFieldId)
	    values ('en' , 'Longitude','',@definationfieldid)
end
end

if (@datatypeid <> 0 and @definitionid <> 0 and not exists (select 1 from uCommerce_ProductDefinitionField where ProductDefinitionId = @definitionid and DataTypeId = @datatypeid and name = 'RelatedAddOnSKU'))
begin
       insert into uCommerce_ProductDefinitionField(DataTypeId,ProductDefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,Facet,Guid,IsVariantProperty)
       values(@datatypeid,@definitionid,'RelatedAddOnSKU',1,0,1,0,0,0,1,NEWID(),1)
	   set @definationfieldid = @@Identity
		insert into uCommerce_productdefinitionFieldDescription(CultureCode,DisplayName,Description,ProductDefinitionFieldId)
		values ('en' , 'RelatedAddOnSKU','',@definationfieldid)
end

if not exists(select 1 from uCommerce_DataType where DefinitionName = 'SiteCoreRecommended')
begin
insert into uCommerce_DataType (TypeName,Nullable,ValidationExpression,BuiltIn,DefinitionName,Deleted,Guid,CreatedOn,
CreatedBy,ModifiedOn,ModifiedBy)
values ('Recommended',1,'',0,'SiteCoreRecommended',0,NEWID(),GETDATE(),'admin',GETDATE(),'admin')
end
select @datatypeid = DataTypeId from uCommerce_DataType where [DefinitionName]='SiteCoreRecommended'
if (@datatypeid <> 0 and @definitionid <> 0 and not exists (select 1 from uCommerce_ProductDefinitionField where ProductDefinitionId = @definitionid and DataTypeId = @datatypeid and name = 'Recommended'))
begin
       insert into uCommerce_ProductDefinitionField(DataTypeId,ProductDefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,Facet,Guid,IsVariantProperty)
       values(@datatypeid,@definitionid,'Recommended',1,0,1,0,0,0,1,NEWID(),0)
	   set @definationfieldid = @@Identity
		insert into uCommerce_productdefinitionFieldDescription(CultureCode,DisplayName,Description,ProductDefinitionFieldId)
		values ('en' , 'Recommended','',@definationfieldid)
end
go


/* insert query*/
update ucommerce_productdefinitionfield
set Name = 'Experience'
where Name = 'HotelFacility'

update uCommerce_ProductDefinitionFieldDescription
set Name = 'Experience'
where Name = 'HotelFacility'
Go

update uCommerce_ProductDefinitionField set deleted = 1 where name='Client' 

update uCommerce_ProductDefinitionField set facet = 1 where name='PackageType' 
update uCommerce_ProductDefinitionField set facet = 1 where name='Experience' 
update uCommerce_ProductDefinitionField set facet = 1 where name='Theme' 
update uCommerce_ProductDefinitionField set facet = 1 where name='StarRating' 
update uCommerce_ProductDefinitionField set facet = 1 where name='MinPrice' 
update uCommerce_ProductDefinitionField set facet = 1 where name='MaxPrice' 
update uCommerce_ProductDefinitionField set facet = 1 where name='RoomType' 
update uCommerce_ProductDefinitionField set facet = 1 where name='Recommended' 
update uCommerce_ProductDefinitionField set facet = 1 where name='Facility'

update uCommerce_ProductDefinitionField set facet = 0 where name='FoodType' 
update uCommerce_ProductDefinitionField set facet = 0 where name='Country' 
update uCommerce_ProductDefinitionField set facet = 0 where name='Region' 
update uCommerce_ProductDefinitionField set facet = 0 where name='Location' 
update uCommerce_ProductDefinitionField set facet = 0 where name='AdOnsType' 
update uCommerce_ProductDefinitionField set facet = 0 where name='UpSaleType' 
update uCommerce_ProductDefinitionField set facet = 0 where name='SpaType' 
update uCommerce_ProductDefinitionField set facet = 0 where name='SupperType' 
update uCommerce_ProductDefinitionField set facet = 0  where name='Client' 
update uCommerce_ProductDefinitionField set facet = 0 where name='Groups' 
update uCommerce_ProductDefinitionField set facet = 0 where name='SuperGroups' 
update uCommerce_ProductDefinitionField set facet = 0 where name='SiteCoreItemGuid' 
update uCommerce_ProductDefinitionField set facet = 0 where name='MaxNumberOfAdults' 
update uCommerce_ProductDefinitionField set facet = 0 where name='MaxNumberOfChildren' 
update uCommerce_ProductDefinitionField set facet = 0 where name='MaxNumberOfPeople' 
update uCommerce_ProductDefinitionField set facet = 0 where name='MaxNumberOfInfants' 
update uCommerce_ProductDefinitionField set facet = 0 where name='Ranking' 
update uCommerce_ProductDefinitionField set facet = 0 where name='Description' 
update uCommerce_ProductDefinitionField set facet = 0 where name='IsDeleted' 
update uCommerce_ProductDefinitionField set facet = 0 where name='IsSellableStandAlone' 
update uCommerce_ProductDefinitionField set facet = 0 where name='IsUnlimited' 
update uCommerce_ProductDefinitionField set facet = 0 where name='IsActive' 
update uCommerce_ProductDefinitionField set facet = 0 where name='SupplierType' 
update uCommerce_ProductDefinitionField set facet = 0 where name='Latitude' 
update uCommerce_ProductDefinitionField set facet = 0 where name='Longitude' 
update uCommerce_ProductDefinitionField set facet = 0 where name='GeoCoordinate' 
update uCommerce_ProductDefinitionField set facet = 0 where name='RelatedAddOnSKU'

Go
update uCommerce_DataType
set DefinitionName = 'SiteCoreHotelExperience' , TypeName ='Experience'
where DefinitionName='SiteCoreHotelFacility' and TypeName='HotelFacility'
Go

Declare @datatypeid int, @definitionid int
declare @definationfieldid int = 0

if not exists(select 1 from uCommerce_DataType where DefinitionName = 'SiteCoreHotelFacility')
begin
insert into uCommerce_DataType (TypeName,Nullable,ValidationExpression,BuiltIn,DefinitionName,Deleted,Guid,CreatedOn,
CreatedBy,ModifiedOn,ModifiedBy)
values ('Facility',1,'',0,'SiteCoreHotelFacility',0,NEWID(),GETDATE(),'admin',GETDATE(),'admin')

end
select @definitionid=ProductDefinitionId from uCommerce_ProductDefinition where Name='Hotel'
select @datatypeid = DataTypeId from uCommerce_DataType where DefinitionName= 'SiteCoreHotelFacility'
if(@datatypeid <> 0)
begin
if not exists(select 1 from uCommerce_ProductDefinitionField where ProductDefinitionId = @definitionid and name = 'Facility')
begin
	   insert into uCommerce_ProductDefinitionField(DataTypeId,ProductDefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,Facet,Guid,IsVariantProperty)
	   values(@datatypeid,@definitionid,'Facility',1,0,1,0,0,0,1,NEWID(),0)
	   set @definationfieldid = @@Identity
		insert into uCommerce_ProductDefinitionFieldDescription(CultureCode,DisplayName,Description,ProductDefinitionFieldId)
	    values ('en' , 'Facility','',@definationfieldid)
end
end