﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AvailableLocationsSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.AvailableLocationsSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Sitecore.Data.Items" %>

<div class="col-xs-12 no-padding">
    <div class="detail-info">
        <div class="detail-info-header">
            <div class="row">
                <div class="col-xs-9">
                    <h2 class="details-header no-margin">
                    <sc:Text Field="Title" runat="server" ID="Title" />
                    </h2>
                </div>
                <div class="col-xs-3">
                    <span class="glyphicon glyphicon-map-marker"></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <p> 
                    <sc:Text Field="Description" runat="server" ID="Description" /> 
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="select-loc" aria-label="..." role="group">
                    <%--<select class="form-control" onchange="SelectedLocation(this.value)" id="LocationDropdown" name="LocationDropdown" runat="server">
                    </select>--%>
                    <asp:DropDownList CssClass="form-control" ID="LocationDropdown" EnableViewState="true" runat="server"></asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
</div>
