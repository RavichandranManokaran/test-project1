﻿/****** Object:  Trigger [dbo].[trPasswordUpdate]    Script Date: 15-03-2016 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TRIGGER [dbo].[trPasswordUpdate]
   ON  [dbo].[aspnet_Membership]
   AFTER UPDATE
AS
    BEGIN
         
        INSERT INTO dbo.PasswordHistories 
        (
            UserName, 
            [Password], 
            PasswordSalt,
ChangedDate
        ) 
        SELECT
            (select username from  aspnet_users where userId = deleted.userID),
            deleted.[Password], 
            deleted.PasswordSalt,
GetDate()
        FROM
            deleted INNER JOIN inserted ON deleted.UserId = inserted.UserId
        WHERE
            (deleted.[Password] <> inserted.[Password]) OR (deleted.PasswordSalt <> inserted.PasswordSalt)
    END
GO

