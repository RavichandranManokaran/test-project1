﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    public class Availability
    {
        public int StartDate { get; set; }

        public int EndDate { get; set; }

        public string SKU { get; set; }

        public string VaiantSKU { get; set; }

        public int AvailableRoom { get; set; }

        public decimal Price { get; set; }

        public List<DayWiseAvaialbility> DayWiseAvailability { get; set; }
    }
    public class DayWiseAvaialbility
    {
        public int DateCounter { get; set; }

        public int AvailableRoom { get; set; }

        public decimal Price { get; set; }

        public Guid PriceBand { get; set; }
    }

    public class RoomAvailabilityDetail
    {
        public DateTime AvailabilityDate { get; set; }

        public int DateCounter { get; set; }

        public string SKU { get; set; }

        public string VaiantSKU { get; set; }

        public int AvailableRoom { get; set; }

        public List<OfferAvailabilityDetail> OfferAvailability { get; set; }
    }

    public class OfferAvailabilityDetail
    {
        public string Name { get; set; }

        public int Id { get; set; }

        public int OfferAvailableRoom { get; set; }
    }

    public class OfferAvailability
    {
        public DateTime AvailabilityDate { get; set; }

        public string SKU { get; set; }

        public string Name { get; set; }

        public int Id { get; set; }

        public int DateCounter { get; set; }

        public int OfferAvailableRoom { get; set; }
    }
}
