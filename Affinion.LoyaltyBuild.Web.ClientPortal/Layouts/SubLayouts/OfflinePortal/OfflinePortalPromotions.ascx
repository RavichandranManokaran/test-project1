﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalPromotions.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalPromotions" %>
<%@ import namespace="Affinion.LoyaltyBuild.Web.ClientPortal.ViewModels" %>
<asp:Repeater runat="server" ID="rptPromotions" OnItemDataBound="PromotionsItemDataBound">
    <ItemTemplate>
            <div class="panel panel-default" id="Div1" runat="server">
                <div class="panel-heading panel_sub_heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion_sub" href="#collapse<%#Container.ItemIndex %>_offers" class="show-hide-list width-full inline-block" aria-expanded="true"><%#Eval("CampaignName") %></a>
                    </h4>
                </div>
                <div id="collapse<%#Container.ItemIndex %>_offers" class="panel-collapse collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <div class="detail-outer">
                            <div class="row cs-search-result-bg2 ">
                                <span class="accent50-f font-3 font-bold-600 "><%#Eval("CampaignName") %></span> <span></span>
                            </div>
                            <div></div>
                            <div class="table-responsive no-margin RoomsandPackage">
                                <table class="table table-striped no-margin tbl-calendar font-5">
                                    <thead>
                                        <tr class="">
                                            <th class="text-center font-6">Occupancy Type<span class="accent34-f">&nbsp;</span></th>
                                            <asp:Repeater ID="rptOfferDateHeader" runat="server" DataSource="<%#GetDates()%>">
                                                <ItemTemplate>
                                                    <th class="text-center font-6"><%#((DateTime)Container.DataItem).ToString("ddd") %> <span class="  accent34-f"><%#((DateTime)Container.DataItem).ToString("dd/MM") %></span></th>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="rptOfferRooms" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td class="text-left accent34-f nowrap"><%#Eval("Name") %></td>
                                                    <asp:Repeater ID="rptOfferProduct" runat="server" DataSource="<%#GetOfferAvailability((PromotionProduct)Container.DataItem, ((PromotionInfo)(((RepeaterItem)Container.Parent.Parent.Parent).DataItem)).CampaignItemId)%>" onitemdatabound="PriceItemDataBound">
                                                        <ItemTemplate>
                                                            <td class="text-center availability-item <%#GetDateClass((Affinion.LoyaltyBuild.Web.ClientPortal.ViewModels.PriceAvailabilityByDate)Container.DataItem) %>">
                                                                <span id="spnNoItem" runat="server">N/A</span>
                                                                <span id="spnPastItem" runat="server" data-toggle="modal"><b><%#Eval("Availability") %></b></span>
                                                                <span id="spnFutureItem" runat="server"                                                                    
                                                                    data-sku='<%#Eval("Sku")%>'
                                                                    data-variatsku='<%#Eval("VariantSku")%>'
                                                                    data-availability='<%#Eval("Availability")%>'
                                                                    data-date='<%#Eval("Date")%>'
                                                                    data-promotionid='<%#Eval("PromotionId")%>'	
                                                                    data-checkoutdate='<%#((DateTime)Eval("Date")).AddDays((int)Eval("NoOfNights"))%>'
                                                                    data-noofnights='<%#Eval("NoOfNights")%>'
                                                                    data-roomtype='<%#Eval("Type") %>'
                                                                    data-noofadults='<%#Eval("NoOfAdults")%>'
                                                                    data-noofchildren='<%#Eval("NoOfChildren")%>'
                                                                    data-currencyid='<%#Eval("CurrencyId")%>'                                                                    
                                                                    onclick="LoadPromoInfo(this);">
                                                                    <b><%#Eval("Availability")%></b>
                                                                  </span>
                                                            </td>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </ItemTemplate>
</asp:Repeater>

 