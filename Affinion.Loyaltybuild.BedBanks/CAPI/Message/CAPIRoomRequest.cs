﻿namespace Affinion.Loyaltybuild.BedBanks.CAPI.Message
{
    /// <summary>
    /// This is a class to hold the Room Request Parameters.
    /// It will be passed to the API controller to perform the actual request.
    /// </summary>
    public class CapiRoomRequest
    {
        public string HotelId, SessionId;

        public CapiRoomRequest(string pHotelid, string pSessionid)
        {
            HotelId = pHotelid;
            SessionId = pSessionid;
        }
    }
}