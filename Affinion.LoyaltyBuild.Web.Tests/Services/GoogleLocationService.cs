﻿#region Directives

using Affinion.LoyaltyBuild.Web.Tests.DTO;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using System.Collections.Generic;

#endregion Directives

namespace Affinion.LoyaltyBuild.Web.Tests.Services
{
    /// <summary>
    /// Google Location Service class: Contains service methods to invoke data related to google map geo locations
    /// </summary>   
    public class GoogleLocationService : ILocationService<GoogleLocationDTO>
    {

        #region Constants

        /// <summary>
        /// Constant Value of Latitude
        /// </summary>
        private const string ConstantLatitude = "Latitude";

        /// <summary>
        /// Constant Value of Longitude
        /// </summary>
        private const string ConstantLongitude = "Longitude";


        /// <summary>
        /// Constant value fo Description
        /// </summary>
        private const string ConstantDescription = "Description";

        /// <summary>
        /// Contant value of GetLactions Field Name
        /// </summary>
        private const string ConstantGetLocationFieldName = "GetLocations";

        /// <summary>
        /// Constant value of SubLocations Field Name
        /// </summary>
        private const string ConstantSubLocationsFieldName = "SubLocations";


        /// <summary>
        /// Constant value of Center location Name
        /// </summary>
        private const string ConstantCenterLocationName = "CenterLocation";

        #endregion Constants
        
        #region Private Properties

        /// <summary>
        /// Gets the current Item
        /// </summary>
        private Item GetCurrentItem
        {
            get
            {
                Item locationItem = Sitecore.Context.Database.GetItem(new ID("{6FE6E265-E673-481B-97BF-DD9704CE14B1}"));
                return locationItem;
            }

        }

        /// <summary>
        /// Gets the SubLocation Multi List Field
        /// </summary>
        private MultilistField GetSubLocationMultilistField
        {
            get
            {
                Item locationItem = this.GetCurrentItem;
                //Get the multilist field
                MultilistField locationMultiListField = locationItem.Fields[ConstantSubLocationsFieldName];
                return locationMultiListField;
            }

        }

        /// <summary>
        /// Returns of GetLocaions MulitilistField
        /// </summary>
        private MultilistField GetLocationsMultiListField
        {
            get
            {
                Item locationItem = this.GetCurrentItem;
                //Get the multilist field
                MultilistField locationMultiListField = locationItem.Fields[ConstantGetLocationFieldName];
                return locationMultiListField;
            }
        }



        #endregion Private Properties
        
        #region Private Methods


        /// <summary>
        /// Get the sub location list as a generic object list
        /// </summary>
        /// <param name="centerLocationName">Center Location Name</param>
        /// <returns>List of GoogleLocationDTO objects</returns>
        private List<GoogleLocationDTO> GetSubLocationList(string centerLocationName)
        {
            //Get the sublocation multilist field
            MultilistField subLocationsField = this.GetSubLocationMultilistField;
            List<GoogleLocationDTO> sublocationList = new List<GoogleLocationDTO>();

            //If field is not null
            if (subLocationsField != null)
            {
                //Iterate through each item
                foreach (Item item in subLocationsField.GetItems())
                {
                    //Checks the selected centerlized location is similar to each item center location
                    if (item.Fields[ConstantCenterLocationName].Value.Equals(centerLocationName))
                    {
                        GoogleLocationDTO sublocation = new GoogleLocationDTO();
                        sublocation = this.AddListItemToDTOType(item, sublocation);                        
                        sublocationList.Add(sublocation);
                    }
                }
            }
            return sublocationList;

        }

        /// <summary>
        /// Add sitecore item values to a DTO object type
        /// </summary>
        /// <param name="item">Item: multi list item</param>
        /// <param name="locationDTO">GoogleLocationDTO object type</param>
        /// <returns>returns GoogleLocationDTO object Type</returns>
        private GoogleLocationDTO AddListItemToDTOType(Item item,GoogleLocationDTO locationDTO)
        {
            locationDTO.LocationName = item.Name;
            locationDTO.Latitude = item.Fields[ConstantLatitude].Value;
            locationDTO.Longitude = item.Fields[ConstantLongitude].Value;
            locationDTO.Description = item.Fields[ConstantDescription].Value;
            return locationDTO;
        }


        #endregion Private Methods
        
        #region Public Methods

        /// <summary>
        /// Get the list of Centrelized locations
        /// </summary>
        /// <returns>An instance of GoogleLocationData containing Latitude,Longitude and the location name</returns>
        public List<GoogleLocationDTO> GetLocations()
        {
            List<GoogleLocationDTO> locationList = new List<GoogleLocationDTO>();
            //Get th location Multilistfield
            MultilistField locationMultiListField = this.GetLocationsMultiListField;

            //If location Multilist field is not null
            if (locationMultiListField != null)
            {
                foreach (Item item in locationMultiListField.GetItems())
                {
                    GoogleLocationDTO location = new GoogleLocationDTO();
                    location = this.AddListItemToDTOType(item, location);                    
                    locationList.Add(location);
                }
            }

            //return of the list of centralized locations
            return locationList;
        }



        /// <summary>
        /// Get the Location Details based on the user selected centralized location 
        /// </summary>
        /// <param name="location"></param>
        /// <returns>returns of location details based of the selected location.(Location Name, Latitude and Longitude)</returns>
        public List<GoogleLocationDTO> GetLocations(string location)
        {

            //Filterd Location List
            List<GoogleLocationDTO> filterdLocationList = new List<GoogleLocationDTO>();


            if (!string.IsNullOrEmpty(location))
            {
                //Get all the location data
                List<GoogleLocationDTO> locationList = this.GetLocations();
                
                //Sub locaiton list
                List<GoogleLocationDTO> subLocationList = new List<GoogleLocationDTO>();

                //Iterate through all the locations to get the selected location details
                foreach (GoogleLocationDTO locationItem in locationList)
                {
                    if (locationItem != null)
                    {
                        //If the location name is equal to selected location name
                        if (locationItem.LocationName.Equals(location))
                        {
                            GoogleLocationDTO currentLocationData = new GoogleLocationDTO();
                            currentLocationData.LocationName = locationItem.LocationName;
                            currentLocationData.Latitude = locationItem.Latitude;
                            currentLocationData.Longitude = locationItem.Longitude;
                            currentLocationData.Description = locationItem.Description;
                            filterdLocationList.Add(currentLocationData);

                            subLocationList = this.GetSubLocationList(location);

                            if (subLocationList != null)
                            {
                                filterdLocationList.AddRange(subLocationList);
                            }

                        }
                    }
                }

                //Returns of the list locations based on the selected value.
                return filterdLocationList; 
            }
            else
            {
                return filterdLocationList;
            }
        }

        #endregion Public Methods



        


    
    }
}