﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           ICurrencyConverter.cs
/// Sub-system/Module:     Suplier  Setup(VS project name)
/// Description:           XXX code will be used to add and retrieve rating data………
/// </summary>


#region Using Directives

#endregion


namespace Affinion.LoyaltyBuild.SupplierSetup.CurrencyCoverter
{
   public interface ICurrencyConverter
    {
        //string Token { get; }

       /// <summary>
       /// set culture 
       /// </summary>
       /// <param name="culture"></param>

        //void SetCulture(CultureInfo culture);

        /// <summary>
        /// Convenstion based on float value
        /// </summary>
        /// <param name="value"></param>
        /// 
        //void Convert(float value);

        /// <summary>
        /// 
        /// Convenstion based on float value and string 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="from"></param>
        /// 
        //void Convert(float value, string from);

       
       /// <summary>
        /// Convenstion based on float value and string 
       /// </summary>
       /// <param name="value"></param>
       /// <param name="from"></param>
       /// <param name="to"></param>
       /// <returns></returns>
       /// 
        //decimal Convert(float value, string from, string to);
       
       /// <summary>
        /// Convenstion based on float value and string 
       /// </summary>
       /// <param name="from"></param>
       /// <param name="to"></param>
       /// <param name="token"></param>
       /// <returns></returns>

        decimal Convert(string from, string to);

       /// <summary>
       /// 
       /// </summary>
       /// <param name="value">amount</param>
       /// <param name="from">currency code</param>
        /// <param name="to1">currency code</param>
        /// <param name="to2">currency code</param>
        /// <param name="to3">currency code</param>
       /// <param name="mid"></param>

        //void Convert(float value, string from, string to1, string to2, string to3, string mid);
    }
}
