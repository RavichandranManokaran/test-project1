﻿using Affinion.LoyaltyBuild.UCom.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.Search.Facets;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi.Filter
{
    public class FilterByOccupancy : IHotelFilter
    {
        //public bool IncludeRoom(UCommerce.Documents.Product product, SearchCriteria criteria)
        //{
        //    int maxAdult = 0;
        //    int maxChild = 0;
        //    int minAdult = 0;
        //    int minChild = 0;
        //    foreach (var item in product.Properties)
        //    {
        //        foreach (var value in item.Value)
        //        {
        //            if (value.Key == ProductDefinationConstant.Adult)
        //            {
        //                maxAdult = Convert.ToInt32(value.Value.ToString());
        //            }
        //            else if (value.Key == ProductDefinationConstant.Child)
        //            {
        //                maxChild = Convert.ToInt32(value.Value.ToString());
        //            }
        //            else if (value.Key == ProductDefinationConstant.Adult)
        //            {
        //                minAdult = Convert.ToInt32(value.Value.ToString());
        //            }
        //            else if (value.Key == ProductDefinationConstant.Child)
        //            {
        //                minChild = Convert.ToInt32(value.Value.ToString());
        //            }
        //        }
        //    }
        //    if (criteria.NumberOfAdult >= minAdult && criteria.NumberOfAdult <= maxAdult && criteria.NumberOfChild >= minChild && criteria.NumberOfChild <= maxChild)
        //    {
        //        return true;
        //    }
        //    return true;
        //    //(product.Properties.Where(x => x.Value.Any(y => y.Key == DefinationConstant.OfferType && y.Value.ToString().Equals(criteria.PackageType, StringComparison.InvariantCultureIgnoreCase))))
        //    //return false;
        //}
        public bool IncludeHotel(UCommerce.Documents.Product product, SearchCriteria criteria)
        {
            return true;
        }

        public UCommerce.Search.Facets.Facet FilterRecordFacet(SearchCriteria criteria)
        {
            if (criteria.RoomType != "all" && !string.IsNullOrEmpty(criteria.RoomType))
            {
                var facet = new Facet();
                facet.FacetValues = new List<FacetValue>();
                facet.Name = ProductDefinationVairantConstant.RoomType;

                //In this example selected values are seperated by a '|' in the querystring value.
                foreach (var value in criteria.RoomType.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    facet.FacetValues.Add(new FacetValue() { Value = value });
                }
                return facet;
            }
            return null;
        }
    }
}
