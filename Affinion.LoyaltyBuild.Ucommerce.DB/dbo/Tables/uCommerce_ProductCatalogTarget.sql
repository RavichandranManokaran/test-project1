﻿CREATE TABLE [dbo].[uCommerce_ProductCatalogTarget] (
    [ProductCatalogTargetId] INT           NOT NULL,
    [Name]                   NVARCHAR (60) NOT NULL,
    CONSTRAINT [PK_ProductCatalogTarget] PRIMARY KEY CLUSTERED ([ProductCatalogTargetId] ASC),
    CONSTRAINT [FK_uCommerce_ProductCatalogTarget_uCommerce_Target] FOREIGN KEY ([ProductCatalogTargetId]) REFERENCES [dbo].[uCommerce_Target] ([TargetId])
);

