﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;
using UCommerce.Marketing.Targets;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{


    public class FixPriceAward : Award, IEntity
    {
        // Fields
        private int? _hashCode;

        // Methods
        public new static IQueryable<FixPriceAward> All()
        {
            return GetRepo().Select();
        }

        public override void Apply(PurchaseOrder order, IList<OrderLine> orderLines)
        {
            foreach (OrderLine line in orderLines)
            {

                Discount discount = this.CreateDiscount(order, orderLines);
                discount.AmountOffTotal = (line.Price * line.Quantity) - this.AmountOff; // this.AmountOff * line.Quantity;
                order.AddDiscount(discount);
                discount.AddOrderLine(line);
                line.AddDiscount(discount);
            }
        }

        public override void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<FixPriceAward, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            FixPriceAward objA = obj as FixPriceAward;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<FixPriceAward, bool>> expression)
        {
            return All().Any<FixPriceAward>(expression);
        }

        public static IList<FixPriceAward> Find(Expression<Func<FixPriceAward, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<FixPriceAward>();
        }

        public static FixPriceAward FirstOrDefault(Expression<Func<FixPriceAward, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<FixPriceAward>();
        }

        public new static FixPriceAward Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<FixPriceAward> GetRepo()
        {
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<FixPriceAward>>();
        }

        public override void Save()
        {
            GetRepo().Save(this);
        }

        public static FixPriceAward SingleOrDefault(Expression<Func<FixPriceAward, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties
        public virtual decimal AmountOff { get; set; }

        public virtual int FixPriceAwardId { get; protected set; }

        public override int Id
        {
            get
            {
                return this.FixPriceAwardId;
            }
        }
    }




}
