﻿using Affinion.LoyaltyBuild.BedBanks.JacTravel.Model;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Message.Interfaces;
using System;
using System.Xml.Serialization;

namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Message
{
    [Serializable()]
    public class JtCancelResponse : JtResponse, IJtResponse
    {
    }
}
