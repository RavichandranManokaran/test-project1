﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GroupAvailabilityCalendarView.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Availability.GroupAvailabilityCalendarView" %>
<input type="hidden" class="roleBasedDisableButtons" id="roleBasedDisableButtons" runat="server" />
<div class="row">
    <div class="col-sm-12">
        <div class="accordian-outer">
            <div class="panel-group Hotelacc">
                <div class="panel panel-default">
                    <div class="panel-heading bg-primary">
                        <h4 class="panel-title">
                            <%--<span><%=hotelData[k].Text%></span>--%>
                            <asp:Label ID="LblHotelName" runat="server" />
                            <a data-toggle="collapse" data-parent="#accordion1" href="#collapse0" class="show-hide-list">
                                <span class="glyphicon glyphicon-triangle-top pull-right"></span>
                            </a>
                        </h4>
                    </div>
                    <div id="collapse0" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div id="dv_<%= currenItem.Month %>">
                                <div class="cal-table-top">
                                    <div class="col-md-12 padding0">
                                        <div class="col-md-6 no-padding">
                                            <div class="cal-table-top-left float-left">
                                                <%-- <button type="button" class="btn btn-default btn-xs" id="reopen_<%= currenItem.Month %>" reopenroom="false" onclick="Supplier.ManageAvailability.CloseOut(<%= currenItem.Month %>,2)"><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span>Reopen </button>
                    <button type="button" class="btn btn-default btn-xs" id="closeout_<%= currenItem.Month %>" closeroom="false" onclick="Supplier.ManageAvailability.CloseOut(<%= currenItem.Month %>,1)"><span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span>Close Out </button>
                    <button type="button" class="btn btn-default btn-xs" id="res_<%= currenItem.Month %>" removerestriction="false" onclick="Supplier.ManageAvailability.CloseOut(<%= currenItem.Month %>,3)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span>Remove restrictions </button>--%>
                                            </div>
                                        </div>
                                        <div class="col-md-6 no-padding">
                                            <%-- <div class="cal-table-top-right float-right">
                    <button type="button" class="btn btn-info font-size-12" data-toggle="modal" data-target="#help"><span class="glyphicon glyphicon-question-sign"></span>Help</button>
                    <button type="button" class="btn btn-info" onclick="Supplier.ManageAvailability.ClearChanges(<%= currenItem.Month %>)">Clear Changes </button>
                    <button id="btn_<%= currenItem.Month %>" type="button" class="btn btn-orange" <%=Disable%> onclick="Supplier.ManageAvailability.UpdateChanges(<%= currenItem.Month %>)">Update and Save Changes </button>
                </div>--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="tabale-top">
                                    <table class="cal-table no-padding" id="calendarTable1_<%= currenItem.Month %>" data-start-day="sun">
                                        <tr>
                                            <th><span class="cal-hed"><%= currenItem.StatDate.ToString("MMM yyyy") %></span></th>
                                            <% for (int i = 0; i <= currenItem.EndDate.Day - currenItem.StatDate.Day; i++)
                   { 
                    
                                            %>
                                            <th onclick="Supplier.ManageAvailability.CloseOutDay(this, '<%= currenItem.Month %>','<%= currenItem.StatDate.AddDays(i).Day %>');" class="<%=  ((currenItem.StatDate.AddDays(i).DayOfWeek == DayOfWeek.Saturday || currenItem.StatDate.AddDays(i).DayOfWeek == DayOfWeek.Sunday) ? "headerSortDown" : "" ) %>" roomdaysclosed="<%=currenItem.RoomAvailability.All(x=>x.AvailabilityData.Any(y=>y.AvialabilityDate == currenItem.StatDate.AddDays(i) &&y.IsCloseOut  )) %>">
                                                <span><%= currenItem.StatDate.AddDays(i).Day.ToString("00") %></span><span> <%= currenItem.StatDate.AddDays(i).DayOfWeek.ToString().Substring(0,2) %> </span></th>
                                            <%} %>
                                        </tr>
                                        <tr class="labelRow">
                                            <td class="left-detail  transparent"></td>
                                            <% for (int i = 0; i <= currenItem.EndDate.Day - currenItem.StatDate.Day; i++)
                   { %>
                                            <td style="<%=((currenItem.StatDate.AddDays(i).DayOfWeek == DayOfWeek.Saturday || currenItem.StatDate.AddDays(i).DayOfWeek == DayOfWeek.Sunday) ? "background-color: rgb(214, 216, 217);": "" ) %>"><span>v</span></td>
                                            <%} %>
                                        </tr>
                                        <%foreach (var room in currenItem.RoomAvailability)
              {
               bool addStyle = false;
                                        %>
                                        <tr>
                                            <td class="left-detail">
                                                <div class="col-md-6 font-weight-bold detail-cal"><%= room.Name %></div>
                                                <div class="col-md-6 block-detail">Booked</div>
                                            </td>
                                            <% for (int i = 0; i <= currenItem.EndDate.Day - currenItem.StatDate.Day; i++)
                   {
                      addStyle = ((currenItem.StatDate.AddDays(i).DayOfWeek == DayOfWeek.Saturday || currenItem.StatDate.AddDays(i).DayOfWeek == DayOfWeek.Sunday));
                       var currentRoomAvailbity = room.AvailabilityData.FirstOrDefault(x => x.AvialabilityDate == currenItem.StatDate.AddDays(i));
                    string defaultclass = string.Format("default{0}", addStyle ? "weekend" : string.Empty);
                                            %>
                                            <td class="<%= currentRoomAvailbity.IsNonEditable? "disablebooking" : defaultclass %>">
                                                <%if (!currentRoomAvailbity.IsNonEditable)
                      { %>
                                                <span class="blokedspan"><%= currentRoomAvailbity.BlockedRoom %></span>
                                                <% } %>
                                            </td>
                                            <%} %>
                                        </tr>

                                        <tr trroom="<%=room.ProductSku %>">
                                            <td class="left-detail borderBottom" onclick="Supplier.ManageAvailability.CloseOutRoom(this,'<%= currenItem.Month %>','<%= room.ProductSku %>');" roomsclosed="<%=room.AvailabilityData.All(x=>x.IsCloseOut) %>" roomsrestriction="<%=room.AvailabilityData.All(x=>x.HasOffer) %> ">
                                                <div class="col-md-6 font-weight-bold detail-cal">
                                                    <%for (int i = 1; i <= room.NumberOfAdult; i++)
                         {%>
                                                    <i class="glyphicon glyphicon-user  md-font"></i>
                                                    <% } %>
                                                    <%for (int i = 1; i <= room.NumberOfChild; i++)
                         {%>
                                                    <i class="glyphicon glyphicon-user sm-font"></i>
                                                    <% } %>
                                                    <%-- <%= room.Name %>--%>
                                                </div>
                                                <div class="col-md-6 block-detail">Allocated</div>
                                            </td>
                                            <% for (int i = 0; i <= currenItem.EndDate.Day - currenItem.StatDate.Day; i++)
                   {
                       addStyle = ((currenItem.StatDate.AddDays(i).DayOfWeek == DayOfWeek.Saturday || currenItem.StatDate.AddDays(i).DayOfWeek == DayOfWeek.Sunday));
                       //var currentRoomAvailbity = room.AvailabilityData.FirstOrDefault(x => x.AvialabilityDate == currenItem.StatDate.AddDays(i)) ?? new Availability();
                       var currentRoomAvailbity = room.AvailabilityData.FirstOrDefault(x => x.AvialabilityDate == currenItem.StatDate.AddDays(i));
                       var roomClosed = currentRoomAvailbity.IsCloseOut;
                       var isOffered = currentRoomAvailbity.HasOffer;
                                            %>
                                            <td>
                                                <%    string classApplied = "";
                        string addClass = string.Empty;
                          if (addStyle)
                          {
                              addClass = "weekend";
                          }
                          //classApplied += string.Format(" offeredClass{0}", currentRoomAvailbity.HasOffer);
                          if (currentRoomAvailbity.IsNonEditable)
                          {
                              classApplied += string.Format(" disablebooking{0}", addClass);
                          }
                          else if (currentRoomAvailbity.IsCloseOut)
                          {
                              classApplied += string.Format(" closeOut{0}", addClass);
                          }
                          else if (currentRoomAvailbity.AvailabelRoom == currentRoomAvailbity.BlockedRoom && currentRoomAvailbity.AvailabelRoom > 0)
                          {
                              classApplied += string.Format(" soldOut{0}", addClass);
                          }
                          else if (currentRoomAvailbity.IsNewRecord || currentRoomAvailbity.AvailabelRoom == 0)
                          {
                              classApplied += string.Format(" unallocate{0}", addClass);
                          }
                          else if (currentRoomAvailbity.HasOffer)
                          {
                              classApplied += string.Format(" limits{0} bookable{1}", addClass,addClass);
                          }
                        
                          else if (currentRoomAvailbity.AvailabelRoom != currentRoomAvailbity.BlockedRoom)
                          {
                        classApplied += string.Format(" bookable{0}", addClass);
                              
                          }  
                        else
                          {
                              classApplied += string.Format(" default{0}", addClass);
                          }                          
                          
                                                %>
                                                <%if (!currentRoomAvailbity.IsNonEditable)
                      {
                          //currentRoomAvailbity.IsNonEditable ? "unallocate" :"default"
                                                %>
                                                <span weekend="<%=addStyle%>" hashid="<%= currenItem.Month %>_<%= room.ProductId.Replace(" ","_") %>_<%= currenItem.StatDate.AddDays(i).Day %>" day="<%=currenItem.StatDate.AddDays(i).Day %>" room="<%=room.ProductSku %>" id="span_<%= currenItem.Month %>_<%= room.ProductId.Replace(" ","_") %>_<%= currenItem.StatDate.AddDays(i).Day %>" class="form-control <%=classApplied %> blokedspan allowselection EnbleSpan_0  EnbleSpan_<%= currenItem.Month %> EnbleSpan tmpfix" isclosed="<%=roomClosed.ToString().ToLower() %>" isofferapplicable="<%= isOffered.ToString().ToLower() %>" orgisclosed="<%=roomClosed.ToString().ToLower() %>" orgisofferapplicable="<%= isOffered.ToString().ToLower() %>" defaultclass="<%=classApplied %>" allocatedroom="<%= currentRoomAvailbity.AvailabelRoom %>">
                                                    <%= currentRoomAvailbity.AvailabelRoom %>
                                                </span>
                                                <%} else {%>
                                                <span weekend="<%=addStyle%>" class="disablebooking blokedspan EnbleSpan_0 EnbleSpan_<%= currenItem.Month %> EnbleSpan tmpfix"></span>
                                                <%} %>
                                                <input month="<%=currenItem.Month %>" dayofweek="<%=(currentRoomAvailbity.IsNonEditable ? -1 : ((int)currenItem.StatDate.AddDays(i).DayOfWeek)) %>" room="<%=room.ProductSku %>" weekend="<%=addStyle%>" maxlength="3" datecounter="<%= currentRoomAvailbity.DateCounter %>" type="text" <%=currentRoomAvailbity.IsNonEditable? "disabled=\"disabled\"" : "" %> uniqidentifier="<%= currentRoomAvailbity.DateCounter %>_<%= room.ProductSku.Replace(" ","_") %>" id="<%= currenItem.Month %>_<%= room.ProductId.Replace(" ","_") %>_<%= currenItem.StatDate.AddDays(i).Day %>" hashid="<%= currenItem.Month %>_<%= room.ProductId.Replace(" ","_") %>_<%= currenItem.StatDate.AddDays(i).Day %>" onchange="Supplier.ManageAvailability.ChangeAvailability(this,false)" onblur="Supplier.ManageAvailability.ChangeAvailability(this,true)" blockedroom="<%= currentRoomAvailbity.BlockedRoom %>" allocatedroom="<%= currentRoomAvailbity.AvailabelRoom %>" autocomplete="off" required class="form-control <%=classApplied %>  EnbleText EnbleText_0 EnbleText_<%= currenItem.Month %>" title="" value="<%=currentRoomAvailbity.IsNonEditable? "" : currentRoomAvailbity.AvailabelRoom.ToString() %>" orgisclosed="<%=roomClosed.ToString().ToLower() %>" orgisofferapplicable="<%= isOffered.ToString().ToLower() %>" defaultclass="<%=classApplied %>" />
                                            </td>

                                            <%} %>
                                        </tr>
                                        <%} %>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {

        $(".Hotelacc").on("hide.bs.collapse", function () {
            $(".show-hide-list").html('<span class="glyphicon glyphicon-triangle-bottom pull-right"></span>');
        });

        $(".Hotelacc").on("show.bs.collapse", function () {
            $(".show-hide-list").html('<span class="glyphicon glyphicon-triangle-top pull-right"></span>');
        });
    });
    </script>
