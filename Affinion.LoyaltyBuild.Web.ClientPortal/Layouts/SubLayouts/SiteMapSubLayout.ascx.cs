﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SiteMapSublayout.ascx.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.SiteMapSublayout
/// Description:           Renders SiteMap details
namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts
{
    #region Using Directives
    using System;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using System.Linq;
    using System.Collections.Generic;
    using System.Web.UI.WebControls;
    using Sitecore.Data.Items;
    using System.IO;
    using System.Web;
    #endregion

    public partial class SiteMapSubLayout : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            try
            {
                string URL = Server.MapPath("/");
                var websiteList = Sitecore.Context.Database.GetItem("/sitecore/system/Modules/Advanced Sitemap/SiteMap Sites").Children;
                string currentSiteName = Sitecore.Context.GetSiteName();
                string currentUrl=HttpContext.Current.Request.Url.AbsoluteUri;
                Uri uri = new Uri(currentUrl);
                string url=uri.Scheme + Uri.SchemeDelimiter + uri.Host;
                //Item website = websiteList.Where(w => w.Fields["Site Name"].Value == currentSiteName).First();
                Item website = websiteList.Where(w => w.Fields["Server URL"].Value == url).First();
                if (website == null)
                    return;
                string xmlFile = website.Fields["SitemMap XML Filename"].Value;               
                string xmlFilePath = string.Concat(@"\", xmlFile);
                SiteMapHandler map = new SiteMapHandler(URL + xmlFilePath);

                SiteMapRepeater.DataSource = map.mapItems.Distinct(); 
                SiteMapRepeater.DataBind();

            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, this);
                throw;
            }
        }

        protected void SiteMapRepeater_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            var data = ((SiteMapParentItem)e.Item.DataItem).ChildItems;
            var siteMapItemRepeater = (Repeater)e.Item.FindControl("SiteMapItemRepeater");
            siteMapItemRepeater.DataSource = data;
            siteMapItemRepeater.DataBind();
        }
    }
}