﻿CREATE PROCEDURE [dbo].[LB_GetChildAgeDetailsByOrderLineID]	
	@OrderLineId as int
AS
BEGIN
	declare @isExist as int
	select @isExist=count (NoOfChild) from LB_RoomReservation WHERE OrderLineId = @OrderLineId

	if (@isExist > 0)
	begin	
		SELECT TOP (
						SELECT NoOfChild FROM LB_RoomReservation WHERE OrderLineId = @OrderLineId
					)
		Age FROM LB_RoomReservationAgeInfo WHERE OrderLineId = @OrderLineId
		ORDER BY ReservationAgeId DESC
	END
END
