﻿using Affinion.LoyaltyBuild.SupplierPortal.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.SupplierPortal.DataAccess
{
    public interface IProviderService
    {
      //  Provider GetProviderById(int providerId);

        List<Provider> GetAllProviders();

        List<Provider> GetProvidersByHotelGroup(string groupId);

        List<Client> GetClientList();

        List<ProviderGroup> GetAllProviderGroups();

       // ProviderGroup GetProviderGroupById(int provierGroupId);

        Provider GetProviderById(string productSku);
        ProviderGroup GetProviderGroupById(string uniqueId);
        // List<Provider> GetProvidersByHotelGroup(string uniqueId);
    }
}
