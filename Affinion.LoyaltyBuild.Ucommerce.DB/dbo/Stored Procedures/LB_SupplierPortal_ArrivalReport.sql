﻿CREATE PROCEDURE [dbo].[LB_SupplierPortal_ArrivalReport]	
	@OrderLineId NVARCHAR(50),
	@ClientName       NVARCHAR(512),
	@DateFrom         DATE,
	@Offername        INT,
	@DateTo           DATE,
	@Provider         int,
	@ReportandOrderby NVARCHAR(50),
	@bookingstatus    Int
AS
BEGIN
SET nocount ON


 if isnull(@ReportandOrderby,'') =''
	begin
	select @ReportandOrderby = (isnull(@ReportandOrderby,'Arrival Date'))
    end

      if isnull(@ReportandOrderby,'') =''
	begin
	select @ReportandOrderby = (isnull(@ReportandOrderby,'Arrival Date'))
    end
	SELECT * FROM(
    (SELECT  BI.BookingNo [Booking Reference],
	          CI.Name [offerName],
			  po.CurrencyId [CurrencyId],
			  op.Deposit [CollectAmount],
	          oa.FirstName [FirstName],
		      oa.LastName [Last Name],
		      oa.Line1 [AddressLine1],
		      oa.Line2 [AddressLine2],
		      oa.City[City],
		      oa.[State]  [Region],
			  c.Name [Country],
		      oa.MobilePhoneNumber [Mobilephonenumber],
		      oa.Phonenumber [Phonenumber],
		      BI.CheckInDate [Arrival Date],
		      BI.CheckOutDate [Departure Date],
			  op.NoOfAdult  [Number of adults],
			  op.NoOfChild  [Number of children],
			  op.ChildAge [Child Age],
			  ol.CreatedOn [Reservationdate],
			  op.BookingThrough  [Bookingthrough],
		      os.Name [Bookingstatus],
		      OLS.CreatedOn [Bookingstatusdate],
		      p.Name [Accomodation],
		      '' [Acknowledgement],
			     CASE WHEN BookingNo IS NOT NULL THEN '#819FF7' --Transferred Out
			  WHEN (SELECT COUNT(1) FROM LB_BookingInfo where BookingNo = BI.BookingNo) > 1 THEN '#F781BE' --Transferred In
			  ELSE '#f3f3f3' END [Rowcolor],  --Not transferred [Rowcolor]
		      CI.CampaignItemId [OfferId],
			  op.[ClientId],
			  op.[GuestName],
			  op.[NoOfGuest],
			  op.[OrderLineId]
			
		   

			  FROM   Ucommerce_orderline ol
		             INNER JOIN Ucommerce_product p ON ol.Sku = ol.Sku
			         INNER JOIN Ucommerce_purchaseorder po ON ol.Orderid = po.Orderid
					 INNER JOIN LB_BookingInfo BI ON ol.Orderlineid = BI.Orderlineid
					 INNER JOIN
			         (
					   select OrderLineId, _NoOfAdult [NoOfAdult], _NoOfChild [NoOfChild],_NoOfGuest [NoOfGuest],GuestName [GuestName],_ChildAge [ChildAge],_BookingingThrough [BookingThrough],_ClientId [ClientId],_LBcommission[Deposit]
					from 
					(
					  select OrderLineId, [key], [value]
					  from uCommerce_OrderProperty --where OrderLineId = @OrderLineID
					) src
					pivot
					(
					max([value])
				 for [key] in ( _NoOfAdult, _NoOfChild,_NoOfGuest,GuestName,_ChildAge,_BookingingThrough,_ClientId,_LBcommission)
			    ) piv
		     ) OP on  rr.OrderLineId=OP.OrderLineId	
			 	      
			         LEFT JOIN Ucommerce_orderaddress oa ON po.Orderid = oa.Orderid
			         LEFT JOIN Ucommerce_country c ON oa.Countryid = c.Countryid
			         LEFT JOIN uCommerce_OrderLineCampaignRelation olr ON ol.OrderLineId=olr.OrderLineId  
			         LEFT JOIN ucommerce_campaignitem ci ON olr.CampaignItemId=ci.CampaignItemId
			         LEFT JOIN LB_OrderLineStatusAudit ols ON BI.orderlineId = ols.OrderlineId and ols.StatusId = BI.StatusID
			         LEFT JOIN uCommerce_OrderStatus os ON ols.StatusId=os.OrderStatusId
			

			WHERE  (@OrderLineId IS NULL	OR @OrderLineId = BI.BookingNo)
	                AND (BI.BookingNo IS NOT NULL)
			        AND (@ClientName IS NULL  OR @ClientName = oa.Firstname OR @ClientName = oa.Lastname)
			        AND (@Provider = p.productId)
		            AND ((@ReportandOrderby='Arrival Date' AND (BI.CheckInDate BETWEEN @DateFrom AND @DateTo)) OR (@ReportandOrderby='Reservation Date' AND (ol.CreatedOn BETWEEN @DateFrom AND @DateTo)))
			        and (@Offername = 0 OR @Offername = ci.Campaignitemid)
                    ANd((@bookingstatus =0 or @bookingstatus =ols.StatusId))
			 )
			 )a
			 order by 
			 CASE 
				    WHEN @ReportandOrderby='Arrival Date' THEN [CheckIndate]
				    ELSE [Reservationdate]
			        END asc;
			 END

