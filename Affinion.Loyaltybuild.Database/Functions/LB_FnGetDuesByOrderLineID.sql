CREATE FUNCTION [dbo].[LB_FnGetDuesByOrderLineID]
(	
	@OrderLineId	int,
	@CurrencyTypeId	int =null
)
RETURNS @LB_fnGetDuesByOrderLineID TABLE 
(
	Total MONEY,
	CurrencyTypeId INT,
	OrderLineId INT
) 
AS
BEGIN
	INSERT INTO @LB_fnGetDuesByOrderLineID
	SELECT SUM([Total]) [Total], [Currency], MAX(OrderLineID)[OrderLineId] FROM
		(SELECT SUM(OLD.Amount) [Total],
				OLD.CurrencyTypeId [Currency],
				MAX(OLD.OrderLineID) [OrderLineId]
		FROM uCommerce_OrderLine  OL
			INNER JOIN LB_OrderLineDue OLD ON OLD.OrderLineID = OL.OrderLineId 
		WHERE
			ISNULL(OLD.IsDeleted,0) = 0 AND 			
			OLD.CurrencyTypeId = (CASE WHEN @CurrencyTypeId IS NOT NULL THEN @CurrencyTypeId ELSE OLD.CurrencyTypeId END) AND			
			OLD.OrderLineID IN (SELECT OrderLineID FROM [dbo].[LB_FnRelatedOrderLines](@OrderLineId))
		GROUP BY 
			OLD.CurrencyTypeId
		UNION ALL
		SELECT -SUM(OLP.Amount) [Total],
				OLP.CurrencyTypeId [Currency],
				MAX(OLP.OrderLineID) [OrderLineId]
		FROM uCommerce_OrderLine  OL
			INNER JOIN LB_OrderLinePayment OLP ON OLP.OrderLineID = OL.OrderLineId 
		WHERE
			OLP.CurrencyTypeId = (CASE WHEN @CurrencyTypeId IS NOT NULL THEN @CurrencyTypeId ELSE OLP.CurrencyTypeId END) AND			
			OLP.OrderLineID IN (SELECT OrderLineID FROM [dbo].[LB_FnRelatedOrderLines](@OrderLineId))
		GROUP BY 
			OLP.CurrencyTypeId) P
	GROUP BY [Currency]
	HAVING
		SUM([Total]) <> 0

	RETURN
END
GO
