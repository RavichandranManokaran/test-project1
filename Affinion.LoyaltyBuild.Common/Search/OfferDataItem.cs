﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SearchHelper.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common.Search
/// Description:           Stores information related to individual offers
/// </summary>
namespace Affinion.LoyaltyBuild.Common.Search
{

    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
    using Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Runtime.Serialization.Formatters.Binary;
    using UCommerce;
    using UCommerce.Api;
    using UCommerce.EntitiesV2;

    /// <summary>
    /// Stores information related to individual offers
    /// </summary>
    public class OfferDataItem : ICloneable
    {
        private Item occupancyItem;
        private Item offerGroupItem;
        private string occupancyName;
        private string offerGroupName;

        /// <summary>
        /// Gets or sets the offer name
        /// </summary>
        public string OfferName { get; set; }

        /// <summary>
        /// Gets or sets the offer sku
        /// </summary>
        public string OfferSku { get; set; }

        /// <summary>
        /// Gets or sets the occupancy type id
        /// </summary>
        public string OccupancyTypeId { get; set; }

        /// <summary>
        /// Gets or sets the occupancy type
        /// </summary>
        public OccupancyType OccupancyType { get; set; }

        /// <summary>
        /// Gets or sets the offer group id
        /// </summary>
        public string OfferGroupId { get; set; }

        /// <summary>
        /// Gets or sets the offer group name
        /// </summary>
        public string OfferGroupName { get; set; }

        /// <summary>
        /// Gets or sets the Offer Price object
        /// </summary>
        public LBPriceCalculation PriceDetails { get; set; }

        /// <summary>
        /// Gets or sets the Offer Price
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Gets or sets the offer price with currency
        /// </summary>
        public string PriceWithCurrency { get; set; }

        /// <summary>
        /// Gets or sets the Offer Price
        /// </summary>
        public decimal DiscountedPrice { get; set; }

        /// <summary>
        /// Gets or sets value indicating weather the price is discounted
        /// </summary>
        public bool IsDiscounted { get; set; }

        /// <summary>
        /// Gets or sets the currency
        /// </summary>
        public Currency CurrencyDetails { get; set; }

        /// <summary>
        /// Gets or sets the currency id
        /// </summary>
        public int CurrencyId { get; set; }

        /// <summary>
        /// Gets or sets the offer's occupancy type availability
        /// </summary>
        public int Availability { get; set; }

        /// <summary>
        /// Gets or sets the offer tip
        /// </summary>
        public string Tip { get; set; }

        /// <summary>
        /// Gets or sets the supplier ID
        /// </summary>
        public string SupplierId { get; set; }

        /// <summary>
        /// Constructs an empty offer object
        /// </summary>
        public OfferDataItem() { }

        public List<RoomAvailabilityDetail> RoomAvailability { get; set; }

        /// <summary>
        /// Constructs a offer item
        /// </summary>
        /// <param name="sku">Product sku</param>
        /// <param name="priceGroup">Price group name</param>
        /// <param name="supplier">Supplier sitecore item</param>
        public OfferDataItem(Category category, string sku, string priceGroup, Item supplier)
        {
            Product product;

            try
            {
                product = CatalogLibrary.GetProduct(sku);
            }
            catch (ArgumentException ex)
            {
                return;
            }

            UpdateProductInformation(category, product, priceGroup);
            this.SupplierId = supplier.ID.ToString();
        }

        /// <summary>
        /// Constructs a offer item
        /// </summary>
        /// <param name="product">UCommerce product</param>
        /// <param name="priceGroup">Price group name</param>
        /// <param name="supplier">Supplier sitecore item</param>
        public OfferDataItem(Category category, Product product, string priceGroup, Item supplier)
        {
            UpdateProductInformation(category, product, priceGroup);
            this.SupplierId = supplier.ID.ToString();
        }

        /// <summary>
        /// Updates the occupancy type information
        /// </summary>
        public void UpdateOccupancyType()
        {
            this.OccupancyType = new OccupancyType(this.OccupancyTypeId);
        }

        /// <summary>
        /// Clones the current object
        /// </summary>
        /// <returns>Returns a fresh copy with current values</returns>
        public object Clone()
        {
            return this.MemberwiseClone();
        }

        /// <summary>
        /// Update ucommerce product information to the offer item
        /// </summary>
        /// <param name="item">The offer sitecore item</param>
        /// <param name="priceGroupName">Current client name</param>
        private void UpdateProductInformation(Category category, Product supplier, string priceGroupName)
        {

            if (supplier != null)
            {
                //List<PriceGroup> priceGroupList = PriceGroup.All().ToList();
                //var priceList = priceGroupList.Where(p => IsClientDefaultPriceGroup(p, priceGroupName));
                //PriceGroup priceGroup = priceList != null && priceList.Count() > 0 ? priceList.First() : null;

                //if (priceGroup != null)
                //{
                //    Money offerPrice = supplier.GetPrice(priceGroup);
                //    this.PriceWithCurrency = offerPrice != null ? offerPrice.ToString() : string.Empty;
                //    this.Price = offerPrice != null ? offerPrice.Value : 0;
                //    this.CurrencyId = offerPrice != null ? offerPrice.Currency.CurrencyId : 0;
                //}

                SearchCriteria criteria = new SearchCriteria();
                LBPriceCalculation price = LoyaltyBuildCatalogLibrary.CalculatePrice(supplier, criteria, category.ProductCatalog);

                if (price != null)
                {
                    this.PriceDetails = price;
                    this.PriceWithCurrency = price.YourPrice.Amount.ToString();
                    this.Price = price.YourPrice.Amount.Value;
                    this.DiscountedPrice = price.Discount.Amount.Value;
                    this.CurrencyDetails = price.YourPrice.Amount.Currency;
                    this.CurrencyId = price.YourPrice.Amount.Currency.CurrencyId;
                    this.IsDiscounted = price.IsDiscounted;
                }
            }

            this.OfferSku = supplier.Sku;
            this.OccupancyTypeId = supplier.Sku;
            this.OfferGroupId = string.Empty;

            this.UpdateOccupancyType();
        }

        /// <summary>
        /// Checks the default price group of the client
        /// </summary>
        /// <param name="priceGroup">Current price group from the ucommerce price group list</param>
        /// <param name="clientName">Current client name</param>
        /// <returns>Returns true if the given price group is the default price group of the given client</returns>
        private static bool IsClientDefaultPriceGroup(PriceGroup priceGroup, string clientName)
        {
            string priceGroupName = priceGroup.Name.ToLowerInvariant();
            clientName = clientName.ToLowerInvariant();
            return priceGroupName.IndexOf(clientName) >= 0;
        }
    }
}
