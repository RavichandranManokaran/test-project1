﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi.Filter
{
    public interface IProductFilter
    {
        /// <summary>
        /// Whether to ignore supplier
        /// </summary>
        /// <param name="filterData"></param>
        /// <returns></returns>
        bool IncludeRoom(UCommerce.Documents.Product product, SearchCriteria criteria);
    }
}
