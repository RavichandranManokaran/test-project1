﻿CREATE TABLE [dbo].[LB_RoomAvailability] (
    [RoomAvailabilityId] UNIQUEIDENTIFIER NOT NULL,
    [Sku]                NVARCHAR (30)    NULL,
    [VariantSku]         NVARCHAR (30)    NOT NULL,
    [NumberAllocated]    INT              NOT NULL,
    [NumberBooked]       INT              NOT NULL,
    [DateCounter]        INT              NOT NULL,
    [CreatedBy]          NVARCHAR (150)   NULL,
    [UpdateBy]           NVARCHAR (150)   NULL,
    [DateCreated]        DATETIME         NULL,
    [DateLastUpdate]     DATETIME         NULL,
    [IsStopSellOn]       BIT              NOT NULL,
    [IsCloseOut]         BIT              NOT NULL,
    [Price]              MONEY            NULL,
    [PackagePrice]       MONEY            NULL,
    [PriceBand]          UNIQUEIDENTIFIER     NULL,
    CONSTRAINT [PK_uCommerce_RoomAvailability] PRIMARY KEY CLUSTERED ([RoomAvailabilityId] ASC)
);

