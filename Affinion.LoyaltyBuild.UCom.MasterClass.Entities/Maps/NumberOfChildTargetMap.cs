﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class NumberOfChildTargetMap : SubclassMap<NumberOfChildTarget>
    {
        // Methods
        public NumberOfChildTargetMap()
        {
            base.Table("LB_NumberOfChildTarget");
            base.KeyColumn("NumberOfChildTargetId");
            base.Map(x => x.NumberOfChildTargetId).ReadOnly();
            base.Map(x => x.NumberOfChild).Not.Nullable();
        }

    }

}
