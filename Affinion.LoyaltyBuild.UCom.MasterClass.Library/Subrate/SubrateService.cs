﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using AffinionEntity = Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using UCommerce.EntitiesV2;
using Sitecore.Data;
using Sitecore.Data.Items;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Queries;
using UCommerce.Infrastructure;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Availability;
using Affinion.LoyaltyBuild.UCom.Common.Constants;
using Affinion.LoyaltyBuild.UCom.Common.Extension;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Subrate
{
    public class SubrateService : ISubrate
    {
        /// <summary>
        /// IRepository Type OrderLineCampaignRelation
        /// </summary>
        private IRepository<OrderLineCampaignRelation> relationRepository = ObjectFactory.Instance.Resolve<IRepository<OrderLineCampaignRelation>>();

        /// <summary>
        /// List Type SubrateItemDetails
        /// </summary>
        /// <returns></returns>
        public List<SubrateItemDetail> GetAllSubRateItems()
        {

            List<SubrateItemDetail> srItems = new List<SubrateItemDetail>();
            List<AffinionEntity.SubRateItem> items = AffinionEntity.SubRateItem.All().ToList();
            if (items != null && items.Any())
            {
                foreach (var item in items)
                {
                    srItems.Add(new SubrateItemDetail(item.SubRateItemCode, item.DisplayName));
                }

            }
            //srItems.Add(new SubrateItem("LBCOM", "LB Commision"));
            //srItems.Add(new SubrateItem("CCPROFEE", "Credit Card  Processing Fee"));
            //srItems.Add(new SubrateItem("CANFEE", "Cacellation Fee"));
            //srItems.Add(new SubrateItem("TRAFEE", "Transfer Fee"));

            return srItems;

        }

        /// <summary>
        /// List Type SubrateTemplatesDetail
        /// </summary>
        /// <returns></returns>
        public List<SubrateTemplatesDetail> GetAllSubRateTemplate()
        {

            List<SubrateTemplatesDetail> srlist = new List<SubrateTemplatesDetail>();
            //TODO : Get all the saved template from uCommerce_SubrateTemplates and remove below code
            List<SubrateTemplates> srTemplate = SubrateTemplates.All().ToList();
            foreach (var item in srTemplate)
            {
                SubrateTemplatesDetail srates = new SubrateTemplatesDetail();
                srates.SubrateTemplateID = item.SubrateTemplateID;
                srates.TemplateName = item.SubrateTemplateName;
                srates.SubrateTemplateCode = item.SubrateTemplateGuid;
                srates.UpdateOn = item.UpdateOn;
                srlist.Add(srates);
            }


            return srlist;
        }

        /// <summary>
        /// Get All the sunrates item for passed item (Template / Offer / Product)
        /// </summary>
        /// <param name="ItemID"></param>
        /// <returns></returns>
        public List<SubratesDetail> GetSubRateByItemId(string ItemID)
        {
            List<SubRate> subrateDetails = SubRate.All().Where(x => x.ItemID.Equals(ItemID)).ToList();
            List<SubratesDetail> lstSubrates = new List<SubratesDetail>();
            foreach (var item in subrateDetails)
            {
                lstSubrates.Add(new SubratesDetail(item.SubrateItemCode, item.ItemID, item.Price, item.VAT, item.IsPercentage, item.ClientID, item.CreatedBy, item.UpdatedOn, item.Currency));
            }
            //            lstSubrates.Add(new SubratesDetail("TRAFEE", "Temp001", 18, true, "NA", "Admin", DateTime.Now));

            return lstSubrates;
        }

        /// <summary>
        /// Save subrate for item ( Template / Offer / Product)
        /// </summary>
        /// <param name="objSubrates"></param>
        /// <param name="ItemID"></param>
        /// <returns></returns>
        public int SaveSubrates(List<SubratesDetail> objSubrates, string ItemID)
        {
            try
            {
                //ITEMID set to null when saving template
                if (String.IsNullOrEmpty(ItemID))
                {
                    if (objSubrates != null && objSubrates.Any())
                    {
                        var subrate = objSubrates.FirstOrDefault();
                        List<SubRate> subrates = SubRate.All().Where(x => x.ClientID.Equals(subrate.ClientID) && x.ItemID.Equals(subrate.ItemID) && x.Currency.Equals(subrate.Currency)).ToList();
                        foreach (var item in objSubrates)
                        {
                            SubRate subRateExist = subrates.FirstOrDefault(x => x.SubrateItemCode.Equals(item.SubrateItemCode) && x.ClientID.Equals(item.ClientID) && x.ItemID.Equals(item.ItemID)) ?? new SubRate();
                            subRateExist.ClientID = item.ClientID;
                            subRateExist.Currency = item.Currency;
                            //subRateExist.Currency  item.
                            subRateExist.CreatedOn = DateTime.Now;
                            subRateExist.CreatedBy = "Admin";
                            subRateExist.IsPercentage = item.IsPercentage;
                            subRateExist.ItemID = item.ItemID;
                            subRateExist.Price = item.Price;
                            subRateExist.SubrateItemCode = item.SubrateItemCode;
                            subRateExist.UpdatedOn = DateTime.Now;
                            subRateExist.VAT = item.VAT;
                            subRateExist.Save();

                        }
                    }
                }
                //if (String.IsNullOrEmpty(ItemID))
                //{
                //    //TODO : Save New Template and get the ID of that template
                //    //TODO : Save New Subrates Object        
                //}


                //else
                //{
                //    //TODO : Update New Object
                //}
                return 1;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.MasterClassBusinessLogic, ex, this);
                return 0;
            }
        }

        /// <summary>
        /// Delete Subrates for item (Template / Offer / Product)
        /// </summary>
        /// <param name="ItemID"></param>
        /// <param name="ClientID"></param>
        /// <returns></returns>
        public int DeleteSubrate(string ItemID, string ClientID = "Admin")
        {
            try
            {
                return 1;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.MasterClassBusinessLogic, ex, this);
                return 0;
            }
        }

        /// <summary>
        /// Save or update templates and return template code
        /// </summary>
        /// <param name="objsrtemplate"></param>
        /// <returns></returns>
        public string SaveTemplate(SubrateTemplatesDetail objsrtemplate)
        {
            //TODO : Save template
            try
            {
                SubrateTemplates subrateTemplate = new SubrateTemplates();
                subrateTemplate.SubrateTemplateGuid = objsrtemplate.SubrateTemplateCode;
                subrateTemplate.CreatedBy = objsrtemplate.CreatedBy;
                subrateTemplate.CreatedOn = objsrtemplate.UpdateOn;
                subrateTemplate.SubrateTemplateName = objsrtemplate.TemplateName;
                subrateTemplate.UpdateOn = objsrtemplate.UpdateOn;
                subrateTemplate.Save();
                return subrateTemplate.SubrateTemplateGuid;
            }
            catch (Exception)
            {
                return string.Empty;
            }
            //return string.Empty;
        }

        /// <summary>
        /// Delete template
        /// </summary>
        /// <param name="SubrateTemplateID"></param>
        /// <returns></returns>
        public int DeleteTemplate(string SubrateTemplateID)
        {
            try
            {
                SubrateTemplates templates = SubrateTemplates.All().Where(x => x.SubrateTemplateID == Convert.ToInt32(SubrateTemplateID)).FirstOrDefault();
                if (templates != null)
                {
                    templates.Delete();
                }
                return 1;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.MasterClassBusinessLogic, ex, this);
                return 0;
            }

        }
        /// <summary>
        /// GetAllClient
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="_database"></param>
        /// <returns></returns>
        public List<ClientDetail> GetAllClient(string itemId, Database _database)
        {
            Item[] items = _database.SelectItems("fast:/sitecore/content/#admin-portal#/#client-setup#/*[@@templatename='ClientDetails']");
            List<ClientDetail> clientList = new List<ClientDetail>();

            int i = 1;
            foreach (Item client in items)
            {
                clientList.Add(new ClientDetail() { Id = i, Name = client.Fields["Name"].Value, ClientGUID = client.ID.ToString().Replace("{", "").Replace("}", "") });
                i++;
            }
            return clientList;
        }



        /// <summary>
        /// Get All the sunrates item for passed item (Template / Offer / Product) & Client
        /// </summary>
        /// <param name="ItemID"></param>
        /// <returns></returns>
        public List<SubratesDetail> GetSubRateByClientAndItemId(string ItemID, string clientId, string currency)
        {
            List<SubRate> subrateDetails = SubRate.All().Where(x => x.ItemID.Equals(ItemID) && x.ClientID.Equals(clientId) && x.Currency.Equals(currency)).ToList();
            List<SubratesDetail> lstSubrates = new List<SubratesDetail>();
            foreach (var item in subrateDetails)
            {
                lstSubrates.Add(new SubratesDetail(item.SubrateItemCode, item.ItemID, item.Price, item.VAT, item.IsPercentage, item.ClientID, item.CreatedBy, item.UpdatedOn, item.Currency));
            }
            return lstSubrates;
        }


        public List<OrderLineCommsion> GetPriceBand(int orderId, int startDateCounter, int endDateCounter)
        {
            List<OrderLineCommsion> commission_subrate = new List<OrderLineCommsion>();
            List<RoomAvailability> roomavailability = new List<RoomAvailability>();
            Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
            IRoomAvailabilityService service = new RoomAvailabilityService();

            List<OrderLine> orderLineList = OrderLine.All().Where(x => x.PurchaseOrder.OrderId == orderId).ToList();
            List<string> variantSKU = new List<string>();
            foreach (var item in orderLineList)
            {
                OrderLineCommsion commision = new OrderLineCommsion();
                commision.OrderLineId = item.OrderLineId;
                //item.OrderProperties.Add(new OrderProperty() { Key = OrderPropertyConstants.CheckInDate, Value = DateTime.Now.ConvertDateToString() });
                //item.OrderProperties.Add(new OrderProperty() { Key = OrderPropertyConstants.NightKey, Value = "3" });
                OrderProperty checkinData = item.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.CheckInDate);
                OrderProperty nightData = item.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.NightKey);
                if (checkinData != null && nightData != null)
                {
                    DateTime checkinDate = checkinData.Value.ToString().ParseFormatDateTime();
                    DateTime checkOutDate = checkinDate.AddDays(Convert.ToInt32(nightData.Value));
                    //range.Add(new DateRange() { StartDate = checkinDate, EndDate = checkOutDate });
                    //Availability Call Put In loop Beacuse in Order if there is 2 orderline with diffe of 1 month then we need to fetch all data. 
                    roomavailability = RoomAvailability.All().Where(x => x.Sku == item.Sku && x.VariantSku == item.VariantSku && x.DateCounter >= checkinDate.ConvetDatetoDateCounter() && x.DateCounter <= checkOutDate.ConvetDatetoDateCounter()).ToList();

                    List<string> priceBand = roomavailability.Select(x => x.PriceBand).Distinct().ToList();
                    string fastQuery = string.Empty;

                    string query = "fast:/sitecore/content/#admin-portal#/#supplier-setup#/global/#_supporting-content#/pricebands//*[@@templatename='PriceBand' and  {0}]";
                    List<string> queryBuilding = new List<string>();
                    foreach (var p in priceBand)
                    {
                        queryBuilding.Add(string.Format("@@id='{0}'", "{" + p + "}"));
                    }
                    query = string.Format(query, string.Join(" or ", queryBuilding));
                    Sitecore.Data.Items.Item[] orderlinePriceBand = workingdb.SelectItems(query);
                    if (orderlinePriceBand.Length > 0)
                    {
                        commision.OrderLinePriceBand = GetCommissiondata(orderlinePriceBand, Convert.ToInt32(nightData.Value));
                    }
                }
            }
            return commission_subrate;
        }


        public List<RommCommission> GetCommissionFromPriceBand(List<string> priceBand, int noOfNights)
        {
            List<RommCommission> commission_subrate = new List<RommCommission>();
            Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
            IRoomAvailabilityService service = new RoomAvailabilityService();

            string fastQuery = string.Empty;

            string query = "fast:/sitecore/content/#admin-portal#/#supplier-setup#/global/#_supporting-content#/pricebands//*[@@templatename='PriceBand' and  {0}]";
            List<string> queryBuilding = new List<string>();
            foreach (var p in priceBand)
            {
                queryBuilding.Add(string.Format("@@id='{0}'", "{" + p + "}"));
            }
            query = string.Format(query, string.Join(" or ", queryBuilding));
            Sitecore.Data.Items.Item[] orderlinePriceBand = workingdb.SelectItems(query);
            if (orderlinePriceBand.Length > 0)
            {
                List<OrderLinePriceBand> commission = GetCommissiondata(orderlinePriceBand, noOfNights);

                foreach (var item in orderlinePriceBand)
                {
                    if (item.Fields["commission"] != null)
                    {
                        Guid commissionItemId;
                        if (Guid.TryParse(item.Fields["commission"].Value, out commissionItemId))
                        {
                            decimal rate = 0;
                            if (item.Fields["Rate"] != null)
                            {
                                decimal.TryParse(item.Fields["Rate"].ToString(), out rate);
                            }
                            OrderLinePriceBand pBand = commission.FirstOrDefault(x => x.CommissionId == commissionItemId);
                            if (pBand != null)
                            {
                                PriceBandCommision bandCommission = pBand.LBSubrate.FirstOrDefault(x => x.Code == Constants.LBCommission);
                                if (bandCommission != null)
                                {
                                    commission_subrate.Add(new RommCommission() { Amount = bandCommission.Commssion, IsPercentage = bandCommission.IsPercentage, PriceBandId = item.ID.Guid.ToString() , RoomRate = rate });
                                }
                            }
                        }
                    }
                }
            }
            return commission_subrate;
        }
        /// <summary>
        /// List Type OrderLineSubrate
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="clientId"></param>
        /// <param name="currency"></param>
        /// <returns></returns>
        public List<OrderLineSubrate> GetOrderSubrate(int orderId, string clientId, string currency = "GBP")
        {
            try
            {
                //GetPriceBand(orderId,9645,9695);
                List<OrderLine> orderLineList = OrderLine.All().Where(x => x.PurchaseOrder.OrderId == orderId).ToList();
                List<OrderLineCampaignRelation> orderLineCampaignMapping = relationRepository.Select(new GetOrderLineSubrateQuery(orderId)).ToList();
                List<string> itemsId = orderLineList.Select(x => x.Sku).ToList();

                itemsId.AddRange(orderLineCampaignMapping.Select(x => x.CampaignItemId.ToString()));
                Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Affinion.LoyaltyBuild.Common.Constants.CurrentSitecoreDatabase);
                List<CurrencyDetail> currencyList = this.GetAllCurrency(workingdb);
                string currencyId = string.Empty;
                if (currencyList != null && currencyList.Any(x => x.Name.Equals(currency, StringComparison.InvariantCultureIgnoreCase)))
                {
                    currencyId = currencyList.FirstOrDefault(x => x.Name.Equals(currency, StringComparison.InvariantCultureIgnoreCase)).CurrencyGUID;
                }

                List<SubRate> srate = SubRate.All().Where(x => itemsId.Contains(x.ItemID) && x.ClientID == clientId).ToList();
                List<SubRateItem> subrateitem = SubRateItem.All().ToList();
                List<OrderLineSubrate> orderLineSubrate = new List<OrderLineSubrate>();
                foreach (var item in orderLineList)
                {
                    bool offerSubrate = false;

                    int campaingItemId = 0;
                    var campaingInfo = orderLineCampaignMapping.FirstOrDefault(x => x.OrderLineId == item.OrderLineId);
                    if (campaingInfo != null)
                    {
                        campaingItemId = campaingInfo.CampaignItemId;
                    }
                    List<SubRate> applicableSubrate = srate.Where(x => (x.ItemID == campaingItemId.ToString() || x.ItemID == item.Sku) && x.Currency.Equals((string.IsNullOrEmpty(currencyId) ? x.Currency : currencyId), StringComparison.InvariantCultureIgnoreCase)).ToList();
                    if (campaingItemId != 0)
                    {
                        if (applicableSubrate.Any(x => x.ItemID == campaingItemId.ToString()))
                        {
                            offerSubrate = true;
                            applicableSubrate = applicableSubrate.Where(x => x.ItemID == campaingItemId.ToString()).ToList();
                        }
                    }
                    List<Subrates> orderLineSubrates = new List<Subrates>();

                    foreach (var rate in applicableSubrate)
                    {
                        string subrateType = string.Empty;
                        var subrateItems = subrateitem.FirstOrDefault(x => x.SubRateItemCode == rate.SubrateItemCode);
                        if (subrateItems != null)
                        { subrateType = subrateItems.DisplayName; }
                        decimal VATAmount = (Math.Round(((rate.Price * rate.VAT) / 100), 2));
                        decimal TotalAmount = rate.Price + VATAmount;
                        orderLineSubrates.Add(new Subrates() { CurrencyId = rate.Currency, PercentageCalculation = rate.IsPercentage, ExVatAmount = rate.Price, SubrateCode = rate.SubrateItemCode, SubrateType = subrateType, VatPercentage = rate.VAT, VatAmount = VATAmount, TotalAmount = TotalAmount });
                    }
                    orderLineSubrate.Add(new OrderLineSubrate() { IsOfferSubrate = offerSubrate, OrderLineId = item.OrderLineId, CampaignId = campaingItemId, ProductSKU = item.Sku, Subrates = orderLineSubrates });
                }

                return orderLineSubrate;
            }
            catch (Exception er)
            {
                throw er;
            }

            //Unreachable code commented
            //return new List<OrderLineSubrate>();
        }



        /// <summary>
        /// List Type GetAllCurrency
        /// </summary>
        /// <param name="_database"></param>
        /// <returns></returns>
        public List<CurrencyDetail> GetAllCurrency(Database _database)
        {
            List<CurrencyDetail> currencyDetail = new List<CurrencyDetail>();

            Item[] currencyList = _database.SelectItems("fast:/sitecore/content/#admin-portal#/global/#_supporting-content#/currency/*[@@templatename='Currency']");


            foreach (Item currency in currencyList)
            {
                currencyDetail.Add(new CurrencyDetail()
                {
                    CurrencyGUID = currency.Fields["ID"].Value,
                    Name = currency.Fields["CurrencyName"].Value
                });
            }

            return currencyDetail;
            /*List<CurrencyDetail> currencyDetail = new List<CurrencyDetail>();
            currencyDetail.Add(new CurrencyDetail() { CurrencyGUID = "119C3405-A770-4691-AD38-717B4F88B03D", Name = "GBP" });
            currencyDetail.Add(new CurrencyDetail() { CurrencyGUID = "49CE930D-6829-44D6-AF60-2631260B7401", Name = "EURO" });*/
        }

        /// <summary>
        /// Save Subrate Award
        /// </summary>
        /// <param name="objSubrates"></param>
        /// <param name="ItemID"></param>
        /// <returns></returns>
        public int SaveSubratesAward(List<SubratesDetail> objSubrates, string ItemID)
        {
            //try
            //{
            //    if (String.IsNullOrEmpty(ItemID))
            //    {
            //        if (objSubrates != null && objSubrates.Any())
            //        {
            //            var subrate = objSubrates.FirstOrDefault();
            //            List<SubrateAwardDetail> subrates = SubrateAwardDetail.All().Where(x => x.ClientID.Equals(subrate.ClientID) && x.ItemID.Equals(subrate.ItemID) && x.Currency.Equals(subrate.Currency)).ToList();
            //            foreach (var item in objSubrates)
            //            {
            //                SubrateAwardDetail subRateExist = subrates.FirstOrDefault(x => x.SubrateItemCode.Equals(item.SubrateItemCode) && x.ClientID.Equals(item.ClientID) && x.ItemID.Equals(item.ItemID)) ?? new SubrateAwardDetail();
            //                subRateExist.ClientID = item.ClientID;
            //                subRateExist.Currency = item.Currency;
            //                subRateExist.CreatedOn = DateTime.Now;
            //                subRateExist.CreatedBy = "Admin";
            //                subRateExist.IsPercentage = item.IsPercentage;
            //                subRateExist.ItemID = item.ItemID;
            //                subRateExist.Price = item.Price;
            //                subRateExist.SubrateItemCode = item.SubrateItemCode;
            //                subRateExist.UpdatedOn = DateTime.Now;
            //                subRateExist.VAT = item.VAT;
            //                subRateExist.Save();

            //            }
            //        }
            //    }
            //    return 1;
            //}
            //catch (Exception ex)
            //{
            //    return 0;
            //}
            return 0;
        }



        /// <summary>
        /// List Type SubrateDetails GetSubRateAwardByClientAndItemId
        /// </summary>
        /// <param name="ItemID"></param>
        /// <param name="clientId"></param>
        /// <param name="currency"></param>
        /// <returns></returns>
        public List<SubratesDetail> GetSubRateAwardByClientAndItemId(string ItemID, string clientId, string currency)
        {
            //List<SubrateAwardDetail> subrateDetails = SubrateAwardDetail.All().Where(x => x.ItemID.Equals(ItemID) && x.ClientID.Equals(clientId) && x.Currency.Equals(currency)).ToList();
            //List<SubratesDetail> lstSubrates = new List<SubratesDetail>();
            //foreach (var item in subrateDetails)
            //{
            //    lstSubrates.Add(new SubratesDetail(item.SubrateItemCode, item.ItemID, item.Price, item.VAT, item.IsPercentage, item.ClientID, item.CreatedBy, item.UpdatedOn, item.Currency));
            //}
            //return lstSubrates;
            return new List<SubratesDetail>();
        }

        //public Item[] GetPriceBandItemById(List<string> priceBandId)
        //{

        //}

        public List<OrderLinePriceBand> GetCommissiondata(Sitecore.Data.Items.Item[] priceBand, int noOfNights)
        {
            List<OrderLinePriceBand> commissionlist = new List<OrderLinePriceBand>();

            Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);

            string query = "fast:/sitecore/content/#admin-portal#/#supplier-setup#/global/#_supporting-content#/commissions//*[@@templatename='Comission' and  {0}]";
            List<string> queryBuilding = new List<string>();
            foreach (var item in priceBand)
            {
                if (item.Fields["commission"] != null)
                {
                    queryBuilding.Add(string.Format("@@id='{0}'", item.Fields["commission"].Value));
                }
            }
            query = string.Format(query, string.Join(" or ", queryBuilding));

            Sitecore.Data.Items.Item[] commission = workingdb.SelectItems(query);
            foreach (Sitecore.Data.Items.Item i in commission)
            {
                OrderLinePriceBand pBand = new OrderLinePriceBand();
                pBand.CommissionId = i.ID.Guid;
                Guid siteCoreItemId = default(Guid);
                if (!string.IsNullOrEmpty(i.Fields[noOfNights.ToString()].Value))
                {
                    if (Guid.TryParse(i.Fields[noOfNights.ToString()].Value, out siteCoreItemId))
                    {
                        pBand.LBSubrate = GetFeesData(i.Fields[noOfNights.ToString()].Value);
                    }
                }
                if (siteCoreItemId == default(Guid) && i.Fields["common"] != null)
                {
                    if (Guid.TryParse(i.Fields["common"].Value, out siteCoreItemId))
                    {
                        pBand.LBSubrate = GetFeesData(i.Fields["common"].Value);
                    }
                }
                commissionlist.Add(pBand);
            }
            return commissionlist;
        }

        public List<PriceBandCommision> GetFeesData(string commisionItemId)
        {
            Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
            Sitecore.Data.Items.Item days = workingdb.GetItem(commisionItemId);

            List<PriceBandCommision> tmpcommsion = new List<PriceBandCommision>();
            List<PriceBandCommision> commsion = new List<PriceBandCommision>();
            if (days == null)
                return new List<PriceBandCommision>();
            var group = days.Fields.GroupBy(x => x.Section);
            foreach (var grp in group)
            {
                if (grp.Key == SiteCoreCommsionConstant.FeePercentage || grp.Key == SiteCoreCommsionConstant.Fees)
                {
                    foreach (var item in grp)
                    {
                        decimal fess = 0;
                        if (decimal.TryParse(item.Value, out fess))
                        {
                            string code = string.Empty;
                            if (item.Name == SiteCoreCommsionConstant.CancellationFee)
                            {
                                code = Constants.CancellationFee;
                            }
                            else if (item.Name == SiteCoreCommsionConstant.CreaditCardFee)
                            {
                                code = Constants.CreditCardProcessingFee;
                            }
                            else if (item.Name == SiteCoreCommsionConstant.TransferFee)
                            {
                                code = Constants.TransferFee;
                            }
                            else if (item.Name == SiteCoreCommsionConstant.LBCommision)
                            {
                                code = Constants.LBCommission;
                            }
                            if (!string.IsNullOrEmpty(code))
                                tmpcommsion.Add(new PriceBandCommision() { Code = code, Commssion = fess, IsPercentage = grp.Key == SiteCoreCommsionConstant.FeePercentage ? true : false });
                        }
                    }
                }
            }
            foreach (var item in tmpcommsion.GroupBy(x => x.Code))
            {
                foreach (var data in item.Where(x => x.Commssion > 0).OrderByDescending(x => x.Commssion))
                {
                    if (!(commsion.Any(x => x.Code == data.Code)))
                        commsion.Add(data);
                }
            }
            if (!commsion.Any(x => x.Code == Constants.LBCommission && x.Commssion > 0))
            {
                return new List<PriceBandCommision>();
            }
            return commsion;
        }

    }
}
