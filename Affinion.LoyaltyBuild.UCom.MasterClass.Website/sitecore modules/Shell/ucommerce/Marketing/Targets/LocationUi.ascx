﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LocationUi.ascx.cs" Inherits="Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.ucommerce.Marketing.Targets.LocationUi" %>
<table cellpadding="0" cellspacing="0" style="width: 100%;">
    <tr>
        <td>
            <asp:PlaceHolder runat="server" ID="NonEditModePlaceHolder">
                <asp:Literal runat="server" ID="ReadOnlyMessage"></asp:Literal>
            </asp:PlaceHolder>

            <asp:PlaceHolder runat="server" ID="EditModePlaceHolder" Visible="false">
               
                <asp:Label runat="server" ID="lblCity" /> <br />
                <asp:CheckboxList runat="server" ID="chklocation"/>
               
            </asp:PlaceHolder>
        </td>
        <td style="width: 50px; text-align: right; vertical-align: top;">
            <asp:ImageButton ID="EditButton" runat="server" ImageUrl="../../Images/pencil.png" meta:resourcekey="Edit" OnClick="EditButton_Click" />
            <asp:ImageButton ID="SaveButton" runat="server" ImageUrl="../../Images/save.gif" meta:resourcekey="Save" Visible="false" OnClick="SaveButton_Click" ValidationGroup="MinOrderAmountGroup" />
            <asp:ImageButton ID="DeleteButton" runat="server" ImageUrl="../../Images/ui/cross.png" meta:resourcekey="Delete" OnClick="DeleteButton_Click" /><br />
        </td>
    </tr>
</table>

