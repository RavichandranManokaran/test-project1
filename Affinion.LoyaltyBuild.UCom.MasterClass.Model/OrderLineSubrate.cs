﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    public class OrderLineSubrate
    {
        public bool IsOfferSubrate { get; set; }

        public int OrderLineId { get; set; }

        public int CampaignId { get; set; }

        public string ProductSKU { get; set; }

        public List<Subrates> Subrates { get; set; }
    }

    public class Subrates
    {
        public bool PercentageCalculation { get; set; }

        public string SubrateType { get; set; }

        public string SubrateCode { get; set; }

        public decimal ExVatAmount { get; set; }

        public decimal VatPercentage { get; set; }

        public decimal VatAmount { get; set; }

        public decimal TotalAmount { get; set; }

        public string CurrencyId { get; set; }

    }
}

