﻿-- =============================================
--	Name		: GetCustomerQueryTypes
--	Purpose		: To get the GetCustomerQueryTypes
--	Author		: Antonysamy
--	Date		: 11/02/2016
-- =============================================

CREATE PROCEDURE [dbo].[OLD_GetCustomerQueryTypes]
AS
BEGIN
SELECT  
		QueryTypeId,
		[Description] QueryType
		FROM uCommerce_QueryTypes 
Order by QueryTypeId  
END

