﻿CREATE PROCEDURE [dbo].[LB_GetPaymentHistory]
@OrderLineId INT	
AS
BEGIN

	DECLARE @ClientId VARCHAR(50)
	SELECT TOP 1 @ClientId = REPLACE(REPLACE([Value],'{',''),'}','') FROM uCommerce_OrderProperty WHERE OrderLineId = @OrderLineId AND [Key] = '_ClientId'

	--Get Order line detail
	SELECT OL.OrderLineId, 
		BI.BookingNo 
	FROM uCommerce_OrderLine OL
		INNER JOIN LB_BookingInfo BI ON OL.OrderLineId = BI.OrderLineId 
	WHERE OL.OrderLineId = @OrderLineId

	--Payment History Total
	SELECT SUM([Total]) [Total], [Currency] FROM
		(SELECT SUM(OLD.TotalAmount) [Total],
				OLD.CurrencyTypeId [Currency]
		FROM uCommerce_OrderLine  OL
			INNER JOIN LB_OrderLineDue OLD ON OLD.OrderLineID = OL.OrderLineId 
		WHERE
			ISNULL(OLD.IsDeleted,0) = 0 AND
			OLD.OrderLineID IN (SELECT OrderLineID FROM [dbo].[LB_FnRelatedOrderLines](@OrderLineId))
		GROUP BY 
			OLD.CurrencyTypeId
		UNION ALL
		SELECT -SUM(OLP.TotalAmount) [Total],
				OLP.CurrencyTypeId [Currency]
		FROM uCommerce_OrderLine  OL
			INNER JOIN LB_OrderLinePayment OLP ON OLP.OrderLineID = OL.OrderLineId 
		WHERE
			OLP.OrderLineID IN (SELECT OrderLineID FROM [dbo].[LB_FnRelatedOrderLines](@OrderLineId))
		GROUP BY 
			OLP.CurrencyTypeId) P
	GROUP BY [Currency]
	HAVING
		SUM([Total]) <> 0

	--PaymentHistory PaymentDues 
		SELECT DISTINCT	OLD.OrderLineDueId [OrderLineDueId],
				OLD.[ParentOrderLineDueId] [ParentId],
				OLD.OrderLineID [OrderLineID],
				OLD.CurrencyTypeId [Currency],
				(OLD.TotalAmount + ISNULL((SELECT SUM(TotalAmount) FROM LB_OrderLineDue WHERE ParentOrderLineDueId = OLD.OrderLineDueId),0)) [Amount],
				--(SELECT max(Description) from uCommerce_ClientIDPaymentMethodMapping where clientid = ca.clientid and paymentmethodid = old.paymentmethodid) as [PaymentMethod], 
				(SELECT max(Description) from [dbo].[LB_FnClientPaymentMethods](@ClientId) WHERE paymentmethodid = OLD.paymentmethodid) as [PaymentMethod],
				--PM.Name [Name],
				DT.[Description] [Reason],
				OLD.CreatedDate [CreatedDate],
				OLD.CreatedBy [CreatedBy],
				DT.CanBeDeleted [CanBeDeleted]
		FROM LB_OrderLineDue OLD 
			INNER JOIN LB_DueTypes DT ON OLD.DueTypeId=dt.DueTypeId		
			INNER JOIN [dbo].[LB_FnClientPaymentMethods](@ClientId) PM ON OLD.PaymentMethodId = PM.PaymentMethodId
			inner join ucommerce_orderline ol on ol.orderlineid = old.OrderLineID
	WHERE 
		ISNULL(OLD.IsDeleted,0) = 0 AND
		OLD.[ParentOrderLineDueId] IS NULL AND
		OLD.OrderLineID IN (SELECT OrderLineID FROM [dbo].[LB_FnRelatedOrderLines](@OrderLineId))
	ORDER BY OLD.CreatedDate

	--Payment History Payments
	SELECT	OLP.OrderLinePaymentID [OrderLinePaymentId],
			OLP.OrderLineID [oderLineID],
			'' as [PaymentMethod],
			--(SELECT max(Description) from uCommerce_ClientIDPaymentMethodMapping where clientid = ca.clientid and paymentmethodid = olp.paymentmethodid) as [PaymentMethod], 
			--PM.Name [Name],
			DT.[Description] [PaymentReason],
			OLP.CommissionAmount [Amount],
			OLP.ReferenceId [ReferenceId],
			OLP.CreatedDate [Createddate],
			OLP.CreatedBy [Createdby],
			OLP.CurrencyTypeId [Currency]			
	FROM LB_OrderLinePayment OLP
		CROSS JOIN LB_DueTypes DT 
	WHERE
		DT.Code = 'COMMISSION' AND
		OLP.CommissionAmount <> 0 AND
		OLP.OrderLineID IN (SELECT OrderLineID FROM [dbo].[LB_FnRelatedOrderLines](@OrderLineId))
	UNION
	SELECT	OLP.OrderLinePaymentID [OrderLinePaymentId],
			OLP.OrderLineID [oderLineID],
			'' as [PaymentMethod],
			--(SELECT max(Description) from uCommerce_ClientIDPaymentMethodMapping where clientid = ca.clientid and paymentmethodid = olp.paymentmethodid) as [PaymentMethod], 
			--PM.Name [Name],
			DT.[Description] [PaymentReason],
			OLP.ProviderAmount [Amount],
			OLP.ReferenceId [ReferenceId],
			OLP.CreatedDate [Createddate],
			OLP.CreatedBy [Createdby],
			OLP.CurrencyTypeId [Currency]			
	FROM LB_OrderLinePayment OLP
		CROSS JOIN LB_DueTypes DT 
	WHERE
		DT.Code = 'PROVIDERAMOUNT' AND
		OLP.ProviderAmount <> 0 AND
		OLP.OrderLineID IN (SELECT OrderLineID FROM [dbo].[LB_FnRelatedOrderLines](@OrderLineId))
	UNION
	SELECT	OLP.OrderLinePaymentID [OrderLinePaymentId],
			OLP.OrderLineID [oderLineID],
			'' as [PaymentMethod],
			--(SELECT max(Description) from [dbo].[LB_FnClientPaymentMethods](@ClientId) WHERE paymentmethodid = olp.paymentmethodid) as [PaymentMethod], 
			--PM.Name [Name],
			DT.[Description] [PaymentReason],
			OLP.ProcessingFeeAmount [Amount],
			OLP.ReferenceId [ReferenceId],
			OLP.CreatedDate [Createddate],
			OLP.CreatedBy [Createdby],
			OLP.CurrencyTypeId [Currency]			
	FROM LB_OrderLinePayment OLP
		CROSS JOIN LB_DueTypes DT 
	WHERE
		DT.Code = 'PROFEE' AND
		OLP.ProcessingFeeAmount <> 0 AND
		OLP.OrderLineID IN (SELECT OrderLineID FROM [dbo].[LB_FnRelatedOrderLines](@OrderLineId))
	
	--Discount History
	SELECT [CampaignItemName] [Name], [AmountOffTotal] [Amount], PO.CurrencyId [Currency]
	FROM uCommerce_OrderLine OL
		INNER JOIN [dbo].[uCommerce_PurchaseOrder] PO ON PO.OrderId = OL.OrderId
		INNER JOIN [dbo].[uCommerce_OrderLineDiscountRelation] OLD ON OL.OrderLineId = OLD.OrderLineId
		INNER JOIN [dbo].[uCommerce_Discount] D ON OLD.DiscountId = D.DiscountId
	WHERE
		OL.OrderLineID IN (SELECT OrderLineID FROM [dbo].[LB_FnRelatedOrderLines](@OrderLineId))
	ORDER BY OL.CreatedOn
END