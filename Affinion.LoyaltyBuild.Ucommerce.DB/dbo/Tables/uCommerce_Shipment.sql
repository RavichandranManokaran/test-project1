﻿CREATE TABLE [dbo].[uCommerce_Shipment] (
    [ShipmentId]        INT            IDENTITY (1, 1) NOT NULL,
    [ShipmentName]      NVARCHAR (128) NOT NULL,
    [CreatedOn]         DATETIME       CONSTRAINT [uCommerce_DF_Shipping_Created] DEFAULT (getdate()) NOT NULL,
    [ShipmentPrice]     MONEY          CONSTRAINT [uCommerce_DF_Shipping_ShippingPrice] DEFAULT ((0)) NOT NULL,
    [ShippingMethodId]  INT            NOT NULL,
    [ShipmentAddressId] INT            NULL,
    [DeliveryNote]      NVARCHAR (MAX) NULL,
    [OrderId]           INT            NOT NULL,
    [TrackAndTrace]     NVARCHAR (512) NULL,
    [CreatedBy]         NVARCHAR (50)  NULL,
    [Tax]               MONEY          DEFAULT ((0)) NOT NULL,
    [TaxRate]           MONEY          DEFAULT ((0)) NOT NULL,
    [ShipmentTotal]     MONEY          DEFAULT ((0)) NOT NULL,
    [ShipmentDiscount]  MONEY          NULL,
    CONSTRAINT [uCommerce_PK_Shipping] PRIMARY KEY CLUSTERED ([ShipmentId] ASC),
    CONSTRAINT [FK_uCommerce_Shipment_uCommerce_OrderAddress] FOREIGN KEY ([ShipmentAddressId]) REFERENCES [dbo].[uCommerce_OrderAddress] ([OrderAddressId]),
    CONSTRAINT [FK_uCommerce_Shipment_uCommerce_ShippingMethod] FOREIGN KEY ([ShippingMethodId]) REFERENCES [dbo].[uCommerce_ShippingMethod] ([ShippingMethodId]),
    CONSTRAINT [uCommerce_FK_Shipment_PurchaseOrder] FOREIGN KEY ([OrderId]) REFERENCES [dbo].[uCommerce_PurchaseOrder] ([OrderId])
);


GO
CREATE NONCLUSTERED INDEX [IX_uCommerce_Shipment_OrderId]
    ON [dbo].[uCommerce_Shipment]([OrderId] ASC)
    INCLUDE([ShipmentId], [ShipmentName], [CreatedOn], [ShipmentPrice], [ShippingMethodId], [ShipmentAddressId], [DeliveryNote], [TrackAndTrace], [CreatedBy], [Tax], [TaxRate], [ShipmentTotal], [ShipmentDiscount]);

