﻿namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal
{
    //<summary>
    /// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
    /// PROPRIETARY INFORMATION The information contained herein (the 
    /// 'Proprietary Information') is highly confidential and proprietary to and 
    /// constitutes trade secrets of Affinion International. The Proprietary Information 
    /// is for Affinion International use only and shall not be published, 
    /// communicated, disclosed or divulged to any person, firm, corporation or 
    /// other legal entity, directly or indirectly, without the prior written 
    /// consent of Affinion International.
    ///
    /// Source File:           hotelPackageSubLayout.cs
    /// Sub-system/Module:     Affinion.LoyaltyBuild.Web
    /// Description:           Used to Bind data to HotelPackageSubLayout
    /// </summary>
    #region Using Directives
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    #endregion
        
    public partial class HotelPackageSubLayout : BaseSublayout
    {
        /// <summary>
        /// Bind Package items to  datasource of PackageRepeater
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Item item = Sitecore.Context.Item;
            try
            {
                Sitecore.Data.Fields.MultilistField multilistField = item.Fields["PackageField"];
                if (multilistField != null)
                {
                    List<Item> list = new List<Item>();
                    //Iterate over all the selected items by using the property TargetIDs
                    foreach (Sitecore.Data.ID id in multilistField.TargetIDs)
                    {
                        Item targetItem = Sitecore.Context.Database.Items[id];
                        list.Add(targetItem);
                    }
                    PackageRepeater.DataSource = list;
                    PackageRepeater.DataBind();
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, null);
                throw;
            }
        }             
    }
}