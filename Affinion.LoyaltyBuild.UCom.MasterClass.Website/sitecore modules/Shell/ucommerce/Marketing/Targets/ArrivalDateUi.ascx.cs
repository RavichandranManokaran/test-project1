﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.EntitiesV2;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using UCommerce.Presentation.Views.Marketing;
using UCommerce.Presentation.Web.Controls;
using UCommerce.Presentation.Web.Marketing;
using Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi;
using Affinion.LoyaltyBuild.UCom.Common.Extension;
using System.Text.RegularExpressions;
using System.Globalization;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.ucommerce.Marketing.Targets
{
    public partial class ArrivalDateUi : ViewEnabledControl<IEditCampaignItemView>, ITargetUi
    {

        protected void Page_Load(object sender, EventArgs e)
        {

            ArrivalDateTarget dataSource = this.DataSource as ArrivalDateTarget;
            this.DeleteButton.OnClientClick = string.Format("return confirm('Delete');");
            this.txtFromDate.Text = dataSource.ArrivalFromDate.ConvertDateToString();
            this.txtToDate.Text = dataSource.ArrivalToDate.ConvertDateToString();
            this.ReadOnlyMessage.Text = this.TargetToString(dataSource);
        }

        protected void SaveButton_Click(object sender, ImageClickEventArgs e)
        {
            if (Page.IsValid)
            {
                ArrivalDateTarget dataSource = this.DataSource as ArrivalDateTarget;
                dataSource.ArrivalFromDate = txtFromDate.Text.ParseFormatDateTime();
                dataSource.ArrivalToDate = txtToDate.Text.ParseFormatDateTime();
                dataSource.Save();
                this.ReadOnlyMessage.Text = this.TargetToString(dataSource);
                this.ToggleEditor();
            }
        }

        protected void ValidateDate(object sender, ServerValidateEventArgs e)
        {
            string controlName = ((BaseValidator)sender).ControlToValidate;
            var control = FindControl(controlName) as TextBox;
            if (Regex.IsMatch(control.Text, @"((0[1-9]|1[0-2])\/((0|1)[1-9]|2[1-9]|3[0-1])\/((19|20)\d\d))$"))
            {
                DateTime dt;
                e.IsValid = DateTime.TryParseExact(e.Value, "MM/dd/yyyy", new CultureInfo("en-US"), DateTimeStyles.None, out dt);
            }
            else
            {
                e.IsValid = false;
            }
        }

        private void ToggleEditor()
        {
            this.NonEditModePlaceHolder.Visible = !this.NonEditModePlaceHolder.Visible;
            this.EditButton.Visible = !this.EditButton.Visible;
            this.EditModePlaceHolder.Visible = !this.EditModePlaceHolder.Visible;
            this.SaveButton.Visible = !this.SaveButton.Visible;
        }

        private string TargetToString(ArrivalDateTarget ArrivalDateTarget)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendFormat("Arrival date ");
            stringBuilder.AppendFormat("From date {0} ", ArrivalDateTarget.ArrivalFromDate.ConvertDateToString());
            stringBuilder.AppendFormat("To date {0} ", ArrivalDateTarget.ArrivalToDate.ConvertDateToString());
            return stringBuilder.ToString();

        }

        protected void DeleteButton_Click(object sender, ImageClickEventArgs e)
        {
            ArrivalDateTarget dataSource = this.DataSource as ArrivalDateTarget;
            if (dataSource != null)
            {
                dataSource.Delete();
            }
            this.Page.Response.Redirect(this.Page.Request.Url.ToString(), true);
        }

        protected void EditButton_Click(object sender, ImageClickEventArgs e)
        {
            this.ToggleEditor();
        }


        public bool EnabledForApply
        {
            get;
            set;
        }

        public bool EnabledForDisplay
        {
            get;
            set;
        }

        public object DataSource
        {
            get;
            set;

        }

        public object New()
        {
            try
            {
                var ArrivalDateTarget = new ArrivalDateTarget();

                ArrivalDateTarget.CampaignItem = View.CampaignItem;
                ArrivalDateTarget.EnabledForApply = true;
                ArrivalDateTarget.ArrivalFromDate = DateTime.Now;
                ArrivalDateTarget.ArrivalToDate = DateTime.Now.AddDays(7);
                ArrivalDateTarget.Save();

                this.DataSource = ArrivalDateTarget;
                return ArrivalDateTarget;
            }
            catch (Exception)
            { 
            
            }
            return null;
        }
    }
}