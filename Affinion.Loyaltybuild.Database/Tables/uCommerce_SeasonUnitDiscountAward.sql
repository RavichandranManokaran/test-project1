CREATE TABLE [dbo].[uCommerce_SeasonUnitDiscountAward](
	[SeasonUnitDiscountAwardId] [int] NOT NULL,
	[Content] [varchar](50) NULL,
 CONSTRAINT [PK_uCommerce_SeasonUnitDiscountAward] PRIMARY KEY CLUSTERED 
(
	[SeasonUnitDiscountAwardId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[uCommerce_SeasonUnitDiscountAward]  WITH CHECK ADD  CONSTRAINT [FK_uCommerce_SeasonUnitDiscountAward_uCommerce_Award] FOREIGN KEY([SeasonUnitDiscountAwardId])
REFERENCES [dbo].[uCommerce_Award] ([AwardId])
GO

ALTER TABLE [dbo].[uCommerce_SeasonUnitDiscountAward] CHECK CONSTRAINT [FK_uCommerce_SeasonUnitDiscountAward_uCommerce_Award]
GO


