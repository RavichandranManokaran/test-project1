﻿using Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration.SiteUI.Base;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration.SiteUI.Presentation;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;
using Sitecore.Data.Items;
using Sitecore.Links;
using System;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;


namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Navigation
{
    public partial class Main_Nav : SitecoreUserControlBase
    {
        private readonly Item homeItem = SiteConfiguration.GetHomeItem();

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Load(object sender, EventArgs e)
        {
            Sitecore.Diagnostics.Tracer.Info("Main Navigation - Page Load");
            Sitecore.Diagnostics.Profiler.StartOperation("Main Navigation - Page Load");

            //load menu items
            LoadMenu();

            Sitecore.Diagnostics.Profiler.EndOperation("Main Navigation - Page Load");

            // microsites are small and have no need for search.
            if (SiteConfiguration.IsMicrosite())
            {
                //txtSearch.Visible = false;
            }
        }

        /// <summary>
        /// load main menus with sub menus
        /// </summary>
        private void LoadMenu()
        {
            Item presentation = SiteConfiguration.GetPresentationSettingsItem();
            if (presentation != null && presentation["Main Menu Type"] == "Inverse") navbar.Attributes.Add("class", "navbar navbar-inverse");
            var currentRole = SitecoreUserProfile.GetItem(UserProfileKeys.currentLoggedinUserRole);

            if (homeItem != null)
            {
                List<Item> nodes = new List<Item>();
                if (homeItem["Show Item In Menu"] == "1") nodes.Add(homeItem);
                foreach (Item i in homeItem.Children)
                {

                    if (SiteConfiguration.DoesItemExistInCurrentLanguage(i) && i["Show Item In Menu"] == "1")
                    {
                        if (i.Name.ToString().Equals("admin"))
                        {
                            if (currentRole != null && (currentRole.ToLower().Equals(Constants.superUser) || currentRole.ToLower().Equals(Constants.accountManagement) || currentRole.ToLower().Contains(Constants.marketing) || currentRole.ToLower().Contains(Constants.IT) || currentRole.ToLower().Equals(Constants.lbAdminRole)))
                                nodes.Add(i);
                            continue;
                        }
                        else if (i.Name.ToString().Equals("group-availability"))
                        {
                            if (currentRole != null && (currentRole.ToLower().Equals(Constants.grpHotelRole)))
                                nodes.Add(i);
                            continue;
                        }
                        else
                        {
                            nodes.Add(i);
                        }
                    }
                }
                rptDropDownMenu.DataSource = nodes;
                rptDropDownMenu.DataBind();
            }

        }

        /// <summary>
        ///  reapeater DropDownMenu ItemDataBound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rptDropDownMenu_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null && (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem))
            {
                Item node = e.Item.DataItem as Item;
                {
                    HyperLink MenuLink = e.Item.FindControl("MenuLink") as HyperLink;
                    Literal MenuText = e.Item.FindControl("MenuText") as Literal;
                    HtmlControl MenuLi = e.Item.FindControl("MenuLi") as HtmlControl;

                    if (MenuLink != null && MenuText != null)
                    {
                        MenuText.Text = node["Menu Title"];
                        if (node["Open Item In a New Tab"] == "1")
                        {
                            MenuLink.Target = "_blank";
                        }
                        MenuLink.NavigateUrl = LinkManager.GetItemUrl(node);
                        if (node.ID == Sitecore.Context.Item.ID) { MenuLi.Attributes.Add("class", "active"); }

                        if (node["Show Children In Menu"] == "1" && node.HasChildren && node.Parent.ID == homeItem.ID)
                        {
                            MenuLink.Attributes.Add("target", "_blank");
                            MenuLi.Attributes.Add("class", "dropdown");
                            MenuLink.Attributes.Add("class", "dropdown-toggle");
                            MenuLink.Attributes.Add("data-toggle", "dropdown");
                            MenuText.Text += "  <b class=\"caret\"></b>";

                            PlaceHolder phSubTree = e.Item.FindControl("phSubMenu") as PlaceHolder;

                            List<Item> nodes = new List<Item>();
                            foreach (Item i in node.Children)
                            {
                                if (SiteConfiguration.DoesItemExistInCurrentLanguage(i) && i["Show Item In Menu"] == "1") { nodes.Add(i); }
                            }

                            Repeater rpt = new Repeater();
                            rpt.DataSource = nodes;
                            rpt.HeaderTemplate = new TopBarRecursiveRepeaterTemplate(ListItemType.Header);
                            rpt.ItemTemplate = rptDropDownMenu.ItemTemplate;
                            rpt.FooterTemplate = new TopBarRecursiveRepeaterTemplate(ListItemType.Footer);
                            rpt.ItemDataBound += new RepeaterItemEventHandler(rptDropDownMenu_ItemDataBound);
                            phSubTree.Controls.Add(rpt);
                            rpt.DataBind();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// txt Search On Text Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txt_Search_OnTextChanged(object sender, EventArgs e)
        {
            //Session["Search"] = txtSearch.Text;
            Response.Redirect(LinkManager.GetItemUrl(SiteConfiguration.GetSearchItem()), true);
        }
    }
}