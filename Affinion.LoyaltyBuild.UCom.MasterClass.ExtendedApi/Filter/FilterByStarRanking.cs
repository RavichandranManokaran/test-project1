﻿using Affinion.LoyaltyBuild.UCom.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.Search.Facets;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi.Filter
{
    public class FilterByStarRanking : IHotelFilter
    {
        public bool IncludeHotel(UCommerce.Documents.Product product, SearchCriteria criteria)
        {
            if (string.IsNullOrEmpty(criteria.StarRanking))
            {
                return true;
            }
            if (criteria.StarRanking == "all")
            {
                return true;
            }
            if (!string.IsNullOrEmpty(criteria.StarRanking))
            {
                KeyValuePair<string, Dictionary<string, object>> experienceProperty = product.Properties.FirstOrDefault(x => x.Value.Any(y => y.Key == ProductDefinationConstant.HotelRating));
                if (experienceProperty.Value != null)
                {
                    foreach (var item in criteria.StarRanking.Split(','))
                    {
                        bool found = experienceProperty.Value.Any(x => x.Value.ToString().Contains(item));
                        if (found)
                        {
                            return true;
                        }
                    }

                }
            }
            return false;
        }


        public UCommerce.Search.Facets.Facet FilterRecordFacet(SearchCriteria criteria)
        {
            if (criteria.StarRanking != "all" && !string.IsNullOrEmpty(criteria.StarRanking))
            {
                var facet = new Facet();
                facet.FacetValues = new List<FacetValue>();
                facet.Name = ProductDefinationConstant.HotelRating;

                //In this example selected values are seperated by a '|' in the querystring value.
                foreach (var value in criteria.StarRanking.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    facet.FacetValues.Add(new FacetValue() { Value = value });
                }
                return facet;
            }
            return null;
        }
    }
}
