﻿//<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           AffinionGeneralPaymentService.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal
/// Description:           Payment service to integrate ucommerce with payment gateway
/// </summary>

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using UCommerce;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure.Globalization;
using UCommerce.Transactions.Payments;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.PaymentService
{
    public class AffinionGeneralPaymentService : AbstractPaymentMethodService
    {

        public override Payment AcquirePayment(Payment paymentToAcquire)
        {
            throw new NotImplementedException();
        }

        public override Payment CancelPayment(Payment paymentToCancel)
        {
            throw new NotImplementedException();
        }

        public override Payment RequestPayment(PaymentRequest paymentRequest)
        {
            if (paymentRequest.Payment == null)
            {
                paymentRequest.Payment = this.CreatePayment(paymentRequest);
            }
            Payment payment = paymentRequest.Payment;

            return payment;
        }

        public override Payment CreatePayment(PaymentRequest request)
        {
            request.PaymentMethod.FeePercent = 0;
            return base.CreatePayment(request);
        }
    }
}