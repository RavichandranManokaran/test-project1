﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SearchKey.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Search
/// Description:           Code is used to store the search keywords.
/// </summary>
namespace Affinion.LoyaltyBuild.Search.Data
{
    #region Using Statements
    using Affinion.LoyaltyBuild.Common;
    using Affinion.LoyaltyBuild.Common.Exceptions;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Search.Entities;
    using Affinion.LoyaltyBuild.Search.SupplierFilters;
    using Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    #endregion

    /// <summary>
    /// Class which includes search related properties.
    /// </summary>
    public class SearchKey
    {
        #region Fields

        private ICollection<string> childrenAges;

        private ICollection<RoomRequest> selectedRooms;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the client item
        /// </summary>
        public Item Client { get; set; }

        /// <summary>
        /// Gets or sets a value indicating client editable status
        /// </summary>
        public bool EnableClient { get; set; }

        /// <summary>
        /// Gets or sets a value indicating transfer booking status
        /// </summary>
        public bool TransferBooking { get; set; }

        /// <summary>
        /// Destination/Hotel name
        /// </summary>
        public string Destination { get; set; }

        /// <summary>
        /// Selected option from the Available Location sub-layout
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// Check-in date
        /// </summary>
        public DateTime CheckinDate { get; set; }

        /// <summary>
        /// Check-out date
        /// </summary>
        public DateTime CheckoutDate { get; set; }

        /// <summary>
        /// Gets the number of nights
        /// </summary>
        public int NoOfNights
        {
            get
            {
                return (int)(CheckoutDate - CheckinDate).TotalDays;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating weather the check-in and checkout dates are flexible
        /// </summary>
        public bool FlexibleDates { get; set; }

        /// <summary>
        /// Number of rooms
        /// </summary>
        public int NumberOfRooms { get; set; }

        /// <summary>
        /// Number of adults
        /// </summary>
        public int NumberOfAdults { get; set; }

        /// <summary>
        /// Number of children
        /// </summary>
        public int NumberOfChildren { get; set; }

        /// <summary>
        /// Number of People
        /// </summary>
        public int NumberOfPeople
        {
            get
            {
                return NumberOfAdults + NumberOfChildren;
            }

        }

        /// <summary>
        /// Number of children
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// Gets or sets a value indicating weather the sorting is enabled or not
        /// </summary>
        public bool EnableSorting { get; set; }

        /// <summary>
        /// Gets or sets the sort mode
        /// </summary>
        public SortMode SortMode { get; set; }

        /// <summary>
        /// Gets or sets the sort value
        /// </summary>
        public string SortValue { get; set; }

        /// <summary>
        /// Gets or sets the search results mode
        /// </summary>
        public SearchResultsMode SearchResultsMode { get; set; }

        /// <summary>
        /// Get or sets the ages of children
        /// </summary>
        public ICollection<RoomRequest> SelectedRooms
        {
            get
            {
                return selectedRooms;
            }

            set
            {
                try
                {
                    selectedRooms = value;
                    SetLessThanTwoChildrenCount();
                    SetNumberOfAdultsAndChildren();
                }
                catch (Exception ex)
                {
                    Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, this);

                }
            }
        }

        /// <summary>
        /// Get or sets the ages of children
        /// </summary>
        public ICollection<string> ChildrenAges
        {
            get
            {
                return childrenAges;
            }
            set
            {
                childrenAges = value;
                SetLessThanTwoChildrenCount();
            }
        }

        public int LessThanTwoChildrenCount
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the page mode
        /// </summary>
        public PageMode Mode { get; set; }



        /// <summary>
        /// Get or sets a list of filters
        /// </summary>
        public ICollection<ISupplierFilter> SupplierFilters { get; set; }

        /// <summary>
        /// Get or Set a StarRanking
        /// </summary>
        public string StarRanking { get; set; }

        /// <summary>
        /// Get or Set a Facilities
        /// </summary>
        public string Facilities { get; set; }

        /// <summary>
        /// Get or Set a Themes
        /// </summary>
        public string Themes { get; set; }

        /// <summary>
        /// Get or Set a Experiences
        /// </summary>
        public string Experiences { get; set; }

        /// <summary>
        /// Get or Set a Local Ranking
        /// </summary>
        public string LocalRanking { get; set; }

        /// <summary>
        /// Gets or sets the offer group/ package
        /// </summary>
        public string OfferGroup { get; set; }

        /// <summary>
        /// Gets or sets the currency code
        /// </summary>
        public string CurrencyCode { get; set; }

        #endregion

        #region Public Methods

        /// <summary>
        /// Sets the sort mode and sort value from a string
        /// </summary>
        /// <param name="sortModeValue">The sort mode as a string</param>
        /// <param name="sortValue">The sort value</param>
        public void SetSortMode(string sortModeValue, string sortValue)
        {
            SortMode sortMode;
            if (Enum.TryParse<SortMode>(sortModeValue, out sortMode))
            {
                this.SortMode = sortMode;
                this.SortValue = sortValue;
            }
            else
            {
                this.SortMode = SortMode.None;
                this.SortValue = string.Empty;
            }
        }

        public SearchKey()
        {
            SelectedRooms = new List<RoomRequest>();
        }

        public SearchKey(Uri url, Item client)
        {
            QueryStringHelper helper = new QueryStringHelper(url);

            this.Client = client;
            this.Destination = helper.GetValue("Destination");
            this.Location = helper.GetValue("Location");
            this.Destination = !string.IsNullOrEmpty(this.Destination) ? this.Destination.ToLowerInvariant() : string.Empty;
            this.Location = !string.IsNullOrEmpty(this.Location) ? this.Location.ToLowerInvariant() : string.Empty;
            this.SetSortMode(helper.GetValue("sortmode"), helper.GetValue("sortvalue"));
            this.SupplierFilters = null;
            this.StarRanking = helper.GetValue("StarRanking");
            this.Facilities = helper.GetValue("Facilities");
            this.Themes = helper.GetValue("Themes");
            this.Experiences = helper.GetValue("Experiences");
            this.LocalRanking = helper.GetValue("LocalRanking");
            this.OfferGroup = helper.GetValue("OfferGroup");
            this.CurrencyCode = helper.GetValue("cur");
            string jsonBase64 = helper.GetValue("JSON");
            SearchKey searchKey = null;
            if (!string.IsNullOrWhiteSpace(jsonBase64))
            {
                var data = Convert.FromBase64String(jsonBase64);
                string json = Encoding.UTF8.GetString(data);

                searchKey = Newtonsoft.Json.JsonConvert.DeserializeObject(json, typeof(SearchKey)) as SearchKey;
            }

            PageMode pageMode = this.Mode = PageMode.Locations;
            if (Enum.TryParse<PageMode>(helper.GetValue("Mode"), out pageMode))
            {
                this.Mode = pageMode;
            }
            if (searchKey != null)
            {
                SelectedRooms = searchKey.SelectedRooms;

                if (IsChildBedSelectionAvailable)
                {
                    NumberOfRooms = searchKey.NumberOfRooms;
                }
            }

            this.UpdateDates(helper);
            this.UpdateOcupancyDetails(helper);
        }

        #endregion

        #region Private Methods

        public static bool IsChildBedSelectionAvailable
        {
            get
            {
                return ItemHelper.GetBooleanClientRule("Enable child bed selection");
                //Item clientItem = SitecoreFieldsHelper.GetLookupFieldTargetItem(ItemHelper.RootItem, "ClientDetails");

                //if (clientItem == null)
                //{
                //    return false;
                //}

                ////Get a multilist field from the current item
                //Sitecore.Data.Fields.MultilistField multilistField = clientItem.Fields["Client Level Rules"];
                //if (multilistField != null)
                //{
                //    //Iterate over all the selected items by using the property TargetIDs
                //    foreach (ID id in multilistField.TargetIDs)
                //    {
                //        Item targetItem = Sitecore.Context.Database.Items[id];

                //        if (targetItem.Name.Equals("childbedselectionavailable"))
                //        {
                //            return true;
                //        }
                //    }
                //}

                //return false;
            }

        }

        /// <summary>
        /// Update date fields
        /// </summary>
        /// <param name="helper">Query helper</param>
        public void UpdateDates(QueryStringHelper helper)
        {
            string checkinDateValue = helper.GetValue("CheckinDate");
            string checkoutDateValue = helper.GetValue("CheckoutDate");
            bool specificDates = false;

            if (!string.IsNullOrEmpty(checkinDateValue) && !string.IsNullOrEmpty(checkoutDateValue))
            {
                this.CheckinDate = DateTimeHelper.ParseDate(checkinDateValue);
                this.CheckoutDate = DateTimeHelper.ParseDate(checkoutDateValue);
            }

            if (Boolean.TryParse(helper.GetValue("FlexibleDates"), out specificDates))
            {
                this.FlexibleDates = specificDates;
            }
        }

        /// <summary>
        /// Update occupancy details
        /// </summary>
        /// <param name="helper">Query helper</param>
        private void UpdateOcupancyDetails(QueryStringHelper helper)
        {
            int noOfAdults = 0;
            if (int.TryParse(helper.GetValue("NoOfAdults"), out noOfAdults))
            {
                this.NumberOfAdults = noOfAdults;
            }

            int noOfChildren = 0;
            if (int.TryParse(helper.GetValue("NoOfChildren"), out noOfChildren))
            {
                this.NumberOfChildren = noOfChildren;
            }

            string childrenAgesString = helper.GetValue("ChildAge");
            if (!string.IsNullOrWhiteSpace(childrenAgesString))
            {
                this.ChildrenAges = childrenAgesString.Split(',');
            }

            if (!IsChildBedSelectionAvailable)
            {

                int noOfRooms = 0;

                if (int.TryParse(helper.GetValue("NoOfRooms"), out noOfRooms))
                {
                    this.NumberOfRooms = noOfRooms;
                }
            }

        }

        /// <summary>
        /// Get less than two years old children count
        /// </summary>
        /// <returns></returns>
        private void SetLessThanTwoChildrenCount()
        {
            int lessThantwoChildren = 0;

            bool isChildBedSelectionAvailable = IsChildBedSelectionAvailable;

            if (isChildBedSelectionAvailable)
            {
                foreach (RoomRequest room in SelectedRooms)
                {
                    lessThantwoChildren += room.ChildrenBeds.Count(a =>
                    {
                        int childAge;

                        ///if age is 0-12 months treat it as less than two years.
                        if (a.Age.Equals(LanguageReader.GetText(Constants.ZeroToTwelveMonthsDictionaryKey, "0-12 months")))
                        {
                            return true;
                        }

                        if (int.TryParse(a.Age, out childAge))
                            return childAge <= 2;
                        else
                            throw new AffinionException("Invalid Age");
                    });
                }
                //lessThantwoChildren = ChildrenAges.Count(a =>
                //{
                //    int childAge;

                //    ///if age is 0-12 months treat it as less than two years.
                //    if (a.Equals(LanguageReader.GetText(Constants.ZeroToTwelveMonthsDictionaryKey, "0-12 months")))
                //    {
                //        return true;
                //    }

                //    if (int.TryParse(a, out childAge))
                //        return childAge <= 2;
                //    else
                //        throw new AffinionException("Invalid Age");

                //});
            }


            else if (!isChildBedSelectionAvailable && ChildrenAges == null)
            {
                LessThanTwoChildrenCount = 0;
                return;
            }
            else if (!isChildBedSelectionAvailable)
            {

                lessThantwoChildren = ChildrenAges.Count(a =>
               {
                   int childAge;

                   ///if age is 0-12 months treat it as less than two years.
                   if (a.Equals(LanguageReader.GetText(Constants.ZeroToTwelveMonthsDictionaryKey, "0-12 months")))
                   {
                       return true;
                   }

                   if (int.TryParse(a, out childAge))
                       return childAge <= 2;
                   else
                       throw new AffinionException("Invalid Age");

               });
            }



            //return lessThantwoChildren;
            LessThanTwoChildrenCount = lessThantwoChildren;
        }

        private void SetNumberOfAdultsAndChildren()
        {
            int adultCount = 0;
            int childrenCount = 0;

            if (IsChildBedSelectionAvailable)
            {
                foreach (RoomRequest room in SelectedRooms)
                {
                    adultCount += room.AdultCount;

                    childrenCount += room.ChildrenBeds.Count;

                }

            }

            NumberOfAdults = adultCount;
            NumberOfChildren = childrenCount;
        }

        #endregion

    }
    public static class SeachKeyExtension
    {
        public static SearchCriteria ConvertSearchKeyToCriteria(this SearchKey searchTerm)
        {
            SearchCriteria criteria = new SearchCriteria();
            criteria.ArrivalDate = searchTerm.CheckinDate;
            criteria.Night = searchTerm.NoOfNights;
            criteria.NumberOfAdult = searchTerm.NumberOfAdults;
            criteria.NumberOfChild = searchTerm.NumberOfChildren;
            criteria.Themes = searchTerm.Themes.ToLower();
            criteria.Location = searchTerm.Destination;
            criteria.StarRanking = searchTerm.StarRanking;
            criteria.LocalRanking = searchTerm.LocalRanking;
            criteria.Facility = searchTerm.Facilities.ToLower();

            return criteria;
        }
    }

}
