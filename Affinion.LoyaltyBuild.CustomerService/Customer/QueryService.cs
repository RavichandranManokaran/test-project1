﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Helpers;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Sitecore.Data.Items;
using Affinion.LoyaltyBuild.Common.Instrumentation;
/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/
namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Customer
{
    public class QueryService : IQueryService
    {

        //Private variables
        private string _connectionString = "uCommerce";
        private const string querytypes = "/sitecore/content/#admin-portal#/#global#/#_supporting-content#/#querytypes#//*[@@templateid='{A2D01DE0-518E-49DE-9199-CCEF8853F045}']";
        private Sitecore.Data.Database context = Sitecore.Context.Database;
        
        /// <summary>
        /// constructor
        /// </summary>
        public QueryService()
        { }


        # region IQueryService Implementation

        /// <summary>
        /// To Save Customer Query
        /// </summary>
        /// <param name="customerQuery">Booking Query</param>
        /// <returns> ValidationResponse  </returns>
        public ValidationResponse SaveCustomerQuery(CustomerQuery customerQuery)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);

            var sql = "LB_ManageCustomerQuery";
            var result = new ValidationResponse();

            using (var command = db.GetStoredProcCommand(sql))
            {
                try
                {
                    db.AddInParameter(command, "@QueryId", DbType.Int32, customerQuery.ID);
                    db.AddInParameter(command, "@OrderLineId", DbType.Int32, customerQuery.OrderLineId);
                    db.AddInParameter(command, "@CustomerId", DbType.Int32, customerQuery.CustomerId);
                    db.AddInParameter(command, "@QueryTypeId", DbType.Int32, customerQuery.QueryTypeId);
                    db.AddInParameter(command, "@Content", DbType.String, customerQuery.Content);
                    db.AddInParameter(command, "@IsSolved", DbType.Boolean, customerQuery.IsSolved);
                    db.AddInParameter(command, "@PartnerId", DbType.String, customerQuery.ClientId);
                    db.AddInParameter(command, "@CampaignId", DbType.String, customerQuery.CampaignGroupID);
                    db.AddInParameter(command, "@ProviderId", DbType.String, customerQuery.ProviderId);
                    db.AddInParameter(command, "@CreatedBy", DbType.String, customerQuery.CreatedBy);
                    db.ExecuteNonQuery(command);

                    result.IsSuccess = true;
                    result.Message = "Customer query saved successfully";
                }
                catch
                {
                    result.IsSuccess = false;
                    result.Message = "Error occurred while saving Customer query"; 
                }
            }
            return result;
        }


        /// <summary>
        /// To get the Customer Notes
        /// </summary>
        /// <param name="OrderLineID">OrderLineID</param>
        /// <returns>BookingNotes object List  </returns>
        public List<CustomerQuery> GetCustomerQuerys(int orderLineID)
        {
            List<CustomerQuery> customerQuerys = new List<CustomerQuery>();
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "LB_GetCustomerQuery";
            using (var command = db.GetStoredProcCommand(sql))
            {
                db.AddInParameter(command, "@OrderLineId", DbType.Int32, orderLineID);
                using (var reader = db.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        var customerQuery = new CustomerQuery
                        {
                            ID = Convert.ToInt32(reader["QueryId"]),
                            QueryTypeId = Convert.ToInt32(reader["QueryTypeId"]),
                           // QueryType = Convert.ToString(reader["Description"]),
                            OrderLineId = Convert.ToInt32(reader["OrderLineId"]),
                            Content = Convert.ToString(reader["Content"]),
                            IsSolved = Convert.ToBoolean(reader["IsSolved"]),                            
                            CreatedDate = Convert.ToDateTime(reader["CreatedDate"])
                        };

                        var userName = Convert.ToString(reader["CreatedBy"]).Split('\\');
                        if (userName.Count() > 0)
                            customerQuery.CreatedBy = userName[0];

                        customerQuerys.Add(customerQuery);
                    }
                }
            }

            if (customerQuerys.Any())
            {
                DataTable queryType = GetQueryTypes();
                foreach(CustomerQuery item in customerQuerys)
                {
                    item.QueryType = queryType.AsEnumerable().Where(r => ((int)r["QueryTypeId"]) == item.QueryTypeId).Select(r => r["QueryType"].ToString()).Single();                 
                }
            }
            return customerQuerys;
        }


        /// <summary>
        /// To get the Customer query
        /// </summary>
        /// <param name="OrderLineID">AttachmentId</param>
        /// <returns>attachment as bytes</returns>
        public byte[] GetCustomerQuerysAttachments(int AttachmentId)
        {
            return null;
        }

        /// <summary>
        /// To get the Customer Query Types
        /// </summary>
        /// <returns> Retun Qyery Type datatable </returns>
        //public DataTable GetCustomerQueryTypes()
        //{
        //    DatabaseProviderFactory factory = new DatabaseProviderFactory();
        //    Database db = factory.Create(_connectionString);
        //    var sql = "GetCustomerQueryTypes";
        //    DataTable customerQueryTypes = null;
        //    using (var command = db.GetStoredProcCommand(sql))
        //    {
        //        var dataSet = db.ExecuteDataSet(command);
        //        customerQueryTypes = dataSet.Tables[0];
        //    }
        //    return customerQueryTypes;
        //}


        /// <summary>
        /// To get the Customer Query Types
        /// </summary>
        /// <returns> Retun Qyery Type from Sitecore </returns>
        public  DataTable GetQueryTypes(bool IsActive=true)
        {
            Item[] QueryTypes = context.SelectItems(querytypes);
            DataTable queryType = new DataTable();
            queryType.Columns.Add("QueryTypeId",Type.GetType("System.Int32"));
            queryType.Columns.Add("QueryType", Type.GetType("System.String"));

            if (QueryTypes != null)
            {
                foreach(Item item in QueryTypes)
                {
                    DataRow dr =queryType.NewRow(); 
                    dr["QueryTypeId"] =item.Fields["QueryTypeId"].Value;
                    dr["QueryType"] = item.DisplayName;

                    if(IsActive)
                    {
                        if(item.Fields["IsActive"].Value == "1")
                            queryType.Rows.Add(dr);
                    }
                        else
                         queryType.Rows.Add(dr);
                }
            }
            return queryType;
        }

        /// <summary>
        /// To get the Customer Query based on criteria 
        /// </summary>
        /// <param name="searchDetails">Search Details</param>
        /// <returns> Return Customer Query as datatable </returns>
        public List<SearchQueryResultItem> SearchCustomerQuery(CustomerQuerySearch searchDetails)
        {
            try
            {
                DatabaseProviderFactory factory = new DatabaseProviderFactory();
                Database db = factory.Create(_connectionString);
                var sql = "LB_SearchCustomerQuery";

                List<SearchQueryResultItem> result = new List<SearchQueryResultItem>();

                using (var command = db.GetStoredProcCommand(sql))
                {
                    db.AddInParameter(command, "@queryTypeID", DbType.Int32, searchDetails.QueryTypeID);
                    db.AddInParameter(command, "@clientId", DbType.String, searchDetails.ClientID);
                    db.AddInParameter(command, "@startDate", DbType.Date, searchDetails.StartDate);
                    db.AddInParameter(command, "@endDate", DbType.Date, searchDetails.EndDate);
                    db.AddInParameter(command, "@firstname", DbType.String, searchDetails.FirstName);
                    db.AddInParameter(command, "@lastname", DbType.String, searchDetails.LastName);
                    db.AddInParameter(command, "@campaignGroupId", DbType.String, searchDetails.CampaignGroupID);
                    db.AddInParameter(command, "@providerId", DbType.String, searchDetails.ProviderId);
                    db.AddInParameter(command, "@bookingReference", DbType.String, searchDetails.BookingReference);
                    db.AddInParameter(command, "@isSolved", DbType.Boolean, searchDetails.IsResolved);

                    using (var reader = db.ExecuteReader(command))
                    {
                        int id = 0;
                        while (reader.Read())
                        {
                            var customerQuery = new SearchQueryResultItem
                            {
                                ID = ++id,
                                QueryId = reader.Value<int>("QueryId"),
                                CustomerName = reader.Value<string>("Name"),
                                Email = reader.Value<string>("EmailAddress"),
                                Phone = reader.Value<string>("PhoneNumber"),
                                Created = reader.Value<string>("CreatedDate"),
                                Updated = reader.Value<string>("UpdatedDate"),
                                IsResolved = reader.Value<bool>("IsSolved"),
                                Content = reader.Value<string>("Content"),
                                HasAttachments = reader.Value<int>("HasAttachments") > 0 ? true : false,
                                ClientId = reader.Value<string>("ClientId"),
                                FirstName = reader.Value<string>("FirstName"),
                                LastName = reader.Value<string>("LastName"),
                                BookingReference = reader.Value<string>("BookingReference"),
                                ProviderName = reader.Value<string>("ProviderName")
                            };
                            result.Add(customerQuery);
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.MasterClassBusinessLogic, ex, this);
                throw;

                //return new List<SearchQueryResultItem>();
            }

           
        }


        /// <summary>
        /// To get the Customer Query based on criteria 
        /// </summary>
        /// <param name="searchDetails">Search Details</param>
        /// <returns> Return Customer Query as datatable </returns>
        public ValidationResponse SearchCustomer(Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model.Customer customer)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "LB_SearchCustomer";
            var result = new ValidationResponse();
            int Customer_Id = 0;

            using (var command = db.GetStoredProcCommand(sql))
            {
                db.AddInParameter(command, "@FirstName", DbType.String, customer.FirstName);
                db.AddInParameter(command, "@LastName", DbType.String, customer.LastName);
                db.AddInParameter(command, "@PhoneNumber", DbType.String, customer.Phone);
                db.AddInParameter(command, "@AddressLine1", DbType.String, customer.AddressLine1);
                db.AddInParameter(command, "@AddressLine2", DbType.String, customer.AddressLine2);
                db.AddInParameter(command, "@CountryId", DbType.Int32, customer.CountryId);
                db.AddInParameter(command, "@LocationId", DbType.Int32, customer.LocationId);
                db.AddInParameter(command, "@ClientId", DbType.String, customer.ClientId);

                using (var reader = db.ExecuteReader(command))
                {
                    int count = 0;
                    while (reader.Read())
                    {
                        Customer_Id = reader.Value<int>("CustomerId");
                        count++;
                    }
                    if (count > 1)
                    {
                        result.Message = "More than One customer found";
                        result.IsSuccess = false;
                    }
                    else if (count == 0)
                    {
                        result.Message = "No Customer found for this search criteria";
                        result.IsSuccess = false;
                    }
                    if (count == 1)
                    {
                        result.Id = Customer_Id;
                        result.IsSuccess = true;
                        result.Message = "Customer record found";
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// To delete the Customer Query based on criteria 
        /// </summary>
        /// <param name="queryId">queryId</param>
        /// <returns> Retun ValidationResponsee </returns>
        public ValidationResponse DeleteCustomerQuery(int queryId)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "LB_DeleteCustomerQuery";
            var result = new ValidationResponse();

            using (var command = db.GetStoredProcCommand(sql))
            {
                try
                {
                    db.AddInParameter(command, "@QueryId", DbType.Int32, queryId);
                    db.ExecuteNonQuery(command);
                    result.IsSuccess = true;
                    result.Message = "Customer query deleted succesfully";
                }
                catch
                {
                    result.IsSuccess = false;
                    result.Message = "Error occurred while deleting Customer query";
                }

            }
            return result;
        }

        /// <summary>
        /// To delete the Customer Query based on criteria 
        /// </summary>
        /// <param name="queryId">attachementId</param>
        /// <returns> Retun ValidationResponsee </returns>
        public ValidationResponse DeleteCustomerQueryAttachments(int queryId)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "LB_DeleteCustomerQueryAttachments";
            var result = new ValidationResponse();

            using (var command = db.GetStoredProcCommand(sql))
            {
                try
                {
                    db.AddInParameter(command, "@QueryId", DbType.Int32, queryId);
                    db.ExecuteNonQuery(command);
                    result.IsSuccess = true;
                    result.Message = "Customer query deleted succesfully";
                }
                catch
                {
                    result.IsSuccess = false;
                    result.Message = "Error occurred while deleting Customer query attachment";
                }

            }
            return result;
        }


        /// <summary>
        /// To Save Customer Query
        /// </summary>
        /// <param name="customerQuery">Booking Query</param>
        /// <returns> ValidationResponse  </returns>
        public ValidationResponse AddCustomerAttachment(CustomerQueryAttachment customerQueryAttachment)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);

            var sql = "LB_ManageCustomerQueryAttachments";
            var result = new ValidationResponse();

            using (var command = db.GetStoredProcCommand(sql))
            {
                try
                {
                    db.AddInParameter(command, "@QueryId", DbType.Int32, customerQueryAttachment.QueryId);
                    db.AddInParameter(command, "@FileType", DbType.String, customerQueryAttachment.FileType);
                    db.AddInParameter(command, "@FileName", DbType.String, customerQueryAttachment.FileName);
                    db.AddInParameter(command, "@FileContent", DbType.Binary, customerQueryAttachment.File);
                    //db.Parameters.Add("@FileContent", SqlDbType.VarBinary, customerQueryAttachment.File.Length).Value = customerQueryAttachment.File;

                    db.ExecuteNonQuery(command);

                    result.IsSuccess = true;
                    result.Message = "Customer query attachment saved successfully";
                }
                catch
                {
                    result.IsSuccess = false;
                    result.Message = "Error occurred while saving Customer query attachment"; ;
                }
            }
            return result;
        }

        /// <summary>
        /// To Save Customer Query
        /// </summary>
        /// <param name="customerQuery">Booking Query</param>
        /// <returns> ValidationResponse  </returns>
        public ValidationResponse UpdateCustomerQuery(CustomerQuery customerQuery)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);

            var sql = "LB_UpdateCustomerQuery";
            var result = new ValidationResponse();

            using (var command = db.GetStoredProcCommand(sql))
            {
                try
                {
                    db.AddInParameter(command, "@QueryId", DbType.Int32, customerQuery.ID);
                    db.AddInParameter(command, "@QueryTypeId", DbType.Int32, customerQuery.QueryTypeId);
                    db.AddInParameter(command, "@IsSolved", DbType.Byte, customerQuery.IsSolved);
                    db.AddInParameter(command, "@Content", DbType.String, customerQuery.Content);

                    db.ExecuteNonQuery(command);

                    result.IsSuccess = true;
                    result.Message = "Customer query updated successfully";
                }
                catch
                {
                    result.IsSuccess = false;
                    result.Message = "Error occurred while saving Customer query"; ;
                }
            }
            return result;
        }



        /// <summary>
        /// To get the Customer Notes
        /// </summary>
        /// <param name="OrderLineID">OrderLineID</param>
        /// <returns>BookingNotes object List  </returns>
        public CustomerQuery GetCustomerQueryDetailById(int queryId)
        {
            CustomerQuery query = new CustomerQuery();
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "LB_GetCustomerQueryDetailById";
            using (var command = db.GetStoredProcCommand(sql))
            {
                db.AddInParameter(command, "@QueryId", DbType.Int32, queryId);
                using (var reader = db.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        var customerQuery = new CustomerQuery
                        {
                            ID = Convert.ToInt32(reader["QueryId"]),
                            QueryTypeId = Convert.ToInt32(reader["QueryTypeId"]),
                            OrderLineId = Convert.ToInt32(reader["OrderLineId"]),
                            Content = Convert.ToString(reader["Content"]),
                            IsSolved = Convert.ToBoolean(reader["IsSolved"]),
                            ClientId = Convert.ToString(reader["PartnerId"]),
                            CampaignGroupID = Convert.ToString(reader["CampaignId"]),
                            ProviderId = Convert.ToString(reader["ProviderId"]),
                            BookingReference = Convert.ToString(reader["BookingReference"]),
                            HasAttachment = Convert.ToBoolean(reader["HasAttachment"])
                        };
                        query = customerQuery;
                    }
                }
            }

            return query;
        }

        /// <summary>
        /// To get the Customer Query attachment
        /// </summary>
        /// <param name="queryId">QueryId</param>
        /// <returns>CustomerQueryAttachment object </returns>
        public CustomerQueryAttachment GetQueryAttachment(int queryId)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "LB_GetCustomerQueryAttachment";
            using (var command = db.GetStoredProcCommand(sql))
            {
                db.AddInParameter(command, "@QueryId", DbType.Int32, queryId);
                using (var reader = db.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        var customerQuery = new CustomerQueryAttachment();
                        //reader["FileContent"]
                        //reader["FileContent"]
                        customerQuery.File = ((byte[])reader["FileContent"]);
                        customerQuery.FileType = Convert.ToString(reader["FileType"]);
                        customerQuery.FileName = Convert.ToString(reader["FileName"]);
                        customerQuery.QueryId = Convert.ToInt32(reader["QueryId"]);

                        return customerQuery;
                    }
                }
            }
            return null;
        }
        # endregion
    }
}

