﻿using System.Xml.Serialization;
using System;
using Affinion.LoyaltyBuild.BedBanks.General;

namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Model.Auth
{
    [Serializable()]
    [XmlType(TypeName = "LoginDetails")]
    public class JtLoginDetails
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Locale { get; set; }
        public int CurrencyID { get; set; }
        public string AgentReference { get; set; }
    }
}
