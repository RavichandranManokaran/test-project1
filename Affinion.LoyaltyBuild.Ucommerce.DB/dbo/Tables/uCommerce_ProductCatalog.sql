﻿CREATE TABLE [dbo].[uCommerce_ProductCatalog] (
    [ProductCatalogId]       INT              IDENTITY (1, 1) NOT NULL,
    [ProductCatalogGroupId]  INT              NOT NULL,
    [Name]                   NVARCHAR (60)    NOT NULL,
    [PriceGroupId]           INT              NOT NULL,
    [ShowPricesIncludingVAT] BIT              CONSTRAINT [uCommerce_DF_Catalog_ShowPricesIncludingVAT] DEFAULT ((1)) NOT NULL,
    [IsVirtual]              BIT              CONSTRAINT [uCommerce_DF_ProductCatalog_IsVirtual] DEFAULT ((0)) NOT NULL,
    [DisplayOnWebSite]       BIT              CONSTRAINT [uCommerce_DF_ProductCatalog_DisplayOnWebSite] DEFAULT ((0)) NOT NULL,
    [LimitedAccess]          BIT              CONSTRAINT [uCommerce_DF_ProductCatalog_LimitedAccess] DEFAULT ((0)) NOT NULL,
    [Deleted]                BIT              CONSTRAINT [uCommerce_DF_ProductCatalog_Deleted] DEFAULT ((0)) NOT NULL,
    [CreatedOn]              DATETIME         CONSTRAINT [uCommerce_DF_ProductCatalog_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedOn]             DATETIME         CONSTRAINT [uCommerce_DF_ProductCatalog_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]              NVARCHAR (512)   CONSTRAINT [uCommerce_DF_ProductCatalog_CreatedBy] DEFAULT (N'(Unknown)') NOT NULL,
    [ModifiedBy]             NVARCHAR (512)   CONSTRAINT [uCommerce_DF_ProductCatalog_ModifiedBy] DEFAULT (N'(Unknown)') NOT NULL,
    [SortOrder]              INT              DEFAULT ((0)) NOT NULL,
    [Guid]                   UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    CONSTRAINT [uCommerce_PK_Catalog] PRIMARY KEY CLUSTERED ([ProductCatalogId] ASC),
    CONSTRAINT [uCommerce_FK_Catalog_CatalogGroup] FOREIGN KEY ([ProductCatalogGroupId]) REFERENCES [dbo].[uCommerce_ProductCatalogGroup] ([ProductCatalogGroupId]),
    CONSTRAINT [uCommerce_FK_Catalog_PriceGroup] FOREIGN KEY ([PriceGroupId]) REFERENCES [dbo].[uCommerce_PriceGroup] ([PriceGroupId])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_ProductCatalog_UniqueName]
    ON [dbo].[uCommerce_ProductCatalog]([Name] ASC, [ProductCatalogGroupId] ASC);

