﻿CREATE TABLE [dbo].[LB_BookingDateThresoldTarget] (
    [BookingDateThresoldTargetId] INT      NOT NULL,
    [MinHours]     INT NULL,
    [MaxHours]     INT NULL,
    CONSTRAINT [PK_BookingDateThresoldTarget] PRIMARY KEY CLUSTERED ([BookingDateThresoldTargetId] ASC),
    CONSTRAINT [FK_BookingDateThresoldTarget_Target] FOREIGN KEY ([BookingDateThresoldTargetId]) REFERENCES [dbo].[uCommerce_Target] ([TargetId])
);

