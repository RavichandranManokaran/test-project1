﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           OfferFilterData.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Search (VS project name)
/// Description:           Data class for offer filters
/// </summary>
namespace Affinion.LoyaltyBuild.Search.OfferFilters
{
    #region Using Statements

    using Affinion.LoyaltyBuild.Search.Data;
    using Sitecore.Data.Items;

    #endregion

    /// <summary>
    /// Data class for offer filters
    /// </summary>
    public class OfferFilterData
    {
        /// <summary>
        /// Holds offer
        /// </summary>
        public OfferDataItem Offer { get; set; }

        /// <summary>
        /// Search keywords
        /// </summary>
        public SearchKey SearchTerm { get; set; }

        /// <summary>
        /// Related client
        /// </summary>
        public Item Client { get; set; }

        /// <summary>
        /// Product supplier
        /// </summary>
        public Item Supplier { get; set; }
    }
}
