﻿ALTER TABLE [dbo].[uCommerce_RoomReservation] ADD
	[OrderReference] VARCHAR(20) NULL,
	[NewOrderReference] VARCHAR(20) NULL,
	[IsCancelled] BIT NULL,
	[OrderLineStatusID] INT NULL
GO

ALTER TABLE dbo.uCommerce_RoomReservation ADD
	CheckInDate datetime NULL,
	CheckOutDate datetime NULL
GO


ALTER TABLE dbo.uCommerce_RoomReservation ALTER COLUMN
	OrderReference varchar(50) NULL
GO

ALTER TABLE dbo.uCommerce_RoomReservation ALTER COLUMN
	[NewOrderReference] varchar(50) NULL

GO

ALTER TABLE  [dbo].[uCommerce_RoomReservation] ADD
	[BookingThrough] VARCHAR(50) NULL,
	[CreatedBy] VARCHAR(50) NULL
GO
