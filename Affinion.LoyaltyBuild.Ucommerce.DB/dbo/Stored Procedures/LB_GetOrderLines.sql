﻿--CREATE PROCEDURE [dbo].[LB_GetOrderLines]
--(@OrderId INT)
--AS
--BEGIN
--	SELECT OL.OrderLineId, OL.Sku, OL.Quantity, OL.ProductName, RR.CheckInDate, RR.CheckOutDate
--	FROM   uCommerce_OrderLine AS OL LEFT JOIN
--		   LB_RoomReservation AS RR ON OL.OrderLineId = RR.OrderLineId
--	WHERE  (OL.OrderId = @OrderId)
--END


CREATE PROCEDURE [dbo].[LB_GetOrderLines]
	(	@OrderId INT	)
AS
BEGIN
	SELECT OL.OrderLineId, OL.Sku, OL.VariantSku, OL.Quantity, OL.ProductName, 
		   BI.CheckInDate, BI.CheckOutDate
	
	FROM   uCommerce_OrderLine OL 
		   LEFT JOIN LB_BookingInfo BI ON OL.OrderLineId = BI.OrderLineId
	
	WHERE  (OL.OrderId = @OrderId)
END
