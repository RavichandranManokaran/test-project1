﻿CREATE PROCEDURE LB_UpdateStatusOnDueAndPay
(	
	@OrderLineId INT,
	@CreatedBy VARCHAR(50)
)
AS
BEGIN
	SELECT @OrderLineId = MAX(OrderLineId) FROM [dbo].[LB_FnRelatedOrderLines](@OrderLineId)

	EXEC [dbo].[LB_AddBookingStatus] @OrderLineId = @OrderLineId, @CreatedBy = @CreatedBy
END
