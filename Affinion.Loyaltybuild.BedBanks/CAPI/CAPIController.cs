﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using System.IO;
using Affinion.Loyaltybuild.BedBanks.CAPI.Message;
using Affinion.Loyaltybuild.BedBanks.CAPI.Model;
using System.Net;
using System.Web.Script.Serialization;

namespace Affinion.Loyaltybuild.BedBanks.CAPI
{
    /// <summary>
    /// This is the main Controller class for communication between CAPI and the Loyaltybuild System.
    /// 
    /// The class is used to create API requests via the 'Model' and 'Message' objects within
    /// this namespace. 
    /// 
    /// The results can be bound to asp repeaters on the presentation layer.
    /// </summary>
    public static class CapiController
    {
        //TODO : These Values should go into a Sitecore Object for easy editing
        public static string Endpoint = "https://capi.cxloyalty.com/Demo/TravelAPI/Hotel/v1/Search";
        public static string RoomEndpoint = "https://capi.cxloyalty.com/Demo/TravelAPI/Hotel/v1/Room";
        public static string Apikey = "160a1393c1b24c9cb4eee0a378be1f95";

        /// <summary>
        /// This function will eventually call CAPI to perform a Search Request.
        /// Currently I am reading sample JS from a .js file to mimic the response.
        /// </summary>
        /// <param name="req">The Request Object</param>
        /// <returns>A CAPISearchResponse Object - Comprising of a Session Key and List of Hotel Objects</returns>
        public static CapiSearchResponse PerformApiSearchRequest(CapiSearchRequest req)
        {
            string response;

            //TODO : Actual API call when access granted
            //Mock the response by reading in some sample JSON from a .js file
            //using (StreamReader sr = new StreamReader(VirtualPathProvider.OpenFile("/example_search_response.js")))
            //{
            //    response = sr.ReadToEnd();
            //}

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(Endpoint);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Headers.Add("Ocp-Apim-Subscription-Key:" + Apikey);
            httpWebRequest.Headers.Add("x-Accept-Currency:GBP");
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = new JavaScriptSerializer().Serialize(new
                {
                    req.Location,
                    LocationType = req.Locationtype,
                    CheckIn = req.Checkin,
                    CheckOut = req.Checkout,
                    req.Adults,
                    req.Children,
                    req.AgeOfChildren,
                    req.MinimumStarRating,
                    req.HotelDisplayOrder,
                    req.MaxResults
                });

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                response = streamReader.ReadToEnd();
            }
            //using (StreamReader sr = new StreamReader(VirtualPathProvider.OpenFile("/example_search_response.js")))
            //{
            //    response = sr.ReadToEnd();
            //}
            

            //Parse the response into a dynamic variable. We use dynamic here because
            //we cannot be certain what fields are present on the JSON Array until runtime
            dynamic jsonResponse = JToken.Parse(response);

            //Create a list of Hotels from the parsed JSON Array
            List<Hotel> myHotelsList = CreateHotelResultsList((JArray)jsonResponse.Hotels);

            //Now insert the response Items into the response object and return
            CapiSearchResponse myResponse = new CapiSearchResponse((string)jsonResponse.SessionId, myHotelsList);

            return myResponse;
        }

        /// <summary>
        /// This private function will eventually call creates a List of Hotel Objects
        /// from a JSON Array which is obtained via the CAPI feed.
        /// </summary>
        /// <param name="hotels">A JArray (JSON Array) of Hotels which is obtained from the CAPI feed</param>
        /// <returns>A List of Hotel Objects</returns>
        private static List<Hotel> CreateHotelResultsList(JArray hotels)
        {
            //Create an empty list of Hotel objects
            List<Hotel> myHotels = new List<Hotel>();

            foreach (dynamic hotel in hotels)
            {
                //Create a new Hotel Object and set its properties from the current iteration of the JArray.
                Hotel myHotel = new Hotel
                {
                    HotelId = hotel.HotelId,
                    Name = hotel.Name
                };

                List<HotelDescription> myDescriptions = new List<HotelDescription>();
                if (hotel.HotelDescriptions != null && hotel.HotelDescriptions.Count != 0)
                {
                    foreach (dynamic description in hotel.HotelDescriptions)
                    {
                        myDescriptions.Add(new HotelDescription((string)description.Name, (string)description.Description));
                    }
                }
                else
                {
                    myDescriptions.Add(new HotelDescription("Description", ""));
                }
                
                myHotel.Descriptions = myDescriptions;

                myHotel.Address = new HotelAddress((string)hotel.Address.Address1,
                                                   (string)hotel.Address.Address2,
                                                   (string)hotel.Address.City,
                                                   (string)hotel.Address.State,
                                                   (string)hotel.Address.Country,
                                                   (string)hotel.Address.GeoCode,
                                                   (string)hotel.Address.Name,
                                                   (string)hotel.Address.PostalCode);

                myHotel.Distance = new HotelDistance((double)hotel.Distance.Amount,
                                                     (string)hotel.Distance.From,
                                                     (string)hotel.Distance.Unit);

                myHotel.CurrencyCode = hotel.CurrencyCode;
                myHotel.LowestAvailableRate = (double) hotel.LowestAvailableRate;
                myHotel.LowestAvailableRateRoomName = hotel.LowestAvailableRateRoomName;
                myHotel.Rating = hotel.Rating;
                myHotels.Add(myHotel);

                List<HotelImage> myImages = new List<HotelImage>();
                int imgCounter = 1;
                foreach (dynamic image in hotel.Images)
                {
                    HotelImage myImage = new HotelImage((string)image.Url, (string)image.Caption, (string)image.Type, (Boolean)image.IsThumbnailImage);
                    myImages.Add(myImage);

                    //If it's the first image, make it the main one (Currently there is no distinction so we have to make our own)
                    if (imgCounter == 1)
                    {
                        myHotel.MainImage = myImage;
                    }
                    imgCounter++;
                }
                myHotel.Images = myImages;
            }

            return myHotels;
        }

        /// <summary>
        /// This function will eventually call CAPI to perform a Room Request.
        /// Currently I am reading sample JS from a .js file to mimic the response.
        /// </summary>
        /// <param name="req">The Request Object</param>
        /// <returns>A CAPIRoomResponse Object - Comprising of a List of Room Objects</returns>
        public static CapiRoomResponse PerformApiRoomRequest(CapiRoomRequest req)
        {
            string response;

            //TODO : Actual API call when access granted
            //Mock the response by reading in some sample JSON from a .js file
            //using (StreamReader sr = new StreamReader(VirtualPathProvider.OpenFile("/example_room_response.js")))
            //{
            //    response = sr.ReadToEnd();
            //}

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(RoomEndpoint);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Headers.Add("Ocp-Apim-Subscription-Key:" + Apikey);
            httpWebRequest.Headers.Add("x-Accept-Currency:GBP");
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = new JavaScriptSerializer().Serialize(new
                {
                    req.HotelId, req.SessionId
                });

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                response = streamReader.ReadToEnd();
            }

            //Parse the response into a dynamic variable. We use dynamic here because
            //we cannot be certain what fields are present on the JSON Array until runtime
            dynamic jsonResponse = JToken.Parse(response);

            //Create a list of Rooms from the parsed JSON Array
            List<Room> myRoomsList = CreateRoomResultsList((JArray)jsonResponse.Rooms);

            //Now insert the response Items into the response object and return
            CapiRoomResponse myResponse = new CapiRoomResponse(myRoomsList);

            return myResponse;
        }

        /// <summary>
        /// This private function will eventually call creates a List of Room Objects
        /// from a JSON Array which is obtained via the CAPI feed.
        /// </summary>
        /// <param name="rooms">A JArray (JSON Array) of Rooms which is obtained from the CAPI feed</param>
        /// <returns>A List of Room Objects</returns>
        private static List<Room> CreateRoomResultsList(JArray rooms)
        {
            List<Room> myRooms = new List<Room>();

            foreach (dynamic room in rooms)
            {
                Room myRoom = new Room((int)room.HotelId, (string)room.Description, (int)room.GuestCount);

                RoomPricing myRoomPricing = new RoomPricing((string)room.Pricing.CurrencyCode, (double)room.Pricing.Tax, (double)room.Pricing.Fee, (double)room.Pricing.Total, 0.0);
                myRoom.Pricing = myRoomPricing;

                myRooms.Add(myRoom);
            }

            return myRooms;
        }
    }
}