

CREATE PROCEDURE [dbo].[LB_BB_checkDbMultiPropertyCache]
	
	-- Input parameters 
	@pi_propertyidlist varchar(max) = 1,
	@pi_availabilityProviderID int = 1,
	
	--Return parameters
	@po_returnstatus INT OUTPUT,
	@po_returnmsg VARCHAR(200) OUTPUT

	
AS
set nocount on;
BEGIN

	--Assume transaction will fail, set the return variable to -1.
	SET @po_returnstatus = -1;
	SET @po_returnmsg = null;

	
	
	
	BEGIN TRY
		DECLARE @providerlistIDs TABLE (providerlistID INT)	 
		DECLARE @providerlistID VARCHAR(10), @pidPos INT
		DECLARE @providerlistlist VARCHAR(MAX)
		DECLARE @myproviderlistID INT = 0
		
		----SET @providerlistlist = LTRIM(RTRIM(@pi_propertyidlist))+ ','
		----SET @pidPos = CHARINDEX(',', @providerlistlist, 1)

		----IF REPLACE(@providerlistlist, ',', '') <> ''
		----BEGIN
		----	WHILE @pidPos > 0
  ----          BEGIN
  ----              SET @providerlistID = LTRIM(RTRIM(LEFT(@providerlistlist, @pidPos - 1)))
  ----              IF @providerlistID <> ''
  ----              BEGIN
		----			INSERT INTO @providerlistIDs (providerlistID) VALUES (CAST(@providerlistID AS INT)) --Use Appropriate conversion

		----			IF NOT @myproviderlistID > 0
		----			BEGIN
		----				SET @myproviderlistID = @providerlistID
		----			END
  ----              END

  ----              SET @providerlistlist = RIGHT(@providerlistlist, LEN(@providerlistlist) - @pidPos)
  ----              SET @pidPos = CHARINDEX(',', @providerlistlist, 1)
                
  ----          END
		----END
		DECLARE @table table(ProviderID varchar(max))
		--declare @pi_propertyidlist varchar(max)='1365398,1367695,1360046,1364555,1369582,1358730,1722771,1363780,1363188,1364089,1368176,1363225,1363163,1360319,1368178,1440517,1366414,1363215,1368281,1357608,1359788,1362181,1658906,1658905,1364315'
		INSERT INTO @table select * from [SplitString](@pi_propertyidlist,',')
		--select * from @table
		Select BBPD._ProviderID, BBPD.XmlResponse, BBPR.[lbProviderID] from LB_BB_providerDetails BBPD 
		inner join LB_BB_provider BBPR on BBPD._ProviderID = BBPR.availabilityProviderRecordID
		where BBPD.availabilityProviderID = 1
		and BBPD._ProviderID IN (SELECT * FROM @table)
		and BBPR.availabilityProviderRecordID IN (SELECT * FROM @table)
		and ExpiryTime > GETDATE()
    END TRY	
	BEGIN CATCH	  
		SET @po_returnstatus = error_number();
		SET @po_returnmsg = error_message();
		RETURN;
	END CATCH
	SET @po_returnstatus = 0;
	SET @po_returnmsg = 'Completed successfully.';	
END








