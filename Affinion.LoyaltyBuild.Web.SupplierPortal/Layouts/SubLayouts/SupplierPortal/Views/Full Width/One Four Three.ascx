﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="One Four Three.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Views.Full_Width.One_Four_Three" %>

<div class="row main-block topmargin">
    <div class="span12">
        <sc:Placeholder id="fullrow1" runat="server" key="full-row1" />
        <div class="bannershadow shadow"></div>
    </div>
</div>

<div class="row main-block">
    <div class="span12">
        <div class="row show-grid hero-list features-list">
            <div class="span3 clear-both">
                <sc:Placeholder id="fullrow2a" runat="server" key="full-row2a" />
            </div>
            <div class="span3">
                <sc:Placeholder id="fullrow2b" runat="server" key="full-row2b" />
            </div>
            <div class="span3">
                <sc:Placeholder id="fullrow2c" runat="server" key="full-row2c" />
            </div>
            <div class="span3">
                <sc:Placeholder id="fullrow2d" runat="server" key="full-row2d" />
            </div>
        </div>
    </div>
</div>

<div class="row show-grid features-block mini-blocks">
    <div class="span4 block1">
        <div class="mini-wrapper">
            <sc:Placeholder id="fullrow3a" runat="server" key="full-row3a" />
        </div>
    </div>
    <div class="span4 block2">
        <div class="mini-wrapper">
            <sc:Placeholder id="fullrow3b" runat="server" key="full-row3b" />
        </div>
    </div>
    <div class="span4 block3">
        <div class="mini-wrapper">
            <sc:Placeholder id="fullrow3c" runat="server" key="full-row3c" />
        </div>
    </div>
</div>
