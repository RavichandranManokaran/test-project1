﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Model.Pricing
{
    /// <summary>
    /// Price class
    /// </summary>
    public class Price
    {
        /// <summary>
        /// Price band id
        /// </summary>
        public string PriceBand { get; set; }

        /// <summary>
        /// Price details
        /// </summary>
        public decimal PriceAmount { get; set; }
        
        /// <summary>
        /// Currency id
        /// </summary>
        public int CurrencyId { get; set; }

        /// <summary>
        /// Commission Details
        /// </summary>
        public Commision Commision { get; set; }
    }

}
