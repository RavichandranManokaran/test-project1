﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalCustomerDetails.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortalCustomerDetails" %>

<div>
    <div class="col-md-12 martop15">
    <div id="CustomerValidationSection" runat="server" visible="true">
         <div class="accordian-outer">
             <div class="panel-group" id="accordion">
                 <div class="panel panel-default">
                     <div class="panel-heading bg-primary">
                        <a id="buttoncustomervalidationdiv" data-toggle="collapse" data-parent="#accordion" href="#maincontentofflineportal_1_customervalidationdiv" class="show-hide-list">
                            <div class="star-rank pull-left padding-left-small padding-top-small">
						        <span class="panel-title">
						            <span>ATG Validation</span>
							    </span>
                            </div>
                        </a>
					</div>
                     <div id="customervalidationdiv" class="panel-collapse collapse in" runat="server">
						<div class="panel-body">
                            <sc:placeholder runat="server" id="partnervalidationofflineportal" key="partnervalidationofflineportal" />
                        </div>
                     </div>
                  </div>
             </div>
        </div>
    </div>
   </div>
    <div id="CustomerPersonalDetails" clientidmode="Static" runat="server" visible="false"  class="col-md-12">
        <asp:Panel ID="pnlInvalidProduct" runat="server" Visible="false">
            <asp:Label ID="invalidProductErrorMessage" runat="server" ForeColor="Red">One or more product in your basket is not available anymore. Please review your basket.</asp:Label>
        </asp:Panel>
        <div class="booking-detail-info" runat="server" id="customerSearchWrapper">
            <div class="accordian-outer">
            <div class="panel-group" id="accordion2" style="margin-bottom: 0">
            <div class="content">
                <%--Add Customer--%>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading bg-primary">
                                 <a id="customaccrodintoogle" data-toggle="collapse" data-parent="#accordion2" href="#maincontentofflineportal_1_customeraddition" class="show-hide-list">
                                    <div class="star-rank pull-left padding-left-small padding-top-small">
                                         <span class="panel-title">
                                            <span><sc:text field="ManageTitle" id="ManageTitle" runat="server" /></span>
                                        </span>
                                    </div>
                                </a>
                            </div>
                            <div id="customeraddition" class="panel-collapse collapse in" runat="server">
                            <div class="panel-body alert-info">
                                <div class="col-sm-12 no-padding-m">
                                    <div class="content_bg_inner_box alert-info">
                                        <div class="col-sm-6 no-padding-m">
                                            <div class="row margin-row">
                                                <div class="col-sm-5">
                                                    <span class="bask-form-titl font-size-12">
                                                        <sc:text field="Client" id="Client" runat="server" />
                                                    </span>
                                                    <span class="accent58-f">*</span>
                                                    <asp:RequiredFieldValidator CssClass="accent58-f" ID="RequiredFieldValidator8" runat="server" ValidationGroup="PartnerValidation" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="ClientDropDown"></asp:RequiredFieldValidator>
                                                </div>
                                                <div class="col-sm-7">
                                                    <asp:DropDownList ID="ClientDropDown" runat="server" CssClass="form-control font-size-12" AutoPostBack="True" OnSelectedIndexChanged="ClientDropDown_SelectedIndexChanged"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="row margin-row">
                                                <div class="col-sm-5">
                                                    <asp:HiddenField ID="CustomerId" runat="server" Value="0" />
                                                    <span class="bask-form-titl font-size-12">
                                                        <sc:text field="CustomerTitle" id="CustomerTitle" runat="server" />
                                                    </span>
                                                </div>
                                                <div class="col-sm-7">
                                                    <asp:DropDownList ID="TitleDropDown" runat="server" CssClass="form-control font-size-12"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="row margin-row">
                                                <div class="col-sm-5">
                                                    <span class="bask-form-titl font-size-12">
                                                        <sc:text field="FirstName" id="OfflineFirstName" runat="server" />
                                                    </span>
                                                     <span class="accent58-f">*</span>
                                                    <asp:RequiredFieldValidator CssClass="accent58-f" ID="FirstNameRequiredFieldValidator" runat="server" ValidationGroup="PartnerValidation" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="FirstName"></asp:RequiredFieldValidator>
                                                </div>
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="FirstName" class="form-control font-size-12" placeholder="First Name" runat="server"> </asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="row margin-row">
                                                <div class="col-sm-5">
                                                    <span class="bask-form-titl font-size-12">
                                                        <sc:text field="LastName" id="OfflineLastName" runat="server" />
                                                    </span>
                                                    <span class="accent58-f">*</span>
                                                    <asp:RequiredFieldValidator CssClass="accent58-f" ID="LastNameRequiredFieldValidatorEdit" runat="server" ValidationGroup="PartnerValidation" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="LastName"></asp:RequiredFieldValidator>
                                                </div>
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="LastName" class="form-control font-size-12" placeholder="Last Name" runat="server"> </asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="row margin-row">
                                                <div class="col-sm-5">
                                                    <span class="bask-form-titl font-size-12">
                                                        <sc:text field="AddressLine1" id="OfflineAddressLine1" runat="server" />
                                                    </span>
                                                    <span class="accent58-f">*</span>
                                                    <asp:RequiredFieldValidator CssClass="accent58-f" ID="AddressLine1RequiredFieldValidator3" ValidationGroup="PartnerValidation" runat="server" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="AddressLine1"></asp:RequiredFieldValidator>
                                                </div>
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="AddressLine1" ValidationGroup="PartnerValidation" class="form-control font-size-12" placeholder="Address Line 1" runat="server"> </asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="row margin-row">
                                                <div class="col-sm-5">
                                                    <span class="bask-form-titl font-size-12">
                                                        <sc:text field="AddressLine2" id="OfflineAddressLine2" runat="server" />
                                                    </span>
                                                </div>
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="AddressLine2" class="form-control font-size-12" placeholder="Address Line 2" runat="server"> </asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="row margin-row">
                                                <div class="col-sm-5">
                                                    <span class="bask-form-titl font-size-12">
                                                        <sc:text field="AddressLine3" id="OfflineAddressLine3" runat="server" />
                                                    </span>
                                                </div>
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="AddressLine3" class="form-control font-size-12" placeholder="Address Line 3" runat="server"> </asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 no-padding-m">
                                            <div class="row margin-row">
                                                <div class="col-sm-5">
                                                    <span class="bask-form-titl font-size-12">
                                                        <sc:text field="Country" id="Country" runat="server" />
                                                    </span>
                                                    <span class="accent58-f">*</span>
                                                    <asp:RequiredFieldValidator CssClass="accent58-f" initialvalue="0" ID="CountryRequiredFieldValidator" runat="server" ValidationGroup="PartnerValidation" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="CountryDropDown"></asp:RequiredFieldValidator>
                                                </div>
                                                <div class="col-sm-7">
                                                    <asp:DropDownList ID="CountryDropDown" ClientIDMode="Static" ValidationGroup="PartnerValidation" runat="server" CssClass="form-control font-size-12"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="row margin-row">
                                                <div class="col-sm-5">
                                                    <span class="bask-form-titl font-size-12">
                                                        <sc:text field="Location" id="Location" runat="server" />
                                                        <span class="accent58-f">*
                                                            <asp:RequiredFieldValidator CssClass="accent58-f" initialvalue="0" ID="LocationRequiredFieldValidator" ValidationGroup="PartnerValidation" runat="server" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="LocationDropDown"></asp:RequiredFieldValidator>
                                                        </span>
                                                    </span>
                                                </div>
                                                <div class="col-sm-7">
                                                    <asp:DropDownList ID="LocationDropDown" ClientIDMode="Static" ValidationGroup="PartnerValidation" runat="server" CssClass="form-control font-size-12"></asp:DropDownList>
                                                    <asp:HiddenField ID="hdfLocation" ClientIDMode="Static" runat="server" />
                                                </div>
                                            </div>
                                            <div class="row margin-row">
                                                <div class="col-sm-5">
                                                    <span class="bask-form-titl font-size-12">
                                                        <sc:text field="PrimaryPhone" id="OfflinePrimaryPhone" runat="server" />
                                                    </span>
                                                    <span class="accent58-f">*</span>
                                                    <asp:RequiredFieldValidator CssClass="accent58-f" ID="PrimaryPhoneRequiredFieldValidator" runat="server" ValidationGroup="PartnerValidation" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="PrimaryPhone"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator CssClass="accent58-f" ID="RegularExpressionValidatorPhone" runat="server" ValidationGroup="PartnerValidation" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="PrimaryPhone"></asp:RegularExpressionValidator>
                                                </div>
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="PrimaryPhone" ValidationGroup="PartnerValidation" class="form-control font-size-12" placeholder="Phone" runat="server"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="row margin-row">
                                                <div class="col-sm-5">
                                                    <span class="bask-form-titl font-size-12">
                                                        <%--Secondary Phone:--%>
                                                        <sc:text field="SecondaryPhone" id="OfflineSecondaryPhone" runat="server" />
                                                    </span>
                                                    <span class="accent58-f display-block-val"></span>
                                                    <asp:RegularExpressionValidator CssClass="accent58-f" ID="RegularExpressionValidatorSecondaryPhone" runat="server" ValidationGroup="PartnerValidation" ErrorMessage="Invalid Entry" ControlToValidate="SecondaryPhone"></asp:RegularExpressionValidator>
                                                </div>
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="SecondaryPhone" class="form-control font-size-12" placeholder="Phone" runat="server"> </asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="row margin-row">
                                                <div class="col-sm-5">
                                                    <span class="bask-form-titl font-size-12">
                                                        <sc:text field="Store" id="Store" runat="server" />
                                                    </span>
                                                     <span class="accent58-f">*</span>
                                                   <asp:RequiredFieldValidator CssClass="accent58-f" InitialValue="0" ID="RequiredFieldValidator1" runat="server" ValidationGroup="PartnerValidation" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="StoreDropDown"></asp:RequiredFieldValidator>
                                                </div>
                                                <div class="col-sm-7">
                                                    <asp:DropDownList ID="StoreDropDown" ValidationGroup="PartnerValidation" runat="server" CssClass="form-control font-size-12" Enabled="False"></asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="row margin-row">
                                                <div class="col-sm-5">
                                                    <span class="bask-form-titl font-size-12">
                                                        <%--Email:--%>
                                                        <sc:text field="Email" id="OfflineEmail" runat="server" />
                                                    </span>
                                                    <span class="accent58-f">*</span>
                                                    <asp:RequiredFieldValidator CssClass="accent58-f" ID="EmailRequiredFieldValidator" runat="server" ValidationGroup="PartnerValidation" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="Email"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator CssClass="accent58-f" ID="RegularExpressionValidatorEmail" runat="server" ValidationGroup="PartnerValidation" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="Email"></asp:RegularExpressionValidator>
                                                </div>
                                                <div class="col-sm-7">
                                                    <asp:TextBox ID="Email" class="form-control font-size-12" placeholder="Email" runat="server" ValidationGroup="PartnerValidation" OnTextChanged="Email_TextChanged" AutoPostBack="True"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="row margin-row">
                                                <div class="col-sm-5">
                                                    <span class="bask-form-titl font-size-12">
                                                        <%--Language:--%>
                                                        <sc:text field="Language" id="Language" runat="server" />
                                                    </span>
                                                </div>
                                                <div class="col-sm-7">
                                                    <asp:DropDownList ID="LanguageDropDown" ValidationGroup="PartnerValidation" runat="server" CssClass="form-control font-size-12"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 no-padding-m">
                                            <div class="row margin-row">
                                                <div class="col-sm-5">
                                                    <span class="bask-form-titl font-size-12">
                                                        <%-- Status:--%>
                                                        <sc:text field="Active" id="Active" runat="server" />
                                                    </span>
                                                </div>
                                                <div class="col-sm-7">
                                                    <asp:CheckBox ID="ActiveCheckBox" ValidationGroup="PartnerValidation" runat="server" Checked="true" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 no-padding-m">
                                            <div class="row margin-row">
                                                <div class="col-sm-5">
                                                    <span class="bask-form-titl font-size-12">
                                                        <%-- Subscribe--%>
                                                        <sc:text field="Subscribe" id="OfflineSubscribe" runat="server" />
                                                    </span>
                                                </div>
                                                <div class="col-sm-7">
                                                    <asp:CheckBox ID="SubscribeCheckBox" ValidationGroup="PartnerValidation" runat="server" />
                                                </div>
                                            </div>
                                            <div class="row margin-row">
                                                <div class="col-sm-3">
                                                    <asp:Button ID="EditAndSave" runat="server" Visible="false" Enabled="false" Text="" ValidationGroup="PartnerValidation" class="btn btn-default add-break pull-right width-full font-size-12" OnClick="EditAndSave_Click" />
                                                </div>
                                                <div class="col-sm-3">
                                                    <asp:Button ID="AddAndSave" runat="server" Visible="false" Text="" ValidationGroup="PartnerValidation" class="btn btn-default add-break pull-right width-full font-size-12" OnClick="AddAndSave_Click" />
                                                </div>
                                                <div class="col-sm-3">
                                                    <asp:Button ID="Search" ValidationGroup="PartnerValidation" Visible="false" runat="server" Text="" class="btn btn-default add-break pull-right width-full search-btn" OnClick="Search_Click" />
                                                </div>
                                            </div>
                                            <div class="row margin-row"">
                                                <div class="col-sm-3">
                                                    <asp:Button ID="ContinueButton" runat="server" Text="Continue" class="btn btn-default add-break pull-right width-full ContinueButton" ValidationGroup="PartnerValidation" OnClick="ContinueButton_Click"/> 
                                                    <asp:HiddenField ID="hdfFlag" runat="server" ClientIDMode="Static" />                                                  
                                                </div>
                                                <div class="col-sm-3">
                                                    <asp:Button ID="Reset" runat="server" Text="" class="btn btn-default add-break pull-right width-full font-size-12" OnClick="Reset_Click" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%--Messages--%>
                <div id="errormessage" class="alert alert-danger msg-customer" runat="server" visible="false" align="center">
                    <b><asp:Literal ID="ErrorMessageLiteral" runat="server" Text="No Result Found" /></b>
                </div>
                <div class="alert alert-success msg-customer" runat="server" id="successmessage" visible="false" align="center">
                    <b><asp:Literal ID="SuccessLiteral" runat="server" Text="New Customer Added Successfully" /></b>
                </div>
                <%-- End Messages--%>
                <%-- Result Grid--%>
                <div runat="server" id="ResultDiv" visible="false">
                    <div id="data-grid" class="table-responsive">
                        <asp:GridView ID="ResultGridView" runat="server" OnSelectedIndexChanged="ResultGridView_SelectedIndexChanged" CssClass="table tbl-calendar" OnRowEditing="ResultGridView_RowEditing">
                            <Columns>
                               
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                 <asp:HiddenField ID="hdfSubcscribe" ClientIDMode="Static" runat="server" />
                <%--Subscription--%>
                <div class="row" id="divSubscription" runat="server" visible="false">
                    <div class="col-sm-12">
                        <div class="panel-group" id="accordionSubscription">
                            <div class="panel panel-default">
                                <div class="panel-heading bg-primary">
                                    <h4 class="panel-title">
                                        <a id="collapseCustomerSubscribeAtag" data-toggle="collapse" data-parent="#accordionSubscription" href="#collapseCustomerSubscribe" class="show-hide-list">
                                            <sc:text field="SubscribeTitle" id="SubscribeTitle" runat="server" />
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseCustomerSubscribe" class="panel-collapse collapse">
                                    <div class="panel-body alert-info">
                                        <div class="col-sm-12 no-padding-m">
                                            <div class="content_bg_inner_box alert-info">
                                                <div class="col-sm-6 no-padding-m">

                                                    <div class="row margin-row">
                                                        <div class="col-sm-5">
                                                            <span class="bask-form-titl font-size-12">

                                                                <sc:text field="Client" id="ClientSubscribe" runat="server" />
                                                                <span class="accent58-f">*
                                                    <asp:RequiredFieldValidator CssClass="accent58-f" ID="RequiredFieldValidatorClientSubscribe" runat="server" ValidationGroup="Subscribe" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="DropDownClientListSubscribe"></asp:RequiredFieldValidator>
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-sm-7">
                                                            <asp:DropDownList ID="DropDownClientListSubscribe" runat="server" CssClass="form-control font-size-12"></asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 no-padding-m">
                                                    <div class="row margin-row">
                                                        <div class="col-sm-5">
                                                            <span class="bask-form-titl font-size-12">

                                                                <sc:text field="Email" id="EmailSubscription" runat="server" />
                                                                <span class="accent58-f">*
                                                                <asp:RequiredFieldValidator CssClass="accent58-f" ID="RequiredFieldValidator10" runat="server" ValidationGroup="Subscribe" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="EmailSubscribe"></asp:RequiredFieldValidator>
                                                                    <asp:RegularExpressionValidator CssClass="accent58-f" ID="RegularExpressionValidatorEmailSubscription" runat="server" ValidationGroup="Subscribe" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="EmailSubscribe"></asp:RegularExpressionValidator>
                                                                    <%--  <asp:RequiredFieldValidator CssClass="accent58-f" ID="RequiredFieldValidator12" runat="server" ValidationGroup="Unsubscribe" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="EmailSubscribe"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator CssClass="accent58-f" ID="RegularExpressionValidatorEmailUnsubscription" runat="server" ValidationGroup="Unsubscribe" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="EmailSubscribe"></asp:RegularExpressionValidator>--%>
                                                                </span>
                                                                <span class="accent58-f display-block-val">
                                                                    <asp:Literal ID="SubscribeMessage" runat="server" Text="" />
                                                                </span>
                                                            </span>
                                                        </div>
                                                        <div class="col-sm-7">

                                                            <asp:TextBox ID="EmailSubscribe" class="form-control font-size-12" placeholder="Email" runat="server" ValidationGroup="Subscribe"></asp:TextBox>

                                                        </div>
                                                    </div>

                                                    <div class="row margin-row">
                                                        <div class="col-sm-4">
                                                            <asp:Button ID="ButtonSubscribe" runat="server" Text="" ValidationGroup="Subscribe" class="btn btn-default add-break pull-right width-full" OnClick="Subscribe_Click" />
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <asp:Button ID="UnSubscribe" runat="server" Text="" ValidationGroup="Subscribe" class="btn btn-default add-break pull-right width-full" OnClick="UnSubscribe_Click" />
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <asp:Button ID="SubscribeReset" runat="server" Text="" class="btn btn-default add-break pull-right width-full" OnClick="SubscribeReset_Click" />
                                                        </div>
                                                        
                                                    </div>
                                                    
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
               </div>
                </div>     
        </div>
    </div>


   <%-- <div class="col-sm-7 no-padding-m" id="PaymentSection" runat="server" visible="true">
                    <div class="col-xs-12 no-padding">
                        <span id="spnPaymentError" class="required"></span>
                    </div>
                    <div class="col-xs-12 no-padding" id="divPaymentPortions">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">Payment Portion</h4>
                            </div>
                            <div class="panel-body  alert-info">
                                <div class="paymentPortion">
                                    <label class="checkbox-inline font-size-12">
                                        <asp:CheckBox ID="fullCheckbox" runat="server" value="Full" Text="Full" />
                                    </label>
                                </div>
                                <div class="paymentPortion">
                                    <label class="checkbox-inline font-size-12">
                                        <asp:CheckBox ID="depositCheckBox" runat="server" value="Partial" Text="Deposit" />
                                    </label>
                                </div>
                                <div class="paymentPortion">
                                    <label class="checkbox-inline font-size-12">
                                        <asp:CheckBox ID="partialCheckBox" runat="server" value="Deposit" Text="Partial" />
                                        <asp:Literal ID="LitInst" runat="server" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 no-padding" id="divPaymentTypes">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">Payment Method</h4>
                            </div>
                            <div class="panel-body  alert-info">
                                <div class="paymentMethod">
                                    <label class="checkbox-inline font-size-12">
                                        <asp:CheckBox ID="creditDebitCheckBox" runat="server" value="CreditCard" Text="Credit/Laser/Debit Card" />
                                    </label>
                                </div>
                                <div class="paymentMethod">
                                    <label class="checkbox-inline font-size-12">
                                        <asp:CheckBox ID="chequeCheckBox" runat="server" value="Cheque" Text="Cheque" />
                                    </label>
                                </div>
                                <div class="paymentMethod">
                                    <label class="checkbox-inline font-size-12">
                                        <asp:CheckBox ID="cashCheckBox" runat="server" value="Cash" Text="Cash" />
                                    </label>
                                </div>
                                <div class="paymentMethod">
                                    <label class="checkbox-inline font-size-12">
                                        <asp:CheckBox ID="poCheckBox" runat="server" value="PostOffice" Text="Post Office / Bank Transfer" />
                                    </label>
                                </div>
                                <div class="paymentMethod">
                                    <label class="checkbox-inline font-size-12">
                                        <asp:CheckBox ID="voucherCheckBox" runat="server" value="Voucher" Text="Voucher & Other" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 no-padding">
                        <asp:Button ValidationGroup="PaymentValidation" runat="server" ClientIDMode="Static" type="submit" OnClick="ProceedToPayment" ID="paymentButton" class="btn btn-default add-break pull-right margin-row font-size-12 marR5" Text="Continue To Payment"></asp:Button>
                    </div>
                </div>--%>

    <asp:HiddenField ID="HiddenField1" runat="server" />
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {
        //Timer for error messages 
        jQuery('.msg-customer').delay(10000).fadeOut();

        //LoadRegion();
        jQuery("#CountryDropDown").change(function () {
            LoadRegion();
        });

        function LoadRegion() {
            var countryVal = jQuery("#CountryDropDown option:selected").html();
            $.get("/handlers/cs/ServerSideEventHandler.ashx?action=LoadLocationByCountry&value=" + countryVal, function (data) {
                if (data) {
                    var jsonData = JSON.parse(data); //This converts the string to json
                    $('#LocationDropDown').find('option').not(':first').remove();
                    for (var i = 0; i < jsonData.length; i++) //The json object has lenght
                    {
                        var object = jsonData[i]; //You are in the current object
                        var objItems = object.split(",");
                        $('#LocationDropDown').append('<option value=' + objItems[0] + '>' + objItems[1] + '</option>'); //now you access the property.

                    }
                }
            });
        }

        //if ($("#hdfLocation").val() != '')
        //{
        //    $('#LocationDropDown option[value="' + $("#hdfLocation").val() + '"]').attr("selected", "selected");
        //}

        jQuery("#LocationDropDown").change(function () {
            $("#hdfLocation").val(jQuery("#LocationDropDown :selected").text());
        });

        $('#buttoncustomervalidationdiv').click(function () {
            if ($("#maincontentofflineportal_1_customervalidationdiv").hasClass("in"))
                $("#maincontentofflineportal_1_customervalidationdiv").collapse('hide');
            else {
                $("#maincontentofflineportal_1_customervalidationdiv").collapse('show');
                var panel = $(this);

                $('html, body').animate({
                    scrollTop: panel.offset().top
                }, 500);
            }
        });

        $('#customaccrodintoogle').click(function () {
            if ($("#maincontentofflineportal_1_customeraddition").hasClass("in"))
                $("#maincontentofflineportal_1_customeraddition").collapse('hide');
            else
                $("#maincontentofflineportal_1_customeraddition").collapse('show');
        });
      
    });
    //Change Subscribe CSS Class
    function ChangeSubscribeCss() {
        jQuery("#divSubscribe").addClass("col-sm-12");
    }
    function FocusSearchResultGrid() {
        document.getElementById('data-grid').scrollIntoView();
    }
    //Edit form 
    function ExpanEditForm() {
        jQuery("#collapseCustomerEdit").addClass("panel-collapse collapse in");
        jQuery("#collapseCustomerEditAtag").removeClass();
        jQuery("#collapseCustomerEditAtag").addClass("show-hide-list");
    }
    //Subscribe
    function ExpanSubscribForm() {
        jQuery("#collapseCustomerSubscribe").addClass("panel-collapse collapse in");
        jQuery("#collapseCustomerSubscribeAtag").removeClass();
        jQuery("#collapseCustomerSubscribeAtag").addClass("show-hide-list");
    }

    if ($("#hdfFlag").val() == "1")
        {
        $("#maincontentofflineportal_1_customeraddition").collapse('hide');
        $("#maincontentofflineportal_1_customervalidationdiv").collapse('hide');
        }
</script>