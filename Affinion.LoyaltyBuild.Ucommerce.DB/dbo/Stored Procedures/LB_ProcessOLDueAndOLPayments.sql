﻿CREATE PROCEDURE [dbo].[LB_ProcessOLDueAndOLPayments]
(      
       @OrderId INT=NULL,
       @IsPartialPayment BIT = 0  
)
AS 
BEGIN

	DECLARE @DueTable TABLE(
		[OrderLineID] INT,
		[DueTypeId] UNIQUEIDENTIFIER,
		[BookingId] UNIQUEIDENTIFIER,
		[TotalAmount] MONEY,
		[CommissionAmount] MONEY,
		[ProviderAmount] MONEY,
		[ProcessingFeeAmount] MONEY,
		[VatAmount] MONEY)
	DECLARE @PaymentTable TABLE(
		[OrderLineID] INT,
		[BookingId] UNIQUEIDENTIFIER,
		[TotalAmount] MONEY,
		[CommissionAmount] MONEY,
		[ProviderAmount] MONEY,
		[ProcessingFeeAmount] MONEY)

	DECLARE 
		@CurrencyId INT,
		@PaymentMethodId INT,
		@CreatedBy VARCHAR(50) = '',
		@DueTypeId UNIQUEIDENTIFIER,
		@PaymentReferenceId VARCHAR(50),
		@OrderPaymentId INT

	--GET CURRENCY AND PAYMENT METHOD
	SELECT TOP 1
		@CurrencyId = PO.CurrencyId,
		@PaymentMethodId = P.PaymentMethodId,
		@OrderPaymentId = P.PaymentId,
		@PaymentReferenceId = CAST(PO.OrderGuid AS VARCHAR(50))
	FROM
		uCommerce_PurchaseOrder PO
		INNER JOIN uCommerce_Payment P ON PO.OrderId = P.OrderId
	WHERE
		PO.OrderId = @OrderId

	--GET CREATED BY
	SELECT TOP 1
		@CreatedBy = ISNULL(BI.CreatedBy,'')
	FROM
		LB_BookingInfo BI
		INNER JOIN uCommerce_OrderLine OL ON Bi.OrderLineId = OL.OrderLineId
	WHERE
		OL.OrderId = @OrderId

	--START INSERT COMMISSION
	SELECT @DueTypeId = DueTypeId FROM LB_DueTypes WHERE Code = 'COMMISSION'
	--INSERT COMMISSION DUE
	INSERT INTO @DueTable				
	SELECT
		OL.OrderLineId,
		@DueTypeId,
		BI.BookingId,
		CAST(ISNULL(OP.Value,'0') AS MONEY),
		CAST(ISNULL(OP.Value,'0') AS MONEY),
		0,
		0,
		0
	FROM
		uCommerce_OrderLine	OL
		INNER JOIN LB_BookingInfo BI ON OL.OrderLineId = BI.OrderLineId
		INNER JOIN uCommerce_OrderProperty OP ON OL.OrderLineId = OP.OrderLineId
	WHERE
		OP.[Key] = '_LBCommissionFee'
		AND OL.OrderId = @OrderId
	--INSERT COMMISSION PAYMENT
	INSERT INTO @PaymentTable				
	SELECT
		OL.OrderLineId,
		BI.BookingId,
		CAST(ISNULL(OP.Value,'0') AS MONEY),
		CAST(ISNULL(OP.Value,'0') AS MONEY),
		0,
		0
	FROM
		uCommerce_OrderLine	OL
		INNER JOIN LB_BookingInfo BI ON OL.OrderLineId = BI.OrderLineId
		INNER JOIN uCommerce_OrderProperty OP ON OL.OrderLineId = OP.OrderLineId
	WHERE
		OP.[Key] = '_LBCommissionFee'
		AND OL.OrderId = @OrderId
	--END INSERT COMMISSION

	--START INSERT PROCESSINGFEE
	SELECT @DueTypeId = DueTypeId FROM LB_DueTypes WHERE Code = 'PROFEE'
	--INSERT PROCESSINGFEE DUE
	INSERT INTO @DueTable				
	SELECT
		OL.OrderLineId,
		@DueTypeId,
		BI.BookingId,
		CAST(ISNULL(OP.Value,'0') AS MONEY),
		0,
		0,
		CAST(ISNULL(OP.Value,'0') AS MONEY),
		0
	FROM
		uCommerce_OrderLine	OL
		INNER JOIN LB_BookingInfo BI ON OL.OrderLineId = BI.OrderLineId
		INNER JOIN uCommerce_OrderProperty OP ON OL.OrderLineId = OP.OrderLineId
	WHERE
		OP.[Key] = 'ProcessingFee'
		AND OL.OrderId = @OrderId
	--INSERT PROCESSINGFEE PAYMENT
	INSERT INTO @PaymentTable				
	SELECT
		OL.OrderLineId,
		BI.BookingId,
		CAST(ISNULL(OP.Value,'0') AS MONEY),
		0,
		0,
		CAST(ISNULL(OP.Value,'0') AS MONEY)
	FROM
		uCommerce_OrderLine	OL
		INNER JOIN LB_BookingInfo BI ON OL.OrderLineId = BI.OrderLineId
		INNER JOIN uCommerce_OrderProperty OP ON OL.OrderLineId = OP.OrderLineId
	WHERE
		OP.[Key] = 'ProcessingFee'
		AND OL.OrderId = @OrderId
	--END INSERT PROCESSINGFEE


	--START INSERT PROVIDERAMOUNT
	SELECT @DueTypeId = DueTypeId FROM LB_DueTypes WHERE Code = 'PROVIDERAMOUNT'
	--INSERT PROVIDERAMOUNT DUE
	INSERT INTO @DueTable				
	SELECT
		OL.OrderLineId,
		@DueTypeId,
		BI.BookingId,
		CAST(ISNULL(OP.Value,'0') AS MONEY),
		0,
		CAST(ISNULL(OP.Value,'0') AS MONEY),
		0,
		0
	FROM
		uCommerce_OrderLine	OL
		INNER JOIN LB_BookingInfo BI ON OL.OrderLineId = BI.OrderLineId
		INNER JOIN uCommerce_OrderProperty OP ON OL.OrderLineId = OP.OrderLineId
	WHERE
		OP.[Key] = '_ProviderAmount'
		AND OL.OrderId = @OrderId
		AND [dbo].[LB_FnIsPaidInFull](OL.OrderLineId) = 1
	--INSERT PROVIDERAMOUNT PAYMENT
	INSERT INTO @PaymentTable				
	SELECT
		OL.OrderLineId,
		BI.BookingId,
		CAST(ISNULL(OP.Value,'0') AS MONEY),
		0,
		CAST(ISNULL(OP.Value,'0') AS MONEY),
		0
	FROM
		uCommerce_OrderLine	OL
		INNER JOIN LB_BookingInfo BI ON OL.OrderLineId = BI.OrderLineId
		INNER JOIN uCommerce_OrderProperty OP ON OL.OrderLineId = OP.OrderLineId
	WHERE
		OP.[Key] = 'ProviderAmountPaidOnCheckOut'
		AND OL.OrderId = @OrderId
		AND [dbo].[LB_FnIsPaidInFull](OL.OrderLineId) = 1
	--END INSERT PROVIDERAMOUNT

	--Insert Dues
	INSERT INTO [dbo].[LB_OrderLineDue]
		([OrderLineID]
		,[BookingId]
		,[DueTypeId]
		,[CurrencyTypeId]
		,[PaymentMethodId]
		,[TotalAmount]
		,[CommissionAmount]
		,[ProviderAmount]
		,[ProcessingFeeAmount]
		,[VatAmount]
		,[Note]
		,[IsDeleted]
		,[CreatedBy]
		,[CreatedDate]
		,[UpdatedBy]
		,[UpdatedDate]
		,[ParentOrderLineDueId])
	SELECT
		OrderLineID,
		BookingId,
		DueTypeId,
		@CurrencyId,
		@PaymentMethodId,
		ROUND(TotalAmount,2),
		ROUND(CommissionAmount,2),
		ROUND(ProviderAmount,2),
		ROUND(ProcessingFeeAmount,2),
		ROUND(VatAmount,2),
		'',
		0,
		@CreatedBy,
		GETDATE(),
		@CreatedBy,
		GETDATE(),
		NULL
	FROM
		@DueTable
	WHERE 
		TotalAmount > 0

	--Insert payment
	INSERT INTO [dbo].[LB_OrderLinePayment]
		([OrderLineID]
		,[BookingId]
		,[CurrencyTypeId]
		,[PaymentMethodID]
		,[TotalAmount]
		,[CommissionAmount]
		,[ProviderAmount]
		,[ProcessingFeeAmount]
		,[OrderPaymentID]
		,[ReferenceID]
		,[CreatedBy]
		,[CreatedDate])
	SELECT
		OrderLineID,
		BookingId,
		@CurrencyId,
		@PaymentMethodId,
		ROUND(TotalAmount,2),
		ROUND(CommissionAmount,2),
		ROUND(ProviderAmount,2),
		ROUND(ProcessingFeeAmount,2),
		@OrderPaymentId,
		@PaymentReferenceId,
		@CreatedBy,
		GETDATE()
	FROM
		@PaymentTable
	WHERE 
		TotalAmount > 0

	--Update Status
	DECLARE @UpdateStatus NVARCHAR(MAX) = ''

	SELECT @UpdateStatus = @UpdateStatus + 'exec [LB_AddBookingStatus] @OrderLineID=' + CAST(OrderLineId AS VARCHAR(10)) + ',@Status=NULL,@CreatedBy=''' + @CreatedBy + ''';'
	FROM uCommerce_OrderLine
	WHERE OrderId = @OrderId

	EXEC sp_executesql @UpdateStatus
END
GO
