///<summary>
/// � 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SuperValuRedPrimaryLayout.aspx.cs
/// Sub-system/Module:     Web.ClientPortal(VS project name)
/// Description:           Master layout of the super valu red theme
/// </summary>
namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.PrimaryLayouts
{
    #region Using Statements
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using System.Web.UI;
    using System.Web.UI.HtmlControls; 
    #endregion

    public partial class SuperValuRedPrimaryLayout : BasePrimaryLayout
    {
        /// <summary>
        /// Gets the main form
        /// </summary>
        public override HtmlForm MainForm
        {
            get
            {
                return this.mainform;
            }
        }

        /// <summary>
        /// Page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, System.EventArgs e)
        {
        }
    }
}
