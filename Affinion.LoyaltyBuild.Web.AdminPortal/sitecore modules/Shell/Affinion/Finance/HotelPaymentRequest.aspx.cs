﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using System.Globalization;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Sitecore.Data.Items;
using Affinion.LoyaltyBuild.Common;
using Sitecore.Data;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;
using Affinion.LoyaltyBuild.AdminPortal.Extensions;
using Affinion.LoyaltyBuild.BedBanks.General;

namespace Affinion.LoyaltyBuild.AdminPortal.sitecore_modules.Shell.Affinion.Finance
{
    public partial class HotelPaymentRequest : System.Web.UI.Page
    {
        IReportsService iReportsService = new ReportsService();
        SearchCriteria objSearchCriteria = new SearchCriteria();
        protected int TotalOfRecords { get; private set; }
        Item[] providers = null;
        Item oracleConfiguration = null;
        string ProviderPath = Constants.ProviderPath;
        APOraclePayment objAPOraclePayment = null;
        APInvoice objAPInvoice = null;
        Sitecore.Data.Database sitecoreDatabase = Sitecore.Configuration.Factory.GetDatabase("master");
        private static string _sortDirection = "DESC";
        private static string _sortExpresssion = "BookingRefNo";
        BedBanksSettings bedBankData = null;

        private static List<APInvoice> _lbAPInvoice = new List<APInvoice>();

        public static List<APInvoice> LBAPInvoice
        {
            get
            {
                return _lbAPInvoice;
            }
           set
            {
                _lbAPInvoice = value;
            }

        }

        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (LBAPInvoice != null)
                    LBAPInvoice.Clear();
                BindProvider();
                BindClient();
            }
            lblMessage.Visible = false;
        }

        private void GetPaymentRequest(string searchData)
        {
            try
            {
                iReportsService = new ReportsService();
                objSearchCriteria = new SearchCriteria();

                if (!string.IsNullOrEmpty(this.DateRange.Value))
                {
                    string[] dates = this.DateRange.Value.ToString().Split('-');
                    objSearchCriteria.DateRange = InputDateFormatting(dates[0]);
                    objSearchCriteria.DateUpTo = InputDateFormatting(dates[1]);
                }
                else
                {
                    objSearchCriteria.DateRange = null;
                    objSearchCriteria.DateUpTo = InputDateFormatting(this.DateUpTo.Text);
                }


                if (!string.IsNullOrEmpty(this.Provider.SelectedValue))
                    objSearchCriteria.Provider = this.Provider.SelectedValue;

                if (!string.IsNullOrEmpty(this.Partner.SelectedValue))
                    objSearchCriteria.Partner = this.Partner.SelectedValue;

                if (!string.IsNullOrEmpty(this.SpaOnly.SelectedValue))
                    objSearchCriteria.Spa = this.SpaOnly.SelectedValue;

                if (!string.IsNullOrEmpty(this.PaymentStatus.SelectedValue))
                    objSearchCriteria.PaymentStatus = int.Parse(this.PaymentStatus.SelectedValue);
                else
                    objSearchCriteria.PaymentStatus = null;

                if (!string.IsNullOrEmpty(searchData))
                    objSearchCriteria.SearchText = searchData.Trim();
                else
                    objSearchCriteria.SearchText = null;

                 LBAPInvoice = iReportsService.GetPaymentRequest(objSearchCriteria);

                 SetRecordCount();

                 if (LBAPInvoice != null && LBAPInvoice.Count > 0)
                {
                    LBAPInvoice.ToList().ForEach(lst =>
                                                 {
                                                     if (!string.IsNullOrEmpty(lst.ProviderID))
                                                     {
                                                         providers = sitecoreDatabase.SelectItems(ProviderPath);
                                                         oracleConfiguration = providers.Where(x => Guid.Parse(x.ID.ToString()).ToString().Equals(Guid.Parse(lst.ProviderID.ToString()).ToString(), StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                                                         if (oracleConfiguration != null && oracleConfiguration.Fields["OracleID"].Value.ToString() != "")
                                                         {
                                                             lst.OracleID = oracleConfiguration.Fields["OracleID"].Value.ToString();
                                                         }
                                                     }
                                                     else
                                                     {
                                                         oracleConfiguration = sitecoreDatabase.GetItem(Sitecore.Data.ID.Parse(lst.ClientID));
                                                         if (oracleConfiguration != null)
                                                         {
                                                             bedBankData = new BedBanksSettings(oracleConfiguration);
                                                             if (bedBankData != null && bedBankData.BedBanksOracleConfigurations!=null && bedBankData.BedBanksOracleConfigurations.OracleId.ToString() != string.Empty)
                                                             {
                                                                 lst.OracleID = bedBankData.BedBanksOracleConfigurations.OracleId;
                                                             }
                                                         }
                                                     }
                                                 }
                                      );

                }
               
                this.ResultGridView.DataSource = LBAPInvoice.SortList(_sortExpresssion, _sortDirection).ToList(); 
                this.ResultGridView.DataBind();

                if (LBAPInvoice != null && LBAPInvoice.Count > 0 && ResultGridView != null && ResultGridView.HeaderRow != null)
                {
                    CheckBox chkSelectAll = (CheckBox)ResultGridView.HeaderRow.FindControl("chkSelectAll");
                    int count = LBAPInvoice.Where(lst => lst.PaymentStatus == 0 && !string.IsNullOrEmpty(lst.OracleID)).ToList().Count;
                    if (count == 0)
                    {
                        chkSelectAll.Enabled = false;
                    }
                }

            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.AdminPortal, ex, null);
                throw;
            }

        }

        private void SetRecordCount()
        {
            TotalOfRecords = 0;
            lblTotalRecords.Text = "0";
            if (LBAPInvoice != null && LBAPInvoice.Count > 0)
            {
                TotalOfRecords = LBAPInvoice.Count;
                lblTotalRecords.Text = LBAPInvoice.Count.ToString();
            }
            else
            {
                LBAPInvoice = new List<APInvoice>();
            }

        }

        private void PersistSupplierPayment(APInvoice objAPInvoice)
        {
            if (LBAPInvoice != null && LBAPInvoice.Count > 0)
            {
                LBAPInvoice.Where(lst => lst.OrderID == objAPInvoice.OrderID && lst.OrderLineID == objAPInvoice.OrderLineID).ToList().
                    ForEach(lstUpdate =>
                    {
                        lstUpdate.SuggestedAmount = objAPInvoice.SuggestedAmount;
                        lstUpdate.IsChecked = objAPInvoice.IsChecked;
                    });
            }
        }

        protected void PaymentRequest_Click(object sender, EventArgs e)
        {
            ClearValues();
            GetPaymentRequest(string.Empty);
            SearchBox.Text = string.Empty;
        }

        private void ClearValues()
        {
            hdnSelect.Value = "false";
            hdnSelectAll.Value = "false";
            LBAPInvoice.Clear();
        }

        private DateTime InputDateFormatting(string date)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(date))
                {
                    return default(DateTime);
                }
                else
                {
                    DateTime tmpDateTime;
                    try
                    {
                        tmpDateTime = DateTime.Parse(date);
                        return tmpDateTime;
                    }
                    catch
                    {
                        string[] words = date.Split('/');
                        string newstring = words[1] + "/" + words[0] + "/" + words[2];
                        DateTime dt = Convert.ToDateTime(newstring);
                        return dt;
                    }
                }
            }
            catch
            {
                return default(DateTime);
            }

        }

        protected void SavePaymentRequest_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (GridViewRow gvRow in this.ResultGridView.Rows)
                {
                    objAPInvoice = new APInvoice();
                    HiddenField hdnOrderID = (HiddenField)gvRow.FindControl("hdnOrderID");
                    HiddenField hdnOrderLineID = (HiddenField)gvRow.FindControl("hdnOrderLineID");
                    Label lblProviderID = (Label)gvRow.FindControl("lblProviderID");

                    TextBox txtSuggestedAmount = (TextBox)gvRow.FindControl("txtSuggestedAmount");
                    CheckBox chkSelect = (CheckBox)gvRow.FindControl("chkSelect");

                    if (chkSelect.Checked)
                        objAPInvoice.IsChecked = true;
                    else
                        objAPInvoice.IsChecked = false;

                    objAPInvoice.ProviderID = lblProviderID.Text;
                    objAPInvoice.OrderID = int.Parse(hdnOrderID.Value);
                    objAPInvoice.OrderLineID = int.Parse(hdnOrderLineID.Value);

                    if (txtSuggestedAmount.Text != string.Empty)
                    {
                        objAPInvoice.SuggestedAmount = decimal.Parse(txtSuggestedAmount.Text);
                        objAPInvoice.PostedAmount = decimal.Parse(txtSuggestedAmount.Text);
                    }
                    else
                    {
                        objAPInvoice.SuggestedAmount = 0;
                        objAPInvoice.PostedAmount = 0;
                    }

                    PersistSupplierPayment(objAPInvoice);

                }

                if (LBAPInvoice != null && LBAPInvoice.Count > 0)
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = string.Empty;
                    if (LBAPInvoice.Where(lst => lst.IsChecked == true).ToList().Count == 0)
                    {
                        lblMessage.Text = "Please select hotel payment request details";
                        return;
                    }
                    List<APOraclePayment> LBSupplierPayment = new List<APOraclePayment>();

                    foreach (APInvoice objAPInvoice in LBAPInvoice)
                    {
                        objAPOraclePayment = new APOraclePayment();
                        objAPOraclePayment.ProviderID = objAPInvoice.ProviderID;
                        objAPOraclePayment.OrderId = objAPInvoice.OrderID;
                        objAPOraclePayment.OrderDate = objAPInvoice.BookingDate;
                        objAPOraclePayment.CurrencyCode = objAPInvoice.Currency;
                        objAPOraclePayment.CurrencyID = objAPInvoice.CurrencyID;
                        objAPOraclePayment.EuroRate = 0;
                        objAPOraclePayment.CustomerID = objAPInvoice.CustomerID;
                        objAPOraclePayment.SuggestedAmount = objAPInvoice.SuggestedAmount;
                        objAPOraclePayment.PostedAmount = objAPInvoice.SuggestedAmount;
                        objAPOraclePayment.CreatedBy = Sitecore.Security.Accounts.User.Current.DisplayName;
                        objAPOraclePayment.UpdatedBy = Sitecore.Security.Accounts.User.Current.DisplayName;
                        objAPOraclePayment.OracleID = objAPInvoice.OracleID;
                        objAPOraclePayment.SupplierPaymentId = objAPInvoice.SupplierPaymentId;
                        objAPOraclePayment.OrderLineID = objAPInvoice.OrderLineID;
                        objAPOraclePayment.IsChecked = objAPInvoice.IsChecked;
                        objAPOraclePayment.ClientID = objAPInvoice.ClientID;
                        if (objAPInvoice.IsChecked && objAPInvoice.PaymentStatus==0)
                        LBSupplierPayment.Add(objAPOraclePayment);
                    }

                    if (LBSupplierPayment != null && LBSupplierPayment.Count > 0)
                    {
                        LBSupplierPayment.Where(lst => lst.IsChecked == true).ToList().
                        ForEach(lstUpdate =>
                        {
                            if (!string.IsNullOrEmpty(lstUpdate.ProviderID))
                            {
                                providers = sitecoreDatabase.SelectItems(ProviderPath);
                                oracleConfiguration = providers.Where(x => Guid.Parse(x.ID.ToString()).ToString().Equals(Guid.Parse(lstUpdate.ProviderID.ToString()).ToString(), StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                                if (oracleConfiguration != null)
                                {
                                    lstUpdate.NominalCode = oracleConfiguration.Fields["AP Nominal Code"].Value.ToString();
                                    if (!string.IsNullOrEmpty(oracleConfiguration.Fields["AP Campaign Accounting ID"].Value))
                                        lstUpdate.CampaignAccountingID = int.Parse(oracleConfiguration.Fields["AP Campaign Accounting ID"].Value.ToString());
                                    lstUpdate.OracleID = oracleConfiguration.Fields["OracleID"].Value.ToString();
                                    lstUpdate.SupplierAddrCode = oracleConfiguration.Fields["Supplier Address Code"].Value.ToString();
                                    if (!string.IsNullOrEmpty(oracleConfiguration.Fields["AP Line Number"].Value))
                                        lstUpdate.LineNumber = int.Parse(oracleConfiguration.Fields["AP Line Number"].Value.ToString());
                                }
                            }
                            else
                            {
                                oracleConfiguration = sitecoreDatabase.GetItem(Sitecore.Data.ID.Parse(lstUpdate.ClientID));
                                if (oracleConfiguration != null)
                                {
                                    bedBankData = new BedBanksSettings(oracleConfiguration);
                                    if (bedBankData != null && bedBankData.BedBanksOracleConfigurations!=null)
                                    {
                                        lstUpdate.NominalCode = bedBankData.BedBanksOracleConfigurations.ApNominalCode;
                                        if (!string.IsNullOrEmpty(bedBankData.BedBanksOracleConfigurations.ApCampaignAccountingId))
                                            lstUpdate.CampaignAccountingID = int.Parse(bedBankData.BedBanksOracleConfigurations.ApCampaignAccountingId);
                                        lstUpdate.OracleID = bedBankData.BedBanksOracleConfigurations.OracleId;
                                        lstUpdate.SupplierAddrCode = bedBankData.BedBanksOracleConfigurations.SupplierAddresCode;
                                        if (!string.IsNullOrEmpty(bedBankData.BedBanksOracleConfigurations.ApLineNumber))
                                            lstUpdate.LineNumber = int.Parse(bedBankData.BedBanksOracleConfigurations.ApLineNumber);
                                    }
                                }
                            }
                        });

                        List<APOraclePayment> lstAPOraclePayment = new List<APOraclePayment>();
                        lstAPOraclePayment = LBSupplierPayment.Where(lst => lst.IsChecked == true && !string.IsNullOrEmpty(lst.OracleID)).ToList();
                        iReportsService = new ReportsService();
                        ValidationResponse objValidationResponse = iReportsService.APPaymentRequest(lstAPOraclePayment, "HotelPaymentRequest");

                        if (objValidationResponse.IsSuccess)
                        {
                            this.SearchBox.Text = string.Empty;
                            ClearValues();
                            GetPaymentRequest(string.Empty);

                        }
                        lblMessage.Text = "Hotel payment request" + objValidationResponse.Message.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.AdminPortal, ex, null);
                throw;
            }

        }

        protected void ResultGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            foreach (GridViewRow row in ResultGridView.Rows)
            {
                objAPInvoice = new APInvoice();
                var chkBox = row.FindControl("chkSelect") as CheckBox;
                var txtSuggestedAmount = row.FindControl("txtSuggestedAmount") as TextBox;

                HiddenField hdnOrderID = (HiddenField)row.FindControl("hdnOrderID");
                Label lblProviderID = (Label)row.FindControl("lblProviderID");
                HiddenField hdnOrderLineID = (HiddenField)row.FindControl("hdnOrderLineID");

                if (chkBox.Checked)
                {
                    objAPInvoice.IsChecked = true;
                }
                else
                {
                    objAPInvoice.IsChecked = false;
                }

                objAPInvoice.ProviderID = lblProviderID.Text;
                objAPInvoice.OrderID = int.Parse(hdnOrderID.Value);
                objAPInvoice.OrderLineID = int.Parse(hdnOrderLineID.Value);

                if (!string.IsNullOrEmpty(txtSuggestedAmount.Text))
                {
                    objAPInvoice.SuggestedAmount = decimal.Parse(txtSuggestedAmount.Text);
                    objAPInvoice.PostedAmount = decimal.Parse(txtSuggestedAmount.Text);
                }
                else
                {
                    objAPInvoice.SuggestedAmount = 0;
                    objAPInvoice.PostedAmount = 0;
                }

                PersistSupplierPayment(objAPInvoice);
            }

            SetRecordCount();
            ResultGridView.PageIndex = e.NewPageIndex;
            this.ResultGridView.DataSource = LBAPInvoice.SortList(_sortExpresssion, _sortDirection).ToList(); 
            this.ResultGridView.DataBind();

            if (LBAPInvoice != null && LBAPInvoice.Count > 0 && ResultGridView != null && ResultGridView.HeaderRow != null)
            {
                CheckBox chkSelectAll = (CheckBox)ResultGridView.HeaderRow.FindControl("chkSelectAll");
                if (hdnSelectAll.Value =="true")
                {
                    chkSelectAll.Checked = true;
                }
                else
                {
                    chkSelectAll.Checked = false;
                }
            }
        }

        private void BindProvider()
        {
            Dictionary<string, string> providerDatasource = new Dictionary<string, string>();

            Item[] hotels = sitecoreDatabase.SelectItems(Constants.BasketPageQueryPath);
            foreach (var hotel in hotels)
            {
                if (hotel != null)
                {
                    providerDatasource.Add(Guid.Parse(hotel.ID.ToString()).ToString(), hotel.Fields["Name"].Value);
                }
            }

            this.Provider.DataSource = providerDatasource;
            this.Provider.DataTextField = "Value";
            this.Provider.DataValueField = "Key";
            this.Provider.DataBind();
            this.Provider.Items.Insert(0, new ListItem { Text = "Select a Provider", Value = "" });
        }

        private void BindClient()
        {
            var ClientITems = new ListItemCollection();
            var items = sitecoreDatabase.SelectItems(Constants.FilteredClientItems) != null ? sitecoreDatabase.SelectItems(Constants.FilteredClientItems).ToList() : null;
            if (items != null)
            {
                foreach (var item in items)
                {
                    ListItem liBox = new ListItem();
                    liBox.Text = SitecoreFieldsHelper.GetValue(item, "Name");
                    liBox.Value = Guid.Parse(item.ID.ToString()).ToString();
                    if (liBox != null)
                    {
                        ClientITems.Add(liBox);
                    }

                }

                Partner.DataSource = ClientITems;
                Partner.DataTextField = "Text";
                Partner.DataValueField = "Value";
                Partner.DataBind();
                Partner.Items.Insert(0, new ListItem { Text = "Select Partner", Value = "" });

            }

        }

        protected void ResultGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdnOrderID = (HiddenField)e.Row.FindControl("hdnOrderID");
                HiddenField hdnOrderLineID = (HiddenField)e.Row.FindControl("hdnOrderLineID");
                Label lblProviderID = (Label)e.Row.FindControl("lblProviderID");
                Label lblOracleID = (Label)e.Row.FindControl("lblOracleID");
                CheckBox chkSelect = (CheckBox)e.Row.FindControl("chkSelect");
                Label lblPaymentStatus = (Label)e.Row.FindControl("lblPaymentStatus");
                TextBox txtSuggestedAmount = (TextBox)e.Row.FindControl("txtSuggestedAmount");
               
                if(string.IsNullOrEmpty(lblOracleID.Text))
                {
                    lblOracleID.Text = "Oracle id </br>missing";
                    lblOracleID.ForeColor = System.Drawing.Color.Red;
                    chkSelect.Checked = false;
                    chkSelect.Enabled = false;
                    txtSuggestedAmount.Enabled = false;
                }

                int count = 0;
                if (LBAPInvoice != null && LBAPInvoice.Count > 0)
                {
                    count = LBAPInvoice.Where(lst => lst.OrderID == int.Parse(hdnOrderID.Value) && lst.OrderLineID == int.Parse(hdnOrderLineID.Value) && lst.IsChecked == true).ToList().Count;
                }

                if (count > 0)
                {
                    if (chkSelect.Enabled)
                    {
                        chkSelect.Checked = true;
                    }
                }
                

                if (lblPaymentStatus.Text != "0")
                {
                    chkSelect.Checked = false;
                    chkSelect.Attributes.Add("style", "display:none");
                    txtSuggestedAmount.Enabled = false;

                }

            }

        }

       protected void btnGo_Click(object sender, EventArgs e)
       {
           ClearValues();
           string msg = string.Empty;
           try
           {
               GetPaymentRequest(SearchBox.Text);
           }
           catch (Exception ex)
           {
               Diagnostics.WriteException(DiagnosticsCategory.AdminPortal, ex, null);
               throw;
           }

       }

       [System.Web.Services.WebMethod]
       public static bool UpdateSelectedCheck(string SelectMode, bool Status, Int32 OrderID, Int32 OrderLineID)
       {
           bool isSuccess = false;
           if (SelectMode == "SelectAll")
           {
               if (LBAPInvoice != null && LBAPInvoice.Count > 0)
               {
                   if (Status)
                   {
                       LBAPInvoice.ToList().ForEach(lst => { lst.IsChecked = true;});
                   }
                   else
                   {
                       LBAPInvoice.ToList().ForEach(lst => { lst.IsChecked = false; });
                   }
               }
           }
           else if (SelectMode == "Select" && OrderID > 0 && OrderLineID>0)
           {
               if (LBAPInvoice != null && LBAPInvoice.Count > 0)
               {
                   if (Status)
                   {
                       LBAPInvoice.Where(lst => lst.OrderID == OrderID && lst.OrderLineID == OrderLineID).ToList().ForEach(lst => { lst.IsChecked = true; });
                   }
                   else
                   {
                       LBAPInvoice.Where(lst => lst.OrderID == OrderID && lst.OrderLineID == OrderLineID).ToList().ForEach(lst => { lst.IsChecked = false; });
                   }
               }
           }

           if (LBAPInvoice != null && LBAPInvoice.Count > 0)
           {
               int count = 0;
               count = LBAPInvoice.Where(lst => lst.IsChecked == true).ToList().Count;
               if(count>0)
               {
                   isSuccess = true;
               }
           }

           return isSuccess;
       }

       protected void ResultGridView_Sorting(object sender, GridViewSortEventArgs e)
       {
           SetSortDirection(_sortDirection);
           if (e != null && e.SortExpression != null)
               _sortExpresssion = e.SortExpression.ToString();

           if (LBAPInvoice != null && LBAPInvoice.Count > 0)
           {
               SetRecordCount();
               this.ResultGridView.DataSource = LBAPInvoice.SortList(_sortExpresssion, _sortDirection).ToList();
               this.ResultGridView.DataBind();
           }
           else
           {
               this.ResultGridView.DataSource = LBAPInvoice;
               this.ResultGridView.DataBind();
           }

       }

       protected void SetSortDirection(string sortDirection)
       {
           if (sortDirection == "ASC")
           {
               _sortDirection = "DESC";
           }
           else
           {
               _sortDirection = "ASC";
           }
       }
    }
}