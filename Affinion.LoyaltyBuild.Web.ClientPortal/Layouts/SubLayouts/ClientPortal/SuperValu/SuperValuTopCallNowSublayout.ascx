﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SuperValuTopCallNowSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.SuperValuTopCallNowSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<p id="PlaceholderText" runat="server">Set the datasource: Required Fields: DesktopImage, MobileImage</p>
<div class="tp-number float-right width-full hidden-xs">
    <sc:Image Field="DesktopImage" CssClass="img-responsive float-right"  ID="DesktopImage" runat="server" />
</div>
<div class="tp-number float-right width-full visible-xs" id="tp-icon">
    <sc:Image Field="MobileImage" CssClass="img-responsive float-right"  ID="MobileImage" runat="server" />
</div>
