﻿CREATE PROCEDURE [dbo].[LB_GetActiveCustomerIdForClient]
(
	@ClientId VARCHAR(50),
	@Email nvarchar(255)
)
AS
BEGIN
    --SELECT TOP 1 CustomerId FROM LB_CustomerAttribute as CA WHERE (CA.BookingReferenceId = @BookingReferenceId) AND (CA.ClientId = @ClientId)-- AND  (CA.Active=1)
	SELECT C.CustomerId FROM uCommerce_Customer AS C 
	INNER JOIN LB_CustomerProperty AS CP ON C.CustomerId = CP.CustomerId
	WHERE  CP.[Key] = '_ClientId' AND CP.[Value]=@ClientId AND C.EmailAddress = @Email
END
