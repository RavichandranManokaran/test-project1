﻿using Affinion.Loyaltybuild.BusinessLogic.Helper;
using Affinion.LoyaltyBuild.Api.Booking.Service;
using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model.Payment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.Infrastructure;
using service = Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Payment;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    public partial class OfflinePortalRefund : BaseSublayout
    {
        private service.IPaymentService _paymentService;
        private int orderLineId = 0;
        private int currencyTypeID = 0;
        protected List<RefundableItem> AvailableCardPayments { get; set; }

        /// <summary>
        /// page load 
        /// </summary>      
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void Page_Load(object sender, EventArgs e)
        {
            _paymentService = ContainerFactory.Instance.GetInstance<service.IPaymentService>();

            int.TryParse(QueryString("olid"), out orderLineId);
            int.TryParse(QueryString("cid"), out currencyTypeID);
            IBookingService bookingservice = ObjectFactory.Instance.Resolve<IBookingService>();

            var booking = bookingservice.GetBooking(orderLineId);
            var clientId = booking.ClientId;

            var paymentMethods = _paymentService.GetPaymentMethods(clientId);
            ddlPaymentMethod.DataSource = paymentMethods;
            ddlPaymentMethod.DataTextField = "Name";
            ddlPaymentMethod.DataValueField = "Id";
            ddlPaymentMethod.DataBind();

            ddlCurrency.DataSource = _paymentService.GetCurrencyListDetails(); ;
            ddlCurrency.DataTextField = "Name";
            ddlCurrency.DataValueField = "CurrencyID";
            ddlCurrency.DataBind();

       

            var tmpDueItem = _paymentService.GetDuesByOrderlineID(orderLineId, currencyTypeID);
            if (tmpDueItem != null)
            {
                if (tmpDueItem.Count > 0)
                {
                    txtAmount.Text = tmpDueItem[0].Amount.ToString("f");
                    currencyTypeID = tmpDueItem[0].CurrencyTypeId;
                }
            }

            ddlCurrency.SelectedValue = currencyTypeID.ToString();
            ddlCurrency.Enabled = false;
            txtAmount.Enabled = false;

            var cardPaymentId = 0;
            var card = paymentMethods.FirstOrDefault(i => i.Name.ToLower().Contains("credit"));
            if (card != null)
                cardPaymentId = card.Id;

            AvailableCardPayments = _paymentService.GetAvailablePaymentsForRefund(orderLineId, cardPaymentId, currencyTypeID);
        }
    }
}