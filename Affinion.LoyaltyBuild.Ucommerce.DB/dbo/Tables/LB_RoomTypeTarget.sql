﻿CREATE TABLE [dbo].[LB_RoomTypeTarget] (
    [RoomTypeTargetId] INT            NOT NULL,
    [RoomType]         NVARCHAR (500) NOT NULL,
    CONSTRAINT [PK_uCommerce_RoomTypeTarget] PRIMARY KEY CLUSTERED ([RoomTypeTargetId] ASC),
    CONSTRAINT [FK_uCommerce_RoomTypeTarget_uCommerce_Target] FOREIGN KEY ([RoomTypeTargetId]) REFERENCES [dbo].[uCommerce_Target] ([TargetId])
);

