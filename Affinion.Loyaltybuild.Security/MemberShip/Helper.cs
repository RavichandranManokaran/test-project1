﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           MembershipHelper.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Security
/// Description:           Helper class for AffinionMembershipProvider.
/// </summary>

#region Using Directives
using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.DataAccess;
using Affinion.LoyaltyBuild.DataAccess.Models;
using Sitecore.Pipelines.LoggingIn;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
#endregion

namespace Affinion.LoyaltyBuild.Security.MemberShip
{
    public class Helper
    {
        
        PasswordHistoryDataAccess passwordHistory = new PasswordHistoryDataAccess();

        public bool Validate(string userName, string password)
        {
            return passwordHistory.Validate(userName, password);
        }

        /// <summary>
        /// Check if newpassword is already used 
        /// </summary>
        /// <param name="userName">user name</param>
        /// <param name="password">new password</param>
        /// <returns>Return true if success false otherwise</returns>
        public bool IsInPasswordHistory(string userName, string password)
        {
            try
            {
                bool isInHistory = false;

                int lastPassCount;

                if (int.TryParse(ConfigurationHelper.GetWebConfigAppSettingValue(Constants.LastHistoryPasswordCountSetting, "10"), out lastPassCount))
                {
                    /// Check if given password exist in the passwordhistory list
                    isInHistory = passwordHistory.Validate(userName, password, false);

                    if (passwordHistory.Count(userName) == lastPassCount && !isInHistory)
                    {
                        passwordHistory.DeleteFirst(userName);
                    }

                } 

                return isInHistory;

            }
            catch (Exception ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.Security, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Record password history data
        /// </summary>
        /// <param name="username">user name</param>
        /// <param name="newPassword">new password</param>
        /// <returns>Return true if success false otherwise</returns>
        public bool RecordHistoryPassword(string username, string newPassword)
        {
            try
            {
                /// Save password data
                return passwordHistory.Add(username, newPassword);
            }
            catch (Exception ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.Security, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Delete password History
        /// </summary>
        /// <param name="username">user name</param>
        /// <returns>Return true if success false otherwise</returns>
        public bool DeleteUserPasswordHistory(string username)
        {
            try
            {
                /// Delete Password data
                return passwordHistory.DeleteAll(username);
            }
            catch (Exception ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.Security, ex, this);
                throw;
            }
        }



        /// <summary>
        /// Record password history data
        /// </summary>
        /// <param name="username">user name</param>
        /// <param name="newPassword">new password</param>
        /// <returns>Return true if success false otherwise</returns>
        public void AddAuditLog(string username, bool isSuccess)
        {
            try
            {
                /// Save password data
                passwordHistory.LoginAudit(username, isSuccess);
            }
            catch (Exception ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.Security, ex, this);
                throw;
            }
        }
      
        
    }
}
