﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Helpers;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using System.IO;
using System.Xml;
using System.Transactions;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using System.Data.Common;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports
{
    public class ReportsService : IReportsService
    {
        // <summary>
        /// private variables
        /// </summary>    
        private string _connectionString = "UCommerce";
        private string _spGetFinanceOverallRevenue = "dbo.LB_GetFinanceOverallRevenueReport";
        private string _spGetFinanceBookingReport = "dbo.LB_GetFinanceBookingReport";
        private string _spGetDailyOracleFinanceReport = "dbo.LB_GetDailyOracleFinanceReport";
        private readonly string _spGetBookingCounterReport = "dbo.LB_BookingCounterReport";
        private readonly string _getAgentPerformanceDetails = "LB_GetAgentPerformance";
        private readonly string _spGetShortageRoomReport = "dbo.LB_RoomShortage";
        private readonly string _spGetProvisionalBookings = "dbo.LB_GetProvisionalBookings";
        private const string FilteredClientItems = @"fast:/sitecore/content/#admin-portal#/#client-setup#/*[@@templatename='ClientDetails' and @Active='1']";

        private string _spGetAPPaymentRequest = "dbo.LB_GetAPPaymentRequest";
        private string _spGetAPManualPaymentRequest = "dbo.LB_GetManualPaymentRequest";
        private string _spAPPaymentRequest = "dbo.LB_APPaymentRequest";
        private string _spInsertApPaymentRequests = "dbo.LB_APSqlJob";
        private string _spInsertArPaymentRequests = "dbo.LB_ARSqlJob";
        private string _spGetAPDemands = "dbo.LB_GetAPDemands";
        private string _spARPaymentRequest = "dbo.LB_ARPaymentRequest";
        private string _spGetOracleFileInfo = "dbo.LB_GetOracleFileInfo";
        private string _spGetARPaymentRequest = "dbo.LB_GetARPaymentRequest";
        private string _spGetOracleInvoiceData = "dbo.LB_GetOracleInvoiceData";
        private string _spDeleteHotelDemands = "dbo.LB_DeleteHotelDemands";
        private string _spDeleteVatPayments = "dbo.LB_DeleteVatPayments";
        private string _spARSummaryReport = "dbo.LB_ARSummaryReport";

        private string _spProviderAvailablityAlertSummary = "dbo.LB_ProviderAvailablityAlertSummary";

        private string _spGetMyBookingVsAgentsReport = "dbo.LB_GetMyBookingVsAgentsReport";


        /// <summary>
        /// constructor
        /// </summary>
        public ReportsService()
        {

        }

        /// <summary>
        /// Gets the UnClaimedReceipts information based receiptDate
        /// </summary>
        /// <param name="receiptDate">Receipt Date</param>
        /// <returns>UnClaimed Receipts details</returns> 
        public List<UnClaimedReceipts> GetUnClaimedReceipts(DateTime receiptDate)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "dbo.LB_GetUnClaimedReceiptsReport";
            List<UnClaimedReceipts> unClaimedReceipts = new List<UnClaimedReceipts>();
            using (var command = db.GetStoredProcCommand(sql))
            {
                db.AddInParameter(command, "ReceiptDate", DbType.Date, receiptDate);

                using (var reader = db.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        var item = new UnClaimedReceipts
                        {
                            ProductId = reader.Value<int>("ProductId"),
                            ProviderName = reader.Value<string>("Name"),
                            CurrencyTypeId = reader.Value<int>("CurrencyTypeId"),
                            ISOCode = reader.Value<string>("ISOCode"),
                            Amount = reader.Value<double>("Amount"),
                            Month = reader.Value<string>("Month"),
                            Year = reader.Value<string>("Year"),
                            Count = reader.Value<int>("Count"),
                        };
                        unClaimedReceipts.Add(item);
                    }
                }
            }
            return unClaimedReceipts;
        }


        // <summary>
        /// Gets the FinanceOverallRevenue information based on search criteria
        /// </summary>
        /// <param name="searchCriteria">SearchCriteria</param>
        /// <returns>Finance Overall Revenue Details</returns>
        public List<FinanceOverallRevenue> GetFinanceOverallRevenueReport(SearchCriteria searchCriteria)
        {
            try
            {
                DatabaseProviderFactory factory = new DatabaseProviderFactory();
                Database db = factory.Create(_connectionString);
                var sql = _spGetFinanceOverallRevenue;
                var financeOverallRevenue = new List<FinanceOverallRevenue>();
                using (var command = db.GetStoredProcCommand(sql))
                {
                    db.AddInParameter(command, "StartDate", DbType.Date, searchCriteria.StartDate);
                    db.AddInParameter(command, "EndDate", DbType.Date, searchCriteria.EndDate);
                    db.AddInParameter(command, "BookingMethodID", DbType.Int16, searchCriteria.BookingMethodID);
                    db.AddInParameter(command, "SubTypeReportID", DbType.Int16, searchCriteria.SubTypeReportID);

                    using (var reader = db.ExecuteReader(command))
                    {
                        while (reader.Read())
                        {
                            var item = new FinanceOverallRevenue
                            {
                                Bookings = reader.Value<int>("Bookings"),
                                ClientID = reader.Value<string>("ClientID"),
                                ClientOffer = reader.Value<string>("ClientOffer"),
                                CurrencyName = reader.Value<string>("CurrencyName"),
                                TotalPrice = reader.Value<decimal>("TotalPrice"),
                                VAT = reader.Value<decimal>("VAT"),
                                LoyaltyBuildRevenue = reader.Value<decimal>("LoyaltyBuildRevenue")
                            };
                            financeOverallRevenue.Add(item);
                        }
                    }
                }
                return financeOverallRevenue;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        // <summary>
        /// Gets the FinanceBooking
        /// </summary>
        /// <param name="searchCriteria">SearchCriteria</param>
        /// <returns>Finance Booking Details</returns>
        public List<FinanceBooking> GetFinanceBookingReport(SearchCriteria searchCriteria)
        {
            try
            {
                DatabaseProviderFactory factory = new DatabaseProviderFactory();
                Database db = factory.Create(_connectionString);
                var sql = _spGetFinanceBookingReport;
                var financeBooking = new List<FinanceBooking>();
                using (var command = db.GetStoredProcCommand(sql))
                {
                    db.AddInParameter(command, "StartDate", DbType.Date, searchCriteria.StartDate);
                    db.AddInParameter(command, "EndDate", DbType.Date, searchCriteria.EndDate);

                    using (var reader = db.ExecuteReader(command))
                    {
                        while (reader.Read())
                        {
                            var item = new FinanceBooking
                            {
                                Bookings = reader.Value<int>("Bookings"),
                                BookingMethodID = reader.Value<int>("BookingMethodID"),
                                BookingMethod = reader.Value<string>("BookingMethod"),
                                PartnerName = reader.Value<string>("PartnerName")
                            };
                            financeBooking.Add(item);
                        }
                    }
                }
                return financeBooking;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // <summary>
        /// Gets the DailyOracleFinanceReport
        /// </summary>
        /// <param name="searchCriteria">SearchCriteria</param>
        /// <returns>DailyOracleFinanceReport</returns>
        public List<DailyOracleFinance> GetDailyOracleFinanceReport(SearchCriteria searchCriteria)
        {
            try
            {
                DatabaseProviderFactory factory = new DatabaseProviderFactory();
                Database db = factory.Create(_connectionString);
                var sql = _spGetDailyOracleFinanceReport;
                var dailyOracleFinance = new List<DailyOracleFinance>();
                using (var command = db.GetStoredProcCommand(sql))
                {
                    db.AddInParameter(command, "StartDate", DbType.Date, searchCriteria.StartDate);

                    using (var reader = db.ExecuteReader(command))
                    {
                        while (reader.Read())
                        {
                            var item = new DailyOracleFinance
                            {
                                Currency = reader.Value<string>("Currency"),
                                Company = reader.Value<string>("Company"),
                                MerchantIdentification = reader.Value<string>("MerchantIdentificationNo"),
                                Partner = reader.Value<string>("ClientName"),
                                Department = reader.Value<string>("Department"),
                                Product = reader.Value<string>("ProductCode"),
                                GlCode = reader.Value<string>("GlCode"),
                                Client = reader.Value<string>("ClientCode"),
                                Project = reader.Value<string>("Project"),
                                Spare = reader.Value<string>("Spare"),
                                AmountCredit = reader.Value<decimal>("AmountCredit"),
                                AmountDebit = reader.Value<decimal>("AmountDebit"),
                                Description = reader.Value<string>("Description")
                            };
                            dailyOracleFinance.Add(item);
                        }
                    }
                }
                return dailyOracleFinance;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///  Gets the BookingCounterReport
        /// </summary>
        /// <returns>GetBookingCounterReport</returns>
        public BookingCounterReport GetBookingCounterReport()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);

            BookingCounterReport obBookingCounterReport = new BookingCounterReport();

            var clients = GetClientList();

            using (var command = db.GetStoredProcCommand(_spGetBookingCounterReport))
            {
                using (var reader = db.ExecuteReader(command))
                {
                    //today booking(s)
                    obBookingCounterReport.Today = MapClientDetails(GetBookingCounter(reader), clients);
                    //yesterday booking(s)using NextResult method in SqlDataReader
                    if (reader.NextResult())
                        obBookingCounterReport.Yesterday = MapClientDetails(GetBookingCounter(reader), clients);
                    //week booking(s)using NextResult method in SqlDataReader
                    if (reader.NextResult())
                        obBookingCounterReport.Week = MapClientDetails(GetBookingCounter(reader), clients);
                    //month booking(s)using NextResult method in SqlDataReader
                    if (reader.NextResult())
                        obBookingCounterReport.Month = MapClientDetails(GetBookingCounter(reader), clients);
                    //year booking(s)using NextResult method in SqlDataReader
                    if (reader.NextResult())
                        obBookingCounterReport.Year = MapClientDetails(GetBookingCounter(reader), clients);

                }

            }
            return obBookingCounterReport;
        }



        /// <summary>
        /// Get AgentPerformance Details
        /// </summary>
        /// <param name="createdby"></param>
        /// <param name="fromdate"></param>
        /// <param name="todate"></param>
        /// <returns></returns>
        public List<AgentPerformanceDetail> GetAgentPerformanceDetails(string createdby, DateTime fromdate, DateTime todate, string clientIds)
        {
            try
            {
                DatabaseProviderFactory factory = new DatabaseProviderFactory();
                Database db = factory.Create(_connectionString);
                var sql = _getAgentPerformanceDetails;
                var agentPerformaceList = new List<AgentPerformanceDetail>();
                using (var command = db.GetStoredProcCommand(sql))
                {
                    db.AddInParameter(command, "@createdby", DbType.String, createdby);
                    db.AddInParameter(command, "@fromdate", DbType.Date, fromdate);
                    db.AddInParameter(command, "@todate", DbType.Date, todate);
                    db.AddInParameter(command, "@ClientIds", DbType.String, clientIds);


                    using (var reader = db.ExecuteReader(command))
                    {
                        while (reader.Read())
                        {
                            var _dateTime = reader.Value<DateTime>("createddate");
                            var item = new AgentPerformanceDetail
                            {
                                AgentName = reader.Value<string>("AgentName"),
                                Date = string.Format("{0}({1})", _dateTime.Date.ToString("dd/MM/yyyy"), _dateTime.DayOfWeek),
                                TotalBookingMade = reader.Value<int>("BookingCount"),
                                TotalBookingConfirmed = reader.Value<int>("confirmedbookingscount"),

                            };
                            agentPerformaceList.Add(item);
                        }
                    }
                }
                return agentPerformaceList;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        ///<summary>
        ///Get Provisional Bookings
        ///</summary>
        ///<return> ProvisionalBookingReport</return>
        public List<ProvisionalBooking> GetProvisionalBooking(ProvisionalBookingFilter searchfilter)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = _spGetProvisionalBookings;
            List<ProvisionalBooking> ProvisionalBookingNew = new List<ProvisionalBooking>();

            using (var command = db.GetStoredProcCommand(sql))
            {
                db.AddInParameter(command, "@DateFrom", DbType.Date, searchfilter.DateFrom);
                db.AddInParameter(command, "@DateTo", DbType.Date, searchfilter.DateTo);
                db.AddInParameter(command, "@Partner", DbType.String, searchfilter.partner);
                db.AddInParameter(command, "@partialpaymentprovisional", DbType.Int16, searchfilter.partialpayment);
                db.AddInParameter(command, "@reportandorderby", DbType.String, searchfilter.reportandorderby);
                var b = db.ExecuteNonQuery(command);
                //var c = db.ExecuteScalar(command);

                using (var reader = db.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        var item = new ProvisionalBooking
                    {
                        BookingReference = reader.Value<string>("BookingReference"),
                        Provider = reader.Value<string>("ProviderName"),
                        ArrivalDate = reader.Value<DateTime>("ArrivalDate"),
                        ReservationDate = reader.Value<DateTime>("ReservationDate"),
                        CustomerName = reader.Value<string>("CustomerName"),

                        PhoneNumber = reader.Value<string>("PhoneNumber"),
                        //MobilePhoneNumber = reader.Value<string>("MobileNumber"),
                        EmailId = reader.Value<string>("Emailid")
                    };
                        ProvisionalBookingNew.Add(item);

                    }
                }
            }
            return ProvisionalBookingNew;
        }

        ///<summary>
        ///Get PriceChangeReport
        ///</summary>
        ///<return>PriceChangeReport</return>       
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <returns>List<string>lstAdditionalData</returns>

        public List<string> PriceChangeReport(DateTime dateFrom, DateTime dateTo)
        {
            List<string> lstAdditionalData = new List<string>();

            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create("uCommerce");

            using (DbCommand cmd = db.GetStoredProcCommand("LB_GetPriceChangeReport"))
            {
                db.AddInParameter(cmd, "@DateFrom", DbType.DateTime, dateFrom);
                db.AddInParameter(cmd, "@DateTo", DbType.DateTime, dateTo);
                using (var reader = db.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        lstAdditionalData.Add(reader["AdditionalData"].ToString());
                    }
                }
            }

            return lstAdditionalData;
        }
        
        #region Private Methods

        /// <summary>
        /// Get the ClientId and Booking(s) from BookingCounter
        /// </summary>
        /// <param name="reader"></param>
        /// <returns>BookingCounter</returns>
        private List<BookingCounter> GetBookingCounter(IDataReader reader)
        {

            List<BookingCounter> bookingCounter = new List<BookingCounter>();

            while (reader.Read())
            {
                bookingCounter.Add(new BookingCounter
                {
                    ClientId = reader.Value<string>("ClientId"),
                    Bookings = reader.Value<int>("Booking(s)")
                });
            }

            return bookingCounter;
        }


        /// <summary>
        /// MapClientDetails for count of client if available
        /// </summary>
        /// <param name="items"></param>
        /// <param name="clients"></param>
        /// <returns>BookingCounter</returns>
        private List<BookingCounter> MapClientDetails(List<BookingCounter> items, List<KeyValuePair<string, string>> clients)
        {
            var bookings = new List<BookingCounter>();

            foreach (var client in clients)
            {
                var booking = new BookingCounter
                {
                    ClientName = client.Value,
                    ClientId = Guid.Parse(client.Key).ToString(),
                };
                //get booking coumt for the client if available              
                if (items != null && items.Count > 0)
                {
                    var noOfBookings = items.Where(i => (Guid.Parse(i.ClientId).ToString()) == (Guid.Parse(client.Key).ToString()));
                    if (noOfBookings != null && noOfBookings.Count() > 0)
                        booking.Bookings = noOfBookings.FirstOrDefault().Bookings;
                    else
                        booking.Bookings = 0;

                }

                //add client to the list
                bookings.Add(booking);
            }

            return bookings;
        }

        /// <summary>
        /// GetClientList
        /// </summary>
        /// <returns>ClientName and Guid from Sitecore</returns>
        private List<KeyValuePair<string, string>> GetClientList()
        {
            return Sitecore.Context.Database.SelectItems(FilteredClientItems)
             .Select(x => new KeyValuePair<string, string>(x.ID.ToString(), x.DisplayName)).ToList();
        }

        /// <summary>
        /// Get String Value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public string GetStringValue(string value)
        {
            return string.IsNullOrWhiteSpace(value) ? null : value.Trim();
        }

        /// <summary>
        /// GetDateValue 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="deflt"></param>
        /// <returns></returns>
        public DateTime? GetDateValue(string value, DateTime? deflt = null)
        {
            DateTime? output = deflt;
            DateTime temp;

            if (DateTime.TryParse(value, out temp))
                output = temp;

            return output;

        }

        #endregion


        #region
        /// <summary>
        ///  Get the Report
        /// </summary>
        /// <returns>Get Shartage Room Report from Database</returns>
        public List<ShortageRoom> GetShortagelist()
        {
            var stockShortage = new List<ShortageRoomData>();
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);

            using (var command = db.GetStoredProcCommand(_spGetShortageRoomReport))
            {
                using (var reader = db.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        stockShortage.Add(new ShortageRoomData
                        {
                            //ProviderID = reader.Value<string>("ProviderID"),
                            VariantSku = reader.Value<string>("VariantSku"),
                            Date = reader.Value<DateTime>("DateCounter"),
                            SKU = reader.Value<string>("Sku"),
                            RoomType = reader.Value<string>("VariantSku")
                        });
                    }
                }
            }

            var RoomDeatls = stockShortage
                .GroupBy(i => i.SKU)
                .Select(i => new ProviderShortage
                {
                    SKU = i.Key,
                    RoomDeatls = i
                        .GroupBy(j => string.Format("{0}#{1}", j.VariantSku, j.Date.Month))
                        .Select(j => new ShortageRoom
                        {
                            RoomType = j.First().RoomType,
                            month = j.First().Date.ToString("MMMM"),
                            Day = string.Join(", ", j.Select(k => k.Date.Day))
                        })
                });

            List<ShortageRoom> obj = new List<ShortageRoom>();
            foreach (var item in RoomDeatls)
            {

                foreach (var child in item.RoomDeatls)
                {
                    ShortageRoom data = new ShortageRoom();
                   // data.ProviderID = item.ProviderId;
                    data.SKU = item.SKU;
                    data.RoomType = child.RoomType;
                    data.month = child.month;
                    data.Day = child.Day;
                    obj.Add(data);
                }
            }


            return obj;

        }


        #endregion



        #region LWP-1000,1001

        public List<APInvoice> GetPaymentRequest(SearchCriteria searchCriteria)
        {
            try
            {
                DatabaseProviderFactory factory = new DatabaseProviderFactory();
                Database db = factory.Create(_connectionString);
                var sql = _spGetAPPaymentRequest;
                var apInvoiceList = new List<APInvoice>();
                using (var command = db.GetStoredProcCommand(sql))
                {
                    db.AddInParameter(command, "@DateRange", DbType.Date, searchCriteria.DateRange);
                    db.AddInParameter(command, "@DateUpTo", DbType.Date, searchCriteria.DateUpTo);
                    db.AddInParameter(command, "@Provider", DbType.String, searchCriteria.Provider);
                    db.AddInParameter(command, "@Partner", DbType.String, searchCriteria.Partner);
                    db.AddInParameter(command, "@Spa", DbType.String, searchCriteria.Spa);
                    db.AddInParameter(command, "@PaymentStatus", DbType.String, searchCriteria.PaymentStatus);
                    db.AddInParameter(command, "@SearchText", DbType.String, searchCriteria.SearchText);


                    using (var reader = db.ExecuteReader(command))
                    {
                        while (reader.Read())
                        {
                            var item = new APInvoice
                            {
                                BookingRefNo = reader.Value<string>("BookingRefNo"),
                                BookingID = reader.Value<string>("BookingID"),
                                OrderID = reader.Value<int>("OrderId"),
                                OrderLineID = reader.Value<int>("OrderLineId"),
                                OfferName = reader.Value<string>("OfferName"),
                                CheckInDate = reader.Value<DateTime>("CheckInDate"),
                                CheckOutDate = reader.Value<DateTime>("CheckOutDate"),
                                CustomerName = reader.Value<string>("CustomerName"),
                                AdultsCount = reader.Value<string>("Adults"),
                                ChildrenCount = reader.Value<string>("Children"),
                                BookingStatus = reader.Value<string>("BookingStatus"),
                                BookingDate = reader.Value<DateTime>("BookingDate"),
                                AccommodationType = reader.Value<string>("AccommodationType"),
                                HotelName = reader.Value<string>("HotelName"),
                                RequestedDate = reader.Value<DateTime>("RequestedDate"),
                                RequestedBy = reader.Value<string>("RequestedBy"),
                                SuggestedAmount = reader.Value<decimal>("SuggestedAmount"),
                                Currency = reader.Value<string>("Currency"),
                                CurrencyID = reader.Value<int>("CurrencyId"),
                                OfferBased = reader.Value<string>("OfferBased"),
                                CustomerID = reader.Value<int>("CustomerId"),
                                ClientID = reader.Value<string>("ClientId").ToString(),
                                ProviderID = reader.Value<string>("ProviderID").ToString(),
                                SupplierPaymentId = reader.Value<int>("SupplierPaymentId"),
                                PaymentStatus = reader.Value<int>("PaymentStatus")
                            };
                            apInvoiceList.Add(item);
                        }
                    }
                }
                return apInvoiceList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public APInvoice GetManualPaymentRequest(SearchCriteria searchCriteria)
        {
            try
            {
                DatabaseProviderFactory factory = new DatabaseProviderFactory();
                Database db = factory.Create(_connectionString);
                var sql = _spGetAPManualPaymentRequest;
                APInvoice objAPInvoice = null;
                using (var command = db.GetStoredProcCommand(sql))
                {
                    db.AddInParameter(command, "@BookingRefNo", DbType.String, searchCriteria.BookingRefNumber);

                    using (var reader = db.ExecuteReader(command))
                    {
                        while (reader.Read())
                        {

                            var item = new APInvoice
                            {
                                OrderID = reader.Value<int>("OrderId"),
                                OrderLineID = reader.Value<int>("OrderLineId"),
                                OfferName = reader.Value<string>("OfferName"),
                                CheckInDate = reader.Value<DateTime>("CheckInDate"),
                                CheckOutDate = reader.Value<DateTime>("CheckOutDate"),
                                CustomerName = reader.Value<string>("CustomerName"),
                                BookingDate = reader.Value<DateTime>("BookingDate"),
                                Currency = reader.Value<string>("Currency"),
                                CurrencyID = reader.Value<int>("CurrencyId"),
                                AccommodationType = reader.Value<string>("AccommodationType"),
                                HotelName = reader.Value<string>("HotelName"),
                                PaymentPaidDate = reader.Value<DateTime>("PaymentPaidDate"),
                                SuggestedAmount = reader.Value<decimal>("SuggestedAmount"),
                                OfferBased = reader.Value<string>("OfferBased"),
                                CustomerID = reader.Value<int>("CustomerId"),
                                ProviderID = reader.Value<string>("ProviderID").ToString(),
                                PaymentStatus = reader.Value<int>("PaymentStatus"),
                                PaymentRequestedDate = reader.Value<DateTime>("PaymentRequestedDate"),
                                PaymentRequestStatus = reader.Value<int>("PaymentRequestStatus"),
                                RequestWithinSixMonth = reader.Value<int>("RequestWithinSixMonth"),
                                ClientID = reader.Value<string>("ClientId").ToString()
                            };
                            objAPInvoice = item;

                        }
                    }
                }
                return objAPInvoice;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ValidationResponse APPaymentRequest(List<APOraclePayment> lstAPOraclePayment, string requestFrom)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = _spAPPaymentRequest;

            var result = new ValidationResponse();

            using (var scope = new TransactionScope())
            {
                try
                {
                    var command = db.GetStoredProcCommand(sql);
                    db.AddInParameter(command, "@OraclePaymentXML", DbType.String, GetAPOraclePaymentXML(lstAPOraclePayment));
                    if (!string.IsNullOrEmpty(requestFrom))
                        db.AddInParameter(command, "@RequestFrom", DbType.String, requestFrom);
                    db.ExecuteNonQuery(command);

                    result.IsSuccess = true;
                    result.Message = " details are saved successfully";
                    scope.Complete();
                }
                catch
                {
                    result.IsSuccess = false;
                    result.Message = " details are not saved successfully";
                }
            }

            return result;

        }

        /// <summary>
        /// Generate automated supplier payment requests
        /// </summary>
        /// <returns></returns>
        public ValidationResponse GenerateSupplierPaymentRequests()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = _spInsertApPaymentRequests;

            var result = new ValidationResponse();

            using (var scope = new TransactionScope())
            {
                try
                {
                    var command = db.GetStoredProcCommand(sql);
                    db.ExecuteNonQuery(command);

                    result.IsSuccess = true;
                    result.Message = " details are saved successfully";
                    scope.Complete();
                }
                catch
                {
                    result.IsSuccess = false;
                    result.Message = " details are not saved successfully";
                }
            }

            return result;

        }

        /// <summary>
        /// Generate automated VAT payment requests
        /// </summary>
        /// <returns></returns>
        public ValidationResponse GenerateVatPaymentRequests()
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = _spInsertArPaymentRequests;

            var result = new ValidationResponse();

            using (var scope = new TransactionScope())
            {
                try
                {
                    var command = db.GetStoredProcCommand(sql);
                    db.ExecuteNonQuery(command);

                    result.IsSuccess = true;
                    result.Message = " details are saved successfully";
                    scope.Complete();
                }
                catch
                {
                    result.IsSuccess = false;
                    result.Message = " details are not saved successfully";
                }
            }

            return result;

        }

        private string GetAPOraclePaymentXML(List<APOraclePayment> lstAPOraclePayment)
        {
            StringBuilder builder = new StringBuilder();
            if (lstAPOraclePayment != null && lstAPOraclePayment.Count > 0)
            {
                using (StringWriter stringWriter = new StringWriter(builder))
                {
                    using (XmlTextWriter writer = new XmlTextWriter(stringWriter))
                    {
                        writer.WriteStartElement("APOraclePayment");

                        foreach (APOraclePayment objAPOraclePayment in lstAPOraclePayment)
                        {
                            writer.WriteStartElement("APOraclePayment");

                            writer.WriteStartElement("SupplierPaymentId");
                            writer.WriteValue(objAPOraclePayment.SupplierPaymentId);
                            writer.WriteEndElement();

                            writer.WriteStartElement("OrderId");
                            writer.WriteValue(objAPOraclePayment.OrderId);
                            writer.WriteEndElement();

                            writer.WriteStartElement("OrderLineId");
                            writer.WriteValue(objAPOraclePayment.OrderLineID);
                            writer.WriteEndElement();

                            writer.WriteStartElement("OrderDate");
                            writer.WriteValue(objAPOraclePayment.OrderDate);
                            writer.WriteEndElement();

                            writer.WriteStartElement("NominalCode");
                            if (!string.IsNullOrEmpty(objAPOraclePayment.NominalCode))
                                writer.WriteValue(objAPOraclePayment.NominalCode.ToString());
                            else
                                writer.WriteValue(string.Empty);
                            writer.WriteEndElement();

                            writer.WriteStartElement("CampaignAccountingId");
                            writer.WriteValue(objAPOraclePayment.CampaignAccountingID.ToString());
                            writer.WriteEndElement();

                            writer.WriteStartElement("OrderCurrencyId");
                            writer.WriteValue(objAPOraclePayment.CurrencyID.ToString());
                            writer.WriteEndElement();

                            writer.WriteStartElement("OrderCurrencyIsoCode");
                            if (!string.IsNullOrEmpty(objAPOraclePayment.CurrencyCode))
                                writer.WriteValue(objAPOraclePayment.CurrencyCode.ToString());
                            else
                                writer.WriteValue(string.Empty);
                            writer.WriteEndElement();

                            writer.WriteStartElement("EuroRate");
                            writer.WriteValue(objAPOraclePayment.EuroRate.ToString());
                            writer.WriteEndElement();

                            writer.WriteStartElement("CustomerId");
                            writer.WriteValue(objAPOraclePayment.CustomerID.ToString());
                            writer.WriteEndElement();

                            writer.WriteStartElement("SuggestedAmount");
                            writer.WriteValue(objAPOraclePayment.SuggestedAmount.ToString());
                            writer.WriteEndElement();

                            writer.WriteStartElement("PostedAmount");
                            writer.WriteValue(objAPOraclePayment.PostedAmount.ToString());
                            writer.WriteEndElement();

                            writer.WriteStartElement("OracleID");
                            if (!string.IsNullOrEmpty(objAPOraclePayment.OracleID))
                                writer.WriteValue(objAPOraclePayment.OracleID.ToString());
                            else
                                writer.WriteValue(string.Empty);
                            writer.WriteEndElement();

                            writer.WriteStartElement("SupplierAddressCode");
                            if (!string.IsNullOrEmpty(objAPOraclePayment.SupplierAddrCode))
                                writer.WriteValue(objAPOraclePayment.SupplierAddrCode.ToString());
                            else
                                writer.WriteValue(string.Empty);
                            writer.WriteEndElement();

                            writer.WriteStartElement("LineNumber");
                            writer.WriteValue(objAPOraclePayment.LineNumber.ToString());
                            writer.WriteEndElement();

                            writer.WriteStartElement("CreatedBy");
                            if (!string.IsNullOrEmpty(objAPOraclePayment.CreatedBy))
                                writer.WriteValue(objAPOraclePayment.CreatedBy.ToString());
                            else
                                writer.WriteValue(string.Empty);
                            writer.WriteEndElement();


                            writer.WriteStartElement("UpdatedBy");
                            if (!string.IsNullOrEmpty(objAPOraclePayment.UpdatedBy))
                                writer.WriteValue(objAPOraclePayment.UpdatedBy.ToString());
                            else
                                writer.WriteValue(string.Empty);
                            writer.WriteEndElement();

                            writer.WriteStartElement("IsDeleted");
                            writer.WriteValue(objAPOraclePayment.IsDeleted.ToString());
                            writer.WriteEndElement();

                            writer.WriteEndElement();

                        }

                        writer.WriteEndElement();

                    }
                }
            }
            return builder.ToString();
        }

        private string GetAROraclePaymentXML(List<AROraclePayment> lstAROraclePayment)
        {
            StringBuilder builder = new StringBuilder();
            if (lstAROraclePayment != null && lstAROraclePayment.Count > 0)
            {
                using (StringWriter stringWriter = new StringWriter(builder))
                {
                    using (XmlTextWriter writer = new XmlTextWriter(stringWriter))
                    {
                        writer.WriteStartElement("APOraclePayment");

                        foreach (AROraclePayment objAROraclePayment in lstAROraclePayment)
                        {
                            writer.WriteStartElement("APOraclePayment");

                            writer.WriteStartElement("SupplierVatPaymentId");
                            writer.WriteValue(objAROraclePayment.SupplierVatPaymentId);
                            writer.WriteEndElement();

                            writer.WriteStartElement("PostedVatAmount");
                            writer.WriteValue(objAROraclePayment.PostedAmount.ToString());
                            writer.WriteEndElement();

                            writer.WriteStartElement("NominalCode");
                            if (!string.IsNullOrEmpty(objAROraclePayment.NominalCode))
                                writer.WriteValue(objAROraclePayment.NominalCode.ToString());
                            else
                                writer.WriteValue(string.Empty);
                            writer.WriteEndElement();

                            writer.WriteStartElement("CampaignAccountingId");
                            writer.WriteValue(objAROraclePayment.CampaignAccountingID.ToString());
                            writer.WriteEndElement();

                            writer.WriteStartElement("OracleIdForVat");
                            if (!string.IsNullOrEmpty(objAROraclePayment.OracleID))
                                writer.WriteValue(objAROraclePayment.OracleID.ToString());
                            else
                                writer.WriteValue(string.Empty);
                            writer.WriteEndElement();

                            writer.WriteStartElement("CustomerAddressCode");
                            if (!string.IsNullOrEmpty(objAROraclePayment.CustomerAddressCode))
                                writer.WriteValue(objAROraclePayment.CustomerAddressCode.ToString());
                            else
                                writer.WriteValue(string.Empty);
                            writer.WriteEndElement();

                            writer.WriteStartElement("InvoiceId");
                            writer.WriteValue(objAROraclePayment.InvoiceId.ToString());
                            writer.WriteEndElement();

                            writer.WriteStartElement("LineNumber");
                            writer.WriteValue(objAROraclePayment.LineNumber.ToString());
                            writer.WriteEndElement();

                            writer.WriteStartElement("UpdatedBy");
                            writer.WriteValue(objAROraclePayment.UpdatedBy.ToString());
                            writer.WriteEndElement();

                            writer.WriteEndElement();

                        }

                        writer.WriteEndElement();
                    }
                }
            }
            return builder.ToString();
        }

        public List<APInvoice> GetDemands(SearchCriteria searchCriteria)
        {
            try
            {
                DatabaseProviderFactory factory = new DatabaseProviderFactory();
                Database db = factory.Create(_connectionString);
                var sql = _spGetAPDemands;
                var apInvoiceList = new List<APInvoice>();
                using (var command = db.GetStoredProcCommand(sql))
                {
                    db.AddInParameter(command, "@DateRange", DbType.Date, searchCriteria.DateRange);
                    db.AddInParameter(command, "@DateUpTo", DbType.Date, searchCriteria.DateUpTo);
                    db.AddInParameter(command, "@Currency", DbType.String, searchCriteria.Currency);
                    db.AddInParameter(command, "@PaymentStatus", DbType.String, searchCriteria.PaymentStatus);
                    db.AddInParameter(command, "@SearchText", DbType.String, searchCriteria.SearchText);

                    using (var reader = db.ExecuteReader(command))
                    {
                        while (reader.Read())
                        {
                            var item = new APInvoice
                            {
                                HotelID = reader.Value<string>("HotelID"),
                                DemandDate = reader.Value<DateTime>("DemandDate"),
                                BookingRefNo = reader.Value<string>("BookingRefNo"),
                                BookingID = reader.Value<string>("BookingID"),
                                OrderID = reader.Value<int>("OrderId"),
                                OrderLineID = reader.Value<int>("OrderLineId"),
                                BookingDate = reader.Value<DateTime>("BookingDate"),
                                CustomerName = reader.Value<string>("CustomerName"),
                                CheckInDate = reader.Value<DateTime>("CheckInDate"),
                                CheckOutDate = reader.Value<DateTime>("CheckOutDate"),
                                PaymentStatus = reader.Value<int>("PaymentStatus"),
                                SuggestedAmount = reader.Value<decimal>("SuggestedAmount"),
                                PostedAmount = reader.Value<decimal>("SuggestedAmount"),
                                DatePostedToOracle = reader.Value<DateTime>("DatePostedToOracle"),
                                Currency = reader.Value<string>("Currency"),
                                OracleFieID = reader.Value<string>("OracleFieID"),
                                SupplierPaymentId = reader.Value<int>("SupplierPaymentId")
                            };
                            apInvoiceList.Add(item);
                        }
                    }
                }
                return apInvoiceList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ValidationResponse ARPaymentRequest(List<AROraclePayment> lstAROraclePayment)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = _spARPaymentRequest;

            var result = new ValidationResponse();

            using (var scope = new TransactionScope())
            {
                try
                {
                    var command = db.GetStoredProcCommand(sql);
                    db.AddInParameter(command, "OraclePaymentXML", DbType.String, GetAROraclePaymentXML(lstAROraclePayment));
                    db.ExecuteNonQuery(command);

                    result.IsSuccess = true;
                    result.Message = "Oracle sales invoice details are updated successfully";
                    scope.Complete();
                }
                catch
                {
                    result.IsSuccess = false;
                    result.Message = "Oracle sales invoice details are not updated successfully";
                }
            }

            return result;
        }

        public List<OracleFileInfo> GetOracleFileInfo()
        {
            try
            {
                DatabaseProviderFactory factory = new DatabaseProviderFactory();
                Database db = factory.Create(_connectionString);
                var sql = _spGetOracleFileInfo;
                var oracleFileInfoList = new List<OracleFileInfo>();
                using (var command = db.GetStoredProcCommand(sql))
                {
                    using (var reader = db.ExecuteReader(command))
                    {
                        while (reader.Read())
                        {
                            var item = new OracleFileInfo
                            {
                                OracleFileId = reader.Value<int>("OracleFileId"),
                                OracleFileName = reader.Value<string>("OracleFileName"),
                                OracleFileType = reader.Value<string>("OracleFileType"),
                                OracleFileStatus = reader.Value<int>("IsClosedOff"),
                                IsPostedToOracle = reader.Value<int>("IsPostedToOracle"),
                                ClosedOutBy = reader.Value<string>("ClosedOutBy"),
                                DatePostedToOracle = reader.Value<DateTime>("ClosedOutDate"),
                                SequenceNo = reader.Value<int>("SequenceNo")
                            };
                            oracleFileInfoList.Add(item);
                        }
                    }
                }
                return oracleFileInfoList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ARInvoice> GetVATPaymentRequest(SearchCriteria searchCriteria)
        {
            try
            {
                DatabaseProviderFactory factory = new DatabaseProviderFactory();
                Database db = factory.Create(_connectionString);
                var sql = _spGetARPaymentRequest;
                var arInvoiceList = new List<ARInvoice>();
                using (var command = db.GetStoredProcCommand(sql))
                {
                    db.AddInParameter(command, "@DateRange", DbType.Date, searchCriteria.DateRange);
                    db.AddInParameter(command, "@DateUpTo", DbType.Date, searchCriteria.DateUpTo);
                    db.AddInParameter(command, "@Currency", DbType.String, searchCriteria.Currency);
                    db.AddInParameter(command, "@PaymentStatus", DbType.String, searchCriteria.PaymentStatus);
                    db.AddInParameter(command, "@SearchText", DbType.String, searchCriteria.SearchText);


                    using (var reader = db.ExecuteReader(command))
                    {
                        while (reader.Read())
                        {
                            var item = new ARInvoice
                            {
                                SupplierVatPaymentId = reader.Value<int>("SupplierVatPaymentId"),
                                HotelID = reader.Value<string>("HotelID"),
                                ProviderName = reader.Value<string>("HotelName"),
                                DemandDate = reader.Value<DateTime>("DemandDate"),
                                BookingRefNo = reader.Value<string>("BookingRefNo"),
                                BookingID = reader.Value<string>("BookingID"),
                                OrderID = reader.Value<int>("OrderID"),
                                OrderLineID = reader.Value<int>("OrderLineId"),
                                CustomerName = reader.Value<string>("CustomerName"),
                                CheckInDate = reader.Value<DateTime>("CheckInDate"),
                                CheckOutDate = reader.Value<DateTime>("CheckOutDate"),
                                PaymentStatus = reader.Value<int>("PaymentStatus"),
                                GrossAmount = reader.Value<decimal>("GrossAmount"),
                                SuggestedAmount = reader.Value<decimal>("SuggestedAmount"),
                                NetAmount = reader.Value<decimal>("NetAmount"),
                                PostedAmount = reader.Value<decimal>("PostedAmount"),
                                Currency = reader.Value<string>("Currency"),
                                DatePostedToOracle = reader.Value<DateTime>("DatePostedToOracle"),
                                OracleFieID = reader.Value<string>("OracleFieID"),
                                ProviderID = reader.Value<string>("ProviderID").ToString(),
                                ClientID = reader.Value<string>("ClientId").ToString()
                            };
                            arInvoiceList.Add(item);
                        }
                    }
                }
                return arInvoiceList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<OracleInvoiceData> GetOracleInvoiceData(int oracleFileID, string requestFrom)
        {
            try
            {
                DatabaseProviderFactory factory = new DatabaseProviderFactory();
                Database db = factory.Create(_connectionString);
                var sql = _spGetOracleInvoiceData;
                var oracleInvoiceDataList = new List<OracleInvoiceData>();
                using (var command = db.GetStoredProcCommand(sql))
                {
                    db.AddInParameter(command, "@OracleFileID", DbType.Int32, oracleFileID);
                    db.AddInParameter(command, "@RequestedFrom", DbType.String, requestFrom);
                    using (var reader = db.ExecuteReader(command))
                    {
                        if (requestFrom == "AP")
                        {
                            while (reader.Read())
                            {
                                var item = new OracleInvoiceData
                                {
                                    InvoiceID = reader.Value<long>("InvoiceID"),
                                    InvoiceDate = reader.Value<DateTime>("InvoiceDate"),
                                    VendorNumber = reader.Value<long>("VendorNumber"),
                                    VendorSiteCode = reader.Value<string>("VendorSiteCode"),
                                    CurrencyCode = reader.Value<string>("CurrencyCode"),
                                    Amount = reader.Value<decimal>("Amount"),
                                    Description = reader.Value<string>("Description"),
                                    ProviderID = reader.Value<string>("ProviderID").ToString(),
                                    ClientID = reader.Value<string>("ClientId").ToString()
                                };
                                oracleInvoiceDataList.Add(item);
                            }
                        }
                        else if (requestFrom == "AR")
                        {
                            while (reader.Read())
                            {
                                var item = new OracleInvoiceData
                                {
                                    InvoiceID = reader.Value<long>("InvoiceID"),
                                    InvoiceDate = reader.Value<DateTime>("InvoiceDate"),
                                    CustomerId = reader.Value<int>("CustomerId"),
                                    CustomerAddressCode = reader.Value<string>("CustomerAddressCode"),
                                    CurrencyCode = reader.Value<string>("CurrencyCode"),
                                    Amount = reader.Value<decimal>("Amount"),
                                    Description = reader.Value<string>("Description"),
                                    ProviderID = reader.Value<string>("ProviderID").ToString(),
                                    SourceInvoiceLineId = reader.Value<int>("SupplierVatPaymentId"),
                                    LineNumber = reader.Value<int>("LineNumber"),
                                    ClientID = reader.Value<string>("ClientId").ToString()

                                };
                                oracleInvoiceDataList.Add(item);
                            }

                        }
                    }
                }
                return oracleInvoiceDataList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ValidationResponse DeleteHotelDemands(int orderID, int orderLineID)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = _spDeleteHotelDemands;

            var result = new ValidationResponse();

            using (var scope = new TransactionScope())
            {
                try
                {
                    var command = db.GetStoredProcCommand(sql);
                    db.AddInParameter(command, "@OrderID", DbType.Int32, orderID);
                    db.AddInParameter(command, "@OrderLineID", DbType.Int32, orderLineID);
                    db.ExecuteNonQuery(command);

                    result.IsSuccess = true;
                    result.Message = "Hotel demand is deleted successfully";
                    scope.Complete();
                }
                catch
                {
                    result.IsSuccess = false;
                    result.Message = "Hotel demand is not deleted successfully";
                }
            }

            return result;

        }

        public ValidationResponse DeleteVATPayment(int SupplierVatPaymentId)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = _spDeleteVatPayments;

            var result = new ValidationResponse();

            using (var scope = new TransactionScope())
            {
                try
                {
                    var command = db.GetStoredProcCommand(sql);
                    db.AddInParameter(command, "@SupplierVatPaymentId", DbType.Int32, SupplierVatPaymentId);
                    db.ExecuteNonQuery(command);

                    result.IsSuccess = true;
                    result.Message = "Vat payment is deleted successfully";
                    scope.Complete();
                }
                catch
                {
                    result.IsSuccess = false;
                    result.Message = "Vat payment is deleted successfully";
                }
            }

            return result;

        }

        public List<FinanceSummaryReport> GetARSummaryReport(SearchCriteria searchCriteria)
        {
            try
            {
                DatabaseProviderFactory factory = new DatabaseProviderFactory();
                Database db = factory.Create(_connectionString);
                var sql = _spARSummaryReport;
                var apSummaryList = new List<FinanceSummaryReport>();
                using (var command = db.GetStoredProcCommand(sql))
                {
                    db.AddInParameter(command, "@OracleFileID", DbType.Int32, searchCriteria.OracleFileID);
                    db.AddInParameter(command, "@DateRange", DbType.Date, searchCriteria.DateRange);
                    db.AddInParameter(command, "@DateUpTo", DbType.Date, searchCriteria.DateUpTo);
                    db.AddInParameter(command, "@Provider", DbType.String, searchCriteria.Provider);
                    db.AddInParameter(command, "@Partner", DbType.String, searchCriteria.Partner);

                    using (var reader = db.ExecuteReader(command))
                    {
                        while (reader.Read())
                        {
                            var item = new FinanceSummaryReport
                            {
                                HotelName = reader.Value<string>("HotelName"),
                                NumberOfSales = reader.Value<int>("NumberOfSales"),
                                Amount = reader.Value<decimal>("ProviderAmount"),
                                CurrencyCode = reader.Value<string>("CurrencyCode")
                            };
                            apSummaryList.Add(item);
                        }
                    }
                }
                return apSummaryList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        #region LWP-903
        public List<ProviderAvailabilityAlert> GetProviderAvailabilityAlertReport(SearchCriteria searchCriteria)
        {
            try
            {
                DatabaseProviderFactory factory = new DatabaseProviderFactory();
                Database db = factory.Create(_connectionString);
                var sql = _spProviderAvailablityAlertSummary;
                var apProviderAvailabilityAlertList = new List<ProviderAvailabilityAlert>();
                using (var command = db.GetStoredProcCommand(sql))
                {
                    db.AddInParameter(command, "@StartDate", DbType.Date, searchCriteria.DateRange);
                    db.AddInParameter(command, "@EndDate", DbType.Date, searchCriteria.DateUpTo);
                    db.AddInParameter(command, "@SupplierType", DbType.String, searchCriteria.SupplierType);
                    db.AddInParameter(command, "@Provider", DbType.String, searchCriteria.Provider);
                    db.AddInParameter(command, "@OccupancyType", DbType.String, searchCriteria.OccupancyType);

                    using (var reader = db.ExecuteReader(command))
                    {
                        while (reader.Read())
                        {
                            var item = new ProviderAvailabilityAlert
                            {
                                ProviderID = reader.Value<string>("ProviderID").ToString(),
                                ProductID = reader.Value<int>("ProductID"),
                                ProductName = reader.Value<string>("ProductName"),
                                RoomTypeName = reader.Value<string>("RoomType"),
                                ProviderDate = reader.Value<DateTime>("ProviderDate"),
                                ReportedDate = DateTime.Now.Date,
                                Year = reader.Value<DateTime>("ProviderDate").Year.ToString(),
                                Month = reader.Value<DateTime>("ProviderDate").Month.ToString(),
                                Day = reader.Value<DateTime>("ProviderDate").Day.ToString(),
                            };
                            apProviderAvailabilityAlertList.Add(item);
                        }
                    }
                }
                return apProviderAvailabilityAlertList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region LWP-5

        public List<MyTodaysBooking> GetMyTodaysBooking(string loggedOnUser, string clientIds)
        {
            try
            {
                var factory = new DatabaseProviderFactory();
                var db = factory.Create(_connectionString);

                var myTodaysBookingList = new List<MyTodaysBooking>();

                using (var command = db.GetStoredProcCommand(_spGetMyBookingVsAgentsReport))
                {
                    db.AddInParameter(command, "@CurrentUser", DbType.String, loggedOnUser);
                    db.AddInParameter(command, "@ClientIds", DbType.String, clientIds);
                    var b = db.ExecuteNonQuery(command);

                    using (var reader = db.ExecuteReader(command))
                    {
                        while (reader.Read())
                        {
                            var item = new MyTodaysBooking
                            {
                                TimeFrame = reader.Value<string>("TimeFrame"),
                                CurrentUser = reader.Value<Int32>("CurrentUser"),
                                OtherUsers = reader.Value<Int32>("OtherUsers"),
                                Total = reader.Value<Int32>("Total"),
                            };
                            myTodaysBookingList.Add(item);
                        }
                    }
                }
                return myTodaysBookingList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}

