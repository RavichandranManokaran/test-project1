﻿
using Affinion.LoyaltyBuild.Api.Booking.Service;
using Affinion.LoyaltyBuild.Model.Product;
using Affinion.LoyaltyBuild.Model.Provider;
using System;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using Affinion.LoyaltyBuild.UCom.Common.Extension;

namespace Affinion.LoyaltyBuild.Api.Booking.Data
{
    /// <summary>
    /// Booking info
    /// </summary>
    public abstract class Booking
    {
        //ucommerce booking
        private readonly OrderLine _booking;
        private readonly int _orderLineId;
        private readonly int _orderId;

        /// <summary>
        /// indexer property
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string this[string key]
        {
            get
            {
                return _booking[key];
            }
            set
            {
                _booking[key] = value;
            }
        }

        /// <summary>
        /// Constructor to load booking info
        /// </summary>
        /// <param name="orderLineId"></param>
        public Booking(int orderLineId)
        {
            _orderLineId = orderLineId;
            _booking = OrderLine.FirstOrDefault(i => i.OrderLineId == orderLineId);
            _orderId = _booking.PurchaseOrder.OrderId;
        }

        #region Public Properties

        /// <summary>
        /// order line info
        /// </summary>
        public OrderLine OrderLine
        {
            get
            {
                return _booking;
            }
        }

        /// <summary>
        /// gets the orderline info
        /// </summary>
        public int OrderLineId { get { return _orderLineId; } }

        /// <summary>
        /// OrderId
        /// </summary>
        public int OrderId
        {
            get
            {
                return _orderId;
            }
        }
        
        /// <summary>
        /// Provider id
        /// </summary>
        public virtual string ProviderId
        {
            get
            {
                return OrderLine.Sku;
            }
        }

        /// <summary>
        /// Booking status
        /// </summary>
        public BookingStatus Status
        {
            get;
            set;
        }

        /// <summary>
        /// get if it is a child
        /// </summary>
        public bool IsChildBooking { get; set; }

        /// <summary>
        /// ProductType Property
        /// </summary>
        public ProductType Type
        {
            get
            {
                ProductType type = ProductType.Room;
                Enum.TryParse<ProductType>(this[Constants.ProductType], out type);
                return type;
            }
        }
        
        /// <summary>
        /// Quantity/no of rooms
        /// </summary>
        public int Quantity
        {
            get
            {
                return _booking.Quantity;
            }
        }

        /// <summary>
        /// CheckinDate Property
        /// </summary>
        public virtual DateTime CheckinDate
        {
            get
            {
                DateTime checkinDate = (this[Constants.CheckinDate]).ParseFormatDateTime(true);
                //Store date from Extension
                //DateTime.TryParse(this[Constants.CheckinDate], out checkinDate);
                return checkinDate;
            }
            set
            {
                //Store date from Extension
                this[Constants.CheckinDate] = value.ConvertDateToString(true); // Convert.ToString(value);
            }
        }

        /// <summary>
        /// CheckoutDate Property
        /// </summary>
        public virtual DateTime CheckoutDate
        {
            get
            {
                DateTime checkoutDate = (this[Constants.CheckoutDate]).ParseFormatDateTime(true);
                //Store date from Extension
                //DateTime.TryParse(this[Constants.CheckinDate], out checkinDate);
                return checkoutDate;
            }
            set
            {
                //Store date from Extension
                this[Constants.CheckoutDate] = value.ConvertDateToString(true);
                //this[Constants.CheckoutDate] = Convert.ToString(value);
            }
        }

        /// <summary>
        /// BookingThrough Property
        /// </summary>
        public string BookingThrough
        {
            get
            {
                if (!string.IsNullOrEmpty(this[Constants.BookingThrough]))
                    return this[Constants.BookingThrough];
                else
                    return string.Empty;
            }
            set
            {
                this[Constants.BookingThrough] = value;
            }
        }

        /// <summary>
        /// CreatedBy Property
        /// </summary>
        public string CreatedBy { get; set; }

        /// <summary>
        /// CreatedBy Property
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Update By
        /// </summary>
        public string UpdateBy { get; set; }
        
        /// <summary>
        /// Update By
        /// </summary>
        public DateTime UpdateDate { get; set; }

        /// <summary>
        /// Current status date
        /// </summary>
        public DateTime? CurrentStatusDate { get; set; }

        /// <summary>
        /// Confirmed date
        /// </summary>
        public DateTime? ConfirmedStatusDate { get; set; }

        /// <summary>
        /// Currency id
        /// </summary>
        public int CurrencyId
        {
            get
            {
                int currency = 0;
                int.TryParse(this[Constants.CurrencyId], out currency);
                return currency;
            }
            set
            {
                this[Constants.CurrencyId] = Convert.ToString(value);
            }
        }

        /// <summary>
        /// Price Property
        /// </summary>
        public decimal Price
        {
            get
            {
                if (OrderLine.Total != null)
                    return OrderLine.Total.Value;

                //retun default
                return 0;
            }
        }

        /// <summary>
        /// Vat
        /// </summary>
        public decimal Vat
        {
            get
            {
                decimal price = 0;
                decimal.TryParse(this[Constants.Vat], out price);
                return price;
            }
            set
            {
                this[Constants.Vat] = Convert.ToString(value);
            }
        }

        /// <summary>
        /// Calculated processing fee
        /// </summary>
        public decimal CalculatedProcessingFee
        {
            get
            {
                decimal price = 0;
                decimal.TryParse(this[Constants.CalculatedProcessingFee], out price);
                return price;
            }
            set
            {
                this[Constants.CalculatedProcessingFee] = Convert.ToString(value);
            }
        }

        /// <summary>
        /// Processing fee
        /// </summary>
        public decimal ProcessingFee
        {
            get
            {
                decimal price = 0;
                decimal.TryParse(this[Constants.ProcessingFee], out price);
                return price;
            }
            set
            {
                this[Constants.ProcessingFee] = Convert.ToString(value);
            }
        }

        /// <summary>
        /// commission Property
        /// </summary>
        public decimal CommissionAmount
        {
            get
            {
                decimal commissionAmount = 0;
                decimal discountcommissionAmount = 0;
                decimal.TryParse(this[Constants.CommissionAmount], out commissionAmount);
                decimal.TryParse(this[Constants.CommissionAmount], out discountcommissionAmount);

                return commissionAmount;
            }
            set
            {
                this[Constants.CommissionAmount] = Convert.ToString(value);
            }
        }

        /// <summary>
        /// ProviderAmount Property
        /// </summary>
        public decimal ProviderAmount
        {
            get
            {
                decimal providerAmount = 0;
                decimal.TryParse(this[Constants.ProviderAmount], out providerAmount);
                return providerAmount;
            }
            set
            {
                this[Constants.ProviderAmount] = Convert.ToString(value);
            }
        }

        /// <summary>
        /// TotalPayableToday Property
        public decimal ProviderAmountPaidOnCheckOut
        {
            get
            {
                decimal providerAmountPaidOnCheckOut = 0;
                decimal.TryParse(this[Constants.ProviderAmountPaidOnCheckOut], out providerAmountPaidOnCheckOut);
                return providerAmountPaidOnCheckOut;
            }
            set
            {
                this[Constants.ProviderAmountPaidOnCheckOut] = Convert.ToString(value);
            }
        }

        /// <summary>
        /// TotalPayableToday Property
        /// </summary>
        public decimal TotalPayableToday
        {
            get
            {
                return ProviderAmountPaidOnCheckOut + CommissionAmount + Vat + ProcessingFee;
            }
        }

        /// <summary>
        /// Client Id Property
        /// </summary>
        public string ClientId
        {
            get
            {
                if (!string.IsNullOrEmpty(this[Constants.ClientId]))
                    return this[Constants.ClientId];
                else
                    return string.Empty;
            }
            set
            {
                this[Constants.ClientId] = value;
            }
        }

        /// <summary>
        /// Booking reference
        /// </summary>
        public string BookingReferenceNo
        {
            get;
            set;
        }
        
        #endregion

        #region Public Methods

        /// <summary>
        /// Saves the changes to the orderline items
        /// </summary>
        public void Save()
        {
            if (_booking != null)
            {
                _booking.Save();
            }
        }

        /// <summary>
        /// Update status
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public virtual bool UpdateStatus(BookingStatus status)
        {
            var bookingService = ObjectFactory.Instance.Resolve<IBookingService>();

            return bookingService.UpdateStatus(OrderLineId, status);
        }

        /// <summary>
        /// Update booking reference
        /// </summary>
        /// <param name="bookingReference"></param>
        /// <returns></returns>
        public virtual bool UpdateBookingReference(string bookingReference)
        {
            var bookingService = ObjectFactory.Instance.Resolve<IBookingService>();

            return bookingService.UpdateBookingReference(OrderLineId, bookingReference);
        }

        #endregion

        #region Abstract Methods

        /// <summary>
        /// Get product details
        /// </summary>
        /// <returns></returns>
        public abstract IProduct GetProduct();

        /// <summary>
        /// Get product details
        /// </summary>
        /// <returns></returns>
        public abstract IProvider GetProvider();

        #endregion
    }
}
