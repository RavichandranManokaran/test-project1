﻿<%@ control language="C#" autoeventwireup="true" codebehind="MarginManagementSearch.ascx.cs" inherits="Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.BedBanks.MarginManagementSearch" %>

<div class="col-sm-12">
    <div class="accordian-outer">
        <div class="panel-group" id="accordion">
            <div class="panel panel-default">
                <div class="panel-heading bg-primary">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="show-hide-list" aria-expanded="true">
                            <sc:text id="title" field="Title Text" runat="server" />
                        </a>
                    </h4>
                </div>
                <div id="collapse1" class="panel-collapse collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <div class="row margin-row">
                            <div class="col-md-5 no-padding">
                                <div class="col-md-4 no-padding"><span class="font-size-12">
                                    <sc:text id="country" field="Country Label Text" runat="server" />
                                </span></div>
                                <div class="col-md-8">
                                    <asp:TextBox ID="txtCountry" runat="server" CssClass="form-control font-size-12" ToolTip="Country"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4 no-padding">
                                <div class="col-md-4 no-padding"><span class="font-size-12">
                                    <sc:text id="location" field="Location Label Text" runat="server" />
                                </span></div>
                                <div class="col-md-8">
                                    <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control font-size-12" ToolTip="Location"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3 no-padding"></div>
                        </div>
                        <div class="row margin-row">
                            <div class="col-md-5 no-padding"></div>
                            <div class="col-md-4 no-padding"></div>
                            <div class="col-md-3 no-padding"></div>
                        </div>
                        <div class="row margin-row">
                            <div class="col-md-5 no-padding">
                                <div class="col-md-4 no-padding"><span class="font-size-12">
                                    <sc:text id="maplocation" field="Map Location Label Text" runat="server" />
                                </span></div>
                                <div class="col-md-8">
                                    <asp:TextBox ID="txtMapLocation" runat="server" CssClass="form-control font-size-12" ToolTip="Map Location"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4 no-padding">
                                <div class="col-md-4 no-padding"><span class="font-size-12">
                                    <sc:text id="provider" field="Provider Name Label Text" runat="server" />
                                </span></div>
                                <div class="col-md-8">
                                    <asp:TextBox ID="txtProvider" runat="server" CssClass="form-control font-size-12" ToolTip="Provider Name"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3 no-padding">
                                <button type="button" runat="server" id="btnExport" onserverclick="btnExport_ServerClick" class="btn btn-default font-size-12 pull-right marL">
                                    <sc:text id="export" field="Export Button Text" runat="server" />
                                </button>
                                <button type="button" runat="server" id="btnSubmit" onserverclick="btnSubmit_ServerClick" class="btn btn-default font-size-12 pull-right" >
                                    <sc:text id="submit" field="Submit Query Button Text" runat="server" />
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
							<div class="col-sm-12 marT15" id="addMarginDiv" runat="server">
								<div class="accordian-outer">
									<div class="panel-group" id="accordion">
										<div class="panel panel-default">
											<div class="panel-heading bg-primary">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="show-hide-list"><sc:Text id="txtSubtitle" field="Add Margin SubTitle Text" runat="server"/></a>
												</h4>
											</div>
											<div id="collapse2" class="panel-collapse collapse in">
												<div class="panel-body"> 
													<div class="row margin-row">
														<div class="col-md-4 no-padding">
															<div class="col-md-4 no-padding"><span class="font-size-12"><sc:Text id="txtFrom" field="Date From Text" runat="server"/></span></div>
															<div class="col-md-8 ">
																<div class="control-group form-group">
																	<div class="controls input-append date form_date input-group date" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
																		<asp:TextBox ID="txtDateFrom" CssClass="form-control" runat="server"/>
                                                                    <span class="add-on input-group-addon"><i class="icon-th glyphicon glyphicon-calendar"></i></span> </div>   	
                                                                         	<div><asp:RequiredFieldValidator ID="rfvDateFrom" runat="server" ErrorMessage="*Required" ControlToValidate="txtDateFrom" ValidationGroup="marginGroup"></asp:RequiredFieldValidator></div>
																	<input type="hidden" id="dtp_input2" value="" /><br/>
																</div>
															</div>
														</div>
														<div class="col-md-4 no-padding">
															<div class="col-md-4 no-padding"><span class="font-size-12"><sc:Text id="txtTo" field="Date To Text" runat="server"/></span></div>
															<div class="col-md-8">
																<div class="control-group form-group">
																	<div class="controls input-append date form_date input-group date" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
																		<asp:TextBox ID="txtDateTo" CssClass="form-control" runat="server"/>
                                                                        <span class="add-on input-group-addon"><i class="icon-th glyphicon glyphicon-calendar"></i></span></div>
																<div><asp:RequiredFieldValidator ID="rfvDateTo" runat="server" ErrorMessage="*Required" ControlToValidate="txtDateTo" ValidationGroup="marginGroup"></asp:RequiredFieldValidator></div>
																	<input type="hidden" id="dtp_input2" value="" /><br/>
																</div>
															</div>
														</div>
														<div class="col-md-4 no-padding">
															<div class="col-md-4 no-padding"><sc:Text id="txtmarginpercent" field="Margin Percentage Text" runat="server"/></div>
															<div class="col-md-8 no-padding">
                                                                <asp:TextBox ID="txtMargin" CssClass="form-control" runat="server"/></div>
                                                                <div><asp:RequiredFieldValidator ID="rfvMargin" runat="server" ErrorMessage="*Required" ControlToValidate="txtMargin" ValidationGroup="marginGroup"></asp:RequiredFieldValidator></div>
														</div>
													</div>
													<div class="row margin-row">
														<div class="col-md-4 no-padding"></div>
														<div class="col-md-4 no-padding"></div>
														<div class="col-md-4 no-padding">
															<button type="submit" id="btnAddMargin" runat="server" onserverclick="btnAddMargin_ServerClick" validationgroup="marginGroup" class="btn btn-default font-size-12 pull-right"><sc:Text id="btnText" field="Add Margin Button Text" runat="server"/></button> 	
														</div>
                                                        <div class="col-md-4 no-padding" id="divWarn" runat="server"><span class="label label-warning"><sc:text id="txtWarning" field="Margin Add Error Text" runat="server" /> <asp:HyperLink ID="linkErrorList" runat="server"></asp:HyperLink></span></div><br/>
                                                        <div class="col-md-4 no-padding" id="divSuccess" runat="server"><span class="label label-success"><sc:text id="txtSuccess" field="Margin Add Success Text" runat="server" /></span></div>
													</div>												
												</div>
											</div> 
										</div>    
									</div>
								</div> 
							</div>   

<div class="col-sm-12">
    <div class="table-responsive">
        <asp:GridView ID="gvSearchData" runat="server" CssClass="table table-striped" EmptyDataText="No Records Found" OnRowCommand="gvSearchData_RowCommand" AutoGenerateEditButton="false" AutoGenerateColumns="false">
            <Columns>
                <asp:BoundField DataField="ProviderName" HeaderText="Provider Name" />
                <asp:BoundField DataField="country" HeaderText="Country" />
                <asp:BoundField DataField="location" HeaderText="Location" />
                <asp:BoundField DataField="mapLocation" HeaderText="Map Location" />
                <asp:BoundField DataField="address" HeaderText="Address" />
                <asp:TemplateField HeaderText="Action">
                    <ItemTemplate>
                        <asp:Button ID="btnEdit" runat="server" CausesValidation="false" CommandName="EditMargin" CssClass="editButton"
                            Text="Edit" CommandArgument=<%# Eval("propertyId")%>/>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>

</div>
		<script type="text/javascript">
		    $(function () {
		        /*--FOR DATE----*/
		        var date = new Date();
		        var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
		        $('.form_date').datetimepicker({
		            weekStart: 1,
		            todayBtn: 1,
		            autoclose: 1,
		            todayHighlight: 1,
		            startView: 2,
		            minView: 2,
		            forceParse: 0,
		            format: 'yyyy-mm-dd',
		            startDate: today
		        });
		    });
		</script>
   <script>
       $(window).on('load', function () {
           target = $('.bedbank-rightpanel').height();
           $('.bedbank-leftpanel').height(target);
       });
   </script>