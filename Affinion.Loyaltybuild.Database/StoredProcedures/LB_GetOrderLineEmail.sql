﻿Create Procedure [dbo].[LB_GetOrderLineEmail]
(
@Email nvarchar(100),
@LastName nvarchar(100)
)
As
Begin
	select ol.orderlineid, ca.ClientId from ucommerce_orderline ol
    inner join LB_RoomReservation rr on ol.OrderLineId = rr.OrderLineId
    inner join ucommerce_purchaseorder po on ol.OrderId = po.OrderId
    inner join uCommerce_Customer c on po.CustomerId = c.CustomerId
	inner join LB_CustomerAttribute ca on c.CustomerId= ca.CustomerId
    where EmailAddress = @Email and LastName=@LastName and  rr.checkindate >= getdate()
End
