﻿namespace Affinion.Loyaltybuild.BedBanks.CAPI.Model
{
    /// <summary>
    /// Summary description for Room
    /// </summary>
    public class Room
    {
        public int HotelId { get; set; }
        public string Description { get; set; }
        public int GuestCount { get; set; }
        public RoomPricing Pricing { get; set; }

        public Room(int pHotelid, string pDescription, int pGuestcount)
        {
            HotelId = pHotelid;
            Description = pDescription;
            GuestCount = pGuestcount;
        }
    }
}