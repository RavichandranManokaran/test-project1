﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SimpleLinkMobileSublayout.ascx.cs
/// Sub-system/Module:     Web.Common(VS project name)
/// Description:           Sublayout for simple link template used for mobile version
/// </summary>

namespace Affinion.LoyaltyBuild.Web.Common.Layouts.SubLayouts.Common
{
    #region Using Directives
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Sitecore.Data.Items;
    using System; 
    #endregion


    /// <summary>
    /// Sublayout for simple link template used for mobile version
    /// </summary>
    public partial class SimpleLinkMobileSublayout : BaseSublayout
    {
        /// <summary>
        /// This function includes the code which needs to run on page load
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The parameter</param>
        private void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Item item = this.GetDataSource();

                if (item != null)
                {
                    this.PlaceholderText.Visible = false;
                    this.Prefix.Text = "<li>";
                    this.Suffix.Text = "</li>";
                    this.TextLink.Item = item;
                }
                else
                {
                    this.TextLink.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, ex.Message);
                throw;
            }
        }
    }
}