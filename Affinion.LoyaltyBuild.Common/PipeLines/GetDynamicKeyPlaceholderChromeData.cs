﻿#region Using Directives
using Affinion.LoyaltyBuild.Common.Exceptions;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Sitecore.Configuration;
using Sitecore.Diagnostics;
using Sitecore.Pipelines.GetChromeData;
using System;
using System.Text.RegularExpressions;

#endregion
///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           GetDynamicKeyPlaceholderChromeData.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Override placeholder chrome data Pipeline
/// </summary>
namespace Affinion.LoyaltyBuild.Common.PipeLines
{
    public class GetDynamicKeyPlaceholderChromeData : GetPlaceholderChromeData
    {
        #region Public Methods

        /// <summary>
        /// Overrides the process method to get placeholder positioning data for the dynamic placeholders
        /// </summary>
        /// <param name="args">The arguments</param>
        public override void Process(GetChromeDataArgs args)
        {
            try
            {
                //base.Process(args);
                Assert.ArgumentNotNull(args, "args");
                Assert.IsNotNull(args.ChromeData, "Chrome Data");

                if ("placeholder".Equals(args.ChromeType, StringComparison.OrdinalIgnoreCase))
                {
                    string placeholderKey = args.CustomData["placeHolderKey"] as string;
                    string placeholderPattern = Settings.GetSetting("DynamicPlaceholderPattern");
                    var regex = new Regex("_Dynamic_");
                    var match = regex.Match(placeholderKey);

                    if (match.Success && match.Groups.Count > 0)
                    {
                        string newPlaceholderKey = match.Groups[0].Value;
                        args.CustomData["placeHolderKey"] = newPlaceholderKey;
                        base.Process(args);
                        args.CustomData["placeHolderKey"] = placeholderKey;
                    }
                    else
                    {
                        base.Process(args);
                    }
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, new object());
                throw;
            }
        } 

        #endregion
    }
}
