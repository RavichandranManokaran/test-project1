﻿
CREATE PROCEDURE [dbo].[OLD_GetDeleteDue]
@orderlinedueid INT

AS
BEGIN

UPDATE duetypes
set duetypes.CanBeDeleted=1
from LB_DueTypes duetypes
inner join LB_OrderLineDue orderlinedue
on duetypes.DueTypeId=orderlinedue.DueTypeId
where orderlinedue.OrderLineDueId=@orderlinedueid


UPDATE orderlinedue
set orderlinedue.IsDeleted=1
from LB_OrderLineDue orderlinedue
inner join LB_DueTypes duetypes
on duetypes.DueTypeId=orderlinedue.DueTypeId
where orderlinedue.OrderLineDueId=@orderlinedueid

END

