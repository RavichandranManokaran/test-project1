﻿namespace Affinion.LoyaltyBuild.Common.Search
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Stores data related to affinion product price
    /// </summary>
    public class ProductPrice
    {
        public string ClientId { get; set; }

        public string OccupancyTypeId { get; set; }

        public decimal Price { get; set; }
    }
}
