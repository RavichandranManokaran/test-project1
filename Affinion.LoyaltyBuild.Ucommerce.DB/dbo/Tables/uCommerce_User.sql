﻿CREATE TABLE [dbo].[uCommerce_User] (
    [UserId]     INT            IDENTITY (1, 1) NOT NULL,
    [ExternalId] NVARCHAR (255) NULL,
    CONSTRAINT [uCommerce_PK_User] PRIMARY KEY CLUSTERED ([UserId] ASC),
    CONSTRAINT [Unique_User_External_Id] UNIQUE NONCLUSTERED ([ExternalId] ASC)
);

