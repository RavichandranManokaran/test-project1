﻿namespace Affinion.LoyaltyBuild.AdminPortal.ClientSetup
{
    #region Using Directives
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using Sitecore.SecurityModel;
    using System;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.ClientSetup.ClientSetup;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using System.Web;
    using System.Text.RegularExpressions;
    using Affinion.Loyaltybuild.Security;
    using Sitecore;
    using Sitecore.Security;
    using System.Linq;
    using Sitecore.Security.AccessControl;
    using LoyaltyBuild = Affinion.LoyaltyBuild.Common;
    #endregion

    /// <summary>
    /// Web forms class for creating a new client
    /// </summary>
    public partial class NewClient : System.Web.UI.Page
    {
        #region Constant variable

        private const string contentRoot = "/sitecore/content";
        private const string adminPortalFolderName = "admin-portal";
        private const string clientSetupFolderName = "client-setup";
        private const string clientPortalFolderName = "client-portal";
        private const string clientPortalTemplatePath = "/sitecore/templates/Branches/Affinion/ClientPortalBranchTemplate";

        private const string clientSetupTemplatePath = "/sitecore/templates/Branches/Affinion/ClientSetup";
        private const string folderTemplate = "/sitecore/templates/Affinion/Common/RootFolder";
        private const string clientPortalTemplatesPath = "/sitecore/templates/Affinion/ClientPortal";
        private const string clientPortalSublayoutsPath = "/sitecore/layout/Sublayouts/Affinion/ClientPortal";

        //private const string clientPortalBranchTemplatesPath = "/sitecore/templates/Branches/Affinion/PortalLayoutBranch";
        //private const string clientPortalStyleBranchTemplatesPath = "/sitecore/templates/Branches/Affinion/ParameterTemplate";


        #endregion

        /// <summary>
        /// Code to execute on page load
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The parameter</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var user = Sitecore.Context.User;

                if (!IsAuthorized(user))
                {
                    /// Unauthorized Access Exception
                    throw new UnauthorizedAccessException();
                }

            }
            catch (UnauthorizedAccessException exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                Response.Redirect(Sitecore.Context.Site.LoginPage);
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                throw;
            }
        }

        /// <summary>
        /// Calls when button clicks and create admin portal and uCommerce store and related information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ButtonCreate_Click(object sender, EventArgs e)
        {

            // call create item function to create new item in sitecore and uCommerce            
            CreateItem();
        }



        /// <summary>
        /// This is use to create item in sitecore and uCommerce end
        /// </summary>
        public void CreateItem()
        {
            try
            {
                LabelValidation.InnerText = string.Empty;

                // local variables
                string clientName = TextBoxClientName.Text.Trim().ToLowerInvariant().Replace(' ', '-');
                string parameterTemplateFolderName = clientName + "ParameterTemplates";
                string parameterTemplateName = clientName + "StockSearchParameters";
                string searchSublayoutName = clientName + "StockSearchSublayout";
                string clientPortalFolderPath = string.Concat(contentRoot, "/", clientPortalFolderName);
                string adminPortalFolderPath = string.Concat(contentRoot, "/", adminPortalFolderName);
                string clientSetupFolderPath = string.Concat(adminPortalFolderPath, "/", clientSetupFolderName);
                // string clientSpecificParameterTemplateFolderPath = string.Concat(clientPortalTemplatesPath + "/" + clientName);
                // string parameterTemplateFolderPath = string.Concat(clientSpecificParameterTemplateFolderPath + "/" + parameterTemplateFolderName);
                // string parameterTemplatePath = string.Concat(parameterTemplateFolderPath + "/" + parameterTemplateName);

                //First get the parent item from the master database
                Database masterDb = Sitecore.Configuration.Factory.GetDatabase("master");
                string clientSetupName = SitecoreItemHelper.SetItemName(clientName.ToLower());
                //string clientFastqueary = "/sitecore/content/#admin-portal#/#client-setup#//*[@@name ='" + clientSetupName + "']";

                Item item = masterDb.GetItem(clientSetupFolderPath);

                var items = item.Children
                 .Where(i => i.Name == clientSetupName)
                 .ToList();

                //Item[] allClients = masterDb.SelectItems(clientFastqueary);

                if (items.Count > 0)
                {
                    LabelValidation.InnerText = "This Client name is not unique!";
                }
                else
                {

                    if (!ValidateCreateClientSetup(clientName))
                        return;

                    //First get the parent item from the master database
                    //Database masterDb = Sitecore.Configuration.Factory.GetDatabase("master");

                    // add clients folder if not exist
                    masterDb.AddItem(contentRoot, folderTemplate, clientPortalFolderName);

                    // Add client portal
                    masterDb.AddItem(clientPortalFolderPath, clientPortalTemplatePath, clientName);

                    // add clients folder if not exist
                    masterDb.AddItem(contentRoot, folderTemplate, adminPortalFolderName);
                    masterDb.AddItem(adminPortalFolderPath, folderTemplate, clientSetupFolderName);

                    // Add client information to admin portal
                    masterDb.AddItem(clientSetupFolderPath, clientSetupTemplatePath, clientName);

                    // Creates uCommerce store
                    //UCommerceInstaller uInstaller = new UCommerceInstaller(clientName, string.Concat(clientName, "Hotels"));
                    //uInstaller.Configure();

                    /* 
                     * Old Code
                    TemplateItem standardFolderTemplate = masterDb.GetItem("{0437FEE2-44C9-46A6-ABE9-28858D9FEE8C}");
                    TemplateItem standardTemplate = masterDb.GetItem("{AB86861A-6030-46C5-B394-E8F99E8B87DB}");
                    TemplateItem standardTemplateSection = masterDb.GetItem("{E269FBB5-3750-427A-9149-7AA950B49301}");
                    TemplateItem standardTemplateField = masterDb.GetItem("{455A3E98-A627-4B40-8035-E683A0331AC7}");
                    TemplateItem sublayoutTemplate = masterDb.GetItem("{0A98E368-CDB9-4E1E-927C-8E0C24A003FB}");

                    //Create Client-specific parameter template and folder for search
                    //Create Client-Specific parameter template folder
                    Item parentItem = masterDb.Items[clientPortalTemplatesPath];
                    //parentItem.Add(clientName, standardFolderTemplate);

                    //create parameter template folder
                    parentItem = masterDb.Items[clientSpecificParameterTemplateFolderPath];
                    parentItem.Add(parameterTemplateFolderName, standardFolderTemplate);

                    //crete parameter template
                    parentItem = masterDb.Items[parameterTemplateFolderPath];
                    parentItem.Add(parameterTemplateName, standardTemplate);

                    //add section to template
                    parentItem = masterDb.Items[parameterTemplatePath];
                    parentItem.Add("Configuration", standardTemplateSection);

                    //add field to template
                    parentItem = masterDb.Items[parameterTemplatePath + "/Configuration"];
                    parentItem.Add("CssClass", standardTemplateField);

                    //Create Client-specific Sublayout for search
                    //Create Client-Specific parameter template folder
                    parentItem = masterDb.Items[clientPortalSublayoutsPath];
                    parentItem.Add(clientName, standardFolderTemplate);

                    //Create Client-specific sublayout
                    //parentItem = masterDb.Items[clientPortalSublayoutsPath+"/"+clientName];
                    parentItem.Add(searchSublayoutName, sublayoutTemplate);

                    */

                    // get the folder path: /sitecore/layout/Sublayouts/Affinion/ClientPortal

                    // masterDb.AddItem(clientPortalSublayoutsPath, clientPortalBranchTemplatesPath, clientName);
                    // masterDb.AddItem(clientPortalTemplatesPath, clientPortalStyleBranchTemplatesPath, clientName);


                    Response.Redirect("/sitecore/shell/sitecore/client/Applications/Launchpad/");
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                throw;
            }
        }

        private bool ValidateCreateClientSetup(string clientName)
        {
            // Validation for white spaces
            if (clientName.Length == 0)
            {
                LabelValidation.InnerText = "Client name is required!";
                return false;
            }

            // Validation which allows only alphanumeric and space
            Regex regex = new Regex(@"^[a-zA-Z0-9\s\-\+\(\)\&\w]*$");
            if (!regex.IsMatch(clientName))
            {
                LabelValidation.InnerText = LanguageReader.GetText("SpecialCharactorValidationErrorMessage", @"Special characters except '&', '(', ')', '+', '-' are not allowed. Please enter a valid name!");
                return false;
            }
            return true;
        }

        /// <summary>
        /// User Authentication
        /// </summary>
        /// <param name="user">Context User</param>
        /// <returns>Returns true if authenticated false otherwise</returns>
        private static bool IsAuthorized(Sitecore.Security.Accounts.User user)
        {
            /// Check if user authorized to access this page
            return SitecoreAccess.IsAllowed(user, AccessRight.ItemRead, LoyaltyBuild.Constants.ClientSetupButtonPath, "core");

        }
    }
}