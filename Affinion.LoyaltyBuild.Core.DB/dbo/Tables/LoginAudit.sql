﻿CREATE TABLE [dbo].[LoginAudit] (
    [ID]            INT            IDENTITY (1, 1) NOT NULL,
    [UserName]      NVARCHAR (128) NOT NULL,
    [IsLoginSucess] BIT            NOT NULL,
    [LoggedInDate]  DATETIME       NOT NULL,
    CONSTRAINT [PK_dbo.LoginAudit] PRIMARY KEY CLUSTERED ([ID] ASC)
);

