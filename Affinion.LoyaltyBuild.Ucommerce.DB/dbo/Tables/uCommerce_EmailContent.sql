﻿CREATE TABLE [dbo].[uCommerce_EmailContent] (
    [EmailContentId] INT            IDENTITY (1, 1) NOT NULL,
    [EmailProfileId] INT            NOT NULL,
    [EmailTypeId]    INT            NOT NULL,
    [CultureCode]    NVARCHAR (50)  NOT NULL,
    [Subject]        NTEXT          NULL,
    [ContentId]      NVARCHAR (255) NULL,
    CONSTRAINT [uCommerce_PK_EmailContent] PRIMARY KEY CLUSTERED ([EmailContentId] ASC),
    CONSTRAINT [uCommerce_FK_EmailContent_EmailProfile] FOREIGN KEY ([EmailProfileId]) REFERENCES [dbo].[uCommerce_EmailProfile] ([EmailProfileId]),
    CONSTRAINT [uCommerce_FK_EmailContent_EmailType] FOREIGN KEY ([EmailTypeId]) REFERENCES [dbo].[uCommerce_EmailType] ([EmailTypeId])
);

