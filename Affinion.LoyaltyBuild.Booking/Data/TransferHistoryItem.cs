﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/
namespace Affinion.LoyaltyBuild.Api.Booking.Data
{
    /// <summary>
    /// Transfer history Item
    /// </summary>
    public class TransferHistoryItem
    {
        /// <summary>
        /// Orderline id
        /// </summary>
        public int OrderLineId { get; set; }

        /// <summary>
        /// Supplier / Provider Name
        /// </summary>
        public string SupplierName { get; set; }

        /// <summary>
        /// Check in / Arrival date
        /// </summary>
        public DateTime ArrivalDate { get; set; }

        /// <summary>
        /// Check out / Departure date
        /// </summary>
        public DateTime DepartureDate { get; set; }

        /// <summary>
        /// Number of Rooms
        /// </summary>
        public int NoOfRooms { get; set; }

        /// <summary>
        /// Number of Nights
        /// </summary>
        public int NoOfNights { get; set; }

        /// <summary>
        /// Number of People
        /// </summary>
        public int NoOfPeople { get; set; }

        /// <summary>
        /// Reservation Date
        /// </summary>
        public DateTime ReservationDate { get; set; }

        /// <summary>
        /// ProviderId 
        /// </summary>
        public string ProviderId { get; set; }
    }
}
