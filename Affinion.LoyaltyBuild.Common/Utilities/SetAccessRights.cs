﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SetAccessRights.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Used to set delete permison to specific role
/// </summary>
#region Using directives
using Sitecore.Data.Items;
using Sitecore.Security.AccessControl;
using Sitecore.Security.Accounts; 
#endregion

namespace Affinion.LoyaltyBuild.Common.Utilities
{
    public static class SetAccessRights
    {
        #region public method
        public static void CheckDeleteAccess(string itemPath)
        {
            bool DeleteAccess = false;

            Sitecore.Data.Database masterDB = Sitecore.Configuration.Factory.GetDatabase("master");
            Item item = masterDB.GetItem(itemPath);

            if (item != null)
            {

                //Role userRole = Role.FromName(domainUser);
                Role userRole = Role.FromName("sitecore\\Supply Team");

                DeleteAccess = item.Access.CanDelete();

                if (DeleteAccess)
                {
                    AccessRuleCollection accessRules = item.Security.GetAccessRules();

                    // Apply Delete access for the "userRole" to only current item's all children                    
                    accessRules.Helper.AddAccessPermission(userRole,
                       AccessRight.ItemDelete,
                       PropagationType.Descendants,
                       AccessPermission.Allow);

                    // Write the rules back to the item
                    item.Editing.BeginEdit();
                    item.Security.SetAccessRules(accessRules);
                    item.Editing.EndEdit();
                }
            }
        } 
        #endregion
        
    }
}
