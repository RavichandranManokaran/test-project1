﻿using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.EmailScheduler
{
    
    public class LastDayBookingReport
    {
        private const string FilteredClientItems = @"fast:/sitecore/content/#admin-portal#/#client-setup#/*[@@templatename='ClientDetails' and @Active='1']";
        public string itemData { get; set; }
        public void Run()
        {
            itemData = "{1D9D1EAF-1AB2-45A8-B582-072B97F9A8A6}";
            int Minute, Hour; 
            int.TryParse(DateTime.Now.Minute.ToString(), out Minute);
            int.TryParse(DateTime.Now.Hour.ToString(), out Hour);
            if (Minute >= 0 && Minute < 10 && Hour==8)
            { 
            Sitecore.Data.Database db = Sitecore.Data.Database.GetDatabase("web");
            Item Data = db.GetItem(Sitecore.Data.ID.Parse(itemData));
            if (Data != null)
            {
                string dReceiverEmail = !string.IsNullOrWhiteSpace(Data.Fields["Default Receiver Email Address"].ToString()) ? Data.Fields["Default Receiver Email Address"].ToString() : string.Empty;
                mailGenarate(dReceiverEmail, Data);
            }
            }
        }
        private void mailGenarate(string receiverId, Item details)
        {
            try
            {
                Affinion.LoyaltyBuild.Communications.IEmailService emailService = new Affinion.LoyaltyBuild.Communications.Services.EmailService();
                string senderId = SitecoreFieldsHelper.GetValue(details, "Sender Email Address");
                if (string.IsNullOrEmpty(senderId))
                    return;
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("LastDayBookingReport: senderId-{0}", senderId));
                string mailSubject = !string.IsNullOrWhiteSpace(SitecoreFieldsHelper.GetValue(details, "Email Subject"))?SitecoreFieldsHelper.GetValue(details, "Email Subject"):string.Empty;
                string msgBodyTemplate = !string.IsNullOrWhiteSpace(SitecoreFieldsHelper.GetValue(details, "Email Body"))?SitecoreFieldsHelper.GetValue(details, "Email Body"):string.Empty;
                string multiEmail = SitecoreFieldsHelper.GetValue(details, "Other Receivers Email Address");
                List<string> ccEmail=null;
                if (!string.IsNullOrWhiteSpace(multiEmail))
                {
                    ccEmail = multiEmail.TrimEnd(';').Split(';').Select(x => x.Trim()).ToList<string>();
                }
                string msgBody = GetMailText(msgBodyTemplate);
                if (ccEmail != null)
                {
                    emailService.Send(senderId, receiverId, ccEmail, mailSubject, msgBody, true);
                }
                else
                {
                    emailService.Send(senderId, receiverId, mailSubject, msgBody, true);
                }
            }
            catch (Exception exception)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("LastDayBookingReport: Exception-{0}", exception.ToString()));
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
            }
        }

        public string GetMailText(string bodyText)
        {
            bodyText = bodyText.Replace("{currentTime}", DateTime.Now.AddMinutes(-DateTime.Now.Minute).AddSeconds(-DateTime.Now.Second).ToString("HH:mm:ss"));
            bodyText = bodyText.Replace("{table}", EmailBodyData());
            return bodyText;
        }

        public string EmailBodyData()
        {
            DataSet ds = uCommerceConnectionFactory.PrevDayBookingData();
            StringBuilder html = new StringBuilder();
            DataTable dt = ds.Tables[0];
            //Table start.
            html.Append("<table border = '1'>");

            //Building the Header row.
            html.Append("<tr>");
            foreach (DataColumn column in dt.Columns)
            {
                html.Append("<th style='width: 141.9pt; border: 1pt solid windowtext; background: yellow none repeat scroll 0% 0%; padding: 0in 5.4pt; height: 38.35pt;'>");
                html.Append(column.ColumnName);
                html.Append("</th>");
            }
            html.Append("<th style='width: 141.9pt; border: 1pt solid windowtext; background: yellow none repeat scroll 0% 0%; padding: 0in 5.4pt; height: 38.35pt;'>Budget</th><th style='width: 141.9pt; border: 1pt solid windowtext; background: yellow none repeat scroll 0% 0%; padding: 0in 5.4pt; height: 38.35pt;'>% of Budget</th>");    //TODO:Remove when Budget data is available
            html.Append("</tr>");

            Sitecore.Data.Database db = Sitecore.Data.Database.GetDatabase("web");
            Item[] clients = db.SelectItems(FilteredClientItems);
            //Building the Data rows.
            foreach (var client in clients)
            {
                if (dt.Rows.Count > 0)
                {
                     foreach (DataRow row in dt.Rows)
                    {
                        html.Append("<tr>");
                        foreach (DataColumn column in dt.Columns)
                        {
                            if (client.ID.ToString().Equals(row[column.ColumnName].ToString(), StringComparison.InvariantCultureIgnoreCase))
                            {
                                html.Append("<td>");
                                html.Append(client.DisplayName);
                                html.Append("</td>");
                            }
                            else
                            {   html.Append("<td>");
                                html.Append(row[column.ColumnName].ToString());
                                html.Append("</td>");
                            }
                        }
                        html.Append("<td>Budget not set</td><td>N/A</td>"); //TODO: Remove when Budget Data is Available
                        html.Append("</tr>");
                    }
                }
                else
                {
                    html.Append("<td>");
                    html.Append(client.DisplayName);
                    html.Append("</td>");
                    html.Append("<td>0</td>");
                    html.Append("<td>Budget not set</td><td>N/A</td>"); //TODO: Remove when Budget Data is Available
                    html.Append("</tr>");
                }
            }
            //Table end.
            html.Append("</table>");
            return html.ToString();
        }
    }
}