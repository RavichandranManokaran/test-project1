

/****** Object:  StoredProcedure [dbo].[LB_BB_getBedBankPropertyMargin]    Script Date: 7/19/2016 7:24:39 PM ******/

---- ====================================================
---- Description: RETURNS MARGIN FOR A PROPERTY BASED ON AVAILABILITY SOURCE
---- Written: RICHARD
---- Created: 2016.04.19
---- ====================================================
CREATE PROCEDURE [dbo].[LB_BB_getBedBankPropertyMargin]
      
      -- Input parameters 
      @pi_propertyid int = 68001,
      @pi_arrivaldate int = 0,
      @pi_departuredate int = 0,
      @pi_availabilityprovidersourceid int = 1,
      
      --Return parameters
      
      @po_returnstatus INT OUTPUT,
      @po_returnmsg VARCHAR(200) OUTPUT,
      @po_margin MONEY OUTPUT,
      @po_propertyMarginID INT OUTPUT
      
AS
set nocount on;
BEGIN

      --Assume transaction will fail, set the return variable to -1.
      SET @po_returnstatus = -1;
      SET @po_returnmsg = null;

      
      
      BEGIN TRY
      SELECT @po_margin = isnull(max(MarginPercentage),0) 
      FROM dbo.LB_BB_provider
      INNER JOIN LB_BB_providerMargin ON LB_BB_provider.ID = LB_BB_providerMargin._providerid
      WHERE availabilityProviderID = @pi_availabilityprovidersourceid
      AND availabilityProviderRecordID = @pi_propertyid     
      AND DATEFROM <= @pi_arrivaldate and Dateto >= @pi_departuredate
      AND isactive = 1
      
      SELECT @po_propertyMarginID = ISNULL(LB_BB_providerMargin.ID,0) FROM dbo.LB_BB_provider
      INNER JOIN LB_BB_providerMargin ON LB_BB_provider.ID = LB_BB_providerMargin._providerid
      WHERE availabilityProviderID = @pi_availabilityprovidersourceid
      AND availabilityProviderRecordID = @pi_propertyid     
      AND DATEFROM <= @pi_arrivaldate and Dateto >= @pi_departuredate
      AND isactive = 1
      --AND LB_BB_providerMargin.marginPercentage = @po_margin
      
      IF @po_margin = 0
      BEGIN
      SELECT @po_margin = Value from dbo.LB_BB_config where ID = 1
      END
      
    END TRY 
      BEGIN CATCH   
            SET @po_returnstatus = error_number();
            SET @po_returnmsg = error_message();
            RETURN;
      END CATCH
      
      SET @po_returnstatus = 0;
      SET @po_returnmsg = 'Completed successfully.';  
END








 
	
		







GO

