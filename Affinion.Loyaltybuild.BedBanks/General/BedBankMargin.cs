﻿namespace Affinion.LoyaltyBuild.BedBanks.General
{
    public class BedBankMargin
    {
        public int PropertyId { get; set; }
        public int MarginId { get; set; }
        public decimal MarginPercentage { get; set; }
        public int DateFromCounter { get; set; }
        public int DateToCounter { get; set; }
    }
}
