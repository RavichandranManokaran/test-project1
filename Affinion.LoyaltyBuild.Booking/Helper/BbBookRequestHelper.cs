﻿using Affinion.LoyaltyBuild.Api.Booking;
using Affinion.LoyaltyBuild.Api.Booking.Data;
using Affinion.LoyaltyBuild.BedBanks.General;
using Affinion.LoyaltyBuild.BedBanks.Helper;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Message;
using Affinion.LoyaltyBuild.BedBanks.Troika;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
using Affinion.LoyaltyBuild.Model.Product;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using data = Affinion.LoyaltyBuild.Api.Search.Data;

namespace Affinion.LoyaltyBuild.Api.Booking.Helper
{
    public class BedBanksBookRequestData
    {
        public string PropertyId { get; set; }
        public string PreBookingToken { get; set; }
        public string ArrivalDate { get; set; }
        public string Duration { get; set; }
        public string GuestTitle { get; set; }
        public string GuestFirstName { get; set; }
        public string GuestLastName { get; set; }
        public string Address { get; set; }
        public string PostCode { get; set; }
        public string BookingCountryId { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string SpecialRequest { get; set; }
        public string RoomToken { get; set; }
        public bool IsDirect { get; set; }
        public string MealBasisId { get; set; }
        public string RoomInfo { get; set; }
        public string RoomType { get; set; }
        public decimal WholeSaleAmount { get; set; }
        public int MarginIdApplied { get; set; }
    }

    public class BbBookRequestHelper
    {
        string address1 = string.Empty;
        string address2 = string.Empty;
        string address3 = string.Empty;
        string phone = string.Empty;
        public List<JtBookResponse> GetBedBanksBookResponse(int orderId)
        {
            Affinion.LoyaltyBuild.Api.Booking.Service.BookingService bs = new Api.Booking.Service.BookingService();
            Affinion.LoyaltyBuild.Api.Booking.Data.BasketInfo basket = bs.GetPurchaseOrder(orderId);
            var bedBanksRooms = basket.Bookings.Where(x => x.Type == ProductType.BedBanks);
            List<JtBookResponse> book = new List<JtBookResponse>();
            //Item clientItem = SitecoreFieldsHelper.GetLookupFieldTargetItem(ItemHelper.RootItem, "ClientDetails");
            if (bedBanksRooms.Count() > 0)
            {
                Item clientItem = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(bedBanksRooms.First().ClientId));

                BedBanksSettings bbs = new BedBanksSettings(clientItem);
                string currId = string.Empty;
                JtBookResponse roomBooked = null;
                foreach (BedBanksBooking booking in bedBanksRooms)
                {
                    if (booking != null)
                    {
                        currId = booking.OrderLine.GetOrderProperty(Constants.BBCurrencyId).Value;
                        BedBanksCurrency actualCurr = null;
                        if (!string.IsNullOrEmpty(currId))
                        {
                            actualCurr = new BedBanksCurrency();
                            actualCurr = ReverseMapCurrency(currId);
                        }
                        if (actualCurr != null)
                            bbs.BedBanksActualCurrency = actualCurr;
                        int lbOrderLineId = booking.OrderLineId;
                        JacTravelApiWrapper jac = new JacTravelApiWrapper();
                        BedBanksBookRequestData bookRequestData = CreateBookRequestData(booking);
                        if (bookRequestData != null)
                        {
                            roomBooked = new JtBookResponse();
                            roomBooked = jac.Book(bookRequestData.PropertyId, bookRequestData.PreBookingToken, bookRequestData.ArrivalDate, bookRequestData.Duration, bookRequestData.GuestTitle, bookRequestData.GuestFirstName, bookRequestData.GuestLastName, bookRequestData.Address, bookRequestData.PostCode, bookRequestData.BookingCountryId, bookRequestData.Phone, bookRequestData.Email, bookRequestData.Email, bookRequestData.RoomToken, bookRequestData.IsDirect, bookRequestData.MealBasisId, bookRequestData.RoomInfo, bookRequestData.RoomType, bookRequestData.WholeSaleAmount, bookRequestData.MarginIdApplied, lbOrderLineId, bbs);
                        }
                        if (roomBooked != null)
                        {
                            string bookingReference = string.IsNullOrEmpty(booking.OrderLine.GetOrderProperty("_BookingReference").Value) ? string.Empty : booking.OrderLine.GetOrderProperty("_BookingReference").Value;
                            string marginPercent = string.IsNullOrEmpty(booking.OrderLine.GetOrderProperty("MarginPercentage").Value) ? string.Empty : booking.OrderLine.GetOrderProperty("MarginPercentage").Value;
                            decimal margin = 0;
                            decimal.TryParse(marginPercent, out margin);
                            decimal marginAmt = bookRequestData.WholeSaleAmount * (margin / 100);
                            if (roomBooked.ReturnStatus.Success.ToLower().Equals("true"))
                            {
                                TroikaDataController.InsertBedBankBookingInfo(
                                    roomBooked.BookingReference,
                                    1,
                                    bookRequestData.PropertyId,
                                    bookRequestData.RoomType,
                                    bookRequestData.MealBasisId,
                                    booking.OrderLineId,
                                    bookRequestData.WholeSaleAmount,
                                    bookRequestData.MarginIdApplied,
                                    bookingReference,
                                    marginAmt,
                                    margin
                                    );
                                booking.OrderLine[Constants.BBBookingReferenceId] = string.IsNullOrEmpty(roomBooked.BookingReference) ? string.Empty : roomBooked.BookingReference;
                            }
                            book.Add(roomBooked);
                        }
                        booking.Save();
                    }
                }
            }
            return book;
        }

        public BedBanksBookRequestData CreateBookRequestData(BedBanksBooking bookingDetails)
        {
            BedBanksBookRequestData bookRequestData = new BedBanksBookRequestData();
            if (bookingDetails != null)
            {
                string clientId = bookingDetails.ClientId;
                string email = string.IsNullOrEmpty(bookingDetails.OrderLine.GetOrderProperty(Constants.LeadGuestEmail).Value) ? string.Empty : bookingDetails.OrderLine.GetOrderProperty(Constants.LeadGuestEmail).Value;
                if (!string.IsNullOrEmpty(email))
                {
                    LoadExistingCustomerInfromation(email, clientId);
                }
                string noOfRooms = bookingDetails.OrderLine.GetOrderProperty("_NoOfRooms").Value;
                string childAges = bookingDetails.OrderLine.GetOrderProperty("_ChildrenAge").Value;
                string bookingToken = bookingDetails.OrderLine.GetOrderProperty("RoomToken").Value;
                string stock = bookingDetails.OrderLine.GetOrderProperty("IsDirect").Value;
                string guestTitle = string.IsNullOrEmpty(bookingDetails.OrderLine.GetOrderProperty(Constants.LeadGuestTitle).Value) ? string.Empty : bookingDetails.OrderLine.GetOrderProperty(Constants.LeadGuestTitle).Value;
                string guestFirstName = string.IsNullOrEmpty(bookingDetails.OrderLine.GetOrderProperty(Constants.LeadGuestFirstName).Value) ? string.Empty : bookingDetails.OrderLine.GetOrderProperty(Constants.LeadGuestFirstName).Value;
                string guestLastName = string.IsNullOrEmpty(bookingDetails.OrderLine.GetOrderProperty(Constants.LeadGuestLastName).Value) ? string.Empty : bookingDetails.OrderLine.GetOrderProperty(Constants.LeadGuestLastName).Value;
                string request = string.IsNullOrEmpty(bookingDetails.OrderLine.GetOrderProperty(Constants.Request).Value) ? string.Empty : bookingDetails.OrderLine.GetOrderProperty(Constants.Request).Value;
                bool isDirect = true;
                bool.TryParse(stock, out isDirect);
                DateTime arrivalDate;
                arrivalDate=DateTime.ParseExact(bookingDetails.ArrivalDate, "dd/MM/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture);
                string id = bookingDetails.OrderLine.GetOrderProperty("MarginId").Value;
                int marginId = 0;
                int.TryParse(id, out marginId);
                bookRequestData.PropertyId = bookingDetails.PropertyId;
                bookRequestData.PreBookingToken = bookingDetails.RoomToken; // BookingToken from Search response
                bookRequestData.ArrivalDate = arrivalDate.ToString("yyyy-MM-dd");
                bookRequestData.Duration = bookingDetails.Duration;
                bookRequestData.GuestTitle = guestTitle;
                bookRequestData.GuestFirstName = guestFirstName;
                bookRequestData.GuestLastName = guestLastName;
                bookRequestData.Address = address1+", "+address2;
                bookRequestData.PostCode = address3;
                bookRequestData.BookingCountryId = string.Empty;
                bookRequestData.Phone = phone;
                bookRequestData.Email = email;
                bookRequestData.SpecialRequest = request; 
                bookRequestData.RoomToken = bookingDetails.PreBookingToken; //PreBook token from pre book response
                bookRequestData.IsDirect = isDirect;
                bookRequestData.MealBasisId = bookingDetails.MealBasisId;
                bookRequestData.RoomInfo = GetRoomInfo(noOfRooms, bookingDetails.NoOfAdults.ToString(), bookingDetails.NoOfChildren.ToString(), childAges); //Create Comma seperated
                bookRequestData.RoomType = bookingDetails.RoomInfo;
                bookRequestData.WholeSaleAmount = bookingDetails.Price;
                bookRequestData.MarginIdApplied = marginId;
            }
            return bookRequestData;
        }

        private static string GetRoomInfo(string pNoOfRooms, string pNoOfAdults, string pNoOfChildren, string pChildAge)
        {
            if (int.Parse(pNoOfRooms) > 0)
            {
                int noOfRooms = 0;
                int noOfAdults = 0;
                int noOfChildren = 0;
                int.TryParse(pNoOfRooms, out noOfRooms);
                int.TryParse(pNoOfAdults, out noOfAdults);
                int.TryParse(pNoOfChildren, out noOfChildren);
                string[] childAge = !string.IsNullOrEmpty(pChildAge) ? pChildAge.Split(',').ToArray() : null;
                var roomInfo = new List<string>();
                int childCount = 0;

                if (noOfRooms > 0 && noOfAdults > 0)
                {
                    for (int roomCount = noOfRooms; roomCount > 0; roomCount--)
                    {
                        int roomAdultValue;
                        int.TryParse(Math.Ceiling(decimal.Parse((noOfAdults / roomCount).ToString())).ToString(), out roomAdultValue);
                        int roomChildValue = 0;
                        string childAges = string.Empty;

                        if (noOfChildren > 0 && childAge != null)
                        {
                            int.TryParse(Math.Ceiling(decimal.Parse((noOfChildren / roomCount).ToString())).ToString(), out roomChildValue);
                            childAges = "," + string.Join("/", childAge.Skip(childCount).Take(roomChildValue));
                            childCount += roomChildValue;
                        }
                        roomInfo.Add(string.Format("{0},{1},0{2}", roomAdultValue.ToString(), roomChildValue.ToString(), childAges));

                        noOfAdults = noOfAdults - roomAdultValue;
                        noOfChildren = noOfChildren - roomChildValue;
                    }
                }
                return string.Join("|", roomInfo);
            }
            return null;
        }

        /// <summary>
        /// LoadExistingCustomerInfromation
        /// </summary>
        /// <param name="bookingReferenceId">bookingReferenceId</param>
        private void LoadExistingCustomerInfromation(string email, string clientId)
        {
            if (string.IsNullOrEmpty(email))
                return;

            bool isExistingCustomer = uCommerceConnectionFactory.IsExistingCustomer(clientId, email);

            if (isExistingCustomer)
            {
                Affinion.LoyaltyBuild.DataAccess.Models.Customer existingCustomer = uCommerceConnectionFactory.GetCustomer(clientId, email);
                address1 = existingCustomer.AddressLine1;
                address2 = existingCustomer.AddressLine2;
                address3 = existingCustomer.AddressLine3;
                phone = existingCustomer.Phone;
            }
        }

        // <summary>
        /// Reverse map currency
        /// </summary>
        /// <param name="properties"></param>
        /// <returns></returns>
        private static BedBanksCurrency ReverseMapCurrency(string currId)
        {
            string currencyId = currId;
            string itemPath = Constants.CurrItemPath;
            Item[] currItems = Sitecore.Context.Database.SelectItems(itemPath);
            Item currItem = currItems.Where(x => x.Fields["Bed Banks Currency Id"].Value.Equals(currencyId)).Select(j => (Item)j).FirstOrDefault();
            BedBanksCurrency curr = new BedBanksCurrency();
            curr.BedBanksCurrencyId = currencyId;
            curr.CurrencyName = SitecoreFieldsHelper.GetItemFieldValue(currItem, "Currency Name");
            curr.uCommerceCurrencyId = SitecoreFieldsHelper.GetItemFieldValue(currItem, "uCommerce Currency Id");

            return curr;
        }
    }
}