﻿CREATE PROCEDURE [dbo].[LB_AddListandSaveHotel](@CustomerId Int, @HotelId VarChar(50), @ListName VarChar(50), @date datetime)
as
declare
@returnId int
begin
INSERT INTO [dbo].[LB_FavoriteList]
     VALUES
           ((select CustomerID from uCommerce_Customer where CustomerId=@CustomerId),
			@ListName, @HotelId,
			(select FirstName from uCommerce_Customer where CustomerId=@CustomerId),
     	    (select FirstName from uCommerce_Customer where CustomerId=@CustomerId),
			 @date,
		     @date)

select @returnId=count(ListID) from LB_FavoriteList

return @returnId

end
