﻿CREATE TABLE [dbo].[uCommerce_OrderStatusAudit] (
    [OrderStatusAuditId] INT            IDENTITY (1, 1) NOT NULL,
    [NewOrderStatusId]   INT            NOT NULL,
    [CreatedOn]          DATETIME       NOT NULL,
    [CreatedBy]          NVARCHAR (50)  NULL,
    [OrderId]            INT            NOT NULL,
    [Message]            NVARCHAR (MAX) NULL,
    CONSTRAINT [uCommerce_PK_OrderStatusAudit] PRIMARY KEY CLUSTERED ([OrderStatusAuditId] ASC),
    CONSTRAINT [uCommerce_FK_OrderStatusAudit_OrderStatus] FOREIGN KEY ([NewOrderStatusId]) REFERENCES [dbo].[uCommerce_OrderStatus] ([OrderStatusId]),
    CONSTRAINT [uCommerce_FK_OrderStatusAudit_PurchaseOrder] FOREIGN KEY ([OrderId]) REFERENCES [dbo].[uCommerce_PurchaseOrder] ([OrderId])
);


GO
CREATE NONCLUSTERED INDEX [IX_uCommerce_OrderStatusAudit_OrderId]
    ON [dbo].[uCommerce_OrderStatusAudit]([OrderId] ASC)
    INCLUDE([OrderStatusAuditId], [NewOrderStatusId], [CreatedOn], [CreatedBy], [Message]);

