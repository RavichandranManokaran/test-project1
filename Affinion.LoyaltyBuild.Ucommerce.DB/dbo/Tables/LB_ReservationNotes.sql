﻿CREATE TABLE [dbo].[LB_ReservationNotes] (
    [NotesId]     INT            IDENTITY (1, 1) NOT NULL,
    [NoteTypeId]  INT            NOT NULL,
    [OrderLineId] INT            NOT NULL,
    [BookingId]    UNIQUEIDENTIFIER   NOT NULL,
    [Note]        NVARCHAR (MAX) NOT NULL,
    [CreatedDate] DATETIME       DEFAULT (getdate()) NOT NULL,
    [CreatedBy]   NVARCHAR (50)  NULL,
	[SpecialRequest] [varchar](150) NULL,
    PRIMARY KEY CLUSTERED ([NotesId] ASC),
    CONSTRAINT [FK_uCommerce_ReservationNotes_NoteTypes] FOREIGN KEY ([NoteTypeId]) REFERENCES [dbo].[LB_NoteTypes] ([NoteTypeId]),
    CONSTRAINT [FK_uCommerce_ReservationNotes_OrderLine] FOREIGN KEY ([OrderLineId]) REFERENCES [dbo].[uCommerce_OrderLine] ([OrderLineId]) ON DELETE CASCADE
);

