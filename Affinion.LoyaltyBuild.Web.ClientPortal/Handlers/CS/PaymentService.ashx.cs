﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic;
using Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using service = Affinion.LoyaltyBuild.Api.Booking.Service;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Handlers.CS
{
    /// <summary>
    /// Summary description for PaymentService
    /// </summary>
    public class PaymentService : IHttpHandler, IRequiresSessionState
    {
        private NameValueCollection _query = null;
        private service.IPaymentService _paymentService;

        public void ProcessRequest(HttpContext context)
        {
            _query = HttpUtility.ParseQueryString( context.Request.Url.Query.ToLower());
            _paymentService = ContainerFactory.Instance.GetInstance<service.IPaymentService>();

            switch (_query["action"])
            {
                case "deletedue":
                    DeleteDue(context);
                    break;
                case "refund":
                    Refund(context);
                    break;
                case "getpaymenttypehistory":
                    break;
                case "getduebycurrencies":
                    GetPaymentByDues(context);
                    break;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }


        private void Refund(HttpContext context)
        {
            var serializer = new JavaScriptSerializer();

            context.Response.ContentType = "text/json";
            var data = serializer.Deserialize<Affinion.LoyaltyBuild.Api.Booking.Data.Payment.CardPaymentDetail>(GetDocumentContents(context.Request));
            context.Response.Write(serializer.Serialize(_paymentService.AddPayment(data)));
        }

        private void DeleteDue(HttpContext context)
        {
           
            var serializer = new JavaScriptSerializer();
            
            context.Response.ContentType = "text/json";
            var data = serializer.Deserialize<DueDelete>(GetDocumentContents(context.Request));
            context.Response.Write(serializer.Serialize(_paymentService.DeleteDue(data.DueId, data.Reason)));
        }

        private void GetPaymentTypeHistoy(HttpContext context)
        {
            var olid = int.Parse(_query["olid"]);
            var paymentTypeId = int.Parse(_query["ptid"]);
            var currencyId = int.Parse(_query["cid"]);

            context.Response.ContentType = "text/plain";
            context.Response.Write("Hello World");
        }

        private void GetPaymentByDues(HttpContext context)
        {
            using (Page page = new Page())
            {
                StringBuilder s = new StringBuilder();

                PlaceHolder f = new PlaceHolder();

                using (StringWriter writer = new StringWriter(CultureInfo.CurrentCulture))
                {
                    OfflinePortalPaymentByCurrencies avp = (OfflinePortalPaymentByCurrencies)page.LoadControl("~/Layouts/SubLayouts/OfflinePortal/OfflinePortalPaymentByCurrencies.ascx");
                    avp.PaymentInfo = _paymentService.GetOrderLinePaymentInfo(int.Parse(_query["olid"]));
                    f.Controls.Add(avp);
                    page.Controls.Add(f);
                    HttpContext.Current.Server.Execute(page, writer, false);
                    s.Clear();
                    s.Append(writer.ToString());
                }

                context.Response.Write(s.ToString());
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Request"></param>
        /// <returns></returns>
        private string GetDocumentContents(System.Web.HttpRequest Request)
        {
            string documentContents;

            using (Stream receiveStream = Request.InputStream)
            {
                using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
                {
                    documentContents = readStream.ReadToEnd();
                }
            }

            return documentContents;
        }

        private class DueDelete
        {
            public int DueId { get; set; }

            public string Reason { get; set; }
        }
    }
}