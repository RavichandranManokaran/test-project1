﻿using Affinion.LoyaltyBuild.Api.Search.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Api.Search.Filters
{
    class CheckInDateFilter:ILoyaltyFilter,ISearchFilter,IBedBankFilter
    {
         private readonly SearchKey _key;

        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="key"></param>
         public CheckInDateFilter(SearchKey key)
        {
            _key = key;
        }

        public void BuildSearch(ref UCom.MasterClass.ExtendedApi.SearchCriteria criteria)
        {
            if (_key.CheckInDate !=null)
            {
                criteria.ArrivalDate = _key.CheckInDate.Value.AddDays(7 * _key.Week);
            }
        }

        /// <summary>
        /// Method to set the search criteria for Bed Bank Checkin Date Search
        /// </summary>
        /// <param name="criteria"></param>
        public void BuildbbSearch(ref BedBanks.General.BBsearchCriteria criteria)
        {
            if (_key.Location.Type != LocationType.City)
                return;
            if (_key.CheckInDate != null)
            {
                criteria.ArrivalDate = _key.CheckInDate.Value.AddDays(7 * _key.Week).ToString("yyyy-MM-dd");
            }
        }
    }
}
