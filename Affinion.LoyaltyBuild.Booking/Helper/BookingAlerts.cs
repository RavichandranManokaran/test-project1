﻿using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Data;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Api.Booking.Helper
{
    static class BookingAlerts
    {
        
      
        #region Alert Constants

        public static readonly string InvalidBooking = "InvalidBooking";
        public static readonly string CancelledSuccessfuly = "CancelledSuccessfuly";
        public static readonly string ErrorCancelling = "ErrorCanceling";
        public static readonly string PastCancelDeadline = "PastCancelDeadline";
        public static readonly string AlreadyCancelled = "AlreadyCancelled";
        public static readonly string ErrorTransferring = "ErrorTransferring";
        public static readonly string TransferSuccessful = "TransferSuccessful";
        public static readonly string ValidateCancel = "ValidateCancel";
        
        #endregion

        /// <summary>
        /// static ocnstructor
        /// </summary>
        private static Item GetItem()
        {
            return Sitecore.Context.Database.GetItem(ID.Parse("{D8F199A3-39A0-4890-87D6-28783A88906D}"));
        }

        /// <summary>
        /// Get alert
        /// </summary>
        /// <param name="alertName"></param>
        /// <returns></returns>
        public static string GetAlert(string alertName)
        {
            Item _alertitem = GetItem();

            if(_alertitem == null)
                return string.Empty;

            //get item value
            return SitecoreFieldsHelper.GetValue(_alertitem, alertName);
        }
    }
}
