﻿
-- =============================================
--	Name		: ManageCustomerQuery
--	Purpose		: To Add or edit the CustomerQuery
--	Author		: Antonysamy
--	Date		:	04/02/2016
-- =============================================

CREATE PROCEDURE [dbo].[LB_ManageCustomerQuery]
	(
	@QueryId			INT = 0,
	@OrderLineId		INT = null,
	@CustomerId			INT = null,
	@QueryTypeId		INT, 
	@Content			VARCHAR(MAX),
	@IsSolved			BIT = 0,
	@PartnerId			varchar(100) = null,
	@CampaignId			varchar(100) = null,
	@ProviderId			varchar(100) = null,
	@AttachmentId		INT = 0,
	@IsAttachAvailable  BIT = 0,
	@FileContent		VARBINARY(MAX) = null,
	@FileType			VARCHAR(50) = null,
	@FileName           VARCHAR(100) = null,
	@CreatedBy		    NVARCHAR(50) = null,
	@UpdatedBy		    NVARCHAR(50) = null
	)
AS

BEGIN

IF(isnull(@CustomerId,0) = 0 AND isnull(@OrderLineId,0)=0)
BEGIN
RETURN
END

declare @verror int = 0
-- if record exists theN update otherwise insert 
IF exists( SELECT 1 FROM LB_CustomerQuery 
		   WHERE QueryId = @QueryId
			 AND @QueryId  <> 0	
			) 
	BEGIN
		UPDATE LB_CustomerQuery 
		SET Content = Content +'\r\n'+ CONVERT(VARCHAR(100),GETDATE()) + ':\r\n' +  @Content,
			IsSolved	=@IsSolved,
			QueryTypeId =@QueryTypeId,
			UpdatedBy	= @CreatedBy,
			UpdatedDate = GETDATE()
		WHERE QueryId	= @QueryId	
	END
ELSE
	BEGIN

	INSERT INTO LB_CustomerQuery(
				 QueryTypeId
				,OrderLineId
				,CustomerId
				,Content
				,IsSolved
				,PartnerId
				,CampaignId
				,ProviderId
				,CreatedDate
				,CreatedBy)
	VALUES  (@QueryTypeId
			,@OrderLineId
			,@CustomerId
			,@Content
			,@IsSolved
			,@PartnerId
			,@CampaignId
			,@ProviderId
			,GETDATE()
			,@CreatedBy)
	END
  /*
  IF (@IsAttachAvailable > 0)
  BEGIN
	EXEC  affinion_ManageCustomerQueryAttachments 
					@QueryId
					,@FileType 
					,@FileName 
					,@FileContent  
	END
*/
 SELECT @verror=@@ERROR
  IF (@verror>0)
  BEGIN
	 ROLLBACK
	 RETURN
  END

END	
			 


