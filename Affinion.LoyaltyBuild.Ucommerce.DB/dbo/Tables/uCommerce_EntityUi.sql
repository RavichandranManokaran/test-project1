﻿CREATE TABLE [dbo].[uCommerce_EntityUi] (
    [EntityUiId]    INT            IDENTITY (1, 1) NOT NULL,
    [Type]          NVARCHAR (512) NOT NULL,
    [VirtualPathUi] NVARCHAR (512) NULL,
    [SortOrder]     INT            NOT NULL,
    CONSTRAINT [PK_EntityUi] PRIMARY KEY CLUSTERED ([EntityUiId] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_uCommerce_EntityUi_Type]
    ON [dbo].[uCommerce_EntityUi]([EntityUiId] ASC);

