﻿using Affinion.LoyaltyBuild.Model.Pricing;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.Availability.SupplierPortal.Controls
{
    public partial class AvailabilityCalendarView : System.Web.UI.UserControl
    {
        public string Disable;
        public int CurrentMonth { get; set; }
        public ProviderAvialbilityData currenItem { get; set; }
        public AvailabilityCalendarView()
        {
        }
        public AvailabilityCalendarView(ProviderAvialbilityData currenItem)
            :this()
        {
            this.currenItem = currenItem;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            this.DataBind();
            //  if(Cu
            checkRoles();
        }

        public void checkRoles()
        {
            string currentUserRole = SitecoreUserProfile.GetItem(UserProfileKeys.currentLoggedinUserRole);
            if (currentUserRole.ToLower().Contains(Constants.accountManagement) || currentUserRole.ToLower().Contains(Constants.marketing) || currentUserRole.ToLower().Contains(Constants.IT) || currentUserRole.ToLower().Contains(Constants.superUser))
            {
                roleBasedDisableButtons.Value = "false";
            }
            else if (currentUserRole.ToLower().Contains(Constants.grpHotelRole) || currentUserRole.ToLower().Contains(Constants.hotelSupervisor) || currentUserRole.ToLower().Contains(Constants.lbAdminRole) || currentUserRole.ToLower().Contains(Constants.hotelRole))
            {
                roleBasedDisableButtons.Value = "false";
            }
            else if (currentUserRole.ToLower().Contains(Constants.finance))
            {
                roleBasedDisableButtons.Value = "true";
            }
        }
    }
}