﻿Create PROCEDURE [dbo].LB_AddGuestChildAgeDetails

	(@OrderLineId as int,

	@ChildrenAge as varchar(100))
AS

BEGIN

	if exists (select 1 from uCommerce_OrderProperty where orderlineid = @OrderLineId and [key] = '_ChildrenAge')
	Begin

	Update [dbo].uCommerce_OrderProperty set [Value]=@ChildrenAge where [Key]='_ChildrenAge' and OrderLineId=@OrderLineId

	END

END
