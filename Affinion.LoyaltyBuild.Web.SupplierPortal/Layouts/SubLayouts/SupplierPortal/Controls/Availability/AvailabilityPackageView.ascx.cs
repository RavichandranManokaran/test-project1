﻿using Affinion.LoyaltyBuild.Model.Pricing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Availability
{
    public partial class AvailabilityPackageView : System.Web.UI.UserControl
    {
        /// <summary>
        /// list Product Price
        /// </summary>
        public List<ProductPrice> ProductPrice{ get; set; }

        /// <summary>
        /// AvailabilityPackageView
        /// </summary>
        public AvailabilityPackageView()
        {
        }

        /// <summary>
        /// AvailabilityPackageView
        /// </summary>
        /// <param name="productPrice"></param>
        public AvailabilityPackageView(List<ProductPrice> productPrice)
            :this()
        {
            this.ProductPrice = productPrice;
        }

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            packageDetailsRepeater.DataSource = ProductPrice;
            packageDetailsRepeater.DataBind();
        }
    }
}