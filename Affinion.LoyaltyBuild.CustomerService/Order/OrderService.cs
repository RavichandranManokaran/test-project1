﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic;
using ucommdata = UCommerce.EntitiesV2;
using System;
using System.Collections.Generic;
using System.Linq;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Payment;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;
using System.IO;
using Sitecore.ApplicationCenter.Applications;
using System.Web;
using System.Transactions;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Provider;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model.Payment;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Helpers;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Subrate;
using Sitecore.Data.Items;
using Affinion.LoyaltyBuild.Common.Utilities;
using System.Data.SqlClient;
using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.UCom.Common.Constants;
using AffinionConstants = Affinion.LoyaltyBuild.Common.Constants;


/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/
namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Order
{
    /// <summary>
    /// Service that provides methods to perform all order related actions
    /// </summary>
    public class OrderService : IOrderService
    {
        //private variables
        private IPaymentService _paymentService;
        private IProviderService _providerService;
        private string _connectionString = "uCommerce";
        private const string SpCheckingNoBookingCount = "LB_CheckBookingByClientIdandDate";

        private const string FilteredGetClientItems = @"fast:/sitecore/content/#admin-portal#/#client-setup#/*[@@templatename='ClientDetails'and @Active='1']";

        /// <summary>
        /// constructor
        /// </summary>
        public OrderService(PaymentService paymentService, ProviderService providerService)
        {
            _paymentService = paymentService;
            _providerService = providerService;
        }

        #region IOrderService Implementation

        /// <summary>
        /// Gets the purchase order information based on the provided order id
        /// </summary>
        /// <param name="orderId">Order id to fetch</param>
        /// <returns>Purchase order details</returns>
        public PurchaseOrder GetOrder(int orderId)
        {
            var order = ucommdata.PurchaseOrder.FirstOrDefault(x => x.OrderId == orderId);

            return GetPurchaseOrderInfoFromUCommerce(order);
        }

        /// <summary>
        /// Gets the purchase order information based on the provided order number/booking reeference
        /// </summary>
        /// <param name="orderId">order number/booking reeference</param>
        /// <returns>Purchase order details</returns>
        public PurchaseOrder GetOrder(string orderNumber)
        {
            var order = ucommdata.PurchaseOrder.FirstOrDefault(x => x.OrderNumber == orderNumber);

            return GetPurchaseOrderInfoFromUCommerce(order);
        }

        /// <summary>
        /// Confirmation mail manage booking
        /// </summary>
        /// <param name="emailid"></param>
        /// <returns></returns>
        public List<int> GetBookingEmailAddress(string emailid, string lastName)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "dbo.LB_GetOrderLineEmail";

            //the result object
            //Booking bookingDetails = null;
            List<int> result = new List<int>();
            //create command
            using (var command = db.GetStoredProcCommand(sql))
            {
                db.AddInParameter(command, "Email", DbType.String, emailid);
                db.AddInParameter(command, "LastName", DbType.String, lastName);

                //execute reader
                using (var reader = db.ExecuteReader(command))
                {

                    while (reader.Read())
                    {
                        var order = reader.Value<int>("OrderLineId");
                        result.Add(order);
                    }
                }
            }



            return result;
        }

        /// <summary>
        /// Gets the Booking information along with the order line info for just the provided order id
        /// </summary>
        /// <param name="orderlineId">Order line id to fetch</param>
        /// <returns>Purchase orer details with order line info</returns>
        public Booking GetBooking(int orderLineId)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "LB_GetBookingDetail";

            //the result object
            Booking bookingDetails = null;

            //create command
            using (var command = db.GetStoredProcCommand(sql))
            {
                db.AddInParameter(command, "OrderLineId", DbType.Int32, orderLineId);

                //execute reader
                using (var reader = db.ExecuteReader(command))
                {

                    if (reader.Read())
                    {
                        var customer = new Model.Customer
                        {
                            CustomerId = reader["CustomerID"] != DBNull.Value ? Convert.ToInt32(reader["CustomerID"]): default(int),
                            FirstName = reader["FirstName"] != DBNull.Value ? Convert.ToString(reader["FirstName"]) : string.Empty,
                            Address = reader["Address"] != DBNull.Value ? Convert.ToString(reader["Address"]) : string.Empty,
                            //AddressLine1 = reader["AddressLine1"] != DBNull.Value ? Convert.ToString(reader["AddressLine1"]) : string.Empty,
                            //AddressLine2 = reader["AddressLine2"] != DBNull.Value ? Convert.ToString(reader["AddressLine2"]) : string.Empty,
                            Email = reader["Email"] != DBNull.Value ? Convert.ToString(reader["Email"]) : string.Empty,
                            Phone = reader["Phonenumber"] != DBNull.Value ? Convert.ToString(reader["Phonenumber"]) : string.Empty,
                            LanguageName = reader["LanguageName"] != DBNull.Value ? Convert.ToString(reader["LanguageName"]) : string.Empty,
                        };

                        bookingDetails = new Booking
                        {
                            OrderId = reader["OrderId"] != DBNull.Value ? Convert.ToInt32(reader["OrderId"]) : default(int),
                            ClinetId = reader["ClientId"] != DBNull.Value ? Convert.ToString(reader["ClientId"]) : string.Empty,
                            BookingReference = reader["BookingReferance"] != DBNull.Value ? Convert.ToString(reader["BookingReferance"]) : string.Empty,
                            CheckInDate =  reader.Value<DateTime>("CheckIndate"),
                            CheckOutDate = reader.Value<DateTime>("CheckOutdate"),
                            ReservationDate = reader.Value<DateTime>("Reservation"),
                            Confirmation = reader.Value<DateTime>("Confirmation"),
                            StatusDate = reader.Value<DateTime>("Statusdate"),
                            OccupancyType = reader["OccupancyType"] != DBNull.Value ? Convert.ToString(reader["OccupancyType"]) : string.Empty,
                            ProviderId = reader.Value<Guid>("ProviderId").ToString(),
                            NumberOfAdults = reader.Value<int>("NoOfAdult"),
                            NumberOfChildren = reader.Value<int>("NoOfChild"),
                            NumberOfNights = reader.Value<int>("NoOfNights"),
                            TotalAmount = reader.Value<decimal>("TotalPrice"),
                            BookingThrough = reader.Value<string>("BookingThrough"),
                            CreatedBy = reader.Value<string>("CreateBy"),
                            UpdatedBy = reader.Value<string>("LastUpdate"),
                            IsCancelled = reader.Value<bool>("IsCancelled"),
                            StatusID = reader.Value<int>("StatusId"),
                            Price = reader.Value<decimal>("Price"),
                            CurrencyId = reader.Value<int>("CurrencyId"),
                            GuestName = reader.Value<string>("GuestName"),
                            NoOfGuest = reader.Value<int>("NoOfGuest"),
                            ChildAge = reader.Value<string>("ChildAge"),
                            Deposit = 0,
                           PayableAtHotel = 0,
                            NumberOfRooms = reader.Value<int>("NoOfRooms"),
                            CurrencyText = "€",
                            Customer = customer,
                            LBCommissionFee = reader.Value<decimal>("CommissionFee"), //LB Commision Fee
                            CreditCardProcessingFee = reader.Value<decimal>("CreditcardProcessingfee")
                        };
                        bookingDetails.BookingNotes = new List<BookingNotes>();
                        if(!string.IsNullOrEmpty(reader.Value<string>("Note")))
                        {
                            BookingNotes objBookingNotes = new BookingNotes();
                            objBookingNotes.Note = reader.Value<string>("Note");
                            objBookingNotes.TypeId = 2;
                            bookingDetails.BookingNotes.Add(objBookingNotes);
                        }

                        //get the Booking Information
                        bookingDetails.OrderLineId = orderLineId;

                        //get Subrate details
                        //if (reader.NextResult())
                        //{
                        //    while (reader.Read())
                        //    {
                        //        var BookingSubrate = new BookingSubrate
                        //        {
                        //            Id = reader.Value<int>("OrderLineId"),
                        //            SubrateCode = reader.Value<string>("SubrateItemCode"),
                        //            Description = reader.Value<string>("DisplayName"),
                        //            Price = reader.Value<Decimal>("Amount"),
                        //            Discount = reader.Value<Decimal>("Discount"),
                        //        };
                        //        bookingDetails.BookingSubrates.Add(BookingSubrate);



                        //    }
                        //}

                        UCommerce.EntitiesV2.OrderLine orderLine = UCommerce.EntitiesV2.OrderLine.All().FirstOrDefault(i => i.OrderLineId == orderLineId);

                        var deposit = bookingDetails.LBCommissionFee;
                        if (deposit != null)
                            bookingDetails.Deposit = deposit;

                        bool isProcessingFeeCharged = false;
                        if (!string.IsNullOrEmpty(orderLine[OrderPropertyConstants.IsProcessingFeeCharged]))
                            bool.TryParse(orderLine[OrderPropertyConstants.IsProcessingFeeCharged], out isProcessingFeeCharged);

                        if (isProcessingFeeCharged)
                        {
                            var processingFee = bookingDetails.CreditCardProcessingFee;
                            if (bookingDetails.CreditCardProcessingFee != null)
                                bookingDetails.ProcessingFee = processingFee;
                        }
                        //set payable at hotel

                        decimal providerAmountPaidOnCheckout = 0;
                        if (orderLine != null && orderLine[OrderPropertyConstants.PaymentMethod] != null)
                        {
                            //decimal.TryParse(orderLine[OrderPropertyConstants.ProviderAmountPaidCheckOut], out providerAmountPaidOnCheckout);
                            decimal.TryParse(orderLine[AffinionConstants.OrderCheckoutAmountColumn], out providerAmountPaidOnCheckout);
                            //decimal.TryParse(orderLine[AffinionConstants.OrderCheckoutAmountColumn], out checkoutAmount);
                            
                            bookingDetails.PayableAtHotel = 0;
                            bookingDetails.Deposit = bookingDetails.Price;
                        }
                        else
                        {
                            bookingDetails.PayableAtHotel = bookingDetails.Price - deposit - providerAmountPaidOnCheckout;
                            bookingDetails.Deposit = bookingDetails.LBCommissionFee + bookingDetails.CreditCardProcessingFee;
                        }
                    

                        //get due details
                        if (reader.NextResult())
                        {
                            if (reader.Read())
                            {
                                while (reader.Read())
                                {
                                    var item = new TotalDueItem
                                    {
                                        OrderLineID = orderLineId,
                                        Amount = reader.Value<decimal>(0),
                                        CurrencyTypeId = reader.GetInt32(1)
                                    };
                                    bookingDetails.DueByCurrencies.Add(item);
                                }
                            }
                        }

                        try
                        {
                            //get accomodation info
                            bookingDetails.AccomodationInfo = _providerService.GetAccomodationDetailsByUcommerceId(bookingDetails.ProviderId);
                        }
                        catch (Exception ex)
                        { }
                    }
                }
                return bookingDetails;
            }
        }

        /// <summary>
        /// Gets the Booking information along with the order line info for just the provided order id
        /// </summary>
        /// <param name="orderlineId">Order line id to fetch</param>
        /// <returns>Purchase orer details with order line info</returns>
        public BaseBooking GetBaseBooking(int orderlineId)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "LB_GetBaseBookingDetail";

            //the result object
            BaseBooking bookingDetails = null;

            //create command
            using (var command = db.GetStoredProcCommand(sql))
            {
                db.AddInParameter(command, "OrderLineId", DbType.Int32, orderlineId);

                //execute reader
                using (var reader = db.ExecuteReader(command))
                {

                    if (reader.Read())
                    {
                        bookingDetails = new Booking
                        {
                            OrderLineId = orderlineId,

                            BookingReference = reader.Value<string>("BookingReferance"),
                            //IsCancelled = reader.Value<int>("IsCancelled"),
                            StatusID=reader.Value<int>("StatusId"),
                            CheckInDate = reader.Value<DateTime>("CheckInDate")
                        };
                    }
                }
            }

            return bookingDetails;
        }

        /// <summary>
        /// Cancels a item in the order
        /// </summary>
        /// <param name="orderId">Order id to fetch</param>
        /// <param name="orderLineId">Id of order line item to cancel</param>
        /// <param name="paymentDue">optional details on refund if refund is involved</param>
        /// <returns>Cancel success flag</returns>
        public ValidationResponse CancelBooking(CancelBookingInfo cancelBookingInfo)
        {
            var result = new ValidationResponse();

            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);

            using (var scope = new TransactionScope())
            {
                try
                {
                    var cancelCommand = db.GetStoredProcCommand("LB_CancelBooking");
                    db.AddInParameter(cancelCommand, "OrderLineId", DbType.Int32, cancelBookingInfo.OrderLineid);
                    db.AddInParameter(cancelCommand, "Note", DbType.String, cancelBookingInfo.noteToProvider ?? string.Empty);
                    db.AddInParameter(cancelCommand, "CancelReason", DbType.String, cancelBookingInfo.cancelReason ?? string.Empty);
                    db.AddInParameter(cancelCommand, "CreatedBy", DbType.String, cancelBookingInfo.CreatedBy ?? string.Empty);
                    db.ExecuteNonQuery(cancelCommand);

                    var canceldueCommand = db.GetStoredProcCommand("LB_AddCancelDue");
                    db.AddInParameter(canceldueCommand, "OrderLineId", DbType.Int32, cancelBookingInfo.OrderLineid);
                    db.AddInParameter(canceldueCommand, "CreatedBy", DbType.String, cancelBookingInfo.CreatedBy ?? string.Empty);
                    db.ExecuteNonQuery(canceldueCommand);

                    var updateStausCommand = db.GetStoredProcCommand("LB_UpdateRoomAvailability");
                    db.AddInParameter(updateStausCommand, "OrderLineId", DbType.Int32, cancelBookingInfo.OrderLineid);
                    db.AddInParameter(updateStausCommand, "IsCancel", DbType.Boolean, true);
                    db.ExecuteNonQuery(updateStausCommand);


                    var updateStatusCommand = db.GetStoredProcCommand("LB_AddBookingStatus");
                    db.AddInParameter(updateStatusCommand, "OrderLineId", DbType.Int32, cancelBookingInfo.OrderLineid);
                    db.AddInParameter(updateStatusCommand, "Status", DbType.Int32, 7);
                    db.AddInParameter(updateStatusCommand, "CreatedBy", DbType.String, cancelBookingInfo.CreatedBy ?? string.Empty);
                    db.ExecuteNonQuery(updateStatusCommand);

                    result.IsSuccess = true;
                    result.Message = "Your order is cancelled successfully";
                    scope.Complete();
                }
                catch(Exception er)
                {
                    result.IsSuccess = false;
                    result.Message = "Error.. Could not complete your cancellation request";
                    result.ErrorMessage = er.Message.ToString();
                }
            }

            return result;
        }

        /// <summary>
        /// Transfer a booking in an order to another booking
        /// </summary>
        /// <param name="orderLineToTransfer">info on order line item to tranfer<param>
        /// <returns>Success flag</returns>
        public ValidationResponse TranferBooking(TransferBookingInfo orderLineToTransfer)
        {
            var result = new ValidationResponse();

            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);

            var subrates = GetSubratesCustomData();
            var orderid = UCommerce.Api.TransactionLibrary.GetBasket(false).PurchaseOrder.OrderId;
            var isFreeTranfer = GetIsFreeTransfer(orderLineToTransfer.OrderLineid);

            using (var scope = new TransactionScope())
            {
                try
                {
                    //var subrateCommand = db.GetStoredProcCommand("AddOrderLineSubrates");
                    //db.AddInParameter(subrateCommand, "orderId", DbType.Int32, orderid);
                    //var parameter = new SqlParameter("@SubrateTVP", SqlDbType.Structured) { Value = subrates };
                    //subrateCommand.Parameters.Add(parameter);
                    //db.ExecuteNonQuery(subrateCommand);

                    var transferCommand = db.GetStoredProcCommand("LB_TransferBooking");
                    db.AddInParameter(transferCommand, "OrderLineId", DbType.Int32, orderLineToTransfer.OrderLineid);
                    db.AddInParameter(transferCommand, "NoOfAdults", DbType.Int32, orderLineToTransfer.NoOdAdult);
                    db.AddInParameter(transferCommand, "NoOfChildren", DbType.Int32, orderLineToTransfer.NoOfChildren);
                    db.AddInParameter(transferCommand, "ChildrenAgeInfo", DbType.String, orderLineToTransfer.ChildrenAgeInfo ?? string.Empty);
                    db.AddInParameter(transferCommand, "Note", DbType.String, orderLineToTransfer.NoteToProvider ?? string.Empty);
                    db.AddInParameter(transferCommand, "CreatedBy", DbType.String, orderLineToTransfer.CreatedBy ?? string.Empty);
                    db.AddInParameter(transferCommand, "CartId", DbType.String, orderLineToTransfer.BasketId);
                    db.AddOutParameter(transferCommand, "NewOrderLineId", DbType.Int32, 8);
                    db.AddInParameter(transferCommand, "OrderReference", DbType.String, orderLineToTransfer.BookingReferenceNumber);
                    db.ExecuteNonQuery(transferCommand);
                    int newOrderLineId = (int)db.GetParameterValue(transferCommand, "NewOrderLineId");

                    var transferDueCommand = db.GetStoredProcCommand("LB_AddTransferDue");
                    db.AddInParameter(transferDueCommand, "OldOrderLineId", DbType.Int32, orderLineToTransfer.OrderLineid);
                    db.AddInParameter(transferDueCommand, "NewOrderLineId", DbType.Int32, newOrderLineId);
                    db.AddInParameter(transferDueCommand, "IsFreeTransfer", DbType.Boolean, isFreeTranfer);
                    db.AddInParameter(transferDueCommand, "CreatedBy", DbType.String, orderLineToTransfer.CreatedBy ?? string.Empty);
                    db.ExecuteNonQuery(transferDueCommand);

                    var updateStatusCommand = db.GetStoredProcCommand("LB_AddBookingStatus");
                    db.AddInParameter(updateStatusCommand, "OrderLineId", DbType.Int32, orderLineToTransfer.OrderLineid);
                    db.AddInParameter(updateStatusCommand, "Status", DbType.Int32, 8);
                    db.AddInParameter(updateStatusCommand, "CreatedBy", DbType.String, orderLineToTransfer.CreatedBy ?? string.Empty);
                    db.ExecuteNonQuery(updateStatusCommand);

                    var updateStatusNewCommand = db.GetStoredProcCommand("LB_AddBookingStatus");
                    db.AddInParameter(updateStatusNewCommand, "OrderLineId", DbType.Int32, newOrderLineId);
                    db.AddInParameter(updateStatusCommand, "Status", DbType.Int32, 6);
                    db.AddInParameter(updateStatusNewCommand, "CreatedBy", DbType.String, orderLineToTransfer.CreatedBy ?? string.Empty);
                    db.ExecuteNonQuery(updateStatusNewCommand);


                    result.IsSuccess = true;
                    result.Message = "Your booking is transferred successfully..";
                    scope.Complete();
                }
                catch
                {
                    result.IsSuccess = false;
                    result.Message = "Error..Could not transfer your booking..";
                }
            }

            return result;
        }

        /// <summary>
        /// Get the transfer history details for the item
        /// </summary>
        /// <param name="orderLineId">order line id</param>
        /// <returns>list of transfer history</returns>
        public List<TransferHistoryItem> GetBookingTransferHistory(int orderLineId)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "LB_GetTransferHistoryDetail";

            //the result object
            var result = new List<TransferHistoryItem>();

            //create command
            using (var command = db.GetStoredProcCommand(sql))
            {
                db.AddInParameter(command, "OrderLineId", DbType.Int32, orderLineId);

                //execute reader
                using (var reader = db.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        TransferHistoryItem tmpresult = new TransferHistoryItem
                        {
                            OrderLineId = reader.Value<int>("OrderLineId"),
                            SupplierName = reader.Value<string>("SupplierName"),
                            ArrivalDate = reader.Value<DateTime>("ArrivalDate"),
                            DepartureDate = reader.Value<DateTime>("DepartureDate"),
                            NoOfRooms = reader.Value<int>("NoOfRooms"),
                            NoOfNights = reader.Value<int>("NoOfNights"),
                            NoOfPeople = reader.Value<int>("NoOfPeople"),
                            ReservationDate = reader.Value<DateTime>("ReservationDate")
                        };
                        result.Add(tmpresult);
                    }
                }
            }
            return result;
        }


        #region "ReminderEmail"

        /// <summary>
        /// Get the Holiday home partial payment Details
        /// </summary>
        /// <returns> Holiday home partial payment related Data Set</returns>
        public DataSet ProvisionalHolidayHomesBookings(string clientId)
        {
            try
            {
                DataSet ds = new DataSet();
                DatabaseProviderFactory factory = new DatabaseProviderFactory();
                Database db = factory.Create(_connectionString);
                var sql = "LB_GetProvisionalHolidayHomesBookings";
                using (var command = db.GetStoredProcCommand(sql))
                {
                    db.AddInParameter(command, "@ClientId", DbType.String, clientId);
                    ds = db.ExecuteDataSet(command);
                }
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #endregion

     
        #region Private Methods

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string GetClientId()
        {
            string clientId = string.Empty;
            ///Get the client item for online portal
            Item clientItem = SitecoreFieldsHelper.GetLookupFieldTargetItem(ItemHelper.RootItem, "ClientDetails");
            if (clientItem != null)
            {
                clientId = clientItem.ID.Guid.ToString();
            }

            return clientId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderlineId"></param>
        /// <returns></returns>
        private DataTable GetSubratesCustomData()
        {
            SubrateService subrateService = new SubrateService();
            string clientId = GetClientId();
            List<OrderLineSubrate> orderLineSubrateList = subrateService.GetOrderSubrate(UCommerce.Api.TransactionLibrary.GetBasket(false).PurchaseOrder.OrderId, clientId, UCommerce.Api.TransactionLibrary.GetBasket(false).PurchaseOrder.BillingCurrency.ISOCode);
            if (orderLineSubrateList.Count > 0)
            {
                DataTable subratesDataTable = new DataTable("SubRatesDataTable");

                // Add three column objects to the table. 
                DataColumn orderLineIdColumn = new DataColumn();
                orderLineIdColumn.DataType = System.Type.GetType("System.Int32");
                orderLineIdColumn.ColumnName = "OrderLineID";
                subratesDataTable.Columns.Add(orderLineIdColumn);

                DataColumn subrateIDColumn = new DataColumn();
                subrateIDColumn.DataType = System.Type.GetType("System.String");
                subrateIDColumn.ColumnName = "SubrateItemCode";
                subratesDataTable.Columns.Add(subrateIDColumn);

                DataColumn amountColumn = new DataColumn();
                amountColumn.DataType = System.Type.GetType("System.Decimal");
                amountColumn.ColumnName = "Amount";
                subratesDataTable.Columns.Add(amountColumn);

                DataColumn discountColumn = new DataColumn();
                discountColumn.DataType = System.Type.GetType("System.Decimal");
                discountColumn.ColumnName = "Discount";
                subratesDataTable.Columns.Add(discountColumn);

                DataColumn isIncludedInUnitPriceColumn = new DataColumn();
                isIncludedInUnitPriceColumn.DataType = System.Type.GetType("System.Boolean");
                isIncludedInUnitPriceColumn.ColumnName = "IsIncludedInUnitPrice";
                subratesDataTable.Columns.Add(isIncludedInUnitPriceColumn);

                foreach (var orderLineSubrate in orderLineSubrateList)
                {
                    int orderLineId = orderLineSubrate.OrderLineId;
                    List<Subrates> subrateList = orderLineSubrate.Subrates;

                    foreach (Subrates subrate in subrateList)
                    {
                        DataRow row = subratesDataTable.NewRow();
                        row["OrderLineId"] = orderLineId;
                        row["SubrateItemCode"] = subrate.SubrateCode;
                        row["Amount"] = subrate.TotalAmount;
                        row["Discount"] = 0;
                        row["IsIncludedInUnitPrice"] = string.Equals(subrate.SubrateCode, "LBCOM") ? true : false;
                        subratesDataTable.Rows.Add(row);
                    }
                }

                return subratesDataTable;
            }

            return null;
        }

        /// <summary>
        /// Convert ucommerce purchase order entity to a local entity
        /// </summary>
        /// <param name="purchaseOrder">ucommerce purchase order</param>
        /// <param name="orderLineId">optional order line item if only one item is required</param>
        /// <returns>local purchase order entity</returns>
        private PurchaseOrder GetPurchaseOrderInfoFromUCommerce(ucommdata.PurchaseOrder purchaseOrder, int? orderLineId = null)
        {
            //return null if the purchase order is not found
            if (purchaseOrder == null)
                return null;

            //set order info
            var order = purchaseOrder.ToLocalData();

            //set customer info
            if (purchaseOrder.Customer != null)
            {
                var custInfo = purchaseOrder.Customer;

                order.Customer = custInfo.ToLocalData();
            }

            //set order items
            if (purchaseOrder.OrderLines != null)
            {
                var orderLinesInfo = purchaseOrder.OrderLines;

                order.OrderItems = new List<Booking>();
                foreach (var item in orderLinesInfo)
                {
                    //add only the selected order item if order item id passed
                    if (orderLineId == null || item.OrderLineId == orderLineId)
                    {
                        order.OrderItems.Add(item.ToLocalData());
                    }
                }
            }

            //return order
            return order;
        }

        /// <summary>
        /// Get if is free transfer
        /// </summary>
        /// <param name="orderLineId"></param>
        /// <returns></returns>
        private bool GetIsFreeTransfer(int orderLineId)
        {
            var ClientId = Constants.HardCodedClientId;
            //var clientID = BasketHelper.GetOrderLineClientId(orderLineId);

            //get allowed free tranfers
            int allowedSameHotelTransfers = 0;
            int allowedDifferentHotelTransfers = 0;
            Item clientItem = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(ClientId));
            if (clientItem != null)
            {
                int.TryParse(SitecoreFieldsHelper.GetValue(clientItem, "TransferAllowedWithinSameProvider") ?? string.Empty, out allowedSameHotelTransfers);
                int.TryParse(SitecoreFieldsHelper.GetValue(clientItem, "TransferAllowedWithDifferentProvider") ?? string.Empty, out allowedDifferentHotelTransfers);
            }

            //get used free transfers
            int usedSameHotelTransfers = 0,
                usedDifferentHotelTransfers = 0;
            var result = GetTranferDetails(orderLineId);

            for (int i = 0; i < result.Count - 1; i++)
            {
                if (result[i].ProviderId == result[i + 1].ProviderId)
                    usedSameHotelTransfers += 1;
                else
                    usedDifferentHotelTransfers += 1;
            }

            //get provider ids for tranferrred and basket item
            var currentCategoryId = result.FirstOrDefault(i => i.OrderLineId == orderLineId).ProviderId;
            var basketOrderLine = UCommerce.Api.TransactionLibrary.GetBasket(false).PurchaseOrder.OrderLines.FirstOrDefault();
            var basketCategoryId = UCommerce.EntitiesV2.CategoryProductRelation
                .FirstOrDefault(j => j.Product.Sku == basketOrderLine.Sku)
                .Category.Id;

            //check is free tranfer
            return (currentCategoryId == basketCategoryId && usedSameHotelTransfers < allowedSameHotelTransfers)
                || (currentCategoryId != basketCategoryId && usedDifferentHotelTransfers < allowedDifferentHotelTransfers);
        }

        /// <summary>
        /// get transfer details
        /// </summary>
        /// <param name="orderLineId"></param>
        /// <returns></returns>
        private List<BookingTransfer> GetTranferDetails(int orderLineId)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "LB_GetTransfer";
            var result = new List<BookingTransfer>();

            //create command
            using (var command = db.GetStoredProcCommand(sql))
            {
                db.AddInParameter(command, "@OrderLineId", DbType.Int32, orderLineId);

                //execute reader
                using (var reader = db.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        BookingTransfer tmpresult = new BookingTransfer
                        {
                            OrderLineId = reader.Value<int>("orderLineid"),
                            CreatedOn = reader.Value<DateTime>("createdon"),
                            ProviderId = reader.Value<int>("providerid")
                            //,
                            // ProviderGuid = reader.Value<string>("providerguid")

                        };
                        result.Add(tmpresult);
                    }
                }

            }

            return result;
        }       

        /// <summary>
        /// GetClientList
        /// </summary>
        /// <returns>ClientName and Guid from Sitecore</returns>
        private List<KeyValuePair<string, string>> GetClientList()
        {
            return Sitecore.Context.Database.SelectItems(FilteredGetClientItems)
             .Select(x => new KeyValuePair<string, string>(x.ID.ToString(), x.DisplayName)).ToList();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// CheckingNoBookingCount
        /// </summary>             
        /// <returns>NoBookingSchedulerModel.ClientId</returns>
        public string CheckingNoBookingCount(string clientId, DateTime fromTime, DateTime toTime)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
     
            Database db = factory.Create(_connectionString);
            NoBookingSchedulerModel obNoBookingEmail = new NoBookingSchedulerModel();
            try
            {
                using (var command = db.GetStoredProcCommand(SpCheckingNoBookingCount))
                {
                    db.AddInParameter(command, "@ClientId", DbType.String, clientId);
                    db.AddInParameter(command, "@Fromtime", DbType.DateTime, fromTime);
                    db.AddInParameter(command, "@Totime", DbType.DateTime, toTime);

                    using (var reader = db.ExecuteReader(command))
                    {
                        while (reader.Read())
                        {
                            obNoBookingEmail.ClientId = reader.Value<string>("ClientId");
                            return obNoBookingEmail.ClientId;
                        }
                    }
                }
                return null;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}

