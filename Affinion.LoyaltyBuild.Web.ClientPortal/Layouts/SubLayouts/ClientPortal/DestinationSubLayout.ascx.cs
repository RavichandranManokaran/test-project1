﻿namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal
{
    //<summary>
    /// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
    /// PROPRIETARY INFORMATION The information contained herein (the 
    /// 'Proprietary Information') is highly confidential and proprietary to and 
    /// constitutes trade secrets of Affinion International. The Proprietary Information 
    /// is for Affinion International use only and shall not be published, 
    /// communicated, disclosed or divulged to any person, firm, corporation or 
    /// other legal entity, directly or indirectly, without the prior written 
    /// consent of Affinion International.
    ///
    /// Source File:           DestinationSubLayout.cs
    /// Sub-system/Module:     Affinion.LoyaltyBuild.Web
    /// Description:           Used to Bind data to Destination subLayout
    /// </summary>
    
    #region Using Directives
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    #endregion

    public partial class DestinationSubLayout : BaseSublayout
    {
        /// <summary>
        /// Bind destination items to  datasource of DestinationRepeater
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            Item item = Sitecore.Context.Item;
            try
            {
                Sitecore.Data.Fields.MultilistField multilistField = item.Fields["DestinationField"];
                if (multilistField != null)
                {
                    List<Item> list = new List<Item>();
                    //Iterate over all the selected items by using the property TargetIDs
                    foreach (Item ResourceItem in multilistField.GetItems())
                    {
                        list.Add(ResourceItem);
                    }
                    DestinationRepeater.DataSource = list;
                    DestinationRepeater.DataBind();
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, null);
                throw;
            }
        }
    }
}