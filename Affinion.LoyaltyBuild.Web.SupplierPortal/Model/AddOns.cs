﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Model
{
    /// <summary>
    /// addOns Property class
    /// </summary>
    public class AddOns
    {
        /// <summary>
        /// Product Name
        /// </summary>
        public string ProductName { get; set; }
 
        /// <summary>
        /// Number Of Adult
        /// </summary>
        public int NumberOfAdult { get; set; }

        /// <summary>        
        /// Number Of Child
        /// </summary>          
        public int NumberOfChild { get; set; }

        /// <summary>
        /// Package Price
        /// </summary>
        public decimal PackagePrice { get; set; }

        /// <summary>
        /// LBShare
        /// </summary>
        public decimal LBShare { get; set; }

        /// <summary>
        /// LB Share Package
        /// </summary>
        public decimal LBSharePKG { get; set; }

        /// <summary>
        /// Product Price
        /// </summary>
        public decimal ProductPrice { get; set; }

        /// <summary>
        /// Total Price
        /// </summary>
        public decimal ProviderPrice { get; set; }

        /// <summary>
        /// Total Package Price
        /// </summary>
        public decimal ProviderPKGPrice { get; set; }

    }
}