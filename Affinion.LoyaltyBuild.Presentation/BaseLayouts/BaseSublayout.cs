﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           BaseSublayout.cs
/// Sub-system/Module:     Common(VS project name)
/// Description:           Base layout with common methods
/// </summary>
namespace Affinion.LoyaltyBuild.Presentation.BaseLayouts
{
    #region Using Statements

    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Search.Data;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using Sitecore.Web.UI.WebControls;
    using System;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Web;
    using System.Web.UI;

    #endregion

    /// <summary>
    /// Base layout with common methods
    /// </summary>
    public class BaseSublayout : UserControl
    {
        #region Fields

        private BasePrimaryLayout parentPage;

        #endregion

        #region Properties

        private NameValueCollection queryString;
        protected string QueryString(string name)
        {
            if (queryString == null)
            {
                var helper = new QueryStringHelper(Request);
                queryString = new NameValueCollection();
                foreach (var item in helper.QueryStringParameters.AllKeys)
                {
                    queryString.Add(item.ToLower(), helper.GetValue(item));
                }
            }
            if (queryString.HasKey(name.ToLower()))
                return queryString[name.ToLower()];

            return string.Empty;
        }

        protected string GetUrl(string url, bool isEncrypted = true)
        {
            return (new QueryStringHelper(new Uri(new Uri(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority)), url))).GetUrl(isEncrypted).PathAndQuery;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Gets the parent page
        /// </summary>
        public BasePrimaryLayout ParentPage
        {
            get
            {
                if (parentPage == null)
                {
                    parentPage = this.Page as BasePrimaryLayout;
                }

                return parentPage;
            }
        }

        /// <summary>
        /// Execute code on preload event
        /// This method will be valid for sublayouts which is attached to 2 levels deep in placeholders
        /// </summary>
        /// <param name="e">The argument</param>
        public virtual void PreLoad(EventArgs e)
        {
            // Sublayout can implement this to execute code before the page load

        }

        /// <summary>
        /// Add javascript global variables
        /// </summary>
        /// <param name="variableName">Variable name</param>
        /// <param name="value">The value</param>
        public void AddJavascriptVariable(string variableName, object value)
        {
            if (this.ParentPage != null)
            {
                if (this.ParentPage.JavascriptVariables.ContainsKey(variableName))
                {
                    this.ParentPage.JavascriptVariables[variableName] = value;
                }
                else
                {
                    this.ParentPage.JavascriptVariables.Add(variableName, value);
                }
            }
        }

        /// <summary>
        /// Gets the data source as an item from the current sublayout
        /// </summary>
        /// <returns>Returns the targeting item pointed by the datasource</returns>
        public Item GetDataSource()
        {
            Item dataSource = null;
            Sublayout sublayout = Parent as Sublayout;

            if (sublayout == null)
            {
                return null;
            }

            Guid dataSourceId;

            if (Guid.TryParse(sublayout.DataSource, out dataSourceId))
            {
                dataSource = Sitecore.Context.Database.GetItem(new ID(dataSourceId));
            }
            else
            {
                dataSource = Sitecore.Context.Database.GetItem(sublayout.DataSource);
            }

            return dataSource;
        }

        /// <summary>
        /// Get parameter value from sublayout
        /// </summary>
        /// <param name="parameterName">The parameter value to read</param>
        /// <returns></returns>
        public string GetParameter(string parameterName)
        {
            Sublayout sublayout = Parent as Sublayout;

            if (sublayout == null)
            {
                return null;
            }
            
            NameValueCollection parameterCollection = Sitecore.Web.WebUtil.ParseUrlParameters(sublayout.Parameters);
            return parameterCollection[parameterName];
        }

        /// <summary>
        /// Get parameter value from sublayout
        /// </summary>
        /// <param name="parameterName">The parameter value to read</param>
        /// <param name="targetItemTextField">The field to read the text. Pass null to load droplist values</param>
        /// <returns></returns>
        public string GetParameter(string parameterName, string targetItemTextField)
        {
            string selectedItemId = HttpUtility.UrlDecode(GetParameter(parameterName));
            Sitecore.Data.ID itemID = null;
            if (!string.IsNullOrEmpty(targetItemTextField) && Sitecore.Data.ID.TryParse(selectedItemId, out itemID))
            {
                Item selectedItem = Sitecore.Context.Database.GetItem(new ID(selectedItemId));
                return selectedItem.Fields[targetItemTextField].Value;
            }

            return selectedItemId;
        }

        /// <summary>
        /// Get the user selected currency information or default
        /// </summary>
        /// <returns>Returns currency information</returns>
        public string GetCurrencyInformation()
        {
            string currencyCode = Request.QueryString["cur"] != null ? Request.QueryString["cur"].ToString() : null;

            if (Request.Cookies["affcurcode"] == null)
            {
                HttpCookie newCookie = new System.Web.HttpCookie("affcurcode");
                Response.Cookies.Add(newCookie);
            }

            HttpCookie currencyCodeCookie = Request.Cookies["affcurcode"];

            if (!string.IsNullOrEmpty(currencyCode))
            {
                currencyCodeCookie.Value = currencyCode;
                return currencyCode;
            }
            else
            {
                if (string.IsNullOrEmpty(currencyCodeCookie.Value))
                {
                    return string.Empty;
                }

                return currencyCodeCookie.Value;
            }
        }

        /// <summary>
        /// Method to get Item from Rendering Parameter which has item path as value
        /// </summary>
        /// <param name="parameterName">Rendering Parameter Name</param>
        /// <returns>Item corresponding to the path mentioned in the Rendering Parameter</returns>
        public Item GetItemFromRenderingParameterPath(string parameterName)
        {
            string itemPath = GetParameter(parameterName, null);
            return (!string.IsNullOrWhiteSpace(itemPath)) ? Sitecore.Context.Database.GetItem(itemPath) : null;
        }

        #endregion
    }
}
