﻿CREATE TABLE [dbo].[uCommerce_UserWidgetSettingProperty] (
    [UserWidgetSettingPropertyId] INT            IDENTITY (1, 1) NOT NULL,
    [Key]                         NVARCHAR (MAX) NULL,
    [Value]                       NVARCHAR (MAX) NULL,
    [UserWidgetSettingId]         INT            NULL,
    PRIMARY KEY CLUSTERED ([UserWidgetSettingPropertyId] ASC),
    FOREIGN KEY ([UserWidgetSettingId]) REFERENCES [dbo].[uCommerce_UserWidgetSetting] ([UserWidgetSettingId])
);

