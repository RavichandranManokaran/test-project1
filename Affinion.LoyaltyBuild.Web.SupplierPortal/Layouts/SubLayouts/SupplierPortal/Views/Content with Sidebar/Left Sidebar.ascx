﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Left Sidebar.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Views.Content_with_Sidebar.Left_Sidebar" %>

<sc:Sublayout ID="breadcrumbs" runat="server" Path="/layouts/SubLayouts/SupplierPortal/Controls/Navigation/Breadcrumbs.ascx" />
<div class="row show-grid clear-both">
 <div id="left-sidebar" class="span3 sidebar">
  <sc:Placeholder id="contentsecondary" runat="server" key="content-secondary" />
  <sc:Placeholder id="contenttertiary" runat="server" key="content-tertiary" />
 </div>
 <div class="span9 main-column two-columns-left"><sc:Placeholder id="contentprimary" runat="server" key="content-primary" /></div>
</div>


