﻿CREATE PROCEDURE [dbo].[LB_GetCancellationMismatchReport]
	AS
BEGIN
SELECT
		(CASE WHEN (CT.Name)='CustomerCancellation' THEN 'Customer Email' Else 'Provider Email' END) [Audience], 
		ch.OrderlineId as [Booking ID], 
		(CASE WHEN ISNULL(BI.StatusId,0)=7 THEN 'Cancelled' Else Null END)  [Booking Status],
		BI.BookingNo as [Booking Reference],
		op.[Value] as [Booking Method],
		CH.[To] as [Customer Email],
		CH.[From] as [Provider Email],
		BI.CreatedDate as [Reservation Date],
		BI.CheckInDate as [ArrivalDate]

 from LB_BookingInfo bi
inner join uCommerce_OrderProperty op on op.OrderLineId=bi.OrderLineId
inner join LB_CommunicationHistory ch on ch.OrderLineId=bi.OrderLineId
inner join LB_CommunicationTypes ct on ct.CommunicationTypeId=ch.CommunicationTypeId

WHERE
        CH.OrderLineId=BI.OrderLineId and
		op.[Key]='_BookingThrough' AND
		(CH.CommunicationTypeId= (select CommunicationTypeId from LB_CommunicationTypes where Name='ProviderCancellation') or
		CH.CommunicationTypeId= (select CommunicationTypeId from LB_CommunicationTypes where Name='CustomerCancellation')) and
		CH.[status]=0 and
		BI.StatusId=7  and
	    CH.CreatedDate = CAST(DATEADD(D, -1,GETDATE()) as date) and
		BI.CreatedDate = CAST(DATEADD(D, -1,GETDATE()) as date)
	
	End

GO
