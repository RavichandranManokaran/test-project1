﻿using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Linq;
using UCommerce.EntitiesV2;
using UCommerce.EntitiesV2.Queries;
using System;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Queries
{
    public class CampaignItemByDateQuery : ICannedQuery<CampaignItem>
    {
        /// <summary>
        /// StartDate
        /// </summary>
        private readonly DateTime _startDate;

        /// <summary>
        /// EndDate
        /// </summary>
        private readonly DateTime _endDate;

        /// <summary>
        /// CampaignItemByDateQuery
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        public CampaignItemByDateQuery(DateTime startDate, DateTime endDate)
        {
            _startDate = startDate;
            _endDate = endDate;
        }

        /// <summary>
        /// IEnumerable type CampaignItem
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public IEnumerable<CampaignItem> Execute(ISession session)
        {
            //var purchaseOrder = session.Query<Campaign>()
            //    .Where(x => (x.StartsOn >= _startDate && x.StartsOn <= _endDate) 
            //    || (x.EndsOn >= _startDate && x.EndsOn <= _endDate)).Fetch(x => x.CampaignItems).ToList();
            var purchaseOrder = session.Query<Campaign>().Where(x =>
                ((_startDate >= x.StartsOn || _endDate >= x.StartsOn) && (_startDate <= x.EndsOn || _endDate <= x.EndsOn)) 
                ).Fetch(x => x.CampaignItems).ToList();
           // var purchaseOrder = session.Query<Campaign>().Where(x => (x.StartsOn <= _startDate) && (x.EndsOn >= _startDate)).Fetch(x => x.CampaignItems).ToList();
            List<CampaignItem> campaginItemList = new List<CampaignItem>();
            foreach (var item in purchaseOrder)
            {
                campaginItemList.AddRange(item.CampaignItems.Where(x => x.Enabled));
            }
            return campaginItemList;
        }
    }
}
