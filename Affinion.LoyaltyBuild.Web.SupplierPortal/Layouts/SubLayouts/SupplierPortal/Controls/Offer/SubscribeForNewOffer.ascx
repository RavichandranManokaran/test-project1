﻿<%@ control language="C#" autoeventwireup="true" codebehind="SubscribeForNewOffer.ascx.cs" inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Offer.SubscribeForNewOffer" %>
<div>
    <form id="frm1" runat="server">
        <asp:textbox id="txtContent" TextMode="MultiLine" columns="50" rows="5" runat="server" cssclass="form-control" clientidmode="Static" TextWrapping="Wrap" verticalscrollbarvisibility="Visible" AcceptsReturn="True" />
    </form>
</div>

<div class="alert alert-warning">
    <strong>Please note</strong> this offer will now appear on list of offers hotel are participating on.    
</div>
