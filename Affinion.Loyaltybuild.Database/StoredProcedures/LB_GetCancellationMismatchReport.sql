

CREATE Procedure [dbo].[LB_GetCancellationMismatchReport]
AS
BEGIN

	--@CurrentDate as DateTime=CAST(DATEADD(D, -1,GETDATE()) as date)
	SELECT
		(CASE WHEN (CT.Name)='CustomerCancellation' THEN 'Customer Email' Else 'Provider Email' END) [Audience], 
		CH.OrderLineId as [Booking ID], 
		(CASE WHEN ISNULL(RR.IsCancelled,0)=1 THEN 'Cancelled' Else Null END)  [Booking Status],
		RR.OrderReference as [Booking Reference],
		RR.BookingThrough as [Booking Method],
		CH.[To] as [Customer Email],
		CH.[From] as [Provider Email],
		RR.CreatedDate as [Reservation Date],
		RR.CheckInDate as [ArrivalDate]
	
	FROM LB_RoomReservation RR
		inner Join uCommerce_CommunicationHistory CH on RR.OrderLineId=CH.OrderLineId
		inner Join uCommerce_CommunicationTypes CT on CH.CommunicationTypeId=CT.CommunicationTypeId

	WHERE
		CH.OrderLineId=RR.OrderLineId and
		(CH.CommunicationTypeId= (select CommunicationTypeId from uCommerce_CommunicationTypes where Name='ProviderCancellation') or
		CH.CommunicationTypeId= (select CommunicationTypeId from uCommerce_CommunicationTypes where Name='CustomerCancellation')) and
		CH.[status]=0 and
		RR.[IsCancelled]=1  and
	    CH.CreatedDate = CAST(DATEADD(D, -1,GETDATE()) as date) and
		RR.CreatedDate = CAST(DATEADD(D, -1,GETDATE()) as date)
End

GO

