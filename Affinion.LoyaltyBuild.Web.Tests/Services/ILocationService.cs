﻿#region Directives

using System.Collections.Generic;

#endregion Directives

namespace Affinion.LoyaltyBuild.Web.Tests.Services
{
    /// <summary>
    /// Location Service Interface
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ILocationService<T>
    {

        /// <summary>
        /// Gets all the location service related data
        /// </summary>
        /// <returns>returns a list of DTO preffered list of locations</returns>
        List<T> GetLocations();

        /// <summary>
        /// Get a list of location based on the selected location
        /// </summary>
        /// <param name="location">location name</param>
        /// <returns>Filtered list of location details</returns>
        List<T> GetLocations(string location);

    }
}
