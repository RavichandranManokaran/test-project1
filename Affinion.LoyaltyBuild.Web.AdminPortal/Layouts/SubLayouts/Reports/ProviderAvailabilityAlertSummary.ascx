﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProviderAvailabilityAlertSummary.ascx.cs" Inherits="Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.Reports.ProviderAvailabilityAlertSummary" %>

<style>
    .input-field {
        width: 45%;
    }
    .dropdown-field {
        width: 45%;
    }
</style>
<div class="content">
    <div class="content-body">
        <div id="errMsg" runat="server" class="col-sm-12 alert alert-danger" style="display: none">
        </div>
        <div>
             <div class="row">
                <div class="col-sm-12 marT20">
                    <div class="accordian-outer">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading bg-primary claim_titl">
                                    <h1 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="show-hide-list"><sc:Text id="scPanelTitle" runat="server"/></a>
                                    </h1>
                                </div>
                                <div id="collapse1" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div>
                                            <div class="content_bg_inner_box summary-detail receipt_body">
                                                <div class="col-xs-12 col-md-12">
                                                    <h4 id="parameters" runat="server" class="no-margin accent54-f"><sc:Text id="scParameters" runat="server"/></h4>
                                                    <div class="mybooking">
                                                        <div class="row margin-row">
                                                            <div class="col-sm-12 no-padding">
                                                                 <div class="col-sm-5">
                                                                    <span class="bask-form-titl font-size-12 pull-right"><sc:Text id="scDateRange" runat="server"/></span>
                                                                </div>
                                                                <div class="col-sm-7">
                                                                    <input type="text" id="DateRange" runat="server" clientidmode="Static" class="form-control datepicker font-size-12 input-field" tabindex="1" />
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12 no-padding marT20">
                                                                 <div class="col-sm-5">
                                                                    <span class="bask-form-titl font-size-12 pull-right"><sc:Text id="scSupplierType" runat="server"/></span>
                                                                </div>
                                                                <div class="col-sm-7">
                                                                    <asp:DropDownList  id="ddlSupplierType" runat="server" clientidmode="Static" AutoPostBack="true" CssClass="form-control font-size-12 dropdown-field" TabIndex="2" OnSelectedIndexChanged="ddlSupplierType_SelectedIndexChanged" >
                                                                     </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-12 no-padding  marT20">
                                                                 <div class="col-sm-5">
                                                                    <span class="bask-form-titl font-size-12 pull-right"><sc:Text id="scSupplier" runat="server"/></span>
                                                                </div>
                                                                <div class="col-sm-7">
                                                                    <asp:DropDownList  id="ddlSupplier" runat="server" clientidmode="Static" CssClass="form-control font-size-12 dropdown-field" TabIndex="3" >
                                                                     </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                             <div class="col-sm-12 no-padding  marT20">
                                                                 <div class="col-sm-5">
                                                                    <span class="bask-form-titl font-size-12 pull-right"><sc:Text id="scOccupencyType" runat="server"/></span>
                                                                </div>
                                                                <div class="col-sm-7">
                                                                    <asp:DropDownList  id="ddlOccupancyType" runat="server" clientidmode="Static" CssClass="form-control font-size-12 dropdown-field" TabIndex="3" >
                                                                     </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                             
                                                            <div class="col-sm-12 no-padding  marT20">
                                                                <div  class="col-sm-5"></div>
                                                                <div class="col-sm-7 col-sm-push-1 no-padding">
                                                                  <asp:Button type="button" ID="Search" runat="server" ClientIDMode="Static" Text="Search"  class="btn btn-info add-break font-size-12 marR5" TabIndex="2" OnClick="Search_Click" />
                                                                  <asp:Button type="button" ID="Export" runat="server" ClientIDMode="Static" Text="Export"  class="btn btn-info add-break font-size-12  marR5" TabIndex="3" OnClick="Export_Click" />
                                                                    <asp:HiddenField ID="hdnDateRange" ClientIDMode="Static" Value="true"  runat="server" />
                                                                </div>
                                                            </div>                                                             
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           
            <%--Html Report --%>
            <div class="row" id="htmlReportGrid" runat="server">
                <div class="col-sm-12">
                    <div>
                        <div class="panel panel-default">
                            <div class="panel-heading claim_titl">
                                <h4 class="panel-title"><sc:Text id="scGridPanelTile" runat="server"/>
                                </h4>
                            </div>
                            <div  id="totalCount"  runat="server"  class="totalCountpad"><sc:Text id="scTototalContText" runat="server"/><span> <%= TotalOfRecords %> </span> <sc:Text id="scGridRecordsFound" runat="server"/> </div>
                            <div class="panel-body alert-info">
                                <%-- Result Grid--%>
                                <div runat="server" id="ResultDiv" clientidmode="Static">
                                    <div id="data-grid" class="table-responsive">
                                         <asp:GridView ID="ResultGridView" runat="server" Width="100%" SelectedRowStyle-Font-Bold="true" CssClass="table tbl-calendar" AutoGenerateColumns="False" AllowPaging="true" PageSize="10" 
                                             PagerSettings-NextPageText=" Next " PagerSettings-PreviousPageText=" Previous " PagerSettings-Mode="NumericFirstLast" PagerSettings-FirstPageText=" First " PagerSettings-LastPageText=" Last " PagerSettings-Position="Bottom" PagerStyle-Font-Bold="true" PagerStyle-CssClass="pagination-grid" OnPageIndexChanging="ResultGridView_PageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Provider ID" HeaderStyle-Width="8%">
                                                    <ItemTemplate>
                                                         <asp:Label ID="lblProviderID" runat="server" Text='<%# Bind("ProductID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Provider Name" HeaderStyle-Width="8%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblProviderName" runat="server" Text='<%# Bind("ProductName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Occupancy Type Name" HeaderStyle-Width="8%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOccupancytypeName" runat="server" Text='<%# Bind("RoomTypeName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Map Location Name" HeaderStyle-Width="8%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMaplocationName" runat="server" Text='<%# Bind("MapLocationName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Star Ranking" HeaderStyle-Width="8%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStarRanking" runat="server" Text='<%# Bind("StarRanking") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Country Name" HeaderStyle-Width="8%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCountryName" runat="server" Text='<%# Bind("CountryName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Year" HeaderStyle-Width="8%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblYear" runat="server" Text='<%# Bind("Year") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Month" HeaderStyle-Width="8%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMonth" runat="server" Text='<%# Bind("Month") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Day" HeaderStyle-Width="8%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDay" runat="server" Text='<%# Bind("Day") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Provider Email" HeaderStyle-Width="8%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblProviderEmail" runat="server" Text='<%# Bind("ProviderEmail") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Provider Type Name" HeaderStyle-Width="8%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblProviderTypeName" runat="server" Text='<%# Bind("ProviderTypeName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ReportedDate" HeaderStyle-Width="12%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblReportedDate" runat="server" Text='<%# Bind("ReportedDate","{0:d}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate>
                                                -- No Records--
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            

        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {

        $('input[id="DateRange"]').daterangepicker();

        $('#DateRange').daterangepicker({
            locale: { cancelLabel: 'Clear' }
        });

        function cb(start, end) {
            $('#DateRange span').html(start + ' - ' + end);
        }
        cb(moment().subtract(29, 'days'), moment());

        $('#DateRange').daterangepicker({
            ranges: {
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Last Two Month': [moment().subtract(2, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Last Four Month': [moment().subtract(4, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Last Six Month': [moment().subtract(6, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        $('#DateRange').on('cancel.daterangepicker', function (ev, picker) {
            //do something, like clearing an input
            $('#DateRange').val('');
            Validation();
        });

        $('#DateRange').on('apply.daterangepicker', function (ev, picker) {
            Validation();
        });

        $('#ddlSupplier').on('change', function () {
            Validation();
        });

        if ($("#hdnDateRange").val() == "false") {
            $('#DateRange').val('');
        }

        Validation();

    });

    function Validation() {

        var DateRange = $("#DateRange").val();
        var Supplier = $("#ddlSupplier").val();
        var OccupancyType = $("#ddlOccupancyType").val();
        var count='<%= TotalOfRecords %>';
        if (DateRange != "" || Supplier != "" || OccupancyType!="") {
            $("#Search").removeAttr('disabled');
            if (count > 0) {
                $("#Export").removeAttr('disabled');
            }
            else {
                $("#Export").attr('disabled', 'disabled');
            }
            $("#hdnDateRange").val("true");
        }
        else {
            $("#Search").attr('disabled', 'disabled');
            $("#Export").attr('disabled', 'disabled');
            $("#hdnDateRange").val("false");
        }
    }
 </script>