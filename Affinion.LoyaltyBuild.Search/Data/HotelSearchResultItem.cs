﻿//<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SupplierSearchResultItem.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Search
/// Description:           Represents search result item
/// </summary>
namespace Affinion.LoyaltyBuild.Search.Data
{
    #region Using Statements

    using Affinion.LoyaltyBuild.Common;
    using Affinion.LoyaltyBuild.Common.Exceptions;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Search.Helpers;
    using Affinion.LoyaltyBuild.Search.OfferFilters;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Availability;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
    using Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi;
    using Newtonsoft.Json;
    using Sitecore.ContentSearch;
    using Sitecore.ContentSearch.SearchTypes;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Reflection;
    using UCommerce.EntitiesV2;
    using Affinion.LoyaltyBuild.UCom.Common.Extension;
    using Affinion.LoyaltyBuild.Model.Product;
    using Affinion.LoyaltyBuild.Model.Provider;

    #endregion

    public class HotelSearchResultItem 
    {
       // private Collection<ProductPrice> productPrices;

        public HotelSearchResultItem()
        {
           // Reason = OfferSearchFailedFilter.Default;
        }

        /// <summary>
        /// Provider Id
        /// </summary>
        public string ProviderId { get; set; }

        /// <summary>
        /// propertyReference ID
        /// </summary>
        public string PropertyReferenceId { get; set; }

        /// <summary>
        /// SitecoreItemId
        /// </summary>
        public string SitecoreItemId { get; set; }

        /// <summary>
        /// provider type
        /// </summary>
        public ProviderType ProviderType { get; set; }

        /// <summary>
        /// IntroductionImage
        /// </summary>
        public string IntroductionImage { get; set; }

        
        public string Name { get; set; }

        
        public string Town { get; set; }

        
        public string Overview { get; set; }

        
        public string StarRanking { get; set; }

       public Item SitecoreItem { get; set; }

        
        public List<Item> Facilities { get; set; }

        
        public string Themes { get; set; }

        
        public string Experiences { get; set; }

        
        public string AddressLine1 { get; set; }
        
        
        public string AddressLine2 { get; set; }

        
        public string AddressLine3 { get; set; }

        
        public string AddressLine4 { get; set; }

        
        public string Location { get; set; }


        
        public string UCommerceId { get; set; }

        
        public string Country { get; set; }

        
        public string LocalRanking { get; set; }


        
        public string LowestPrice { get; set; }

        
        public int CurrencyID { get; set; }

        
        public List<ProviderInfo> RoomCollection { get; set; }

        public int MaxAdults { get; set; }

        public int MaxChild { get; set; }

        public int MinAdults { get; set; }

        public string OldPrice { get; set; }

        public string BBCurrencyId { get; set; }
    }
}
