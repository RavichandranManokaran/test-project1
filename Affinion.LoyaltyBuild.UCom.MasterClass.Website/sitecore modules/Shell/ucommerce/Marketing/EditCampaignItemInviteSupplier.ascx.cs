﻿using Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi;
using Affinion.LoyaltyBuild.UCom.Common.Constants;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.EntitiesV2;
using UCommerce.Presentation.Views.Catalog;
using UCommerce.Presentation.Views.Marketing;
using UCommerce.Presentation.Web.Controls;
using UCommerce.Presentation.Web.Pages;
using Affinion.LoyaltyBuild.UCom.MasterClass.Website.Configuration;
using Affinion.LoyaltyBuild.UCom.MasterClass.Website.CustomSiteCoreDataType;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using UCommerce.Security;
using UCommerce.Infrastructure;
using Affinion.LoyaltyBuild.UCom.MasterClass.Model;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.Ucommerce.Marketing
{
    public partial class EditCampaignItemInviteSupplier : ViewEnabledControl<IEditCampaignItemView>
    {
        IUserService service = ObjectFactory.Instance.Resolve<IUserService>();

        List<SupplierInvite> listSupplierInvite;
        List<SupplierInvite> listSupplierSubscribed;
        public IList<ICommand> GetCommands()
        {
            return null;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            CampaignItemProperty property = ((CampaignItemProperty)base.View.CampaignItem[CampaignDataTyeConstant.IsApprove]);
            CampaignItemProperty invited = ((CampaignItemProperty)base.View.CampaignItem[CampaignDataTyeConstant.IsInviteSend]);
            bool isApprove = false;
            if (property != null)
            {
                if (bool.TryParse(property.Value, out isApprove))
                {
                }
            }
            bool isInvited = false;
            if (invited != null)
            {
                if (bool.TryParse(invited.Value, out isInvited))
                {
                }
            }

            if (isApprove)
            {
                dvApprovePanel.Visible = false;
                dvsupplierpanel.Visible = true;
                listSupplier.Visible = false;
                if (!this.IsPostBack)
                {
                    BindCity();
                    BindExperience();
                    BindRegion();
                    BindTheme();
                    BindHotelGroup();
                    BindRating();
                    BindHotelType();
                    BindCountry();
                    BindJson();
                    LoadSupplierInviteByCapaignItem();
                }
            }
            else
            {
                dvsupplierpanel.Visible = false;
                dvApprovePanel.Visible = true;
                User u = service.GetCurrentUser();
                List<PromotionUser> userList = SiteConfiguration.GetPromotionUser();
                if (isInvited)
                {

                    btnInviteForApproval.Visible = false;
                    if (!(View.CampaignItem.CreatedBy.Equals(u.UserName, StringComparison.InvariantCultureIgnoreCase)))
                    {
                        if (userList.Any(x => x.UserName.ToLower() == u.UserName.ToLower() && x.UserName.ToLower() != View.CampaignItem.CreatedBy.ToLower()))
                        {
                            btnApproval.Visible = true;
                            btnReject.Visible = true;
                        }
                        else
                        {
                            btnApproval.Enabled = false;
                            btnReject.Enabled = false;
                        }
                    }
                    else
                    {
                        btnApproval.Enabled = false;
                        btnReject.Enabled = false;
                    }
                }
                else
                {
                    if ((View.CampaignItem.CreatedBy.Equals(u.UserName, StringComparison.InvariantCultureIgnoreCase)))
                    {
                        btnInviteForApproval.Visible = true;
                    }
                    else
                    {
                        btnInviteForApproval.Enabled = false;
                    }
                    btnApproval.Visible = false;
                    btnReject.Visible = false;
                }
            }
        }
        private void LoadSupplierInviteByCapaignItem()
        {
            JavaScriptSerializer jsonData = new JavaScriptSerializer();
            FilterSupplier data = JsonConvert.DeserializeObject<FilterSupplier>(string.Format("{{{0}}}", hdQuery.Value));
            List<SupplierInvite> allSupplierByCampaign = new List<SupplierInvite>();
            listSupplierInvite = new List<SupplierInvite>();
            listSupplierSubscribed = new List<SupplierInvite>();

            allSupplierByCampaign = SupplierInvite.All().Where(x => x.CampaignItem == CampaignItemId).ToList();


            List<Product> product = new List<Product>();
            if (data != null)
            {
                product = SearchLibrary.FilterData1(data);
            }
            listSupplier.Items.Clear();



            //List<Category> category = new List<Category>();
            //if (data != null)
            //{
            //    category = SearchLibrary.FilterData(data);
            //}
            //listSupplier.Items.Clear();

            //List<Product> product = new List<Product>();
            if (allSupplierByCampaign.Count > 0)
            {
                product = product.Where(x => !allSupplierByCampaign.Any(y => y.Sku.Equals(x.Sku))).ToList();
            }
            //category = category.Where(x => !allSupplierByCampaign.Any(y => y.CategoryGuid.Equals(x.Guid.ToString(), StringComparison.InvariantCultureIgnoreCase))).ToList();
            listSupplierInvite = allSupplierByCampaign.Where(x => x.CampaignItem == CampaignItemId && !x.IsSubscribe).ToList();
            listSupplierSubscribed = allSupplierByCampaign.Where(x => x.CampaignItem == CampaignItemId && x.IsSubscribe).ToList();
            //if (category != null && category.Any())
            //{
            //    listSupplier.DataSource = category;
            //    listSupplier.DataBind();
            //}
            if (product != null && product.Any())
            {
                listSupplier.DataSource = product;
                listSupplier.DataBind();
            }
            if (listSupplier.Items.Count > 0)
            {
                listSupplier.Visible = true;
            }
            else
            {
                //lblNoSupplier.Visible = true;
                listSupplier.Visible = false;
            }
            IVSuppliersubscribe.Items.Clear();
            lvSupplier.Items.Clear();

            if (listSupplierSubscribed != null && listSupplierSubscribed.Any())
            {
                IVSuppliersubscribe.DataSource = listSupplierSubscribed;
                IVSuppliersubscribe.DataBind();
            }
            else
            {
                IVSuppliersubscribe.DataSource = null;
                IVSuppliersubscribe.DataBind();
            }

            if (listSupplierInvite != null && listSupplierInvite.Any())
            {
                lvSupplier.DataSource = listSupplierInvite;
                lvSupplier.DataBind();
            }
            else
            {
                lvSupplier.DataSource = null;
                lvSupplier.DataBind();
            }


        }
        protected void IVSuppliersubscribe_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {
            //string empid = "";
            IVSuppliersubscribe.Enabled = false;
            Label lbl = (IVSuppliersubscribe.Items[e.ItemIndex].FindControl("SupplierInviteId")) as Label;
            int inviteSupplierId = Convert.ToInt32(lbl.Text);
            SupplierInvite supplierInvite = SupplierInvite.Get(inviteSupplierId);
            if (supplierInvite != null)
            {
                try
                {
                    supplierInvite.Delete();
                }
                catch (Exception ex)
                {
                    Diagnostics.WriteException(DiagnosticsCategory.MasterClassWebsite, ex, this);
                    throw;
                }
            }
            IVSuppliersubscribe.Enabled = true;
            LoadSupplierInviteByCapaignItem();
        }

        protected void lvSupplier_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {
            //string empid = "";
            lvSupplier.Enabled = false;
            Label lbl = (lvSupplier.Items[e.ItemIndex].FindControl("SupplierInviteId")) as Label;
            int inviteSupplierId = Convert.ToInt32(lbl.Text);
            SupplierInvite supplierInvite = SupplierInvite.Get(inviteSupplierId);
            if (supplierInvite != null)
            {
                try
                {
                    supplierInvite.Delete();
                }
                catch (Exception ex)
                {
                    Diagnostics.WriteException(DiagnosticsCategory.MasterClassWebsite, ex, this);
                    throw;
                }
            }
            lvSupplier.Enabled = true;
            LoadSupplierInviteByCapaignItem();
        }

        public int CampaignItemId
        {
            get
            {
                return base.View.CampaignItem.CampaignItemId;
            }
        }
        private void BindCity()
        {
            List<string> datatypeList = BindDataByType(CustomDataTypeConstant.Location, "City");
            if (datatypeList != null)
            {
                foreach (var item in datatypeList)
                {
                    drpLocation.Items.Add(new ListItem() { Text = item, Value = item });
                }
            }
        }
        private void BindExperience()
        {
            List<string> datatypeList = BindDataByType(CustomDataTypeConstant.Experience, "Experience");
            if (datatypeList != null)
            {
                foreach (var item in datatypeList)
                {
                    drpExeperience.Items.Add(new ListItem() { Text = item, Value = item });
                }
            }
        }
        private void BindRegion()
        {
            List<string> datatypeList = BindDataByType(CustomDataTypeConstant.Region, "Region");
            if (datatypeList != null)
            {
                foreach (var item in datatypeList)
                {
                    drpRegion.Items.Add(new ListItem() { Text = item, Value = item });
                }
            }
        }
        private void BindTheme()
        {
            List<string> datatypeList = BindDataByType(CustomDataTypeConstant.HotelTheme, "Theme");
            if (datatypeList != null)
            {
                foreach (var item in datatypeList)
                {
                    drpTheme.Items.Add(new ListItem() { Text = item, Value = item });
                }
            }
        }
        private void BindHotelGroup()
        {
            List<string> datatypeList = BindDataByType(CustomDataTypeConstant.Groups, "Groups");
            if (datatypeList != null)
            {
                foreach (var item in datatypeList)
                {
                    drpGroup.Items.Add(new ListItem() { Text = item, Value = item });
                }
            }
        }
        private void BindRating()
        {
            List<string> datatypeList = BindDataByType(CustomDataTypeConstant.HotelRanking, "Rating");
            if (datatypeList != null)
            {
                foreach (var item in datatypeList)
                {
                    drpHotelRating.Items.Add(new ListItem() { Text = item, Value = item });
                }
            }
        }
        private void BindCountry()
        {
            List<string> datatypeList = BindDataByType(CustomDataTypeConstant.Country, "Country");
            if (datatypeList != null)
            {
                foreach (var item in datatypeList)
                {
                    drpCountry.Items.Add(new ListItem() { Text = item, Value = item });
                }
            }
        }
        private void BindHotelType()
        {
            List<string> datatypeList = BindDataByType(CustomDataTypeConstant.SupplierType, "Hotel type");
            if (datatypeList != null)
            {
                foreach (var item in datatypeList)
                {
                    drpHotelType.Items.Add(new ListItem() { Text = item, Value = item });
                }
            }
        }
        private List<string> BindDataByType(string typeName, string selectstring = null)
        {
            List<string> data = SiteConfiguration.GetSiteCoreItem(typeName);
            data.Insert(0, string.Format("Select {0}", (string.IsNullOrEmpty(selectstring) ? typeName : selectstring)));
            return data;
        }

        protected void btn_Filter_Click(object sender, EventArgs e)
        {
            SaveFilterCriteria();
            LoadSupplierInviteByCapaignItem();
            chkInviteAll.Checked = true;
        }

        private void SaveFilterCriteria()
        {
            try
            {
                SupplierFilter filterData = SupplierFilter.All().FirstOrDefault(x => x.CampaignItem == CampaignItemId);
                if (filterData != null)
                {
                    filterData.SearchFilter = (hdQuery.Value);
                    filterData.CampaignItem = CampaignItemId;
                    filterData.Save();
                }
                else
                {
                    filterData = new SupplierFilter();
                    filterData.SearchFilter = hdQuery.Value;
                    filterData.CampaignItem = CampaignItemId;
                    filterData.Save();
                }
            }
            catch { }
        }
        private void BindJson()
        {
            SupplierFilter filterData = SupplierFilter.All().FirstOrDefault(x => x.CampaignItem == CampaignItemId);
            if (filterData != null)
            {
                hdQuery.Value = (string.Format("{0}", filterData.SearchFilter));
            }

        }

        protected void btnInviteSupplier_Click(object sender, EventArgs e)
        {
            if (!chkInviteAll.Checked && !chkInviteSelected.Checked)
            {
                lblError.Text = "Please select at least one option (Invite selected or Invite All)";
            }

            else if (chkInviteAll.Checked && chkInviteSelected.Checked)
            {
                lblError.Text = "Please select Only one option (Invite selected or Invite All)";
            }
            else
            {
                List<SupplierInvite> supplierData = SupplierInvite.All().Where(x => x.CampaignItem.Equals(CampaignItemId)).ToList();
                List<string> categoryInvited = new List<string>();
                foreach (var item in listSupplier.Items)
                {
                    var selected = ((CheckBox)item.FindControl("chkSelected"));
                    bool addSupplier = (chkInviteAll != null && chkInviteAll.Checked);
                    if (!addSupplier)
                    {
                        addSupplier = (chkInviteSelected != null && chkInviteSelected.Checked);
                        if (addSupplier)
                        {
                            addSupplier = (selected != null && selected.Checked);
                        }
                    }
                    if (addSupplier)
                    {
                        var productsku = ((HiddenField)item.FindControl("hdId"));
                        string categoryId = string.Empty;
                        if (productsku != null && !string.IsNullOrEmpty(productsku.Value))
                        {
                            categoryId = productsku.Value;
                        }
                        if (!supplierData.Any(x => x.Sku.Equals(categoryId)))
                        {
                            SupplierInvite target = new SupplierInvite();
                            target.Name = ((Label)item.FindControl("SupplierName")).Text;
                            // target.CategoryGuid = categoryId.ToUpper();
                            target.Sku = categoryId;
                            target.IsSubscribe = chkSubscibe.Checked;
                            target.CampaignItem = CampaignItemId;
                            target.Save();
                            categoryInvited.Add(target.Sku);
                        }
                    }
                }
                if (categoryInvited != null && categoryInvited.Any())
                    SiteConfiguration.GetSupplierDetail(categoryInvited, View.CampaignItem.Name);
                LoadSupplierInviteByCapaignItem();
            }
        }

        protected void listSupplier_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {

            (listSupplier.FindControl("DataPager1") as DataPager).SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            LoadSupplierInviteByCapaignItem();
        }

        protected void btnInviteForApproval_Click(object sender, EventArgs e)
        {
            //CampaignItemProperty property = ((CampaignItemProperty)base.View.CampaignItem[CampaignDataTyeConstant.IsApprove]);
            CampaignItemProperty invited = ((CampaignItemProperty)base.View.CampaignItem[CampaignDataTyeConstant.IsInviteSend]);
            if (invited == null)
            {
                CampaignItemProperty property = new CampaignItemProperty();
                property.Value = "True";
                property.CampaignItem = base.View.CampaignItem;
                DefinitionField isInviteDefination = DefinitionField.All().Where(x => x.Name == CampaignDataTyeConstant.IsInviteSend && x.Definition.Name == "Default Campaign Item").FirstOrDefault();
                property.DefinitionField = isInviteDefination;
                property.Save();
            }
            else
            {
                invited.Value = "True";
                invited.Save();
            }
            SendPromotionEmail(4);
            SaveAuditTrail(CampaignDataTyeConstant.InviteMessage);
            this.Page.Response.Redirect(this.Page.Request.Url.ToString(), true);
        }

        private void SendPromotionEmail(int type)
        {
            try
            {
                List<PromotionUser> userList = SiteConfiguration.GetPromotionUser();
                List<string> to = new List<string>();
                List<string> cc = new List<string>();
                if (type == 4)
                {
                    PromotionUser user = userList.FirstOrDefault(x => x.UserName.ToLower() == View.CampaignItem.CreatedBy);
                    if (user != null)
                    {
                        cc.Add(user.EmailAddress);
                    }
                    foreach (var item in userList)
                    {
                        if (item.UserName.ToLower() != View.CampaignItem.CreatedBy.ToLower())
                        {
                            to.Add(item.EmailAddress);
                        }
                    }
                }
                else if (type == 5 || type == 6)
                {
                    PromotionUser user = userList.FirstOrDefault(x => x.UserName.ToLower() == View.CampaignItem.CreatedBy);
                    if (user != null)
                    {
                        to.Add(user.EmailAddress);
                    }
                    foreach (var item in userList)
                    {
                        if (item.UserName.ToLower() != View.CampaignItem.CreatedBy.ToLower())
                        {
                            cc.Add(item.EmailAddress);
                        }
                    }
                }
                Affinion.LoyaltyBuild.Communications.IEmailService emailService = new Affinion.LoyaltyBuild.Communications.Services.EmailService();
                string emailtemplate = SiteConfiguration.GetEmailTemplateHtml(type);
                emailtemplate = emailtemplate.Replace("%%promotioname%%", View.CampaignItem.Name);
                emailtemplate = emailtemplate.Replace("%%username%%", service.GetCurrentUserName());
                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress("info@loyaltybuild.com");

                    if (to != null)
                    {
                        foreach (string toAddress in to)
                        {
                            if (toAddress.Length > 0)
                                mail.To.Add(new MailAddress(toAddress.ToString()));
                        }
                    }

                    if (cc != null)
                    {
                        foreach (string ccAddress in cc)
                        {
                            if (ccAddress.Length > 0)
                                mail.CC.Add(new MailAddress(ccAddress.ToString()));
                        }
                    }
                    mail.Subject = string.Format("Promotion Alert {0}", (type == 4 ? ("Created") : (type == 5 ? "Approved" : "Rejected")));
                    mail.IsBodyHtml = true;
                    mail.Body = emailtemplate;
                    if (to.Count > 0 || cc.Count > 0)
                    {
                        try
                        {
                            emailService.Send(mail);
                        }
                        catch (Exception er)
                        {
                        }
                    }
                }

            }
            catch (Exception er)
            {

            }
            //throw new NotImplementedException();
        }

        protected void btnApproval_Click(object sender, EventArgs e)
        {
            CampaignItemProperty approveProperty = ((CampaignItemProperty)base.View.CampaignItem[CampaignDataTyeConstant.IsApprove]);
            if (approveProperty == null)
            {
                CampaignItemProperty property = new CampaignItemProperty();
                property.Value = "True";
                property.CampaignItem = base.View.CampaignItem;
                DefinitionField isInviteDefination = DefinitionField.All().Where(x => x.Name == CampaignDataTyeConstant.IsApprove && x.Definition.Name == "Default Campaign Item").FirstOrDefault();
                property.DefinitionField = isInviteDefination;
                property.Save();
            }
            else
            {
                approveProperty.Value = "True";
                approveProperty.Save();
            }
            SendPromotionEmail(5);
            SaveAuditTrail(CampaignDataTyeConstant.ApproveMessage);
            this.Page.Response.Redirect(this.Page.Request.Url.ToString(), true);
        }

        private void SaveAuditTrail(string message)
        {
            PromotionAuditTrail trail = new PromotionAuditTrail();
            trail.Message = message;
            trail.EntityName = "CampaignItem";
            trail.Record = CampaignItemId;
            trail.AuditedBy = service.GetCurrentUserName();
            trail.AuditedDate = DateTime.Now;
            trail.Save();
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            CampaignItemProperty invited = ((CampaignItemProperty)base.View.CampaignItem[CampaignDataTyeConstant.IsInviteSend]);
            if (invited == null)
            {
                CampaignItemProperty property = new CampaignItemProperty();
                property.Value = "False";
                property.CampaignItem = base.View.CampaignItem;
                DefinitionField isInviteDefination = DefinitionField.All().Where(x => x.Name == CampaignDataTyeConstant.IsInviteSend && x.Definition.Name == "Default Campaign Item").FirstOrDefault();
                property.DefinitionField = isInviteDefination;
                property.Save();
            }
            else
            {
                invited.Value = "False";
                invited.Save();
            }
            SendPromotionEmail(6);
            SaveAuditTrail(CampaignDataTyeConstant.RejectMessage);
            this.Page.Response.Redirect(this.Page.Request.Url.ToString(), true);
        }


    }
}
