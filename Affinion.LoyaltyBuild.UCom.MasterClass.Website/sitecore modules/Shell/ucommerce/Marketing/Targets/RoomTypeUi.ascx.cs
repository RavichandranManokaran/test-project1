﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.EntitiesV2;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using UCommerce.Presentation.Views.Marketing;
using UCommerce.Presentation.Web.Controls;
using UCommerce.Presentation.Web.Marketing;
using Affinion.LoyaltyBuild.UCom.Common.Constants;
using Affinion.LoyaltyBuild.UCom.MasterClass.Website.Configuration;
using Affinion.LoyaltyBuild.UCom.MasterClass.Website.CustomSiteCoreDataType;
using Affinion.LoyaltyBuild.Common.Instrumentation;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.ucommerce.Marketing.Targets
{
    public partial class RoomTypeUi : ViewEnabledControl<IEditCampaignItemView>, ITargetUi
    {
        List<string> datatypeList;

        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (datatypeList == null)
            //{
            //    DataType roomType = DataType.All().Where(x => x.TypeName == DataTypeConstant.RoomType).ToList().FirstOrDefault();
            //    if (roomType != null)
            //    {
            //        datatypeList = roomType.DataTypeEnums.ToList();
            //    }
            //    datatypeList.Insert(0, new DataTypeEnum() { Value = "" });
            //}
            datatypeList = SiteConfiguration.GetSiteCoreItem(CustomDataTypeConstant.OccupancyTypes);
            drpRoomType.Items.Clear();
            if (datatypeList != null)
            {
                foreach (var item in datatypeList)
                {
                    drpRoomType.Items.Add(new ListItem() { Text = item, Value = item });
                }
            }
            RoomTypeTarget roomTypeData = this.DataSource as RoomTypeTarget;
            this.DeleteButton.OnClientClick = string.Format("return confirm('Delete');");
            if (datatypeList.Any(x => x == roomTypeData.RoomType))
            {
                drpRoomType.SelectedValue = roomTypeData.RoomType;
            }
            this.ReadOnlyMessage.Text = this.TargetToString(roomTypeData);
        }

        /// <summary>
        /// Save Room detail
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveButton_Click(object sender, ImageClickEventArgs e)
        {
            RoomTypeTarget roomTypeData = this.DataSource as RoomTypeTarget;
            roomTypeData.RoomType = drpRoomType.SelectedValue;
            roomTypeData.Save();
            this.ReadOnlyMessage.Text = this.TargetToString(roomTypeData);
            this.ToggleEditor();
        }

        /// <summary>
        /// enable edit & display view in page
        /// </summary>
        private void ToggleEditor()
        {
            this.NonEditModePlaceHolder.Visible = !this.NonEditModePlaceHolder.Visible;
            this.EditButton.Visible = !this.EditButton.Visible;
            this.EditModePlaceHolder.Visible = !this.EditModePlaceHolder.Visible;
            this.SaveButton.Visible = !this.SaveButton.Visible;
        }

        /// <summary>
        /// View Roomtype Act
        /// </summary>
        /// <param name="RoomTypeTarget"></param>
        /// <returns></returns>
        private string TargetToString(RoomTypeTarget RoomTypeTarget)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendFormat("RoomType ");
            stringBuilder.AppendFormat("{0} ", RoomTypeTarget.RoomType);
            return stringBuilder.ToString();
        }

        /// <summary>
        /// Delete room type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteButton_Click(object sender, ImageClickEventArgs e)
        {
            RoomTypeTarget roomTypeData = this.DataSource as RoomTypeTarget;
            if (roomTypeData != null)
            {
                roomTypeData.Delete();
            }
            this.Page.Response.Redirect(this.Page.Request.Url.ToString(), true);
        }

        /// <summary>
        /// Edit room type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditButton_Click(object sender, ImageClickEventArgs e)
        {
            this.ToggleEditor();
        }

        /// <summary>
        /// get set for apply
        /// </summary>
        public bool EnabledForApply
        {
            get;
            set;
        }

        /// <summary>
        /// get set for display
        /// </summary>
        public bool EnabledForDisplay
        {
            get;
            set;
        }

        /// <summary>
        /// Get set for datasource
        /// </summary>
        public object DataSource
        {
            get;
            set;

        }

        /// <summary>
        /// Create new Room Type
        /// </summary>
        /// <returns></returns>
        public object New()
        {
            var RoomTypeTarget = new RoomTypeTarget();
            try
            {
                RoomTypeTarget.CampaignItem = View.CampaignItem;
                RoomTypeTarget.EnabledForApply = true;
                RoomTypeTarget.RoomType = string.Empty;
                RoomTypeTarget.Save();
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.MasterClassWebsite, ex, this);
                throw;
            }
            this.DataSource = RoomTypeTarget;
            return RoomTypeTarget;
        }
    }
}