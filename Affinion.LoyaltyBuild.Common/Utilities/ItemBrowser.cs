﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           ItemBrowser.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Used to log diagnostic data
/// </summary>
namespace Affinion.LoyaltyBuild.Common.Utilities
{
    #region Using Namespaces
    using Affinion.LoyaltyBuild.Common.Exceptions;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using Sitecore.Security.Accounts;
    using Sitecore.SecurityModel;
    using System;
    using System.Linq;
    using System.Xml;
    #endregion

    /// <summary>
    /// Provides functionality related to item retrieval and alteration 
    /// </summary>
    public static class ItemHelper
    {
        #region Private Constants

        private const string FolderTemplatePath = "/sitecore/templates/System/Templates/Template Folder";

        /// <summary>
        /// Master database name
        /// </summary>
        private const string masterDatabase = "master";

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the user to edit sitecore content
        /// </summary>
        public static User ContentEditingUser
        {
            get
            {
                return User.FromName(@"sitecore\admin", true);
            }
        }

        /// <summary>
        /// Gets the root item of the current site
        /// </summary>
        public static Item RootItem
        {
            get
            {
                if(Sitecore.Context.Item == null)
                {
                    return null;
                }

                // return Sitecore.Context.Database.GetItem(Sitecore.Context.Site.RootPath);
                ID rootTemplateId = new ID("{64AE939C-5354-4C4D-A418-5E6D26CC04BE}");
                var items = Sitecore.Context.Item.Axes.GetAncestors().Where(item => item.Template.HasBaseTemplate(rootTemplateId, true, true));
                return items.Any() ? items.FirstOrDefault() : null;
            }
        }

        /// <summary>
        /// Gets or sets the active icon
        /// this use the direct reference to master database. should be used in codes related to sitecore content editor
        /// </summary>
        public static Item ActiveIcon
        {
            get
            {
                return Sitecore.Configuration.Factory.GetDatabase("master").Items[Constants.IconPath + "active-icon"];
            }
        }

        /// <summary>
        /// Gets or sets the inactive icon
        /// this use the direct reference to master database. should be used in codes related to sitecore content editor
        /// </summary>
        public static Item InactiveIcon
        {
            get
            {
                return Sitecore.Configuration.Factory.GetDatabase("master").Items[Constants.IconPath + "inactive-icon"];
            }
        }

        /// <summary>
        /// Gets or sets the approved icon
        /// this use the direct reference to master database. should be used in codes related to sitecore content editor
        /// </summary>
        public static Item ApprovedIcon
        {
            get
            {
                return Sitecore.Configuration.Factory.GetDatabase("master").Items[Constants.IconPath + "approved-icon"];
            }
        }

        /// <summary>
        /// Gets or sets the unapproved icon
        /// this use the direct reference to master database. should be used in codes related to sitecore content editor
        /// </summary>
        public static Item UnapprovedIcon
        {
            get
            {
                return Sitecore.Configuration.Factory.GetDatabase("master").Items[Constants.IconPath + "unapproved-icon"];
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// return  item by drop Link field value of left string item 
        /// </summary>
        /// <param name="database">Database to read items</param>
        /// <param name="key">Item name of the key item in global/keys folder</param>
        /// <returns>Pointed Item by link field value of key</returns>
        public static Item GetItemByKey(this Database database, string key)
        {
            Diagnostics.Trace(DiagnosticsCategory.Common, string.Format("GetItemByKey" + "=" + key));
            // Diagnostics.Trace(DiagnosticsCategory.Common, string.Format("Context  Database" + "=" + database));

            Item contentItem = null;
            string keysFolderPath = ConfigurationHelper.GetConfigValue("AffinionGuids");
            if(string.IsNullOrEmpty(keysFolderPath))
            {
                keysFolderPath = "/sitecore/content/settings/guids";
            }

            Item keysFolder = database.GetItem(keysFolderPath);
            if (keysFolder == null)
            {
                Diagnostics.Trace(DiagnosticsCategory.Common, "No item found at : " + keysFolderPath);
                return null;
            }

            try
            {
                Item item = keysFolder.Axes.GetDescendants().Where(i => SitecoreFieldsHelper.GetValue(i, "Key") == key).FirstOrDefault();
                contentItem = SitecoreFieldsHelper.GetDropLinkFieldTargetItem(item, "Item");
                // Diagnostics.Trace(DiagnosticsCategory.Common, contentItem.Paths.ContentPath + " loaded by key : " + key);
            }
            catch (ArgumentNullException ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                throw;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                throw;
            }

            return contentItem;
        }

        /// <summary>
        /// Get the client rule item by its rule name
        /// </summary>
        /// <param name="ruleName">The rule name as defined at the admin portal global rules folder</param>
        /// <returns>Returns the rule item</returns>
        public static Item GetClientRule(string ruleName)
        {
            try
            {
                Item clientItem = SitecoreFieldsHelper.GetLookupFieldTargetItem(RootItem, "ClientDetails");
                var ruleItems = SitecoreFieldsHelper.GetMutiListItems(clientItem, "Client Level Rules").Where(i => SitecoreFieldsHelper.GetDropLinkFieldValue(i, "RuleName", "RuleName").ToLowerInvariant() == ruleName.ToLowerInvariant());

                if(ruleItems.Any())
                {
                    return ruleItems.FirstOrDefault();
                }

                return null;
            }
            catch(Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, new AffinionException(ruleName + " - exception occurred.", ex), null);
                return null;
            }
        }

        /// <summary>
        /// If the rule is selected in client level rules multilist, it will considered as turned on
        /// </summary>
        /// <param name="ruleName">The boolean rule name to check</param>
        /// <returns>Returns true if the rule is selected</returns>
        public static bool GetBooleanClientRule(string ruleName)
        {
            Item ruleItem = GetClientRule(ruleName);
            return ruleItem != null;
        }

        /// <summary>
        /// Add folder to the database
        /// </summary>
        /// <param name="database">Database to add item</param>
        /// <param name="parentItemPath">Path to parent content item</param>
        /// <param name="folderName">Name of the folder to be created</param>
        public static void AddFolder(this Database database, string parentItemPath, string folderName)
        {
            CreateItem(database, parentItemPath, FolderTemplatePath, folderName, false);
        }

        /// <summary>
        /// Add folder to the database
        /// </summary>
        /// <param name="database">Database to add item</param>
        /// <param name="parentItemPath">Path to parent content item</param>
        /// <param name="folderName">Name of the folder to be created</param>
        /// <param name="overrideIfExists">Override if the item already exists</param>
        public static void AddFolder(this Database database, string parentItemPath, string folderName, bool overrideIfExists)
        {
            CreateItem(database, parentItemPath, FolderTemplatePath, folderName, overrideIfExists);
        }

        /// <summary>
        /// Add item to the database
        /// </summary>
        /// <param name="database">Database to add item</param>
        /// <param name="parentItemPath">Path to parent content item</param>
        /// <param name="templatePath">Path to template item</param>
        /// <param name="childName">Name of the item to be created</param>
        public static void AddItem(this Database database, string parentItemPath, string templatePath, string childName)
        {
            CreateItem(database, parentItemPath, templatePath, childName, false);
        }

        /// <summary>
        /// Add item to the database
        /// </summary>
        /// <param name="database">Database to add item</param>
        /// <param name="parentItemPath">Path to parent content item</param>
        /// <param name="templatePath">Path to template item</param>
        /// <param name="childName">Name of the item to be created</param>
        /// <param name="overrideIfExists">Override if the item already exists</param>
        public static void AddItem(this Database database, string parentItemPath, string templatePath, string childName, bool overrideIfExists)
        {
            CreateItem(database, parentItemPath, templatePath, childName, overrideIfExists);
        }

        /// <summary>
        /// Method use to create media item in sitecore
        /// </summary>
        /// <param name="parentFolderPath"></param>
        /// <param name="newFolderName"></param>
        public static void AddMeidiaFolder(string parentFolderPath, string newFolderName)
        {
            try
            {
                Sitecore.Resources.Media.MediaCreatorOptions options = new Sitecore.Resources.Media.MediaCreatorOptions();
                /// Store the file in the database, not as a file
                options.FileBased = false;
                /// Remove file extension from item name
                options.IncludeExtensionInItemName = false;
                /// Overwrite any existing file with the same name
                options.KeepExisting = true;
                /// Do not make a versioned template
                options.Versioned = false;
                /// set the path            
                options.Destination = string.Concat(parentFolderPath, "/", newFolderName);
                /// Set the database
                options.Database = Sitecore.Configuration.Factory.GetDatabase(masterDatabase);

                /// Now create the folder
                Sitecore.Resources.Media.MediaCreator creator = new Sitecore.Resources.Media.MediaCreator();
                using (new SecurityDisabler()) // Use the SecurityDisabler object to override the security settings
                {
                    creator.CreateFromFolder(newFolderName, options);
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                throw;
            }
        }

        /// <summary>
        /// Method use to remove item in sitecore
        /// </summary>
        /// <param name="itemPath"></param>
        /// <param name="database"></param>
        public static void RemoveItem(string itemPath, Database database)
        {
            try
            {
                Item item = database.GetItem(itemPath);
                using (new SecurityDisabler()) // Use the SecurityDisabler object to override the security settings
                {
                    if (item != null && !item.ID.IsNull)
                        item.Delete();
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                throw;
            }
        }

        /// <summary>
        /// True if the template has the base template.
        /// </summary>
        /// <param name="me">The template for which to check
        /// the template and base templates.</param>
        /// <param name="templateID">The ID of the template
        /// for which to check.</param>
        /// <param name="includeSelf">Whether to check the
        /// template itself (false by default).</param>
        /// <param name="recursive">Whether to check base
        /// templates for base templates (true by default).</param>
        /// <returns>
        /// True if the template has the base template.
        /// </returns>
        public static bool HasBaseTemplate(this TemplateItem me, ID templateID, bool includeSelf = false, bool recursive = true)
        {
            if (includeSelf && me.ID == templateID)
            {
                return true;
            }

            if (recursive)
            {
                foreach (TemplateItem baseTemplate in me.BaseTemplates)
                {
                    if (baseTemplate.HasBaseTemplate(
                      templateID,
                      true /*includeSelf*/,
                      true /*recursive*/))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Set active status according to change of any of the supplier type change
        /// </summary>
        /// <param name="currentItem">Currently saving item</param>
        /// <param name="supplierTypeTemplateID">Supplier Type template ID</param>
        /// <param name="fieldName">Active field name</param>
        public static void ActivateSupplierType(Item currentItem, string supplierTypeTemplateID, string fieldName)
        {
            if (string.IsNullOrEmpty(supplierTypeTemplateID) || !string.Equals(currentItem.TemplateID.ToString(), supplierTypeTemplateID))
            {
                return;
            }
            ///Get the new value of the IsActive Field
            string fieldValue = currentItem.Fields[fieldName].Value;
            ///Two locations of supplier types
            ///Path of the supplier type created online
            string supplierTypePathCreatedList = string.Format("{0}/{1}", Constants.SupplierSetupPath, currentItem.Name);
            ///Path of the supplier type in master list
            string supplierTypePathMasterList = string.Format("{0}/global/_supporting-content/supplier-types/{0}", Constants.SupplierSetupPath, currentItem.Name);
            ///Created supplier type item
            Item matchingSupplierTypeCreated = Sitecore.Configuration.Factory.GetDatabase("master").GetItem(supplierTypePathCreatedList);
            ///Supplier type of the master list
            Item matchingSupplierTypeMaster = Sitecore.Configuration.Factory.GetDatabase("master").GetItem(supplierTypePathMasterList);

            if (matchingSupplierTypeCreated != null)
            {
                ///Update supplier type IsActive field
                UpdateFieldValue(matchingSupplierTypeCreated, fieldName, fieldValue);
                ///Update supplier type icon
                ChangeIconField(matchingSupplierTypeCreated, null, fieldName);
            }

            if (matchingSupplierTypeMaster != null)
            {
                ///Update supplier type IsActive field
                UpdateFieldValue(matchingSupplierTypeMaster, fieldName, fieldValue);
                ///Update supplier type icon
                ChangeIconField(matchingSupplierTypeMaster, null, fieldName);
            }
        }

        /// <summary>
        /// Change the icon of the item when set to active status
        /// </summary>
        /// <param name="item">Item to change the item</param>
        /// <param name="templateID">Template to filter item</param>
        /// <param name="fieldName">Field to check active status</param>
        /// <param name="isClientSetup">Client setup or supplier setup</param>
        public static void ChangeIconField(Item item, string templateID, string fieldName)
        {
            try
            {
                if (!string.Equals(item.Database.Name, "master"))
                {
                    return;
                }

                if (item.Name == "$name")
                {
                    return;
                }

                ID itemTemplate = item.TemplateID;
                string templateValue = itemTemplate.ToString();

                if (!string.IsNullOrEmpty(templateID) && !string.Equals(templateID, templateValue))
                {
                    return;
                }

                if (item.Fields[fieldName] == null)
                {
                    return;
                }
                ///Change the icon
                ChangeIcon(item, fieldName);
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                throw;
            }
        }

        /// <summary>
        /// Update given field with given field value
        /// </summary>
        /// <param name="itemToUpdate">Updating item</param>
        /// <param name="fieldName">Field name to update</param>
        /// <param name="fieldValue">Field value to set</param>
        public static void UpdateFieldValue(Item itemToUpdate, string fieldName, string fieldValue)
        {
            try
            {
                if (!string.Equals(itemToUpdate.Fields[fieldName].Value, fieldValue))
                {
                    itemToUpdate.Editing.BeginEdit();
                    itemToUpdate.Fields[fieldName].Value = fieldValue;//Update field value
                    itemToUpdate.Editing.EndEdit();
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                throw;
            }
        }

        #endregion

        #region private methods

        private static void CreateItem(Database database, string parentItemPath, string templatePath, string childName, bool overrideIfExists)
        {
            try
            {
                // get child item to check weather it is created
                var childItem = database.GetItem(string.Concat(parentItemPath, "/", childName));

                if (childItem != null && !overrideIfExists)
                {
                    return;
                }
                else if (childItem != null && overrideIfExists)
                {
                    DeleteItem(childItem);
                }

                //Get the patent item path
                Item parentItem = database.GetItem(parentItemPath);

                if (parentItem == null)
                {
                    throw new ArgumentNullException("parentItemPath", "Parent item not found at " + parentItemPath + ". Can not create child item - " + childName + ".");
                }

                //Retrive the templare from given path
                var template = database.GetItem(templatePath);

                if (template == null)
                {
                    throw new ArgumentNullException("templatePath", "Template not found at " + templatePath + ". Can not create child item - " + childName + ".");
                }

                //Add new item to parent item.
                if (templatePath.Contains("templates/Branches"))
                {
                    CreateItem(parentItem, childName, (BranchItem)template);
                }
                else
                {
                    CreateItem(parentItem, childName, (TemplateItem)template);
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                throw;
            }
        }

        private static void CreateItem(Item parent, string childName, TemplateItem template)
        {
            try
            {
                if (ContentEditingUser == null)
                {
                    throw new Exception("Conent Editing User not found.");
                }

                using (new Sitecore.Security.Accounts.UserSwitcher(ContentEditingUser))
                {
                    parent.Editing.BeginEdit();
                    parent.Add(childName, template);
                    parent.Editing.EndEdit();
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                throw;
            }
        }

        private static void CreateItem(Item parent, string childName, BranchItem branchTemplate)
        {
            try
            {
                if (ContentEditingUser == null)
                {
                    throw new Exception("Conent Editing User not found.");
                }

                using (new Sitecore.Security.Accounts.UserSwitcher(ContentEditingUser))
                {
                    parent.Editing.BeginEdit();
                    parent.Add(childName, branchTemplate);
                    parent.Editing.EndEdit();
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                throw;
            }
        }

        private static void DeleteItem(Item item)
        {
            try
            {
                if (ContentEditingUser == null)
                {
                    throw new Exception("Conent Editing User not found.");
                }

                using (new Sitecore.Security.Accounts.UserSwitcher(ContentEditingUser))
                {
                    item.Editing.BeginEdit();
                    item.Delete();
                    item.Editing.EndEdit();
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                throw;
            }
        }

        /// <summary>
        /// Change the icon of the given item
        /// </summary>
        /// <param name="item">Updating item</param>
        /// <param name="fieldName">Field name to check active status (IsActive)</param>
        /// <param name="activeIconPath">Active icon path</param>
        /// <param name="inactiveIconPath">InActive icon path</param>
        private static void ChangeIcon(Item item, string fieldName)
        {
            string fieldValue = item.Fields[fieldName].Value;

            using (new SecurityDisabler())
            {
                try
                {
                    item.Editing.BeginEdit();

                    if (fieldValue == "1")
                    {
                        SetIcon(item, ActiveIcon["__Icon"]);
                    }
                    else
                    {
                        SetIcon(item, InactiveIcon["__Icon"]);
                    }
                }
                finally
                {
                    item.Editing.EndEdit();
                    item.Editing.AcceptChanges();
                }
            }
        }

        /// <summary>
        /// Change the icon of the given item
        /// </summary>
        /// <param name="item">Item to update icon</param>
        /// <param name="path">Path of the icon</param>
        private static void SetIcon(Item item, string path)
        {
            if (!string.Equals(item.Fields["__icon"].Value, path))
            {
                item.Fields["__icon"].Value = path;
            }
        }

        #endregion
    }
}
