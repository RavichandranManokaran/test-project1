﻿CREATE TABLE [dbo].[uCommerce_DefinitionType] (
    [DefinitionTypeId] INT            NOT NULL,
    [Name]             NVARCHAR (512) NOT NULL,
    [Deleted]          BIT            NOT NULL,
    [SortOrder]        INT            CONSTRAINT [DF_uCommerce_DefinitionType_SortOrder] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_uCommerce_DefinitionType] PRIMARY KEY CLUSTERED ([DefinitionTypeId] ASC)
);

