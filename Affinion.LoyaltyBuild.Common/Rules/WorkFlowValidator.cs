﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
///Source File:           WorkFlowValidator.cs
///Sub-system/Module:     Affinion.LoyaltyBuild.Common
///Description:           Class for WorkFlow Aprrover Validator rule
///</summary>

#region using directives
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Validators;
using Sitecore.Workflows;
using System;
using System.Runtime.Serialization;
#endregion

namespace Affinion.LoyaltyBuild.Common.Rules
{
    [Serializable]
    public class WorkFlowValidator : StandardValidator
    {
        #region constructors
        public WorkFlowValidator()
        {

        }

        public WorkFlowValidator(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {

        }
        #endregion

        #region protected methods
        protected override ValidatorResult Evaluate()
        {
            try
            {
                Item item = base.GetItem();
                Database masterDB = Factory.GetDatabase("master");
                IWorkflow workflow = masterDB.WorkflowProvider.GetWorkflow(item);
                var contentHistory = workflow.GetHistory(item);
                string lastUser = contentHistory[contentHistory.Length - 1].User;

                string currentUser = Sitecore.Context.User.Name;

                if (lastUser == currentUser)
                {
                    return ValidatorResult.CriticalError;
                }

                return ValidatorResult.Valid;
            }
            catch(Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, this);
                throw;
            }
        }

        protected override ValidatorResult GetMaxValidatorResult()
        {
            return GetFailedResult(ValidatorResult.CriticalError);
        }
        #endregion

        #region public methods
        public override string Name
        {
            get { return "You can't Approve"; }
        }
        #endregion
    }
}
