﻿/*<summary>
Description: Stored procedure to purge Existing Bookings which has arrival date older than one year
</summary>*/
CREATE PROCEDURE [dbo].[PurgeExistingBookings]

	AS

	DECLARE @orderIdToDelete int
	DECLARE @orderLineIdToDelete int
	DECLARE @activeOrderLines int
	
	-- Select the Order Ids to be deleted (Arrival date older than one year)
	SELECT DISTINCT PO.OrderId into #orderIdTemp 
	  FROM [dbo].[uCommerce_PurchaseOrder] PO
	  Inner JOIN [dbo].[uCommerce_OrderLine] OL ON OL.OrderId = PO.OrderId 
	  Inner JOIN [dbo].uCommerce_RoomReservation RR ON RR.OrderLineId = OL.OrderLineId 
	  WHERE RR.CheckInDate < dateadd(year,-1,getdate())				--dateadd(dd,-23,getdate())

	WHILE exists (SELECT * from #orderIdTemp)
	BEGIN
				
		SELECT @orderIdToDelete = (SELECT top 1 orderid
						   FROM #orderIdTemp)

		SELECT @activeOrderLines = (SELECT count(RR.OrderLineId) 
									FROM [dbo].uCommerce_RoomReservation RR 
									Inner JOIN [dbo].uCommerce_OrderLine OL ON OL.OrderLineId = RR.OrderLineId 
									WHERE OL.OrderId = @orderIdToDelete 
									AND RR.CheckInDate >= dateadd(dd,-12,getdate())) --Set this to one year

		BEGIN TRANSACTION
			IF (@activeOrderLines = 0)
			/*  Order line items belonging to order is older than one year. Can delete the order line */
			BEGIN
				/* Get order lines to delete */
				SELECT DISTINCT OL.OrderLineId into #orderLineIdTemp 
				FROM [dbo].uCommerce_OrderLine OL 
				WHERE OL.OrderId = @orderIdToDelete

				While exists (Select * FROM #orderLineIdTemp)
				BEGIN
			
					SELECT @orderLineIdToDelete = (select top 1 OrderLineId
								   FROM #orderLineIdTemp)
			
					/* Delete from uCommerce_CommunicationHistory table */
					DELETE FROM [dbo].[uCommerce_CommunicationHistory] 
					WHERE OrderLineId = @orderLineIdToDelete

					/* Delete from uCommerce_CustomerQuery table */
					DELETE FROM [dbo].[uCommerce_CustomerQuery] 
					WHERE OrderLineId = @orderLineIdToDelete

					/* Delete from uCommerce_OrderLineDiscountRelation table */
					DELETE FROM [dbo].[uCommerce_OrderLineDiscountRelation] 
					WHERE OrderLineId = @orderLineIdToDelete

					/* Delete from uCommerce_OrderLineStatus table */
					DELETE FROM [dbo].[uCommerce_OrderLineStatus] 
					WHERE OrderLineId = @orderLineIdToDelete

					/* Delete from uCommerce_OrderProperty table */
					DELETE FROM [dbo].[uCommerce_OrderProperty] 
					WHERE OrderLineId = @orderLineIdToDelete

					/* Delete from uCommerce_ReservationNotes table */
					DELETE FROM [dbo].[uCommerce_ReservationNotes] 
					WHERE OrderLineId = @orderLineIdToDelete

					/* Delete from uCommerce_RoomReservation table */
					DELETE FROM [dbo].uCommerce_RoomReservation 
					WHERE OrderLineId = @orderLineIdToDelete

					/* Delete from uCommerce_OrderLine table */
					DELETE FROM [dbo].uCommerce_OrderLine 
					WHERE OrderLineId = @orderLineIdToDelete

					/* Delete from #orderLineIdTemp table */
					DELETE FROM #orderLineIdTemp
					WHERE OrderLineId = @orderLineIdToDelete
				END --End of while

				--Drop Temp Tables
				Drop Table #orderLineIdTemp
				
			/* Delete from uCommerce_OrderAddress table */
			DELETE FROM [dbo].[uCommerce_OrderAddress] 
			WHERE OrderId = @orderIdToDelete

			/* Delete from uCommerce_OrderStatusAudit table */
			DELETE FROM [dbo].[uCommerce_OrderStatusAudit] 
			WHERE OrderId = @orderIdToDelete

			/* Delete from Payment Property table all paymet property rows belonging to Payment done for the Order*/
			DELETE FROM [dbo].[uCommerce_PaymentProperty] 
			WHERE PaymentId in (Select PaymentId FROM uCommerce_Payment
								Where OrderId = @orderIdToDelete)
			
			
			/* Delete from uCommerce_Payment table */
			DELETE FROM [dbo].[uCommerce_Payment] 
			WHERE OrderId = @orderIdToDelete
			
			/* Delete from uCommerce_OrderProperty table */
			DELETE FROM [dbo].[uCommerce_OrderProperty] 
			WHERE OrderId = @orderIdToDelete
			
			/* Delete from uCommerce_PurchaseOrder table */
			DELETE FROM [dbo].[uCommerce_PurchaseOrder] 
			WHERE OrderId = @orderIdToDelete

			DELETE FROM #orderIdTemp
			WHERE OrderId = @orderIdToDelete

		END	--End If

		-- Rollback the transaction if there were any errors
		IF @@ERROR <> 0
		 BEGIN
			--Drop Temp Tables
			Drop Table #orderLineIdTemp
			Drop Table #orderIdTemp

			-- Rollback the transaction
			ROLLBACK

			-- Raise an error and return
			DECLARE @msg nvarchar(2048) = error_message()
			RAISERROR (@msg, 16, 1)
			RETURN
		 END

		--  Commit the deletion for selected Order Id
		COMMIT TRANSACTION
		
	END	--End While

	--Drop Temp Table
	 Drop Table #orderIdTemp

  GO