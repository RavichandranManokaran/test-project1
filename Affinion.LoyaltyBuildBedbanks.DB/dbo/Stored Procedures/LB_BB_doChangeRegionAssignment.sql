
/****** Object:  StoredProcedure [dbo].[LB_BB_doChangeRegionAssignment]    Script Date: 7/19/2016 7:23:45 PM ******/
---- ====================================================
---- Description:	RETURNS MARGIN FOR A PROPERTY BASED ON AVAILABILITY SOURCE
---- Written: RICHARD
---- Created: 2016.04.19
---- ====================================================

CREATE PROCEDURE [dbo].[LB_BB_doChangeRegionAssignment]
	
	-- Input parameters 
	@pi_regionid int,
	@pi_maplocationid int,
	@pi_status int,
	--Return parameters
	@po_returnstatus INT OUTPUT,
	@po_returnmsg VARCHAR(200) OUTPUT

	
AS
set nocount on;
BEGIN

	--Assume transaction will fail, set the return variable to -1.
	SET @po_returnstatus = -1;
	SET @po_returnmsg = null;

	
	
	BEGIN TRY
	
	IF @pi_status = 0 
	BEGIN
		Delete from LB_BB_mappings
		where MapfromRecordID = @pi_regionid
		and mapfrom = 'Location'
		and mapsFromColumn = 'Region'
		and mapsTo = 'Maplocation'
		and mapstoColumn = 'MaplocationID'
		and maptoRecordID = @pi_maplocationid
		
		Update LB_BB_Location
		Set isMatched = 3
		where RegionID = @pi_regionid
		
	END
	
	IF @pi_status = 1
	BEGIN
		Insert into LB_BB_mappings values ('Location',@pi_regionid,'Region','Maplocation',@pi_maplocationid,'MaplocationID')
	END
      
    END TRY	
	BEGIN CATCH	  
		SET @po_returnstatus = error_number();
		SET @po_returnmsg = error_message();
		RETURN;
	END CATCH
	
	SET @po_returnstatus = 0;
	SET @po_returnmsg = 'Completed successfully.';	
END














GO

