﻿<%@ control language="C#" autoeventwireup="true" codebehind="OfflinePortalTransferHistory.ascx.cs" inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalTransferHistory" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="col-md-12">
    <div class="accordian-outer">
        <div class="panel-group" id="accordion">
            <div class="panel panel-default">
                <div class="panel-heading bg-primary">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse0" class="show-hide-list"><sc:text id="TransferHistory" field="TransferHistoryText" runat="server"/></a>
                    </h4>
                </div>                
                <div id="divResult" class="panel-collapse collapse in" runat="server" >
                    <div class="panel-body alert-info">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel panel-default">
                                    <div>
                                        <div class="table-responsive">
                                            <asp:gridview class="table tbl-calendar" enableviewstate="false" id="TransferHistoryGridView" runat="server" ShowHeaderWhenEmpty="true"  autogeneratecolumns="false" Width="100%">
                                            <Columns>
                                                <%--<asp:BoundField DataField="SupplierName" HeaderText="Supplier Name" />
                                                <asp:BoundField DataField="ArrivalDate" HeaderText="Arrival Date" />
                                                <asp:BoundField DataField="DepartureDate" HeaderText="Departure Date" />
                                                <asp:BoundField DataField="NoOfRooms" HeaderText="No of Rooms" />
                                                <asp:BoundField DataField="NoOfNights" HeaderText="No of Nights" />
                                                <asp:BoundField DataField="NoOfPeople" HeaderText="No of People" />
                                                <asp:BoundField DataField="ReservationDate" HeaderText="Reservation Date" />--%>
                                            </Columns>
                                        </asp:gridview>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
