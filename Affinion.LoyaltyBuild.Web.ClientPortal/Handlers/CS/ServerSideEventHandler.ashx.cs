﻿using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Data;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Handlers.CS
{
    /// <summary>
    /// Summary description for ServerSideEventHandler
    /// </summary>
    public class ServerSideEventHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            switch (context.Request.QueryString["action"])
            {
                case "LoadLocationByCountry":
                    LoadLocationByCountry(context);
                    break;
                case "LoadStoreByClientID":
                    LoadStoreByClientID(context);
                    break;
                case "SetClientId":
                    SetClientId(context);
                    break;
                default:
                    break;
            }

        }

        private static void LoadLocationByCountry(HttpContext context)
        {
            var Country=context.Request.QueryString["value"];
            Database contextDb = Sitecore.Context.Database;
            var LocationItems = new List<string>();
            dynamic items = contextDb.GetItem(Constants.Location) != null ? contextDb.GetItem(Constants.Location).GetChildren() : null;
            if (items != null)
            {
                foreach (var item in items)
                {
                    if (SitecoreFieldsHelper.GetDropLinkFieldValue(item, "Country", "CountryName") == Country)
                    {
                        if (SitecoreFieldsHelper.CheckBoxChecked(item, "IsActive"))
                            LocationItems.Add(SitecoreFieldsHelper.GetValue(item, "ID") + "," + SitecoreFieldsHelper.GetValue(item, "Name"));
                    }
                }
            }

            var serializer = new JavaScriptSerializer();
            context.Response.Write(serializer.Serialize(LocationItems));
        }

        private static void LoadStoreByClientID(HttpContext context)
        {
           // Item clientItem = currentDB.GetItem(context.Request.QueryString["value"]);
            try
            {
               
                //query for the client item in admin portal
                Database currentDB = Sitecore.Context.Database;
                Item clientItem = Sitecore.Context.Database.GetItem(Constants.SupplierLocations + context.Request.QueryString["value"] + "");
                if (clientItem != null)
                {
                    //Bind data to drop-down
                    Collection<Item> items = SitecoreFieldsHelper.GetMutiListItems(clientItem, "Branches");
                   var itemBranch = SitecoreFieldsHelper.GetDropdownDataSource(items, "BranchLocationName", "BranchLocationName");

                   var serializer = new JavaScriptSerializer();
                   context.Response.Write(serializer.Serialize(itemBranch));
                }
            }
            catch (Exception ex)
            {

            }

        }

        private static void SetClientId(HttpContext context)
        {
            try
            {
                var clientCookies = context.Response.Cookies.Get("_clientid");
                if (clientCookies != null && string.IsNullOrEmpty(clientCookies.Value))
                    context.Response.Cookies.Set(new HttpCookie("_clientid", context.Request.QueryString["value"]));
            }
            catch (Exception ex)
            {

            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}