﻿CREATE TABLE [dbo].[LB_SupplierOfferUnSubScribe] (
    [SupplierOfferUnSubScribeId] INT            IDENTITY (1, 1) NOT NULL,
    [Reason]                     NVARCHAR (500) NOT NULL,
    [CategoryGuid]               NVARCHAR (500) NOT NULL,
    [CampaignItem]               INT            NOT NULL,
    [CreatedDate]                DATETIME       NULL,
    [CreatedBy]                  NVARCHAR (100) NULL,
    CONSTRAINT [PK_uCommerce_SupplierOfferUnSubScribe] PRIMARY KEY CLUSTERED ([SupplierOfferUnSubScribeId] ASC)
);

