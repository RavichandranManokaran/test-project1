﻿CREATE PROCEDURE [dbo].[LB_SaveOrderLineDue] 
(
    -- Add the parameters for the stored procedure here
	@OrderLineDueId INT = NULL,
	@OrderLineID INT,
	@BookingId uniqueidentifier = NULL,
    @CurrencyTypeId INT,
    @DueTypeId uniqueidentifier,
    @TotalAmount MONEY ,
    @CommissionAmount MONEY ,
    @ProviderAmount MONEY,
	@ProcessingFeeAmount MONEY,
	@Vat MONEY =NULL,
	@Note VARCHAR(MAX),
	@CreatedBy VARCHAR(50),
	@CreatedDate DATETIME,
	@PaymentMethodId INT
)
AS
BEGIN
	DECLARE @Result INT

	IF(@BookingId IS NULL)
	BEGIN
		SELECT @BookingId = BookingId FROM LB_BookingInfo WHERE OrderLineId = @OrderLineID
	END

	IF(@OrderLineDueId IS NULL)
	BEGIN
		--Insert a new Due
		INSERT INTO [dbo].[LB_OrderLineDue]
			(
			[OrderLineID]
			,[BookingId]
			,[CurrencyTypeId]
			,[DueTypeId]
			,[TotalAmount]
			,[CommissionAmount]
			,[ProviderAmount]
			,[ProcessingFeeAmount]
			,[VatAmount]
			,[Note]
			,[CreatedDate]
			,[IsDeleted]
			,[PaymentMethodId]
			)
		VALUES
			(
			@OrderLineID,
			@BookingId,
			@CurrencyTypeId,
			@DueTypeId,
			@TotalAmount,
			@CommissionAmount,
			@ProviderAmount,
			@ProcessingFeeAmount,
			@Vat,
			@Note, 
			GETDATE(),
			0,
			@PaymentMethodId
			)
	
		SET @Result=SCOPE_IDENTITY();
	END
	ELSE
	BEGIN
		--Update Due
		UPDATE [dbo].LB_OrderLineDue SET
			OrderLineId=@OrderLineID,
			BookingId=@BookingId,	
			DueTypeId=@DueTypeId,
			CurrencyTypeId=@CurrencyTypeId,
			TotalAmount=@TotalAmount,
			CommissionAmount=@CommissionAmount,
			ProviderAmount=@ProviderAmount,
			ProcessingFeeAmount=@ProcessingFeeAmount,
			VatAmount=ISNULL(@Vat,0),
			Note=@Note,
			UpdatedBy=@CreatedBy,
			UpdatedDate=GETDATE(),
			PaymentMethodID = @PaymentMethodId
		WHERE
			OrderLineDueId = @OrderLineDueId

		SET @Result =@OrderLineDueId;
	END
	
	--Return ID
	SELECT @Result
END