﻿CREATE TABLE [dbo].[uCommerce_CategoryTarget] (
    [CategoryTargetId] INT            NOT NULL,
    [Name]             NVARCHAR (MAX) NOT NULL,
    [CategoryGuids]    NVARCHAR (MAX) DEFAULT (NULL) NULL,
    CONSTRAINT [PK_uCommerce_CategoryTarget] PRIMARY KEY CLUSTERED ([CategoryTargetId] ASC),
    CONSTRAINT [FK_uCommerce_CategoryTarget_uCommerce_CategoryTarget] FOREIGN KEY ([CategoryTargetId]) REFERENCES [dbo].[uCommerce_Target] ([TargetId])
);



