﻿using Affinion.LoyaltyBuild.AdminPortal.Model;
using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.Reports
{
    public partial class RealexMisMatchReportSublayout : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            MisMatchData mmd = new MisMatchData();
            mmd.ReadFile();
            //uCommerceConnectionFactory.StoreRealexReport(mmd.realexData);.
            DataTable report = mmd.CompareData();
            gvTestTable.DataSource = report;
            gvTestTable.DataBind();
        }
    }
}