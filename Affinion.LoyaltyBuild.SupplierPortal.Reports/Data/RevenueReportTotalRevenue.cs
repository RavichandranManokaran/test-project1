﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.SupplierPortal.Reports.Data
{
    /// <summary>
    /// Contains details for the items of revenue report item
    /// </summary>
    public class RevenueReportTotalRevenue : RevenueBase
    {

    }

    /// <summary>
    /// Collection to manage Revenue Report
    /// </summary>
    public class RevenueReportTotalRevenueCollection:IEnumerable<RevenueReportTotalRevenue>,IList<RevenueReportTotalRevenue>
    {
        //Private variables declaration
        private List<RevenueReportTotalRevenue> _items;

        /// <summary>
        /// Constructor
        /// </summary>
        public RevenueReportTotalRevenueCollection()
        {
            _items = new List<RevenueReportTotalRevenue>();
        }

        /// <summary>
        /// Add method to add item
        /// </summary>
        /// <param name="item"></param>
        public void Add(RevenueReportTotalRevenue item)
        {
            _items.Add(item);
        }

        /// <summary>
        /// Override method IEnumenrable class
        /// </summary>
        /// <returns></returns>
        public IEnumerator<RevenueReportTotalRevenue> GetEnumerator()
        {
            foreach (var item in _items)
                yield return item;
        }

        /// <summary>
        /// Override method IEnumenrable class
        /// </summary>
        /// <returns></returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            foreach (var item in _items)
                yield return item;
        }

        public int IndexOf(RevenueReportTotalRevenue item)
        {
            throw new NotImplementedException();
        }

        public void Insert(int index, RevenueReportTotalRevenue item)
        {
            throw new NotImplementedException();
        }

        public void RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        public RevenueReportTotalRevenue this[int index]
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }


        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(RevenueReportTotalRevenue item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(RevenueReportTotalRevenue[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public int Count
        {
            get { return _items.Count; }
        }

        public bool IsReadOnly
        {
            get { throw new NotImplementedException(); }
        }

        public bool Remove(RevenueReportTotalRevenue item)
        {
            throw new NotImplementedException();
        }
    }
}
