﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SuperValuHotelInfoSublayout.ascx.cs
/// Sub-system/Module:     Web.ClientPortal(VS project name)
/// Description:           Sublayout for supervalu hotel info page
/// </summary>
namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts
{
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Sitecore.Data.Items;
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Web.UI.WebControls;
    using UCommerce.Api;
    using UCommerce.EntitiesV2;
    using UCommerce.EntitiesV2.Definitions;
    using Sitecore.Data.Fields;
    using Affinion.LoyaltyBuild.Common.Instrumentation;

    /// <summary>
    /// Sublayout for supervalu hotel info page
    /// </summary>
    public partial class SuperValuHotelInfoSublayout : BaseSublayout
    {
        /// <summary>
        /// This function includes the code which needs to run on page load
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The parameter</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Item item = this.GetDataSource();
                item = item == null ? Sitecore.Context.Item : item;

                DataBindControls(item);

                Item hotelItem = Sitecore.Context.Database.GetItem(new Sitecore.Data.ID("{5831F8BA-1F3F-4F8B-8EDF-92989B443B49}"));
                ReferenceField storeItemField = (ReferenceField)hotelItem.Fields["StoreItem"];

                if (storeItemField != null)
                {
                    Item id = storeItemField.TargetItem;
                    string skuName = ((MultilistField)id.Fields["Products"]).GetItems()[0].Fields["sku"].Value;
                    Product hotel = CatalogLibrary.GetProduct(skuName);
                    var hotelCategories = hotel.GetCategories().Where(product => product.Name == hotel.Name).ToList();
                    this.DemoText.Text = hotel.Name;

                    if (hotelCategories.Any())
                    {
                        Category hotelCategory = hotelCategories[0];

                        var availabilityCategories = hotelCategory.Categories.Where(cat => cat.Name == "Availability").ToList();

                        if (availabilityCategories.Any())
                        {
                            Category availabilityCategory = availabilityCategories[0];

                            foreach (Product product in availabilityCategory.Products)
                            {
                                this.DemoText.Text += (", " + product.Name);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, ex.Message);
                throw;
            }
        }

        #region Private Methods
        /// <summary>
        /// Data bind controls
        /// </summary>
        /// <param name="item">Sitecore item</param>
        private void DataBindControls(Item item)
        {
            if (item != null)
            {
                this.ShortcutButtons.DataSource = SitecoreFieldsHelper.GetMutiListItems(item, "ShortcutButtons");
                this.ShortcutButtons.DataBind();

                this.HotelsList.DataSource = SitecoreFieldsHelper.GetMutiListItems(item, "AvailableHotels");
                this.HotelsList.ItemDataBound += HotelsList_ItemDataBound;
                this.HotelsList.DataBind();
            }
        }

        /// <summary>
        /// Binds data to the facilities repeater to display available facilities of each hotel
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The arguments</param>
        private void HotelsList_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            try
            {
                Item item = e.Item.DataItem as Item;
                Repeater repeater = e.Item.FindControl("HotelFacilities") as Repeater;
                Literal packagePrice = e.Item.FindControl("PackagePrice") as Literal;

                if (item != null && repeater != null && packagePrice != null)
                {
                    repeater.DataSource = SitecoreFieldsHelper.GetMutiListItems(item, "Facilities");
                    repeater.DataBind();

                    ReferenceField storeItemField = (ReferenceField)item.Fields["StoreItem"];

                    if (storeItemField != null)
                    {
                        Item id = storeItemField.TargetItem;
                        string skuName = ((MultilistField)id.Fields["Products"]).GetItems()[0].Fields["sku"].Value;
                        Product hotel = CatalogLibrary.GetProduct(skuName);
                        var packages = hotel.Variants.ToList();

                        if (packages.Count() > 0)
                        {
                            // set price
                            PriceCalculation price = CatalogLibrary.CalculatePrice(packages[0]);
                            if (price != null && price.YourPrice != null)
                            {
                                packagePrice.Text = price.YourPrice.Amount.ToString();
                            }
                            else
                            {
                                packagePrice.Text = "-";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, ex.Message);
                throw;
            }
        }
        
        #endregion
    }
}