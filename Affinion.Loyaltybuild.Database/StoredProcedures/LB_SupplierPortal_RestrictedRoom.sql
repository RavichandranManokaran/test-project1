﻿CREATE PROCEDURE [dbo].[LB_SupplierPortal_RestrictedRoom] 
															@DateFrom DATE,
															@DateTo DATE,
															@Provider INT
  AS
  BEGIN
    
	select hotelid, hotelname, offerid, offername, datesrestricted from
(
	select hotelid, hotelname, offerid, offername, datesrestricted, count(*) as [Count] from 
	(
		SELECT  
		Category.CategoryId [HotelID],
			Category.Name [HotelName],
			CampaignItem.CampaignItemId [OfferID],
			CampaignItem.Name [OfferName],
			CAST('1/1/1990' AS Datetime) + offeravailability.DateCounter [DatesRestricted]	
		FROM LB_OfferAvailability offeravailability
			inner join uCommerce_CampaignItem CampaignItem on offeravailability.CampaignItem=CampaignItem.CampaignItemId
			inner join uCommerce_Product Product on offeravailability.ProviderOccupancyTypeID=Product.sku
			inner join uCommerce_CategoryProductRelation CategoryProductRelation on Product.ProductId=CategoryProductRelation.ProductId
			inner join uCommerce_Category Category on CategoryProductRelation.CategoryId=Category.CategoryId
		WHERE
			CampaignItem.[Enabled] = 1 AND
			CampaignItem.Deleted = 0 AND
			offeravailability.NumberAllocated >0 AND
			(( CAST('1/1/1990' AS Datetime) + DateCounter between @Datefrom and @Dateto) or (CAST('1/1/1990' AS Datetime) + DateCounter between @Datefrom and @Dateto)) AND Category.CategoryId=@Provider
	) a
	GROUP BY hotelid, hotelname, offerid, offername, datesrestricted
)b
END



