﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           LanguageReader.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Code is used to read key values and return corresponding text value.
/// </summary>

#region
using System;
using Sitecore.Data.Items;
using Sitecore.Globalization;
using Affinion.LoyaltyBuild.Common.Instrumentation;
#endregion

namespace Affinion.LoyaltyBuild.Common.Utilities
{
    public static class LanguageReader
    {
        #region Private Properties
        //private string defaultText;
        //private string resultText;
        #endregion

        //Constructor
        public static string GetText(string key, string defaultValue)
        {
            try
            {
                //get the current item
                Item item = Sitecore.Context.Item;

                return SearchText(item, key, defaultValue);
            }
            catch (Exception ex)
            {
                //log the error and throw 
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                throw;
            }
        }

        public static string GetText(Item item, string key, string defaultValue)
        {
            try
            {
                return SearchText(item, key, defaultValue);
            }
            catch (Exception ex)
            {
                //log the error and throw 
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                throw;
            }
        }

        #region Private Methods
        /// <summary>
        /// Search the text for the given item or key. if does not exist return the default value.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="keyValue"></param>
        /// <param name="defaultTextValue"></param>
        private static string SearchText(Item item, string keyValue, string defaultTextValue)
        {
            string resultText = string.Empty;
            //defaultText = defaultTextValue;
            Item contentFolder = null;

            if (item != null)
            {
                //get the item url
                string path = Sitecore.Links.LinkManager.GetItemUrl(item);
                //check whether a folder named "content" exists within that item
                contentFolder = Sitecore.Context.Database.GetItem("fast:" + path + "/content");
            }
            resultText = LocateAndGetText(keyValue, contentFolder);
            //if the folder does not exist, return the default value
            //resultText = string.IsNullOrEmpty(resultText) ? defaultTextValue : resultText;
            resultText = (string.IsNullOrEmpty(resultText) || string.Equals(resultText, keyValue)) ? defaultTextValue : resultText;

            return resultText;
        }

        /// <summary>
        /// Search for the location to retrieve the text and gives the output
        /// </summary>
        /// <param name="textKey"></param>
        /// <param name="contentFolder"></param>
        /// <returns>text value</returns>   
        private static string LocateAndGetText(string textKey, Item contentFolder)
        {
            string text = string.Empty;
            if (contentFolder == null)
            {
                return text = GetTextFromDictionary(textKey);
            }
            else
            {
                //retrieve path to the content folder
                string path = Sitecore.Links.LinkManager.GetItemUrl(contentFolder);
                //search whether the item which contains value of textKey exists.
                Item targetItem = Sitecore.Context.Database.GetItem("fast:" + path + "/" + textKey);

                if (targetItem == null)
                {
                    return text = GetTextFromDictionary(textKey);
                }
                else
                {
                    //TODO: Need to double check with the assigned field name for the value
                    text = targetItem.Fields["Text"].ToString();
                    return text;
                }
            }
        }

        /// <summary>
        /// Returns the value for the given key
        /// </summary>
        /// <param name="textKey"></param>
        private static string GetTextFromDictionary(string textKey)
        {
            string dictionaryText = string.Empty;
            dictionaryText = Translate.Text(textKey);
            return dictionaryText;
        }
        #endregion


        #region Public Methods
        /// <summary>
        /// Return the result text
        /// </summary>
        /// <returns>resultText</returns>
        //public static string DisplayText()
        //{
        //    try
        //    {
        //        //This will add trace information to the log
        //        Diagnostics.Trace(DiagnosticsCategory.Common, "Laguage Reader started");
        //        return resultText;
        //    }
        //    catch (Exception ex)
        //    {
        //        //log the error and throw 
        //        Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
        //        throw;
        //    }

        //}
        #endregion
    }
}
