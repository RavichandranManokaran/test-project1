﻿
///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           JsonCommon.cs
/// Sub-system/Module:     Client Setup(VS project name)
/// Description:           Json common Classes to format the json data
/// </summary>

#region Using Directives
using System.Collections.ObjectModel;
#endregion

namespace Affinion.LoyaltyBuild.SupplierSetup.CurrencyConverter
{
        /// <summary>
        /// 
        /// </summary>
        public class Column
        {
            public string QuoteCurrency { get; set; }
            public decimal Rate { get; set; }
        }
        /// <summary>
        /// 
        /// </summary>
        public class Line
        {
            public string BaseCurrency { get; set; }
            public Collection<Column> Columns { get; set; }
        }
        /// <summary>
        /// 
        /// </summary>
        public class RootObject
        {
            public string Outcome { get; set; }
            public object Message { get; set; }
            public string Identity { get; set; }
            public double Delay { get; set; }
            public Collection<Line> Lines { get; set; }
        }
    }

