﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Script.Serialization;

using System.Linq;
using System.Web;
using Affinion.LoyaltyBuild.Search.Data;
using Affinion.LoyaltyBuild.Api.Booking.Data.Basket;
using Affinion.LoyaltyBuild.Model.Product;
using Affinion.LoyaltyBuild.Web.ClientPortal.Helper;
using System.Globalization;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.UCom.Common.Extension;
using Affinion.Loyaltybuild.BusinessLogic;
using Affinion.LoyaltyBuild.Web.ClientPortal.ViewModels;
using Newtonsoft.Json;
using Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi;
using Affinion.LoyaltyBuild.Model.Common;
using Affinion.LoyaltyBuild.Common.Exceptions;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Handlers.CS
{
    /// <summary>
    /// Summary description for AddtoBasket
    /// </summary>
    public class AddtoBasket : IHttpHandler
    {
        
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/json";
            var response = new ValidationResult();

            try
            {
                string strJson = new StreamReader(context.Request.InputStream).ReadToEnd();

                var roomType = JsonConvert.DeserializeObject<BasketRequest>(strJson);

                IProduct product = null;
                IAddToBasketRequest request = null;

                switch (roomType.RoomType)
                {
                    case ProductType.Room:
                        product = new Room();
                        request = JsonConvert.DeserializeObject<AddRoomRequest>(strJson);
                        break;
                    case ProductType.Package:
                        product = new Package();
                        request = JsonConvert.DeserializeObject<AddRoomRequest>(strJson);
                        break;
                    case ProductType.Addon:
                        product = new Addon();
                        request = JsonConvert.DeserializeObject<AddOnRequest>(strJson);
                        break;
                    case ProductType.BedBanks:
                        product = new BedBankProduct();
                        request = JsonConvert.DeserializeObject<AddBedBanksRequest>(strJson);
                        break;
                }

                if (request == null)
                    throw new AffinionException("Invalid add to basket request");

                product.Type = roomType.RoomType;
                product.Sku = request.Sku;
                product.VariantSku = request.VariantSku;
                int requiredRooms = request.NoOfRooms;
                request.NoOfRooms = 1;
                if (Sitecore.Security.Accounts.User.Current != null)
                    request.CreatedBy = Sitecore.Security.Accounts.User.Current.LocalName;

                for (int i = 0; i < requiredRooms; i++)
                    Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.AddToBasket(product, 1, request);

                
                response.IsSuccess = true;
            }
            catch (AffinionException affEx)
            {
                response.IsSuccess = false;
                response.Message = affEx.Message;
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("BasketError", ex, this);
                response.IsSuccess = false;
                response.Message = "Error adding items to the basket";
            }

            var serializer = new JavaScriptSerializer();
            context.Response.Write(serializer.Serialize(response));
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public class JSONOBJECTS
        {
            public string DateCounter { get; set; }
            public string CheckInDate { get; set; }
            public string Destination { get; set; }
            public string NoOfAdults { get; set; }
            public string CheckoutDate { get; set; }
            public string NoOfChildren { get; set; }
            public string NoOfNights { get; set; }
            public string NoOfRooms { get; set; }
            public string CreatedBy { get; set; }
            public string Sku { get; set; }
            public string VarianceSku { get; set; }
            public string ProductType { get; set; }
            public string Price { get; set; }
            public string RoomCount { get; set; }
            public string BookingThrough { get; set; }

        }

        // public void Deserialize<T>(string context)
        //{
        //    string jsonData = context;

        //    //cast to specified objectType
        //    var obj = (T)new JavaScriptSerializer().Deserialize<T>(jsonData);
        //    return obj;
        //}

    }
}
