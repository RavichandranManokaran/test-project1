﻿using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
using System.Data;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Customer;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Data;
using Affinion.LoyaltyBuild.Common.CardValidationCheck;
namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    public partial class OfflinePortalSearchCustomerQueryEditPage : BaseSublayout
    {
        public const string PlsEnterNotes = "Pls Enter Notes anto";
        
        private QueryService _customerQueryService = new QueryService();
        public CustomerQuery queryData = new CustomerQuery();
        protected void Page_Load(object sender, EventArgs e)
        {

            Item dataSourceitem = this.GetDataSource();
            if (!Page.IsPostBack)
            {
                if (queryData != null)
                {
                    txtPartner.Value = queryData.ClientName;
                    queryData.Content = queryData.Content.Replace("\\r\\n", Environment.NewLine).Replace("//r//n", Environment.NewLine);
                    txtNotes.Value = CredictCardCheck.CredictCard(queryData.Content);
                    txtProvider.Value = queryData.ProviderName;
                    txtPartner.Value = queryData.ClientName;
                    lblReferenceNo.Value = queryData.BookingReference;
                    hdquerysolved.Value = (queryData.IsSolved ? 1 : 0).ToString();
                    hdquerytype.Value = queryData.QueryTypeId.ToString();
                    if (queryData.IsSolved)
                    {
                        rdryes.Checked = true;
                    }
                    else
                    {
                        rdryno.Checked = true;
                    }
                    if (!queryData.HasAttachment)
                    {
                        btnViewFile.Style.Add("display", "none"); ;
                        btnDelete.Style.Add("display", "none"); ;
                    }
                }
                BindData(dataSourceitem);
            }
            //txtNotes.Value = ob.Content.ToString();
            //IsResolved.SelectedValue = ob.IsSolved.ToString();
        }

        private void BindData(Item dataSourceitem)
        {
            BindSitecoreFieldData(dataSourceitem);
            BindQueryType();
            //  BindClientDropDown();
            // BindCompaignGroup(dataSourceitem);
            // BindProvider();
        }

        private void BindSitecoreFieldData(Item dataSourceitem)
        {

            Database currentDB = Sitecore.Configuration.Factory.GetDatabase("web");
            Sitecore.Context.Item = currentDB.SelectItems("/sitecore/content/#offline-portal#/#home#//*[@@TemplateName='EditCustomerQuery']").ToList().FirstOrDefault();
            
            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "QueryDetailHeading", EditCustomerQueryTitleText);
            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "EnquiryType", EnquiryType);
            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "Partner", Partner);
            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "Provider", Provider);
            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "IsResolved", IsResolved);
            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "ReferenceNo", ReferenceNo);
            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "Notes", Notes);
            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "AddNote", AddNote);

            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "CloseButton", CloseText);
            //SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "FileUploadButton", FileUploadText);
            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "ViewFileButton", ViewFileText);
            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "DeleteFileButton", DeleteFileText);

            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "UpdateButton", UpdateText);
            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "ResetButton", ResetText);

        }

        private void BindQueryType()
        {
            DataTable customerQueryTypes = null;
            customerQueryTypes = _customerQueryService.GetQueryTypes();
            ddlEnquiryType.DataSource = customerQueryTypes;
            ddlEnquiryType.DataValueField = "QueryTypeId";
            ddlEnquiryType.DataTextField = "QueryType";
            ddlEnquiryType.DataBind();
            if (queryData.QueryTypeId != 0)
            {
                ddlEnquiryType.SelectedValue = queryData.QueryTypeId.ToString();
            }
        }


    }
}