﻿ /*
 © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
 PROPRIETARY INFORMATION The information contained herein (the 
 'Proprietary Information') is highly confidential and proprietary to and 
 constitutes trade secrets of Affinion International. The Proprietary Information 
 is for Affinion International use only and shall not be published, 
 communicated, disclosed or divulged to any person, firm, corporation or 
 other legal entity, directly or indirectly, without the prior written 
 consent of Affinion International.
 */
namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    /// <summary>
    /// Entity to hold the accomodation information like name and address
    /// This data comes from the sitecore
    /// </summary>
    public class Accomodation : IAccomodation
    {
        /// <summary>
        /// Title of the hotel
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Hotel's Sitecore Item Id
        /// </summary>
        public string HotelItemId { get; set; }

        /// <summary>
        /// Address Line 1 of Provider
        /// </summary>
        public string AddressLine1 { get; set; }

        /// <summary>
        /// Address Line 2 of Provider
        /// </summary>
        public string AddressLine2 { get; set; }

        /// <summary>
        /// Address Line 3 of Provider
        /// </summary>
        public string AddressLine3 { get; set; }

        /// <summary>
        /// Address Line 4 of Provider
        /// </summary>
        public string AddressLine4 { get; set; }
        
        /// <summary>
        /// Town part of the Provider's address
        /// </summary>
        public string Town { get; set; }

        /// <summary>
        /// Country part of the Provider's address
        /// </summary>
        public string CountryName { get; set; }

        /// <summary>
        /// Complete Adddress of the hotel
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Phone
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string EmailId { get; set; }

        /// <summary>
        /// Postal code of the Provider
        /// </summary>
        public string Pincode { get; set; }

        /// <summary>
        /// Introduction Image of Provider
        /// </summary>
        public string ImageUrl { get; set; }

        /// <summary>
        /// GeoCoordinates of the provider
        /// </summary>
        public string GeoCoordinates { get; set; }
    }
}
