﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class FixPriceAwardMap : SubclassMap<FixPriceAward>
    {
        // Methods
        public FixPriceAwardMap()
        {
            base.Table("LB_FixPriceAward");
            base.KeyColumn("FixPriceAwardId");
            base.Map(x => x.FixPriceAwardId).ReadOnly();
            base.Map(x => x.AmountOff).Not.Nullable();
        }

    }

}
