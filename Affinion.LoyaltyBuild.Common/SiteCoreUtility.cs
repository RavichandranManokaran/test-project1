﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Common
{
    public class SiteCoreUtility
    {
        public static Sitecore.Data.Database GetCurrentDatabase()
        {
            return Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
        }

        public static Sitecore.Data.Database GetMasterDatabase()
        {
            return Sitecore.Configuration.Factory.GetDatabase("master");
        }

    }
}
