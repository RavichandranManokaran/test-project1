﻿CREATE TABLE [dbo].[uCommerce_UserGroupPermission] (
    [PermissionId] INT IDENTITY (1, 1) NOT NULL,
    [UserGroupId]  INT NULL,
    [RoleId]       INT NULL,
    PRIMARY KEY CLUSTERED ([PermissionId] ASC),
    FOREIGN KEY ([RoleId]) REFERENCES [dbo].[uCommerce_Role] ([RoleId]),
    FOREIGN KEY ([UserGroupId]) REFERENCES [dbo].[uCommerce_UserGroup] ([UserGroupId])
);

