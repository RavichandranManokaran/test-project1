﻿/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/// <summary>
/// Entity to hold UnClaimed Receipts related information
/// </summary>
namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model
{
    public class UnClaimedReceipts
    {
        /// <summary>
        /// Category Id
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// Provider Name
        /// </summary>
        public string ProviderName { get; set; }

        /// <summary>
        ///Currency Type Id
        /// </summary>
        public int CurrencyTypeId { get; set; }

        /// <summary>
        ///Currency Code
        /// </summary>
        public string ISOCode { get; set; }

        /// <summary>
        /// Amount
        /// </summary>
        public double Amount { get; set; }

        /// <summary>
        /// Month of of Last Booking
        /// </summary>
        public string Month { get; set; }

        /// <summary>
        /// Year of Last Booking
        /// </summary>
        public string Year { get; set; }

        /// <summary>
        /// Factor
        /// </summary>
        public double AmountInEuro { get; set; }

        /// <summary>
        /// Record Count
        /// </summary>
        public int Count { get; set; }

    }
}
