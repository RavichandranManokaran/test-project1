﻿using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
using Affinion.LoyaltyBuild.AdminPortal.Model;
using Sitecore.Links;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration.SiteUI.Base;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration.SiteUI.Presentation;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;



namespace Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.Reports
{
    public partial class ReportsMainMenu : BaseSublayout
    {
       
        Item HomeItem;
        /// <summary>
        /// page load event 
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            //Calling LoadMenu Function
            //LoadMenu();


        }

        /// <summary>
        /// Report Menu
        /// </summary>
        private void LoadMenu()
        {

            HomeItem = this.GetDataSource();
            if (HomeItem != null)
            {
                List<Item> nodes = new List<Item>();
                if (HomeItem["Show Item In Menu"] == "1") nodes.Add(HomeItem);
             
                this.Mainmenu.DataSource = nodes;
                this.Mainmenu.DataBind();
            }
        }

        protected void MainmenuItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Item node = (Item)e.Item.DataItem;

                HyperLink MenuLink = (HyperLink)e.Item.FindControl("MenuLink");
                Literal MenuText = (Literal)e.Item.FindControl("MenuText");
                HtmlControl MenuLi = (HtmlControl)e.Item.FindControl("MenuLi");

                if (MenuLink != null && MenuText != null)
                {
                    MenuText.Text = node["Menu Title"];
                    MenuLink.NavigateUrl = LinkManager.GetItemUrl(node);
                    if (node.ID == Sitecore.Context.Item.ID)
                    {
                        MenuLi.Attributes.Add("class", "active");
                    }

                    if (node["Show Children In Menu"] == "1" && node.HasChildren && node.ID == HomeItem.ID)
                    {
                        MenuLi.Attributes.Add("class", "dropdown");
                        MenuLink.Attributes.Add("class", "dropdown-toggle");
                        MenuLink.Attributes.Add("data-toggle", "dropdown");
                        MenuText.Text += "  <b class=\"caret\"></b>";

                        PlaceHolder phSubTree = (PlaceHolder)e.Item.FindControl("phSubMenu");

                        List<Item> nodes = new List<Item>();
                        foreach (Item i in node.Children)
                        {
                            if (SiteConfiguration.DoesItemExistInCurrentLanguage(i) && i["Show Item In Menu"] == "1") { nodes.Add(i); }
                        }

                        Repeater rpt = new Repeater();
                        rpt.DataSource = nodes;
                        rpt.HeaderTemplate = new TopBarRecursiveRepeaterTemplate(ListItemType.Header);
                        rpt.ItemTemplate = Mainmenu.ItemTemplate;
                        rpt.FooterTemplate = new TopBarRecursiveRepeaterTemplate(ListItemType.Footer);
                        rpt.ItemDataBound += new RepeaterItemEventHandler(MainmenuItemDataBound);
                        phSubTree.Controls.Add(rpt);
                        rpt.DataBind();
                    }
                }
            }
        }


    }
}