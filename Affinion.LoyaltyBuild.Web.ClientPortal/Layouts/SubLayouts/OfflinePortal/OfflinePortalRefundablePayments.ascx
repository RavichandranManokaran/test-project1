﻿<%@ control language="C#" autoeventwireup="true" codebehind="OfflinePortalRefundablePayments.ascx.cs" inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalRefundablePayments" %>
<%if(PaymentItems!=null){ %>
<div class="table-responsive">
    <table id="refundablepayments" class="table tbl-calendar clickable">
        <thead>
            <tr>
                <th>Date</th>
                <th>Payment Method</th>
                <th>Amount</th>
                <th>Available Amount</th>
                <th>ReferenceId</th>
            </tr>
        </thead>
        <tbody>
            <%foreach(var item in PaymentItems) {%>
            <tr data-ref="<%=item.ReferenceId %>" data-availamt="<%=item.AvailableAmount %>">
                <td><%=item.CreatedDate %></td>
                <td><%=item.PaymentMethod %></td>
                <td><%=item.Amount %></td>
                <td><%=item.AvailableAmount %></td>
                <td><%=item.ReferenceId %></td>
            </tr>
            <%} %>
        </tbody>
    </table>
</div>
<%} %>