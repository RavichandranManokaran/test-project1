﻿using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using Affinion.LoyaltyBuild.UCom.MasterClass.Website.Configuration;
using Affinion.LoyaltyBuild.UCom.MasterClass.Website.CustomSiteCoreDataType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.Presentation.Views.Marketing;
using UCommerce.Presentation.Web.Controls;
using UCommerce.Presentation.Web.Marketing;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.Ucommerce.Marketing.Targets
{
    public partial class PortalTypeUi : ViewEnabledControl<IEditCampaignItemView>, ITargetUi
    {
        List<string> datatypeList;

        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (datatypeList == null)
            //{
            //    DataType portalType = DataType.All().Where(x => x.TypeName == DataTypeConstant.portalType).ToList().FirstOrDefault();
            //    if (portalType != null)
            //    {
            //        datatypeList = portalType.DataTypeEnums.ToList();
            //    }
            //    datatypeList.Insert(0, new DataTypeEnum() { Value = "" });
            //}
            datatypeList = SiteConfiguration.GetSiteCoreItem(CustomDataTypeConstant.PortalType);
            drpPortalType.Items.Clear();
            drpPortalType.Items.Add("Select Portal");
            if (datatypeList != null)
            {
                foreach (var item in datatypeList)
                {
                    drpPortalType.Items.Add(new ListItem() { Text = item, Value = item });
                }
            }
            PortalTypeTarget portalTypeData = this.DataSource as PortalTypeTarget;
            this.DeleteButton.OnClientClick = string.Format("return confirm('Delete');");
            if (datatypeList.Any(x => x == portalTypeData.PortalType))
            {
                drpPortalType.SelectedValue = portalTypeData.PortalType;
            }
            this.ReadOnlyMessage.Text = this.TargetToString(portalTypeData);
        }

        /// <summary>
        /// Save portal detail
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveButton_Click(object sender, ImageClickEventArgs e)
        {
            PortalTypeTarget portalTypeData = this.DataSource as PortalTypeTarget;
            portalTypeData.PortalType = drpPortalType.SelectedValue;
            portalTypeData.Save();
            this.ReadOnlyMessage.Text = this.TargetToString(portalTypeData);
            this.ToggleEditor();
        }

        /// <summary>
        /// enable edit & display view in page
        /// </summary>
        private void ToggleEditor()
        {
            this.NonEditModePlaceHolder.Visible = !this.NonEditModePlaceHolder.Visible;
            this.EditButton.Visible = !this.EditButton.Visible;
            this.EditModePlaceHolder.Visible = !this.EditModePlaceHolder.Visible;
            this.SaveButton.Visible = !this.SaveButton.Visible;
        }

        /// <summary>
        /// View portaltype Act
        /// </summary>
        /// <param name="PortalTypeTarget"></param>
        /// <returns></returns>
        private string TargetToString(PortalTypeTarget PortalTypeTarget)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendFormat("Portal type ");
            stringBuilder.AppendFormat("{0} ", PortalTypeTarget.PortalType);
            return stringBuilder.ToString();
        }

        /// <summary>
        /// Delete portal type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteButton_Click(object sender, ImageClickEventArgs e)
        {
            PortalTypeTarget portalTypeData = this.DataSource as PortalTypeTarget;
            if (portalTypeData != null)
            {
                portalTypeData.Delete();
            }
            this.Page.Response.Redirect(this.Page.Request.Url.ToString(), true);
        }

        /// <summary>
        /// Edit portal type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditButton_Click(object sender, ImageClickEventArgs e)
        {
            this.ToggleEditor();
        }

        /// <summary>
        /// get set for apply
        /// </summary>
        public bool EnabledForApply
        {
            get;
            set;
        }

        /// <summary>
        /// get set for display
        /// </summary>
        public bool EnabledForDisplay
        {
            get;
            set;
        }

        /// <summary>
        /// Get set for datasource
        /// </summary>
        public object DataSource
        {
            get;
            set;

        }

        /// <summary>
        /// Create new portal Type
        /// </summary>
        /// <returns></returns>
        public object New()
        {
            var portalTypeTarget = PortalTypeTarget.FirstOrDefault(x => x.CampaignItem.CampaignItemId == View.CampaignItem.CampaignItemId) ?? new PortalTypeTarget();
            try
            {
                portalTypeTarget.CampaignItem = View.CampaignItem;
                portalTypeTarget.EnabledForApply = true;
                portalTypeTarget.PortalType = string.Empty;
                portalTypeTarget.Save();
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.MasterClassWebsite, ex, this);
                throw;
            }
            this.DataSource = portalTypeTarget;
            return portalTypeTarget;
        }
    }
}