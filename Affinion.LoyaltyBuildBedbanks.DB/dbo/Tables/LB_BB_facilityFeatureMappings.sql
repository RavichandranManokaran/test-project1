
/****** Object:  Table [dbo].[LB_BB_facilityFeatureMappings]    Script Date: 7/19/2016 7:59:20 PM ******/

CREATE TABLE [dbo].[LB_BB_facilityFeatureMappings](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[_availabilityProviderID] [int] NOT NULL,
	[FacilityID] [int] NOT NULL,
	[FeatureID] [int] NOT NULL,
 CONSTRAINT [PK__FacilityFeatureMappings] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[LB_BB_facilityFeatureMappings]  WITH CHECK ADD  CONSTRAINT [FK__facilityFeatureMappings__availabilityProvider1] FOREIGN KEY([_availabilityProviderID])
REFERENCES [dbo].[LB_BB_availabilityProvider] ([ID])
GO

ALTER TABLE [dbo].[LB_BB_facilityFeatureMappings] CHECK CONSTRAINT [FK__facilityFeatureMappings__availabilityProvider1]
GO

