﻿CREATE PROCEDURE [dbo].[LB_SetBookingReferenceNumber]
	@OrderLineId as int,
	@BookingReference as varchar(50)
AS
BEGIN
	
	DECLARE @OrderId INT

	UPDATE [dbo].[LB_BookingInfo] Set BookingNo=@BookingReference
	WHERE OrderLineId = @OrderLineId

	--Insert into Order Property
	SELECT @OrderId = OrderId FROM uCommerce_OrderLine WHERE OrderLineId = @OrderLineId

	IF NOT EXISTS(SELECT TOP 1 OrderId FROM uCommerce_OrderProperty Where OrderId = @OrderId AND
	OrderLineId = @OrderLineId AND [KEY] = '_BookingReference')
		INSERT INTO uCommerce_OrderProperty Values(@OrderId, @OrderLineId,'_BookingReference',@BookingReference)
	ELSE
		UPDATE uCommerce_OrderProperty SET [Value] = @BookingReference WHERE OrderId = @OrderId AND OrderLineId = @OrderLineId
		AND [KEY] = '_BookingReference' 
	
END