﻿using Affinion.LoyaltyBuild.Api.Search.Data;
using Affinion.LoyaltyBuild.Model.Provider;
using System.Collections.Generic;
using System.Linq;

namespace Affinion.LoyaltyBuild.Api.Search.Helpers
{
    /// <summary>
    /// class for Provider Mergering
    /// </summary>
    internal static class ProviderMerger
    {
        /// <summary>
        /// Method for Merge Results
        /// </summary>
        /// <param name="collections"></param>
        /// <param name="clientId"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public static IList<SearchResult> MergeResults(IList<ProviderCollection> collections, string clientId)
        {
            var result = new List<SearchResult>() { };
            
            //implement any client based merger rules here
            if (collections != null)
            {
                result.AddRange(collections
                    .Where(i => i != null && i.CollectionType == ProviderType.LoyaltyBuild)
                    .SelectMany(i => (List<SearchResult>)i));

                var bedbanks = FilerMatchingBedbanks(result, collections
                    .Where(i => i != null && i.CollectionType == ProviderType.BedBanks)
                    .SelectMany(i => (List<SearchResult>)i));

                result.AddRange(bedbanks);
            }

            return result;
        }

        /// <summary>
        /// Logic to Remove Matching bed banks hotels
        /// </summary>
        /// <param name="collections"></param>
        /// <returns></returns>
        public static IEnumerable<SearchResult> FilerMatchingBedbanks(IList<SearchResult> loyaltyHotels, IEnumerable<SearchResult> bedbanks)
        {
            return bedbanks.Where(i => !loyaltyHotels.Any(j => j.SitecoreItemId == i.SitecoreItemId)).ToList();
        }
    }
}
