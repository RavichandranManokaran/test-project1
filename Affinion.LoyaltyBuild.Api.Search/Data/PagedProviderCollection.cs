﻿
using Affinion.LoyaltyBuild.Model.Provider;
using System;
using System.Collections.Generic;
namespace Affinion.LoyaltyBuild.Api.Search.Data
{
    /// <summary>
    /// Paged provider collection
    /// </summary>
    [Serializable]
    public class PagedProviderCollection : List<SearchResult>, IProviderCollection<SearchResult>
    {

        /// <summary>
        /// gets service type
        /// </summary>
        public ProviderType CollectionType
        {
            get { return ProviderType.Mixed; }
        }

        /// <summary>
        /// Total pages returned
        /// </summary>
        public int TotalPages
        {
            get
            {
                return (int)Math.Ceiling(((decimal)Count) / PageSize);
            }
        }

        /// <summary>
        /// Page size
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// Sort field
        /// </summary>
        public SortField SortOn { get; set; }

        /// <summary>
        /// Sort direction
        /// </summary>
        public SortDirection SortDirection { get; set; }
    }
}
