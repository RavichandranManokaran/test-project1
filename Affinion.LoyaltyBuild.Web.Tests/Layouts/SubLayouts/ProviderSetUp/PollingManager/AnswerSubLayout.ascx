﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AnswerSubLayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.Test.Layouts.SubLayouts.ProviderSetup.PollingManager.AnswerSubLayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<sc:Text Field="Answer" runat="server" />
<sc:Text Field="TotalPollCount" runat="server" />