﻿CREATE TABLE [dbo].[EventQueue] (
    [Id]            UNIQUEIDENTIFIER NOT NULL,
    [EventType]     NVARCHAR (256)   NOT NULL,
    [InstanceType]  NVARCHAR (256)   NOT NULL,
    [InstanceData]  NVARCHAR (MAX)   NOT NULL,
    [InstanceName]  NVARCHAR (128)   NOT NULL,
    [RaiseLocally]  INT              NOT NULL,
    [RaiseGlobally] INT              NOT NULL,
    [UserName]      NVARCHAR (128)   NOT NULL,
    [Stamp]         ROWVERSION       NOT NULL,
    [Created]       DATETIME         CONSTRAINT [DF_EventQueue_Created] DEFAULT (getutcdate()) NOT NULL
);


GO
CREATE CLUSTERED INDEX [IX_Stamp]
    ON [dbo].[EventQueue]([Stamp] ASC);

