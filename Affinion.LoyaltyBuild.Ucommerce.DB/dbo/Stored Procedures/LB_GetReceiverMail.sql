﻿CREATE PROCEDURE [dbo].[LB_GetReceiverMail]( @Id int )
AS

	SELECT TOP 10  OA.EmailAddress
	FROM 	[DBO].[UCOMMERCE_ORDERLINE] L
	INNER JOIN  [uCommerce_OrderAddress] OA  ON L.OrderId = OA.OrderId
	WHERE L.ORDERID = @Id

