﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using System;
using Affinion.LoyaltyBuild.UCom.Common.Extension;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using Affinion.LoyaltyBuild.Common.Instrumentation;
/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.CampaignTarget
{
    /// <summary>
    /// Service that provides methods to perform all Campaign Targets related actions
    /// </summary>
    public class CampaignTargets : ICampaignTarget
    {
        /// <summary>
        /// GetArrivalDateTargetByCampaignId
        /// </summary>
        /// <param name="campaignItemId">campaignItemId</param>
        /// <returns><list>CampaginItemDateRange</list>GetArrivalDateTargetByCampaignId</returns>
        public List<CampaginItemDateRange> GetArrivalDateTargetByCampaignId(int campaignItemId)
        {
            List<CampaginItemDateRange> cItemDateRange = new List<CampaginItemDateRange>();
            List<ArrivalDateTarget> arrivalDate = ArrivalDateTarget.All().Where(x => x.CampaignItem.CampaignItemId == campaignItemId).ToList();
            foreach (var item in arrivalDate)
            {
                cItemDateRange.Add(new CampaginItemDateRange() { EndDate = item.ArrivalToDate.ConvertDateToString(), StartDate = item.ArrivalFromDate.ConvertDateToString() });
            }
            return cItemDateRange;
        }

        /// <summary>
        /// GetReservaionDateTargetByCampaignId
        /// </summary>
        /// <param name="campaignItemId">campaignItemId</param>
        /// <returns><list>CampaginItemDateRange</list>GetReservaionDateTargetByCampaignId</returns>
        public List<CampaginItemDateRange> GetReservaionDateTargetByCampaignId(int campaignItemId)
        {
            List<CampaginItemDateRange> cItemDateRange = new List<CampaginItemDateRange>();
            List<ReservationDateTarget> resDate = ReservationDateTarget.All().Where(x => x.CampaignItem.CampaignItemId == campaignItemId).ToList();
            foreach (var item in resDate)
            {
                cItemDateRange.Add(new CampaginItemDateRange() { EndDate = item.ReservationToDate.ConvertDateToString(), StartDate = item.ReservationFromDate.ConvertDateToString() });
            }
            return cItemDateRange;

        }

        /// <summary>
        /// GetClientTargetByCampaignId
        /// </summary>
        /// <param name="campaignItemId">campaignItemId</param>
        /// <returns><list>ClientList</list>GetClientTargetByCampaignId</returns>
        public List<ClientList> GetClientTargetByCampaignId(int campaignItemId)
        {
            List<ClientList> clientList = new List<ClientList>();
            List<ClientTarget> cTarget = ClientTarget.All().Where(x => x.CampaignItem.CampaignItemId == campaignItemId).ToList();
            foreach (var item in cTarget)
            {
                foreach (var s in item.ClientGUID.Split('|'))
                {
                    clientList.Add(new ClientList() { ClientId = s });
                }
            }
            return clientList;
        }

        /// <summary>
        /// Get applicable no of nights for campaign
        /// </summary>
        /// <param name="campaignItemId">Campaign item id</param>
        /// <returns>No of nights collection</returns>
        public ICollection<int> GetNightTargetByCampaignId(int campaignItemId)
        {
            try
            {
                ///No of nights for matching campaign
                List<int> nightList = NightTarget.All().Where(x => x.CampaignItem.CampaignItemId == campaignItemId).Select(a => a.Night).ToList();

                return nightList;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.MasterClassBusinessLogic, ex, this);
                throw;
            }
        }
    }
}
