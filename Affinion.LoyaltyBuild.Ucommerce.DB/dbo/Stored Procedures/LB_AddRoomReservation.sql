﻿--CREATE  PROCEDURE [dbo].[LB_AddRoomReservation]
--	@OrderLineId as int,
--	@CheckinDate as datetime,
--	@CheckOutDate as datetime,
--	@BookingThrough as varchar(20),
--	@CreatedByName as varchar(50) =NULL

--AS
--BEGIN
--IF not EXISTS (SELECT 1 FROM dbo.LB_RoomReservation WHERE OrderLineId = @OrderLineId)
--	BEGIN
--		INSERT INTO [dbo].LB_RoomReservation
--			( 
--				OrderLineId,
--				CheckinDate,
--				CheckOutDate,
--				--OrderReference,
--				BookingThrough,
--				CreatedBy,
--				CreatedDate
--			)
--			VALUES
--			(
--				@OrderLineId,
--				@CheckinDate,
--				@CheckOutDate,
--				@BookingThrough,
--				@CreatedByName,
--				Getdate()
--			)
--			EXEC [LB_AddBookingStatus] @OrderLineId = @OrderLineId, @Status = 1, @CreatedBy = @CreatedByName
--	END
--END





CREATE  PROCEDURE [dbo].[LB_AddRoomReservation]
	@OrderLineId as int,
	@OrderId as int,
	@CheckinDate as datetime,
	@CheckOutDate as datetime,
	@BookingThrough as varchar(20),
	@CreatedByName as varchar(50) =NULL

AS
BEGIN
IF not EXISTS (SELECT 1 FROM dbo.LB_BookingInfo WHERE OrderLineId = @OrderLineId)
	BEGIN
		INSERT INTO [dbo].LB_BookingInfo
			( 
				OrderLineId,
				CheckinDate,
				CheckOutDate,
				--OrderReference,
				--BookingThrough,
				CreatedBy,
				CreatedDate
			)
			VALUES
			(
				@OrderLineId,
				@CheckinDate,
				@CheckOutDate,
				--@BookingThrough,
				@CreatedByName,
				Getdate()
			)

		INSERT INTO [dbo].uCommerce_OrderProperty
			(	 
				OrderId,
				OrderLineId,
				[Key],
				[Value]
			)
			Values
			(
				@OrderId,
				@OrderLineId,
				'_BookingingThrough',
				@BookingThrough
			)

			EXEC [LB_AddBookingStatus] @OrderLineId = @OrderLineId, @Status = 1, @CreatedBy = @CreatedByName
	END
END