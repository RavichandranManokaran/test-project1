﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.DataAccess.DatabaseScripts
{
    public class AuditTrailScripts : AbstractDatabaseScript
    {
        //public override string GetConnectionStringName()
        //{
        //    return "TestDB";
        //}

        protected override void AddScripts()
        {
            AffinionScriptData tableScript = new AffinionScriptData();
            tableScript.ScriptName = "LB_AuditTrail";
            tableScript.ScriptType = AffinionScriptType.TableCreation;
            tableScript.Script = @"CREATE TABLE [dbo].[LB_AuditTrail](
	                                            [Id] [int] IDENTITY(1,1) NOT NULL,
	                                            [AuditName] [nvarchar](100) NOT NULL,
	                                            [Message] [nvarchar](300) NULL,
	                                            [Operation] [nvarchar](100) NOT NULL,
	                                            [ObjectType] [nvarchar](100) NOT NULL,
	                                            [ObjectId] [nvarchar](100) NULL,
	                                            [Section] [nvarchar](100) NOT NULL,
	                                            [OldValue] [nvarchar](800) NULL,
	                                            [NewValue] [nvarchar](800) NULL,
	                                            [Username] [nvarchar](100) NOT NULL,
	                                            [Date] [datetime] NOT NULL,
	                                            [Time] [datetime] NOT NULL,
	                                            [AdditionalData] [nvarchar](800) NULL,
	                                            [Remarks] [nvarchar](300) NULL
                                            ) ON [PRIMARY]";

            AffinionScriptData addSpScript = new AffinionScriptData();
            addSpScript.ScriptName = "LB_AuditLogMessage";
            addSpScript.ScriptType = AffinionScriptType.SPCreation;
            addSpScript.Script = @"CREATE PROCEDURE [dbo].[LB_AuditLogMessage] 
	                                            @AuditName nvarchar(100),
	                                            @Message nvarchar(300) = NULL,
	                                            @Operation nvarchar(100),
	                                            @ObjectType nvarchar(100),
	                                            @ObjectId nvarchar(100) = NULL,
	                                            @Section nvarchar(100),
	                                            @OldValue nvarchar(800) = NULL,
	                                            @NewValue nvarchar(800) = NULL,
	                                            @Username nvarchar(100),
	                                            @Date datetime,
	                                            @Time datetime,
	                                            @AdditionalData nvarchar(800) = NULL,
	                                            @Remarks nvarchar(300) = NULL
                                            AS
                                            BEGIN
	                                            -- SET NOCOUNT ON added to prevent extra result sets from
	                                            -- interfering with SELECT statements.
	                                            SET NOCOUNT ON;

                                                -- Insert statements for procedure here
	                                            INSERT INTO [dbo].[LB_AuditTrail]
	                                            (
	                                            [AuditName],
	                                            [Message],
	                                            [Operation],
	                                            [ObjectType],
	                                            [ObjectId],
	                                            [Section],
	                                            [OldValue],
	                                            [NewValue],
	                                            [Username],
	                                            [Date],
	                                            [Time],
	                                            [AdditionalData],
	                                            [Remarks]
	                                            )
	                                            VALUES 
	                                            (
	                                            @AuditName,
	                                            @Message,
	                                            @Operation,
	                                            @ObjectType,
	                                            @ObjectId,
	                                            @Section,
	                                            @OldValue,
	                                            @NewValue,
	                                            @Username,
	                                            @Date,
	                                            @Time,
	                                            @AdditionalData,
	                                            @Remarks)
                                            END";

            AffinionScriptData readSpScript = new AffinionScriptData();
            readSpScript.ScriptName = "LB_AuditReadLogMessage";
            readSpScript.ScriptType = AffinionScriptType.SPCreation;
            readSpScript.Script = @"CREATE PROCEDURE [dbo].[LB_AuditReadLogMessage] 
	                                @AuditName nvarchar(100) = NULL,
	                                @Operation nvarchar(100) = NULL,
	                                @ObjectType nvarchar(100) = NULL,
	                                @ObjectId nvarchar(100) = NULL,
	                                @Section nvarchar(100) = NULL,
	                                @TextValue nvarchar(300) = NULL,
	                                @Username nvarchar(100) = NULL,
	                                @StartDateTime datetime = NULL,
	                                @EndDateTime datetime = NULL
                                AS
                                BEGIN
	                                -- SET NOCOUNT ON added to prevent extra result sets from
	                                -- interfering with SELECT statements.
	                                SET NOCOUNT ON;

                                    SELECT [Id]
                                      ,[AuditName]
                                      ,[Message]
                                      ,[Operation]
                                      ,[ObjectType]
                                      ,[ObjectId]
                                      ,[Section]
                                      ,[OldValue]
                                      ,[NewValue]
                                      ,[Username]
                                      ,[Date]
                                      ,[Time]
                                      ,[AdditionalData]
                                      ,[Remarks]
                                  FROM [dbo].[LB_AuditTrail]
                                  WHERE (@AuditName IS NULL OR @AuditName = '' OR  DATALENGTH(@AuditName) = 0 OR [AuditName]=@AuditName)
                                  AND (@Operation IS NULL OR @Operation = '' OR DATALENGTH(@Operation) = 0 OR [Operation]=@Operation)
                                  AND (@ObjectType IS NULL OR @ObjectType = '' OR DATALENGTH(@ObjectType) = 0 OR [ObjectType]=@ObjectType)
                                  AND (@ObjectId IS NULL OR @ObjectId = '' OR DATALENGTH(@ObjectId) = 0 OR [ObjectId]=@ObjectId)
                                  AND (@Section IS NULL OR @Section = '' OR DATALENGTH(@Section) = 0 OR [Section]=@Section)
                                  AND (@TextValue IS NULL OR @TextValue = '' OR DATALENGTH(@TextValue) = 0 OR [OldValue] like '%'+@TextValue+'%' OR [NewValue] like '%'+@TextValue+'%' OR [AdditionalData] like '%'+@TextValue+'%' OR [Remarks] like '%'+@TextValue+'%')
                                  AND (@Username IS NULL OR @Username = '' OR DATALENGTH(@Username) = 0 OR [Username]=@Username)
                                  AND (@StartDateTime IS NULL OR @StartDateTime = '' OR DATALENGTH(@StartDateTime) = 0 OR [Time]>=@StartDateTime)
                                  AND (@EndDateTime IS NULL OR @EndDateTime = '' OR DATALENGTH(@EndDateTime) = 0 OR [Time]<=@EndDateTime)
                               END";
            

            this.scripts.Add(tableScript);
            this.scripts.Add(addSpScript);
            this.scripts.Add(readSpScript);
        }
    }
}
