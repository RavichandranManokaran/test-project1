﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TestNotificationsSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.Tests.Layouts.SubLayouts.ClientSetup.Notifications.TestNotificationsSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<!--Adding references to jquery libraries.-->
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<!--script for generating the popup-->
<script>
  $(function() {
      $("#notificationDiv").dialog();
  });
  </script>

<div ID="notificationDiv" >
 <!--Label retrieves the assigned field value of the particular Sitecore item.-->
    <asp:Label ID="Label1" runat="server" />
</div>
