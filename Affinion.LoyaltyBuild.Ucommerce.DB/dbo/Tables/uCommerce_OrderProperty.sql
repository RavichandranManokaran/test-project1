﻿CREATE TABLE [dbo].[uCommerce_OrderProperty] (
    [OrderPropertyId] INT            IDENTITY (1, 1) NOT NULL,
    [OrderId]         INT            NOT NULL,
    [OrderLineId]     INT            NULL,
    [Key]             NVARCHAR (255) NOT NULL,
    [Value]           NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_uCommerce_OrderProperty] PRIMARY KEY CLUSTERED ([OrderPropertyId] ASC),
    CONSTRAINT [FK_uCommerce_OrderProperty_uCommerce_OrderLine] FOREIGN KEY ([OrderLineId]) REFERENCES [dbo].[uCommerce_OrderLine] ([OrderLineId]),
    CONSTRAINT [FK_uCommerce_OrderProperty_uCommerce_PurchaseOrder] FOREIGN KEY ([OrderId]) REFERENCES [dbo].[uCommerce_PurchaseOrder] ([OrderId])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_uCommerce_OrderProperty]
    ON [dbo].[uCommerce_OrderProperty]([Key] ASC, [OrderId] ASC, [OrderLineId] ASC);


GO
CREATE NONCLUSTERED INDEX [uCommerce_OrderProperty_OrderLineId]
    ON [dbo].[uCommerce_OrderProperty]([OrderLineId] ASC)
    INCLUDE([OrderPropertyId], [OrderId], [Key], [Value]);

