﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    public class ProviderShortage
    {
        public string ProviderId{get;set;}
        public string SKU { get; set; }
        public IEnumerable<ShortageRoom> RoomDeatls{get;set;}
    }

    public class ShortageRoom
    {
        public string ProviderID { get;set; }
        public string SKU { get; set; }
        public string RoomType { get; set; }
        public string month { get; set; }
        public string Day { get; set; }   
    
    }


    public class ShortageRoomData
    {
        public string VariantSku { get; set; }
        public string ProviderID { get;set; }
        public string SKU { get; set; }
        public string RoomType { get; set; }
        public DateTime Date { get; set; }
    }
}
