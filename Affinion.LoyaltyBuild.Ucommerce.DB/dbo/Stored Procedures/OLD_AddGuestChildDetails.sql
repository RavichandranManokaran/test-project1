﻿CREATE PROCEDURE [dbo].[OLD_AddGuestChildDetails]
	
	@OrderLineId as int,
	@Age as int

AS
BEGIN
-- Delete all records for particular orderline id
delete from [dbo].[LB_RoomReservationAgeInfo] where [OrderLineId] = @OrderLineId

	INSERT INTO [dbo].[LB_RoomReservationAgeInfo]
           ([OrderLineId]
           ,[Age])
     VALUES
           (@OrderLineId, @Age)
	END

