﻿CREATE TABLE [dbo].[uCommerce_OrderLineStatus]
(
                [OrderLineStatusId] INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
                [OrderLineId] INT NOT NULL, 
				[StatusId] INT NOT NULL,
                [CreatedBy] VARCHAR(50) NOT NULL,
                [CreatedDate] DATETIME NOT NULL
)