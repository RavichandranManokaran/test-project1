﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TermsAndConditionSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal.TermsAndConditionSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="detail-hotel margin-bottom-15">
    <div class="detail-hotel-top"></div>
      <div class="col-xs-12">
               <ul class="info-top-link list-group">
                        <li class="width-full display-block-val"><a class="list-group-item alert-info" href="#terms">Terms & Conditions  </a> </li>
                        <li class="width-full display-block-val"><a class="list-group-item alert-info" href="#prices">Break Prices</a> </li>

                     </ul>
                
            </div>


     <div class="col-xs-12">  
                    <div class="content_bg_inner_box light-box" id="terms">
                               <h2>Terms & Conditions </h2>
                              <sc:Placeholder ID="Placeholder2" Key="tandctab" runat="server" />
                   </div>
            <div class="content_bg_inner_box light-box" id="prices" >
                               <h2>Break Prices</h2>
                                 <sc:Placeholder ID="Placeholder1" Key="breakpricetab" runat="server" />
                </div>
              </div>





<%--    <div class="tab-content">
        <div id="tandc" class="tab-pane fade active in">
            <div class="location tabDetail">
                   <sc:Placeholder ID="tandctab" Key="tandctab" runat="server" />
            </div>
        </div>
        <div id="breakprice" class="tab-pane fade">
            <div class="tabDetail">
                       <sc:Placeholder ID="breakpricetab" Key="breakpricetab" runat="server" />
            </div>
        </div>

    </div>--%>
</div>
