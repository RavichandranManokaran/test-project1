﻿CREATE TABLE [dbo].[LB_OrderLinePayment] (
    [OrderLinePaymentID] INT            IDENTITY (1, 1) NOT NULL,
    [OrderLineID]        INT            NOT NULL,
    [BookingId]    UNIQUEIDENTIFIER   NOT NULL,
    [CurrencyTypeId]     INT            NOT NULL,
    [PaymentMethodID]    INT            NOT NULL,
    [TotalAmount]                     MONEY         NOT NULL,
    [CommissionAmount]           MONEY         NOT NULL,
    [ProviderAmount]             MONEY         NOT NULL,
    [ProcessingFeeAmount] MONEY         NOT NULL,
    [OrderPaymentID]     INT            NOT NULL,
    [ReferenceID]        NVARCHAR (100) NOT NULL,
    [CreatedBy]          NVARCHAR (50)   NOT NULL,
	[CreatedDate]        DATETIME       NOT NULL,
    PRIMARY KEY CLUSTERED ([OrderLinePaymentID] ASC)
);

