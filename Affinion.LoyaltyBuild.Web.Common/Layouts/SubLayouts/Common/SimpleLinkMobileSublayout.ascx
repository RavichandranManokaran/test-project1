﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SimpleLinkMobileSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.Common.Layouts.SubLayouts.Common.SimpleLinkMobileSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>


<asp:Literal ID="Prefix" runat="server" />
<p id="PlaceholderText" runat="server">Set the datasource: Required Fields: Link</p>
    <div class="basket float-right header-right hidden-xs favorite">
        <span class="fa fa-heart white-font"></span> 
        <sc:Link Field="Link" ID="TextLink" runat="server"></sc:Link>
               
    </div>

<asp:Literal ID="Suffix" runat="server" />