﻿--********************************************************************************************************************************************************************************
--CREATE STORED PROCEDURE SCRIPTS For AGENT PERFORMANCE
--********************************************************************************************************************************************************************************
CREATE PROCEDURE [dbo].[LB_GetAgentPerformance] (@createdby VARCHAR(MAX),@ClientIds varchar(MAX), @fromdate  DATE, @todate    DATE) 
AS 
  BEGIN 

  --To Drop If Already GetAgentPerformance Temp table Exist
  IF OBJECT_ID('tempdb..#TempGetAgentPerformance') IS NOT NULL DROP TABLE #TempGetAgentPerformance

  --Create GetAgentPerformance Temp Table & Create Index
  Create table #TempGetAgentPerformance (Createdby varchar(50),CreatedDate datetime,orderlineid int)

  create index TempGAP_OrderLineIdIndex1 on #TempGetAgentPerformance(orderlineid)
   

  --Dump the Booking Info into the Temp Table with the Given Clause
  insert into #TempGetAgentPerformance
   SELECT BI.createdby, BI.CreatedDate, BI.orderlineid
              FROM   LB_BookingInfo BI
              WHERE  BI.createdby IN (SELECT * FROM   dbo.Splitstring(@createdby, ','))  AND
                     BI.createddate BETWEEN @fromdate and @todate

 --Remove All the Records which are not made via offline 
	delete from #TempGetAgentPerformance
	where orderlineid not in (
	SELECT        T.OrderLineId
	FROM            uCommerce_OrderLine AS O INNER JOIN
							 uCommerce_OrderProperty AS P ON O.OrderLineId = P.OrderLineId CROSS JOIN
							 [#TempGetAgentPerformance] AS T
	WHERE (P.[Key] = '_BookingThrough') AND (P.Value = 'Offline') AND (O.OrderLineId = T.OrderLineId))

--Remove All the Records which are not Belonging to provided ClientID's
		delete from #TempGetAgentPerformance
		where OrderLineId  not in (
		SELECT        T.OrderLineId
		FROM            uCommerce_OrderLine AS O INNER JOIN
								 uCommerce_OrderProperty AS P ON O.OrderLineId = P.OrderLineId CROSS JOIN
								 [#TempGetAgentPerformance] AS T
		WHERE        (P.[Key] = '_ClientId') AND (P.Value IN
									 (SELECT        Item
									   FROM            dbo.SplitString(@ClientIds, ','))) AND (O.OrderLineId = T.OrderLineId))



  --Select the Required Records with the Temp Table & OrderLine Table
		SELECT        AgentName, CreatedDate, BookingCount, confirmedbookingscount
		FROM            (SELECT        A.Createdby AS AgentName, A.CreatedDate, A.BookingCount, ISNULL(B.confirmedbookingscount, 0) AS confirmedbookingscount
		FROM            (SELECT        Createdby, CONVERT(varchar, CreatedDate, 101) AS CreatedDate, COUNT(*) AS BookingCount
		FROM            [#TempGetAgentPerformance]
		GROUP BY CONVERT(varchar, CreatedDate, 101), Createdby) AS A LEFT OUTER JOIN
		(SELECT        Createdby, CONVERT(varchar, CreatedDate, 101) AS CreatedDate, COUNT(*) AS confirmedbookingscount
		FROM            [#TempGetAgentPerformance] AS CB
		WHERE        EXISTS
						(SELECT        1 AS Expr1
							FROM            LB_OrderLineStatusAudit AS ols
							WHERE        (StatusId = 6) AND (OrderLineId = CB.orderlineid))
		GROUP BY CONVERT(varchar, CreatedDate, 101), CB.createdby) AS B ON A.Createdby = B.Createdby AND A.CreatedDate = B.CreatedDate) AS C


	--To Drop GetAgentPerformance Temp table
	IF OBJECT_ID('tempdb..#TempGetAgentPerformance') IS NOT NULL DROP TABLE #TempGetAgentPerformance
	
  END