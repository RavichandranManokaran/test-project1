﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/
namespace Affinion.LoyaltyBuild.Api.Booking.Data.Payment
{
    /// <summary>
    /// Entity for Payment realted Information
    /// </summary>
    public class PaymentDetail
    {
        /// <summary>
        /// orderline paymentid
        /// </summary>
        public int OrderLinePaymentID { get; set; }

        /// <summary>
        /// orderlineid
        /// </summary>
        public int OrderLineID { get; set; }

        /// <summary>
        /// Currency types
        /// </summary>
        public int CurrencyTypeId { get; set; }

        /// <summary>
        /// Payment method id
        /// </summary>
        public int PaymentMethodID { get; set; }

        /// <summary>
        /// Realex Id
        /// </summary>
        public string ReferenceID { get; set; }

        /// <summary>
        /// Amount
        /// </summary>    
        public decimal Amount { get; set; }

        /// <summary>
        /// OrderpaymentId
        /// </summary>
        public int OrderPaymentID { get; set; }

        /// <summary>
        /// Created Date
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Created By
        /// </summary>
        public string CreatedBy { get; set; }

    }
}
