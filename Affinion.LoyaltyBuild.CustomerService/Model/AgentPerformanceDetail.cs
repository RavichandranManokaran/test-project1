﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    /// <summary>
    /// Agent Performance Detail
    /// </summary>
    public class AgentPerformanceDetail
    {
        /// <summary>
        /// Agent Name
        /// </summary>
        public string AgentName { get; set; }

        /// <summary>
        /// Date
        /// </summary>
        public string Date { get; set; }

        /// <summary>
        /// TotalBookingMade
        /// </summary>
        public int TotalBookingMade { get; set; }

        /// <summary>
        /// Total Booking Confirmed
        /// </summary>
        public int TotalBookingConfirmed { get; set; }
    }
}
