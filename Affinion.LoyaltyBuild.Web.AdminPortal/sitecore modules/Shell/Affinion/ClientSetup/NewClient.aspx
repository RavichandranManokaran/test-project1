﻿<%@ Page Title="" Language="C#" MasterPageFile="~/sitecore modules/Shell/Affinion/ClientSetup/ClientSetup.Master" AutoEventWireup="true" CodeBehind="NewClient.aspx.cs" Inherits="Affinion.LoyaltyBuild.AdminPortal.ClientSetup.NewClient" ValidateRequest="false" %>

<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ OutputCache Location="None" VaryByParam="none" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container-outer width-full overflow-hidden-att app-bg-cl">
        <div class="container-outer-inner width-full">
            <div class="container">
                <div class="col-md-6 sup-login-layout-outer">
                    <div class="sup-login-layout">
                        <div class="sup-login-layout-inner">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="login-header">
                                        <img src="/Resources/images/LBLogo-home.png" alt="logo" title="" class="img-responsive">
                                    </div>
                                </div>
                            </div>
                            <%-- <form>--%>
                            <div class="col-md-12">
                                <div class="input-set">
                                    <div class="col-md-12">
                                        <h2 class="text-center">Client Setup</h2>
                                    </div>
                                    <div class="col-md-12">
                                        <p>Client Name</p>
                                        <asp:TextBox ID="TextBoxClientName" placeholder="Client Name" runat="server" class="form-control" autocomplete="off" required=""></asp:TextBox>
                                        <%--<input type="text" placeholder="Client Name" id="username" name="username" autocomplete="off" required="" class="form-control" title="">--%>
                                    </div>
                                    <%-- <div class="col-md-12">
                                            <p>Default language</p>
                                            <select class="form-control">
                                                <option>Select language</option>
                                                <option>English</option>
                                                <option>France</option>
                                                <option>german</option>
                                                <option>italy</option>
                                            </select>
                                        </div>--%>
                                    <div class="col-md-12">
                                        <p id="LabelValidation" style="color: red; font: bold" runat="server"></p>
                                    </div>
                                    <div>
                                        <div class="sup-btn-top">
                                            <div class="col-md-12">
                                                <%--<button type="submit" class="sup-btn">Create</button>--%>
                                                <asp:Button ID="ButtonCreate" runat="server" Text="Create" OnClick="ButtonCreate_Click" OnClientClick="LabelMsg()" class="sup-btn" />
                                            </div>
                                            <div class="col-md-12">
                                                <%--<button type="submit" class="sup-btn">Cancel</button>--%>
                                                <input type="reset" value="Cancel" class="sup-btn" onclick="redirectToLaunchpad();" />
                                            </div>
                                            <p class="bis-txt-center"></p>
                                            <a class="bis-back" href="#"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%-- </form>--%>
                        </div>
                    </div>
                </div>
            </div>

            <script type="text/javascript">
                function redirectToLaunchpad() {
                    window.location.replace("/sitecore/shell/sitecore/client/Applications/Launchpad/");
                }
            </script>

            <script type="text/javascript">
                function LabelMsg() {
                   // alert("example");
                   // $("#LabelValidation").innerHTML = "test";
                    // document.getElementById('col-md-12').style.display = "none";                                    
                    //document.getElementById("LabelValidation").innerHTML = "Paragraph changed!nbmm";
                }
            </script>

            <script type="text/javascript" language="javascript">
                $(document).ready(function()
                {
                    $('#<%= ButtonCreate.ClientID %>').click(function(e) 
                    {               
                        $('#<%= LabelValidation.ClientID %>').hide();                        
                           
                    });
                });
            </script>
</asp:Content>
