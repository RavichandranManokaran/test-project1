﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Availability;
using Affinion.LoyaltyBuild.SupplierPortal.DataAccess.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;

//using Sitecore.Data;

namespace Affinion.LoyaltyBuild.SupplierPortal.DataAccess.Service
{

     
    /// <summary>
    /// Offer service
    /// </summary>
    public class OfferService : IOfferService 
    {
        //Private variables
        private string _connectionString = "uCommerce";

        protected readonly ICampaignSearch offerSearch;

        public OfferService()            
        {
            offerSearch = new CampaignSearch();
        }
        /// <summary>
        /// Get all active offers
        /// </summary>
        /// <returns></returns>
        public List<Data.Offer> GetAllOffers()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get all active office subscribed by the provide
        /// </summary>
        /// <param name="providerId"></param>
        /// <returns></returns>
        //public List<Data.Offer> GetParticipatingOffersByProvider(int providerId)
        //{
           
        //    //var category = UCommerce.EntitiesV2.Category.FirstOrDefault(x => x.CategoryId == providerId);
        //    //DateTime startDate =DateTime.Now.AddMonths(-2);
        //    //DateTime endDate = startDate.AddMonths(2);



        //    List<Data.Offer> list = new List<Data.Offer>();

        //    DatabaseProviderFactory factory = new DatabaseProviderFactory();
        //    Database db = factory.Create(_connectionString);

        //    var sql = "Supplierportal_GetOffers";
          
        //    using (var command = db.GetStoredProcCommand(sql))
        //    {
        //        try
        //        {
        //            db.AddInParameter(command, "@ProviderId", DbType.Int32, providerId);

        //            db.ExecuteNonQuery(command);

                  

        //        }
        //        catch
        //        {
                  

        //        }
        //    }

        //    //if (category != null && category.Guid != null)
        //    //{
        //    //    list = offerSearch.GetCampaignDetailBySupplier(category.Guid.ToString(), startDate, endDate)
        //    //    .Where(x => x.IsSubscibe)
        //    //    .Select(x => new Data.Offer
        //    //    {
        //    //        Id = x.CampaignItemId,
        //    //        Name = x.OfferDescription
        //    //    })
        //    //    .ToList();
        //    //}

        //    return list;           
        //}

        public List<Offer> GetParticipatingOffersByProvider(string productSku)
        {
            List<Offer> offerinfo = new List<Offer>();
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "LB_Supplierportal_GetOffers";

            
            using (var command = db.GetStoredProcCommand(sql))
            {
                db.AddInParameter(command, "@ProductSku", DbType.String, productSku);
                using (var reader = db.ExecuteReader(command))
                {
                    while (reader.Read())
                    {
                        var offer = new Offer
                        {
                            Id = reader.GetInt32(0),
                            Name = reader.GetString(1)
                        };
                        offerinfo.Add(offer);
                    }
                }
            }
            return offerinfo;
        }


        /// <summary>
        /// Get all active offers provider not participating on
        /// </summary>
        /// <param name="providerId"></param>
        /// <returns></returns>
        public List<Data.Offer> GetNotParticipatingOffersByProvider(int providerId)
        {
            throw new NotImplementedException();
        }
    }
}
