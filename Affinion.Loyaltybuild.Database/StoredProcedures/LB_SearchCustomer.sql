Create PROCEDURE [dbo].[LB_SearchCustomer](
	@FirstName nvarchar(512),
	@LastName nvarchar(512),
	@PhoneNumber nvarchar(50) = null,
	@AddressLine1 nvarchar(50)=null,
	@AddressLine2 nvarchar(50)=Null,
	@CountryId int = null,
	@LocationId int =null,
	@ClientId varchar(50)= null
)
AS
begin
	SELECT  C.CustomerId FROM  [dbo].uCommerce_Customer  C
	INNER JOIN  [dbo].LB_CustomerAttribute CA
	ON	C.CustomerId = CA.CustomerId 
	WHERE	(@FirstName	IS NULL OR C.FirstName Like @FirstName + '%')
		AND (@LastName IS NULL OR C.LastName Like @LastName + '%') 
		AND (isnull(@PhoneNumber ,'')= '' OR  ( C.MobilePhoneNumber= @PhoneNumber))
		AND (isnull(@AddressLine1,'') ='' OR CA.AddressLine1 Like @AddressLine1 + '%') 
		AND (isnull(@AddressLine2,'') =''	 OR CA.AddressLine2 Like @AddressLine2 + '%')
		AND (isnull(@CountryId,0)	 = 0 OR (CA.CountryId = @CountryId))
		AND (isnull(@LocationId,0) = 0 OR (CA.LocationId = @LocationId))
		AND  (isnull(@ClientId,'') ='' OR (CA.ClientId = @ClientId))
end


Go

