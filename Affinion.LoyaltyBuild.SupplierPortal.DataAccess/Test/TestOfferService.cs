﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.SupplierPortal.DataAccess.Test
{
    /// <summary>
    /// Test Offer service which return dummy data
    /// </summary>
    public class TestOfferService : IOfferService
    {
        /// <summary>
        /// Get all active offers
        /// </summary>
        /// <returns></returns>
        public List<Data.Offer> GetAllOffers()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get all active office subscribed by the provide
        /// </summary>
        /// <param name="providerId"></param>
        /// <returns></returns>
        public List<Data.Offer> GetParticipatingOffersByProvider(string providerId)
        {
            var offers = new List<Data.Offer>();

            switch (providerId)
            {
                case "1":
                    offers.Add(new Data.Offer { Id = 1, Name = "Offer 1" });
                    offers.Add(new Data.Offer { Id = 2, Name = "Offer 2" });
                    offers.Add(new Data.Offer { Id = 3, Name = "Offer 3" });
                    break;
                case "2":
                    offers.Add(new Data.Offer { Id = 4, Name = "Offer 4" });
                    offers.Add(new Data.Offer { Id = 5, Name = "Offer 5" });
                    offers.Add(new Data.Offer { Id = 6, Name = "Offer 6" });
                    break;
                case "3":
                    offers.Add(new Data.Offer { Id = 7, Name = "Offer 7" });
                    offers.Add(new Data.Offer { Id = 8, Name = "Offer 8" });
                    offers.Add(new Data.Offer { Id = 9, Name = "Offer 9" });
                    break;
            }
            return offers;
        }

        /// <summary>
        /// Get all active offers provider not participating on
        /// </summary>
        /// <param name="providerId"></param>
        /// <returns></returns>
        public List<Data.Offer> GetNotParticipatingOffersByProvider(int providerId)
        {
            throw new NotImplementedException();
        }
    }
}
