﻿<%@ Page Language="c#" CodePage="65001" AutoEventWireup="true" UICulture="en" Culture="en-GB" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.PrimaryLayouts.SupplierPortal.Main" CodeBehind="Main.aspx.cs" %>

<%@ Register Src="~/layouts/SubLayouts/SupplierPortal/Controls/Navigation/Tertiary Nav.ascx" TagPrefix="uc1" TagName="TertiaryNav" %>

<!DOCTYPE HTML>
<html>
<head runat="server">
    <title>
        <sc:sublayout id="title" runat="server" path="/layouts/SubLayouts/SupplierPortal/Controls/Meta/Title.ascx" />
    </title>
    <sc:sublayout id="meta" runat="server" path="/layouts/SubLayouts/SupplierPortal/Controls/Meta/Metadata.ascx" />
    <meta name="robots" content="follow, index" />
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,crome=1" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <%--Bootstrap--%>
    <link href="/Resources/styles/bootstrapsupplier.min.css" rel="stylesheet" type="text/css" />
    <link href="/Resources/styles/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="/Resources/styles/bootstrap-select.css" rel="stylesheet" type="text/css">
    <%--<!--jQuery UI-->
    <link href="/assets/datePicker/jquery-ui.custom.min.css" rel="stylesheet" type="text/css" />
    --%>
    <link href="/Resources/styles/affinion-supplieradmin.min.css" rel="stylesheet" type="text/css" />
    <link href="/Resources/styles/themesupplier.css" rel="stylesheet" type="text/css" />
    <link href="/Resources/styles/affinion-supplier.css" rel="stylesheet" type="text/css" />
    <link href="/Resources/datePicker/jquery-ui.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="/Resources/styles/simple-sidebar.css" rel="stylesheet" />
    <link href="/Resources/styles/print.css" rel="stylesheet" />
    <link href="/Resources/styles/CustomeStyle.css" rel="stylesheet" />
    <link href="/Resources/styles/affinion-tooltips.css" rel="stylesheet" />
    <link href="/Resources/chosen/chosen.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="shortcut icon" type="image/x-icon" href="/Resources/images/favicon.ico" />

    <sc:sublayout id="csstheme" runat="server" path="/layouts/SubLayouts/SupplierPortal/Controls/Meta/CssTheme.ascx" />
    <%-- <sc:VisitorIdentification runat="server" />--%>
    <!--[if lte IE 8]>
 <link rel="stylesheet" type="text/css" href="/assets/css/IE-fix.css" />
 <![endif]-->
</head>
<body id="mainbody" class="cbp-spmenu-push" runat="server">
    <form method="post" runat="server" id="mainform">
        <div class="container-outer width-full overflow-hidden-att app-bg">
            <div class="top-header-login width-full">
                <asp:ScriptManager ID="singleScriptManager" runat="server"></asp:ScriptManager>
                <div id="out_container" clientidmode="Static" runat="server"></div>
                <div class="row">
                    <!-- THE LINE AT THE VERY TOP OF THE PAGE -->
                    <div id="topline" runat="server"></div>
                    <div class="col-md-12 header-bg">
                        <div class="sup-login-layout-inner">
                            <div class="col-md-2 no-padding">
                                <div class="login-header" id="logospan" runat="server">
                                    <sc:image id="Logo" field="Site Logo" alt="LoyalityBuild" cssclass="pull-left img-responsive" runat="server" maxheight="72" maxwidth="250"></sc:image>
                                </div>
                            </div>
                            <div class="col-md-10 no-padding">
                                <div class="header-right">
                                    <div class="col-md-12 right-header-top">
                                        <div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 right-header-middle">
                                        <div class="col-md-7"></div>
                                        <div class=" float-right">
                                            <div class="logoff">
                                                <sc:sublayout id="tertiaryNav" runat="server" path="/layouts/SubLayouts/SupplierPortal/Controls/Navigation/Tertiary Nav.ascx" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 right-header-bottom">
                                        <div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="nav-container">
                    <sc:sublayout id="MainNav" runat="server" path="/layouts/SubLayouts/SupplierPortal/Controls/Navigation/Main Nav.ascx" />
                </div>
            </div>

            <div class="container-outer width-full overflow-hidden-att app-bg">
                <div class="container">
                    <div class="row"></div>
                    <div class="middle-body-container">
                        <div>
                            <div class="col-md-12">
                                <sc:placeholder id="maincontent" runat="server" key="main-content" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 footer-client-top">
                <div class="footer-client col-md-12">
                    <div class="footer-nav">
                        <sc:placeholder id="footerright" runat="server" key="footer-right" />
                        <sc:fieldrenderer id="Copyright" runat="server" fieldname="Copyright" />
                    </div>
                </div>
            </div>

            <%--<div id="mtp-wrapper">
                <div id="mtp-content">
                    <sc:Placeholder ID="visitdetails" runat="server" Key="visit-details" />
                </div>
            </div>--%>
        </div>
    </form>
    <!-- Placed at the end of the document so the pages load faster -->
    <%--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script>window.jQuery || document.write("<script src='/assets/js/jquery-1.7.2.min.js'><\/script>")</script>--%>


    <script src="/Resources/scripts/jquery.2.1.0.min.js" type="text/javaScript"></script>
    <script src="/Resources/scripts/jquery-ui.custom.min.js" type="text/javaScript"></script>
    <script src="/Resources/scripts/bootstrap.min.js" type="text/javaScript"></script>
    <script type="text/javascript" src="/Resources/ckeditor/ckeditor.js"></script>

    <script src="/Resources/scripts/affinion-framework.js"></script>
    <script src="/Resources/scripts/affinion-plugins.js"></script>

    <script src="/Resources/scripts/languages/datepicker-zh-CN.js" type="text/javascript"></script>
    <script src="/Resources/scripts/languages/datepicker-fr.js" type="text/javascript"></script>
    <script src="/Resources/scripts/languages/datepicker-en-US.js" type="text/javascript"></script>

    <script type="text/javascript" src="/Resources/scripts/affinion-tooltips.js"></script>
    <script type="text/javascript" src="/Resources/scripts/Supplier.js"></script>
    <script type="text/javascript" src="/Resources/scripts/Supplier.ManageAvailability.js"></script>
    <script type="text/javascript" src="/Resources/scripts/Supplier.ManageOfferAvailability.js"></script>
    <script type="text/javascript" src="/Resources/scripts/supplier.GroupAvailability.js"></script>
    <script src="/Resources/chosen/chosen.jquery.min.js" type="text/javascript"></script>
    <script src="/Resources/scripts/bootstrap-select.js" type="text/javaScript"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>


</body>
</html>
