///<summary>
/// � 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           ClientSetupPrimaryLayout.ascx.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.AdminPortal(VS project name)
/// Description:           Sublayout for simple image template
/// </summary>

namespace Affinion.LoyaltyBuild.AdminPortal.Layouts.PrimaryLayouts.ClientSetup
{
    #region Using Directives
    using System;
    using System.Web;
    using System.Web.UI; 
    #endregion

    public partial class ClientSetupPrimaryLayout : Page
    {
        protected void Page_Load(object sender, System.EventArgs e)
        {
        }
    }
}
