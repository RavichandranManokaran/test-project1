﻿CREATE PROCEDURE [dbo].[LB_GetBookingConfirmationDetails]  ( @Id int )



AS

BEGIN

SELECT         max(P.ProductId) as 'ProductId',  P.Sku, L.OrderLineId, L.OrderId, L.Sku AS Sku, L.Price, L.Quantity, L.CreatedOn, L.Discount, L.VAT, L.Total, L.VATRate, L.VariantSku, 

			  L.ShipmentId, L.UnitDiscount, op.ProductName,

              L.CreatedBy, L.PaidInFull, OP.CheckInDate, OP.Processingfee [ProcessingFee], OP.CheckOutDate, OP.GuestName, OP.NoOfGuest ,OP.NoOfAdult AS AdultCount, OP.NoOfChild AS ChildCount, RN.Note [SpecialRequest], OA.EmailAddress, OA.PhoneNumber, 

                         OA.AddressName, OA.FirstName, OA.LastName, BI.BookingNo

FROM					 uCommerce_OrderLine AS L INNER JOIN

                         uCommerce_Product AS P ON L.Sku = P.Sku INNER JOIN

                         LB_BookingInfo AS BI ON L.OrderLineId = BI.OrderLineId LEFT OUTER JOIN

                         uCommerce_OrderAddress AS OA ON L.OrderId = OA.OrderId  LEFT OUTER JOIN

                         LB_ReservationNotes AS RN ON L.OrderLineId = RN.OrderLineId INNER JOIN
										 
						 (
							SELECT OrderId, OrderLineId, [ProviderAmount], [CommissionFee], [Processingfee], [CheckOutDate], [CheckInDate] ,[NoOfChild],[NoOfGuest],[GuestName],[NoOfAdult] ,[ProductName]   
							FROM 
							(
								SELECT
									OrderId,
									OrderLineId,
									_NoOfAdult [NoOfAdult],
									_Checkoutdate [CheckOutDate],
									_CheckIndate [CheckInDate],
									_NoOfChildren [NoOfChild],
									_NoOfGuest [NoOfGuest] ,
									_GuestName [GuestName],										
									_providerAmount [ProviderAmount],
									_LbCommisionFee [CommissionFee],
									ProcessingFee [Processingfee],
									_RoomType [ProductName]
								FROM (
								SELECT
									OrderId,
									OrderLineId,
									[key],
									[value]
								FROM 
									uCommerce_OrderProperty) src
								PIVOT(
									MAX([value])
									FOR [key] IN (_NoOfAdult, _Checkoutdate,_CheckIndate, _NoOfChildren, _NoOfGuest, _GuestName, _providerAmount, _LbCommisionFee, ProcessingFee,_RoomType)
									) piv
									) aa 
									) OP ON OP.OrderLineId = 	 bi.OrderLineId
								WHERE 
									(L.OrderId = @Id)
								GROUP BY 
									P.Sku, L.OrderLineId, L.OrderId, L.Sku, op.ProductName, L.Price, L.Quantity, L.CreatedOn, L.Discount, L.VAT, L.Total, L.VATRate, L.VariantSku, 
									L.ShipmentId, L.UnitDiscount, L.CreatedBy, L.PaidInFull, OP.CheckInDate, OP.CheckOutDate, OP.GuestName, OP.NoOfGuest, OP.NoOfChild, RN.SpecialRequest, OA.EmailAddress, OA.PhoneNumber, 
									OA.AddressName, OA.FirstName, OA.LastName, BI.BookingNo , RN.Note,op.Processingfee,OP.NoOfAdult
		
END



					
