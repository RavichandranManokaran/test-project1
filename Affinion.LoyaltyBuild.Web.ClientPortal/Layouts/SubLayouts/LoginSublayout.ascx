﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoginSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.LoginSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<li>
    <asp:Literal ID="LoginLiteral" Text="Set the datasource to view values" runat="server" />
    <a href="#" data-toggle="modal" data-target="#exampleModal" data-whatever="@fat">
        <div class="basket float-right header-right hidden-xs" id="SignIn" runat="server">
            <i class="fa fa-user"></i>Sign in
        </div>
    </a>
</li>

<!-- Welcome message -->

<li class="no-padding-right">
    <a href="#">
        <div class="basket float-right header-right hidden-xs" id="DivWelcome" runat="server">
        </div>
    </a>
</li>

<li>
    <div class="basket float-right header-right hidden-xs" id="DivLogOff" runat="server">
        <asp:LinkButton ID="LblLogOut" OnClick="LblLogOut_Click" runat="server" Text="Log out"></asp:LinkButton>
    </div>
</li>


<!-- Modal -->
<div class="modal fade bs-example-modal-sm sigin-modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="glyphicon glyphicon-user"></i>Sign In</h4>
            </div>
            <div class="modal-body">
                <div class="form-group no-margin">
                    <div class="col-sm-12">
                        <a class="btn btn-block btn-social btn-facebook">
                            <sc:Placeholder ID="SocialFacebook" Key="SocialFacebook" runat="server" />
                            Sign in with Facebook
                        </a>
                    </div>
                    <div class="col-sm-12">
                        <a class="btn btn-block btn-social btn-google">
                            <sc:Placeholder ID="SocialGoogle" Key="SocialGoogle" runat="server" />
                            Sign in with Google
                        </a>
                    </div>
                  <%--   <div class="col-sm-12">
                        <a class="btn btn-block btn-social btn-instagram">
                            <sc:Placeholder ID="SocialIinstagram" Key="SocialIinstagram" runat="server" />
                            Sign in with Instagram
                        </a>
                    </div>--%>
                    <div class="col-sm-12">
                        <a class="btn btn-block btn-social btn-twitter">
                            <sc:Placeholder ID="SocialTwitter" Key="SocialTwitter" runat="server" />
                            Sign in with Twitter
                        </a>
                        <%--  <sc:Placeholder ID="SocialTwitter" Key="SocialTwitter" runat="server" />--%>
                    </div>
                </div>
                <div class="col-sm-12">
                    <span class="social-title">-------------------Or------------------</span>
                </div>
                <div class="form-group">
                    <label class="control-label">User name:</label>
                    <%--<input type="text" class="form-control" id="username" placeholder="User name">--%>
                    <asp:TextBox ID="TextUsername" CssClass="form-control" Text="" placeholder="User name" ClientIDMode="Static" runat="server" />
                </div>
                <div class="form-group">
                    <label class="control-label">Password:</label>
                    <%--<input type="password" class="form-control" id="password" placeholder="Password">--%>
                    <asp:TextBox TextMode="Password" ID="TextPassword" CssClass="form-control" Text="" placeholder="Password" ClientIDMode="Static" runat="server" />
                </div>
                <div class="form-group no-margin">
                    <a href="#">Forgot Password?</a>
                    <p>Don't have an account yet? <a href="#">Create an account</a></p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="ButtonLogin" runat="server" class="btn btn-primary ">Access your account </button>
                <button type="button" id="ButtonCancel" runat="server" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    jQuery(document).ready(function () {

        jQuery('#TextUsername').keypress(function (e) {
            if (e.keyCode == 13) {
                handleEnterKeyPress(e);
            }
        });

        jQuery('#TextPassword').keypress(function (e) {
            if (e.keyCode == 13) {
                handleEnterKeyPress(e);
            }
        });

    });

    function handleEnterKeyPress(e) {
        e.preventDefault();
        document.getElementById('<%= ButtonLogin.ClientID %>').click();
    };
</script>




