﻿CREATE PROCEDURE [dbo].[LB_SupplierPortal_AvailablityReport]

															@DateFrom DATETIME,
															@DateTo DATETIME,
															@Provider INT
  AS
  BEGIN
  
select p.Name,
p.ProductId,
pp.Value,
 1 [ClosedOutDate],
 0 [SoldOutDate]

FROM LB_RoomAvailability RA 
INNER JOIN uCommerce_Product P on p.VariantSku=RA.VariantSku
INNER JOIN uCommerce_ProductProperty PP on pp.ProductId=p.ProductId
INNER JOIN uCommerce_ProductDefinitionField df on df.ProductDefinitionFieldId=pp.ProductDefinitionFieldId


WHERE df.Name ='RoomType' AND
      RA.Iscloseout=1  AND
	  (RA.DateCounter between @DateFrom and @DateTo) AND
      (@Provider=0 OR @Provider IS NULL OR @Provider = P.ParentProductId)
	
	UNION
select p.Name,
p.ProductId,
pp.Value,
 0 [ClosedOutDate],
 1 [SoldOutDate]

FROM LB_RoomAvailability RA 
INNER JOIN uCommerce_Product P on p.VariantSku=RA.VariantSku
INNER JOIN uCommerce_ProductProperty PP on pp.ProductId=p.ProductId
INNER JOIN uCommerce_ProductDefinitionField df on df.ProductDefinitionFieldId=pp.ProductDefinitionFieldId


WHERE RA.NumberAllocated=RA.NumberBooked AND
      RA.Iscloseout=0  AND
	  (RA.DateCounter between @DateFrom and @DateTo) AND
      (@Provider=0 OR @Provider IS NULL OR @Provider = P.ParentProductId)

	  END
