﻿#region Using Directives
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using System;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Collections.Generic;
using Affinion.LoyaltyBuild.Common.Utilities;
#endregion

namespace Affinion.LoyaltyBuild.Web.Test.Layouts.SubLayouts.ProviderSetup.PollingManager
{
    public partial class ViewAllPolls : System.Web.UI.UserControl
    {
        #region Constant Variables
        private const string WebDB = "web";
        private const string SelectPolls = "fast:/sitecore/content/Home/*";
        private const string PollTemplate = "/sitecore/templates/User Defined/Polling/Polls";
        private const string SelectPollsRadioButton = "viewPollRadioButton";
        private const string SelectPollsRadioButtonGroup = "selectPollsRadioButtonGroup";        
        #endregion

        #region Private variables
        private RadioButton viewPollRadioButton = new RadioButton();
        private Item dataItem=null;
        //Connect with the Web Database
        private Database db = Factory.GetDatabase(WebDB);
        private List<Item> itemList = new List<Item>();
        private string selectedRadioButton = null;
        #endregion

        #region Page_Load region
        /// <summary>
        /// View All available polls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Load(object sender, EventArgs e)
        {
            if (this.IsPostBack)
            {
                selectedRadioButton = Request["selectPollRadioButton"];
            }
            GetAvailablePolls();
        }
        #endregion

        #region GetAvailablePolls region
        /// <summary>
        /// Returns all the available polls 
        /// </summary>
        private void GetAvailablePolls()
        {
            //Select all available poll items
            var pollList = db.SelectItems(SelectPolls);

            //get the template related to poll
            TemplateItem template = db.GetItem(PollTemplate);

            foreach (var item in pollList)
            {
                //Check the item template is equal to the template of poll. if yes bind to repeater
                if (item.Template.FullName == template.FullName)
                {
                    repeaterDiv.Controls.Add(viewPollRadioButton);
                    itemList.Add(item);
                }                
            }
            getavailablePolls.DataSource = itemList;
            getavailablePolls.DataBind();
        }
        #endregion

        #region GetCheckedItem_region
        /// <summary>
        /// Get the item that checked by the user
        /// </summary>
        /// <returns>Returns the item</returns>
        public Item GetCheckedItem()
        {
            if (viewPollRadioButton.Checked == true)
            {
                return dataItem;
            }
            else
            {
                return null;
            }
        } 
        #endregion

        protected void SubmitButton_Click(object sender, EventArgs e)
        {
            var pollList = db.GetItem(selectedRadioButton);
            Response.Redirect("http://pollingmanager/" + pollList.DisplayName + "?PollID=" + selectedRadioButton);
        }        
    }
}