﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SuperValuBookingBarSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.SuperValuBookingBarSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Sitecore.Data.Items" %>

<script>       
    jQuery(function () {
        jQuery("#collapse-button-tit").click(function () {
            jQuery("#top-des").toggle();
        });
    });      
</script>

<div class="width-full">
    <div class="visible-xs">
        <div class="book-detail">
            <a id="collapse-button-tit" href="#">
                <span class="glyphicon glyphicon-th-large"></span><sc:Text Field="BookNowLabel" ID="BookNowLabel" runat="server" />
            </a>
        </div>
    </div>
    <div id="wrap-search">
        <div class="row">
            <div class="container">
                <div id="top-des">
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="search-left">
                                <div class="col-xs-12">
                                    <h4><sc:Text Field="DateSectionTitle" ID="DateSectionTitle" runat="server" /></h4>
                                </div>
                                <div class="col-xs-7">
                                    <div class="left-div">
                                        <h5><sc:Text Field="CheckinTitle" ID="CheckinTitle" runat="server" /></h5>
                                        <div class="input-group">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default glyphicon glyphicon-calendar calander-icon" type="button"></button>
                                            </span>
                                            <input type="text" class="form-control booking-controle" placeholder="When to order..." id="datepicker">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-5">
                                    <div class="left-div">
                                        <h5><sc:Text Field="NightsLabel" ID="NightsLabel" runat="server" /></h5>
                                        <div role="group" aria-label="...">
                                            <asp:DropDownList ID="NightsList" CssClass="form-control" runat="server"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="row">
                            <div class="blue-search-col">
                                <div class="col-xs-12">
                                    <h4><sc:Text Field="RoomSectionTitle" ID="RoomSectionTitle" runat="server" /></h4>
                                </div>
                                <div class="col-xs-4">
                                    <div class="left-div">
                                        <h5><sc:Text Field="RoomsLabel" ID="RoomsLabel" runat="server" /></h5>
                                        <div role="group" aria-label="...">
                                            <asp:DropDownList ID="RoomsList" CssClass="form-control" runat="server"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="left-div">
                                        <h5><sc:Text Field="AdultsLabel" ID="AdultsLabel" runat="server" /></h5>
                                        <div role="group" aria-label="...">
                                            <asp:DropDownList ID="AdultsList" CssClass="form-control" runat="server"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="left-div">
                                        <h5><sc:Text Field="ChildrenLabel" ID="ChildrenLabel" runat="server" /></h5>
                                        <div role="group" aria-label="...">
                                            <asp:DropDownList ID="ChildrenList" CssClass="form-control" runat="server"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>

                    <div class="col-sm-4">
                        <div class="row">
                            <div class="search-left">
                                <div class="col-xs-12">
                                    <h4><sc:Text Field="PackageSectionTitle" ID="PackageSectionTitle" runat="server" /></h4>
                                </div>
                                <div class="col-xs-8">
                                    <div class="left-div">
                                        <h5></h5>
                                        <div role="group" aria-label="...">
                                            <asp:Repeater ID="PackageList" runat="server">
                                                <HeaderTemplate>
                                                    <select class="form-control">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <option value="<%#((Item)Container.DataItem).ID.ToString() %>"><sc:Text Field="Title" Item="<%#(Item)Container.DataItem %>" runat="server" /></option>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </select>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="right-div">
                                        <button type="submit" class="btn btn-primary float-right btn-bg"><sc:Text Field="SearchLabel" ID="SearchLabel" runat="server" /></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>