﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Maps
{
    public class CommissionAwardMap : SubclassMap<CommissionAward>
    {
        // Methods
        public CommissionAwardMap()
        {
            base.Table("LB_CommissionAward");
            base.KeyColumn("CommissionAwardId");
            base.Map(x => x.CommissionAwardId).ReadOnly();
            base.Map(x => x.DiscountAmount).Not.Nullable();
            base.Map(x => x.IsPercentage).Not.Nullable();
            base.Map(x => x.OnOrderLine).Not.Nullable();
        }

    }

}
