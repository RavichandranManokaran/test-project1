﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           OfferOccupancyTypeFilter.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Search (VS project name)
/// Description:           Filter offers by occupancy type configuration
/// </summary>
namespace Affinion.LoyaltyBuild.Search.OfferFilters
{
    #region Using Statements

    using Affinion.LoyaltyBuild.Common;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Search.Data;
    using Sitecore.Data.Items;
    using System;

    #endregion

    /// <summary>
    /// Filter offers by occupancy type configuration
    /// </summary>
    public class OfferOccupancyTypeFilter : IOfferFilter
    {
        #region Public Methods
        /// <summary>
        /// Returns true if the offer is valid and fine to show on client page
        /// </summary>
        /// <param name="filterData">Filter meta data</param>
        /// <returns>Returns true to include the offer</returns>
        public bool IncludeOffer(OfferFilterData filterData)
        {
            try
            {
                return true;

                //if (filterData == null)
                //{
                //    throw new ArgumentNullException("filterData");
                //}

                //if (filterData.SearchTerm.NumberOfRooms > 1)
                //{
                //    ///This filtration would happen as a bulk separately.
                //    return true;
                //}

                ////if (filterData != null && filterData.Offer != null)
                //if (filterData.Offer != null)
                //{
                //    bool adultAndChildCheck = false;

                //    if (filterData.Offer.OccupancyType == null)
                //    {
                //        return false;
                //    }

                //    // If the item does not match with filter go to next item
                //    if (FilterSearchResults(filterData.Client, filterData.SearchTerm, filterData.Offer.OccupancyType))
                //    {
                //        adultAndChildCheck = true;
                //    }

                //    bool activeCheck = filterData.Offer.OccupancyType.IsActive;

                //    return adultAndChildCheck && activeCheck;

                //}

                //return false;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.BusinessLogic, ex, null);
                throw;
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Filters search results
        /// </summary>
        /// <param name="clientItem">Client</param>
        /// <param name="keywords">Search keywords</param>
        /// <param name="occupancyItem">Occupancy type</param>
        /// <returns>True when item matches search</returns>
        private static bool FilterSearchResults(Item clientItem, SearchKey keywords, OccupancyType occupancyType)
        {

            if (keywords.NumberOfAdults > occupancyType.MaxAdults)
            {
                ///Offer cannot afford no of adults. so ignore this offer
                return false;
            }

            string clientCountry = SitecoreFieldsHelper.GetValue(clientItem, "Country");

            int enteredNoOfChildren = keywords.NumberOfChildren;

            ///Ireland related validations
            if (enteredNoOfChildren > 0 && clientCountry.ToLower().Equals(Constants.Ireland.ToLower()))
            {
                if (keywords.LessThanTwoChildrenCount > 1)
                {
                    ///Please note where a user selects age 0-2yrs, this child is ignored for all of the Irish clients
                    ///Only one child can sleep with 
                    enteredNoOfChildren = enteredNoOfChildren - 1;
                }
            }

            if (enteredNoOfChildren > occupancyType.MaxChildren)
            {
                ///Offer cannot afford no of children. so ignore this offer
                return false;
            }

            return true;
        }

        #endregion

    }
}
