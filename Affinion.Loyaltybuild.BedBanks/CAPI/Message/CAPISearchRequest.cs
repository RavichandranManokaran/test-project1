﻿namespace Affinion.Loyaltybuild.BedBanks.CAPI.Message
{
    /// <summary>
    /// This is a class to hold the search Request Parameters.
    /// It will be passed to the API controller to perform the actual request.
    /// </summary>
    public class CapiSearchRequest
    {
        public string Location, Locationtype, Checkin, Checkout, Adults, Children, AgeOfChildren, MinimumStarRating, HotelDisplayOrder, MaxResults;

        public CapiSearchRequest(string pLocation, string pCheckin, string pCheckout, string pAdults, string pChildren, string pAgeOfChildren)
        {
            Location = pLocation;
            Locationtype = "Airport";
            Checkin = pCheckin;
            Checkout = pCheckout;
            Adults = pAdults;
            Children = pChildren;
            AgeOfChildren = pAgeOfChildren;
            MinimumStarRating = "3";
            HotelDisplayOrder = "ByPriceLowToHigh";
            MaxResults = "250";
        }
    }
}