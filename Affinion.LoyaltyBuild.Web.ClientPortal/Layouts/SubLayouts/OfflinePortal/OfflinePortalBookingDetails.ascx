﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalBookingDetails.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalBookingDetails" %>

<div class="col-sm-12" id="divBooking" runat="server">
    <div class="accordian-outer">
        <div class="panel-group" id="accordion1">
            <div class="panel panel-default">
                <div class="panel-heading bg-primary">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion1" href="#collapse0" class="show-hide-list">
                            <sc:text id="TextBookingDetails" field="BookingStatusText" runat="server" />
                        </a>
                    </h4>
                </div>

                <div id="collapse0" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <div id="printcontent" runat="server">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="content_bg_inner_box summary-detail alert-info">
                                        <div class="col-sm-6 col-xs-12 no-padding" style="width: 50%; float: left">
                                            <div class="col-sm-12 no-padding" id="divDates" runat="server">
                                                <div class="col-sm-12">
                                                    <h2 class="no-margin">
                                                        <sc:text id="TextDates" field="DatesText" runat="server" />
                                                    </h2>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span class="accent33-f">
                                                            <sc:text id="TextArrival" field="ArrivalText" runat="server" />
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span><b>
                                                            <asp:Literal runat="server" ID="litArrivalDay"></asp:Literal></b></span>
                                                    </div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span class="accent33-f">
                                                            <sc:text id="TextDeparture" field="DepartureText" runat="server" />
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span><b>
                                                            <asp:Literal runat="server" ID="litDepartureDay" /></b></span>
                                                    </div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span class="accent33-f">
                                                            <sc:text id="TextConfirmation" field="ConfirmationText" runat="server" />
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span>
                                                            <asp:Literal runat="server" ID="litConfirmationDay" /></span>
                                                    </div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span class="accent33-f">
                                                            <sc:text id="TextReservation" field="ReservationText" runat="server" />
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span>
                                                            <asp:Literal runat="server" ID="litReservationDay" /></span>
                                                    </div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span class="accent33-f">
                                                            <sc:text id="TextStatusDate" field="StatusDateText" runat="server" />
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span>
                                                            <asp:Literal runat="server" ID="litStatusDay" /></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 no-padding" id="divCustomer" runat="server">
                                                <div class="col-sm-12">
                                                    <h2 class="no-margin">
                                                        <sc:text id="TextCustomer" field="CustomerText" runat="server" />
                                                    </h2>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span class="accent33-f">
                                                            <sc:text id="TextName" field="NameText" runat="server" />
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span>
                                                            <asp:Literal runat="server" ID="litName" /></span>
                                                    </div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span class="accent33-f">
                                                            <sc:text id="TextAddress" field="AddressText" runat="server" />
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span>
                                                            <asp:Literal runat="server" ID="litAddress" /></span>
                                                    </div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span class="accent33-f">
                                                            <sc:text id="TextPhone" field="PhoneText" runat="server" />
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span>
                                                            <asp:Literal runat="server" ID="litPhone" /></span>
                                                    </div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span class="accent33-f">
                                                            <sc:text id="TextEmail" field="EmailText" runat="server" />
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span>
                                                            <asp:Literal runat="server" ID="litEmail" /></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 no-padding" id="divPayments" runat="server">
                                                <div class="col-sm-12">
                                                    <h2 class="no-margin">
                                                        <sc:text id="TextPaymentDetails" field="PaymentDetailsText" runat="server" />
                                                    </h2>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span class="accent33-f">
                                                            <sc:text id="TextTotalPrice" field="TotalPriceText" runat="server" />
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span>
                                                            <asp:Literal runat="server" ID="litPrice" /></span>
                                                    </div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span class="accent33-f">
                                                            <sc:text id="TextDeposit" field="DepositText" runat="server" />
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span>
                                                            <asp:Literal runat="server" ID="litDeposit" /></span>
                                                    </div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span class="accent33-f">
                                                            <sc:text id="TextProcesingFee" field="ProcessingFeeText" runat="server" />
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <asp:Literal runat="server" ID="litProcesingFee" /><span></span>
                                                    </div>
                                                </div>
                                                <div class="row margin-row" style="display: none;">
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span class="accent33-f">
                                                            <sc:text id="TextDueAtCheckout" field="DueAtCheckoutText" runat="server" />
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span>
                                                            <asp:Literal runat="server" ID="litCheckout" /></span>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-12 no-padding" style="width: 50%; float: left">
                                            <div class="col-sm-12 no-padding" id="divAccomodationInfo" runat="server">
                                                <div class="col-sm-12">
                                                    <h2 class="no-margin">
                                                        <sc:text id="TextAccommodation" field="AccommodationText" runat="server" />
                                                    </h2>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span class="accent33-f">
                                                            <sc:text id="TextAccName" field="NameText" runat="server" />
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span>
                                                            <asp:Literal runat="server" ID="litAccName" /></span>
                                                    </div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span class="accent33-f">
                                                            <sc:text id="TextAccAddress" field="AddressText" runat="server" />
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span>
                                                            <asp:Literal runat="server" ID="litAccAddress" /></span>
                                                    </div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span class="accent33-f">
                                                            <sc:text id="TextAccPhone" field="PhoneText" runat="server" />
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span>
                                                            <asp:Literal runat="server" ID="litAccPhone" /></span>
                                                    </div>
                                                </div>
                                                <div class="row margin-row">
                                                    <asp:PlaceHolder ID="plcEmail" runat="server">
                                                        <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                            <span class="accent33-f">
                                                                <sc:text id="TextAccEmail" field="EmailText" runat="server" />
                                                            </span>
                                                        </div>
                                                        <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                            <span>
                                                                <asp:Literal runat="server" ID="litAccEmail" /></span>
                                                        </div>
                                                    </asp:PlaceHolder>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span class="accent33-f">
                                                            <sc:text id="TextNoofRooms" field="NoOfRoomsText" runat="server" />
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;"><span>1</span></div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span class="accent33-f">
                                                            <sc:text id="TextNoofAdults" field="NoOfAdultsText" runat="server" />
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span>
                                                            <asp:Literal runat="server" ID="litNoofAdults" /></span>
                                                    </div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span class="accent33-f">
                                                            <sc:text id="TextNoofChildren" field="NoOfChildrenText" runat="server" />
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span>
                                                            <asp:Literal runat="server" ID="litNoofChildren" /></span>
                                                    </div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span class="accent33-f">
                                                            <sc:text id="TextNoOfNights" field="NoOfNights" runat="server" />
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span>
                                                            <asp:Literal runat="server" ID="litNoOfNights" /></span>
                                                    </div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span class="accent33-f">
                                                            <sc:text id="TextOccupancyType" field="OccupancyTypeText" runat="server" />
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span>
                                                            <asp:Literal runat="server" ID="litOccupancyType" /></span>
                                                    </div>
                                                </div>
                                                <div class="row margin-row" style="display: none;">
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;"><span class="accent33-f">Cost</span></div>
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span>
                                                            <asp:Literal runat="server" ID="litCost" /></span>
                                                    </div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span class="accent33-f">
                                                            <sc:text id="TextHotelDue" field="HotelDueText" runat="server" />
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span>
                                                            <asp:Literal runat="server" ID="litHotelDue" /></span>
                                                    </div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span class="accent33-f">
                                                            <sc:text id="TextOutstanding" field="OutstandingText" runat="server" />
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span>
                                                            <asp:Literal runat="server" ID="litOutstanding" /></span>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-sm-12 no-padding" id="divAdditionalInfo" runat="server">
                                                <div class="col-sm-12">
                                                    <h2 class="no-margin">
                                                        <sc:text id="TextAdditionalInformation" field="AdditionalInformationText" runat="server" />
                                                    </h2>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span class="accent33-f">
                                                            <sc:text id="TextCreatedBy" field="CreatedByText" runat="server" />
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span>
                                                            <asp:Literal runat="server" ID="litCreatedBy" /></span>
                                                    </div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span class="accent33-f">
                                                            <sc:text id="TextMethod" field="MethodText" runat="server" />
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span>
                                                            <asp:Literal runat="server" ID="litMethod" /></span>
                                                    </div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span class="accent33-f">
                                                            <sc:text id="TextLastUpdate" field="LastUpdateText" runat="server" />
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span>
                                                            <asp:Literal runat="server" ID="litLastUpdate" /></span>
                                                    </div>
                                                </div>
                                                <div class="row margin-row">
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span class="accent33-f">
                                                            <sc:text id="TextBookingRef" field="BookingRefText" runat="server" />
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-6 font-size-12" style="width: 50%; float: left; font-size: 12px;">
                                                        <span>
                                                            <asp:Literal runat="server" ID="litBookingRef" /></span>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-sm-12 no-padding">
                                            <div class="col-sm-3 col-xm-12">
                                            </div>
                                            <div class="col-sm-3 col-xm-12">
                                                <a href="<%=GetUrl("/existingbooking") %>" class="btn btn-default add-break pull-right margin-row width-full">
                                                    <sc:text id="TextBookingList" field="ReturntoBookingListText" runat="server" />
                                                </a>
                                                <!--<button type="submit" class="btn btn-default add-break pull-right margin-row width-full">Transfer</button>   -->
                                            </div>
                                            <div class="col-sm-3 col-xm-12">

                                                <asp:Button ID="PrintButton" runat="server" OnClick="PrintButton_Click" class="btn btn-default add-break pull-right margin-row width-full" Text="Print" />
                                                <%--<button type="submit" value="Print Div" onclick="PrintElem('#printcontent')">Print</button>--%>

                                                <!--<button type="submit" class="btn btn-default add-break pull-right margin-row width-full">Print</button>-->
                                            </div>
                                            <%-- <div class="col-sm-3 col-xm-12">
                                            <!--<button type="submit" class="btn btn-default add-break pull-right margin-row width-full">Print</button>-->
                                            <button type="submit" class="btn btn-default add-break pull-right margin-row width-full" onclick="location.href='search-bookingv2.html'">Back</button>
                                        </div>--%>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>










