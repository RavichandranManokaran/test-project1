﻿
CREATE TABLE [dbo].[LB_SupplierInvite](
	[SupplierInviteId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[SKU] [nvarchar](30) NOT NULL,
	[IsSubscribe] [bit] NOT NULL,
	[CampaignItem] [int] NOT NULL,
	[Offerinformation] [varchar](max) NULL,
 CONSTRAINT [PK_uCommerce_SupplierInvite] PRIMARY KEY CLUSTERED 
(
	[SupplierInviteId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

