﻿CREATE TABLE [dbo].[uCommerce_Product] (
    [ProductId]             INT              IDENTITY (1, 1) NOT NULL,
    [ParentProductId]       INT              NULL,
    [Sku]                   NVARCHAR (30)    NOT NULL,
    [VariantSku]            NVARCHAR (30)    NULL,
    [Name]                  NVARCHAR (512)   NOT NULL,
    [DisplayOnSite]         BIT              CONSTRAINT [uCommerce_DF_Product_DisplayOnSite] DEFAULT ((1)) NOT NULL,
    [ThumbnailImageMediaId] NVARCHAR (100)   NULL,
    [PrimaryImageMediaId]   NVARCHAR (100)   NULL,
    [Weight]                DECIMAL (18, 4)  CONSTRAINT [uCommerce_DF_Product_Weight] DEFAULT ((0)) NOT NULL,
    [ProductDefinitionId]   INT              NOT NULL,
    [AllowOrdering]         BIT              CONSTRAINT [uCommerce_DF_Product_AllowOrdering] DEFAULT ((1)) NOT NULL,
    [ModifiedBy]            NVARCHAR (50)    NULL,
    [ModifiedOn]            DATETIME         CONSTRAINT [uCommerce_DF_Product_LastModified] DEFAULT (getdate()) NOT NULL,
    [CreatedOn]             DATETIME         CONSTRAINT [uCommerce_DF_Product_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]             NVARCHAR (50)    NULL,
    [Rating]                FLOAT (53)       CONSTRAINT [DF_uCommerce_Product_AverageRating] DEFAULT ((0)) NULL,
    [Guid]                  UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    CONSTRAINT [uCommerce_PK_Product] PRIMARY KEY CLUSTERED ([ProductId] ASC),
    CONSTRAINT [FK_uCommerce_Product_ParentProduct] FOREIGN KEY ([ParentProductId]) REFERENCES [dbo].[uCommerce_Product] ([ProductId]),
    CONSTRAINT [uCommerce_FK_Product_ProductDefinition] FOREIGN KEY ([ProductDefinitionId]) REFERENCES [dbo].[uCommerce_ProductDefinition] ([ProductDefinitionId])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Product_UniqueSkuAndVariantSku]
    ON [dbo].[uCommerce_Product]([Sku] ASC, [VariantSku] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_uCommerce_Product_ParentProductId]
    ON [dbo].[uCommerce_Product]([ParentProductId] ASC);

