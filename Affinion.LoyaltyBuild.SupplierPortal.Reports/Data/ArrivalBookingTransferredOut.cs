﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.SupplierPortal.Reports.Data
{
    /// <summary>
    /// Contains details for the arrival booking transferred out report item
    /// </summary>
    public class ArrivalBookingTransferredOut : ArrivalBase
    {

    }
    /// <summary>
    /// Collection to manage Arrival booking transferred out
    /// </summary>
    public class ArrivalBookingTransferredOutCollection : IEnumerable<ArrivalBookingTransferredOut>,IList<ArrivalBookingTransferredOut>
    {
        //Private variables declaration
        private List<ArrivalBookingTransferredOut> _items;

        /// <summary>
        /// Constructor
        /// </summary>
        public ArrivalBookingTransferredOutCollection()
        {
            _items = new List<ArrivalBookingTransferredOut>();
        }

        /// <summary>
        /// Add method to add item
        /// </summary>
        /// <param name="item"></param>
        public void Add(ArrivalBookingTransferredOut item)
        {
            _items.Add(item);
        }

        /// <summary>
        /// Override method IEnumenrable class
        /// </summary>
        /// <returns></returns>
        public IEnumerator<ArrivalBookingTransferredOut> GetEnumerator()
        {
            foreach (var item in _items)
                yield return item;
        }

        /// <summary>
        /// Override method IEnumenrable class
        /// </summary>
        /// <returns></returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            foreach (var item in _items)
                yield return item;
        }
         public int IndexOf(ArrivalBookingTransferredOut item)
        {
            throw new NotImplementedException();
        }

        public void Insert(int index, ArrivalBookingTransferredOut item)
        {
            throw new NotImplementedException();
        }

        public void RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        public ArrivalBookingTransferredOut this[int index]
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }


        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(ArrivalBookingTransferredOut item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(ArrivalBookingTransferredOut[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public int Count
        {
            get { return _items.Count; }
        }

        public bool IsReadOnly
        {
            get { throw new NotImplementedException(); }
        }

        public bool Remove(ArrivalBookingTransferredOut item)
        {
            throw new NotImplementedException();
        }
    }
}

    

