﻿using Affinion.LoyaltyBuild.Model.Provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Api.Search.ServiceProviders
{
    public interface IProviderService
    {
        /// <summary>
        /// Get service provider by type
        /// </summary>
        /// <param name="providerType"></param>
        /// <returns></returns>
        IServiceProvider GetServiceProvider(ProviderType providerType);

        /// <summary>
        /// Get search providers matching the client
        /// </summary>
        /// <returns></returns>
        IEnumerable<IServiceProvider> GetServiceProvidersByClient(string clientId);

    }
}
