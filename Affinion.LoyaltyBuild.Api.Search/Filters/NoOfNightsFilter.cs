﻿using Affinion.LoyaltyBuild.Api.Search.Data;
using Sitecore.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;

namespace Affinion.LoyaltyBuild.Api.Search.Filters
{
    class NoOfNightsFilter:ILoyaltyFilter,ISearchFilter,IBedBankFilter
    {
        private readonly SearchKey _key;

        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="key"></param>
        public NoOfNightsFilter(SearchKey key)
        {
            _key = key;
        }

        /// <summary>
        /// Build Search Criteria with Number of Nights
        /// </summary>
        /// <param name="criteria"></param>
        public void BuildSearch(ref UCom.MasterClass.ExtendedApi.SearchCriteria criteria)
        {
            if (_key.NoOfNights > 0)
            {
                criteria.Night = (int)_key.NoOfNights;
            }
        }

        /// <summary>
        /// Method to set the search criteria for Bed Bank Duration Search
        /// </summary>
        /// <param name="criteria"></param>
        public void BuildbbSearch(ref BedBanks.General.BBsearchCriteria criteria)
        {
            if (_key.Location.Type != LocationType.City)
                return;
            if (_key.NoOfNights > 0)
                criteria.Duration = _key.NoOfNights.ToString();
        }
    }
}
