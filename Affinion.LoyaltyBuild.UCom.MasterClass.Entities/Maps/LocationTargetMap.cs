﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class LocationTargetMap : SubclassMap<LocationTarget>
    {
        // Methods
        public LocationTargetMap()
        {
            base.Table("LB_LocationTarget");
            base.KeyColumn("LocationTargetId");
            base.Map(x => x.LocationTargetId).ReadOnly();
            base.Map(x => x.Location).Not.Nullable();
        }

    }

}
