CREATE PROCEDURE [dbo].[LB_GetManualPaymentRequest]
@BookingRefNo VARCHAR(MAX)=NULL
AS
BEGIN
DECLARE @OrderLineID BIGINT
DECLARE @OrderID BIGINT
DECLARE @PaymentRequestStatus TINYINT
DECLARE @PaymentRequestDate DATETIME
DECLARE @PaymentPaidDate DATETIME
DECLARE @PaymentSatus TINYINT
DECLARE @SuggestedAmount DECIMAL(18,2)
DECLARE @RequestWithinSixMonth TINYINT

SET @OrderLineID=(SELECT OrderLineId FROM LB_BookingInfo WHERE BookingNo=@BookingRefNo AND StatusId=6)
SET @OrderID=(SELECT TOP 1 OrderId FROM uCommerce_OrderLine WHERE OrderLineId=ISNULL(@OrderLineID,0))

IF EXISTS(SELECT OrderId FROM LB_Supplier_Payments WHERE OrderId=ISNULL(@OrderID,0) AND OrderLineID=ISNULL(@OrderLineID,0))
BEGIN
	SET @PaymentRequestStatus= 1
END
ELSE
BEGIN
	SET @PaymentRequestStatus= 0
END

IF EXISTS(
          SELECT * FROM LB_BookingInfo BI
		  INNER JOIN ucommerce_OrderLine OL ON BI.OrderLineId=OL.OrderLineId 
		  WHERE BI.BookingNo=@BookingRefNo AND OL.OrderId=@OrderID AND OL.OrderLineID=@OrderLineID AND DATEDIFF(mm, BI.CheckOutDate,GETDATE())<=6
		 )
BEGIN
         SET @RequestWithinSixMonth=1
END
ELSE
BEGIN
         SET @RequestWithinSixMonth=0
END

SET @PaymentSatus=(SELECT ISNULL(IsPaidToAccounts,0) FROM LB_Supplier_Payments WHERE OrderId=ISNULL(@OrderID,0) AND OrderLineId=ISNULL(@OrderLineID,0))
SET @PaymentRequestDate=(SELECT DateCreated FROM LB_Supplier_Payments WHERE OrderId=ISNULL(@OrderID,0) AND OrderLineId=ISNULL(@OrderLineID,0))
SET @SuggestedAmount =(SELECT ISNULL(SuggestedAmount,0) FROM LB_Supplier_Payments WHERE OrderId=ISNULL(@OrderID,0) AND OrderLineId=ISNULL(@OrderLineID,0))

IF @PaymentSatus >0
BEGIN
    SET @PaymentPaidDate=(SELECT DatePostedToOracle FROM LB_Supplier_Payments WHERE OrderId=ISNULL(@OrderID,0) AND OrderLineId=ISNULL(@OrderLineID,0))
END

		SELECT DISTINCT
		PPD.SiteCoreItemGuid [ProviderID],
		OL.OrderId,
		OL.OrderLineId,
		CI.Name [Offername],
		BI.CheckInDate,
		BI.CheckOutDate,
		PO.CreatedDate [BookingDate],
		(C.FirstName+' '+C.LastName) [CustomerName],
		CU.ISOCode [Currency],
		CU.CurrencyId [CurrencyId],
		(CASE WHEN OP.RoomType IS NOT NULL THEN OP.RoomType ELSE OP.RoomInfo END) [AccommodationType],
		OP.ClientId [ClientId],
		P.Name [HotelName],
		(CASE WHEN @SuggestedAmount IS NULL THEN SUM(OLD.ProviderAmount) ELSE @SuggestedAmount END)  [SuggestedAmount] ,
		(CASE WHEN CI.Name IS NULL THEN '' ELSE 'Offer Based' END)  [OfferBased],
		PO.CustomerId,
		ISNULL(@PaymentSatus,0) [PaymentStatus],
		@PaymentRequestStatus [PaymentRequestStatus],
		@PaymentPaidDate [PaymentPaidDate],
		@PaymentRequestDate [PaymentRequestedDate],
		@RequestWithinSixMonth [RequestWithinSixMonth]
FROM LB_BookingInfo BI
		INNER JOIN ucommerce_OrderLine OL ON BI.OrderLineId=OL.OrderLineId
		INNER JOIN LB_OrderLineDue OLD ON OL.OrderLineId=OLD.OrderLineId AND BI.BookingId=OLD.BookingId
		INNER JOIN (
		   SELECT OrderLineId,OrderId, PaymentMethod,_RoomType [RoomType],_ClientId [ClientId],RoomInfo
			FROM 
			(
			  SELECT OrderLineId,OrderId, [key], [value]
			  FROM uCommerce_OrderProperty WHERE OrderLineId=@OrderLineID AND OrderId=@OrderID
			) src
			PIVOT
			(
			  MAX([value])
			  FOR [key] IN (PaymentMethod,_RoomType,_ClientId,RoomInfo)
			) piv
		) OP ON OL.OrderId=OP.OrderId AND OL.OrderLineId=OP.OrderLineId AND (OP.PaymentMethod='Full' OR OP.PaymentMethod='Partial') 
		INNER JOIN LB_OrderLineStatusAudit OLS ON OP.OrderLineId=OLS.OrderLineId
		INNER JOIN uCommerce_OrderStatus OS ON OLS.StatusId=OS.OrderStatusId AND OS.OrderStatusId=6
		INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId=PO.OrderId
		INNER JOIN uCommerce_Customer C ON PO.CustomerId=C.CustomerId
		INNER JOIN uCommerce_Currency CU ON PO.CurrencyId=CU.CurrencyId
		INNER JOIN Ucommerce_product P ON OL.Sku = P.Sku AND P.ParentProductId IS NULL AND P.VariantSku IS NULL
		INNER JOIN uCommerce_ProductDefinition PD ON P.ProductDefinitionId=PD.ProductDefinitionId AND PD.Name='Hotel'
		INNER JOIN (
		   SELECT ProductId,SiteCoreItemGuid
			FROM 
			(
			  SELECT ProductId,PD.Name [key], PP.value
			  FROM uCommerce_ProductDefinitionField PD 
			  INNER JOIN uCommerce_ProductProperty PP ON PD.ProductDefinitionFieldId=PP.ProductDefinitionFieldId
			  WHERE PD.Name IN ('SiteCoreItemGuid')
			) src
			PIVOT
			(
			  MAX([value])
			  FOR [key] IN (SiteCoreItemGuid)
			) piv
		) PPD ON PPD.ProductId=P.ProductId 
		LEFT JOIN uCommerce_OrderLineCampaignRelation OLCR ON OL.OrderLineId=OLCR.OrderLineId  
		LEFT JOIN ucommerce_campaignitem CI ON OLCR.CampaignItemId=CI.CampaignItemId
WHERE 
	    BI.BookingNo IS NOT NULL AND
	    PO.CustomerId IS NOT NULL AND
		BI.CheckOutDate <= DATEADD(mm,6,BI.CheckOutDate) AND
		BI.BookingNo= @BookingRefNo

GROUP BY

		CI.Name,
		BI.CheckInDate,
		BI.CheckOutDate,
		PO.CreatedDate ,
		C.FirstName,
		C.LastName,
		CU.ISOCode,
		CU.CurrencyId,
		OP.RoomType,
		OP.RoomInfo,
		OP.ClientId,
		P.Name,
		PO.CustomerId,
		OLD.ProviderAmount,
		PPD.SiteCoreItemGuid,
		OL.OrderLineId,
		OL.OrderId
		

END
