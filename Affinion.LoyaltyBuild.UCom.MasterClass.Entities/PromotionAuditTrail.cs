﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class PromotionAuditTrail : IEntity
    {
        private int? _hashCode;



        // Methods
        public new static IQueryable<PromotionAuditTrail> All()
        {
            return GetRepo().Select();
        }

        public virtual void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<PromotionAuditTrail, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            PromotionAuditTrail objA = obj as PromotionAuditTrail;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<PromotionAuditTrail, bool>> expression)
        {
            return All().Any<PromotionAuditTrail>(expression);
        }

        public static IList<PromotionAuditTrail> Find(Expression<Func<PromotionAuditTrail, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<PromotionAuditTrail>();
        }

        public static PromotionAuditTrail FirstOrDefault(Expression<Func<PromotionAuditTrail, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<PromotionAuditTrail>();
        }

        public static PromotionAuditTrail Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<PromotionAuditTrail> GetRepo()
        {
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<PromotionAuditTrail>>();
        }


        public virtual void Save()
        {
            GetRepo().Save(this);
        }

        public static PromotionAuditTrail SingleOrDefault(Expression<Func<PromotionAuditTrail, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties
        public virtual int Id
        {
            get
            {
                return this.PromotionAuditTrailId;
            }
        }

        public virtual int PromotionAuditTrailId { get; protected set; }

        public virtual string EntityName { get; set; }

        public virtual int Record { get; set; }

        public virtual string Message { get; set; }

        public virtual string AuditedBy { get; set; }

        public virtual DateTime AuditedDate { get; set; }

    }
}
