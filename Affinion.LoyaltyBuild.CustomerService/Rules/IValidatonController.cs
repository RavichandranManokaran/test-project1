﻿using RuleModel=Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Rules.Model;
namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Validators
{
    public interface  IValidatonController
    {
        RuleModel.ValidationResponse Execute(RuleModel.ValidationRequest request);
    }
}
