﻿using Affinion.LoyaltyBuild.Api.Search.Data;
using Affinion.LoyaltyBuild.Api.Search.Filters;
using Affinion.LoyaltyBuild.Model.Product;
using Affinion.LoyaltyBuild.Model.Provider;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sitecore.Data.Items;
using Sitecore.Data;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Availability;


namespace Affinion.LoyaltyBuild.Api.Search.ServiceProviders
{
    /// <summary>
    /// Loyaltybuild provider service
    /// </summary>
    public class LoyaltyProvider : IServiceProvider
    {
        /// <summary>
        /// Search providers based on filter
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        public ProviderCollection SearchProviders(IList<ISearchFilter> filters)
        {
            if (filters == null)
                return null;

            var criteria = new SearchCriteria();

            foreach (var i in filters)
            {
                ILoyaltyFilter loyaltyFilter = i as ILoyaltyFilter;

                if (loyaltyFilter != null)
                    loyaltyFilter.BuildSearch(ref criteria);
            }

            //if no matching category return null
            if (criteria.SearchCategory == null)
                return null;

            var hotels = LoyaltyBuildCatalogLibrary.SearchHotels(criteria);

            ProviderCollection lbProviderCollection = ConvertHotelDetailListToProviderCollection(hotels, criteria.ArrivalDate != DateTime.MinValue);
            return lbProviderCollection;
        }

        /// <summary>
        /// Search providers based on filter asyncronusly
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        public async Task<ProviderCollection> SearchProvidersAsync(IList<ISearchFilter> filters)
        {
            return await Task.Run<ProviderCollection>(() =>
                SearchProviders(filters));
        }

        /// <summary>
        /// Convert hotel to providers
        /// </summary>
        /// <param name="lbHotels"></param>
        /// <returns></returns>
        public ProviderCollection ConvertHotelDetailListToProviderCollection(List<HotelDetail> lbHotels, bool isDateSelected)
        {
            ProviderCollection lbProviderCollection = new ProviderCollection(ProviderType.LoyaltyBuild);
            foreach (var lbHotelDetailList in lbHotels)
            {
                if (!isDateSelected || lbHotelDetailList.HotelProductDetail.Any(i => i.Availability > 0))
                {
                    SearchResult searchResultItem = new SearchResult();
                    searchResultItem.Name = lbHotelDetailList.Name;
                    searchResultItem.ProviderId = lbHotelDetailList.ItemId;
                    searchResultItem.ProviderType = ProviderType.LoyaltyBuild;
                    searchResultItem.SitecoreItemId = lbHotelDetailList.ItemId;
                    searchResultItem.StarRanking = lbHotelDetailList.StarRanking;
                    searchResultItem.LocalRanking = lbHotelDetailList.LocalRanking;

                    List<IProduct> productCollection = new List<IProduct>();
                    foreach (var hpd in lbHotelDetailList.HotelProductDetail)
                    {
                        Room room = new Room();
                        room.Sku = hpd.Sku;
                        room.Name = hpd.Name;
                        room.Type = ProductType.Room;
                        room.VariantSku = hpd.VariatSku;
                        room.Price = hpd.Price;
                        room.CurrencyId = 5; //hard coded. need to change
                        productCollection.Add(room);
                    }
                    if (productCollection.Count > 0)
                    {
                        searchResultItem.Products = productCollection;
                        lbProviderCollection.Add(searchResultItem);
                    }
                }
            }
            return lbProviderCollection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="itemid"></param>
        /// <returns></returns>
        private Item GetSiteCoreItemDetails(string itemid)
        {
            Item outputitem = null;

            try
            {
                outputitem = Sitecore.Context.Database.GetItem(new ID(itemid));
                return outputitem;
            }
            catch
            {
                return outputitem;
            }

        }
        /// <summary>
        /// Get provider details
        /// </summary>
        /// <param name="prodiverInfo"></param>
        /// <returns></returns>
        public IProvider GetProviderDetails(IBaseProvider providerInfo)
        {
            var siteCoreId = providerInfo.SitecoreItemId;
            if(string.IsNullOrEmpty(siteCoreId))
            {
                var hotel = UCommerce.EntitiesV2.Product.FirstOrDefault(i => i.Sku == providerInfo.ProviderId && i.ParentProduct == null);
                siteCoreId = hotel.ProductProperties.FirstOrDefault(i => i.ProductDefinitionField.Name.Equals("SiteCoreItemGuid")).Value;
            }

            Item currentSitecoreItem = GetSiteCoreItemDetails(siteCoreId);
            if (currentSitecoreItem != null)
            {
                Hotel provider = new Hotel();
                provider.ProviderId = providerInfo.ProviderId;
                provider.ProviderType = ProviderType.LoyaltyBuild;
                provider.SitecoreItemId = providerInfo.ProviderId;
                if (!string.IsNullOrWhiteSpace(currentSitecoreItem["Name"]))
                    provider.Name = currentSitecoreItem.Fields["Name"].ToString();
                if (!string.IsNullOrWhiteSpace(currentSitecoreItem["EmailID1"]))
                    provider.Email = currentSitecoreItem.Fields["EmailID1"].ToString();
                if (!string.IsNullOrWhiteSpace(currentSitecoreItem["Phone"]))
                    provider.Phone = currentSitecoreItem.Fields["Phone"].ToString();
                if (!string.IsNullOrWhiteSpace(currentSitecoreItem["Overview"]))
                    provider.Overview = currentSitecoreItem.Fields["Overview"].ToString();
                provider.SitecoreItem = currentSitecoreItem;
                var ranking = 0;
                int.TryParse(SitecoreFieldsHelper.GetDropLinkFieldValue(currentSitecoreItem, "StarRanking", "Name"), out ranking);
                provider.StarRanking = ranking;
                provider.Location = SitecoreFieldsHelper.GetLookupFieldTargetItemValue(currentSitecoreItem, "Location", "Name");
                if (!string.IsNullOrWhiteSpace(currentSitecoreItem["Town"]))
                    provider.Town = currentSitecoreItem.Fields["Town"].ToString();
                provider.Facilities = SitecoreFieldsHelper.GetMutiListItems(currentSitecoreItem, "Facilities").ToList();
                if (!string.IsNullOrWhiteSpace(currentSitecoreItem["Themes"]))
                    provider.Themes = currentSitecoreItem.Fields["Themes"].ToString();
                if (!string.IsNullOrWhiteSpace(currentSitecoreItem["Experiences"]))
                    provider.Experiences = currentSitecoreItem.Fields["Experiences"].ToString();
                if (!string.IsNullOrWhiteSpace(currentSitecoreItem["AddressLine1"]))
                     provider.AddressLine1 = currentSitecoreItem["AddressLine1"];
                if (!string.IsNullOrWhiteSpace(currentSitecoreItem["AddressLine2"]))
                    provider.AddressLine2 = currentSitecoreItem["AddressLine2"];
                if (!string.IsNullOrWhiteSpace(currentSitecoreItem["AddressLine3"]))
                    provider.AddressLine3 = currentSitecoreItem["AddressLine3"];
                if (!string.IsNullOrWhiteSpace(currentSitecoreItem["AddressLine4"]))
                    provider.AddressLine4 = currentSitecoreItem["AddressLine4"];
                provider.Country = SitecoreFieldsHelper.GetLookupFieldTargetItemValue(currentSitecoreItem, "Country", "CountryName");

                var address = new List<string>();
                if (!string.IsNullOrWhiteSpace(provider.AddressLine1))
                    address.Add(provider.AddressLine1.Trim());
                if (!string.IsNullOrWhiteSpace(provider.AddressLine2))
                    address.Add(provider.AddressLine2.Trim());
                if (!string.IsNullOrWhiteSpace(provider.AddressLine3))
                    address.Add(provider.AddressLine3.Trim());
                if (!string.IsNullOrWhiteSpace(provider.AddressLine4))
                    address.Add(provider.AddressLine4.Trim());
                if (!string.IsNullOrWhiteSpace(provider.Location))
                    address.Add(provider.Location.Trim());
                if (!string.IsNullOrWhiteSpace(provider.Country))
                    address.Add(provider.Country.Trim());
                provider.Address = string.Join(", ", address);
                //provider.CurrencyID = Convert.ToInt32(currentSitecoreItem.Fields["CurrencyID"]);
                //provider.IntroductionImage = currentSitecoreItem.Fields["Introduction Image"].ToString();
                //provider.StarRanking = SitecoreFieldsHelper.GetDropLinkFieldValue(currentSitecoreItem, "StarRanking", "Name");
                //SitecoreFieldsHelper.GetMutiListItems(currentSitecoreItem, "Facilities");
                //SitecoreFieldsHelper.GetMutiListItems(currentSitecoreItem, "Themes")
                //SitecoreFieldsHelper.GetMutiListItems(currentSitecoreItem, "Experiences")
                //StoreHelper.GetPriceWithCurrency(price, null, this.UcommerceCurrencyDetails.ISOCode        
                //provider.Products
                return provider;
            }
            return null;            
        }

        /// <summary>
        /// Get Particular Provider Details with its Products
        /// </summary>
        /// <param name="providerSearch"></param>
        /// <returns></returns>
        public IProvider GetProviderWithProduct(ProviderSearch providerSearch)
        {
            var hotel = GetProviderDetails(providerSearch);

            //Room products = new Room();
            List<IProduct> productDetails = new List<IProduct>();
            
            var service = new RoomAvailabilityService();
            var details = service.GetProductDetailByProviderId(providerSearch.ProviderId);
            foreach (var item in details)
            {
                if (item.ProductType == 1 || item.ProductType == 3)
                {
                    IProduct product = new Room();// (item.ProductType == 1) ? (IProduct)new Room() : (IProduct)new Package();

                    product.Name = item.ProductName;
                    product.Price = item.ProductPrice;
                    product.Sku = item.CombineSKU.Split('@').First(); 
                    product.VariantSku = item.ProductSKU;
                    product.Type = ProductType.Room;// (item.ProductType == 1) ? ProductType.Room : ProductType.Package;
                    productDetails.Add(product);
                }
            }
            hotel.Products = productDetails;
            return hotel;
        }
    }
}
