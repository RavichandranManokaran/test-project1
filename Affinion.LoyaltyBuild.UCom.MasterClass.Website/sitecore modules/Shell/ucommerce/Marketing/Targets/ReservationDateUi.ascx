﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReservationDateUi.ascx.cs" Inherits="Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.ucommerce.Marketing.Targets.ReservationDateUi" %>



<%--<link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
      <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
      <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
      <script>
          $(function () {
              
              $('<%= datepicker1.ClientID %>').datepicker();
              $('<%= datepicker1.ClientID %>').datepicker("setDate", "10w+1");
          });
      </script>--%>

<table cellpadding="0" cellspacing="0" style="width: 100%;">
    <tr>
        <td>
            <asp:PlaceHolder runat="server" ID="NonEditModePlaceHolder">
                <asp:Literal runat="server" ID="ReadOnlyMessage"></asp:Literal>
            </asp:PlaceHolder>

            <asp:PlaceHolder runat="server" ID="EditModePlaceHolder" Visible="false">

                <%--<p>Enter Date: <input type="text" runat="server" id="datepicker1"></p>--%>
                <p>
                    From Date :<asp:TextBox runat="server" ID="txtFromDate" placeholder="mm/dd/yyyy"></asp:TextBox><br />
                    To Date :
                    <asp:TextBox runat="server" ID="txtToDate" placeholder="mm/dd/yyyy"></asp:TextBox>
                </p>
                <br />

                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                    ControlToValidate="txtFromDate" ErrorMessage="Invalid date format."
                    ValidationExpression="((0[1-9]|1[0-2])\/((0|1)[0-9]|2[0-9]|3[0-1])\/((19|20)\d\d))$" Display="Dynamic"></asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                    ControlToValidate="txtToDate" ErrorMessage="Invalid date format."
                    ValidationExpression="((0[1-9]|1[0-2])\/((0|1)[0-9]|2[0-9]|3[0-1])\/((19|20)\d\d))$" Display="Dynamic"></asp:RegularExpressionValidator>

                <asp:CustomValidator ID="fromdatevalidator" runat="server" OnServerValidate="ValidateDate"
                    ControlToValidate="txtFromDate" ErrorMessage="Invalid From Date."  ValidationGroup="Group3" Display="Dynamic" />

                <asp:CustomValidator ID="todatevalidator" runat="server" OnServerValidate="ValidateDate"
                    ControlToValidate="txtToDate" ErrorMessage="Invalid To Date." ValidationGroup="Group3" Display="Dynamic" />
            </asp:PlaceHolder>
        </td>
        <td style="width: 50px; text-align: right; vertical-align: top;">
            <asp:ImageButton ID="EditButton" runat="server" ImageUrl="../../Images/pencil.png" meta:resourcekey="Edit" OnClick="EditButton_Click" />
            <asp:ImageButton ID="SaveButton" runat="server" ImageUrl="../../Images/save.gif" meta:resourcekey="Save" Visible="false" ValidationGroup="Group3" OnClick="SaveButton_Click"/>
            <asp:ImageButton ID="DeleteButton" runat="server" ImageUrl="../../Images/ui/cross.png" meta:resourcekey="Delete" OnClick="DeleteButton_Click" /><br />
        </td>
    </tr>
</table>

