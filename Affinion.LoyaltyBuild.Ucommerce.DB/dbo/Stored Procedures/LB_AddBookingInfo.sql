﻿CREATE  PROCEDURE [dbo].[LB_AddBookingInfo]
	@OrderLineId as int
AS
BEGIN
DECLARE @CheckInDate datetime, @CheckOutDate datetime, @CreatedBy varchar(100)
	DECLARE @id uniqueidentifier
SET @id = NEWID()
IF not EXISTS (SELECT 1 FROM dbo.LB_BookingInfo WHERE OrderLineId = @OrderLineId)
	BEGIN
		IF EXISTS (SELECT 1 FROM uCommerce_OrderProperty WHERE OrderLineId = @OrderLineId)	
		BEGIN
			select @CheckInDate = _CheckinDate, @CheckOutDate=_CheckoutDate, @CreatedBy=Createdby
			from 
			(
			  select OrderLineId, [key], [value]
			  from uCommerce_OrderProperty where OrderLineId = @OrderLineId
			) src
			pivot
			(
			  max([value])
			  for [key] in (_CheckinDate, _CheckoutDate, Createdby)
			) piv;
		 
			INSERT INTO [dbo].LB_BookingInfo
				( 
					BookingId,
					OrderLineId,
					CheckinDate,
					CheckOutDate,
					StatusId,				
					CreatedBy,
					CreatedDate,
					UpdatedBy,
					UpdatedDate
				)
				VALUES
				(
					@id,
					@OrderLineId,
					@CheckinDate,
					@CheckOutDate,
					1,
					@CreatedBy,
					Getdate(),
					@CreatedBy,
					Getdate()
				)
				--add status log
				INSERT INTO [dbo].[LB_OrderLineStatusAudit]
				([OrderLineID],
				[StatusId],
				[CreatedBy],
				[CreatedOn]
				)
				 VALUES
				(@OrderLineId,
				1,
				@CreatedBy,
				GETDATE()
				)			
		END
	END
END
GO
