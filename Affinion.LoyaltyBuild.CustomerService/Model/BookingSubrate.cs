﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/


namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    /// <summary>
    /// Booking subrate info
    /// </summary>
    public class BookingSubrate
    {
        /// <summary>
        /// Subrate ID
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Subrate Code
        /// </summary>
        public string SubrateCode { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Price
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Discount Amount
        /// </summary>
        public decimal Discount { get; set; }

        /// <summary>
        /// Total Amount
        /// </summary>
        public decimal Total
        {
            get
            {
                return Price - Discount;
            }
        }
    }
}
