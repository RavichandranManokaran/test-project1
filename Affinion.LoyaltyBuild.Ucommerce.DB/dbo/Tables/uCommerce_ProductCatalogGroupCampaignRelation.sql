﻿CREATE TABLE [dbo].[uCommerce_ProductCatalogGroupCampaignRelation] (
    [ProductCatalogGroupCampaignRelationId] INT IDENTITY (1, 1) NOT NULL,
    [CampaignId]                            INT NULL,
    [ProductCatalogGroupId]                 INT NULL,
    CONSTRAINT [uCommerce_PK_ProductCatalogGroupCampaignRelation] PRIMARY KEY CLUSTERED ([ProductCatalogGroupCampaignRelationId] ASC),
    FOREIGN KEY ([CampaignId]) REFERENCES [dbo].[uCommerce_Campaign] ([CampaignId]),
    FOREIGN KEY ([ProductCatalogGroupId]) REFERENCES [dbo].[uCommerce_ProductCatalogGroup] ([ProductCatalogGroupId])
);

