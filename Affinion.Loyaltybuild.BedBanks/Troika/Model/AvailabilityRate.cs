﻿namespace Affinion.LoyaltyBuild.BedBanks.Troika.Model
{
    public class AvailabilityRate
    {
        public int ClientOfferId;
        public int ClientOfferItemId;
        public int CoicrId;
        public int CountryId;
        public int CurrencyCodeId;
        public string CurrencyName;
        public string Description;
        public int IplId;
        public string IplLevel;
        public int IsCore;
        public int IsHiddenGem;
        public string ItemId;
        public int NightsStay;
        public int Priority;
        public string RateId;
        public int RateTypeId;
        public int Savings;
        public int TimeLimitedOfferNoCancelation;
        public string Value;
        public string WholesaleValue;
        public string CommissionValue;
        public int MarginIdApplied;
    }
}
