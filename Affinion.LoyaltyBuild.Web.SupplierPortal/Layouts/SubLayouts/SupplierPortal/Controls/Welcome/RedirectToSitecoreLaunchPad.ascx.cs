﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;
using Sitecore.Security.Accounts;
using Sitecore.Publishing;
using Sitecore.Security.Authentication;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Welcome
{
    /// <summary>
    /// class redirect to sitecore launchPad 
    /// </summary>
    public partial class RedirectToSitecoreLaunchPad : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                RedirectToLaunchPad();
            }
        }



        #region Private methods
        /// <summary>
        /// Redirect to sitecore launch pad method
        /// </summary>
        private void RedirectToLaunchPad()
        {
            var CurrentUserRole = SitecoreUserProfile.GetItem(UserProfileKeys.currentLoggedinUserRole);

            if (CurrentUserRole != null && 
                (CurrentUserRole.Equals(Constants.superUser,StringComparison.InvariantCultureIgnoreCase)
                || CurrentUserRole.Equals(Constants.accountManagement,StringComparison.InvariantCultureIgnoreCase)
                || CurrentUserRole.Equals(Constants.marketing, StringComparison.InvariantCultureIgnoreCase)
                || CurrentUserRole.Equals(Constants.lbAdminRole, StringComparison.InvariantCultureIgnoreCase)
                || CurrentUserRole.Equals(Constants.IT, StringComparison.InvariantCultureIgnoreCase)))
            {
                var currentLoggedinUser = SitecoreUserProfile.GetItem(UserProfileKeys.loggedinDomainUser);
                if (currentLoggedinUser != null)
                {
                    string newTicket = Sitecore.Web.Authentication.TicketManager.CreateTicket(currentLoggedinUser, @"~/sitecore/client/Applications/Launchpad");
                    Sitecore.Web.Authentication.TicketManager.Relogin(newTicket);

                    if (this.Request.QueryString["sc_mode"] == "preview")
                    {
                        using (new UserSwitcher(currentLoggedinUser, true))
                            PreviewManager.StoreShellUser(Sitecore.Configuration.Settings.Preview.AsAnonymous);
                    }
                    else
                    {
                        AuthenticationManager.Login(currentLoggedinUser);
                        Response.Redirect(@"~/sitecore/client/Applications/Launchpad");
                    }
                }

            }
        }
        #endregion
    }
}