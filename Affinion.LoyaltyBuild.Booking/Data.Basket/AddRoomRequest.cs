﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Api.Booking.Data.Basket
{
    /// <summary>
    /// Add to basket room request
    /// </summary>
    [Serializable]
    public class AddRoomRequest : IAddToBasketRequest
    {
        /// <summary>
        /// Check In Date
        /// </summary>
        [OrderProperty(Constants.CheckinDate)]
        public DateTime CheckinDate { get; set; }

        /// <summary>
        /// Check Out Date
        /// </summary>
        [OrderProperty(Constants.CheckoutDate)]
        public DateTime CheckOutDate { get; set; }
        
        /// <summary>
        /// NoOfAdults Property
        /// </summary>
        [OrderProperty(Constants.NoOfAdults)]
        public int NoOfAdults { get; set; }
        
        /// <summary>
        /// NoOfChildren Property
        /// </summary>
        [OrderProperty(Constants.NoOfChildren)]
        public int NoOfChildren { get; set; }

        /// <summary>
        /// RoomType Property
        /// </summary>
        [OrderProperty(Constants.RoomType)]
        public string RoomType { get; set; }

        /// <summary>
        /// BookingThrough Property
        /// </summary>
        [OrderProperty(Constants.BookingThrough)]
        public string BookingThrough { get; set; }

        /// <summary>
        /// Nights Property
        /// </summary>
        [OrderProperty(Constants.Nights)]
        public int NoofNights { get; set; }
       
        /// <summary>
        /// CreatedBy Property
        /// </summary>
        [OrderProperty(Constants.CreatedBy)]
        public string CreatedBy { get; set; }

        /// <summary>
        /// CurrencyID Property
        /// </summary>
        [OrderProperty(Constants.CurrencyId)]
        public int CurrencyId { get; set; }
        
        /// <summary>
        /// Sku
        /// </summary>
        public string Sku { get; set; }

        /// <summary>
        /// Variant Sku
        /// </summary>
        public string VariantSku { get; set; }
        
        /// <summary>
        /// NoOfRooms Property
        /// </summary>
        [OrderProperty(Constants.NoOfRooms)]
        public int NoOfRooms { get; set; }
        
        /// <summary>
        /// ChildrenAges Property
        /// </summary>
        [OrderProperty(Constants.ChildrenAges)]
        public string ChildrenAges { get; set; }

        /// <summary>
        /// Destination Property
        /// </summary>
        [OrderProperty(Constants.Destination)]
        public string Destination { get; set; }

        /// <summary>
        /// OrderLineId Property
        /// </summary>
        [OrderProperty(Constants.OrderLineId)]
        public int OrderLineId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [OrderProperty(Constants.ClientId)]
        public string ClientId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [OrderProperty(Constants.BookingReferenceNo)]
        public string BookingReferenceNo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [OrderProperty(Constants.UserSelectedCampaign)]
        public int? CampaignId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
           
        }
    }
}
