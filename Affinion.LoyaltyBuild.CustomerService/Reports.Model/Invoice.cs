﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model
{
    public class Invoice
    {
        public string HotelID { get; set; }
        public DateTime DemandDate { get; set; }
        public string BookingRefNo { get; set; }
        public string BookingID { get; set; }
        public string CustomerName { get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }
        public int PaymentStatus { get; set; }
        public decimal GrossAmount { get; set; }
        public decimal NetAmount { get; set; }
        public decimal SuggestedAmount { get; set; }
        public decimal? PostedAmount { get; set; }
        public DateTime DatePostedToOracle { get; set; }
        public string Currency { get; set; }
        public int CurrencyID { get; set; }
        public string OracleFieID { get; set; }
        public int IsDeleted { get; set; }
        public int IsSubmittedToOracle { get; set; }
        public int CustomerID { get; set; }
        public DateTime BookingDate { get; set; }
        public string ClientID { get; set; }
        public string ProviderID { get; set; }
        public int OrderID { get; set; }
        public int OrderLineID { get; set; }

    }
}
