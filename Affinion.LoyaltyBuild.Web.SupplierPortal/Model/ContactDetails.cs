﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Helper
{
    public class ContactDetails
    {
        public string ItemId { get; set; }
        public string ContactName { get; set; }
        public string JobTitle { get; set; }
        public string ContactEmail { get; set; }
        public string ContactDirectPhone { get; set; }
    }
}