﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Store.aspx.cs" Inherits="Affinion.LoyaltyBuild.UCom.MasterClass.Website.Layouts.Store" %>
<%@ Register assembly="Sitecore.Kernel" namespace="Sitecore.Web.UI.WebControls" tagPrefix="sc" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
            <a href="/Store/basket.aspx">Basket</a>
            <p>Loyalty Build store!</p>
        </div>
        <sc:placeholder key="menu" runat="server" />
		<sc:placeholder key="main" runat="server" /> 
    </div>
    </form>
</body>
</html>
