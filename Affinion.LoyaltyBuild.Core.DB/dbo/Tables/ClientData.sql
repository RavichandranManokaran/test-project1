﻿CREATE TABLE [dbo].[ClientData] (
    [SessionKey] NVARCHAR (50)    NOT NULL,
    [Data]       NTEXT            NOT NULL,
    [Accessed]   DATETIME         NOT NULL,
    [ID]         UNIQUEIDENTIFIER CONSTRAINT [DF_ClientData_ID] DEFAULT (newid()) NOT NULL,
    [DAC_Index]  INT              NULL
);


GO
CREATE CLUSTERED INDEX [DAC_Clustered]
    ON [dbo].[ClientData]([DAC_Index] ASC);


GO
CREATE NONCLUSTERED INDEX [ndxKey]
    ON [dbo].[ClientData]([SessionKey] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [ndxID]
    ON [dbo].[ClientData]([ID] ASC);

