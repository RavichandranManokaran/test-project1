﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ViewAllPolls.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.Test.Layouts.SubLayouts.ProviderSetup.PollingManager.ViewAllPolls" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Sitecore.Data.Items" %>


<div id="repeaterDiv" runat="server">
    <asp:Repeater ID="getavailablePolls" runat="server">
        <ItemTemplate>
            <input type="radio" name="selectPollRadioButton" value="<%#((Item)Container.DataItem).ID.ToGuid().ToString() %>" />
            <sc:Text Field="PollTitle" Item="<%#(Item)Container.DataItem %>" runat="server" />
        </ItemTemplate>
    </asp:Repeater>
</div>
<asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>

<asp:Button ID="SubmitButton" runat="server" Text="Button" OnClientClick="OnSubmitButtonClick()" OnClick="SubmitButton_Click" />