﻿using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.EntitiesV2;
using UCommerce.Presentation.Web;
using UCommerce.Presentation.Web.Pages;
using UCommerce.Web.UI.Catalog.Dialogs;

namespace UCommerce.Web.UI.Marketing.Dialogs
{
    public partial class SelectSupplierTarget : ProtectedPage
    {
        // Fields


        // Methods
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.SaveButton.Text = "Save";
                List<Category> supplier = Category.All().Where(x => x.Definition.Name.Equals("Hotel")).ToList();
                listSupplier.DataSource = supplier;
                listSupplier.DataTextField = "Name";
                listSupplier.DataValueField = "CategoryId";
                listSupplier.DataBind();
            }
        }

        protected void SaveButton_Clicked(object sender, EventArgs e)
        {
            //int id = this.catalogItemSelector.GetSelectedValues().FirstOrDefault<int>();


            if (this.listSupplier.SelectedItem == null)
            {
                this.Page.ClientScript.RegisterClientScriptBlock(base.GetType(), "close", base.JavaScriptFactory.CreateJavascript(new string[] { base.JavaScriptFactory.CloseModalWindowFunction() }));
            }
            else
            {

                int num2;
                Category category = Category.Get(Convert.ToInt32(this.listSupplier.SelectedValue));
                int.TryParse(base.Request.QueryString["TargetId"], out num2);
                SupplierTarget target = SupplierTarget.Get(num2);
                List<string> supplierId = target.SupplierId.Split('|').ToList();
                if (!supplierId.Any(x => x.Equals(category.CategoryId.ToString())))
                {
                    target.Name = AddPipe(target.Name ?? string.Empty, category.Name);
                    target.SupplierId = AddPipe(target.SupplierId ?? string.Empty, category.CategoryId.ToString());
                    target.Save();
                }
                this.Page.ClientScript.RegisterClientScriptBlock(base.GetType(), "refresh", base.JavaScriptFactory.CreateJavascript(new string[] { base.JavaScriptFactory.ContentFrameFunction(string.Format("/marketing/editcampaignitem.aspx?id={0}&parent={1}", QueryString.Common.Id, QueryString.Common.ParentId)), base.JavaScriptFactory.CloseModalWindowFunction() }));
            }
        }

        private string AddPipe(string source, string addedstring)
        {
            return source + addedstring + "|";
        }

    }




}
