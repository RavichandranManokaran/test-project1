﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           EmailHelper.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.BusinessLogic
/// Description:           Consists of Email Scheduler Helpers
/// </summary>

using Affinion.LoyaltyBuild.Common.Exceptions;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.Loyaltybuild.BusinessLogic.Helper
{
    public class EmailHelper
    {
        /// <summary>
        /// Get the Post Break Email Details
        /// </summary>
        /// <returns>Post Break Email related Data Set</returns>
        public static DataSet GetPostBreakEmailDetails(string clientId)
        {
            try
            {
                DataSet ds = new DataSet();
                ds = uCommerceConnectionFactory.GetPostBreakEmailDetails(clientId);
                return ds;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }
    }
}
