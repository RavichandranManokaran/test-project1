﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           AffinionCache.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common.Caching.AffinionCache
/// Description:           Extend the Sitecore cache

#region Using Statements

using Sitecore.Caching;

#endregion

namespace Affinion.LoyaltyBuild.Common.Caching
{
    public class AffinionCache : CustomCache
    {
        #region Public Methods

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="name"></param>
        /// <param name="maxSize"></param>
        public AffinionCache(string name, long maxSize)
            : base(name, maxSize)
        {

        }

        /// <summary>
        /// Set cache value
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public new void SetString(string key, string value)
        {
            base.SetString(key, value);
        }

        /// <summary>
        /// Get cache value
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public new string GetString(string key)
        {
            return base.GetString(key);
        }

        #endregion
    }
}
