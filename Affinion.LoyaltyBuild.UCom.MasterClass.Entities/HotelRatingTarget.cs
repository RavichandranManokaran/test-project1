﻿
using Affinion.LoyaltyBuild.UCom.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;
using UCommerce.Marketing.Targets;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class HotelRatingTarget : Target, IOrderLineTarget, IEntity, ITarget
    {
        private int? _hashCode;

        public virtual bool IsSatisfiedBy(OrderLine orderLine)
        {
            OrderProperty forSearch = orderLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.ForSearch);
            if (forSearch != null)
            {
                OrderProperty hotelRating = orderLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.HotelRating);
                if (hotelRating != null)
                {
                    return this.HotelRating.Equals(hotelRating.Value, StringComparison.InvariantCultureIgnoreCase);
                }
                return false;
            }
            else
            {
                Product product = Product.All().Where(x => x.Sku == orderLine.Sku).FirstOrDefault();
                ProductProperty property = product[ProductDefinationConstant.HotelRating];
                if (property != null && this.HotelRating == (property.Value))
                {
                    return true;
                }
            }
            return false;

        }

        // Methods
        public new static IQueryable<HotelRatingTarget> All()
        {
            return GetRepo().Select();
        }

        public override void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<HotelRatingTarget, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            HotelRatingTarget objA = obj as HotelRatingTarget;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<HotelRatingTarget, bool>> expression)
        {
            return All().Any<HotelRatingTarget>(expression);
        }

        public static IList<HotelRatingTarget> Find(Expression<Func<HotelRatingTarget, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<HotelRatingTarget>();
        }

        public static HotelRatingTarget FirstOrDefault(Expression<Func<HotelRatingTarget, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<HotelRatingTarget>();
        }

        public new static HotelRatingTarget Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<HotelRatingTarget> GetRepo()
        {
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<HotelRatingTarget>>();
        }


        public override void Save()
        {
            GetRepo().Save(this);
        }

        public static HotelRatingTarget SingleOrDefault(Expression<Func<HotelRatingTarget, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties
        public override int Id
        {
            get
            {
                return this.HotelRatingTargetId;
            }
        }

        public virtual int HotelRatingTargetId { get; protected set; }

        public virtual string HotelRating { get; set; }



    }
}