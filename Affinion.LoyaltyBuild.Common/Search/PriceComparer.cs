﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SearchHelper.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common.Search
/// Description:           Compares the price
/// </summary>
namespace Affinion.LoyaltyBuild.Common.Search
{
    using Sitecore.ContentSearch.SearchTypes;
    using System.Collections;
    using System.Collections.Generic;

    /// <summary>
    /// Compares the price
    /// </summary>
    public class PriceComparer : IComparer<SupplierSearchResultItem>
    {
        public int Compare(SupplierSearchResultItem x, SupplierSearchResultItem y)
        {
            if (x == null || y == null)
            {
                return 0;
            }

            return x.LowestPrice.CompareTo(y.LowestPrice);
        }
    }
}
