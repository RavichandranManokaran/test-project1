﻿
using Affinion.LoyaltyBuild.UCom.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;
using UCommerce.Marketing.Targets;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class NumberOfChildTarget : Target, IOrderLineTarget, IEntity, ITarget
    {
        private int? _hashCode;

        public virtual bool IsSatisfiedBy(OrderLine orderLine)
        {
            OrderProperty searchIgnor = orderLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.IgnorCampaignSearchCriteria);
            if (searchIgnor != null)
                return true;

            OrderProperty childProperty = orderLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.NoOfChildKey);
            if (childProperty != null)
            {
                if (childProperty.Value == this.NumberOfChild.ToString())
                    return true;
            }
            return false;
        }



        // Methods
        public new static IQueryable<NumberOfChildTarget> All()
        {
            return GetRepo().Select();
        }

        public override void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<NumberOfChildTarget, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            NumberOfChildTarget objA = obj as NumberOfChildTarget;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<NumberOfChildTarget, bool>> expression)
        {
            return All().Any<NumberOfChildTarget>(expression);
        }

        public static IList<NumberOfChildTarget> Find(Expression<Func<NumberOfChildTarget, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<NumberOfChildTarget>();
        }

        public static NumberOfChildTarget FirstOrDefault(Expression<Func<NumberOfChildTarget, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<NumberOfChildTarget>();
        }

        public new static NumberOfChildTarget Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<NumberOfChildTarget> GetRepo()
        {
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<NumberOfChildTarget>>();
        }


        public override void Save()
        {
            GetRepo().Save(this);
        }

        public static NumberOfChildTarget SingleOrDefault(Expression<Func<NumberOfChildTarget, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties
        public override int Id
        {
            get
            {
                return this.NumberOfChildTargetId;
            }
        }

        public virtual int NumberOfChildTargetId { get; protected set; }

        public virtual int NumberOfChild { get; set; }



    }
}
