﻿/****** Object:  StoredProcedure [dbo].[LB_SaveOrderLineDue]    Script Date: 2/12/2016 11:39:51 AM ******/
DROP PROCEDURE [dbo].[LB_SaveOrderLineDue]
GO

/****** Object:  StoredProcedure [dbo].[LB_SaveOrderLineDue]    Script Date: 2/12/2016 11:39:51 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[LB_SaveOrderLineDue] 
(
    -- Add the parameters for the stored procedure here
	@OrderLineDueId INT = NULL,
	@OrderLineID INT,
    @SubrateItemCode VARCHAR(50),
	@OrderLinePaymentID INT,
	@CurrencyTypeId INT,
    @DueTypeId INT,
    @Amount MONEY ,
    @CommissionAmount MONEY ,
    @TransferAmount MONEY ,
	@CancellationAmount MONEY,
    @ClientAmount MONEY ,
    @ProviderAmount MONEY,
    @CollectionByProviderAmount MONEY ,
    @Note VARCHAR(MAX),
	@CreatedBy VARCHAR(50),
	@CreatedDate DATETIME,
	@PaymentMethodId INT
)
AS
BEGIN
	DECLARE @Result INT

	IF(@OrderLineDueId IS NULL)
	BEGIN
		--Insert a new Due
		INSERT INTO [dbo].[LB_OrderLineDue]
			(
			[OrderLineID]
			,[SubrateItemCode]
			,[OrderLinePaymentID]
			,[CurrencyTypeId]
			,[DueTypeId]
			,[Amount]
			,[CommissionAmount]
			,[TransferAmount]
			,[CancellationAmount]
			,[CollectionByProviderAmount]
			,[ClientAmount]
			,[ProviderAmount]
			,[Note]
			,[CreatedDate]
			,[IsDeleted]
			,[PaymentMethodId]
			)
		VALUES
			(
			@OrderLineID,
			@SubrateitemCode,
			@OrderLinePaymentID,
			@CurrencyTypeId,
			@DueTypeId,
			@Amount,
			@CommissionAmount,
			@TransferAmount,
			@CancellationAmount,
			@ClientAmount, 
			@ProviderAmount,
			@CollectionByProviderAmount,
			@Note, 
			GETDATE(),
			0,
			@PaymentMethodId
			)
	
		SET @Result=SCOPE_IDENTITY();
	END
	ELSE
	BEGIN
		--Update Due
		UPDATE [dbo].LB_OrderLineDue SET
			OrderLineId=@OrderLineID,
			DueTypeId=@DueTypeId,
			CurrencyTypeId=@CurrencyTypeId,
			Amount=@Amount,
			CommissionAmount=@CommissionAmount,
			TransferAmount=@TransferAmount,
			ClientAmount=@ClientAmount,
			ProviderAmount=@ProviderAmount,
			Note=@Note,
			UpdatedBy=@CreatedBy,
			UpdatedDate=GETDATE(),
			PaymentMethodID = @PaymentMethodId
		WHERE
			OrderLineDueId = @OrderLineDueId

		SET @Result =@OrderLineDueId;
	END
	
	--Return ID
	SELECT @Result
END

GO

