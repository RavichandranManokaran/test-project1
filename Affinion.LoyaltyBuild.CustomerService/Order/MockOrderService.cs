﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Sitecore.Web.UI.HtmlControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sitecore.Data.Items;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Order
{
    public class MockOrderService
    {
        /// <summary>
        /// Gets the Booking information along with the order line info for just the provided order id
        /// </summary>
        /// <param name="orderId">Order id to fetch</param>
        /// <param name="orderlineId">Order line id to fetch</param>
        /// <returns>Purchase orer details with order line info</returns>

        public Booking GetBooking(int orderlineId)
        {
            Booking booking = new Booking();
            IAccomodation accInfo = new Accomodation();

            booking.Customer.FirstName = "Prasanna";
            booking.Customer.LastName = "srinivasan";
            booking.Customer.Address = "DLF";
            booking.Customer.Phone = "9789917";
            booking.Customer.Email = "shrinivas@gmail.com";
            booking.ReservationDate = DateTime.Parse("2016-02-06 10:02:11.280");
            booking.NumberOfNights = 1;
            booking.NumberOfRooms = 1;
            booking.Deposit = 50000;
            accInfo.Title = "ITC";
            accInfo.Address = "Guindy";
            booking.OccupancyType = "Double bed room";
            booking.NumberOfChildren = 3;
            booking.NumberOfAdults = 4;
            booking.BookingReference = "25666";

            booking.BookingReference = "922770417";
            booking.CheckInDate = DateTime.Parse("2016-01-12 10:02:11.280");
            booking.CheckOutDate = DateTime.Parse("2016-01-13 10:02:11.290");
            booking.Price = 2800;
            booking.CurrencyText = "Rs. ";
            booking.AccomodationInfo = accInfo;
            booking.IsCancelled = false;

            accInfo.HotelItemId = "{F891A780-E574-4767-8098-BA55E967615C}";

            Sitecore.Data.ID hotelItemId;
            if (Sitecore.Data.ID.TryParse(accInfo.HotelItemId, out hotelItemId))
            {
                Item hotelItem = Sitecore.Context.Database.GetItem(hotelItemId);
                if (hotelItem != null)
                {
                    Sitecore.Data.Fields.ImageField hotelimgFld = hotelItem.Fields["IntroductionImage"];
                    if (hotelimgFld != null && hotelimgFld.MediaItem != null)
                    {
                        Sitecore.Data.Items.MediaItem image = new Sitecore.Data.Items.MediaItem(hotelimgFld.MediaItem);
                        accInfo.ImageUrl = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image));
                    }
                    accInfo.Title = hotelItem.DisplayName;
                    accInfo.EmailId = (hotelItem.Fields["EmailID2"] != null) ? hotelItem.Fields["EmailID2"].Value : string.Empty;
                    accInfo.Phone = (hotelItem.Fields["Phone"] != null) ? hotelItem.Fields["Phone"].Value : string.Empty;
                    accInfo.GeoCoordinates = (hotelItem.Fields["GeoCoordinates"] != null) ? hotelItem.Fields["GeoCoordinates"].Value : string.Empty;
                    
                    string addressLine1 = (hotelItem.Fields["AddressLine1"] != null) ? hotelItem.Fields["AddressLine1"].Value : string.Empty;
                    string addressLine2 = (hotelItem.Fields["AddressLine2"] != null) ? hotelItem.Fields["AddressLine2"].Value : string.Empty;
                    string addressLine3 = (hotelItem.Fields["AddressLine3"] != null) ? hotelItem.Fields["AddressLine3"].Value : string.Empty;
                    string addressLine4 = (hotelItem.Fields["AddressLine4"] != null) ? hotelItem.Fields["AddressLine4"].Value : string.Empty;
                    string town = (hotelItem.Fields["Town"] != null) ? hotelItem.Fields["Town"].Value : string.Empty;

                    Item countryItem = (hotelItem.Fields["Country"] != null && !string.IsNullOrWhiteSpace(hotelItem.Fields["Country"].Value)) ? ((Sitecore.Data.Fields.LookupField)hotelItem.Fields["Country"]).TargetItem : null;
                    string countryName = countryItem.Fields["CountryName"].Value;
                    accInfo.Address = string.Format("{0}{1}{2}{3}{4}{5}", addressLine1, addressLine2, addressLine3, addressLine4, town, countryName);
                }
            }

            return booking;
        }

    }
}
