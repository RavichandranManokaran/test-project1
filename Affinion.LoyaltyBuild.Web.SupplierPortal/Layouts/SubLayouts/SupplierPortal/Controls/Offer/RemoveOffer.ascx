﻿<%@ Control EnableViewState="false" Language="C#" AutoEventWireup="true" CodeBehind="RemoveOffer.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Offer.RemoveOffer" %>
<table class="table tbl-offer">
    <tbody>
        <tr class="offers">
            <th class="alert-info">Loyaltybuild Offers </th>
            <th class="alert-info">Hotel Net rate per break incl VAT </th>
            <th class="alert-info">Net rate paid to hotel at time of break</th>

        </tr>
        <tr>
            <td><%= offerDetail.OfferName %></td>
            <td><%= offerDetail.HotelDiscountedRate.ToString("0.##") %></td>
            <td><%= offerDetail.HotelRate.ToString("0.##") %></td>
        </tr>
    </tbody>
</table>

<table class="table tbl-offer">
    <tbody>
        <tr class="offers">
            <th class="alert-info">Bookings Received </th>
            <th class="alert-info">Revenue Generated </th>
            <th class="alert-info">Future Arrivals</th>

        </tr>
        <tr>
            <td><%= offerDetail.BookingReceived %></td>
            <td><%= offerDetail.RevenueGenerated.ToString("0.##") %></td>
            <td><%= offerDetail.FutureArrival.ToString("0.##") %></td>
        </tr>
    </tbody>
</table>
<div class="panel panel-default">
    <div class="panel-heading">Reason for opting out of this promotion</div>
    <div class="panel-body alert-info">
        <table class="table tbl-offer no-margin">
            <tbody>
                <tr>
                    <td>
                        <form id="frm1" runat="server">
                            <asp:DropDownList ID="drpReasonList" CssClass="form-control" ClientIDMode="Static" runat="server">
                            </asp:DropDownList>
                        </form>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="alert alert-warning">
    <strong>Please note</strong> this promotion will now appear on list of offers hotel are NOT participating on.
    Participate again at any time
</div>
