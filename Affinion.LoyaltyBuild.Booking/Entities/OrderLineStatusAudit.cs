﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;

namespace Affinion.LoyaltyBuild.Api.Booking.Entities
{
    public class OrderLineStatusAudit : IAuditCreatedEntity, IEntity
    {
        private int? _hashCode;
        internal static IRepository<OrderLineStatusAudit> _repo;

        public virtual int Id
        {
            get
            {
                return this.OrderLineStatusAuditId;
            }
        }

        public virtual int OrderLineStatusAuditId { get; protected set; }

        public virtual DateTime CreatedOn { get; set; }

        public virtual string CreatedBy { get; set; }

        public virtual OrderStatus Status { get; set; }

        public virtual OrderLine OrderLine { get; set; }

        public static bool operator ==(OrderLineStatusAudit x, OrderLineStatusAudit y)
        {
            if (object.ReferenceEquals((object)x, (object)y))
                return true;
            if (x == null || y == null)
                return false;
            return x.Equals((object)y);
        }

        public static bool operator !=(OrderLineStatusAudit x, OrderLineStatusAudit y)
        {
            return !(x == y);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
                this._hashCode = new int?(this.Id.GetHashCode());
            return this._hashCode.Value;
        }

        public override bool Equals(object obj)
        {
            OrderLineStatusAudit OrderLineStatusAudit = obj as OrderLineStatusAudit;
            if (object.ReferenceEquals((object)OrderLineStatusAudit, (object)null))
                return false;
            if (object.Equals((object)OrderLineStatusAudit.Id, (object)0) && object.Equals((object)this.Id, (object)0))
                return object.ReferenceEquals((object)this, obj);
            return this.Id.Equals(OrderLineStatusAudit.Id);
        }

        internal static IRepository<OrderLineStatusAudit> GetRepo()
        {
            if (OrderLineStatusAudit._repo != null)
                return OrderLineStatusAudit._repo;
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<OrderLineStatusAudit>>();
        }

        public static OrderLineStatusAudit Get(object id)
        {
            return OrderLineStatusAudit.GetRepo().Get(id);
        }

        public static OrderLineStatusAudit SingleOrDefault(Expression<Func<OrderLineStatusAudit, bool>> expression)
        {
            return OrderLineStatusAudit.GetRepo().SingleOrDefault(expression);
        }

        public static OrderLineStatusAudit FirstOrDefault(Expression<Func<OrderLineStatusAudit, bool>> expression)
        {
            return OrderLineStatusAudit.GetRepo().Select(expression).FirstOrDefault<OrderLineStatusAudit>();
        }

        public static bool Exists(Expression<Func<OrderLineStatusAudit, bool>> expression)
        {
            return OrderLineStatusAudit.All().Any<OrderLineStatusAudit>(expression);
        }

        public static IList<OrderLineStatusAudit> Find(Expression<Func<OrderLineStatusAudit, bool>> expression)
        {
            return (IList<OrderLineStatusAudit>)OrderLineStatusAudit.GetRepo().Select(expression).ToList<OrderLineStatusAudit>();
        }

        public static IQueryable<OrderLineStatusAudit> All()
        {
            return OrderLineStatusAudit.GetRepo().Select();
        }

        public virtual void Save()
        {
            OrderLineStatusAudit.GetRepo().Save(this);
        }

        public virtual void Delete()
        {
            OrderLineStatusAudit.GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<OrderLineStatusAudit, bool>> expression)
        {
            OrderLineStatusAudit.GetRepo().DeleteMany(expression);
        }
    }
}
