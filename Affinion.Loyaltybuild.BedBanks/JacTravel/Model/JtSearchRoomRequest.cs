﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Model
{
    [Serializable()]
    [XmlType(TypeName = "RoomRequest")]
    public class JtSearchRoomRequest
    {
        public string Adults { get; set; }
        public string Children { get; set; }
        public string Infants { get; set; }
        public List<JtChildAge> ChildAges { get; set; }
    }
}
