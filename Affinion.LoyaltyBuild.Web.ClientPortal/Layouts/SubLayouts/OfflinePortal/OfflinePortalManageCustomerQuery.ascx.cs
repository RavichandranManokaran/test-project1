﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using iTextSharp.text.pdf;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Customer;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
using Affinion.LoyaltyBuild.Web.ClientPortal.Helper;
using Sitecore.Data.Items;
using Affinion.LoyaltyBuild.Common.Utilities;



namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    public partial class OfflinePortalManageCustomerQuery : BaseSublayout
    {
        #region "Private"
        private int _orderLineId;
        private List<CustomerQuery> _customerQuerys;
        private CustomerQuery _customerQuery = new CustomerQuery();
        private QueryService _customerQueryService = new QueryService();
        private string _createdBy = string.Empty;
        private const string _errorCredirCardMessage = "Customer query content having credit card no.";
        #endregion
        #region "Protected Method"
        protected ValidationResponse ValidationResponse { get; private set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            int olid;
            int.TryParse(this.QueryString("olid"), out olid);
            _orderLineId = olid;            
            _createdBy = Sitecore.Security.Accounts.User.Current.LocalName;
            BindSitecoreTexts();
            if (!Page.IsPostBack)
            {
                BindGridViewHeaders(GvCustomerQuery);
                BindGridData();
                BindQueryType();
            }
        }

        protected void BtnSave_onclick(object sender, EventArgs e)
        {
            try
            {
                int quieryId = 0;
                int.TryParse(hfQueryId.Value, out quieryId);

                if (!HasCreditCardNumber(taContent.Value))
                {
                    _customerQuery.ID = quieryId;
                    _customerQuery.Content = taContent.Value;
                    _customerQuery.OrderLineId = _orderLineId;
                    _customerQuery.IsSolved = rbIsSolvedYes.Checked == true ? true : false;
                    _customerQuery.QueryTypeId = Convert.ToInt32(ddlQueryType.SelectedValue);
                    _customerQuery.CreatedBy = _createdBy;
                    SaveCustomerQuery(_customerQuery);
                    BindGridData();
                    ClearForm();
                }
                else
                    divErrorMsg.Visible = false;
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        protected void BtnNew_onclick(object sender, EventArgs e)
        {
            ClearForm();
        }

        protected void LinkButtonConvertExcel_Click(object sender, EventArgs e)
        {
            ExportGridToExcel();
        }

        protected void LinkButtonConvertPdf_Click(object sender, EventArgs e)
        {
            ExportGridToPdf();
        }

        protected void LinkButtonConvertDoc_Click(object sender, EventArgs e)
        {
            ExportToDocument();
        }

        protected void GvCustomerQuery_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {

                if (e.CommandName == "Select")
                {
                    LinkButton lbEdit = (LinkButton)e.CommandSource;
                    int i = Convert.ToInt32(lbEdit.CommandArgument);
                    hfQueryId.Value = GvCustomerQuery.DataKeys[i].Value.ToString();
                    string QueryTypeID = GvCustomerQuery.Rows[i].Cells[1].Text;
                    taContent.Value = GvCustomerQuery.Rows[i].Cells[3].Text;
                    bool IsSolved = Convert.ToBoolean(GvCustomerQuery.Rows[i].Cells[4].Text);
                    if (IsSolved)
                    {
                        rbIsSolvedYes.Checked = true;
                        rbIsSolvedNo.Checked = false;
                    }
                    else
                    {
                        rbIsSolvedNo.Checked = true;
                        rbIsSolvedYes.Checked = false;
                    }
                    ddlQueryType.SelectedValue = QueryTypeID;
                }
            }

            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        protected void GvCustomerQuery_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int queryId = 0;
            if (int.TryParse(GvCustomerQuery.DataKeys[e.RowIndex].Value.ToString(), out queryId))
            {
                DeleteCustomerQuery(queryId);
                BindGridData();
                ClearForm();
            }
        }

        protected void GvCustomerQuery_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GvCustomerQuery.PageIndex = e.NewPageIndex;
            BindGridData();
        }

        #endregion

        # region "Private Method"

        /// <summary>
        ///  To bind the Customer Query grid
        /// </summary>
        private void BindGridData()
        {
            try
            {
                _customerQuerys = new List<CustomerQuery>();

                _customerQuerys = _customerQueryService.GetCustomerQuerys(_orderLineId);
                if (_customerQuerys.Any())
                {
                    GvCustomerQuery.DataSource = _customerQuerys;
                    GvCustomerQuery.DataBind();

                }
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        /// <summary>
        ///  To bind the QueryType DropDown
        /// </summary>
        private void BindQueryType()
        {
            DataTable customerQueryTypes = null;
            customerQueryTypes = _customerQueryService.GetQueryTypes();
            ddlQueryType.DataSource = customerQueryTypes;
            ddlQueryType.DataValueField = "QueryTypeId";
            ddlQueryType.DataTextField = "QueryType";
            ddlQueryType.DataBind();
        }

        /// <summary>
        ///  Save the CustomerQuery
        /// </summary>
        private void SaveCustomerQuery(CustomerQuery customerQuery)
        {
            ValidationResponse saveFlag = _customerQueryService.SaveCustomerQuery(customerQuery);
            ValidationResponse = saveFlag;
        }

        /// <summary>
        ///  After save or new clear the control
        /// </summary>
        private void ClearForm()
        {
            hfQueryId.Value = null;
            taContent.Value = string.Empty;
            ddlQueryType.SelectedIndex = 0;
            rbIsSolvedNo.Checked = true;
            rbIsSolvedYes.Checked = false;
        }

        /// <summary>
        ///  Delete the CustomerQuery
        /// </summary>
        private void DeleteCustomerQuery(int queryId)
        {
            ValidationResponse deleteFlag = _customerQueryService.DeleteCustomerQuery(queryId);
            ValidationResponse = deleteFlag;
        }

        /// <summary>
        ///  Export Grid To Pdf
        /// </summary>
        private void ExportGridToPdf()
        {
            try
            {
                Response.Clear();
                Response.Buffer = true;
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Charset = string.Empty;
                string FileName = CreateFileName();

                using (StringWriter strwritter = new StringWriter())
                {
                    HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName + ".pdf");

                    //Temporary Grid
                    GridView tempGrid = GvCustomerQuery;
                    tempGrid.AllowPaging = false;
                    tempGrid.CssClass = "table table-striped table-bordered";
                    tempGrid.AutoGenerateColumns = false;

                    //Hide edit button and hidden column
                    tempGrid.Columns[0].Visible = false;
                    tempGrid.Columns[1].Visible = false;
                    tempGrid.Columns[7].Visible = false;
                    tempGrid.Columns[8].Visible = false;
                    tempGrid.DataSource = _customerQueryService.GetCustomerQuerys(_orderLineId);
                    tempGrid.DataBind();

                    tempGrid.GridLines = GridLines.Both;
                    tempGrid.HeaderStyle.Font.Bold = true;
                    tempGrid.RenderControl(htmltextwrtter);

                    StringReader sr = new StringReader(strwritter.ToString());
                    Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
                    HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                    PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                    pdfDoc.Open();
                    htmlparser.Parse(sr);
                    pdfDoc.Close();
                    Response.Write(pdfDoc);
                    Response.End();

                }
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }

        }


        private void BindGridViewHeaders(GridView gridView)
        {
            //string ID = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "ID Column Text", "ID");
            //string queryTypeId = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "QueryTypeID Column Text", "QueryTypeID");
            string QueryType = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "QueryType Column Text", "Query Type");
            string Content = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Content Column Text", "Content");
            string CreatedBy = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "CreatedBy Column Text", "Created By");
            string CreatedDate = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "CreatedDate Column Text", "Created Date");
            string Resolved = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Resolved Column Text", "Resolved");
            //BindBoundField(ID, "ID", gridView);
            //BindBoundField(queryTypeId, "QueryTypeID", gridView);
            BindBoundField(QueryType, "QueryType", gridView);
            BindBoundField(Content, "Content", gridView);
            BindBoundField(CreatedBy, "CreatedBy", gridView);
            BindBoundField(CreatedDate, "CreatedDate", gridView);
        }


        private void BindBoundField(string headerText, string dataField, GridView grid)
        {
            BoundField boundField = new BoundField();
            boundField.HeaderText = headerText;
            boundField.DataField = dataField;
            grid.Columns.Add(boundField);
        }


        /// <summary>
        ///  Export Grid To Excel
        /// </summary>
        private void ExportGridToExcel()
        {
            try
            {
                Response.Clear();
                Response.Buffer = true;
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Charset = string.Empty;
                string FileName = CreateFileName();

                using (StringWriter strwritter = new StringWriter())
                {
                    HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName + ".xls");

                    //Temporary Grid
                    GridView tempGrid = GvCustomerQuery;
                    tempGrid.AllowPaging = false;
                    tempGrid.CssClass = "table table-striped table-bordered";
                    tempGrid.AutoGenerateColumns = false;

                    //Hide select button column
                    tempGrid.Columns[0].Visible = false;
                    tempGrid.Columns[1].Visible = false;
                    tempGrid.Columns[7].Visible = false;
                    tempGrid.Columns[8].Visible = false;
                    tempGrid.DataSource = _customerQueryService.GetCustomerQuerys(_orderLineId);
                    tempGrid.DataBind();

                    tempGrid.GridLines = GridLines.Both;
                    tempGrid.HeaderStyle.Font.Bold = true;
                    tempGrid.RenderControl(htmltextwrtter);

                    Response.Write(strwritter.ToString());
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }

        }

        /// <summary>
        ///  Export Grid To Word Document
        /// </summary>
        private void ExportToDocument()
        {
            try
            {
                Response.Clear();
                Response.Buffer = true;
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Charset = string.Empty;
                string FileName = CreateFileName();

                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.ContentType = "application/vnd.word";
                Response.AddHeader("content-disposition", "attachment;filename=" + FileName + ".doc");

                StringWriter stringWrite = new StringWriter();
                HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                GridView tempGrid = GvCustomerQuery;

                tempGrid.AllowPaging = false;
                tempGrid.CssClass = "table table-striped table-bordered";
                tempGrid.AutoGenerateColumns = false;
                //Hide select button column
                tempGrid.Columns[0].Visible = false;
                tempGrid.Columns[1].Visible = false;
                tempGrid.Columns[7].Visible = false;
                tempGrid.Columns[8].Visible = false;
                tempGrid.DataSource = _customerQueryService.GetCustomerQuerys(_orderLineId);
                tempGrid.DataBind();
                tempGrid.RenderControl(htmlWrite);
                Response.Write(stringWrite.ToString());
                Response.Flush();
                Response.End();
                tempGrid.Dispose();
            }

            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Create File Name
        /// </summary>
        /// <returns>file name</returns>
        private string CreateFileName()
        {
            return "Customer Query" + DateTime.Now.ToString();
        }

        /// <summary>
        /// Check the credit card no in the text
        /// </summary>
        private bool HasCreditCardNumber(string inputCardNumber)
        {
            bool result = false;

            var input = inputCardNumber.Replace("-", string.Empty).Replace(" ", string.Empty);
            Regex regex = new Regex(@"\d{16}");
            Match match = regex.Match(input);
            if (match.Success)
            {
                divErrorMsg.Visible = true;
                return true;
            }
            divErrorMsg.Visible = false;
            return result;
        }
        #endregion

        #region BindErrorMessages
        private void BindSitecoreTexts()
        {
            Item conditionalMessageItem = this.GetItemFromRenderingParameterPath("Conditional Messages Link");
            string requiredFieldMessageFieldName = "Not Enter Card Number Message";
            string DonotenterCardFieldName = "Required Field Message";
           // SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, requiredFieldMessageFieldName, txtContentRequiredText);
            //SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, DonotenterCardFieldName, txtDonotenterText);
        }
        #endregion
    }
}