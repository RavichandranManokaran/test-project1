﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.Common.Constants
{
    public static class BookingOrderConstant
    {
        public const string CommissionAmount = "_LBCommissionFee";
        public const string CalculatedProcessingFee = "CalculatedProcessingFee";
        public const string ProcessingFee = "ProcessingFee";
        public const string ProviderAmount = "_ProviderAmount";

        public const string CommissionAmountDiscount = "_LBCommissionFeeDiscount";
        public const string CalculatedProcessingFeeDiscount = "CalculatedProcessingFeeDiscount";
        public const string ProcessingFeeDiscount = "ProcessingFeeDiscount";
        public const string ProviderAmountDiscount = "_ProviderAmountDiscount";

    }
}