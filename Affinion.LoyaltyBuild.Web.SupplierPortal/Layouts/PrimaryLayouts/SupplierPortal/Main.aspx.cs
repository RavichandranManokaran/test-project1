using System;
using System.Web;
using System.Web.UI;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration.SiteUI.Base;
using System.Text;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;
using Sitecore.Data.Fields;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.PrimaryLayouts.SupplierPortal
{
    public partial class Main : SitecorePageLayoutBase
    {
        private void Page_Load(object sender, System.EventArgs e)
        {
            //if (!Sitecore.Context.PageMode.IsNormal) Page.EnableViewState = false;

            // This page is setting a lot fo the presentation details.  This is due tot he flexible nature of this site.
            Item PresentationSettings = SiteConfiguration.GetPresentationSettingsItem();
            var LoggedInSuppliersCheck = SitecoreUserProfile.GetItem(UserProfileKeys.loggedInUserName);

            if (Sitecore.Context.IsAdministrator && Sitecore.Context.IsLoggedIn)
            {
            }
            else if(LoggedInSuppliersCheck == null || LoggedInSuppliersCheck == "")
            {
                Response.Redirect(Constants.redirectToLogOn);
            }

            // Set the page logo
            if (PresentationSettings != null)
            {
                if (PresentationSettings[Constants.LogoLocation] == Constants.Header)
                {
                    Logo.Item = PresentationSettings;
                    // set the page layout
                    out_container.Attributes.Add("class", PresentationSettings[Constants.LayoutStyle].ToLower().Replace(" ", "-"));
                    // Show/Hide the top line
                    if (PresentationSettings[Constants.ShowTopLine] != "1")
                        topline.Attributes.Add("class", "top_line_plain");
                    // set the background image / color
                    if (PresentationSettings[Constants.BackgroundImage] != string.Empty)
                    {
                        ImageField imgField = ((Sitecore.Data.Fields.ImageField)PresentationSettings.Fields[Constants.BackgroundImage]);
                        mainbody.Style.Add(Constants.backgroundimage, MediaManager.GetMediaUrl(imgField.MediaItem));

                        if (imgField.MediaItem.Parent.Key == Constants.patterns) mainbody.Attributes.Add("class", Constants.backgroundpattern);
                        else mainbody.Attributes.Add("class", Constants.backgroundcover);
                    }
                    else if (PresentationSettings[Constants.BackgroundImage] != string.Empty)
                    {
                        mainbody.Style.Add(Constants.backgroundcolor, PresentationSettings[Constants.BackgroundColor]);
                    }
                }
            }
            else
            {
                Logo.Visible = false;
                logospan.Visible = false;
                //tertiarynavspan.Attributes.Add("class", "span12");            
            }


            // Copyright.Item = SiteConfiguration.GetSiteSettingsItem();
        }
    }
}
