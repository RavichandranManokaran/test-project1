﻿using Affinion.LoyaltyBuild.Model.Provider;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Api.Search.Data
{
    
    /// <summary>
    ///  Provider Service Interface to get provider data
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IProviderCollection<T> : IList<T>
    {
        /// <summary>
        ///  Gets Collection type
        /// </summary>
        ProviderType CollectionType { get; }
    }
}
