﻿//<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           OfflinePortalSearchBooking.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal
/// Description:           Used to Bind data to OfflinePortalSearchBooking subLayout
/// </summary>

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    #region Using Directives
    using Affinion.Loyaltybuild.BusinessLogic;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.DataAccess.Models;
    using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
    using Affinion.LoyaltyBuild.Search.Helpers;
    using Sitecore.Data.Items;
    using Sitecore.Text;
    using Sitecore.Web;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Configuration;
    using System.Web.UI.WebControls;
    using Affinion.LoyaltyBuild.Web.ClientPortal.Helper;
    using System.Globalization;
    using Affinion.LoyaltyBuild.Common.CardValidationCheck;

    #endregion

    public partial class OfflinePortalSearchBooking : System.Web.UI.UserControl
    {
        #region Private
        private const string ClientItemPath = "/sitecore/content/admin-portal/client-setup/{0}";
        #endregion

        #region Protected
        protected string providerSearchItemId = string.Empty;
        #endregion

        #region LWD-926
        private static string _sortDirection = "DESC";
        private static string _sortExpresssion = "CreatedOn";
        private DataView dvBookingDetails = null;
        #endregion

        public string userName;

        public bool IsListOut = false;
        int checkListOut = 0;

        #region LWD-1010
        SearchBooking searchData = null;
        #endregion
        #region Page Load
        private void Page_Load(object sender, EventArgs e)
        {
            Page.MaintainScrollPositionOnPostBack = true;

            userName = !string.IsNullOrWhiteSpace(SitecoreUserProfileHelper.GetItem(UserProfileKeys.CreatedBy)) ? SitecoreUserProfileHelper.GetItem(UserProfileKeys.CreatedBy) : string.Empty;

            if (!IsPostBack)
            {
                InitPage();
                #region LWD-1010
                if (Request.QueryString["_e"] != null && Request.QueryString["_e"].ToString() == "1")
                {
                    RetainSearchData();
                }
                else
                {
                    Session["SearchBookingData"] = null;
                }
                #endregion
            }

            //Search Provider item link
            providerSearchItemId = HiddenFieldProviderSearchItemId.Value;

            //Expand grid view if rows not empty
            if (GridViewBookingResults.Rows.Count > 0)
            {
                //Link button 
                LinkButtonEnable(true);
                divResult.Visible = true;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "CallExpanResultGrid", "ExpanResultGrid()", true);
            }
            else
            {
                //Link button 
                LinkButtonEnable(false);
                divResult.Visible = false;
            }

        }
        #endregion

        #region Protected Methods
        protected void LinkButtonTransferBooking_Click(object sender, EventArgs e)
        {
            AnchorTagHelperData TransferABookingLink = SitecoreFieldsHelper.GetUrl(Sitecore.Context.Item, "TransferABooking");
            if (TransferABookingLink != null)
            {
                //Add Query string parameters
                Dictionary<string, string> querystringParam = GetQueryStringParams();
                if (querystringParam != null)
                {
                    PageRedirect(TransferABookingLink.Url, querystringParam);
                }
            }
        }

        protected void LinkButtonCancelBooking_Click(object sender, EventArgs e)
        {
            AnchorTagHelperData CancelBookingLink = SitecoreFieldsHelper.GetUrl(Sitecore.Context.Item, "CancelBooking");
            if (CancelBookingLink != null)
            {
                //Add Query string parameters
                Dictionary<string, string> querystringParam = GetQueryStringParams();
                if (querystringParam != null)
                {
                    PageRedirect(CancelBookingLink.Url, querystringParam);
                }
            }
        }

        protected void LinkButtonViewTransferHistory_Click(object sender, EventArgs e)
        {
            AnchorTagHelperData TransferHistoryLink = SitecoreFieldsHelper.GetUrl(Sitecore.Context.Item, "TransferHistory");
            if (TransferHistoryLink != null)
            {
                //Add Query string parameters
                Dictionary<string, string> querystringParam = GetQueryStringParams();
                if (querystringParam != null)
                {
                    PageRedirect(TransferHistoryLink.Url, querystringParam);
                }

            }
        }

        protected void LinkButtonPaymentHistory_Click(object sender, EventArgs e)
        {
            AnchorTagHelperData PaymentHistoryLink = SitecoreFieldsHelper.GetUrl(Sitecore.Context.Item, "PaymentHistory");
            if (PaymentHistoryLink != null)
            {
                //Add Query string parameters
                Dictionary<string, string> querystringParam = GetQueryStringParams();
                if (querystringParam != null)
                {
                    PageRedirect(PaymentHistoryLink.Url, querystringParam);
                }
            }

        }

        protected void LinkButtonConvertExcel_Click(object sender, EventArgs e)
        {
            ExportGridToExcel();

        }

        protected void LinkButtonComms_Click(object sender, EventArgs e)
        {
            AnchorTagHelperData CommsLink = SitecoreFieldsHelper.GetUrl(Sitecore.Context.Item, "Comms");
            if (CommsLink != null)
            {
                //Add Query string parameters
                Dictionary<string, string> querystringParam = GetQueryStringParams();
                if (querystringParam != null)
                {
                    PageRedirect(CommsLink.Url, querystringParam);
                }

            }
        }


        protected void LinkButtonBookingDetail_Click(object sender, EventArgs e)
        {
            AnchorTagHelperData BookingDetailLink = SitecoreFieldsHelper.GetUrlOfItem(Sitecore.Context.Item);
            LinkButtonBookingDetail.PostBackUrl = "/BookingDetails";
            if (BookingDetailLink != null)
            {
                //Add Query string parameters
                Dictionary<string, string> querystringParam = GetQueryStringParams();
                if (querystringParam != null)
                {
                    PageRedirect(LinkButtonBookingDetail.PostBackUrl, querystringParam);
                }

            }

        }


        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            #region LWD-926
            _sortDirection = "DESC";
            _sortExpresssion = "CreatedOn";
            #endregion
            divTotalRecords.Visible = true;
            userName = string.Empty;
            IsListOut = false;
            // Get Booking Details 
            DataSet result = this.GetBookingDetails(CreateSearchBookingObj(IsListOut), userName);

            //Bind grid
            GridDataBinding(result);

            int resutlCount = result != null ? result.Tables[0].Rows.Count : 0;
            lblTotalRecords.Text = result.Tables[0].Rows.Count.ToString();
            //Message Handling
            GridResultsMessageHandling(resutlCount);
        }

        protected void ButtonListOut_Click(object sender, EventArgs e)
        {
            #region LWD-926
            _sortDirection = "DESC";
            _sortExpresssion = "CreatedOn";
            #endregion
            ResetForm();
            divTotalRecords.Visible = true;
            //Pagination trick
            ClientList.SelectedValue = DropDownListClient.SelectedValue;
            ReservationDate.Text = DateTime.Now.Date.ToShortDateString();
            checkListOut = 1;
            // Get Booking Details 
            DataSet result = this.GetBookingDetails(CreateSearchBookingObj(true), userName);

            //Bind grid
            GridDataBinding(result);

            //Result count
            int resutlCount = result != null ? result.Tables[0].Rows.Count : 0;
            lblTotalRecords.Text = result.Tables[0].Rows.Count.ToString();
            //Message Handling
            GridResultsMessageHandling(resutlCount);
        }

        protected void GridViewBookingResults_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                // Get the currently selected row using the SelectedRow property.
                GridViewRow row = GridViewBookingResults.SelectedRow;

                int orderId = 0;
                int orderLineId = 0;
                var rowCells = row.Cells;

                if (rowCells != null)
                {
                    if (int.TryParse(rowCells[0].Text, out orderLineId))
                    {
                        this.HiddenFieldOrderLineId.Value = rowCells[0].Text;
                    }
                    if (int.TryParse(rowCells[1].Text, out orderId))
                    {
                        this.HiddenFieldOrderId.Value = rowCells[1].Text;
                    }

                    this.HiddenFieldOrderGuid.Value = rowCells[2].Text;

                    //Link button 
                    //LinkButtonEnable(true);                   
                    string url = "/BookingDetails";
                    //Add Query string parameters
                    Dictionary<string, string> querystringParam = GetQueryStringParams();
                    if (querystringParam != null)
                    {
                        PageRedirect(url, querystringParam);
                    }


                }

            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }

        }

        protected void ButtonReset_Click(object sender, EventArgs e)
        {
            #region LWD-926
            _sortDirection = "DESC";
            _sortExpresssion = "CreatedOn";
            #endregion
            ResetForm();
            errormessage.Visible = false;
            successmessage.Visible = false;
            divTotalRecords.Visible = false;
        }


        protected void GridViewBookingResults_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            GridViewBookingResults.PageIndex = e.NewPageIndex;
            DataSet result;
            // Get Booking Details 
            if (checkListOut == 1)
            {
                result = this.GetBookingDetails(CreateSearchBookingObj(true), userName);
            }
            else
            {
                result = this.GetBookingDetails(CreateSearchBookingObj(), string.Empty);
            }

            //Bind grid
            GridDataBinding(result);
            //ButtonSubmit_Click(null, null);
            #region LWD-1010
            if (Session["SearchBookingData"] != null)
            {
                searchData = Session["SearchBookingData"] as SearchBooking;
                searchData.PageIndex = e.NewPageIndex;
            }
            #endregion
        }
        #endregion

        #region Private Methods
        private void InitPage()
        {
            try
            {
                LoadClientDropDown(ClientList);
                LoadClientDropDown(DropDownListClient);
                LoadBookingMethodDropDown();
                LoadBookingStatusDropDown();

                BindGridViewHeaders(GridViewBookingResults);

                this.ButtonSubmit.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Submit", "Submit");
                this.ButtonReset.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Reset", "Reset");
                this.ButtonListOut.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "ListOut", "List Out");

                var providerSearchItemPath = SitecoreFieldsHelper.GetUrl(Sitecore.Context.Item, "ProviderSearch");
                if (providerSearchItemPath != null)
                {
                    Item providerSearchItem = Sitecore.Context.Database.GetItem(providerSearchItemPath.Url);
                    HiddenFieldProviderSearchItemId.Value = providerSearchItem != null ? providerSearchItem.ID.ToString() : string.Empty;
                }

                this.GridViewBookingResults.PageSize = SitecoreFieldsHelper.GetInteger(Sitecore.Context.Item, "GridPageSize", 10);

            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }

        }

        /// <summary>
        /// Bind Table Header
        /// </summary>
        private void BindGridViewHeaders(GridView gridView)
        {
            string bookingRef = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "ThBookingReference", "Booking Ref");
            string campaign = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "ThCampaign", "Campaign");
            string fullName = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "ThFullName", "Full Name");
            string checkInCheckOutDates = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "ThCheckInCheckOutDates", "Arrival Date / Departure Date");
            string noOfAdult = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "ThNoOfAdult", "No Of Adults");
            string noOfChild = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "ThNoOfChild", "No Of Children");
            string createdOn = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "ThCreatedOn", "Reservation Date");
            string bookingThrough = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "ThBookingThrough", "Booking Method");
            string bookingStatus = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "ThBookingStatus", "Booking Status");
            string accommodation = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "ThAccommodation", "Accommodation");

            BindBoundField(bookingRef, "BookingReferenceId", gridView);
            BindBoundField(campaign, "Campaign", gridView);
            BindBoundField(fullName, "FullName", gridView);
            BindBoundField(checkInCheckOutDates, "CheckInCheckOutDates", gridView);
            BindBoundField(noOfAdult, "NoOfAdult", gridView);
            BindBoundField(noOfChild, "NoOfChild", gridView);
            BindBoundField(createdOn, "CreatedOn", gridView);
            BindBoundField(bookingThrough, "BookingThrough", gridView);
            BindBoundField(bookingStatus, "BookingStatus", gridView);
            BindBoundField(accommodation, "Accommodation", gridView);

        }

        /// <summary>
        /// Bind Bound Field to grid
        /// </summary>
        /// <param name="headerText">Header Text</param>
        /// <param name="dataField"><Data Field/param>
        /// <param name="grid">Grid</param>
        private void BindBoundField(string headerText, string dataField, GridView grid)
        {
            BoundField boundField = new BoundField();
            boundField.HeaderText = headerText;
            boundField.DataField = dataField;
            #region LWD-926
            boundField.SortExpression = dataField;
            #endregion
            grid.Columns.Add(boundField);
        }



        /// <summary>
        /// Get Query String Params
        /// </summary>
        /// <returns>Query String Params</returns>
        private Dictionary<string, string> GetQueryStringParams()
        {
            try
            {

                string orderGuid = HiddenFieldOrderGuid.Value;
                string orderId = HiddenFieldOrderId.Value;
                string orderLine = HiddenFieldOrderLineId.Value;

                if (!string.IsNullOrEmpty(orderGuid) && !string.IsNullOrEmpty(orderId) && !string.IsNullOrEmpty(orderLine))
                {
                    //Add Query string parameters
                    Dictionary<string, string> querystringParam = new Dictionary<string, string>();
                    querystringParam.Add("OGID", orderGuid);
                    querystringParam.Add("OID", orderId);
                    querystringParam.Add("OLID", orderLine);

                    return querystringParam;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }

        }

        /// <summary>
        /// Grid Results Message Handling
        /// </summary>
        /// <param name="resultCount">resultCount</param>
        private void GridResultsMessageHandling(int resultCount)
        {
            try
            {
                if (resultCount > 0)
                {
                    divResult.Visible = true;
                    errormessage.Visible = false;

                    //Focus Search Result Grid
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "CallExpanResultGrid", "ExpanResultGrid()", true);

                    //Link button 
                    LinkButtonEnable(true);
                }
                else
                {
                    literalErrorMessage.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "NoResultsFound", "No Results Found");
                    errormessage.Visible = true;
                    divResult.Visible = false;

                    //Link button 
                    LinkButtonEnable(false);
                }
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }

        }

        /// <summary>
        /// Create Search Booking Obj
        /// </summary>
        /// <returns>Search Booking Obj</returns>
        private SearchBooking CreateSearchBookingObj(bool listOut = false)
        {
            try
            {
                searchData = new SearchBooking();
                string clientId = ClientList.SelectedValue == string.Empty ? GetClientIdListString() : ClientList.SelectedValue;
                string cliemtListOutId = DropDownListClient.SelectedValue == string.Empty ? GetClientIdListString() : DropDownListClient.SelectedValue;

                if (clientId!=string.Empty && clientId != "{0}")
                searchData.Client = listOut ? cliemtListOutId : clientId;
                searchData.FirstName = InputStringFormatting(TextBoxFirstName.Text);
                searchData.LastName = InputStringFormatting(TextBoxLastName.Text);
                searchData.Provider = InputStringFormatting(TextBoxProvider.Text);

                #region LWD-1010
                if (RadioRange.Checked)
                    searchData.SearchDateBy = 1;
                else if (RadioSpecific.Checked)
                {
                    searchData.SearchDateBy = 2;
                }
                searchData.BookingReferenceMethod = DropDownBookingReference.SelectedValue;
                searchData.IsListOut = listOut;
                Session["SearchBookingData"] = searchData;
                #endregion


                GetCreateSearchBookingObj(listOut, searchData);

                return searchData;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, null);
                throw;
            }

        }

        private void GetCreateSearchBookingObj(bool listOut, SearchBooking searchData)
        {
            //Date base on Range
            if (this.RadioRange.Checked)
            {
                searchData.ReservationFrom = InputDateFormatting(ReservationDateFrom.Text);
                searchData.ReservationTo = InputDateFormatting(ReservationDateTo.Text);

                #region LWD-1010
                searchData.StrReservationFrom = ReservationDateFrom.Text;
                searchData.StrReservationTo = ReservationDateTo.Text;
                #endregion
            }

            //Date base on Specific
            if (this.RadioSpecific.Checked)
            {
                searchData.ReservationDate = InputDateFormatting(ReservationDate.Text);
                searchData.ArrivalDate = InputDateFormatting(ArrivalDate.Text);

                #region LWD-1010
                searchData.StrReservationDate = ReservationDate.Text;
                searchData.StrArrivalDate = ArrivalDate.Text;
                #endregion
            }
            if (DropDownBookingReference.SelectedIndex == 0)
            {
                searchData.BookingReference = InputStringFormatting(CredictCardCheck.CredictCard(TextBoxBookingReference.Text));
            }
            else if (DropDownBookingReference.SelectedIndex == 1)
            {
                searchData.EmailValidation = InputStringFormatting(CredictCardCheck.CredictCard(TextBoxBookingReference.Text));
            }
            else
            {
                searchData.NumericValidation = InputStringFormatting(CredictCardCheck.CredictCard(TextBoxBookingReference.Text));
            }

            searchData.BookingMethod = listOut ? string.Empty : DropDownBookingMethod.SelectedValue;

            //Set ArrivalDate to DateTime now if list-out
            if (listOut)
            {
                searchData.ReservationDate = DateTime.Now;
            }

            int bookingStatus;

            //BookingMethod bookingType = (BookingMethod)Enum.Parse(typeof(BookingMethod), DropDownBookingMethod.SelectedValue);

            if (int.TryParse(DropDownBookingStatus.SelectedValue, out bookingStatus))
            {
                searchData.BookingStatus = bookingStatus;
            }
        }

        /// <summary>
        ///  Grid Data Binding
        /// </summary>
        /// <param name="dataSet">Data set</param>
        private void GridDataBinding(DataSet dataSet)
        {
            try
            {
                //OrderLineIdcolumn 
                ShowIdGridColumns(true);

                GridViewBookingResults.AutoGenerateColumns = false;
                #region LWD-926
                dvBookingDetails = null;
                if (dataSet != null && dataSet.Tables != null && dataSet.Tables.Count > 0)
                {
                    dvBookingDetails = new DataView(dataSet.Tables[0]);
                    dvBookingDetails.Sort = _sortExpresssion + " " + _sortDirection;
                    GridViewBookingResults.DataSource = dvBookingDetails;
                    GridViewBookingResults.DataBind();
                }
                else
                {
                    GridViewBookingResults.DataSource = dataSet;
                    GridViewBookingResults.DataBind();
                }


                #endregion LWD-926
                GridViewBookingResults.SelectedIndex = -1;
                ////hide columns 
                ShowIdGridColumns(false);
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, null);
                throw;
            }

        }

        /// <summary>
        /// Set Id Grid Columns visibility
        /// </summary>
        /// <param name="visible">visible</param>
        private void ShowIdGridColumns(bool visible)
        {
            var gridColumns = GridViewBookingResults.Columns;
            if (gridColumns.Count > 0)
            {
                //OrderLineIdcolumn 
                gridColumns[0].Visible = visible;
                //OrderId
                gridColumns[1].Visible = visible;
                //OrderGuid
                gridColumns[2].Visible = visible;
            }

        }

        /// <summary>
        /// Reset Form
        /// </summary>
        private void ResetForm()
        {
            LoadClientDropDown(this.ClientList);
            LoadBookingMethodDropDown();
            LoadBookingStatusDropDown();
            TextBoxFirstName.Text = string.Empty;
            TextBoxLastName.Text = string.Empty;
            TextBoxProvider.Text = string.Empty;
            ReservationDate.Text = string.Empty;
            ReservationDateFrom.Text = string.Empty;
            ReservationDateTo.Text = string.Empty;
            ArrivalDate.Text = string.Empty;
            TextBoxBookingReference.Text = string.Empty;

            HiddenFieldOrderGuid.Value = string.Empty;
            HiddenFieldOrderId.Value = string.Empty;
            HiddenFieldOrderLineId.Value = string.Empty;

            divResult.Visible = false;

            LinkButtonEnable(false);
            #region LWD-1010
            Session["SearchBookingData"] = null;
            #endregion

        }

        /// <summary>
        /// Input String Formating
        /// </summary>
        /// <param name="input">string</param>
        /// <returns>string</returns>
        private string InputStringFormatting(string input)
        {
            return string.IsNullOrEmpty(input) ? string.Empty : input.Trim();

        }

        /// <summary>
        /// Input Date Formatting
        /// </summary>
        /// <param name="date">Date</param>
        /// <returns>DateTime</returns>
        private DateTime InputDateFormatting(string date)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(date))
                {
                    return default(DateTime);
                }
                else
                {
                    DateTime tmpDateTime;
                    if (DateTime.TryParseExact(date,
                                       "yyyy-dd-MM",
                                       CultureInfo.InvariantCulture,
                                       DateTimeStyles.None,
                                       out tmpDateTime))
                    {
                        return tmpDateTime;
                    }
                    else
                    {
                        string[] words = date.Split('/');
                        string newstring = words[1] + "/" + words[0] + "/" + words[2];
                        DateTime dt = Convert.ToDateTime(newstring);
                        return dt;
                    }
                }
            }
            catch
            {
                return default(DateTime);
            }


            //return string.IsNullOrEmpty(date) ? default(DateTime) : Convert.ToDateTime(date);


            //if (String.IsNullOrWhiteSpace(date))
            //{
            //    return default(DateTime);
            //}
            //else
            //{
            //    try
            //    {
            //        string[] words = date.Split('/');
            //        string newstring = words[1] + "/" + words[0] + "/" + words[2];
            //        DateTime dt = Convert.ToDateTime(newstring);
            //        return dt;
            //    }
            //    catch(Exception e)
            //    {
            //        DateTime dt = Convert.ToDateTime(date);
            //        return dt;
            //    }
            //}     
        }

        /// <summary>
        /// Load Client DropDown
        /// </summary>
        private void LoadClientDropDown(DropDownList dropDownList)
        {
            try
            {
                var clientItems = SearchHelper.GetUserSpecificClientItems();

                if (clientItems != null)
                {
                    /// Extract Clients Filtered Items
                    var clientFilterList = (from client in clientItems
                                            orderby client.DisplayName
                                            select new { DiplayName = client.DisplayName, Id = client.ID.Guid });

                    /// Load Client List Drop down
                    dropDownList.DataSource = clientFilterList != null ? clientFilterList.ToList() : null;

                    /// Set data text and data value fields
                    dropDownList.DataTextField = "DiplayName";
                    dropDownList.DataValueField = "Id";


                    dropDownList.DataBind();
                }

                ListItem allClients = new ListItem("All Clients", string.Empty);
                dropDownList.Items.Insert(0, allClients);

                /// Assign respective client images to dropdown
                foreach (ListItem clientListItem in dropDownList.Items)
                {
                    if (!clientListItem.Text.Equals(allClients.Text))
                    {
                        Item clientItem = Sitecore.Context.Database.GetItem(string.Format(ClientItemPath, clientListItem.Value));
                        clientListItem.Attributes.Add("data-image", GetImageURL(clientItem, "Customer Service Image"));
                    }
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, null);
                throw;
            }
        }


        /// <summary>
        /// Method used to Get Image URL of Sitecore Image field
        /// </summary>
        /// <param name="currentItem"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        private string GetImageURL(Item currentItem, string fieldName)
        {
            string imageURL = string.Empty;
            Sitecore.Data.Fields.ImageField imageField = currentItem.Fields[fieldName];
            if (imageField != null && imageField.MediaItem != null)
            {
                Sitecore.Data.Items.MediaItem image = new Sitecore.Data.Items.MediaItem(imageField.MediaItem);
                imageURL = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image));
            }
            return imageURL;
        }

        /// <summary>
        /// Load Booking Method DropDown
        /// </summary>
        private void LoadBookingMethodDropDown()
        {
            try
            {
                DropDownBookingMethod.DataSource = Enum.GetValues(typeof(BookingMethod));
                DropDownBookingMethod.DataBind();

                ListItem allClients = new ListItem("Please Select", string.Empty);
                DropDownBookingMethod.Items.Insert(0, allClients);

            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        /// <summary>
        ///  Load Booking Status Drop Down
        /// </summary>
        private void LoadBookingStatusDropDown()
        {
            try
            {
                //Bind locationList data to the dropdown
                DropDownBookingStatus.DataSource = uCommerceConnectionFactory.GetBookingStatus();
                DropDownBookingStatus.DataTextField = "Name";
                DropDownBookingStatus.DataValueField = "OrderStatusId";
                DropDownBookingStatus.DataBind();
                ListItem selectLocation = new ListItem("Please Select", string.Empty);
                DropDownBookingStatus.Items.Insert(0, selectLocation);
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Convert string list to comma separated string
        /// Prepare string list for SQL IN query
        /// </summary>
        /// <returns>comma separated string id list</returns>
        private string GetClientIdListString()
        {
            var clientList = GetClientList();
            string clientIdList = "{0}";//Dummy

            if (clientList != null)
            {
                //Convert string list to comma separated string as "{6C464DB8},{6451DB8}"
                clientIdList = string.Join(",", clientList);
            }

            return clientIdList;
        }

        /// <summary>
        /// Get Client List
        /// </summary>
        /// <returns>Client List</returns>
        private IList<string> GetClientList()
        {
            try
            {
                /// Get Client items
                //Sitecore.Data.Database currentDb = Sitecore.Context.Database;
                var clientItems = SearchHelper.GetUserSpecificClientItems(); //currentDb.SelectItems(ClientListQuery);

                if (clientItems != null)
                {
                    /// Extract Clients Filtered Items
                    var clientIds = (from client in clientItems
                                     orderby client.DisplayName
                                     select client.ID.Guid.ToString());
                    return clientIds != null ? clientIds.ToList() : null;
                }

                return null;
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Link Button Enable
        /// </summary>
        /// <param name="enable"></param>
        /// <returns></returns>
        private void LinkButtonEnable(bool enable)
        {
            divLinks.Visible = enable;
            LinkButtonConvertExcel.Visible = enable;

            //Hide links if the row is not selected
            if (string.IsNullOrEmpty(HiddenFieldOrderGuid.Value) || string.IsNullOrEmpty(HiddenFieldOrderId.Value) ||
                string.IsNullOrEmpty(HiddenFieldOrderLineId.Value))
            {
                divLinks.Visible = false;
            }

        }

        /// <summary>
        /// Get Booking Details
        /// </summary>
        /// <returns></returns>
        private DataSet GetBookingDetails(SearchBooking data, string tempUsername)
        {
            try
            {

                return data != null ? uCommerceConnectionFactory.GetBookingDetailes(data, tempUsername) : null;

            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Page Redirect
        /// </summary>
        /// <param name="link">Redirect link</param>
        /// <param name="queryStringParam">Query string parameter</param>
        /// <param name="encrypted">Is parameter encrypted</param>
        private void PageRedirect(string link, Dictionary<string, string> queryStringParam, bool encrypted = true)
        {
            try
            {

                if (encrypted)
                {
                    string host = HttpContext.Current.Request.Url.Host;
                    UriBuilder uribuilder = new UriBuilder(host + link);
                    QueryStringHelper queryHelper = new QueryStringHelper(uribuilder.Uri);

                    foreach (var data in queryStringParam)
                    {
                        queryHelper.SetValue(data.Key, data.Value);
                    }

                    WebUtil.Redirect(queryHelper.GetUrl(true).AbsoluteUri);
                }
                else
                {
                    //set query string params
                    UrlString url = new UrlString(link);

                    foreach (var data in queryStringParam)
                    {
                        url.Append(data.Key, data.Value);
                    }

                    WebUtil.Redirect(url.GetUrl(), false);

                }
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }

        }

        /// <summary>
        ///  Export Grid To Excel
        /// </summary>
        private void ExportGridToExcel()
        {
            try
            {
                Response.Clear();
                Response.Buffer = true;
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Charset = string.Empty;
                string FileName = CreateFileName();

                //StringWriter strwritter = new StringWriter();

                using (StringWriter strwritter = new StringWriter())
                {
                    HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);

                    //Temporary Grid
                    GridView tempGrid = GridViewBookingResults;
                    tempGrid.AllowPaging = false;
                    #region LWD-926
                    tempGrid.AllowSorting = false;
                    #endregion
                    tempGrid.CssClass = "table table-striped table-bordered";

                    tempGrid.AutoGenerateColumns = false;

                    //Hide select button column
                    tempGrid.Columns[3].Visible = false;
                    #region LWD-926
                    DataSet dsBookings;
                    // Get Booking Details 
                    dsBookings = GetBookingInformation(tempGrid);
                    #endregion

                    tempGrid.GridLines = GridLines.Both;
                    tempGrid.HeaderStyle.Font.Bold = true;
                    tempGrid.RenderControl(htmltextwrtter);

                    Response.Write(strwritter.ToString());
                    Response.End();

                }


            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }

        }
        /// <summary>
        ///  Gets booking details from dataset
        /// </summary>
        private DataSet GetBookingInformation(GridView tempGrid)
        {
            DataSet dsBookings;
            if (checkListOut == 1)
            {
                dsBookings = this.GetBookingDetails(CreateSearchBookingObj(true), userName);
            }
            else
            {
                dsBookings = this.GetBookingDetails(CreateSearchBookingObj(), string.Empty);
            }
            dvBookingDetails = null;
            lblTotalRecords.Text = dsBookings.Tables.Count.ToString();
            if (dsBookings != null && dsBookings.Tables != null && dsBookings.Tables.Count > 0)
            {
                dvBookingDetails = new DataView(dsBookings.Tables[0]);
                dvBookingDetails.Sort = _sortExpresssion + " " + _sortDirection;
                tempGrid.DataSource = dvBookingDetails;
                tempGrid.DataBind();
            }
            else
            {
                tempGrid.DataSource = dsBookings;
                tempGrid.DataBind();
            }
            return dsBookings;
        }

        /// <summary>
        /// Create File Name
        /// </summary>
        /// <returns>file name</returns>
        private string CreateFileName()
        {
            return "Existing Booking" + DateTime.Now.ToString() + ".xls";
        }
        #endregion

        #region LWD-926
        protected void GridViewBookingResults_Sorting(object sender, GridViewSortEventArgs e)
        {
            SetSortDirection(_sortDirection);
            if (e != null && e.SortExpression != null)
                _sortExpresssion = e.SortExpression.ToString();
            // Get Booking Details 


            DataSet dsBookings;
            // Get Booking Details 
            if (checkListOut == 1)
            {
                dsBookings = this.GetBookingDetails(CreateSearchBookingObj(true), userName);
            }
            else
            {
                dsBookings = this.GetBookingDetails(CreateSearchBookingObj(), string.Empty);
            }
            GridDataBinding(dsBookings);

            #region LWD-1010
            if (Session["SearchBookingData"] != null)
            {
                searchData = Session["SearchBookingData"] as SearchBooking;
                searchData.SortExpresssion = _sortExpresssion;
                searchData.SortDirection = _sortDirection;
            }
            #endregion
        }

        protected void SetSortDirection(string sortDirection)
        {
            switch (sortDirection)
            {
                case "DESC":
                    _sortDirection = "ASC";
                    break;
                case "ASC":
                    _sortDirection = "DESC";
                    break;
            }
        }
        #endregion

        #region LWD-1010
        private void RetainSearchData()
        {
            if (Session["SearchBookingData"] != null)
            {
                divTotalRecords.Visible = true;
                searchData = Session["SearchBookingData"] as SearchBooking;
                if (!string.IsNullOrEmpty(searchData.SortExpresssion))
                _sortExpresssion = searchData.SortExpresssion;
                if (!string.IsNullOrEmpty(searchData.SortDirection))
                _sortDirection = searchData.SortDirection;

                TextBoxFirstName.Text = searchData.FirstName;
                TextBoxLastName.Text = searchData.LastName;
                TextBoxProvider.Text = searchData.Provider;

                if (!string.IsNullOrEmpty(searchData.BookingMethod))
                    this.DropDownBookingMethod.SelectedValue = searchData.BookingMethod;
                //search data based on date
                SearchDataByDate();

                
                if (!string.IsNullOrEmpty(searchData.BookingReferenceMethod))
                    this.DropDownBookingReference.SelectedValue = searchData.BookingReferenceMethod;

                if (!string.IsNullOrEmpty(searchData.BookingReference))
                    this.TextBoxBookingReference.Text = searchData.BookingReference;

                if (searchData.BookingStatus>0)
                    this.DropDownBookingStatus.SelectedValue = searchData.BookingStatus.ToString();



                this.GridViewBookingResults.PageIndex = searchData.PageIndex;

                DataSet dsBookings;
                // Get Booking Details 
                dsBookings = GetBookingDetails();

                GridDataBinding(dsBookings);

                int resutlCount = 0;
                if (dsBookings != null && dsBookings.Tables != null && dsBookings.Tables.Count > 0)
                    resutlCount = dsBookings.Tables[0].Rows.Count;

                lblTotalRecords.Text = resutlCount.ToString();
                //Message Handling
                GridResultsMessageHandling(resutlCount);

            }
        }
        /// <summary>
        /// Search Data By date
        /// </summary>
        private DataSet GetBookingDetails()
        {
            DataSet dsBookings;
            if (searchData.IsListOut)
            {
                if (!string.IsNullOrEmpty(searchData.Client))
                    DropDownListClient.SelectedValue = searchData.Client;

                dsBookings = this.GetBookingDetails(searchData, userName);
            }
            else
            {
                if (!string.IsNullOrEmpty(searchData.Client))
                    ClientList.SelectedValue = searchData.Client;

                dsBookings = this.GetBookingDetails(searchData, string.Empty);
            }
            return dsBookings;
        }
        /// <summary>
        /// Get Booking Details 
        /// </summary>
        private void SearchDataByDate()
        {
            if (searchData.SearchDateBy > 0)
            {
                if (searchData.SearchDateBy == 1)
                {
                    RadioRange.Checked = true;
                    RadioSpecific.Checked = false;

                    if (!string.IsNullOrEmpty(searchData.StrReservationFrom))
                        this.ReservationDateFrom.Text = searchData.StrReservationFrom;
                    if (!string.IsNullOrEmpty(searchData.StrReservationTo))
                        this.ReservationDateTo.Text = searchData.StrReservationTo;
                }
                else if (searchData.SearchDateBy == 2)
                {
                    RadioRange.Checked = false;
                    RadioSpecific.Checked = true;

                    if (!string.IsNullOrEmpty(searchData.StrReservationDate))
                        this.ReservationDate.Text = searchData.StrReservationDate;
                    if (!string.IsNullOrEmpty(searchData.StrArrivalDate))
                        this.ArrivalDate.Text = searchData.StrArrivalDate;

                }
            }
        }
        #endregion


    }
}