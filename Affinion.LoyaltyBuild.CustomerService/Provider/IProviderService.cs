﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Provider
{
    /// <summary>
    /// Interface to define Provider realted actions 
    /// </summary>
    public interface IProviderService
    {
        /// <summary>
        /// Get Accomodation Details By Item ID
        /// </summary>
        /// <param name="itemId">itemId</param>
        /// <returns>Object of type IAccomodation</returns>
        IAccomodation GetAccomodationDetailsById(string itemId);

    

        /// <summary>
        /// Get Accomodation Details By UCommerce ID
        /// </summary>
        /// <param name="uCommerceId">uCommerceId</param>
        /// <returns>Object of type IAccomodation</returns>
        IAccomodation GetAccomodationDetailsByUcommerceId(string uCommerceId);
    }
}
