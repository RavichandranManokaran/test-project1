﻿using Affinion.LoyaltyBuild.Api.Search.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Api.Search.Providers
{
    /// <summary>
    /// Bedbanks provider service
    /// </summary>
    public class BedBanksProvider : ISearchProvider
    {
        /// <summary>
        /// Search providers based on filter
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        public ProviderCollection SearchProviders(IList<Filters.ISearchFilter> filters)
        {

            return null;
        }

        /// <summary>
        /// Search providers based on filter asyncronusly
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        public async Task<ProviderCollection> SearchProvidersAsync(IList<Filters.ISearchFilter> filters)
        {

            return await Task.Run<ProviderCollection>(() =>
                SearchProviders(filters));
        }
    }
}
