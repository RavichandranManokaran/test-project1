﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FinanceOverallRevenueByClientReport.ascx.cs" Inherits="Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.Reports.FinanceOverallRevenueByClientReport" %>

<style>
    .input-field {
        width: 20%;
    }
    .dropdown-field {
        width: 45%;
    }
</style>

<div class="content">
    <div class="content-body">
        <div id="errMsg" runat="server" class="col-sm-12 alert alert-danger" style="display:none">
        </div>
        <div>
            <div class="row">
                <div class="col-sm-12 marT20">
                    <div class="accordian-outer">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading bg-primary claim_titl">
                                    <h1 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="show-hide-list"><sc:Text id="Finance" field="Finance Overall Revenue by client report" runat="server"/></a>
                                    </h1>
                                </div>
                                <div id="collapse1" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div>
                                            <div class="content_bg_inner_box summary-detail receipt_body">
                                                <div class="col-xs-12 col-md-12">
                                                    <div class="mybooking ">
                                                        <h2 id="parameters" runat="server" class="no-margin accent54-f"><sc:Text id="scFill" runat="server"/></h2>
                                                        <div class="row margin-row">
                                                            <div class="col-sm-5">
                                                                <span class="bask-form-titl font-size-12"><sc:Text id="scGBP" runat="server"/></span>
                                                            </div>
                                                            <div class="col-sm-7 input-field">
                                                                <input type="text" id="GBP" clientidmode="Static" runat="server" class="form-control font-size-12" tabindex="1">
                                                            </div>
                                                        </div>
                                                        <div class="row margin-row">
                                                            <div class="col-sm-5"><span class="bask-form-titl font-size-12"><sc:Text id="scSEk" runat="server"/></span></div>
                                                            <div class="col-sm-7 input-field">
                                                                <input type="text" id="SEK" clientidmode="Static" runat="server" class="form-control font-size-12" tabindex="2">
                                                            </div>
                                                        </div>
                                                        <div class="row margin-row">
                                                            <div class="col-sm-5"><span class="bask-form-titl font-size-12"><sc:Text id="scDKK" runat="server"/></span></div>
                                                            <div class="col-sm-7 input-field">
                                                                <input type="text" id="DKK" clientidmode="Static" runat="server" class="form-control font-size-12" tabindex="3">
                                                            </div>
                                                        </div>
                                                        <div class="row margin-row">
                                                            <div class="col-sm-5"><span class="bask-form-titl font-size-12"><sc:Text id="scNOK" runat="server"/></span></div>
                                                            <div class="col-sm-7 input-field">
                                                                <input type="text" id="NOK" clientidmode="Static" runat="server" class="form-control font-size-12" tabindex="4">
                                                            </div>
                                                        </div>
                                                        <div class="row margin-row">
                                                            <div class="col-sm-5"><span class="bask-form-titl font-size-12"><sc:Text id="scCHF" runat="server"/></span></div>
                                                            <div class="col-sm-7 input-field">
                                                                <input type="text" id="CHF" clientidmode="Static" runat="server" class="form-control font-size-12" tabindex="5">
                                                            </div>
                                                        </div>
                                                        <div class="row margin-row">
                                                            <div class="col-sm-5"><span class="bask-form-titl font-size-12"><sc:Text id="scUSD" runat="server"/></span></div>
                                                            <div class="col-sm-7 input-field">
                                                                <input type="text" id="USD" clientidmode="Static" runat="server" class="form-control font-size-12" tabindex="6">
                                                            </div>
                                                        </div>
                                                        <div class="row margin-row">
                                                            <div class="col-sm-5">
                                                                <span class="bask-form-titl font-size-12"><sc:Text id="scBookingMethod" runat="server"/>
                                                                <span class="accent58-f">*
                                                                </span>
                                                                </span>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <asp:DropDownList ID="BookingMethod" ClientIDMode="Static" runat="server" CssClass="form-control font-size-12 dropdown-field" TabIndex="7">
                                                                    <asp:ListItem>Select Booking Method</asp:ListItem>
                                                                    <asp:ListItem Value="1">Online</asp:ListItem>
                                                                    <asp:ListItem Value="2">Offline</asp:ListItem>
                                                                    <asp:ListItem Value="3">Both Online &amp; Offline</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="row margin-row">
                                                            <div class="col-sm-5">
                                                                <span class="bask-form-titl font-size-12"><sc:Text id="scSubType" runat="server"/>
                                                                <span class="accent58-f">*
                                                                </span>
                                                                </span>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <asp:DropDownList ID="SubTypeReport" ClientIDMode="Static" runat="server" CssClass="form-control font-size-12 dropdown-field" TabIndex="8">
                                                                    <asp:ListItem>Select Subtype Report</asp:ListItem>
                                                                    <asp:ListItem Value="1">With booking fee(Credit card process fee)</asp:ListItem>
                                                                    <asp:ListItem Value="2">Without booking fee(Credit card process fee)</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                         <div class="row margin-row">
                                                            <div class="col-sm-5">
                                                                <span class="bask-form-titl font-size-12"><sc:Text id="scFilter" runat="server"/></span>
                                                            </div>
                                                         </div>
                                                        <div class="row margin-row">
                                                            <div class="col-sm-5">
                                                                <span class="bask-form-titl font-size-12"><sc:Text id="scStartDate" runat="server"/>
                                                                <span class="accent58-f">*
                                                                </span>
                                                                </span>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <div class="form-group">
                                                                    <div class="input-group date">
                                                                       <input type="text" id="StartDate" runat="server" clientidmode="Static" class="form-control datepicker width-85 pull-left font-size-12" tabindex="9" /> 
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row margin-row">
                                                            <div class="col-sm-5">
                                                                <span class="bask-form-titl font-size-12"><sc:Text id="scEndDate" runat="server"/>
                                                                <span class="accent58-f">*
                                                                </span>
                                                                </span>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <div class="form-group">
                                                                    <div class="input-group date">
                                                                       <input type="text" id="EndDate" runat="server" clientidmode="Static" class="form-control datepicker width-85 pull-left font-size-12" tabindex="10" />                     
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 pull-right">
                                                            <asp:Button type="button" ID="Search" runat="server" ClientIDMode="Static" Text="Search" OnClick="Search_Click" class="btn btn-info add-break font-size-12 pull-right" TabIndex="11" />
                                                       </div>

                                                        <input type="hidden" id="CurrencyExchangeName" clientidmode="Static" runat="server" />
                                                        <input type="hidden" id="CurrencyExchangeRate" clientidmode="Static" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <%--Html Report --%>
            <div class="row" id="htmlReportGrid" runat="server">
                <div class="col-sm-12">
                    <div>
                        <div class="panel panel-default">
                            <div class="panel-heading claim_titl">
                                <h4 class="panel-title"><sc:Text id="scBookingReport" runat="server"/>
                                </h4>
                            </div>
                            <div id="totalCount"  runat="server" class="totalCountpad" ><sc:Text id="Bookingdetails" field="Booking details reports Total of" runat="server"/><span> <%= TotalOfRecords %> </span> record(s) found. </div>
                            <div class="panel-body alert-info">
                                <%-- Result Grid--%>
                                 <div runat="server" id="ResultDiv" clientidmode="Static">
                                    <div id="data-grid" class="table-responsive">
                                        <asp:GridView ID="ResultGridView" runat="server" Width="100%" SelectedRowStyle-Font-Bold="true" CssClass="table tbl-calendar" AutoGenerateColumns="False" AllowPaging="true" PageSize="10" OnPageIndexChanging="ResultGridView_PageIndexChanging"
                                             PagerSettings-NextPageText=" Next " PagerSettings-PreviousPageText=" Previous " PagerSettings-Mode="NumericFirstLast" PagerSettings-FirstPageText=" First " PagerSettings-LastPageText=" Last " PagerSettings-Position="Bottom" PagerStyle-Font-Bold="true" PagerStyle-CssClass="pagination-grid">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Bookings">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtBookings" runat="server" Text='<%# Bind("Bookings") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBookings" runat="server" Text='<%# Bind("Bookings") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Client Name">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtPartnerName" runat="server" Text='<%# Bind("ClientName") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPartnerName" runat="server" Text='<%# Bind("ClientName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Client Offer">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtClientOffer" runat="server" Text='<%# Bind("ClientOffer") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblClientOffer" runat="server" Text='<%# Bind("ClientOffer") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Currency Name">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtCurrencyName" runat="server" Text='<%# Bind("CurrencyName") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCurrencyName" runat="server" Text='<%# Bind("CurrencyName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Accounting Rates">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtAccountingRates" runat="server" Text='<%# Bind("AccountingRates") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAccountingRates" runat="server" Text='<%# Bind("AccountingRates") %>' DataFormatString="{0:0.0000000}"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Budget Rates">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtBudgetRates" runat="server" Text='<%# Bind("BudgetRates") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBudgetRates" runat="server" Text='<%# Bind("BudgetRates") %>' DataFormatString="{0:0.0000000}"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="LoyaltyBuild Revenue">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtLoyaltyBuildRevenue" runat="server" Text='<%# Bind("LoyaltyBuildRevenue") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLoyaltyBuildRevenue" runat="server" Text='<%# Bind("LoyaltyBuildRevenue") %>' DataFormatString="{0:0.0000000}"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Vat">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtVat" runat="server" Text='<%# Bind("VAT") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblVat" runat="server" Text='<%# Bind("VAT") %>' DataFormatString="{0:0.00}"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>

                                            <EmptyDataTemplate>
                                                ---No Records---
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {

       
       jQuery("#StartDate").datepicker({
                defaultDate: new Date(),
                dateFormat: "<%=Affinion.LoyaltyBuild.Common.Utilities.DateTimeHelper.MonthDateFormat%>",
                minDate: -180,
                maxDate: 0,
                numberOfMonths: 1,
                showOn: "both",
                buttonImage: "images/calendar.png",
                buttonImageOnly: false,
                buttonClass: "glyphicon-calendar"
            }).next(".ui-datepicker-trigger").addClass("btn btn-default glyphicon glyphicon-calendar calander-icon pull-left");

        jQuery("#EndDate").datepicker({
            defaultDate: new Date(),
            dateFormat: "<%=Affinion.LoyaltyBuild.Common.Utilities.DateTimeHelper.MonthDateFormat%>",
                minDate: -180,
                maxDate: 0,
                numberOfMonths: 1,
                showOn: "both",
                buttonImage: "images/calendar.png",
                buttonImageOnly: false,
                buttonClass: "glyphicon-calendar"
                }).next(".ui-datepicker-trigger").addClass("btn btn-default glyphicon glyphicon-calendar calander-icon pull-left");

                Validation();
    });

    $("#GBP").on("keypress keyup blur",function (event) {
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
        Validation();
    });

    $("#SEK").on("keypress keyup blur", function (event) {
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
        Validation();
    });

    $("#DKK").on("keypress keyup blur", function (event) {
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
        Validation();
    });

    $("#NOK").on("keypress keyup blur", function (event) {
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
        Validation();
    });

    $("#CHF").on("keypress keyup blur", function (event) {
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
        Validation();
    });

    $("#USD").on("keypress keyup blur", function (event) {
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
        Validation();
    });

    $("#BookingMethod").change(function () {
        Validation();
    });

    $("#SubTypeReport").change(function () {
        Validation();
    });

    $("#StartDate").change(function () {
        Validation();
    });

    $("#EndDate").change(function () {
        Validation();
    });

    function Validation() {
        var isCurrencyEnabled = false;
        var isFilterByEnabled = false;
        var gbp = $("#GBP").val();
        var sek = $("#SEK").val();
        var dkk = $("#DKK").val();
        var nok = $("#NOK").val();
        var chf = $("#CHF").val();
        var usd = $("#USD").val();
        var bookingMethod = $("#BookingMethod").val();
        var subTypeReport = $("#SubTypeReport").val();
        var startDate = $("#StartDate").val();
        var endDate = $("#EndDate").val();

        if (gbp != "" || sek != "" || dkk != "" || nok != "" || chf != "" || usd != "")
            isCurrencyEnabled = true;
        else
            isCurrencyEnabled = false;

        if (startDate != "" && endDate != "" && bookingMethod != "Select Booking Method" && subTypeReport != "Select Subtype Report")
            isFilterByEnabled = true;
        else
            isFilterByEnabled = false;

        if (isCurrencyEnabled && isFilterByEnabled)
            $("#Search").removeAttr('disabled');
        else
            $("#Search").attr('disabled', 'disabled');

    }

    function Clear(fieldName) {
        $("#CurrencyExchangeName").val("");
        $("#CurrencyExchangeRate").val("");
        if (fieldName == "GBP") {
            $("#SEK").val("");
            $("#DKK").val("");
            $("#NOK").val("");
            $("#CHF").val("");
            $("#USD").val("");
        }
        else if (fieldName == "SEK") {
            $("#GBP").val("");
            $("#DKK").val("");
            $("#NOK").val("");
            $("#CHF").val("");
            $("#USD").val("");
        }
        else if (fieldName == "DKK") {
            $("#GBP").val("");
            $("#SEK").val("");
            $("#NOK").val("");
            $("#CHF").val("");
            $("#USD").val("");
        }
        else if (fieldName == "NOK") {
            $("#GBP").val("");
            $("#SEK").val("");
            $("#DKK").val("");
            $("#CHF").val("");
            $("#USD").val("");
        }
        else if (fieldName == "CHF") {
            $("#GBP").val("");
            $("#SEK").val("");
            $("#DKK").val("");
            $("#NOK").val("");
            $("#USD").val("");
        }
        else if (fieldName == "USD") {
            $("#GBP").val("");
            $("#SEK").val("");
            $("#DKK").val("");
            $("#NOK").val("");
            $("#CHF").val("");
        }
    }
</script>
