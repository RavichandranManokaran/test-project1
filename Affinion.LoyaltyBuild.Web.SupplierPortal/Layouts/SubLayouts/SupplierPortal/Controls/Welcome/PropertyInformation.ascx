﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PropertyInformation.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Welcome.PropertyInformation" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<span id="confirmredirect" style="display: none"></span>
<div class="container-outer width-full overflow-hidden-att app-bg">
    <div class="container">
       <div class="container1">
        <div>
               <asp:updatepanel id="UpdatePanel1" runat="server" updatemode="Conditional">
          <ContentTemplate>
                    <div class="row">
                        <div class="col-md-12">
                            <div id="groupSection" runat="server" visible="false">
                                <h3 class="title-text font-weight-bold">
                                    <asp:Label ID="groupHotelLabel" runat="server" Text="GROUP:"></asp:Label>
                                    <asp:Label ID="groupHotelNameLabel" runat="server" Text="" /><br />

                                </h3>
                            </div>
                            <div>
                                <h3 class="title-text font-weight-bold">
                                    <asp:Label ID="HotelNameLabel" runat="server" Text="Property Information: " Visible="false" />
                                    <asp:Label ID="HotelName" runat="server" Visible="false" ReadOnly="true" Text="" />
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div id="propInfoContent" runat="server" visible="false" class="middle-body-container">
                        <div class="row">
                            <div class="content">
                            <div class="col-md-12">
                                <div class="left-form">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <div class="item">
                                                <label>Property Name:</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-7">
                                            <div class="item col-xs-6">
                                                <asp:TextBox ID="PropNameLabel" runat="server" ReadOnly="true" CssClass="form-control" MaxLength="20" placeholder="Property Name" AutoPostBack="false" onchange="javascript:setflag();"  />

                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="item">
                                            <label>Address:</label>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="item col-xs-6">
                                            <asp:TextBox ID="txtAddress" runat="server" ReadOnly="true" CssClass="form-control" MaxLength="250" placeholder="Address" AutoPostBack="false" onchange="javascript:setflag();" />
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="item">
                                            <label>Telephone:</label>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="item col-xs-6">
                                                <asp:TextBox ID="txtTelephone" runat="server" ReadOnly="true" MaxLength="10" CssClass="form-control" AutoPostBack="false" placeholder="Telephone" onchange="javascript:setflag();" />
                                        </div>
                                        <div class="item  col-xs-4" style="margin-left: 10px;">
                                            <asp:RequiredFieldValidator Display="Dynamic" ID="ReqFieldValidTelephone" runat="server" CssClass="spvalidationmessage" ControlToValidate="txtTelephone" ErrorMessage="Field Cannot Be Left Empty"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator Display="Dynamic" ID="phoneValidation" runat="server" CssClass="spvalidationmessage" ControlToValidate="txtTelephone" ValidationExpression="[0-9]{10}" ErrorMessage="Enter Only Valid Number"></asp:RegularExpressionValidator>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="item">
                                            <label>Fax:</label>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="item col-xs-6">
                                            <asp:TextBox ID="txtFax" runat="server" CssClass="form-control" ReadOnly="true" placeholder="Fax" AutoPostBack="false" onchange="javascript:setflag();" MaxLength="10" />
                                            <div class="item  col-xs-4" style="margin-left: 10px;">
                                            </div>
                                        </div>
                                        <div class="item col-xs-4" style="margin-left: 10px;">
                                            <asp:RequiredFieldValidator Display="Dynamic" ID="RegFieldValidFax" runat="server" CssClass="spvalidationmessage" ControlToValidate="txtFax" ErrorMessage="Field Cannot Be Left Empty"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator Display="Dynamic" ID="RegValidationForFax" runat="server" CssClass="spvalidationmessage" ControlToValidate="txtFax" ValidationExpression="[0-9]{10}" ErrorMessage="Enter Only Valid Number"></asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                    </div>
                                </div>
                                <div class="row hr-line">
                                    <div class="col-xs-4">
                                        <div class="item">
                                            <label>Hotel e-mail:</label><p>(for booking)</p>

                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="item col-xs-6">
                                            <asp:TextBox ID="txtHotelEmail" runat="server" ReadOnly="true" CssClass="form-control" placeholder="Hotel e-mail" AutoPostBack="false" onchange="javascript:setflag();" />
                                        </div>
                                        <div class="item  col-xs-4" style="margin-left: 10px;">
                                            <asp:RegularExpressionValidator ID="RegularExpForEmail" runat="server" Display="Dynamic" CssClass="spvalidationmessage" ControlToValidate="txtHotelEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="Enter Only Valid Email Id"></asp:RegularExpressionValidator>

                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                    </div>
                                </div>
                                <asp:Repeater ID="contactRepeater" runat="server">
                                    <ItemTemplate>
                                        <asp:Label ID="ItemId" runat="server" Text='<%# Eval("ItemId") %>' Style="display: none;" />
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <div class="item">
                                                    <label>Contact Name:</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-7">
                                                <div class="item col-xs-6">
                                                    <asp:TextBox ID="txtContactName" runat="server" ReadOnly="true" CssClass="form-control" Text='<%# Eval("ContactName") %>' initialText='<%# Eval("ContactName") %>' placeholder="Contact Name" onchange="javascript:setflag();" AutoPostBack="false"/>
                                                </div>
                                                <div class="item  col-xs-4" style="margin-left: 10px;">
                                                    <asp:RequiredFieldValidator ID="ReqFieldForContactName" runat="server" CssClass="spvalidationmessage" ControlToValidate="txtContactName" ErrorMessage="Field Cannot Be Left Empty"></asp:RequiredFieldValidator>

                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <div class="item">
                                                    <label>Contact Job Title:</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-7">
                                                <div class="item col-xs-6">
                                                    <asp:TextBox ID="txtContactJobTitle" runat="server" ReadOnly="true" onchange="javascript:setflag();" CssClass="form-control" Text='<%# Eval("JobTitle") %>' initialText='<%# Eval("JobTitle") %>' placeholder="Contact Job Title" AutoPostBack="false"/>

                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                            </div>
                                        </div>
                                        <%--<div class="row hr-line">--%>
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <div class="item">
                                                    <label>Contact e-mail:</label>
                                                    <p>(for communication)</p>
                                                </div>
                                            </div>
                                            <div class="col-xs-7">
                                                <div class="item col-xs-6">
                                                    <asp:TextBox ID="txtContactEmail" runat="server" ReadOnly="true" CssClass="form-control" Text='<%# Eval("ContactEmail") %>' initialText='<%# Eval("ContactEmail") %>' placeholder="Contact e-mail" AutoPostBack="false" onchange="javascript:setflag();" />
                                                </div>
                                                <div class="item  col-xs-4" style="margin-left: 10px;">
                                                    <asp:RegularExpressionValidator ID="RegExpForEmailContact" runat="server" CssClass="spvalidationmessage" ControlToValidate="txtContactEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage="Enter Only Valid Email Id"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-4">
                                                <div class="item">
                                                    <label>Contact direct telephone:</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-7">
                                                <div class="item col-xs-6">
                                                    <asp:TextBox ID="txtContactDirectPhone" runat="server" ReadOnly="true" CssClass="form-control" Text='<%# Eval("ContactDirectPhone") %>' initialText='<%# Eval("ContactDirectPhone") %>' placeholder="Contact direct telephone" CausesValidation="True" MaxLength="10" AutoPostBack="false" onchange="javascript:setflag();" />
                                                    <asp:Label ID="lblPhoneOnfocus" runat="server" CssClass="custom" Font-Size="Smaller"></asp:Label>
                                                </div>

                                                <div class="item  col-xs-4" style="margin-left: 10px;">
                                                    <asp:RegularExpressionValidator ID="RegExpForPhone" runat="server" CssClass="spvalidationmessage" ControlToValidate="txtContactDirectPhone" ValidationExpression="[0-9]{10}" ErrorMessage="Enter a Valid Phone Number"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                            </div>
                                            <div class="col-xs-4">
                                            </div>
                                            <div class="row hr-line">
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>

                                <br />
                            </div>
                            </div>
                            </div><div class="row hr-line">
                                <div class="col-xs-4">
                                    &nbsp;
                                </div>
                                <div class="col-xs-4">
                                    &nbsp;
                                </div>
                                <div class="col-xs-4">
                                    &nbsp;
                                </div>
                            </div>
                            <div class="row" style="align-content: center">
                                <div class="col-xs-4">
                                    &nbsp;
                                </div>
                                <div class="col-xs-4" style="display: inline-flex;">
                             <div class="btnsubmit">
                                    <asp:Button ID="Submit" Text="Submit Changes for Approval" runat="server" CssClass="form-control margin-right-10" CausesValidation="true" OnClick="Update_Click" Enabled="false" Width="215px" Visible="false"/>
                              </div>
                                </div>
                                <div class="col-xs-12">
                                    <asp:Label ID="updateLblMsg" runat="server" Visible="false" CssClass="spudatemessage" />
                                </div>
                                <div class="col-xs-4">
                                    &nbsp;
                                </div>

                            </div>
                        </div>
                    </div>
               </ContentTemplate> 
                   </asp:updatepanel>


        </div>
    </div>


</div>

