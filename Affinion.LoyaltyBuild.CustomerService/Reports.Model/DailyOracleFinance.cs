﻿/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/// <summary>
/// Entity to hold daily oracle finance related information
/// </summary>

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model
{
    public class DailyOracleFinance
    {
      /// <summary>
      /// Currency
      /// </summary>
      public string Currency { get; set; }
      /// <summary>
      /// Company
      /// </summary>
      public string Company { get; set; }
      /// <summary>
      /// MerchantIdentification
      /// </summary>
      public string MerchantIdentification { get; set; }
      /// <summary>
      /// Partner
      /// </summary>
      public string Partner { get; set; }
      /// <summary>
      /// Department
      /// </summary>
      public string Department { get; set; }
      /// <summary>
      /// Product
      /// </summary>
      public string Product { get; set; }
      /// <summary>
      /// GlCode
      /// </summary>
      public string GlCode { get; set; }
      /// <summary>
      /// Client
      /// </summary>
      public string Client { get; set; }
      /// <summary>
      /// Project
      /// </summary>
      public string Project { get; set; }
      /// <summary>
      /// Spare
      /// </summary>
      public string Spare { get; set; }
      /// <summary>
      /// AmountDebit
      /// </summary>
      public decimal AmountDebit { get; set; }
      /// <summary>
      /// AmountCredit
      /// </summary>
      public decimal AmountCredit { get; set; }
      /// <summary>
      /// Description
      /// </summary>
      public string Description { get; set; }
      /// <summary>
      /// Count
      /// </summary>
      public decimal Count { get; set; }
      /// <summary>
      /// ClientID
      /// </summary>
      public string ClientID { get; set; }
    }
}
