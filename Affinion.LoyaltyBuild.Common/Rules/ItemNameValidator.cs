﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
///Source File:           ItemNameValidator.cs
///Sub-system/Module:     Affinion.LoyaltyBuild.Common
///Description:           Class for Item Name Validator rules
///</summary>

#region Using Directives
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Diagnostics;
using Sitecore.Rules;
using Sitecore.Rules.Actions;
using System;
using System.Text.RegularExpressions;
#endregion

namespace Affinion.LoyaltyBuild.Common.Rules
{
    public class ItemNameValidator<T> : RuleAction<T> where T : RuleContext
    {
        #region public methods
        /// <summary>
        /// Renames the item name by converting spaces to dashes 
        /// and Uppercase letters to Lowercase
        /// </summary>
        /// <param name="ruleContext">Pass the ruleContext</param>
        public override void Apply(T ruleContext)
        {
            try
            {
                Assert.ArgumentNotNull(ruleContext, "ruleContext");
                var item = ruleContext.Item;
                if (item == null)
                    return;

                // If spaces are found in the name, replace them.
                string newName = SitecoreItemHelper.SetItemName(item.Name.ToLower());
                if (item.Name == newName)
                {
                    // If no spaces were found, do nothing.
                    return;
                }

                // Edit the item that was just saved.
                item.Editing.BeginEdit();
                try
                {
                    // Set the display name to the original name (with spaces).
                    item.Appearance.DisplayName = item.Name;

                    // Set the item name to the new name (with dashes).
                    item.Name = newName;
                }
                finally
                {
                    item.Editing.EndEdit();
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, this);
                throw;
            }
        }
        #endregion
    }
}
