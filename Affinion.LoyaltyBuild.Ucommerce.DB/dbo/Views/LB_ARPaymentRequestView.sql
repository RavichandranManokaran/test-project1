CREATE VIEW [dbo].[LB_ARPaymentRequestView]
AS
SELECT DISTINCT
		VAT.SupplierVatPaymentId [SupplierVatPaymentId],
		P.ProductId [HotelID],
		P.Name [HotelName],
		PPD.SiteCoreItemGuid [ProviderID],
		VAT.DateCreated [DemandDate],
		'REF='+BI.BookingNo [BookingRefNo],
		'ID = '+CONVERT(VARCHAR,VAT.OrderId) AS BookingID, 
		VAT.OrderId [OrderID],
		VAT.OrderLineId,
		(C.FirstName+' '+C.LastName) [CustomerName],
		BI.CheckInDate,
		BI.CheckOutDate,
		ISNULL(VAT.IsPaidToAccounts,0) [PaymentStatus],
		VAT.GrossAmount [GrossAmount],
		VAT.SuggestedVatAmount [SuggestedAmount],
		VAT.NetAmount [NetAmount],
		VAT.PostedVatAmount [PostedAmount],
		VAT.OrderCurrencyIsoCode [Currency],
		VAT.DatePostedToOracle [DatePostedToOracle],
		'AR'+CONVERT(VARCHAR,VAT.OracleFileId) [OracleFieID],
		OP.ClientId
FROM LB_BookingInfo BI
		INNER JOIN ucommerce_OrderLine OL ON BI.OrderLineId=OL.OrderLineId
		INNER JOIN LB_Vat_Payments VAT ON  OL.OrderId=VAT.OrderId AND VAT.OrderLineId=OL.OrderLineId
		INNER JOIN (
		   SELECT OrderLineId,OrderId,_ClientId [ClientId]
			FROM 
			(
			  SELECT OrderLineId,OrderId, [key], [value]
			  FROM uCommerce_OrderProperty 
			) src
			PIVOT
			(
			  MAX([value])
			  FOR [key] IN (_ClientId)
			) piv
		) OP ON VAT.OrderId=OP.OrderId AND VAT.OrderLineId=OP.OrderLineId
		INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId=PO.OrderId AND VAT.OrderId=PO.OrderId
		INNER JOIN LB_CustomerProperty CP ON PO.CustomerId=CP.CustomerId
		INNER JOIN uCommerce_Customer C ON CP.CustomerId=C.CustomerId
		INNER JOIN Ucommerce_product P ON OL.Sku = P.Sku AND P.ParentProductId IS NULL AND P.VariantSku IS NULL
	    INNER JOIN uCommerce_ProductDefinition PD ON P.ProductDefinitionId=PD.ProductDefinitionId AND PD.Name='Hotel'	
		INNER JOIN (
		   SELECT ProductId,SiteCoreItemGuid
			FROM 
			(
			  SELECT ProductId,PD.Name [key], PP.value
			  FROM uCommerce_ProductDefinitionField PD 
			  INNER JOIN uCommerce_ProductProperty PP ON PD.ProductDefinitionFieldId=PP.ProductDefinitionFieldId
			  WHERE PD.Name IN ('SiteCoreItemGuid')
			) src
			PIVOT
			(
			  MAX([value])
			  FOR [key] IN (SiteCoreItemGuid)
			) piv
		) PPD ON PPD.ProductId=P.ProductId 
		 
WHERE 
	    BI.BookingNo IS NOT NULL AND
	    PO.CustomerId IS NOT NULL 
				


