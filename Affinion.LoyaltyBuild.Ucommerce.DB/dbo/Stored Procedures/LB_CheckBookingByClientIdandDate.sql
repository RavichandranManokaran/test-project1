﻿CREATE PROCEDURE [dbo].[LB_CheckBookingByClientIdandDate]

@ClientId NVARCHAR(100),
@Fromtime DATETIME,
@Totime DATETIME

AS
BEGIN

SELECT 
	OP.OrderLineId,
	Value as ClientId 
	FROM
	uCommerce_OrderProperty AS OP
	Join
	LB_OrderLineStatusAudit AS OS
	On OP.OrderLineId = OS.OrderLineId
	WHERE [KEY]  = 'ClientId' and
	CONVERT(datetime,OS.CreatedOn,103) between @FROMTIME and @TOTIME
	and (OS.StatusId = 5 or -- If the booking is provisonal
			 OS.StatusId = 6) -- If the booking is confirmed

END

