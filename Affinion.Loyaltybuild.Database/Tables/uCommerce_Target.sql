﻿CREATE TABLE [dbo].[uCommerce_Target](
	[TargetId] [int] IDENTITY(1,1) NOT NULL,
	[CampaignItemId] [int] NOT NULL,
	[EnabledForDisplay] [bit] NOT NULL,
	[EnabledForApply] [bit] NOT NULL,
 CONSTRAINT [PK_uCommerce_Target] PRIMARY KEY CLUSTERED 
(
	[TargetId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


