SET IDENTITY_INSERT uCommerce_OrderStatus ON
IF NOT EXISTS(SELECT 1 FROM uCommerce_OrderStatus Where Name='Transferred' AND OrderStatusId=8)
BEGIN
      INSERT INTO uCommerce_OrderStatus(OrderStatusId,Name)
	  VALUES(8,'Transferred')
END
SET IDENTITY_INSERT uCommerce_OrderStatus OFF

