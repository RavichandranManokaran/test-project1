﻿CREATE TABLE [dbo].[uCommerce_CustomerAttribute]
(
	[CustomerId] INT NOT NULL PRIMARY KEY, 
    [ClientId] VARCHAR(50) NOT NULL, 
    [Title] VARCHAR(50) NULL, 
    [AddressLine1] NVARCHAR(50) NOT NULL, 
    [AddressLine2] NVARCHAR(50) NULL, 
    [AddressLine3] NVARCHAR(50) NULL, 
    [CountryId] INT NOT NULL, 
    [LocationId] INT NOT NULL, 
	[BranchId] VARCHAR(50) NULL,
	[Subscribe] BIT NOT NULL DEFAULT 0,
    [BookingReferenceId] VARCHAR(50) NULL, 
    [Active] BIT NOT NULL DEFAULT 1   
	
)
