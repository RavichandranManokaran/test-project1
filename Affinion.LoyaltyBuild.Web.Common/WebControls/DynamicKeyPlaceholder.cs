﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           DynamicKeyPlaceholder.cs
/// Sub-system/Module:     Web.Common(VS project name)
/// Description:           This webcontrol can produce dynamic keys and allows to exist multiple times as a sitecore placeholder at the same page
/// </summary>
namespace Affinion.LoyaltyBuild.Web.Common.WebControls
{
    using Sitecore;
    using Sitecore.Common;
    using Sitecore.Configuration;
    using Sitecore.Data.Items;
    using Sitecore.Data.Managers;
    using Sitecore.Data.Templates;
    using Sitecore.Diagnostics;
    using Sitecore.Layouts;
    using Sitecore.Web.UI;
    using Sitecore.Web.UI.WebControls;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Web;
    using System.Web.UI;

    /// <summary>
    /// This webcontrol can produce dynamic keys and allows to exist multiple times as a sitecore placeholder at the same page
    /// </summary>
    public class DynamicKeyPlaceholder : WebControl, IExpandable
    {
        protected string key = Placeholder.DefaultPlaceholderKey;
        protected string dynamicKey;
        protected Placeholder placeholder;

        /// <summary>
        /// Gets the inner place holder
        /// </summary>
        public Placeholder InnerPlaceholder
        {
            get { return placeholder; }
        }

        /// <summary>
        /// Gets or sets the initial key
        /// </summary>
        public string Key
        {
            get
            {
                return key;
            }
            set
            {
                if (value == null)
                {
                    key = value.ToLower();
                }
            }
        }

        /// <summary>
        /// Gets or sets the dynamic key
        /// </summary>
        protected string DynamicKey
        {
            get
            {
                if (dynamicKey != null)
                {
                    return dynamicKey;
                }

                dynamicKey = key;

                // find the last placeholder processed, will help us find our parent
                var stack = Switcher<Placeholder, PlaceholderSwitcher>.GetStack(false);

                if (stack == null || stack.Count == 0)
                {
                    // not used within a placeholder apparently. dynamic key is actually not necessary in this case.
                    return dynamicKey;
                }

                var current = stack.Peek();

                // find the rendering reference we are contained in
                var renderings = Sitecore.Context.Page.Renderings.Where(rendering => (rendering.Placeholder
                    == current.ContextKey || rendering.Placeholder == current.Key) && rendering.AddedToPage);

                var renderingReferences = renderings as IList<RenderingReference> ?? renderings.ToList();

                if (renderings.Any())
                {
                    dynamicKey = key + "_Dynamic_" + renderings.Count().ToString();
                }

                return dynamicKey;
            }
        }

        /// <summary>
        /// Create child placeholders
        /// </summary>
        protected override void CreateChildControls()
        {
            Sitecore.Diagnostics.Tracer.Debug("DynamicKeyPlaceholder: Adding dynamic placeholder with Key" + DynamicKey);

            placeholder = new Placeholder { Key = this.DynamicKey };
            this.Controls.Add(placeholder);

            placeholder.Expand();
        }

        /// <summary>
        /// Render the elements
        /// </summary>
        /// <param name="output">The output</param>
        protected override void DoRender(HtmlTextWriter output)
        {
            base.RenderChildren(output);
        }

        #region IExpandable Members

        /// <summary>
        /// Implements the expand method
        /// </summary>
        public void Expand()
        {
            this.EnsureChildControls();
        }

        #endregion
    }
}