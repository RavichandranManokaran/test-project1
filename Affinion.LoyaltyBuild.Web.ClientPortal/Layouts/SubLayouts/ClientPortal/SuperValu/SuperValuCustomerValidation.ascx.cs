﻿namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal
{
    using System;
    using Affinion.Loyaltybuild.BusinessLogic.Helper;
    using Affinion.LoyaltyBuild.Common;
    using Affinion.LoyaltyBuild.Common.Exceptions;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Communications.ExceptionServices;
    using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Sitecore.Data.Items;
    using System.Data;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Web.UI;
    using Validators = Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Validators;
    using ValidatorsModel = Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Rules.Model;
    using System.Web.UI.WebControls;

    public partial class SuperValuCustomerValidation : BaseSublayout
    {
        ExceptionMailHelper ES = new ExceptionMailHelper();
        Sitecore.Data.Database currentDb = Sitecore.Context.Database;

        private Item clientItem = SitecoreFieldsHelper.GetLookupFieldTargetItem(ItemHelper.RootItem, "ClientDetails");

        protected string RequiredFieldMessage { get; set; }
        public Item ValidateContinueToDetailItem { get; set; }

        private string SelectedOption
        {
            get
            {
                if (SuperValuOption1.Checked)
                    return "RealRewardCardNoOption";
                else if (SuperValuOption2.Checked)
                    return "CollectorCardCustomer";
                else
                    return "NoOptionSelected";
            }
        }

        private bool CanPerformServerValidation { get; set; }

        private string ClientId
        {
            get
            {
                return clientItem.ID.ToString();
            }
        }

        private string ClientName
        {
            get
            {
                return clientItem.Name;
            }
        }

        private string CollectorCardValue
        {
            get
            {
                return TxtCollectorCard.Text;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            try
            {
                //  LabelAccomodationValidation.Text = string.Empty;
                Item item = this.GetDataSource();

                if (!UCommerce.Api.TransactionLibrary.HasBasket())
                {
                    SuperValuValidation1.Visible = true;
                }

                InitializeFields(item);
            }
            catch (Exception exception)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, exception.Message);
                throw;
            }
        }

        private bool? IsValidationNumericType()
        {
            var ruleName = "isValidationNumberOnly";
            ValidatorsModel.ValidationRequest validationRequest = new ValidatorsModel.ValidationRequest();

            validationRequest.ClientId = ClientId;
            validationRequest.RequestingApplication = ValidatorsModel.Application.Online;
            validationRequest.RuleCategory = ValidatorsModel.RuleCategory.PartnerValidation;
            validationRequest.RuleName = ruleName;
            var response = new Validators.ValidationController().Execute(validationRequest);

            bool validationResponse = false;
            if (response != null && response.IsValidationSuccess && response.PartnerValidation != null && response.PartnerValidation.ContainsKey(ruleName))
            {
                return response.PartnerValidation.TryGetValue(ruleName, out validationResponse)
                                ? validationResponse
                                : false;
            }
            return null;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ContinueToDetailButton_Click(object sender, EventArgs e)
        {
            string input = string.Empty;
            bool canProceed = true;
            switch (SelectedOption)
            {
                //When 1st option is Opted
                case "RealRewardCardNoOption":
                    {
                        input = CollectorCardValue;
                        //If Validation is of only Numeric Then ignore Remaining Validation & Perform Regex Check
                        var numericValidationCheck = IsValidationNumericType();
                        if (numericValidationCheck == null)
                        {
                            //Need to Add Logic
                            canProceed = false;
                        }
                        else
                        {
                            //If Numberic Check Needs to be performed
                            if ((bool)numericValidationCheck)
                            {
                                //Contains Only Numeric
                                canProceed = Regex.IsMatch(CollectorCardValue, @"^[0-9]+$", RegexOptions.None);
                                if (!canProceed)
                                {
                                    canProceed = false;
                                    CanPerformServerValidation = true;
                                    ServerCustomValidator.Text = "Invalid Card Input";
                                    Page.Validate("SuperValuCardValidationGroup");          //Need to work on Triggering server side validation                          
                                }
                            }
                            else
                            {
                                ValidatorsModel.ValidationRequest validationRequest = new ValidatorsModel.ValidationRequest();
                                validationRequest.ClientId = ClientId;
                                validationRequest.InputToValidate = input;
                                validationRequest.RequestingApplication = ValidatorsModel.Application.Online;
                                validationRequest.RuleCategory = ValidatorsModel.RuleCategory.Content;
                                var response = new Validators.ValidationController().Execute(validationRequest);
                                if (response.IsValidationSuccess)
                                {
                                    canProceed = true;
                                    //Proceed with Success Scenario. 
                                    //Need to work on How we are going to Handle in next user Control
                                }
                                else
                                {
                                    ServerCustomValidator.Text = response.ErrorMessage;
                                    CanPerformServerValidation = true;
                                    Page.Validate("SuperValuCardValidationGroup");
                                    canProceed = false;
                                }
                            }

                            if(canProceed)
                            {
                                //Client:ClientID
                                //ParameterKey:PartnerValidationCard
                                //ParameterValue:CardNo
                                //CustomerReference:EmailId [For ATG] & Empty for SuperValu
                                string[] parameterValue = new string[] 
                                                            {
                                                                string.Format("Client:{0}",ClientId),
                                                                string.Format("ParameterKey:{0}",SelectedOption),
                                                                string.Format("ParameterValue:{0}",input),
                                                                string.Format("CustomerReference:{0}",string.Empty),
                                                                string.Format("IsClientValidationSuccess:True")
                                                            };

                                    //string.Format("Client:{0};ParameterKey:{1};ParameterValue:{2};CustomerReference:{3}", ClientId, SelectedOption, input, string.Empty);
                                Sitecore.Events.Event.RaiseEvent("lb:ClientValidationSuccess", parameterValue);
                            }
                        }
                        //Sitecore.Events.Event.RaiseEvent("lb:ClientValidationSuccess", "vikashraajb@virtusapolaris.com");
                    }
                    break;

                //When 2nd option is Opted
                case "CollectorCardCustomer":
                    {
                        input = TxtRealRewardsCardId.Text;

                        //Yet To Receive the Information on the same.
                        ClientcustomValidator.Text = "Invalid Real Rewards Card No";
                        Sitecore.Events.Event.RaiseEvent("lb:ClientValidationSuccess", "vikashraajb@virtusapolaris.com");
                    }
                    break;

                //When No option is Opted
                case "NoOptionSelected":
                default:
                    canProceed = false;
                    break;
            }

            //Use canProceed to perform any action

        }

        protected void PerformServerValidationOnCard(object sender, ServerValidateEventArgs args)
        {
            if (CanPerformServerValidation)
            {
                args.IsValid = false;
                CanPerformServerValidation = false; //To Avoid Repeated Runs
            }
        }


        /// <summary>
        /// Generate Email on validation 
        /// </summary>
        private void mailGenarate()
        {
            try
            {
                //Database currentDB = Sitecore.Context.Database;
                string ValidateContinueToDetailItemPath = string.Concat("/sitecore/content/client-portal/", clientItem.Name, "/global/_supporting-content/validation-email");
                ValidateContinueToDetailItem = currentDb.GetItem(ValidateContinueToDetailItemPath).Children.FirstOrDefault(i => i.TemplateName == "GeneralEmail");
                Affinion.LoyaltyBuild.Communications.IEmailService emailService = new Affinion.LoyaltyBuild.Communications.Services.EmailService();

                string senderID = SitecoreFieldsHelper.GetValue(ValidateContinueToDetailItem, "SenderEmail");
                string mailSubject = SitecoreFieldsHelper.GetValue(ValidateContinueToDetailItem, "Subject");
                string msgBody = SitecoreFieldsHelper.GetValue(ValidateContinueToDetailItem, "EmailBody");
                string receiver = SitecoreFieldsHelper.GetValue(ValidateContinueToDetailItem, "ReceiverEmail");
                if (senderID == null || receiver == null)
                {
                    return;
                }
                emailService.Send(senderID, receiver, mailSubject, msgBody, true);
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, exception, this);
            }

        }
        /// <summary>
        /// Validate fields 
        /// </summary>
        /// <param name="item">datasource item</param>
        private void ValidateFields(Item item)
        {
            Item conditionalMessageItem = this.GetItemFromRenderingParameterPath("Conditional Messages Link");
           // string realRewardsCardId = TxtRealRewardsCardId.Text;
            //string collectorCard = TxtCollectorCard.Text;


            if (Page.IsValid)
            {
             //   TxtRealRewardsCardId.CssClass = "form-control";
              //  TxtCollectorCard.CssClass = "form-control";
                //Need to Check Why Required
                //Sitecore.Events.Event.RaiseEvent("lb:ClientValidationSuccess", email);               
            }

        }

        /// <summary>
        /// Validate the accomodation page data
        /// </summary>
        /// <returns>return true if validation successfull</returns>
        private bool ValidateAccomodation(Item item)
        {
            try
            {
                int orderID = BasketHelper.GetBasketOrderId();
                DataSet ordeLineIDs = uCommerceConnectionFactory.GetOrderLines(orderID);

                for (int i = 0; i < ordeLineIDs.Tables[0].Rows.Count; i++)
                {
                    string orderLineID = ordeLineIDs.Tables[0].Rows[i]["OrderLineId"].ToString();
                    DataSet reservationCount = uCommerceConnectionFactory.GetAccomodationDetailsByOrderLineId(orderLineID);
                    string guestName = reservationCount.Tables[0].Rows[0]["GuestName"].ToString();
                    if (guestName.Length <= 0)
                    {
                        return false;
                    }
                }
                return true;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException("Error in validating accomadation", exception.InnerException);
            }
        }
        private void BindSitecoreErrorMessages()
        {
            Item item = this.GetItemFromRenderingParameterPath("Conditional Messages Link");
            Item currentPageItem = Sitecore.Context.Item;
            //Need to Add items to SiteCore and Map Back
        }
        
        private void InitializeFields(Item item)
        {
            if (item != null)
            {
                ContinueToDetailButton.Attributes["class"] += "sec-payment";
            }
        }
    }
}
