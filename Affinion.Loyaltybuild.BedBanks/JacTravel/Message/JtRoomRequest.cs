﻿namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Message
{
    /// <summary>
    /// This is a class to hold the Room Request Parameters.
    /// It will be passed to the API controller to perform the actual request.
    /// </summary>
    public class JtRoomRequest
    {
        public string HotelId, SessionId;

        public JtRoomRequest(string pHotelid, string pSessionid)
        {
            HotelId = pHotelid;
            SessionId = pSessionid;
        }
    }
}