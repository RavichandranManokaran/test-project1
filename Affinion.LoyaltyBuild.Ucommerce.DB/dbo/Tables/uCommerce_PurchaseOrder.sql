﻿CREATE TABLE [dbo].[uCommerce_PurchaseOrder] (
    [OrderId]               INT              IDENTITY (1, 1) NOT NULL,
    [OrderNumber]           NVARCHAR (50)    NULL,
    [CustomerId]            INT              NULL,
    [OrderStatusId]         INT              NOT NULL,
    [CreatedDate]           DATETIME         CONSTRAINT [uCommerce_DF_Order_CreatedDate] DEFAULT (getdate()) NOT NULL,
    [CompletedDate]         DATETIME         NULL,
    [CurrencyId]            INT              NOT NULL,
    [ProductCatalogGroupId] INT              NOT NULL,
    [BillingAddressId]      INT              NULL,
    [Note]                  NTEXT            NULL,
    [BasketId]              UNIQUEIDENTIFIER CONSTRAINT [uCommerce_DF_PurchaseOrder_BasketId] DEFAULT (newid()) NOT NULL,
    [VAT]                   MONEY            NULL,
    [OrderTotal]            MONEY            NULL,
    [ShippingTotal]         MONEY            NULL,
    [PaymentTotal]          MONEY            NULL,
    [TaxTotal]              MONEY            NULL,
    [SubTotal]              MONEY            NULL,
    [OrderGuid]             UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [ModifiedOn]            DATETIME         DEFAULT (getdate()) NOT NULL,
    [CultureCode]           NVARCHAR (60)    NULL,
    [Discount]              MONEY            NULL,
    [DiscountTotal]         MONEY            NULL,
    CONSTRAINT [uCommerce_PK_Order] PRIMARY KEY CLUSTERED ([OrderId] ASC),
    CONSTRAINT [FK_uCommerce_PurchaseOrder_uCommerce_OrderAddress] FOREIGN KEY ([BillingAddressId]) REFERENCES [dbo].[uCommerce_OrderAddress] ([OrderAddressId]),
    CONSTRAINT [uCommerce_FK_Order_Currency] FOREIGN KEY ([CurrencyId]) REFERENCES [dbo].[uCommerce_Currency] ([CurrencyId]),
    CONSTRAINT [uCommerce_FK_Order_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[uCommerce_Customer] ([CustomerId]),
    CONSTRAINT [uCommerce_FK_Order_OrderStatus1] FOREIGN KEY ([OrderStatusId]) REFERENCES [dbo].[uCommerce_OrderStatus] ([OrderStatusId]),
    CONSTRAINT [uCommerce_FK_Order_ProductCatalogGroup] FOREIGN KEY ([ProductCatalogGroupId]) REFERENCES [dbo].[uCommerce_ProductCatalogGroup] ([ProductCatalogGroupId])
);


GO
CREATE NONCLUSTERED INDEX [IX_Order]
    ON [dbo].[uCommerce_PurchaseOrder]([OrderId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_uCommerce_PurchaseOrder_BasketId]
    ON [dbo].[uCommerce_PurchaseOrder]([BasketId] ASC);

