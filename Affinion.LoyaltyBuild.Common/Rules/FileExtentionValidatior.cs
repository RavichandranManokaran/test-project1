﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
///Source File:           FileExtentionValidatior.cs
///Sub-system/Module:     Affinion.LoyaltyBuild.Common.Rules
///Description:           Class for Field  Validator rules
///</summary>
#region Using Directives
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Sitecore.Data.Items;
using Sitecore.Data.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
#endregion

namespace Affinion.LoyaltyBuild.Common.Rules
{
    /// <summary>
    /// File to validate the extension
    /// </summary>
    [Serializable]
    public class FileExtentionValidatior : StandardValidator
    {

        #region constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public FileExtentionValidatior()
        {

        }

        public FileExtentionValidatior(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {

        }
        #endregion

        /// <summary>
        /// ValidatorResult for given field
        /// </summary>
        /// <returns></returns>
        protected override ValidatorResult Evaluate()
        {
            try
            {
                // get the master database
                var db = Sitecore.Configuration.Factory.GetDatabase("master");

                Item myItem = base.GetItem();

                // get the parameter from rule
                string fileExtention = base.Parameters["Extension"].ToString();

                var fieldValue = base.GetControlValidationValue();

                if (fieldValue.Contains('{'))
                {

                    string ItemID = fieldValue.Split('{')[1].Split('}')[0];

                    Item itemToValidate = db.GetItem(ItemID);

                    MediaItem mediaItem = new MediaItem(itemToValidate);

                    // check the file extension
                    if (mediaItem.Extension.Length == 0)
                    {
                        return ValidatorResult.Valid;
                    }

                    if (mediaItem.Extension != fileExtention)
                    {
                        Text = String.Format("Please Select a valid " + fileExtention + " document");
                        return ValidatorResult.FatalError;
                    }
                }
                else
                {
                    if (fieldValue.Length != 0)
                    {
                        if (db.GetItem(string.Concat("/sitecore/media library/", fieldValue)) == null)
                        {
                            Text = String.Format("File not exists");
                            return ValidatorResult.FatalError;
                        }

                        MediaItem mediaItem = new MediaItem(db.GetItem(string.Concat("/sitecore/media library/", fieldValue)));
                        if (mediaItem.Extension != fileExtention)
                        {
                            Text = String.Format("Please Select a valid " + fileExtention + " document");
                            return ValidatorResult.FatalError;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, this);
                throw;
            }

            return ValidatorResult.Valid;
        }

        /// <summary>
        /// ValidatorResult of a field
        /// </summary>
        /// <returns></returns>
        protected override ValidatorResult GetMaxValidatorResult()
        {
            return GetFailedResult(ValidatorResult.FatalError);
        }

        /// <summary>
        /// overide the name
        /// </summary>
        public override string Name
        {
            get { return "File extension mismatch"; }
        }
    }
}
