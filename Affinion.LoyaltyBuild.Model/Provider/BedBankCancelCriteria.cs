﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Model.Provider
{
    /// <summary>
    /// Bed bank cancellation criteria
    /// </summary>
    [Serializable]
    [JsonObject(Title = "bbc")]
    public class BedBankCancelCriteria
    {
        /// <summary>
        /// Start Date
        /// </summary>
        [JsonProperty(PropertyName = "sd")]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// End Date
        /// </summary>
        [JsonProperty(PropertyName = "ed")]
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Penality Amount
        /// </summary>
        [JsonProperty(PropertyName = "amt")]
        public decimal Amount { get; set; }
    }
}
