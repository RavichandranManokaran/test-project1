/*jslint browser: true*/
/*global $, jQuery*/

/*
Custom ResponsiveMenu 
*/
// Anonymous Function Wrapper

(function () {
    "use strict";
    //Responsive Menu Handler
    //Read State 
    function pushMenu() {
        var menuRight, body;
        menuRight = jQuery('#ty-res-nav');
        body = jQuery("body");
        jQuery(body).toggleClass("cbp-spmenu-push-toleft");
        jQuery(menuRight).toggleClass("cbp-spmenu-open");
        // body...
    }
    //No Push Menu
    function noPushMenu() {
        var body = jQuery("body");
        jQuery("#ty-res-nav").removeClass("cbp-spmenu-open");
        jQuery(body).removeClass("cbp-spmenu-push-toleft");
    }
    /*Enable Responsive Menu*/
    function enableResMenu() {

        jQuery(".cbp-spmenu-push").css({
            "right": "240px"
        });
        jQuery("#ty-res-nav").removeClass();
        jQuery("#ty-res-nav").addClass("cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right ty-margin-top-navi");
        jQuery(".ty-nav-back").removeClass("ty-nav-back");
        jQuery("#nav2").removeClass("ty-navbar");
        jQuery(".ty-noPadding-left-right").addClass("ty-5padding");
        jQuery(".ty-social-icons").prependTo('#ty-topleft-links');
        //Menu Replacement
        jQuery("#ty-nav-user").appendTo("#nav2"); // Login user
        jQuery("#lang-globe").appendTo("#nav2"); // Language selector
        /*Global Menu Re-Structure Enable On Responsive */
        jQuery(".ty-topleft-links-home").appendTo("#nav2");

        jQuery(".ty-topleft-links-home").show();
        jQuery("#lang-globe").addClass("ty-taylor-nav2 ty-nav-responsive");
        jQuery("#ty-nav-user").removeClass();
        jQuery("#ty-nav-user").addClass("nav navbar-nav ty-taylor-nav2 ty-nav-responsive");
        jQuery("#ty-nav-user").show();
        //        init();

    }
    /*Disable Responsive Menu*/
    function disableResMenu() {

        jQuery(".cbp-spmenu-push").css({
            "right": "0"
        });
        jQuery("#nav2").addClass("ty-navbar");
        jQuery("#ty-res-nav").removeClass();
        jQuery("#ty-res-nav").addClass("navbar navbar-default");
        jQuery(".ty-taylor-nav2").addClass("ty-nav-back");
        jQuery("#ty-nav-user").appendTo("#ty-nav-Main");
        jQuery("#ty-nav-user").removeClass();
        jQuery(".ty-5padding").addClass("ty-noPadding-left-right");
        jQuery(".ty-noPadding-left-right").removeClass("ty-5padding");
        jQuery("#ty-nav-user").addClass("nav navbar-nav navbar-right");
        jQuery("#lang-globe").appendTo(".lang-globe-holder-nav1");
        /*Global Menu Re-Structure Disable On Responsive */
        jQuery(".ty-topleft-links-home").appendTo("#ty-topleft-links");

        jQuery("#lang-globe").removeClass();
        jQuery("#lang-globe").addClass('nav navbar-nav');
        jQuery(".ty-social-icons").prependTo('.ty-social-icons-holder');
    }

    jQuery(document).on('click', '*', function (evt) {
        var resNav = jQuery('#ty-res-nav'),
            trigger = jQuery('#ty-navtrigger');
        if (jQuery(this).is(trigger)) {
            evt.stopPropagation(); // stop the event propagating to ancestral elements
            trigger.css("background", "#0093d0");
            pushMenu();
        } else if (!jQuery(this).closest(resNav).length) {
            //<-- on click outside, hide menu   
            trigger.css("background", "#0093d0");
            noPushMenu();
        }
    });
    //Document Ready Event Handler for ResMenu
    jQuery(document).ready(function () {
        var width, height;
        width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
        height = (window.innerHeight > 0) ? window.innerHeight : screen.height;
        if (width <= 767) {
            enableResMenu();
            jQuery("#nav2").css("max-height", (height - 60));
        } else {
            disableResMenu();
        }
    });
    //On Resize CrosBrowser Handler
    jQuery(window).on('resize', function () {
        noPushMenu();
        var width, height;
        width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
        height = (window.innerHeight > 0) ? window.innerHeight : screen.height;
        if (width <= 767) {
            enableResMenu();
            jQuery("#nav2").css("max-height", (height - 60));
        } else {
            disableResMenu();
        }
    });
}());
//End of Wrapper Annon