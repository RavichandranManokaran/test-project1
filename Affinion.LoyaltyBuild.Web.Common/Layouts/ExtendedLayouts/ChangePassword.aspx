﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="Affinion.LoyaltyBuild.Web.Common.Layouts.ExtendedLayouts.ChangePassword" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Change Password</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link href="/Resources/styles/affinion-admin.min.css" rel="stylesheet" />
    <link href="/Resources/styles/changePasswordCustom.css" rel="stylesheet" />
    <link rel="stylesheet" href="/Resources/styles/bootstrap.min.css"/>
    <link rel="stylesheet" href="/Resources/styles/affinion-admin.min.css"/>
    <link rel="stylesheet" href="/Resources/styles/theme1/theme1.css"/>
    <link rel="stylesheet" href="/Resources/styles/affinion-common.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

    <%--<link href="/sitecore/login/default.css" rel="stylesheet" />--%>
    <style>
        #NewPassword_info {
            position: absolute;
            top: 438px;
            right: 494px;
            width: 250px;
            padding: 15px;
            background: #fefefe;
            font-size: .875em;
            border-radius: 5px;
            box-shadow: 0 1px 3px #ccc;
            border: 1px solid #ddd;
            z-index: 1;
        }

            #NewPassword_info h4 {
                margin: 0 0 10px 0;
                padding: 0;
                font-weight: normal;
            }

            #NewPassword_info::before {
                display: none;
            }
            .info{
              cursor: pointer;
             }
    </style>



</head>
<body class="change-password">
    <form id="LoginForm" runat="server">
        <div class="container-outer width-full overflow-hidden-att app-bg-cl">
            <div class="container-outer-inner width-full">
                <div class="container">
                    <div id="Body">
                        <div id="Banner">
                            <div id="BannerPartnerLogo">
                            </div>
                            <%-- <img id="CompanyLogo" src="/Resources/images/LBLogo-home.png" alt="Company Logo" border="0" />--%>
                        </div>
                        <div id="Menu">
                            &nbsp;
                        </div>

                        <div id="FullPanel">
                            <div id="FullTopPanel" style="padding: 0px 0px 32px 0px">
                                <div class="FullTitle">
                                </div>
                                <div id="NewPassword_info" hidden="hidden">
                                    <h4>Password must meet the following requirements:</h4>
                                    <ul>
                                        <li id="letter" class="invalid">At least <strong>one letter</strong></li>
                                        <li id="capital" class="invalid">At least <strong>one capital letter</strong></li>
                                        <li id="number" class="invalid">At least <strong>2 numbers</strong></li>
                                        <li id="special" class="invalid">At least <strong>2 special characters</strong></li>
                                        <li id="length" class="invalid">Be at least <strong>8 characters</strong></li>
                                    </ul>
                                </div>

                                <div class="Centered">
                                    <asp:ChangePassword ID="UserChangePasswordController" runat="server"
                                        CancelDestinationPageUrl="/sitecore/default.aspx"
                                        DisplayUserName="true"
                                        ContinueDestinationPageUrl="/sitecore/default.aspx"
                                        CssClass="change-password-control"
                                        ChangePasswordFailureText="Password incorrect or New Password invalid. <br/>Please note that historical passwords cannot be used.<br/>Try again with different password."
                                        NewPasswordRegularExpressionErrorMessage="[Uppercase,Lowercase,Number,SpecialCharacter]">
                                        <CancelButtonStyle CssClass="cancel-button" />
                                        <ChangePasswordButtonStyle CssClass="submit-button" />
                                        <ChangePasswordTemplate>
                                            <div class="col-md-6 sup-login-layout-outer">
                                                <div class="sup-login-layout">
                                                    <div class="sup-login-layout">
                                                        <div class="sup-login-layout-inner">
                                                            <div class="col-md-12">
                                                                <div class="col-md-12">
                                                                    <div class="login-header">
                                                                        <img src="/Resources/images/LBLogo-home.png" alt="logo" title="" class="img-responsive">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="input-set">
                                                                    <div class="col-md-12">
                                                                        <h2 class="text-center">Password Reset Page</h2>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">User Name:</asp:Label>
                                                                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName" ErrorMessage="User Name is required." ForeColor="Red" ToolTip="User Name is required." ValidationGroup="UserChangePasswordController"><b>Required Field</b></asp:RequiredFieldValidator>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <asp:TextBox ID="UserName" runat="server" CssClass="textbox" ReadOnly="true"></asp:TextBox>

                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <asp:Label ID="CurrentPasswordLabel" runat="server" AssociatedControlID="CurrentPassword">Current Password<span style="color:red">*</span> :</asp:Label>
                                                                        <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPassword" ErrorMessage="Password is required." ForeColor="Red" ToolTip="Password is required." ValidationGroup="UserChangePasswordController"><b>Required Field</b></asp:RequiredFieldValidator>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <asp:TextBox ID="CurrentPassword" runat="server" CssClass="textbox" TextMode="Password"></asp:TextBox>

                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <asp:Label ID="NewPasswordLabel" runat="server" AssociatedControlID="NewPassword">New Password<span style="color:red">*</span> :</asp:Label>
                                                                        <span class="glyphicon glyphicon-info-sign info" id="pass_info"></span>
                                                                        <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPassword" ErrorMessage="New Password is required." ForeColor="Red" ToolTip="New Password is required." ValidationGroup="UserChangePasswordController"><b>Required Field</b></asp:RequiredFieldValidator>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <div data-tip="This is the text of the tooltip2">
                                                                            <asp:TextBox ID="NewPassword" runat="server" CssClass="textbox" TextMode="Password"></asp:TextBox>
                                                                        </div>
                                                                        <%--<span class="span-space-detail"><b>[Uppercase,Lowercase,Number,SpecialCharacter]</b></span>--%>
                                                                    </div>

                                                                    <div class="col-md-12">
                                                                        <asp:Label ID="ConfirmNewPasswordLabel" runat="server" AssociatedControlID="ConfirmNewPassword">Confirm New Password<span style="color:red">*</span> :</asp:Label>
                                                                        <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword" ErrorMessage="Confirm New Password is required." ForeColor="Red" ToolTip="Confirm New Password is required." ValidationGroup="UserChangePasswordController"><b>Required Field</b></asp:RequiredFieldValidator>
                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <asp:TextBox ID="ConfirmNewPassword" runat="server" CssClass="textbox" TextMode="Password"></asp:TextBox>

                                                                    </div>
                                                                    <div class="col-md-12">
                                                                        <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword" ControlToValidate="ConfirmNewPassword" Display="Dynamic" ErrorMessage="The Confirm New Password must match the New Password entry." ForeColor="Red" ValidationGroup="UserChangePasswordController"></asp:CompareValidator>
                                                                    </div>
                                                                    <div class="col-md-12 erro-msg">
                                                                        <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                                                    </div>
                                                                    <div class="sup-btn-top">
                                                                        <div class="col-md-12">
                                                                            <asp:Button ID="ChangePasswordPushButton" runat="server" CommandName="ChangePassword" CssClass="submit-button" class="sup-btn" Text="Change Password" ValidationGroup="UserChangePasswordController" />
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <asp:Button ID="CancelPushButton" runat="server" CausesValidation="False" CommandName="Cancel" CssClass="cancel-button" Text="Cancel" class="sup-btn" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </ChangePasswordTemplate>
                                        <ContinueButtonStyle CssClass="submit-button" />
                                        <TitleTextStyle CssClass="titleText" />
                                        <PasswordHintStyle CssClass="password-hint" ForeColor="Yellow" />
                                        <InstructionTextStyle CssClass="instruction-text" ForeColor="White" />
                                        <SuccessTemplate>
                                            <div class="sucess-msg">
                                                <div class="sucess-msg-inner">
                                                    <div class="col-md-12">
                                                        <span>Change Password Complete</span>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <span>Your password has been changed!</span>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <asp:Button ID="Button1" runat="server" CausesValidation="False" CommandName="Continue" CssClass="submit-button" Text="Continue" />
                                                    </div>
                                                </div>
                                            </div>
                                        </SuccessTemplate>
                                        <SuccessTextStyle ForeColor="White" />
                                        <LabelStyle CssClass="label" />
                                        <TextBoxStyle CssClass="textbox" />
                                        <FailureTextStyle CssClass="erro-msg" />
                                        <ValidatorTextStyle CssClass="erro-msg" />
                                    </asp:ChangePassword>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script>
        $("#pass_info").hover(function () {
            $("div[id$=NewPassword_info]").show();
        });

        $("input[id$=NewPassword]").mouseout(function () {
            $("div[id$=NewPassword_info]").hide();
        });

    </script>
</body>
</html>

