﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.SupplierPortal.Reports.Filters
{
    /// <summary>
    /// Arrival report filters used by all three arrival report items
    /// </summary>
    public class ArrivalReportFilter
    {
        /// <summary>
        /// Booking reference no
        /// </summary>
        public string BookingReference { get; set; }
        /// <summary>
        /// Client name
        /// </summary>
        public string ClientName { get; set; }
        /// <summary>
        /// Date from
        /// </summary>
        public DateTime? DateFrom { get; set; }
        /// <summary>
        /// Offer Id
        /// </summary>
        public int? OfferId { get; set; }
        /// <summary>
        /// Date to
        /// </summary>
        public DateTime? DateTo { get; set; }
        /// <summary>
        /// Provider
        /// </summary>
        public int? Provider { get; set; }
        /// <summary>
        /// Report Order By
        /// </summary>
        public string ReportOrderBy { get; set; }
        /// <summary>
        /// Report Order By
        /// </summary>
        public string BookingStatus { get; set; }
        
    }
}

