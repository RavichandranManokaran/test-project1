﻿using System;

namespace Affinion.LoyaltyBuild.Api.Search.Data
{
    /// <summary>
    /// Properties for SearchLocation
    /// </summary>
    [Serializable]
    public class SearchLocation : IEquatable<SearchLocation>
    {
        /// <summary>
        /// Location Name
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Location Type
        /// </summary>
        public LocationType Type { get; set; }

        /// <summary>
        /// SearchLocation ItemId
        /// </summary>
        public string ItemId { get; set; }

        /// <summary>
        /// compare if same
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(SearchLocation other)
        {

            return other != null &&
                Text == other.Text &&
                Type == other.Type &&
                ItemId == other.ItemId;
        }
    }
}
