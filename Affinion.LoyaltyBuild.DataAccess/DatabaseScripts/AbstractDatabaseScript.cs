﻿namespace Affinion.LoyaltyBuild.DataAccess.DatabaseScripts
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public abstract class AbstractDatabaseScript : IAffinionDatabaseScript
    {
        protected List<AffinionScriptData> scripts;

        public AbstractDatabaseScript()
        {
            scripts = new List<AffinionScriptData>();
            this.AddScripts();
        }

        // [Conditional("DEBUG")]
        public virtual bool ExecuteScripts()
        {
            try
            {
                // Run only in debug mode to avoid production issues
                // this should be run only in developer machines 
                if (!System.Web.HttpContext.Current.IsDebuggingEnabled)
                {
                    return false;
                }

                // first run table creation then SPs
                List<AffinionScriptData> sortedList = scripts.Where(s => s.ScriptType == AffinionScriptType.TableCreation).ToList();
                sortedList.AddRange(scripts.Where(s => s.ScriptType == AffinionScriptType.SPCreation));

                foreach (AffinionScriptData scriptObject in sortedList)
                {
                    string scriptName = scriptObject.ScriptName;
                    string script = scriptObject.Script;
                    string scriptType = scriptObject.ScriptType == AffinionScriptType.TableCreation ? "U" : "P";
                    DataConnection connection = new DataConnection(this.GetConnectionStringName());
                    DataObject controlData = new DataObject();

                    // P = sp, U = table
                    controlData.Command = String.Format("SELECT * FROM sys.objects WHERE type = '{0}' AND name = '{1}'", scriptType, scriptName);
                    controlData.CommandType = System.Data.CommandType.Text;

                    controlData = connection.Read(controlData);

                    if (controlData.Exception != null)
                    {
                        break;
                    }

                    if(controlData.Data != null && controlData.Data.Rows.Count > 0)
                    {
                        continue;
                    }

                    controlData.Command = String.Format("exec('{0}')", script.Replace("'", "''"));
                    controlData = connection.Write(controlData);

                    if(controlData.Exception != null)
                    {
                        return false;
                    }
                }

                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public virtual string GetConnectionStringName()
        {
            return "UCommerce";
        }

        /// <summary>
        /// Add scripts to the dictionary object
        /// </summary>
        protected virtual void AddScripts() { }
    }
}
