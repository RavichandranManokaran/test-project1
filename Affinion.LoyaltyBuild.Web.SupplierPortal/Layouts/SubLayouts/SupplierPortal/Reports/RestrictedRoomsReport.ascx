﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RestrictedRoomsReport.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Reports.RestrictedRoomsReport" %>
<%@ Register TagPrefix="ajaxToolkit" Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<script language="javascript" type="text/javascript">
function DateDifference(sender, args) {
        var startDate = Date.parse(document.getElementById('tbStartDate').value);
        var endDate = Date.parse(document.getElementById('tbEndDate').value);
        var timeDiff = endDate - startDate;

        daysDiff = Math.floor(timeDiff / (1000 * 60 * 60 * 24));

        if (daysDiff > 366) {
            args.IsValid = false;
        }
        else {
            args.IsValid = true;
        }
    }
</script>

<style type="text/css">

     #maincontent_0_rvShowRecord_ctl09
       {

           max-height:350px;

       }     


</style>

<div class="supplier-report">
    <div>
        <h3 class="title-text font-weight-bold"><sc:Text id="RestrictedTitle" runat="server" /></h3>
    </div>
    <div>
        <sc:Text id="AvailabityReportInformation" runat="server" />
        <asp:Label ID="lblshow" runat="server" ></asp:Label>
    </div>
    <div class="col-xs-12">
        <asp:ValidationSummary ID="valsum" CssClass="alert alert-danger validation-summary" runat="server" ShowValidationErrors="true" DisplayMode="BulletList" ShowSummary="true" />
    </div>
    <asp:UpdatePanel ID="uplFilters" runat="server">
        <ContentTemplate>
            <ajaxtoolkit:calendarextender id="dateFromCalendarExtender" runat="server" animated="false"
                targetcontrolid="txtDatefrom"
                popupposition="BottomLeft"
                format="dd/MM/yyyy"
                popupbuttonid="btnDateFrom" />
            <ajaxtoolkit:calendarextender id="dateToCalendarExtender" runat="server" animated="false"
                targetcontrolid="txtDateTo"
                popupposition="BottomLeft"
                format="dd/MM/yyyy"
                popupbuttonid="btnDateTo" />
            <div class="row report-filter">
                <div class="col-xs-10">
                    <div class="row">
                        <div class="col-xs-2">
                              <sc:Text id="datefromText" runat="server" />
                            <asp:Label ID="lbldatefrom" runat="server"  AssociatedControlID="txtdatefrom"></asp:Label><span class="required">*</span>
                        </div>
                        <div class="col-xs-3">
                            <div class="input-group">
                                <asp:RequiredFieldValidator ID="datefromRequiredFieldValidator" ErrorMessage="From date is required" CssClass="highligh-error" ControlToValidate="txtDatefrom" runat="server" />
                                <asp:TextBox CssClass="form-control" ID="txtDatefrom" runat="server"></asp:TextBox>

                                <div class="input-group-addon">
                                    <asp:LinkButton ID="btnDateFrom" runat="server" CssClass="calendar glyphicon glyphicon-calendar" />
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-2" runat="server" id="providerLabel">
                            <label></label><sc:Text id="ProviderText" runat="server" /><span class="required">*</span>
                        </div>
                        <div class="col-xs-3" runat="server" id="providerDropDown">                            
                            <asp:RequiredFieldValidator ID="rqvalProvider" ErrorMessage="Provider is required" CssClass="highligh-error" InitialValue="0" ControlToValidate="ddlProvider" runat="server" />
                            <asp:DropDownList CssClass="form-control" ID="ddlProvider" AutoPostBack="true" runat="server" AppendDataBoundItems="false">
                                <asp:ListItem Selected="True" Text="-Select-" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                             <sc:Text id="dateToText" runat="server" />
                            <asp:Label ID="labdateto" runat="server"  AssociatedControlID="txtdateto"></asp:Label><span class="required">*</span>
                        </div>
                        <div class="col-xs-3">
                            <div class="input-group">
                                <asp:CompareValidator ID="cmpVal2" ControlToCompare="txtDatefrom" CssClass="highligh-error"
                                    ControlToValidate="txtDateTo" Type="Date" Operator="GreaterThanEqual"
                                    ErrorMessage="Start date should be greater than or equal to end date" runat="server"></asp:CompareValidator>
                                <asp:RequiredFieldValidator ID="datetoRequiredFieldValidator" ErrorMessage="To date is required" CssClass="highligh-error" ControlToValidate="txtDateTo" runat="server" />
                                <asp:CustomValidator ID="cusValDateTo" ErrorMessage="Report can only be viewed for 12 months at a time" CssClass="highligh-error" runat="server" ControlToValidate="txtDateTo" Display="None"></asp:CustomValidator>
                                <asp:TextBox CssClass="form-control" ID="txtDateto" runat="server"></asp:TextBox>

                                <div class="input-group-addon">
                                    <asp:LinkButton ID="btnDateTo" runat="server" CssClass="calendar glyphicon glyphicon-calendar" />
                                </div>
                            </div>
                            
                        </div>
                        <div class="col-xs-3">
                            <button ID="btnDisplay" runat="server" OnClick="Display_Click" Text="Submit" CssClass="btn btn-default">
                                 <sc:Text id="SubmitText" runat="server" />
                            </button>
                        </div>
                    </div>
                </div>
            </div>

        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnDisplay" />
        </Triggers>
    </asp:UpdatePanel>
    <div>
        <rsweb:reportviewer id="rvDisplayrecord" runat="server" width="100%" asyncrendering="false"></rsweb:reportviewer>
    </div>
</div>
