﻿using Newtonsoft.Json;
using System;
using System.Runtime.Serialization;
using System.Web;
using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Data.Items;
using System.Collections.Specialized;
using System.Collections.Generic;

namespace Affinion.LoyaltyBuild.Api.Search.Data
{
    /// <summary>
    /// Implementation for ISerializable interface
    /// </summary>
    [Serializable]
    public class SearchKey : IEquatable<SearchKey>
    {

        /// <summary>
        /// Constructor
        /// </summary>
        /// <returns></returns>
        public SearchKey(HttpContext context)
        {
            if (context != null && context.Request != null)
            {
                var client = context.Request.Cookies["_clientid"];

                //get the client id from cookie or root item
                if (client != null)
                    LoadData(context, client.Value);
                else
                {
                    var item = SitecoreFieldsHelper.GetLookupFieldTargetItem(ItemHelper.RootItem, "ClientDetails");
                    if (item != null)
                        LoadData(context, item.ID.ToString());
                }
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <returns></returns>
        public SearchKey(HttpContext context, string clientID)
        {
            LoadData(context, clientID);
        }

        /// <summary>
        /// load data
        /// </summary>
        /// <returns></returns>
        public void LoadData(HttpContext context, string clientID)
        {
            if (context != null && context.Request != null)
            {
                var queryString = context.Request.QueryString;

                this.ClientId = clientID;
                
                //set location
                if (queryString["Destination"] != null)
                {
                    this.Location = new SearchLocation();
                    var type = LocationType.FreeText;
                    Enum.TryParse(queryString["DestinationType"], out type);
                    this.Location.Type = type;
                    this.Location.ItemId = queryString["DestinationId"];
                    this.Location.Text = queryString["Destination"];
                }

                //set checkin date
                if (queryString["CheckinDate"] != null)
                {
                    string myDate = queryString["CheckinDate"];
                    string strDate = myDate.Substring(6, 4) + "/" + myDate.Substring(3, 2) + "/" + myDate.Substring(0, 2);
                    this.CheckInDate = string.IsNullOrEmpty(strDate) ? (DateTime?)null : DateTime.Parse(strDate);
                }

                //Week
                if (queryString["Week"] != null)
                {
                    int tempVal;
                    if (Int32.TryParse(queryString["Week"], out tempVal) && tempVal > 0)
                        this.Week = tempVal;
                }

                //set checkout date
                if (queryString["CheckoutDate"] != null)
                {
                    string myDate = queryString["CheckoutDate"];
                    string strDate = myDate.Substring(6, 4) + "/" + myDate.Substring(3, 2) + "/" + myDate.Substring(0, 2);
                    this.CheckOutDate = string.IsNullOrEmpty(strDate) ? (DateTime?)null : DateTime.Parse(strDate);
                }

                //set no of adults
                if (queryString["NoOfAdults"] != null)
                {
                    int tempVal;
                    if (Int32.TryParse(queryString["NoOfAdults"], out tempVal) && tempVal > 0)
                        this.NoOfAdults = tempVal;
                }

                //set no of children
                if (queryString["NoOfChildren"] != null)
                {
                    int tempVal;
                    if (Int32.TryParse(queryString["NoOfChildren"], out tempVal) && tempVal > 0)
                        this.NoOfChildren = tempVal;
                }

                //set no of room
                if (queryString["NoOfRooms"] != null)
                {
                    this.NoOfRooms = Convert.ToInt32(queryString["NoOfRooms"]);
                }

                //set no of nights
                if (queryString["NoOfNights"] != null)
                {
                    this.NoOfNights = Convert.ToInt32(queryString["NoOfNights"]);
                }

                //set flexible date
                if (queryString["FlexibleDates"] != null)
                {
                    this.FlexibleDates = Convert.ToBoolean(queryString["FlexibleDates"]);
                    if (this.FlexibleDates)
                    {
                        this.CheckInDate = null;
                        this.CheckOutDate = null;
                    }
                }

                //set child age
                if (queryString["ChildAge"] != null)
                {
                    this.ChildAge = Convert.ToString(queryString["ChildAge"]);
                }

                //set Themes
                if (queryString["Themes"] != null)
                {
                    this.Themes = Convert.ToString(queryString["Themes"]);
                }

                //set Facilities
                if (queryString["Facilities"] != null)
                {
                    this.Facilities = Convert.ToString(queryString["Facilities"]);
                }

                //set Experiences
                if (queryString["Experiences"] != null)
                {
                    this.Experience = Convert.ToString(queryString["Experiences"]);
                }

                //set StarRanking
                if (queryString["StarRanking"] != null)
                {
                    this.StarRanking = Convert.ToString(queryString["StarRanking"]);
                }

                //set LocalRanking
                if (queryString["LocalRanking"] != null)
                {
                    this.LocalRanking = Convert.ToString(queryString["LocalRanking"]);
                }

                if (queryString["mode"] != null)
                {
                    var mode = BookingMode.Normal;
                    Enum.TryParse<BookingMode>(queryString["mode"], out mode);
                    this.Mode = mode;
                }

                if (queryString["olid"] != null)
                {
                    this.OldOrderLineId = Convert.ToString(queryString["olid"]);
                }

                if (queryString["ClientList"] != null)
                {
                    this.ClientList = Convert.ToString(queryString["ClientList"]);
                }
                
            }
        }


        #region Public Properties

        /// <summary>
        /// Client Id
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        ///  Search Location
        /// </summary>
        public SearchLocation Location { get; set; }

        /// <summary>
        ///  No Of Children
        /// </summary>
        public int? NoOfChildren { get; set; }

        /// <summary>
        /// No Of Adults
        /// </summary>
        public int? NoOfAdults { get; set; }

        /// <summary>
        /// CheckIn date
        /// </summary>
        public DateTime? CheckInDate { get; set; }

        /// <summary>
        /// Week current searched
        /// </summary>
        public int Week { get; set; }
        
        /// <summary>
        /// CheckOut Date
        /// </summary>
        public DateTime? CheckOutDate { get; set; }

        /// <summary>
        ///  No Of Rooms
        /// </summary>
        public int NoOfRooms { get; set; }

        /// <summary>
        ///  No Of Nights
        /// </summary>
        public int NoOfNights { get; set; }
        
        /// <summary>
        /// Age of all the children in comma seperated values
        /// </summary>
        public string ChildAge { get; set; }

        /// <summary>
        ///  Flexible Dates
        /// </summary>
        public bool FlexibleDates { get; set; }

        /// <summary>
        /// Gets or sets the themes filter value
        /// </summary>
        public string Themes { get; set; }

        /// <summary>
        /// Gets or sets the Facilities filter value
        /// </summary>
        public string Facilities { get; set; }

        /// <summary>
        /// Gets or sets the Experience filter value
        /// </summary>
        public string Experience { get; set; }

        /// <summary>
        /// Gets or sets the StarRanking filter value
        /// </summary>
        public string StarRanking { get; set; }

        /// <summary>
        /// Gets or sets the LocalRanking filter value
        /// </summary>
        public string LocalRanking { get; set; }

        /// <summary>
        /// Transfer Booking
        /// </summary>
        public BookingMode Mode { get; set; }

        /// <summary>
        /// Order line to transfer
        /// </summary>
        public string OldOrderLineId { get; set; }

        public string ClientList { get; set; }


        #endregion

        /// <summary>
        /// Create querystring from search 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string CreateSearchString()
        {
            //Create aa query string object here based on the properties
            var values = new List<string>();
            
            //build data
            if(this.Location != null)
            {
                values.Add(string.Format("{0}={1}","Destination", this.Location.Text));
                values.Add(string.Format("{0}={1}","DestinationType", (int)this.Location.Type));
                values.Add(string.Format("{0}={1}","DestinationId", this.Location.ItemId));
            }
            if(this.CheckInDate != null)
            {
                values.Add(string.Format("{0}={1}","CheckinDate", this.CheckInDate.Value.ToString("dd/MM/yyyy")));
            }
            if (this.Week != 0)
            {
                values.Add(string.Format("{0}={1}", "Week", this.Week));
            }
            if(this.CheckOutDate != null)
            {
                values.Add(string.Format("{0}={1}", "CheckoutDate", this.CheckOutDate.Value.ToString("dd/MM/yyyy")));
            }
            if(this.NoOfAdults > 0)
            {
                values.Add(string.Format("{0}={1}","NoOfAdults", this.NoOfAdults.ToString()));
            }
            if(this.NoOfChildren > 0)
            {
                values.Add(string.Format("{0}={1}","NoOfChildren", this.NoOfChildren.ToString()));
            }
            if(this.NoOfRooms > 0)
            {
                values.Add(string.Format("{0}={1}","NoOfRooms", this.NoOfRooms.ToString()));
            }
            if(this.NoOfNights > 0)
            {
                values.Add(string.Format("{0}={1}","NoOfNights", this.NoOfNights.ToString()));
            }
            if(this.FlexibleDates)
            {
                values.Add(string.Format("{0}={1}","FlexibleDates", this.FlexibleDates.ToString()));
            }
            if(this.ChildAge != null)
            {
                values.Add(string.Format("{0}={1}","ChildAge", this.ChildAge));
            }
            if (this.Themes != null)
            {
                values.Add(string.Format("{0}={1}", "Themes", this.Themes));
            }
            if (this.Facilities != null)
            {
                values.Add(string.Format("{0}={1}", "Facilities", this.Facilities));
            }
            if (this.StarRanking != null)
            {
                values.Add(string.Format("{0}={1}", "StarRanking", this.StarRanking));
            }
            if (this.LocalRanking != null)
            {
                values.Add(string.Format("{0}={1}", "LocalRanking", this.LocalRanking));
            }

            if (this.Experience != null)
            {
                values.Add(string.Format("{0}={1}", "Experiences", this.Experience));
            }

            if (this.Mode != BookingMode.Normal)
            {
                values.Add(string.Format("{0}={1}", "mode", this.Mode.ToString()));
            }

            if (this.OldOrderLineId != null)
            {
                values.Add(string.Format("{0}={1}", "olid", this.OldOrderLineId));
            }

            if (this.ClientList != null)
            {
                values.Add(string.Format("{0}={1}", "ClientList", this.ClientList));
            }

            return string.Join("&", values);
        }
        
        /// <summary>
        /// Compares the searchkeys
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(SearchKey other)
        {
            return other != null && ClientId.Equals(other.ClientId) &&
                NoOfChildren == other.NoOfChildren &&
                NoOfAdults == other.NoOfAdults &&
                CheckInDate == other.CheckInDate &&
                Week == other.Week &&
                CheckOutDate == other.CheckOutDate &&
                Location.Equals(other.Location)&& Themes== other.Themes
                && LocalRanking==other.LocalRanking
                && Experience==other.Experience
                &&StarRanking==other.StarRanking;
        }

        public bool IsValid 
        {
            get
            {
                return !string.IsNullOrEmpty(ClientId)
                    && Location != null;
            }
        }
    }

    public enum BookingMode
    {
        Normal,
        Edit,
        Transfer
    }
}
