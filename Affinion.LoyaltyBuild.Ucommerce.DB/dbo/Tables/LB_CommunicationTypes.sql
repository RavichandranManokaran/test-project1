﻿CREATE TABLE [dbo].[LB_CommunicationTypes]
(
	[CommunicationTypeId] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Name] NVARCHAR(50) NULL
)
