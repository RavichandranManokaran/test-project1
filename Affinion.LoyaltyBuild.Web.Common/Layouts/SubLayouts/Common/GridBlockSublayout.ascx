﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GridBlockSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.Common.Layouts.SubLayouts.Common.GridBlockSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Register TagPrefix="wc" Namespace="Affinion.LoyaltyBuild.Web.Common.WebControls" Assembly="Affinion.LoyaltyBuild.Web.Common" %>

<p>Grid content goes here</p>
<%--<sc:Placeholder ID="Content" Key="content" runat="server" />--%>
<wc:DynamicKeyPlaceholder Key="content" runat="server" />