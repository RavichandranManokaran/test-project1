﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalSearchResultsSublayoutNew.ascx.cs" Inherits="Affinion.Loyaltybuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalSearchResultsSublayoutNew" %>
<%@ Register Assembly="Sitecore.Kernel" Namespace="Sitecore.Web.UI.WebControls" TagPrefix="sc" %>
<%@ Import Namespace="Affinion.LoyaltyBuild.Model.Provider" %>
<%@ Import Namespace="Affinion.LoyaltyBuild.Model.Product" %>
<%@ Import Namespace="Affinion.LoyaltyBuild.Common.Utilities" %>
<style type="text/css">
    .bedBanks {
        background-color: rgba(203, 124, 3, 0.65) !important;
    }

        .bedBanks .fa-star {
            color: #ffffff !important;
        }
</style>
<div class="col-md-12">
    <div class="search-result-top">
        <p>
            <asp:Literal ID="litSearchText" runat="server" />
        </p>
    </div>
</div>
<div class="col-md-12">
    <div class="search-nav no-margin" id="divSort" runat="server" visible="false">
        <ul class="nav nav-tabs" id="divSearchSort">
            <li class="sort-by">
                <sc:text field="SortBy" id="SortByLabel" runat="server" />
            </li>
            <li role="presentation" class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <sc:text field="SortByPriceLabel" runat="server" />
                <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="<%=BasePageUrl %>&<%=GetSearchText(Affinion.LoyaltyBuild.Api.Search.Data.SortField.Price, Affinion.LoyaltyBuild.Api.Search.Data.SortDirection.Ascending) %>">
                        <sc:text field="PriceLowestToHighest" id="PriceLowestToHighestLabel" runat="server" />
                    </a></li>
                    <li><a href="<%=BasePageUrl %>&<%=GetSearchText(Affinion.LoyaltyBuild.Api.Search.Data.SortField.Price, Affinion.LoyaltyBuild.Api.Search.Data.SortDirection.Descending) %>">
                        <sc:text field="PriceHighestToLowest" id="PriceHighestToLowestLabel" runat="server" />
                    </a></li>
                </ul>
            </li>
            <li role="presentation" class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <sc:text field="SortByRateLabel" runat="server" />
                <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="<%=BasePageUrl %>&<%=GetSearchText(Affinion.LoyaltyBuild.Api.Search.Data.SortField.Rate, Affinion.LoyaltyBuild.Api.Search.Data.SortDirection.Ascending) %>">
                        <sc:text field="StarsLowestToHighest" id="StarsLowestToHighestLabel" runat="server" />
                    </a></li>
                    <li><a href="<%=BasePageUrl %>&<%=GetSearchText(Affinion.LoyaltyBuild.Api.Search.Data.SortField.Rate, Affinion.LoyaltyBuild.Api.Search.Data.SortDirection.Descending) %>">
                        <sc:text field="StarsHighestToLowest" id="StarsHighestToLowestLabel" runat="server" />
                    </a></li>
                </ul>
            </li>
            <li role="presentation" class="dropdown"><a href="<%=BasePageUrl %>&<%=GetSearchText(Affinion.LoyaltyBuild.Api.Search.Data.SortField.Recommended, Affinion.LoyaltyBuild.Api.Search.Data.SortDirection.Descending) %>" role="button" aria-haspopup="true" aria-expanded="false">
                <sc:text field="SortByRecommendLabel" runat="server" />
            </a></li>
            <li role="presentation" class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <sc:text field="SortByHotelNameLabel" runat="server" />
                <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="<%=BasePageUrl %>&<%=GetSearchText(Affinion.LoyaltyBuild.Api.Search.Data.SortField.Name, Affinion.LoyaltyBuild.Api.Search.Data.SortDirection.Ascending) %>">
                        <sc:text field="SupplierNameAscendingOrder" id="SupplierNameAscendingOrderLabel" runat="server" />
                    </a></li>
                    <li><a href="<%=BasePageUrl %>&<%=GetSearchText(Affinion.LoyaltyBuild.Api.Search.Data.SortField.Name, Affinion.LoyaltyBuild.Api.Search.Data.SortDirection.Descending) %>">
                        <sc:text field="SupplierNameDescendingOrder" id="SupplierNameDescendingOrderLabel" runat="server" />
                    </a></li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="col-md-12 no-padding  margin-row" runat="server" id="divPreviousNext" visible="false" clientidmode="Static">
        <div class="no-padding-left col-md-6 padding-top-1">
            <a class="btn btn-secondry booking-margin-top pull-left btn-bg" href="<%=PreviousWeekUrl %>" id="lnkPreviousWeek">
                <sc:text field="PreviousWeek" id="scPrevWeek" runat="server" />
            </a>
        </div>
        <div class="no-padding-right col-md-6">
            <a class="btn btn-secondry booking-margin-top pull-right  btn-bg" href="<%=NextWeekUrl %>" id="lnkNextWeek">
                <sc:text field="NextWeek" id="scNextWeek" runat="server" />
            </a>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="content-body">
        <div class="accordian-outer">
            <div class="panel-group tbl-accordion" id="accordion">
                <asp:Repeater ID="rptProviders" runat="server" EnableViewState="false" OnItemDataBound="ProviderItemDataBound">
                    <ItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading bg-primary <%#(((IProvider)Container.DataItem).ProviderType != ProviderType.BedBanks)?"loyaltyBuild":"bedBanks"%>">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse<%#Container.ItemIndex %>" aria-expanded="true" class="">
                                    <div class="star-rank pull-left width-full">
                                        <asp:Repeater ID="rptStar" runat="server"
                                            DataSource="<%#System.Linq.Enumerable.Range(1, ((IGenericProvider)Container.DataItem).StarRanking) %>">
                                            <ItemTemplate>
                                                <span class="fa fa-star"></span>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <span class="panel-title">
                                            <span data-toggle="collapse" data-parent="#accordion" href="#collapse<%#Container.ItemIndex %>" class="show-hide-list" aria-expanded="true">
                                                <%#Eval("Name") %>
                                            </span>
                                        </span>
                                    </div>
                                </a>
                            </div>
                            <div id="collapse<%#Container.ItemIndex %>" class="panel-collapse collapse in" aria-expanded="true">
                                <div class="panel-body">
                                    <div>
                                        <div class="row cs-search-result-bg1">
                                            <span class="padding-right-5"><%#Eval("Address") %></span>
                                            <span class="pull-right">
                                                <asp:Repeater ID="rptFacilities" runat="server" DataSource="<%#((IGenericProvider)Container.DataItem).Facilities%>">
                                                    <ItemTemplate>
                                                        <sc:link field="Link" id="FacilityLink" runat="server" item="<%#(Sitecore.Data.Items.Item)Container.DataItem %>">
                                                            <sc:Image Field="Icon" ID="FacilityImage" runat="server" Item="<%#(Sitecore.Data.Items.Item)Container.DataItem %>" />
                                                        </sc:link>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </span>
                                        </div>

                                        <button type="button" class="btn btn-primary booking-margin-top float-right btn-bg" data-toggle="modal" onclick="OpenPopup(this)" data-supplierid='<%#Eval("SitecoreItemId")%>,<%#Eval("ProviderId") %>' data-target="#info-page">Get Provider Details</button>

                                        <div class="panel-group margin-row" id="accordion_sub" runat="server" visible="<%#IsDateSelected%>">
                                            <div class="panel panel-default">
                                                <div class="panel-heading panel_sub_heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion_sub" href="#collapse<%#Container.ItemIndex %>_sub_room" class="show-hide-list width-full inline-block" aria-expanded="true">Rooms and Packages</a>
                                                    </h4>
                                                </div>
                                                <div id="collapse<%#Container.ItemIndex %>_sub_room" class="panel-collapse collapse in" aria-expanded="true">
                                                    <div class="panel-body">
                                                        <div class="detail-outer">
                                                            <div class="row cs-search-result-bg2 ">
                                                                <span class="accent50-f font-3 font-bold-600 ">Rooms and Packages</span> <span></span>
                                                            </div>
                                                            <div></div>
                                                            <div class="table-responsive no-margin RoomsandPackage">
                                                                <table class="table table-striped no-margin tbl-calendar font-5" data-hotelname="<%#Eval("Name") %>">
                                                                    <thead>
                                                                        <tr class="">
                                                                            <th class="text-center font-6 ">Occupancy Type<span class="accent34-f">&nbsp;</span></th>
                                                                            <asp:Repeater ID="rptDateHeader" runat="server" DataSource="<%#GetDates()%>">
                                                                                <ItemTemplate>
                                                                                    <th class="text-center font-6"><%#((DateTime)Container.DataItem).ToString("ddd") %> <span class="accent34-f"><%#((DateTime)Container.DataItem).ToString("dd/MM") %></span></th>
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <asp:Repeater ID="rptRooms" runat="server">
                                                                            <ItemTemplate>
                                                                                <tr>
                                                                                    <td class="text-left accent34-f nowrap"><%#Eval("Name") %></td>
                                                                                    <asp:Repeater ID="rptProduct" runat="server" DataSource="<%#GetPriceByDate((IProduct)Container.DataItem)%>" OnItemDataBound="PriceItemDataBound">
                                                                                        <ItemTemplate>
                                                                                            <td class="text-center availability-item <%#GetDateClass((Affinion.LoyaltyBuild.Web.ClientPortal.ViewModels.PriceAvailabilityByDate)Container.DataItem) %>">
                                                                                                <span id="spnNoItem" runat="server">N/A</span>
                                                                                                <span id="spnPastItem" runat="server"><b><%#Eval("Availability") %></b> (<%#((decimal)Eval("Price")).FormatCurrency((int)Eval("CurrencyId")) %>)</span>
                                                                                                <span id="spnFutureItem" runat="server" data-toggle="modal" data-target="#myModal"
                                                                                                    data-sku='<%#Eval("Sku")%>'
                                                                                                    data-variatsku='<%#Eval("VariantSku")%>'
                                                                                                    data-price='<%#Eval("Price")%>'
                                                                                                    data-currencyid='<%#Eval("CurrencyId")%>'
                                                                                                    data-availability='<%#Eval("Availability")%>'
                                                                                                    data-date='<%#Eval("Date")%>'
                                                                                                    data-checkoutdate='<%#((DateTime)Eval("Date")).AddDays((int)Eval("NoOfNights"))%>'
                                                                                                    data-noofnights='<%#Eval("NoOfNights")%>'
                                                                                                    data-roomtype='<%#Eval("Type") %>'
                                                                                                    data-noofadults='<%#Eval("NoOfAdults")%>'
                                                                                                    data-noofchildren='<%#Eval("NoOfChildren")%>'
                                                                                                    onclick="LoadAvailabiltyForCheckout(this);"><b><span><%#Eval("Availability") %></span></b> (<%#((decimal)Eval("Price")).FormatCurrency((int)Eval("CurrencyId")) %>)</span>
                                                                                            </td>
                                                                                        </ItemTemplate>
                                                                                    </asp:Repeater>
                                                                                </tr>
                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div runat="server" class="promotioncontainer" id="divPromotions" visible="<%#(((IProvider)Container.DataItem).ProviderType != ProviderType.BedBanks) %>"
                                                data-variants="<%#GetVaraintSkus(((IProvider)Container.DataItem).Products)%>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                <div class="col-xs-12 no-padding">
                    <div runat="server" id="divPager" visible="false" clientidmode="static">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md add-basket-popup">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><asp:literal runat="server"  id="baketText" Text="Add to Basket"></asp:literal></h4>
            </div>
            <div class="modal-body">
                <div class="col-xs-12">
                    <div class="col-xs-12">
                        <div class="col-xs-3">
                            <label class="control-label" for="lg">Quantity:<sc:text field="Noofroom" id="NoofroomLabel" runat="server" /></label>
                        </div>
                        <div class="col-xs-9">
                            <select id="ddlNoOfRooms" class="form-control midle-content-margin-top margin-bottom-small"></select>
                        </div>
                    </div>
                    <div class="col-xs-12 martop10">
                        <div class="col-xs-4">
                            <button type="button" id="btnCancel" data-dismiss="modal" class="btn-co1 pop_paddng">
                                <sc:text field="Cancel" id="Text1" runat="server" />
                            </button>
                        </div>
                        <div class="col-xs-4 no-padding-left" id="divAddToBasket" runat="server">
                            <button type="button" id="btnAddToCart" data-dismiss="modal" class="btn-co1 pop_paddng" onclick="CS.BasketService.AddToBasket(null)">
                                <sc:text field="AddtoBasket" id="Text2" runat="server" />
                            </button>
                        </div>
                        <div class="col-xs-4 no-padding-left">
                            <button type="button" id="btnCheckout" data-dismiss="modal" class="btn-co1 pop_paddng" onclick="CS.BasketService.AddToBasket('<%=(QueryString("mode").ToLower()=="transfer"?"transfer":"checkout")%>')">
                                <sc:text field="Checkout" id="scCheckOut" runat="server" />
                            </button>
                        </div>
                    </div>
                    <asp:HiddenField ID="hdnClientID" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hdnOldOrderLineID" runat="server" ClientIDMode="Static" />
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>

    </div>
</div>
<div class="modal fade" id="promotionModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="promotion-title">Add To Basket</h4>
            </div>
            <div class="modal-body">
                <div class="promotion-info">
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $("#<%=divPager.ClientID%>").pagination({
            hrefTextPrefix: "<%=BasePageUrl%>&<%=(string.IsNullOrEmpty(SortText)?"":(SortText + "&"))%>page=",
            items: <%=ResultSize.ToString()%>,
            itemsOnPage: <%=PageSize.ToString()%>,
            currentPage: <%=CurrentPage.ToString()%>,
            cssStyle: 'compact-theme'
        });

        <%if(CheckInDate != null){%>
        $(".promotioncontainer").each(function(){
            var container = $(this);
            var variants = $(this).data("variants");
            $.get("/handlers/offeravailabilityandprice.ashx?VariantSku=" + variants + "&CheckInDate=<%=CheckInDate.Value.ToString("yyyy-MM-dd")%>&NoOfNoghts=<%=NoOfNights.ToString()%>&Currency=<%=CurrencyId.ToString()%>&Week=<%=QueryString("week")%>",
                function(data) {
                    container.html(data);
                });
        });
        <%}%>
    });
      
    function LoadAvailabiltyForCheckout(obj)
    {
        CS.BasketService.CreateBasketItem(obj);
        
        var ddl = $('#ddlNoOfRooms');
        ddl.find('option')
            .remove()
            .end();
        var avail = $(obj).data("availability");
        $("")
        var mode = '<%=QueryString("mode").ToLower()%>'
        if(mode === 'transfer')
            avail = 1;
        
        for (var n = 1; n <= (avail < 10 ? avail : 10); n++){
            ddl.append($('<option>', {
                value: n,
                text: n
            }))
        }
        var noOfRooms = $("#NoOfRooms").val();
        if(avail < noOfRooms)
            noOfRooms = avail;

        ddl.find("option[value=" + noOfRooms + "]").attr("selected", "selected");
    }
    
    /* Load promtion Info*/
    function LoadPromoInfo(e)
    { 
        CS.BasketService.CreateBasketItem(obj);
        
        var avail = spn.data("availability");
        var mode = '<%=QueryString("mode").ToLower()%>'
        if(mode === 'transfer')
            avail = 1;

        $.ajax({
            type: "GET",
            url: "/handlers/ApplicablePromotionCheck.ashx",
            dataType: "text",
            data: {
                "sku": $(e).data("sku"),
                "variantsku": $(e).data("variatsku"),
                "promotionid":$(e).data("promotionid"), 
                "checkoutdate": $(e).data("checkoutdate"),
                "availability": avail,
                "date": $(e).data("date"),
                "noofnights": $(e).data("noofnights"),
                "roomtype": $(e).data("roomtype"),
                "noofadults": $(e).data("noofadults"),
                "noofchildren": $(e).data("noofchildren"),
                "currencyid":$(e).data("currencyid")
            },
            success: function (response) {
                $(".promotion-info").html(response);
                $('#promotionModal').modal('show');
            },
            error: function (response) {
                CS.Common.ShowAlert('Error processing request');
            }
        });
    }

    function OpenPopup(obj) {
        if (obj) {
            var supplierid = obj.getAttribute('data-supplierid').split(',');
            if(supplierid[0]!=null && supplierid[0].length>0 )
            {
                window.open("/Layouts/ProviderDetails.aspx?supplierid=" + supplierid[0], "List", "toolbar=no, location=no,status=yes,menubar=no,scrollbars=yes,resizable=no, width=1000,height=500,left=200,top=100");
                return false;
            }
            else
            {
                window.open("/Layouts/BBProviderDetails.aspx?supplierid=" + supplierid[1], "List", "toolbar=no, location=no,status=yes,menubar=no,scrollbars=yes,resizable=no, width=1000,height=500,left=200,top=100");
                return false;
            }
        }
    }

    $(document).ready(function () {
        cur_window_width = $(".RoomsandPackage").width();
        full_width = cur_window_width;
        $('.RoomsandPackage').css({
            'width': full_width,
        });
        $('.RoomsandPackage').scrollLeft(cur_window_width/2);
    }); 
</script>

