﻿using Sitecore.Data;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Affinion.LoyaltyBuild.AdminPortal.Extensions
{
    public static class ItemExt
    {
        /// <summary>
        /// True if the template associated with the item
        /// or any of its base templates
        /// is the template specified by ID.
        /// </summary>
        /// <param name="me">The item for which to check
        /// the template and base templates.</param>
        /// <param name="templateID">The ID of the template
        /// for which to check.</param>
        /// <returns>
        /// True if the template associated with the item
        /// or any of its base templates
        /// is the template specified by ID.
        /// </returns>
        public static bool HasTemplateOrBaseTemplate(
          this Item me,
          ID templateID)
        {
            return me.Template.HasBaseTemplate(
              templateID,
              true /*includeSelf*/,
              true /*recursive*/);
        }
    }

    /// <summary>
    /// Extension methods for Sitecore.Data.Items.TemplateItem.
    /// </summary>
    public static class TemplateItemExt
    {
        /// <summary>
        /// True if the template has the base template.
        /// </summary>
        /// <param name="me">The template for which to check
        /// the template and base templates.</param>
        /// <param name="templateID">The ID of the template
        /// for which to check.</param>
        /// <param name="includeSelf">Whether to check the
        /// template itself (false by default).</param>
        /// <param name="recursive">Whether to check base
        /// templates for base templates (true by default).</param>
        /// <returns>
        /// True if the template has the base template.
        /// </returns>
        public static bool HasBaseTemplate(
          this TemplateItem me,
          ID templateID,
          bool includeSelf = false,
          bool recursive = true)
        {
            if (includeSelf && me.ID == templateID)
            {
                return true;
            }

            if (recursive)
            {
                foreach (TemplateItem baseTemplate in me.BaseTemplates)
                {
                    if (baseTemplate.HasBaseTemplate(
                      templateID,
                      true /*includeSelf*/,
                      true /*recursive*/))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }

    public static class ItemAxesExt
    {
        /// <summary>
        /// Iterate the item and its ancestors associated with
        /// a data template or where that template inherits from
        /// one or more specified data templates.
        /// </summary>
        /// <param name="me">The object associated with the item
        /// for which to check the template and base templates.</param>
        /// <param name="templates">The list of templates for
        /// which to check.</param>
        /// <param name="includeSelf">Whether to check the item (true)
        /// or only its ancestors (false by default).</param>
        /// <param name="includeAncestors">Whether to include ancestors
        /// of the item (true by default) or not (false).</param>
        /// <returns>
        /// Iterator of the item and its ancestors that are associated with
        /// a data template or where that template inherits from
        /// one or more specified data templates.
        /// </returns>
        public static IEnumerable<Item> GetAncestorsWithTemplates(
          this ItemAxes me,
          TemplateItem[] templates,
          bool includeSelf = false,
          bool includeAncestors = true)
        {
            Item[] ancestors = me.SelectItems("ancestor-or-self::*");
            Item item = ancestors[0];
            foreach (TemplateItem templateItem in templates)
            {
                if (includeSelf)
                {
                    if (item.HasTemplateOrBaseTemplate(templateItem.ID))
                    {
                        yield return item;
                    }
                }

                if (!includeAncestors || item.Parent == null)
                {
                    continue;
                }

                foreach (Item ancestor in GetAncestorsWithTemplates(
                  item.Parent.Axes,
                  templates,
                  true /*includeSelf*/,
                  true /*includeAncestors*/))
                {
                    yield return ancestor;
                }
            }
        }
    }
}

