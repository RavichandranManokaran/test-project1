﻿CREATE TABLE [dbo].[History] (
    [Id]             UNIQUEIDENTIFIER CONSTRAINT [DF_History_Id] DEFAULT (newid()) NOT NULL,
    [Category]       VARCHAR (50)     NOT NULL,
    [Action]         VARCHAR (50)     NOT NULL,
    [ItemId]         UNIQUEIDENTIFIER NOT NULL,
    [ItemLanguage]   VARCHAR (50)     NOT NULL,
    [ItemVersion]    INT              NOT NULL,
    [ItemPath]       NVARCHAR (MAX)   CONSTRAINT [DF_History_ItemPath] DEFAULT ('') NOT NULL,
    [UserName]       NVARCHAR (250)   NOT NULL,
    [TaskStack]      VARCHAR (MAX)    CONSTRAINT [DF_History_TaskStack] DEFAULT ('') NOT NULL,
    [AdditionalInfo] NVARCHAR (MAX)   CONSTRAINT [DF_History_AdditionalInfo] DEFAULT ('') NOT NULL,
    [Created]        DATETIME         NOT NULL,
    [DAC_Index]      INT              NULL
);


GO
CREATE CLUSTERED INDEX [DAC_Clustered]
    ON [dbo].[History]([DAC_Index] ASC);


GO
CREATE NONCLUSTERED INDEX [ndxCreated]
    ON [dbo].[History]([Created] ASC);

