﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditCampaignItemInviteSupplier.ascx.cs" Inherits="Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.Ucommerce.Marketing.EditCampaignItemInviteSupplier" %>
<link rel="stylesheet" href="/Resources/styles/bootstrap.min.css" />
<link rel="stylesheet" href="/Resources/styles/affinion.min.css" />
<link href="/Resources/styles/affinion-admin.min.css" rel="stylesheet" />
<link href="/Resources/styles/font-awesome.min.css" rel="stylesheet" />
<link href="/Resources/styles/supplier.css" rel="stylesheet" />
<script src="/Resources/Scripts/CampaignItem.js"></script>
<style>
    .firstcol {
        width: 80%;
        padding: 0;
    }

    .width100 {
        width: 100% !important;
        /*float: left;
    background: #f0f0f0;
    font-family: Arial,sans-serif;
    font-size: 14px;
    color: #000;
    border: 1px solid #cccccc;
    padding-bottom: 20px;*/
    }

    .radio input[type="radio"], .radio-inline input[type="radio"], .checkbox input[type="checkbox"], .checkbox-inline input[type="checkbox"] {
        position: absolute;
        margin-top: 4px;
        margin-left: 0px;
    }

    .spanbtn {
        height: 30px;
        margin-left: -2px;
        margin-top: 0px;
        background-color: rgb(227, 227, 227);
    }



    .scrolltbl tbody, table thead {
        display: block;
    }

    .scrolltbl tbody {
        height: 300px;
        overflow-y: auto;
        overflow-x: hidden;
    }

    .scrolltbl {
        width: 350px; /* can be dynamic */
    }

        .scrolltbl tr {
            width: 100% !important;
            display: table;
        }
        .h4Margin {margin-left:15px;
    }
        .searchtxt
        {margin-left: 18px; width: 95%;}
        .searchtxt-invite{margin-left: 12px; width: 95%;}
         .searchtxt-Subscribe{margin-left: 12px; width: 95%;}
</style>

<div class="col-md-12">
    <div id="dvApprovePanel" runat="server"  ClientIDMode="Static">
        <asp:Button ID="btnInviteForApproval" runat="server" CssClass="btn btn_save"  Text="Send Invite For Approval" OnClick="btnInviteForApproval_Click"></asp:Button>        
        <asp:Button ID="btnApproval" runat="server" CssClass="btn btn_save" Text="Approve Campaign" OnClick="btnApproval_Click"></asp:Button>        
         <asp:Button ID="btnReject" runat="server" CssClass="btn btn_save" Text="Reject Campaign" OnClick="btnReject_Click" ></asp:Button>        
    </div>
    <div id="dvsupplierpanel" runat="server"  ClientIDMode="Static" >
    <div class="panel panel-default" style="border: none;">

        <div class="rate_bg width100">
            <h3>Supplier Invite</h3>
            <div class="col-xs-12 ">
                <h4>Search</h4>
            </div>

            <div class="col-xs-12">
                <div class="col-xs-12 supplier-box">
                    <div class="col-xs-12 no-padding">
                        <div class="col-xs-3">
                            <div class="col-xs-3 no-padding">
                                <span class="supply-name">Country</span>
                            </div>
                            <div class="col-xs-8 no-padding-right">
                                <asp:HiddenField ID="hdQuery" runat="Server" ClientIDMode="Static" />
                                <asp:DropDownList ID="drpCountry" placeholder="Select Country" runat="server" AutoPostBack="false" Style="width: 100%; height: 30px;">
                                </asp:DropDownList>

                            </div>
                            <div class="col-xs-1 no-padding">
                                <a href="javascript:void(0)" id="btnCountry" onclick="CheckValue('<%= drpCountry.ClientID %>','UlCountry');"><span class="supply-icon btn glyphicon glyphicon-plus-sign spanbtn"></span></a>
                            </div>
                        </div>
                        <div class="col-xs-3 no-padding-left">
                            <div class="col-xs-3 no-padding">
                                <span class="supply-name">Region</span>
                            </div>
                            <div class="col-xs-8 no-padding-right">
                                <asp:DropDownList ID="drpRegion" runat="server" AutoPostBack="false" Style="width: 100%; height: 30px;">
                                </asp:DropDownList>
                            </div>
                            <div class="col-xs-1 no-padding">
                                <a href="javascript:void(0)" id="btnRegion" onclick="CheckValue('<%= drpRegion.ClientID %>','UIRegion');"><span class="supply-icon btn glyphicon glyphicon-plus-sign spanbtn"></span></a>
                            </div>
                        </div>
                        <div class="col-xs-3 no-padding-left">
                            <div class="col-xs-3 no-padding">
                                <span class="supply-name">Location</span>
                            </div>
                            <div class="col-xs-8 no-padding-right">
                                <asp:DropDownList ID="drpLocation" runat="server" AutoPostBack="false" Style="width: 100%; height: 30px;">
                                </asp:DropDownList>
                            </div>
                            <div class="col-xs-1 no-padding">
                                <a href="javascript:void(0)" id="btnLocation" onclick="CheckValue('<%= drpLocation.ClientID %>','UILocation');"><span class="supply-icon btn glyphicon glyphicon-plus-sign spanbtn"></span></a>
                            </div>
                        </div>
                        <div class="col-xs-3 no-padding-left">
                            <div class="col-xs-4 no-padding">
                                <span class="supply-name">Hotel Type</span>
                            </div>
                            <div class="col-xs-7 no-padding">
                                <asp:DropDownList ID="drpHotelType" runat="server" AutoPostBack="false" Style="width: 100%; height: 30px;">
                                </asp:DropDownList>
                            </div>
                            <div class="col-xs-1 no-padding">
                                <a href="javascript:void(0)" id="btnHotelType" onclick="CheckValue('<%= drpHotelType.ClientID %>','UIHotelType');"><span class="supply-icon btn glyphicon glyphicon-plus-sign spanbtn"></span></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 no-padding margin-top12">
                        <div class="col-xs-3">
                            <div class="col-xs-3 no-padding">
                                <span class="supply-name">Theme</span>
                            </div>
                            <div class="col-xs-8 no-padding-right">
                                <asp:DropDownList ID="drpTheme" runat="server" AutoPostBack="false" Style="width: 100%; height: 30px;">
                                </asp:DropDownList>
                            </div>
                            <div class="col-xs-1 no-padding">
                                <a href="javascript:void(0)" id="btnTheme" onclick="CheckValue('<%= drpTheme.ClientID %>','UITheme');"><span class="supply-icon btn glyphicon glyphicon-plus-sign spanbtn"></span></a>
                            </div>
                        </div>
                        <div class="col-xs-3 no-padding-left">
                            <div class="col-xs-3 no-padding">
                                <span class="supply-name">Group</span>
                            </div>
                            <div class="col-xs-8 no-padding-right">
                                <asp:DropDownList ID="drpGroup" runat="server" AutoPostBack="false" Style="width: 100%; height: 30px;">
                                </asp:DropDownList>
                            </div>
                            <div class="col-xs-1 no-padding">
                                <a href="javascript:void(0)" id="btnGroup" onclick="CheckValue('<%= drpGroup.ClientID %>','UIGroup');"><span class="supply-icon btn glyphicon glyphicon-plus-sign spanbtn"></span></a>
                            </div>
                        </div>
                        <div class="col-xs-3 no-padding-left">
                            <div class="col-xs-3 no-padding">
                                <span class="supply-name">Experience</span>
                            </div>
                            <div class="col-xs-8 no-padding-right">
                                <asp:DropDownList ID="drpExeperience" runat="server" AutoPostBack="false" Style="width: 100%; height: 30px;">
                                </asp:DropDownList>
                            </div>
                            <div class="col-xs-1 no-padding">
                                <a href="javascript:void(0)" id="btnExperience" onclick="CheckValue('<%= drpExeperience.ClientID %>','UIExperience');"><span class="supply-icon btn glyphicon glyphicon-plus-sign spanbtn"></span></a>
                            </div>
                        </div>
                        <div class="col-xs-3 no-padding-left">
                            <div class="col-xs-4 no-padding">
                                <span class="supply-name">Hotel Rating</span>
                            </div>
                            <div class="col-xs-7 no-padding">
                                <asp:DropDownList ID="drpHotelRating" runat="server" AutoPostBack="false" Style="width: 100%; height: 30px;">
                                </asp:DropDownList>
                            </div>
                            <div class="col-xs-1 no-padding">
                                <a href="javascript:void(0)" id="btnHotelRating" onclick="CheckValue('<%= drpHotelRating.ClientID %>','UIRating');"><span class="supply-icon btn glyphicon glyphicon-plus-sign spanbtn"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 ">
                <h4>Invitation Criteria</h4>
            </div>
            <div class="col-xs-12">
                <div class="col-xs-12 supplier-box">
                    <div class="col-xs-12 no-padding">
                        <div class="col-xs-5">
                            <div class="col-xs-3 no-padding-left">
                                <h4>Country</h4>
                                <div>
                                    <ul class="list-group" id="UlCountry">
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xs-3 no-padding-left">
                                <h4>Region</h4>
                                <div>
                                    <ul class="list-group" id="UIRegion">
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xs-3 no-padding-left">
                                <h4>Location</h4>
                                <div>
                                    <ul class="list-group" id="UILocation">
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xs-3 no-padding">
                                <h4>Hotel Type</h4>
                                <div>
                                    <ul class="list-group" id="UIHotelType">
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-5 no-padding-left">
                            <div class="col-xs-3 no-padding-left">
                                <h4>Theme</h4>
                                <div>
                                    <ul class="list-group" id="UITheme">
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xs-3 no-padding-left">
                                <h4>Group</h4>
                                <div>
                                    <ul class="list-group" id="UIGroup">
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xs-3 no-padding-left">
                                <h4>Experience</h4>
                                <div>
                                    <ul class="list-group" id="UIExperience">
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xs-3 no-padding">
                                <h4>Rating</h4>
                                <div>
                                    <ul class="list-group" id="UIRating">
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-1 no-padding-left">
                            <h4>&nbsp;</h4>
                            <asp:Button ID="btn_Filter" Style="margin-left: -7px;" runat="server" CssClass="btn btn_save" OnClick="btn_Filter_Click" OnClientClick="SetJsonData()" Text="Filter Supplier"></asp:Button>
                            <%--<a class="btn btn_save" id="btn_save" type="button">Submit</a>--%>
                        </div>
                    </div>
                </div>
            </div>
              <div class="col-xs-12">
                  <asp:Label ID="lblError"  runat="server" ></asp:Label>
              </div>
            <div class="col-xs-12">
                <div class="col-xs-4 no-padding">
                   <h4 class="h4Margin">Search Results</h4>
                      <input type="text" class="form-control searchtxt" id="filterTable-input" placeholder="Search" />
                    <asp:ListView ID="listSupplier" runat="server"
                        GroupPlaceholderID="groupPlaceHolder1" ItemPlaceholderID="itemPlaceHolder1">
                        <LayoutTemplate>
                            <table aria-describedby="orderTable_info" id="tblserachsupplier" class="cl_tabl scrolltbl" border="0" style="" width="100%" cellspacing="0">
                                <thead class="subrate clrate_thead">
                                    <%--<tr>
                                        <td>
                                            <input type="text" id="txtlistsupplier" placeholder="Search " />
                                            <input type="button" value="Search" onclick="return filterresult()" />
                                        </td>
                                    </tr>--%>
                                    <tr id="Tr2" runat="server" style="">

                                        <th class="firstcol" id="Th3" runat="server" colspan="2">Supplier Name</th>
                                    </tr>
                                </thead>
                                <tbody id="itemPlaceholder1" runat="server">
                                </tbody>
                            </table>

                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td class="firstcol">
                                    <asp:HiddenField ID="hdId" runat="server" Value='<%# Eval("Sku") %>' />
                                    <asp:Label ID="SupplierName" runat="server" Text='<%# Eval("Name") %>' />
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkSelected" runat="server" CssClass="chk_Supplier" />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>



                    <%-- <asp:ListView ID="listSupplier" runat="server"
                        GroupPlaceholderID="groupPlaceHolder1" ItemPlaceholderID="itemPlaceHolder1" OnPagePropertiesChanging="listSupplier_PagePropertiesChanging">
                        <LayoutTemplate>
                            <table  aria-describedby="orderTable_info" class="cl_tabl" border="0" style="" width="100%" cellspacing="0">
                                <thead class="subrate clrate_thead">
                                    <tr id="Tr2" runat="server" style="">

                                        <th id="Th3" style="width: 80%;" runat="server">Supplier Name</th>
                                        <th id="Th1" runat="server"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:PlaceHolder runat="server" ID="groupPlaceHolder1"></asp:PlaceHolder>
                                    <tr class="nobackground">
                                        <td colspan="2">
                                            <asp:DataPager ID="DataPager1" runat="server" PagedControlID="listSupplier" PageSize="10">
                                                <Fields>
                                                    <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="false" ShowPreviousPageButton="true"
                                                        ShowNextPageButton="false" />
                                                    <asp:NumericPagerField ButtonType="Link" />
                                                    <asp:NextPreviousPagerField ButtonType="Link" ShowNextPageButton="true" ShowLastPageButton="false" ShowPreviousPageButton="false" />
                                                </Fields>
                                            </asp:DataPager>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                        </LayoutTemplate>
                        <GroupTemplate>
                            <tr>
                                <asp:PlaceHolder runat="server" ID="itemPlaceHolder1"></asp:PlaceHolder>
                            </tr>
                        </GroupTemplate>
                        <ItemTemplate>
                            <td>
                                <asp:HiddenField ID="hdId" runat="server" Value='<%# Eval("Guid") %>' />
                                <asp:Label ID="SupplierName" runat="server" Text='<%# Eval("Name") %>' />
                            </td>
                            <td>
                                <asp:CheckBox ID="chkSelected" runat="server" CssClass="chk_Supplier" />
                            </td>
                        </ItemTemplate>
                    </asp:ListView>--%>
                </div>
                <div class="col-xs-2">
                    <h4>&nbsp;</h4>
                    <div class="checkbox">
                        <asp:CheckBox ID="chkSubscibe" runat="server" Text="Auto Subscription" />
                    </div>
                    <div class="checkbox">
                        <asp:CheckBox ID="chkInviteSelected" runat="server" Text="Invite Selected" name="invitetype"  ClientIDMode="Static"/>
                    </div>
                    <div class="checkbox">
                        <asp:CheckBox ID="chkInviteAll" runat="server" Text="Invite All"  name="invitetype"  ClientIDMode="Static" />
                    </div>
                    <div>
                        <asp:Button ID="btnInviteSupplier" runat="server" CssClass="btn btn_save" OnClick="btnInviteSupplier_Click" Text="Invite Supplier" OnClientClick="return ValidateSelecton()"   ></asp:Button>
                    </div>
                </div>
                <div class="col-xs-3 no-padding">
                    <h4 class="h4Margin">Invited Supplier</h4>
                     <input type="text" class="form-control searchtxt-invite" id="filterTable-InviteSupplier" placeholder="Search" />
                    <asp:ListView ID="lvSupplier" runat="server" OnItemDeleting="lvSupplier_ItemDeleting">
                        <LayoutTemplate>
                            <table aria-describedby="orderTable_info" class="cl_tabl scrolltbl" id="tblInvitesupplier" border="0" style="" width="100%" cellspacing="0">
                                <thead class="subrate clrate_thead">
                                    <tr id="Tr2" runat="server" style="">

                                        <th id="Th3" runat="server">Supplier Name</th>
                                    </tr>
                                </thead>
                                <tbody id="itemPlaceholder" runat="server">
                                </tbody>
                            </table>

                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td class="firstcol">
                                    <asp:Label ID="SupplierInviteId" Visible="false" runat="server" Text='<%# Eval("SupplierInviteId") %>' />
                                    <asp:Label ID="SupplierName" runat="server" Text='<%# Eval("Name") %>' />
                                </td>
                                <td>
                                    <asp:LinkButton ID="DeleteButton" CssClass="pull-right btn btn-default glyphicon glyphicon-remove-sign red" runat="server" meta:resourcekey="Delete" CommandName="Delete" /><br />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </div>
                <div class="col-xs-3 no-padding-right">
                     <h4 class="h4Margin">Subscribe supplier</h4>
                    <input type="text" class="form-control searchtxt-Subscribe" id="filterTable-Subscribe" placeholder="Search" />
                    <asp:ListView ID="IVSuppliersubscribe" runat="server" OnItemDeleting="IVSuppliersubscribe_ItemDeleting">
                        <LayoutTemplate>
                            <table aria-describedby="orderTable_info"  id="tblsearchSubscribe" class="cl_tabl scrolltbl" border="0" style="" width="100%" cellspacing="0">
                                <thead class="subrate clrate_thead">
                                    <tr id="Tr2" runat="server" style="">

                                        <th id="Th3" runat="server">Supplier Name</th>
                                    </tr>
                                </thead>
                                <tbody id="itemPlaceholder" runat="server">
                                </tbody>
                            </table>

                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td class="firstcol">
                                    <asp:Label ID="SupplierInviteId" Visible="false" runat="server" Text='<%# Eval("SupplierInviteId") %>' />
                                    <asp:Label ID="SupplierName" runat="server" Text='<%# Eval("Name") %>' />
                                </td>
                                <td>
                                    <asp:LinkButton ID="DeleteButton" CssClass="pull-right btn btn-default glyphicon glyphicon-remove-sign red" runat="server" meta:resourcekey="Delete" CommandName="Delete" /><br />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>

                </div>
            </div>
        </div>
    </div>
        </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        DisplayJSONData();
        setsearch();
    });
    function ValidateSelecton() {
        var isValid = false;
        if ($("#chkInviteAll").is(":checked")) {
            isValid = true;
        }
        else if ($("#chkInviteSelected").is(":checked")) {
            isValid = true;
        }
        if (!isValid)
        {
            alert("Atleast select one option Invite All / Invite Selected" );
        }
        return isValid;
    }
</script>

<%--<asp:Panel runat="server" ID="PropertyPanel">
    <table width="100%">
        <td width="125">
            <asp:DropDownList ID="drpRegion" runat="server" AutoPostBack="false"></asp:DropDownList>
        </td>
        <td width="125">
            <asp:DropDownList ID="drpCity" runat="server" AutoPostBack="false"></asp:DropDownList>
        </td>
        <td width="125">
            <asp:DropDownList ID="drpHotelType" runat="server" AutoPostBack="false"></asp:DropDownList>
        </td>
        <td width="125">
            <asp:DropDownList ID="drpHotelRating" runat="server" AutoPostBack="false"></asp:DropDownList>
        </td>
        <td width="125">
            <asp:DropDownList ID="drpTheme" runat="server" AutoPostBack="false"></asp:DropDownList>
        </td>
        <td width="125">
            <asp:DropDownList ID="drpExeperience" runat="server" AutoPostBack="false"></asp:DropDownList>
        </td>
        <td width="125">
            <asp:DropDownList ID="drpGroup" runat="server" AutoPostBack="false"></asp:DropDownList>
        </td>

        <td width="125">
            <asp:Button ID="btnFilter" runat="server" OnClick="btnFilter_Click" Text="Filter Supplier"></asp:Button>
        </td>
        <td style="display: none">
            <asp:DropDownList ID="drpRoomType" runat="server" AutoPostBack="false" Visible="false"></asp:DropDownList>
        </td>
        <td style="display: none">
            <asp:DropDownList ID="drpAddon" runat="server" AutoPostBack="false" Visible="false"></asp:DropDownList>
        </td>

    </table>
    <table>
        <td style="vertical-align: top">
            <asp:Label ID="lblNoSupplier" Text="No Supplier Found" runat="server"></asp:Label>
            <asp:ListBox
                ID="listSupplier"
                Rows="20"
                runat="server" SelectionMode="Multiple" />
            <asp:CheckBox ID="chkSubscibe" runat="server" Text="Auto subscription for supplier" Visible="false" />
            <asp:Button ID="btnSubscribe" runat="server" Text="Subscribe" OnClick="Subscribe_Onclick" Visible="false" />
            <asp:Button ID="btnAllSubscibe" runat="server" Text="Subscribe All" OnClick="SubscribeAll_Onclick" Visible="false" />

        </td>
        <td style="vertical-align: top">
            <asp:ListView ID="lvSupplier" runat="server" OnItemDeleting="lvSupplier_ItemDeleting">
                <LayoutTemplate>
                    <table id="Table1" runat="server">
                        <tr id="Tr1" runat="server">
                            <td id="Td1" runat="server">
                                <table id="itemPlaceholderContainer" runat="server" border="0" style="">
                                    <tr id="Tr2" runat="server" style="">
                                        <th id="Th1" runat="server"></th>
                                        <th id="Th3" runat="server">Supplier Name</th>
                                    </tr>
                                    <tr id="itemPlaceholder" runat="server">
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="Tr3" runat="server">
                            <td id="Td2" runat="server" style=""></td>
                        </tr>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr style="">
                        <td>
                            <asp:ImageButton ID="DeleteButton" runat="server" ImageUrl="../Images/ui/cross.png" meta:resourcekey="Delete" CommandName="Delete" /><br />
                        </td>
                        <td>
                            <asp:Label ID="SupplierInviteId" Visible="false" runat="server" Text='<%# Eval("SupplierInviteId") %>' />
                            <asp:Label ID="SupplierName" runat="server" Text='<%# Eval("Name") %>' />
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </td>
    </table>

</asp:Panel>--%>
