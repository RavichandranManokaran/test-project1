﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Api.Booking.Data.Basket
{
    /// <summary>
    /// Add to basket request
    /// </summary>
    public interface IAddToBasketRequest
    {
        /// <summary>
        /// CurrencyID Property
        /// </summary>
        int CurrencyId { get; set; }

        /// <summary>
        /// Sku
        /// </summary>
        string Sku { get; set; }

        /// <summary>
        /// Variant Sku
        /// </summary>
        string VariantSku { get; set; }

        /// <summary>
        /// NoOfRooms Property
        /// </summary>
        int NoOfRooms { get; set; }

        /// <summary>
        /// Client Id
        /// </summary>
        string ClientId { get; set; }

        /// <summary>
        /// Created By
        /// </summary>
        string CreatedBy { get; set; }
    }
}
