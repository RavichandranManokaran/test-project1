﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           StoreHelper.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common.Utilities
/// Description:           Store helper
/// </summary>


namespace Affinion.LoyaltyBuild.Search.Helpers
{
    #region Using directives
    using Affinion.LoyaltyBuild.Common.Exceptions;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Search.Data;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Availability;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
    using Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Linq;
    using UCommerce.Api;
    using UCommerce.EntitiesV2;
    #endregion

    public static class StoreHelper
    {
        /// <summary>
        /// Get supplier's products if it is associated with the current client
        /// </summary>
        /// <param name="supplier">Sitecore supplier item</param>
        /// <returns>Returns supplier products</returns>
        public static SupplierData GetProducts(Item supplier)
        {
            if (supplier == null)
            {
                return null;
            }

            Item client = SitecoreFieldsHelper.GetLookupFieldTargetItem(ItemHelper.RootItem, "ClientDetails");

            return GetProducts(supplier, client);
        }

        /// <summary>
        /// Get supplier's products if it is associated with the given client
        /// </summary>
        /// <param name="supplier">Sitecore supplier item</param>
        /// <param name="client">The client to check the supplier relationship</param>
        /// <returns>Returns supplier products</returns>
        public static SupplierData GetProducts(Item supplier, Item client)
        {
            SearchKey searchTerm = new SearchKey();
            searchTerm.CheckinDate = DateTime.Now;
            searchTerm.CheckoutDate = DateTime.Now.AddDays(7);
            searchTerm.Client = client;

            return GetProducts(supplier, client, searchTerm);
        }

        /// <summary>
        /// Get supplier's products if it is associated with the given client
        /// </summary>
        /// <param name="supplier">Sitecore supplier item</param>
        /// <param name="client">The client to check the supplier relationship</param>
        /// <param name="searchTerm">The search term to get offers</param>
        /// <returns>Returns supplier products</returns>
        public static SupplierData GetProducts(Item supplier, Item client, SearchKey searchTerm)
        {
            try
            {
                SupplierData products = new SupplierData();
                string clientName = SitecoreFieldsHelper.GetValue(client, "Name", client.Name);
                string priceGroup = SitecoreFieldsHelper.GetLookupFieldTargetItemValue(client, "PriceGroup", "Title");
                Category hotel = GetUcommerceCategory(supplier);

                if (hotel == null)
                {
                    return null;
                }

                decimal lowestPrice = 0;
                string lowestClientPrice = string.Empty;
                string lowestPriceOccupancyId = string.Empty;

                List<Product> roomProducts = hotel.Products.Where(p => MatchProductToDefinition(p, "hotelroom")).ToList();

                foreach (Product product in roomProducts)
                {
                    OfferDataItem offerItem = new OfferDataItem(hotel, product, priceGroup, supplier, searchTerm);
                    List<OfferDataItem> offers = new List<OfferDataItem>();
                    offers.Add(offerItem);
                    offers.AddRange(offerItem.GetRelatedOffers());

                    foreach (OfferDataItem offer in offers)
                    {
                        if (lowestPrice == 0 || (offer.Price > 0 && offer.Price < lowestPrice))
                        {
                            lowestPrice = offer.Price;
                            lowestClientPrice = offer.GetPriceWithCurrency(offer.Price);
                            lowestPriceOccupancyId = offer.OccupancyTypeId;
                        }

                        //if (offer.Price > 0)
                        //{
                        products.Offers.Add(offer);
                        //}
                    }
                }

                products.ClientId = client.ID.ToString();
                products.ClientName = clientName;
                products.LowestPrice = lowestPrice;
                products.LowestPriceWithCurrency = lowestClientPrice;
                products.LowestPriceOccupancyTypeId = lowestPriceOccupancyId;

                return products;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, typeof(SupplierData));
                return null;
            }
        }

        /// <summary>
        /// Get supplier's addon products
        /// </summary>
        /// <param name="supplier">Sitecore supplier item</param>
        /// <returns>Returns supplier products</returns>
        public static Collection<AddonProduct> GetAddons(Item supplier)
        {
            if (supplier == null)
            {
                return null;
            }

            Item client = SitecoreFieldsHelper.GetLookupFieldTargetItem(ItemHelper.RootItem, "ClientDetails");

            return GetAddons(supplier, client);
        }

        /// <summary>
        /// Get supplier's products if it is associated with the given client
        /// </summary>
        /// <param name="supplier">Sitecore supplier item</param>
        /// <param name="client">The client to check the supplier relationship</param>
        /// <returns>Returns supplier products</returns>
        public static Collection<AddonProduct> GetAddons(Item supplier, Item client)
        {
            try
            {
                Collection<AddonProduct> addonProducts = new Collection<AddonProduct>();
                string clientName = SitecoreFieldsHelper.GetValue(client, "Name", client.Name);
                Category hotel = GetUcommerceCategory(supplier);

                if (hotel == null)
                {
                    return null;
                }

                // List<Product> roomProducts = ProductLibrary.GetAddonProudct(categoryId);
                List<Product> roomProducts = hotel.Products.Where(p => p.ProductDefinition.Name == "Add On").ToList();
                foreach (Product product in roomProducts)
                {
                    AddonProduct addonItem = new AddonProduct();
                    addonItem.Name = product.Name;
                    addonItem.Sku = product.Sku;
                    SearchCriteria criteria = new SearchCriteria();
                    LBPriceCalculation price = LoyaltyBuildCatalogLibrary.CalculatePrice(product, criteria, hotel.ProductCatalog);
                    addonItem.Price = price.YourPrice.Amount.Value;
                    addonProducts.Add(addonItem);
                }

                return addonProducts;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, typeof(SupplierData));
                return null;
            }
        }

        /// <summary>
        /// Gets the ucommerce category ID
        /// </summary>
        /// <param name="item">The supplier item</param>
        /// <returns>Returns the ucommerce id</returns>
        public static int GetUcommerceCategoryId(Item item)
        {
            Category supplierCategory = GetUcommerceCategory(item);

            if (supplierCategory == null)
            {
                return 0;
            }

            return supplierCategory.CategoryId;
        }

        /// <summary>
        /// Gets the ucommerce category
        /// </summary>
        /// <param name="item">The supplier item</param>
        /// <returns>Returns the ucommerce category</returns>
        public static Category GetUcommerceCategory(Item item)
        {
            try
            {
                string categoryId = SitecoreFieldsHelper.GetValue(item, "UCommerceCategoryID");

                if (string.IsNullOrEmpty(categoryId))
                {
                    return null;
                }

                Guid categoryGuid;
                if(!Guid.TryParse(categoryId, out categoryGuid))
                {
                    return null;
                }

                Category supplierCategory = Category.All().Where(x => x.Guid == categoryGuid).FirstOrDefault();
                return supplierCategory;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, typeof(SupplierData));
                return null;
            }
        }


        public static string GetUcommerceProductSku(Item item)
        {
            Product prod = GetUcommerceProduct(item);

            if (prod == null)
            {
                return string.Empty ;
            }

            return prod.Sku;
        }

        /// <summary>
        /// Gets the ucommerce Product
        /// </summary>
        /// <param name="item">The Product item</param>
        /// <returns>Returns the Product item</returns>
        public static Product GetUcommerceProduct(Item item)
        {
            try
            {
                string ProductSku = SitecoreFieldsHelper.GetValue(item, "SKU");
                if (string.IsNullOrEmpty(ProductSku))
                {
                    return null;
                }
                Product supplierProduct = Product.All().Where(x => x.Sku == ProductSku).FirstOrDefault();
                return supplierProduct;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, typeof(SupplierData));
                return null;
            }
        }

        /// <summary>
        /// Get the symbol of the currency
        /// </summary>
        /// <param name="currency">Currency ISO code</param>
        /// <returns>Returns the predefined currency symbol</returns>
        public static string GetCurrencySymbol(string currency)
        {
            string symbol = string.Empty;
            switch (currency)
            {
                case "EUR":
                    symbol = "€";
                    break;
                case "GBP":
                    symbol = "£";
                    break;
                default:
                    symbol = currency;
                    break;
            }
            return symbol;
        }

        /// <summary>
        /// Get the price with currency details
        /// </summary>
        /// <param name="price">The final price (price - discount)</param>
        /// <returns>Returns a formatted string</returns>
        public static string GetPriceWithCurrency(decimal price, CurrencyInfo currencyInfo = null, string currencyCode = null)
        {
            string priceWithCurrency = string.Empty;

            if (currencyInfo == null && string.IsNullOrEmpty(currencyCode))
            {
                CultureInfo culture = CultureInfo.CurrentCulture;
                priceWithCurrency = price.ToString("c", culture);
            }
            else if (currencyInfo == null && !string.IsNullOrEmpty(currencyCode))
            {
                currencyInfo = new CurrencyInfo();
                currencyInfo.UpdateCurrency(currencyCode);
                if (currencyInfo.Errors == null)
                {
                    return currencyInfo.GetPriceWithCurrency(price);
                }
                else
                {
                    return price.ToString("f2");
                }
            }
            else
            {
                return currencyInfo.GetPriceWithCurrency(price);
            }

            return priceWithCurrency;
        }

        /// <summary>
        /// Match the product to the given definition
        /// </summary>
        /// <param name="product">The product</param>
        /// <param name="productDefinitionName">The string name of product definition</param>
        /// <returns>Returns true if matched</returns>
        private static bool MatchProductToDefinition(Product product, string productDefinitionName)
        {
            bool include = product.DisplayOnSite;
            include = include && product.AllowOrdering;
            include = include && product.ProductDefinition.Name.Replace(" ", "").Equals(productDefinitionName, StringComparison.InvariantCultureIgnoreCase);
            return include;
        }
    }
}
