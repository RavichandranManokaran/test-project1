﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HotelDetailsSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.HotelDetailsSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Sitecore.Data.Items" %>
<%@ Import Namespace="Affinion.LoyaltyBuild.Search" %>
<%@ Import Namespace="Affinion.LoyaltyBuild.Search.Data" %>
<%@ Import Namespace="Affinion.LoyaltyBuild.Common.Utilities" %>
<%@ Import Namespace="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts" %>

<script type="text/javascript" src="/Resources/scripts/jssor.slider-20.mini.js"></script>
<script type="text/javascript" src="/Resources/scripts/detail-gallary.js"></script>

<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />

<script>
    jQuery(document).ready(function () {
        jQuery("table tr:even").css({ "background-color": "#fafefe", "border-top": "solid 1px #cff2f6", "border-bottom": "solid 1px #cff2f6" });
        jQuery("table tr:odd").css("background-color", "#fff");
        jQuery("#SelectRoomAlertWrapper").css('display', 'none');
        jQuery("table.room-type input[type='submit']").click(function (e) {

            if (!ValidateAddToCart()) {
                e.preventDefault();
                return false;
            }
        })
    });

    function ValidateAddToCart() {
        if (jQuery("table.room-type select option:selected").length != jQuery("table.room-type select option:selected[value='0']").length) {
            return true;
        }
        jQuery("#SelectRoomAlertWrapper").css('display', 'block');
        return false;
    }



    //jQuery(function () {
    //    $("#check-in-date").datepicker();
    //});
    //jQuery(function () {
    //    jQuery("#check-out-date").datepicker();
    //});
    //$("[data-toggle=popover]").popover({
    //    html: true,
    //    content: function () {
    //        return $('#popover-content').html();
    //    }
    //});
    var clickedListItemId;
    jQuery(document).on('change', 'input[type="checkbox"]', function () {
        var Id = this.value;
        var newListName = jQuery("#txtNewList").val();
        var HotelId = GetParameterValues('id');

        if (HotelId != null && HotelId != "" && Id != "") {
            var CallBackURL = "/Pages/AddToFavorite.aspx";
            if (jQuery('.chkNewList').is(":checked")) {
                return;
            }
            clickedListItemId = "input#" + $(this).attr('id');
            if (jQuery(this).is(":checked")) {
                $(this).after('<div id="loading">Loading...</div>');
                jQuery.ajax({
                    url: CallBackURL,
                    type: 'get',
                    data: { id: Id, hotelid: HotelId },
                    contenttype: "application/json;charset=utf-8",
                    traditional: true,
                    success: function (results) {
                        return false;
                    },
                    complete: function () {
                        jQuery('#loading').remove();
                        $(clickedListItemId).each(function () {
                            $(this).prop("checked", true);
                        });
                    }
                });
            }
            else {
                $(this).after('<div id="loading">Loading...</div>');
                jQuery.ajax({
                    url: CallBackURL,
                    type: 'get',
                    data: { id: Id, hotelid: HotelId },
                    contenttype: "application/json;charset=utf-8",
                    traditional: true,
                    success: function (results) {
                        return false;
                    },
                    complete: function () {
                        jQuery('#loading').remove();
                        $(clickedListItemId).each(function () {
                            $(this).prop("checked", false);
                        });
                    }
                });
            }
        }


    });


    $(document).on('click', '.favoriteWrapper a', function () {
        var aaaa = $(this).attr("aria-describedby");
        if (aaaa != null && aaaa != "") {
            $(".favoriteWrapper .existingLitWrapper input").each(function () {
                if ($(this).is(":checked")) {
                    var localid = "div#" + aaaa + " input#" + $(this).attr("id");
                    $(localid).prop("checked", true);
                }
                else {
                    var localid = "div#" + aaaa + " input#" + $(this).attr("id");
                    $(localid).prop("checked", false);
                }
            });
        }
    });



    jQuery(document).on('change', '.chkNewList', function (event) {
        $(this).after('<div id="loading">Loading...</div>');
        var CallBackURL = "/Pages/AddToFavorite.aspx";
        var varCustomerId = 68;
        var newListName = jQuery(".hiddenListName").val();
        var varHotelId = GetParameterValues('id');
        if (varHotelId != null && varHotelId != "" && jQuery(this).is(":checked")) {
            jQuery.ajax({
                url: CallBackURL,
                type: 'get',
                data: { listName: newListName, CustomerId: varCustomerId, HotelId: varHotelId, delay: 3 },
                contenttype: "application/json;charset=utf-8",
                traditional: true,
                success: function (results) {
                    if (results.indexOf("hidAddToFavoriteSuccess") >= 0) {
                        jQuery('.createdListContentTemp').html(results);
                        var htmlToAppend = jQuery('.createdListContentTemp .newListWrapper').html();
                        jQuery(".existingLitWrapper").append(htmlToAppend);
                        jQuery(this).val("");
                        jQuery('.createdListContentTemp').html("");
                    }
                },
                complete: function () {
                    jQuery('#loading').remove();
                    jQuery('.chkNewList').prop('checked', false);
                    jQuery('.chkNewList').prop('disabled', true);
                }
            });
            return false;
        }
        else {
            return true;
        }
    });

    jQuery(document).on('keyup', '.txtNewList', function (event) {
        $(".hiddenListName").val(this.value);
    });

    jQuery(document).on('keypress', '.txtNewList', function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        var CallBackURL = "/Pages/AddToFavorite.aspx";
        jQuery('.chkNewList').prop('disabled', false);
        var varCustomerId = 68;
        if (keycode == '13') {
            $(this).after('<div id="loading">Loading...</div>');
            var newListName = jQuery(this).val();
            var varHotelId = GetParameterValues('id');
            if (varHotelId != null && varHotelId != "") {
                jQuery.ajax({
                    url: CallBackURL,
                    type: 'get',
                    data: { listName: newListName, CustomerId: varCustomerId, HotelId: varHotelId, delay: 3 },
                    contenttype: "application/json;charset=utf-8",
                    traditional: true,
                    success: function (results) {
                        if (results.indexOf("hidAddToFavoriteSuccess") >= 0) {
                            jQuery('.createdListContentTemp').html(results);
                            var htmlToAppend = jQuery('.createdListContentTemp .newListWrapper').html();
                            jQuery(".existingLitWrapper").append(htmlToAppend);
                            jQuery(this).val("");
                            jQuery('.createdListContentTemp').html("");
                        }
                    },
                    complete: function () {
                        jQuery('#loading').remove();
                        jQuery('.chkNewList').prop('checked', false);
                        jQuery('.chkNewList').prop('disabled', true);

                    }
                });
                return false;
            }
            else {
                return true;

            }
        }
    });

    function GetParameterValues(param) {
        var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < url.length; i++) {
            var urlparam = url[i].split('=');
            if (urlparam[0] == param) {
                return urlparam[1];
            }
        }
    }
</script>
<%--<div class="col-xs-12">
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>We have no availability!</strong> here between 07 Feb 2016 and 14 Feb 2016
Sorry, our last available space at Ballyliffin Hotel was booked 10 hours, 12 minutes ago!
    </div>
</div>--%>

<!-- start of flexible dates grid -->
<asp:PlaceHolder ID="plcLBwrapper" runat="server">
<div class="col-xs-12" id="flexibleDatesContainer" runat="server">

    <div class="col-xs-12 check-avilability alert alert-success accent54-f">
        <div>
            <div class="col-xs-12">
                <span class="display-block-val search-box-margin margin-row accent67-f">
                    <sc:text id="FlexibleDatesSummaryLabel" field="FlexibleDatesSummaryLabel" runat="server" />
                </span>
            </div>
            <div class="col-xs-12">
                <div class="col-md-3"></div>
                <div class="col-md-3">
                    <a href="<%=GetFlexibleDatesUrl(-42).ToString() %>" class="btn btn-green margin-bottom-12 display-block-val"><i class="fa fa-arrow-circle-left"></i>
                        <sc:text id="FlexibleDatesPreviousButtonLabel" field="FlexibleDatesPreviousButtonLabel" runat="server" />
                    </a>
                </div>
                <div class="col-md-3">
                    <a href="<%=GetFlexibleDatesUrl(42).ToString() %>" class="btn btn-green margin-bottom-12 display-block-val">
                        <sc:text id="FlexibleDatesNextButtonLabel" field="FlexibleDatesNextButtonLabel" runat="server" />
                        <i class="fa fa-arrow-circle-right"></i></a>

                </div>
                <div class="col-md-3"></div>
                <div class="next-prew">
                </div>
            </div>
            <div class="col-xs-12">

                <asp:Repeater ID="rptFlexibleDates" runat="server">

                    <ItemTemplate>

                        <%# this.GetFlexibleDatesRowStartHtml((int)Container.ItemIndex) %>

                        <div class="col-xs-12 col-md-1 col-sm-6 no-padding">
                            <div class="panel panel-default availabale-r<%# ((FlexibleDateData)Container.DataItem).Availability ? "" : " opacity5" %>">
                                <div class="panel-heading text-center font-bold accent70-bg"><%#((FlexibleDateData)Container.DataItem).Date.ToString("MMM dd") %></div>
                                <div class="panel-body padding-6-3 min-height-70">
                                    <span class="text-center font-bold-600 pull-left width-full accent70-f"><%#((FlexibleDateData)Container.DataItem).Price %></span>
                                    <span class="pull-left width-full text-center font-size-9 accent68-f<%# ((FlexibleDateData)Container.DataItem).Availability ? " display-non-class" : "" %>">
                                        <sc:text id="NoAvailabilityLabel" field="NoAvailabilityLabel" runat="server" />
                                    </span>

                                    <span class="pull-left width-full text-center<%# ((FlexibleDateData)Container.DataItem).Availability ? "" : " display-non-class" %>">
                                        <input type="radio" name="FlexibleDatesRadio" <%# ((FlexibleDateData)Container.DataItem).Availability && ((FlexibleDateData)Container.DataItem).Date > DateTime.Today ? "" : "disabled='disabled'" %> value="<%#((FlexibleDateData)Container.DataItem).Date.ToString(DateTimeHelper.DateFormat) %>" <%#CheckFlexibleDateRadioButton(((FlexibleDateData)Container.DataItem).Date) %> />

                                    </span>
                                </div>
                            </div>
                        </div>

                        <%# this.GetFlexibleDatesRowEndHtml((int)Container.ItemIndex) %>
                    </ItemTemplate>

                </asp:Repeater>

            </div>

        </div>
    </div>
</div>

<!-- end of flexible dates grid -->


<div class="col-xs-12">
    <div class="col-xs-12 alert alert-warning">
        <div class="col-xs-12 col-md-3">
            <span class="display-block-val search-box-margin">
                <strong>
                    <sc:text id="CheckinLabel" field="CheckinDateLabel" runat="server" />
                </strong>
                <asp:Label ID="CheckingDateLabel" Text="" runat="server"></asp:Label>
            </span>
        </div>
        <div class="col-xs-12 col-md-3">
            <span class="display-block-val search-box-margin">
                <strong>
                    <sc:text id="NightLabel" field="NoOfNightsLabel" runat="server" />
                </strong>
                <asp:Label ID="NumberOfNightsLabel" runat="server" Text=""></asp:Label>
            </span>
        </div>
        <div class="col-xs-12 col-md-3">
        </div>
        <div class="col-xs-12 col-md-3">
            <a href="#avilabillity" class="btn btn-secondry pull-right width-full">
                <sc:text id="ChangeDateLabel" field="Change Date Button" runat="server" />
            </a>
        </div>
    </div>
</div>


<div class="col-xs-12">
    <ul class="info-top-link list-group">
        <li class="display-block-val"><a href="#facilities" class="list-group-item alert-info">
            <sc:text id="FacilitiesLabel" field="FacilitiesLabel" runat="server" />
        </a></li>
        <li class="display-block-val">
            <a href="#GoodToKnow" class="list-group-item alert-info">
                <sc:text id="Text1" field="GoodToKnowLabel" runat="server" />
            </a>
        </li>
        <li class="display-block-val">
            <a href="#CheckInCheckOutTime" class="list-group-item alert-info">
                <sc:text id="Text2" field="CheckInCheckOutTimeLabel" runat="server" />
            </a>
        </li>
        <%--<li class="display-block-val"><a href="#payment" class="list-group-item alert-info">Payment Methods </a></li>
        <li class="display-block-val"><a href="#checkin" class="list-group-item alert-info">Check-In / Check-Out Time</a> </li>--%>
    </ul>

</div>
<div id="hotelDetailsDiv">

    <!-- Example row of columns -->
    <div class="col-md-12 no-padding">
        <div class="col-xs-12 col-md-8">
            <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; height: 456px; overflow: hidden; background-color: #24262e;">
                <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
                    <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
                    <div style="position: absolute; display: block; background: url('imag/loading.gif') no-repeat center center; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
                </div>
                <div id="imageGallery" data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 800px; height: 356px; overflow: hidden;">

                    <asp:Repeater ID="ImageGalleryRepeater" runat="server">
                        <ItemTemplate>
                            <div style="display: none;">
                                <image data-u="image" src="<%# Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl((Sitecore.Data.Items.Item)Container.DataItem)) %>" />
                                <image data-u="thumb" src="<%# Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl((Sitecore.Data.Items.Item)Container.DataItem)) %>" />
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                </div>

                <!-- Thumbnail Navigator -->
                <div data-u="thumbnavigator" class="jssort01" style="position: absolute; left: 0px; bottom: 0px; width: 800px; height: 100px;" data-autocenter="1">
                    <!-- Thumbnail Item Skin Begin -->
                    <div data-u="slides" style="cursor: default;" id="image_bottom">
                        <div data-u="prototype" class="p">
                            <div class="w">
                                <div data-u="thumbnailtemplate" class="t"></div>
                            </div>
                            <div class="c"></div>
                        </div>
                    </div>
                    <!-- Thumbnail Item Skin End -->
                </div>
                <!-- Arrow Navigator -->
                <span data-u="arrowleft" class="jssora05l" style="top: 158px; left: 8px; width: 40px; height: 40px;"></span>
                <span data-u="arrowright" class="jssora05r" style="top: 158px; right: 8px; width: 40px; height: 40px;"></span>
            </div>
        </div>
        <div class="col-xs-12 col-md-4">
            <div class="hotel-search-box-detail">
                <%--<div class="col-md-12">--%>
                <div class="top-bar float-right">
                </div>
                <%--</div>--%>
                <div class="col-xs-12 row">
                    <div class="hotel-name-detail">
                        <h2 class="no-margin pull-left">
                            <sc:text id="HotelName" field="Name" runat="server" />
                        </h2>
                        <div class="star-rank pull-left padding-left-small padding-top-small">
                            <asp:Repeater ID="StarRepeater" runat="server">
                                <ItemTemplate>
                                    <span class="fa fa-star"></span>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        <a href="" id="savelater" runat="server" onserverclick="savelater_ServerClick">
                            <sc:image id="Image1" field="SavedForLaterButton" runat="server" />
                        </a>
                        <div class="favoriteWrapper" style="display: block;">
                            <a data-placement="bottom" data-toggle="popover" data-title="My List" data-container="form" type="button" data-html="true" href="#" id="login">
                                <sc:image id="btnAddToFav" field="AddToFavoriteButton" runat="server" />
                            </a>
                            <div id="popover-content" class="popover-content hide">
                                <div class="existingLitWrapper">
                                    <asp:CheckBoxList ID="chkFavList" runat="server"></asp:CheckBoxList>
                                </div>
                                <div class="NewListCreationWrapper">
                                    <input type="checkbox" id="chkNewList" class="chkNewList" disabled />
                                    <input type="text" id="txtNewList" class="txtNewList" title="newlist" />
                                </div>
                                <div class="createdListContentTemp" style="display: none;"></div>
                            </div>
                        </div>
                        <div class="hotel-name-detail-bottom width-full pull-left">
                           <p id="Hoteladdress" >
                                 <asp:Label ID="fulladdress" runat="server" ></asp:Label>
                               <%-- <p><sc:text id="AddressLine1" field="AddressLine1" runat="server" />,</p>
                                <p><sc:text id="AddressLine2" field="AddressLine2" runat="server" />,</p>
                                <p><sc:text id="AddressLine3" field="AddressLine3" runat="server" />,</p>
                                <p><sc:text id="AddressLine4" field="AddressLine4" runat="server" />,</p>
                                <p><sc:text id="Town" field="Town" runat="server" />,</p>--%>
                                <sc:text id="CountryName" field="CountryName" runat="server" />
                            </p>
                            <p>
                            <p>
                                <a href data-toggle="modal" data-target="#dialog-map"><span class="fa fa-map-marker" title="Show map"></span>
                                    <sc:text id="ShowMapLabel" field="ShowMapLabel" runat="server" />
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
                <%--<div class="travel-rating">
                        <p>
                            <sc:text id="TravellerRatingLabel" field="TripAdvisorTravellerRatingLabel" runat="server" />
                        </p>
                    </div>
                    <div class="travel-rating-icon">
                        <asp:Image ID="hotelRating" runat="server" />
                    </div>
                    <div class="travel-rating-review">
                        <p>
                            <asp:HyperLink ID="noOfReviewsLink" runat="server">
                                <sc:text id="noOfReviews" field="NoOfReviews" runat="server" />
                                <sc:text id="ReviewsLabel" field="ReviewsLabel" runat="server" />
                            </asp:HyperLink>
                        </p>
                    </div>--%>
                <ul class="event">
                    <asp:Repeater ID="FacilitiesRepeater" runat="server">
                        <ItemTemplate>
                            <li>
                                <sc:image field="Icon" item="<%#(Item)Container.DataItem %>" runat="server" />
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </div>


            <div class="content_bg_inner_box alert-info">
                <p>
                    <sc:text id="Overview" field="Overview" runat="server" />
                </p>
                <%--<h2>
                    <sc:text id="ContactDetailsLabel" field="ContactDetailsLabel" runat="server" />
                </h2>--%>
                <%--<div class="col-md-12 no-padding">
                    <div class="col-md-4 no-padding">
                        <p>
                            <span class="glyphicon glyphicon-map-marker"></span>
                            <%--<sc:text id="AddressLabel" field="AddressLabel" runat="server" />--%>
                <%-- </p>
                    </div>
                    <div class="col-md-8 no-padding">--%>

                <%--<p>
                            <sc:text id="AddressLine1" field="AddressLine1" runat="server" />
                            <sc:text id="AddressLine2" field="AddressLine2" runat="server" />
                            <sc:text id="AddressLine3" field="AddressLine3" runat="server" />
                            <sc:text id="AddressLine4" field="AddressLine4" runat="server" />
                            <sc:text id="Town" field="Town" runat="server" />
                            <sc:text id="CountryName" field="CountryName" runat="server" />
                        </p>

                    </div>
                </div>--%>
                <%--<div class="col-md-12 no-padding">
                    <div class="col-md-4 no-padding">
                        <p>
                            <span class="glyphicon glyphicon-earphone"></span>
                            <sc:text id="PhoneLabel" field="PhoneLabel" runat="server" />
                        </p>
                    </div>
                    <div class="col-md-8 no-padding">
                        <p>
                            <sc:text id="Phone" field="Phone" runat="server" />
                        </p>
                    </div>
                </div>
                <div class="col-md-12 no-padding">
                    <div class="col-md-4 no-padding">
                        <p>
                            <span class="glyphicon glyphicon-print"></span>
                            <sc:text id="FaxLabel" field="FaxLabel" runat="server" />
                        </p>
                    </div>
                    <div class="col-md-8 no-padding">
                        <p>
                            <sc:text id="Fax" field="Fax" runat="server" />
                        </p>
                    </div>
                </div>--%>
            </div>
            <%--<div class="info-map">
                <a href="#">
                    <div id="map_canvas" class="gmap"></div>
                </a>
            </div>--%>
        </div>
    </div>

    <div class="col-xs-12">

        <div class="col-xs-12 check-avilability content_bg_inner_box alert-info" id="avilabillity">
            <div>
                <div class="col-xs-12">
                    <h2>
                        <sc:text id="AvailabilityLabel" field="AvailabilityLabel" runat="server" />
                    </h2>
                </div>
                <div class="col-xs-12 margin-row">
                    <span>
                        <sc:text id="AvailabilityBoxTitleLabel" field="AvailabilityBoxTitleLabel" runat="server" />
                        <sc:text id="HotelNameForAvailability" field="Name" runat="server" />
                        ?
                    </span>
                </div>
                <div class="col-md-4 col-xs-12">

                    <span>
                        <sc:text id="CheckinDateLabel" field="CheckinDateLabel" runat="server" />
                    </span>
                    <div class="width-full">
                        <asp:TextBox ID="CheckInDate" CssClass="form-control datepicker width-avilability-textbox pull-right" Text="" ClientIDMode="Static" runat="server" data-provide="datepicker" />
                        <asp:RequiredFieldValidator ID="rfvCheckInDate" runat="server" ForeColor="Red" ControlToValidate="CheckInDate" Display="Dynamic">
                            <span class="pull-left" style="margin-left: 40px;">
                                <sc:text id="txtCheckInDateRequiredText" runat="server" visible="false" />
                            </span>
                        </asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">

                    <span>
                        <sc:text id="CheckoutDateLabel" field="NoOfNightsLabel" runat="server" />
                    </span>
                    <%--<asp:TextBox ID="CheckOutDate" CssClass="form-control" Text="" ClientIDMode="Static" runat="server" />--%>
                    <div role="group" aria-label="...">
                        <asp:DropDownList CssClass="form-control" ID="NoOfNights" ClientIDMode="Static" runat="server">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvNumberOfNights" runat="server" ForeColor="Red" ControlToValidate="NoOfNights" Display="Dynamic">
                            <sc:text id="txtNumberOfNightsRequiredText" runat="server" visible="false" />
                        </asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <span></span>
                    <%--<asp:Button CssClass="btn btn-primary booking-margin-top btn-bg pull-left" ID="CheckAvailabilityButton" OnClientClick="javascript:__doPostBack('CheckAvailabilityButton')" runat="server" Text=""/>--%>
                    <a href="" class="btn btn-primary booking-margin-top btn-bg pull-left" id="CheckAvailabilityBtn" runat="server" onclick="javascript:__doPostBack('CheckAvailabilityButton')">
                        <sc:text id="CheckAvailabilityLabel" field="CheckAvailabilityButton" runat="server"/>
                    </a>
                </div>
            </div>
        </div>

        <!-- mine added -->
        <div class="row">
            <div class="col-xs-12">
                <div id="demo">
                    <asp:Repeater ID="OffersList" runat="server" OnItemDataBound="OffersList_ItemDataBound">
                        <HeaderTemplate>
                            <div class="show-price-list no-padding">
                                <table class="room-type">
                                    <tbody>
                                        <tr style="border-top-style: solid; border-top-width: 1px; border-top-color: rgb(207, 242, 246); border-bottom-style: solid; border-bottom-width: 1px; border-bottom-color: rgb(207, 242, 246); background-color: rgb(250, 254, 254);">
                                            <th>
                                                <sc:text id="RoomTypesLabel" field="RoomTypesLabel" runat="server" />
                                            </th>
                                            <th>&nbsp;</th>
                                            <th>
                                                <sc:text id="PricePerBrakeLabel" field="PricePerBrakeLabel" runat="server" />
                                            </th>
                                            <th>
                                                <sc:text id="RoomsLabel" field="RoomsLabel" runat="server" />
                                            </th>
                                            <th></th>
                                        </tr>
                        </HeaderTemplate>

                        <ItemTemplate>
                            <tr style="background-color: rgb(255, 255, 255);">
                                <td><span><%#GetRoomName((ProviderInfo)Container.DataItem)%></span></td>
                                <%--<td><span class="text-red"><%#((OfferDataItem)Container.DataItem).Tip %></span></td>--%>
                                <td></td>
                                <td><span><%#this.GetRoomPrice((ProviderInfo)Container.DataItem) %>
                                    <sc:text field="OfferPriceSuffixLabel" runat="server" />
                                </span></td>
                                <td>
                                    <div aria-label="..." role="group">
                                        <asp:DropDownList ID="drpAvailability" runat="server" CssClass="form-control">
                                        </asp:DropDownList>

                                    </div>
                                </td>
                                <td>
                                    <asp:Button ID="ButtonAddToBasket1" OnClick="ButtonAddToBasket_Click" CommandArgument='<%#GetVariantSkuAndProviderID((ProviderInfo)Container.DataItem) %>' class="btn btn-primary booking-margin-top btn-bg pull-right" runat="server" Text='<%#Affinion.LoyaltyBuild.Common.Utilities.SitecoreFieldsHelper.GetValue(Sitecore.Context.Item ,"AddToBasketLabel") %>' Visible="false"/>
                                </td>
                            </tr>
                        </ItemTemplate>

                        <FooterTemplate>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <asp:Button ID="ButtonAddToBasketOneClick" OnClick="ButtonAddToBasketOneClick_Click" class="btn btn-primary booking-margin-top btn-bg pull-right" runat="server" Text='<%#Affinion.LoyaltyBuild.Common.Utilities.SitecoreFieldsHelper.GetValue(Sitecore.Context.Item ,"AddToBasketLabel") %>' />
                                </td>
                                <td></td>
                            </tr>
                            </tbody>
                                </table>
                            </div>
                        </FooterTemplate>
                    </asp:Repeater>
                    <asp:Repeater ID="roomTypeBBRepeater" runat="server" OnItemDataBound="roomTypeBBRepeater_ItemDataBound">
                        <HeaderTemplate>

                            <table class="room-type" style="width:100%;">
                                <tbody>

                                    <tr>
                                        <th>Room Types</th>
                                        <th>&nbsp;</th>
                                        <th>Best Rates</th>
                                        <th>No. rooms</th>
                                    </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><span><%# Eval("RoomType") %>(<%# (int.Parse(Eval("MealBasisID").ToString()))>0?Eval("MealBasis"):"" %>)</span></td>
                                <td><span class="text-red"></span></td>
                                <td>
                                    <span class="display-block-val"><%# Eval("PriceToDisplay") %> per break</span>
                                </td>
                                <td>
                                    <div aria-label="..." role="group">
                                        <asp:DropDownList ID="availabilityDDL" CssClass="form-control" runat="server"></asp:DropDownList>
                                    </div>
                                </td>
                                <td>
                                    <asp:Button ID="ButtonBBAddToBasket1" OnClick="ButtonAddToBasket_Click" CommandArgument='<%#GetVariantSkuAndProviderID((ProviderInfo)Container.DataItem) %>' class="btn btn-primary booking-margin-top btn-bg pull-right" runat="server" Text='<%#Affinion.LoyaltyBuild.Common.Utilities.SitecoreFieldsHelper.GetValue(Sitecore.Context.Item ,"AddToBasketLabel") %>' Visible="false" />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            <tr style="background-color: rgb(255, 255, 255);">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <asp:Button ID="ButtonBBAddToBasketOneClick" OnClick="ButtonBBAddToBasketOneClick_Click" class="btn btn-primary booking-margin-top btn-bg pull-right btn-basket" runat="server" Text='AddtoBasket' />
                                </td>
                            </tr>
                            </tbody>
         </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <div class="alert alert-danger msg-margin-top" id="SelectRoomAlertWrapper">
                    <sc:text id="txtSelectRoomText" runat="server" visible="false" />
                </div>
                <div class="row">
                    <div class="col-xs-12" id="AvailableAlert" runat="server">
                        <div class="alert alert-danger msg-margin-top">
                            <sc:text id="NoOfferFoundMessage" field="NoOfferFoundMessage" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- offer end-->

        <div class="detail-hotel margin-bottom-15">
            <div class="content_bg_inner_box light-box" id="facilities">
                <h2>
                    <sc:text id="facilityHeading" field="FacilitiesLabel" runat="server" />
                </h2>
                <sc:text id="Facilities" field="Facility" runat="server" />
            </div>
            <div class="content_bg_inner_box light-box" id="GoodToKnow">
                <h2>
                    <sc:text id="GoodToKnowLabel" field="GoodToKnowLabel" runat="server" />
                </h2>
                <sc:text id="GoodToKnow" field="GoodToKnow" runat="server" />
            </div>
            <div class="content_bg_inner_box light-box" id="CheckInCheckOutTime">
                <h2>
                    <sc:text id="CheckInCheckOutTimeLabel" field="CheckInCheckOutTimeLabel" runat="server" />
                </h2>
                <sc:date field="CheckinTimeLabel" id="CheckinTimeLabel" runat="server" format="HH:mm tt" />
                <sc:date field="CheckinTime" id="CheckinTime" runat="server" format="HH:mm tt" />
                <br />
                <sc:date field="CheckoutTimeLabel" id="CheckoutTimeLabel" runat="server" format="HH:mm tt" />
                <sc:date field="CheckoutTime" id="CheckoutTime" runat="server" format="HH:mm tt" />
            </div>

            <%--<div class="content_bg_inner_box light-box" id="Offers">
                        <h2>Offer Information</h2>
                        <div>
                           <div class="row">
                              <div class="col-sm-4 col-xs-12 margin-row">
                                 <a href="#off1" class="btn btn-info" data-toggle="collapse"><i class="btn-offer glyphicon glyphicon-cutlery"></i>Breakfast</a>
                                 <div id="off1" class="collapse">
                                    <div class="alert alert-warning inline-block width-full no-margin">
                                       <div class="col-xs-12">
                                          <div>
                                             <p>prepared full English and continental breakfast buffet is available. The Bar serves an array of wines, spirits, beers and local ales</p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-sm-4 col-xs-12 margin-row">
                                 <a href="#off2" class="btn btn-info" data-toggle="collapse"><i class="btn-offer glyphicon glyphicon-cutlery"></i>Lunch</a>
                                 <div id="off2" class="collapse">
                                    <div class="alert alert-warning inline-block width-full no-margin">
                                       <div class="col-xs-12">
                                          <div>
                                             <p>prepared full English and continental breakfast buffet is available. The Bar serves an array of wines, spirits, beers and local ales</p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-sm-4 col-xs-12 margin-row">
                                 <a href="#off3" class="btn btn-info" data-toggle="collapse"><i class="btn-offer glyphicon glyphicon-cutlery"></i>Dinner</a>
                                 <div id="off3" class="collapse">
                                    <div class="alert alert-warning inline-block width-full no-margin">
                                       <div class="col-xs-12">
                                          <div>
                                             <p>prepared full English and continental breakfast buffet is available. The Bar serves an array of wines, spirits, beers and local ales</p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-sm-4 col-xs-12 margin-row">
                                 <a href="#off4" class="btn btn-info" data-toggle="collapse"><i class="btn-offer glyphicon fa fa-star"></i>Spa (Global)</a>
                                 <div id="off4" class="collapse">
                                    <div class="alert alert-warning inline-block width-full no-margin">
                                       <div class="col-xs-12">
                                          <div>
                                             <p>prepared full English and continental breakfast buffet is available. The Bar serves an array of wines, spirits, beers and local ales</p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-sm-4 col-xs-12 margin-row">
                                 <a href="#off5" class="btn btn-info" data-toggle="collapse"><i class="btn-offer glyphicon fa fa-star"></i>Golf (Global)</a>
                                 <div id="off5" class="collapse">
                                    <div class="alert alert-warning inline-block width-full no-margin">
                                       <div class="col-xs-12">
                                          <div>
                                             <p>prepared full English and continental breakfast buffet is available. The Bar serves an array of wines, spirits, beers and local ales</p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-sm-4 col-xs-12 margin-row">
                                 <a href="#off6" class="btn btn-info btn-accent8" data-toggle="collapse"><i class="btn-offer glyphicon glyphicon-cutlery"></i>Ice cream</a>
                                 <div id="off6" class="collapse">
                                    <div class="alert alert-warning inline-block width-full no-margin">
                                       <div class="col-xs-12">
                                          <div>
                                             <p>prepared full English and continental breakfast buffet is available. The Bar serves an array of wines, spirits, beers and local ales</p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>--%>
        </div>
    </div>
</div>
<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>

<div class="modal fade" id="dialog-map" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><span class="fa fa-map-marker"></span>
                    <sc:text id="MapLabelText" field="MapLabel" runat="server" />
                </h4>
            </div>
            <div class="modal-body">
                <div id="map_canvas" class="large-gmap"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <sc:text id="MapCloseText" field="MapCloseButton" runat="server" />
                </button>
            </div>
        </div>
    </div>
</div>

<input type="hidden" class="hiddenListName" />
</asp:PlaceHolder>
<script type="text/javascript">
    //Store reference to the google map object
    var map;



    //Set the required Geo Information
    var latitude = '<%=latitude%>';
    var lognitude = '<%=lognitude%>';

    var mapOptions = {
        center: new google.maps.LatLng(latitude, lognitude),
        //center: coords,
        zoom: 6,
    };

    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    var marker;
    marker = new google.maps.Marker({
        position: new google.maps.LatLng(latitude, lognitude),
        map: map
    });

    //google.maps.event.addListener(marker, 'click', function () {
    //    window.location.href = marker.url;
    //});

    jQuery(document).ready(function () {
        jQuery('#dialog-map').on('shown.bs.modal', function () {

            if (navigator.geolocation) {
                //navigator.geolocation.getCurrentPosition(initializeMap);
                google.maps.event.trigger(map, "resize");
                map.setCenter(new google.maps.LatLng(latitude, lognitude));
                //loadMap();
            }
            else {
                alert("Sorry, your browser does not support geolocation services.");
            }

        });

        jQuery("input:radio[name='FlexibleDatesRadio']").change(function () {

            if (jQuery(this).is(':checked')) {
                __doPostBack('FlexibleDateRadioButton');
            }

        });
    });

    jQuery("#CheckInDate").datepicker({
        defaultDate: "+1w",
        dateFormat: "<%=DateTimeHelper.DateFormatJavaScript%>",
        numberOfMonths: 1,
        minDate: 0,
        showOn: "both",
        buttonImage: "calendar.png",
        buttonImageOnly: false,
        buttonClass: "glyphicon-calendar"
        //onClose: function (selectedDate) {

        //    var checkoutDate = new Date(selectedDate);
        //    if (checkoutDate.toString() != "Invalid Date") {
        //        checkoutDate.setDate(checkoutDate.getDate() + 1);
        //    }

        //    jQuery("#CheckOutDate").datepicker("option", "minDate", checkoutDate);
        //}
    }).next(".ui-datepicker-trigger").addClass("btn btn-default glyphicon glyphicon-calendar calander-icon pull-right");
    //jQuery("#CheckOutDate").datepicker({
    //    defaultDate: "+1w",
    //    numberOfMonths: 1,
    //    minDate: 0,
    //    onClose: function (selectedDate) {
    //        // jQuery("#CheckInDate").datepicker("option", "maxDate", selectedDate);
    //    }
    //});
    jQuery('#CheckInDate').attr('readonly', true);

    jQuery("[data-toggle=popover]").popover({
        html: true,
        content: function () {
            return jQuery('#popover-content').html();
        }
    });

</script>
