﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    public class NoBookingSchedulerModel
    {
        /// <summary>
        /// NoBookingId
        /// </summary>
        public int NoBookingId { get; set; }

        /// <summary>
        /// ClientId
        /// </summary>
        public Guid ClientGuid { get; set; }

        /// <summary>
        /// ClientId
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// ClientName
        /// </summary>
        public string ClientName { get; set; }

        /// <summary>
        /// FromDate
        /// </summary>
        public DateTime FromDate { get; set; }

        /// <summary>
        /// ToDate
        /// </summary>
        public DateTime ToDate { get; set; }

        /// <summary>
        /// TimeFrom
        /// </summary>
        public TimeSpan TimeFrom { get; set; }

        /// <summary>
        /// TimeTo
        /// </summary>
        public TimeSpan TimeTo { get; set; }

        /// <summary>
        /// TimeFrom
        /// </summary>
        public string TimeFromString { get; set; }

        /// <summary>
        /// TimeTo
        /// </summary>
        public string TimeToString { get; set; }

        /// <summary>
        /// Duration
        /// </summary>
        public TimeSpan Duration { get; set; }

        /// <summary>
        /// Comments
        /// </summary>
        public string Comments { get; set; }

        /// <summary>
        /// CreatedOn
        /// </summary>
        public DateTime CreatedOn { get; set; }

        /// <summary>
        /// CreatedBy
        /// </summary>
        public string CreatedBy { get; set; }

        /// <summary>
        /// LastEmailTrigger
        /// </summary>
        public string LastEmailTrigger { get; set; }

        /// <summary>
        /// Profile
        /// </summary>
        public string Profile { get; set; }

        /// <summary>
        /// Day of Week
        /// </summary>
        public string DayofWeek { get; set; }
    }
}
