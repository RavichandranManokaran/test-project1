﻿using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Linq;

using UCommerce.EntitiesV2.Queries;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Queries
{
    public class GetOrderLineSubrateQuery : ICannedQuery<OrderLineCampaignRelation>
    {
        /// <summary>
        /// _orderId
        /// </summary>
        private readonly int _orderId;

        /// <summary>
        /// OrderLineSubrateQuery
        /// </summary>
        /// <param name="orderId"></param>
        public GetOrderLineSubrateQuery(int orderId)
        {
            _orderId = orderId;
        }

        /// <summary>
        /// IEnumerable Type OrderLineCampaignRelation
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public IEnumerable<OrderLineCampaignRelation> Execute(ISession session)
        {
            IEnumerable<OrderLineCampaignRelation> source = session.Query<OrderLineCampaignRelation>().Where(cmp => cmp.OrderId == _orderId);

            return (from x in source.ToList<OrderLineCampaignRelation>()
                    select x);


        }


    }
}
