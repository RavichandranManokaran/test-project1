﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.AdminPortal.Extensions
{
    public static class Extensions
    {
        public static IEnumerable<T> SortList<T>(this IEnumerable<T> list,string sortingProperty, string sortExpression)
        {
            PropertyInfo prop = typeof(T).GetProperty(sortingProperty);

            if (prop == null)
            {
                throw new Exception("No property '" + sortingProperty + "' in + " + typeof(T).Name + "'");
            }

            if (sortExpression.ToLower()=="desc")
                return list.OrderByDescending(x => prop.GetValue(x, null));
            else
                return list.OrderBy(x => prop.GetValue(x, null));
        }
    }
}