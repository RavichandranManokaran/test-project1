namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.PrimaryLayouts.OfflinePortal
{
    #region Using Namespaces

    using Affinion.LoyaltyBuild.Common;
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    using System.Web.Security;
    using System.Web.UI.HtmlControls;
    using System.Web.UI;
    using Affinion.Loyaltybuild.Security;
    using Sitecore.Text;
    using Sitecore.Web;
    using Affinion.LoyaltyBuild.Web.ClientPortal.Helper;
    using Affinion.LoyaltyBuild.SupplierPortal.DataAccess;
    using Affinion.LoyaltyBuild.SupplierPortal.DataAccess.Service;


    #endregion

    public partial class OfflinePortalPrimaryLayout : BasePrimaryLayout
    {
        //private Hashtable dataHashTable = new Hashtable();
        public static readonly string portal = "offline";
        private TimeSpan TimeSpanToUnlockUserAccount { get; set; }
        private TimeSpan timeSpanToExpirePassword;
        private string ChangePasswordPage { get; set; }
       

        private Dictionary<string, dynamic> dictionary = new Dictionary<string, dynamic>();
        protected readonly ILoginService loginSvc;


        public OfflinePortalPrimaryLayout()
        {
            this.loginSvc = new LoginService();
        }

        /// <summary>
        /// Gets the main form
        /// </summary>

        public override HtmlForm MainForm
        {
            get
            {
                return this.mainform;
            }
        }

        public string Favicon
        {
            get
            {
                return Sitecore.Context.Site.Properties["favicon"];
            }
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            //Set the portal type
            SiteSettings.PortalType = Constants.PortalType.Offline;
            Item currentItem = Sitecore.Context.Item;
            pageTitle.InnerHtml = currentItem.Fields["BrowserTitle"].Value;

            this.breadcrumbofflineportal.Visible = true;


            if (TimeSpan.TryParse(ConfigurationHelper.GetWebConfigAppSettingValue(Constants.OfflinePortaltimeSpanToExpirePassword, "90:00:00:00"), out timeSpanToExpirePassword))
            {

                if (!Sitecore.Context.PageMode.IsPageEditor && Sitecore.Context.User.IsAuthenticated)
                {
                    GetLinks();

                    //Check if current page is changepassword page
                    if (!string.IsNullOrEmpty(ChangePasswordPage) && ChangePasswordPage.Contains(Request.FilePath))
                    {
                            this.breadcrumbofflineportal.Visible = false;                     
                    }                    
                    else if (SitecoreAccess.HasUserPasswordExpired(DomainUser, timeSpanToExpirePassword))
                    {
                        
                        var r = new Random();
                        string uniqueNumber = loginSvc.GenerateRandomNumbers(r);
                        loginSvc.InsertChangePasswordHistory(uniqueNumber, DomainUser.UserName);
                        UrlString url = new UrlString(ChangePasswordPage);
                        url.Append("id", uniqueNumber);
                        WebUtil.Redirect(url.GetUrl());
                    }
                   
                   
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public dynamic GetPageData(string key)
        {
            dynamic value = null;
            if (dictionary.ContainsKey(key))
            {
                dictionary.TryGetValue(key, value);
            }

            return value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool SetPageData(string key, dynamic value)
        {
            dictionary.Add(key, value);
            return true;
        }

        /// <summary>
        /// Domain User
        /// </summary>
        private static MembershipUser DomainUser
        {
            get
            {
                return Membership.GetUser(Sitecore.Context.User.Name);
            }

        }

        /// <summary>
        /// Page Settings
        /// </summary>
        private void GetLinks()
        {
            try
            {

                string itemPath = Constants.OfflinePortalSettings;
                Item settings = Sitecore.Context.Database.GetItem(itemPath);

                if (settings != null)
                {
                    //Links
                    this.ChangePasswordPage = GetLink(settings, "ChangePassword");
                }

            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Get url from link field
        /// </summary>
        /// <param name="item">Item</param>
        /// <param name="fieldName">Field name</param>
        /// <returns>Url</returns>
        private static string GetLink(Item item, string fieldName)
        {
            var link = SitecoreFieldsHelper.GetUrl(item, fieldName);
            return link.Url;
        }

        /// <summary>
        /// Verify Rendering In Server Form
        /// </summary>
        /// <param name="control"></param>
        public override void VerifyRenderingInServerForm(Control control)
        {
            //required to avoid the run time error "  
            //Control 'GridView1' of type 'Grid View' must be placed inside a form tag with runat=server."  
        }  
    }
}
