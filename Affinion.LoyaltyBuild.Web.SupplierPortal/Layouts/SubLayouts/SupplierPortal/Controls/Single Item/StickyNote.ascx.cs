﻿using Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration.SiteUI.Base;
using System;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Single_Item
{
    public partial class StickyNote : SitecoreSingleItemUserControl
    {
        private void Page_Load(object sender, EventArgs e)
        {
            WriteAlertsIfNeeded();
            HideIfNoVersionUnlessPageEditing();

            if (!IsDataSourceItemNull)
            {
                pnlStickyNote.CssClass = String.Format("span3 {0}-stick stick", DataSourceItem.Fields["Note Type"]);
            }
            else
            {
                pnlStickyNote.Visible = false;
            }
        }
    }
}