﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.SupplierPortal.DataAccess.Data
{
    public class ProviderGroup
    {
         /// <summary>
        /// provider group id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// provider group name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// ucommerce guid
        /// </summary>
        public string UCommerceGuid { get; set; }

      
        /// <summary>
        /// Sitecore Id
        /// </summary>
        public string SitecoreId { get; set; }
        
    }
}
