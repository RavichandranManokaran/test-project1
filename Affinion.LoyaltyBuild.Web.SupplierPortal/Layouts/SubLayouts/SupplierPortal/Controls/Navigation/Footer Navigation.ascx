﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Footer Navigation.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Navigation.Footer_Navigation" %>
<asp:Repeater ID="rptMenu" runat="server" OnItemDataBound="rptMenu_ItemDataBound">
 <HeaderTemplate><ul></HeaderTemplate>
 <ItemTemplate><li><asp:HyperLink ID="MenuLink" runat="server"><asp:Literal ID="MenuText" runat="server" /></asp:HyperLink></li></ItemTemplate>   
 <FooterTemplate></ul></FooterTemplate>
</asp:Repeater>
 <!-- Modal -->
  <div class="modal fade" id="chngPswrd" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Change Password</h4>
        </div>
        <div class="modal-body">
        <div class="col-md-12 login-form-margin" id="OldPassword">
                    <span style="color:black">Old Password :</span>
                           <asp:textbox runat="server" id="OldPasswordText" textmode="Password" cssclass="form-control"></asp:textbox>
                    <asp:requiredfieldvalidator id="RequiredFieldOldPassword" runat="server" controltovalidate="OldPasswordText" errormessage="*Required" forecolor="Red" ValidationGroup="popupValidation"></asp:requiredfieldvalidator>
                    
                </div>
                <div class="col-md-12 login-form-margin" id="NewPassword">
                    <span style="color:black">New Password :</span>
                              <asp:textbox id="NewPasswordText" runat="server" cssclass="form-control" textmode="Password"></asp:textbox>
                    <asp:requiredfieldvalidator id="RequiredFieldNewPassword" runat="server" controltovalidate="NewPasswordText" errormessage="*Required" forecolor="Red" ValidationGroup="popupValidation"></asp:requiredfieldvalidator>
                   
                </div>
                <div class="col-md-12 login-form-margin" id="ConfirmPassword">
                    <span style="color:black">Confirm Password : </span><asp:textbox id="ConfirmPasswordText" runat="server" cssclass="form-control" textmode="Password"></asp:textbox>
                    <asp:requiredfieldvalidator id="RequiredFieldConfirmPassword" runat="server" controltovalidate="ConfirmPasswordText" errormessage="*Required" forecolor="Red" ValidationGroup="popupValidation"></asp:requiredfieldvalidator>                    
                </div>
                <asp:button id="btnSubmit" runat="server" text="Submit" OnClick="btnSubmit_Click" cssclass="btn btn-primary btn-bg" style="height: 26px" ValidationGroup="popupValidation" />
                <asp:button id="btnCancel" runat="server" text="Cancel" data-dismiss="modal" cssclass="btn btn-primary btn-bg" style="height: 26px" CausesValidation="False" />
        </div>        
      </div>
      
    </div>
  </div> 