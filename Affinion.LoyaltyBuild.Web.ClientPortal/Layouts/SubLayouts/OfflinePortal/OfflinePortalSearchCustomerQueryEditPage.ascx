﻿<%@ Control Language="C#" EnableViewState="false" AutoEventWireup="true" CodeBehind="OfflinePortalSearchCustomerQueryEditPage.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalSearchCustomerQueryEditPage" %>
 
<div class="content" id="custqueryedit">
    <div class="content-body">
        <div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading bg-primary">
                                <h4 class="panel-title"><sc:text id="EditCustomerQueryTitleText" field="QueryDetailHeading" runat="server" /> 
                                </h4>
                            </div>
                            <div id="collapse0" class="panel-collapse collapse in">
                                <div class="panel-body alert-info">
                                    <form runat="server">
                                        <div class="col-sm-12 no-padding-m">
                                            <div class="content_bg_inner_box alert-info">
                                                <div class="col-sm-12 no-padding-m">
                                                    <div class="row margin-row">
                                                        <div class="col-sm-3"><span class="bask-form-titl font-size-12"> <sc:text id="EnquiryType" field="EnquiryType" runat="server" /> <span class="accent58-f"> *</span></span></div>
                                                        <div class="col-sm-3">
                                                            <asp:DropDownList CssClass="form-control font-size-12" ID="ddlEnquiryType" ClientIDMode="Static" runat="server">
                                                            </asp:DropDownList>
                                                            <input type="hidden" id="hdquerytype" ClientIDMode="Static"  runat="server" />
                                                            <input type="hidden" id="hdquerysolved" runat="server"  ClientIDMode="Static" />
                                                        </div>
                                                    </div>

                                                    <div class="row margin-row">
                                                        <div class="col-sm-3"><span class="bask-form-titl font-size-12"> <sc:text id="Partner" field="Partner" runat="server" /> </span></div>
                                                        <div class="col-sm-3">
                                                            <input type="text" id="txtPartner" class="form-control font-size-12" runat="server" name="txtPartner" readonly />
                                                        </div>
                                                    </div>
                                                    <div class="row margin-row">
                                                        <div class="col-sm-3"><span class="bask-form-titl font-size-12"> <sc:text id="Provider" field="Provider" runat="server" /> </span></div>
                                                        <div class="col-sm-3">
                                                            <input type="text" id="txtProvider" class="form-control font-size-12" name="txtProvider" runat="server" readonly />
                                                        </div>
                                                    </div>
                                                    <div class="row margin-row">
                                                        <div class="col-sm-3"><span class="bask-form-titl font-size-12"> <sc:text id="IsResolved" field="IsResolved" runat="server" /> </span></div>
                                                        <div class="col-sm-3" id="IsResolved">
                                                            <input type="radio" class="font-size-12" name="option" value="Yes" id="rdryes" runat="server"  ClientIDMode="Static"  />Yes 
									                        <input type="radio" class="font-size-12" name="option" value="No" style="margin-left: 10px;" id="rdryno" runat="server"   ClientIDMode="Static" />No
                                                        </div>
                                                    </div>
                                                    <div class="row margin-row">
                                                        <div class="col-sm-3"><span class="bask-form-titl font-size-12"> <sc:text id="ReferenceNo" field="ReferenceNo" runat="server" /> </span></div>
                                                        <div class="col-sm-3">
                                                            <input type="text" id="lblReferenceNo" class="form-control font-size-12" runat="server" name="lblReferenceNo" readonly />

                                                        </div>
                                                    </div>
                                                    <div class="row margin-row pull-right" style="width: 46%; margin-top: -21%;">
                                                        <div class="col-sm-3"><span class="bask-form-titl font-size-12"> <sc:text id="Notes" field="Notes" runat="server" /> </span></div>
                                                        <div class="col-sm-9">
                                                            <textarea id="txtNotes" class="form-control font-size-12" style="min-height: 207px;" runat="server" readonly="readonly"></textarea>
                                                            <asp:CustomValidator id="CAnotes" ControlToValidate="txtNotes" ClientValidationFunction="ClientValidate" Display="Dynamic" ErrorMessage="*Dont Enter Card Number!" ForeColor="Red" Font-Name="verdana"  Font-Size="10pt" runat="server"/>
                                                        </div>
                                                    </div>
                                                    <div class="row margin-row">
                                                        <div class="col-sm-3"><span class="bask-form-titl font-size-12"> <sc:text id="AddNote" field="AddNote" runat="server" /> </span><span class="accent58-f"> *</span></span></div>
                                                        <div class="col-sm-9">
                                                            <textarea id="txtAddNotes" class="form-control font-size-12" style="min-height: 159px;" runat="server"  ClientIDMode="Static"  ></textarea>
                                                            <asp:CustomValidator id="addNotes" ControlToValidate="txtAddNotes" ClientValidationFunction="ClientValidate" Display="Dynamic" ErrorMessage="*Dont Enter Card Number!" ForeColor="Red" Font-Name="verdana"  Font-Size="10pt" runat="server"/>
                                                            
                                                        </div>
                                                    </div>

                                                    <div class="row margin-row pull-right col-sm-12" style="padding: 0;">

                                                        <div class="col-sm-14">
                                                            <input type="file" id="btnselectfile" style="display: none" />
                                                            <button type="button"  class="btn btn-default add-break pull-right width-full font-size-12" style="width: 100px;float:left" data-dismiss="modal"> <sc:text id="CloseText" field="CloseButton" runat="server" /> </span> </button>
                                                            <button id="btnFileUpload" class="btn btn-default add-break pull-right width-full font-size-12" style="width: 100px;float:left" type="button"> <sc:text id="FileUploadText" field="FileUploadButton" runat="server" /> </button>
                                                            <button id="btnViewFile" class="btn btn-default add-break pull-right width-full font-size-12" style="width: 100px;float:left" type="button" runat="server" ClientIDMode="Static"> <sc:text id="ViewFileText" field="ViewFileButton" runat="server" /> </button>
                                                            <button id="btnDelete" class="btn btn-default add-break pull-right width-full font-size-12" style="width: 100px;float:left" type="button" runat="server" ClientIDMode="Static"> <sc:text id="DeleteFileText" field="DeleteFileButton" runat="server" /> </button>
                                                            <button id="btnUpdate" class="btn btn-default add-break pull-right width-full font-size-12" style="width: 100px;float:left" type="button"> <sc:text id="UpdateText" field="UpdateButton" runat="server" /> </button>
                                                            <button id="btnReset" class="btn btn-default add-break pull-right width-full font-size-12" style="width: 100px;float:left" type="button">  <sc:text id="ResetText" field="ResetButton" runat="server" /> </button>
                                                           
                                                        </div>

                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

