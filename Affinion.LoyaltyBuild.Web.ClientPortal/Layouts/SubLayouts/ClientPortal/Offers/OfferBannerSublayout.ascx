﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfferBannerSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal.Offers.OfferBannerSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="width-full">

    <!-- Jumbotron -->
    <div class="row slider">
        <div class="col-md-12">
            <div class="slider-top">

                <div class="offerpic">

                    <div class="offer_name">
                        <span>
                            <sc:FieldRenderer ID="OfferTitleRenderer" FieldName="Title" runat="server" />
                        </span>
                    </div>
                    <sc:Image ID="OfferImage" Field="DesktopImage" runat="server" CssClass="offer_img img-responsive" />

                    <%--<img class="offer_img img-responsive" src="images/offer2.jpg">--%>
                </div>
                <sc:Placeholder ID="offersearchpane" Key="offersearchpane" runat="server" />
                <%--<div class="booking-detail offerbooking-detail" id="show-hide">
                            <div class="search-inner">
                                <div class="booking-top width-full divider">
                                    <h4 class="no-margin"><i class="glyphicon glyphicon-th-large"></i>Find the best deals on accommodation</h4>
                                </div>
                                <div class="booking-top width-full divider">
                                </div>
                                <div class="col-xs-12">
                                    <h5>Destination/hotel name</h5>
                                    <input type="text" class="form-control" placeholder="Detaile Info" />
                                </div>
                                <div class="col-xs-12">
                                    <h5>Check-in-date</h5>
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default glyphicon glyphicon-calendar calander-icon" type="button"></button>
                                        </span>
                                        <input type="text" class="form-control" placeholder="Check-in date" id="datepicker" />
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <h5>Check-out-date</h5>
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default glyphicon glyphicon-calendar calander-icon" type="button"></button>
                                        </span>
                                        <input type="text" class="form-control" placeholder="Check-out-date" id="datepicker">
                                    </div>
                                </div>
                                <div class="booking-deatail-order-room width-full inline-block">
                                    <div class="col-xs-12 col-md-4">
                                        <h5>Rooms</h5>
                                        <div role="group" aria-label="...">
                                            <select class="form-control">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <h5>Adults</h5>
                                        <div role="group" aria-label="...">
                                            <select class="form-control">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-4">
                                        <h5>Children</h5>
                                        <div role="group" aria-label="...">
                                            <select class="form-control">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <h5>Choose Your offers </h5>
                                    <select class="form-control styled-select">
                                        <option selected="selected" value="746">Show me everything</option>
                                        <option value="205">Breakfast</option>
                                        <option value="252">Breakfast + 1 Dinner</option>
                                        <option value="748">Breakfast  - Inspired Collection</option>
                                        <option value="248">Breakfast + 1 Dinner + Spa</option>
                                        <option value="238">Self Catering Accommodation</option>

                                    </select>
                                </div>
                                <div class="col-xs-12">
                                    <button type="submit" class="btn btn-primary booking-margin-top float-right btn-bg">Submit</button>
                                </div>
                            </div>
                        </div>--%>

                <%--<sc:Placeholder ID="searchHotels" Key="searchHotels" runat="server" />--%>
            </div>

        </div>

    </div>
</div>
