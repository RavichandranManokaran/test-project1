﻿using System;
using System.Xml.Serialization;

namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Model.Room
{
    [Serializable]
    [XmlType(TypeName = "Adjustment")]
    public class JtAdjustment
    {
        public string AdjustmentType { get; set; }
        public string SpecialOfferTypeID { get; set; }
        public string AdjustmentName { get; set; }
        public string ContractArrangementID { get; set; }
        public string Total { get; set; }
    }
}
