﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SearchHelper.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common.Search
/// Description:           This field will generate and store the readable value instead of the Id of "Location" field of SupplierDetails template
/// </summary>
namespace Affinion.LoyaltyBuild.Common.Search.ComputedFields
{
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Sitecore.ContentSearch;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using System;

    /// <summary>
    /// This field will generate and store the readable value instead of the Id of "Location" field of SupplierDetails template
    /// </summary>
    public class LocationField : ComputedField
    {
        /// <summary>
        /// Calculates the field value
        /// </summary>
        /// <param name="indexable">The indexable item</param>
        /// <returns>Returns the calculated value</returns>
        public override object ComputeFieldValue(IIndexable indexable)
        {
            try
            {
                var supplier = ContentDatabase.GetItem(ID.Parse(indexable.Id));

                string locationName = SitecoreFieldsHelper.GetLookupFieldTargetItemValue(supplier, "Location", "Name");
                Item countryItem = SitecoreFieldsHelper.GetLookupFieldTargetItem(supplier, "Location");
                string country = SitecoreFieldsHelper.GetLookupFieldTargetItemValue(countryItem, "Country", "CountryName");
                return string.Format("{0},{1}", locationName, country);
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                return string.Empty;
            }
        }
    }
}
