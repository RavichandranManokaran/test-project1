
/****** Object:  Table [dbo].[LB_BB_Facilities]    Script Date: 7/19/2016 7:59:10 PM ******/

CREATE TABLE [dbo].[LB_BB_Facilities](
	[FacilityID] [int] NOT NULL,
	[FacilityGroupID] [varchar](50) NULL,
	[FacilityGroup] [varchar](50) NULL,
	[FacilityType] [varchar](50) NULL,
	[Facility] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[FacilityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
