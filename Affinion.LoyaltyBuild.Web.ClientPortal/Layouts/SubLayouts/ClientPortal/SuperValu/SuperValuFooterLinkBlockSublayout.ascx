﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SuperValuFooterLinkBlockSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.SuperValuFooterLinkBlockSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Sitecore.Data.Items" %>

<div class="col-sm-4">
    <div class="footer-left-area">
        <asp:Literal ID="PlaceholderText" Text="Set the datasource: Required Fields: Title, Body, Items" runat="server" />
        <h2><sc:Text Field="Title" ID="Title" runat="server" /></h2>
        <sc:FieldRenderer FieldName="Body" ID="Body" runat="server" />
        <asp:Repeater ID="LinksBlock" runat="server">

            <HeaderTemplate>
                <ul class="no-padding">
            </HeaderTemplate>
            <ItemTemplate>
                <li>
                    <sc:Link Field="Link" ID="Link" Item="<%#(Item)Container.DataItem %>" runat="server" />
                </li>
            </ItemTemplate>
            <FooterTemplate>
                </ul>
            </FooterTemplate>

        </asp:Repeater> 
    </div>
</div>
