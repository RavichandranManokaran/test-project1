﻿CREATE PROCEDURE [dbo].LB_SelectInvalidLogAttemptTime

       @Count  nvarchar  ,
	   @UserId    varchar(200)       
     

AS

BEGIN 

     SET NOCOUNT ON 



     SELECT FailedPasswordAttemptWindowStart   

     FROM   [dbo].[aspnet_Membership]  

     WHERE  

           UserId=@UserId and FailedPasswordAnswerAttemptCount =@Count 



END 