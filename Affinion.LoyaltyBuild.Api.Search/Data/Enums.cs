﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Api.Search.Data
{
    /// <summary>
    /// Enum that defines the sortfields
    /// </summary>
    [Serializable]
    public enum SortField
    {
        Price, 
        Name, 
        Default,   
        Rate,
        Recommended
    }

    /// <summary>
    /// Enum that defines the sort direction
    /// </summary>
    [Serializable]
    public enum SortDirection
    {
        Ascending,
        Descending,
        None
    }

    /// <summary>
    /// Enum that defines the type of location
    /// </summary>
    [Serializable]
    public enum LocationType
    {
        FreeText,
        Country,
        City,
        Hotel
    }
}
