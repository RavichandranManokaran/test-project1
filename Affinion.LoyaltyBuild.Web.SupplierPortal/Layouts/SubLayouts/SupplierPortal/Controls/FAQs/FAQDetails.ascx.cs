﻿using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.FAQs
{

    /// <summary>
    /// FAQDetails
    /// </summary>
    public partial class FAQDetails : BaseSublayout
    {


        #region variable declaration part

        private readonly string questionField = "Question";
        private readonly string answerField = "Answer";
        protected List<FAQ> FaqList { get; private set; }

        /// <summary>
        /// Title
        /// </summary>
        protected string Title
        {
            get { return Sitecore.Context.Item["Title"]; }
        }

        #endregion


        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            FaqList = new List<FAQ>();

            //fetch all the faq's from sitecore
            FetchQuestionAndAnswer();
            
            
        }

        /// <summary>
        /// fetch all the faq's from sitecore
        /// </summary>
        private void FetchQuestionAndAnswer()
        {
            Item datasourceItem = this.GetDataSource();
            List<Item> faqs = new List<Item>();
            if (datasourceItem != null)
            {
                var faqItems = datasourceItem.GetChildren();
                if (faqItems != null && faqItems.Count > 0)
                {
                    faqs = faqItems.ToList<Item>();
                        
                    foreach (var faq in faqs)
                    {
                        FAQ faqFields = new FAQ();
                        faqFields.Question = SitecoreFieldsHelper.GetItemFieldValue(faq, questionField);
                        faqFields.Answer = SitecoreFieldsHelper.GetItemFieldValue(faq, answerField);
                        FaqList.Add(faqFields);
                    }
                }
            }
        }
    }
}