﻿CREATE TABLE [dbo].[LB_CustomerQuery_Attachments]
(
	[AttachmentId] [int] IDENTITY(1,1) NOT NULL,
	[QueryId] [int] NOT NULL,
	[FileType] [nvarchar](50) NOT NULL,
	[FileName] [nvarchar](100) NOT NULL,
	[FileContent] [varbinary](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[AttachmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


ALTER TABLE [dbo].[LB_CustomerQuery]  WITH CHECK ADD  CONSTRAINT [FK_LB_CustomerQuery] FOREIGN KEY([QueryId])
REFERENCES [dbo].[LB_CustomerQuery] ([QueryId])
GO

ALTER TABLE [dbo].[LB_CustomerQuery] CHECK CONSTRAINT [FK_LB_CustomerQuery]
GO

