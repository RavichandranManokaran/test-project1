﻿/****** Object:  StoredProcedure [dbo].[LB_AddBookingPayment]    Script Date: 2/12/2016 11:42:44 AM ******/
DROP PROCEDURE [dbo].[LB_AddBookingPayment]
GO

/****** Object:  StoredProcedure [dbo].[LB_AddBookingPayment]    Script Date: 2/12/2016 11:42:44 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Prasanna SR
-- Create date: 12 Feb 2016
-- Description:	Add booking payment
-- =============================================
/****** Object:  StoredProcedure [dbo].[LB_AddBookingPayment]    Script Date: 6/9/2016 7:26:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[LB_AddBookingPayment]
	@OrderLineId INT,
	@CurrencyId INT,
	@PaymentMethodId INT,
	@Amount INT,
	@ReferenceId NVARCHAR(MAX),
	@CreatedBy VARCHAR(50),
	@CommissionAmount MONEY = NULL,
	@ProviderAmount MONEY = NULL,
	@ProcessingFee MONEY = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE 
		@OrderId INT,
		@OrderPaymentId INT,
		@OrderLinePaymentId INT
	SELECT @OrderId = OrderId FROM uCommerce_OrderLine WHERE OrderLineId = @OrderLineId

	INSERT INTO [dbo].[uCommerce_Payment]
        ([TransactionId]
        ,[PaymentMethodName]
        ,[Created]
        ,[PaymentMethodId]
        ,[Fee]
        ,[FeePercentage]
        ,[PaymentStatusId]
        ,[Amount]
        ,[OrderId]
        ,[FeeTotal]
        ,[ReferenceId])
     SELECT
        NULL
        ,PM.Name
        ,GETDATE()
        ,@PaymentMethodId
        ,0
        ,0
        ,1
        ,@Amount
        ,@OrderId
        ,0
        ,@ReferenceId
	FROM 
		uCommerce_PaymentMethod PM
	WHERE
		PaymentMethodId = @PaymentMethodId

	SET @OrderPaymentId = SCOPE_IDENTITY()

	IF(@ProviderAmount IS NOT NULL AND @ProviderAmount <> 0)
		INSERT INTO [dbo].[LB_OrderLinePayment]
			([OrderLineID]
			,[CurrencyTypeId]
			,[PaymentMethodID]
			,[ReferenceID]
			,[Amount]
			,[OrderPaymentID]
			,[CreatedBy]
			,[CreatedDate]
			,[DueTypeId])
		 VALUES
			(@OrderLineId
			,@CurrencyId
			,@PaymentMethodId
			,@ReferenceId
			,@ProviderAmount
			,@OrderPaymentId
			,@CreatedBy
			,GETDATE(),
			1)

	IF(@CommissionAmount IS NOT NULL AND @CommissionAmount <> 0)
	BEGIN
		INSERT INTO [dbo].[LB_OrderLinePayment]
			([OrderLineID]
			,[CurrencyTypeId]
			,[PaymentMethodID]
			,[ReferenceID]
			,[Amount]
			,[OrderPaymentID]
			,[CreatedBy]
			,[CreatedDate]
			,[DueTypeId])
		 VALUES
			(@OrderLineId
			,@CurrencyId
			,@PaymentMethodId
			,@ReferenceId
			,@CommissionAmount
			,@OrderPaymentId
			,@CreatedBy
			,GETDATE(),
			2)
	END
	
	IF(@ProcessingFee IS NOT NULL AND @ProcessingFee <> 0)
	BEGIN
		INSERT INTO [dbo].[LB_OrderLinePayment]
			([OrderLineID]
			,[CurrencyTypeId]
			,[PaymentMethodID]
			,[ReferenceID]
			,[Amount]
			,[OrderPaymentID]
			,[CreatedBy]
			,[CreatedDate]
			,[DueTypeId])
		 VALUES
			(@OrderLineId
			,@CurrencyId
			,@PaymentMethodId
			,@ReferenceId
			,@ProcessingFee
			,@OrderPaymentId
			,@CreatedBy
			,GETDATE(),
			3)
	END

	DECLARE @UnMatchedAmount BIT = 1
	IF(@Amount IS NOT NULL AND @Amount>0)
	BEGIN
		DECLARE @Payments TABLE (DueTypeId INT, Amount MONEY)
		INSERT INTO @Payments
		SELECT [DueTypeId], SUM(Amount) FROM
		(
			SELECT [PaymentDueTypeId] DueTypeId, SUM(Amount) [Amount] FROM LB_OrderLineDue OLD 
				INNER JOIN LB_DueTypes DT ON OLD.DueTypeId = DT.DueTypeId
			WHERE 
				OLD.Amount > 0 AND 
				DT.[PaymentDueTypeId] IS NOT NULL AND 
				OLD.OrderLineID IN (SELECT OrderLineID FROM [dbo].[LB_RelatedOrderLines](@OrderLineId))
			GROUP BY [PaymentDueTypeId]
			UNION
			SELECT DueTypeId, SUM(-Amount) [Amount] FROM uCommerce_OrderLinePayment OLP
			WHERE 
				OLP.Amount > 0 AND 
				OLP.OrderLineID IN (SELECT OrderLineID FROM [dbo].[LB_RelatedOrderLines](@OrderLineId))
			GROUP BY DueTypeId
		)  Dues
		GROUP BY [DueTypeId]
		HAVING SUM(Amount) > 0

		IF (SELECT SUM(Amount) FROM @Payments) = @Amount
		BEGIN
			SET @UnMatchedAmount = 0

			INSERT INTO [dbo].[LB_OrderLinePayment]
				([OrderLineID]
				,[CurrencyTypeId]
				,[PaymentMethodID]
				,[ReferenceID]
				,[Amount]
				,[OrderPaymentID]
				,[CreatedBy]
				,[CreatedDate]
				,[DueTypeId])
			 SELECT
				@OrderLineId
				,@CurrencyId
				,@PaymentMethodId
				,@ReferenceId
				,Amount
				,@OrderPaymentId
				,@CreatedBy
				,GETDATE()
				,DueTypeId
			FROM @Payments
		END
	END

	IF(@Amount IS NOT NULL AND @Amount<>0 AND (@Amount < 0 OR @UnMatchedAmount = 1))
	BEGIN
		INSERT INTO [dbo].[LB_OrderLinePayment]
			([OrderLineID]
			,[CurrencyTypeId]
			,[PaymentMethodID]
			,[ReferenceID]
			,[Amount]
			,[OrderPaymentID]
			,[CreatedBy]
			,[CreatedDate]
			,[DueTypeId])
		 VALUES
			(@OrderLineId
			,@CurrencyId
			,@PaymentMethodId
			,@ReferenceId
			,@Amount
			,@OrderPaymentId
			,@CreatedBy
			,GETDATE()
			,2)
	END

END

GO

