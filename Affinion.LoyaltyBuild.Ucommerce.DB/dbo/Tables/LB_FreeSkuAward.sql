﻿CREATE TABLE [dbo].[LB_FreeSkuAward] (
    [FreeSkuAwardId] INT            NOT NULL,
    [FreeSkuType]    NVARCHAR (500) NOT NULL,
    CONSTRAINT [PK_uCommerce_FreeSkuAward] PRIMARY KEY CLUSTERED ([FreeSkuAwardId] ASC),
    CONSTRAINT [FK_uCommerce_FreeSkuAward_uCommerce_Award] FOREIGN KEY ([FreeSkuAwardId]) REFERENCES [dbo].[uCommerce_Award] ([AwardId])
);

