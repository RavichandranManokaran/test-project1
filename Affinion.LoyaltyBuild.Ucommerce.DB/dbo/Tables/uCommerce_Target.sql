﻿CREATE TABLE [dbo].[uCommerce_Target] (
    [TargetId]          INT IDENTITY (1, 1) NOT NULL,
    [CampaignItemId]    INT NOT NULL,
    [EnabledForDisplay] BIT NOT NULL,
    [EnabledForApply]   BIT NOT NULL,
    CONSTRAINT [PK_uCommerce_Target] PRIMARY KEY CLUSTERED ([TargetId] ASC),
    CONSTRAINT [FK_uCommerce_Target_uCommerce_Target] FOREIGN KEY ([CampaignItemId]) REFERENCES [dbo].[uCommerce_CampaignItem] ([CampaignItemId])
);

