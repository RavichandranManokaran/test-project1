﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.SupplierPortal.Reports.Data
{
    /// <summary>
    /// Contains details for the item arrival booking never transferred report item
    /// </summary>
    public class ArrivalBookingNeverTransferred : ArrivalBase
    {
        /// <summary>
        /// Booking status information
        /// </summary>
        public string BookingStatus { get; set; }
        /// <summary>
        /// Booking status information
        /// </summary>
        public DateTime BookingStatusDate { get; set; }
        /// <summary>
        /// Acknowledgement
        /// </summary>
        public string Acknowledgement { get; set; }
        /// <summary>
        /// AddressLine1
        /// </summary>
        public string Line1 { get; set; }
        /// <summary>
        /// Address Line2
        /// </summary>
        public string Line2 { get; set; }
        /// <summary>
        /// City
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// Region
        /// </summary>
        public string Region { get; set; }
        /// <summary>
        /// Country
        /// </summary>
        public string Country { get; set; }
        /// <summary>
        /// PhoneNumber
        /// </summary>
        public string  PhoneNumber { get; set; }
        /// <summary>
        /// MobilePhoneNUmber
        /// </summary>
        public string  MobilePhoneNumber { get; set; }
        /// <summary>
        /// Amount collected from client (Paid amt-vat-commision)
        /// </summary>
        public decimal CollectAmount { get; set; }
        /// <summary>
        /// Booking through onile/offline
        /// </summary>
        public string BookingThrough { get; set; }
        /// <summary>
        /// Child Age
        /// </summary>
        public int? Child1Age { get; set; }
        /// <summary>
        /// Child Age
        /// </summary>
        public int? Child2Age { get; set; }
        /// <summary>
        /// Child Age
        /// </summary>
        public int? Child3Age { get; set; }
        /// <summary>
        /// Child Age
        /// </summary>
        public int? Child4Age { get; set; }
        /// <summary>
        /// Child Age
        /// </summary>
        public int? Child5Age { get; set; }
        /// <summary>
        /// Child Age
        /// </summary>
        public int? Child6Age { get; set; }
        /// <summary>
        /// Child Age
        /// </summary>
        public int? Child7Age { get; set; }
        /// <summary>
        /// Child Age
        /// </summary>
        public int? Child8Age { get; set; }
    }

    /// <summary>
    /// Collection to manage Arrival booking never transferred
    /// </summary>
    public class ArrivalBookingNeverTransferredCollection : IEnumerable<ArrivalBookingNeverTransferred>,IList<ArrivalBookingNeverTransferred>
    {
        //Private variables declaration
        private List<ArrivalBookingNeverTransferred> _items;

        /// <summary>
        /// Constructor
        /// </summary>
        public ArrivalBookingNeverTransferredCollection()
        {
            _items = new List<ArrivalBookingNeverTransferred>();
        }

        /// <summary>
        /// Add method to add item
        /// </summary>
        /// <param name="item"></param>
        public void Add(ArrivalBookingNeverTransferred item)
        {
            _items.Add(item);
        }

        /// <summary>
        /// Override method IEnumenrable class
        /// </summary>
        /// <returns></returns>
        public IEnumerator<ArrivalBookingNeverTransferred> GetEnumerator()
        {
            foreach (var item in _items)
                yield return item;
        }

        /// <summary>
        /// Override method IEnumenrable class
        /// </summary>
        /// <returns></returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            foreach (var item in _items)
                yield return item;
        }



        public int IndexOf(ArrivalBookingNeverTransferred item)
        {
            throw new NotImplementedException();
        }

        public void Insert(int index, ArrivalBookingNeverTransferred item)
        {
            throw new NotImplementedException();
        }

        public void RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        public ArrivalBookingNeverTransferred this[int index]
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }


        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(ArrivalBookingNeverTransferred item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(ArrivalBookingNeverTransferred[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public int Count
        {
            get { return _items.Count; }
        }

        public bool IsReadOnly
        {
            get { throw new NotImplementedException(); }
        }

        public bool Remove(ArrivalBookingNeverTransferred item)
        {
            throw new NotImplementedException();
        }
    }
}
