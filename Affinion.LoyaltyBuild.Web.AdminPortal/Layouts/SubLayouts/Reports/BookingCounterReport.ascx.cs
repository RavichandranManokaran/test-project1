﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Sitecore.Data.Items;
using Affinion.LoyaltyBuild.Common.Utilities;


namespace Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.Reports
{
    public partial class BookingCounterReport : System.Web.UI.UserControl
    {    
        protected void Page_Load(object sender, EventArgs e)
        {
            /// <summary>
            ///Binding data(s) into gridview control when page getting loaded
            /// </summary>
            BindData();
            BindSiteCore();
        }

        #region Private Methods

        /// <summary>
        ///Binding data(s) into gridview control
        /// </summary>
        private void BindData()
        {
           //display date and 
            
            string[] words = DateTime.Now.ToString("dd-MMM-yyyy HH:mm:ss").Split(' ');
            lblDateTime.Text = words[0] + " at " + words[1];
            lblDateTimeYesterday.Text = words[0] + " at " + words[1];
            lblDateTimeYear.Text= words[0] + " at " + words[1];
            lblDateTimeMonth.Text=words[0] + " at " + words[1];
            lblDateTimeWeek.Text = words[0] + " at " + words[1];


            ReportsService obReportsService = new ReportsService();
            //getting booking counter datas and storing into result variable
            var result = obReportsService.GetBookingCounterReport();
            //binding today bookings
            BindBookingTotal(grdvwToday, result.Today);
            //grdvwToday.DataSource = result.Today;
            grdvwToday.DataBind();
            //binding yesterday bookings
            BindBookingTotal(grdvwYesterday, result.Yesterday);
            //grdvwYesterday.DataSource = result.Yesterday;
            grdvwYesterday.DataBind();
            //binding week bookings
            BindBookingTotal(grdvwWeek, result.Week);
            //grdvwWeek.DataSource = result.Week;
            grdvwWeek.DataBind();
            //binding month bookings
            BindBookingTotal(grdvwMonth, result.Month);
            //grdvwMonth.DataSource = result.Month;
            grdvwMonth.DataBind();
            //binding year bookings
            BindBookingTotal(grdvwYear, result.Year);
           // grdvwYear.DataSource = result.Year;
            grdvwYear.DataBind();
        }

        #endregion
        /// <summary>
        /// Binding Booking Total 
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="model"></param>
        private void BindBookingTotal(GridView grid, List<BookingCounter> model)
        {


            grid.Columns[1].FooterText = model.Sum(i => i.Bookings).ToString(); //model.Bookings.ToString();
            //grid.FooterRow.Coloumn1.Text = model1.Bookings.ToString(); // model.Sum(i => i.Bookings).ToString();
            grid.DataSource = model;
            grid.DataBind();
        }

        /// <summary>
        /// Binding Sitecore Items
        /// </summary>
        private void BindSiteCore()
        {
            Item currentItem = Sitecore.Context.Item;
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "TodayBooking", scTodayBooking);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "List Of Today Booking Partner", scTodayBookingPartner);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Yesterday Booking", scYesterdayBooking);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "List of Yesterday Booking By Partners", scYesterdayBookingPartner);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Week up to date", scWeek);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "List of Week Booking By Partners", scWeekPartner);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Month up to date", scMonth);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "List of Month Booking By Partners", scMonthPartner);

            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Year to date", scYear);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "List of Year Booking By Partners", scYearPartner);
    
        }
    }
}