﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalBasketSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalBasketSublayout" %>
<%@ Import Namespace="Sitecore.Data.Items" %>
<%@ Import Namespace="Affinion.LoyaltyBuild.Common.SessionHelper" %>
<%@ Import Namespace="Affinion.LoyaltyBuild.Common.SessionHelper" %>
<style type="text/css">
    .link {
        color: red;
        text-decoration: none;
        background-color: none;
    }

    .accmdtmsg {
        color: red;
        float: left;
        font-size: 13px;
        margin-left: 37px;
        margin-top: 6px;
    }
</style>
<div>
    <div style="visibility: hidden; margin-top: 60px"></div>
    <div class="container booking-detail-info no-padding">
        <div class="content">
            <asp:Repeater ID="PanelRepeater" runat="server" OnItemDataBound="PanelRepeater_ItemDataBound">
                <ItemTemplate>
                    <div class="panel panel-default">
                        <div class="panel-heading bg-primary">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href='#<%# Eval("SitecoreItem.Name") + "" +  Eval("Sku")%>' class="show-hide-list">
                                    <sc:text item="<%#(Item) ((BasketInfo)Container.DataItem).SitecoreItem %>" id="PanelTitle" field="Name" runat="server" />
                                </a>
                            </h4>
                        </div>
                        <div id='<%# Eval( "SitecoreItem.Name") + "" +  Eval("Sku")%>' class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div>
                                    <div class="col-sm-12 bask-title">
                                        <div>
                                            <div class="hotel-name-detail">
                                                <h2 class="no-margin">
                                                    <asp:Label ID="lblhotelname" runat="server"></asp:Label>
                                                    <sc:text item="<%#(Item) ((BasketInfo)Container.DataItem).SitecoreItem %>" id="StarRanking" field="StarRanking" runat="server" visible="False" />
                                                    <asp:Repeater ID="StarRepeater" runat="server">
                                                        <ItemTemplate>
                                                            <span class="fa fa-star"></span>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </h2>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-6">
                                            <div class="content_bg_inner_box alert-info">
                                                <div class="col-md-12">
                                                    <div class="title">
                                                        <h2 class="no-margin">Booking Detail</h2>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="col-md-6 col-xs-6 no-padding">
                                                        <span class="tit accent33-f">Check-in :</span>
                                                    </div>
                                                    <div class="col-md-6 col-xs-6 no-padding">
                                                        <span>
                                                            <%#((BasketInfo)Container.DataItem).CheckinDate %>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="col-md-6 col-xs-6 no-padding">
                                                        <span class="tit accent33-f">Check-out :</span>
                                                    </div>
                                                    <div class="col-md-6 col-xs-6 no-padding">
                                                        <span><%#((BasketInfo)Container.DataItem).CheckoutDate %></span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="col-md-6 col-xs-6 no-padding">
                                                        <span class="tit accent33-f">Nights:
                                                        </span>
                                                    </div>
                                                    <div class="col-md-6 col-xs-6 no-padding">
                                                        <span><%#((BasketInfo)Container.DataItem).Nights %></span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="col-md-6 col-xs-6 no-padding">
                                                        <span class="tit accent33-f">Room Type :</span>
                                                    </div>
                                                    <div class="col-md-6 col-xs-6 no-padding">
                                                        <span><%#((BasketInfo)Container.DataItem).RoomType%></span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="col-md-6 col-xs-6 no-padding">
                                                        <span class="tit accent33-f">Rooms :</span>
                                                    </div>
                                                    <div class="col-md-6 col-xs-6 no-padding">
                                                        <span><%#((BasketInfo)Container.DataItem).NoOfRooms %></span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="col-md-6 col-xs-6 no-padding">
                                                        <span class="tit accent33-f">Price :</span>
                                                    </div>
                                                    <div class="col-md-6 col-xs-6 no-padding">
                                                        <span><%#((BasketInfo)Container.DataItem).Price %></span>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="col-md-6 col-xs-6 no-padding">
                                                        <button id="ButtonSetAccomodation" type="button" onclick="OpenPopup('<%#((BasketInfo)Container.DataItem).OrderLineId %>' , '<%#((BasketInfo)Container.DataItem).NoOfRooms %>')" class="btn btn-default add-break margin-3"><span class="fa fa-unlink"></span><%=SetAccommodationText%> </button>
                                                    </div>
                                                    <div class="col-md-6 col-xs-6 no-padding" id="">
                                                        <input runat="server" id="validateacc" type="checkbox" checked="<%#(string.IsNullOrEmpty(((BasketInfo)Container.DataItem).GuestName)?false:true) %>" data-validate="accomodation" data-olid="<%#((BasketInfo)Container.DataItem).OrderLineId %>" style="display: none;" />
                                                        <span style="display: none;" class="required martop10 accmdtmsg" id="spanAccomodationValidation" data-olid="<%#((BasketInfo)Container.DataItem).OrderLineId %>"><%=AccomodationDetailsRequiredMsg%></span>
                                                    </div>
                                                    <div class="col-md-6 col-xs-6 no-padding">
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 martop10">
                                                    <div class="no-margin  col-md-6 col-xs-6 no-padding">
                                                        <span class="glyphicon glyphicon-edit">
                                                            <asp:LinkButton ID="EditLinkButton" Text="Edit" runat="server" class="gray-color" />
                                                        </span>
                                                    </div>
                                                    <div class="no-margin  col-md-6 col-xs-6 no-padding">
                                                        <span class="glyphicon glyphicon-trash" style="color: red"></span>
                                                        <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="return RemoveConfirmation();" OnClick="lblRemove_Click" CommandArgument='<%#Eval("Sku") %>' CssClass="link"><%=RemoveBasketText %></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-6  no-padding-left"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
           <%-- <input type="hidden" runat="server" id="hdnValidateAcc" />--%>
            <asp:TextBox id="hdnValidateAcc" style="display:none;" runat="server"/>
            <asp:CustomValidator id="GuestName" ValidateEmptyText="true" ControlToValidate="hdnValidateAcc" ValidationGroup="PartnerValidation" ClientValidationFunction="IsValidAccomation"  Display="None" ErrorMessage=""  runat="server"/>
            <div class="alert alert-danger msg-margin-top" id="DivEmptyBasket" runat="server" visible="false">
                <asp:Label ID="EmptyBasket" runat="server" Text=""></asp:Label>
            </div>
        </div>
        <div>
            <div class="no-padding col-md-6 col-md-offset-3" id="subrateSection" runat="server">
                <div class="content_bg_inner_box no-margin alert-info">
                    <div class="col-xs-12">
                        <div class="col-xs-8 no-padding">
                            <span class="accent33-f"><%=ProcessingFeeText%></span>
                        </div>
                        <div class="col-xs-4 no-padding">
                            <asp:Literal ID="ProcessingFee" runat="server" />
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="col-xs-8 no-padding">
                            <span class="accent33-f"><%=TotalPayableTodayText%></span>
                        </div>
                        <div class="col-xs-4 no-padding">
                            <asp:Literal ID="BookingDeposit" runat="server" />
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="col-xs-8 no-padding">
                            <span></span>
                            <div>
                                <asp:Literal ID="PayableAtAccommodationText" runat="server" />
                            </div>
                        </div>
                        <div class="col-xs-4 no-padding">
                            <span>
                                <asp:Literal ID="TotalPayableAccommodation" runat="server" />
                            </span>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="col-xs-8 no-padding">
                            <span class="accent33-f"><%=TotalPriceText%></span>
                        </div>
                        <div class="col-xs-4 no-padding">
                            <asp:Literal ID="TotalPrice" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 no-padding martop10" id="showAdditionalDetails" runat="server">
            <div class="panel panel-default" runat="server" visible="false">
                <div class="panel-heading">
                    <h4 class="panel-title">Additional Information</h4>
                </div>
                <div class="panel-body alert-info">
                    <div class="row margin-row">
                        <div class="col-sm-5"><span class="bask-form-titl font-size-12">Promotion Method:</span></div>
                        <div class="col-sm-7">
                            <select class="form-control font-size-12">
                                <option>Promotion Method</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                            </select>
                        </div>
                    </div>
                    <div class="row margin-row">
                        <div class="col-sm-5"><span class="bask-form-titl font-size-12">Card Location:</span></div>
                        <div class="col-sm-7">
                            <input type="text" class="form-control font-size-12" placeholder="Card Location">
                        </div>
                    </div>
                    <div class="row margin-row">
                        <div class="col-sm-5"><span class="bask-form-titl font-size-12">Is SMS Requested:</span></div>
                        <div class="col-sm-7">
                            <label class="checkbox-inline">
                                <input type="checkbox" value="">Yes
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" value="">No
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default" id="PaymentSection" runat="server" visible="false">
                <div class="panel-heading">
                    <h4 class="panel-title"><%=PaymentText%></h4>
                </div>
                <div class="panel-body">
                    <div class="col-md-12 no-padding">
                        <asp:Button runat="server" ClientIDMode="Static" type="submit" ID="reCalculateButton" OnClick="reCalculateOnclick" class="btn btn-default add-break pull-right margin-row font-size-12" Text="Re-Calculate Payment"></asp:Button>
                        <asp:Button runat="server" ClientIDMode="Static" type="submit" ID="paymentButton" OnClick="customerValidationOnclick" class="btn btn-default add-break pull-right margin-row font-size-12 marR5" Text="Continue To Payment"></asp:Button>
                    </div>
                    <div class="col-md-6 col-xs-12 no-padding">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><%=PaymentPortionText%></h4>
                            </div>
                            <div class="panel-body  alert-info">
                                <div class="paymentPortion">
                                    <label class="checkbox-inline font-size-12">
                                        <asp:CheckBox ID="fullCheckbox" runat="server" Text="Full" />
                                    </label>
                                </div>
                                <div class="paymentPortion">
                                    <label class="checkbox-inline font-size-12">
                                        <asp:CheckBox ID="depositCheckBox" runat="server" Text="Deposit" />
                                    </label>
                                </div>
                                <div class="paymentPortion">
                                    <label class="checkbox-inline font-size-12">
                                        <asp:CheckBox ID="partialCheckBox" runat="server" Text="Partial" />
                                        <asp:Literal ID="LitInst" runat="server" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12 no-padding-right">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><%=PaymentMethodText%></h4>
                            </div>
                            <div class="panel-body  alert-info">
                                <div class="paymentMethod">
                                    <label class="checkbox-inline font-size-12">
                                        <asp:CheckBox ID="creditDebitCheckBox" runat="server" Text="Credit/Laser/Debit Card" />
                                    </label>
                                </div>
                                <div class="paymentMethod">
                                    <label class="checkbox-inline font-size-12">
                                        <asp:CheckBox ID="chequeCheckBox" runat="server" Text="Cheque" />
                                    </label>
                                </div>
                                <div class="paymentMethod">
                                    <label class="checkbox-inline font-size-12">
                                        <asp:CheckBox ID="cashCheckBox" runat="server" Text="Cash" />
                                    </label>
                                </div>
                                <div class="paymentMethod">
                                    <label class="checkbox-inline font-size-12">
                                        <asp:CheckBox ID="poCheckBox" runat="server" Text="Post Office / Bank Transfer" />
                                    </label>
                                </div>
                                <div class="paymentMethod">
                                    <label class="checkbox-inline font-size-12">
                                        <asp:CheckBox ID="voucherCheckBox" runat="server" Text="Voucher & Other" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <span class="basketPageErrorMessage" style="color: red"></span>
                </div>
            </div>
        </div>
    </div>
</div>
<script lang="javascript" type="text/javascript">
    function RemoveConfirmation() {
        var message = "<%=ConfirmationMessage%>";
        return confirm(message);
    }

    function OpenPopup(OrderlineId, NoOfRooms) {
        var GuestRange = "<%=GuestCount%>";
       window.open("/set-accommodation?Rooms=" + NoOfRooms + "&GuestRange=" + GuestRange + "&orderid=" + OrderlineId + "+", "List", "toolbar=no, location=no,status=yes,menubar=no,scrollbars=yes,resizable=no, width=900,height=600,left=430,top=100");
       return false;
   }

   $(function () {
       $("#reCalculateButton").attr("disabled", "disabled");
       /*script for check box basket page-Offline Portal*/
       $('.paymentPortion input[type = checkbox]').change(function () {
           $("#reCalculateButton").removeAttr("disabled");
           $("#paymentButton").attr("disabled", "disabled");
           if (this.checked) {
               $('.paymentPortion input[type = checkbox]').prop('checked', false);
               $(this).prop('checked', true);
           }

       });
       /*script for check box basket page-Offline Portal*/
       $('.paymentMethod input[type = checkbox]').change(function () {
           $("#reCalculateButton").removeAttr("disabled");
           $("#paymentButton").attr("disabled", "disabled");
           if (this.checked) {
               $('.paymentMethod input[type = checkbox]').prop('checked', false);
               $(this).prop('checked', true);
           }

       });

       $('#reCalculateButton').click(function (e) {
           if (!ValidatePaymentSelection()) {
               e.preventDefault();
           }
       });

       $("#paymentButton").click(function (e) {
           if (!ValidatePaymentSelection()) {
               e.preventDefault();
           }
           $("#hdValidate").val("Y");
       });
       $('.personalDetailValidation').click(function (e) {
           for (var i = 0; i < Page_Validators.length; i++) {
               if (!Page_Validators[i].isvalid) {
                   $("#" + Page_Validators[i].controltovalidate)
                    .addClass("has-error-input");
                                     
               }
               else {
                   $("#" + Page_Validators[i].controltovalidate)
                    .removeClass("has-error-input");
               }
           }
       });
   });
   

   function IsValidAccomation(oSrc, args) {
       args.IsValid = SetAccomodationValidation();
   }

   

   /* Script for payment check box validation- basket-offline portal*/
   function ValidatePaymentSelection() {
       var paymentMethodMsg = "<%=SelectPaymentMethodMsg%>";
           var paymentPortionMsg = "<%=SelectPaymentPortionMsg%>";
           var paymentMethodcheckboxes = $('.paymentMethod input[type="checkbox"]');
           var paymentPortioncheckboxes = $('.paymentPortion input[type="checkbox"]');
           var countCheckedboxPaymentmethod = paymentMethodcheckboxes.filter(':checked').length;
           var countCheckedboxPaymentPortion = paymentPortioncheckboxes.filter(':checked').length;
           if (countCheckedboxPaymentmethod < 1)
               $('.basketPageErrorMessage').text(paymentMethodMsg);
           else if (countCheckedboxPaymentPortion < 1)
               $('.basketPageErrorMessage').text(paymentPortionMsg);

           return (SetAccomodationValidation() && countCheckedboxPaymentmethod == 1 && countCheckedboxPaymentPortion == 1);
       }

       function SetAccomodationValidation() {
           var checkedCount = 0;
           $('input[data-olid]').each(function () {
               var olid = $(this).data("olid");
               if (!$(this).prop("checked")) {
                   checkedCount++;
                   $("span[data-olid='" + olid + "']").css("display", "block");
               }
           });
           return checkedCount == 0;
       }

</script>
