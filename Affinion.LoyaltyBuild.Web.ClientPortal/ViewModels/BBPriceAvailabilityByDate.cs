﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.ViewModels
{
    public class BBPriceAvailabilityByDate : PriceAvailabilityByDate
    {
        public string ProviderId { get; set; }

        public string PreBookingToken { get; set; }

        public string MealBasisId { get; set; }

        public string PropertyReferenceId { get; set; }

        public string RoomInfo { get; set; }

        public decimal OldPrice { get; set; }

        public int MarginId { get; set; }

        public decimal MarginPercentage { get; set; }

        public string BBCurrencyId { get; set; }

        public bool IsDirectStock { get; set; }
    }
}