﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Maps
{
    public class ProcessingFeeAwardMap : SubclassMap<ProcessingFeeAward>
    {
        // Methods
        public ProcessingFeeAwardMap()
        {
            base.Table("LB_ProcessingFeeAward");
            base.KeyColumn("ProcessingFeeAwardId");
            base.Map(x => x.ProcessingFeeAwardId).ReadOnly();
            base.Map(x => x.DiscountAmount).Not.Nullable();
            base.Map(x => x.IsPercentage).Not.Nullable();
            base.Map(x => x.OnOrderLine).Not.Nullable();
        }

    }

}
