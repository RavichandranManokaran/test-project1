﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.Api;
using UCommerce.EntitiesV2;
using UCommerce.Extensions;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.Controls
{
	public partial class ProductDetails : System.Web.UI.UserControl
	{
	    public Product Product { get; set; }
		protected void Page_Load(object sender, EventArgs e)
		{
		    ProductSkuLabel.Text = Product.Sku;
            ProductDetailsLink.NavigateUrl =
		        "/store/product.aspx?product={0}".FormatWith(Product.ProductId);

		    PriceCalculation priceCalc = CatalogLibrary.CalculatePrice(Product);
		    ProductPriceLabel.Text = priceCalc.YourPrice.Amount.ToString();
		}
	}
}