﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Subrate
{
    public interface ISubrate
    {
        /// <summary>
        /// GetAllSubrateItems
        /// </summary>
        /// <returns></returns>
        List<SubrateItemDetail> GetAllSubRateItems();

        /// <summary>
        /// GetAllClient
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="current"></param>
        /// <returns></returns>
        List<ClientDetail> GetAllClient(string itemId,Sitecore.Data.Database current);

        /// <summary>
        /// GetAllSubrateTemplate
        /// </summary>
        /// <returns></returns>
        List<SubrateTemplatesDetail> GetAllSubRateTemplate();

        /// <summary>
        /// GetAllCurrency
        /// </summary>
        /// <param name="_database"></param>
        /// <returns></returns>
        List<CurrencyDetail> GetAllCurrency(Sitecore.Data.Database _database);

        /// <summary>
        /// GetSubRateByItemId
        /// </summary>
        /// <param name="ItemID"></param>
        /// <returns></returns>
        List<SubratesDetail> GetSubRateByItemId(string ItemID);

        /// <summary>
        /// GetSubRateByClientAndItemId
        /// </summary>
        /// <param name="ItemID"></param>
        /// <param name="clientId"></param>
        /// <param name="currency"></param>
        /// <returns></returns>
        List<SubratesDetail> GetSubRateByClientAndItemId(string ItemID, string clientId, string currency);

        /// <summary>
        /// SaveSubrates
        /// </summary>
        /// <param name="objSubrates"></param>
        /// <param name="ItemID"></param>
        /// <returns></returns>
        int SaveSubrates(List<SubratesDetail> objSubrates, string ItemID);

        /// <summary>
        /// DeleteSubrate
        /// </summary>
        /// <param name="ItemID"></param>
        /// <param name="ClientID"></param>
        /// <returns></returns>
        int DeleteSubrate(string ItemID, string ClientID = "Admin");

        /// <summary>
        /// SubrateTemplate
        /// </summary>
        /// <param name="objsrtemplate"></param>
        /// <returns></returns>
        string SaveTemplate(SubrateTemplatesDetail objsrtemplate);

        /// <summary>
        /// DeleteTemplate
        /// </summary>
        /// <param name="SubrateTemplateID"></param>
        /// <returns></returns>
        int DeleteTemplate(string SubrateTemplateID);

        /// <summary>
        /// List of OrderLineSubrate
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="clientId"></param>
        /// <param name="currency"></param>
        /// <returns></returns>
        List<OrderLineSubrate> GetOrderSubrate(int orderId, string clientId, string currency = "GBP");

        /// <summary>
        /// GetOrderSubrate
        /// </summary>
        /// <param name="objSubrates"></param>
        /// <param name="ItemID"></param>
        /// <returns></returns>
        int SaveSubratesAward(List<SubratesDetail> objSubrates, string ItemID);

        /// <summary>
        /// List of SubrateDetails
        /// </summary>
        /// <param name="ItemID"></param>
        /// <param name="clientId"></param>
        /// <param name="currency"></param>
        /// <returns></returns>
        List<SubratesDetail> GetSubRateAwardByClientAndItemId(string ItemID, string clientId, string currency);


    }
}
