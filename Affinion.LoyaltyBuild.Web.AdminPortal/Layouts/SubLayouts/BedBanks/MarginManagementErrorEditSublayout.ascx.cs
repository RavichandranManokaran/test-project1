﻿using Affinion.LoyaltyBuild.BedBanks.Troika;
using Affinion.LoyaltyBuild.Common.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.BedBanks
{
    public partial class MarginManagementErrorEditSublayout : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string pageUrl = SitecoreFieldsHelper.GetItemFieldValue(Sitecore.Context.Item, "Margin Search Page Link");
                if(!string.IsNullOrEmpty(pageUrl))
                  backLink.NavigateUrl = pageUrl;
            }
            QueryStringHelper helper = new QueryStringHelper(Request.Url);
            string list = helper.GetValue("ListId");
            if (!IsPostBack)
            {
                ViewState["PreviousPage"] = Request.UrlReferrer;
                DataTable errorList = TroikaDataController.GetMarginErrorList();
                if (errorList != null)
                {
                    ddlErrorList.DataSource = errorList.DefaultView;
                    ddlErrorList.DataTextField = "ListName";
                    ddlErrorList.DataValueField = "ListID";
                    ddlErrorList.DataBind();
                }

                if (!string.IsNullOrEmpty(list))
                {
                    ddlErrorList.SelectedIndex = ddlErrorList.Items.IndexOf(ddlErrorList.Items.FindByValue(list));
                    int listId;
                    int.TryParse(list, out listId);
                    DataTable providerList = TroikaDataController.GetProviderDetailsFromErrorList(listId);
                    if (providerList != null)
                    {
                        gvSearchData.DataSource = providerList;
                        gvSearchData.DataBind();
                    }
                }
            }
        }

        protected void gvSearchData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName != "EditMargin") return;
            string queryString = e.CommandArgument.ToString();
            string listId = ddlErrorList.SelectedItem.Value;
            string previousPage = SitecoreFieldsHelper.GetItemFieldValue(Sitecore.Context.Item, "Margin Edit Page Link");

            Response.Redirect(string.Format("{0}?propertyId={1}&ErrorListId={2}&type=add", previousPage, queryString, listId));
        }

        protected void ddlErrorList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string list = ddlErrorList.SelectedItem.Value;
            int listId;
            int.TryParse(list, out listId);
            DataTable providerList = TroikaDataController.GetProviderDetailsFromErrorList(listId);
            if (providerList != null)
            {
                gvSearchData.DataSource = providerList;
                gvSearchData.DataBind();
            }
        }
    }
}