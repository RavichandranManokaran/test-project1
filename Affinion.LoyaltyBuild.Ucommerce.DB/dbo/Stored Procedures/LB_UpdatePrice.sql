﻿-- exec UpdatePrice 9190,1,10,20,'1B252FB2-E612-4132-B296-C04B6AC74456','DI001'

Create procedure UpdatePrice

@datecounter int ,



@IsAddOn bit,



@TotalPrice decimal,



@PackagePrice decimal,



@PriceBand uniqueidentifier,



@AddOnSku varchar(50)



AS

declare @PriceDiff decimal

declare @oldprice decimal

if(@isaddon = 1)



begin
DECLARE @tempCTE table (SKU varchar(30), VariantSKU varchar(30))

insert into  @tempCTE
select up.Sku ,up.VariantSku from uCommerce_Product up
inner join uCommerce_ProductProperty upp on up.ProductId = upp.productid
inner join uCommerce_ProductDefinitionField updf on upp.ProductDefinitionFieldId = updf.ProductDefinitionFieldId
where updf.Name ='RelatedAddOnSKU' and upp.value like '%'+ @AddOnSku +'%'


select top 1 @oldprice = (PackagePrice - Price)    from LB_RoomAvailability ra 
join @tempCTE  t on ra.Sku = t.SKU and ra.variantsku = t.VariantSKU
where ra.DateCounter >= @datecounter

set @PriceDiff = @PackagePrice - @oldprice

update LB_RoomAvailability

set PackagePrice = PackagePrice+ @PriceDiff

from LB_RoomAvailability ra 

join @tempCTE  t on ra.Sku = t.SKU and ra.variantsku = t.VariantSKU

where ra.DateCounter >= @datecounter



select top 1 @oldprice = price from  LB_RoomAvailability

where DateCounter>=@datecounter and VariantSku = @AddOnSku



set @PriceDiff = @TotalPrice - @oldprice



update LB_RoomAvailability

set Price = Price + @PriceDiff,

PackagePrice= case when PackagePrice = 0 then 0 else PackagePrice + @PriceDiff end 

where DateCounter>=@datecounter and VariantSku = @AddOnSku

end



else

begin



--select * from LB_RoomAvailability



select top 1 @oldprice = price from  LB_RoomAvailability

where DateCounter>=@datecounter and PriceBand = @PriceBand

set @PriceDiff = @TotalPrice - @oldprice



update LB_RoomAvailability

set Price = Price + @PriceDiff,

PackagePrice= case when PackagePrice = 0 then 0 else PackagePrice + @PriceDiff end 

where PriceBand = @PriceBand and DateCounter >=@datecounter

end




