﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BreadcrumbSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.BreadcrumbSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="col-md-12">
    <div class="breadcrumb-top">
        <ol class="breadcrumb">
            <li><a href="home.html">Home</a></li>
            <li class="active">Location Finder </li>
        </ol>
    </div>
</div>
