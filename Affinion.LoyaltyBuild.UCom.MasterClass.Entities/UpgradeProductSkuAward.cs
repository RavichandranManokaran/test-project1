﻿


using Affinion.LoyaltyBuild.UCom.Common.Constants;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce;
using UCommerce.Catalog;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;
using UCommerce.Runtime;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class UpgradeProductSkuAward : Award, IEntity
    {
        // Fields
        private int? _hashCode;
        // Methods
        public new static IQueryable<UpgradeProductSkuAward> All()
        {
            return GetRepo().Select();
        }

        public override void Apply(PurchaseOrder order, IList<OrderLine> orderLines)
        {
            bool skuReplace = false;
            List<string> data = this.UpgradedRoomType.Split('|').Where(y => !string.IsNullOrEmpty(y)).ToList();
            foreach (OrderLine line in orderLines)
            {
                Product availableProduct = ProductLibrary.GetAvilabilityBySku(line.Sku, this.UpgradedRoomType);
                List<OrderProperty> orderLineProperty = line.OrderProperties.ToList();
                if (availableProduct != null)
                {
                    int index = orderLineProperty.FindIndex(x => x.Key.Equals(OrderPropertyConstants.UpgradeSku));
                    if (index != -1 && line.OrderProperties.ElementAt(index).Value == "true")
                    {
                        var priceGroup = SiteContext.Current.CatalogContext.CurrentPriceGroup;
                        IPricingService pricingService = ObjectFactory.Instance.Resolve<IPricingService>();
                        Money productPrice = pricingService.GetProductPrice(availableProduct, priceGroup);
                        if ((productPrice != null) && (priceGroup != null))
                        {
                            ITaxService taxService = ObjectFactory.Instance.Resolve<ITaxService>();
                            Money money2 = taxService.CalculateTax(availableProduct, priceGroup, productPrice);
                            Currency currency = productPrice.Currency ?? Currency.All().First<Currency>();
                            Money money3 = new Money(productPrice.Value, currency);
                            decimal discountOffer = (money3.Value) - (line.Price);
                            var discount = new Discount
                            {
                                CampaignName = CampaignItem.Campaign.Name,
                                CampaignItemName = CampaignItem.Name,
                                Description = Name,
                                AmountOffTotal = discountOffer
                            };
                            line.Sku = availableProduct.Sku;
                            line.ProductName = availableProduct.Name;
                            line.Price = money3.Value;
                            line.Total = line.Price + line.VAT;
                            //line.VAT = money2.Value;
                            line.AddDiscount(discount);
                            order.AddDiscount(discount);
                            skuReplace = true;
                        }
                    }
                    else
                    {
                        line.AddOrderProperty(new OrderProperty() { Key = OrderPropertyConstants.UpgradeAvailable, Value = "true" });
                        line.AddOrderProperty(new OrderProperty() { Key = OrderPropertyConstants.UpgradeMessage, Value = this.UpgradedRoomType });
                    }
                }
                if (skuReplace)
                {
                    int index = orderLineProperty.FindIndex(x => x.Key.Equals(OrderPropertyConstants.UpgradeSku));
                    if (index != -1)
                    {
                        line.OrderProperties.ElementAt(index).Value = "false";
                        index = orderLineProperty.FindIndex(x => x.Key.Equals(OrderPropertyConstants.UpgradeAvailable));
                        if (index != -1)
                        {
                            line.OrderProperties.ElementAt(index).Value = "false";
                        }
                    }
                }
            }


        }

        private bool UpgradeProductSkuAwarded(OrderLine orderLines)
        {
            return orderLines.OrderProperties.Any(y => (y.Key == OrderPropertyConstants.FreeSku || y.Key == OrderPropertyConstants.FreeSkuApplied) && y.Value == "true");
        }

        public override void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<UpgradeProductSkuAward, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            UpgradeProductSkuAward objA = obj as UpgradeProductSkuAward;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<UpgradeProductSkuAward, bool>> expression)
        {
            return All().Any<UpgradeProductSkuAward>(expression);
        }

        public static IList<UpgradeProductSkuAward> Find(Expression<Func<UpgradeProductSkuAward, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<UpgradeProductSkuAward>();
        }

        public static UpgradeProductSkuAward FirstOrDefault(Expression<Func<UpgradeProductSkuAward, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<UpgradeProductSkuAward>();
        }

        public new static UpgradeProductSkuAward Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<UpgradeProductSkuAward> GetRepo()
        {
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<UpgradeProductSkuAward>>();
            
        }

        public override void Save()
        {
            GetRepo().Save(this);
        }

        public static UpgradeProductSkuAward SingleOrDefault(Expression<Func<UpgradeProductSkuAward, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties
        public virtual string UpgradedRoomType { get; set; }

        public virtual int UpgradeProductSkuAwardId { get; protected set; }

        public override int Id
        {
            get
            {
                return this.UpgradeProductSkuAwardId;
            }
        }
    }




}
