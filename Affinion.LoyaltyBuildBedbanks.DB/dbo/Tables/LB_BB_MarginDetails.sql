
/****** Object:  Table [dbo].[LB_BB_MarginDetails]    Script Date: 7/19/2016 8:00:51 PM ******/

CREATE TABLE [dbo].[LB_BB_MarginDetails](
	[MarginID] [int] IDENTITY(1,1) NOT NULL,
	[MarginType] [varchar](200) NULL,
	[MarginPercentage] [decimal](18, 0) NULL,
	[ProviderID] [varchar](200) NULL,
	[StartDate] [date] NULL,
	[EndDate] [date] NULL,
	[IsActive] [bit] NULL,
	[IsGlobal] [bit] NULL,
	[DateAdded] [datetime] NULL,
	[DateModified] [datetime] NULL,
	[DateEnabled] [datetime] NULL,
	[DateDisabled] [datetime] NULL,
	[DateApproved] [datetime] NULL,
	[CreatedBy] [varchar](100) NULL,
	[ApprovedBy] [varchar](100) NULL
) ON [PRIMARY]
GO
--INSERT INTO LB_BB_MarginDetails(MarginType,MarginPercentage,CreatedBy) values ('Default Margin',10,'admin')
--GO
