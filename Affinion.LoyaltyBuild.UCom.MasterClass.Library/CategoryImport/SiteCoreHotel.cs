﻿using Affinion.LoyaltyBuild.UCom.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using UCommerce.Pipelines;
using UCommerce.Infrastructure.Globalization;
using UCommerce.Infrastructure;
/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.CategoryImport
{
    /// <summary>
    /// Service that provides methods to perform SiteCoreHotel related actions
    /// </summary>
    public class SiteCoreHotel : ISiteCoreHotel
    {
        private readonly IPipeline<Product> _saveProductPipeline;
        public int parentCategoryId { get; set; }
        public bool independentHotel { get; set; }
        public int independentCategoryId { get; set; }
        public bool groupHotel { get; set; }
        /// <summary>
        /// SaveSiteCountry 
        /// </summary>
        /// <param name="countryDetail">countryDetail</param>
        /// <returns>save country id</returns>

        public LBProductInfo SaveSiteCoreHotel(Model.LBHotelDetail hotelDetail)
        {
            try
            {
                LBProductInfo shotelDetail = new LBProductInfo();
                ProductCatalog catelog = null;
                Product existingProduct = !string.IsNullOrEmpty(hotelDetail.SKU) ? Product.All().Where(x => x.Sku == hotelDetail.SKU && x.ParentProduct == null).FirstOrDefault() : null;
                List<Category> relatedCategory = new List<Category>();
                if (existingProduct != null)
                {
                    relatedCategory = existingProduct.GetCategories().ToList();
                    if (existingProduct != null)
                    {
                        bool removeCategory = false;
                        ProductProperty cProperty = existingProduct.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationConstant.Country);
                        if (cProperty != null)
                        {
                            if (!cProperty.Value.Equals(hotelDetail.Country, StringComparison.InvariantCultureIgnoreCase))
                            {
                                removeCategory = true;
                            }
                        }
                        else
                        {
                            removeCategory = true;
                        }

                        ProductProperty locationProperty = existingProduct.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationConstant.Location);
                        if (locationProperty != null)
                        {
                            if (!locationProperty.Value.Equals(hotelDetail.Location, StringComparison.InvariantCultureIgnoreCase))
                            {
                                removeCategory = true;
                            }
                        }
                        else
                        {
                            removeCategory = true;
                        }

                        if (removeCategory)
                        {
                            foreach (var item in relatedCategory)
                            {
                                existingProduct.RemoveCategory(item);
                            }
                            existingProduct.Save();
                        }
                    }
                }
                if (hotelDetail.CatalogId == null)
                {
                    hotelDetail.CatalogId = new List<int>();
                }
                if (relatedCategory.Count > 0)
                {
                    hotelDetail.CatalogId.AddRange(relatedCategory.Select(x => x.ProductCatalog.ProductCatalogId));
                }
                if (hotelDetail.CatalogId == null || hotelDetail.CatalogId.Count == 0)
                {
                    catelog = ProductCatalog.FirstOrDefault(x => x.Name.ToLower() == "loyaltybuild");
                    hotelDetail.CatalogId = new List<int>();
                    hotelDetail.CatalogId.Add(catelog.ProductCatalogId);
                }
                hotelDetail.CatalogId = hotelDetail.CatalogId.Distinct().ToList();
                foreach (var item in hotelDetail.CatalogId)
                {
                    existingProduct = !string.IsNullOrEmpty(hotelDetail.SKU) ? Product.All().Where(x => x.Sku == hotelDetail.SKU && x.ParentProduct == null).FirstOrDefault() : null;
                    catelog = ProductCatalog.Get(item);
                    if (catelog == null)
                    {
                        catelog = ProductCatalog.All().FirstOrDefault();
                    }
                    Category countryCategory = Category.All().Where(x => x.Name.ToLower() == hotelDetail.Country.ToLower() && x.ProductCatalog == catelog).FirstOrDefault();
                    if (countryCategory == null)
                    {
                        countryCategory = CreateCategory(catelog, hotelDetail.Country);
                    }
                    Category cityCategory = Category.All().Where(x => x.Name.ToLower() == hotelDetail.Location.ToLower() && x.ProductCatalog == catelog && x.ParentCategory.CategoryId == countryCategory.CategoryId).FirstOrDefault();
                    if (cityCategory == null)
                    {
                        cityCategory = CreateCategory(catelog, hotelDetail.Location, countryCategory);
                    }

                    //Product existingProduct = Product.All().Where(x => x.ProductProperties.Any(y => y.ProductDefinitionField.Name.Equals(DefinationConstant.SiteCoreItemGuid) && y.Value == hotelDetail.SiteCoreItemId)).FirstOrDefault();
                    Product product = existingProduct ?? new Product();
                    product.Name = hotelDetail.HotelName;
                    if (existingProduct == null)
                    {
                        product.AddCategory(cityCategory, 1);
                    }
                    else
                    {
                        List<Category> categoryList = product.GetCategories().ToList();
                        if (!categoryList.Any(x => x.CategoryId == cityCategory.Id))
                        {
                            product.AddCategory(cityCategory, 1);
                        }
                    }
                    if (product.Id == 0)
                    {
                        product.DisplayOnSite = false;
                        int maxProductId = Product.All().Max(x => x.ProductId);
                        product.Sku = string.Format("{0}_{1}", hotelDetail.HotelName.Length > 2 ? hotelDetail.HotelName.Substring(0, 2) : hotelDetail.HotelName, maxProductId + 1);
                    }
                    ProductDefinition productDefination = GetProductDefinition(ref product);

                    SaveLocationDefination(hotelDetail, product, productDefination);

                    //Save New Hotel Type

                    SaveNewHotelType(hotelDetail, product, productDefination);
                    // Code Commented for client bcz its map on item saving
                    //ProductProperty clientProperty = category.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == DefinationConstant.Client);
                    //if (clientProperty != null)
                    //{
                    //    clientProperty.Value = hotelDetail.Client ?? string.Empty;
                    //}
                    //else
                    //{
                    //    DefinitionField hotelClientDefination = DefinitionField.All().Where(x => x.Name == DefinationConstant.Client && !x.Deleted && x.ProductDefinition.ProductDefinitionId == hotelDefination.ProductDefinitionId).FirstOrDefault();
                    //    if (hotelClientDefination != null)
                    //        category.ProductProperties.Add(new ProductProperty() { Value = hotelDetail.Client, ProductDefinitionField = hotelClientDefination, Category = category });
                    //}

                    ProductProperty countryProperty = product.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationConstant.Country);
                    if (countryProperty != null)
                    {
                        countryProperty.Value = hotelDetail.Country;
                    }
                    else
                    {
                        ProductDefinitionField hotelCountryDefination = ProductDefinitionField.All().Where(x => x.Name == ProductDefinationConstant.Country && !x.Deleted && x.ProductDefinition.ProductDefinitionId == productDefination.ProductDefinitionId).FirstOrDefault();
                        if (hotelCountryDefination != null)
                            product.ProductProperties.Add(new ProductProperty() { Value = hotelDetail.Country, ProductDefinitionField = hotelCountryDefination, Product = product });
                    }



                    ProductProperty hotelGroupPro = product.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationConstant.Groups);
                    if (hotelGroupPro != null)
                    {
                        hotelGroupPro.Value = hotelDetail.Groups;
                    }
                    else
                    {
                        ProductDefinitionField hotelGroupDefination = ProductDefinitionField.All().Where(x => x.Name == ProductDefinationConstant.Groups && !x.Deleted && x.ProductDefinition.ProductDefinitionId == productDefination.ProductDefinitionId).FirstOrDefault();
                        if (hotelGroupDefination != null)
                            product.ProductProperties.Add(new ProductProperty() { Value = hotelDetail.Groups, ProductDefinitionField = hotelGroupDefination, Product = product });
                    }

                    ProductProperty hotelRegion = product.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationConstant.Region);
                    if (hotelRegion != null)
                    {
                        hotelRegion.Value = hotelDetail.Region;
                    }
                    else
                    {
                        ProductDefinitionField hotelRegionDefination = ProductDefinitionField.All().Where(x => x.Name == ProductDefinationConstant.Region && !x.Deleted && x.ProductDefinition.ProductDefinitionId == productDefination.ProductDefinitionId).FirstOrDefault();
                        if (hotelRegionDefination != null)
                            product.ProductProperties.Add(new ProductProperty() { Value = hotelDetail.Region, ProductDefinitionField = hotelRegionDefination, Product = product });
                    }

                    ProductProperty hotelSuperGroup = product.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationConstant.SuperGroups);
                    if (hotelSuperGroup != null)
                    {
                        hotelSuperGroup.Value = hotelDetail.SuperGroups;
                    }
                    else
                    {
                        ProductDefinitionField hotelSuperGroupDefination = ProductDefinitionField.All().Where(x => x.Name == ProductDefinationConstant.SuperGroups && !x.Deleted && x.ProductDefinition.ProductDefinitionId == productDefination.ProductDefinitionId).FirstOrDefault();
                        if (hotelSuperGroupDefination != null)
                            product.ProductProperties.Add(new ProductProperty() { Value = hotelDetail.SuperGroups, ProductDefinitionField = hotelSuperGroupDefination, Product = product });
                    }

                    ProductProperty hotelExperience = product.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationConstant.Experience);
                    if (hotelExperience != null)
                    {
                        hotelExperience.Value = hotelDetail.HotelExperience;
                    }
                    else
                    {
                        ProductDefinitionField hotelExperienceDefination = ProductDefinitionField.All().Where(x => x.Name == ProductDefinationConstant.Experience && !x.Deleted && x.ProductDefinition.ProductDefinitionId == productDefination.ProductDefinitionId).FirstOrDefault();
                        if (hotelExperienceDefination != null)
                            product.ProductProperties.Add(new ProductProperty() { Value = hotelDetail.HotelExperience, ProductDefinitionField = hotelExperienceDefination, Product = product });
                    }

                    ProductProperty hotelFacility = product.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationConstant.HotelFacility);
                    if (hotelFacility != null)
                    {
                        hotelFacility.Value = hotelDetail.HotelFacility;
                    }
                    else
                    {
                        ProductDefinitionField hotelFacilityDefination = ProductDefinitionField.All().Where(x => x.Name == ProductDefinationConstant.HotelFacility && !x.Deleted && x.ProductDefinition.ProductDefinitionId == productDefination.ProductDefinitionId).FirstOrDefault();
                        if (hotelFacilityDefination != null)
                            product.ProductProperties.Add(new ProductProperty() { Value = hotelDetail.HotelFacility, ProductDefinitionField = hotelFacilityDefination, Product = product });
                    }


                    ProductProperty hotelTheme = product.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationConstant.Theme);
                    if (hotelTheme != null)
                    {
                        hotelTheme.Value = hotelDetail.Theme;
                    }
                    else
                    {
                        ProductDefinitionField hotelThemeDefination = ProductDefinitionField.All().Where(x => x.Name == ProductDefinationConstant.Theme && !x.Deleted && x.ProductDefinition.ProductDefinitionId == productDefination.ProductDefinitionId).FirstOrDefault();
                        if (hotelThemeDefination != null)
                            product.ProductProperties.Add(new ProductProperty() { Value = hotelDetail.Theme, ProductDefinitionField = hotelThemeDefination, Product = product });
                    }

                    ProductProperty startRating = product.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationConstant.HotelRating);
                    if (startRating != null)
                    {
                        startRating.Value = hotelDetail.HotelRating;
                    }
                    else
                    {
                        ProductDefinitionField startRatingDefination = ProductDefinitionField.All().Where(x => x.Name == ProductDefinationConstant.HotelRating && !x.Deleted && x.ProductDefinition.ProductDefinitionId == productDefination.ProductDefinitionId).FirstOrDefault();
                        if (startRatingDefination != null)
                            product.ProductProperties.Add(new ProductProperty() { Value = hotelDetail.HotelRating, ProductDefinitionField = startRatingDefination, Product = product });
                    }

                    ProductProperty weekendSerchRestriction = product.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationConstant.WeekendSerchRestriction);
                    if (weekendSerchRestriction != null)
                    {
                        weekendSerchRestriction.Value = hotelDetail.WeekendSerchRestriction.ToString().ToLower();
                    }
                    else
                    {
                        ProductDefinitionField weekendSerchRestrictionDefination = ProductDefinitionField.All().Where(x => x.Name == ProductDefinationConstant.WeekendSerchRestriction && !x.Deleted && x.ProductDefinition.ProductDefinitionId == productDefination.ProductDefinitionId).FirstOrDefault();
                        if (weekendSerchRestrictionDefination != null)
                            product.ProductProperties.Add(new ProductProperty() { Value = hotelDetail.WeekendSerchRestriction.ToString().ToLower(), ProductDefinitionField = weekendSerchRestrictionDefination, Product = product });
                    }


                    hotelDetail.SKU = product.Sku;
                    //product.Save();
                    PipelineFactory.Create<Product>("SaveProduct").Execute(product);
                    shotelDetail.ProductId = product.ProductId;
                    shotelDetail.SKU = product.Sku;
                    return shotelDetail;
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.MasterClassBusinessLogic, ex, this);
            }
            return new LBProductInfo();
        }
        private ProductDefinition GetProductDefinition(ref Product product)
        {
            ProductDefinition hotelDefination = ProductDefinition.All().Where(x => x.Name == ProductDefinationConstant.Hotel).FirstOrDefault();
            if (product.ProductId == 0)
            {
                product.ProductDefinition = hotelDefination;
                product.CreatedBy = "admin";
                product.CreatedOn = DateTime.Now;
                product.Save();
            }
            //foreach (Language language in ObjectFactory.Instance.Resolve<ILanguageService>().GetAllLanguages())
            //{
            //    ProductDescription productDescription = new ProductDescription
            //    {
            //        DisplayName = product.Name,
            //        CultureCode = language.CultureCode,
            //        Product = product
            //    };
            //    product.AddProductDescription(productDescription);
            //}
            return hotelDefination;
        }


        private static void SaveLocationDefination(Model.LBHotelDetail hotelDetail, Product product, ProductDefinition hotelDefination)
        {
            ProductProperty itemGuidProperty = product.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationConstant.SiteCoreItemGuid);
            if (itemGuidProperty != null)
            {
                itemGuidProperty.Value = hotelDetail.SiteCoreItemId;
            }
            else
            {
                ProductDefinitionField siteCoreDefination = ProductDefinitionField.All().Where(x => x.Name == ProductDefinationConstant.SiteCoreItemGuid && !x.Deleted && x.ProductDefinition.ProductDefinitionId == hotelDefination.ProductDefinitionId).FirstOrDefault();
                if (siteCoreDefination != null)
                {
                    product.ProductProperties.Add(new ProductProperty() { Value = hotelDetail.SiteCoreItemId, ProductDefinitionField = siteCoreDefination, Product = product });
                }
            }

            ProductProperty locationProperty = product.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationConstant.Location);
            if (locationProperty != null)
            {
                locationProperty.Value = hotelDetail.Location;
            }
            else
            {
                ProductDefinitionField locationDefination = ProductDefinitionField.All().Where(x => x.Name == ProductDefinationConstant.Location && !x.Deleted && x.ProductDefinition.ProductDefinitionId == hotelDefination.ProductDefinitionId).FirstOrDefault();
                if (locationDefination != null)
                {
                    product.ProductProperties.Add(new ProductProperty() { Value = hotelDetail.Location, ProductDefinitionField = locationDefination, Product = product });
                }
            }
        }

        private static void SaveNewHotelType(Model.LBHotelDetail hotelDetail, Product product, ProductDefinition hotelDefination)
        {
            ProductProperty hotelTypeProperty = product.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationConstant.HotelType);
            if (hotelTypeProperty != null)
            {
                hotelTypeProperty.Value = hotelDetail.HotelType;
            }
            else
            {
                ProductDefinitionField hotelTypeDefination = ProductDefinitionField.All().Where(x => x.Name == ProductDefinationConstant.HotelType && !x.Deleted && x.ProductDefinition.ProductDefinitionId == hotelDefination.ProductDefinitionId).FirstOrDefault();
                if (hotelTypeDefination != null)
                    product.ProductProperties.Add(new ProductProperty() { Value = hotelDetail.HotelType, ProductDefinitionField = hotelTypeDefination, Product = product });
            }

            ProductProperty geoProperty = product.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationConstant.GeoCoOrdinate);
            if (geoProperty != null)
            {
                geoProperty.Value = hotelDetail.GeoCoOrdinate;
            }
            else
            {
                ProductDefinitionField geoDefination = ProductDefinitionField.All().Where(x => x.Name == ProductDefinationConstant.GeoCoOrdinate && !x.Deleted && x.ProductDefinition.ProductDefinitionId == hotelDefination.ProductDefinitionId).FirstOrDefault();
                if (geoDefination != null)
                    product.ProductProperties.Add(new ProductProperty() { Value = hotelDetail.GeoCoOrdinate, ProductDefinitionField = geoDefination, Product = product });
            }

            ProductProperty latituteProperty = product.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationConstant.Latitude);
            if (latituteProperty != null)
            {
                latituteProperty.Value = hotelDetail.Latitude;
            }
            else
            {
                ProductDefinitionField latituteDefination = ProductDefinitionField.All().Where(x => x.Name == ProductDefinationConstant.Latitude && !x.Deleted && x.ProductDefinition.ProductDefinitionId == hotelDefination.ProductDefinitionId).FirstOrDefault();
                if (latituteDefination != null)
                    product.ProductProperties.Add(new ProductProperty() { Value = hotelDetail.Latitude, ProductDefinitionField = latituteDefination, Product = product });
            }

            ProductProperty longitudeProperty = product.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationConstant.Longitute);
            if (longitudeProperty != null)
            {
                longitudeProperty.Value = hotelDetail.Longitude;
            }
            else
            {
                ProductDefinitionField longitudeDefination = ProductDefinitionField.All().Where(x => x.Name == ProductDefinationConstant.Longitute && !x.Deleted && x.ProductDefinition.ProductDefinitionId == hotelDefination.ProductDefinitionId).FirstOrDefault();
                if (longitudeDefination != null)
                    product.ProductProperties.Add(new ProductProperty() { Value = hotelDetail.Longitude, ProductDefinitionField = longitudeDefination, Product = product });
            }

            //Save New Hotel Type

            ProductProperty ratingProperty = product.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationConstant.HotelRating);
            if (ratingProperty != null)
            {
                ratingProperty.Value = hotelDetail.HotelRating;
            }
            else
            {
                ProductDefinitionField hotelRatingDefination = ProductDefinitionField.All().Where(x => x.Name == ProductDefinationConstant.HotelRating && !x.Deleted && x.ProductDefinition.ProductDefinitionId == hotelDefination.ProductDefinitionId).FirstOrDefault();
                if (hotelRatingDefination != null)
                    product.ProductProperties.Add(new ProductProperty() { Value = hotelDetail.HotelRating, ProductDefinitionField = hotelRatingDefination, Product = product });
            }
        }

        private Category CreateCategory(ProductCatalog catelog, string categoryName, Category parentCategory = null)
        {
            try
            {
                Definition categoryDefination = Definition.All().Where(x => x.DefinitionId == 1).FirstOrDefault();
                if (categoryDefination != null)
                {
                    Category pCategory = new Category();
                    pCategory.Name = categoryName;
                    pCategory.ProductCatalog = catelog;
                    pCategory.DisplayOnSite = true;
                    if (parentCategory != null)
                        pCategory.ParentCategory = parentCategory;
                    pCategory.Definition = categoryDefination;
                    //pCategory.Save();
                    PipelineFactory.Create<Category>("SaveCategory").Execute(pCategory);

                    return pCategory;
                }
            }
            catch (Exception)
            {
                return null;
            }
            return null;
        }


        public Model.LBHotelDetail GetHotelDetailByGUID(string categoryId)
        {
            Category catgoryDetail = Category.All().FirstOrDefault(x => x.Guid.ToString() == categoryId);
            if (catgoryDetail != null)
            {
                return new Model.LBHotelDetail()
                {
                    DisplayName = catgoryDetail.Name,
                    HotelName = catgoryDetail.Name
                };
            }
            return null;
        }


        public int SaveSiteCountry(Model.CountryDetail countryDetail)
        {
            if (!string.IsNullOrEmpty(countryDetail.CultureCode) && !string.IsNullOrEmpty(countryDetail.CountryName))
            {
                Country country = Country.All().Where(x => x.Name == countryDetail.CountryName && !x.Deleted).FirstOrDefault();
                if (country == null)
                {
                    try
                    {
                        country = new Country();
                        country.Culture = countryDetail.CultureCode;
                        country.Name = countryDetail.CountryName;
                        country.Save();
                        return country.Id;
                    }
                    catch (Exception ex)
                    {
                        Diagnostics.WriteException(DiagnosticsCategory.MasterClassBusinessLogic, ex, this);
                        return 0;
                    }
                }
                else
                {
                    if (country.Culture != countryDetail.CultureCode)
                    {
                        country.Culture = countryDetail.CultureCode;
                        country.Save();
                    }
                    return country.Id;
                }
            }
            return 0;

        }

        /// <summary>
        /// GetHotelDetailByGUID 
        /// </summary>
        /// <param name="sitecoreItemId">sitecoreItemId</param>
        /// <returns>gets the hotel information by guid</returns>
        public bool DeleteCategory(string sitecoreItemId)
        {
            try
            {
                Category existingCategory = Category.All().Where(x => x.CategoryProperties.Any(y => y.DefinitionField.Name.Equals(ProductDefinationConstant.SiteCoreItemGuid) && y.Value == sitecoreItemId)).FirstOrDefault();
                if (existingCategory != null)
                {
                    existingCategory.Deleted = true;
                    //existingCategory.Save();
                    PipelineFactory.Create<Category>("SaveCategory").Execute(existingCategory);

                }
            }
            catch (Exception)
            {
            }
            return false;
        }

        /// <summary>
        /// UpdateSupplierClientDetail 
        /// </summary>
        /// <param name="supplier">supplier</param>
        /// <returns>update the supllier and client details</returns>
        public bool UpdateSupplierClientDetail(Model.LBAssignSupplier supplier)
        {
            if (supplier != null)
            {
                if ((supplier.AddedHotel.Any())
                    || (supplier.RemoveHotel.Any()))
                {
                    List<int> productList = new List<int>();
                    productList.AddRange(supplier.AddedHotel);
                    productList.AddRange(supplier.RemoveHotel);
                    List<Product> existingCategory = Product.All().Where(x => productList.Contains(x.ProductId)).ToList();
                    SaveSupplierCategory(supplier, existingCategory);
                }
            }
            return true;

        }

        private static void SaveSupplierCategory(Model.LBAssignSupplier supplier, List<Product> productList)
        {
            if (productList != null && productList.Any())
            {
                foreach (var item in supplier.AddedHotel)
                {
                    var suppleProduct = productList.FirstOrDefault(x => x.ProductId == item);
                    if (suppleProduct != null)
                    {
                        List<Category> categoryList = suppleProduct.GetCategories().ToList();
                        foreach (var category in categoryList)
                        {
                            Category productCatelogCategory = Category.All().Where(x => x.Name == category.Name && x.ProductCatalog.ProductCatalogId == supplier.CatalogId).FirstOrDefault();
                            if (productCatelogCategory == null)
                            {
                                ProductCatalog catalog = ProductCatalog.All().Where(x => x.ProductCatalogId == supplier.CatalogId).FirstOrDefault();
                                Category parentCategory = category.ParentCategory;
                                Category productCatelogParentCategory = null;
                                if (parentCategory != null)
                                {
                                    productCatelogParentCategory = Category.All().Where(x => x.Name == parentCategory.Name && x.ProductCatalog.ProductCatalogId == supplier.CatalogId).FirstOrDefault();
                                    if (productCatelogParentCategory == null)
                                    {
                                        productCatelogParentCategory = CopyCategory(catalog, parentCategory);
                                    }
                                }
                                productCatelogCategory = CopyCategory(catalog, category);
                                if (productCatelogParentCategory != null)
                                {
                                    productCatelogCategory.ParentCategory = productCatelogParentCategory;
                                }
                                //productCatelogCategory.Save();
                                PipelineFactory.Create<Category>("SaveCategory").Execute(productCatelogCategory);
                            }
                            if (!categoryList.Any(x => x.CategoryId == productCatelogCategory.CategoryId))
                            {
                                suppleProduct.AddCategory(productCatelogCategory, 1);
                            }
                        }
                        if (categoryList.Count == 0)
                        {
                            bool categoryFound = false;
                            ProductProperty regionProperty = suppleProduct.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationConstant.Region);
                            if (regionProperty != null && !string.IsNullOrEmpty(regionProperty.Value))
                            {
                                Category c = Category.All().Where(x => x.Name == regionProperty.Value && x.ProductCatalog.ProductCatalogId == supplier.CatalogId).FirstOrDefault();
                                if (c != null)
                                {
                                    categoryFound = true;
                                    suppleProduct.AddCategory(c, 1);
                                }

                            }
                            ProductProperty locationProperty = suppleProduct.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationConstant.Location);
                            if (!categoryFound && locationProperty != null && !string.IsNullOrEmpty(locationProperty.Value))
                            {
                                Category c = Category.All().Where(x => x.Name == locationProperty.Value && x.ProductCatalog.ProductCatalogId == supplier.CatalogId).FirstOrDefault();
                                if (c != null)
                                {
                                    categoryFound = true;
                                    suppleProduct.AddCategory(c, 1);
                                }
                            }
                        }
                        suppleProduct.DisplayOnSite = true;
                        PipelineFactory.Create<Product>("SaveProduct").Execute(suppleProduct);
                        //suppleProduct.Save();
                    }
                }

                foreach (var item in supplier.RemoveHotel)
                {
                    var suppleProduct = productList.FirstOrDefault(x => x.ProductId == item);
                    if (suppleProduct != null)
                    {
                        List<Category> categoryList = suppleProduct.GetCategories().ToList();
                        List<Category> tmpcategoryList = categoryList.Where(x => x.ProductCatalog.ProductCatalogId == supplier.CatalogId).ToList();
                        foreach (var cList in tmpcategoryList)
                        {
                            suppleProduct.RemoveCategory(cList);
                        }
                        if (categoryList.Count == tmpcategoryList.Count)
                        {
                            suppleProduct.DisplayOnSite = false;
                        }
                        PipelineFactory.Create<Product>("SaveProduct").Execute(suppleProduct);
                        //suppleProduct.Save();

                    }
                }
            }
        }

        private static Category CopyCategory(ProductCatalog catalog, Category parentCategory)
        {

            Category productCatelogParentCategory = new Category();
            productCatelogParentCategory.CreatedBy = parentCategory.CreatedBy;
            productCatelogParentCategory.CreatedOn = DateTime.Now;

            productCatelogParentCategory.Definition = parentCategory.Definition;
            productCatelogParentCategory.Deleted = parentCategory.Deleted;
            productCatelogParentCategory.DisplayOnSite = parentCategory.DisplayOnSite;
            productCatelogParentCategory.SortOrder = parentCategory.SortOrder;
            productCatelogParentCategory.Name = parentCategory.Name;
            productCatelogParentCategory.ModifiedBy = parentCategory.ModifiedBy;
            productCatelogParentCategory.ModifiedOn = DateTime.Now;
            productCatelogParentCategory.CreatedBy = parentCategory.CreatedBy;
            productCatelogParentCategory.ProductCatalog = catalog;
            //productCatelogParentCategory.Save();
            PipelineFactory.Create<Category>("SaveCategory").Execute(productCatelogParentCategory);
            return productCatelogParentCategory;
        }


        public LBProductInfo SaveProducVariant(Model.LBProductDetail detail)
        {
            LBProductInfo productData = new LBProductInfo();
            Product hotelProduct = Product.All().Where(x => x.Sku == detail.ParentSKU && x.ParentProduct == null).FirstOrDefault();
            Product existingProduct = Product.All().Where(x => x.Sku == detail.ParentSKU && x.VariantSku == detail.SKU).FirstOrDefault();
            Product product = existingProduct ?? new Product();
            product.DisplayOnSite = true;
            product.Name = detail.Name;
            product.Sku = detail.ParentSKU;
            product.ParentProduct = hotelProduct;
            if (product.Id == 0)
            {
                int maxProductId = Product.All().Max(x => x.ProductId) + 1;

                product.VariantSku = string.Format("{0}_{1}", product.Name.Length > 2 ? product.Name.Substring(0, 2) : product.Name, maxProductId);
                ProductDefinition productDefination = GetProductDefinition(ref product);

            }

            ProductProperty addOnProperty = product.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationVairantConstant.FoodType);
            if (addOnProperty != null)
            {
                addOnProperty.Value = detail.FoodType;
            }
            else
            {
                ProductDefinitionField hotelCountryDefination = ProductDefinitionField.All().Where(x => x.Name == ProductDefinationVairantConstant.FoodType && !x.Deleted && x.IsVariantProperty).FirstOrDefault();
                if (hotelCountryDefination != null)
                    product.ProductProperties.Add(new ProductProperty() { Value = detail.FoodType, ProductDefinitionField = hotelCountryDefination, Product = product });
            }

            ProductProperty activeProperty = product.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationVairantConstant.IsActive);
            if (activeProperty != null)
            {
                activeProperty.Value = detail.IsActive.ToString();
            }
            else
            {
                ProductDefinitionField activeDefination = ProductDefinitionField.All().Where(x => x.Name == ProductDefinationVairantConstant.IsActive && !x.Deleted && x.IsVariantProperty).FirstOrDefault();
                if (activeDefination != null)
                    product.ProductProperties.Add(new ProductProperty() { Value = detail.IsActive.ToString(), ProductDefinitionField = activeDefination, Product = product });
            }

            ProductProperty deletedProperty = product.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationVairantConstant.IsDeleted);
            if (deletedProperty != null)
            {
                deletedProperty.Value = detail.IsDeleted.ToString();
            }
            else
            {
                ProductDefinitionField deletedDefination = ProductDefinitionField.All().Where(x => x.Name == ProductDefinationVairantConstant.IsDeleted && !x.Deleted && x.IsVariantProperty).FirstOrDefault();
                if (deletedDefination != null)
                    product.ProductProperties.Add(new ProductProperty() { Value = detail.IsDeleted.ToString(), ProductDefinitionField = deletedDefination, Product = product });
            }

            ProductProperty sellableStandAloneProperty = product.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationVairantConstant.IsSellableStandAlone);
            if (sellableStandAloneProperty != null)
            {
                sellableStandAloneProperty.Value = detail.IsSellableStaandalone.ToString();
            }
            else
            {
                ProductDefinitionField hotelCountryDefination = ProductDefinitionField.All().Where(x => x.Name == ProductDefinationVairantConstant.IsSellableStandAlone && !x.Deleted && x.IsVariantProperty).FirstOrDefault();
                if (hotelCountryDefination != null)
                    product.ProductProperties.Add(new ProductProperty() { Value = detail.IsSellableStaandalone.ToString(), ProductDefinitionField = hotelCountryDefination, Product = product });
            }

            ProductProperty unlimitedProperty = product.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationVairantConstant.IsUnlimited);
            if (unlimitedProperty != null)
            {
                unlimitedProperty.Value = detail.IsUnlimited.ToString();
            }
            else
            {
                ProductDefinitionField hotelCountryDefination = ProductDefinitionField.All().Where(x => x.Name == ProductDefinationVairantConstant.IsUnlimited && !x.Deleted && x.IsVariantProperty).FirstOrDefault();
                if (hotelCountryDefination != null)
                    product.ProductProperties.Add(new ProductProperty() { Value = detail.IsUnlimited.ToString(), ProductDefinitionField = hotelCountryDefination, Product = product });
            }

            ProductProperty maxAdultProperty = product.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationVairantConstant.MaxNumberOfAdults);
            if (maxAdultProperty != null)
            {
                maxAdultProperty.Value = detail.MaxNumberOfAdults.ToString();
            }
            else
            {
                ProductDefinitionField hotelCountryDefination = ProductDefinitionField.All().Where(x => x.Name == ProductDefinationVairantConstant.MaxNumberOfAdults && !x.Deleted && x.IsVariantProperty).FirstOrDefault();
                if (hotelCountryDefination != null)
                    product.ProductProperties.Add(new ProductProperty() { Value = detail.MaxNumberOfAdults.ToString(), ProductDefinitionField = hotelCountryDefination, Product = product });
            }

            ProductProperty maxChildProperty = product.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationVairantConstant.MaxNumberOfChildren);
            if (maxChildProperty != null)
            {
                maxChildProperty.Value = detail.MaxNumberOfChildren.ToString();
            }
            else
            {
                ProductDefinitionField hotelCountryDefination = ProductDefinitionField.All().Where(x => x.Name == ProductDefinationVairantConstant.MaxNumberOfChildren && !x.Deleted && x.IsVariantProperty).FirstOrDefault();
                if (hotelCountryDefination != null)
                    product.ProductProperties.Add(new ProductProperty() { Value = detail.MaxNumberOfChildren.ToString(), ProductDefinitionField = hotelCountryDefination, Product = product });
            }

            ProductProperty maxInfrantProperty = product.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationVairantConstant.MaxNumberOfInfants);
            if (maxInfrantProperty != null)
            {
                maxInfrantProperty.Value = detail.MaxNumberOfAdults.ToString();
            }
            else
            {
                ProductDefinitionField hotelCountryDefination = ProductDefinitionField.All().Where(x => x.Name == ProductDefinationVairantConstant.MaxNumberOfInfants && !x.Deleted && x.IsVariantProperty).FirstOrDefault();
                if (hotelCountryDefination != null)
                    product.ProductProperties.Add(new ProductProperty() { Value = detail.MaxNumberOfInfrant.ToString(), ProductDefinitionField = hotelCountryDefination, Product = product });
            }

            ProductProperty maxpeopleProperty = product.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationVairantConstant.MaxNumberOfPeople);
            if (maxpeopleProperty != null)
            {
                maxpeopleProperty.Value = detail.MaxNumberOfPeople.ToString();
            }
            else
            {
                ProductDefinitionField hotelCountryDefination = ProductDefinitionField.All().Where(x => x.Name == ProductDefinationVairantConstant.MaxNumberOfPeople && !x.Deleted && x.IsVariantProperty).FirstOrDefault();
                if (hotelCountryDefination != null)
                    product.ProductProperties.Add(new ProductProperty() { Value = detail.MaxNumberOfPeople.ToString(), ProductDefinitionField = hotelCountryDefination, Product = product });
            }



            ProductProperty packageProperty = product.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationVairantConstant.PackageType);
            if (packageProperty != null)
            {
                packageProperty.Value = detail.PackageType;
            }
            else
            {
                ProductDefinitionField hotelCountryDefination = ProductDefinitionField.All().Where(x => x.Name == ProductDefinationVairantConstant.PackageType && !x.Deleted && x.IsVariantProperty).FirstOrDefault();
                if (hotelCountryDefination != null)
                    product.ProductProperties.Add(new ProductProperty() { Value = detail.PackageType, ProductDefinitionField = hotelCountryDefination, Product = product });
            }

            ProductProperty relatedSKUProperty = product.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationVairantConstant.RelatedSKU);
            if (relatedSKUProperty != null)
            {
                relatedSKUProperty.Value = detail.RelatedSku;
            }
            else
            {
                ProductDefinitionField hotelCountryDefination = ProductDefinitionField.All().Where(x => x.Name == ProductDefinationVairantConstant.RelatedSKU && !x.Deleted && x.IsVariantProperty).FirstOrDefault();
                if (hotelCountryDefination != null)
                    product.ProductProperties.Add(new ProductProperty() { Value = detail.RelatedSku, ProductDefinitionField = hotelCountryDefination, Product = product });
            }

            ProductProperty relatedAddOnSKUProperty = product.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationVairantConstant.RelatedAddOnSKU);
            if (relatedAddOnSKUProperty != null)
            {
                relatedAddOnSKUProperty.Value = detail.RelatedAddOnSku;
            }
            else
            {
                ProductDefinitionField hotelCountryDefination = ProductDefinitionField.All().Where(x => x.Name == ProductDefinationVairantConstant.RelatedAddOnSKU && !x.Deleted && x.IsVariantProperty).FirstOrDefault();
                if (hotelCountryDefination != null)
                    product.ProductProperties.Add(new ProductProperty() { Value = detail.RelatedSku, ProductDefinitionField = hotelCountryDefination, Product = product });
            }


            ProductProperty roomTypeProperty = product.ProductProperties.FirstOrDefault(x => x.ProductDefinitionField.Name == ProductDefinationVairantConstant.RoomType);
            if (roomTypeProperty != null)
            {
                roomTypeProperty.Value = detail.RoomType;
            }
            else
            {
                ProductDefinitionField hotelCountryDefination = ProductDefinitionField.All().Where(x => x.Name == ProductDefinationVairantConstant.RoomType && !x.Deleted && x.IsVariantProperty).FirstOrDefault();
                if (hotelCountryDefination != null)
                    product.ProductProperties.Add(new ProductProperty() { Value = detail.RoomType, ProductDefinitionField = hotelCountryDefination, Product = product });
            }
            //product.Save();
            PipelineFactory.Create<Product>("SaveProduct").Execute(product);
            productData.ProductId = product.Id;
            productData.SKU = product.Sku;
            productData.VSKU = product.VariantSku;
            return productData;
        }

        public int CreateCataLog(string name)
        {
            ProductCatalog catalog = ProductCatalog.All().Where(x => x.Name == name).FirstOrDefault();

            if (catalog == null)
            {
                //TODO : Change As Required
                PriceGroup group = PriceGroup.All().FirstOrDefault();
                ProductCatalogGroup catalogGroup = ProductCatalogGroup.All().Where(x => x.Name == "LoyaltyBuild").FirstOrDefault();
                if (group == null)
                    return 0;
                catalog = new ProductCatalog();
                catalog.CreatedOn = DateTime.Now;
                catalog.CreatedBy = "admin";
                catalog.DisplayOnWebSite = true;
                catalog.Name = name;
                catalog.ProductCatalogGroup = catalogGroup;
                catalog.PriceGroup = group;
                // catalog.Save();
                PipelineFactory.Create<ProductCatalog>("SaveProductCatalog").Execute(catalog);
            }
            return catalog.Id;
        }
    }
}
