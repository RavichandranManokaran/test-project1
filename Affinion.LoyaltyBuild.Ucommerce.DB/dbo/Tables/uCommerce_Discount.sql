﻿CREATE TABLE [dbo].[uCommerce_Discount] (
    [DiscountId]       INT            IDENTITY (1, 1) NOT NULL,
    [OrderId]          INT            NOT NULL,
    [CampaignName]     NVARCHAR (512) NULL,
    [CampaignItemName] NVARCHAR (512) NULL,
    [Description]      NVARCHAR (512) NULL,
    [AmountOffTotal]   MONEY          NOT NULL,
    [CreatedOn]        DATETIME       NOT NULL,
    [ModifiedOn]       DATETIME       NOT NULL,
    [CreatedBy]        NVARCHAR (50)  NOT NULL,
    [ModifiedBy]       NVARCHAR (50)  NULL,
    CONSTRAINT [PK_uCommerce_Discount] PRIMARY KEY CLUSTERED ([DiscountId] ASC),
    CONSTRAINT [FK_uCommerce_Discount_uCommerce_PurchaseOrder] FOREIGN KEY ([OrderId]) REFERENCES [dbo].[uCommerce_PurchaseOrder] ([OrderId])
);


GO
CREATE NONCLUSTERED INDEX [IX_uCommerce_Discount_OrderId]
    ON [dbo].[uCommerce_Discount]([OrderId] ASC)
    INCLUDE([DiscountId], [CampaignName], [CampaignItemName], [Description], [AmountOffTotal], [CreatedOn], [ModifiedOn], [CreatedBy], [ModifiedBy]);

