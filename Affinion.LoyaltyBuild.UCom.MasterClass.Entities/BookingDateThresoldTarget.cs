﻿
using Affinion.LoyaltyBuild.UCom.Common.Constants;
using Affinion.LoyaltyBuild.UCom.Common.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;
using UCommerce.Marketing.Targets;


namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    /// <summary>
    /// Act target to set the thresold hours before a booking can be done
    /// This stores Min and Max hours before can be booking and uses the time diff b/w Checkin and reservation date
    /// to determine if the promotion is applicable
    /// </summary>
    public class BookingDateThresoldTarget : Target, IOrderLineTarget, IEntity, ITarget
    {
        private int? _hashCode;

        /// <summary>
        /// Determines if this promotion is applicable
        /// </summary>
        /// <param name="orderLine">order line to verify</param>
        /// <returns>bool flag</returns>
        public virtual bool IsSatisfiedBy(OrderLine orderLine)
        {
            OrderProperty searchIgnor = orderLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.IgnorCampaignSearchCriteria);
            if (searchIgnor != null)
                return true;

            OrderProperty arrivalDateProperty = orderLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.ArrivalDateKey);
            OrderProperty reservationDateProperty = orderLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.ReservationDateKey);
            if (arrivalDateProperty != null && reservationDateProperty != null)
            {
                DateTime arrivalDate = arrivalDateProperty.Value.ParseFormatDateTime(true);
                DateTime reservationDate = reservationDateProperty.Value.ParseFormatDateTime(true);
                double hoursDiff = (arrivalDate - reservationDate).TotalHours;

                //chek if the hours fall b/w min and max hours
                if (hoursDiff >= (MinHours ?? int.MinValue) && hoursDiff <= (MaxHours ?? int.MaxValue))
                {
                    return true;
                }

            }

            //default flag
            return false;
        }

        #region Implemented methods

        /// <summary>
        /// returns all acts
        /// </summary>
        /// <returns></returns>
        public new static IQueryable<BookingDateThresoldTarget> All()
        {
            return GetRepo().Select();
        }

        /// <summary>
        /// deletes this acy
        /// </summary>
        public override void Delete()
        {
            GetRepo().Delete(this);
        }

        /// <summary>
        /// delets acts based on the expression
        /// </summary>
        /// <param name="expression"></param>
        public static void Delete(Expression<Func<BookingDateThresoldTarget, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        /// <summary>
        /// compare targets
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            BookingDateThresoldTarget objA = obj as BookingDateThresoldTarget;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        /// <summary>
        /// exists method
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static bool Exists(Expression<Func<BookingDateThresoldTarget, bool>> expression)
        {
            return All().Any<BookingDateThresoldTarget>(expression);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static IList<BookingDateThresoldTarget> Find(Expression<Func<BookingDateThresoldTarget, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<BookingDateThresoldTarget>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static BookingDateThresoldTarget FirstOrDefault(Expression<Func<BookingDateThresoldTarget, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<BookingDateThresoldTarget>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static new BookingDateThresoldTarget Get(object id)
        {
            return GetRepo().Get(id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        internal static IRepository<BookingDateThresoldTarget> GetRepo()
        {

            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<BookingDateThresoldTarget>>();
        }

        /// <summary>
        /// 
        /// </summary>
        public override void Save()
        {
            GetRepo().Save(this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static BookingDateThresoldTarget SingleOrDefault(Expression<Func<BookingDateThresoldTarget, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public override int Id
        {
            get
            {
                return this.BookingDateThresoldTargetId;
            }
        }

        /// <summary>
        /// target id
        /// </summary>
        public virtual int BookingDateThresoldTargetId { get; protected set; }

        /// <summary>
        /// Min hours
        /// </summary>
        public virtual int? MinHours { get; set; }

        /// <summary>
        /// Max hours
        /// </summary>
        public virtual int? MaxHours { get; set; }



    }
}
