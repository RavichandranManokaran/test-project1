CREATE PROCEDURE [dbo].[LB_ARPaymentRequest]
@OraclePaymentXML NVARCHAR(MAX)=NULL
AS
BEGIN
DECLARE @XmlDocumentHandle INT
DECLARE @XmlDocument NVARCHAR(MAX)
DECLARE @OracleFileSequenceNo INT
DECLARE @OracleFileName VARCHAR(50)

SET @XmlDocument =@OraclePaymentXML

DECLARE  @AROraclePayment TABLE
(
    SupplierVatPaymentId  INT,
	PostedVatAmount DECIMAL(11,2),
	NominalCode VARCHAR(50),
	CampaignAccountingId  INT,
	OracleIdForVat VARCHAR(50),
	CustomerAddressCode NVARCHAR(150),
	InvoiceId INT,
	LineNumber INT,
	UpdatedBy VARCHAR(80)

)

EXEC sp_xml_preparedocument @XmlDocumentHandle OUTPUT, @XmlDocument

SET @OracleFileSequenceNo=(SELECT SequenceNo FROM LB_OracleFile WHERE IsClosedOff=0 AND OracleFileType='AR')

IF @OracleFileSequenceNo IS NULL OR @OracleFileSequenceNo=0
BEGIN
          SET @OracleFileSequenceNo=(SELECT MAX(SequenceNo)+1 FROM LB_OracleFile WHERE IsClosedOff=1 AND OracleFileType='AR')
		  IF @OracleFileSequenceNo IS NULL OR @OracleFileSequenceNo=0
		  BEGIN
		       SET @OracleFileName='AR1'
			   SET @OracleFileSequenceNo=1
		  END
		  ELSE
		  BEGIN
		       SET @OracleFileName='AR'+CONVERT(VARCHAR,@OracleFileSequenceNo)
		  END
          INSERT INTO LB_OracleFile(OracleFileName,OracleFileType,SequenceNo) VALUES(@OracleFileName,'AR',@OracleFileSequenceNo)
END

INSERT INTO @AROraclePayment
SELECT    *
FROM      OPENXML (@XmlDocumentHandle, '/APOraclePayment/APOraclePayment',2)
           WITH 
		   (
		        SupplierVatPaymentId  INT,
				PostedVatAmount DECIMAL(11,2),
				NominalCode VARCHAR(50),
				CampaignAccountingId  INT,
				OracleIdForVat VARCHAR(50),
				CustomerAddressCode NVARCHAR(150),
				InvoiceId INT,
				LineNumber INT,
				UpdatedBy VARCHAR(80)

			)
EXEC sp_xml_removedocument @XmlDocumentHandle

UPDATE  
       LB_V_P SET 
	               LB_V_P.PostedVatAmount=AROP.PostedVatAmount,
				   LB_V_P.DateUpdated=GETDATE(),
				   LB_V_P.UpdatedBy=AROP.UpdatedBy,
				   LB_V_P.DatePostedToOracle=GETDATE(),
				   LB_V_P.NominalCode=AROP.NominalCode,
				   LB_V_P.CampaignAccountingId=AROP.CampaignAccountingId,
				   LB_V_P.OracleIdForVat=AROP.OracleIdForVat,
				   LB_V_P.OracleFileId=@OracleFileSequenceNo,
				   LB_V_P.CustomerAddressCode=AROP.CustomerAddressCode,
				   LB_V_P.InvoiceId =AROP.InvoiceId,
				   LB_V_P.LineNumber =AROP.LineNumber
					
FROM 
       LB_Vat_Payments LB_V_P,@AROraclePayment AROP
WHERE 
       LB_V_P.SupplierVatPaymentId=AROP.SupplierVatPaymentId AND

	   AROP.SupplierVatPaymentId >0



END

GO


