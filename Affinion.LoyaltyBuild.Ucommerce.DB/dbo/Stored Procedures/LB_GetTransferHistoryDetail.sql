﻿CREATE PROCEDURE [dbo].[LB_GetTransferHistoryDetail]
(	 
	@OrderLineId int=NULL	
)
AS 
BEGIN
	IF (@OrderLineId IS NOT NULL)
	BEGIN		
		SELECT OL.OrderLineId [OrderLineId],			
			P.Name [SupplierName],
			BI.CheckInDate [ArrivalDate],
			BI.CheckOutDate [DepartureDate],
			1 [NoOfRooms],
			CASE WHEN DATEDIFF(day, BI.CheckInDate,BI.checkOutdate) IS NULL THEN 0 ELSE DATEDIFF(day, BI.CheckInDate,BI.checkOutdate) END as [NoOfNights],
			 
			OP.NoOfGuest [NoOfPeople],	
			--CASE WHEN RR.NoOfGuest IS NULL THEN 0 ELSE RR.NoOfGuest END as [NoOfPeople], 		
			OL.CreatedOn [ReservationDate]
		FROM [dbo].uCommerce_OrderLine OL 			
			INNER JOIN [dbo].LB_BookingInfo BI ON OL.OrderLineId = BI.OrderLineId 
			INNER JOIN [dbo].uCommerce_Product P ON OL.Sku = P.Sku AND P.VariantSku IS NULL
			--INNER JOIN [dbo].uCommerce_CategoryProductRelation CPR ON P.ProductId = CPR.ProductId 
			--INNER JOIN [dbo].uCommerce_Category C ON CPR.CategoryId = C.CategoryId	
			Inner Join 
			(
					select OrderLineId, NoOfGuest
					from 
					(
					select OrderLineId, [key], [value]
					from uCommerce_OrderProperty  WHERE OrderLineId in (SELECT OrderLineId from [dbo].[LB_FnRelatedOrderLines](@OrderLineId))
					) src
					pivot
					(
					max([value])
				 for [key] in ( NoOfGuest)
			) piv
		) OP ON BI.OrderLineId=OP.OrderLineId
			
					
		WHERE 				
			OL.OrderLineId in (SELECT OrderLineId from [dbo].[LB_FnRelatedOrderLines](@OrderLineId))
			AND OL.OrderLineId != @OrderLineId
		order by OL.CreatedOn asc;			
	END
END



