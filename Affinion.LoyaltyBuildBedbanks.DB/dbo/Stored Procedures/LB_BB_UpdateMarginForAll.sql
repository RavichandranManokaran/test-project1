﻿CREATE procedure [dbo].[LB_BB_UpdateMarginForAll](@marginDetails varchar(max), @marginPercentage decimal, @startDate date, @endDate date)
as
BEGIN
DECLARE @table table(ProviderID varchar(max))
DECLARE @matchingProvider table(MarginID int, ProviderID varchar(max))
DECLARE @listName varchar(max)
DECLARE @listID int
INSERT INTO @table select * from SplitString(@marginDetails,',')
--select * from @table
SET @listName='Error_'+CAST(GETDATE() as varchar)
INSERT INTO @matchingProvider Select md.MarginID, md.ProviderID  from LB_BB_MarginDetails md inner join @table temp on md.providerId = temp.ProviderID where (@startDate BETWEEN md.StartDate AND md.EndDate) OR (@endDate BETWEEN md.StartDate AND md.EndDate)
	--select * from @matchingProvider
	IF EXISTS (select * from @matchingProvider)
		BEGIN
		INSERT INTO LB_BB_MarginErrorList(ListName, DateAdded) Values (@listName, GETDATE())
		Select @listID = ListID from LB_BB_MarginErrorList where ListName=@listName
		INSERT INTO LB_BB_MarginErrorDetail(ListID, ProviderID, DateAdded) SELECT @listID, md.ProviderID, GETDATE() from LB_BB_MarginDetails md 
		inner join @matchingProvider temp ON md.ProviderID=temp.ProviderID 
		where (@startDate BETWEEN md.StartDate AND md.EndDate) OR (@endDate BETWEEN md.StartDate AND md.EndDate)
		delete from @table where ProviderID IN (select ProviderID from LB_BB_MarginErrorDetail where ListID=@listID)
	END
		INSERT INTO LB_BB_MarginDetails(ProviderID, MarginType, marginPercentage, startDate, EndDate, DateAdded, IsActive, CreatedBy) SELECT temp.ProviderID, 'Provider', @marginPercentage, @startDate, @endDate, GETDATE(), 1, 'admin'
		from @table temp
		--inner join LB_BB_MarginDetails md ON temp.ProviderID=md.ProviderID
		--WHERE (@startDate NOT BETWEEN StartDate AND EndDate) OR (@endDate NOT BETWEEN StartDate AND EndDate)
		Select ListName, ListID from LB_BB_MarginErrorList where ListName=@listName
END
GO