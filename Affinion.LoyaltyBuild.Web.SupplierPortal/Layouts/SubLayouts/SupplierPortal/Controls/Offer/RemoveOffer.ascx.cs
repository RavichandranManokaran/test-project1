﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Offer
{
    public partial class RemoveOffer : System.Web.UI.UserControl
    {
        public OfferDetail offerDetail;

        public int SupplierInviteId;

        public int CampaignItemId;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                
                drpReasonList.Items.Clear();
                
                drpReasonList.Items.Insert(0, new ListItem() { Text = "Select Reason", Value = "" });

                List<string> reaason = SiteConfiguration.GetOfferRemoveReason();
                foreach (var item in reaason)
                {
                    drpReasonList.Items.Add(new ListItem() { Text = item, Value = item });
                    
                }
            }
            this.DataBind();
        }


    }
}