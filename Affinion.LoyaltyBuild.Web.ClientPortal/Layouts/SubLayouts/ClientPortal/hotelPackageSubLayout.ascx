﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HotelPackageSubLayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal.HotelPackageSubLayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Sitecore.Data.Items" %>

<div class="row contentarea-outer">
    <div class="col-lg-12 visible-xs visible-sm">
        <div class="book-detail">
            <a id="collapse-button" href="#">
                <span class="glyphicon glyphicon-th-large"></span>Show Package
            </a>
        </div>
    </div>

    <div class="packages-out" id="accordion">
        <div class="col-lg-12">
            <h3 class="no-margin-top">
                <sc:Text Field="Pac_Section_Heading" runat="server" />
            </h3>
        </div>


        <asp:Repeater ID="PackageRepeater" runat="server">

            <ItemTemplate>

                <div class="col-xs-6 col-sm-3">
                    <div class="common-box-inner common-box">
                        <h4 class="text-center">
                            <!-- -->
                        </h4>
                        <div class="box-image">
                            <div class="box-img">
                                <a href="#"><strong></strong>
                                    <sc:Image Field="Pac_Image" Item="<%#(Item)Container.DataItem %>" alt="" CssClass="img-responsive img-enlarge" runat="server" />
                                </a>
                            </div>
                            <a href="#" class="booking-link"><%#Affinion.LoyaltyBuild.Common.Utilities.SitecoreFieldsHelper.GetValue((Item)Container.DataItem,"Pac_Booking_lin") %>
                            </a>
                        </div>

                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>

    </div>

</div>


