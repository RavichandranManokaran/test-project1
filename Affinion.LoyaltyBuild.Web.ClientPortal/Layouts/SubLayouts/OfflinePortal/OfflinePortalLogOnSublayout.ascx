﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalLogOnSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalLogOnSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<div class="form-outer">
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><strong>
                    <sc:Text Field="FormTitle" runat="server" />
                </strong></h3>
            </div>
            <div class="panel-body">
                <div id="errormessage" class="alert alert-danger" runat="server" visible="false">
                    <asp:Literal ID="literalMessage" runat="server" />
                </div>
                <div class="form-group">
                    <sc:Text Field="UserName" runat="server" />
                    <asp:TextBox ID="TextBoxUser" runat="server" placeholder="Username" class="form-control" required="" />
                </div>
                <div class="form-group">
                    <sc:Text Field="Password" runat="server" />
                    <asp:TextBox ID="TextBoxPassword" TextMode="Password" runat="server" placeholder="Password" class="form-control" required="" ViewStateMode="Disabled" />
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <asp:HyperLink ID="HyperLinkForgotPassword" runat="server">
                                <sc:Text field="ForgotPassword" runat="server"/>
                            </asp:HyperLink>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <asp:Button ID="ButtonLogOn" OnClick="ButtonLogOn_Click" runat="server" class="btn btn-sm btn-default" />
                </div>
                        
            </div>
        </div>
    </div>
</div>