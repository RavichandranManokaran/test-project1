﻿/*
 Pre-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be executed before the build script.	
 Use SQLCMD syntax to include a file in the pre-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the pre-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

GO
IF OBJECT_ID('[dbo].[Ucommerce_Campaignsupplier]', 'U') IS NOT NULL
  DROP TABLE [dbo].[Ucommerce_Campaignsupplier]; 

GO

--GO
--IF OBJECT_ID('[dbo].[uCommerce_Booking_Availability]', 'U') IS NOT NULL
--  DROP TABLE [dbo].[uCommerce_Booking_Availability]; 

--GO

Alter_OrderLine.sql
