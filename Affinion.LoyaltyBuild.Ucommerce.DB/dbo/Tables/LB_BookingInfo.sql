﻿CREATE TABLE [dbo].[LB_BookingInfo] (
    [OrderLineId]       INT            NOT NULL,
    [BookingId]    UNIQUEIDENTIFIER   NOT NULL,
    [BookingNo] VARCHAR (20)   NULL,
    [StatusId]       INT            NOT NULL,
    [CheckInDate] DATETIME NULL, 
    [CheckOutDate] DATETIME NULL, 
    [CreatedBy]         NVARCHAR (50)  NULL,
    [CreatedDate]       DATETIME           NULL,
    [UpdatedBy] NVARCHAR(50) NULL, 
    [UpdatedDate] DATETIME NULL, 
    CONSTRAINT [PK_RoomReservation] PRIMARY KEY CLUSTERED ([OrderLineId], [BookingId]),
	CONSTRAINT [uCommerce_FK_BookingInfo_OrderLine] FOREIGN KEY ([OrderLineId]) REFERENCES [dbo].[uCommerce_OrderLine] ([OrderLineId]) ON DELETE CASCADE
);

