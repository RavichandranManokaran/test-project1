﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SubscribeSublayout.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.SubscribeSublayout
/// Description:           Subscribe the user

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts
{
    #region using Directives
    using Affinion.LoyaltyBuild.Common;
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Communications.Services;
    using Sitecore.Data.Items;
    using System;
    using System.Text.RegularExpressions;
    using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
    #endregion

    public partial class SubscribeSublayout : BaseSublayout
    {
        private Item subsciptionItem;
        public Item conditionalMessageItem; 

        private void Page_Load(object sender, EventArgs e)
        {
           conditionalMessageItem = this.GetItemFromRenderingParameterPath("Conditional Messages Link");
            
           subsciptionItem = this.GetDataSource();
           SitecoreFieldsHelper.BindSitecoreText(subsciptionItem, "ButtonText", txtBtnText);
           SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, "Required Field Message", txtEmailRequiredText);
           SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, "Email Validation Message", txtEmailRegularText);
            if (subsciptionItem != null)
            {
                LiteralSubscription.Visible = false;
                if (SitecoreFieldsHelper.GetItemFieldValue(subsciptionItem,Constants.Message)!=string.Empty)
                {
                    this.message.Item = subsciptionItem;
                }
                else
                {
                    return;
                }
            }            
            Subscribelabel.Text = string.Empty;
        }

        /// <summary>
        /// Calls when the Subscribe button clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SubscribeButton_Click(object sender, EventArgs e)
        {
            //CreateSubscribe();
            uCommerceConnectionFactory.AddSubscribedCustomer(GetClientId(), CustomerId(), emailTextBox.Text, Constants.PromotionalMail);

            ///Set LabelValidation text
            Subscribelabel.Text = subsciptionItem.Fields[Constants.ConfirmationMessage].Value;

            ///Empty the emailTextBox value
            emailTextBox.Text = string.Empty;
        }

        /// <summary>
        /// Get the ClientId
        /// </summary>
        private static string GetClientId()
        {
            Item clientItem = SitecoreFieldsHelper.GetLookupFieldTargetItem(ItemHelper.RootItem, "ClientDetails");
            return clientItem.ID.Guid.ToString();
        }

        /// <summary>
        /// Get CustomerId
        /// </summary>
        /// <returns></returns>
        private static int CustomerId()
        {
            int UserId = 68;
            return UserId;
        }

        #region Private Methods
        private void CreateSubscribe()
        {
            Subscribelabel.Text = string.Empty;
            try
            {
                if (!string.IsNullOrWhiteSpace(emailTextBox.Text))
                {
                    ///Get the root item of the Subscription folder
                    Item rootItem = Sitecore.Context.Database.GetItem(Sitecore.Context.Site.RootPath);
                    string parentItemPath = rootItem.Paths.FullPath + Constants.FolderName;
                    string templatePath = Constants.SubscribeTemplatePath;

                    ///Get the email from the text box
                    string itemName = emailTextBox.Text;

                    ///Replace "@" sign with "at" and "." sign with "dot" for item name
                    string newItemName = SitecoreItemHelper.ResetItemName(itemName, "@", "at");
                    string completeName = SitecoreItemHelper.ResetItemName(newItemName, "\\.", "dot");

                    var itemExist = ItemExist(itemName, parentItemPath);

                    if (itemExist)
                    {
                        Subscribelabel.Text = Constants.EmailExist;
                    }
                    else
                    {
                        ///Create item for email address
                        CreateChildItem(itemName, parentItemPath, templatePath, completeName);
                    }
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Check the item exists in the subscription folder
        /// </summary>
        /// <param name="itemName">email address</param>
        /// <param name="folderPath">path of the subscription folder</param>
        /// <returns></returns>
        private bool ItemExist(string itemName, string folderPath)
        {
            Item[] folderItems = Sitecore.Context.Database.SelectItems(string.Concat(folderPath, "/*"));
            bool flag = false;
            foreach (Item folderItem in folderItems)
            {
                if (!itemName.Equals(folderItem.Fields[Constants.EmailID].Value))
                {
                    flag = false;
                }
                else
                {
                    flag = true;
                    break;
                }
            }
            return flag;
        }

        /// <summary>
        /// Create Child Item
        /// </summary>
        /// <param name="email">Email ID</param>
        /// <param name="parentItemPath">parent item path</param>
        /// <param name="templatePath">Template path that needs to create item</param>
        /// <param name="compleName">item name</param>
        private void CreateChildItem(string email, string parentItemPath, string templatePath, string completeName)
        {
            try
            {
                ///Create Item
                ItemHelper.AddItem(Sitecore.Context.Database, parentItemPath, templatePath, completeName);

                ///Get the item ID of the newly created item
                var newItem = Sitecore.Context.Database.GetItem(string.Concat(parentItemPath, Constants.SlashText, completeName));

                if (!newItem.ID.IsNull)
                {
                    using (new Sitecore.Security.Accounts.UserSwitcher(ItemHelper.ContentEditingUser))
                    {
                        newItem.Editing.BeginEdit();

                        ///Add values to the Email filed.
                        newItem.Fields[Constants.EmailID].Value = email;
                        newItem.Editing.EndEdit();
                    }
                }
                else
                {
                    
                    Subscribelabel.Text = SitecoreFieldsHelper.GetValue(conditionalMessageItem,"Item Not Exist Message");
                }

                ///Set LabelValidation text
                Subscribelabel.Text = subsciptionItem.Fields[Constants.ConfirmationMessage].Value;

                ///Empty the emailTextBox value
                emailTextBox.Text = string.Empty;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, this);
                throw;
            }
        }     
        #endregion    
    }
}