﻿CREATE Procedure [dbo].[LB_GetUnClaimedReceiptsReport]
(
	@ReceiptDate datetime
)
AS
Begin	
	Select P.ProductId, P.Name ,OLP.CurrencyTypeId ,CU.ISOCode, 
	SUM(OLP.TotalAmount) Amount,
	MONTH(MAX(OL.CreatedOn)) [Month], 
	YEAR(MAX(OL.CreatedOn)) [Year],
	COUNT(1) [Count]
	FROM 
		LB_OrderLinePayment OLP
	INNER JOIN 	[dbo].uCommerce_OrderLine OL  ON OLP.OrderLineID = OL.OrderLineId
	INNER JOIN [dbo].uCommerce_PurchaseOrder PO ON OL.OrderId = PO.OrderId
	INNER JOIN [dbo].LB_BookingInfo BI ON OL.OrderLineId = BI.OrderLineId 
	INNER JOIN [dbo].uCommerce_Product P ON OL.Sku = P.Sku
	--INNER JOIN [dbo].uCommerce_CategoryProductRelation CPR ON P.ProductId = CPR.ProductId 
	--INNER JOIN [dbo].uCommerce_Category C ON CPR.CategoryId = C.CategoryId
	INNER JOIN [dbo].uCommerce_Customer CUS ON PO.CustomerId = CUS.CustomerId 
	INNER JOIN [dbo].LB_CustomerProperty CA ON PO.CustomerId = CA.CustomerId 
	INNER JOIN [dbo].LB_OrderLineStatusAudit OLS ON BI.StatusID = OLS.StatusId
	INNER JOIN [dbo].uCommerce_OrderStatus OS ON OLS.StatusId = OS.OrderStatusId
	INNER JOIN [dbo].uCommerce_Currency CU ON CU.CurrencyId = OLP.CurrencyTypeId
	INNER JOIN [dbo].uCommerce_OrdelinePaymentRequest OLR  ON  OLR.OrderLinePaymentID = OLP.OrderLinePaymentID
	WHERE OLP.CreatedDate >= @ReceiptDate and isnull(OLR.IsProviderPaid,0) = 0
	GROUP BY P.ProductId, P.Name,OLP.CurrencyTypeId,CU.ISOCode
end
