﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalSearchBookingSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalSearchBooking" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<script type="text/javascript" src="/Resources/scripts/affinionCustom.js"></script>

<script src="/Resources/scripts/jquery.nanoscroller.js"></script>
<script src="/Resources/scripts/callscroller.js"></script>

<script src="/Resources/scripts/jquery.dd.min.js"></script>
<link href="/Resources/styles/dd.css" rel="stylesheet" />

<script type="text/javascript">

    //Expand grid
    function ExpanResultGrid() {
        jQuery("#collapseResult").addClass("panel-collapse collapse in");
        jQuery("#collapseResultAtag").removeClass();
        jQuery("#collapseResultAtag").addClass("show-hide-list");

    }

    jQuery(document).ready(function () {
        //Timer for error messages 
        $('.msg-customer').delay(5000).fadeOut();

    });

</script>

<%-- %><div class="content">
    <div class="content-body">--%>

<div class="row">
    <div class="col-sm-12 ">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <sc:text field="QuickSearchTitle" runat="server" />
                </h4>
            </div>
            <div class="panel-body alert-info">
                <div class="panel-body alert-info">
                    <div class="col-sm-12 no-padding-m">
                        <div class="content_bg_inner_box alert-info">
                            <div class="col-sm-6 no-padding-m">
                                <div class="row margin-row">
                                    <div class="col-sm-5">
                                        <span class="bask-form-titl font-size-12">
                                            <%-- Client:--%>
                                            <sc:text field="Client" runat="server" />
                                        </span>
                                    </div>
                                    <div class="col-sm-7">

                                        <asp:DropDownList ID="ClientList" runat="server" ClientIDMode="Static" CssClass="form-control"></asp:DropDownList>

                                    </div>
                                </div>
                                <asp:HiddenField ID="HiddenFieldOrderLineId" runat="server" Value="" />
                                <asp:HiddenField ID="HiddenFieldOrderId" runat="server" Value="" />
                                <asp:HiddenField ID="HiddenFieldOrderGuid" runat="server" Value="" />
                                <asp:HiddenField ID="HiddenFieldProviderSearchItemId" runat="server" Value="" />
                                <div class="row margin-row">
                                    <div class="col-sm-5">
                                        <span class="bask-form-titl font-size-12">
                                            <%--First name: --%>
                                            <sc:text field="FirstName" runat="server" />
                                        </span>
                                    </div>
                                    <div class="col-sm-7">
                                        <%-- <input type="text"  class="form-control font-size-12" placeholder="First name" >--%>
                                        <asp:TextBox ID="TextBoxFirstName" class="form-control font-size-12 " runat="server" placeholder="First name"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row margin-row">
                                    <div class="col-sm-5">
                                        <span class="bask-form-titl font-size-12">
                                            <%-- Last name: --%>
                                            <sc:text field="LastName" runat="server" />
                                        </span>
                                    </div>
                                    <div class="col-sm-7">
                                        <%-- <input type="text"  class="form-control font-size-12" placeholder="Last Name" > --%>
                                        <asp:TextBox ID="TextBoxLastName" class="form-control font-size-12 " runat="server" placeholder="Last name"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row margin-row">
                                    <div class="col-sm-5">
                                        <span class="bask-form-titl font-size-12">
                                            <%-- Provider: --%>
                                            <sc:text field="Provider" runat="server" />

                                        </span>
                                    </div>
                                    <div class="col-sm-7">
                                        <%-- <input type="text"  class="form-control font-size-12" placeholder="Provider" >--%>
                                        <asp:TextBox ID="TextBoxProvider" class="form-control font-size-12 " runat="server" ClientIDMode="Static" placeholder="Provider"></asp:TextBox>

                                    </div>
                                </div>


                                <div class="row margin-row">
                                    <div class="col-sm-5">
                                        <span class="bask-form-titl font-size-12">
                                            <%-- Booking Method:--%>
                                            <sc:text field="BookingMethod" runat="server" />
                                        </span>
                                    </div>
                                    <div class="col-sm-7">
                                        <asp:DropDownList ID="DropDownBookingMethod" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 no-padding-m">
                                <div class="row margin-row">
                                    <div class="col-sm-5">
                                        <span class="bask-form-titl font-size-12">
                                            <sc:text field="SearchDateBy" runat="server" />
                                        </span>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <asp:RadioButton ID="RadioRange" runat="server" GroupName="dateOptradio" CssClass="range" ClientIDMode="Static"></asp:RadioButton>
                                            <%--<input id="RadioRange" type="radio" class="range" name="optradio" runat="server">--%>
                                            <span class="font-size-12">
                                                <sc:text field="Range" runat="server" />
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <asp:RadioButton ID="RadioSpecific" runat="server" GroupName="dateOptradio" Checked="true" CssClass="specific" ClientIDMode="Static"></asp:RadioButton>
                                            <span class="font-size-12">
                                                <sc:text field="Specific" runat="server" />
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row margin-row"></div>
                                <div class="range-div" id="RangeDateDiv" runat="server">
                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <%-- Reservation From: --%>
                                                <sc:text field="ReservationFrom" runat="server" />

                                            </span>
                                        </div>
                                        <div class="col-sm-7">
                                            <div class="input-group">
                                                <span class="input-group-btn">
                                                    <asp:TextBox ID="ReservationDateFrom" placeholder="From" CssClass="form-control font-size-12" Text="" ClientIDMode="Static" runat="server" />
                                                    <%--  <input type="text"  class="form-control datepicker font-size-12" placeholder="From" id="check-in-date" data-provide="datepicker"> --%>
                                                </span>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <!--<span class="bask-form-titl font-size-12">Locations:</span>-->
                                            <span class="bask-form-titl font-size-12">
                                                <%--  Reservation To: --%>
                                                <sc:text field="ReservationTo" runat="server" />
                                            </span>
                                        </div>
                                        <div class="col-sm-7">
                                            <div class="input-group">
                                                <span class="input-group-btn">
                                                    <%-- <input type="text"  class="form-control font-size-12" placeholder="To"  id="check-out-date"> --%>
                                                    <asp:TextBox ID="ReservationDateTo" placeholder="Reservation To" CssClass="form-control font-size-12" Text="" ClientIDMode="Static" runat="server" />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="specific-div" id="SpecificDateDiv" runat="server">
                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <%-- Reservation Date: --%>
                                                <sc:text field="ReservationDate" runat="server" />
                                            </span>
                                        </div>
                                        <div class="col-sm-7">
                                            <div class="input-group">
                                                <span class="input-group-btn">
                                                    <asp:TextBox ID="ReservationDate" placeholder="Reservation Date" CssClass="form-control font-size-12" Text="" ClientIDMode="Static" runat="server" />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <%--Arrival Date:--%>
                                                <sc:text field="ArrivalDate" runat="server" />
                                            </span>
                                        </div>
                                        <div class="col-sm-7">
                                            <div class="input-group">
                                                <span class="input-group-btn">
                                                    <asp:TextBox ID="ArrivalDate" placeholder="Arrival Date" CssClass="form-control font-size-12" Text="" ClientIDMode="Static" runat="server" />
                                                    <%-- <input type="text"  class="form-control font-size-12" placeholder="Arrival Date"  id="arrival-date"> --%>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!--<div class="row margin-row">
                                    <div class="col-sm-5"><span class="bask-form-titl font-size-12">Reservation Date:</span></div>
                                    <div class="col-sm-7"><div class="input-group">
                              <span class="input-group-btn">
                              <button class="btn btn-default glyphicon glyphicon-calendar calander-icon" type="button" ></button>
                              </span>
                              <input type="text"  class="form-control" placeholder="Reservation Date"  id="reservation-date">
                           </div></div>
                                 </div>-->

                                <div class="row margin-row">
                                    <div class="col-sm-5">
                                        <span class="bask-form-titl font-size-12">
                                            <%--  Booking reference: --%>
                                            <%--<sc:text field="BookingReference" runat="server" />--%>
                                            <asp:DropDownList ID="DropDownBookingReference" runat="server" CssClass="form-control">
                                                <asp:ListItem Selected="True">Booking Reference</asp:ListItem>
                                                <asp:ListItem>Email Validation</asp:ListItem>
                                                <asp:ListItem>Numeric Validation</asp:ListItem>
                                            </asp:DropDownList>


                                        </span>
                                    </div>
                                    <div class="col-sm-7">
                                        <%--<input type="text"  class="form-control " placeholder="Booking Reference" >--%>
                                        <asp:TextBox ID="TextBoxBookingReference" class="form-control font-size-12" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row margin-row">
                                    <div class="col-sm-5">
                                        <span class="bask-form-titl font-size-12">
                                            <%-- Booking Status:--%>
                                            <sc:text field="BookingStatus" runat="server" />
                                        </span>
                                    </div>
                                    <div class="col-sm-7">
                                        <asp:DropDownList ID="DropDownBookingStatus" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row margin-row">
                                    <div class="col-sm-6">

                                        <asp:Button ID="ButtonSubmit" runat="server" Text="Button" class="btn btn-default add-break pull-right width-full search-btn" OnClick="ButtonSubmit_Click" />
                                    </div>
                                    <!--<div class="col-sm-4">
                                       <button class="btn btn-default add-break pull-right width-full" type="submit">Add Client</button> 
                                    </div>-->
                                    <div class="col-sm-6">

                                        <asp:Button ID="ButtonReset" runat="server" Text="Button" class="btn btn-default add-break pull-right width-full search-btn" OnClick="ButtonReset_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 no-padding-m">
                    <span>
                        <sc:text field="DailyActivity" runat="server" />
                    </span>
                    <div class="content_bg_inner_box alert-info">
                        <div class="col-xs-4">

                            <asp:DropDownList ID="DropDownListClient" runat="server" ClientIDMode="Static" CssClass="form-control"></asp:DropDownList>
                        </div>
                        <div class="col-xs-4">
                            <asp:Button ID="ButtonListOut" runat="server" Text="Button" class="btn btn-default add-break pull-right width-full" OnClick="ButtonListOut_Click" />

                        </div>
                        <div class="col-xs-4">
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div runat="server" id="divTotalRecords" visible="false">
            <asp:Label ID="lbltotalNumberOfRecords" runat="server" Text="Total no of records:"></asp:Label>
            <asp:Label ID="lblTotalRecords" runat="server"></asp:Label>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="col-md-12 padding-top-1 margin-bottom-15 inline-block">

                    <div class="row">
                        <div id="divLinks" runat="server">
                            <asp:LinkButton ID="LinkButtonTransferBooking" class="btn btn-gray btn-common col-xs-12 col-md-2 pull-left margin-bottom-small" OnClick="LinkButtonTransferBooking_Click" runat="server"> <sc:Text Field="TransferABookingText" runat="server"/></asp:LinkButton>
                            <asp:LinkButton ID="LinkButtonCancelBooking" class="btn btn-gray btn-common col-xs-12 col-md-2 pull-left margin-bottom-small" OnClick="LinkButtonCancelBooking_Click" runat="server"> <sc:Text Field="CancelBookingText" runat="server"/></asp:LinkButton>
                            <asp:LinkButton ID="LinkButtonViewTransferHistory" class="btn btn-gray btn-common col-xs-12 col-md-2 pull-left margin-bottom-small" OnClick="LinkButtonViewTransferHistory_Click" runat="server"> <sc:Text Field="TransferHistoryText" runat="server"/></asp:LinkButton>
                            <asp:LinkButton ID="LinkButtonPaymentHistory" class="btn btn-gray btn-common col-xs-12 col-md-2 pull-left margin-bottom-small" OnClick="LinkButtonPaymentHistory_Click" runat="server"> <sc:Text Field="PaymentHistoryText" runat="server"/></asp:LinkButton>
                            <asp:LinkButton ID="LinkButtonComms" class="btn btn-gray btn-common col-xs-12 col-md-2 pull-left margin-bottom-small" OnClick="LinkButtonComms_Click" runat="server"> <sc:Text Field="CommsText" runat="server"/></asp:LinkButton>
                            <asp:LinkButton ID="LinkButtonBookingDetail" class="btn btn-gray btn-common col-xs-12 col-md-2 pull-left margin-bottom-small" OnClick="LinkButtonBookingDetail_Click" runat="server"> <sc:Text Field="BookingDetailText" runat="server"/></asp:LinkButton>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <%--Messages--%>
        <div id="errormessage" class="alert alert-danger msg-customer" runat="server" visible="false" align="center">
            <b>
                <asp:Literal ID="literalErrorMessage" runat="server" Text="No Result Found" />
            </b>
        </div>
        <div class="alert alert-success msg-customer" runat="server" id="successmessage" visible="false" align="center">
            <b>
                <asp:Literal ID="literalSuccess" runat="server" Text="New Customer Added Successfully" />
            </b>
        </div>
        <%-- End Messages--%>
        <div class="row" id="divResult" runat="server" visible="false">
            <div class="col-sm-12">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading bg-primary">
                            <h4 class="panel-title">
                                <a id="collapseResultAtag" data-toggle="collapse" data-parent="#accordion" href="#collapseResult" class="show-hide-list collapsed">Search Booking Result</a>
                            </h4>
                        </div>
                        <div id="collapseResult" class="panel-collapse collapse">
                            <div class="panel-body alert-info">
                                <div class="col-sm-12 no-padding-m">

                                    <asp:LinkButton ID="LinkButtonConvertExcel" class="btn btn-default margin-right-5 pull-left margin-bottom-small" OnClick="LinkButtonConvertExcel_Click" runat="server" Visible="false"> <sc:Text Field="ConvertExcelText" runat="server"/><i class="glyphicon glyphicon-circle-arrow-down"></i></asp:LinkButton>
                                    <div class="content_bg_inner_box alert-info">

                                        <div class="table-responsive text-center">

                                            <asp:GridView ID="GridViewBookingResults" OnSelectedIndexChanged="GridViewBookingResults_SelectedIndexChanged" class="table table-striped table-bordered" SelectedRowStyle-Font-Bold="true" SelectedRowStyle-ForeColor="#026199" SelectedRowStyle-BackColor="#381cb" Width="100%" AllowPaging="true" PageSize="10" runat="server" OnPageIndexChanging="GridViewBookingResults_PageIndexChanging" PagerSettings-NextPageText=" Next " PagerSettings-PreviousPageText=" Previous " PagerSettings-Mode="NumericFirstLast" PagerSettings-FirstPageText=" First " PagerSettings-LastPageText=" Last " PagerSettings-Position="Bottom" PagerStyle-Font-Bold="true" PagerStyle-CssClass="pagination-grid" AllowSorting="true" OnSorting="GridViewBookingResults_Sorting">
                                                <Columns>
                                                    <asp:BoundField DataField="OrderLineId" HeaderText="Order Line Id"></asp:BoundField>
                                                    <asp:BoundField DataField="OrderId" HeaderText="Order Id"></asp:BoundField>
                                                    <asp:BoundField DataField="OrderGuid" HeaderText="Order Guid"></asp:BoundField>
                                                    <asp:CommandField ButtonType="Button" ShowSelectButton="True" ControlStyle-CssClass="btn btn-default"></asp:CommandField>
                                                    <%--  <asp:BoundField DataField="BookingReferenceId" HeaderText="Booking Ref"></asp:BoundField>
                                                    <asp:BoundField DataField="Campaign" HeaderText="Campaign"></asp:BoundField>
                                                    <asp:BoundField DataField="FullName" HeaderText="Customer"></asp:BoundField>
                                                    <asp:BoundField DataField="CheckInCheckOutDates" HeaderText="Arrival Date / Departure Date"></asp:BoundField>
                                                    <asp:BoundField DataField="NoOfAdult" HeaderText="No Of Adults"></asp:BoundField>
                                                    <asp:BoundField DataField="NoOfChild" HeaderText="No Of Children"></asp:BoundField>
                                                    <asp:BoundField DataField="CreatedOn" HeaderText="Reservation Date"></asp:BoundField>
                                                    <asp:BoundField DataField="BookingThrough" HeaderText="Booking Method"></asp:BoundField>
                                                    <asp:BoundField DataField="BookingStatus" HeaderText="Booking Status"></asp:BoundField>
                                                    <asp:BoundField DataField="Accommodation" HeaderText="Accommodation"></asp:BoundField> --%>
                                                </Columns>
                                            </asp:GridView>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<%--   </div>
</div> --%>


<script type="text/javascript">

    jQuery(document).ready(function () {

        //$("#ClientList").msDropdown({ roundedCorner: false });
        //$("#DropDownListClient").msDropdown({ roundedCorner: false });

        jQuery("#ReservationDateTo").datepicker({
            defaultDate: null,
            dateFormat: 'dd/mm/yy',
            <%--dateFormat: "<%=DateTimeHelper.DateFormatJavaScript%>",--%>
            minDate: null,
            numberOfMonths: 1,
            showOn: "both",
            buttonImage: "resources/images/calendar.png",
            buttonImageOnly: false,
            buttonClass: "glyphicon-calendar",
        }).next(".ui-datepicker-trigger").addClass("btn btn-default glyphicon glyphicon-calendar calander-icon btn-calnder");


        jQuery("#ReservationDateFrom").datepicker({
            defaultDate: null,
            dateFormat: 'dd/mm/yy',
            minDate: null,
            numberOfMonths: 1,
            showOn: "both",
            buttonImage: "resources/images/calendar.png",
            buttonImageOnly: false,
            buttonClass: "glyphicon-calendar",

            onClose: function (selectedDate) {

                var reservationDateFrom = new Date(selectedDate);
                if (reservationDateFrom.toString() != "Invalid Date") {
                    reservationDateFrom.setDate(reservationDateFrom.getDate());
                }

                jQuery("#ReservationDateTo").datepicker("option", "minDate", reservationDateFrom);
                jQuery(".ui-datepicker-trigger").addClass("btn btn-default glyphicon glyphicon-calendar calander-icon btn-calnder")
            }

        }).next(".ui-datepicker-trigger").addClass("btn btn-default glyphicon glyphicon-calendar calander-icon btn-calnder");


        jQuery("#ArrivalDate").datepicker({
            defaultDate: null,
            dateFormat: 'dd/mm/yy',
                    <%--dateFormat: "<%=DateTimeHelper.DateFormatJavaScript%>",--%>
            minDate: null,
            numberOfMonths: 1,
            showOn: "both",
            buttonImage: "resources/images/calendar.png",
            buttonImageOnly: false,
            buttonClass: "glyphicon-calendar",
        }).next(".ui-datepicker-trigger").addClass("btn btn-default glyphicon glyphicon-calendar calander-icon btn-calnder");


        jQuery("#ReservationDate").datepicker({
            defaultDate: null,
            dateFormat: 'dd/mm/yy',
            <%--dateFormat: "<%=DateTimeHelper.DateFormatJavaScript%>",--%>
            minDate: null,
            numberOfMonths: 1,
            showOn: "both",
            buttonImage: "resources/images/calendar.png",
            buttonImageOnly: false,
            buttonClass: "glyphicon-calendar",

            onClose: function (selectedDate) {

                var reservationDate = new Date(selectedDate);
                if (reservationDate.toString() != "Invalid Date") {
                    reservationDate.setDate(reservationDate.getDate());
                }

                jQuery("#ArrivalDate").datepicker("option", "minDate", reservationDate);
                jQuery(".ui-datepicker-trigger").addClass("btn btn-default glyphicon glyphicon-calendar calander-icon btn-calnder")
            }

        }).next(".ui-datepicker-trigger").addClass("btn btn-default glyphicon glyphicon-calendar calander-icon btn-calnder");


        jQuery("form").submit(function (e) {

            if (SubmitClientClick() == false) {
                e.preventDefault();
            }

        });


        $("#tech").msDropdown(); //no need to pass main css name - it will use default theme
        $("#tech").msDropdown(); //no need to pass main css name - it will use default theme
        $("#mycomp").msDropdown({ mainCSS: 'blue' }); //you dont have to pass here if you have in select element- data-maincss="blue"
        $("#flat").msDropdown({ roundedCorner: false });
        $("#flat2").msDropdown({ roundedCorner: false });

        ////initial load
        //$(".specific-div").show();
        //$(".range-div").hide();     

        //$('input[type="radio"]').click(function () {
        //    if ($(this).attr("value") == "RadioRange") {
        //        $(".specific-div").hide();
        //        $(".range-div").show();
        //        alert("RadioRange");
        //    }
        //    if ($(this).attr("value") == "RadioSpecific") {
        //        $(".specific-div").show();
        //        $(".range-div").hide();
        //        alert("RadioSpecific");
        //    }

        //});

        if ($("input[type=radio][value='RadioSpecific']:checked").val()) {

            $(".range-div").hide();
            $(".specific-div").show();

        } else if ($("input[type=radio][value='RadioRange']:checked").val()) {

            $(".range-div").show();
            $(".specific-div").hide();
        }

    });

    $(".range").click(function () {
        $(".range-div").show();
        $(".specific-div").hide();
    });

    $(".specific").click(function () {
        $(".specific-div").show();
        $(".range-div").hide();
    });

    var providerSearch = jQuery('#TextBoxProvider');

    providerSearch.autocomplete({
        source: function (request, response) {
            $.ajax({
                type: "POST",
                url: "/webservices/autocomplete.asmx/GetSuggestions",
                data: JSON.stringify({ clientId: "<%=this.providerSearchItemId%>", term: providerSearch.val() }),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var d = eval(data);
                    response($.map(eval(d.d), function (item) {
                        return {
                            label: item.Text,
                            value: item.Text,
                            itemId: item.ItemId,
                            destinationType: item.Type
                        }
                    }));

                },
                error: function () {
                    response([]);
                }
                        });
        }
    });
</script>
