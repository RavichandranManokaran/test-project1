﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.IO;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Globalization;
using System.Web.Script.Serialization;
using Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal;
using Sitecore.Data.Items;
using Sitecore.Data;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Handlers.CS
{
    /// <summary>
    /// Summary description for OfflinePortalAlertsHandler
    /// </summary>
    public class OfflinePortalAlertsHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/javascript";
            context.Response.Write(LoadUserControl(context));
            context.Response.End();
        }

        protected string LoadUserControl(HttpContext context)
        {

            Database currentDB = Sitecore.Context.Database;
            Item data = currentDB.SelectItems("/sitecore/content/#offline-portal#/#home#//*[@@TemplateName='AlertMessages']").ToList().FirstOrDefault();
            using (Page page = new Page())
            {
                StringBuilder s = new StringBuilder();
                PlaceHolder f = new PlaceHolder();

                string url = "~/Layouts/SubLayouts/OfflinePortal/OfflinePortalAlerts.ascx";

                using (StringWriter writer = new StringWriter(CultureInfo.CurrentCulture))
                {
                    OfflinePortalAlerts avp = (OfflinePortalAlerts)page.LoadControl(url);
                    avp.Alerts = data;
                    f.Controls.Add(avp);
                    page.Controls.Add(f);
                    HttpContext.Current.Server.Execute(page, writer, false);
                    s.Clear();
                    s.Append(writer.ToString());
                }
                return s.ToString();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}