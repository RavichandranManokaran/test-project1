﻿
CREATE PROCEDURE [dbo].[LB_Supplierportal_RevenueReport]
@BreakdownbyCampaign BIT,
@DateType VARCHAR(10),
@Breakdown VARCHAR(10),
@ProviderGroup INT,
@Provider INT,
@OfferName INT,
@Partner VARCHAR(50),
@DateFrom DATETIME,
@DateTo DATETIME
AS
BEGIN
	SELECT 
		ISNULL(MAX(CI.Name),'') [Offername], 
		GroupByDate [Date], 
		SUM([Booking])[NoOfBookings], 
		Sum(ISNULL([Revenue],0)) [TotalRevenue]
	FROM
	(SELECT          
		CASE WHEN @BreakdownbyCampaign = 1 THEN OCR.[CampaignItemId] ELSE NULL END [CampaignItemId],
			CASE 
			WHEN @DateType = 'Arrival' THEN
			CASE 
				 WHEN @Breakdown = 'Monthly' THEN
					DATEADD(month, DATEDIFF(month, 0, RR.CheckinDate), 0)
				 WHEN @Breakdown = 'Weekly' THEN
					DATEADD(WEEK, DATEDIFF(week, 0, RR.CheckinDate), 0)
				 ELSE
					'1/1/1990'
			END
			WHEN @DateType = 'Reserve' THEN
			CASE 
				 WHEN @Breakdown = 'Monthly' THEN
					DATEADD(month, DATEDIFF(month, 0, Ol.CreatedOn), 0)
				 WHEN @Breakdown = 'Weekly' THEN
					DATEADD(WEEK, DATEDIFF(week, 0, Ol.CreatedOn), 0)
				 ELSE
					'1/1/1990'
			END
		END [GroupByDate],
		(OL.quantity) [Booking],
		(isnull(OLP.Amount,0)) [Revenue] 
	FROM  ucommerce_orderline OL
		INNER JOIN LB_RoomReservation RR on OL.OrderLineId = RR.OrderLineId
		INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId = PO.OrderId 
		INNER JOIN LB_CustomerAttribute CA ON PO.CustomerId = CA.CustomerId
		INNER JOIN uCommerce_Product P ON P.Sku = OL.Sku
		INNER JOIN uCommerce_CategoryProductRelation CPR ON P.ProductId = CPR.ProductId
		INNER JOIN uCommerce_Category CY ON Cy.CategoryId = CPR.CategoryId
		LEFT JOIN LB_OrderLinePayment OLP on OL.OrderLineId = OLP.OrderLineId
		LEFT JOIN [dbo].[uCommerce_OrderLineCampaignRelation] OCR ON OL.OrderLineId = OCR.OrderLineID
	WHERE 
		((@DateType = 'Arrival' AND RR.CheckInDate BETWEEN @DateFrom  AND @DateTo) OR
		(@DateType = 'Reserve' AND OL.CreatedOn BETWEEN @DateFrom  AND @DateTo)) AND
		(@ProviderGroup IS NULL OR @ProviderGroup = 0 OR CY.ParentCategoryId = @ProviderGroup) AND
		(@Provider IS NULL OR @Provider = 0 OR CY.CategoryId = @Provider) AND
		(@Partner IS NULL OR (CA.ClientId ) = @Partner) AND
		(@OfferName IS NOT NULL OR OCR.CampaignItemId = @OfferName)
		AND OCR.CampaignItemId IS NOT NULL
	) OA
	LEFT JOIN ucommerce_campaignitem CI ON OA.CampaignItemId=CI.CampaignItemId
	GROUP BY OA.CampaignItemId, GroupByDate
END
