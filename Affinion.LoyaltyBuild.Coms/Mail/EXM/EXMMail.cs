﻿using Sitecore.Data;
using Sitecore.Modules.EmailCampaign.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Comms.Mail.EXM
{

    /// <summary>
    /// Implementation for Imail Service
    /// </summary>
    public class EXMMail : IMail
    {
        /// <summary>
        /// private variables
        /// </summary>
        private MessageItem _message;
        private List<Recepient> _recepients;

        /// <summary>
        /// method to get messageItemId
        /// </summary>
        /// <param name="itemId">itemId</param>
        public EXMMail(string itemId)
        {
            var messageItemId = new ID(itemId);
            _message = Sitecore.Modules.EmailCampaign.Factory.GetMessage(messageItemId);
        }

        /// <summary>
        /// Message
        /// </summary>
        public MessageItem Message { get { return _message; } }

        /// <summary>
        /// Check Sitecore user or not
        /// </summary>
        public bool IsSitecoreUser { get; set; }

        /// <summary>
        /// email subject
        /// </summary>
        public string Subject
        {
            get
            {
                return _message.Subject;
            }
            set
            {
                _message.Subject = value;
            }
        }

        /// <summary>
        /// Email Body
        /// </summary>
        public string Body
        {
            get
            {
                return _message.Body;
            }
            set
            {
                _message.Body = value;
            }
        }

        /// <summary>
        /// Email Recepients
        /// </summary>
        public List<Recepient> Recepients
        {
            get
            {
                return _recepients;
            }
        }

        /// <summary>
        /// Add Recepients
        /// </summary>
        /// <param name="recepient"></param>
        public void AddRecipient(Recepient recepient)
        {
            _recepients.Add(recepient);
        }

        /// <summary>
        /// From address
        /// </summary>
        public string FromAddress
        {
            get
            {
                return _message.FromAddress;
            }
            set
            {
                _message.FromAddress = value;
            }
        }
    }
}
