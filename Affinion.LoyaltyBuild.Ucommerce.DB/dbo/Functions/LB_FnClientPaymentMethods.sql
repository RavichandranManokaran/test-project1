﻿CREATE FUNCTION [dbo].[LB_FnClientPaymentMethods]
(	
	@ClientId VARCHAR(50)
)
RETURNS @ClientPaymentMethodsTable TABLE
(
	PaymentMethodId INT,
	[Description] VARCHAR(100),
	AvailableOnline BIT,
	AvailableOffline BIT
) 
AS
BEGIN
    set @ClientId = REPLACE(@ClientId,'}','')
	set @ClientId = REPLACE(@ClientId,'{','')
	INSERT INTO @ClientPaymentMethodsTable
	SELECT 
	PM.PaymentMethodId, 
	PMD.DisplayName, 
	(CASE WHEN CHARINDEX ('online', PP2.Value) > 0 THEN 1 ELSE 0 END) IsOnline,
	(CASE WHEN CHARINDEX('offline', PP2.Value) > 0 THEN 1 ELSE 0 END) IsOffline
	FROM 
	uCommerce_PaymentMethod PM
	INNER JOIN uCommerce_PaymentMethodDescription PMD ON PM.PaymentMethodId = PMD.PaymentMethodId AND PMD.CultureCode = 'en'
	INNER JOIN uCommerce_PaymentMethodProperty PP1 ON PM.PaymentMethodId = PP1.PaymentMethodId
	INNER JOIN uCommerce_DefinitionField DF1 ON PP1.DefinitionFieldId = DF1.DefinitionFieldId AND DF1.Name = 'Client Id'
	INNER JOIN uCommerce_PaymentMethodProperty PP2 ON PM.PaymentMethodId = PP2.PaymentMethodId
	INNER JOIN uCommerce_DefinitionField DF2 ON PP2.DefinitionFieldId = DF2.DefinitionFieldId AND DF2.Name = 'Portal Type'
	WHERE PP1.Value LIKE '%' + @ClientId + '%'

	RETURN
END