﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalManageCustomerSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalManageCustomerSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<script type="text/javascript">

    jQuery(document).ready(function () {
        //Timer for error messages 
        $('.msg-customer').delay(10000).fadeOut();

        
        jQuery("#CountryDropDown").change(function () {
            var countryVal = jQuery("#CountryDropDown option:selected").html();
            $.get("/handlers/cs/ServerSideEventHandler.ashx?action=LoadLocationByCountry&value=" + countryVal, function (data) {
                if (data) {
                    var jsonData = JSON.parse(data); //This converts the string to json
                    $('#LocationDropDown').find('option').not(':first').remove();
                    for (var i = 0; i < jsonData.length; i++) //The json object has lenght
                    {
                        
                        var object = jsonData[i]; //You are in the current object
                        var objItems = object.split(",");                       
                        $('#LocationDropDown').append('<option value=' + objItems[0] + '>' + objItems[1] + '</option>'); //now you access the property.

                    }
                }
            });
        });

        jQuery("#LocationDropDown").change(function () {
            $("#hdfLocation").val(jQuery("#LocationDropDown :selected").text());
        });

        jQuery("#ClientDropDown").change(function () {
            loadstore();
        });
        
        jQuery("#StoreDropDown").change(function () {
            $("#hdfStore").val(jQuery("#StoreDropDown :selected").val());
        });

        $("#Email").change(function () {
            if($(this).length > 0)
                $("#SubscribeCheckBox").attr("disabled", false);

        });


        //if (jQuery("#ClientDropDown").val() != "") {
        //    loadstore();

        //    var t =$("#StoreDropDown");
        //    var items = $("#hdfStore").val();
        //    //$("#StoreDropDown option[value=" + items + "]").prop("selected", true);
        //    $("#StoreDropDown option:eq(1)").prop("selected", true);
        //    alert(jQuery("#StoreDropDown option:selected").html());
        //}

    });

    function loadstore()
    {
        var countryVal = jQuery("#ClientDropDown option:selected").html();
        $.get("/handlers/cs/ServerSideEventHandler.ashx?action=LoadStoreByClientID&value=" + countryVal, function (data) {
            if (data) {
                var jsonData = JSON.parse(data); //This converts the string to json
                $('#StoreDropDown').find('option').not(':first').remove();
                //$('#StoreDropDown').append('<option value=' + "0" + '>' + "Please Select" + '</option>'); //now you access the property.
                for (var i = 0; i < jsonData.length; i++) //The json object has lenght
                {
                    var object = jsonData[i]; //You are in the current object
                    // var objItems = object.split(",");
                    $('#StoreDropDown').append('<option value=' + object.Value + '>' + object.Text + '</option>'); //now you access the property.

                }
            }
        });
    }
    //Change Subscribe CSS Class
    function ChangeSubscribeCss() {

        jQuery("#divSubscribe").addClass("col-sm-12");
    }

    function FocusSearchResultGrid() {
        document.getElementById('data-grid').scrollIntoView();
    }
    //Edit form 
    function ExpanEditForm() {
        jQuery("#collapseCustomerEdit").addClass("panel-collapse collapse in");
        jQuery("#collapseCustomerEditAtag").removeClass();
        jQuery("#collapseCustomerEditAtag").addClass("show-hide-list");

    }

    //Subscribe
    function ExpanSubscribForm() {
        jQuery("#collapseCustomerSubscribe").addClass("panel-collapse collapse in");
        jQuery("#collapseCustomerSubscribeAtag").removeClass();
        jQuery("#collapseCustomerSubscribeAtag").addClass("show-hide-list");
    }

</script>


<div class="container booking-detail-info">

    <div class="content">

        <%--Add Customer--%>
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <%--Client Addition--%><%--<a id="collapseCustomerEditAtag" data-toggle="collapse" data-parent="#accordion" href="#collapseCustomerEdit" class="show-hide-list collapsed">--%>
                            <sc:Text Field="ManageTitle" runat="server" />
                            <%--      </a>--%>
                        </h4>
                    </div>

                    <div class="panel-body alert-info">

                        <div class="col-sm-12 no-padding-m">
                            <div class="content_bg_inner_box alert-info">
                                <div class="col-sm-6 no-padding-m">
                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <%-- Select Client:--%>
                                                <sc:Text Field="Client" runat="server" />
                                                <span class="accent58-f">*
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ValidationGroup="Edit" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="ClientDropDown"></asp:RequiredFieldValidator>
                                                </span>
                                            </span>
                                        </div>
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ClientDropDown" runat="server" CssClass="form-control font-size-12" ClientIDMode="Static" ></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <asp:HiddenField ID="CustomerId" runat="server" Value="0" />

                                                <sc:Text Field="CustomerTitle" runat="server" />
                                            </span>
                                        </div>
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="TitleDropDown" runat="server" CssClass="form-control font-size-12"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <%--       First Name:--%>
                                                <sc:Text Field="FirstName" runat="server" />
                                                <span class="accent58-f">*
                                                        <asp:RequiredFieldValidator ID="FirstNameRequiredFieldValidatorEdit" ValidationGroup="Edit" runat="server" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="FirstName"></asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="FirstNameRequiredFieldValidatorSearch" ValidationGroup="Search" runat="server" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="FirstName"></asp:RequiredFieldValidator>
                                                </span></span>
                                        </div>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="FirstName" class="form-control font-size-12 validateCreditCard" placeholder="First Name" runat="server"> </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <%--          Last Name:--%>
                                                <sc:Text Field="LastName" runat="server" />
                                                <span class="accent58-f">*
                                                        <asp:RequiredFieldValidator ID="LastNameRequiredFieldValidatorEdit" runat="server" ValidationGroup="Edit" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="LastName"></asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="LastNameRequiredFieldValidatorSearch" ValidationGroup="Search" runat="server" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="LastName"></asp:RequiredFieldValidator>
                                                </span></span>
                                        </div>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="LastName" class="form-control font-size-12 validateCreditCard" placeholder="Last Name" runat="server"> </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <%-- Address Line1:--%>
                                                <sc:Text Field="AddressLine1" runat="server" />
                                                <span class="accent58-f">*
                                                        <asp:RequiredFieldValidator ID="AddressLine1RequiredFieldValidator3" ValidationGroup="Edit" runat="server" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="AddressLine1"></asp:RequiredFieldValidator>
                                                </span></span>
                                        </div>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="AddressLine1" ValidationGroup="Edit" class="form-control font-size-12 validateCreditCard" placeholder="Address Line 1" runat="server"> </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <%-- Address Line2:--%>
                                                <sc:Text Field="AddressLine2" runat="server" />
                                            </span>
                                        </div>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="AddressLine2" class="form-control font-size-12 validateCreditCard" placeholder="Address Line 2" runat="server"> </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <%--Address Line3:--%>
                                                <sc:Text Field="AddressLine3" runat="server" />
                                            </span>
                                        </div>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="AddressLine3" class="form-control font-size-12 validateCreditCard" placeholder="Address Line 3" runat="server"> </asp:TextBox>
                                        </div>
                                    </div>
                                    <%--                                                <div class="row margin-row">
                                                    <div class="col-sm-5">
                                                        <span class="bask-form-titl font-size-12">
                                                            <%--Address Line3:
                                                            <sc:Text Field="AddressLine4" runat="server" />
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-7">
                                                        <asp:TextBox ID="AddressLine4" class="form-control font-size-12" placeholder="Address Line4" runat="server"> </asp:TextBox>
                                                    </div>
                                                </div>--%>
                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <%-- Subscribe--%>
                                                <sc:Text Field="Subscribe" runat="server" />
                                            </span>
                                        </div>
                                        <div class="col-sm-7">
                                            <asp:CheckBox ID="SubscribeCheckBox" ClientIDMode="Static" ValidationGroup="Edit" runat="server" Enabled="false" />
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="col-sm-6 no-padding-m">
                                    <%--<div class="row margin-row">
                                                    <div class="col-sm-5">
                                                        <span class="bask-form-titl font-size-12">
                                                            <%-- Select Client:
                                                            <sc:Text Field="Client" runat="server" />
                                                            <span class="accent58-f">*
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ValidationGroup="Edit" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="ClientDropDown"></asp:RequiredFieldValidator>
                                                            </span>
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-7">
                                                        <asp:DropDownList ID="ClientDropDown" runat="server" CssClass="form-control font-size-12"></asp:DropDownList>
                                                    </div>
                                                </div>--%>
                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <%--Country:--%>
                                                <sc:Text Field="Country" runat="server" />
                                                <span class="accent58-f">*
                                                    <asp:RequiredFieldValidator ID="CountryRequiredFieldValidator" runat="server" ValidationGroup="Edit" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="CountryDropDown"></asp:RequiredFieldValidator>
                                                </span>

                                            </span>
                                        </div>
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="CountryDropDown" ClientIDMode="Static" ValidationGroup="Edit" runat="server" CssClass="form-control font-size-12"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <%--Location:--%>
                                                <sc:Text Field="Location" runat="server" />
                                                <span class="accent58-f">*
                                                    <asp:RequiredFieldValidator ID="LocationRequiredFieldValidator" ValidationGroup="Edit" runat="server" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="LocationDropDown"></asp:RequiredFieldValidator>
                                                </span>
                                            </span>
                                        </div>
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="LocationDropDown" ClientIDMode="Static" ValidationGroup="Edit" runat="server" CssClass="form-control font-size-12"></asp:DropDownList>
                                            <asp:HiddenField ID="hdfLocation" ClientIDMode="Static" runat="server" />
                                        </div>
                                    </div>
                                    <%--                                                <div class="row margin-row">
                                                    <div class="col-sm-5">
                                                        <span class="bask-form-titl font-size-12">
                                                            <%-- Language:
                                                            <sc:Text Field="Language" runat="server" />
                                                            <span class="accent58-f">*
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ValidationGroup="Edit" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="LanguageDropDown"></asp:RequiredFieldValidator>
                                                            </span>
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-7">
                                                        <asp:DropDownList ID="LanguageDropDown" ValidationGroup="Edit" runat="server" CssClass="form-control font-size-12"></asp:DropDownList>

                                                    </div>
                                                </div>--%>

                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <%--Primary Phone:--%>
                                                <sc:Text Field="PrimaryPhone" runat="server" />
                                                <span class="accent58-f">*      
                                                                 <asp:RequiredFieldValidator ID="PrimaryPhoneRequiredFieldValidator" runat="server" ValidationGroup="Edit" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="PrimaryPhone"></asp:RequiredFieldValidator>
                                                </span>
                                                <span class="accent58-f display-block-val">
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorPhone" runat="server" ValidationGroup="Edit" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="PrimaryPhone"></asp:RegularExpressionValidator>
                                                </span>
                                            </span>

                                        </div>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="PrimaryPhone" ValidationGroup="Edit" class="form-control font-size-12 validateCreditCard" placeholder="Phone" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <%--Secondary Phone:--%>
                                                <sc:Text Field="SecondaryPhone" runat="server" />
                                                <span class="accent58-f display-block-val">
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorSecondaryPhone" runat="server" ValidationGroup="Edit" ErrorMessage="Invalid Entry" ControlToValidate="SecondaryPhone"></asp:RegularExpressionValidator>
                                                </span>
                                            </span>
                                        </div>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="SecondaryPhone" class="form-control font-size-12 validateCreditCard" placeholder="Phone" runat="server"> </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <sc:Text Field="Store" runat="server" />
                                                <%--<span class="accent58-f">*      
                                                   <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ValidationGroup="Edit" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="StoreDropDown"></asp:RequiredFieldValidator>
                                                </span>--%>
                                            </span>
                                        </div>
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="StoreDropDown" ClientIDMode="Static" ValidationGroup="Edit" runat="server" CssClass="form-control font-size-12" ></asp:DropDownList>
                                             <asp:HiddenField ID="hdfStore" ClientIDMode="Static" runat="server" />
                                        </div>
                                    </div>
                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <%--Email:--%>
                                                <sc:Text Field="Email" runat="server" />
                                                <span class="accent58-f">*      
                                                   <asp:RequiredFieldValidator ID="EmailRequiredFieldValidator" runat="server" ValidationGroup="Edit" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="Email"></asp:RequiredFieldValidator>
                                                </span>
                                                <span class="accent58-f">
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidatorEmail" runat="server" ValidationGroup="Edit" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="Email"></asp:RegularExpressionValidator>
                                                </span>
                                            </span>
                                        </div>
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="Email" class="form-control font-size-12 validateCreditCard" ClientIDMode="Static" placeholder="Email" runat="server" ValidationGroup="Edit" ></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <%--Language:--%>
                                                <sc:Text Field="Language" runat="server" />
                                            </span>
                                        </div>
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="LanguageDropDown" ValidationGroup="Edit" runat="server" CssClass="form-control font-size-12"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row margin-row">
                                        <div class="col-sm-5">
                                            <span class="bask-form-titl font-size-12">
                                                <%-- Status:--%>
                                                <sc:Text Field="Active" runat="server" />
                                            </span>
                                        </div>
                                        <div class="col-sm-7">
                                            <asp:CheckBox ID="ActiveCheckBox" ValidationGroup="Edit" runat="server" Checked="true" />
                                            <%--<asp:DropDownList ID="StatusDropDownList" ValidationGroup="Edit" runat="server" CssClass="form-control font-size-12"></asp:DropDownList>--%>
                                        </div>
                                    </div>
                                    <div class="row margin-row">
                                        <div class="col-sm-3">
                                            <%--Enabled="false"--%>
                                            <asp:Button ID="EditAndSave" runat="server" Enabled="false"  Text="" ValidationGroup="Edit" class="btn btn-default add-break pull-right width-full font-size-12" OnClick="EditAndSave_Click" />
                                        </div>
                                        <div class="col-sm-3">
                                            <asp:Button ID="AddAndSave" runat="server" Text="" ValidationGroup="Edit" class="btn btn-default add-break pull-right width-full font-size-12" OnClick="AddAndSave_Click" />
                                        </div>
                                        <div class="col-sm-3">
                                            <asp:Button ID="Search" ValidationGroup="Search" runat="server" Text="" class="btn btn-default add-break pull-right width-full search-btn" OnClick="Search_Click" />
                                        </div>
                                        <div class="col-sm-3">
                                            <asp:Button ID="Reset" runat="server" Text="" class="btn btn-default add-break pull-right width-full font-size-12" OnClick="Reset_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>

        <%--Messages--%>
        <div id="errormessage" class="alert alert-danger msg-customer" runat="server" visible="false" align="center">
            <b>
                <asp:Literal ID="ErrorMessageLiteral" runat="server" Text="No Result Found" />
            </b>
        </div>
        <div class="alert alert-success msg-customer" runat="server" id="successmessage" visible="false" align="center">
            <b>
                <asp:Literal ID="SuccessLiteral" runat="server" Text="New Customer Added Successfully" />
            </b>
        </div>
        <%-- End Messages--%>
        <%-- Result Grid--%>
        <div runat="server" id="ResultDiv" visible="false">

            <%-- <div class="row">--%><%-- <div class="col-sm-12">--%><%--<div class="panel panel-default"> --%><%--      <div class="panel-heading">
                                    <h4 class="panel-title">Search Detail</h4>
                                </div>--%><%--            <div class="panel-body alert-info">--%>
            <div id="data-grid" class="table-responsive">
                <asp:GridView ID="ResultGridView" runat="server" OnSelectedIndexChanged="ResultGridView_SelectedIndexChanged" CssClass="table tbl-calendar" OnRowEditing="ResultGridView_RowEditing">
                    <Columns>
                        <%--  <asp:BoundField DataField="CustomerId" />
                        <asp:BoundField DataField="FullName" HeaderText="Customer" />
                        <asp:BoundField DataField="FullAddress" HeaderText="Address" />
                        <asp:BoundField DataField="EmailAddress" HeaderText="Email Address" />
                        <asp:BoundField DataField="MobilePhoneNumber" HeaderText="Mobile Phone No" />--%>
                        <%-- <asp:CommandField ShowSelectButton="True" SelectText="<i class='glyphicon glyphicon-edit icons-positionRight'></i>" />--%>
                    </Columns>
                </asp:GridView>
            </div>
            <%-- </div>--%><%-- </div>--%>            <%-- </div>--%><%-- </div>--%><%--   </div>--%><%--  End Result Grid--%>
        </div>
        <%--Subscription--%>
        <div class="row">
            <div class="col-sm-12">
                <div class="panel-group" id="accordionSubscription">
                    <div class="panel panel-default">
                        <div class="panel-heading bg-primary">
                            <h4 class="panel-title">
                                <a id="collapseCustomerSubscribeAtag" data-toggle="collapse" data-parent="#accordionSubscription" href="#collapseCustomerSubscribe" class="show-hide-list collapsed">

                                    <sc:Text Field="SubscribeTitle" runat="server" />
                                </a>
                            </h4>
                        </div>
                        <div id="collapseCustomerSubscribe" class="panel-collapse collapse">
                            <div class="panel-body alert-info">
                                <div class="col-sm-12 no-padding-m">
                                    <div class="content_bg_inner_box alert-info">
                                        <div class="col-sm-6 no-padding-m">

                                            <div class="row margin-row">
                                                <div class="col-sm-5">
                                                    <span class="bask-form-titl font-size-12">

                                                        <sc:Text Field="Client" runat="server" />
                                                        <span class="accent58-f">*
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorClientSubscribe" runat="server" ValidationGroup="Subscribe" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="DropDownClientListSubscribe"></asp:RequiredFieldValidator>
                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ValidationGroup="Unsubscribe" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="DropDownClientListSubscribe"></asp:RequiredFieldValidator>--%>
                                                        </span>
                                                    </span>
                                                </div>
                                                <div class="col-sm-7">
                                                    <asp:DropDownList ID="DropDownClientListSubscribe" runat="server" CssClass="form-control font-size-12"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 no-padding-m">
                                            <div class="row margin-row">
                                                <div class="col-sm-5">
                                                    <span class="bask-form-titl font-size-12">

                                                        <sc:Text Field="Email" runat="server" />
                                                        <span class="accent58-f">*
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ValidationGroup="Subscribe" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="EmailSubscribe"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorEmailSubscription" runat="server" ValidationGroup="Subscribe" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="EmailSubscribe"></asp:RegularExpressionValidator>
                                                            <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ValidationGroup="Unsubscribe" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="EmailSubscribe"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorEmailUnsubscription" runat="server" ValidationGroup="Unsubscribe" ErrorMessage="<span class='fa fa-warning'></span>" ControlToValidate="EmailSubscribe"></asp:RegularExpressionValidator>--%>
                                                        </span>
                                                        <span class="accent58-f display-block-val">
                                                            <asp:Literal ID="SubscribeMessage" runat="server" Text="" />
                                                        </span>
                                                    </span>
                                                </div>
                                                <div class="col-sm-7">

                                                    <asp:TextBox ID="EmailSubscribe" class="form-control font-size-12 validateCreditCard" placeholder="Email" runat="server" ValidationGroup="Subscribe"></asp:TextBox>

                                                </div>
                                            </div>

                                            <div class="row margin-row">
                                                <div class="col-sm-4">
                                                    <asp:Button ID="ButtonSubscribe" runat="server" Text="" ValidationGroup="Subscribe" class="btn btn-default add-break pull-right width-full" OnClick="Subscribe_Click" />
                                                </div>
                                                <div class="col-sm-4">
                                                    <asp:Button ID="UnSubscribe" runat="server" Text="" ValidationGroup="Subscribe" class="btn btn-default add-break pull-right width-full" OnClick="UnSubscribe_Click" />
                                                </div>
                                                <div class="col-sm-4">
                                                    <asp:Button ID="SubscribeReset" runat="server" Text="" class="btn btn-default add-break pull-right width-full" OnClick="SubscribeReset_Click" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

