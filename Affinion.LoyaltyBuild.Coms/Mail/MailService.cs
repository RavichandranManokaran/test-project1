﻿using System.Reflection;
using System.Linq;
using System;
using System.Collections.Generic;
using Affinion.LoyaltyBuild.Comms.Mail.EXM;
using Affinion.LoyaltyBuild.Comms.Mail.SMTP;

namespace Affinion.LoyaltyBuild.Comms.Mail
{ 
    /// <summary>
    /// abstract class for EXM and SMTP mail Creation
    /// </summary>
    public abstract class MailService
    {
        /// <summary>
        /// create mailer
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static MailService CreateMailer(MailType type)
        {
            switch (type)
            {
                case MailType.EXM:
                    return new EXMService();
                case MailType.SMTP:
                    return new SmtpMailService();
            }

            return null;
        }

        /// <summary>
        ///Method For Send Mail
        /// </summary>
        /// <param name="mail">Imail</param>
        public abstract void SendMail(IMail mail);
    }
    
    /// <summary>
    /// Enum of Mail Types
    /// </summary>
    public enum MailType
    {
        EXM,
        SMTP
    }
}
