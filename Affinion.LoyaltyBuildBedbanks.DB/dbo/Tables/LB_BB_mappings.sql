
/****** Object:  Table [dbo].[LB_BB_mappings]    Script Date: 7/19/2016 8:00:41 PM ******/

CREATE TABLE [dbo].[LB_BB_mappings](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[mapFrom] [varchar](50) NOT NULL,
	[mapFromRecordID] [int] NOT NULL,
	[mapsFromColumn] [varchar](50) NOT NULL,
	[mapsTo] [varchar](50) NOT NULL,
	[mapToRecordID] [int] NOT NULL,
	[mapsToColumn] [varchar](50) NOT NULL,
 CONSTRAINT [PK__mappings] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


