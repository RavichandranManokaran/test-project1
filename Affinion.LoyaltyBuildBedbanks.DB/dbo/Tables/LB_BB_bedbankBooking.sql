
CREATE TABLE [dbo].[LB_BB_bedbankBooking](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[bookingID] [int] NOT NULL,
	[bedbankProviderID] [int] NOT NULL,
	[roomType] [varchar](150) NOT NULL,
	[mealBasis] [varchar](150) NOT NULL,
	[wholeSaleAmount] [money] NOT NULL,
	[propertyMarginID] [int] NOT NULL,
	[LBOrderLineID] [int] NULL,
	[LBBookingReference] [varchar](50) NULL,
	[MarginAmount] [decimal](18, 0) NULL,
	[MarginPercentage] [decimal](18, 0) NULL,
	[Location] [varchar](50) NULL,
	[Map Location] [varchar](50) NULL,
 CONSTRAINT [PK_bedbankBooking] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
