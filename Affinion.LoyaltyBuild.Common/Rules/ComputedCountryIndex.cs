﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using System.Threading.Tasks;
using Sitecore.Data.Items;
using Sitecore.Data;
using Sitecore.Diagnostics;

namespace Affinion.LoyaltyBuild.Common.Rules
{
    public class ComputedCountryIndex : IComputedIndexField
    {
        public object ComputeFieldValue(IIndexable indexable)
        {
            //TRANSLATE THE INDEXABLE PARAM INTO AN ITEM
            var item = indexable as SitecoreIndexableItem;

            Assert.ArgumentNotNull(item, "item");

            //CHECK IF THE ITEM HAS A 'COUNTRY' FIELD (IF IT DOESN'T WE RETURN NULL)
            IIndexableDataField countryField = indexable.GetFieldByName("Country");
            if (countryField != null)
            {
                //GET THE ACTUAL COUNTRY ITEM (USING THE SAME DB) AND RETURN THE 'TITLE' VALUE
                Item country = item.Item.Database.GetItem(new ID(countryField.Value.ToString()));
                return country["Title"];
            }
            return null;
        }

        public string FieldName { get; set; }

        public string ReturnType { get; set; }
    }
}
