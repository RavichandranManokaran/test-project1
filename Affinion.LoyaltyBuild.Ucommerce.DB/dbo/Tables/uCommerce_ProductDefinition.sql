﻿CREATE TABLE [dbo].[uCommerce_ProductDefinition] (
    [ProductDefinitionId] INT              IDENTITY (1, 1) NOT NULL,
    [Name]                NVARCHAR (512)   NOT NULL,
    [Description]         NVARCHAR (MAX)   NULL,
    [Deleted]             BIT              CONSTRAINT [uCommerce_DF_ProductDefinition_Deleted] DEFAULT ((0)) NOT NULL,
    [SortOrder]           INT              CONSTRAINT [DF_uCommerce_ProductDefinition_SortOrder] DEFAULT ((0)) NOT NULL,
    [Guid]                UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [CreatedOn]           DATETIME         CONSTRAINT [DF_uCommerce_ProductDefinition_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]           NVARCHAR (50)    DEFAULT ('') NOT NULL,
    [ModifiedOn]          DATETIME         CONSTRAINT [DF_uCommerce_ProductDefinition_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]          NVARCHAR (50)    DEFAULT ('') NOT NULL,
    CONSTRAINT [uCommerce_PK_ProductDefinition] PRIMARY KEY CLUSTERED ([ProductDefinitionId] ASC),
    CONSTRAINT [uCommerce_FK_ProductDefinition_ProductDefinition] FOREIGN KEY ([ProductDefinitionId]) REFERENCES [dbo].[uCommerce_ProductDefinition] ([ProductDefinitionId])
);

