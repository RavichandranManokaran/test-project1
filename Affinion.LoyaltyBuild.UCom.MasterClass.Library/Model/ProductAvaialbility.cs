﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    public class SearchAvaialbility
    {
        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }

        public List<string> ProductDetail { get; set; }

    }
    public class ProductAvaialbility
    {
        public string SKU { get; set; }
        public string VariantSKU { get; set; }
        public decimal Price { get; set; }
        public int RoomAvaialbility { get; set; }
        public List<ProdutDayWiseAvaialbility> DayWiseAvaialbility { get; set; }
    }

    public class ProdutDayWiseAvaialbility
    {
        public int Date { get; set; }
        public decimal Price { get; set; }
        public int RoomAvaialbility { get; set; }
    }
}
