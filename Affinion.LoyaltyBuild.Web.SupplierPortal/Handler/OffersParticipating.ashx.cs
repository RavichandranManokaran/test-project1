﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.Availability;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.Availability.SupplierPortal.Controls;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Offer;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Affinion.LoyaltyBuild.UCom.Common.Extension;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Handler
{
    /// <summary>
    /// Summary description for OffersParticipating
    /// </summary>
    public class OffersParticipating : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                
                context.Response.ContentType = "text/plain";
                string sDate = context.Request["startDate"];
                string eDate = context.Request["endDate"];
                string month = context.Request["month"];
                string offerId = context.Request["OfferId"];
                if (!String.IsNullOrEmpty(sDate) && !String.IsNullOrEmpty(eDate))
                {
                    DateTime startDate = sDate.ParseFormatDateTime();// DateTime.ParseExact(sDate, formats, CultureInfo.InvariantCulture, DateTimeStyles.None);
                    DateTime endDate = eDate.ParseFormatDateTime(); // DateTime.ParseExact(eDate, formats, CultureInfo.InvariantCulture, DateTimeStyles.None);
                    context.Response.Write(LoadOffers(offerId, startDate, endDate, Convert.ToInt32(month), context));
                }

                //else
                //{
                //    //string msg = "Please configure hotel in sitecore to manage room availability ";
                //    context.Response.Write(string.Empty);
                //}
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(ex.Message, new object());
            }
        }

        protected static string LoadOffers(string offerId, DateTime startDate, DateTime endDate, int month, HttpContext context)
        {
            using (Page page = new Page())
            {
                StringBuilder s = new StringBuilder();
                AvailabilityHelper dataForOffers = new AvailabilityHelper(context);
                List<SupplierInviteDetail> supplierInviteData = new List<SupplierInviteDetail>();
                bool loadprice = true;
                supplierInviteData = dataForOffers.GetCampaignDetailBySupplier(startDate, endDate, loadprice);
                PlaceHolder f = new PlaceHolder();
                using (StringWriter writer = new StringWriter(CultureInfo.CurrentCulture))
                {
                    if (string.IsNullOrEmpty(offerId) || offerId == "0")
                    {
                        OffersView avp = (OffersView)page.LoadControl("~/Layouts/SubLayouts/SupplierPortal/Controls/Availability/OffersView.ascx");
                        avp.currentOffers = supplierInviteData;
                        f.Controls.Add(avp);
                        page.Controls.Add(f);
                    }
                    else
                    {
                        DisplayOffer avp = (DisplayOffer)page.LoadControl("~/Layouts/SubLayouts/SupplierPortal/Controls/Offer/DisplayOffer.ascx");
                        avp.currentOffers = supplierInviteData;
                        avp.IsOfferRestriction = true;
                        f.Controls.Add(avp);
                        page.Controls.Add(f);
                    }
                    HttpContext.Current.Server.Execute(page, writer, false);
                    s.Clear();
                    s.Append(writer.ToString());
                }


                return s.ToString();
            }

        }

        public bool IsReusable
        {
            get { return true; }
        }
    }
}