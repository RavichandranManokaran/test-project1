﻿using System.Configuration;

namespace Affinion.LoyaltyBuild.BedBanks.Troika.Config
{
    public static class TroikaConfigController
    {
        public static Client GetClient(string clientId)
        {
            var clientConfigSection = ConfigurationManager.GetSection("troikaConfigSection") as TroikaConfigSection;

            if (clientConfigSection == null || clientConfigSection.Members == null || clientConfigSection.Members.Count < 1)
                return null;

            return clientConfigSection.Members[clientId];
        }

        public static Currency GetClientCurrency(string clientId, string currencyId)
        {
            Client c = GetClient(clientId);
            
            if (c == null || c.Members == null || c.Members.Count < 1)
                return null;

            return c.Members[currencyId];
        }
    }
}
