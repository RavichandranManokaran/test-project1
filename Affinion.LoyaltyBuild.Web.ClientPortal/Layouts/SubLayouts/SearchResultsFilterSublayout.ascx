﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchResultsFilterSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.SearchResultsFilterSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<script>

    function activate_accordion(course_id) {
        var active_item;
        if (course_id == false) {
            active_item = false;
        } else {
            active_item = $('.accordion div[id="course-' + course_id + '"]').attr('active');
        }

        $(".accordion").accordion({
            collapsible: true,
            active: parseInt(active_item),
            heightStyle: "content",
            icons: {
                "header": "ui-icon-plus",
                "activeHeader": "ui-icon-minus"
            }
        });
    }

</script>

<div class="col-xs-12 no-padding">
    <div class="advancedSearchLink margin-common-inner-box">
        <div class="accordion ui-accordion ui-widget ui-helper-reset" role="tablist">
            <div id="course-25" class="ui-accordion-header ui-state-default ui-accordion-icons ui-accordion-header-active ui-state-active ui-corner-top" role="tab" aria-controls="ui-id-1" aria-selected="true" aria-expanded="true" tabindex="0"><span class="ui-accordion-header-icon ui-icon ui-icon-minus"></span>Filter by: Themes &amp; Facilities</div>
            <div class="theme-facility-inner alert alert-info  ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content-active" id="ui-id-1" aria-labelledby="course-25" role="tabpanel" aria-hidden="false" style="display: block;">
                <div class="theme-common">
                    <div class="col-btn-list-fs">
                        <button type="button" class="btn theme width-full text-left btn-common collapsed" data-toggle="collapse" data-target="#tf-icon1" aria-expanded="false">
                            Themes
                        </button>
                    </div>
                    <div id="tf-icon1" class="collapse" aria-expanded="false" style="height: 0px;">
                        <div class="row">
                            <div class="col-xs-9">
                                <label class="checkbox-inline">
                                    <input type="checkbox" value="" aria-hidden="True" >City/Large Town</label>
                            </div>
                            <div class="col-xs-3">
                                <span>(52)</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="theme-common">
                    <div class="col-btn-list-fs">
                        <button type="button" class="btn facilities width-full text-left btn-common collapsed" data-toggle="collapse" data-target="#tf-icon2" aria-expanded="false">
                            Facilities
                        </button>
                    </div>
                    <div id="tf-icon2" class="collapse" aria-expanded="false" style="height: 0px;">
                        <div class="row">
                            <div class="col-xs-9">
                                <label class="checkbox-inline">
                                    <input type="checkbox" value="">City/Large Town</label>
                            </div>
                            <div class="col-xs-3">
                                <span>(52)</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="theme-common">
                    <div class="col-btn-list-fs">
                        <button type="button" class="btn facilities width-full text-left btn-common collapsed" data-toggle="collapse" data-target="#tf-icon3" aria-expanded="false">
                            Experiences
                        </button>
                    </div>
                    <div id="tf-icon3" class="collapse">
                        <div class="row">
                            <div class="col-xs-9">
                                <label class="checkbox-inline">
                                    <input type="checkbox" value="">City/Large Town</label>
                            </div>
                            <div class="col-xs-3">
                                <span>(52)</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="theme-common">
                    <div class="col-btn-list-fs">
                        <button type="button" class="btn facilities width-full text-left btn-common collapsed" data-toggle="collapse" data-target="#tf-icon4" aria-expanded="false">
                            Star Ranking
                        </button>
                    </div>
                    <div id="tf-icon4" class="collapse" aria-expanded="false" style="height: 0px;">
                        <div class="row">
                            <div class="col-xs-9">
                                <label class="checkbox-inline">
                                    <input type="checkbox" value="">City/Large Town</label>
                            </div>
                            <div class="col-xs-3">
                                <span>(52)</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <asp:Button type="submit" ID="Filter" class="btn btn-default add-break" OnClick="Filter_Click" runat="server" />
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function () {

        activate_accordion(40);
        jQuery("table tr:even").css({ "background-color": "#fafefe", "border-top": "solid 1px #cff2f6", "border-bottom": "solid 1px #cff2f6" });
        jQuery("table tr:odd").css("background-color", "#fff");

    });
</script>
