﻿using System;

namespace Affinion.LoyaltyBuild.Api.Booking.Data
{
    /// <summary>
    /// Addon booking
    /// </summary>
    public class AddOnBooking : Booking
    {
        /// <summary>
        /// Constructor to load booking info
        /// </summary>
        /// <param name="orderLineId"></param>
        public AddOnBooking(int orderLineId)
            : base(orderLineId)
        {
        }

        /// <summary>
        /// Get product details
        /// </summary>
        /// <returns></returns>
        public override Model.Product.IProduct GetProduct()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get product details
        /// </summary>
        /// <returns></returns>
        public override Model.Provider.IProvider GetProvider()
        {
            throw new NotImplementedException();
        }
    }
}
