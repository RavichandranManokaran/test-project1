﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MarginManagementErrorEditSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.BedBanks.MarginManagementErrorEditSublayout" %>

<style type="text/css">
    .ddl {
        width:120% !important;
    }
</style>
<asp:HyperLink ID="backLink" CssClass="form form-control" runat="server" Text="Back To Search Result"></asp:HyperLink>
<div class="col-sm-12 marT15" id="addMarginDiv" runat="server">
								<div class="accordian-outer">
									<div class="panel-group" id="accordion">
										<div class="panel panel-default">
											<div class="panel-heading bg-primary">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="show-hide-list"><sc:Text id="title" runat="server" field="Title Text"/></a>
												</h4>
											</div>
											<div id="collapse2" class="panel-collapse collapse in">
												<div class="panel-body"> 
													<div class="row margin-row">
														<div class="col-md-4 no-padding">
															<div class="col-md-4 no-padding"><sc:Text id="ddlText" runat="server" field="DropDown Label Text"/></div>
															<div class="col-md-8 no-padding">
                                                                <asp:DropDownList ID="ddlErrorList" OnSelectedIndexChanged="ddlErrorList_SelectedIndexChanged" AutoPostBack="true" CssClass="form form-control ddl" runat="server" />
															</div>
														</div>
													</div>
													<div class="row margin-row">
														<div class="col-md-4 no-padding"></div>
														<div class="col-md-4 no-padding"></div>
														<div class="col-md-4 no-padding">
														
														</div>
													</div>												
												</div>
											</div> 
										</div>    
									</div>
								</div> 
							</div>   
<div class="col-sm-12">
    <div class="table-responsive">
        <asp:GridView ID="gvSearchData" runat="server" CssClass="table table-striped" OnRowCommand="gvSearchData_RowCommand" AutoGenerateEditButton="false" AutoGenerateColumns="false">
            <Columns>
                <asp:BoundField DataField="ProviderName" HeaderText="Provider Name" />
                <asp:BoundField DataField="country" HeaderText="Country" />
                <asp:BoundField DataField="location" HeaderText="Location" />
                <asp:BoundField DataField="mapLocation" HeaderText="Map Location" />
                <asp:BoundField DataField="address" HeaderText="Address" />
                <asp:TemplateField HeaderText="Action">
                    <ItemTemplate>
                        <asp:Button ID="btnEdit" runat="server" CausesValidation="false" CommandName="EditMargin" CssClass="editButton"
                            Text="Edit" CommandArgument=<%# Eval("ProviderId") %>/>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
</div>
   <script>
       $(window).on('load', function () {
           target = $('.bedbank-rightpanel').height();
           $('.bedbank-leftpanel').height(target);
       });
   </script>