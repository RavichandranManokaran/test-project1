﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration.SupplierPortal.User
{
    public enum UserRole
    {
        Supplier,
        SupplierGroupAdmin,
        LoyaltyAdmin
    }
}