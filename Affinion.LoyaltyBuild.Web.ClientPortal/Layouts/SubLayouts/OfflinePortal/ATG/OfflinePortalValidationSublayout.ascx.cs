﻿using Affinion.Loyaltybuild.BusinessLogic.Helper;
using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.DataAccess.Models;
using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
using Affinion.LoyaltyBuild.Search.Helpers;
using Sitecore.Data;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using Affinion.LoyaltyBuild.Common.SessionHelper;
using UCommerce.Api;
using Sitecore.Data.Managers;
namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.ATG
{
    public partial class OfflinePortalValidationSublayout : BaseSublayout
    {
        bool isATG = false;

        /// <summary>
        /// page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            LoadCustomerValidation();
            BindSitecoreTexts();
        }

        /// <summary>
        /// Load validation controls
        /// </summary>
        private void LoadCustomerValidation()
        {
            Item item = Sitecore.Context.Database.GetItem("{F8D66343-5F4A-4D86-A282-1047D487FFC9}");
            this.isATG = SitecoreFieldsHelper.GetBoolean(item, Constants.IsATGFieldName);
            this.EmailTitleText.Item = item;
            this.ConfirmEmailTitleText.Item = item;
            this.ContinueToPersonalDetailTitle.Item = item;
            if (this.isATG)
            {
                this.EmailList1ConsentLabel.Item = item;
                this.EmailList2ConsentLabel.Item = item;
                this.InstructionText.Item = item;
                this.EmailValidationTitleText.Item = item;
            }
            else
            {
                this.FirstDescription.Item = item;
                this.SecondDescription.Item = item;
                this.ThirdDescription.Item = item;
            }
            SetControlVisibility(this.isATG);
            btnToPersonalDetail.Attributes["class"] += "sec-payment";
        }

        /// <summary>
        /// btnToPersonalDetail_Click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnToPersonalDetail_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Item item = Sitecore.Context.Database.GetItem("{F8D66343-5F4A-4D86-A282-1047D487FFC9}");
                isATG = SitecoreFieldsHelper.GetBoolean(item, Constants.IsATGFieldName);
                if (this.isATG)
                {
                    //HandleMailLists(txtConfirmEmail.Text);

                }

                Sitecore.Events.Event.RaiseEvent("lb:PartnerValidationSuccess", txtConfirmEmail.Text);
                
                if (EmailList1ConsentCheckBox.Checked && EmailList2ConsentCheckBox.Checked)
                    Sitecore.Events.Event.RaiseEvent("lb:Subscribtion", Constants.ServiceAndPromotional);
                else if (EmailList1ConsentCheckBox.Checked)
                    Sitecore.Events.Event.RaiseEvent("lb:Subscribtion", Constants.PromotionalMail);
                else if (EmailList2ConsentCheckBox.Checked)
                    Sitecore.Events.Event.RaiseEvent("lb:Subscribtion", Constants.ServiceImprovement);
            }

        }

        /// <summary>
        /// GetClientID
        /// </summary>
        /// <returns></returns>
        private static string GetClientID()
        {
            int? orderId = BasketHelper.GetBasketOrderId();
            var clientId = BasketHelper.GetOrderClientId(orderId.Value);
            return clientId;
        }

        /// <summary>
        /// CustomerID
        /// </summary>
        /// <returns></returns>
        private static int CustomerID()
        {
            int UserId = 68;
            return UserId;
        }

        /// <summary>
        /// HandleMailLists
        /// </summary>
        /// <param name="email"></param>
        private void HandleMailLists(string email)
        {
            Item rootItem = Sitecore.Context.Database.GetItem(Sitecore.Context.Site.RootPath);

            try
            {
                if (EmailList1ConsentCheckBox.Checked && EmailList2ConsentCheckBox.Checked)
                {
                    uCommerceConnectionFactory.AddSubscribedCustomer(GetClientID(), CustomerID(), txtConfirmEmail.Text, Constants.ServiceAndPromotional);
                }

                else if (EmailList1ConsentCheckBox.Checked)
                {
                    uCommerceConnectionFactory.AddSubscribedCustomer(GetClientID(), CustomerID(), txtConfirmEmail.Text, Constants.PromotionalMail);
                }
                else if (EmailList2ConsentCheckBox.Checked)
                {
                    uCommerceConnectionFactory.AddSubscribedCustomer(GetClientID(), CustomerID(), txtConfirmEmail.Text, Constants.ServiceImprovement);
                }
            }
            catch (ArgumentException exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, exception, this);
            }
        }

        /// <summary>
        /// Set Control Visibility
        /// </summary>
        /// <param name="isATG"></param>
        private void SetControlVisibility(bool isATG)
        {
            if (isATG)
            {
                FirstDescriptionDiv.Visible = false;
                SecondDescriptionDiv.Visible = false;
                ThirdDescriptionDiv.Visible = false;
            }
            else
            {
                EmailList1ConsentCheckBox.Visible = false;
                EmailList2ConsentCheckBox.Visible = false;
                EmailListConsent1Div.Visible = false;
                EmailListConsent2Div.Visible = false;
            }
        }

        /// <summary>
        /// Bind sitecore texts
        /// </summary>
        private void BindSitecoreTexts()
        {
            Item conditionalMessageItem = this.GetItemFromRenderingParameterPath("Conditional Messages Link");
            string emailisRequiredMessageFieldName = "Email is Required Message";
            string emailValidationMessageFieldName = "Email Validation Message";
            string emailMismatchMessageFieldName = "Email Mismatch Message";
            string emailconfirmMessageFieldName = "Confirm Email Message";
            SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, emailisRequiredMessageFieldName, txtEmailRequiredText);
            SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, emailValidationMessageFieldName, txtvalidEmailText);
            SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, emailMismatchMessageFieldName, txtEmailMismatchText);
            SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, emailconfirmMessageFieldName, txtConfirmEmailText);
        }
    }
}