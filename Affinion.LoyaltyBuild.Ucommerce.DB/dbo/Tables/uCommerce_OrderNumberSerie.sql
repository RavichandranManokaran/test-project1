﻿CREATE TABLE [dbo].[uCommerce_OrderNumberSerie] (
    [OrderNumberId]   INT              IDENTITY (1, 1) NOT NULL,
    [OrderNumberName] NVARCHAR (128)   NOT NULL,
    [Prefix]          NVARCHAR (50)    NULL,
    [Postfix]         NVARCHAR (50)    NULL,
    [Increment]       INT              NOT NULL,
    [CurrentNumber]   INT              NOT NULL,
    [Deleted]         BIT              DEFAULT ((0)) NOT NULL,
    [Guid]            UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    CONSTRAINT [uCommerce_PK_OrderNumbers_1] PRIMARY KEY CLUSTERED ([OrderNumberId] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_OrderNumbers]
    ON [dbo].[uCommerce_OrderNumberSerie]([OrderNumberName] ASC);

