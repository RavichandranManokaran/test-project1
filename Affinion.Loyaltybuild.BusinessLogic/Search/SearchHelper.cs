﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SearchHelper.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.BusinessLogic.Search
/// Description:           Search helper
/// </summary>
namespace Affinion.LoyaltyBuild.BusinessLogic.Search
{
    #region Using Directives

    using Affinion.LoyaltyBuild.BusinessLogic.Search.SupplierFilters;
    using Affinion.LoyaltyBuild.Common;
    using Affinion.LoyaltyBuild.Common.Exceptions;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Search;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Sitecore.ContentSearch;
    using Sitecore.ContentSearch.Linq.Utilities;
    using Sitecore.ContentSearch.SearchTypes;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Web.UI.WebControls;

    #endregion

    /// <summary>
    /// Helper class for search related functionalities
    /// </summary>
    public static class SearchHelper
    {
        #region Fields

        private static string clientId;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the page count for the retrieved result set
        /// </summary>
        public static int PageCount { get; set; }

        #endregion

        #region Public Methods

        public static SupplierSearchResults SearchContent(SearchKey searchKeywords)
        {
            try
            {
                if (searchKeywords == null)
                {
                    throw new ArgumentNullException("searchKeywords", "Passed search term is not valid.");
                }

                if (searchKeywords.Client == null)
                {
                    throw new AffinionException("Please assign a valid client from client portal root item.");
                }

                clientId = searchKeywords.Client.ID.ToString();
                var searchIndex = ContentSearchManager.GetIndex("affinion_suppliers_index");
                SupplierSearchResults searchResults = ValidateInput(searchKeywords);

                if (!string.IsNullOrEmpty(searchResults.Error))
                {
                    return searchResults;
                }

                using (var context = searchIndex.CreateSearchContext())
                {
                    searchResults = new SupplierSearchResults();
                    var filterPredicate = PredicateBuilder.True<SupplierSearchResultItem>().And(i => i.TemplateName == "SupplierDetails");

                    // get supplier items from index
                    var results = context.GetQueryable<SupplierSearchResultItem>().Where(filterPredicate);
                    var filteredResults = FilterResults(results, searchKeywords);
                    results = null;

                    searchResults = FillResults(filteredResults, searchKeywords);
                    searchKeywords = TagSearchResult(searchKeywords, searchResults);

                    // sort and tag results set
                    searchResults.AllResults = SortResults(searchResults.AllResults, searchKeywords).ToArray();
                }

                return searchResults;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, null);
                throw;
            }
        }

        /// <summary>
        /// Do the pagination related functionalities for the sublayout.
        /// </summary>
        /// <param name="totalItems"></param>
        public static PagedDataSource GetPagedDataSource(SupplierSearchResultItem[] list, int pageNumber, int itemsPerPage)
        {
            if (list == null || pageNumber == 0 || itemsPerPage == 0)
            {
                return null;
            }

            PagedDataSource pagedDataSource = new PagedDataSource();
            PageCount = (int)Math.Ceiling(list.Length / (double)itemsPerPage);

            pagedDataSource.DataSource = list;
            pagedDataSource.AllowPaging = true;
            pagedDataSource.CurrentPageIndex = pageNumber > 0 ? pageNumber - 1 : 0;
            pagedDataSource.PageSize = itemsPerPage;

            return pagedDataSource;
        }

        /// <summary>
        /// Gets the map cordinates of the items in current page
        /// </summary>
        /// <param name="dataSource">The data source of the current page</param>
        /// <param name="searchKeywords">Current search term</param>
        /// <returns>Returns the cordinates of loaded map locations</returns>
        public static MapData GetCordinates(PagedDataSource dataSource, SearchKey searchKeywords)
        {
            MapData mapData = new MapData();

            foreach (var item in dataSource)
            {
                SearchResultItem resultsItem = item as SearchResultItem;
                GeoCordinate cordinate = new GeoCordinate();

                if (resultsItem == null)
                {
                    continue;
                }

                Item location = SitecoreFieldsHelper.GetDropLinkFieldTargetItem(resultsItem.GetItem(), "MapLocation");

                if (location == null)
                {
                    continue;
                }

                string locationData = SitecoreFieldsHelper.GetValue(location, "MapLocation");
                cordinate.Location = SitecoreFieldsHelper.GetValue(location, "Name");
                cordinate.SupplierId = resultsItem.ItemId.ToString();

                if (!string.IsNullOrWhiteSpace(locationData))
                {
                    List<string> latLongList = locationData.Split(',').ToList<string>();
                    cordinate.Latitude = latLongList[0];
                    cordinate.Longitute = latLongList[1];
                }
                else
                {
                    cordinate.Latitude = "0";
                    cordinate.Longitute = "0";
                }

                mapData.Locations.Add(cordinate);
            }

            mapData.ContextItemId = Sitecore.Context.Item.ID.ToString();
            mapData.ResultsType = searchKeywords.SearchResultsMode;

            return mapData;
        }

        /// <summary>
        /// Validate keywords for malicious input and return true if it is safe to continue
        /// </summary>
        /// <param name="keywords">The keywords</param>
        /// <returns>Returns true if the search is safe to continue</returns>
        public static bool ValidateKeywords(string keywords)
        {
            // TODO: implement validation
            return true;
        }

        public static SupplierSearchResults ValidateInput(SearchKey searchTerm)
        {
            SupplierSearchResults resultSet = new SupplierSearchResults();

            if (searchTerm == null)
            {
                return resultSet;
            }

            if (!ValidateKeywords(searchTerm.Destination))
            {
                resultSet.Error = "Invalid value for destination field.";
            }
            else if (!(searchTerm.CheckinDate >= DateTime.Today && searchTerm.CheckoutDate >= searchTerm.CheckinDate))
            {
                resultSet.Error = "Please add a valid check-in date and check-out date.";
            }

            if (!string.IsNullOrEmpty(resultSet.Error))
            {
                resultSet.AllResults = new SupplierSearchResultItem[0];
                resultSet.ResultsByLocation = new SupplierSearchResultItem[0];
                resultSet.ResultsBySupplierName = new SupplierSearchResultItem[0];
            }

            return resultSet;
        }

        /// <summary>
        /// Gets a specific suplier search results item by id
        /// </summary>
        /// <param name="id">Supplier sitecore id</param>
        /// <returns></returns>
        public static SupplierSearchResultItem GetSupplier(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return null;
            }

            var searchIndex = ContentSearchManager.GetIndex("affinion_suppliers_index");
            SupplierSearchResultItem searchResult = null;

            using (var context = searchIndex.CreateSearchContext())
            {
                // get supplier items from index
                var results = context.GetQueryable<SupplierSearchResultItem>().Where(i => i.TemplateName == "SupplierDetails").ToList();
                var suplliers = results.Where(i => i.ItemId.ToString() == id);
                if (suplliers.Any())
                {
                    searchResult = suplliers.First();
                }
            }

            return searchResult;
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Gets the price from index field for comparison
        /// </summary>
        /// <param name="price">Price field value from supplier index</param>
        /// <returns>Returns the decimal value</returns>
        private static decimal GetPrice(string price, decimal ignoreValue)
        {
            try
            {
                if (string.IsNullOrEmpty(price))
                {
                    return ignoreValue;
                }

                string clientPrice = price.Split(',').Where(s => s.Contains(clientId)).First().ToString().Replace(clientId + "=", "");
                decimal value = decimal.Parse(clientPrice);

                if (value == 0)
                {
                    return ignoreValue;
                }

                return value;
            }
            catch (NullReferenceException ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.BusinessLogic, ex, null);
                // no price information found for the given client
                return ignoreValue;
            }
        }

        /// <summary>
        /// Sort search results
        /// </summary>
        /// <param name="results">Current results set</param>
        /// <param name="searchKeywords">Current search term</param>
        /// <returns>Returns the sorted results set</returns>
        private static IEnumerable<SupplierSearchResultItem> SortResults(IEnumerable<SupplierSearchResultItem> results, SearchKey searchKeywords)
        {
            if (searchKeywords.SortMode == SortMode.SupplierName && searchKeywords.SortValue == "0")
            {
                results = results.OrderBy(i => i.SupplierName).ToList();
            }
            else if (searchKeywords.SortMode == SortMode.SupplierName && searchKeywords.SortValue == "1")
            {
                results = results.OrderByDescending(i => i.SupplierName).ToList();
            }
            else if (searchKeywords.SortMode == SortMode.Price && searchKeywords.SortValue == "0")
            {
                results = results.OrderBy(i => GetPrice(i.LowestPrice, decimal.MaxValue)).ToList();
            }
            else if (searchKeywords.SortMode == SortMode.Price && searchKeywords.SortValue == "1")
            {
                results = results.OrderByDescending(i => GetPrice(i.LowestPrice, decimal.MinValue)).ToList();
            }
            else if (LocationSearchSort(ref results, searchKeywords))
            {
                return results;
            }
            else if (HotelSearchSort(ref results, searchKeywords))
            {
                return results;
            }

            /// Offline Portal Sortings
            else if (searchKeywords.SortMode == SortMode.Stars && searchKeywords.SortValue == "0")
            {
                results = results.OrderBy(i => i.StarRanking).ToList();
            }
            else if (searchKeywords.SortMode == SortMode.Stars && searchKeywords.SortValue == "1")
            {
                results = results.OrderByDescending(i => i.StarRanking).ToList();
            }
            else if (searchKeywords.SortMode == SortMode.Recommend && searchKeywords.SortValue == "0")
            {
                results = results.OrderBy(i => i.LocalRanking).ToList();
            }
            else if (searchKeywords.SortMode == SortMode.DistanceFromCityCentre && searchKeywords.SortValue == "0")
            {
                // TODO: Implement Sort By DistanceFromCityCentre
            }

            return results;
        }

        private static bool LocationSearchSort(ref IEnumerable<SupplierSearchResultItem> results, SearchKey searchKeywords)
        {
            ///When searched for a location or mixed and select sort by ranking in ascending, sort by ranking and then by supplier name
            if ((searchKeywords.Mode == PageMode.Locations || searchKeywords.Mode == PageMode.LocationFinder)
                && searchKeywords.SortMode == SortMode.Rate && searchKeywords.SortValue == "0")
            {
                results = results.OrderBy(i =>
                {
                    if (string.IsNullOrWhiteSpace(i.LocalRanking))
                    {
                        return string.Empty;
                    }

                    return i.LocalRanking;

                }).ThenBy(i => i.SupplierName).ToList();

                return true;
            }
            ///When searched for a location or mixed without selecting sort order, sort by ranking in descending and then by supplier name in ascending
            ///Or When searched for a location or mixed and select sort by ranking in descending, sort by ranking in descending and then by supplier name in ascending
            else if ((searchKeywords.Mode == PageMode.Locations || searchKeywords.Mode == PageMode.LocationFinder)
                && (searchKeywords.SortMode == SortMode.None || (searchKeywords.SortMode == SortMode.Rate && searchKeywords.SortValue == "1")))
            {
                results = results.OrderByDescending(i =>
                {
                    if (string.IsNullOrWhiteSpace(i.LocalRanking))
                    {
                        return string.Empty;
                    }

                    return i.LocalRanking;

                }).ThenBy(i => i.SupplierName).ToList();

                return true;
            }

            return false;
        }

        private static bool HotelSearchSort(ref IEnumerable<SupplierSearchResultItem> results, SearchKey searchKeywords)
        {
            ///When searched for a hotel without selecting sort order, sort by ranking in descending and then by price in ascending   
            ///Or When searched for a hotel and select sort by ranking in descending, sort by ranking in descending and then by price in ascending
            //if (searchKeywords.SortMode == SortMode.None && searchKeywords.SearchResultsMode == SearchResultsMode.SupplierName
            //   || (searchKeywords.SortMode == SortMode.Rate && searchKeywords.SortValue == "1" && searchKeywords.SearchResultsMode == SearchResultsMode.Location))
            if (searchKeywords.Mode == PageMode.Hotels && (searchKeywords.SortMode == SortMode.None
               || (searchKeywords.SortMode == SortMode.Rate && searchKeywords.SortValue == "1")))
            {
                results = results.OrderByDescending(i =>
                {
                    if (string.IsNullOrWhiteSpace(i.LocalRanking))
                    {
                        return string.Empty;
                    }

                    return i.LocalRanking;

                }).ThenBy(i =>
                {
                    decimal price = GetPrice(i.LowestPrice, decimal.MaxValue);
                    ///return maximum value if the price is 0 so sorting would happen correctly
                    return price;

                }).ThenByDescending(i =>
                {
                    return i.StarRanking;

                }).ThenBy(i => i.SupplierName).ToList();

                return true;
            }
            ///When searched for a hotel and select sort by ranking in descending, sort by ranking in descending and then by price in ascending
            else if (searchKeywords.Mode == PageMode.Hotels && searchKeywords.SortMode == SortMode.Rate && searchKeywords.SortValue == "0")
            {
                results = results.OrderBy(i =>
                {
                    if (string.IsNullOrWhiteSpace(i.LocalRanking))
                    {
                        return string.Empty;
                    }

                    return i.LocalRanking;

                }).ThenBy(i =>
                {
                    decimal price = GetPrice(i.LowestPrice, decimal.MaxValue);
                    ///return maximum value if the price is 0 so sorting would happen correctly
                    return price;

                }).ThenBy(i =>
                {
                    return i.StarRanking;
                }).ThenBy(i => i.SupplierName).ToList();

                return true;
            }

            return false;

        }

        /// <summary>
        /// Format search results and fill the return object
        /// </summary>
        /// <param name="list">The list of supplier search results</param>
        /// <param name="searchKeywords">The search keywords object</param>
        /// <returns>Returns the search results</returns>
        private static SupplierSearchResults FillResults(IEnumerable<SupplierSearchResultItem> list, SearchKey searchKeywords)
        {
            SupplierSearchResults searchResults = new SupplierSearchResults();

            if (!string.IsNullOrEmpty(searchKeywords.Destination))
            {
                IEnumerable<SupplierSearchResultItem> resultsBySupplierName = list.Where(i => MatchText(i.SupplierName, searchKeywords.Destination));
                IEnumerable<SupplierSearchResultItem> resultsByDestination = SearchResultsByLocation(searchKeywords.Destination, list);
                searchResults.ResultsBySupplierName = resultsBySupplierName.ToArray();
                searchResults.ResultsByLocation = resultsByDestination.ToArray();

                List<SupplierSearchResultItem> allResults = new List<SupplierSearchResultItem>(resultsBySupplierName.Union(resultsByDestination));
                searchResults.AllResults = allResults.ToArray();
            }
            else
            {
                if (!string.IsNullOrEmpty(searchKeywords.Location))
                {
                    IEnumerable<SupplierSearchResultItem> resultsByLocation = SearchResultsByLocation(searchKeywords.Location, list);
                    searchResults.AllResults = searchResults.ResultsByLocation = resultsByLocation.ToArray();
                }
                else
                {
                    searchResults.AllResults = list.ToArray();
                    searchResults.ResultsByLocation = new SupplierSearchResultItem[0];
                }

                searchResults.ResultsBySupplierName = new SupplierSearchResultItem[0];
            }

            return searchResults;
        }

        /// <summary>
        /// Filter results by location
        /// </summary>
        /// <param name="location">keyword containing the location</param>
        /// <param name="list">results list</param>
        /// <returns>Returns the filterd list</returns>
        private static IEnumerable<SupplierSearchResultItem> SearchResultsByLocation(string location, IEnumerable<SupplierSearchResultItem> list)
        {
            if (location == "all")
            {
                return list;
            }

            List<SupplierSearchResultItem> resultsByLocation = list.Where(i =>
                    MatchText(i.AddressLine1, location) ||
                    MatchText(i.AddressLine2, location) ||
                    MatchText(i.AddressLine3, location) ||
                    MatchText(i.AddressLine4, location) ||
                    MatchText(i.Location, location) ||
                    MatchText(i.Country, location) ||
                    MatchText(i.Town, location)).ToList();

            return resultsByLocation;
        }

        /// <summary>
        /// Tag results set depending on the type of search it performed
        /// </summary>
        /// <param name="searchKeywords">The search terms object</param>
        /// <param name="searchResults">The current results set</param>
        /// <returns>Returns the updated search term object with search type tag</returns>
        private static SearchKey TagSearchResult(SearchKey searchKeywords, SupplierSearchResults searchResults)
        {
            if (searchResults.AllResults.Length > 0 && searchResults.ResultsByLocation.Length > 0 && searchResults.ResultsBySupplierName.Length <= 0)
            {
                searchKeywords.SearchResultsMode = SearchResultsMode.Location;
            }
            else if (searchResults.AllResults.Length > 0 && searchResults.ResultsByLocation.Length <= 0 && searchResults.ResultsBySupplierName.Length > 0)
            {
                searchKeywords.SearchResultsMode = SearchResultsMode.SupplierName;
            }
            else if (searchResults.AllResults.Length > 0)
            {
                searchKeywords.SearchResultsMode = SearchResultsMode.Mixed;
            }
            else
            {
                searchKeywords.SearchResultsMode = SearchResultsMode.None;
            }

            return searchKeywords;
        }

        /// <summary>
        /// Performs a case in-sensitive match on two strings
        /// </summary>
        /// <param name="value1">The first string value</param>
        /// <param name="value2">The second string value</param>
        /// <returns>Returns true if match found</returns>
        private static bool MatchText(string value1, string value2)
        {
            if (string.IsNullOrEmpty(value1) || string.IsNullOrEmpty(value2))
            {
                return false;
            }

            return value1.ToLowerInvariant().IndexOf(value2.ToLowerInvariant()) >= 0;
        }


        /// <summary>
        /// Filter search results
        /// </summary>
        /// <param name="results">Full results set returned from index</param>
        /// <param name="searchTerm">The search term</param>
        /// <returns>Returns a filtered list</returns>
        private static IEnumerable<SupplierSearchResultItem> FilterResults(IEnumerable<SupplierSearchResultItem> results, SearchKey searchTerm)
        {
            List<SupplierSearchResultItem> filteredResults = new List<SupplierSearchResultItem>();
            Type[] filters = GetSupplierFilters();

            //Item clientItem = SitecoreFieldsHelper.GetLookupFieldTargetItem(ItemHelper.RootItem, "ClientDetails");
            Item clientItem = searchTerm.Client;

            if (clientItem == null)
            {
                throw new AffinionException("Supplier portal root item is not created with the correct template. It should contain a field 'ClientDetails' pointing to a valid client details item.");
            }


            foreach (SupplierSearchResultItem resultItem in results)
            {
                SupplierFilterData filterData = new SupplierFilterData();
                filterData.Client = clientItem;
                filterData.SearchTerm = searchTerm;
                filterData.ResultsItem = resultItem;

                if (SkipSupplier(filters, filterData))
                {
                    continue;
                }

                filteredResults.Add(resultItem);
            }

            return filteredResults;
        }

        /// <summary>
        /// Returns true to skip the offer
        /// </summary>
        /// <param name="offer">Current offer</param>
        /// <param name="searchKey">Search term</param>
        /// <param name="Supplier">Current supplier</param>
        /// <param name="Client">Current Client</param>
        /// <returns>Returns true to include the offer</returns>
        private static bool SkipSupplier(Type[] types, SupplierFilterData filterData)
        {
            bool skip = false;
            foreach (Type type in types)
            {
                ISupplierFilter instance = (ISupplierFilter)Activator.CreateInstance(type);
                skip = !instance.IncludeSupplier(filterData);

                if (skip)
                {
                    break;
                }
            }

            return skip;
        }

        /// <summary>
        /// Gets Immplementations of IOfferFilter in the "Affinion.LoyaltyBuild.BusinessLogic.Search.OfferFilters" namespace
        /// </summary>
        /// <returns>Returns an array of types</returns>
        private static Type[] GetSupplierFilters()
        {
            return Assembly.GetExecutingAssembly().GetTypes().
                Where(t =>
                    String.Equals(t.Namespace, "Affinion.LoyaltyBuild.BusinessLogic.Search.SupplierFilters", StringComparison.Ordinal) &&
                    Type.Equals(t.GetInterface("ISupplierFilter"), typeof(ISupplierFilter))
                ).ToArray();
        }

        #endregion

        #region Offlile Portal

        /// <summary>
        /// Query to get Filtered call centre user
        /// </summary>
        private const string FilteredCallCentreUserItem = @"fast://sitecore/content/#admin-portal#/#client-setup#/global/#_supporting-content#/callcentreuser/*[@@templatename='CallCentreUser' and @User ='{0}']";

        /// <summary>
        /// Query to get Filtered Client items
        /// </summary>        
        private const string FilteredClientItems = @"fast:/sitecore/content/#admin-portal#/#client-setup#/*[@@templatename='ClientDetails' and @Active='1' and @CallCentre='{0}']";

        /// <summary>
        /// Retrive user specific client items for offline portal
        /// </summary>
        /// <returns></returns>
        public static List<Item> GetUserSpecificClientItems()
        {
            try
            {
                /// Get Sitecore Master DB
                Database masterDb = Sitecore.Configuration.Factory.GetDatabase(Constants.MasterDatabase);

                Item callCentreUserItem = masterDb.SelectItems(string.Format(FilteredCallCentreUserItem, Sitecore.Context.User.Name)).SingleOrDefault();

                string callCentre = SitecoreFieldsHelper.GetDropLinkFieldValue(callCentreUserItem, "CallCentre", "CallCentre");

                if (callCentre.Length != 0)
                {
                    /// Get Client items
                    var clientItems = masterDb.SelectItems(string.Format(FilteredClientItems, callCentre));

                    return clientItems.ToList();
                }
                return null;
            }
            catch (InvalidOperationException invalidEx)
            {
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, invalidEx, null);
                throw new InvalidOperationException("Multiple call centres can not be assigned for single user");
            }            
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, null);
                throw;
            }
        }

        /// <summary>
        /// We will remove this from next release, Only temporary purpose [Please don't use this]
        /// </summary>
        /// <param name="searchKeywords"></param>
        /// <returns></returns>
        public static SupplierSearchResults TemporarySearchContent(SearchKey searchKeywords)
        {
            try
            {
                if (searchKeywords == null)
                {
                    throw new ArgumentNullException("searchKeywords", "Passed search term is not valid.");
                }

                if (searchKeywords.Client == null)
                {
                    throw new AffinionException("Please assign a valid client from client portal root item.");
                }

                clientId = searchKeywords.Client.ID.ToString();
                var searchIndex = ContentSearchManager.GetIndex("affinion_suppliers_index");

                //SupplierSearchResults searchResults = ValidateInput(searchKeywords);
                SupplierSearchResults searchResults = new SupplierSearchResults();

                if (!string.IsNullOrEmpty(searchResults.Error))
                {
                    return searchResults;
                }

                using (var context = searchIndex.CreateSearchContext())
                {
                    searchResults = new SupplierSearchResults();
                    var filterPredicate = PredicateBuilder.True<SupplierSearchResultItem>().And(i => i.TemplateName == "SupplierDetails");

                    // get supplier items from index
                    var results = context.GetQueryable<SupplierSearchResultItem>().Where(filterPredicate);
                    var filteredResults = FilterResults(results, searchKeywords);
                    results = null;

                    searchResults = FillResults(filteredResults, searchKeywords);
                    searchKeywords = TagSearchResult(searchKeywords, searchResults);

                    // sort and tag results set
                    searchResults.AllResults = SortResults(searchResults.AllResults, searchKeywords).ToArray();
                }

                return searchResults;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, null);
                throw;
            }
        }

        #endregion
    }
}
