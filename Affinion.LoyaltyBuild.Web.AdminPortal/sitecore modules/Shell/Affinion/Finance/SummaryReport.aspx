﻿<%@ Page Title="" Language="C#" MasterPageFile="~/sitecore modules/Shell/Affinion/Finance/MainLayout.Master" AutoEventWireup="true" CodeBehind="SummaryReport.aspx.cs" Inherits="Affinion.LoyaltyBuild.AdminPortal.sitecore_modules.Shell.Affinion.Finance.SummaryReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {

            $('input[id="DateRange"]').daterangepicker();

            $('#DateRange').daterangepicker({
                locale: { cancelLabel: 'Clear' }
            });

            function cb(start, end) {
                $('#DateRange span').html(start + ' - ' + end);
            }
            cb(moment().subtract(29, 'days'), moment());

            $('#DateRange').daterangepicker({
                ranges: {
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    'Last Two Month': [moment().subtract(2, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    'Last Four Month': [moment().subtract(4, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    'Last Six Month': [moment().subtract(6, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            $('#DateRange').on('cancel.daterangepicker', function (ev, picker) {
                //do something, like clearing an input
                $('#DateRange').val('');
                Validation();
            });

            $('#DateRange').on('apply.daterangepicker', function (ev, picker) {
                Validation();
            });

             $("#ddlOracleFile").change(function () {
                Validation();
            });
            $("#Provider").change(function () {
                Validation();
            });

            $("#Partner").change(function () {
                Validation();
            });

            if ($("#hdnDateRange").val() == "false")
            {
                $('#DateRange').val('');
            }

            Validation();

        });

        function Validation() {
            var OracleFileName = $("#ddlOracleFile").val();
            var DateRange = $("#DateRange").val();
            var Supplier = $("#Provider").val();
            var Client = $("#Partner").val();
            if (DateRange!="")
                $("#hdnDateRange").val("true");
            else
                $("#hdnDateRange").val("false");

            if (OracleFileName != "" || (DateRange != "" && Supplier != "") || (DateRange != "" && Client != "")) {
                $("#btnSummaryReport").removeAttr('disabled');
            }

            else {
                $("#btnSummaryReport").attr('disabled', 'disabled');
            }

        }


 </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuItems" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Title" runat="server">
    AR - Summary Report
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Body" runat="server">
   <div class="row">
			<div class="col-md-12">
                 <div>
                    <h4 class="title-text font-weight-bold"><asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label></h4>
                </div>  
			</div>
		</div>
     <div class="panel-heading bg-primary">
                                    <h4 class="panel-title">
                                        <span>Summary Report</span>
                                        <a data-toggle="collapse" data-parent="#accordion1" href="#collapse0" class="show-hide-list">
                                            <span class="glyphicon glyphicon-triangle-top pull-right"></span>
                                        </a>
                                    </h4>
                                </div>
     <div id="collapse0" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="col-xs-6 no-padding">
                                                     <div class="col-xs-4">
                                                        <div><b>Oracle File Name</b></div>
                                                        <div>
                                                            <asp:DropDownList ID="ddlOracleFile" ClientIDMode="Static" runat="server" CssClass="form-control font-size-12 dropdown-field" TabIndex="1">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-4 no-padding-left">
                                                        <div><b>Date Range:</b></span></div>
                                                        <div class="form-group">
                                                            <div class='input-group date'>
                                                                <input type="text" id="DateRange"  runat="server" clientidmode="Static" class="form-control datepicker width-100 pull-left font-size-12" tabindex="2" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <div><b>Supplier</b></div>
                                                        <div>
                                                            <asp:DropDownList ID="Provider" ClientIDMode="Static" runat="server" CssClass="form-control font-size-12 dropdown-field" TabIndex="3">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 no-padding">
                                                    <div class="col-xs-4">
                                                        <div><b>Client</b></div>
                                                        <div>
                                                            <asp:DropDownList ID="Partner" ClientIDMode="Static" runat="server" CssClass="form-control font-size-12 dropdown-field" TabIndex="4">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-xs-12">
                                                <div class="col-xs-9 no-padding"></div>
                                                 <div class="col-xs-3 no-padding-right">
                                                    <asp:Button type="button" ID="btnSummaryReport" runat="server" ClientIDMode="Static" Text="Get Summary" class="btn btn-primary pull-right fullWidth" TabIndex="5" OnClick="btnSummaryReport_Click" />
                                                     <asp:HiddenField ID="hdnDateRange" ClientIDMode="Static" Value="true"  runat="server" />
                                                </div> 
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 marT15">
                                                <div class="row" id="htmlReportGrid" runat="server">
                                                    <div class="col-sm-12">
                                                        <div>
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading claim_titl">
                                                                    <h4 class="panel-title">Report by provider
                                                                    </h4>
                                                                </div>
                                                                <div id="totalCount" runat="server" class="totalCountpad">Provider reports Total of <span><%= ProviderTotalOfRecords %> </span>record(s) found. </div>
                                                                <div class="panel-body alert-info">
                                                                    <%-- Result Grid--%>
                                                                    <div runat="server" id="ResultDiv" clientidmode="Static">
                                                                        <div id="data-grid" class="table-responsive">
                                                                            <asp:GridView ID="ProviderResultGridView" runat="server" ClientIDMode="Static" Width="100%" SelectedRowStyle-Font-Bold="true" class="table table-striped table-bordered" AutoGenerateColumns="False" AllowPaging="true" PageSize="10"
                                                                                PagerSettings-NextPageText=" Next " PagerSettings-PreviousPageText=" Previous " PagerSettings-Mode="NumericFirstLast" PagerSettings-FirstPageText=" First " PagerSettings-LastPageText=" Last " PagerSettings-Position="Bottom" PagerStyle-Font-Bold="true" PagerStyle-CssClass="pagination-grid" OnPageIndexChanging="ProviderResultGridView_PageIndexChanging">
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="Supplier">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblSupplier" runat="server" Text='<%# Bind("HotelName") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                     <asp:TemplateField HeaderText="Number Of Sales">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblNumberOfSales" runat="server" Text='<%# Bind("NumberOfSales") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
																					<asp:TemplateField HeaderText="Total">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblTotalByProvider" runat="server" Text='<%# Bind("Amount") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Currency">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblCurrency" runat="server" Text='<%# Bind("CurrencyCode") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                                <EmptyDataTemplate>
                                                                                    ---No Records---
                                                                                </EmptyDataTemplate>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="row">
                                            <div class="col-xs-12 marT15">
                                                <div class="row" id="Div1" runat="server">
                                                    <div class="col-sm-12">
                                                        <div>
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading claim_titl">
                                                                    <h4 class="panel-title">Report by currency
                                                                    </h4>
                                                                </div>
                                                                <div id="Div2" runat="server" class="totalCountpad">Currency reports Total of <span><%= CurrencyTotalOfRecords %> </span>record(s) found. </div>
                                                                <div class="panel-body alert-info">
                                                                    <%-- Result Grid--%>
                                                                    <div runat="server" id="Div3" clientidmode="Static">
                                                                        <div id="data-grid" class="table-responsive">
                                                                            <asp:GridView ID="CurrencyResultGridView" runat="server" ClientIDMode="Static" Width="100%" SelectedRowStyle-Font-Bold="true" class="table table-striped table-bordered" AutoGenerateColumns="False" AllowPaging="true" PageSize="10"
                                                                                PagerSettings-NextPageText=" Next " PagerSettings-PreviousPageText=" Previous " PagerSettings-Mode="NumericFirstLast" PagerSettings-FirstPageText=" First " PagerSettings-LastPageText=" Last " PagerSettings-Position="Bottom" PagerStyle-Font-Bold="true" PagerStyle-CssClass="pagination-grid" OnPageIndexChanging="CurrencyResultGridView_PageIndexChanging" >
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="Currency">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblCurrencyCode" runat="server" Text='<%# Bind("CurrencyCode") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                     <asp:TemplateField HeaderText="Total">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblTotalByCurrency" runat="server" Text='<%# Bind("Amount") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                                <EmptyDataTemplate>
                                                                                    ---No Records---
                                                                                </EmptyDataTemplate>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
</asp:Content>
