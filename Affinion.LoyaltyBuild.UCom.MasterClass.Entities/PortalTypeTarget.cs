﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;
using UCommerce.Marketing.Targets;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class PortalTypeTarget : Target, IOrderLineTarget, IEntity, ITarget
    {
        private int? _hashCode;

        public virtual bool IsSatisfiedBy(OrderLine orderLine)
        {
            return CheckPortalIsValidOrNot();
        }

        private bool CheckPortalIsValidOrNot()
        {
            try
            {
                if (!string.IsNullOrEmpty(this.PortalType))
                {
                    string offlinePortal = Sitecore.Configuration.Settings.GetSetting("OfflinePortalSiteName");
                    string onlinePortal = Sitecore.Configuration.Settings.GetSetting("OnlinePortalSiteName");
                    List<string> lstofflinePortal = !string.IsNullOrEmpty(offlinePortal) ? offlinePortal.Split('|').ToList() : new List<string>();
                    List<string> lstonlinePortal = !string.IsNullOrEmpty(onlinePortal) ? onlinePortal.Split('|').ToList() : new List<string>();
                    if (this.PortalType.ToLower() == "online")
                    {
                        return lstonlinePortal.Any(x => x.Equals(Sitecore.Context.Site.Name, StringComparison.InvariantCultureIgnoreCase));
                    }
                    else if (this.PortalType.ToLower() == "offline")
                    {
                        return lstofflinePortal.Any(x => x.Equals(Sitecore.Context.Site.Name, StringComparison.InvariantCultureIgnoreCase));
                    }
                }
            }
            catch (Exception er)
            {
                return false;
            }
            return true;
        }


        // Methods
        public new static IQueryable<PortalTypeTarget> All()
        {
            return GetRepo().Select();
        }

        public override void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<PortalTypeTarget, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            PortalTypeTarget objA = obj as PortalTypeTarget;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<PortalTypeTarget, bool>> expression)
        {
            return All().Any<PortalTypeTarget>(expression);
        }

        public static IList<PortalTypeTarget> Find(Expression<Func<PortalTypeTarget, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<PortalTypeTarget>();
        }

        public static PortalTypeTarget FirstOrDefault(Expression<Func<PortalTypeTarget, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<PortalTypeTarget>();
        }

        public new static PortalTypeTarget Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<PortalTypeTarget> GetRepo()
        {
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<PortalTypeTarget>>();

        }


        public override void Save()
        {
            GetRepo().Save(this);
        }

        public static PortalTypeTarget SingleOrDefault(Expression<Func<PortalTypeTarget, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties
        public override int Id
        {
            get
            {
                return this.PortalTypeTargetId;
            }
        }

        public virtual int PortalTypeTargetId { get; protected set; }

        public virtual string PortalType { get; set; }






    }
}
