﻿using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal
{
    public partial class ClientPortalMyFavorite : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BindList();
        }

        protected void rptList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            string name = e.Item.DataItem as string;
            Label ListName = e.Item.FindControl("lblList") as Label;
            CheckBox chkList = e.Item.FindControl("chkList") as CheckBox;
            ListName.Text = name;
}

        private void BindList()
        {
            List<string> ListName=new List<string>();
            DataSet ds = uCommerceConnectionFactory.GetFavoriteList(68);
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                ListName.Add(ds.Tables[0].Rows[i]["ListName"].ToString());
            }
            rptList.DataSource = ListName;
            rptList.DataBind();
        }
    }
}