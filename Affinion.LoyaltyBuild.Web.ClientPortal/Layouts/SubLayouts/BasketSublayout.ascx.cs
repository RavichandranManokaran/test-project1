﻿//<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           BasketSublayout.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal
/// Description:           Used to Load Hotel related details to Basket Page
/// </summary>

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts
{
    #region Using Directives
    using Affinion.LoyaltyBuild.Common;
    using Affinion.LoyaltyBuild.Common.Exceptions;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.SessionHelper;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Search;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.WebControls;
    using UCommerce.EntitiesV2;
    using Affinion.Loyaltybuild.BusinessLogic.Helper;
    using System.Data;
    using System.Web.UI.HtmlControls;
    using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
    using Affinion.LoyaltyBuild.Search.Helpers;
    using Affinion.LoyaltyBuild.Presentation;
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Rules;
    using Affinion.LoyaltyBuild.UCom.Common.Constants;
    using UCommerce.Api;
    using Affinion.LoyaltyBuild.Model.Product;
    using Affinion.LoyaltyBuild.Model.Provider;
    using Affinion.LoyaltyBuild.Search.Data;
    using bookinghelper = Affinion.LoyaltyBuild.Api.Booking.Helper;

    #endregion

    public partial class BasketSublayout : BaseSublayout
    {
        private Item clientItem = SitecoreFieldsHelper.GetLookupFieldTargetItem(ItemHelper.RootItem, "ClientDetails");
        // public string voucherRuleId = "{E9FDD77D-55DF-4E6A-82AE-E2EA1EDE04CB}";
        Literal CheckInLiteral = null;
        Literal CheckOutLiteral = null;
        Literal RoomsLiteral = null;
        Literal PeopleLiteral = null;
        Literal PriceLiteral = null;
        

        

        List<Item> hotels = null;
        public string qty = string.Empty;
        public string orderLineId = string.Empty;
        public string CurrencySymbol { get; set; }
        public string CurrencyCode { get; set; }
        public string FormattedCurrency { get; set; }
        public string BillingCurrency { get; set; }
        public string LabelAccomodationValidation { get; set; }
        ISitecorePaymentService sitecorePaymentService { get; set; }
        string PaymentPortion = string.Empty;


        public string ConfirmationMessage
        {
            get
            {
                return SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "ConfirmationMessage");
            }
        }

        // Guest Availability popup page dropdown loading range
        public string GuestRange
        {
            get
            {
                return SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "GuestCount");
            }
        }

        /// <summary>
        /// Page load related events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            LabelAccomodationValidation = string.Empty;
            //ButtonProceedToCheckOut.Enabled = true;
            // Put user code to initialize the page here
            hotels = Sitecore.Context.Database.SelectItems(Constants.BasketPageQueryPath).ToList();


            Item supplierOfferItems = Sitecore.Context.Database.GetItem(Constants.SupplierOfferPath);

            //Show Hide PayByVoucherSection
            SetPayByVoucherSection();

            SetMessage();
            ///Set currency labels (symbol or sign) in ascx.
            BindSitecoreTexts();

            if (Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.BasketCount > 0)
            {
                if (UCommerce.Api.TransactionLibrary.GetBasket(false).PurchaseOrder != null)
                {

                    int orderId = BasketHelper.GetBasketOrderId();
                    SetCurrency(orderId);


                    if (!IsPostBack)
                    {
                        //reset order payment method on load
                        var order = TransactionLibrary.GetBasket(false).PurchaseOrder;
                        order[OrderPropertyConstants.PaymentMethod] = string.Empty;
                        order.Save();

                      //  if (!PopulateBasketPaymentDetails())
                        //{
                        //    throw new AffinionException(string.Format("Subrates are not configured for selected hotels. OrderID: {0}", orderId));
                        //}
                        var basket = bookinghelper.BasketHelper.GetBasket();
                        ProcessingFee.Text = CurrencyHelper.FormatCurrency(basket.ProcessingFee, basket.CurrencyId);
                        TotalpayableAtAccommodation.Text = CurrencyHelper.FormatCurrency(basket.TotalPayableAccomodation, basket.CurrencyId);
                        BookingDeposit.Text = CurrencyHelper.FormatCurrency(basket.TotalPayableToday, basket.CurrencyId);
                        TotalPrice.Text = CurrencyHelper.FormatCurrency(basket.TotalAmount, basket.CurrencyId);

                        //sitecorePaymentService = new SitecorePaymentService();
                        //var paymentMethod = sitecorePaymentService.GetPaymentMethod(order.OrderId);

                        //show partial payment option when all items in basket have partial payment applicable
                        //ShowPartial(paymentMethod);
                        
                    }
                    if (Session["listBasketInfo"] != null)
                    {
                        List<BasketInfo> basketItems = PopulateBasketHotelInfo();

                        //logic to get distinct the basket values
                        var items = basketItems.GroupBy(i => i.Sku).Select(i => i.First()).GroupBy(y => y.SupplierId).Select(z => z.FirstOrDefault());
                        // var items = basket.Bookings.GroupBy(i => i.ProviderId);
                        PanelRepeater.DataSource = items;
                        PanelRepeater.DataBind();
                        //PopulateAccommodationData();
                    }
                    else
                    {
                        DisplayEmptyMessage();
                    }
                }
            }
            else
            {
                DisplayEmptyMessage();
            }

            this.ShowHideControls();
        }

        /// <summary>
        /// ShowPartial
        /// </summary>
        /// <param name="paymentMethod"></param>
        private void ShowPartial(List<UCom.MasterClass.BusinessLogic.Rules.Model.OrderLinePaymentmethod> paymentMethod)
        {
            Item currentItem = Sitecore.Context.Item;
            if (paymentMethod != null && paymentMethod.Count(i => i.Paymentmethod == Constants.PaymentMethod.PartialPayment) == paymentMethod.Count)
            {
                if (rbtFull.Checked)
                {
                    CalcPayment(rbtFull);
                }
                OrderLine orderLine = UCommerce.Api.TransactionLibrary.GetBasket(false).PurchaseOrder.OrderLines.FirstOrDefault();
                if (orderLine != null)
                {
                    string daysBefore = orderLine[OrderPropertyConstants.DaysBefore];
                    string percent = orderLine[OrderPropertyConstants.PercentageOfAmount];
                    showPaymentMethod.Visible = true;
                    showPaymentMethod.Style.Remove("display");
                    string pricePayablePriorText = (currentItem != null && currentItem.Fields["TextPricePayablePrior"] != null) ? currentItem.Fields["TextPricePayablePrior"].Value : string.Empty;
                   // PayableAtAccommodationText.Text = string.Format(pricePayablePriorText, daysBefore);
                    string instText = (currentItem != null && currentItem.Fields["TextInstruction"] != null) ? currentItem.Fields["TextInstruction"].Value : string.Empty;
                    LitInst.Text = string.Format(instText, percent, daysBefore);
                }
            }
            //else
              //  PayableAtAccommodationText.Text = (currentItem != null && currentItem.Fields["TextPayableatAccmmodation"] != null) ? currentItem.Fields["TextPayableatAccmmodation"].Value : string.Empty; ;
        }

        /// <summary>
        /// Show hide controls depending on the client configurations
        /// </summary>
        private void ShowHideControls()
        {
            this.ProcessingFeeRow.Visible = ItemHelper.GetBooleanClientRule("Is the processing fee displayed for this client");
        }

        /// <summary>
        /// Set Message
        /// </summary>
        private void SetMessage()
        {
            Item conditionalMessageItem = GetConditionalMessageItem();

            if (conditionalMessageItem != null && conditionalMessageItem.Fields["Accommodation Details Required Message"] != null)
                LabelAccomodationValidation = conditionalMessageItem.Fields["Accommodation Details Required Message"].Value;           
        }

        /// <summary>
        /// Get conditional message item
        /// </summary>
        private Item GetConditionalMessageItem()
        {
            Item conditionalMessageItem = this.GetItemFromRenderingParameterPath("Conditional Messages Link");

            if (conditionalMessageItem != null)
                return conditionalMessageItem;
            else
                return null;
        }
        /// <summary>
        /// Populates Hotel info for a basket items
        /// </summary>
        /// <returns>Basket Info items list</returns>
        private List<BasketInfo> PopulateBasketHotelInfo()
        {
            List<BasketInfo> basketItemList = NewPopulatedDataSourceForBasket();
            List<Item> hotelList = new List<Item>();
            
            foreach (var item in basketItemList)
            {
                if (item.SitecoreItem != null)
                {
                    hotelList.Add(item.SitecoreItem);
                }
                else
                    return basketItemList;
            }
            List<BasketInfo> basketItems = new List<BasketInfo>();
            foreach (var item in basketItemList)
            {
                if (hotelList.Contains(item.SitecoreItem))
                {
                    basketItems.Add(item);
                    hotelList.Remove(item.SitecoreItem);
                }
            }
            return basketItems;
        }

        /// <summary>
        /// Displays a Message when Basket is Empty 
        /// </summary>
        private void DisplayEmptyMessage()
        {
            DivEmptyBasket.Visible = true;
            EmptyBasket.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "EmptyBasketMessage");
            showPaymentDetails.Visible = false;
            ButtonAddAnotherBreak.Visible = false;
        }

        /// <summary>
        /// New Populated DataSource For Basket
        /// </summary>
        /// <returns></returns>
        private List<Affinion.LoyaltyBuild.Common.SessionHelper.BasketInfo> NewPopulatedDataSourceForBasket()
        {

            var basket = Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.GetBasket();
            List<Affinion.LoyaltyBuild.Common.SessionHelper.BasketInfo> listBasketInfo = new List<Affinion.LoyaltyBuild.Common.SessionHelper.BasketInfo>();
            string noOfAdults = string.Empty, noOfChilderen = string.Empty, noofRooms = string.Empty, roomtype = string.Empty, providerstype = string.Empty;
            string guestName = string.Empty;
            string startDate = null; string endDate = null; string cancelAmount = null;

            foreach (var item in basket.Bookings)
            {
                orderLineId = item.OrderLineId.ToString();
                var providers = item.GetProvider();
                Item hotelItem = null;
                if (providers.ProviderType == Model.Provider.ProviderType.LoyaltyBuild)
                {
                    var hotel = (Affinion.LoyaltyBuild.Model.Provider.Hotel)providers;
                    //hotel.Town
                    hotelItem = hotel.SitecoreItem;
                    providerstype = hotel.ProviderType.ToString();

                }
                else if (providers.ProviderType == Model.Provider.ProviderType.BedBanks)
                {
                    var hotel = (Affinion.LoyaltyBuild.Model.Provider.BedBankHotel)providers;
                    providerstype = hotel.ProviderType.ToString();
                }
                DateTime checkout, checkin;
                checkout = item.CheckoutDate;
                checkin = item.CheckinDate;
                string coDate = String.Format("{0:dd/MM/yyyy}", checkout);
                string ciDate = String.Format("{0:dd/MM/yyyy}", checkin);
                string coTime = String.Format("{0:HH:mm}", checkout);
                string ciTime = String.Format("{0:HH:mm}", checkin);

                //int orderLineId = Convert.ToInt32(item.OrderLineId);
                //string sku = Convert.ToString(item.GetProduct().Sku);
                string sku = item.OrderLine.Sku;
                //string OrderId = Convert.ToString(item.OrderLineId);
                string bbPropertyName = null; int bbStarRanking = 0; string imageUrl = null; string bbroomType = string.Empty; string oldPriceForBB = string.Empty;
                List<BedBankErrata> roomErrata = new List<BedBankErrata>();
                List<BedBankCancelCriteria> roomcancelCriteria = new List<BedBankCancelCriteria>();
                CurrencyInfo ci = new CurrencyInfo();
                if (item.Type == Model.Product.ProductType.Room || item.Type == Model.Product.ProductType.Package)
                {
                    Affinion.LoyaltyBuild.Api.Booking.Data.RoomBooking roomBooking = (Affinion.LoyaltyBuild.Api.Booking.Data.RoomBooking)item;
                    noOfAdults = Convert.ToString(roomBooking.NoOfAdults);
                    noOfChilderen = Convert.ToString(roomBooking.NoOfChildren);
                    noofRooms = Convert.ToString(roomBooking.Quantity);
                    roomtype = roomBooking.Type.ToString();
                    guestName = roomBooking.GuestName;

                }
                else if (item.Type == Model.Product.ProductType.BedBanks)
                {
                    Affinion.LoyaltyBuild.Api.Booking.Data.BedBanksBooking bbRoomBooking = (Affinion.LoyaltyBuild.Api.Booking.Data.BedBanksBooking)item;

                    noOfAdults = Convert.ToString(bbRoomBooking.NoOfAdults);
                    noOfChilderen = Convert.ToString(bbRoomBooking.NoOfChildren);
                    noofRooms = Convert.ToString(bbRoomBooking.Quantity);
                    roomtype = bbRoomBooking.RoomInfo;
                    guestName = bbRoomBooking.LeadGuestFirstName;
                    checkin = bbRoomBooking.CheckinDate;
                    checkout = bbRoomBooking.CheckinDate.AddDays(double.Parse(bbRoomBooking.Duration));
                    bbPropertyName = bbRoomBooking.PropertyName;
                    bbStarRanking = bbRoomBooking.StarRanking;
                    imageUrl = bbRoomBooking.ImageUrl;
                    bbroomType = bbRoomBooking.RoomInfo;              
                    oldPriceForBB = item.OrderLine.GetOrderProperty("OldPrice").Value;
                    int tempOlid = 0;
                    int.TryParse(orderLineId, out tempOlid);
                    roomErrata = bbRoomBooking.GetErrataFromDB(tempOlid);
                    roomcancelCriteria = bbRoomBooking.CancelCriterias;
                }

                Common.SessionHelper.BasketInfo bookingobject = new Common.SessionHelper.BasketInfo();
                bookingobject.Sku = sku;
                bookingobject.ProductType = Convert.ToString(item.Type);
                bookingobject.CheckinDate = ciDate;
                bookingobject.CheckoutDate = String.Format("{0:dd/MM/yyyy}", checkout);
                bookingobject.Nights = (checkout - checkin).ToString("dd");
                bookingobject.Price = (item.OrderLine.Total ?? 0).ToString("#.##");
                bookingobject.TotalPayableToday = Convert.ToString(item.TotalPayableToday);
                bookingobject.ProviderAmount = (decimal)item.ProviderAmount;
                bookingobject.Commissionamount = (decimal)item.CommissionAmount;
                bookingobject.NoOfRooms = noofRooms;
                if (item.Type == ProductType.BedBanks)
                    bookingobject.RoomType = bbroomType;
                else
                    bookingobject.RoomType = item.GetProduct().Name;
                bookingobject.GuestName = Convert.ToString(guestName);
                bookingobject.Adults = noOfAdults;
                bookingobject.People = string.Concat(noOfAdults, " ", "Adults", " ", noOfChilderen, " ", "Children");
                bookingobject.OrderLineId = Convert.ToString(item.OrderLineId);
                bookingobject.SitecoreItem = hotelItem;
                bookingobject.ProviderType = providerstype;
                bookingobject.BBPropertyName = bbPropertyName;
                bookingobject.BBStarRanking = bbStarRanking.ToString();
                bookingobject.BBImageUrl = imageUrl;
                bookingobject.OldPrice = oldPriceForBB;
                bookingobject.SupplierId = item.ProviderId;
                bookingobject.CancellationStartDate = startDate;
                bookingobject.CancellationEndDate = endDate;
                bookingobject.CancellationAmount = cancelAmount;
                bookingobject.Errata = roomErrata;
                bookingobject.CancelCriteria = roomcancelCriteria;
                listBasketInfo.Add(bookingobject);

            }

            return listBasketInfo;
        }

        /// <summary>
        /// Use to populate the AccommodationData
        /// </summary>
        private void PopulateAccommodationData()
        {
            try
            {
                List<RoomReservation> roomReservations = new List<RoomReservation>();
                roomReservations.Add(new RoomReservation { NoOfGuest = 2, GuestName = "Demo 1", OrderLineId = "1176" });
                roomReservations.Add(new RoomReservation { NoOfGuest = 2, GuestName = "Demo 2", OrderLineId = "1178" });
                roomReservations.Add(new RoomReservation { NoOfGuest = 2, GuestName = "Demo 3", OrderLineId = "1178" });
                roomReservations.Add(new RoomReservation { NoOfGuest = 2, GuestName = "Demo 4", OrderLineId = "1179" });
                roomReservations.Add(new RoomReservation { NoOfGuest = 2, GuestName = "Demo 5", OrderLineId = "1180" });
                roomReservations.Add(new RoomReservation { NoOfGuest = 2, GuestName = "Demo 6", OrderLineId = "1181" });

                Random rnd = new Random();
                int count = rnd.Next(1, 6);


                this.RepeaterGuestAccordian.DataSource = roomReservations.GetRange(0, count);
                this.RepeaterGuestAccordian.DataBind();
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }


        #region protected methods

        protected void PanelRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater starRepeater = (Repeater)e.Item.FindControl("StarRepeater");
                Repeater bookingDetailsRepeater = (Repeater)e.Item.FindControl("BookingDetailsRepeater");
                HtmlGenericControl divBBName = e.Item.FindControl("BBNameDiv") as HtmlGenericControl;
                HtmlGenericControl divLBName = e.Item.FindControl("LBNameDiv") as HtmlGenericControl;
                HtmlGenericControl divLBHotelName = e.Item.FindControl("LBHotelNamediv") as HtmlGenericControl;
                HtmlGenericControl divBBHotelName = e.Item.FindControl("BBHotelNamediv") as HtmlGenericControl;
                

                var dataItem = ((BasketInfo)e.Item.DataItem).SitecoreItem;                 
               // var supplierItem = ((BasketInfo)e.Item.DataItem);

                // Booking infor controls
                //Checkin Checkout Rooms People Price
                CheckInLiteral = (Literal)e.Item.FindControl("Checkin");
                CheckOutLiteral = (Literal)e.Item.FindControl("Checkout");
                RoomsLiteral = (Literal)e.Item.FindControl("Rooms");
                PeopleLiteral = (Literal)e.Item.FindControl("People");
                PriceLiteral = (Literal)e.Item.FindControl("Price");

                //Bind ranking related fields
                string ranking = string.Empty;
                if (dataItem != null)
                {
                    ranking = SitecoreFieldsHelper.GetDropLinkFieldValue(dataItem, "StarRanking", "Name");
                    divBBHotelName.Visible = false;
                    divBBName.Visible = false;
                    divLBHotelName.Visible = true;
                    divLBName.Visible = true;
                   
                }
                else
                {
                    ranking = ((BasketInfo)e.Item.DataItem).BBStarRanking;
                    divBBHotelName.Visible = true;
                    divBBName.Visible = true;
                    divLBHotelName.Visible = false;
                    divLBName.Visible = false;
                  
                }

                if (e != null && dataItem != null)
                {
                    int count = 0;
                    var isValid = Int32.TryParse((((BasketInfo)e.Item.DataItem).NoOfRooms), out count);
                    qty = count.ToString();

                    orderLineId = ((BasketInfo)e.Item.DataItem).OrderLineId.ToString();
                }
                

                if (!string.IsNullOrWhiteSpace(ranking))
                {
                    int starRank;
                    bool result = Int32.TryParse(ranking, out starRank);
                    if (result)
                    {
                        List<int> listOfStars = Enumerable.Repeat(starRank, starRank).ToList();
                        starRepeater.DataSource = listOfStars;
                        starRepeater.DataBind();
                    }
                    else
                    {
                        Diagnostics.WriteException(DiagnosticsCategory.ClientPortal,
                        new AffinionException("Unable to parse star rank:" + dataItem.DisplayName), this);
                    }
                }

                //if (IsPatialPayment(supplierType, advanceDays, checkinDate))
                //{
                //    //paymentType.Visible = false;
                //    System.Web.UI.HtmlControls.HtmlContainerControl paymentType=(System.Web.UI.HtmlControls.HtmlContainerControl)(e.Item.FindControl("paymentType"));
                //    paymentType.Visible=true;
                //}

                //bind Roomwise booking details 
                //bookingDetailsRepeater.DataSource = PopulateBasket(dataItem);
                bookingDetailsRepeater.DataSource = PopulateBasket(e.Item);
                bookingDetailsRepeater.DataBind();
                
            }
        }

        #endregion

        #region Private Function

        ///// <summary>
        ///// Use to populate list with sitecore items and other basket related information to show the basket page.
        ///// </summary>
        ///// <returns></returns>
        //private List<BasketInfo> PopulatedDataSourceForBasket()
        //{
        //    try
        //    {
        //        List<BasketInfo> listBasketInfo = new List<BasketInfo>();

        //        listBasketInfo = BasketHelper.GetPopulateBastketListFromDB();



        //        List<Item> supplierList = Sitecore.Context.Database.SelectItems(Constants.BasketPageQueryPath).ToList();

        //        // Query to get the supplier which has the same UCommerceCategoryID           

        //        supplierList = (from itm in listBasketInfo
        //                        join supplier in hotels on itm.CategoryId equals StoreHelper.GetUcommerceCategoryId(supplier).ToString()
        //                        select supplier).ToList();

        //        foreach (var item in listBasketInfo)
        //        {
        //            item.SitecoreItem = supplierList.FirstOrDefault();

        //            // Set the country name from Item ID
        //            if (item != null && item.SitecoreItem != null)
        //            {
        //                SetCountyInfo(item);
        //                supplierList.RemoveAt(0);   // remove first item

        //                //OrderLine orderLine = PurchaseOrder.Get(or).OrderLines.Where(i => i.OrderLineId == item.OrderLineId).FirstOrDefault();
        //                OrderLine orderLine = UCommerce.Api.TransactionLibrary.GetBasket(false).PurchaseOrder.OrderLines.Where(i => Convert.ToString(i.OrderLineId).Equals(item.OrderLineId)).FirstOrDefault();

        //                if (orderLine != null && orderLine.Discounts != null && orderLine.Discounts.Count != 0)
        //                {
        //                    item.RoomType = string.Concat(item.RoomType, "(", orderLine.Discounts.First().CampaignItemName, ")");
        //                }

        //                if (orderLine != null)
        //                {
        //                    DataSet reservationCount = uCommerceConnectionFactory.GetAccomodationDetailsByOrderLineId(orderLine.OrderLineId.ToString());
        //                    string guestName = reservationCount.Tables[0].Rows[0]["GuestName"].ToString();
        //                    if (guestName.Length > 0)
        //                    {
        //                        item.GuestName = guestName;
        //                    }
        //                }
        //            }

        //        }

        //        return listBasketInfo;
        //    }
        //    catch (Exception exception)
        //    {
        //        Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
        //        /// Throw a custom exception with a custom message
        //        throw new AffinionException(exception.Message, exception);
        //    }
        //}

        private static void SetCountyInfo(BasketInfo item)
        {
            if (item.SitecoreItem.Fields["Country"] != null)
            {
                item.Country = Sitecore.Context.Database.GetItem(item.SitecoreItem.Fields["Country"].Value).DisplayName;

                if (item.SitecoreItem.Fields["AddressLine1"].Value.Length > 0)
                    item.AddressInfo = string.Concat(item.SitecoreItem.Fields["AddressLine1"].Value, ",");

                if (item.SitecoreItem.Fields["AddressLine3"].Value.Length > 0)
                    item.AddressInfo = string.Concat(item.AddressInfo, item.SitecoreItem.Fields["AddressLine3"].Value, ",");

                if (item.SitecoreItem.Fields["AddressLine4"].Value.Length > 0)
                    item.AddressInfo = string.Concat(item.AddressInfo, item.SitecoreItem.Fields["AddressLine4"].Value, ",");

                if (item.SitecoreItem.Fields["AddressLine2"].Value.Length > 0)
                    item.AddressInfo = string.Concat(item.AddressInfo, item.SitecoreItem.Fields["AddressLine2"].Value, ",");

                if (item.SitecoreItem.Fields["Town"].Value.Length > 0)
                    item.AddressInfo = string.Concat(item.AddressInfo, item.SitecoreItem.Fields["Town"].Value, ",");

            }
        }

        /// <summary>
        /// Shows and Hides PayByVoucher Section
        /// </summary>
        private void SetPayByVoucherSection()
        {

            if (Sitecore.Context.PageMode.IsPageEditorEditing)
            {
                return;
            }
            //else if (clientItem.Fields["Client Level Rules"].Value.Contains(voucherRuleId))
            //{
            //    Item clientLevelRule = Sitecore.Context.Database.GetItem(voucherRuleId);
            //    if (clientLevelRule.Fields["RuleValue"].Value != null && clientLevelRule.Fields["RuleValue"].Value == "False")
            //    {
            //        PayByGiftVoucher.Attributes["class"] = "hidden";
            //    }
            //}

            if (!ItemHelper.GetBooleanClientRule("Is Voucher allowed for this offer online"))
            {
                //PayByGiftVoucher.Attributes["class"] = "hidden";
            }
        }

        /// <summary>
        /// Populates Basket Order Price Details 
        /// </summary>
        private bool PopulateBasketPaymentDetails()
        {

            int orderId = BasketHelper.GetBasketOrderId();
            string clientId = clientItem.ID.Guid.ToString();
            //BasketInfo basketInfo = BasketHelper.GetPopulateBasketPaymentDetails(orderId, clientId);
            Affinion.LoyaltyBuild.Api.Booking.Data.BasketInfo newBasketData = Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.GetBasket();
            if (newBasketData == null)
            {
                return false;
            }

            ProcessingFee.Text = StoreHelper.GetPriceWithCurrency(Convert.ToDecimal(newBasketData.ProcessingFee), null, BillingCurrency);
            BookingDeposit.Text = StoreHelper.GetPriceWithCurrency(Convert.ToDecimal(newBasketData.TotalPayableToday), null, BillingCurrency);
            TotalpayableAtAccommodation.Text = StoreHelper.GetPriceWithCurrency(Convert.ToDecimal(newBasketData.TotalPayableAccomodation), null, BillingCurrency);
            TotalPrice.Text = StoreHelper.GetPriceWithCurrency(Convert.ToDecimal(newBasketData.TotalAmount), null, BillingCurrency);

            return true;
        }

        /// <summary>
        /// FormatTotalPrice Method
        /// 
        /// </summary>
        /// <param name="price"></param>
        /// <returns></returns>
        private string FormatTotalPrice(string price)
        {
            if (price == string.Empty || price == "")
                return "0.00";
            else if (price.Split('.')[1].Length == 0)
                return (string.Format("{0}.00", price));
            else if (price.Split('.')[1].Length == 1)
                return (string.Format("{0}0", price));
            else
                return price;
        }

        /// <summary>
        /// Set currency in ascx
        /// </summary>
        /// <param name="orderId">Current order id</param>
        private void SetCurrency(int orderId)
        {
            BillingCurrency = PurchaseOrder.Get(orderId).BillingCurrency.ISOCode;
        }

        /// <summary>
        /// Bind text from sitecore
        /// </summary>
        private void BindSitecoreTexts()
        {
            Item currentPageItem = Sitecore.Context.Item;
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "TextMyBookingDetails", BookingDetails);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "TextReservationCompleted", ReservationDetails);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "TextOfferSelected", OfferSelected);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "PaymentTypeMessage", PaymentTypeText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "TextPricePayableToday", PayableTodayText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "TextProcessingFee", ProcessingFeeText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "TextPayableToday", TotalPayableTodayText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "TextTotalPrice", TotalPriceText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "TextGreatChoice", GreatChoiceText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "TextFullPayment", FullText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "TextPartialPayment", PartialText);
        }
        /// <summary>
        /// Populate Roomwise info in Basket Page
        /// </summary>
        /// <returns>Basket Info item list</returns>
        //private List<BasketInfo> PopulateBasket(Item supplier)
        private List<BasketInfo> PopulateBasket(RepeaterItem repItem)
        {
            List<BasketInfo> listBasketInfo = new List<BasketInfo>();
            List<BasketInfo> itemList = new List<BasketInfo>();
            var dataItem = ((BasketInfo)repItem.DataItem).SitecoreItem;
            var provider = ((BasketInfo)repItem.DataItem).ProviderType;
            listBasketInfo = NewPopulatedDataSourceForBasket();

            foreach(var listItem in listBasketInfo)
            {
                if(listItem.ProductType.Equals(ProductType.BedBanks.ToString()) && provider==ProviderType.BedBanks.ToString())
                {
                    itemList.Add(listItem);
                }
                else
                {
                    if (provider == ProviderType.LoyaltyBuild.ToString() && (listItem.ProductType.Equals(ProductType.Room.ToString()) || listItem.ProductType.Equals(ProductType.Package.ToString())) && listItem.SitecoreItem.Name == dataItem.Name)
                    {

                        //BasketInfo Testitem = listBasketInfo.GroupBy(i => i.Sku).Select(i => i.FirstOrDefault());
                        itemList.Add(listItem);
                        //itemList.Add(Testitem);
                    }
                }
            }

            return itemList;
        }

        /// <summary>
        /// Checks whether partial payment 
        /// </summary>
        /// <param name="supplierType">Supplier Type</param>
        /// <param name="advanceDays">Advance days</param>
        /// <param name="checkinDate">Checkin date</param>
        /// <returns></returns>
        //private bool IsPatialPayment(string supplierType, int advanceDays, DateTime checkinDate)
        //{
        //    if (((checkinDate - DateTime.Today).TotalDays >= advanceDays) && string.Equals(supplierType, "holiday homes"))
        //    {
        //        return true;
        //    }
        //    return false;
        //}


        #endregion

        protected void lblRemove_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)(sender);
            string skuId = btn.CommandArgument;
            BasketHelper.RemoveCartItemBySkuId(skuId);

            //Rfresh the page
            Response.Redirect(Request.RawUrl);
        }

        protected void ButtonAddAnotherBreak_Click(object sender, EventArgs e)
        {
            Response.Redirect("/");
        }

        protected void BookingDetailsRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                BasketInfo dataItem = ((BasketInfo)e.Item.DataItem);
                Literal priceLiteral = (Literal)e.Item.FindControl("Price");
                Literal newPriceLiteral = (Literal)e.Item.FindControl("NewPrice");
                HtmlGenericControl newPriceInfoDiv= e.Item.FindControl("NewPriceInfo") as HtmlGenericControl;
                HtmlGenericControl divBBImage = (HtmlGenericControl)e.Item.FindControl("bbImageDiv");
                HtmlGenericControl divLBImage = e.Item.FindControl("lbImageDiv") as HtmlGenericControl;
                HtmlGenericControl divBBErrataInfo = e.Item.FindControl("errataInfoDiv") as HtmlGenericControl;
                HtmlGenericControl divOldPrice = e.Item.FindControl("PriceDiv") as HtmlGenericControl;
                Repeater errataRepeater = (Repeater)e.Item.FindControl("ErrataRepeater");
                Repeater cancellationCriteriaRepeater = (Repeater)e.Item.FindControl("CancellationCriteriaRepeater");
                Literal nonTransferableText = e.Item.FindControl("nonTransferableText") as Literal;
                Literal cancellationHeader = e.Item.FindControl("cancellationHeader") as Literal;
                Literal litPriceInfo = e.Item.FindControl("litPriceInfo") as Literal;
                priceLiteral.Text = StoreHelper.GetPriceWithCurrency(Convert.ToDecimal(dataItem.Price), null, BillingCurrency);
                divBBImage.Visible = false;
                divLBImage.Visible = true;
                newPriceInfoDiv.Visible = false;
                divBBErrataInfo.Visible = false;
                nonTransferableText.Visible = false;
                cancellationHeader.Visible = false;
                litPriceInfo.Visible = false;
                if (dataItem.ProductType.Equals(ProductType.BedBanks.ToString()))
                {
                    divBBErrataInfo.Visible = true;
                    divBBImage.Visible = true;
                    divLBImage.Visible = false;
                    priceLiteral.Text = StoreHelper.GetPriceWithCurrency(Convert.ToDecimal(dataItem.OldPrice),null,BillingCurrency);
                    nonTransferableText.Visible = true;
                    cancellationHeader.Visible = true;
                    litPriceInfo.Visible = true;
                    Item conditionalMessageItem = GetConditionalMessageItem();
                    nonTransferableText.Text = SitecoreFieldsHelper.GetItemFieldValue(conditionalMessageItem, "Bed Banks Non Transferable Message");
                    cancellationHeader.Text = SitecoreFieldsHelper.GetItemFieldValue(conditionalMessageItem, "Bed Banks Cancellation Header");
                    litPriceInfo.Text = SitecoreFieldsHelper.GetItemFieldValue(conditionalMessageItem, "Bed Banks Price Change Message");
                    decimal tempOldPrice = 0; decimal tempNewPrice = 0;
                    decimal.TryParse(dataItem.OldPrice,out tempOldPrice);
                    decimal.TryParse(dataItem.Price, out tempNewPrice);
                    if (!(tempOldPrice==tempNewPrice))
                    {
                        newPriceInfoDiv.Visible = true;
                        divOldPrice.Attributes.Add("style", "text-decoration:line-through");
                        newPriceLiteral.Text = "(" + StoreHelper.GetPriceWithCurrency(tempNewPrice, null, BillingCurrency) + ")";
                    }

                    errataRepeater.DataSource = dataItem.Errata;
                    errataRepeater.DataBind();
                    cancellationCriteriaRepeater.DataSource = dataItem.CancelCriteria;
                    cancellationCriteriaRepeater.DataBind();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RbtPayment_CheckedChanged(object sender, EventArgs e)
        {
            var rbtPayment = (HtmlInputRadioButton)sender;
            if (rbtPayment.Checked)
            {
                CalcPayment(rbtPayment);
            }
        }

        private void CalcPayment(HtmlInputRadioButton rbtPayment)
        {
            var order = TransactionLibrary.GetBasket(false).PurchaseOrder;
            order[OrderPropertyConstants.PaymentMethod] = rbtPayment.Attributes["Value"];
            order.Save();
            PopulateBasketPaymentDetails();
        }

        protected void ErrataRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var dataItem=(BedBankErrata)e.Item.DataItem;
            Literal literalerrataSubject = e.Item.FindControl("BBerrataSubject") as Literal;
            Literal literalerrataDescription = e.Item.FindControl("BBerrataDescription") as Literal;

            literalerrataSubject.Text = dataItem.Subject;
            literalerrataDescription.Text = dataItem.Description;
        }

        protected void CancellationCriteriaRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var dataItem = (BedBankCancelCriteria)e.Item.DataItem;
            Literal literalcancelCriteria = e.Item.FindControl("cancelCriteria") as Literal;
            Item conditionalMessageItem = GetConditionalMessageItem();
            literalcancelCriteria.Text = string.Format(SitecoreFieldsHelper.GetItemFieldValue(conditionalMessageItem, "Bed Banks Cancellation Message"), dataItem.StartDate.ToString("dd/MM/yyyy"), dataItem.EndDate.ToString("dd/MM/yyyy"), StoreHelper.GetPriceWithCurrency(Convert.ToDecimal(dataItem.Amount), null, BillingCurrency));
            
        }
    }
}