﻿CREATE TABLE [dbo].[Descendants] (
    [ID]         UNIQUEIDENTIFIER NULL,
    [Ancestor]   UNIQUEIDENTIFIER NOT NULL,
    [Descendant] UNIQUEIDENTIFIER NOT NULL,
    [DAC_Index]  INT              NULL,
    CONSTRAINT [Descendants_PK] PRIMARY KEY NONCLUSTERED ([Ancestor] ASC, [Descendant] ASC)
);


GO
CREATE CLUSTERED INDEX [DAC_Clustered]
    ON [dbo].[Descendants]([DAC_Index] ASC);


GO
CREATE NONCLUSTERED INDEX [Descendant]
    ON [dbo].[Descendants]([Descendant] ASC);

