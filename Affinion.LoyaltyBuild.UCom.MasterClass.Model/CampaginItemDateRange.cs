﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{

    /// <summary>
    ///Gets Offer Created date and end date
    /// </summary>
    public class CampaginItemDateRange
    {
        /// <summary>
        ///Gets Offer Created date
        /// </summary>
        private string _startDate;

        public string StartDate
        {
            get
            {
                return string.IsNullOrEmpty(_startDate) ? "-" : _startDate;
            }
            set
            {
                _startDate = value;
            }
        }

        private string _endDate;

        /// <summary>
        ///Gets Offer EndDate
        /// </summary>

        public string EndDate
        {
            get
            {
                return string.IsNullOrEmpty(_endDate) ? "-" : _endDate;
            }
            set
            {
                _endDate = value;
            }
        }

    }
    /// <summary>
    /// list of client ids
    /// </summary>
    public class ClientList
    {
        /// <summary>
        ///Client Id
        /// </summary>
        public string ClientId { get; set; }

    }
}
