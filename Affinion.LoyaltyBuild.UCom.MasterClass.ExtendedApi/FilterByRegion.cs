﻿using Affinion.LoyaltyBuild.UCom.Common.Constants;
using Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi.Filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.Search.Facets;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi
{
    public class FilterByRegion : IHotelFilter
    {
        public bool IncludeHotel(UCommerce.Documents.Product product, SearchCriteria criteria)
        {
            return true;
        }

        public UCommerce.Search.Facets.Facet FilterRecordFacet(SearchCriteria criteria)
        {
            if (criteria.Region != "all" && !string.IsNullOrEmpty(criteria.Region))
            {
                var facet = new Facet();
                facet.FacetValues = new List<FacetValue>();
                facet.Name = ProductDefinationConstant.Region;
                foreach (var value in criteria.Region.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    facet.FacetValues.Add(new FacetValue() { Value = value });
                }
                return facet;
            }
            return null;
        }
    }
}
