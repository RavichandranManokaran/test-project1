﻿CREATE TABLE [dbo].[uCommerce_ProductRelationType] (
    [ProductRelationTypeId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]                  NVARCHAR (128) NOT NULL,
    [Description]           NVARCHAR (MAX) NULL,
    [CreatedOn]             DATETIME       CONSTRAINT [DF_uCommerce_ProductRelation_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]             NVARCHAR (50)  NOT NULL,
    [ModifiedOn]            DATETIME       CONSTRAINT [DF_uCommerce_ProductRelation_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]            NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_uCommerce_ProductRelation] PRIMARY KEY CLUSTERED ([ProductRelationTypeId] ASC)
);

