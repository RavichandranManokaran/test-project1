﻿CREATE PROCEDURE [dbo].[LB_InsertChangePasswordHistory]
       @UniqueId    varchar(200) ,
	   @UserName varchar(200)     
AS
BEGIN 
     SET NOCOUNT ON 

     INSERT INTO [dbo].[ChangePasswordHistory]
          ( 
           
           [UniqueId]                    ,
            [UserName] 
			               
          ) 
     VALUES 
          ( 
            @UniqueId                  ,
            @UserName             
			    
          ) 

END 

GO