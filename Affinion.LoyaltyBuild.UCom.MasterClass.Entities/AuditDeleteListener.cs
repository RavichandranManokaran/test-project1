﻿using Affinion.LoyaltyBuild.Audit;
using NHibernate;
using NHibernate.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UCommerce.EntitiesV2;
using UCommerce.Marketing.Awards;
using UCommerce.Marketing.Targets;
using UCommerce.Security;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{

    public class AuditDeleteListener : IPostDeleteEventListener
    {
        private const string _noValueString = "*No Value*";

        public IUserService UserService { get; set; }

        public AuditDeleteListener(IUserService userService)
        {
            this.UserService = userService;
        }



        public void OnPostDelete(PostDeleteEvent deleteEvent)
        {
            if (deleteEvent.Entity is ITarget || deleteEvent.Entity is IAward || deleteEvent.Entity is CampaignItem || deleteEvent.Entity is CampaignItemProperty)
            {
                StringBuilder stroldValue = new StringBuilder();
                StringBuilder strnewValue = new StringBuilder();


                var entityFullName = deleteEvent.Entity.GetType().FullName;

                var propertyNames = deleteEvent.Persister.PropertyNames;

                var session = deleteEvent.Session.GetSession(EntityMode.Poco);

                foreach (var propName in propertyNames)
                {
                    strnewValue.Append(string.Format("\"{0}\":\"{1}\",", propName, _noValueString));
                    stroldValue.Append(string.Format("\"{0}\":\"{1}\",", propName, deleteEvent.Persister.GetPropertyValue(deleteEvent.Entity, propName, EntityMode.Poco)));
                }
                AuditTrail.Instance.Log(new AuditData()
                {
                    AuditName = AuditNames.Promotion,
                    Message = string.Empty,
                    NewValue = string.Format("{{{0}}}", strnewValue.ToString()),
                    ObjectId = deleteEvent.Id.ToString(),
                    ObjectType = AuditObjectType.Ucommerce,
                    Operation = AuditOperations.Delete,
                    OldValue = string.Format("{{{0}}}", stroldValue.ToString()),
                    Remarks = strnewValue.ToString(),
                    Username = this.UserService.GetCurrentUserName(),
                    Section = AuditSection.AdminPortalUcommerce

                });
                session.Flush();
            }
        }
    }
}
