﻿CREATE TABLE [dbo].[Notifications] (
    [Id]           UNIQUEIDENTIFIER NOT NULL,
    [ItemId]       UNIQUEIDENTIFIER NOT NULL,
    [Language]     NVARCHAR (50)    NOT NULL,
    [Version]      INT              NOT NULL,
    [Processed]    BIT              NOT NULL,
    [InstanceType] NVARCHAR (256)   NOT NULL,
    [InstanceData] NVARCHAR (MAX)   NOT NULL,
    [Created]      DATETIME         NOT NULL,
    [DAC_Index]    INT              NULL,
    CONSTRAINT [PK_Notifications] PRIMARY KEY NONCLUSTERED ([Id] ASC)
);


GO
CREATE CLUSTERED INDEX [DAC_Clustered]
    ON [dbo].[Notifications]([DAC_Index] ASC);

