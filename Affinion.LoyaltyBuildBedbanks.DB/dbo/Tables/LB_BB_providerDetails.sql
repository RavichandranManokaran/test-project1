/****** Object:  Table [dbo].[LB_BB_providerDetails]    Script Date: 7/19/2016 8:02:30 PM ******/

CREATE TABLE [dbo].[LB_BB_providerDetails](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[_ProviderID] [int] NOT NULL,
	[availabilityProviderID] [int] NOT NULL,
	[XmlResponse] [varchar](max) NOT NULL,
	[ExpiryTime] [datetime] NOT NULL,
 CONSTRAINT [PK__providerDetails] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

