﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ViewPrice.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Offer.ViewPrice" %>
<table border="2" cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <th>Res. Date From</th>
        <th>Res. Date To</th>
        <th>Arr. Date From</th>
        <th>Arr. Date To</th>
        <th>Room Name</th>
        <th>Price</th>

    </tr>
    <% for(int i=0;i<OfferPrice.Count;i++){ %>
    <tr>
        <td><%= OfferPrice[i].ReservationDate.StartDate %></td>
        <td><%= OfferPrice[i].ReservationDate.EndDate %></td>
        <td><%= OfferPrice[i].ArrivalDate.StartDate %></td>
        <td><%= OfferPrice[i].ArrivalDate.EndDate %></td>
        <td><%= OfferPrice[i].RoomName %></td>
        <td><%=  (OfferPrice[i].price == -1 ? "NA" : OfferPrice[i].price.ToString("0.##")) %></td>
    </tr>
    <% } %>
</table>
