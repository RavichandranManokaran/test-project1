﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="One Four Two.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Views.Full_Width.One_Four_Two" %>


<div class="row main-block topmargin">
    <div class="span12">
        <sc:Placeholder ID="fullrow1" runat="server" Key="full-row1" />
    </div>
</div>

<div class="row main-block">
    <div class="span12">
        <div class="row show-grid hero-list features-list">
            <div class="span3">
                <sc:Placeholder ID="fullrow2a" runat="server" Key="full-row2a" />
            </div>
            <div class="span3">
                <sc:Placeholder ID="fullrow2b" runat="server" Key="full-row2b" />
            </div>
            <div class="span3">
                <sc:Placeholder ID="fullrow2c" runat="server" Key="full-row2c" />
            </div>
            <div class="span3">
                <sc:Placeholder ID="fullrow2d" runat="server" Key="full-row2d" />
            </div>
        </div>
    </div>
</div>

<div class="row show-grid features-block">
    <div class="span6">
        <sc:Placeholder ID="fullrow3a" runat="server" Key="full-row3a" />
    </div>
    <div class="span6">
        <sc:Placeholder ID="fullrow3b" runat="server" Key="full-row3b" />
    </div>
</div>
