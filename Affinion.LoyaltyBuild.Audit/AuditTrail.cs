﻿namespace Affinion.LoyaltyBuild.Audit
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Exceptions;
    using Affinion.LoyaltyBuild.DataAccess;
    using Affinion.LoyaltyBuild.DataAccess.DatabaseScripts;
    using System.Data;
    using Newtonsoft.Json.Linq;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Newtonsoft.Json;

    /// <summary>
    /// Having functions related to Audit Trail
    /// </summary>
    public class AuditTrail
    {
        private static AuditTrail instance;

        /// <summary>
        /// Gets the singleton instance of the Audit Trail object
        /// </summary>
        public static AuditTrail Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AuditTrail();
                }

                return instance;
            }
        }

        /// <summary>
        /// Private constructor
        /// </summary>
        private AuditTrail() { }

        /// <summary>
        /// Log the audit Record
        /// </summary>
        /// <param name="data">The record object to store</param>
        /// <returns>Returns true if the code has been written successfully</returns>
        public bool Log(AuditData data)
        {
            try
            {
                // Validate inputs
                this.ValidateAuditData(data);

                // initiate connection
                DataConnection connection = new DataConnection();
                DataObject controlData = new DataObject();

                // add parameters
                controlData.DatabaseScript = new AuditTrailScripts();
                controlData.Command = "LB_AuditLogMessage";
                controlData.Parameters.Add("@AuditName", data.AuditName.ToString());
                controlData.Parameters.Add("@Message", data.Message);
                controlData.Parameters.Add("@Operation", data.Operation.ToString());
                controlData.Parameters.Add("@ObjectType", data.ObjectType.ToString());
                controlData.Parameters.Add("@ObjectId", data.ObjectId);
                controlData.Parameters.Add("@Section", data.Section.ToString());
                controlData.Parameters.Add("@OldValue", data.OldValue);
                controlData.Parameters.Add("@NewValue", data.NewValue);
                controlData.Parameters.Add("@Username", data.Username);
                controlData.Parameters.Add("@Date", data.Date.ToShortDateString());
                controlData.Parameters.Add("@Time", data.Time);
                controlData.Parameters.Add("@AdditionalData", data.AdditionalData.ToString(Formatting.None));
                controlData.Parameters.Add("@Remarks", data.Remarks);

                // write data
                controlData = connection.Write(controlData);

                return true;
            }
            catch(Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.AuditTrail, new AffinionException("Error in Audit Trail log function", ex), this);
                return false;
            }
        }

        /// <summary>
        /// Log multiple audit Records
        /// </summary>
        /// <param name="dataList">List of audit data records to store</param>
        /// <returns>Returns true if the code has been written successfully</returns>
        public bool Log(IEnumerable<AuditData> dataList)
        {
            try
            {
                foreach (AuditData data in dataList)
                {
                    instance.Log(data);
                }

                return true;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.AuditTrail, new AffinionException("Error in Audit Trail bulk log function", ex), this);
                return false;
            }
        }

        /// <summary>
        /// Read Audit records
        /// </summary>
        /// <param name="data">The conctrol object with parameters.</param>
        /// <returns>Returns list of audit records or null</returns>
        public IEnumerable<AuditData> Read(AuditSearchData data)
        {
            try
            {
                // initiate connection
                DataConnection connection = new DataConnection();
                DataObject controlData = new DataObject();

                // add parameters
                controlData.DatabaseScript = new AuditTrailScripts();
                controlData.Command = "LB_AuditReadLogMessage";

                if (data.AuditName != AuditNames.Undefined)
                    controlData.Parameters.Add("@AuditName", data.AuditName.ToString());

                if (data.Operation != AuditOperations.Undefined)
                    controlData.Parameters.Add("@Operation", data.Operation.ToString());

                if (data.ObjectType != AuditObjectType.Undefined)
                {
                    controlData.Parameters.Add("@ObjectType", data.ObjectType.ToString());
                    controlData.Parameters.Add("@ObjectId", data.ObjectId);
                }

                if (data.Section != AuditSection.Undefined)
                    controlData.Parameters.Add("@Section", data.Section.ToString());

                controlData.Parameters.Add("@TextValue", data.TextValue);
                controlData.Parameters.Add("@Username", data.Username);
                controlData.Parameters.Add("@StartDateTime", data.StartDateTime);
                controlData.Parameters.Add("@EndDateTime", data.EndDateTime);

                // write data
                controlData = connection.Read(controlData);

                if (controlData.Exception == null)
                {
                    IEnumerable<AuditData> records = controlData.Data.Rows.OfType<DataRow>().Select(record => this.GetAuditRecord(record));
                    return records;
                }

                return null;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.AuditTrail, new AffinionException("Error in Audit Trail read function", ex), this);
                return null;
            }
        }

        public bool Purge(AuditSearchData data)
        {
            // implement logiv
            return false;
        }

        /// <summary>
        /// Validates the Audit data parameters before writing
        /// </summary>
        /// <param name="data">The audit data object</param>
        private void ValidateAuditData(AuditData data)
        {
            StringBuilder errorMessage = new StringBuilder();

            if (data.AuditName == null || data.AuditName == AuditNames.Undefined)
            {
                errorMessage.Append("Invalid audit name.");
            }

            if (data.Operation == null || data.Operation == AuditOperations.Undefined)
            {
                errorMessage.AppendLine("Invalid operation type.");
            }

            if (data.ObjectType == null || data.ObjectType == AuditObjectType.Undefined)
            {
                errorMessage.AppendLine("Invalid object type.");
            }

            if (data.Section == null || data.Section == AuditSection.Undefined)
            {
                errorMessage.AppendLine("Invalid section name.");
            }

            if (data.Username == null)
            {
                errorMessage.AppendLine("Invalid username.");
            }

            if (!string.IsNullOrEmpty(errorMessage.ToString()))
            {
                throw new AffinionException(errorMessage.ToString());
            }
        }

        /// <summary>
        /// Generate AuditData object from database record
        /// </summary>
        /// <param name="record">Database record as a datatable row</param>
        /// <returns>Returns the AuditData object</returns>
        private AuditData GetAuditRecord(DataRow record)
        {
            return new AuditData()
            {
                Id = record.Field<int>("Id"),
                AuditName = (AuditNames)Enum.Parse(typeof(AuditNames), record.Field<string>("AuditName"), true),
                Message = record.Field<string>("Message"),
                Operation = (AuditOperations)Enum.Parse(typeof(AuditOperations), record.Field<string>("Operation"), true),
                ObjectType = (AuditObjectType)Enum.Parse(typeof(AuditObjectType), record.Field<string>("ObjectType"), true),
                ObjectId = record.Field<string>("ObjectId"),
                Section = (AuditSection)Enum.Parse(typeof(AuditSection), record.Field<string>("Section"), true),
                OldValue = record.Field<string>("OldValue"),
                NewValue = record.Field<string>("NewValue"),
                Username = record.Field<string>("Username"),
                Date = record.Field<DateTime>("Date"),
                Time = record.Field<DateTime>("Time"),
                AdditionalData = JObject.Parse(record.Field<string>("AdditionalData")),
                Remarks = record.Field<string>("Remarks"),
            };
        }
    }
}
