﻿--CREATE PROCEDURE [dbo].[LB_GetAccomodationDetailsByOrderLineId](@OrderLineId int)
--AS
--BEGIN
--   select [GuestName] from 
--  [dbo].[LB_RoomReservation]
--  where [OrderLineId] = @OrderLineId
--END


CREATE PROCEDURE [dbo].[LB_GetAccomodationDetailsByOrderLineId]
	( @OrderLineId int )
AS
BEGIN
  -- select [Value] from 
  --[dbo].[uCommerce_OrderProperty]
  --where [Key]='GuestName' AND [OrderLineId] = @OrderLineId


		select _GuestName [GuestName]
		from 
		(
			select OrderLineId, [key], [value]
			from uCommerce_OrderProperty where OrderLineId=@OrderLineId
		) src
		pivot
		(
			max([value])
			for [key] in (_GuestName)
		)	piv

END