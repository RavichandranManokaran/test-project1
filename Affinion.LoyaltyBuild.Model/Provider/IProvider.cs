﻿using Affinion.LoyaltyBuild.Model.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Model.Provider
{
    /// <summary>
    /// Base Provider interface
    /// </summary>
    public interface IProvider : IBaseProvider
    {
        /// <summary>
        /// Name
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// List of products for that provider
        /// </summary>
        IList<IProduct> Products { get; set; }
    }
}
