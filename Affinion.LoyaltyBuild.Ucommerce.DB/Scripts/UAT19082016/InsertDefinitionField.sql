﻿
Declare @datatypeid int=0
IF NOT EXISTS(select 1 from uCommerce_DefinitionField where Name = 'Commission' )
BEGIN
select @datatypeid=DataTypeId from uCommerce_DataType where [DefinitionName]='SiteCoreCommission'

IF @datatypeid > 0 

BEGIN
     Insert into uCommerce_DefinitionField(DataTypeId,DefinitionId,Name,DisplayOnSite,Multilingual,RenderInEditor,Searchable,SortOrder,Deleted,BuiltIn,DefaultValue,Guid)
  values (@datatypeid , 2,'Commission',1,0,1,0,0,0,0,'',NEWID())
END
	
END