﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Article Title and Body.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Article_Title_and_Body" %>

<article>
 <h1><sc:FieldRenderer ID="FieldRenderer1" runat="server" FieldName="Title" /></h1>
 <sc:FieldRenderer ID="frBody" runat="server" FieldName="Body" />
</article>
