﻿CREATE TABLE [dbo].[uCommerce_Currency] (
    [CurrencyId]   INT              IDENTITY (1, 1) NOT NULL,
    [ISOCode]      NVARCHAR (50)    NOT NULL,
    [ExchangeRate] INT              NOT NULL,
    [Deleted]      BIT              DEFAULT ((0)) NOT NULL,
    [Guid]         UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    CONSTRAINT [uCommerce_PK_Currency] PRIMARY KEY CLUSTERED ([CurrencyId] ASC)
);

