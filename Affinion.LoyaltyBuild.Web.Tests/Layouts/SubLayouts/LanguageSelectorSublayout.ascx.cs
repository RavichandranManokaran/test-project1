﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
///Source File:           LanguageSelectorSublayout.ascx.cs
///Sub-system/Module:     Affinion.LoyaltyBuild.Web.Tests
///Description:           LanguageSelector presantation and manuplation.
///
#region using Directives
using Affinion.LoyaltyBuild.Common.Languages;
using System.Collections.Generic;
using System;
using Sitecore.Links;
#endregion

namespace Affinion.LoyaltyBuild.Web.Tests.Layouts.SubLayouts
{
    
    public partial class LanguageSelectorSublayout : System.Web.UI.UserControl
    {
        private void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            LanguageRepository languageRepository = new LanguageRepository();
            List<Country> languages = languageRepository.GetSelectedLanguages();

            LanguageSelector.DataSource = languages;
            LanguageSelector.DataBind(); 
        }
    }
}