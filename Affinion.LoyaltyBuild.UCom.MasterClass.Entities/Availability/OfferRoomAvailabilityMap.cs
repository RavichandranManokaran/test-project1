﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce;
using UCommerce.EntitiesV2.Maps;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Availability
{
    public class OfferRoomAvailabilityMap : BaseClassMap<OfferRoomAvailability>
    {
        public OfferRoomAvailabilityMap()
        {
            base.Table("LB_OfferAvailability");
            base.Id(x => x.OfferAvailibilityID).GeneratedBy.GuidNative();
            Map(x => x.CampaignItem).Not.Nullable();
            Map(x => x.Sku).Not.Nullable();
            Map(x => x.VariantSku).Not.Nullable();
            Map(x => x.NumberAllocated).Not.Nullable();
            Map(x => x.NumberBooked).Not.Nullable();
            Map(x => x.DateCounter).Not.Nullable();
            Map(x => x.IsRestrictedForSale).Not.Nullable();
            Map(x => x.CreatedBy).Nullable();
            Map(x => x.UpdateBy).Nullable();
            Map(x => x.DateCreated).Nullable();
            Map(x => x.DateLastUpdate).Nullable();            
        }
    }
}
