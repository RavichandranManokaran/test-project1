﻿CREATE TABLE [dbo].[LB_RecentlySeenHotels] (
    [RSeenID]    INT           IDENTITY (1, 10) NOT NULL,
    [CustomerID] INT           NULL,
    [HotelID]    VARCHAR (50)  NULL,
    [CreatedBy]  VARCHAR (100) NULL,
    [UpdatedBy]  VARCHAR (100) NULL,
    [RSeenDate]  DATETIME      NULL
);

