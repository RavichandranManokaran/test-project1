﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.SupplierPortal.Reports.Data
{
    /// <summary>
    /// Base Revenue report item used by two revenue report items
    /// </summary>
    public class RevenueBase
    {
        /// <summary>
        /// Number Of Bookings
        /// </summary>
        public int? NumberOfBookings { get; set; }

        /// <summary>
        /// Total Revenue
        /// </summary>
        public int? TotalRevenue { get; set; }
    }
}
