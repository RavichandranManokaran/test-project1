﻿using Affinion.LoyaltyBuild.BedBanks.Troika;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Message.Interfaces;
using System;
using System.IO;
using System.Text;
using System.Diagnostics;
using System.Xml.Serialization;
using Affinion.LoyaltyBuild.BedBanks.General;
using System.Collections.Specialized;
namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Message
{
    [Serializable()]
    [XmlType(TypeName = "CancelRequest")]
    public class JtCancelRequest : JtRequest, IJtRequest
    {
        public string BookingReference { get; set; }
        public double CancellationCost { get; set; }
        public string CancellationToken { get; set; }

        //Parameterless Constructor - Needed for XML Serialization
        private JtCancelRequest() { }
        BedBanksSettings bbsettings;
        //The real constructor
        public JtCancelRequest(string pBookingReference, double pCancellationCost, string pCancellationToken, BedBanksSettings bbs)
        {
            this.bbsettings = bbs;
            int currId = 0;
            if (bbs.BedBanksActualCurrency != null)
            {
                int.TryParse(bbs.BedBanksActualCurrency.BedBanksCurrencyId, out currId);
                LoginDetails = JtController.GenerateLoginDetails(currId,bbs.BedBanksUserName, bbs.BedBanksPassword);
            }
            else
            {
                int.TryParse(bbs.BedBanksDefaultCurrency.BedBanksCurrencyId, out currId);
                LoginDetails = JtController.GenerateLoginDetails(currId, bbs.BedBanksUserName, bbs.BedBanksPassword);
            }
            BookingReference = pBookingReference;
            CancellationCost = pCancellationCost;
            CancellationToken = pCancellationToken;
        }

        public IJtResponse Fire()
        {
            Stopwatch apiTimer = Stopwatch.StartNew();
            string response = JtController.PerformApiRequest(this);
            apiTimer.Stop();

            //READ THE RESULTS
            XmlRootAttribute xRoot = new XmlRootAttribute
            {
                ElementName = "CancelResponse",
                IsNullable = true
            };

            XmlSerializer serializer = new XmlSerializer(typeof(JtCancelResponse), xRoot);

            JtCancelResponse cRes;

            try
            {
                MemoryStream memStream = new MemoryStream(Encoding.UTF8.GetBytes(response));
                cRes = (JtCancelResponse)serializer.Deserialize(memStream);
            }
            catch (Exception fnfExc)
            {
                NameValueCollection exception = new NameValueCollection();
                exception.Add(Constants.ExceptionMessage, fnfExc.Message);
                exception.Add(Constants.ExceptionFlow, Constants.CancelRequest);
                SendExceptionMail(exception, bbsettings.ClientItemId);
                throw new Exception(fnfExc.Message);
            }

            cRes.ResponseTime = apiTimer.ElapsedMilliseconds;
            TroikaDataController.InsertApiResponseTime(1, "Cancel", cRes.ResponseTime);

            return cRes;
        }
    }
}
