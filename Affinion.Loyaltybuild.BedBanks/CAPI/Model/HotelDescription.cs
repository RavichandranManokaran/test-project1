﻿namespace Affinion.Loyaltybuild.BedBanks.CAPI.Model
{
    /// <summary>
    /// Summary description for HotelDescription
    /// </summary>
    public class HotelDescription
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public HotelDescription(string pName, string pDescription)
        {
            Name = pName;
            Description = pDescription;
        }
    }
}