
/****** Object:  Table [dbo].[LB_BB_availabilityProvider]    Script Date: 7/19/2016 7:57:29 PM ******/

CREATE TABLE [dbo].[LB_BB_availabilityProvider](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[source] [varchar](50) NOT NULL,
 CONSTRAINT [PK__availabilityProvider] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

--INSERT INTO LB_BB_availabilityProvider (source) values ('JACTRAVEL')
--INSERT INTO LB_BB_availabilityProvider (source) values ('LOYALTYBUILD')
--GO
