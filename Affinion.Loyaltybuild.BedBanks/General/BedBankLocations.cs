﻿using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.BedBanks.General
{
    public class BedBankLocations
    {
        /// <summary>
        /// Name of Location
        /// </summary>
        public string LocationName { get; set; }

        /// <summary>
        /// Location Id in Bed Banks
        /// </summary>
        public string BedBanksLocationId { get; set; }

        /// <summary>
        /// Location Item in Sitecore
        /// </summary>
        public Item SitecoreLocationItem { get; set; }

        
    }
}
