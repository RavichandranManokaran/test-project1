
CREATE procedure [dbo].[LB_AddCustomerForSubscription](
	@EmailAddress varchar(100),
	@CustomerId int,
	@ClientId varchar(100),
	@SubscriptionType varchar(50))
As
Declare
@MailAddress int,
@SubscriptionTypeCheck int
Begin
select @MailAddress=count(EmailID) from uCommerce_Subscriber where EmailID=@EmailAddress and ClientID=@ClientId 
select @SubscriptionTypeCheck=count(EmailID) from uCommerce_Subscriber where EmailID=@EmailAddress and ClientID=@ClientId and SubscriptionID<>(select SubscriptionID from uCommerce_Subscription where SubscriptionType=@SubscriptionType)
if(@MailAddress = 0)
	begin
		insert into uCommerce_Subscriber 
		(
		ClientID,
		CustomerID,
		EmailID,
		SubscriptionID,
		SubscribeDate,
		UnsubscribeReason,
		UnsubscribeDate
		)
		Values
		(
		@ClientId,
		@CustomerId,
		@EmailAddress,
		(select SubscriptionId from uCommerce_Subscription where SubscriptionType=@SubscriptionType),
		CONVERT(date, getdate()),
		'',
		''
		)
	End
if(@SubscriptionTypeCheck > 0)
	begin
		update uCommerce_Subscriber set SubscriptionID=(select SubscriptionID from uCommerce_Subscription where SubscriptionType=@SubscriptionType) where EmailID=@EmailAddress
	End
End
GO

