﻿
using Affinion.LoyaltyBuild.Api.Search.Data;
using Affinion.LoyaltyBuild.Api.Search.Filters;
using Affinion.LoyaltyBuild.BedBanks.General;
using Affinion.LoyaltyBuild.BedBanks.Helper;
using Affinion.LoyaltyBuild.BedBanks.JacTravel;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Message;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Model;
using Affinion.LoyaltyBuild.BedBanks.Troika;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.Model.Product;
using Affinion.LoyaltyBuild.Model.Provider;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using data = Affinion.LoyaltyBuild.Api.Search.Data;

namespace Affinion.LoyaltyBuild.Api.Search.ServiceProviders
{
    /// <summary>
    /// Bedbanks provider service
    /// </summary>
    public class BedBanksProvider : IServiceProvider
    {

        /// <summary>
        /// Search providers based on filter
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        public ProviderCollection SearchProviders(IList<Filters.ISearchFilter> filters)
        {
            if (filters == null)
                return null;

            var criteria = new BBsearchCriteria();

            foreach (var i in filters)
            {
                IBedBankFilter bedBankFilter = i as IBedBankFilter;
                if (bedBankFilter != null)
                    bedBankFilter.BuildbbSearch(ref criteria);
            }
            try
            {
                if (string.IsNullOrWhiteSpace(criteria.ArrivalDate) || string.IsNullOrWhiteSpace(criteria.RegionId))
                    return null;
                
                JacTravelApiWrapper jac = new JacTravelApiWrapper();
                var hotels = jac.Search(criteria);
                hotels=FilterResults(hotels,criteria);
                ProviderCollection bbProviderCollection = ConvertBBHotelDetailListToProviderCollection(hotels, criteria.ArrivalDate, criteria.BedBankData);
                return bbProviderCollection;
            }
            catch (Exception ex)
            {
                //return empty object on exception
                Sitecore.Diagnostics.Log.Error(string.Format("Bedbanks Error: {0}", ex.Message), ex, this);
                return null;
            }

        }

        private JtSearchResponse FilterResults(JtSearchResponse hotels, BBsearchCriteria criteria)
        {
            if(!string.IsNullOrWhiteSpace(criteria.StarRanking))
            {
                List<PropertyResult> filteredResult = new List<PropertyResult>();
                string[] starList= criteria.StarRanking.Split('|');
                foreach (var star in starList)
                {
                    float tempStar = 0f;
                    float.TryParse(star, out tempStar);

                    var hotel = hotels.PropertyResults.Where(x => (float.Parse(x.Rating)) == tempStar).ToList<PropertyResult>();
                    if (hotel != null)
                        filteredResult.AddRange(hotel);
                    
                }
                hotels.PropertyResults = filteredResult;               
            }
            if (!string.IsNullOrWhiteSpace(criteria.Facility))
            {
                List<PropertyResult> filteredResult = new List<PropertyResult>();
                List<string> facilityIds = criteria.Facility.Split('|').ToList<string>();
                foreach (var propResult in hotels.PropertyResults)
                {
                    var bbFacilities = propResult.PropertyDetails.Facilities;
                    bool isFacilityPresent = bbFacilities.Any(bbbf => facilityIds.Any(facId => bbbf.FacilityID.ToString().Equals(facId, StringComparison.InvariantCultureIgnoreCase)));
                    if (isFacilityPresent)
                    {
                        filteredResult.Add(propResult);
                    }
                }
                hotels.PropertyResults = filteredResult;
            }
            return hotels;
        }

        /// <summary>
        /// Method to convert BB hotels to provider Collection
        /// </summary>
        /// <param name="hotels"></param>
        /// <returns></returns>
        private static ProviderCollection ConvertBBHotelDetailListToProviderCollection(JtSearchResponse hotels, string arrivalDate, BedBanksSettings bbs)
        {
            string BBCurrencyId = string.Empty;
            ProviderCollection bbProviderCollection = new ProviderCollection(ProviderType.BedBanks);
            List<PropertyResult> bbHotels = hotels.PropertyResults.ToList<PropertyResult>();
            if (bbHotels != null)
            {
                int ucomCurrencyId;
                if (bbs.BedBanksActualCurrency == null)
                {
                    ucomCurrencyId = ReverseMapCurrency(bbs.BedBanksDefaultCurrency);
                    BBCurrencyId=bbs.BedBanksDefaultCurrency.BedBanksCurrencyId;
                }
                else
                {
                    ucomCurrencyId = ReverseMapCurrency(bbs.BedBanksActualCurrency);
                    BBCurrencyId = bbs.BedBanksActualCurrency.BedBanksCurrencyId;
                }
                decimal margin=0;
                int marginId=0;
                List<string> providerList = bbHotels.Select(x => x.PropertyID).ToList<string>();
                Dictionary<string, string> marginInfo = GetMargin(string.Join(",", providerList), arrivalDate);
               
                foreach (var bbHotel in bbHotels)
                {
                    SearchResult searchResultItem = new SearchResult();
                    searchResultItem.ProviderId = string.IsNullOrEmpty(bbHotel.PropertyID) ? string.Empty : bbHotel.PropertyID;
                    searchResultItem.Name = string.IsNullOrEmpty(bbHotel.PropertyName) ? string.Empty : bbHotel.PropertyName;
                    searchResultItem.ProviderType = ProviderType.BedBanks;
                    if (!string.IsNullOrWhiteSpace(bbHotel.Rating))
                        searchResultItem.StarRanking = Convert.ToInt32((Convert.ToDouble(bbHotel.Rating))); 
                    else
                    searchResultItem.StarRanking = 0;
                    int currTemp = 0;
                    int.TryParse(BBCurrencyId, out currTemp);
                    searchResultItem.CurrencyId =currTemp;
                    if (bbHotel.PropertyDetails != null)
                        searchResultItem.SitecoreItemId = string.IsNullOrEmpty(bbHotel.PropertyDetails.SitecoreHotelId) ? string.Empty : bbHotel.PropertyDetails.SitecoreHotelId;
                    searchResultItem.PropertyReferenceId = string.IsNullOrEmpty(bbHotel.PropertyReferenceID) ? string.Empty : bbHotel.PropertyReferenceID;
                    var products = new List<IProduct>();
                    
                    int mealBasisId = 0;
                    string mealBasis = string.Empty;

                    foreach (var item in bbHotel.RoomTypes)
                    {
                        var marginData = marginInfo.Where(key => key.Key == bbHotel.PropertyID).Select(y => y.Value).FirstOrDefault();
                        if (marginData != null)
                        {
                            string[] marginValues=marginData.Split(',');
                            decimal.TryParse(marginValues[1], out margin);
                            int.TryParse(marginValues[0], out marginId);
                        }
                        if (margin == 0)
                        {
                            var defaultMargin =marginInfo.Where(key => key.Key.Equals("Default Margin")).Select(y => y.Value).FirstOrDefault();
                            if (defaultMargin != null)
                            {
                                string[] marginValues = defaultMargin.Split(',');
                                decimal.TryParse(marginValues[1], out margin);
                                int.TryParse(marginValues[0], out marginId);
                            }
                        }

                        if (item != null && item.RoomType != null)
                        {
                            int.TryParse(item.MealBasisID, out mealBasisId);
                            if(mealBasisId>0)
                            {
                                mealBasis = " ("+item.MealBasis+")";
                            }
                            products.Add(new BedBankProduct
                            {
                                Sku = "BedBanksProduct",
                                VariantSku = "DefaultRoomType",
                                Name = item.RoomType + mealBasis,
                                Type = ProductType.BedBanks,
                                Price = item.Total,
                                CurrencyId=ucomCurrencyId,
                                MarginId=marginId,
                                FinalPrice=item.Total+((margin/100)*item.Total),
                                MarginAmount=margin*item.Total/100,
                                MarginPercentage=margin,
                                TokenForBooking = (item.BookingToken == null) ? item.PropertyRoomTypeID : item.BookingToken,
                                MealBasisID=item.MealBasisID,
                                ProviderId=bbHotel.PropertyID,
                                BBCurrencyId=BBCurrencyId,
                                PropertyReferenceId=string.IsNullOrEmpty(bbHotel.PropertyReferenceID) ? string.Empty : bbHotel.PropertyReferenceID
                            });
                        }
                    }
                    searchResultItem.Products = products;
                    bbProviderCollection.Add(searchResultItem);
                }   
            }
            return bbProviderCollection;
        }

        /// <summary>
        /// Search providers based on filter asyncronusly
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        public async Task<ProviderCollection> SearchProvidersAsync(IList<Filters.ISearchFilter> filters)
        {
            return await Task.Run<ProviderCollection>(() =>
                SearchProviders(filters));
        }

        /// <summary>
        /// Get provider details
        /// </summary>
        /// <param name="prodiverInfo"></param>
        /// <returns></returns>
        public IProvider GetProviderDetails(IBaseProvider providerInfo)
        {
            //D
            //var hotel =(BedBankHotel)providerInfo;
            JtPropertyDetailsResponse propertyDetailsRes = JtController.GetSinglePropertyDetails(providerInfo.ProviderId);
            if (propertyDetailsRes == null)
            {
                //JtSearchResponse provider = JtController.SearchOnPropReferenceId(searchKey.);
                return null;
            } 
            decimal stars;
           
            decimal.TryParse(propertyDetailsRes.Rating, out stars);
            BedBankHotel provider = new BedBankHotel()
            {
                ProviderId = propertyDetailsRes.PropertyId,
                PropertyReferenceId = propertyDetailsRes.PropertyReferenceId,
                SitecoreItemId=propertyDetailsRes.SitecoreHotelId,
                ProviderType = ProviderType.BedBanks,
                IntroductionImage = propertyDetailsRes.CMSBaseURL + propertyDetailsRes.MainImage,
                Name = propertyDetailsRes.PropertyName,
                AddressLine1 = propertyDetailsRes.Address1 + ", " + propertyDetailsRes.Address2,
                AddressLine2 = propertyDetailsRes.Region,
                Country = propertyDetailsRes.Country,
                Region = propertyDetailsRes.TownCity,
                Postcode = propertyDetailsRes.Postcode,
                Latitude = propertyDetailsRes.Latitude,
                Longitude = propertyDetailsRes.Longitude,
                StrapLine = propertyDetailsRes.Strapline,
                Description = propertyDetailsRes.Description,
                Images = propertyDetailsRes.Images.Select(x => propertyDetailsRes.CMSBaseURL + x.Image).ToList<string>(),
                Phone = propertyDetailsRes.Telephone,
                Email= string.Empty,
                StarRanking = (int)stars,
                MinAdults = int.Parse(propertyDetailsRes.MinAdults),
                MaxAdults = int.Parse(propertyDetailsRes.MaxAdults),
                MaxChildren = int.Parse(propertyDetailsRes.MaxChildren),
                Facilities = MapBedBanksFacilities(propertyDetailsRes.Facilities)
                
            };
            var address = new List<string>();
            if (!string.IsNullOrWhiteSpace(provider.AddressLine1))
                address.Add(provider.AddressLine1.Trim());
            if (!string.IsNullOrWhiteSpace(provider.AddressLine2))
                address.Add(provider.AddressLine2.Trim());
            if (!string.IsNullOrWhiteSpace(provider.Region))
                address.Add(provider.Region.Trim());
            if (!string.IsNullOrWhiteSpace(provider.Country))
                address.Add(provider.Country.Trim());
            provider.Address = string.Join(", ", address);

            return provider;
        }

        private List<Item> MapBedBanksFacilities(JtFacility[] facilitites)
        {
            
            Item clientItem = SitecoreFieldsHelper.GetLookupFieldTargetItem(ItemHelper.RootItem, "ClientDetails");
            if (clientItem == null)
            {
                var searchKey = new data.SearchKey(HttpContext.Current);
                //check client id
                if (searchKey != null && searchKey.ClientId != null)
                    clientItem = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(searchKey.ClientId));
                Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "Map BedBanks Facilities ClientId" + searchKey.ClientId??string.Empty);
                if (HttpContext.Current.Request.Cookies["_clientId"] != null)
                    Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "Map BedBanks Facilities ClientId from cookies" + HttpContext.Current.Request.Cookies["_clientId"].Value);
                if (clientItem == null)
                {
                    return null;
                }
            }
            List<Item> facilityItem = new List<Item>();
            BedBanksSettings bedBankData = new BedBanksSettings(clientItem);
            List<BedBanksFacility> bedBanksFacility = bedBankData.BedBanksFacility;
            if ((facilitites != null) && (bedBanksFacility != null))
            {
                List<BedBanksFacility> matchingFacilities = bedBanksFacility.Where(bbf => facilitites.Any(fac => int.Parse(bbf.FacilityId) == fac.FacilityID)).ToList<BedBanksFacility>();
                if (matchingFacilities != null)
                {
                    foreach (var matchingfacility in matchingFacilities)
                    {
                        facilityItem.Add(matchingfacility.SitecoreFacilityItem);
                    }
                }
            }
            return facilityItem;
        }

        private static int ReverseMapCurrency(BedBanksCurrency curr)
        {
            int currencyId = 0;
            if (curr != null)
            { 
            Item ucomCurrencyItem = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(curr.uCommerceCurrencyId));
            if (ucomCurrencyItem != null)
            { 
                   int.TryParse(ucomCurrencyItem.Fields["UCommerceId"].Value, out currencyId);
            }
            }
            return currencyId;
        }

        private static Dictionary<string, string> GetMargin(string providerId, string arrivalDate)
        {
            Dictionary<string, string> marginDetails = new Dictionary<string, string>();
            DataTable marginData = TroikaDataController.GetMarginForEAchBBHotel(providerId, arrivalDate.ToString());
            string marginId = marginData.Rows[0][0].ToString();
            string marginPercentage = marginData.Rows[0][1].ToString();
            for (int rowCount = 0; rowCount < marginData.Rows.Count; rowCount++)
            {
                marginDetails.Add(marginData.Rows[rowCount][2].ToString(), marginData.Rows[rowCount][0].ToString()+","+marginData.Rows[rowCount][1].ToString());
            }
            return marginDetails;
        } 

        public IProvider GetProviderWithProduct(ProviderSearch provider)
        {
            return null;
        }
    }
}
