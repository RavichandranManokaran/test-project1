﻿Create procedure LB_BB_ReverseMapLBtoBB(@sitecoreItemId varchar(max))
as
begin

if exists ( select prod.XmlResponse from LB_BB_Provider pro
  inner join LB_BB_providerDetails prod on pro.availabilityProviderRecordID=prod._ProviderID
  where pro.lbProviderID=@sitecoreItemId)
  begin
  select cast(prod.XmlResponse as xml).value('data(PropertyDetailsResponse/PropertyReferenceID)[1]','varchar(100)') as PropertyReferenceID, prod._ProviderID as ProviderID from LB_BB_Provider pro
  inner join LB_BB_providerDetails prod on pro.availabilityProviderRecordID=prod._ProviderID
  where pro.lbProviderID=@sitecoreItemId
  end
  end