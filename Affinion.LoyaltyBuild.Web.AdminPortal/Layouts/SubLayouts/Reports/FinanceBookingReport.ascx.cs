﻿using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.Reports
{
    public partial class FinanceBookingReport : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ExcelReport_Click(object sender, EventArgs e)
        {
            GetFinanceBookingReport("Excel");
        }

        protected void HtmlReport_Click(object sender, EventArgs e)
        {
            GetFinanceBookingReport("Html");
        }

        private void GetFinanceBookingReport(string reportFormat)
        {
            try
            {
                IReportsService iReportsService = new ReportsService();
                SearchCriteria objSearchCriteria = new SearchCriteria();
                DateTime startDate = new DateTime();
                DateTime endDate = new DateTime();
                if (DateTime.TryParse(this.StartDate.Value, out startDate))
                    objSearchCriteria.StartDate = startDate;
                if (DateTime.TryParse(this.EndDate.Value, out endDate))
                    objSearchCriteria.EndDate = endDate;
                List<FinanceBooking> lstFinanceBooking = iReportsService.GetFinanceBookingReport(objSearchCriteria);
                this.ResultGridView.DataSource = lstFinanceBooking;
                this.ResultGridView.DataBind();
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.AdminPortal, ex, null);
                throw;
            }
        }

       
    }
}