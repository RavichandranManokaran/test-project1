﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           UCommerceInstaller.cs
/// Sub-system/Module:     ClientSetup(VS project name)
/// Description:           UCommerceInstaller create uCommerce store and related basic details
/// </summary>

#region Using Directives
using Affinion.LoyaltyBuild.Common.Instrumentation;
using System;
using System.Linq;
using UCommerce.EntitiesV2;
using UCommerce.EntitiesV2.Factories;
#endregion

namespace Affinion.LoyaltyBuild.ClientSetup.ClientSetup
{
    public class UCommerceInstaller
    {
        #region Private variables

        private string newCatalogGroupName;
        private string newCatalogName;

        #endregion

        #region Private methods

        /// <summary>
        /// Create Catalog Group
        /// </summary>
        /// <returns>ProductCatalogGroup</returns>
        private ProductCatalogGroup CreateCatalogGroup()
        {
            var group = ProductCatalogGroup.SingleOrDefault(c => c.Name == newCatalogGroupName) ?? new ProductCatalogGroupFactory().NewWithDefaults(newCatalogGroupName);
            group.ProductReviewsRequireApproval = true;
            group.Deleted = false;
            group.CreateCustomersAsMembers = false;
            group.DomainId = null;  
            group.Save();
            //group.OrderNumberSerie = GetDefaultOrderNumberSeries();
            //group.EmailProfile = GetDefaultEmailProfile();
            group.Save();
            return group;
        }

        /// <summary>
        /// Use to create Product Catelog
        /// </summary>
        /// <returns>ProductCatalog</returns>
        private ProductCatalog CreateProductCatalog(ProductCatalogGroup catalogGroup)
        {
            var catalog = catalogGroup.ProductCatalogs.SingleOrDefault(c => c.Name == newCatalogName) ?? new ProductCatalogFactory().NewWithDefaults(catalogGroup, newCatalogName);

            catalog.DisplayOnWebSite = true;
            catalog.Deleted = false;
            catalog.ShowPricesIncludingVAT = true;

            // Versions of CatalogFactory prior to 3.6 did not
            // add catalog to catalog group. Need to do it
            // if not already done to make sure roles and 
            // permissions are created properly.
            if (!catalogGroup.ProductCatalogs.Contains(catalog))
                catalogGroup.ProductCatalogs.Add(catalog);

            catalog.Save();

            var priceGroup = PriceGroup.SingleOrDefault(p => p.Name == "EUR 15 pct");
            if (priceGroup != null)
                catalog.PriceGroup = priceGroup;

            catalog.Save();
            return catalog;
        }

        /// <summary>
        /// Create Catalogue
        /// </summary>
        /// <param name="catalog">ProductCatalog</param>
        private void CreateCatalogue(ProductCatalog catalog)
        {
            CreateHotelOffers(catalog);
        }

        /// <summary>
        /// This function will CreateHotelOffers 
        /// </summary>
        /// <param name="catalog">ProductCatalog</param>
        private void CreateHotelOffers(ProductCatalog catalog)
        {
            var SupplierOffer = CreateCatalogCategory(catalog, string.Concat(newCatalogGroupName, "HotelOffers"), string.Empty);

            var accessoryDefinition = GetProductDefinition(string.Concat(newCatalogGroupName, "HotelOffers"));
            //CreateHotelOffersSubCategory(SupplierOffer, accessoryDefinition);

        }

        /// <summary>
        /// Use to create subcategory under Hotels
        /// </summary>
        /// <param name="SupplierOffer">Category</param>
        /// <param name="shirtDefinition">ProductDefinition</param>
        //private void CreateHotelOffersSubCategory(Category SupplierOffer, ProductDefinition hotelOffers)
        //{
        //    var casual = CreateChildCategory(SupplierOffer, string.Concat(newCatalogGroupName, "Hotels"), string.Empty);
        //}

        /// <summary>
        /// Create Child category for given category
        /// </summary>
        /// <param name="parent">Category category</param>
        /// <param name="name">name of the category</param>
        /// <param name="imageId">imageId</param>
        /// <returns></returns>
        //private static Category CreateChildCategory(Category parent, string name, string imageId)
        //{
        //    var category = CreateCategory(parent.ProductCatalog, name, imageId);
        //    parent.AddCategory(category);
        //    parent.Save();
        //    return category;
        //}


        /// <summary>
        /// Use to Create Category
        /// </summary>
        /// <param name="catalog">ProductCatalog</param>
        /// <param name="name">name of the category</param>
        /// <returns></returns>
        //private static Category CreateCategory(ProductCatalog catalog, string name)
        //{
        //    var definition = Definition.SingleOrDefault(d => d.Name == "Default Category Definition");
        //    var category = Category.SingleOrDefault(c => c.Name == name) ?? new CategoryFactory().NewWithDefaults(catalog, definition, name);
        //    category.DisplayOnSite = true;

        //    category.Save();
        //    return category;
        //}

        /// <summary>
        /// Enable Payment Method For Catalog
        /// </summary>
        /// <param name="catalogGroup"></param>
        private static void EnablePaymentMethodForCatalog(ProductCatalogGroup catalogGroup)
        {
            var paymentMethods = PaymentMethod.All();
            foreach (var method in paymentMethods)
            {
                method.ClearEligibleProductCatalogGroups();
                method.AddEligibleProductCatalogGroup(catalogGroup);
                method.Save();
            }
        }

        /// <summary>
        /// Enable Shipping Method For Catalog
        /// </summary>
        /// <param name="catalogGroup">catalogGroup</param>
        private static void EnableShippingMethodForCatalog(ProductCatalogGroup catalogGroup)
        {
            var shippingMethods = ShippingMethod.All();
            foreach (var method in shippingMethods)
            {
                method.ClearEligibleProductCatalogGroups();
                method.AddEligibleProductCatalogGroup(catalogGroup);
                method.Save();
            }
        }

        /// <summary>
        /// Create Catalog Category
        /// </summary>
        /// <param name="catalog">ProductCatalog</param>
        /// <param name="name">name of the product catelog</param>
        /// <param name="imageId">image id</param>
        /// <returns></returns>
        private static Category CreateCatalogCategory(ProductCatalog catalog, string name, string imageId)
        {
            var category = CreateCategory(catalog, name, imageId);
            catalog.AddCategory(category);
            return category;
        }

        /// <summary>
        /// Create new Category
        /// </summary>
        /// <param name="catalog">ProductCatalog</param>
        /// <param name="name">Catelog name</param>
        /// <param name="imageId">Image id</param>
        /// <returns></returns>
        private static Category CreateCategory(ProductCatalog catalog, string name, string imageId)
        {
            var definition = GetDefaultDefinition();
            var category = Category.SingleOrDefault(c => c.Name == name) ?? new CategoryFactory().NewWithDefaults(catalog, definition, name);
            category.DisplayOnSite = true;

            GenericHelpers.DoForEachCulture(language =>
            {
                if (category.GetDescription(language) == null)
                    category.AddCategoryDescription(new CategoryDescription() { CultureCode = language, DisplayName = name });
            });

            category.ImageMediaId = imageId;
            category.Save();
            return category;
        }

        /// <summary>
        /// Use to retrive default definition
        /// </summary>
        /// <returns></returns>
        private static Definition GetDefaultDefinition()
        {
            var definition = Definition.SingleOrDefault(d => d.Name == "Default Category Definition");
            if (definition == null)
                definition = new Definition { Name = "Default Category Definition", DefinitionType = DefinitionType.Get(1), Deleted = false, Guid = Guid.NewGuid(), SortOrder = 1, Description = "Default Category Definition" };
            definition.Save();
            return definition;
        }


        /// <summary>
        /// Use to retrive ProductDefinition by name
        /// </summary>
        /// <param name="name">Name of the Product definition</param>
        /// <returns></returns>
        private static ProductDefinition GetProductDefinition(string name)
        {
            ProductDefinition definition = null;
            try
            {
                if (name != null)
                {
                    definition = ProductDefinition.SingleOrDefault(d => d.Name == name);
                    
                }
                //if (definition == null)
                //    throw new ArgumentOutOfRangeException(name); 
            }
            catch (Exception e)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, e, null);
                throw;
            }

            return definition;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// This is the class constructor use to construct object
        /// </summary>
        /// <param name="catalogGroupName"> Catalog Group Name</param>
        /// <param name="catalogName">Catalog Name</param>
        public UCommerceInstaller(string catalogGroupName, string catalogName)
        {
            newCatalogGroupName = catalogGroupName;
            newCatalogName = catalogName;
        }

        /// <summary>
        /// Creates related Catgory groups and Category
        /// </summary>
        public void Configure()
        {
            // Call default settings to initalize: Product definition

            var settings = new Settings();
            settings.Configure(newCatalogGroupName);

            var catalogGroup = CreateCatalogGroup();
            var catalog = CreateProductCatalog(catalogGroup);
            EnablePaymentMethodForCatalog(catalogGroup);
            EnableShippingMethodForCatalog(catalogGroup);
            CreateCatalogue(catalog);
        }
        #endregion
    }
}
