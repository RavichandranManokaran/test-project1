﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.Search.Facets;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi.Filter
{
    public interface IHotelFilter
    {
        /// <summary>
        /// Whether to ignore supplier
        /// </summary>
        /// <param name="filterData"></param>
        /// <returns></returns>
        bool IncludeHotel(UCommerce.Documents.Product product, SearchCriteria criteria);

        Facet FilterRecordFacet(SearchCriteria criteria);
    }
}
