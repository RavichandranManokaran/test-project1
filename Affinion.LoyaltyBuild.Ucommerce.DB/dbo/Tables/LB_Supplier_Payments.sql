﻿CREATE TABLE [dbo].[LB_Supplier_Payments](
	[SupplierPaymentId] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [int] NOT NULL,
	[OrderDate] [datetime] NOT NULL,
	[NominalCode] [varchar](50) NULL,
	[CampaignAccountingId] [varchar](50) NULL,
	[OrderCurrencyId] [int] NOT NULL,
	[OrderCurrencyIsoCode] [varchar](5) NOT NULL,
	[EuroRate] [decimal](11, 2) NULL,
	[CustomerId] [int] NOT NULL,
	[SuggestedAmount] [decimal](11, 2) NULL,
	[PostedAmount] [decimal](11, 2) NULL,
	[IsPaidToAccounts] [tinyint] NOT NULL,
	[DatePostedToOracle] [datetime] NULL,
	[OracleId] [varchar](50) NULL,
	[OracleFileId] [int] NULL,
	[SupplierAddressCode] [nvarchar](80) NULL,
	[LineNumber] [int] NULL,
	[CreatedBy] [varchar](80) NULL,
	[UpdatedBy] [varchar](80) NULL,
	[DateCreated] [datetime] NULL,
	[DateUpdated] [datetime] NULL,
	[IsDeleted] [tinyint] NULL,
	[OrderLineId] [int] NULL,
 CONSTRAINT [uCommerce_PK_SupplierPaymentId] PRIMARY KEY CLUSTERED 
(
	[SupplierPaymentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[LB_Supplier_Payments] ADD  CONSTRAINT [DF_LB_Supplier_Payments_IsPaidToAccounts]  DEFAULT ((0)) FOR [IsPaidToAccounts]
GO

ALTER TABLE [dbo].[LB_Supplier_Payments] ADD  CONSTRAINT [DF_uCommerce_LB_Supplier_Payments_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
