﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Model.Provider
{
    /// <summary>
    /// Base Provider interface
    /// </summary>
    public interface IBaseProvider
    {
        /// <summary>
        /// Provider Id
        /// </summary>
        string ProviderId { get; set; }

        /// <summary>
        /// SitecoreItemId
        /// </summary>
        string SitecoreItemId { get; set; }

        /// <summary>
        /// provider type
        /// </summary>
        ProviderType ProviderType { get; set; }
    }
}
