﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.SupplierPortal.Reports.Data
{
    /// <summary>
    /// Restricted Rooms data to get from DB
    /// </summary>
    public class RestrictedRoomsData
    {
        /// <summary>
        /// Hotel id
        /// </summary>
        public int HotelId { get; set; }

        /// <summary>
        /// Hotel/Provider name
        /// </summary>
        public string HotelName { get; set; }

        /// <summary>
        /// Restricted Offers string
        /// </summary>
        public int OfferId { get; set; }

        /// <summary>
        /// Offer name
        /// </summary>
        public string OfferName { get; set; }

        /// <summary>
        /// Date on which the room is closed out or sold out
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Date on which the room is closed out or sold out
        /// </summary>
        public DateTime FirstDate { get; set; }

    }

    /// <summary>
    /// Contains details for restricted rooms report item
    /// </summary>
    public class RestrictedRoomsReport
    {
        /// <summary>
        /// Hotel id
        /// </summary>
        public int HotelId { get; set; }
        
        /// <summary>
        /// Hotel/Provider name
        /// </summary>
        public string HotelName { get; set; }

        /// <summary>
        /// Offer name
        /// </summary>
        public string OfferName { get; set; }

        /// <summary>
        /// Restricted Offers string
        /// </summary>
        public int OfferId { get; set; }

        /// <summary>
        /// Restricted dates DateTime
        /// </summary>
        public DateTime Month { get; set; }

        /// <summary>
        /// Restricted date string
        /// </summary>
        public string DatesRestricted { get; set; }

    }

    /// <summary>
    /// Collection to manage Restricted Rooms
    /// </summary>
    public class RestrictedRoomsCollection : IEnumerable<RestrictedRoomsReport>
    {
        //Private variables declaration
        private List<RestrictedRoomsReport> _items;

        /// <summary>
        /// Constructor
        /// </summary>
        public RestrictedRoomsCollection()
        {
            _items = new List<RestrictedRoomsReport>();
        }

        /// <summary>
        /// Add method to add item
        /// </summary>
        /// <param name="item"></param>
        public void Add(RestrictedRoomsReport item)
        {
            _items.Add(item);
        }

        /// <summary>
        /// Override method IEnumenrable class
        /// </summary>
        /// <returns></returns>
        public IEnumerator<RestrictedRoomsReport> GetEnumerator()
        {
            foreach (var item in _items)
                yield return item;
        }

        /// <summary>
        /// Override method IEnumenrable class
        /// </summary>
        /// <returns></returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            foreach (var item in _items)
                yield return item;
        }
    }
}
