﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DailyOracleFinanceReport.ascx.cs" Inherits="Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.Reports.DailyOracleFinanceReport" %>

<div class="content">
    <div class="content-body">
        <div id="errMsg" runat="server" class="col-sm-12 alert alert-danger" style="display: none">
        </div>
        <div>
            <div class="row">
                <div class="col-sm-12 marT20">
                    <div class="accordian-outer">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading bg-primary claim_titl">
                                    <h1 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="show-hide-list"><sc:Text id="scOracleReport" runat="server"/></a>
                                    </h1>
                                </div>
                                <div id="collapse1" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div>
                                            <div class="content_bg_inner_box summary-detail receipt_body">
                                                <div class="col-xs-12 col-md-12">
                                                    <h2 id="parameters" runat="server" class="no-margin accent54-f"><sc:Text id="scParmeters" runat="server"/></h2>
                                                    <div class="mybooking" style="text-align:center">
                                                        <div class="row margin-row">
                                                            <div class="col-sm-6">
                                                                <div class="form-group pull-right">
                                                                    <div class="input-group date">
                                                                        <input type="text" id="CurrentDate" runat="server" clientidmode="Static" class="form-control datepicker width-85 pull-left font-size-12" TabIndex="1" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                               <asp:Button type="button" ID="Search" runat="server" ClientIDMode="Static" Text="Search" OnClick="Search_Click" class="btn btn-info add-break font-size-12 pull-left marR5" TabIndex="2" />
                                                                <asp:Button type="button" ID="Export" runat="server" ClientIDMode="Static" Text="Export" OnClick="Export_Click" class="btn btn-info add-break font-size-12 pull-left marR5" TabIndex="3" />
                                                            </div>
                                                             
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <%--Html Report --%>
            <div class="row" id="htmlReportGrid" runat="server">
                <div class="col-sm-12">
                    <div>
                        <div class="panel panel-default">
                            <div class="panel-heading claim_titl">
                                <h4 class="panel-title"><sc:Text id="scOracleDailyFinance" runat="server"/>
                                </h4>
                            </div>
                            <div  id="totalCount"  runat="server"  class="totalCountpad"><sc:Text id="scOracleDaily" runat="server"/><span> <%= TotalOfRecords %> </span> record(s) found. </div>
                            <div class="panel-body alert-info">
                                <%-- Result Grid--%>
                                <div runat="server" id="ResultDiv" clientidmode="Static">
                                    <div id="data-grid" class="table-responsive">
                                         <asp:GridView ID="ResultGridView" runat="server" Width="100%" SelectedRowStyle-Font-Bold="true" CssClass="table tbl-calendar" AutoGenerateColumns="False" AllowPaging="true" PageSize="10" OnPageIndexChanging="ResultGridView_PageIndexChanging"
                                             PagerSettings-NextPageText=" Next " PagerSettings-PreviousPageText=" Previous " PagerSettings-Mode="NumericFirstLast" PagerSettings-FirstPageText=" First " PagerSettings-LastPageText=" Last " PagerSettings-Position="Bottom" PagerStyle-Font-Bold="true" PagerStyle-CssClass="pagination-grid" OnRowDataBound="ResultGridView_RowDataBound">
                                            <Columns>
                                                <asp:TemplateField HeaderText="CURRENCYID" HeaderStyle-Width="5%">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtBookings" runat="server" Text='<%# Bind("Currency") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBookings" runat="server" Text='<%# Bind("Currency") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="COMPANY" HeaderStyle-Width="5%">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtBookingMethodID" runat="server" Text='<%# Bind("Company") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBookingMethodID" runat="server" Text='<%# Bind("Company") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="MERCHANTIDENTIFICATION" HeaderStyle-Width="5%">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtBookingMethod" runat="server" Text='<%# Bind("MerchantIdentification") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBookingMethod" runat="server" Text='<%# Bind("MerchantIdentification") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="PARTNER" HeaderStyle-Width="5%">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtPartnerName" runat="server" Text='<%# Bind("Partner") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPartnerName" runat="server" Text='<%# Bind("Partner") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="DEPARTMENT" HeaderStyle-Width="5%">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtDepartment" runat="server" Text='<%# Bind("Department") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDepartment" runat="server" Text='<%# Bind("Department") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="PRODUCT" HeaderStyle-Width="5%">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtProduct" runat="server" Text='<%# Bind("Product") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblProduct" runat="server" Text='<%# Bind("Product") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="GLCODE" HeaderStyle-Width="5%">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtGlCode" runat="server" Text='<%# Bind("GlCode") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblGlCode" runat="server" Text='<%# Bind("GlCode") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="CLIENT" HeaderStyle-Width="5%">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtClient" runat="server" Text='<%# Bind("Client") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblClient" runat="server" Text='<%# Bind("Client") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="PROJECT" HeaderStyle-Width="5%">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtProject" runat="server" Text='<%# Bind("Project") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblProject" runat="server" Text='<%# Bind("Project") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="SPARE" HeaderStyle-Width="5%">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtSpare" runat="server" Text='<%# Bind("Spare") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSpare" runat="server" Text='<%# Bind("Spare") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="AMOUNTDEBIT" HeaderStyle-Width="5%">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtAmountDebit" runat="server" Text='<%# Bind("AmountDebit") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAmountDebit" runat="server" Text='<%# Bind("AmountDebit") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="AMOUNTCREDIT" HeaderStyle-Width="5%">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtAmountCredit" runat="server" Text='<%# Bind("AmountCredit") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAmountCredit" runat="server" Text='<%# Bind("AmountCredit") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="DESCRIPTION" HeaderStyle-Width="15%">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtDescription" runat="server" Text='<%# Bind("Description") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>

                                            <EmptyDataTemplate>
                                                <sc:Text id="scNoRecords" field="DisplayNo Records" runat="server"/>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {

        jQuery("#CurrentDate").datepicker({
            defaultDate: new Date(),
            dateFormat: "<%=Affinion.LoyaltyBuild.Common.Utilities.DateTimeHelper.MonthDateFormat%>",
            minDate: -180,
            maxDate: 0,
            numberOfMonths: 1,
            showOn: "both",
            buttonImage: "images/calendar.png",
            buttonImageOnly: false,
            buttonClass: "glyphicon-calendar"
        }).next(".ui-datepicker-trigger").addClass("btn btn-default glyphicon glyphicon-calendar calander-icon pull-left");

        Validation();
    });

    $("#CurrentDate").change(function () {
        Validation();
    });

    function Validation() {
        var isEnabled = false;
        var startDate = $("#CurrentDate").val();
        if (startDate != "")
            isEnabled = true;
        else
            isEnabled = false;

        if (isEnabled) {
            $("#Search").removeAttr('disabled');
        }
        else {
            $("#Search").attr('disabled', 'disabled');
            $("#Export").attr('disabled', 'disabled');
        }
    }
    
</script>
