﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Api.Booking.Extension
{
    /// <summary>
    /// Generic extension methods
    /// </summary>
    public static class GenericExtensionMethods
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="reader"></param>
        /// <param name="columnName"></param>
        /// <returns></returns>
        public static Nullable<T> NullValue<T>(this IDataReader reader, string columnName)
            where T : struct
        {
            if (reader[columnName] == DBNull.Value)
                return null;

            var t = typeof(T);
            t = Nullable.GetUnderlyingType(t) ?? t;

            return (T)Convert.ChangeType(reader[columnName], t);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="reader"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        public static Nullable<T> NullValue<T>(this IDataReader reader, int column)
            where T : struct
        {
            if (reader[column] == DBNull.Value)
                return null;

            var t = typeof(T);
            t = Nullable.GetUnderlyingType(t) ?? t;

            return (T)Convert.ChangeType(reader[column], t);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="reader"></param>
        /// <param name="columnName"></param>
        /// <returns></returns>
        public static T Value<T>(this IDataReader reader, string columnName)
        {
            var t = typeof(T);
            t = Nullable.GetUnderlyingType(t) ?? t;

            if (reader[columnName] == DBNull.Value)
                return default(T);

            return (T)Convert.ChangeType(reader[columnName], t);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="reader"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        public static T Value<T>(this IDataReader reader, int column)
        {
            var t = typeof(T);
            t = Nullable.GetUnderlyingType(t) ?? t;

            if (reader[column] == DBNull.Value)
                return default(T);

            return (T)Convert.ChangeType(reader[column], t);
        }
    }
}
