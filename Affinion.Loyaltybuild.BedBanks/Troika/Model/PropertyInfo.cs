﻿using System;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Message;

namespace Affinion.LoyaltyBuild.BedBanks.Troika.Model
{
    [Serializable]
    public class PropertyInfo : JtResponse
    {
        public int BaseCurrencyId{get;set;}
        public string CampaignProviderLingualText { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public string LanguageID { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string MultiLineAddress { get; set; }
        public string Phone { get; set; }
        public string PictureFileName { get; set; }
        public string ProviderId { get; set; }
        public string ProviderName { get; set; }
        public string StarRanking { get; set; }
        public string WebInformation { get; set; }
        public string WebsiteUrl { get; set; }
        public string PropertyReferenceID { get; set; }

        public long ResponseTime { get; set; }
    }
}
