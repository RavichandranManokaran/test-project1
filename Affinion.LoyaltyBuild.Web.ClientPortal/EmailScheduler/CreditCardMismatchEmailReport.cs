﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data;
using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Data.Items;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports;
using System.Text;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common;
using System.Data;
using Affinion.LoyaltyBuild.Web.ClientPortal.Models;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.EmailScheduler
{
    public class CreditCardMismatchEmailReport
    {
        //private variable declarations
        private Item _mailItem;
        private string _hotalName = string.Empty;
        private string _toEmail = string.Empty;
        private Database _currentDB;
        private string _mailContentPath = "/sitecore/content/admin-portal/global/_supporting-content/creditcardmismatchemailreport";
        public void Run(Sitecore.Data.Items.Item[] items, Sitecore.Tasks.CommandItem command, Sitecore.Tasks.ScheduleItem schedule)
        {
            if (schedule.LastRun.Date != DateTime.Now.Date)
            {
                ExecuteJob();
            }
        }

        /// <summary>
        /// ExecuteJob method for get all provider details 
        /// </summary>
        public void ExecuteJob()
        {
            try
            {
                _currentDB = Sitecore.Configuration.Factory.GetDatabase("web");
                _mailItem = _currentDB.GetItem(_mailContentPath);
                string filepath = _mailItem.Fields["FilePath"].Value;
                Diagnostics.Trace(DiagnosticsCategory.ExternalServices, "CreditCardMismatchEmailReport:ExecuteJob() method started  " + DateTime.Now);
                MisMatchData mmd = new MisMatchData();
                mmd.ReadFile(filepath);
                DataTable dt = mmd.CompareData();

                if (dt.Rows.Count > 0)
                {
                    StringBuilder htmlTable = new StringBuilder();
                    htmlTable.Append("<table class='table table-striped table-bordered' cellpadding='0' cellspacing='0' style='width:100%'>");
                    htmlTable.Append("<thead>");
                    htmlTable.Append("<tr style='padding:5px 10px;font-size:12px;border-bottom:1px solid transparent;border-top-right-radius:3px;border-top-left-radius:3px;background:#026199;color:#ffffff;'>");
                    foreach (DataColumn c in dt.Columns)
                    {
                        htmlTable.Append("<th style='padding:5px;border:1px solid rgba(3,129,203,0.1);border-top:0;line-height:1.4;background:#026199;color:#ffffff;'>");
                        htmlTable.Append(c.ColumnName);
                        htmlTable.Append("</th>");
                    }
                    htmlTable.Append("</tr>");
                    htmlTable.Append("</thead>");
                    htmlTable.Append("<tbody>");
                    foreach (DataRow r in dt.Rows)
                    {
                        htmlTable.Append("<tr style='background-color:#f9f9f9;'>");
                        foreach (DataColumn c in dt.Columns)
                        {
                            htmlTable.Append("<td style='padding:5px;border:1px solid rgba(3,129,203,0.1);line-height:1.4;vertical-align:top;color:#0371b2;font-size:11px;'>");
                            htmlTable.Append(r[c.ColumnName]);
                            htmlTable.Append("</td>");
                        }
                        htmlTable.Append("</tr>");
                    }
                    htmlTable.Append("</tbody>");
                    htmlTable.Append("</table>");

                    if (htmlTable != null)
                        PrepareMail(string.Format(htmlTable.ToString()));
                }
            }
            catch (Exception exception)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("CreditCardMismatchEmailReport: Exception-{0}", exception.ToString()));
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
            }
            Diagnostics.Trace(DiagnosticsCategory.ExternalServices, "CreditCardMismatchEmailReport:ExecuteJob() method ended  " + DateTime.Now);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="table"></param>
        private void PrepareMail(string table)
        {
            try
            {
                string toaddress = _mailItem.Fields["ToAddress"].Value;
                string fromaddress = _mailItem.Fields["FromAddress"].Value;
                string mailBody = _mailItem.Fields["EmailBody"].Value;
                mailBody = mailBody.Replace("{Table}", !string.IsNullOrWhiteSpace(table) ? table : string.Empty);
                string subject = _mailItem.Fields["Subject"].Value;
                string today = DateTime.Now.ToString("dd/MM/yyyy");
                subject = subject.Replace("{Date}", !string.IsNullOrWhiteSpace(today) ? today : string.Empty);
                //Send(string from, string to, string subject, string message, bool isHtml);
                Affinion.LoyaltyBuild.Communications.IEmailService emailService = new Affinion.LoyaltyBuild.Communications.Services.EmailService();
                emailService.Send(fromaddress, toaddress, subject, mailBody, true);

            }
            catch (Exception exception)
            {

                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("CreditCardMismatchEmailReport: Exception-{0}", exception.ToString()));
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
            }



        }     
    }
}