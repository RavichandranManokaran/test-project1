
CREATE TABLE [dbo].[uCommerce_SeasonFixPriceAward](
	[SeasonFixPriceAwardId] [int] NOT NULL,
	[Content] [varchar](50) NULL,
 CONSTRAINT [PK_uCommerce_SeasonFixPriceAward] PRIMARY KEY CLUSTERED 
(
	[SeasonFixPriceAwardId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[uCommerce_SeasonFixPriceAward]  WITH CHECK ADD  CONSTRAINT [FK_uCommerce_SeasonFixPriceAward_uCommerce_Award] FOREIGN KEY([SeasonFixPriceAwardId])
REFERENCES [dbo].[uCommerce_Award] ([AwardId])
GO

ALTER TABLE [dbo].[uCommerce_SeasonFixPriceAward] CHECK CONSTRAINT [FK_uCommerce_SeasonFixPriceAward_uCommerce_Award]
GO


