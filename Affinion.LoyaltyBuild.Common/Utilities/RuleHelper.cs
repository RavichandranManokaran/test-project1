﻿using Affinion.LoyaltyBuild.Common.Exceptions;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Common.Utilities
{
    public class RuleHelper
    {

        /// <summary>
        /// If the rule is selected in client level rules multilist, it will considered as turned on
        /// </summary>
        /// <param name="ruleName">The boolean rule name to check</param>
        /// <returns>Returns true if the rule is selected</returns>
        public static bool GetBooleanClientRuleOfflinePortal(string ruleName, Item rootItem)
        {
            Item ruleItem = GetClientRuleForOfflinePortal(ruleName, rootItem);
            return ruleItem != null;
        }

        /// <summary>
        /// Get the client rule item by its rule name
        /// </summary>
        /// <param name="ruleName">The rule name as defined at the admin portal global rules folder</param>
        /// <returns>Returns the rule item</returns>
        public static Item GetClientRuleForOfflinePortal(string ruleName, Item clientItem)
        {
            try
            {
                var ruleItems = SitecoreFieldsHelper.GetMutiListItems(clientItem, "Client Level Rules").Where(i => SitecoreFieldsHelper.GetDropLinkFieldValue(i, "RuleName", "RuleName").ToLowerInvariant() == ruleName.ToLowerInvariant());

                if (ruleItems.Any())
                {
                    return ruleItems.FirstOrDefault();
                }

                return null;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, new AffinionException(ruleName + " - exception occurred.", ex), null);
                return null;
            }
        }
    }
}
