﻿using Affinion.Loyaltybuild.BusinessLogic.Helper;
using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.SessionHelper;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
using Affinion.LoyaltyBuild.Search.Helpers;
using Sitecore.Data;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using UCommerce.Api;
using UCommerce.EntitiesV2;
using bookinghelper = Affinion.LoyaltyBuild.Api.Booking.Helper;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts
{
    public partial class OfflinePortalCustomerDetails : System.Web.UI.UserControl
    {

        private string SelectText = "Please Select";
        // private string isValidCustomer = String.Empty;
        static int customerId;

        /// <summary>
        /// page load even
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //SetCheckBoxValues();
            try
            {
                if (UCommerce.Api.TransactionLibrary.HasBasket())
                {
                    Sitecore.Events.Event.Subscribe("lb:PartnerValidationSuccess", ClientValidationSuccess);
                    Sitecore.Events.Event.Subscribe("lb:Subscribtion", Subscribtion);

                    InitializeFields();
                }
            }
            catch (Exception exception)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, exception.Message);
                throw;
            }
        }

        /// <summary>
        /// load customer search
        /// </summary>
        /// <param name="confirmCustomerEmail"></param>
        private void LoadCustomerSearch(string confirmCustomerEmail)
        {
            string customerEmail = confirmCustomerEmail;
            InitPage();
            if (!string.IsNullOrWhiteSpace(customerEmail))
            {
                Email.Text = customerEmail;
                Email.ReadOnly = true;
                string clientId = Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.GetBasket().Bookings.FirstOrDefault().ClientId;
                string clientID = Guid.Parse(clientId.ToString()).ToString();
                LoadStore();
                if (!string.IsNullOrWhiteSpace(clientID))
                {
                    ClientDropDown.SelectedIndex = ClientDropDown.Items.IndexOf(ClientDropDown.Items.FindByValue(clientID.ToLower()));
                    LoadStore();
                    // ClientDropDown.Items.FindByValue(clientId).Selected = true;
                    ClientDropDown.Enabled = false;
                }
                var customerDetails = uCommerceConnectionFactory.GetCustomer(clientId, customerEmail);
                if (customerDetails != null && customerDetails.CustomerId != 0)
                {
                    this.LoadCustomerDetailes(customerDetails);
                    customerId = customerDetails.CustomerId;
                }
            }


        }

        /// <summary>
        /// Initialize sitecore fields
        /// </summary>
        private void InitializeFields()
        {
            Item item = Sitecore.Context.Database.GetItem("/sitecore/content/offline-portal/home/customersearch");
            if (item != null)
            {
                //this.Title.Item = item;
                this.ManageTitle.Item = item;
                this.Client.Item = item;
                this.CustomerTitle.Item = item;
                this.OfflineFirstName.Item = item;
                this.OfflineLastName.Item = item;
                this.OfflineAddressLine1.Item = item;
                this.OfflineAddressLine2.Item = item;
                this.OfflineAddressLine3.Item = item;
                this.Active.Item = item;
                this.Country.Item = item;
                this.Location.Item = item;
                this.Country.Item = item;
                this.OfflinePrimaryPhone.Item = item;
                this.OfflineSecondaryPhone.Item = item;
                this.Store.Item = item;
                this.OfflineEmail.Item = item;
                this.Language.Item = item;
                this.OfflineSubscribe.Item = item;
                this.ClientSubscribe.Item = item;
                this.EmailSubscription.Item = item;
                this.SubscribeTitle.Item = item;

            }
            if (!Page.IsPostBack)
            {
                //Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.GetBasket().Bookings.FirstOrDefault().

                //var orderID = BasketHelper.GetBasketOrderId();

                var clientID = Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.GetBasket().Bookings.FirstOrDefault().ClientId;
                if (!String.IsNullOrWhiteSpace(clientID))
                {
                    Item rootItem = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(clientID));

                    if ((rootItem != null && !RuleHelper.GetBooleanClientRuleOfflinePortal("Do you require Partner Validation for Offline", rootItem)) || IsValidated)
                    {
                        this.CustomerValidationSection.Visible = false;
                        this.CustomerPersonalDetails.Visible = true;
                        var customerReference = UCommerce.Api.TransactionLibrary.GetBasket(false).PurchaseOrder["CustomerReference"];
                        LoadCustomerSearch(customerReference);
                    }
                    else
                    {
                        this.CustomerPersonalDetails.Visible = false;
                        this.CustomerValidationSection.Visible = true;
                    }
                }
                Search.Visible = false;
                ContinueButton.Enabled = true;
            }
        }

        /// <summary>
        /// Search Click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Search_Click(object sender, EventArgs e)
        {
            successmessage.Visible = false;

            if (Page.IsValid)
            {

                //id column 
                ResultGridView.Columns[0].Visible = true;

                DataSet result = GetCustomerDetailes();

                ResultGridView.AutoGenerateColumns = false;

                ResultGridView.DataSource = result;
                ResultGridView.DataBind();
                ResultGridView.SelectedIndex = -1;

                //hide id column 
                ResultGridView.Columns[0].Visible = false;

                if (result.Tables[0].Rows.Count > 0)
                {
                    ResultDiv.Visible = true;
                    errormessage.Visible = false;

                    //Focus Search Result Grid
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "CallFocusSearchResultGrid", "FocusSearchResultGrid()", true);
                }
                else
                {
                    ErrorMessageLiteral.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "NoResultsFound", "No Results Found");
                    errormessage.Visible = true;
                    ResultDiv.Visible = false;
                }
            }
        }

        /// <summary>
        /// ResultGridView SelectedIndexChanged event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ResultGridView_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Get the currently selected row using the SelectedRow property.
            GridViewRow row = ResultGridView.SelectedRow;

            customerId = 0;

            if (int.TryParse(row.Cells[0].Text, out customerId))
            {
                LoadCustomerDetailes(customerId);
                AddAndSave.Enabled = false;
                EditAndSave.Enabled = true;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "CallExpanEditForm", "ExpanEditForm()", true);
            }
            // PanelPayment.Visible = true;
        }

        /// <summary>
        /// Reset Click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Reset_Click(object sender, EventArgs e)
        {
            ResetEditForm(true);
        }

        /// <summary>
        /// Add And Save Click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddAndSave_Click(object sender, EventArgs e)
        {
            errormessage.Visible = false;
            ResultDiv.Visible = false;

            if (Page.IsValid)
            {
                this.RecordCustomer();
            }
            //PaymentSection.Visible = true;
            Search.Enabled = true;
        }

        /// <summary>
        /// ResultGridView Row Editing event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ResultGridView_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        /// <summary>
        /// Edit And Save Click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditAndSave_Click(object sender, EventArgs e)
        {
            errormessage.Visible = false;
            ResultDiv.Visible = false;

            if (Page.IsValid)
            {
                this.RecordCustomer(true);
            }
            Search.Enabled = true;
            //recently added by kumaran
            AddAndSave.Enabled = true;
        }

        /// <summary>
        /// Subscribe Click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Subscribe_Click(object sender, EventArgs e)
        {
            string message = string.Empty;

            if (Subscribe(DropDownClientListSubscribe.SelectedValue, EmailSubscribe.Text.Trim(), ref message))
            {
                successmessage.Visible = true;
                errormessage.Visible = false;
                //ResetEditForm(true);
            }
            else
            {
                //if email exist
                if (string.IsNullOrEmpty(message))
                {
                    SubscribeMessage.Text = ErrorMessageLiteral.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Failed", "Operation Failed");
                }
                else
                {
                    SubscribeMessage.Text = ErrorMessageLiteral.Text = message;
                }

                successmessage.Visible = false;
                errormessage.Visible = true;

            }
        }

        /// <summary>
        /// Subscribe Reset Click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SubscribeReset_Click(object sender, EventArgs e)
        {
            ResetSubscribeForm(true);
        }

        /// <summary>
        /// Email TextChanged event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Email_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Email.Text) && !string.IsNullOrWhiteSpace(Email.Text))
            {
                // SubscribeCheckBox.Enabled = true;
            }

            ValidateFields();
        }

        /// <summary>
        /// ClientDropDown SelectedIndexChanged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ClientDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ClientDropDown.SelectedIndex != 0)
            {
                LoadStore();

                ValidateFields();
            }
        }

        private void LoadStore()
        {
            StoreDropDown.Enabled = true;
            Sitecore.Data.Database currentDB = Sitecore.Context.Database;
            Item clientItem = currentDB.GetItem(ClientDropDown.SelectedValue);

            if (clientItem != null)
            {
                this.LoadClientSpecificDropDown("Branches", "BranchLocationName", "BranchLocationName", StoreDropDown, clientItem.Name);
            }
        }

        /// <summary>
        /// UnSubscribe Click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void UnSubscribe_Click(object sender, EventArgs e)
        {
            string message = string.Empty;

            if (Unsubscribe(DropDownClientListSubscribe.SelectedValue, EmailSubscribe.Text.Trim(), ref message))
            {
                successmessage.Visible = true;
                errormessage.Visible = false;
            }
            else
            {
                //if email exist
                if (string.IsNullOrEmpty(message))
                {
                    SubscribeMessage.Text = ErrorMessageLiteral.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Failed", "Operation Failed");
                }
                else
                {
                    SubscribeMessage.Text = ErrorMessageLiteral.Text = message;
                }

                successmessage.Visible = false;
                errormessage.Visible = true;

            }

        }

        /// <summary>
        /// page unload event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Unload(object sender, EventArgs e)
        {
            Sitecore.Events.Event.Unsubscribe("lb:PartnerValidationSuccess", ClientValidationSuccess);
            Sitecore.Events.Event.Unsubscribe("lb:Subscribtion", Subscribtion);
            Sitecore.Events.Event.Unsubscribe("lb:CustomerMappingSuccess", CustomerMappingSuccess);
        }

        /// <summary>
        /// Event raised on client validation success
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CustomerMappingSuccess(object sender, EventArgs e)
        {
            CustomerPersonalDetails.Visible = true;
        }

        /// <summary>
        /// Continue Button Click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ContinueButton_Click(object sender, EventArgs e)
        {
            UpdateOrderLineStatus();
            Sitecore.Events.Event.RaiseEvent("lb:CustomerMappingSuccess", "true");
           CustomerPersonalDetails.Visible = true;
           CustomerValidationSection.Visible = true;
           
            customeraddition.Attributes.Remove("in");
            hdfFlag.Value = "1";
            //PaymentSection.Visible = true;
        }
        #region Private Methods
        /// <summary>
        /// initialize the page
        /// </summary>
        private void InitPage()
        {
            try
            {
                //Load Drop downs
                this.LoadDropDowns();

                //Load Regular Expression Validate
                this.LoadSitecoreFieldData();

                //Bind Table Headers
                this.BindGridViewHeaders(ResultGridView);

            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Bind Table Header
        /// </summary>
        private void BindGridViewHeaders(GridView gridView)
        {

            string fullname = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "FullName", "Full Name");
            string fullAddress = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "FullAddress", "Full Address");
            string emailAddress = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "EmailAddress", "Email Address");
            string mobilePhoneNumber = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "MobilePhoneNumber", "Mobile Phone Number");

            BindBoundField("CustomerId", "CustomerId", gridView);
            BindBoundField(fullname, "FullName", gridView);
            BindBoundField(fullAddress, "FullAddress", gridView);
            BindBoundField(emailAddress, "EmailAddress", gridView);
            BindBoundField(mobilePhoneNumber, "MobilePhoneNumber", gridView);

            BindCommandField(gridView);

        }

        /// <summary>
        /// Bind Bound Field to grid
        /// </summary>
        /// <param name="headerText">Header Text</param>
        /// <param name="dataField"><Data Field/param>
        /// <param name="grid">Grid</param>
        private void BindBoundField(string headerText, string dataField, GridView grid)
        {
            BoundField boundField = new BoundField();
            boundField.HeaderText = headerText;
            boundField.DataField = dataField;
            grid.Columns.Add(boundField);
        }

        /// <summary>
        /// Bind Command Field
        /// </summary>
        /// <param name="grid">Grid</param>
        private void BindCommandField(GridView grid)
        {
            CommandField commandField = new CommandField();
            commandField.ShowSelectButton = true;
            commandField.SelectText = "<span class=\"btn btn-default add-break pull-right font-size-12\">Select Customer</span>";
            grid.Columns.Add(commandField);

        }

        /// <summary>
        /// Load Drop Downs
        /// </summary>
        private void LoadDropDowns()
        {
            try
            {
                this.LoadCountryDropDown();
                this.LoadLocationDropDown();
                this.LoadTitles();
                this.LoadClientDropDown();
                //this.LoadClientDropDownSubscription();
                this.LoadLanguageDropDown();
                // this.LoadStore();
                //this.LoadStoreDropDown();
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }

        }

        /// <summary>
        /// LoadRegular Expression Validate
        /// </summary>
        private void LoadSitecoreFieldData()
        {
            try
            {
                this.Search.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Search", "Search");
                //this.SearchReset.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Reset", "Reset");
                this.AddAndSave.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "AddAndSave", "Add & Save");
                this.EditAndSave.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "EditAndSave", "Edit Details");
                this.Reset.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Reset", "Reset");
                this.SuccessLiteral.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Success", "Operation Succeeded");
                this.SelectText = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "PleaseSelectText", "Please Select");
                this.ButtonSubscribe.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Subscribe", "Subscribe");
                this.UnSubscribe.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Unsubscribe", "Unsubscribe");
                this.SubscribeReset.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Reset", "Reset");

                string invalidPhoneNumberText = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "InvalidPhoneNumber", "Invalid Phone Number");

                this.RegularExpressionValidatorPhone.ValidationExpression = Constants.PhoneNumberValidationRegex;
                this.RegularExpressionValidatorSecondaryPhone.ValidationExpression = Constants.PhoneNumberValidationRegex;
                this.RegularExpressionValidatorEmail.ValidationExpression = Constants.EmailValidationRegex;
                this.RegularExpressionValidatorEmailSubscription.ValidationExpression = Constants.EmailValidationRegex;

                this.RegularExpressionValidatorPhone.ErrorMessage = invalidPhoneNumberText;
                this.RegularExpressionValidatorSecondaryPhone.ErrorMessage = invalidPhoneNumberText;
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }

        }

        /// <summary>
        /// Fire validate
        /// </summary>
        private void ValidateFields()
        {
            this.RegularExpressionValidatorPhone.Validate();
            this.RegularExpressionValidatorSecondaryPhone.Validate();
            this.RegularExpressionValidatorEmail.Validate();
            this.RegularExpressionValidatorEmailSubscription.Validate();
            this.RegularExpressionValidatorPhone.Validate();
            this.RegularExpressionValidatorSecondaryPhone.Validate();
        }
        /// <summary>
        /// Unsubscribe
        /// </summary>
        /// <param name="email">email</param>
        /// <param name="clientItem">Item</param>
        /// <param name="path">Item path</param>
        /// <param name="massage">message</param>
        /// <returns></returns>
        private bool Unsubscribe(string clientId, string email, ref string massage)
        {
            try
            {
                if (!string.IsNullOrEmpty(clientId) && !string.IsNullOrEmpty(email))
                {
                    Item clientSetupItem = Sitecore.Context.Database.GetItem(clientId);
                    Item clientItem = Sitecore.Context.Database.GetItem(@"/sitecore/content/client-portal/" + clientSetupItem.Name);

                    return SubscribeHelper.DeleteSubscription(email, clientItem, Constants.FolderName);
                }
                return false;
            }
            catch (ArgumentException ex)
            {
                massage = ex.Message;
                return false;
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Get Customer details by id
        /// </summary>
        /// <param name="customerId">customer Id</param>
        private void LoadCustomerDetailes(int customerId)
        {
            try
            {
                DataSet customerDataSet = this.GetCustomerDetailesById(customerId);

                var dataRow = customerDataSet.Tables[0].Rows[0];
                bool active = false;
                bool subscribe = false;

                CustomerId.Value = dataRow["CustomerId"].ToString();
                FirstName.Text = dataRow["FirstName"].ToString();
                LastName.Text = dataRow["LastName"].ToString();
                AddressLine1.Text = dataRow["AddressLine1"].ToString();
                AddressLine2.Text = dataRow["AddressLine2"].ToString();
                AddressLine3.Text = dataRow["AddressLine3"].ToString();
                PrimaryPhone.Text = dataRow["MobilePhoneNumber"].ToString();
                SecondaryPhone.Text = dataRow["PhoneNumber"].ToString();
                if (dataRow["LanguageName"] != null)
                    LanguageDropDown.SelectedIndex = LanguageDropDown.Items.IndexOf(LanguageDropDown.Items.FindByValue(dataRow["LanguageName"].ToString()));

                Email.Text = dataRow["EmailAddress"].ToString();

                TitleDropDown.SelectedIndex = TitleDropDown.Items.IndexOf(TitleDropDown.Items.FindByValue(dataRow["Title"].ToString()));
                CountryDropDown.SelectedIndex = CountryDropDown.Items.IndexOf(CountryDropDown.Items.FindByValue(dataRow["CountryId"].ToString()));
                LocationDropDown.SelectedIndex = LocationDropDown.Items.IndexOf(LocationDropDown.Items.FindByValue(dataRow["LocationId"].ToString()));
                ClientDropDown.SelectedIndex = ClientDropDown.Items.IndexOf(ClientDropDown.Items.FindByValue(dataRow["ClientId"].ToString()));

                if (!string.IsNullOrEmpty(dataRow["BranchId"].ToString()))
                {
                    StoreDropDown.Enabled = true;
                    StoreDropDown.SelectedIndex = StoreDropDown.Items.IndexOf(StoreDropDown.Items.FindByValue(dataRow["BranchId"].ToString()));
                }

                if (bool.TryParse(dataRow["Active"].ToString(), out active))
                {
                    ActiveCheckBox.Checked = active;
                }

                if (bool.TryParse(dataRow["Subscribe"].ToString(), out subscribe))
                {
                    SubscribeCheckBox.Checked = subscribe;
                }

                this.ClientDropDown_SelectedIndexChanged(null, null);

                this.Email_TextChanged(null, null);


            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
            }
        }

        /// <summary>
        /// LoadCustomerDetailesByEmail
        /// </summary>
        /// <param name="customerInfo"></param>
        private void LoadCustomerDetailesByEmail(DataSet customerInfo)
        {
            try
            {

                var dataRow = customerInfo.Tables[0].Rows.Count > 0 ? customerInfo.Tables[0].Rows[0] : null;
                bool active = false;
                bool subscribe = false;
                CustomerId.Value = dataRow["CustomerId"].ToString();
                FirstName.Text = dataRow["FirstName"].ToString();
                LastName.Text = dataRow["LastName"].ToString();
                AddressLine1.Text = dataRow["AddressLine1"].ToString();
                AddressLine2.Text = dataRow["AddressLine2"].ToString();
                AddressLine3.Text = dataRow["AddressLine3"].ToString();
                PrimaryPhone.Text = dataRow["MobilePhoneNumber"].ToString();
                SecondaryPhone.Text = dataRow["PhoneNumber"].ToString();

                if (dataRow["LanguageName"] != null)
                    SetLanguageDropDownValue(dataRow["LanguageName"].ToString());

                Email.Text = dataRow["EmailAddress"].ToString();
                TitleDropDown.SelectedIndex = TitleDropDown.Items.IndexOf(TitleDropDown.Items.FindByValue(dataRow["Title"].ToString()));
                CountryDropDown.SelectedIndex = CountryDropDown.Items.IndexOf(CountryDropDown.Items.FindByValue(dataRow["CountryId"].ToString()));
               
                
                LocationDropDown.SelectedIndex = LocationDropDown.Items.IndexOf(LocationDropDown.Items.FindByValue(dataRow["LocationId"].ToString()));
                ClientDropDown.SelectedIndex = ClientDropDown.Items.IndexOf(ClientDropDown.Items.FindByValue(dataRow["ClientId"].ToString()));
                ClientDropDown.Enabled = false;
                if (!string.IsNullOrEmpty(dataRow["BranchId"].ToString()))
                {
                    StoreDropDown.Enabled = true;
                    StoreDropDown.SelectedIndex = StoreDropDown.Items.IndexOf(StoreDropDown.Items.FindByValue(dataRow["BranchId"].ToString()));
                }

                if (bool.TryParse(dataRow["Active"].ToString(), out active))
                {
                    ActiveCheckBox.Checked = active;
                }

                if (bool.TryParse(dataRow["Subscribe"].ToString(), out subscribe))
                {
                    SubscribeCheckBox.Checked = subscribe;
                }
                this.ClientDropDown_SelectedIndexChanged(null, null);
                this.Email_TextChanged(null, null);
                Search.Enabled = false;
                Email.ReadOnly = true;
                AddAndSave.Enabled = false;
                EditAndSave.Enabled = true;
                //  this.PanelPayment.Visible = true;

            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
            }
        }

        /// <summary>
        /// LoadCustomerDetailesByEmail
        /// </summary>
        /// <param name="customerInfo"></param>
        private void LoadCustomerDetailes(Affinion.LoyaltyBuild.DataAccess.Models.Customer customer)
        {
            try
            {
                //UCommerce.EntitiesV2.PurchaseOrder order = UCommerce.Runtime.SiteContext.Current.OrderContext.GetBasket().PurchaseOrder;
                var Customer = UCommerce.EntitiesV2.Customer.Get(Convert.ToInt32(customer.CustomerId));
                if (Customer != null)
                {
                    // var customerDetail = Customer;
                    // var customerAddress = Customer.Addresses.FirstOrDefault();

                    bool active = false;
                    bool subscribe = false;
                    CustomerId.Value = customer.CustomerId.ToString();
                    FirstName.Text = customer.FirstName;
                    LastName.Text = customer.LastName;
                    AddressLine1.Text = customer.AddressLine1;
                    AddressLine2.Text = customer.AddressLine2;
                    AddressLine3.Text = customer.AddressLine3;
                    PrimaryPhone.Text = customer.Phone;
                    SecondaryPhone.Text = customer.MobilePhone;
                    SetLanguageDropDownValue(customer.Language);

                    Email.Text = customer.EmailAddress;

                    TitleDropDown.SelectedIndex = TitleDropDown.Items.IndexOf(TitleDropDown.Items.FindByValue(customer.Title));
                    CountryDropDown.SelectedIndex = CountryDropDown.Items.IndexOf(CountryDropDown.Items.FindByValue(Convert.ToString(customer.CountryId)));
                    LoadLocationDropDown();
                    int locationId = 0;
                    if (int.TryParse(customer.LocationId, out locationId))
                    {
                        LocationDropDown.SelectedIndex = LocationDropDown.Items.IndexOf(LocationDropDown.Items.FindByValue(Convert.ToString(customer.LocationId)));
                                               
                    }
                    else
                    {
                        LocationDropDown.SelectedIndex = LocationDropDown.Items.IndexOf(LocationDropDown.Items.FindByText(Convert.ToString(customer.LocationId)));
                        
                    }
                    
                    //LocationDropDown.SelectedIndex = LocationDropDown.Items.IndexOf(LocationDropDown.Items.FindByValue(Convert.ToString(customer.LocationId)));
                    ClientDropDown.SelectedIndex = ClientDropDown.Items.IndexOf(ClientDropDown.Items.FindByValue(customer.ClientId));

                    ClientDropDown.Enabled = false;
                    if (!string.IsNullOrEmpty(customer.BranchId))
                    {
                        StoreDropDown.Enabled = true;
                        StoreDropDown.SelectedIndex = StoreDropDown.Items.IndexOf(StoreDropDown.Items.FindByValue(customer.BranchId));
                    }

                    if (!string.IsNullOrEmpty(customer.Language))
                        LanguageDropDown.SelectedIndex = LanguageDropDown.Items.IndexOf(LanguageDropDown.Items.FindByValue(customer.Language));

                    if (bool.TryParse(customer.Active.ToString(), out active))
                        ActiveCheckBox.Checked = true;

                    SubscribeCheckBox.Checked = customer.Subscribe == 1 ? true : false;
                    //this.ClientDropDown_SelectedIndexChanged(null, null);
                    //this.Email_TextChanged(null, null);
                    hdfLocation.Value = customer.LocationId.ToString();
                    hdfSubcscribe.Value = customer.SubscribeType.ToString();
                    Search.Enabled = false;
                    Email.ReadOnly = true;
                    AddAndSave.Enabled = false;
                    EditAndSave.Enabled = true;
                    //  this.PanelPayment.Visible = true;
                }
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
            }
        }

        private void LoadLoactions()
        {
            try
            {
                if (CountryDropDown.SelectedIndex <= 0)
                    return;

                var items = Sitecore.Context.Database.GetItem(Constants.Location).GetChildren();
                if (items != null)
                {
                    foreach (Item item in items)
                    {
                        if (SitecoreFieldsHelper.GetDropLinkFieldValue(item, "Country", "CountryName") == CountryDropDown.SelectedItem.Text)
                        {
                            if (SitecoreFieldsHelper.CheckBoxChecked(item, "IsActive"))
                                LocationDropDown.Items.Add(new ListItem { Text = SitecoreFieldsHelper.GetValue(item, "Name"), Value = SitecoreFieldsHelper.GetValue(item, "ID") });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        /// <summary>
        /// SetLanguageDropDownValue
        /// </summary>
        /// <param name="languageCode"></param>
        private void SetLanguageDropDownValue(string languageCode)
        {
            try
            {
                ListItem item = LanguageDropDown.Items.FindByValue(languageCode);
                if (item != null)
                    LanguageDropDown.SelectedIndex = LanguageDropDown.Items.IndexOf(LanguageDropDown.Items.FindByValue(languageCode));
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Populate Location DropDown
        /// </summary>
        private void LoadLocationDropDown()
        {
            try
            {
                Database contextDb = Sitecore.Context.Database;
                var LocationItems = new ListItemCollection();
                dynamic items = contextDb.GetItem(Constants.Location) != null ? contextDb.GetItem(Constants.Location).GetChildren() : null;
                if (items != null)
                {
                    foreach (var item in items)
                    {
                        if (SitecoreFieldsHelper.GetDropLinkFieldValue(item, "Country", "CountryName") == CountryDropDown.SelectedItem.Text)
                        {
                            if (SitecoreFieldsHelper.CheckBoxChecked(item, "IsActive"))
                            {

                                ListItem liBox = new ListItem();
                                liBox.Text = SitecoreFieldsHelper.GetValue(item, "Name");
                                liBox.Value = SitecoreFieldsHelper.GetValue(item, "ID");
                                if (liBox != null)
                                {
                                    LocationItems.Add(liBox);
                                }
                            }

                        }
                    }
                    BindDropdown(LocationDropDown, LocationItems, "Text", "Value");

                    ListItem allLocation = new ListItem("Please Select", "0");
                    LocationDropDown.Items.Insert(0, allLocation);

                }
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }

        }

        /// <summary>
        ///  Populate Country DropDown
        /// </summary>
        private void LoadCountryDropDown()
        {

            CountryDropDown.Items.Clear();
            ListItem allLocation = new ListItem("Please Select", "0");
            LocationDropDown.Items.Insert(0, allLocation);
            ListItem allCountry = new ListItem("Please Select", "0");
            CountryDropDown.Items.Insert(0, allCountry);

            Database contextDb = Sitecore.Context.Database;
            dynamic items = contextDb.GetItem(Constants.Countries) != null ? contextDb.GetItem(Constants.Countries).GetChildren() : null;
            if (items != null)
            {
                foreach (var item in items)
                {
                    ListItem liBox = new ListItem();
                    liBox.Text = SitecoreFieldsHelper.GetValue(item, "CountryName");
                    liBox.Value = SitecoreFieldsHelper.GetValue(item, "UCommerceCountryID");
                    liBox.Attributes.Add("data-itemid", item.ID.ToString());
                    liBox.Attributes.Add("data-culture", SitecoreFieldsHelper.GetValue(item, "Culture"));
                    CountryDropDown.Items.Add(liBox);
                }
            }
        }

        /// <summary>
        /// Load Client Specific Drop Down
        /// </summary>
        private void LoadClientSpecificDropDown(string dropdownFieldName, string targetItemsTextField, string targetItemsValueField, ListControl dropdown, string client)
        {
            try
            {
                //query for the client item in admin portal
                Item clientItem = Sitecore.Context.Database.GetItem("/sitecore/content/admin-portal/client-setup/" + client + "");
                if (clientItem != null)
                {
                    //Bind data to drop-down
                    Collection<Item> items = SitecoreFieldsHelper.GetMutiListItems(clientItem, dropdownFieldName);
                    dropdown.DataSource = SitecoreFieldsHelper.GetDropdownDataSource(items, targetItemsTextField, targetItemsValueField);
                    dropdown.DataTextField = "Text";
                    dropdown.DataValueField = "Value";
                    dropdown.DataBind();
                    //Set the default list item
                    ListItem select = new ListItem(SelectText, "0");
                    dropdown.Items.Insert(0, select);
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }

        }

        /// <summary>
        /// Populate Titles
        /// </summary>
        private void LoadTitles()
        {
            Sitecore.Data.Database currentDb = Sitecore.Context.Database;

            //Load location data to a locationList
            Item titleList = currentDb.GetItem(Constants.TitleListPath);
            TitleDropDown.AppendDataBoundItems = true;
            if (titleList != null)
            {
                var titles = (from loc in titleList.Children
                              orderby loc.DisplayName
                              select loc.DisplayName);

                //Bind locationList data to the drop down
                //TitleDropDown.SelectedIndex = 0;
                TitleDropDown.DataSource = titles != null ? titles.ToList() : null;
                TitleDropDown.DataBind();
            }
            ListItem selectTitle = new ListItem(SelectText, string.Empty);
            TitleDropDown.Items.Insert(0, selectTitle);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private IList<string> GetClientList()
        {
            try
            {
                /// Get Client items
                //Sitecore.Data.Database currentDb = Sitecore.Context.Database;
                var clientItems = SearchHelper.GetUserSpecificClientItems(); //currentDb.SelectItems(ClientListQuery);

                if (clientItems != null)
                {
                    /// Extract Clients Filtered Items
                    var clientIds = (from client in clientItems
                                     orderby client.DisplayName
                                     select client.ID.Guid.ToString());
                    return clientIds != null ? clientIds.ToList() : null;
                }

                return null;
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Subscribe
        /// </summary>
        /// <param name="clientItemId">Client Item Id</param>
        /// <param name="email">Email</param>
        private bool Subscribe(string clientItemId, string email, ref string message)
        {
            try
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "CallExpanSubscribForm", "ExpanSubscribForm()", true);

                if (!string.IsNullOrEmpty(clientItemId))
                {
                    Item clientSetupItem = Sitecore.Context.Database.GetItem(clientItemId);
                    Item clientItem = Sitecore.Context.Database.GetItem(@"/sitecore/content/client-portal/" + clientSetupItem.Name);

                    return SubscribeHelper.CreateSubscription(email, clientItem, Constants.FolderName);

                }
                return false;

            }
            catch (ArgumentException ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                //Email already exist
                message = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "EmailAlreadyExist", "Email Already Exist"); ;

                return false;
            }

            catch (Exception ex)
            {
                //SubscribeMessage.Text = ex.Message;
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;

            }
        }

        /// <summary>
        /// Load Client Drop Down
        /// </summary>
        private void LoadClientDropDown()
        {
            /// Get Client items
            //Sitecore.Data.Database currentDb = Sitecore.Context.Database;
            var clientItems = SearchHelper.GetUserSpecificClientItems();//currentDb.SelectItems(ClientListQuery);

            if (clientItems != null)
            {
                /// Extract Clients Filtered Items
                var clientFilterList = (from client in clientItems
                                        orderby client.DisplayName
                                        select new { DiplayName = client.DisplayName, Id = client.ID.Guid });

                /// Load Client List Drop down
                this.ClientDropDown.DataSource = clientFilterList != null ? clientFilterList.ToList() : null;

                /// Set data text and data value fields
                ClientDropDown.DataTextField = "DiplayName";
                ClientDropDown.DataValueField = "Id";
                this.ClientDropDown.DataBind();

            }

            ListItem selectClient = new ListItem(SelectText, string.Empty);
            ClientDropDown.Items.Insert(0, selectClient);

        }


        /// <summary>
        /// Load Client Drop Down Subscription
        /// </summary>
        private void LoadClientDropDownSubscription()
        {
            /// Get Client items
            //Sitecore.Data.Database currentDb = Sitecore.Context.Database;
            var clientItems = SearchHelper.GetUserSpecificClientItems();//currentDb.SelectItems(ClientListQuery);

            if (clientItems != null)
            {
                /// Extract Clients Filtered Items
                var clientFilterList = (from client in clientItems
                                        orderby client.DisplayName
                                        select new { DiplayName = client.DisplayName, Id = client.ID.Guid });

                /// Load Client List Drop down
                this.DropDownClientListSubscribe.DataSource = clientFilterList != null ? clientFilterList.ToList() : null;

                /// Set data text and data value fields
                DropDownClientListSubscribe.DataTextField = "DiplayName";
                DropDownClientListSubscribe.DataValueField = "Id";

                this.DropDownClientListSubscribe.DataBind();
            }


            ListItem selectClient = new ListItem(SelectText, string.Empty);
            DropDownClientListSubscribe.Items.Insert(0, selectClient);

        }


        /// <summary>
        /// GetCustomerDetailes
        /// </summary>
        /// <returns></returns>
        private DataSet GetCustomerDetailes()
        {
            try
            {
                return uCommerceConnectionFactory.GetCustomerDetailes(CreateCustomerObj(), GetClientIdListString());
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }

        }


        /// <summary>
        /// Convert string list to comma separated string
        /// Prepare string list for SQL IN query
        /// </summary>
        /// <returns>comma separated string id list</returns>
        private string GetClientIdListString()
        {
            var clientList = GetClientList();
            string clientIdList = "{0}";//Dummy

            if (clientList != null)
            {
                //Convert string list to comma separated string as "{6C464DB8},{6451DB8}"
                clientIdList = string.Join(",", clientList);
            }

            return clientIdList;
        }

        /// <summary>
        /// Get Customer Details By Id
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <returns></returns>
        private DataSet GetCustomerDetailesById(int customerId)
        {
            try
            {
                return uCommerceConnectionFactory.GetCustomerDetailesById(customerId);

            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }

        }

        /// <summary>
        /// Write customer records to db
        /// </summary>
        private int RecordCustomer(bool isAnUpdate = false)
        {
            try
            {
                int result;
                bool success = false;
                string message = string.Empty;
                Affinion.LoyaltyBuild.DataAccess.Models.Customer customer = CreateCustomerObj();
                var customerReference = UCommerce.Api.TransactionLibrary.GetBasket(false).PurchaseOrder["CustomerReference"];

                UCommerce.EntitiesV2.PurchaseOrder order = UCommerce.Runtime.SiteContext.Current.OrderContext.GetBasket().PurchaseOrder;
                var objuComCustomer = UCommerce.EntitiesV2.Customer.Find(f => f.EmailAddress == customerReference).FirstOrDefault();

                foreach (var orderline in order.OrderLines)
                {
                    orderline[Affinion.LoyaltyBuild.Api.Booking.Constants.LeadGuestEmail] = customer.EmailAddress;
                }
                order.Save();

                var country = UCommerce.EntitiesV2.Country.Get(Convert.ToInt32(CountryDropDown.SelectedValue));
                if (country == null)
                {
                    country = new Country()
                    {
                        Name = CountryDropDown.SelectedItem.Text,
                        Culture = CountryDropDown.SelectedItem.Attributes["data-culture"] == null ? "en-US" : CountryDropDown.SelectedItem.Attributes["data-culture"]
                    };
                }
                if (string.IsNullOrEmpty(customerReference) || objuComCustomer == null)
                {

                    Address addressInfo = new Address
                    {
                        FirstName = customer.FirstName,
                        LastName = customer.LastName,
                        EmailAddress = customer.EmailAddress,
                        PhoneNumber = customer.Phone,
                        MobilePhoneNumber = customer.MobilePhone,
                        Line1 = string.IsNullOrEmpty(customer.AddressLine1) ? string.Empty : customer.AddressLine1,
                        Line2 = string.IsNullOrEmpty(customer.AddressLine2) ? string.Empty : customer.AddressLine2,
                        City = string.IsNullOrEmpty(customer.AddressLine3) ? string.Empty : customer.AddressLine3,
                        State = hdfLocation.Value,
                        AddressName = "",
                        Attention = "",
                        CompanyName = "",
                        PostalCode = "",
                        Country = country
                    };


                    objuComCustomer = new Customer
                    {
                        FirstName = customer.FirstName,
                        LastName = customer.LastName,
                        EmailAddress = customer.EmailAddress,
                        PhoneNumber = customer.Phone,
                        MobilePhoneNumber = customer.MobilePhone,
                    };



                    objuComCustomer.AddAddress(addressInfo);
                    objuComCustomer.Save();

                    customer.CustomerId = objuComCustomer.CustomerId;

                    result = uCommerceConnectionFactory.AddCustomer(customer);
                }

                else
                {
                    var objOrderAddress = UCommerce.EntitiesV2.OrderAddress.Find(f => f.EmailAddress == customerReference).FirstOrDefault();
                    //var objCustomerAddress = objuComCustomer.Addresses.FirstOrDefault();
                    var objCustomerAddress = objuComCustomer.Addresses.FirstOrDefault(p => p.Customer.CustomerId == customerId);

                    objuComCustomer.FirstName = customer.FirstName;
                    objuComCustomer.LastName = customer.LastName;
                    objuComCustomer.EmailAddress = customer.EmailAddress;
                    objuComCustomer.PhoneNumber = customer.Phone;
                    objuComCustomer.MobilePhoneNumber = customer.MobilePhone;

                    if (objCustomerAddress == null)
                    {
                        objCustomerAddress = new UCommerce.EntitiesV2.Address();
                        objuComCustomer.AddAddress(objCustomerAddress);

                    }
                    objCustomerAddress.FirstName = customer.FirstName;
                    objCustomerAddress.LastName = customer.LastName;
                    objCustomerAddress.EmailAddress = customer.EmailAddress;
                    objCustomerAddress.PhoneNumber = customer.Phone;
                    objCustomerAddress.MobilePhoneNumber = customer.MobilePhone;
                    objCustomerAddress.Line1 = string.IsNullOrEmpty(customer.AddressLine1) ? string.Empty : customer.AddressLine1;
                    objCustomerAddress.Line2 = string.IsNullOrEmpty(customer.AddressLine2) ? string.Empty : customer.AddressLine2;
                    objCustomerAddress.City = string.IsNullOrEmpty(customer.AddressLine3) ? string.Empty : customer.AddressLine3;
                    objCustomerAddress.State = hdfLocation.Value;
                    objCustomerAddress.AddressName = "";
                    objCustomerAddress.Attention = "";
                    objCustomerAddress.CompanyName = "";
                    objCustomerAddress.PostalCode = "";
                    if (country != null)
                        objCustomerAddress.Country = country;


                    //if (objuComCustomer.Addresses.FirstOrDefault(p => p.EmailAddress == customer.EmailAddress) == null)
                    //    objuComCustomer.Addresses.Add(objCustomerAddress);
                    //else
                        //objCustomerAddress.Save();
                    objuComCustomer.Save();
                    customer.CustomerId = objuComCustomer.CustomerId;

                    result = uCommerceConnectionFactory.UpdateCustomer(customer);
                }


                OrderAddress orderAddressInfo = new OrderAddress
                {
                    FirstName = customer.FirstName,
                    LastName = customer.LastName,
                    EmailAddress = customer.EmailAddress,
                    PhoneNumber = customer.Phone,
                    MobilePhoneNumber = customer.MobilePhone,
                    Line1 = string.IsNullOrEmpty(customer.AddressLine1) ? string.Empty : customer.AddressLine1,
                    Line2 = string.IsNullOrEmpty(customer.AddressLine2) ? string.Empty : customer.AddressLine2,
                    City = string.IsNullOrEmpty(customer.AddressLine3) ? string.Empty : customer.AddressLine3,
                    State = hdfLocation.Value,
                    AddressName = "",
                    Attention = "",
                    CompanyName = "",
                    PostalCode = "",
                    Country = country
                };

                order.Customer = objuComCustomer;
                order.AddOrderAddress(orderAddressInfo);
                order.Save();

                if (result > 0)
                {
                    success = SubscriptionMessageHandling(ref message);

                    //Ignore subscription response
                    success = true;
                    customerId = result;
                    ContinueButton.Enabled = true;
                }
                if (success)
                {
                    successmessage.Visible = true;
                    errormessage.Visible = false;
                    // ResetEditForm();
                }
                else
                {
                    ErrorMessageLiteral.Text = message;

                    if (string.IsNullOrEmpty(message))
                    {
                        ErrorMessageLiteral.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Failed", "Operation Failed");
                    }

                    errormessage.Visible = true;
                    successmessage.Visible = false;
                }

                return customerId = objuComCustomer.CustomerId;
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw ex;
            }
        }

        /// <summary>
        /// Subscription Message Handling
        /// </summary>
        /// <param name="message">Message</param>
        /// <returns>True if success</returns>
        private bool SubscriptionMessageHandling(ref string message)
        {
            bool success = false;
            string email = Email.Text;
            string client = ClientDropDown.SelectedValue;

            //Subscription 
            if (!string.IsNullOrEmpty(email))
            {
                if (SubscribeCheckBox.Checked)
                {
                    if (this.Subscribe(client, email.Trim(), ref message))
                    {
                        success = true;
                    }
                    else
                    {
                        success = false;
                    }

                }
                else
                {
                    if (this.Unsubscribe(client, email.Trim(), ref message))
                    {
                        success = true;
                    }
                    else
                    {
                        //Ignore unsubscribe error
                        success = true;
                    }
                }
            }

            return success;
        }

        /// <summary>
        /// Create Customer Obj
        /// </summary>
        /// <returns>Customer</returns>
        private Affinion.LoyaltyBuild.DataAccess.Models.Customer CreateCustomerObj()
        {
            try
            {
                string firstName = FirstName.Text;
                string lastName = LastName.Text;

                int countryId;
                int locationId;

                //id column 
                ResultGridView.Columns[0].Visible = true;

                Affinion.LoyaltyBuild.DataAccess.Models.Customer customer = new Affinion.LoyaltyBuild.DataAccess.Models.Customer();
                customer.CustomerId = string.IsNullOrEmpty(CustomerId.Value) ? 0 : int.Parse(CustomerId.Value);
                customer.Title = TitleDropDown.SelectedValue;
                customer.FirstName = firstName.Length > 1 ? firstName.First().ToString().ToUpper().Trim() + firstName.Substring(1) : firstName.ToUpper().Trim();
                customer.LastName = lastName.Length > 1 ? lastName.First().ToString().ToUpper().Trim() + lastName.Substring(1) : lastName.ToUpper().Trim();
                customer.AddressLine1 = string.IsNullOrEmpty(AddressLine1.Text) ? null : AddressLine1.Text.Trim();
                customer.AddressLine2 = string.IsNullOrEmpty(AddressLine2.Text) ? null : AddressLine2.Text.Trim();
                customer.AddressLine3 = string.IsNullOrEmpty(AddressLine3.Text) ? null : AddressLine3.Text.Trim();
                customer.Phone = string.IsNullOrEmpty(PrimaryPhone.Text) ? null : PrimaryPhone.Text.Trim();
                customer.MobilePhone = string.IsNullOrEmpty(SecondaryPhone.Text) ? null : SecondaryPhone.Text.Trim();
                customer.Language = LanguageDropDown.SelectedValue;
                customer.EmailAddress = string.IsNullOrEmpty(Email.Text) ? null : Email.Text.Trim();
                customer.ClientId = ClientDropDown.SelectedValue;
                customer.BranchId = StoreDropDown.SelectedValue;
                customer.Active = ActiveCheckBox.Checked ? 1 : 0;
                customer.BookingReferenceId = string.Empty;
                customer.Subscribe = SubscribeCheckBox.Checked ? 1 : 0;
                customer.SubscribeType = hdfSubcscribe.Value;

                if (int.TryParse(CountryDropDown.SelectedValue, out countryId))
                {
                    customer.CountryId = countryId;
                }

                //if (int.TryParse(hdfLocation.Value, out locationId))
                //{
                    customer.LocationId = hdfLocation.Value;
                //}

                return customer;
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }



        /// <summary>
        /// Reset Edit form
        /// </summary>
        private void ResetEditForm(bool clearMessages = false)
        {
            FirstName.Text = string.Empty;
            LastName.Text = string.Empty;
            AddressLine1.Text = string.Empty;
            AddressLine2.Text = string.Empty;
            AddressLine3.Text = string.Empty;
            PrimaryPhone.Text = string.Empty;
            SecondaryPhone.Text = string.Empty;
            LanguageDropDown.SelectedIndex = -1;
            Email.Text = string.Empty;
            Email.ReadOnly = false;
            AddAndSave.Enabled = true;
            Search.Enabled = true;
            CountryDropDown.ClearSelection();
            LocationDropDown.ClearSelection();
            TitleDropDown.ClearSelection();
            //  ClientDropDown.ClearSelection();
            this.LoadCountryDropDown();
            this.LoadLocationDropDown();
            this.LoadTitles();
            // this.LoadClientDropDown();
            // this.StoreDropDown.ClearSelection();
            //this.StoreDropDown.Enabled = false;
            //SubscribeCheckBox.Checked = false;
            //SubscribeCheckBox.Enabled = false;
            ActiveCheckBox.Checked = true;
            ActiveCheckBox.Enabled = false;


            EditAndSave.Enabled = false;
            AddAndSave.Enabled = true;

            if (clearMessages)
            {
                successmessage.Visible = false;
                errormessage.Visible = false;
                ResultDiv.Visible = false;
            }

        }

        /// <summary>
        /// ResetSubscribeForm
        /// </summary>
        /// <param name="expanSubscribeForm"></param>
        private void ResetSubscribeForm(bool expanSubscribeForm)
        {

            this.LoadClientDropDownSubscription();
            this.EmailSubscribe.Text = string.Empty;
            this.SubscribeMessage.Text = string.Empty;

            if (expanSubscribeForm)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "CallExpanSubscribForm", "ExpanSubscribForm()", true);
            }
        }
        /// <summary>
        /// Update Order Line Status
        /// </summary>
        private void UpdateOrderLineStatus()
        {
            customerId = RecordCustomer();
            BasketHelper.SetBookingReferenceBooking(customerId);
        }

        /// <summary>
        /// Add Personal Details ToO rder
        /// </summary>
        private void AddPersonalDetailsToOrder()
        {
            PersonalDetails details = new PersonalDetails();
            details.FistName = FirstName.Text;
            details.LastName = LastName.Text;
            details.Address1 = AddressLine1.Text.Trim();
            details.Address2 = AddressLine2.Text.Trim();
            details.Address3 = AddressLine3.Text.Trim();
            details.CountyRegion = hdfLocation.Value;
            details.Country = CountryDropDown.SelectedItem.Text;
            details.MobilePhone = SecondaryPhone.Text;
            details.Phone = PrimaryPhone.Text;
            details.Email = Email.Text;
            details.Store = StoreDropDown.SelectedValue;
            details.SendEmail = SubscribeCheckBox.Checked;
            details.TermsAndConditions = true;
            details.OrderId = BasketHelper.GetBasketOrderId();
            BasketHelper.AddPersonalDetails(details);
            BasketHelper.SetBookingReferenceBooking(customerId);
        }
        /// <summary>
        /// Load Language Drop Down
        /// </summary>
        private void LoadLanguageDropDown()
        {
            try
            {
                Item itemName = Sitecore.Context.Item;
                if (itemName != null)
                {
                    var langNames = new ListItemCollection();
                    var languages = Sitecore.Data.Managers.ItemManager.GetContentLanguages(itemName);
                    if (languages != null)
                    {
                        foreach (var language in languages)
                        {
                            ListItem listItems = new ListItem();
                            listItems.Text = language.Title;
                            listItems.Value = language.Name;
                            langNames.Add(listItems);
                            LanguageDropDown.Visible = true;
                        }
                        BindDropdown(LanguageDropDown, langNames, "Text", "Value");
                        LanguageDropDown.Items.Insert(0, new ListItem { Text = "Please Select", Value = "0" });
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Bind DropDownList
        /// </summary>
        private void BindDropdown(DropDownList ddlControl, object dataSource, string text, string value)
        {
            try
            {
                ddlControl.DataSource = dataSource;
                ddlControl.DataTextField = text;
                ddlControl.DataValueField = value;
                ddlControl.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Event raised on client validation success
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClientValidationSuccess(object sender, EventArgs e)
        {
            if (e != null)
            {
                var customerRefernce = Sitecore.Events.Event.ExtractParameter(e, 0).ToString();

                //set order property for "CustomerReference"
                if (UCommerce.Api.TransactionLibrary.HasBasket())
                {
                    var order = UCommerce.Api.TransactionLibrary.GetBasket().PurchaseOrder;
                    order["CustomerReference"] = customerRefernce;
                    order.Save();
                   // this.CustomerValidationSection.Visible = false;
                    string classDetails=string.Empty;
                    classDetails=customervalidationdiv.Attributes["class"].ToString();
                    classDetails=classDetails.Replace("in",string.Empty).ToString().Trim();

                    customervalidationdiv.Attributes.Remove("class");
                    customervalidationdiv.Attributes.Add("class",classDetails);

                    this.CustomerPersonalDetails.Visible = true;
                    LoadCustomerSearch(customerRefernce);
                    

                }
            }
        }

        private void Subscribtion(object sender, EventArgs e)
        {
            if (e != null)
            {
                hdfSubcscribe.Value = Sitecore.Events.Event.ExtractParameter(e, 0).ToString();
            }
        }

        private bool IsValidated
        {
            get
            {
                return TransactionLibrary.HasBasket() &&
                    !string.IsNullOrEmpty(TransactionLibrary.GetBasket().PurchaseOrder["CustomerReference"]);
            }
        }


        /////// <summary>
        /////// page unload event
        /////// </summary>
        /////// <param name="sender"></param>
        /////// <param name="e"></param>
        ////protected void Page_Unload(object sender, EventArgs e)
        ////{
            
        ////}

        ///// <summary>
        ///// Event raised on client validation success
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //private void CustomerMappingSuccess(object sender, EventArgs e)
        //{
        //    PaymentSection.Visible = true;
        //}

        ///// <summary>
        ///// set check box values for payment portion and type
        ///// </summary>
        //private void SetCheckBoxValues()
        //{
        //    var methods = bookinghelper.BasketHelper.GetAllPaymentMethods();

        //    cashCheckBox.Enabled = false;
        //    chequeCheckBox.Enabled = false;
        //    voucherCheckBox.Enabled = false;
        //    poCheckBox.Enabled = false;

        //    partialCheckBox.Enabled = false;

        //    cashCheckBox.InputAttributes.Add("value", Affinion.LoyaltyBuild.Common.Constants.PaymentType.Cash);
        //    chequeCheckBox.InputAttributes.Add("value", Affinion.LoyaltyBuild.Common.Constants.PaymentType.Cheque);
        //    creditDebitCheckBox.InputAttributes.Add("value", Affinion.LoyaltyBuild.Common.Constants.PaymentType.CreditCard);
        //    voucherCheckBox.InputAttributes.Add("value", Affinion.LoyaltyBuild.Common.Constants.PaymentType.Voucher);
        //    poCheckBox.InputAttributes.Add("value", Affinion.LoyaltyBuild.Common.Constants.PaymentType.PostOffice);

        //    depositCheckBox.InputAttributes.Add("value", Affinion.LoyaltyBuild.Common.Constants.PaymentMethod.DepositOnly);
        //    fullCheckbox.InputAttributes.Add("value", Affinion.LoyaltyBuild.Common.Constants.PaymentMethod.FullPayment);
        //    partialCheckBox.InputAttributes.Add("value", Affinion.LoyaltyBuild.Common.Constants.PaymentMethod.PartialPayment);
        //}

        ///// <summary>
        ///// Proceed to payment
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //protected void ProceedToPayment(object sender, EventArgs e)
        //{

        //    var basket = bookinghelper.BasketHelper.GetBasket();

        //    basket.Order[Constants.OrderCheckoutAmountColumn] = basket.TotalPayableToday.ToString("F");
        //    basket.Order.Save();

        //    string redirectUrl = Request.Url.ToString().Replace("http:", "https:");
        //    redirectUrl = redirectUrl.Replace(Request.RawUrl, "/paymentgateway");
        //    Response.Redirect(redirectUrl, false);
        //}

        #endregion



    }
}