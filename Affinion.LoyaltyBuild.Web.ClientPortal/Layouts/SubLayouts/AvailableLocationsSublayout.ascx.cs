﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           AvailableLocationsSublayout.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.HotelDetailsSublayout
/// Description:           filter the search results from available locations

#region UsingDirectives
using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Data.Items;
using System;
using System.Linq;
using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
using Affinion.LoyaltyBuild.Common;
using System.Web.UI.WebControls;
#endregion

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts
{
    public partial class AvailableLocationsSublayout : BaseSublayout
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Uri requestUrl = Request.Url;
                string requestQuery = requestUrl.Query;
                
                if(!this.IsPostBack)
                {
                    LoadLocations();
                }

                if (this.IsPostBack && this.Request != null && !string.IsNullOrEmpty(this.Request.Params["__EVENTTARGET"]) && this.Request.Params["__EVENTTARGET"].Contains("LocationDropdown"))
                {
                    QueryStringHelper helper = new QueryStringHelper(requestUrl);
                    helper.SetValue("Location", LocationDropdown.SelectedValue);

                    Response.Redirect(helper.GetUrl().ToString(), true);
                }

                if (!this.IsPostBack && !string.IsNullOrEmpty(requestQuery))
                {
                    //Bind data to the text boxes in the result page
                    PrefillControls(requestUrl);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void LoadLocations()
        {
            //Load location data to a locationList
            Item item = this.GetDataSource();
             

            if (item != null)
            {

                this.Title.Item =
                this.Description.Item = item;
                var loactions = (from loc in item.Children
                                 orderby loc.DisplayName
                                 select loc).ToList();

                //Bind locationList data to the dropdown
                LocationDropdown.DataSource = SitecoreFieldsHelper.GetDropdownDataSource(loactions, "Name", "Name");
                LocationDropdown.DataTextField = "Text";
                LocationDropdown.DataValueField = "Value";
                LocationDropdown.DataBind();
                LocationDropdown.AutoPostBack = true;
                ListItem showAllItem = new ListItem(Constants.PleaseSelectText, "all");
                LocationDropdown.Items.Insert(0, showAllItem);
            }
        }

        private void PrefillControls(Uri requestUrl)
        {
            QueryStringHelper helper = new QueryStringHelper(requestUrl);
            string location = helper.GetValue("Location");
            if (!string.IsNullOrEmpty(location))
                this.LocationDropdown.SelectedValue = helper.GetValue("Location");
        }
    }
}