﻿using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.Search.Helpers;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Customer;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Handlers.CS
{
    /// <summary>
    /// Summary description for customerquerydetail
    /// </summary>
    public class customerquerydetail : IHttpHandler
    {
        private NameValueCollection _query = null;
        private QueryService _queryService;

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                _query = HttpUtility.ParseQueryString(context.Request.Url.Query.ToLower());
                _queryService = new QueryService();
                int queryId = Convert.ToInt32(_query["queryid"]);
                var searchResult = _queryService.GetCustomerQueryDetailById(queryId);
                var clientListItem = SearchHelper.GetUserSpecificClientItems();
                if (clientListItem != null)
                {
                    var clientFilterList = (from client in SearchHelper.GetUserSpecificClientItems()
                                            orderby client.DisplayName
                                            select new { DiplayName = client.DisplayName, ItemName = client.ID.Guid.ToString() }).ToList();
                    var clientFilterData = clientFilterList.FirstOrDefault(x => x.ItemName == searchResult.ClientId);
                    if (clientFilterData != null)
                    {
                        searchResult.ClientName = clientFilterData.DiplayName;
                    }
                }

               Item[] hotels = Sitecore.Context.Database.SelectItems(Constants.BasketPageQueryPath);
                foreach (var hotel in hotels)
                {
                    if (hotel != null && hotel.Fields["ID"] != null && !string.IsNullOrEmpty(hotel.Fields["ID"].Value) && hotel.ID.ToString() == searchResult.ProviderId)
                    {
                        searchResult.ProviderName = hotel.DisplayName;
                        break;
                    }
                }
                context.Response.Write(LoadUserControl(context, searchResult));
            }
            catch (Exception er)
            {
                context.Response.Write("Error" + er.Message);
            }
            context.Response.End();
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        protected string LoadUserControl(HttpContext context, CustomerQuery data)
        {
            using (Page page = new Page())
            {
                StringBuilder s = new StringBuilder();
                PlaceHolder f = new PlaceHolder();

                string url = string.Format("~/Layouts/SubLayouts/OfflinePortal/OfflinePortalSearchCustomerQueryEditPage.ascx");

                using (StringWriter writer = new StringWriter(CultureInfo.CurrentCulture))
                {

                    OfflinePortalSearchCustomerQueryEditPage avp = (OfflinePortalSearchCustomerQueryEditPage)page.LoadControl(url);
                    avp.queryData = data;
                    f.Controls.Add(avp);
                    page.Controls.Add(f);
                    HttpContext.Current.Server.Execute(page, writer, false);
                    s.Clear();
                    s.Append(writer.ToString());
                }
                return s.ToString();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Request"></param>
        /// <returns></returns>
        private string GetDocumentContents(System.Web.HttpRequest Request)
        {
            string documentContents;

            using (Stream receiveStream = Request.InputStream)
            {
                using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
                {
                    documentContents = readStream.ReadToEnd();
                }
            }

            return documentContents;
        }
    }
}