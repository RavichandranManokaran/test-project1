﻿using Affinion.LoyaltyBuild.Common.Exceptions;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts
{
    public partial class ClientPortalSavedHotelSublayout : BaseSublayout
    {
        protected string savedText { get; private set; }
        protected string closeText { get; private set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            BindSitecoreTexts();
            DataSet ds = uCommerceConnectionFactory.GetSavedHotel(68);
            DisplayHotel(ds);
            enableScript.Visible = !Sitecore.Context.PageMode.IsPageEditor;
        }

        private void BindSitecoreTexts()
        {
            Item dataSourceItem = this.GetDataSource();
            savedText = (dataSourceItem != null && dataSourceItem.Fields["TextSavedHotels"] != null) ? dataSourceItem.Fields["TextSavedHotels"].Value : string.Empty;
            closeText = (dataSourceItem != null && dataSourceItem.Fields["TextClose"] != null) ? dataSourceItem.Fields["TextClose"].Value : string.Empty;
            SitecoreFieldsHelper.BindSitecoreText(dataSourceItem, "TextSavedHotels", savedhotelText);
            //SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "TextClose", closeText);
        }

        private void DisplayHotel(DataSet Hotels)
        {
            List<Item> HotelItem = new List<Item>();
            DataTable dt = new DataTable();
            //string name, location, ImgUrl, link, linkRowName = "link", TitleRowName = "Title", LocationsRowName = "Locations", ImgSrcRowName = "ImgSrc", NoOfReviewRowName = "NoOfReview", RatingRowName = "Rating";
            //int NoOfReviews, Rating;
            //dt.Columns.Add(TitleRowName);
            //dt.Columns.Add(LocationsRowName);
            //dt.Columns.Add(ImgSrcRowName);
            //dt.Columns.Add(linkRowName);
            //dt.Columns.Add(NoOfReviewRowName);
            //dt.Columns.Add(RatingRowName);
            ////string hotelPath = "/sitecore/content/#admin-portal#/#supplier-setup#/hotels//*[@@templateid='{A7235C94-2EF5-4A3B-87DB-0D4DCF10B4DB}']";
            //Item[] HotelItems = Sitecore.Context.Database.SelectItems(hotelPath);
            //if (HotelItems != null && HotelItems.Length > 0)
            //{
                for (int i = 0; i < Hotels.Tables[0].Rows.Count;i++ )
                {
                    //Item hotelItem = HotelItems.Where(item => item.Fields["UCommerceCategoryId"].Value.Equals(uCommerceHotel)).FirstOrDefault();
                    HotelItem.Add(Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(Hotels.Tables[0].Rows[i][0])));
                    //if (hotelItem != null)
                    //{
                    //    link = "/hotelinformation?id=" + hotelItem.ID.ToString();
                    //    name = hotelItem.Fields["Name"].Value;
                    //    Item locationName = Sitecore.Context.Database.GetItem(hotelItem.Fields["Location"].Value);
                    //    Item CountryName = Sitecore.Context.Database.GetItem(locationName.Fields["Country"].Value);
                    //    location = locationName.Fields["Name"].Value + ", " + CountryName.Fields["CountryName"].Value;
                    //    Sitecore.Data.Fields.ImageField imgField = ((Sitecore.Data.Fields.ImageField)hotelItem.Fields["IntroductionImage"]);
                    //    ImgUrl = Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField.MediaItem);
                    //    DataRow row = dt.NewRow();
                    //    row[linkRowName] = link;
                    //    row[TitleRowName] = name;
                    //    row[LocationsRowName] = location;
                    //    row[ImgSrcRowName] = ImgUrl;
                    //    row[NoOfReviewRowName] = int.TryParse(hotelItem.Fields["NoOfReviews"].Value, out NoOfReviews) ? NoOfReviews : 0;
                    //    row[RatingRowName] = int.TryParse(hotelItem.Fields["Rating"].Value, out Rating) ? Rating : 0; 
                    //    dt.Rows.Add(row);
                        
                   // }
                //}
            }
            rptHotelList.DataSource = HotelItem;
            rptHotelList.DataBind();
        }

        protected void btnShowSavedHotels_Click(object sender, EventArgs e)
        {
            DataSet ds = uCommerceConnectionFactory.GetSavedHotel(68);
            DisplayHotel(ds);
        }


        private List<int> BindStarRankings(Item item)
        {
            List<int> listOfStars = new List<int>();
            ///Bind star rank
            string ranking = SitecoreFieldsHelper.GetDropLinkFieldValue(item, "StarRanking", "Name");

            if (!string.IsNullOrWhiteSpace(ranking))
            {
                int starRank;

                bool result = Int32.TryParse(ranking, out starRank);

                if (result)
                {
                    //starRank = int.Parse(ranking);
                    listOfStars = Enumerable.Repeat(starRank, starRank).ToList();
                }
                else
                {
                    Diagnostics.WriteException(DiagnosticsCategory.ClientPortal,
                        new AffinionException("Unable to parse star rank:" + item.DisplayName), this);
                }
            }
            return listOfStars;
        }
        protected void rptHotelList_DataBinding(object sender, RepeaterItemEventArgs e)
        {
            Item hotelItem = e.Item.DataItem as Item;
            if (hotelItem != null)
            {
                Item locationItem = (hotelItem.Fields["Location"] != null && !string.IsNullOrWhiteSpace(hotelItem.Fields["Location"].Value)) ? ((Sitecore.Data.Fields.LookupField)hotelItem.Fields["Location"]).TargetItem : null;
                Item countryItem = (locationItem != null && locationItem.Fields["Country"] != null && !string.IsNullOrWhiteSpace(locationItem.Fields["Country"].Value)) ? ((Sitecore.Data.Fields.LookupField)locationItem.Fields["Country"]).TargetItem : null;

                string location = locationItem != null ? locationItem.Fields["Name"].Value : string.Empty;
                string country = countryItem != null ? countryItem.Fields["CountryName"].Value : string.Empty;

                Image hotelImage = e.Item.FindControl("imgHotelImage") as Image;
                Label hotelLocation = e.Item.FindControl("lblLocation") as Label;
                HyperLink linkHotelName = e.Item.FindControl("linkHotelName") as HyperLink;
                Repeater starRepeater = e.Item.FindControl("rptStar") as Repeater;
                Literal reviewNumber = e.Item.FindControl("litNOR") as Literal;
                Literal Rating = e.Item.FindControl("litRank") as Literal;

                Sitecore.Data.Fields.ImageField imgField = hotelItem.Fields["IntroductionImage"];
                hotelImage.ImageUrl = ((imgField.MediaItem) != null) ? Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField.MediaItem) : string.Empty;
                linkHotelName.Text = hotelItem.Fields["Name"].Value;
                hotelLocation.Text = (!string.IsNullOrWhiteSpace(location) && !string.IsNullOrWhiteSpace(country)) ? string.Format("{0}, {1}", location, country) : string.Format("{0}{1}", location, country);
                linkHotelName.NavigateUrl = "/hotelinformation?id=" + hotelItem.ID.ToString() + "&NoOfRooms=1&NoOfAdults=1&NoOfChildren=0";

                starRepeater.DataSource = BindStarRankings(hotelItem);
                starRepeater.DataBind();
            }
        }
    }
}