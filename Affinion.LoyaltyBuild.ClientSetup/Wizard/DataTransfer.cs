﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion Inernational. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           DataTransfer.cs
/// Sub-system/Module:     ClientSetup(VS project name)
/// Description:           DataTransfer Class use to store transfering data
/// </summary>


#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 
#endregion

namespace Affinion.LoyaltyBuild.ClientSetup.Wizard
{
    /// <summary>
    /// This class use to transfer data 
    /// </summary>
    public class DataTransfer
    {

        #region Class Properties 

        // private static attribute for singleton object
        private static DataTransfer DataTransferingObject = null;

        // DictionaryData which is use to store key value of data pair
        public Dictionary<string, string> DictionaryData { get; set; }

        // use to store JSon string from client
        public string JSonString { get; set; } 

        #endregion

        #region Public methods
        /// <summary>
        /// class constructor creates object and initalize
        /// </summary>
        public DataTransfer()
        {
            DictionaryData = new Dictionary<string, string>();
        }

        /// <summary>
        /// property which will set singleton object of this class
        /// </summary>
        public static DataTransfer Instance
        {
            get
            {
                if (DataTransferingObject == null)
                {
                    DataTransferingObject = new DataTransfer();
                    // DictionaryData = new Dictionary<string, string>();
                }
                return DataTransferingObject;
            }
        } 
        #endregion
    }
}
