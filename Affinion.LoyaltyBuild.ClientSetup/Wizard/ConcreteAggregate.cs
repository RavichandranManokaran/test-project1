﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion Inernational. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           ConcreteAggregate.cs
/// Sub-system/Module:     ClientSetup(VS project name)
/// Description:           Concrete Aggregate Class
/// </summary>


#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#endregion
namespace Affinion.LoyaltyBuild.ClientSetup.Wizard
{
    /// <summary>
    /// class implements the IWizardAggregate interface and is the class that creates the ConcreteIterator.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class ConcreteAggregate<T> : IWizardAggregate<T>
    {
        #region Private Properties
        private IWizardIterator<T> iterator; 
        #endregion

        #region Public Methodes

        /// <summary>
        /// constructor of the class
        /// </summary>        
        public ConcreteAggregate()
        {
            (this as IWizardAggregate<T>).CreateIterator();  //create the iterator
        }


        /// <summary>
        /// create singleton object
        /// </summary>
        /// <returns></returns>
        IWizardIterator<T> IWizardAggregate<T>.CreateIterator()
        {
            //create iterator if not already done
            if (iterator == null)
                iterator = new ConcreteIterator<T>(this);
            return iterator;
        }

        List<T> IWizardAggregate<T>.GetAll()
        {
            List<T> list = new List<T>();
            list.Add(iterator.First());
            while (!iterator.IsDone())
            {
                list.Add(iterator.Next());
            }
            return list;
        }

        void IWizardAggregate<T>.AddItem(T item)
        {
            iterator.AddItem(item);
        } 

        #endregion
    }
}

