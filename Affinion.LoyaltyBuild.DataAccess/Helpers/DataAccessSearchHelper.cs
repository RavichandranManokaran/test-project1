﻿//<summary>
/// © 2016 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           DataAccessSearchHelper.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.DataAccess.Helpers
/// Description:           Search data access class
/// </summary>

#region Using Directives
using Affinion.LoyaltyBuild.Common.Instrumentation;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic; 
#endregion

namespace Affinion.LoyaltyBuild.DataAccess.Helpers
{
    public static class DataAccessSearchHelper
    {
        public static IList<dynamic> GetAvailableProductList(int productSku, int checkInDateCount, int checkOutDateCount)
        {
            try
            {
                List<dynamic> results = new List<dynamic>();

                //UCommerce.Infrastructure.Configuration.RuntimeConfigurationSection.ConnectionString
                DataConnection dataConnection = new DataConnection();

                //RuntimeConfigurationSection runtimeConfigurationSection = new RuntimeConfigurationSection();
                //string connectionString = runtimeConfigurationSection.ConnectionString;
                using (SqlConnection sqlConnection = new SqlConnection(dataConnection.ConnectionString))
                {
                    //DataObject dataObject = new DataObject();
                    SqlCommand sqlCommand = new SqlCommand("LB_GetAvailableProductList", sqlConnection);
                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.Parameters.Add(new SqlParameter("CheckInDateCount", checkInDateCount));

                    sqlCommand.Parameters.Add(new SqlParameter("CheckOutDateCount", checkOutDateCount));

                    sqlCommand.Parameters.Add(new SqlParameter("ProductSku", productSku));

                    sqlConnection.Open();

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();


                    while (sqlDataReader.Read())
                    {
                        dynamic result = new ExpandoObject();

                        result.AvailabilityId = sqlDataReader.GetInt32(0);
                        result.DateCounter = sqlDataReader.GetInt32(1);
                        result.ProviderOccupancyTypeID = sqlDataReader.GetInt32(2);
                        result.NumberAllocated = sqlDataReader.GetInt32(3);
                        result.NumberBooked = sqlDataReader.GetInt32(4);

                        results.Add(result);
                    }

                    sqlConnection.Close();
                }

                return results;

            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.DataAccess, exception, null);
                throw;
            }
        }
    }
}
