﻿CREATE PROCEDURE [dbo].[LB_ManageReservationNotes]
	(
	@OrderLineId	INT,
	@NoteTypeId		INT, 
	@Note			VARCHAR(MAX),
	@CreatedBy		NVARCHAR(100)
	)
AS
BEGIN
	-- if record exists the update otherwise inser except notes type 'Internal Note'
	IF exists( SELECT 1 FROM LB_ReservationNotes 
			   WHERE OrderLineId = @OrderLineId
				 AND NoteTypeId  = @NoteTypeId	
				 AND @NoteTypeId <> 1
				) 
		BEGIN
			UPDATE LB_ReservationNotes 
			SET Note = @Note,
				CreatedBy = @CreatedBy,
				CreatedDate = GETDATE()
			WHERE OrderLineId = @OrderLineId
						AND NoteTypeId  = @NoteTypeId	
						AND @NoteTypeId <> 1
		END
	ELSE
		BEGIN
		DECLARE @BookingId NVARCHAR(MAX)=NULL
		SET @BookingId=(SELECT BookingId FROM LB_BookingInfo WHERE OrderLineId=@OrderLineId)
			INSERT INTO LB_ReservationNotes(
						 NoteTypeId
						,OrderLineId
						,BookingId
						,Note
						,CreatedDate
						,CreatedBy)
			VALUES(@NoteTypeId,@OrderLineId,@BookingId,@Note,GETDATE(),@CreatedBy)
		END
END
