﻿using System;
using System.Configuration;

namespace Affinion.LoyaltyBuild.BedBanks.Troika.Config
{
    public class CurrencyCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new Currency();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((Currency)element).Id;
        }

        protected override string ElementName
        {
            get
            {
                return "currency";
            }
        }

        protected override bool IsElementName(string elementName)
        {
            return !String.IsNullOrEmpty(elementName) && elementName == "currency";
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.BasicMap;
            }
        }

        public Currency this[int index]
        {
            get
            {
                return base.BaseGet(index) as Currency;
            }
        }

        public new Currency this[string key]
        {
            get
            {
                return base.BaseGet(key) as Currency;
            }
        }
    }
}