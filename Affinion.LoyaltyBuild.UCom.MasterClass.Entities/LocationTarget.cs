﻿
using Affinion.LoyaltyBuild.UCom.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;
using UCommerce.Marketing.Targets;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class LocationTarget : Target, IOrderLineTarget, IEntity, ITarget, IDisplayTarget
    {
        private int? _hashCode;

        public virtual bool IsSatisfiedBy(OrderLine orderLine)
        {
            OrderProperty forSearch = orderLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.ForSearch);
            if (forSearch != null)
            {
                OrderProperty locationProperty = orderLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.Location);
                if (locationProperty != null)
                {
                    return this.Location.Split('|').Any(x => x.Equals(locationProperty.Value, StringComparison.InvariantCultureIgnoreCase));
                }
                return false;
            }
            else
            {
                Product product = Product.All().Where(x => x.Sku == orderLine.Sku).FirstOrDefault();
                ProductProperty property = product[ProductDefinationConstant.Location];
                if (property != null && this.Location.Split('|').Any(z => z.Equals(property.Value)))
                {
                    return true;
                }
            }
            return false;

        }

        public virtual bool IsSatisfiedBy(UCommerce.Marketing.TargetingContext targetingContext)
        {
            return true;
        }

        // Methods
        public new static IQueryable<LocationTarget> All()
        {
            return GetRepo().Select();
        }

        public override void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<LocationTarget, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            LocationTarget objA = obj as LocationTarget;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<LocationTarget, bool>> expression)
        {
            return All().Any<LocationTarget>(expression);
        }

        public static IList<LocationTarget> Find(Expression<Func<LocationTarget, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<LocationTarget>();
        }

        public static LocationTarget FirstOrDefault(Expression<Func<LocationTarget, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<LocationTarget>();
        }

        public new static LocationTarget Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<LocationTarget> GetRepo()
        {
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<LocationTarget>>();
        }


        public override void Save()
        {
            GetRepo().Save(this);
        }

        public static LocationTarget SingleOrDefault(Expression<Func<LocationTarget, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties
        public override int Id
        {
            get
            {
                return this.LocationTargetId;
            }
        }

        public virtual int LocationTargetId { get; protected set; }

        public virtual string Location { get; set; }





    }
}
