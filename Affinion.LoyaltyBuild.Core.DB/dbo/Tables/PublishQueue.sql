﻿CREATE TABLE [dbo].[PublishQueue] (
    [ID]        UNIQUEIDENTIFIER CONSTRAINT [DF_PublishQueue_ID] DEFAULT (newid()) NOT NULL,
    [ItemID]    UNIQUEIDENTIFIER NOT NULL,
    [Language]  NVARCHAR (50)    CONSTRAINT [DF_PublishQueue_Language] DEFAULT ('en') NOT NULL,
    [Version]   INT              CONSTRAINT [DF_PublishQueue_Version] DEFAULT (1) NOT NULL,
    [Action]    NVARCHAR (50)    CONSTRAINT [DF_PublishQueue_Action] DEFAULT ('') NOT NULL,
    [Date]      DATETIME         NOT NULL,
    [Index]     INT              IDENTITY (1, 1) NOT NULL,
    [DAC_Index] INT              NULL
);


GO
CREATE CLUSTERED INDEX [DAC_Clustered]
    ON [dbo].[PublishQueue]([DAC_Index] ASC);


GO
CREATE NONCLUSTERED INDEX [ndxDate]
    ON [dbo].[PublishQueue]([Date] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [ndxID]
    ON [dbo].[PublishQueue]([ID] ASC);

