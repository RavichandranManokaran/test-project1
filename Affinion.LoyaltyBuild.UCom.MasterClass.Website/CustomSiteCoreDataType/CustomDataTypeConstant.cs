﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.CustomSiteCoreDataType
{
    public static class CustomDataTypeConstant
    {
        public const string Location = "SiteCoreLocation";
        public const string Country = "SiteCoreCountry";
        public const string Region = "SiteCoreRegion";
        public const string HotelRanking = "SiteCoreHotelRanking";
        public const string HotelTheme = "SiteCoreHotelTheme";
        public const string Experience = "SiteCoreHotelExperience";
        public const string Facility = "SiteCoreHotelFacility";
        public const string SupplierType = "SiteCoreSupplierType";
        public const string Client = "SiteCoreClient";
        public const string OccupancyTypes = "SiteCoreOccupancyTypes";
        public const string OfferType = "SiteCoreOfferType";
        public const string PackageType = "SiteCorePackageType";
        public const string Groups = "SiteCoreGroups";
        public const string SuperGroups = "SiteCoreSuperGroups";
        public const string AddOn = "SiteCoreAddOn";
        public const string AddOnMultiSelect = "SiteCoreAddOnMulti";
        public const string Hotel = "SiteCoreHotel";
        public const string PaymentMethod = "SiteCorePaymentMethod";
        public const string Commission = "SiteCoreCommission";
        public const string Recommended = "SiteCoreRecommended";
        public const string ClientMulti = "SiteCoreClientMulti";
        public const string PortalTypeMulti = "SiteCorePortalTypeMulti";
        public const string PortalType = "SiteCorePortalType";
        public const string SingleClient = "SiteCoreSingleClient";
        public const string Currency = "SiteCoreCurrency";
        
    }
    
}