﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AvailabilityPackageView.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Availability.AvailabilityPackageView" %>



<asp:Repeater ID="packageDetailsRepeater" runat="server">
    <ItemTemplate>
        <div class="popupbox">
            <h4><%#Eval("Name")%>
            </h4>
            <div class="row no-margin">
                <asp:Repeater ID="packageDetailsInnerRepeater" runat="server" datasource="<%#((Affinion.LoyaltyBuild.Model.Pricing.ProductPrice)Container.DataItem).RelatedPrice %>">
                    <ItemTemplate>
                        <div class="col-md-12 PadTB10px">
                            <div class="col-md-6 no-padding"><%#((Affinion.LoyaltyBuild.Model.Pricing.ProductPrice)Container.DataItem).Name %>
                            </div>
                            <div class="col-md-3 no-padding"><%#((Affinion.LoyaltyBuild.Model.Pricing.ProductPrice)Container.DataItem).PriceAmount %>
                            </div>
                            <div class="col-md-3 no-padding">
                                (<span><%#((Affinion.LoyaltyBuild.Model.Pricing.ProductPrice)Container.DataItem).PriceAmount-((Affinion.LoyaltyBuild.Model.Pricing.ProductPrice)Container.DataItem).Commision.Day1 %></span>
                                +<span><%#((Affinion.LoyaltyBuild.Model.Pricing.ProductPrice)Container.DataItem).Commision.Day1 %></span>)
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                <div class="col-md-12 PadTB10px alert-warning">
                    <div class="col-md-6 no-padding">Total</div>
                    <div class="col-md-3 no-padding"><%#((Affinion.LoyaltyBuild.Model.Pricing.ProductPrice)Container.DataItem).PriceAmount %>
                    </div>
                    <div class="col-md-3 no-padding">
                        (<span><%#((Affinion.LoyaltyBuild.Model.Pricing.ProductPrice)Container.DataItem).PriceAmount-((Affinion.LoyaltyBuild.Model.Pricing.ProductPrice)Container.DataItem).Commision.Day1 %></span>
                                +<span><%#((Affinion.LoyaltyBuild.Model.Pricing.ProductPrice)Container.DataItem).Commision.Day1 %></span>)
                    </div>
                </div>
            </div>
        </div>
    </ItemTemplate>
</asp:Repeater>
