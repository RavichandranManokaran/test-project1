﻿using Affinion.LoyaltyBuild.Api.Booking.Data.Basket;
using Affinion.LoyaltyBuild.Api.Booking.Service;
using Affinion.LoyaltyBuild.Common.CardValidationCheck;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Email;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using UCommerce.Infrastructure;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Handlers.CS
{
    /// <summary>
    /// Summary description for BookingHandler
    /// </summary>
    public class BookingHandler : IHttpHandler
    {
        private IBookingService _bookingService;
        private IMailService mailService;

        /// <summary>
        /// Constructor
        /// </summary>
        public BookingHandler()
        {
            _bookingService = ObjectFactory.Instance.Resolve<IBookingService>();
        }

        /// <summary>
        /// Process request
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context)
        {
            string request = null;
            string response = string.Empty;

            context.Response.ContentType = "text/json";
            switch (context.Request.QueryString["action"].ToLower())
            {
                case "precancel":
                    int orderLine = 0;
                    int.TryParse(context.Request.QueryString["olid"], out orderLine);
                    response = PreCancel(orderLine);
                    break;
                case "cancel":
                    request = GetDocumentContents(context.Request);
                    response = CancelOrder(request);
                    break;
                case "transfer":
                    request = GetDocumentContents(context.Request);
                    response = TransferOrder(request);
                    break;
            }
            context.Response.Write(response);
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Precancel booking
        /// </summary>
        /// <param name="orderLineId"></param>
        /// <returns></returns>
        private string PreCancel(int orderLineId)
        {
            var serializer = new JavaScriptSerializer();

            return serializer.Serialize(_bookingService.ValidateCancelBooking(orderLineId));
        }

        /// <summary>
        /// Cancel booking
        /// </summary>
        private string CancelOrder(string content)
        {
            var serializer = new JavaScriptSerializer();

            var cancelInfo = serializer.Deserialize<CancelRequest>(content);
            cancelInfo.NoteToProvider = CredictCardCheck.CredictCard(cancelInfo.NoteToProvider);
            cancelInfo.CancelReason = CredictCardCheck.CredictCard(cancelInfo.CancelReason);
            cancelInfo.CancelledBy = Sitecore.Security.Accounts.User.Current.DisplayName;
            var cancelStatus = _bookingService.CancelBooking(cancelInfo);
            if (cancelStatus.IsSuccess)
            {
                SendEmail(cancelInfo.OrderLineId);
            }
            return serializer.Serialize(cancelStatus);
        }

        /// <summary>
        /// Transfer booking
        /// </summary>
        private void SendEmail(int orderlineid)
        {
            try
            {
                BookingService bookingObj = new BookingService();

                var booling = bookingObj.GetBooking(orderlineid);
                mailService = new MailService(booling.ClientId);
                if (booling.Type == Model.Product.ProductType.BedBanks)
                {
                    mailService.GenerateEmail(MailTypes.BedBanksCustomerCancellaton, orderlineid);
                }
                else
                {
                    mailService.GenerateEmail(MailTypes.CustomerCancellation, orderlineid);
                    mailService.GenerateEmail(MailTypes.ProviderCancellation, orderlineid);
                }
            }
            catch (Exception exception)
            {
                Affinion.LoyaltyBuild.Common.Instrumentation.Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, string.Format("Cancelbooking Email: Exception-{0}", exception.ToString()));
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
            }
        }

        /// <summary>
        /// Transfer booking
        /// </summary>
        private string TransferOrder(string content)
        {
            var serializer = new JavaScriptSerializer();

            var transferInfo = serializer.Deserialize<TransferRequest>(content);

            return serializer.Serialize(_bookingService.TransferBooking(transferInfo));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Request"></param>
        /// <returns></returns>
        private string GetDocumentContents(System.Web.HttpRequest Request)
        {
            string documentContents;

            using (Stream receiveStream = Request.InputStream)
            {
                using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
                {
                    documentContents = readStream.ReadToEnd();
                }
            }

            return documentContents;
        }

    }
}