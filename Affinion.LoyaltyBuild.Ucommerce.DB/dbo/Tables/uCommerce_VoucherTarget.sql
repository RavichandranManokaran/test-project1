﻿CREATE TABLE [dbo].[uCommerce_VoucherTarget] (
    [VoucherTargetId] INT           NOT NULL,
    [Name]            NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_uCommerce_SingleUseVoucher_1] PRIMARY KEY CLUSTERED ([VoucherTargetId] ASC),
    CONSTRAINT [FK_uCommerce_VoucherTarget_uCommerce_Target] FOREIGN KEY ([VoucherTargetId]) REFERENCES [dbo].[uCommerce_Target] ([TargetId])
);

