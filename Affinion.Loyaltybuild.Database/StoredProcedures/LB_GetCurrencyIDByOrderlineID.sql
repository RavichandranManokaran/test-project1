﻿/* Procedure for fetching LB_GetCurrencyIDByOrderlineID */
CREATE PROCEDURE [dbo].[LB_GetCurrencyIDByOrderlineID]
(	 
	@OrderLineId int=NULL	
)
AS 
	IF (@OrderLineId IS NOT NULL)
	BEGIN		
		select case when po.currencyid is null then 0 else po.currencyid end as [CurrencyID],
		c.ExchangeRate [ExchangeRate] from ucommerce_purchaseorder po
        inner join ucommerce_orderline ol on ol.orderid = po.orderid
		inner join uCommerce_Currency c on c.CurrencyId = po.CurrencyId
		where ol.orderlineid = @OrderLineId					
	END	
RETURN 
