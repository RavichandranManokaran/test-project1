﻿using System.Collections.Generic;
using System.Linq;
using UCommerce.EntitiesV2;

namespace Affinion.LoyaltyBuild.Api.Booking.Data
{
    /// <summary>
    /// Basket info
    /// </summary>
    public class BasketInfo
    {
        private readonly int _orderId;
        private readonly PurchaseOrder _order;

        public BasketInfo(int orderId)
        {
            _orderId = orderId;
            _order = PurchaseOrder.FirstOrDefault(i => i.OrderId == orderId);
        }

        /// <summary>
        /// Order Id
        /// </summary>
        public int OrderId { get { return _orderId; } }

        /// <summary>
        /// Purchase order
        /// </summary>
        public PurchaseOrder Order { get { return _order; } }

        /// <summary>
        /// Bookings
        /// </summary>
        public IList<Booking> Bookings { get; set; }

        /// <summary>
        /// processing fee
        /// </summary>
        public decimal ProcessingFee
        {
            get
            {
                if (Bookings.Count > 0)
                {
                    return Bookings.Max(i => i.ProcessingFee);
                }
                return 0;

            }
        }
        
        /// <summary>
        /// total amount
        /// </summary>
        public decimal TotalAmount
        {
            get
            {
                return Bookings.Sum(i => i.Price) + ProcessingFee;
            }
        }

        /// <summary>
        /// total payable today
        /// </summary>
        public decimal TotalPayableToday
        {
            get
            {
                return Bookings.Sum(i => i.CommissionAmount) +
                    Bookings.Sum(i => i.ProviderAmountPaidOnCheckOut) +
                    ProcessingFee;
            }
        }
        
        /// <summary>
        /// total payable accomodation
        /// </summary>
        public decimal TotalPayableAccomodation
        {
            get
            {
                return TotalAmount - TotalPayableToday;
            }
        }

        /// <summary>
        /// Currency id
        /// </summary>
        public int CurrencyId
        {
            get
            {
                return Order.BillingCurrency.CurrencyId;
            }
        }
    }
}
