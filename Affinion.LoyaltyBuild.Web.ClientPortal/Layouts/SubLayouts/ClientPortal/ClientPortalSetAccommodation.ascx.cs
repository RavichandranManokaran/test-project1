﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           ClientPortalSetAccommodation.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal
/// Description:           Set the accommodation details

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal
{
    #region Using Directives
    using Affinion.Loyaltybuild.BusinessLogic.Helper;
    using Affinion.LoyaltyBuild.Common.Exceptions;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.SessionHelper;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.WebControls; 
    #endregion

    public partial class ClientPortalSetAccommodation : BaseSublayout
    {
        private List<RoomReservation> roomReservations = new List<RoomReservation>();
        private List<RoomReservationAgeInfo> roomReservationAgeInfo = new List<RoomReservationAgeInfo>();
        private List<int> ChildAgeList = new List<int>();
        public Sitecore.Web.UI.WebControls.Text txtGuestNameRegularMessage;
        public Sitecore.Web.UI.WebControls.Text txtGuestNameRequiredMessage;

        // private Item AccommodationItem = null;

        string Rooms = string.Empty;

        string OrderLineId = string.Empty;
        //Repeater RepeaterChildAge = null;

        //use to manuplate the dropdown list
        private string GuestRange = string.Empty;


        public override void PreLoad(EventArgs e)
        {
            if (!IsPostBack)
            {
                Uri pageUri = Request.Url;
                QueryStringHelper helper = new QueryStringHelper(pageUri);

                Rooms = helper.GetValue("Rooms");
                OrderLineId = helper.GetValue("orderid");
                GuestRange = helper.GetValue("GuestRange");
            }
        }
        /// <summary>
        /// Page load related operations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Page_Load(object sender, EventArgs e)
        {
            this.BindSitecoreTexts();
            LabelValidateFieldErrorMsg.Text = string.Empty;

            if (!IsPostBack)
            {
                int count = 0;
                var isValid = Int32.TryParse(Rooms, out count);

                //Populate the accodamation informaiton from database
                PopulateAccommodationData(count);

                //Populate the list of child ages
                PopulateChildAgeFromDB();
            }
        }

        /// <summary>
        /// Use to populate the AccommodationData
        /// </summary>
        private void PopulateAccommodationData(int roomCount)
        {
            try
            {
                List<RoomReservation> roomReservations = new List<RoomReservation>();

                // Call Database to retrive saved data.
                List<RoomReservation> ListRoomReservation = BasketHelper.GetRoomReservationByOrderLineId(OrderLineId);
                if (ListRoomReservation.Count > 0)
                {
                    this.RepeaterGuestAccordian.DataSource = ListRoomReservation;
                    this.RepeaterGuestAccordian.DataBind();

                    //this.TextSpecialRequest.Text = ListRoomReservation[0].SpecialRequest;
                }
                else
                {
                    for (int i = 0; i < roomCount; i++)
                    {
                        roomReservations.Add(new RoomReservation { NoOfGuest = 1, GuestName = string.Empty, OrderLineId = string.Empty, SpecialRequest = string.Empty });
                    }
                    this.RepeaterGuestAccordian.DataSource = roomReservations.GetRange(0, roomCount);
                    this.RepeaterGuestAccordian.DataBind();
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Populate ChildAge From DB
        /// </summary>
        private void PopulateChildAgeFromDB()
        {
            try
            {
                //Clear the list
                ChildAgeList.Clear();

                // Fill the age into the list
                ChildAgeList = BasketHelper.PopulateChildAgeFromDB(OrderLineId.Trim());
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Save Guest details in to DB
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            #region Local variables
            string guestName = string.Empty;
            string specialRequest = string.Empty;
            int numberOfGuest = 0;
            int numberOfChild = 0;
            int numberOfAdult = 0;

            TextBox TextGuestFullName = null;
            TextBox TextSpecialRequest = null;
            DropDownList DropdownGuest = null;
            DropDownList DropdownChild = null;
            DropDownList DropdownAdult = null;
            Repeater ChildAge = null;

            HiddenField HiddenRoomReservationId = null;

            #endregion


            try
            {
                Uri pageUri = Request.Url;
                QueryStringHelper helper = new QueryStringHelper(pageUri);
                string orderLineId = helper.GetValue("orderid");
                string roomReservationId = string.Empty;

                foreach (RepeaterItem item in RepeaterGuestAccordian.Items)
                {
                    if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                    {
                        DropdownGuest = (DropDownList)item.FindControl("DropdownGuest");
                        DropdownChild = (DropDownList)item.FindControl("DropDownNoOfChildren");
                        DropdownAdult = (DropDownList)item.FindControl("DropDownNoOfAdults");
                        TextGuestFullName = (TextBox)item.FindControl("TextGuestFullName");
                        TextSpecialRequest = (TextBox)item.FindControl("TextSpecialRequest");


                        HiddenRoomReservationId = (HiddenField)item.FindControl("HiddenRoomReservationId");


                        if (TextGuestFullName != null)
                            guestName = TextGuestFullName.Text.Trim();

                        if (TextSpecialRequest != null)
                            specialRequest = TextSpecialRequest.Text;

                        if (DropdownGuest != null)
                            numberOfGuest = Convert.ToInt32(DropdownGuest.SelectedValue);

                        if (DropdownAdult != null)
                            numberOfAdult = Convert.ToInt32(DropdownAdult.SelectedValue);

                        if (DropdownChild != null)
                            numberOfChild = Convert.ToInt32(DropdownChild.SelectedValue);

                        if (HiddenRoomReservationId != null)
                            roomReservationId = HiddenRoomReservationId.Value.ToString();
                        // Populate list of roomReservations
                        roomReservations.Add(new RoomReservation { OrderLineId = orderLineId, NoOfGuest = numberOfGuest, GuestName = guestName, NoOfAdults = numberOfAdult, NoOfChildren = numberOfChild, SpecialRequest = specialRequest });

                        ChildAge = (Repeater)item.FindControl("RepeaterChildAge");
                    }

                    // Get the child selection form another repearter
                    GetChildSection(ChildAge, orderLineId, roomReservationId, item);
                }
                if (guestName.Length > 0)
                {
                    SetGuestDetails(TextGuestFullName);
                }
                else
                {
                    TextGuestFullName.CssClass = "form-control has-error-input";
                   // LabelValidateFieldErrorMsg.Text = "Please fill required fields.";
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        private void SetGuestDetails(TextBox textGuestFullName)
        {
            textGuestFullName.CssClass = "form-control";
            LabelValidateFieldErrorMsg.Text = string.Empty;
            // Save to database
            BasketHelper.AddGuestDetails(roomReservations);

            //Save child Age details
            //BasketHelper.AddGuestChildAgeDetails(roomReservationAgeInfo);
            //Response.Redirect(Request.RawUrl);
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "Close", "window.close()", true);
        }

        private void GetChildSection(Repeater childAge, string orderLineId, string roomReservationId, RepeaterItem item)
        {
            if (childAge != null)
            {
                foreach (RepeaterItem ageDrpList in childAge.Items)
                {
                    if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                    {
                        var ageDropdown = (DropDownList)ageDrpList.FindControl("DropDownAge");
                        if (ageDropdown != null)
                        {
                            roomReservationAgeInfo.Add(new RoomReservationAgeInfo { Age = ageDropdown.SelectedIndex + 1, OrderLineId = Convert.ToInt32(orderLineId)});
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Bound item on data binding of repeater
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RepeaterGuestAccordian_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            // Default value is = 4
            int numberRange = 4;
            bool result = Int32.TryParse(GuestRange, out numberRange);

            if (e != null)
            {
                DropDownList DropdownGuest = (DropDownList)e.Item.FindControl("DropdownGuest");
                TextBox TextGuestFullName = (TextBox)e.Item.FindControl("TextGuestFullName");
                TextBox TextSpecialRequest = (TextBox)e.Item.FindControl("TextSpecialRequest");

                HiddenField HiddenRoomReservationId = (HiddenField)e.Item.FindControl("HiddenRoomReservationId");

                DropDownList DropDownNoOfAdults = (DropDownList)e.Item.FindControl("DropDownNoOfAdults");
                DropDownList DropDownNoOfChildren = (DropDownList)e.Item.FindControl("DropDownNoOfChildren");

                // Child Age repeater
                Repeater RepeaterChildAge = (Repeater)e.Item.FindControl("RepeaterChildAge");

                DropdownGuest.DataSource = Enumerable.Range(1, numberRange);
                DropdownGuest.DataBind();

                // Binds Adults and child details
                DropDownNoOfChildren.SelectedIndexChanged += DropDownNoOfChildren_SelectedIndexChanged;

                DropDownNoOfAdults.DataSource = Enumerable.Range(1, 4);
                DropDownNoOfAdults.DataBind();

                DropDownNoOfChildren.DataSource = Enumerable.Range(0, 11);
                DropDownNoOfChildren.DataBind();


                DropdownGuest.SelectedValue = (((RoomReservation)(e.Item.DataItem)).NoOfGuest).ToString();
                DropDownNoOfAdults.SelectedValue = (((RoomReservation)(e.Item.DataItem)).NoOfAdults).ToString();
                DropDownNoOfChildren.SelectedValue = (((RoomReservation)(e.Item.DataItem)).NoOfChildren).ToString();

                // Bind the Dropdown child
                BindChildAges(RepeaterChildAge, ((RoomReservation)(e.Item.DataItem)).NoOfChildren);

                // Bind Sitecore Error messages
                txtGuestNameRegularMessage = e.Item.FindControl("txtGuestNameRegularText") as Sitecore.Web.UI.WebControls.Text;
                txtGuestNameRequiredMessage = e.Item.FindControl("txtGuestNameRequiredText") as Sitecore.Web.UI.WebControls.Text;

            }
        }

        /// <summary>
        /// Use to bind the RepeaterChildAge when there is a record
        /// </summary>
        /// <param name="RepeaterChildAge"></param>
        /// <param name="childCount"></param>
        private void BindChildAges(Repeater RepeaterChildAge, int childCount)
        {
            List<int> childAges = new List<int>();

            ChildAgeList = BasketHelper.PopulateChildAgeFromDB(OrderLineId);
            ChildAgeList.Reverse();

            for (int i = 0; i < childCount; i++)
            {
                childAges.Add(i);
            }

            RepeaterChildAge.DataSource = childAges;
            RepeaterChildAge.DataBind();

            // ToDO: Write SP to get the current age for that particular child and set the seleted value

            if (RepeaterChildAge != null)
            {
                // use to iterate the ChildAgeList positions
                int count = 0;
                foreach (RepeaterItem ageDrpList in RepeaterChildAge.Items)
                {
                    //if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
                    //{
                    var ageDropdown = (DropDownList)ageDrpList.FindControl("DropDownAge");
                    if (ageDropdown != null)
                    {
                        if (ChildAgeList.Count > 0)
                        {
                            ageDropdown.SelectedIndex = ChildAgeList[count] - 1;
                        }
                        else
                        {
                            ageDropdown.SelectedIndex = 0;
                        }
                        // roomReservationAgeInfo.Add(new RoomReservationAgeInfo { Age = ageDropdown.SelectedIndex + 1, OrderLineId = Convert.ToInt32(orderLineId), RoomReservationId = roomReservationId });
                    }

                    count++;
                    //}
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DropDownNoOfChildren_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<int> childAges = new List<int>();
            DropDownList DropDownNoOfChildren = sender as DropDownList;


            // Bind Child age repeater with No of children selection
            for (int i = 0; i < Convert.ToInt32(DropDownNoOfChildren.SelectedValue); i++)
            {
                childAges.Add(i);
            }

            foreach (var item in DropDownNoOfChildren.Parent.Controls)
            {
                if (item.GetType().Name.Equals("Repeater"))
                {
                    if (item != null)
                    {
                        Repeater RepeaterChildAge = item as Repeater;
                        RepeaterChildAge.DataSource = childAges;
                        RepeaterChildAge.DataBind();
                    }
                }
            }
        }

        private void BindSitecoreTexts()
        {
            Item conditionalMessageItem = this.GetItemFromRenderingParameterPath("Conditional Messages Link");
            string ageofChildrenRequiredMessageFieldName = "Age of Children Required Message";
            string notValidCharacterMessageFieldName = "Not Valid Character Message";
            string guestNameRequiredMessageFieldName = "Guest Name Required Message";

            SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, ageofChildrenRequiredMessageFieldName, txtSelectChildAgeMessage);
            SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, notValidCharacterMessageFieldName, txtGuestNameRegularMessage);
            SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, guestNameRequiredMessageFieldName, txtGuestNameRequiredMessage);
        }
    }
}