﻿CREATE TABLE [dbo].[uCommerce_ProductTarget] (
    [ProductTargetId] INT            NOT NULL,
    [Skus]            NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_uCommerce_ProductTarget] PRIMARY KEY CLUSTERED ([ProductTargetId] ASC),
    CONSTRAINT [FK_uCommerce_ProductTarget_uCommerce_Target] FOREIGN KEY ([ProductTargetId]) REFERENCES [dbo].[uCommerce_Target] ([TargetId])
);

