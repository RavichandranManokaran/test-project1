﻿CREATE PROCEDURE [dbo].[LB_DeleteCustomerQueryAttachments]
(
	@QueryId int
) 
AS 
BEGIN
	DELETE FROM [dbo].[LB_CustomerQuery_Attachments] 
	WHERE QueryId = @QueryId
END
