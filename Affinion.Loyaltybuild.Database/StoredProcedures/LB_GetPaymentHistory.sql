﻿DROP PROCEDURE LB_GetPaymentHistory
GO

CREATE PROCEDURE [dbo].[LB_GetPaymentHistory]
	@OrderLineId INT	
AS
BEGIN
	--Get Order line detail
	SELECT OL.OrderLineId, 
		RR.OrderReference 
	FROM uCommerce_OrderLine OL
		INNER JOIN LB_RoomReservation RR ON OL.OrderLineId = RR.OrderLineId 
	WHERE OL.OrderLineId = @OrderLineId

	--Payment History Total
	SELECT SUM([Total]) [Total], [Currency] FROM
		(SELECT SUM(OLD.Amount) [Total],
				OLD.CurrencyTypeId [Currency]
		FROM uCommerce_OrderLine  OL
			INNER JOIN LB_OrderLineDue OLD ON OLD.OrderLineID = OL.OrderLineId 
		WHERE
			ISNULL(OLD.IsDeleted,0) = 0 AND
			OLD.OrderLineID IN (SELECT OrderLineID FROM [dbo].[LB_FnRelatedOrderLines](@OrderLineId))
		GROUP BY 
			OLD.CurrencyTypeId
		UNION ALL
		SELECT -SUM(OLP.Amount) [Total],
				OLP.CurrencyTypeId [Currency]
		FROM uCommerce_OrderLine  OL
			INNER JOIN LB_OrderLinePayment OLP ON OLP.OrderLineID = OL.OrderLineId 
		WHERE
			OLP.OrderLineID IN (SELECT OrderLineID FROM [dbo].[LB_FnRelatedOrderLines](@OrderLineId))
		GROUP BY 
			OLP.CurrencyTypeId) P
	GROUP BY [Currency]
	HAVING
		SUM([Total]) <> 0

	--PaymentHistory PaymentDues 
	SELECT	OLD.OrderLineDueId [OrderLineDueId],
			OLD.OrderLineID [OrderLineID],
			OLD.CurrencyTypeId [Currency],
			OLD.Amount [Amount],
			(SELECT max(Description) from uCommerce_ClientIDPaymentMethodMapping where clientid = ca.clientid and paymentmethodid = old.paymentmethodid) as [PaymentMethod], 
			--PM.Name [Name],
			DT.[Description] [Reason],
			OLD.CreatedDate [CreatedDate],
			OLD.CreatedBy [CreatedBy],
			DT.CanBeDeleted [CanBeDeleted]
	FROM LB_OrderLineDue OLD 
		INNER JOIN LB_DueTypes DT ON OLD.DueTypeId=dt.DueTypeId		
		inner join ucommerce_orderline ol on ol.orderlineid = old.OrderLineID
		inner join ucommerce_purchaseorder po on po.orderid = ol.OrderId 
		inner join LB_customerattribute ca on ca.customerid = po.customerid
		LEFT JOIN uCommerce_PaymentMethod PM ON OLD.PaymentMethodID = PM.PaymentMethodId
	WHERE 
		ISNULL(OLD.IsDeleted,0) = 0 AND
		OLD.OrderLineID IN (SELECT OrderLineID FROM [dbo].[LB_FnRelatedOrderLines](@OrderLineId))
	ORDER BY OLD.CreatedDate

	--Payment History Payments
	SELECT	OLP.OrderLinePaymentID [OrderLinePaymentId],
			OLP.OrderLineID [oderLineID],
			(SELECT max(Description) from uCommerce_ClientIDPaymentMethodMapping where clientid = ca.clientid and paymentmethodid = olp.paymentmethodid) as [PaymentMethod], 
			--PM.Name [Name],
			DT.[Description] [PaymentReason],
			OLP.Amount [Amount],
			OLP.ReferenceId [ReferenceId],
			OLP.CreatedDate [Createddate],
			OLP.CreatedBy [Createdby],
			OLP.CurrencyTypeId [Currency]			
	FROM LB_OrderLinePayment OLP
		INNER JOIN uCommerce_PaymentMethod PM ON PM.PaymentMethodId = OLP.PaymentMethodID
		inner join ucommerce_orderline ol on ol.orderlineid = olp.OrderLineID
		INNER JOIN LB_DueTypes DT ON dt.DueTypeId=OLP.DueTypeId	
		inner join ucommerce_purchaseorder po on po.orderid = ol.OrderId 
		inner join LB_customerattribute ca on ca.customerid = po.customerid
	WHERE
		OLP.OrderLineID IN (SELECT OrderLineID FROM [dbo].[LB_FnRelatedOrderLines](@OrderLineId))
	ORDER BY OLP.CreatedDate

	--Discount History
	SELECT [CampaignItemName] [Name], [AmountOffTotal] [Amount], PO.CurrencyId [Currency]
	FROM uCommerce_OrderLine OL
		INNER JOIN [dbo].[uCommerce_PurchaseOrder] PO ON PO.OrderId = OL.OrderId
		INNER JOIN [dbo].[uCommerce_OrderLineDiscountRelation] OLD ON OL.OrderLineId = OLD.OrderLineId
		INNER JOIN [dbo].[uCommerce_Discount] D ON OLD.DiscountId = D.DiscountId
	WHERE
		OL.OrderLineID IN (SELECT OrderLineID FROM [dbo].[LB_FnRelatedOrderLines](@OrderLineId))
	ORDER BY OL.CreatedOn
END 

GO
