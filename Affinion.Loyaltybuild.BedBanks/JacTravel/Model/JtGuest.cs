﻿using System;
using System.Xml.Serialization;

namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Model
{
    [Serializable]
    [XmlType(TypeName = "Guest")]
    public class JtGuest
    {
        public string Type { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Age { get; set; }
        public string DateOfBirth { get; set; }
        public string Nationality { get; set; }
    }
}
