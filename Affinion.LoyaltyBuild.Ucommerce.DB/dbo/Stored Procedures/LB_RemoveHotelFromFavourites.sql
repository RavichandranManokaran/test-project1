﻿
CREATE PROCEDURE [dbo].[LB_RemoveHotelFromFavourites] (
		@ListID int,
		@CustomerID int,
		@RemoveHotelId varchar(max) )
as
Declare
		@hotelId varchar(max)
Begin
		select @hotelId=HotelId from LB_FavoriteList where ListID=@ListID AND CustomerID=@CustomerID
		if(@hotelId is NOT NULL)
		begin
			update LB_FavoriteList set HotelID=Replace((select HotelID from LB_FavoriteList where ListID=@ListID AND CustomerID=@CustomerID), @RemoveHotelId,'') where ListID=@ListID
		End
End
		
