﻿CREATE TABLE [dbo].[uCommerce_CommunicationHistory]
(
	[HistoryId] INT NOT NULL PRIMARY KEY IDENTITY, 
    [CommunicationTypeId] INT NOT NULL, 
	[OrderLineId] INT NOT NULL, 
    [CreatedDate] DATETIME NOT NULL DEFAULT GETDATE(),
	[From] VARCHAR(100),
	[To] VARCHAR(100),
	[EmailSubject] NVARCHAR(250),
	[EmailText] NVARCHAR(MAX),
	[EmailHtml] NVARCHAR(MAX)
    CONSTRAINT [FK_uCommerce_CommunicationHistory_CommunicationTypes] FOREIGN KEY ([CommunicationTypeId]) REFERENCES [uCommerce_CommunicationTypes]([CommunicationTypeId]),
	CONSTRAINT [FK_uCommerce_CommunicationHistory_OrderLine] FOREIGN KEY ([OrderLineId]) REFERENCES [uCommerce_OrderLine]([OrderLineId])
)