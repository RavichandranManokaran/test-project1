﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           TripAdvisorSchedule.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.ExternalServices.ScheduleTasks.TripAdvisorSchedule
/// Description:           Called by Sitecore schedule for TripAdvisor

using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.ExternalServices.Services;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Affinion.LoyaltyBuild.ExternalServices.ScheduleTasks
{
    public class TripAdvisorSchedule
    {
        #region Public Methods

        /// <summary>
        /// Set TripAdvisor Id and set hotel details
        /// </summary>        
        public void SetTripAdvisorId()
        {
            try
            {
                Diagnostics.Trace(DiagnosticsCategory.ExternalServices, "SetTripAdvisorId() method started");

                Database master = Sitecore.Configuration.Factory.GetDatabase("master");

                //Item supplierSetup = master.GetItem("/sitecore/content/admin-portal/supplier-setup");
                Item supplierSetup = master.GetItem(Constants.SupplierSetupPath);

                IEnumerable<Item> hotels = supplierSetup.Axes.GetDescendants().Where(a => a.TemplateName.Equals("SupplierDetails"));

                foreach (Item hotel in hotels)
                {
                    try
                    {
                        Diagnostics.Trace(DiagnosticsCategory.ExternalServices, "Get TripAdvisor details for " + hotel.DisplayName);

                        UpdateSupplier(hotel);

                    }
                    catch (Exception ex)
                    {
                        Diagnostics.WriteException(DiagnosticsCategory.ExternalServices, ex, this);
                    }
                }

                Diagnostics.Trace(DiagnosticsCategory.ExternalServices, "SetTripAdvisorId() method ended");

            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ExternalServices, ex, this);
            }
        }

        private void UpdateSupplier(Item hotel)
        {
            //string tripAdvisorId = hotel.Fields["TripAdvisorId"].Value;
            string tripAdvisorId = SitecoreFieldsHelper.GetValue(hotel, "TripAdvisorId");

            if (string.IsNullOrWhiteSpace(tripAdvisorId))
            {
                SetHotelTripAdvisorId(hotel);
            }
            else
            {
                dynamic hotelRating = TripAdvisorService.GetHotelDetails(tripAdvisorId);

                SetHotelTripAdvisorDetails(hotel, hotelRating);
            }
        }
        #endregion

        #region Private Methods

        protected static void SetHotelTripAdvisorId(Item hotel)
        {
            //string latitudeAndLongitude = hotel.Fields["GeoCoordinates"].Value;
            //string hotelName = hotel.Fields["__Display name"].Value;

            string latitudeAndLongitude = SitecoreFieldsHelper.GetValue(hotel, "GeoCoordinates"); ;
            string hotelName = SitecoreFieldsHelper.GetValue(hotel, "__Display name");

            //string locationsRequest = CreateHotelMapperRequestUrl(latitudeAndLongitude, hotelName);
            dynamic locationResponse = TripAdvisorService.GetMappedHotels(latitudeAndLongitude, hotelName);

            if (locationResponse != null)
            {
                //Begin Editing Sitecore Item  
                using (new EditContext(hotel))
                {
                    hotel.Fields["TripAdvisorId"].Value = locationResponse.location_id;

                }

                //dynamic hotelRating = TripAdvisorService.GetHotelDetails(hotel.Fields["TripAdvisorId"].Value);
                dynamic hotelRating = TripAdvisorService.GetHotelDetails(SitecoreFieldsHelper.GetValue(hotel, "TripAdvisorId"));

                SetHotelTripAdvisorDetails(hotel, hotelRating);
            }
        }

        /// <summary>
        /// Set hotel trip advisor details.
        /// </summary>
        /// <param name="hotel">Hotel Sitecore item</param>
        /// <param name="hotelRating">Rating of supplier</param>
        protected static void SetHotelTripAdvisorDetails(Item hotel, dynamic hotelRating)
        {
            if (hotelRating != null)
            {
                //Begin Editing Sitecore Item  
                using (new EditContext(hotel))
                {
                    ///Set rating image
                    LinkField ratingImageLinkField = hotel.Fields["RatingImage"];
                    ratingImageLinkField.Url = hotelRating.rating_image_url;

                    ///Set rating image
                    LinkField hotelLinkField = hotel.Fields["HotelUrl"];
                    hotelLinkField.Url = hotelRating.web_url;

                    ///Set no of reviews
                    hotel.Fields["NoOfReviews"].Value = hotelRating.num_reviews;

                    ///Set rating
                    hotel.Fields["Rating"].Value = hotelRating.rating;
                }
            }
        }

        #endregion
    }
}
