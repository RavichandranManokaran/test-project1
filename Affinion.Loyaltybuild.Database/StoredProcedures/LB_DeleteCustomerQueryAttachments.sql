
/****** Object:  StoredProcedure [dbo].[LB_DeleteCustomerQueryAttachments]    Script Date: 2/20/2016 11:40:56 AM ******/
DROP PROCEDURE [dbo].[LB_DeleteCustomerQueryAttachments]

GO

/****** Object:  StoredProcedure [dbo].[LB_DeleteCustomerQueryAttachments]    Script Date: 2/20/2016 11:40:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 CREATE PROCEDURE [dbo].[LB_DeleteCustomerQueryAttachments]
(
	@QueryId int
) 
AS 
BEGIN
	DELETE FROM [dbo].[LB_CustomerQuery_Attachments] 
	WHERE QueryId = @QueryId
END

GO





