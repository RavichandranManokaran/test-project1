﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.Presentation.Web.Controls;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.CustomSiteCoreDataType
{
    public class LBDropDownListControlAdapter : IControlAdapter
    {
        public bool Adapts(System.Web.UI.Control control)
        {
            return (control is DropDownList);
        }

        public object GetValue(Control control)
        {
            DropDownList list = control as DropDownList;
            if (list == null)
            {
                return null;
            }
            return list.SelectedValue;
        }
    }
}
