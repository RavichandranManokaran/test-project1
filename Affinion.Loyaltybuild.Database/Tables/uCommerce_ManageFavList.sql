﻿CREATE TABLE [dbo].[uCommerce_ManageFavList](
	[ListID] [uniqueidentifier] NULL,
	[CustomerID] [int] NULL,
	[ListName] [varchar](100) NULL,
	[HotelID] [varchar](100) NULL,
	[CreatedBy] [varchar](100) NULL,
	[UpdateBy] [varchar](100) NULL,
	[DateCreated] [datetime] NULL,
	[DateLastUpdate] [datetime] NULL
)

GO
