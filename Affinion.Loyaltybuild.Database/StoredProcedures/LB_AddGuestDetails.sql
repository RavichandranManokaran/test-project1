﻿CREATE PROCEDURE [dbo].[LB_AddGuestDetails]
	@RoomReservationId as int,
	@OrderLineId as int,
	@NoOfGuest as int,
	@NoOfAdult as int,
	@NoOfChild as int,
	@GuestName as varchar(50),
	@SpecialRequest as varchar(150)

AS
BEGIN

IF EXISTS (SELECT 1 FROM dbo.LB_RoomReservation WHERE OrderLineId = @OrderLineId and RoomReservationId = @RoomReservationId)
	BEGIN
	  UPDATE [dbo].[LB_RoomReservation]
	   SET --[OrderLineId] = @OrderLineId
		  [NoOfGuest] = @NoOfGuest
		  ,[GuestName] = @GuestName
		  ,[NoOfAdult] = @NoOfAdult
		  ,[NoOfChild] = @NoOfChild
		  ,[SpecialRequest] =@SpecialRequest
	  WHERE RoomReservationId = @RoomReservationId
	END
END
