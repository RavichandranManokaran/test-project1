﻿CREATE TABLE [dbo].[uCommerce_DefinitionTypeDescription] (
    [DefinitionTypeDescriptionId] INT            IDENTITY (1, 1) NOT NULL,
    [DefinitionTypeId]            INT            NOT NULL,
    [DisplayName]                 NVARCHAR (512) NOT NULL,
    [Description]                 NVARCHAR (MAX) NULL,
    [CultureCode]                 NVARCHAR (60)  NOT NULL,
    CONSTRAINT [PK_DefinitionTypeDescription] PRIMARY KEY CLUSTERED ([DefinitionTypeDescriptionId] ASC),
    CONSTRAINT [FK_DefinitionTypeDescription_uCommerce_DefinitionType] FOREIGN KEY ([DefinitionTypeId]) REFERENCES [dbo].[uCommerce_DefinitionType] ([DefinitionTypeId])
);

