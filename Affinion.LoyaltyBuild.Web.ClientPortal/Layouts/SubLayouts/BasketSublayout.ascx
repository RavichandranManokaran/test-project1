﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BasketSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.BasketSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Sitecore.Data.Items" %>
<%@ Import Namespace="Affinion.LoyaltyBuild.Common.SessionHelper" %>
<%@ Import Namespace="Affinion.LoyaltyBuild.Common.SessionHelper" %>

<div class="content">
    <div class="content-top">
        <div class="col-sm-12">
            <div class="checkout-wrap">
                <div class="step">
                    <ul class="checkout-bar">
                        <li class="visited first">
                            <a href="#">
                                <sc:text id="OfferSelected" runat="server" />
                            </a>
                        </li>
                        <li class="visited Second">
                            <sc:text id="BookingDetails" runat="server" />
                            </span></li>
                        <li class="next">
                            <sc:text id="ReservationDetails" runat="server" />
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="content-body basket-info">
        <div class="accordian-outer">
            <div class="panel-group" id="basket-details">
                <div class="alert alert-danger msg-margin-top" id="DivEmptyBasket" runat="server" visible="false">
                    <asp:Label ID="EmptyBasket" runat="server" Text=""></asp:Label>
                </div>
                <asp:Repeater ID="PanelRepeater" runat="server" OnItemDataBound="PanelRepeater_ItemDataBound">
                    <ItemTemplate>
                        <div class="panel panel-default" id="hotel-<%#Eval("Sku")%>">
                            <div class="panel-heading bg-primary">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href='#<%# Eval( "SitecoreItem.Name") + "" +  Eval("Sku")%>' class="show-hide-list">
                                        <div id="LBNameDiv" class="pull-left" runat="server">
                                            <sc:text item="<%#(Item) ((BasketInfo)Container.DataItem).SitecoreItem%>" id="PanelTitle" field="Name" runat="server" />
                                        </div>
                                        <div id="BBNameDiv" class="pull-left" runat="server">
                                            <asp:Literal ID="BBName" Text='<%#Eval("BBPropertyName") %>' runat="server"></asp:Literal>
                                        </div>
                                    </a>
                                </h4>
                            </div>

                            <div id='<%# Eval( "SitecoreItem.Name") + "" +  Eval("Sku")%>' class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div>
                                        <div class="col-sm-12 bask-title">
                                            <div>
                                                <div class="hotel-name-detail">
                                                    <h2 class="no-margin">
                                                        <div id="LBHotelNamediv" runat="server">
                                                            <sc:text item="<%#(Item) ((BasketInfo)Container.DataItem).SitecoreItem %>" id="HotelName" field="Name" runat="server" />
                                                            <sc:text item="<%#(Item) ((BasketInfo)Container.DataItem).SitecoreItem %>" id="StarRanking" field="StarRanking" runat="server" visible="False" />
                                                        </div>
                                                        <div id="BBHotelNamediv" runat="server">
                                                            <asp:Label ID="BBHotelName" Text='<%# Eval("BBPropertyName") %>' runat="server"></asp:Label>
                                                        </div>
                                                        <asp:Repeater ID="StarRepeater" runat="server">
                                                            <ItemTemplate>
                                                                <span class="fa fa-star"></span>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </h2>
                                                    <div class="hotel-name-detail-bottom">
                                                        <span>
                                                            <sc:text id="LocationDesc" field="TextLocationDescription" runat="server" />
                                                            <sc:text item="<%# (Item) ((BasketInfo)Container.DataItem).SitecoreItem %>" id="Rating" field="Rating" runat="server" />
                                                            <sc:text id="FromText" field="TextFrom" runat="server" />
                                                            <sc:text item="<%# (Item) ((BasketInfo)Container.DataItem).SitecoreItem %>" id="NoOfReviews" field="NoOfReviews" runat="server" />
                                                            <sc:text id="ReviewsText" field="TextGuestReviews" runat="server" />
                                                        </span>
                                                        <p>
                                                            <span class="fa fa-map-marker"></span>
                                                            <span><%#((BasketInfo)Container.DataItem).AddressInfo %></span>
                                                            <span><%#((BasketInfo)Container.DataItem).Country %></span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <asp:Repeater ID="BookingDetailsRepeater" runat="server" OnItemDataBound="BookingDetailsRepeater_ItemDataBound">
                                            <ItemTemplate>
                                                <div class="col-xs-12" data-name="booking" id="booking-<%#Eval("OrderLineId")%>">
                                                    <div class="row">
                                                        <div class="content_bg_inner_box alert-info booking-details">
                                                            <div class="col-xs-12 col-md-5">
                                                                <div id="bbImageDiv" class="basket-img" runat="server">
                                                                    <asp:Image ID="imgBBHotel" ImageUrl='<%#Eval("BBImageUrl") %>' runat="server" />
                                                                </div>

                                                                <div class="basket-img" id="lbImageDiv" runat="server">
                                                                    <sc:image item="<%# (Item) ((BasketInfo)Container.DataItem).SitecoreItem %>" id="HotelImage" field="IntroductionImage" runat="server" cssclass="img-responsive img-thumbnail" />
                                                                </div>
                                                                <div>
                                                                    <a class="gray-color pull-left msg-margin-top display-block-val" href="javascript:void(0);" data-remove="<%#((BasketInfo)Container.DataItem).OrderLineId %>">
                                                                        <sc:text id="removeText" field="RemoveBasketLabel" runat="server" class="gray-color" />
                                                                    </a>

                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-7">
                                                                <div class="content_bg_inner_box alert-info booking-details">
                                                                    <div class="col-md-12">
                                                                        <div class="title">
                                                                            <h2 class="no-margin">
                                                                                <sc:text id="BookingDetailsText" field="TextBookingDetail" runat="server" />
                                                                            </h2>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12">
                                                                        <div class="col-sm-4 col-xs-6 no-padding">
                                                                            <span class="tit accent33-f">
                                                                                <sc:text id="CheckinText" field="TextCheckin" runat="server" />
                                                                            </span>
                                                                        </div>
                                                                        <div class="col-sm-8 col-xs-6 no-padding">
                                                                            <span>
                                                                                <%#((BasketInfo)Container.DataItem).CheckinDate %>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12">
                                                                        <div class="col-sm-4 col-xs-6 no-padding">
                                                                            <span class="tit accent33-f">
                                                                                <sc:text id="CheckoutText" field="TextCheckout" runat="server" />
                                                                            </span>
                                                                        </div>
                                                                        <div class="col-sm-8 col-xs-6 no-padding">
                                                                            <span><%#((BasketInfo)Container.DataItem).CheckoutDate %></span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12">
                                                                        <div class="col-sm-4 col-xs-6 no-padding">
                                                                            <span class="tit accent33-f">
                                                                                <sc:text id="NightsText" field="TextNights" runat="server" />
                                                                            </span>
                                                                        </div>
                                                                        <div class="col-sm-8 col-xs-6 no-padding">
                                                                            <span><%#((BasketInfo)Container.DataItem).Nights %></span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12">
                                                                        <div class="col-sm-4 col-xs-6 no-padding">
                                                                            <span class="tit accent33-f">
                                                                                <sc:text id="RoomTypeText" field="TextRoomType" runat="server" />
                                                                            </span>
                                                                        </div>
                                                                        <div class="col-sm-8 col-xs-6 no-padding">
                                                                            <span><%#((BasketInfo)Container.DataItem).RoomType %></span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12">
                                                                        <div class="col-sm-4 col-xs-6 no-padding">
                                                                            <span class="tit accent33-f">
                                                                                <sc:text id="PriceText" field="TextPrice" runat="server" />
                                                                            </span>
                                                                        </div>
                                                                        <div class="col-sm-2 col-md-2 no-padding marR10" id="PriceDiv" runat="server">
                                                                            <asp:Literal ID="Price" runat="server" />
                                                                        </div>
                                                                        <div id="BBNewPrice" class="pull-left" runat="server">
                                                                            <span>
                                                                                <asp:Literal ID="NewPrice" runat="server" /></span>
                                                                        </div>
                                                                        <div id="NewPriceInfo" runat="server" class="col-sm-12 col-md-12 no-padding font11"><asp:literal ID="litPriceInfo" runat="server"></asp:literal></div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-xs-12 no-padding">
                                                                    <div class="col-xs-5 no-padding">
                                                                        <button id="ButtonSetAccomodation" type="button" onclick="OpenPopup( '<%#((BasketInfo)Container.DataItem).OrderLineId %>' , '<%#((BasketInfo)Container.DataItem).NoOfRooms %>')" data-olid="<%#((BasketInfo)Container.DataItem).OrderLineId %>" class="btn btn-default add-break margin-3 buttonaccomodation">
                                                                            <span class="fa fa-unlink"></span>
                                                                            <sc:text id="SetAccBtn" field="BtnSetAccommodation" runat="server" />
                                                                        </button>
                                                                         <span class="accent58-f">*</span>
                                                                    </div>
                                                                    <div class="col-xs-7 no-padding">
                                                                        <input type="checkbox" <%#(string.IsNullOrEmpty(((BasketInfo)Container.DataItem).GuestName)?"":"checked='checked'") %> data-validate="accomodation" data-olid="<%#((BasketInfo)Container.DataItem).OrderLineId %>" style="display: none;" />
                                                                        <span style="display: none;" class="accmdt" id="spanAccomodationValidation" data-olid="<%#((BasketInfo)Container.DataItem).OrderLineId %>"><%=LabelAccomodationValidation%></span>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <%--Errata feed start--%>
                                                                <div class="col-xs-12" id="errataInfoDiv" runat="server">
                                                                    <div class="row">
                                                                        <div class="content_bg_inner_box alert-warning">
                                                                            <div class="col-md-12">
                                                                           <asp:Repeater ID="ErrataRepeater" runat="server" OnItemDataBound="ErrataRepeater_ItemDataBound">
                                                                                <ItemTemplate>
                                                                                    <div class="col-md-3 no-padding"><asp:Literal ID="BBerrataSubject" runat="server"></asp:Literal></div>
                                                                                    <div class="col-md-9 no-padding"><asp:Literal ID="BBerrataDescription" runat="server"></asp:Literal></div>
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                            </div>
                                                                            <div class="col-md-12">
                                                                                 <div class="col-md-3 no-padding"><span><asp:literal ID="cancellationHeader" runat="server"></asp:literal></span></div>
                                                                                 <div class="col-md-9 no-padding">
                                                                                    <asp:Repeater ID="CancellationCriteriaRepeater" runat="server" OnItemDataBound="CancellationCriteriaRepeater_ItemDataBound">
                                                                                        <ItemTemplate>                                                               
                                                                                        <span><asp:Literal ID="cancelCriteria" runat="server"></asp:Literal></span>
                                                                                        </ItemTemplate>
                                                                                    </asp:Repeater>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12 required"><asp:literal ID="nonTransferableText" runat="server"></asp:literal></div>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <%--Errata feed End--%>
                                                            
                                                        </div>

                                                        <div class="col-xs-12" id="paymentType" runat="server" visible="false">
                                                            <div class="padding-box accent54-f alert  alert-success  inline-block width-full no-margin" runat="server">
                                                                <div class="row">
                                                                    <div class="col-xs-12"><span>Please Choose Your preferred payment option below</span></div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-3 col-xs-12">
                                                                        <label class="radio-inline font-bold-600 ">

                                                                            <input type="radio" name="optradio">Full Payment</label>
                                                                    </div>
                                                                    <div class="col-md-9 col-xs-12"><span>( Total price payable today )</span></div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-3 col-xs-12">
                                                                        <label class="radio-inline font-bold-600 ">
                                                                            <input type="radio" name="optradio">Partial Payment</label>
                                                                    </div>
                                                                    <div class="col-md-9 col-xs-12"><span>( Approximately 25% payable today with the due 35 days prior to arrival ) </span></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>


                                            </ItemTemplate>
                                        </asp:Repeater>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>


                <div class="col-xs-12 no-padding martop10" id="btn-continue" style="margin-bottom: 10px;">
                    <asp:Button ID="ButtonAddAnotherBreak" OnClick="ButtonAddAnotherBreak_Click" class="btn btn-default pro-check" runat="server" Text="Continue Shopping" CausesValidation="False" />
                </div>
                <div class="col-xs-12 no-padding" id="btn-payment">
                    <div class="col-md-5 no-padding">
                        <input type="hidden" id="hdnPaymentMethod" runat="server" clientidmode="static" />
                        <div id="showPaymentMethod" clientidmode="static" runat="server" class="padding-box accent54-f alert  alert-success  inline-block width-full no-margin" style="display: none;" visible="false">
                            <div class="col-xs-12 no-padding">
                                <span>
                                    <sc:text id="PaymentTypeText" runat="server" />
                                </span>
                            </div>
                            <div class="col-xs-12 no-padding">
                                <input type="radio" id="rbtFull" runat="server" groupname="Payment" onserverchange="RbtPayment_CheckedChanged" /><sc:text id="FullText" runat="server" /><br />
                                <span>
                                    <sc:text id="PayableTodayText" runat="server" />
                                </span>
                            </div>
                            <div class="col-xs-12 no-padding">
                                <input type="radio" id="rbtPartial" runat="server" groupname="Payment" onserverchange="RbtPayment_CheckedChanged"><sc:text id="PartialText" runat="server" /><br />
                                <span id="partial-payment-selection-label">
                                    <asp:Literal ID="LitInst" runat="server" />
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-6 no-padding" id="showPaymentDetails" runat="server">
                        <div class="content_bg_inner_box no-margin alert-info">
                            <div class="col-xs-12" id="ProcessingFeeRow" runat="server">
                                <div class="col-xs-8 no-padding">
                                    <span class="accent33-f">
                                        <sc:text id="ProcessingFeeText" runat="server" />
                                    </span>
                                </div>
                                <div class="col-xs-4 no-padding">
                                    <span class="pull-right" id="order-pro-fee">
                                        <asp:Literal ID="ProcessingFee" runat="server" />
                                    </span>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="col-xs-8 no-padding">
                                    <span class="accent33-f">
                                        <sc:text id="TotalPayableTodayText" runat="server" />
                                    </span>
                                </div>
                                <div class="col-xs-4 no-padding">
                                    <span class="pull-right" id="order-payable-today">
                                        <asp:Literal ID="BookingDeposit" runat="server" />
                                    </span>
                                    <%--<span class="pull-right">€ 00.00</span>--%>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="col-xs-8 no-padding">
                                    <span class="accent33-f">
                                        <sc:text field="AccommodationPayableText" id="AccommodationPayableTextlabel" runat="server" />
                                    </span>
                                    <span class="accent33-f" id="order-payable-acc-label">
                                        <asp:Literal ID="PayableAtAccommodationText" runat="server" />
                                    </span>
                                </div>
                                <div class="col-xs-4 no-padding">
                                    <span class="pull-right" id="order-payable-acc">
                                        <asp:Literal ID="TotalpayableAtAccommodation" runat="server" />
                                    </span>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="col-xs-8 no-padding">
                                    <span class="accent33-f font-bold-600">
                                        <sc:text id="TotalPriceText" runat="server" />
                                    </span>
                                </div>
                                <div class="col-xs-4 no-padding pull-right">
                                    <span class="pull-right font-bold-600" id="order-total">
                                        <asp:Literal ID="TotalPrice" runat="server" />
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="dialog-confirm">
                    <p><%=ConfirmationMessage %></p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="no-room" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><span class="fa fa-group"></span>
                    <sc:text id="GreatChoiceText" runat="server" />
                </h4>
            </div>
            <div class="modal-body row">
                <div class="no-room inline-block width-full col-md-12">
                    <asp:Repeater ID="RepeaterGuestAccordian" runat="server">
                        <ItemTemplate>
                            <div class="col-md-6 col-xs-12">
                                <div class="content_bg_inner_box alert-info ">
                                    <div class="room-title">
                                        <h4 class="no-margin">
                                            <sc:text id="Room1Text" field="TextRoom" runat="server" />
                                        </h4>
                                    </div>
                                    <span>
                                        <sc:text id="GuestText" field="TextGuest" runat="server" />
                                    </span>
                                    <select class="form-control">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                    </select>
                                    <span>
                                        <sc:text id="FullGuestNameText" field="TextFullguestName" runat="server" />
                                    </span>
                                    <input type="text" placeholder="Full Guest Name" class="form-control">
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>

            </div>
            <div class="modal-footer">
                <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="btn btn-default" />
                <asp:Button ID="ButtonClose" runat="server" Text="Close" CssClass="btn btn-default" />
            </div>
        </div>
    </div>
</div>


<script lang="javascript" type="text/javascript">
    function OpenPopup(OrderlineId, NoOfRooms) {
        var GuestRange = "<%=GuestRange %>";
        window.open("/set-accommodation?Rooms=" + NoOfRooms + "&GuestRange=" + GuestRange + "&orderid=" + OrderlineId + "", "List", "toolbar=no, location=no,status=yes,menubar=no,scrollbars=yes,resizable=no, width=900,height=600,left=430,top=100");
        return false;
    }

    jQuery(function ($) {
        var olid;
        $("#dialog-confirm").dialog({
            resizable: false,
            autoOpen: false,
            modal: true,
            buttons: {
                "Yes": function () {
                    RemoveBasketItem();
                    $(this).dialog("close");
                },
                "No": function () {
                    $(this).dialog("close");
                }
            }
        });
        $("#rbtFull,#rbtPartial").change(function (e) {
            if ($(this).is(':checked')) {
                $("#hdnPaymentMethod").val($(this).val());
                UpdatePayment()
            }
            e.preventDefault();
            e.stopPropagation();
        })
        function RemoveBasketItem() {
            $.get("/handlers/cs/baskethelper.ashx?action=removeitem&olid=" + olid, function (data) {
                if (data.Success) {
                    RemoveItem();
                    SetupPartialPayment();
                }
            });
        }
        function SetupPartialPayment() {
            $.get("/handlers/cs/baskethelper.ashx?action=partialsettings", function (data) {
                if (data != null && data.IsPartial) {
                    $("#showPaymentMethod").show();
                    if ($("#hdnPaymentMethod").val() == "")
                        $("#hdnPaymentMethod").val(data.DefaultPayment);
                    $("#partial-payment-selection-label").html(data.PartialPaymentText);
                    $("#order-payable-acc-label").html(data.AccomodationPaymentText);
                }
                UpdatePayment();
            });
        }
        function UpdatePayment(typ) {
            var paymentMethod = ($("#hdnPaymentMethod").val() != "") ? "&method=" + $("#hdnPaymentMethod").val() : "";
            var paymentType = (typ != null) ? "&type=" + typ : "";
            $.get("/handlers/cs/baskethelper.ashx?action=calculatepayment" + paymentMethod + paymentType, function (data) {
                if (data != null) {
                    $("#order-pro-fee").html(data.ProcessingFee);
                    $("#order-payable-today").html(data.TotalPayableToday);
                    $("#order-payable-acc").html(data.TotalPayableAtAccommodation);
                    $("#order-total").html(data.TotalPrice);
                }
            });
        }
        $(".ui-dialog-titlebar").hide();
        $("a[data-remove]").click(function () {
            olid = $(this).data("remove");
            $("#dialog-confirm").dialog("open");
        });
        $('.sec-payment').click(function (e) {
            var result = SetAccomodationValidation();
            if (!Page_ClientValidate("EmailValidationGroup") || !result) {

                for (var i = 0; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $("#" + Page_Validators[i].controltovalidate)
                         .addClass("has-error-input");
                    }
                    else {
                        $("#" + Page_Validators[i].controltovalidate)
                         .removeClass("has-error-input");
                    }
                }
                e.preventDefault();
                return false;
            }
        });

        $('.ContinueToSecurePaymentButton').click(function (e) {
            var result = SetAccomodationValidation();
            if (!Page_ClientValidate("CustomerDetailsValidation") || !result) {

                for (var i = 0; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $("#" + Page_Validators[i].controltovalidate).addClass('has-error-input');
                    }
                    else {

                        if ($("#" + Page_Validators[i].controltovalidate).val().length > 0)
                            $("#" + Page_Validators[i].controltovalidate).removeClass("has-error-input");
                    }
                }
                e.preventDefault();
                return false;
            }
        });
        function SetAccomodationValidation() {
            var count = 0
            $('input[data-olid]').each(function () {
                var olid = $(this).data("olid");
                if (!$(this).prop("checked")) {
                    count++;
                    $("span[data-olid='" + olid + "']").css("display", "block");
                }
            });
            return count == 0;
        }
        function RemoveItem() {
            $(".basket-count").html($("div[data-name='booking']").length - 1);

            if ($("div[data-name='booking']").length == 1) {
                $("#basket-details").empty();
                $("#btn-continue").empty();
                $("#btn-payment").empty();
                $(".booking-detail-info").empty();
                return;
            }
            if ($("#booking-" + olid).siblings("div[data-name='booking']").length == 0) {
                var panel = $("#booking-" + olid).closest('.panel');
                panel.hide("fast", function () {
                    panel.empty();
                    panel.remove();
                });
                return;
            }
            var booking = $("#booking-" + olid);
            booking.hide("fast", function () {
                booking.empty();
                booking.remove();
            });
        }

        $('input[data-olid]').each(function () {
            var CurrentOlid = $(this).data("olid");
            if ($(this).prop("checked")) {
                $(".buttonaccomodation[data-olid='" + CurrentOlid + "']").css("background-color", "#428243");
                $("span[data-olid='" + olid + "']").css("display", "none");
            }
        });
    });
</script>
