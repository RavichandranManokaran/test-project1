﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OracleSalesInvoicesReport.ascx.cs" Inherits="Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.Reports.OracleSalesInvoicesReport" %>
<div class="container-outer width-full overflow-hidden-att app-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div>
					<h3 class="title-text font-weight-bold">Oracle Sales Invoices</h3>
				</div>   
			</div>
		</div>
		<div class="middle-body-container" style="padding:0;">
			<div class="row">
				<div class="col-sm-12">
					<div class="accordian-outer">
						<div class="panel-group" id="accordion1">
							<div class="panel panel-default">
								<div class="panel-heading bg-primary">
									<h4 class="panel-title">
									    <span>Oracle Sales Invoices : <asp:Label ID="lblTotalRecords" runat="server" Text="0"></asp:Label> records(s)</span>
									<a data-toggle="collapse" data-parent="#accordion1" href="#collapse0" class="show-hide-list">
									<span class="glyphicon glyphicon-triangle-top pull-right"></span>
									</a>
									</h4>
								</div>
								<div id="collapse0" class="panel-collapse collapse in">
									<div class="panel-body">
										<div class="row">
                                            <div class="col-xs-12"> 
												<div class="col-xs-12 no-padding">
													<div class="col-xs-3 no-padding-left">
														<div><b>Date Range:</b></div>
														<div>
															<div>
																 <input type="text" id="DateRange" runat="server" clientidmode="Static" class="form-control datepicker font-size-12" tabindex="1" />
															</div>
														</div>
													</div>
													<div class="col-xs-3 no-padding-left">
														<div><b>Currency:</b></div>
														<div>
															<asp:DropDownList ID="Currency" ClientIDMode="Static" runat="server" CssClass="form-control font-size-12 dropdown-field" TabIndex="6">
                                                            </asp:DropDownList> 
														</div>
													</div>
                                                    <div class="col-xs-3 no-padding">
														<div><b>Payment Status:</b></div>
														<div>
															<asp:DropDownList ID="PaymentStatus" ClientIDMode="Static" runat="server" CssClass="form-control font-size-12 dropdown-field" TabIndex="6">
                                                                <asp:ListItem>Select Payment Status</asp:ListItem>
                                                                <asp:ListItem Value="1">Paid</asp:ListItem>
                                                                <asp:ListItem Value="2">UnPaid</asp:ListItem>
                                                                 <asp:ListItem Value="3">Pending</asp:ListItem>
                                                            </asp:DropDownList> 
														</div>
													</div>
													<div class="col-xs-3 no-padding-right"> 
														<div><b>&nbsp;</b></div>
														<div><asp:Button ID="OracleSalesInvoice" class="btn btn-primary"  ClientIDMode="Static" Text="Get Oracle Sales Invoice" runat="server" OnClick="OracleSalesInvoice_Click"/></div>
													</div>
												</div>
											</div>
											
										</div>
										<div class="row">
											<div class="col-xs-12 marT15"> 
												<div class="col-xs-9 no-padding"> <label class="pull-right"><b>Search:</b></label></div>
												<div class="col-xs-3 no-padding-right">
													<input type="text" class="form-control" placeholder="search"/>
												</div>
											</div>
											<div class="col-xs-12 marT15">
												<div class="row" id="htmlReportGrid" runat="server">
                                                    <div class="col-sm-12">
                                                        <div>
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading claim_titl">
                                                                    <h4 class="panel-title"><span>Oracle Sales Invoices</span> reports.
                                                                    </h4>
                                                                </div>
                                                                <div id="totalCount" runat="server" class="totalCountpad"><span>Oracle Sales Invoices</span>
									                                details reports Total of <span><%= TotalOfRecords %> </span>record(s) found. </div>
                                                                <div class="panel-body alert-info">
                                                                    <%-- Result Grid--%>
                                                                    <div runat="server" id="ResultDiv" clientidmode="Static">
                                                                        <div id="data-grid" class="table-responsive">
                                                                            <asp:GridView ID="ResultGridView" runat="server" Width="100%" SelectedRowStyle-Font-Bold="true" class="table table-striped table-bordered" AutoGenerateColumns="False" AllowPaging="true" PageSize="10"
                                                                                PagerSettings-NextPageText=" Next " PagerSettings-PreviousPageText=" Previous " PagerSettings-Mode="NumericFirstLast" PagerSettings-FirstPageText=" First " PagerSettings-LastPageText=" Last " PagerSettings-Position="Bottom" PagerStyle-Font-Bold="true" PagerStyle-CssClass="pagination-grid">
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="Supplier ID">
                                                                                        <ItemTemplate>
                                                                                           <asp:Label ID="lblSupplierID" runat="server" Text='<%# Bind("SupplierID") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Provider Name">
                                                                                        <ItemTemplate>
                                                                                           <asp:Label ID="lblProviderName" runat="server" Text='<%# Bind("ProviderName") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Demand Date">
                                                                                        <ItemTemplate>
                                                                                           <asp:Label ID="lblDemandDate" runat="server" Text='<%# Bind("DemandDate") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Booking Identifier">
                                                                                        <ItemTemplate>
                                                                                            <table style="width: 100%">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblBookingIdentifier" runat="server" Text='<%# Bind("BookingIdentifier") %>'></asp:Label></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblBookingRef" runat="server" Text='<%# Bind("BookingRef") %>'></asp:Label></td>
                                                                                                </tr>
                                                                                               
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Guest Name">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblGuestName" runat="server" Text='<%# Bind("GuestName") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Arrival Date">
                                                                                        <ItemTemplate>
                                                                                           <asp:Label ID="lblArrivalDate" runat="server" Text='<%# Bind("ArrivalDate") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Departure Date">
                                                                                        <ItemTemplate>
                                                                                           <asp:Label ID="lblDepartureDate" runat="server" Text='<%# Bind("DepartureDate") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Paid in Oracle">
                                                                                        <ItemTemplate>
                                                                                           <asp:Label ID="lblPaidInOracle" runat="server" Text='<%# Bind("PaidInOracle") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                     <asp:TemplateField HeaderText="Gross Amount">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblGrossAmount" runat="server" Text='<%# Bind("GrossAmount") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Suggest Amount">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblSuggestAmount" runat="server" Text='<%# Bind("SuggestAmount") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                     <asp:TemplateField HeaderText="Posted Amount">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblPostedAmount" runat="server" Text='<%# Bind("PostedAmount") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                      <asp:TemplateField HeaderText="Date Posted to Oracle">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblDatePostedToOracle" runat="server" Text='<%# Bind("DatePostedToOracle") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                      <asp:TemplateField HeaderText="Date Posted to Oracle">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblDatePostedToOracle" runat="server" Text='<%# Bind("DatePostedToOracle") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField>
                                                                                        <HeaderTemplate>
                                                                                            <table style="width: 100%">
                                                                                                <tr>
                                                                                                    <td colspan="2">
                                                                                                        <asp:Label ID="lblPost" runat="server" Text="Post"></asp:Label></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblSelectAll" runat="server" Text="Select All:"></asp:Label></td>
                                                                                                    <td>
                                                                                                        <asp:CheckBox ID="chkSelectAll" runat="server" />

                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </HeaderTemplate>
                                                                                        <ItemTemplate>
                                                                                            <table style="width: 100%">
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:CheckBox ID="chkSelect" runat="server" /></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td>
                                                                                                        <asp:Label ID="lblPostedDate" runat="server" Text='<%# Bind("DatePostedToOracle") %>'></asp:Label></td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                     <asp:TemplateField HeaderText="Oracle File">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblOracleFileID" runat="server" Text='<%# Bind("OracleFileID") %>'></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>

                                                                                <EmptyDataTemplate>
                                                                                    ---No Records---
                                                                                </EmptyDataTemplate>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
											</div>
                                             <div class="col-xs-12">
                                                <asp:Button type="button" ID="PostToOracle" runat="server" ClientIDMode="Static" Text="Post Selected Oracle Sales Invoice" class="btn btn-primary pull-right" TabIndex="7" OnClick="PostToOracle_Click" />
                                            </div>
										</div>
                                        
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div> 
		</div> 
	</div>
</div>
													
<script type="text/javascript">
    $(document).ready(function () {
        $('input[id="DateRange"]').daterangepicker();
 });
 </script>