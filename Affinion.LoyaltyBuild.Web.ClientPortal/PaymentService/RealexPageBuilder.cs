﻿//<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           RealexPageBuilder.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal
/// Description:           Build POST request to payment gateway
/// </summary>

using Affinion.Loyaltybuild.BusinessLogic.Helper;
using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UCommerce.EntitiesV2;
using UCommerce.Transactions.Payments;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Exceptions;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.PaymentService
{
    public class RealexPageBuilder : AbstractPageBuilder
    {
        private string autoSettleFlag;

        #region Public properties
        public string GatewayUrl { get; set; }
        public string MerchantId { get; set; }
        public string Secret { get; set; }
        public string CurrentTimeStamp { get; set; }
        public string OrderGuidId { get; set; }
        public string Account { get; set; }
        public string PaymentCurrency { get; set; }
        public string AmountToPay { get; set; }
        public string ShaHash { get; set; }
        public string ResponseUrl { get; set; }
        public string AnythingElse { get; set; }
        public string AutoSettleFlag 
         {
            get
            {
                return autoSettleFlag;
            }
            set
            {
                autoSettleFlag = string.Equals(value, "True") ? "1" : "0";
            }
        }
        #endregion

        protected override void BuildBody(System.Text.StringBuilder page, PaymentRequest paymentRequest)
        {
            InitializeSettings(paymentRequest);

            string formStart = "<form method=\"POST\" action=\"" + GatewayUrl + "\" id=\"realexPayment\" name=\"realex\">";
            page.Append(formStart);

            string merchantId = "<input type=\"hidden\" name=\"MERCHANT_ID\" value=\"" + MerchantId + "\" />";
            page.Append(merchantId);

            string orderId = "<input type=\"hidden\" name=\"ORDER_ID\" value=\"" + OrderGuidId + "\" />";
            page.Append(orderId);

            string account = "<input type=\"hidden\" name=\"ACCOUNT\" value=\"" + Account + "\" />";
            page.Append(account);

            string currency = "<input type=\"hidden\" name=\"CURRENCY\" value=\"" + PaymentCurrency + "\" />";
            page.Append(currency);

            string amount = "<input type=\"hidden\" name=\"AMOUNT\" value=" + AmountToPay + " />";
            page.Append(amount);

            string timeStamp = "<input type=\"hidden\" name=\"TIMESTAMP\" value=\"" + CurrentTimeStamp + "\" />";
            page.Append(timeStamp);

            string sha1Hash = "<input type=\"hidden\" name=\"SHA1HASH\" value=\"" + ShaHash + "\" />";
            page.Append(sha1Hash);

            string responseUrl = "<input type=\"hidden\" name=\"MERCHANT_RESPONSE_URL\" value=\"" + ResponseUrl + "\" />";
            page.Append(responseUrl);

            string anythingElse = "<input type=\"hidden\" name=\"ANYTHING ELSE\" value=" + AnythingElse + " />";
            page.Append(anythingElse);

            string autoSettleFlag = "<input type=\"hidden\" name=\"AUTO_SETTLE_FLAG\" value=" + AutoSettleFlag + " />";
            page.Append(autoSettleFlag);

            string formSubmit = "<script>document.getElementById('realexPayment').submit();</script>";
            page.Append(formSubmit);

            string formEnd = "</form>";
            page.Append(formEnd);
        }

        public override string Build(PaymentRequest paymentRequest)
        {
            try
            {
                return base.Build(paymentRequest);
            }
            catch (Exception exception)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, exception.Message);
                throw new AffinionException("Error in building Realex payment page", exception);
            }
        }

        /// <summary>
        /// Initialize POST parameters to load payment gateway
        /// </summary>
        /// <param name="paymentRequest">Current payment request with order details</param>
        private void InitializeSettings(PaymentRequest paymentRequest)
        {
            var paymentMethodProperties = paymentRequest.PaymentMethod.PaymentMethodProperties;

            GatewayUrl = paymentMethodProperties.FirstOrDefault(x => x.DefinitionField.Name == "GatewayUrl").Value;
            MerchantId = paymentMethodProperties.FirstOrDefault(x => x.DefinitionField.Name == "MerchantId").Value;
            Secret = paymentMethodProperties.FirstOrDefault(x => x.DefinitionField.Name == "Secret").Value;
            ResponseUrl = paymentMethodProperties.FirstOrDefault(x => x.DefinitionField.Name == "ResponseUrl").Value;
            Account = paymentMethodProperties.FirstOrDefault(x => x.DefinitionField.Name == "Account").Value;
            AutoSettleFlag = paymentMethodProperties.FirstOrDefault(x => x.DefinitionField.Name == "AutoSettleFlag").Value;

            CurrentTimeStamp = PaymentGatewayHelper.GetNowTimestamp();
            OrderGuidId = paymentRequest.PurchaseOrder.OrderGuid.ToString();
            PaymentCurrency = paymentRequest.PurchaseOrder.BillingCurrency.ISOCode;
            ///Amount in cents as per Realex guide. Removed unwanted decimal points
            AmountToPay = (paymentRequest.Amount.Value * 100).ToString().Split('.')[0];
            ShaHash = PaymentGatewayHelper.GetHash(string.Format("{0}.{1}", PaymentGatewayHelper.GetHash(string.Format("{0}.{1}.{2}.{3}.{4}", CurrentTimeStamp, MerchantId, OrderGuidId, AmountToPay, PaymentCurrency)), Secret));
            ///Stored order id to get it back on response. This parameter will return with same values in the response
            AnythingElse = paymentRequest.PurchaseOrder.OrderId.ToString();
        }
    }
}