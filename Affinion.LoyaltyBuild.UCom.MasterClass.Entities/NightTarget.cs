﻿
using Affinion.LoyaltyBuild.UCom.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;
using UCommerce.Marketing.Targets;
using Affinion.LoyaltyBuild.UCom.Common.Extension;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class NightTarget : Target, IOrderLineTarget, IEntity, ITarget
    {
        private int? _hashCode;

        public virtual bool IsSatisfiedBy(OrderLine orderLine)
        {
            OrderProperty searchIgnor = orderLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.IgnorCampaignSearchCriteria);
            if (searchIgnor != null)
                return true;

            OrderProperty nightProperty = orderLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.NightKey);
            if (nightProperty != null)
            {
                int noOfNights = 0;
                int.TryParse(nightProperty.Value, out noOfNights);
                int noOfActNights = this.Night;
                if (noOfNights >= noOfActNights)
                {
                    FreeNightAward freeAwrd = FreeNightAward.All().Where(x => x.CampaignItem.CampaignItemId == CampaignItem.CampaignItemId).FirstOrDefault();
                    if (freeAwrd != null)
                    {
                        OrderProperty arrivalDateProperty = orderLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.ArrivalDateKey);
                        if (arrivalDateProperty != null && nightProperty != null)
                        {
                            DateTime arrivalDate = arrivalDateProperty.Value.ParseFormatDateTime(true);
                            arrivalDate.AddDays(noOfNights - 1);
                            int lastArrivalDay = (int)arrivalDate.Date.DayOfWeek;

                            List<int> arrivalDays = new List<int>();
                            if (!string.IsNullOrEmpty(this.LastArrivalDay))
                            {
                                int validday = 0;
                                arrivalDays = this.LastArrivalDay.Split('|').Where(x => !string.IsNullOrEmpty(x) && int.TryParse(x, out validday)).Select(x => Convert.ToInt32(x)).ToList();
                            }
                            if (arrivalDays.Any(x => x == lastArrivalDay))
                            {
                                return true;
                            }
                        }
                    }
                    else
                    {
                        return true;
                    }
                    //else if (nightProperty.Value == this.Night.ToString())
                    //    return true;
                }
            }
            return false;
        }



        internal static IRepository<NightTarget> _repo = null;
        private static object lockItem = 0;

        // Methods
        public new static IQueryable<NightTarget> All()
        {
            return GetRepo().Select();
        }

        public override void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<NightTarget, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            NightTarget objA = obj as NightTarget;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<NightTarget, bool>> expression)
        {
            return All().Any<NightTarget>(expression);
        }

        public static IList<NightTarget> Find(Expression<Func<NightTarget, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<NightTarget>();
        }

        public static NightTarget FirstOrDefault(Expression<Func<NightTarget, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<NightTarget>();
        }

        public new static NightTarget Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<NightTarget> GetRepo()
        {
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<NightTarget>>();
        }


        public override void Save()
        {
            GetRepo().Save(this);
        }

        public static NightTarget SingleOrDefault(Expression<Func<NightTarget, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties
        public override int Id
        {
            get
            {
                return this.NightTargetId;
            }
        }

        public virtual int NightTargetId { get; protected set; }

        public virtual int Night { get; set; }

        public virtual string LastArrivalDay { get; set; }

    }
}
