﻿using Affinion.LoyaltyBuild.UCom.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;
using UCommerce.Marketing.Targets;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class OrderLineRestrictionTarget : Target, IOrderLineTarget, IEntity, ITarget, IPurchaseOrderTarget
    {
        private int? _hashCode;

        public virtual bool IsSatisfiedBy(OrderLine orderLine)
        {
            if (orderLine.OrderLineId > 0)
            {
                OrderProperty property = orderLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.OrderLinRestrictionCampaign);
                if (property == null)
                    return false;
                int appliedCampaignItemId = 0;
                if (int.TryParse(property.Value, out appliedCampaignItemId))
                {
                    return appliedCampaignItemId == CampaignItem.CampaignItemId;
                }
            }
            return true;

        }

        public virtual bool IsSatisfiedBy(PurchaseOrder purchaseOrder)
        {
            int noOfOrderLineApplied = 0;
            if (purchaseOrder.OrderId > 0)
            {
                foreach (var item in purchaseOrder.OrderLines)
                {
                    if (this.NoOfOrderLine < noOfOrderLineApplied)
                        continue;
                    noOfOrderLineApplied++;
                    OrderProperty property = item.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.OrderLinRestrictionCampaign);
                    if (property == null)
                    {
                        item.AddOrderProperty(new OrderProperty() { Key = OrderPropertyConstants.OrderLinRestrictionCampaign, Value = CampaignItem.CampaignItemId.ToString(), Order = purchaseOrder, OrderLine = item });
                    }
                    else
                    {
                        property.Value = CampaignItem.CampaignItemId.ToString();
                    }
                }
            }
            return true;
        }


        // Methods
        public new static IQueryable<OrderLineRestrictionTarget> All()
        {
            return GetRepo().Select();
        }

        public override void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<OrderLineRestrictionTarget, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            OrderLineRestrictionTarget objA = obj as OrderLineRestrictionTarget;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<OrderLineRestrictionTarget, bool>> expression)
        {
            return All().Any<OrderLineRestrictionTarget>(expression);
        }

        public static IList<OrderLineRestrictionTarget> Find(Expression<Func<OrderLineRestrictionTarget, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<OrderLineRestrictionTarget>();
        }

        public static OrderLineRestrictionTarget FirstOrDefault(Expression<Func<OrderLineRestrictionTarget, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<OrderLineRestrictionTarget>();
        }

        public new static OrderLineRestrictionTarget Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<OrderLineRestrictionTarget> GetRepo()
        {
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<OrderLineRestrictionTarget>>();

        }


        public override void Save()
        {
            GetRepo().Save(this);
        }

        public static OrderLineRestrictionTarget SingleOrDefault(Expression<Func<OrderLineRestrictionTarget, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties
        public override int Id
        {
            get
            {
                return this.OrderLineRestrictionTargetId;
            }
        }

        public virtual int OrderLineRestrictionTargetId { get; protected set; }

        public virtual int NoOfOrderLine { get; set; }








    }
}
