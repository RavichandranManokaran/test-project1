﻿//<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           ClientPortalBookingDetailsSublayout.ascx.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal
/// Description:           Used for the functionalities in booking details page.
/// </summary>

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal
{
    #region Using Directives
    using Affinion.Loyaltybuild.BusinessLogic.Helper;
    using Affinion.LoyaltyBuild.Common;
    using Affinion.LoyaltyBuild.Common.Exceptions;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.SessionHelper;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Communications.ExceptionServices;
    using Affinion.LoyaltyBuild.DataAccess.Models;
    using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using UCommerce.Api;
    //using UCommerce.EntitiesV2;
    using UCommerce.Runtime;

    #endregion

    public partial class ClientPortalBookingDetailsSublayout : BaseSublayout
    {
        ExceptionMailHelper ES = new ExceptionMailHelper();
        Sitecore.Data.Database currentDb = Sitecore.Context.Database;

        #region Fields
        //regular expressions
        Regex emailValidator = new Regex(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
        Regex phoneValidator = new Regex(@"^([0-9\+]{10,15})+$");

        #endregion

        public Item ValidateContinueToDetailItem { get; set; }


        public string qty = string.Empty;
        public string orderLineId = string.Empty;

        protected string RequiredFieldMessage { get; set; }


        /// <summary>
        /// Page load event 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            BindSitecoreErrorMessages();

            // Put user code to initialize the page here
            try
            {
                Sitecore.Events.Event.Subscribe("lb:ClientValidationSuccess", ClientValidationSuccess);

                Item item = this.GetDataSource();
                if (Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.BasketCount == 0)
                {
                    CustomerValidationSection.Visible = false;
                    PersonalDetails.Visible = false;
                }

                InitializeFields(item);

                if (!Page.IsPostBack)
                {
                    //Load values for the dropdown
                    LoadDropDown("Countries", "CountryName", "UCommerceCountryID", CountriesDropdown);
                    LoadDropDown("Branches", "BranchLocationName", "BranchLocationName", ClientStoreDropdown);
                    LoadDropDown("Regions", "Name", "ID", CountyRegionDropdown);
                    LoadTitles();
                }
            }
            catch (Exception exception)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, exception.Message);
                throw;
            }
        }

        /// <summary>
        /// function that executes when the vontinue to secure payment button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ContinueToSecurePaymentButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (Page.IsValid)
                {
                    if (!ValidateAvailability())
                    {
                        return;
                    }
                    ValidatePersonalDetails();
                }
            }
            catch (Exception exception)
            {
                //Send email to user if user is not authenticated but has provided name and email address
                string personalEmail = PersonalEmailAddress.Text;
                string emailingName = FirstName.Text;
                if (!Sitecore.Context.User.IsAuthenticated && !string.IsNullOrEmpty(personalEmail) && (!string.IsNullOrEmpty(emailingName)))
                {
                    ES.SendExceptionEmail(exception.Message, "clientexceptionnotificationemail", emailingName, personalEmail);
                }

                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, exception, this);
                throw new AffinionException("Error in continuing to secure payment", exception);
            }
        }

        /// <summary>
        /// page unload event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Unload(object sender, EventArgs e)
        {

            Sitecore.Events.Event.Unsubscribe("ValidationSuccess", ClientValidationSuccess);

        }



        #region Private Methods

        /// <summary>
        /// Event raised on client validation success
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClientValidationSuccess(object sender, EventArgs e)
        {
            if (e != null)
            {
                var customerRefernce = Sitecore.Events.Event.ExtractParameter(e, 0).ToString();

                //set order property for "CustomerReference"
                if (UCommerce.Api.TransactionLibrary.HasBasket())
                {
                    var order = UCommerce.Api.TransactionLibrary.GetBasket().PurchaseOrder;
                    // we have to save the customer email 
                    order["CustomerReference"] = customerRefernce;
                    order.Save();
                    SetControlVisibility();
                }
            }
        }

        /// <summary>
        /// is customer validated
        /// </summary>
        private bool IsValidated
        {
            get
            {
                return TransactionLibrary.HasBasket() &&
                    !string.IsNullOrEmpty(TransactionLibrary.GetBasket().PurchaseOrder["CustomerReference"]);
            }
        }

        /// <summary>
        /// Set fields text and visibility
        /// </summary>
        /// <param name="item"></param>
        private void InitializeFields(Item item)
        {
            if (item != null)
            {
                this.Title.Item = item;
                this.FirstNameLabel.Item = item;
                this.LastNameLabel.Item = item;
                this.Address1.Item = item;
                this.Address2.Item = item;
                this.Address3.Item = item;
                this.CountyRegion.Item = item;
                this.Country.Item = item;
                this.Mobile.Item = item;
                this.Phonenumber.Item = item;
                this.Email.Item = item;
                this.ConfirmEmail.Item = item;
                this.Store.Item = item;
                this.SpecialOffersAndDetails.Item = item;
                this.TermsAndConditionsLabel.Item = item;
                this.Language.Item = item;

                SetControlVisibility();
            }
        }

        /// <summary>
        /// Load Personal Details Form
        /// </summary>
        /// <param name="item"></param>
        /// <param name="email"></param>
        private void LoadPersonalDetailsForm(Item item, string email)
        {
            //If validation succeeded, continue to secure payment.
            //Change the class of continue to detail button
            // ContinueToDetailButton.Attributes["class"] += "sec-payment";
            //hide the current div and show the personal details div
            Page.ClientScript.RegisterStartupScript(this.GetType(), "secpayment", "paymentFunction()", true);
            ContinueToSecurePaymentButton.Text = item.Fields["ContinueToSecurePaymentButtonTitle"].Value;
            //Set the email address
            PersonalEmailAddress.Text = email;
            ConfirmPersonalEmailAddress.Text = email;
            PersonalEmailAddress.ReadOnly = !string.IsNullOrEmpty(email);
            ConfirmPersonalEmailAddress.ReadOnly = !string.IsNullOrEmpty(email);
            //LoadTitles();
            //LoadExistingCustomerInfromation(email);
        }

        /// <summary>
        /// Populate Titles
        /// </summary>
        private void LoadTitles()
        {
            //Sitecore.Data.Database currentDb = Sitecore.Context.Database;

            //Load location data to a locationList
            Item titleList = currentDb.GetItem(Constants.TitleListPath);

            if (titleList != null)
            {
                var titles = (from loc in titleList.Children
                              orderby loc.DisplayName
                              select loc.DisplayName);

                //Bind locationList data to the dropdown
                TitleDropDown.DataSource = titles != null ? titles.ToList() : null;
                TitleDropDown.DataBind();
            }

            ListItem selectTitle = new ListItem(Constants.PleaseSelectText, string.Empty);
            TitleDropDown.Items.Insert(0, selectTitle);
        }

        /// <summary>
        /// LoadExistingCustomerInfromation
        /// </summary>
        /// <param name="bookingReferenceId">bookingReferenceId</param>
        private void LoadExistingCustomerInfromation(string email)
        {
            if (string.IsNullOrEmpty(email))
                return;

            bool isExistingCustomer = uCommerceConnectionFactory.IsExistingCustomer(GetClientId(), email);

            if (isExistingCustomer)
            {
                Affinion.LoyaltyBuild.DataAccess.Models.Customer existingCustomer = uCommerceConnectionFactory.GetCustomer(GetClientId(), email);

                PersonalEmailAddress.Text = existingCustomer.EmailAddress;
                ConfirmPersonalEmailAddress.Text = existingCustomer.EmailAddress;
                if (FirstName.Text == string.Empty)
                    FirstName.Text = existingCustomer.FirstName;
                if (LastName.Text == string.Empty)
                    LastName.Text = existingCustomer.LastName;
                if (AddressLine1.Text == string.Empty)
                    AddressLine1.Text = existingCustomer.AddressLine1;
                if (AddressLine2.Text == string.Empty)
                    AddressLine2.Text = existingCustomer.AddressLine2;
                if (AddressLine3.Text == string.Empty)
                    AddressLine3.Text = existingCustomer.AddressLine3;
                if (Phone.Text == string.Empty)
                    Phone.Text = existingCustomer.Phone;
                if (MobilePhone.Text == string.Empty)
                    MobilePhone.Text = existingCustomer.MobilePhone;
                if (CountriesDropdown.SelectedIndex == 0)
                    SetCountriesDropdownValue(existingCustomer);
                if (existingCustomer.Language != null)
                    SetLanguageDropDownValue(existingCustomer);
                if (TitleDropDown.SelectedIndex == 0)
                    SetTitleDropDownValue(existingCustomer);
                if (CountyRegionDropdown.SelectedIndex == 0)
                    SetCountyRegionDropDownValue(existingCustomer);
                if (ClientStoreDropdown.SelectedIndex == 0)
                    SetClientStoreDropDownValue(existingCustomer);

                if (existingCustomer.Subscribe == 1)
                    SendEmails.Checked = true;

            }
            else
            {
                //FirstName.Text = string.Empty;
                //LastName.Text = string.Empty;
                //AddressLine1.Text = string.Empty;
                //AddressLine2.Text = string.Empty;
                //AddressLine3.Text = string.Empty;
                //Phone.Text = string.Empty;
                //MobilePhone.Text = string.Empty;
                //SendEmails.Checked = false;
                //TermsAndConditions.Checked = false;
            }
            return;
        }

        /// <summary>
        /// Set County Region DropDown Value
        /// </summary>
        /// <param name="existingCustomer"></param>
        private void SetCountyRegionDropDownValue(Customer existingCustomer)
        {
            ListItem item = CountyRegionDropdown.Items.FindByValue(existingCustomer.LocationId.ToString());
            if (item != null)
            {
                CountyRegionDropdown.SelectedValue = existingCustomer.LocationId.ToString();
            }
        }

        /// <summary>
        /// Set Country DropDownV alue
        /// </summary>
        /// <param name="existingCustomer"></param>
        private void SetCountriesDropdownValue(Customer existingCustomer)
        {
            ListItem item = CountriesDropdown.Items.FindByValue(existingCustomer.CountryId.ToString());
            if (item != null)
            {
                CountriesDropdown.SelectedValue = existingCustomer.CountryId.ToString();
            }
        }

        /// <summary>
        /// Set Language DropDown Value
        /// </summary>
        /// <param name="existingCustomer"></param>
        private void SetLanguageDropDownValue(Customer existingCustomer)
        {
            ListItem item = LanguageDropDown.Items.FindByValue(existingCustomer.Language.ToString());
            if (item != null)
            {
                LanguageDropDown.SelectedValue = existingCustomer.Language.ToString();
            }
        }

        /// <summary>
        /// Set Title DropDown Value
        /// </summary>
        /// <param name="existingCustomer"></param>
        private void SetTitleDropDownValue(Customer existingCustomer)
        {
            ListItem item = TitleDropDown.Items.FindByValue(existingCustomer.Title);
            if (item != null)
            {
                TitleDropDown.SelectedValue = existingCustomer.Title;
            }
        }

        /// <summary>
        /// Set ClientStore DropDown Value
        /// </summary>
        /// <param name="existingCustomer"></param>
        private void SetClientStoreDropDownValue(Customer existingCustomer)
        {
            ListItem item = ClientStoreDropdown.Items.FindByValue(existingCustomer.BranchId.ToString());
            if (item != null)
            {
                ClientStoreDropdown.SelectedValue = existingCustomer.BranchId.ToString();
            }
        }

        /// <summary>
        /// Get the ClientId
        /// </summary>
        private string GetClientId()
        {
            Item clientItem = SitecoreFieldsHelper.GetLookupFieldTargetItem(ItemHelper.RootItem, Constants.ClientDetailsFieldName);
            return clientItem.ID.Guid.ToString();
        }

        /// <summary>
        /// Load countries for country dropdown
        /// </summary>
        private static void LoadDropDown(string dropdownFieldName, string targetItemsTextField, string targetItemsValueField, ListControl dropdown)
        {
            //get the parent item
            string client = Sitecore.Context.Item.Parent.Name;
            Database currentDB = Sitecore.Context.Database;
            //query for the client item in admin portal
            Item clientItem = currentDB.GetItem("/sitecore/content/admin-portal/client-setup/" + client + "");
            if (clientItem != null)
            {
                //Bind data to dropdown
                Collection<Item> items = SitecoreFieldsHelper.GetMutiListItems(clientItem, dropdownFieldName);
                dropdown.DataSource = SitecoreFieldsHelper.GetDropdownDataSource(items, targetItemsTextField, targetItemsValueField);
                dropdown.DataTextField = "Text";
                dropdown.DataValueField = "Value";
                dropdown.DataBind();
                //Set the default list item
                dropdown.Items.Insert(0, Constants.PleaseSelectText);
            }

        }


        /// <summary>
        /// Bind Sitecore Error Messages
        /// </summary>
        private void BindSitecoreErrorMessages()
        {
            Item item = this.GetItemFromRenderingParameterPath("Conditional Messages Link");
            SitecoreFieldsHelper.BindSitecoreText(item, "Not Valid Character Message", revTextFirstName);
            SitecoreFieldsHelper.BindSitecoreText(item, "Not Valid Character Message", revTextLastName);
            SitecoreFieldsHelper.BindSitecoreText(item, "Not Valid Character Message", revTextAddressLine1);
            SitecoreFieldsHelper.BindSitecoreText(item, "Not Valid Character Message", revTextAddressLine2);
            SitecoreFieldsHelper.BindSitecoreText(item, "Not Valid Character Message", revTextAddressLine3);
            SitecoreFieldsHelper.BindSitecoreText(item, "Required Field Message", FirstNameRequiredMessage);
            SitecoreFieldsHelper.BindSitecoreText(item, "Required Field Message", LastNameRequiredMessage);
            SitecoreFieldsHelper.BindSitecoreText(item, "Required Field Message", AddressLine1RequiredMessage);
            SitecoreFieldsHelper.BindSitecoreText(item, "Required Field Message", RequiredMobilePhoneMessage);
            SitecoreFieldsHelper.BindSitecoreText(item, "Phone Number Validation Message", MobilePhoneValidationMessage);
            SitecoreFieldsHelper.BindSitecoreText(item, "Required Field Message", PhoneNumberRequiredMessage);
            SitecoreFieldsHelper.BindSitecoreText(item, "Phone Number Validation Message", PhoneValidationMessage);
            SitecoreFieldsHelper.BindSitecoreText(item, "Email Validation Message", EmailValidationFieldMessage);
            SitecoreFieldsHelper.BindSitecoreText(item, "Required Field Message", EmailRequiredMessage);
            SitecoreFieldsHelper.BindSitecoreText(item, "Email Mismatch Message", EmailMismatchMessage);
            SitecoreFieldsHelper.BindSitecoreText(item, "Required Field Message", EmailRequiredMessage2);
            SitecoreFieldsHelper.BindSitecoreText(item, "Required Field Message", RequiredClientStoreMessage);
            SitecoreFieldsHelper.BindSitecoreText(item, "Required Field Message", RequiredCountryRegionMessage);
            SitecoreFieldsHelper.BindSitecoreText(item, "Required Field Message", RequiredCountryMessage);
            SitecoreFieldsHelper.BindSitecoreText(item, "Accept Terms and Conditions Message", RequiredTermsCondtionsMsg);

            Item currentPageItem = Sitecore.Context.Item;
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "TextValidator", TextPersonalDetails);


        }

        /// <summary>
        /// Validations for the personal details section
        /// </summary>
        private void ValidatePersonalDetails()
        {
            Item item = this.GetItemFromRenderingParameterPath("Conditional Messages Link");
            string email = PersonalEmailAddress.Text;
            //add customer information to ucommerce_orderaddress
           // AddPersonalDetailsToOrder();

            //Inserting the customer information to customerproperty
            int customerid = RecordCustomer();
            BasketHelper.SetBookingReferenceBooking(customerid);
            this.SetPayableAmountSession();
            if (!Request.IsSecureConnection)
            {
                string redirectUrl = Request.Url.ToString().Replace("http:", "https:");
                redirectUrl = redirectUrl.Replace(Request.RawUrl, "/paymentgateway");
                Response.Redirect(redirectUrl, false);
            }
            else
                Response.Redirect("/paymentgateway", false);

        }

        /// <summary>
        /// Checkamount Session will be used at the time of initiating Payment
        /// </summary>
        private void SetPayableAmountSession()
        {
            int orderId = BasketHelper.GetBasketOrderId();
            string clientId = this.GetClientId();
            BasketInfo basketInfo = BasketHelper.GetPopulateBasketPaymentDetails(orderId, clientId);
            UCommerce.EntitiesV2.PurchaseOrder order = UCommerce.Runtime.SiteContext.Current.OrderContext.GetBasket().PurchaseOrder;
            if (basketInfo != null && order != null)
            {
                order[Constants.OrderCheckoutAmountColumn] = basketInfo.TotalPayableToday;
                order.Save();
            }
        }

        /// <summary>
        /// Add Personal Details To Order
        /// </summary>
        private void AddPersonalDetailsToOrder()
        {
            PersonalDetails details = new PersonalDetails();
            details.FistName = FirstName.Text;
            details.LastName = LastName.Text;
            details.Address1 = AddressLine1.Text.Trim();
            details.Address2 = AddressLine2.Text.Trim();
            details.Address3 = AddressLine3.Text.Trim();
            details.CountyRegion = CountyRegionDropdown.SelectedValue;
            details.Country = CountriesDropdown.SelectedItem.Text;
            details.MobilePhone = MobilePhone.Text;
            details.Phone = Phone.Text;
            details.Email = PersonalEmailAddress.Text;
            details.Store = ClientStoreDropdown.SelectedValue;
            details.SendEmail = SendEmails.Checked;
            details.TermsAndConditions = TermsAndConditions.Checked;
            details.OrderId = BasketHelper.GetBasketOrderId();
            UCommerce.EntitiesV2.Basket basket = SiteContext.Current.OrderContext.GetBasket();
            UCommerce.EntitiesV2.PurchaseOrder order = basket.PurchaseOrder;
            foreach (var orderline in order.OrderLines)
            {
                orderline[Affinion.LoyaltyBuild.Api.Booking.Constants.LeadGuestEmail] = details.Email;
            }
            order.Save();
            BasketHelper.AddPersonalDetails(details);
        }

        /// <summary>
        /// Record the customer information
        /// </summary>
        private int RecordCustomer()
        {
            try
            {
                int result = 0;
                int? customerId = uCommerceConnectionFactory.GetExistingCustomerId(GetClientId(), PersonalEmailAddress.Text);
                Affinion.LoyaltyBuild.DataAccess.Models.Customer customer = null;

                UCommerce.EntitiesV2.PurchaseOrder order = UCommerce.Runtime.SiteContext.Current.OrderContext.GetBasket().PurchaseOrder;
                var objuComCustomer = UCommerce.EntitiesV2.Customer.Find(f => f.CustomerId == customerId).FirstOrDefault();
                foreach (var orderline in order.OrderLines)
                {
                    orderline[Affinion.LoyaltyBuild.Api.Booking.Constants.LeadGuestEmail] = PersonalEmailAddress.Text;
                }

                var country = UCommerce.EntitiesV2.Country.Get(Convert.ToInt32(CountriesDropdown.SelectedValue));
                if (country == null)
                {
                    country = new UCommerce.EntitiesV2.Country()
                    {
                        Name = CountriesDropdown.SelectedItem.Text,
                        Culture = CountriesDropdown.SelectedItem.Attributes["data-culture"] == null ? "en-US" : CountriesDropdown.SelectedItem.Attributes["data-culture"]
                    };
                }
                if (!customerId.HasValue)
                {
                    customer = CreateCustomer(customerId);


                    var addressInfo = new UCommerce.EntitiesV2.Address
                    {
                        FirstName = customer.FirstName,
                        LastName = customer.LastName,
                        EmailAddress = customer.EmailAddress,
                        PhoneNumber = customer.Phone,
                        MobilePhoneNumber = customer.MobilePhone,
                        Line1 = string.IsNullOrEmpty(customer.AddressLine1) ? string.Empty : customer.AddressLine1,
                        Line2 = string.IsNullOrEmpty(customer.AddressLine2) ? string.Empty : customer.AddressLine2,
                        City = string.IsNullOrEmpty(customer.AddressLine3) ? string.Empty : customer.AddressLine3,
                        State = Convert.ToString(customer.LocationId),
                        AddressName = "",
                        Attention = "",
                        CompanyName = "",
                        PostalCode = "",
                        Country = country
                    };


                    objuComCustomer = new UCommerce.EntitiesV2.Customer
                    {
                        FirstName = customer.FirstName,
                        LastName = customer.LastName,
                        EmailAddress = customer.EmailAddress,
                        PhoneNumber = customer.Phone,
                        MobilePhoneNumber = customer.MobilePhone,
                    };



                    objuComCustomer.AddAddress(addressInfo);
                    objuComCustomer.Save();

                    customer.CustomerId = objuComCustomer.CustomerId;

                    result = uCommerceConnectionFactory.AddCustomer(customer);
                }

                else
                {
                    customer = CreateCustomer(null);

                    //var objOrderAddress = UCommerce.EntitiesV2.OrderAddress.Find(p => p.EmailAddress == customer.EmailAddress).FirstOrDefault();
                    // var objCustomerAddress = objuComCustomer.Addresses.FirstOrDefault();
                    var objCustomerAddress = objuComCustomer.Addresses.FirstOrDefault(p => p.Customer.CustomerId == customerId);

                    objuComCustomer.FirstName = customer.FirstName;
                    objuComCustomer.LastName = customer.LastName;
                    objuComCustomer.EmailAddress = customer.EmailAddress;
                    objuComCustomer.PhoneNumber = customer.Phone;
                    objuComCustomer.MobilePhoneNumber = customer.MobilePhone;

                    if (objCustomerAddress == null)
                    {
                        objCustomerAddress = new UCommerce.EntitiesV2.Address();
                        objuComCustomer.AddAddress(objCustomerAddress);
                    }
                        
                    objCustomerAddress.FirstName = customer.FirstName;
                    objCustomerAddress.LastName = customer.LastName;
                    objCustomerAddress.EmailAddress = customer.EmailAddress;
                    objCustomerAddress.PhoneNumber = customer.Phone;
                    objCustomerAddress.MobilePhoneNumber = customer.MobilePhone;
                    objCustomerAddress.Line1 = string.IsNullOrEmpty(customer.AddressLine1) ? string.Empty : customer.AddressLine1;
                    objCustomerAddress.Line2 = string.IsNullOrEmpty(customer.AddressLine2) ? string.Empty : customer.AddressLine2;
                    objCustomerAddress.City = string.IsNullOrEmpty(customer.AddressLine3) ? string.Empty : customer.AddressLine3;
                    objCustomerAddress.State = Convert.ToString(customer.LocationId);
                    objCustomerAddress.AddressName = "";
                    objCustomerAddress.Attention = "";
                    objCustomerAddress.CompanyName = "";
                    objCustomerAddress.PostalCode = "";
                    if (country != null)
                        objCustomerAddress.Country = country;


                    //if (objuComCustomer.Addresses.FirstOrDefault(p => p.EmailAddress == customer.EmailAddress) == null)
                    //    objuComCustomer.Addresses.Add(objCustomerAddress);
                    //else
                    //    objCustomerAddress.Save();
                    objuComCustomer.Save();
                    customer.CustomerId = objuComCustomer.CustomerId;

                    result = uCommerceConnectionFactory.UpdateCustomer(customer);
                }


                var orderAddressInfo = new UCommerce.EntitiesV2.OrderAddress
                {
                    FirstName = customer.FirstName,
                    LastName = customer.LastName,
                    EmailAddress = customer.EmailAddress,
                    PhoneNumber = customer.Phone,
                    MobilePhoneNumber = customer.MobilePhone,
                    Line1 = string.IsNullOrEmpty(customer.AddressLine1) ? string.Empty : customer.AddressLine1,
                    Line2 = string.IsNullOrEmpty(customer.AddressLine2) ? string.Empty : customer.AddressLine2,
                    City = string.IsNullOrEmpty(customer.AddressLine3) ? string.Empty : customer.AddressLine3,
                    State = Convert.ToString(customer.LocationId),
                    AddressName = "",
                    Attention = "",
                    CompanyName = "",
                    PostalCode = "",
                    Country = country
                };

                order.Customer = objuComCustomer;
                order.AddOrderAddress(orderAddressInfo);
                order.Save();

                //if (customerId.HasValue)
                //{
                //    Customer customer = CreateCustomer(customerId);
                //    uCommerceConnectionFactory.UpdateCustomer(customer);

                //}
                //else
                //{

                //    Customer customer = CreateCustomer(null);
                //    customerId = uCommerceConnectionFactory.AddCustomer(customer);

                //}
                //BasketHelper.AssignCustomerToOrder(customerId.Value);

                return result;
            }
            catch (Exception exception)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, exception, this);
                throw new AffinionException("Error in recording customer", exception.InnerException);
            }
        }

        /// <summary>
        /// Create Customer entity
        /// </summary>
        /// <param name="customerId">customerId</param>
        private Affinion.LoyaltyBuild.DataAccess.Models.Customer CreateCustomer(int? customerId)
        {
            int countryId;
            int locationId;

            var customer = new Affinion.LoyaltyBuild.DataAccess.Models.Customer();
            customer.CustomerId = (customerId.HasValue) ? customerId.Value : 0;
            customer.FirstName = FirstName.Text;
            customer.LastName = LastName.Text;
            customer.AddressLine1 = AddressLine1.Text;
            customer.AddressLine2 = AddressLine2.Text;
            customer.AddressLine3 = AddressLine3.Text;
            customer.Phone = Phone.Text;
            customer.MobilePhone = MobilePhone.Text; ;
            customer.EmailAddress = PersonalEmailAddress.Text;
            customer.ClientId = GetClientId();
            customer.Active = 1;
            customer.Title = (TitleDropDown.SelectedIndex == 0) ? string.Empty : TitleDropDown.SelectedItem.Text;
            customer.BranchId = ClientStoreDropdown.SelectedItem.Text;

            if (Int32.TryParse(CountriesDropdown.SelectedValue, out countryId))
            {
                customer.CountryId = countryId;

            }

            customer.Language = LanguageDropDown.SelectedValue;

            //if (Int32.TryParse(CountyRegionDropdown.SelectedValue, out locationId))
            //{
            customer.LocationId = CountyRegionDropdown.SelectedItem.Text;

            //}
            if (SendEmails.Checked)
                customer.Subscribe = 1;


            return customer;
        }

        /// <summary>
        /// Validate availability before payment.
        /// </summary>
        /// <returns>Returns the first product which exceeds the available no of rooms</returns>
        private bool ValidateAvailability()
        {
            string product = BasketHelper.GetInvalidReservation();
            Diagnostics.Trace(DiagnosticsCategory.ClientPortal, "REALEX: invalid product: " + product + "");
            if (!string.IsNullOrEmpty(product))
            {
                EmptyBasket.Visible = true;
                EmptyBasket.Text = "No enough rooms for " + product + " category";
                return false;
            }
            return true;
        }

        /// <summary>
        /// Set control visibility according to configurations
        /// </summary>
        /// <param name="isATG">Whether the client is ATG</param>
        private void SetControlVisibility()
        {
            if (UCommerce.Api.TransactionLibrary.HasBasket())
            {
                if (!ItemHelper.GetBooleanClientRule("Do you require Partner Validation for Online") || IsValidated)
                {
                    this.CustomerValidationSection.Visible = false;

                    var customerReference = string.Empty;
                    if (IsValidated)
                        customerReference = UCommerce.Api.TransactionLibrary.GetBasket(false).PurchaseOrder["CustomerReference"];
                    LoadPersonalDetailsForm(this.GetDataSource(), customerReference);
                }
            }
        }

        #endregion




    }
}