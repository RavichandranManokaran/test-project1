﻿using Affinion.LoyaltyBuild.Common.Exceptions;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
using Affinion.LoyaltyBuild.SupplierPortal.DataAccess;
using Affinion.LoyaltyBuild.SupplierPortal.DataAccess.Service;
using Sitecore.Data.Items;
using Sitecore.Links;
using Sitecore.Security.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Secure
{
    public partial class ChangePassword : BaseSublayout
    {

        #region private variables

        private Dictionary<string, string> _sitecoreMessages;

        private String UserName { get; set; }

        #endregion

        protected readonly ILoginService loginSvc;
        protected String SCClickHereToContinue { get; set; }
        protected String SCHomePageUrl { get; set; }

        public ChangePassword()
        {
            this.loginSvc = new LoginService();
            this._sitecoreMessages = new Dictionary<string, string>();

        }

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            BindSitecoreValues();

        }

        /// <summary>
        /// btnSubmit_Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string uniqueID = Request.QueryString["id"];
            if (!String.IsNullOrWhiteSpace(uniqueID))
            {

                UserName = loginSvc.SelectChangePasswordHistory(uniqueID);
                string currentPassword = OldPasswordText.Text;
                string newPassword = NewPasswordText.Text;
                string ConfirmPassword = ConfirmPasswordText.Text;
                bool status = false;
                // bool isLocked = false;
                if (!String.IsNullOrWhiteSpace(UserName))
                {
                    if (newPassword.Equals(ConfirmPassword))
                    {

                        using (new Sitecore.SecurityModel.SecurityDisabler())
                        {
                            MembershipUser user = Membership.GetUser(UserName, true);
                            if (user != null)
                            {

                                try
                                {
                                    Affinion.LoyaltyBuild.Security.MemberShip.Helper membershipHelper = new Affinion.LoyaltyBuild.Security.MemberShip.Helper();
                                    if (membershipHelper.IsInPasswordHistory(UserName, newPassword))
                                    {
                                        DisplayMessage(GetMessage(Constants.PasswordExistsInHistory));
                                        return;
                                    }
                                    else
                                    {
                                        status = user.ChangePassword(currentPassword, newPassword);
                                        if (status)
                                        {

                                            bool isSuccess = Sitecore.Security.Authentication.AuthenticationManager.Login(UserName, newPassword, false);
                                            if (isSuccess)
                                            {
                                                loginSvc.DeleteChangePasswordHistory(UserName);
                                                continueToClickDiv.Visible = true;
                                                changePasswordBody.Visible = false;
                                            }
                                            else
                                            {
                                                DisplayMessage(GetMessage(Constants.AuthenticationFailed));
                                            }
                                        }
                                        else
                                        {
                                            DisplayMessage(GetMessage(Constants.ProvideOldPasswordCorrectly));

                                        }
                                    }

                                }
                                catch (ArgumentException)
                                {
                                    DisplayMessage(GetMessage(Constants.NewPasswordDoesNotMatchWithRegex));

                                }

                                catch (Exception ex)
                                {
                                    DisplayMessage(GetMessage(Constants.PasswordHasNotBeenChanged));

                                    //Log 
                                    Diagnostics.WriteException(DiagnosticsCategory.SupplierPortal, ex, this);
                                }
                            }
                        }
                    }
                    else
                    {
                        DisplayMessage(GetMessage(Constants.PasswordDoesNotMatch));

                    }
                }
                else
                {
                    DisplayMessage(GetMessage(Constants.AlreadyChanged));

                }
            }
            
           

        }

        /// <summary>
        /// Bind Sitecore Values
        /// </summary>
        private void BindSitecoreValues()
        {
            Item conditionalMessageItem = this.GetItemFromRenderingParameterPath("Conditional Messages Link");
            if (conditionalMessageItem != null)
            {
                _sitecoreMessages.Add(Constants.PasswordExistsInHistory, SitecoreFieldsHelper.GetValue(conditionalMessageItem, Constants.PasswordExistsInHistory));
                _sitecoreMessages.Add(Constants.PasswordChanged, SitecoreFieldsHelper.GetValue(conditionalMessageItem, Constants.PasswordChanged));
                _sitecoreMessages.Add(Constants.ProvideOldPasswordCorrectly,  SitecoreFieldsHelper.GetValue(conditionalMessageItem, Constants.ProvideOldPasswordCorrectly));
                _sitecoreMessages.Add(Constants.NewPasswordDoesNotMatchWithRegex,  SitecoreFieldsHelper.GetValue(conditionalMessageItem, Constants.NewPasswordDoesNotMatchWithRegex));
                _sitecoreMessages.Add(Constants.PasswordHasNotBeenChanged, SitecoreFieldsHelper.GetValue(conditionalMessageItem, Constants.PasswordHasNotBeenChanged));
                _sitecoreMessages.Add(Constants.PasswordDoesNotMatch, SitecoreFieldsHelper.GetValue(conditionalMessageItem, Constants.PasswordDoesNotMatch));
                _sitecoreMessages.Add(Constants.AuthenticationFailed, SitecoreFieldsHelper.GetValue(conditionalMessageItem, Constants.AuthenticationFailed));
                _sitecoreMessages.Add(Constants.AlreadyChanged, SitecoreFieldsHelper.GetValue(conditionalMessageItem, Constants.AlreadyChanged));
                
                SCClickHereToContinue = SitecoreFieldsHelper.GetValue(conditionalMessageItem, Constants.ClickHereToContinue);
               
            }
            Item currentItem = Sitecore.Context.Item;
            if (currentItem != null)
            {
                Sitecore.Data.Fields.LinkField lnkFld = currentItem.Fields[Constants.HomePageUrl];

                SCHomePageUrl = lnkFld != null ? LinkManager.GetItemUrl(lnkFld.TargetItem) : string.Empty;

            }

        }

        /// <summary>
        /// Displays a Message when Basket is Empty 
        /// </summary>
        private void DisplayMessage(string message)
        {
            DisplayLabelMessage.Text = message;


        }
        /// <summary>
        /// Get Message
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private string GetMessage(string key)
        {
            if (_sitecoreMessages.ContainsKey(key))
                return _sitecoreMessages[key];

            return string.Empty;
        }


    }
}