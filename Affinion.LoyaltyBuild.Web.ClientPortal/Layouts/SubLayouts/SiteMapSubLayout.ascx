﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SiteMapSubLayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.SiteMapSubLayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="row">
    <div class="col-xs-12">
        <div class="site_map">
            <h1>Site Map</h1>
            <div class="group_container clearfix">
                <asp:Repeater ID="SiteMapRepeater" runat="server" OnItemDataBound="SiteMapRepeater_ItemDataBound">
                    <ItemTemplate>
                        <div class="one_group col-md-3">
                            <h3>
                                <asp:HyperLink ID="grouplink" runat="server" Text='<%# Eval("Name") %>' NavigateUrl='<%# Eval("Url") %>'></asp:HyperLink>
                           </h3>
                            <ul>
                                <asp:Repeater ID="SiteMapItemRepeater" runat="server">
                                    <ItemTemplate>
                                <li>
                                    <asp:HyperLink ID="link" runat="server" Text='<%# Eval("Name") %>' NavigateUrl='<%# Eval("Url") %>'></asp:HyperLink>
                                </li>
                                     </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
</div>
