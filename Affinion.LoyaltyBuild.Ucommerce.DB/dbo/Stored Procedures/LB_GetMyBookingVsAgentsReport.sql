﻿--********************************************************************************************************************************************************************************
--CREATE STORED PROCEDURE SCRIPTS For Agent Booking Comparison With Other Agents for Day\Week\Month\Year of the Same Clients
--********************************************************************************************************************************************************************************
CREATE PROCEDURE [dbo].[LB_GetMyBookingVsAgentsReport]
	@CurrentUser varchar(50),
	@ClientIds varchar(50)=NULL
AS
BEGIN
	SET NOCOUNT ON;

declare @TodaysDate as date =FORMAT(GETDATE(),'dd-MMM-yyyy')
declare @YesterdayDate as date=FORMAT(GETDATE()-1,'dd-MMM-yyyy')
declare @MonthStartDate as date=FORMAT ( GETDATE(), '01-MMM-yyyy')
declare @YearStartDate as date=FORMAT ( GETDATE(), '01-Jan-yyyy')
declare @WeekStartDate as date=FORMAT(DATEADD(dd, @@DATEFIRST - DATEPART(dw, GETDATE())-6, GETDATE()), 'dd-MMM-yyyy')


IF OBJECT_ID('tempdb..#TempGetMyBookingVsAgentsReport') IS NOT NULL DROP TABLE #TempGetMyBookingVsAgentsReport

create table #TempGetMyBookingVsAgentsReport(OrderLineId int,CreatedBy varchar(50),CreatedDate date)

CREATE INDEX #TempIndex ON #TempGetMyBookingVsAgentsReport (CreatedDate)

CREATE INDEX #TemporderLineIndex ON #TempGetMyBookingVsAgentsReport (OrderLineId)

insert into #TempGetMyBookingVsAgentsReport
select distinct b.OrderLineId,b.createdby,FORMAT(CreatedDate, 'dd-MMM-yyyy') as CreatedDate
from LB_BookingInfo b, LB_OrderLineStatusAudit L
where b.OrderLineId=l.OrderLineId
and l.StatusId=6
and FORMAT (createdon,'dd-MMM-yyyy') between @YearStartDate and @TodaysDate

delete from #TempGetMyBookingVsAgentsReport
where OrderLineId not in (
select T.OrderLineId
from [uCommerce_OrderLine] O, uCommerce_OrderProperty P, #TempGetMyBookingVsAgentsReport T
where o.orderlineid =p.OrderLineId
and p.[Key]='_BookingThrough'
and p.Value='Offline'
and  o.OrderLineId =T.OrderLineId)

IF (@ClientIds IS NOT NULL) OR (LEN(@ClientIds) > 0)
delete from #TempGetMyBookingVsAgentsReport
where OrderLineId  not in (
select T.OrderLineId
from [uCommerce_OrderLine] O, uCommerce_OrderProperty P, #TempGetMyBookingVsAgentsReport T
where o.orderlineid =p.OrderLineId
and p.[Key]='_ClientId'
and p.Value  in  (SELECT * FROM   dbo.Splitstring(@ClientIds, ','))
and  o.OrderLineId =T.OrderLineId)

select TT.Id, TT.Timeframe,TT.Currentuser,TT.OtherUsers,TT.CurrentUser+TT.OtherUsers as Total from 
(select 1 as Id,
'Today' as Timeframe,
(select count(*) Count  from #TempGetMyBookingVsAgentsReport T where CreatedBy=@CurrentUser and T.CreatedDate=@TodaysDate) as CurrentUser,
(select count(*) Count  from #TempGetMyBookingVsAgentsReport T where CreatedBy!=@CurrentUser and T.CreatedDate=@TodaysDate ) as OtherUsers
union
select 2 as Id,'Yesterday' as Timeframe,
(select count(*) Count  from #TempGetMyBookingVsAgentsReport T where CreatedBy=@CurrentUser and T.CreatedDate=@YesterdayDate)as CurrentUser,
(select count(*) Count  from #TempGetMyBookingVsAgentsReport T where CreatedBy!=@CurrentUser and T.CreatedDate=@YesterdayDate) as OtherUsers
union
select 4 as Id,'MTD' as Timeframe,
(select count(*) Count  from #TempGetMyBookingVsAgentsReport T where CreatedBy=@CurrentUser and T.CreatedDate between @MonthStartDate  and @TodaysDate )as CurrentUser,
(select count(*) Count  from #TempGetMyBookingVsAgentsReport T where CreatedBy!=@CurrentUser and T.CreatedDate between @MonthStartDate  and @TodaysDate )as OtherUsers
union
select 5 as Id,'YTD' as Timeframe,
(select count(*) Count  from #TempGetMyBookingVsAgentsReport T where CreatedBy=@CurrentUser and T.CreatedDate between @YearStartDate  and @TodaysDate )as CurrentUser,
(select count(*) Count  from #TempGetMyBookingVsAgentsReport T where CreatedBy!=@CurrentUser and T.CreatedDate between @YearStartDate  and @TodaysDate )as OtherUsers
union 
select 3 as Id,'WTD' as Timeframe,
(select count(*) Count  from #TempGetMyBookingVsAgentsReport T where CreatedBy=@CurrentUser and T.CreatedDate between @WeekStartDate  and @TodaysDate )as CurrentUser,
(select count(*) Count  from #TempGetMyBookingVsAgentsReport T where CreatedBy!=@CurrentUser and T.CreatedDate between @WeekStartDate  and @TodaysDate )as OtherUsers) TT


END