﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Handler.Offer
{
    /// <summary>
    /// Summary description for AnnualReportHandler
    /// </summary>
    public class AnnualReportHandler : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                context.Response.ContentType = "text/plain";
                string offerId = context.Request["offerId"];
                context.Response.Write(LoadUserControl(context, offerId));
            }
            catch (Exception er)
            {
                context.Response.Write("ERROR" + er.Message.ToString());
                Sitecore.Diagnostics.Log.Error(er.Message, new object());
            }
        }



        protected static string LoadUserControl(HttpContext context, string offerId)
        {
            using (Page page = new Page())
            {
                StringBuilder s = new StringBuilder();

                OfferDetail offerDetails = new OfferDetail();
                AvailabilityHelper staticDataForRoom = new AvailabilityHelper(context);

                int campaignItemId = Convert.ToInt32(offerId);
                string supplierInviteId = staticDataForRoom.GetSupplierInviteId(campaignItemId).ToString();
                offerDetails = staticDataForRoom.GetCampaignById(Convert.ToInt32(campaignItemId), Convert.ToInt32(supplierInviteId), false, true);
                PlaceHolder f = new PlaceHolder();


                using (StringWriter writer = new StringWriter(CultureInfo.CurrentCulture))
                {
                    Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Offer.AnnualReport removeOfferPopup = (Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Offer.AnnualReport)page.LoadControl("~/Layouts/SubLayouts/SupplierPortal/Controls/Offer/AnnualReport.ascx");
                    removeOfferPopup.offerDetail = offerDetails;
                    f.Controls.Add(removeOfferPopup);
                    page.Controls.Add(f);
                    HttpContext.Current.Server.Execute(page, writer, false);
                    s.Clear();
                    s.Append(writer.ToString());
                }

                return s.ToString();
            }

        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}