﻿<%@ Control AutoEventWireup="true" CodeBehind="AvailabilityCalendarView.ascx.cRoomAvailabilitys" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.Availability.SupplierPortal.Controls.AvailabilityCalendarView" Language="C#" %>

<input type="hidden" class="roleBasedDisableButtons" id="roleBasedDisableButtons" runat="server" />
<div id="dv_<%= currenItem.Month %>">
   <%-- <div class="cal-table-top">
        <div class="col-md-12 padding0">
            <div class="col-md-6 no-padding">
                <div class="cal-table-top-left float-left">
                    <button type="button" class="btn btn-default btn-xs" id="reopen_1" reopenroom="false" onclick="Supplier.ManageAvailability.CloseOut(1,2)"><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span>Reopen </button>
                    <button type="button" class="btn btn-default btn-xs" id="closeout_1" closeroom="false" onclick="Supplier.ManageAvailability.CloseOut(1,1)"><span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span>Close Out </button>
                    <button type="button" class="btn btn-default btn-xs" id="res_1" removerestriction="false" onclick="Supplier.ManageAvailability.CloseOut(1,3)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span>Remove restrictions </button>
                </div>
            </div>
            <div class="col-md-6 no-padding">
                <div class="cal-table-top-right float-right">
                    <button type="button" class="btn btn-info font-size-12" data-toggle="modal" data-target="#help"><span class="glyphicon glyphicon-question-sign"></span>Help</button>
                    <button type="button" class="btn btn-info" onclick="Supplier.ManageAvailability.ClearChanges(0)">Clear All Changes </button>
                    <button id="btn_1" type="button" class="btn btn-orange" onclick="Supplier.ManageAvailability.UpdateChanges(0)">Update and Save Changes </button>
                </div>
            </div>
        </div>
    </div>--%>
    <div class="tabale-top availability">
        <table class="cal-table no-padding" id="calendarTable1_<%= currenItem.Month %>" data-start-day="sun">
            <tr>
                <th><span class="cal-hed"><%= currenItem.StatDate.ToString("MMM yyyy") %></span></th>
                <% for (int i = 0; i <= currenItem.EndDate.Day - currenItem.StatDate.Day; i++)
                   { 
                    
                %>
                <th onclick="Supplier.ManageAvailability.CloseOutDay(this, '<%= currenItem.Month %>','<%= currenItem.StatDate.AddDays(i).Day %>');" class="<%=  ((currenItem.StatDate.AddDays(i).DayOfWeek == DayOfWeek.Saturday || currenItem.StatDate.AddDays(i).DayOfWeek == DayOfWeek.Sunday) ? "headerSortDown" : "" ) %>" roomdaysclosed="<%=currenItem.RoomAvailability.All(x=>x.AvailabilityData.Any(y=>y.AvialabilityDate == currenItem.StatDate.AddDays(i) &&y.IsCloseOut  )) %>">
                    <span><%= currenItem.StatDate.AddDays(i).Day.ToString("00") %></span><span> <%= currenItem.StatDate.AddDays(i).DayOfWeek.ToString().Substring(0,2) %> </span></th>
                <%} %>
            </tr>

            <tr class="labelRow">
                <td class="transparent" style="height:auto;padding:3px 0px;">&nbsp;</td>
                <% for (int i = 0; i <= currenItem.EndDate.Day - currenItem.StatDate.Day; i++)
                   { %>
                <td style="<%=((currenItem.StatDate.AddDays(i).DayOfWeek == DayOfWeek.Saturday || currenItem.StatDate.AddDays(i).DayOfWeek == DayOfWeek.Sunday) ? "background-color: rgb(214, 216, 217);": "" ) %>"><span>v</span></td>
                <%} %>
            </tr>
            <%var compareProducts=0; %>
            <%foreach (var room in currenItem.RoomAvailability)
              {
              
               bool addStyle = false;
               
               
            %>

            <% if(room.ProductTye==1||room.ProductTye==2){             

                var enabledDate = new System.Collections.Generic.List<string>();
            %>
            <% if(compareProducts!=room.ProductTye){ 
                         compareProducts=room.ProductTye;%>
            <tr>
                <td>
                    <div class="col-md-12 title no-padding">

                     
                        
                        <% if(room.ProductTye==1){ %>
                        <h2 class="no-margin"><span class="glyphicon glyphicon-bed">&nbsp;<b>Room<b></span></h2>

                        <%}else if(room.ProductTye==2){ %>
                        <h2 class="no-margin"><span class="glyphicon glyphicon-plus-sign">&nbsp;<b>Addon<b></span></h2>
                        <%} %>
                     
                    </div>
                </td>
                <td colspan="<%=currenItem.EndDate.Day%>">
                    <div class="col-md-12 title no-padding">
                        <h2 class="no-margin">&nbsp;</h2>
                    </div>
                </td>
            </tr>
            <%} %>
            <tr>
                <td class="left-detail">
                    <div class="col-md-10 font-weight-bold detail-cal overflowTip" style="float:left;"><%= room.Name %></div>
                    <div class="col-md-2 no-padding block-detail" style="float:left;">
                        <a href="#" data-toggle="tooltip" data-placement="bottom" title="Booked" class="blue-tooltip">
                            <span class="glyphicon glyphicon-check"></span>
                        </a>
                    </div>
                </td>
                <% for (int i = 0; i <= currenItem.EndDate.Day - currenItem.StatDate.Day; i++)
                   {
                      addStyle = ((currenItem.StatDate.AddDays(i).DayOfWeek == DayOfWeek.Saturday || currenItem.StatDate.AddDays(i).DayOfWeek == DayOfWeek.Sunday));
                       var currentRoomAvailbity = room.AvailabilityData.FirstOrDefault(x => x.AvialabilityDate == currenItem.StatDate.AddDays(i));
                    string defaultclass = string.Format("default{0}", addStyle ? "weekend" : string.Empty);
                %>
                <td class="<%= currentRoomAvailbity.IsNonEditable? "disablebooking" : defaultclass %>">
                    <%if (!currentRoomAvailbity.IsNonEditable)
                      { 
                        enabledDate.Add(currenItem.StatDate.AddDays(i).Day.ToString());
                    %>
                    <span class="blokedspan"><%= currentRoomAvailbity.BlockedRoom %></span>
                    <% } %>
                </td>
                <%} %>
            </tr>




            <tr trroom="<%=room.ProductSku %>">
                <td class="left-detail <%=(room.ProductTye==2?"borderBottom":"") %>" onclick="Supplier.ManageAvailability.CloseOutRoom(this,'<%= currenItem.Month %>','<%= room.ProductSku %>');" roomsclosed="<%=room.AvailabilityData.All(x=>x.IsCloseOut) %>" roomsrestriction="<%=room.AvailabilityData.All(x=>x.HasOffer) %> ">
                    <div class="col-md-10 no-padding wdt85" style="float:left">
                        <% if(room.ProductTye==1){ %>
                        <div class="col-md-6 no-padding" style="float:left">
                            <%if (room.NumberOfAdult>0)
                                {%>
                            <div class="userbox">
                                <span class="glyphicon glyphicon-user"></span>
                            </div>
                            <span class="topcount"><%=room.NumberOfAdult %></span>
                                <%} else {%>
                            <div class="userbox">
                                <span class="glyphicon glyphicon-user disabled"></span>
                            </div>
                            <%}%>
                        </div>


                        <div class="col-md-6 no-padding" style="float:left;">
                            <%if (room.NumberOfChild>0)
                                {%>
                            <div class="userbox">
                                <span class="glyphicon glyphicon-user sm-font"></span>
                            </div>

                            <span class="topcount"><%=room.NumberOfChild %></span>
                            <%} else {%>
                            <div class="userbox">
                                <span class="glyphicon glyphicon-user sm-font disabled"></span>
                            </div>
                            <%}%>
                        </div>
                        <% }%>
                        <%-- <%= room.Name %>--%>
                    </div>
                    <div class="col-md-2  no-padding block-detail" style="float:left;">
                        <a href="#" data-toggle="tooltip" data-placement="bottom" title="Allocated" class="blue-tooltip">
                            <span class="glyphicon glyphicon-list-alt"></span>
                        </a>
                    </div>
                </td>
                <% for (int i = 0; i <= currenItem.EndDate.Day - currenItem.StatDate.Day; i++)
                   {
                       addStyle = ((currenItem.StatDate.AddDays(i).DayOfWeek == DayOfWeek.Saturday || currenItem.StatDate.AddDays(i).DayOfWeek == DayOfWeek.Sunday));
                       //var currentRoomAvailbity = room.AvailabilityData.FirstOrDefault(x => x.AvialabilityDate == currenItem.StatDate.AddDays(i)) ?? new Availability();
                       var currentRoomAvailbity = room.AvailabilityData.FirstOrDefault(x => x.AvialabilityDate == currenItem.StatDate.AddDays(i));
                       var roomClosed = currentRoomAvailbity.IsCloseOut;
                       var isOffered = currentRoomAvailbity.HasOffer;
                %>
                <td class="<%=(room.ProductTye==2?"borderBottom":"") %>">
                    <%    string classApplied = "";
                        string addClass = string.Empty;
                          if (addStyle)
                          {
                              addClass = "weekend";
                          }
                          //classApplied += string.Format(" offeredClass{0}", currentRoomAvailbity.HasOffer);
                          if (currentRoomAvailbity.IsNonEditable)
                          {
                              classApplied += string.Format(" disablebooking{0}", addClass);
                          }
                          else if (currentRoomAvailbity.IsCloseOut)
                          {
                              classApplied += string.Format(" closeOut{0}", addClass);
                          }
                          else if (currentRoomAvailbity.AvailabelRoom == currentRoomAvailbity.BlockedRoom && currentRoomAvailbity.AvailabelRoom > 0)
                          {
                              classApplied += string.Format(" soldOut{0}", addClass);
                          }
                          else if (currentRoomAvailbity.IsNewRecord || currentRoomAvailbity.AvailabelRoom == 0)
                          {
                              classApplied += string.Format(" unallocate{0}", addClass);
                          }
                          else if (currentRoomAvailbity.HasOffer)
                          {
                              classApplied += string.Format(" limits{0} bookable{1}", addClass,addClass);
                          }
                        
                          else if (currentRoomAvailbity.AvailabelRoom != currentRoomAvailbity.BlockedRoom)
                          {
                        classApplied += string.Format(" bookable{0}", addClass);
                              
                          }  
                        else
                          {
                              classApplied += string.Format(" default{0}", addClass);
                          }                          
                          
                    %>
                    <%if (!currentRoomAvailbity.IsNonEditable)
                      {
                          //currentRoomAvailbity.IsNonEditable ? "unallocate" :"default"
                    %>
                    <span oldpriceband="<%= currentRoomAvailbity.PriceBand %>" weekend="<%=addStyle%>" hashid="<%= currenItem.Month %>_<%= room.ProductId.Replace(" ","_") %>_<%= currenItem.StatDate.AddDays(i).Day %>" dayofweek="<%=(currentRoomAvailbity.IsNonEditable ? -1 : ((int)currenItem.StatDate.AddDays(i).DayOfWeek)) %>" day="<%=currenItem.StatDate.AddDays(i).Day %>" room="<%=room.ProductSku %>" oldselectedpg="" oldprice="" id='span_<%= currenItem.Month %>_<%= room.ProductId.Replace(" ","_") %>_<%= currenItem.StatDate.AddDays(i).Day %>' class="form-control <%=classApplied %> blokedspan allowselection EnbleSpan_0 EnbleSpan_<%= currenItem.Month %> EnbleSpan tmpfix" isclosed="<%=roomClosed.ToString().ToLower() %>" isofferapplicable="<%= isOffered.ToString().ToLower() %>" orgisclosed="<%=roomClosed.ToString().ToLower() %>" orgisofferapplicable="<%= isOffered.ToString().ToLower() %>" defaultclass="<%=classApplied %>" allocatedroom="<%= currentRoomAvailbity.AvailabelRoom %>" selectedpricegroup="<%= currentRoomAvailbity.PriceBand%>">
                        <%= currentRoomAvailbity.AvailabelRoom %>
                    </span>
                    <%} else {%>
                    <span weekend="<%=addStyle%>" class="disablebooking blokedspan EnbleSpan_<%= currenItem.Month %> EnbleSpan tmpfix"></span>
                    <%} %>
                    <input newpriceband="<%= currentRoomAvailbity.PriceBand %>" oldpriceband="<%= currentRoomAvailbity.PriceBand %>" addonprice="<%=room.AdditionalPrice %>" producttype="<%=currentRoomAvailbity.flag%>" dependson="<%=room.DependsOn%>" month="<%=currenItem.Month %>" dayofweek="<%=(currentRoomAvailbity.IsNonEditable ? -1 : ((int)currenItem.StatDate.AddDays(i).DayOfWeek)) %>" dayofweek="<%=(currentRoomAvailbity.IsNonEditable ? -1 : ((int)currenItem.StatDate.AddDays(i).DayOfWeek)) %>" room="<%=room.ProductSku %>" weekend="<%=addStyle%>" maxlength="3" datecounter="<%= currentRoomAvailbity.DateCounter %>" type="text" <%=currentRoomAvailbity.IsNonEditable? "disabled=\"disabled\"" : "" %> uniqidentifier="<%= currentRoomAvailbity.DateCounter %>_<%= room.ProductSku.Replace(" ","_") %>" id='<%= currenItem.Month %>_<%= room.ProductId.Replace(" ","_") %>_<%= currenItem.StatDate.AddDays(i).Day %>' hashid="<%= currenItem.Month %>_<%= room.ProductId.Replace(" ","_") %>_<%= currenItem.StatDate.AddDays(i).Day %>" onchange="Supplier.ManageAvailability.ChangeAvailability(this,false)" onblur="Supplier.ManageAvailability.ChangeAvailability(this,true)" blockedroom="<%= currentRoomAvailbity.BlockedRoom %>" allocatedroom="<%= currentRoomAvailbity.AvailabelRoom %>" autocomplete="off" required class="form-control <%=classApplied %>  EnbleText_0 EnbleText_<%= currenItem.Month %>" title="" value="<%=currentRoomAvailbity.IsNonEditable? "" : currentRoomAvailbity.AvailabelRoom.ToString() %>" orgisclosed="<%=roomClosed.ToString().ToLower() %>" orgisofferapplicable="<%= isOffered.ToString().ToLower() %>" defaultclass="<%=classApplied %>" pkgid="<%= currenItem.Month %>_<%=(currentRoomAvailbity.IsNonEditable ? -1 : ((int)currenItem.StatDate.AddDays(i).DayOfWeek)) %>_<%= currenItem.StatDate.AddDays(i).Day %>" />
                </td>

                <%} %>
            </tr>

            <% if(room.ProductTye==1){ %>
                
            <tr traddons="<%=room.Addons %>" trpkgroom="<%=room.pkgroom %>" roomsku="<%=room.ProductSku %>">
                <td class="left-detail borderBottom">
                    <%if(room.ProductTye==1){ %>
                    <div class="col-md-10 no-padding wdt85" style="float:left;">
                         <a href="#" data-toggle="tooltip" data-placement="bottom" title="Package" class="blue-tooltip">
                        <button class="btn btn-info font-size-12 marTM5 packageModalButton"
                            data-enabled-dates="<%= string.Join(",", enabledDate) %>"
                            data-noofdays="<%=currenItem.EndDate.Day %>"
                            data-year="<%=currenItem.StatDate.ToString("yyyy") %>"
                            data-month="<%=currenItem.StatDate.ToString("MM") %>"
                            data-roomsku="<%=room.ProductSku %>"
                            data-roomname="<%=room.Name %>"
                            type="button"
                            <%=string.IsNullOrEmpty(room.DependsOn)?"disabled = 'disabled'":"" %>
                            onclick="Supplier.ManageAvailability.PackageModal(this)" style="border:1px solid #e5e5e5;border-radius:4px;padding:6px 8px;">
                            <span class="glyphicon glyphicon-th" aria-hidden="true"></span>
                        </button>
                        <span class="glyphicon glyphicon-th" aria-hidden="true" style="color:#fff;"></span>
                         </a>

                    </div>
                    <div class="col-md-2 no-padding" style="float:left;">
                        <a href="#" data-toggle="tooltip" data-placement="bottom" title="Price" class="blue-tooltip">
                            <span class="glyphicon glyphicon-euro"></span>
                        </a>
                    </div>
                    <%} %>
                    
                </td>



                <% for (int i = 0; i <= currenItem.EndDate.Day - currenItem.StatDate.Day; i++)
                   {
                      addStyle = ((currenItem.StatDate.AddDays(i).DayOfWeek == DayOfWeek.Saturday || currenItem.StatDate.AddDays(i).DayOfWeek == DayOfWeek.Sunday));
                       var currentRoomAvailbity = room.AvailabilityData.FirstOrDefault(x => x.AvialabilityDate == currenItem.StatDate.AddDays(i));
                     var roomClosed = currentRoomAvailbity.IsCloseOut;
                       var isOffered = currentRoomAvailbity.HasOffer;
                    string defaultclass = string.Format("default{0}", addStyle ? "weekend" : string.Empty);
                %>
                <td class="borderBottom <%= currentRoomAvailbity.IsNonEditable? "disablebooking" : defaultclass %>">

                    <%if(currentRoomAvailbity.flag==1) 
                        {%>

                    <% if(currentRoomAvailbity.IsNonEditable) { %>
                    <span class="blokedspan addonspan" id='addonspan_<%= currenItem.Month %>_<%=(currentRoomAvailbity.IsNonEditable ? -1 : ((int)currenItem.StatDate.AddDays(i).DayOfWeek)) %>_<%= currenItem.StatDate.AddDays(i).Day %>'></span>
                    <%} else { %>

                    <div class="form-group float-left">
                        <select data-dropupauto="false" class="pricedropdown show-tick form-control wdt55" producttype="<%=currentRoomAvailbity.flag%>" uniqidentifier="<%= currentRoomAvailbity.DateCounter %>_<%= room.ProductSku.Replace(" ","_") %>" dependson="<%=room.DependsOn%>"
                            id="<%= currenItem.Month %>_<%= currenItem.StatDate.AddDays(i).Day %>_<%= room.ProductId.Replace(" ","_") %>"
                            pkgid="<%= currenItem.Month %>_<%=(currentRoomAvailbity.IsNonEditable ? -1 : ((int)currenItem.StatDate.AddDays(i).DayOfWeek)) %>_<%= currenItem.StatDate.AddDays(i).Day %>" dayofweek="<%=(currentRoomAvailbity.IsNonEditable ? -1 : ((int)currenItem.StatDate.AddDays(i).DayOfWeek)) %>" day="<%=currenItem.StatDate.AddDays(i).Day %>" allocatedroom="<%= currentRoomAvailbity.AvailabelRoom %>" room="<%=room.ProductSku %>" datecounter="<%= currentRoomAvailbity.DateCounter %>" hashid="<%= currenItem.Month %>_<%= room.ProductId.Replace(" ","_") %>_<%= currenItem.StatDate.AddDays(i).Day %>" newrate="" oldrate="<%= currentRoomAvailbity.Price %>" newpriceband="<%= currentRoomAvailbity.PriceBand %>" oldpriceband="<%= currentRoomAvailbity.PriceBand %>" blockedroom="<%= currentRoomAvailbity.BlockedRoom %>" allocatedroom="<%= currentRoomAvailbity.AvailabelRoom %>" month="<%=currenItem.Month %>" onchange="Supplier.ManageAvailability.ChangePriceGroup(this,false)" isclosed="<%=roomClosed.ToString().ToLower() %>" isofferapplicable="<%= isOffered.ToString().ToLower() %>" orgisclosed="<%=roomClosed.ToString().ToLower() %>" orgisofferapplicable="<%= isOffered.ToString().ToLower() %>"
                            data-none-selected-text="">
                            <option data-hidden="true"></option>
                            <% foreach (var dd in currentRoomAvailbity.ddPrice)
                        { 
                            if(dd.ItemId == currentRoomAvailbity.PriceBand){ %>
                            <option data-subtext="(<%= dd.Rate%>+ <%= dd.Commision%>)" value="<%= dd.ItemId%>" price="<%= dd.Price%>" selected="selected"><span class="text"><%= dd.Price %></span></option>
                            <%} else{ %>
                            <option data-subtext="(<%= dd.Rate%>+ <%= dd.Commision%>)" value="<%= dd.ItemId %>" price="<%= dd.Price%>"><span class="text"><%= dd.Price %></option>
                            <%}
                            } %>
                        </select>
                    </div>

                    <%} %>
                    <%}

                   else if(currentRoomAvailbity.flag==2){ 
                        if(currentRoomAvailbity.AvailabelRoom>0)
                        {%>
                    <span class="blokedspan addonspan" id='addonspan_<%= currenItem.Month %>_<%=(currentRoomAvailbity.IsNonEditable ? -1 : ((int)currenItem.StatDate.AddDays(i).DayOfWeek)) %>_<%= currenItem.StatDate.AddDays(i).Day %>'><%= currentRoomAvailbity.Price %></span>
                    <%}else{%>
                    <span class="blokedspan"></span>
                    <% } } %>                   
                     
                </td>
                
                <%} %>
            </tr>
            <%}%>
            <%} %>
            <% } %>
        </table>
    </div>
</div>


<!-- Modal Popup -->
<div class="modal fade" id="PackageModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal-room-name"></h4>
            </div>
            <div class="modal-body">
                <div class="row no-margin">
                    <div class="col-md-12 PadTB10px">
                        <label class="pull-left">Day</label>
                        <div class="col-md-4" class="form-control" role="group" id="div-package-date-drop-down" aria-label="..."></div>
                    </div>
                </div>
                <div id="package-info"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<script type="text/javaScript">


    // Show Bootstrap Tooltip Over Truncated Text 
    $(document).on('mouseenter', ".overflowTip", function () {
        var $this = $(this);
        if (this.offsetWidth < this.scrollWidth && !$this.attr('title')) {
            $this.tooltip({
                title: $this.text(),
                placement: "bottom"
            });
            $this.tooltip('show');
        }
    });

    function SetPicker() {
    }


    jQuery(function ($) {

        //$('.pricedropdown').selectpicker({
        //    dropupAuto: false
        //});

        //$('[data-toggle="tooltip"]').tooltip();

        //$('.availability table tr td .pricedropdown').addClass('dropup');
        //$('.availability table td:last-child .dropdown-menu').css("right", "0");
        //$('.availability table td:last-child .dropdown-menu').css("left", "auto");
        //$('.availability table td:nth-last-child(2) .dropdown-menu').css("right", "0");
        //$('.availability table td:nth-last-child(2) .dropdown-menu').css("left", "auto");

        //$('#bulk-upload table tr:last td .pricedropdown').addClass('dropup');
        //$('#bulk-upload table td:last-child .dropdown-menu').css("right", "0");
        //$('#bulk-upload table td:last-child .dropdown-menu').css("left", "auto");

    });

    $(function () {

        $('.headerSortUp, .headerSortDown').each(function (i) {
            var n = $(this).index() + 1;
            $('.tabale-top tr td:nth-child(' + n + ') .btn-default').css('background-color', '#cccccc');
            $('.tabale-top tr td:nth-child(' + n + ') .pricedropdown').css('background-color', '#cccccc');
            $('.tabale-top tr td:nth-child(' + n + ') .bulkpricedropdown').css('background-color', '#cccccc');

        });

    });



</script>
