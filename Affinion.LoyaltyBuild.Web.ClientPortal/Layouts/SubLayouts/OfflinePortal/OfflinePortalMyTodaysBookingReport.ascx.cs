﻿using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports;
using Sitecore.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Data.Items;
using Affinion.LoyaltyBuild.Search.Helpers;
using System.Web.UI.DataVisualization.Charting;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    public partial class OfflinePortalMyTodaysBookingReport : System.Web.UI.UserControl
    {
        private IReportsService iReportsService;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            iReportsService = new ReportsService();
            BindSitecoreText();

            if (!IsPostBack)
            {                
                GenerateMyTodaysBookingReport();
            }
        }

        private void BindSitecoreText()
        {
            Item currentPageItem = Sitecore.Context.Item;
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "ChartTitle", ChartTitle);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "StarMessage", StarMessage);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "OtherUser", OtherUser);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "CurrentUser", CurrentUser);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "RefreshButton", RefreshButton);
        }

        private string GetUserNameFromDomainLogonID(string domainLoginId)
        {
            return (string.IsNullOrEmpty(domainLoginId) || !domainLoginId.Contains('\\'))
                                                ? domainLoginId
                                                : Convert.ToString(domainLoginId.Split('\\')[1]).Trim();
        }

        private void GenerateMyTodaysBookingReport()
        {
            var clientIds = GetEligibleClientIds();

            var myBookingCountList = iReportsService.GetMyTodaysBooking(GetUserNameFromDomainLogonID(Sitecore.Context.User.Name), clientIds);

            if (myBookingCountList != null)
            {
                MyTodaysBookingGrid.DataSource = myBookingCountList;
                MyTodaysBookingGrid.DataBind();

                MyBookingReportChart.Series[0].Points.DataBindXY(myBookingCountList.Select(f => f.TimeFrame).ToArray(), myBookingCountList.Select(f => f.CurrentUser).ToArray());
                MyBookingReportChart.Series[1].Points.DataBindXY(myBookingCountList.Select(f => f.TimeFrame).ToArray(), myBookingCountList.Select(f => f.OtherUsers).ToArray());
            }
        }

        /// <summary>
        /// To Get All the Eligible Client Id's for the particular LoggedOn User
        /// Gets the Client Ids which are mapped against the Callcenter to the user
        /// User-> CallCenter->Clients
        /// </summary>
        /// <returns></returns>
        private string GetEligibleClientIds()
        {
            try
            {
                var clientIds = SearchHelper.GetUserSpecificClientItems();

                if (clientIds != null && clientIds.Any())
                    return string.Join(",", clientIds.Where(w => !w.ID.IsNull).Select(f => f.ID.ToString()).ToArray());

                return string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// To Regenerate the Report Again with updated records information after the last generation if any
        /// </summary>
        protected void RegenerateReportButton_Click(object sender, EventArgs e)
        {
            try
            {
                GenerateMyTodaysBookingReport();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}