﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Api.Booking.Data.Basket
{
    /// <summary>
    /// Order property attribute
    /// </summary>
    [System.AttributeUsage(System.AttributeTargets.Property, AllowMultiple = false)]
    public class OrderPropertyAttribute : Attribute
    {
        public string Name { get; set; }

        public OrderPropertyAttribute(string name)
        {
            this.Name = name;
        }
    }
}
