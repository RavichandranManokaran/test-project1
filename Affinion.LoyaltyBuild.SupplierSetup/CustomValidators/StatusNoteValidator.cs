﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           StatusNoteValidator.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.SupplierSetup.CustomValidators
/// Description:           Handling validation for status note field when deativating a supplier
/// </summary>

using Affinion.LoyaltyBuild.Common.Instrumentation;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Data.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.SupplierSetup.CustomValidators
{
    [Serializable]
    public class StatusNoteValidator : StandardValidator
    {
        protected override ValidatorResult Evaluate()
        {
            try
            {
                Item validatingItem = base.GetItem();

                Field statusNote = validatingItem.Fields["StatusNote"];
                Field isActive = validatingItem.Fields["IsActive"];

                if (isActive.Value == "1")
                {
                    return ValidatorResult.Valid;
                    
                }
                else if (string.IsNullOrEmpty(statusNote.Value))
                {
                    this.Text = string.Format("Status note cannot be empty when a supplier is set to inactive");
                    return ValidatorResult.FatalError;
                }
                return ValidatorResult.Valid;
                
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.SupplierSetup, ex, null);
                throw;
            }
        }

        protected override ValidatorResult GetMaxValidatorResult()
        {
            return GetFailedResult(ValidatorResult.FatalError);
        }

        public override string Name
        {
            get { return "Status note cannot be empty when a supplier is set to inactive"; }
        }
    }
}
