﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SearchHelper.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Search
/// Description:           Search helper
/// </summary>
namespace Affinion.LoyaltyBuild.Search.Helpers
{
    #region Using Directives

    using Affinion.LoyaltyBuild.Common.Exceptions;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Search.Data;
    using Affinion.LoyaltyBuild.Search.SupplierFilters;
    using Sitecore.ContentSearch;
    using Sitecore.ContentSearch.Linq.Utilities;
    using Sitecore.ContentSearch.SearchTypes;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Reflection;
    using System.Web.UI.WebControls;

    #endregion

    /// <summary>
    /// Helper class for search related functionalities
    /// </summary>    
    public static class SearchHelper
    {
        #region Fields

        private static string clientId;
        private const string FilteredGetClientItems = @"fast:/sitecore/content/#admin-portal#/#client-setup#/*[@@templatename='ClientDetails'and @Active='1']";

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the page count for the retrieved result set
        /// </summary>
        public static int PageCount { get; set; }

        /// <summary>
        /// Gets the supplier search index configuration name
        /// </summary>
        public static string SupplierSearchConfig
        {
            get
            {
                return "affinion_suppliers_index";
            }
        }

        #endregion

        #region Public Methods

        public static SupplierSearchResults SearchContent(SearchKey searchKeywords, List<string> lstHotelNames = null)
        {
            try
            {
                if (searchKeywords == null)
                {
                    throw new ArgumentNullException("searchKeywords", "Passed search term is not valid.");
                }

                if (searchKeywords.Client == null)
                {
                    throw new AffinionException("Please assign a valid client from client portal root item.");
                }

                clientId = searchKeywords.Client.ID.ToString();
                var searchIndex = ContentSearchManager.GetIndex(SupplierSearchConfig);
                SupplierSearchResults searchResults = ValidateInput(searchKeywords);

                if (!string.IsNullOrEmpty(searchResults.Error))
                {
                    return searchResults;
                }

                using (var context = searchIndex.CreateSearchContext())
                {
                    searchResults = new SupplierSearchResults();

                    if (lstHotelNames == null)
                    {
                        var filterPredicate = PredicateBuilder.True<SupplierSearchResultItem>().And(i => i.TemplateName == "SupplierDetails");

                        // get supplier items from index
                        var results = context.GetQueryable<SupplierSearchResultItem>().Where(filterPredicate);
                        var filteredResults = FilterResults(results, searchKeywords);
                        results = null;

                        searchResults = FillResults(filteredResults, searchKeywords);
                        searchKeywords = TagSearchResult(searchKeywords, searchResults);
                    }
                    else
                    {
                        var filterPredicate = PredicateBuilder.True<SupplierSearchResultItem>().And(i => i.TemplateName == "SupplierDetails" && lstHotelNames.Contains(i.Name));

                        // get supplier items from index
                        var results = context.GetQueryable<SupplierSearchResultItem>().Where(filterPredicate);
                        var filteredResults = FilterResults(results, searchKeywords);
                        results = null;

                        searchResults = FillResults(filteredResults, searchKeywords);
                        searchKeywords = TagSearchResult(searchKeywords, searchResults);

                    }

                    // sort and tag results set
                    searchResults.AllResults = SortResults(searchResults.AllResults, searchKeywords).ToArray();
                }

                return searchResults;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, null);
                throw;
            }
        }

        /// <summary>
        /// Get suggested supplier names, locations and countries
        /// </summary>
        /// <param name="clientName">Client name</param>
        /// <param name="term">Term for suggestions</param>
        /// <returns>Suggestions collection</returns>
        public static IEnumerable<string> GetSuggestions(Item clientSetupItem, string term)
        {
            try
            {
                Diagnostics.Trace(DiagnosticsCategory.Search, string.Concat("SearchHelper.GetSuggestions term: ", term, " clientName: ", clientId));

                //Item clientSetupItem = null;
                string clientSetupItemId = string.Empty;

                if (string.IsNullOrWhiteSpace(term))
                {
                    return new List<string>();
                }

                //if (!string.IsNullOrWhiteSpace(clientId))
                //{
                //    clientId = clientId.ToUpperInvariant();
                //    Item clientPortalItem = Sitecore.Context.Database.GetItem(new ID(clientId));

                //    clientSetupItem = SitecoreFieldsHelper.GetDropLinkFieldTargetItem(clientPortalItem, "ClientDetails");

                if (clientSetupItem == null)
                {
                    clientSetupItemId = string.Empty;
                    Diagnostics.Trace(DiagnosticsCategory.Search, string.Concat("Client set-up item not found"));
                }
                else
                {
                    clientSetupItemId = clientSetupItem.ID.ToString();
                }
                //}


                var searchIndex = ContentSearchManager.GetIndex(SupplierSearchConfig);

                List<string> suggestions = new List<string>();
                using (var context = searchIndex.CreateSearchContext())
                {
                    term = term.ToLowerInvariant();

                    var filterPredicate = PredicateBuilder.True<SupplierSearchResultItem>().And(i => i.TemplateName == "SupplierDetails");

                    /// get supplier items from index
                    var results = context.GetQueryable<SupplierSearchResultItem>().Where(filterPredicate).ToList();

                    ///If client name not passed return all suppliers for term matching name
                    suggestions.AddRange(results.Where(p => (string.IsNullOrWhiteSpace(clientId) || p.Clients.ToUpperInvariant().Contains(clientSetupItemId))
                        && p.SupplierName.ToLowerInvariant().IndexOf(term) >= 0).Select(a => a.SupplierName));

                    ///If client name not passed return all suppliers for term matching location
                    suggestions.AddRange(results.Where(p => (string.IsNullOrWhiteSpace(clientId) || p.Clients.ToUpperInvariant().Contains(clientSetupItemId))
                    && p.Location.ToLowerInvariant().IndexOf(term) >= 0).Select(a => a.Location));

                    ///If client name not passed return all suppliers for term matching country
                    suggestions.AddRange(results.Where(p => (string.IsNullOrWhiteSpace(clientId) || p.Clients.ToUpperInvariant().Contains(clientSetupItemId))
                    && p.Country.ToLowerInvariant().IndexOf(term) >= 0).Select(a => a.Country));

                    suggestions = suggestions.Distinct().ToList();
                }
                return suggestions;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Search, ex, null);
                throw;
            }
        }

        /// <summary>
        /// Do the pagination related functionalities for the sub-layout.
        /// </summary>
        /// <param name="totalItems"></param>
        public static PagedDataSource GetPagedDataSource(SupplierSearchResultItem[] list, int pageNumber, int itemsPerPage)
        {
            if (list == null || pageNumber == 0 || itemsPerPage == 0)
            {
                return null;
            }

            PagedDataSource pagedDataSource = new PagedDataSource();
            PageCount = (int)Math.Ceiling(list.Length / (double)itemsPerPage);

            pagedDataSource.DataSource = list;
            pagedDataSource.AllowPaging = true;
            pagedDataSource.CurrentPageIndex = pageNumber > 0 ? pageNumber - 1 : 0;
            pagedDataSource.PageSize = itemsPerPage;

            return pagedDataSource;
        }


        /// <summary>
        /// Do the pagination related functionalities for the sub-layout.
        /// </summary>
        /// <param name="totalItems"></param>
        public static PagedDataSource GetHotelSearchPagedDataSource(HotelSearchResultItem[] list, int pageNumber, int itemsPerPage)
        {
            if (list == null || itemsPerPage == 0)
            {
                return null;
            }

            PagedDataSource pagedDataSource = new PagedDataSource();
            PageCount = (int)Math.Ceiling(list.Length / (double)itemsPerPage);

            pagedDataSource.DataSource = list;
            pagedDataSource.AllowPaging = true;
            pagedDataSource.CurrentPageIndex = pageNumber > 0 ? pageNumber - 1 : 0;
            pagedDataSource.PageSize = itemsPerPage;

            return pagedDataSource;
        }


        /// <summary>
        /// Gets the map coordinates of the items in current page
        /// </summary>
        /// <param name="dataSource">The data source of the current page</param>
        /// <param name="searchKeywords">Current search term</param>
        /// <returns>Returns the coordinates of loaded map locations</returns>
        public static MapData GetCordinates(PagedDataSource dataSource, SearchKey searchKeywords)
        {
            MapData mapData = new MapData();

            foreach (var item in dataSource)
            {
                SearchResultItem resultsItem = item as SearchResultItem;
                GeoCordinate cordinate = new GeoCordinate();

                if (resultsItem == null)
                {
                    continue;
                }

                Item location = SitecoreFieldsHelper.GetDropLinkFieldTargetItem(resultsItem.GetItem(), "MapLocation");

                if (location == null)
                {
                    continue;
                }

                string locationData = SitecoreFieldsHelper.GetValue(location, "MapLocation");
                cordinate.Location = SitecoreFieldsHelper.GetValue(location, "Name");
                cordinate.SupplierId = resultsItem.ItemId.ToString();
                cordinate.Latitude = "0";
                cordinate.Longitute = "0";


                if (!string.IsNullOrWhiteSpace(locationData))
                {
                    List<string> latLongList = locationData.Split(',').ToList<string>();
                    if (latLongList.Count == 2)
                    {
                        cordinate.Latitude = latLongList[0];
                        cordinate.Longitute = latLongList[1];
                    }
                }

                mapData.Locations.Add(cordinate);
            }

            mapData.ContextItemId = Sitecore.Context.Item.ID.ToString();
            mapData.ResultsType = searchKeywords.SearchResultsMode;

            return mapData;
        }

        /// <summary>
        /// Gets the map coordinates of the items in current page
        /// </summary>
        /// <param name="dataSource">The data source of the current page</param>
        /// <param name="searchKeywords">Current search term</param>
        /// <returns>Returns the coordinates of loaded map locations</returns>
        public static MapData GetNewSearchCordinates(PagedDataSource dataSource, SearchKey searchKeywords)
        {
            MapData mapData = new MapData();

            foreach (var item in dataSource)
            {
                HotelSearchResultItem resultsItem = item as HotelSearchResultItem;
                GeoCordinate cordinate = new GeoCordinate();

                if (resultsItem == null)
                {
                    continue;
                }

                if (resultsItem.SitecoreItem == null)
                {
                    continue;
                }
                Item location = SitecoreFieldsHelper.GetDropLinkFieldTargetItem(resultsItem.SitecoreItem, "MapLocation");

                if (location == null)
                {
                    continue;
                }

                string locationData = SitecoreFieldsHelper.GetValue(location, "MapLocation");
                cordinate.Location = SitecoreFieldsHelper.GetValue(location, "Name");
                cordinate.SupplierId = resultsItem.SitecoreItemId.ToString();
                cordinate.Latitude = "0";
                cordinate.Longitute = "0";


                if (!string.IsNullOrWhiteSpace(locationData))
                {
                    List<string> latLongList = locationData.Split(',').ToList<string>();
                    if (latLongList.Count == 2)
                    {
                        cordinate.Latitude = latLongList[0];
                        cordinate.Longitute = latLongList[1];
                    }
                }

                mapData.Locations.Add(cordinate);
            }

            mapData.ContextItemId = Sitecore.Context.Item.ID.ToString();
            mapData.ResultsType = searchKeywords.SearchResultsMode;

            return mapData;
        }

        /// <summary>
        /// Validate keywords for malicious input and return true if it is safe to continue
        /// </summary>
        /// <param name="keywords">The keywords</param>
        /// <returns>Returns true if the search is safe to continue</returns>
        public static bool ValidateKeywords(string keywords)
        {
            // TODO: implement validation
            return true;
        }

        public static SupplierSearchResults ValidateInput(SearchKey searchTerm)
        {
            SupplierSearchResults resultSet = new SupplierSearchResults();

            if (searchTerm == null)
            {
                return resultSet;
            }

            if (!ValidateKeywords(searchTerm.Destination))
            {
                resultSet.Error = "Invalid value for destination field.";
            }
            else if (!(searchTerm.CheckinDate >= DateTime.Today && searchTerm.CheckoutDate >= searchTerm.CheckinDate))
            {
                resultSet.Error = "Please add a valid check-in date and check-out date.";
            }

            if (!string.IsNullOrEmpty(resultSet.Error))
            {
                resultSet.AllResults = new SupplierSearchResultItem[0];
                resultSet.ResultsByLocation = new SupplierSearchResultItem[0];
                resultSet.ResultsBySupplierName = new SupplierSearchResultItem[0];
            }

            return resultSet;
        }

        /// <summary>
        /// Gets a specific supplier search results item by id
        /// </summary>
        /// <param name="id">Supplier sitecore id</param>
        /// <returns></returns>
        public static SupplierSearchResultItem GetSupplier(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return null;
            }

            var searchIndex = ContentSearchManager.GetIndex(SupplierSearchConfig);
            SupplierSearchResultItem searchResult = null;

            using (var context = searchIndex.CreateSearchContext())
            {
                // get supplier items from index
                var results = context.GetQueryable<SupplierSearchResultItem>().Where(i => i.TemplateName == "SupplierDetails").ToList();
                var suplliers = results.Where(i => i.ItemId.ToString() == id);
                if (suplliers.Any())
                {
                    searchResult = suplliers.First();
                }
            }

            return searchResult;
        }

        /// <summary>
        /// Get all supplier items by client
        /// </summary>
        /// <param name="clientSitecoreId">The sitecore id of the client</param>
        /// <returns>Returns a collection of suppliers</returns>
        public static IEnumerable<SupplierSearchResultItem> GetSuppliersByClient(string clientSitecoreId)
        {
            if (string.IsNullOrEmpty(clientSitecoreId))
            {
                return null;
            }

            var searchIndex = ContentSearchManager.GetIndex(SupplierSearchConfig);
            IEnumerable<SupplierSearchResultItem> searchResult = null;

            using (var context = searchIndex.CreateSearchContext())
            {
                // get supplier items from index
                var results = context.GetQueryable<SupplierSearchResultItem>().Where(i => i.TemplateName == "SupplierDetails").ToList();
                var suplliers = results.Where(i => i.Clients.Contains(clientSitecoreId));
                if (suplliers.Any())
                {
                    return suplliers;
                }
            }

            return searchResult;
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Gets the price from index field for comparison
        /// </summary>
        /// <param name="price">Price field value from supplier index</param>
        /// <returns>Returns the decimal value</returns>
        private static decimal GetPrice(SupplierSearchResultItem resultItem, decimal ignoreValue)
        {
            try
            {
                Collection<ProductPrice> productPrices = resultItem.ProductPrices;

                if (resultItem == null || productPrices.Count == 0)
                {
                    return ignoreValue;
                }

                //string price = resultItem.LowestPrice;
                //if (string.IsNullOrEmpty(price))
                //{
                //    return ignoreValue;
                //}

                //string clientPrice = price.Split(',').Where(s => s.Contains(clientId)).First().ToString().Replace(clientId + "=", "");
                //decimal value = decimal.Parse(clientPrice);

                ///Get lowest price of the products
                decimal value = productPrices.Where(a => a.ClientId.Equals(clientId)).Min(p => p.Price);

                if (value == 0)
                {
                    return ignoreValue;
                }

                return value;
            }
            catch (NullReferenceException ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.BusinessLogic, ex, null);
                // no price information found for the given client
                return ignoreValue;
            }
        }

        /// <summary>
        /// Sort search results
        /// </summary>
        /// <param name="results">Current results set</param>
        /// <param name="searchKeywords">Current search term</param>
        /// <returns>Returns the sorted results set</returns>
        private static IEnumerable<SupplierSearchResultItem> SortResults(IEnumerable<SupplierSearchResultItem> results, SearchKey searchKeywords)
        {
            if (searchKeywords.SortMode == SortMode.SupplierName && searchKeywords.SortValue == "0")
            {
                results = results.OrderBy(i => i.SupplierName).ToList();
            }
            else if (searchKeywords.SortMode == SortMode.SupplierName && searchKeywords.SortValue == "1")
            {
                results = results.OrderByDescending(i => i.SupplierName).ToList();
            }
            else if (searchKeywords.SortMode == SortMode.Price && searchKeywords.SortValue == "0")
            {
                results = results.OrderBy(i => GetPrice(i, decimal.MaxValue)).ToList();
            }
            else if (searchKeywords.SortMode == SortMode.Price && searchKeywords.SortValue == "1")
            {
                results = results.OrderByDescending(i => GetPrice(i, decimal.MinValue)).ToList();
            }
            else if (LocationSearchSort(ref results, searchKeywords))
            {
                return results;
            }
            else if (HotelSearchSort(ref results, searchKeywords))
            {
                return results;
            }

            /// Offline Portal Sorting
            else if (searchKeywords.SortMode == SortMode.Stars && searchKeywords.SortValue == "0")
            {
                results = results.OrderBy(i => i.StarRanking).ToList();
            }
            else if (searchKeywords.SortMode == SortMode.Stars && searchKeywords.SortValue == "1")
            {
                results = results.OrderByDescending(i => i.StarRanking).ToList();
            }
            else if (searchKeywords.SortMode == SortMode.Recommend && searchKeywords.SortValue == "0")
            {
                results = results.OrderBy(i => i.LocalRanking).ToList();
            }
            else if (searchKeywords.SortMode == SortMode.Recommend && searchKeywords.SortValue == "1")
            {
                results = results.OrderByDescending(i => i.LocalRanking).ToList();
            }
            else if (searchKeywords.SortMode == SortMode.DistanceFromCityCentre && searchKeywords.SortValue == "0")
            {
                // TODO: Implement Sort By DistanceFromCityCentre
            }

            return results;
        }

        private static bool LocationSearchSort(ref IEnumerable<SupplierSearchResultItem> results, SearchKey searchKeywords)
        {
            ///When searched for a location or mixed and select sort by ranking in ascending, sort by ranking and then by supplier name
            if ((searchKeywords.Mode == PageMode.Locations || searchKeywords.Mode == PageMode.LocationFinder)
                && searchKeywords.SortMode == SortMode.Rate && searchKeywords.SortValue == "0")
            {
                results = results.OrderBy(i =>
                {
                    if (string.IsNullOrWhiteSpace(i.LocalRanking))
                    {
                        return string.Empty;
                    }

                    return i.LocalRanking;

                }).ThenBy(i => i.SupplierName).ToList();

                return true;
            }
            ///When searched for a location or mixed without selecting sort order, sort by ranking in descending and then by supplier name in ascending
            ///Or When searched for a location or mixed and select sort by ranking in descending, sort by ranking in descending and then by supplier name in ascending
            else if ((searchKeywords.Mode == PageMode.Locations || searchKeywords.Mode == PageMode.LocationFinder)
                && (searchKeywords.SortMode == SortMode.None || (searchKeywords.SortMode == SortMode.Rate && searchKeywords.SortValue == "1")))
            {
                results = results.OrderByDescending(i =>
                {
                    if (string.IsNullOrWhiteSpace(i.LocalRanking))
                    {
                        return string.Empty;
                    }

                    return i.LocalRanking;

                }).ThenBy(i => i.SupplierName).ToList();

                return true;
            }

            return false;
        }

        private static bool HotelSearchSort(ref IEnumerable<SupplierSearchResultItem> results, SearchKey searchKeywords)
        {
            ///When searched for a hotel without selecting sort order, sort by ranking in descending and then by price in ascending   
            ///Or When searched for a hotel and select sort by ranking in descending, sort by ranking in descending and then by price in ascending
            //if (searchKeywords.SortMode == SortMode.None && searchKeywords.SearchResultsMode == SearchResultsMode.SupplierName
            //   || (searchKeywords.SortMode == SortMode.Rate && searchKeywords.SortValue == "1" && searchKeywords.SearchResultsMode == SearchResultsMode.Location))
            if (searchKeywords.Mode == PageMode.Hotels && (searchKeywords.SortMode == SortMode.None
               || (searchKeywords.SortMode == SortMode.Rate && searchKeywords.SortValue == "1")))
            {
                results = results.OrderByDescending(i =>
                {
                    if (string.IsNullOrWhiteSpace(i.LocalRanking))
                    {
                        return string.Empty;
                    }

                    return i.LocalRanking;

                }).ThenBy(i =>
                {
                    //decimal price = GetPrice(i.LowestPrice, decimal.MaxValue);
                    if (i.ProductPrices.Count == 0)
                    {
                        return decimal.MaxValue;
                    }

                    decimal price = i.ProductPrices.Min(p => p.Price);

                    ///return maximum value if the price is 0 so sorting would happen correctly
                    return price;

                }).ThenByDescending(i =>
                {
                    return i.StarRanking;

                }).ThenBy(i => i.SupplierName).ToList();

                return true;
            }
            ///When searched for a hotel and select sort by ranking in descending, sort by ranking in descending and then by price in ascending
            else if (searchKeywords.Mode == PageMode.Hotels && searchKeywords.SortMode == SortMode.Rate && searchKeywords.SortValue == "0")
            {
                results = results.OrderBy(i =>
                {
                    if (string.IsNullOrWhiteSpace(i.LocalRanking))
                    {
                        return string.Empty;
                    }

                    return i.LocalRanking;

                }).ThenBy(i =>
                {
                    decimal price = GetPrice(i, decimal.MaxValue);
                    ///return maximum value if the price is 0 so sorting would happen correctly
                    return price;

                }).ThenBy(i =>
                {
                    return i.StarRanking;
                }).ThenBy(i => i.SupplierName).ToList();

                return true;
            }

            return false;

        }

        /// <summary>
        /// Format search results and fill the return object
        /// </summary>
        /// <param name="list">The list of supplier search results</param>
        /// <param name="searchKeywords">The search keywords object</param>
        /// <returns>Returns the search results</returns>
        private static SupplierSearchResults FillResults(IEnumerable<SupplierSearchResultItem> list, SearchKey searchKeywords)
        {
            SupplierSearchResults searchResults = new SupplierSearchResults();

            if (!string.IsNullOrEmpty(searchKeywords.Destination))
            {
                IEnumerable<SupplierSearchResultItem> resultsBySupplierName = list.Where(i => MatchText(i.SupplierName, searchKeywords.Destination));
                IEnumerable<SupplierSearchResultItem> resultsByDestination = SearchResultsByLocation(searchKeywords.Destination, list);
                searchResults.ResultsBySupplierName = resultsBySupplierName.ToArray();
                searchResults.ResultsByLocation = resultsByDestination.ToArray();

                List<SupplierSearchResultItem> allResults = new List<SupplierSearchResultItem>(resultsBySupplierName.Union(resultsByDestination));
                searchResults.AllResults = allResults.ToArray();
            }
            else
            {
                if (!string.IsNullOrEmpty(searchKeywords.Location))
                {
                    IEnumerable<SupplierSearchResultItem> resultsByLocation = SearchResultsByLocation(searchKeywords.Location, list);
                    searchResults.AllResults = searchResults.ResultsByLocation = resultsByLocation.ToArray();
                }
                else
                {
                    searchResults.AllResults = list.ToArray();
                    searchResults.ResultsByLocation = new SupplierSearchResultItem[0];
                }

                searchResults.ResultsBySupplierName = new SupplierSearchResultItem[0];
            }

            return searchResults;
        }

        /// <summary>
        /// Filter results by location
        /// </summary>
        /// <param name="location">keyword containing the location</param>
        /// <param name="list">results list</param>
        /// <returns>Returns the filterd list</returns>
        private static IEnumerable<SupplierSearchResultItem> SearchResultsByLocation(string location, IEnumerable<SupplierSearchResultItem> list)
        {
            if (location == "all")
            {
                return list;
            }

            List<SupplierSearchResultItem> resultsByLocation = list.Where(i =>
                    MatchText(i.AddressLine1, location) ||
                    MatchText(i.AddressLine2, location) ||
                    MatchText(i.AddressLine3, location) ||
                    MatchText(i.AddressLine4, location) ||
                    MatchText(i.Location, location) ||
                    MatchText(i.Country, location) ||
                    MatchText(i.Town, location)).ToList();

            return resultsByLocation;
        }

        /// <summary>
        /// Filter results by StarRanking
        /// </summary>
        /// <param name="starRanking">keyword containing the StarRanking</param>
        /// <param name="list">results list</param>
        /// <returns>Returns the filterd list</returns>
        private static IEnumerable<SupplierSearchResultItem> SearchResultsByStraRanking(string starRanking, IEnumerable<SupplierSearchResultItem> list)
        {
            if (starRanking == "all")
            {
                return list;
            }

            List<SupplierSearchResultItem> resultsByStraRanking = list.Where(i =>

                    MatchText(i.StarRanking, starRanking) ||
                    MatchText(i.StarRanking, starRanking)).ToList();

            return resultsByStraRanking;
        }

        /// <summary>
        /// Tag results set depending on the type of search it performed
        /// </summary>
        /// <param name="searchKeywords">The search terms object</param>
        /// <param name="searchResults">The current results set</param>
        /// <returns>Returns the updated search term object with search type tag</returns>
        private static SearchKey TagSearchResult(SearchKey searchKeywords, SupplierSearchResults searchResults)
        {
            if (searchResults.AllResults.Length > 0 && searchResults.ResultsByLocation.Length > 0 && searchResults.ResultsBySupplierName.Length <= 0)
            {
                searchKeywords.SearchResultsMode = SearchResultsMode.Location;
            }
            else if (searchResults.AllResults.Length > 0 && searchResults.ResultsByLocation.Length <= 0 && searchResults.ResultsBySupplierName.Length > 0)
            {
                searchKeywords.SearchResultsMode = SearchResultsMode.SupplierName;
            }
            else if (searchResults.AllResults.Length > 0)
            {
                searchKeywords.SearchResultsMode = SearchResultsMode.Mixed;
            }
            else
            {
                searchKeywords.SearchResultsMode = SearchResultsMode.None;
            }

            return searchKeywords;
        }

        /// <summary>
        /// Performs a case in-sensitive match on two strings
        /// </summary>
        /// <param name="value1">The first string value</param>
        /// <param name="value2">The second string value</param>
        /// <returns>Returns true if match found</returns>
        private static bool MatchText(string value1, string value2)
        {
            if (string.IsNullOrEmpty(value1) || string.IsNullOrEmpty(value2))
            {
                return false;
            }

            return value1.ToLowerInvariant().IndexOf(value2.ToLowerInvariant()) >= 0;
        }


        /// <summary>
        /// Filter search results
        /// </summary>
        /// <param name="results">Full results set returned from index</param>
        /// <param name="searchTerm">The search term</param>
        /// <returns>Returns a filtered list</returns>
        private static IEnumerable<SupplierSearchResultItem> FilterResults(IEnumerable<SupplierSearchResultItem> results, SearchKey searchTerm)
        {
            if (searchTerm == null)
            {
                throw new ArgumentNullException("searchTerm");
            }

            List<SupplierSearchResultItem> filteredResults = new List<SupplierSearchResultItem>();
            ICollection<ISupplierFilter> supplierFilters = searchTerm.SupplierFilters;
            ICollection<ISupplierFilter> filters = supplierFilters != null ? supplierFilters : GetSupplierFilters();

            //Item clientItem = SitecoreFieldsHelper.GetLookupFieldTargetItem(ItemHelper.RootItem, "ClientDetails");
            Item clientItem = searchTerm.Client;

            if (clientItem == null)
            {
                throw new AffinionException("Supplier portal root item is not created with the correct template. It should contain a field 'ClientDetails' pointing to a valid client details item.");
            }


            foreach (SupplierSearchResultItem resultItem in results)
            {
                SupplierFilterData filterData = new SupplierFilterData();
                filterData.Client = clientItem;
                filterData.SearchTerm = searchTerm;
                filterData.ResultsItem = resultItem;

               /* if (SkipSupplier(filters, filterData))
                {
                    continue;
                }*/

                FilterProductPrices(resultItem, searchTerm);
                //resultItem.SetProductPrices(filteredProductPrices);

                filteredResults.Add(resultItem);
            }

            return filteredResults;
        }

        /// <summary>
        /// Filter campaign offers which does not fulfil search criteria
        /// </summary>
        /// <param name="resultItem">SupplierSearchResultItem</param>
        /// <param name="searchTerm">SearchKey</param>
        private static void FilterProductPrices(SupplierSearchResultItem resultItem, SearchKey searchTerm)
        {
            Collection<ProductPrice> productPrices = new Collection<ProductPrice>();

            ValidateFilterProductPricesParameters(resultItem);

            foreach (ProductPrice product in resultItem.ProductPrices)
            {
                bool isValidProduct = true;

                ///If product is a base product add it to the product list
                if (product.offerType == OfferTypes.BaseProduct)
                {
                    productPrices.Add(product);
                    continue;
                }

                ///Not a base product
                foreach (var campaign in resultItem.ApplicableCampaigns)
                {
                    ///Find applicable campaign for the product
                    if (campaign.CampaignItemId == product.CampaignItemId)
                    {
                        ///Check whether check-in and checkout date fall within campaign dates
                        if (searchTerm.CheckinDate < campaign.StartDate || searchTerm.CheckinDate >= campaign.EndDate
                           || searchTerm.CheckoutDate <= campaign.StartDate || searchTerm.CheckoutDate > campaign.EndDate)
                        {
                            //productPrices.Add(product);
                            isValidProduct = false;
                            break;
                        }

                        ///Check whether offer number of night matches, searched number of nights
                        if (campaign.NoOfNights != null && campaign.NoOfNights.Count != 0 && !campaign.NoOfNights.Contains(searchTerm.NoOfNights))
                        {
                            isValidProduct = false;
                            break;
                        }

                        ///Check whether subrate is added to offer
                        if (!SubrateApplied(searchTerm.CurrencyCode, product))
                        {
                            isValidProduct = false;
                            break;
                        }
                    }
                }

                ///If offer criteria is matched, add offer to product list
                if (isValidProduct)
                {
                    productPrices.Add(product);
                }
            }
            resultItem.SetProductPrices(productPrices);

            //return  productPrices;
        }

        /// <summary>
        /// Validate parameter
        /// </summary>
        /// <param name="resultItem">SupplierSearchResultItem</param>
        private static void ValidateFilterProductPricesParameters(SupplierSearchResultItem resultItem)
        {
            if (resultItem == null)
            {
                throw new ArgumentNullException("resultItem");
            }

            if (resultItem.ProductPrices == null)
            {
                throw new ArgumentNullException("resultItem.ProductPrices");
            }

            if (resultItem.ApplicableCampaigns == null)
            {
                // throw new ArgumentNullException("resultItem.ApplicableCampaigns");
            }
        }

        private static bool SubrateApplied(string currencyCode, ProductPrice product)
        {
            OfferDataItem offer = new OfferDataItem();
            offer.ClientId = new Guid(product.ClientId);
            offer.CurrencyDetails = new CurrencyInfo(currencyCode);

            offer.UpdateSubrateInformation(product.CampaignItemId.ToString(), offer);

            if (offer.SubrateApplied)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Returns true to skip the offer
        /// </summary>
        /// <param name="offer">Current offer</param>
        /// <param name="searchKey">Search term</param>
        /// <param name="Supplier">Current supplier</param>
        /// <param name="Client">Current Client</param>
        /// <returns>Returns true to include the offer</returns>
        private static bool SkipSupplier(ICollection<ISupplierFilter> filters, SupplierFilterData filterData)
        {
            bool skip = false;
            foreach (ISupplierFilter filter in filters)
            {
                skip = !filter.IncludeSupplier(filterData);

                if (skip)
                {
                    break;
                }
            }

            return skip;
        }

        /// <summary>
        /// Gets Immplementations of IOfferFilter in the "Affinion.LoyaltyBuild.BusinessLogic.Search.OfferFilters" namespace
        /// </summary>
        /// <returns>Returns a collection of filters</returns>
        private static ICollection<ISupplierFilter> GetSupplierFilters()
        {
            ICollection<ISupplierFilter> filters = new Collection<ISupplierFilter>();

            try
            {
                Type[] types = Assembly.GetExecutingAssembly().GetTypes().
                    Where(t =>
                        String.Equals(t.Namespace, "Affinion.LoyaltyBuild.Search.SupplierFilters", StringComparison.Ordinal) &&
                        Type.Equals(t.GetInterface("ISupplierFilter"), typeof(ISupplierFilter))
                    ).ToArray();

                foreach (Type type in types)
                {
                    ISupplierFilter instance = (ISupplierFilter)Activator.CreateInstance(type);
                    filters.Add(instance);
                }

                return filters;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, null);
                return null;
            }
        }

        #endregion

        #region Offlile Portal

        /// <summary>
        /// Query to get Filtered call centre user
        /// </summary>
        private const string FilteredCallCentreUserItem = @"fast://sitecore/content/#admin-portal#/#client-setup#/global/#_supporting-content#/callcentreuser/*[@@templatename='CallCentreUser' and @User ='{0}']";

        /// <summary>
        /// Query to get Filtered Client items
        /// </summary>        
        private const string FilteredClientItems = @"fast:/sitecore/content/#admin-portal#/#client-setup#/*[@@templatename='ClientDetails' and @Active='1' and @CallCentre='{0}']";

        /// <summary>
        /// Query to get All Users Under a Specific Call Center
        /// </summary>
        private const string FilteredCallCentreUsersOnLocation = @"fast://sitecore/content/#admin-portal#/#client-setup#/global/#_supporting-content#/callcentreuser/*[@@templatename='CallCentreUser' and @CallCentre='{0}']";
        
        /// <summary>
        /// Query to Get Call Centre Details with Call Center Name
        /// </summary>
        private const string FilteredCallCentrItemOnName= @"fast://sitecore/content/#admin-portal#/#client-setup#/global/#_supporting-content#/callcentre/*[@@templatename='callcentre' and @callcentre ='{0}']";
        /// <summary>
        /// Retrive user specific client items for offline portal
        /// </summary>
        /// <returns></returns>
        public static List<Item> GetUserSpecificClientItems()
        {
            try
            {
                /// Get Sitecore Context DB
                Database contextDb = Sitecore.Context.Database;

                Item callCentreUserItem = contextDb.SelectItems(string.Format(FilteredCallCentreUserItem, Sitecore.Context.User.Name)).SingleOrDefault();

                string callCentre = SitecoreFieldsHelper.GetDropLinkFieldValue(callCentreUserItem, "CallCentre", "CallCentre");

                if (callCentre.Length != 0)
                {
                    /// Get Client items
                    var clientItems = contextDb.SelectItems(string.Format(FilteredClientItems, callCentre));

                    return clientItems.ToList();
                }
                return null;
            }
            catch (InvalidOperationException invalidEx)
            {
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, invalidEx, null);
                throw new InvalidOperationException("Multiple call centres can not be assigned for single user");
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, null);
                throw;
            }
        }

        /// <summary>
        /// Get Logged-in users call centre item
        /// </summary>
        /// <returns>String call centre</returns>
        public static Item GetUserCallCentre()
        {
            try
            {

                /// Get Sitecore Context DB
                Database contextDb = Sitecore.Context.Database;
                Item callCentreUserItem = contextDb.SelectItems(string.Format(FilteredCallCentreUserItem, Sitecore.Context.User.Name)).SingleOrDefault();
                return SitecoreFieldsHelper.GetDropLinkFieldTargetItem(callCentreUserItem, "CallCentre");
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, null);
                throw;
            }
        }


        public static List<Item> GetCallCentreUsersOnCallCentreName(string callCentreName)
        {
            try
            {

                /// Get Sitecore Context DB
                Database contextDb = Sitecore.Context.Database;
                List<Item> callCentreUserItem = new List<Item>();

                //Get the Call Centre Details with Its Name
                var callCentreDetail = contextDb.SelectItems(string.Format(FilteredCallCentrItemOnName, callCentreName)).FirstOrDefault();

                if (callCentreDetail != null)
                {
                    //Get all the Agent Details with the Call Centre id
                    callCentreUserItem = contextDb.SelectItems(string.Format(FilteredCallCentreUsersOnLocation, callCentreDetail.ID)).ToList();
                }
                return callCentreUserItem;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, null);
                throw;
            }
        }
       
        #endregion

        #region Client Portal           

        /// <summary>
        /// Get Client Items
        /// </summary>
        /// <returns>client Items</returns>
        public static List<Item> GetClientItems()
        {
            Database contextDb = Sitecore.Context.Database;
            var clientItems = contextDb.SelectItems(string.Format(FilteredGetClientItems));
            return clientItems.ToList();
        }

        #endregion
    }
}
