﻿CREATE PROCEDURE [dbo].[LB_UpdateRoomAvailability]
	@orderlineid int=null,
	@IsCancel bit=0 
AS
BEGIN 
	DECLARE @CampaignItemID int
	DECLARE @Count INT
	DECLARE @Sku VARCHAR(50)
	SET @Count = CASE WHEN @IsCancel= 0 THEN +1 ELSE -1 END
	IF @orderlineid IS NOT NULL
	BEGIN		
		BEGIN TRY
			SELECT TOP 1 @CampaignItemID = CampaignItemId from ucommerce_orderlinecampaignrelation where OrderLineId = @orderlineid
			SELECT TOP 1 @Sku = Sku from ucommerce_orderline where OrderLineId = @orderlineid
			IF @campaignitemID IS NOT NULL
			BEGIN
				--Update Offer Availability table 			
				UPDATE oa SET oa.NumberBooked = (oa.NumberBooked+@Count)  FROM LB_OfferAvailability oa
				INNER JOIN ucommerce_orderline ol on ol.Sku = oa.VariantSku
				INNER JOIN LB_BookingInfo BI on BI.orderlineid = ol.orderlineid
				WHERE 
					ol.orderlineid = @orderlineid AND 
					oa.VariantSku = @Sku AND
					oa.CampaignItem = @campaignitemID AND
					BI.checkindate <= dbo.GetDateByCounter(oa.datecounter) AND				
					BI.checkoutdate > dbo.GetDateByCounter(oa.datecounter)
			END	
			--Update Room Availability table 
			UPDATE ra SET ra.NumberBooked = (ra.NumberBooked+@Count) FROM LB_RoomAvailability ra
			INNER JOIN ucommerce_orderline ol on ol.Sku = ra.VariantSku
			INNER JOIN LB_BookingInfo BI on BI.orderlineid = ol.orderlineid
			WHERE 
				ol.orderlineid = @orderlineid AND 
				ra.VariantSku = @Sku AND
				BI.checkindate <= dbo.GetDateByCounter(ra.datecounter) AND				
				BI.checkoutdate > dbo.GetDateByCounter(ra.datecounter)
		END TRY
		BEGIN CATCH
		END CATCH	
	END
 END
 
