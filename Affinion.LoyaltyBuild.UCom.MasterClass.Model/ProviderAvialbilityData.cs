﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    public class ProviderAvialbilityData
    {
        public ProviderAvialbilityData()
        {
            RoomAvailability = new List<RoomDetail>();
        }

        public DateTime StatDate { get; set; }

        public DateTime EndDate { get; set; }

        public int Month { get; set; }

        public List<RoomDetail> RoomAvailability { get; set; }


    }

    public class RoomAndOfferAvaialbityShortage
    {
        public RoomAndOfferAvaialbityShortage()
        {
            this.OfferShortage = new List<OfferShortage>();
            this.RoomShortage = new List<RoomShortage>();
        }
        public DateTime Date { get; set; }

        public List<OfferShortage> OfferShortage { get; set; }

        public List<RoomShortage> RoomShortage { get; set; }

    }

    public class RoomShortage
    {
        public string ProductSku { get; set; }

        public string RoomName { get; set; }

        public int AvailableRoom { get; set; }

        public int BookedRoom { get; set; }

        public bool DataExist { get; set; }

        public bool IsCloseOut { get; set; }

        public bool IsSoldOut { get; set; }
        //public bool 
    }

    public class OfferShortage
    {
        public string RoomName { get; set; }

        public string ProductSku { get; set; }

        public int CampaignItemId { get; set; }

        public string OfferName { get; set; }

        public int AvailableRoom { get; set; }

        public int BookedRoom { get; set; }

        public bool DataExist { get; set; }
    }
}
