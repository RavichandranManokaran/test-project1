﻿create PROCEDURE [dbo].[Lb_addcanceldue] @OrderLineId INT, 
                                 @CreatedBy   VARCHAR(50)
                                                       
                                                         
AS 
BEGIN 
	-- SET NOCOUNT ON added to prevent extra result sets from 
	-- interfering with SELECT statements. 
	SET nocount ON; 

	-- Insert statements for procedure here 
	DECLARE @CancelFee       MONEY, 
			@CurrencyId      INT, 
			@CreatedDate     DATETIME, 
			@PaymentMethodId INT 

	SELECT TOP 1 
		@CreatedDate = Getdate(), 
		@CancelFee = OP.[value], 
		@CurrencyId = PO.[currencyid] 
	FROM   [ucommerce_orderproperty] OP 
		INNER JOIN [ucommerce_orderline] OL ON OP.[orderlineid] = OL.[orderlineid] 
		INNER JOIN [ucommerce_purchaseorder] PO ON OL.[orderid] = PO.[orderid] 
	WHERE  
		OP.orderlineid = @OrderLineId 
		AND OP.[key] = '_CancellationFee'
      
	DECLARE @bbcancelcharge MONEY = 0
	SELECT 
		@bbcancelcharge = CONVERT(MONEY, OP.[value]) 
	FROM   ucommerce_orderproperty OP 
	WHERE  
		OP.[key] = '_BBCancellationCharges' 
        AND OP.orderlineid = @OrderLineId 

	SET @CancelFee = @CancelFee + @bbcancelcharge 
      
		
    --Set the commision amount due as delete 
	UPDATE OLD 
	SET 
		[DueTypeId] = DT2.DueTypeId, 
		[totalamount] = @CancelFee, 
		[updatedby] = @CreatedBy, 
		[updateddate] = @CreatedDate 
	FROM 
		[dbo].[lb_orderlinedue] OLD
		INNER JOIN LB_DueTypes DT1 ON OLD.DueTypeId = DT1.DueTypeId AND DT1.Code = 'COMMISSION'
		CROSS JOIN LB_DueTypes DT2
	WHERE  
		DT2.Code = 'CANCELFEE'
		AND OLD.[orderlineid] = @OrderLineId

	--Add Refund for provider Amount 
	IF EXISTS (SELECT 1 WHERE  1 = [dbo].[Lb_fnispaidinfull](@OrderLineId)) 
	BEGIN 
        DECLARE @ProviderAmt MONEY,
			@Bookingid UNIQUEIDENTIFIER,
			@Duetype UNIQUEIDENTIFIER

		SELECT @Bookingid= BookingId  from [dbo].[LB_BookingInfo]  where OrderLineId=@OrderLineId;
			
		SELECT TOP 1 
			@ProviderAmt = -ol.ProviderAmount
		FROM [LB_OrderLineDue] ol 
			INNER JOIN [LB_DueTypes] dt on ol.DueTypeId=dt.DueTypeId 
		WHERE 
			ol.OrderLineID=@OrderLineId and dt.code='PROVIDERAMOUNT'
			
		SELECT @Duetype = DueTypeId from LB_DueTypes where CODE='REFUND'

		SELECT TOP 1 @PaymentMethodID = P.PaymentMethodId FROM uCommerce_OrderLine OL 
		INNER JOIN uCommerce_Payment P ON OL.OrderId = P.OrderId
		WHERE OL.OrderLineId IN (SELECT * FROM dbo.LB_FnRelatedOrderLines(@OrderLineId))
		
		EXEC [Lb_saveorderlinedue] 
			NULL, 
			@OrderLineId, 
			@Bookingid,
			@CurrencyId, 
			@Duetype, 
			@ProviderAmt, 
			0, 
			@ProviderAmt,               
			0,
			0, 
			'', 
			@CreatedBy, 
			@CreatedDate, 
			@PaymentMethodId 

	END
END
