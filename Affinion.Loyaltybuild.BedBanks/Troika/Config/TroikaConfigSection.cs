﻿using System.Configuration;

namespace Affinion.LoyaltyBuild.BedBanks.Troika.Config
{
    public class TroikaConfigSection : ConfigurationSection
    {
        [ConfigurationProperty("clientCollection", IsDefaultCollection = true, IsKey = false, IsRequired = true)]
        public ClientCollection Members
        {
            get
            {
                return base["clientCollection"] as ClientCollection;
            }

            set
            {
                base["clientCollection"] = value;
            }
        }
    }
}
