﻿//<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           RealexPaymentMethodService.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal
/// Description:           Payment service to integrate ucommerce with payment gateway
/// </summary>

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using UCommerce;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure.Globalization;
using UCommerce.Transactions.Payments;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.PaymentService
{
    public class RealexPaymentMethodService : ExternalPaymentMethodService
    {
        protected override bool AcquirePaymentInternal(UCommerce.EntitiesV2.Payment payment, out string status)
        {
            throw new NotImplementedException();
        }

        protected override bool CancelPaymentInternal(UCommerce.EntitiesV2.Payment payment, out string status)
        {
            throw new NotImplementedException();
        }

        public override void ProcessCallback(UCommerce.EntitiesV2.Payment payment)
        {
            throw new NotImplementedException();
        }

        protected override bool RefundPaymentInternal(UCommerce.EntitiesV2.Payment payment, out string status)
        {
            throw new NotImplementedException();
        }

        public override string RenderPage(PaymentRequest paymentRequest)
        {
            RealexPageBuilder PageBuilder = new RealexPageBuilder();
            return PageBuilder.Build(paymentRequest);
        }

        public override UCommerce.EntitiesV2.Payment RequestPayment(PaymentRequest paymentRequest)
        {
            if (paymentRequest.Payment == null)
            {
                paymentRequest.Payment = this.CreatePayment(paymentRequest);
            }
            Payment payment = paymentRequest.Payment;
            string url = string.Format("/{0}/{1}/PaymentRequest.axd", payment.PaymentMethod.PaymentMethodId, payment["paymentGuid"]);
            HttpContext.Current.Response.Redirect(url, false);
            return payment;
        }

        public override Payment CreatePayment(PaymentRequest request)
        {
            request.PaymentMethod.FeePercent = 0;
            return base.CreatePayment(request);
        }
    }
}