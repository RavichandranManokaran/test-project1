﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SuperValuPromoBoxLinkSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.SuperValuPromoBoxLinkSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<li>
    <p id="PlaceholderText" runat="server">Set the datasource: Required Fields: Link, Text</p>
    <sc:Link Field="Link" ID="TextLink" runat="server"></sc:Link>
    <p><sc:Text Field="Text" ID="Text" runat="server" /></p>
</li>
