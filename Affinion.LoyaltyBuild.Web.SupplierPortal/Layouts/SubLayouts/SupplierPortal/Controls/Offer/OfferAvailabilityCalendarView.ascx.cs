﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Offer
{
    public partial class OfferAvailabilityCalendarView : System.Web.UI.UserControl
    {
        public int CurrentMonth { get; set; }
        public OfferAvailabilityCalendarView()
        {
        }

        public OfferAvailabilityCalendarView(ProviderAvialbilityData currenItem)
            : this()
        {
            this.currenItem = currenItem;
        }

        public ProviderAvialbilityData currenItem { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.DataBind();
            //  if(Cu
            this.checkRoles();
        }


        public void checkRoles()
        {
            string currentUserRole = SitecoreUserProfile.GetItem(UserProfileKeys.currentLoggedinUserRole);
            if (currentUserRole.ToLower().Contains(Constants.accountManagement) || currentUserRole.ToLower().Contains(Constants.marketing) || currentUserRole.ToLower().Contains(Constants.IT) || currentUserRole.ToLower().Contains(Constants.superUser))
            {
                roleBasedDisableButtons.Value = "false";
            }
            else if (currentUserRole.ToLower().Contains(Constants.grpHotelRole) || currentUserRole.ToLower().Contains(Constants.hotelSupervisor) || currentUserRole.ToLower().Contains(Constants.lbAdminRole))
            {
                roleBasedDisableButtons.Value = "false";
            }
            else if (currentUserRole.ToLower().Contains(Constants.finance)|| currentUserRole.ToLower().Contains(Constants.hotelRole))
            {
                roleBasedDisableButtons.Value = "true";
            }
        }
    }
}