﻿using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Model.Room
{
    [Serializable]
    [XmlType(TypeName = "RoomType")]
    public class JtRoomType
    {
        //API Response Fields
        public string Seq { get; set; }
        public string PropertyRoomTypeID { get; set; }
        public string BookingToken { get; set; }
        public string RoomType { get; set; }
        public string MealBasisID { get; set; }
        public string RoomView { get; set; }
        public string MealBasis { get; set; }
        public string SubTotal { get; set; }
        public string Adults { get; set; }
        public string Children { get; set; }
        public string Infants { get; set; }
        public JtAdjustment[] Adjustments { get; set; }
        public string OptionalSupplements { get; set; }
        public string Discount { get; set; }
        public Boolean OnRequest { get; set; }
        public double LocalTax { get; set; }
        public double NetTotal { get; set; }
        public decimal Total { get; set; }
        public List<Erratum> Errata { get; set; }

        //Custom Fields
        public decimal Margin { get { return 10.0m; } }
        public decimal TotalIncMargin { get; set; }
        public string PropertyID { get; set; }
        public string Sku { get; set; }
        public string VariantSku { get; set; }
        public string PriceToDisplay { get; set; }
        public string CurrencyId { get; set; } 
    }
}