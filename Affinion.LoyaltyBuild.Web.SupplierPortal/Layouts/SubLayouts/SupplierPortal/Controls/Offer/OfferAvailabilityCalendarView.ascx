﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfferAvailabilityCalendarView.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Offer.OfferAvailabilityCalendarView" %>
<input type="hidden" class="roleBasedDisableButtons" id="roleBasedDisableButtons" runat="server" />
<div id="dv_<%= currenItem.Month %>">
   <%--<div class="cal-table-top no-padding">
      <div class="col-md-12 padding0">
          <div class="col-md-3 no-padding">
              <button id="btnbackavilability" clientidmode="Static" runat="server" class="btn btn-link font-size-12 view_btn" type="button" onclick="Supplier.ManageOfferAvailability.RedirectToBacktoAvailability()">
                  <span aria-hidden="true" class="glyphicon glyphicon-backward"></span>&nbsp;Back to Manage Availbility 
              </button>
          </div>
          <div class="col-md-9 no-padding">
             <div class="cal-table-top-right float-right">
                  <button type="button" class="btn btn-info font-size-12 noprint" data-toggle="modal" data-target="#help"><span class="glyphicon glyphicon-question-sign"></span>Help</button>
                  <button class="btn btn-info font-size-12" type="button" onclick="Supplier.ManageOfferAvailability.GetReport()">Annual Reports</button>
                  <button type="button" class="btn btn-info noprint" onclick="Supplier.ManageOfferAvailability.ClearChanges(<%= currenItem.Month %>)">Clear Changes </button>
                  <button id="btn_<%= currenItem.Month %>" type="button" class="btn btn-orange noprint" onclick="Supplier.ManageOfferAvailability.UpdateChanges(<%= currenItem.Month %>)">Update and Save Changes </button>
              </div>
          </div>
      </div>
      </div>--%>
   <div class="tabale-top Offeavailability">
      <table class="cal-table" id="calendarTable1_<%= currenItem.Month %>" data-start-day="sun">
         <tr>
            <th><span class="cal-hed"><%= currenItem.StatDate.ToString("MMM yyyy") %></span></th>
            <% for (int i = 0; i <= currenItem.EndDate.Day - currenItem.StatDate.Day; i++)
               { %>
            <th onclick="Supplier.ManageOfferAvailability.CloseOutDay(this, '<%= currenItem.Month %>','<%= currenItem.StatDate.AddDays(i).Day %>');" class="<%=  ((currenItem.StatDate.AddDays(i).DayOfWeek == DayOfWeek.Saturday || currenItem.StatDate.AddDays(i).DayOfWeek == DayOfWeek.Sunday) ? "headerSortDown" : "" ) %>" roomdaysclosed="<%=currenItem.RoomAvailability.All(x=>x.AvailabilityData.Any(y=>y.AvialabilityDate == currenItem.StatDate.AddDays(i) &&y.IsCloseOut  )) %>">
               <span><%= currenItem.StatDate.AddDays(i).Day.ToString("00") %></span><span> <%= currenItem.StatDate.AddDays(i).DayOfWeek.ToString().Substring(0,2) %> </span>
            </th>
            <%} %>
         </tr>
         <tr class="labelRow">
            <td class="left-detail  transparent"></td>
            <% for (int i = 0; i <= currenItem.EndDate.Day - currenItem.StatDate.Day; i++)
               { %>
            <td style="<%=((currenItem.StatDate.AddDays(i).DayOfWeek == DayOfWeek.Saturday || currenItem.StatDate.AddDays(i).DayOfWeek == DayOfWeek.Sunday) ? "background-color: rgb(214, 216, 217);": "" ) %>"><span>v</span></td>
            <%} %>
         </tr>
         <%foreach (var room in currenItem.RoomAvailability)
            {
                bool addStyle = false;
            %>
         <% if(room.ProductTye==1||room.ProductTye==3){%>
         <tr>
            <td class="left-detail">
               <div class="col-md-12 margin-top-calander-title">
                  <div class="col-md-5 no-padding">
                     <span><%= room.Name %> </span>
                  </div>
                  <%if (room.NumberOfAdult>0)
                     {%>
                  <div class="col-md-4 no-padding">
                     <div class="userbox">
                        <span class="glyphicon glyphicon-user"></span>
                     </div>
                     <span class="topcount"><%=room.NumberOfAdult %></span>
                  </div>
                  <%} else {%>
                  <div class="userbox">
                     <span class="glyphicon glyphicon-user disabled"></span>
                  </div>
                  <% } %>
                  <%if (room.NumberOfChild>0)
                     {%>
                  <div class="col-md-3 no-padding">
                     <div class="userbox">
                        <span class="glyphicon glyphicon-user sm-font"></span>
                     </div>
                     <span class="topcount"><%=room.NumberOfChild %></span>
                  </div>
                  <%} else {%>
                  <div class="userbox">
                     <span class="glyphicon glyphicon-user sm-font disabled"></span>
                  </div>
                  <% } %>
               </div>
               <div class="row">
                  <div class="col-md-12 margin-top-calander">Total Unsold (All Promotions)</div>
               </div>
            </td>
            <% for (int i = 0; i <= currenItem.EndDate.Day - currenItem.StatDate.Day; i++)
               {
                   addStyle = ((currenItem.StatDate.AddDays(i).DayOfWeek == DayOfWeek.Saturday || currenItem.StatDate.AddDays(i).DayOfWeek == DayOfWeek.Sunday));
                   var currentRoomAvailbity = room.AvailabilityData.FirstOrDefault(x => x.AvialabilityDate == currenItem.StatDate.AddDays(i));
                   string defaultclass = string.Format("default{0}", addStyle ? "weekend" : string.Empty);
                     string spanclassApplied = "";
                      string addClass = string.Empty;
                      if (addStyle)
                      {
                          addClass = "weekend";
                      }
                      if (currentRoomAvailbity.IsNonEditable)
                      {
                          spanclassApplied += string.Format(" disablebooking{0}", addClass);
                      }
                      else if (currentRoomAvailbity.IsCloseOut)
                      {
                          spanclassApplied += string.Format(" closeOut{0}", addClass);
                      }
                      else if (currentRoomAvailbity.AvailabelRoom == currentRoomAvailbity.BlockedRoom && currentRoomAvailbity.AvailabelRoom > 0)
                      {
                          spanclassApplied += string.Format(" soldOut{0}", addClass);
                      }
                      else if (currentRoomAvailbity.IsNewRecord || currentRoomAvailbity.AvailabelRoom == 0)
                      {
                          spanclassApplied += string.Format(" unallocate{0}", addClass);
                      }
                      else if (currentRoomAvailbity.AvailabelRoom != currentRoomAvailbity.BlockedRoom)
                      {
                    spanclassApplied += string.Format(" bookable{0}", addClass);
                          
                      }  
                      else
                      {
                          spanclassApplied += string.Format(" default{0}", addClass);
                      }
                      
               %>
            <td class="<%= currentRoomAvailbity.IsNonEditable? "disablebooking" : defaultclass %>">
               <%if (!currentRoomAvailbity.IsNonEditable)
                  { %>
               <span class="blokedspan <%=spanclassApplied%>"><%= currentRoomAvailbity.TotalUnSoldRoom %></span>
               <% } %>
            </td>
            <%} %>
         </tr>
         <tr trroom="<%=room.ProductSku %>">
            <td class="left-detail " roomsrestriction="<%=room.AvailabilityData.All(x=>x.HasOffer) %> ">
               <div class="row">
                  <div class="col-md-12 restriction-detail"><span>This Promotion Restriction</span></div>
               </div>
            </td>
            <% for (int i = 0; i <= currenItem.EndDate.Day - currenItem.StatDate.Day; i++)
               {
                   addStyle = ((currenItem.StatDate.AddDays(i).DayOfWeek == DayOfWeek.Saturday || currenItem.StatDate.AddDays(i).DayOfWeek == DayOfWeek.Sunday));
                   var currentRoomAvailbity = room.AvailabilityData.FirstOrDefault(x => x.AvialabilityDate == currenItem.StatDate.AddDays(i));
                   var roomClosed = currentRoomAvailbity.IsCloseOut;
                   var isOffered = currentRoomAvailbity.HasOffer;
               %>
            <td>
               <%    string classApplied = "";
                  string addClass = string.Empty;
                  if (addStyle)
                  {
                      addClass = "weekend";
                  }
                  if (currentRoomAvailbity.IsNonEditable)
                  {
                      classApplied += string.Format(" disablebooking{0}", addClass);
                  }
                  else if (currentRoomAvailbity.IsCloseOut)
                  {
                      classApplied += string.Format(" closeOut{0}", addClass);
                  }
                  else if (currentRoomAvailbity.OfferAvailabelRoom == currentRoomAvailbity.OfferBlockedRoom && currentRoomAvailbity.OfferAvailabelRoom > 0)
                  {
                      classApplied += string.Format(" soldOut{0}", addClass);
                  }
                  else if (currentRoomAvailbity.IsNewOfferRecord || currentRoomAvailbity.OfferAvailabelRoom == 0)
                  {
                      classApplied += string.Format(" unallocate{0}", addClass);
                  }
                  else if (currentRoomAvailbity.OfferAvailabelRoom != currentRoomAvailbity.OfferBlockedRoom)
                  {
                      classApplied += string.Format(" limits{0}", addClass);
                  }
                  else
                  {
                      classApplied += string.Format(" default{0}", addClass);
                  }
                  
                  %>
               <input maxsellableroom="<%= currentRoomAvailbity.TotalUnSoldRoom + currentRoomAvailbity.OfferBlockedRoom %>"
                  minsellableroom="<%= currentRoomAvailbity.OfferBlockedRoom %>"
                  datecounter="<%= currentRoomAvailbity.DateCounter %>" type="text"
                  <%=currentRoomAvailbity.IsNonEditable || currentRoomAvailbity.AvailabelRoom==0 ? "disabled=\"disabled\"" : "" %>
                  productsku=" <%=currentRoomAvailbity.ProductSku %>"
                  offerid="<%=currentRoomAvailbity.OfferId %>"
                  month="<%=currenItem.Month %>"
                  offerallocatedroom="<%=currentRoomAvailbity.OfferAvailabelRoom %>"
                  onchange="Supplier.ManageOfferAvailability.ChangeAvailability(this,false)" onblur="Supplier.ManageOfferAvailability.ChangeAvailability(this,true)"
                  autocomplete="off" required class="form-control <%=classApplied %>  EnbleText EnbleText_<%= currenItem.Month %>" title=""
                  value="<%=currentRoomAvailbity.IsNonEditable? "" : currentRoomAvailbity.OfferAvailabelRoom.ToString() %>"
                  defaultclass="<%=classApplied %>" weekend="<%=addStyle%>" maxlength="3"
                  hashid="<%= currenItem.Month %>_<%= room.ProductId.Replace(" ","_") %>_<%= currenItem.StatDate.AddDays(i).Day %>" />
                                
            </td>
            <%} %>
         </tr>         
         <tr>
            <td class="left-detail borderBottom">
               <div class="row">
                  <div class="col-md-12 block-detail">Total Sold (This Promotion)</div>
               </div>
            </td>
            <% for (int i = 0; i <= currenItem.EndDate.Day - currenItem.StatDate.Day; i++)
               {
                   addStyle = ((currenItem.StatDate.AddDays(i).DayOfWeek == DayOfWeek.Saturday || currenItem.StatDate.AddDays(i).DayOfWeek == DayOfWeek.Sunday));
                   var currentRoomAvailbity = room.AvailabilityData.FirstOrDefault(x => x.AvialabilityDate == currenItem.StatDate.AddDays(i));
                   string defaultclass = string.Format("default{0}", addStyle ? "weekend" : string.Empty);
               %>
            <td class="<%= currentRoomAvailbity.IsNonEditable? "disablebooking" : defaultclass %>">
               <%if (!currentRoomAvailbity.IsNonEditable)
                  { %>
               <span class="blokedspan <%=defaultclass%>"><%= currentRoomAvailbity.OfferBlockedRoom %></span>
               <% } %>
            </td>
            <%} %>
         </tr>
         <%} %>
         <%} %>
      </table>
   </div>
</div>