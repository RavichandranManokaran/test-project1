﻿CREATE TABLE [dbo].[uCommerce_CategoryDescription] (
    [CategoryDescriptionId] INT            IDENTITY (1, 1) NOT NULL,
    [CategoryId]            INT            NOT NULL,
    [DisplayName]           NVARCHAR (512) NOT NULL,
    [Description]           NVARCHAR (MAX) NULL,
    [CultureCode]           NVARCHAR (60)  NOT NULL,
    [ContentId]             INT            NULL,
    [RenderAsContent]       BIT            CONSTRAINT [uCommerce_DF_CategoryDescription_RenderAsContent] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [uCommerce_PK_CategoryDescription] PRIMARY KEY CLUSTERED ([CategoryDescriptionId] ASC),
    CONSTRAINT [uCommerce_FK_CategoryDescription_Category] FOREIGN KEY ([CategoryId]) REFERENCES [dbo].[uCommerce_Category] ([CategoryId]),
    CONSTRAINT [Unique_CategoryDescription_CultureCode_CategoryId] UNIQUE NONCLUSTERED ([CultureCode] ASC, [CategoryId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_uCommerce_CategoryDescription_CategoryId]
    ON [dbo].[uCommerce_CategoryDescription]([CategoryId] ASC)
    INCLUDE([CategoryDescriptionId], [DisplayName], [Description], [CultureCode]);

