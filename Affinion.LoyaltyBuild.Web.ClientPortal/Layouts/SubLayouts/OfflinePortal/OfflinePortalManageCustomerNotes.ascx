﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalManageCustomerNotes.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalManageCustomerNotes" %>


<%if(ValidationResponse != null){%> 
<div class="col-sm-12 alert alert-<%= ValidationResponse.IsSuccess ? "success" : "danger" %>"> 
    <%= ValidationResponse.Message %> 
</div> 
<%} %>

<div id="divMsg" runat="server">
</div>


<div class="col-sm-12">
    <div class="accordian-outer">
        <div class="panel-group" id="accordion">
            <div class="panel panel-default">
                <div class="panel-heading bg-primary">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse0" class="show-hide-list" aria-expanded="true"><sc:text id="titleTxt" runat="server"/></a>
                    </h4>
                </div>
                <div id="collapse0" class="panel-collapse collapse in" aria-expanded="true">
                    <div class="panel-body alert-info">
                        <div class="row">
                            <div class="col-sm-12">

                                <div class="col-sm-6">
                                    <p><sc:text id="IntrnlNoteTxt" runat="server"/></p>
                                    <textarea id="txtInternalNote" runat="server" rows="5" class="form-control font-size-12" readonly=""></textarea>
                                </div>
                                <div class="col-sm-6">
                                    <p><sc:text id="AddIntNotetxt" runat="server"/></p>
                                    <textarea id="txtAddiInternalNote" runat="server" rows="5" class="form-control font-size-12"></textarea>
                                    <asp:CustomValidator id="internalNote" ControlToValidate="txtAddiInternalNote" ClientValidationFunction="ClientValidate" Display="Dynamic" ErrorMessage="*Dont Enter Card Number!" ForeColor="Red" Font-Name="verdana"  Font-Size="10pt" runat="server"/>
                                    <Button ID="btnInternalNote" runat="server" onserverclick="AddiInternalNote_Click" class="btn btn-default add-break pull-right margin-row"><sc:text id="btnIntlNoteTxt" runat="server"/></Button>
                                </div>
                                <div class="col-sm-6">
                                    <p><sc:text id="ntpTxt" runat="server"/></p>
                                    <textarea id="txtNotetoProvider" runat="server" rows="5" class="form-control font-size-12"></textarea>
                                     <asp:CustomValidator id="noteToProvider" ControlToValidate="txtNotetoProvider" ClientValidationFunction="ClientValidate" Display="Dynamic" ErrorMessage="*Dont Enter Card Number!" ForeColor="Red" Font-Name="verdana"  Font-Size="10pt" runat="server">
                                       </asp:CustomValidator>
                                    <Button ID="btnNotetoProvider" onserverclick="NotetoProvider_Click" runat="server" class="btn btn-default add-break pull-right margin-row"><sc:text id="btnProNotTxt" runat="server"/></Button>
                                </div>
                                <div class="col-sm-6">
                                    <p><sc:text id="srTxt" runat="server"/></p>
                                    <textarea id="txtSpecialRequests" runat="server" rows="5" class="form-control font-size-12"></textarea>
                                     <asp:CustomValidator id="specialRequest" ControlToValidate="txtSpecialRequests" ClientValidationFunction="ClientValidate" Display="Dynamic" ErrorMessage="*Dont Enter Card Number!"  ForeColor="Red" Font-Name="verdana"  Font-Size="10pt" runat="server">
                                        </asp:CustomValidator>
                                    <Button ID="btnSpecialRequests" runat="server" onserverclick="SpecialRequests_Click" class="btn btn-default add-break pull-right margin-row"><sc:text id="btnSrTxt" runat="server"/></Button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




