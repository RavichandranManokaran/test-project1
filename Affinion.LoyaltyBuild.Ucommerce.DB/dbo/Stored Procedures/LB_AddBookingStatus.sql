﻿CREATE PROCEDURE [dbo].[LB_AddBookingStatus] 
@OrderLineId INT, 
@Status      INT = NULL, 
@CreatedBy   VARCHAR(50) 
AS 
BEGIN 

DECLARE @CurrentStatus INT=0
SELECT @CurrentStatus = StatusId FROM LB_BookingInfo WHERE OrderLineId = @OrderLineId

    IF (@Status IS NULL) 
	BEGIN 
		IF EXISTS (SELECT 1 FROM LB_BookingInfo WHERE OrderLineId = @OrderLineId AND StatusId NOT IN (7,8))
		BEGIN
			--Confirmed 
			SET @Status = 6 

			DECLARE @LatestOrderLineId INT 

			SELECT @LatestOrderLineId = Max(orderlineid) 
			FROM   [dbo].[Lb_fnrelatedorderlines](@OrderLineId) 

			--Set Provisional if not cancelled or transfered
			IF (@OrderLineId = @LatestOrderLineId) 
				AND EXISTS 
				(SELECT Sum([total]) [Total], [currency] FROM (
					SELECT Sum(OLD.totalamount) [Total], OLD.currencytypeid   [Currency] 
					FROM ucommerce_orderline OL INNER JOIN lb_orderlinedue OLD ON OLD.orderlineid = OL.orderlineid 
					WHERE ISNULL(OLD.isdeleted, 0) = 0 AND OLD.orderlineid IN (SELECT orderlineid FROM [dbo].[Lb_fnrelatedorderlines](@OrderLineId)) 
					GROUP  BY OLD.currencytypeid 
					UNION ALL 
					SELECT -Sum(OLP.totalamount) [Total], OLP.currencytypeid [Currency] 
					FROM   ucommerce_orderline OL INNER JOIN lb_orderlinepayment OLP ON OLP.orderlineid = OL.orderlineid 
					WHERE  OLP.orderlineid IN (SELECT orderlineid FROM [dbo].[Lb_fnrelatedorderlines](@OrderLineId)) 
					GROUP  BY OLP.currencytypeid) P 
				GROUP  BY [currency] 
				HAVING Sum([total]) > 0) 
			BEGIN 
				SET @Status = 5 
			END 
		END 
	END

	IF @Status IS NOT NULL AND @CurrentStatus <> @Status
	BEGIN
		--add status log 
		INSERT INTO [dbo].[lb_orderlinestatusaudit] 
					([orderlineid], 
					[statusid], 
					[createdby], 
					[createdon]) 
		VALUES      (@OrderLineId, 
					@Status, 
					@CreatedBy, 
					Getdate()) 

		--update current status 
		UPDATE lb_bookinginfo 
		SET    statusid = @Status 
		WHERE  orderlineid = @OrderLineId 
	END
END 

