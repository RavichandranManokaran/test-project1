﻿using Affinion.LoyaltyBuild.Model.Product;
using Affinion.LoyaltyBuild.Model.Provider;
using Affinion.LoyaltyBuild.Web.ClientPortal.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    public partial class OfflinePortalPromotions : System.Web.UI.UserControl
    {
        public List<PromotionAvailabilityByDate> PromotionAvailability { get; set; }
        public List<PromotionInfo> Promotions { get; set; }
        public DateTime CheckinDate { get; set; }
        private List<DateTime> _dates = new List<DateTime>();
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            _dates.AddRange(Enumerable.Range(-7, 15)
                        .Select(i => CheckinDate.AddDays(i)));
            //set datasource
            rptPromotions.DataSource = this.Promotions;
            rptPromotions.DataBind();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected List<DateTime> GetDates()
        {
            return _dates;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PromotionsItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var rptOfferRooms = (Repeater)e.Item.FindControl("rptOfferRooms");
            rptOfferRooms.DataSource = ((PromotionInfo)e.Item.DataItem).VariantSkus.Where(i => PromotionAvailability.Any(j => j.VariantSku == i.VariantSku));
            rptOfferRooms.DataBind();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="product"></param>
        /// <param name="campaignItemId"></param>
        /// <returns></returns>
        protected IEnumerable<PromotionAvailabilityByDate> GetOfferAvailability(PromotionProduct product, int campaignItemId)
        {
            foreach (var date in _dates)
            {
                if (PromotionAvailability.Any(i => i.VariantSku == product.VariantSku && i.Date == date && i.PromotionId==campaignItemId))
                    yield return PromotionAvailability.FirstOrDefault(i => i.VariantSku == product.VariantSku && i.Date == date && i.PromotionId == campaignItemId);
                else
                    yield return new PromotionAvailabilityByDate();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="availabilityInfo"></param>
        /// <returns></returns>
        protected string GetDateClass(PriceAvailabilityByDate availabilityInfo)
        {
            if (availabilityInfo.Date == CheckinDate)
                return availabilityInfo.Availability > 0 ? "body-text accent54-bg accent36-f" : "availability-zero accent36-f";

            if (availabilityInfo.Date < CheckinDate || availabilityInfo.Availability <= 0)
                return "disable-box";

            return "body-text";
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Sender"></param>
        /// <param name="e"></param>
        protected void PriceItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item ||
                e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var val = (PriceAvailabilityByDate)e.Item.DataItem;
                var spnPastItem = (HtmlGenericControl)e.Item.FindControl("spnPastItem");
                var spnFutureItem = (HtmlGenericControl)e.Item.FindControl("spnFutureItem");
                var spnNoItem = (HtmlGenericControl)e.Item.FindControl("spnNoItem");

                spnNoItem.Visible = (val.Availability <= 0);
                spnPastItem.Visible = (val.Availability > 0 && val.Date < CheckinDate);
                spnFutureItem.Visible = (val.Availability > 0 && val.Date >= CheckinDate);

            }
        }

    }
}