﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.SupplierPortal.DataAccess
{
    static class ExtensionHelper
    {
        public static bool IsGuid(this string str)
        {
            return !string.IsNullOrEmpty(str) &&
                (new Regex(@"^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$", RegexOptions.Compiled)).IsMatch(str);
        }
    }
}
