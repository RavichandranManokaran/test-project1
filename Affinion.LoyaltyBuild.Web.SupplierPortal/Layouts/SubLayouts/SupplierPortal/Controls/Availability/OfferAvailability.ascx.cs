﻿using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Availability
{
    public partial class OfferAvailability : System.Web.UI.UserControl
    {
        public string Disable;
        protected void Page_Load(object sender, EventArgs e)
        {
            //checkRoles();
            Disable = Request.QueryString["disabled"];
        }

        //public void checkRoles()
        //{
        //    string currentUserRole = SitecoreUserProfile.GetItem(UserProfileKeys.currentLoggedinUserRole);
        //    if (currentUserRole.ToLower().Contains(Constants.accountManagement) || currentUserRole.ToLower().Contains(Constants.marketing) || currentUserRole.ToLower().Contains(Constants.IT) || currentUserRole.ToLower().Contains(Constants.superUser))
        //    {
        //        Disable = "";
        //    }
        //    else if (currentUserRole.ToLower().Contains(Constants.grpHotelRole) || currentUserRole.ToLower().Contains(Constants.hotelRole) || currentUserRole.ToLower().Contains(Constants.hotelSupervisor) || currentUserRole.ToLower().Contains(Constants.lbAdminRole))
        //    {
        //        Disable = "";
        //    }
        //    else if (currentUserRole.ToLower().Contains(Constants.finance))
        //    {
        //        Disable = "disabled";
        //    }
        //}
    }
}