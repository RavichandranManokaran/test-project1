﻿--exec GetRoomAvaialibility 'SINBI01,SINBI01,SINBI01,SINBI01,SINBI01,SINBI01,SINBI01,SINBI01,SINBI01,SINBI01,SINBI01,SINBI01',9670,9672
CREATE procedure GetRoomAvaialibility
@VariantSKU varchar(max),
@sdate int,
@edate int
As
select  (lba.NumberAllocated - lba.NumberBooked) as AvaialbileRoom, Sku, VariantSku , DateCounter, Price, Priceband , lba.IsCloseOut as IsCloseOut from LB_RoomAvailability lba
join uCommerce_ParseArrayToTable (@VariantSKU,',',0) tmp on lba.VariantSku = tmp.stringvalue
where DateCounter>= @sdate and datecounter <= @edate
