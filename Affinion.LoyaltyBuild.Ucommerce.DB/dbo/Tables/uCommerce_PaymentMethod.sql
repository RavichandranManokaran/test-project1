﻿CREATE TABLE [dbo].[uCommerce_PaymentMethod] (
    [PaymentMethodId]          INT             IDENTITY (1, 1) NOT NULL,
    [Name]                     NVARCHAR (128)  NOT NULL,
    [FeePercent]               DECIMAL (18, 4) CONSTRAINT [uCommerce_DF_Table_1_FeePercant] DEFAULT ((0)) NOT NULL,
    [ImageMediaId]             NVARCHAR (255)  NULL,
    [PaymentMethodServiceName] NVARCHAR (512)  NULL,
    [Enabled]                  BIT             CONSTRAINT [uCommerce_DF_PaymentMethod_Enabled] DEFAULT ((1)) NOT NULL,
    [Deleted]                  BIT             CONSTRAINT [uCommerce_DF_PaymentMethod_Deleted] DEFAULT ((0)) NOT NULL,
    [ModifiedOn]               DATETIME        NOT NULL,
    [ModifiedBy]               NVARCHAR (50)   NULL,
    [Pipeline]                 NVARCHAR (128)  NULL,
    [DefinitionId]             INT             NULL,
    CONSTRAINT [uCommerce_PK_PaymentMethod] PRIMARY KEY CLUSTERED ([PaymentMethodId] ASC),
    FOREIGN KEY ([DefinitionId]) REFERENCES [dbo].[uCommerce_Definition] ([DefinitionId])
);

