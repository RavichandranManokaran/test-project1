﻿CREATE TABLE [dbo].[LB_SupplierPaymentRequests](
	[ProviderPaymentRequestId] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [int] NOT NULL,
	[OrderLineId] [int] NOT NULL,
	[SuggestedProviderAmount] [decimal](18, 0) NOT NULL,
	[IsProviderPaymentRequested] [tinyint] NULL,
	[DateProviderPaymentRequested] [datetime] NULL,
 CONSTRAINT [uCommerce_PK_ProviderPaymentRequestId] PRIMARY KEY CLUSTERED 
(
	[ProviderPaymentRequestId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[LB_SupplierPaymentRequests] ADD  CONSTRAINT [DF_ProviderPaymentRequest_IsProviderPaymentRequested]  DEFAULT ((0)) FOR [IsProviderPaymentRequested]
GO


