﻿
CREATE PROCEDURE [dbo].[affinion_GetDuesByOrderLineID]
	@OrderLineId INT,
	@CurrencyTypeId INT=null
AS
BEGIN
	SELECT SUM(OLD.Amount) - Sum(OLP.Amount) [Total],
		   ISNULL(@CurrencyTypeId,OLP.CurrencyTypeId) [Currency],
		   max(OLD.OrderLineID) [OrderLineId]		   
	FROM uCommerce_OrderLine  OL
		INNER JOIN uCommerce_OrderLineDue OLD ON OLD.OrderLineID = OL.OrderLineId 
		INNER JOIN uCommerce_OrderLinePayment OLP ON OLD.CurrencyTypeId = OLP.CurrencyTypeId
	WHERE
		ISNULL(OLD.IsDeleted,0) = 0 
		AND (ISNULL(@CurrencyTypeId,OLP.CurrencyTypeId) = OLD.CurrencyTypeId)
		AND OLD.OrderLineID IN (SELECT OrderLineID FROM [dbo].[affinion_RelatedOrderLines](@OrderLineId))
	GROUP BY 
	 OLP.CurrencyTypeId
	HAVING
		(SUM(OLD.Amount) - Sum(OLP.Amount)) <> 0
END

GO
