﻿create PROCEDURE [dbo].[LB_GetPreviousDayBooking]
as
begin
	select ca.clientid as [Partner Name],count(ol.OrderLineId)as 'Booking(s)' from LB_CustomerAttribute as ca
	inner join
	uCommerce_Customer as c on ca.CustomerId=c.CustomerId
	inner join 
	uCommerce_PurchaseOrder as po on c.CustomerId=po.CustomerId
	inner join
	uCommerce_OrderLine as ol on po.OrderId=ol.OrderId
	where 
	CAST(ol.CreatedOn AS DATE)=CAST( DATEADD(DAY, -1, GETDATE()) as date) 
	group by ca.clientid
end