﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LocationSelectorSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal.LocationSelectorSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<div class="detail-info">
    <div class="detail-info-header map-location-left">
        <div class="map-location-left-header">
            <div class="col-xs-10">
                <h2 class="no-margin">Select Available Locations</h2>
            </div>
            <div class="col-xs-2">
                <span class="fa fa-map-marker"></span>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="map-location-left-body">
            <p>Accommodation providers are subject to availability based on the dates you select. If you can't find what you are looking for, select another date and search again.</p>
        </div>
    </div>
    <div class="col-xs-12 select-location">
        <div role="group" aria-label="..." class="select-loc">
            <asp:DropDownList ID="AvailableLocationDropDown" runat="server" CssClass="form-control"></asp:DropDownList>
        </div>
        <div class="need-booking-top no-margin">
            <h2>Need Help Booking?</h2>
            <div class="need-booking">
                <%--<p><span>
                    <img src="images/Telephone-Ringing.png" alt="" />
                   </span>
                    <strong>0818 220 088</strong>
                </p>--%>
                <sc:Placeholder ID="TobbarRichText" Key="TobbarRichText" runat="server" />
                <%--<div class="tp-number float-right hidden-xs">
                    <sc:Placeholder ID="TobbarRichText" Key="TobbarRichText" runat="server" />
                </div>--%>
            </div>
        </div>
    </div>
</div>
