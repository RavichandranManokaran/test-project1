﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
/// </summary>
namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Email
{
    public interface IMailService
    {
        /// <summary>
        /// Generate email and send
        /// </summary>
        /// <param name="mailType"> Type of email </param>
        /// <param name="orderlineid"> Orderlineid </param>
        /// <param name="additionalInfo"> additionalInformation </param>
        /// <returns></returns>
        void GenerateEmail(MailTypes mailType, int orderlineid, NameValueCollection additionalInfo = null);
    }
}
