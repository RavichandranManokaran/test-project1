﻿    ///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           Constants.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common.Constants
/// Description:           Holds constants used throughout the project

using System;
namespace Affinion.LoyaltyBuild.Common
{
    /// <summary>
    /// Holds constants
    /// </summary>
    public static class Constants
    {
        #region Cache Keys

        public const string TripAdvisorKeyCacheKey = "TripAdvisorKey-6712ef4e-db8c-49f6-80e3-25df5c7be087";

        #endregion

        #region Web.Config Keys

        public const string TripAdvisorKeyConfigKey = "TripAdvisorKey";
        public static string CurrentSitecoreDatabase = Sitecore.Configuration.Settings.GetSetting("CurrentSitecoreDatabase");//"web";

        #endregion

        #region special charactor validation

        /// <summary>
        /// Regular expression for special charactor validation
        /// </summary>
        public const string SpecialCharactorValidationRegex = @"^[a-zA-Z0-9\s\-\+\(\)\&\w]*$";

        #endregion

        #region Custom Launchpad Button Paths

        /// <summary>
        /// Supplier Setup Launchpad Button Path
        /// </summary>
        public const string SupplierSetupButtonPath = "/sitecore/client/Applications/Launchpad/PageSettings/Buttons/LBSection/Supplier Setup";

        /// <summary>
        /// Client Setup Launchpad Button Path
        /// </summary>
        public const string ClientSetupButtonPath = "/sitecore/client/Applications/Launchpad/PageSettings/Buttons/LBSection/Client Setup";

        /// <summary>
        /// Launchpad Path
        /// </summary>
        public const string LaunchpadPath = "/sitecore/shell/sitecore/client/Applications/Launchpad/";


        #endregion

        #region Template Paths

        /// <summary>
        /// Subscribe Template Path
        /// </summary>
        public const string SubscribeTemplatePath = "/sitecore/templates/Affinion/ClientPortal/Subscribe";
        /// <summary>
        /// Subscribe Template Path-paymentRule
        /// </summary>

        public const string PaymentMethodPath= "/sitecore/content/admin-portal/global/_supporting-content/payment-method/*";

        /// <summary>
        ///payment rule template item id
        /// </summary>
        public const string paymentRuleTemplate = "{DF848F88-81B3-4646-B931-5404C25BACAA}";

        /// <summary>
        /// client details template fast query
        /// </summary>
        public const string clientDetailsTemplate = "/sitecore/content/#admin-portal#/#client-setup#//*[@@templateid='{4CFC54E0-069A-4344-85E3-2BD99DA576E6}']";
       /// <summary>
        /// partial payment details template
        /// </summary>
        public const string partialPaymentDetails = "{59B5739C-623B-46AE-9266-37BAEE2D1136}";

        /// <summary>
        /// Active Client Items
        /// </summary>
        public static readonly string  FilteredClientItems = @"fast:/sitecore/content/#admin-portal#/#client-setup#/*[@@templatename='ClientDetails' and @Active='1']";

        /// <summary>
        /// Call Center User Template
        /// </summary>
        public static readonly string CallCenterUserTemplate = @"fast:/sitecore/content/#admin-portal#/#client-setup#/#global#/#_supporting-content#/#callcentreuser#/*[@@templatename='CallCentreUser']";

        #endregion

        #region Database name

        /// <summary>
        /// Master database name
        /// </summary>
        public const string MasterDatabase = "master";

        /// <summary>
        /// Core database name
        /// </summary>
        public const string CoreDatabase = "core";

        #endregion

        #region Custom Text

        /// <summary>
        /// Please Select text
        /// </summary>
        public const string PleaseSelectText = "Please Select";

        /// <summary>
        /// Slash text
        /// </summary>
        public const string SlashText = "/";

        /// <summary>
        /// Message
        /// </summary>
        public const string Message = "Message";

        /// <summary>
        /// Subscription Folder
        /// </summary>
        public const string FolderName = "/subscription";

        /// <summary>
        /// Improve service subscription folder
        /// </summary>
        public const string ServiceImprovementMailFolder = "/emaillists/improveservice";

        /// <summary>
        /// Promotional mail subscription folder
        /// </summary>
        public const string PromotionalMailfolder = "/emaillists/promotional";

        /// <summary>
        /// EmailID
        /// </summary>
        public const string EmailID = "Email";

        /// <summary>
        /// Confirmation Message
        /// </summary>
        public const string ConfirmationMessage = "ConfirmationMessage";

        /// <summary>
        /// Error Message
        /// </summary>
        public const string ErrorMesage = "Item not exist";

        /// <summary>
        /// Email Exist Message
        /// </summary>
        public const string EmailExist = "Email already exist";

        /// <summary>
        /// Email Not Exist Message
        /// </summary>
        public const string EmailNotFound = "Email Not Found";

        /// <summary>
        /// Supplier type text
        /// </summary>
        public const string SupplierTypeText = "SupplierType";

        /// <summary>
        /// Subscription for Promotional Mail
        /// </summary>
        public const string PromotionalMail = "PromotionalMail";

        /// <summary>
        /// Subscription for ServiceImprovement Mail
        /// </summary>
        public const string ServiceImprovement = "ServiceImprovement";

        /// <summary>
        /// Subscription for both Promotional And ServiceImprovement
        /// </summary>
        public const string ServiceAndPromotional="Both";

        

        #endregion

        #region Custom Paths

        /// <summary>
        /// Sitecore Logon path
        /// </summary>
        public const string SitecoreLogOnPath = "~/sitecore/login";

        #endregion

        #region Custom Query Paths

        /// <summary>
        /// Path to query suppliers for basket page
        /// </summary>
        public const string BasketPageQueryPath = "/sitecore/content/#admin-portal#/#supplier-setup#//*[@@TemplateName='SupplierDetails' and @IsActive='1']"; // "/sitecore/content/#admin-portal#/#supplier-setup#/hotels/*[@@templatename='SupplierDetails' ]";

        public const string GetHotelByUComId = "/sitecore/content/#admin-portal#/#supplier-setup#//*[@@TemplateName='SupplierDetails' and @IsActive='1' and @UCommerceCategoryID='{0}']";

        public const string GetAllclients = "/sitecore/content/#admin-portal#/#client-setup#//*[@@TemplateName='ClientDetails']";
        
        #endregion

        #region Other
        public const string Ireland = "IRELAND";
        public const string ConditionalMessageItemId = "ConditionalMessageItemId";
        public const string ExceptionFlow = "ExceptionFlow";
        public const string ExceptionMessage = "ExceptionMessage";
        #endregion

        #region Sitecore Fields
        public const string ChildAgeRequiredErrorMessageFieldName = "ChildAgeRequiredErrorMessage";

        public const string CountryFieldName = "Country";

        static public readonly string ClientDetailsFieldName = "ClientDetails";

        public const string IsATGFieldName = "IsATG";

        #endregion

        #region Sitecore Item Paths

        public const string TitleListPath = "/sitecore/content/offline-portal/global/_supporting-content/customer/title";
        public const string SupplierSetupPath = "/sitecore/content/admin-portal/supplier-setup";
        public const string SupplierLocations = "/sitecore/content/admin-portal/client-setup/";
        public const string StarRankingsPath = "/sitecore/content/admin-portal/supplier-setup/global/_supporting-content/star-rankings";
        public const string ThemesPath = "/sitecore/content/admin-portal/supplier-setup/global/_supporting-content/themes";
        public const string FacilitiesPath = "/sitecore/content/admin-portal/supplier-setup/global/_supporting-content/facilities";
        public const string ExperiencesPath = "/sitecore/content/admin-portal/supplier-setup/global/_supporting-content/experiences";
        public const string LocalRankingPath = "/sitecore/content/admin-portal/supplier-setup/global/_supporting-content/local-rankings";
        public const string IconPath = "/sitecore/content/admin-portal/global/_supporting-content/icons/";
        public const string MapLocation = "/sitecore/content/admin-portal/global/_supporting-content/map-locations";
        public const string Countries = "/sitecore/content/admin-portal/global/_supporting-content/countries";
        public const string Location = "/sitecore/content/admin-portal/global/_supporting-content/locations";
        public const string Pricaband = "/sitecore/content/admin-portal/supplier-setup/global/_supporting-content/pricebands";
       

        /// <summary>
        /// Path of supplier offers
        /// </summary>
        static public readonly string SupplierOfferPath = "/sitecore/content/admin-portal/supplier-setup/global/_supporting-content/supplier-offers";

        /// <summary>
        ///path of copyright text 
        /// </summary>
        static public readonly string copyrightpath = "/global/_supporting-content/footercontents/copyrighttexts/copyright1";

        /// <summary>
        /// Item Id of Client Portal Root Item
        /// </summary>
        static public readonly string ClientPortalRootTemplateIdString = "{42B2F6EA-4B65-4869-9DFF-BFAD5B8865E3}";

        /// <summary>
        /// Template Id of Top Search
        /// </summary>
        static public readonly string TopSearchTemplateIdString = "{BAFAAE66-9EE6-4584-A04C-22D737524CC3}";

        /// <summary>
        /// Top Search Field ID
        /// </summary>
        static public readonly string TopSearchFieldIdString = "{44795F23-DA47-4726-BA0F-6F82178D8AB7}";

        /// <summary>
        /// Hotel Information Template Id
        /// </summary>
        static public readonly string HotelInformationTemplateIdString = "{0C5B7C6D-C546-40C8-B508-0441E8BE0509}";
        #endregion

        #region Dictionary Keys

        public const string ZeroToTwelveMonthsDictionaryKey = "ZeroToTwelveMonths";

        public const string DefaultDropDownTextDictionaryKey = "DefaultDropDownText";
        #endregion

        #region Offline Portal
        public const string OfflinePortalSettings = @"/sitecore/content/offline-portal/home/settings";
        public const string OfflinePortaltimeSpanToExpirePassword = "OfflinePortaltimeSpanToExpirePassword";
        public const string OfflinePortalRoleCallCenterSupervisor = "sitecore\\Outsource Call Centre Supervisor";
        public const string OfflinePortalRoleIrishCallCenterSupervisor = "sitecore\\Irish Call Centre Supervisor";
        public const string OfflinePortalRoleSwedishCallCenterSupervisor = "sitecore\\Swedish Call Centre Supervisor";

        #endregion

        #region Stored Procedure
        public const string GetPurchaseOrderDetails = "LB_GetPurchaseOrderDetails";
        static public readonly string UpdateCustomOrderDetails = "LB_UpdateCustomOrderDetails";
        static public readonly string AddGuestDetails = "LB_AddGuestDetails";
        static public readonly string AddGuestChildAgeDetails = "LB_AddGuestChildAgeDetails";
        public const string AddPerssonalDetails = "LB_AddPersonalDetails";
        public const string GetUCommerceCategoryIDForPurchaseOrder = "uCommerce_GetUCommerceCategoryIDForPurchaseOrder";
        public const string GetRoomReservationByOrderLineId = "LB_GetRoomReservationByOrderLineId";
        public const string GetBookingConfirmationDetails = "LB_GetBookingConfirmationDetails";
        public const string GetCustomerDetailes = "LB_GetCustomerDetails";
        static public readonly string GetCustomerList = "LB_GetCustomerList";
        public const string AddCustomer = "LB_AddCustomer";
        public const string UpdateCustomer = "LB_UpdateCustomer";
        public const string GetPostBreakEmailDetails = "LB_GetPostBreakEmailDetails";
        static public readonly string GetAccomodationDetailsByOrderLineId = "LB_GetAccomodationDetailsByOrderLineId";
        public const string GetReciverMail = "LB_GetReceiverMail";
        public const string NewList = "LB_AddToFavoriteList";
        static public readonly string AddRoomReservation = "LB_AddRoomReservation";
        public const string GetRoomReservationAgeInfo = "LB_GetRoomReservationAgeInfo";
        public static string GetFavoriteHotelList = "LB_GetFavoriteHotelList";
        // Use to fetch accodomation child age related information
        static public readonly string GetChildAgeDetailsByOrderLineID = "LB_GetChildAgeDetailsByOrderLineID";
        //static public readonly string GetCustomerForClient = "GetCustomerForClient";

        public const string ManageBooking = "ManageBooking";



        static public readonly string GetCustomerForClient = "GetCustomerForClient";
        static public readonly string GetActiveCustomerIdForClient = "LB_GetActiveCustomerIdForClient";
        public const string GetCustomerBookingReferenceDetails = "LB_GetCustomerBookingReferenceDetails";
        public const string SetBookingReferenceNumber = "LB_SetBookingReferenceNumber";
        public const string UpdateCustomersBookingReferenceSequence = "LB_UpdateCustomersBookingReferenceSequence";
        public const string setRecentlySeen = "LB_RecentlySeen";
        public const string getRecentlySeen = "LB_GetRecentlySeen";
        public const string spSaveForLater = "LB_SaveForLater";
        public const string spShowSavedHotel = "LB_ShowSavedHotel";
        public const string UpdateCustomerInOrder = "LB_UpdateCustomerInOrder";
        public const string UpdateCheckoutAmount = "UpdateOrderCheckoutAmount";
        public const string UpdateOrderLinePayabaleAtAccomodation = "UpdateOrderLinePayabaleAtAccomodation";
        public const string GetNoOfNightsByOrderId = "LB_GetNoOfNights";
        public const string GetExistingBookings = "LB_GetExistingBookings";
        public const string GetBookingStatusList = "LB_GetBookingStatusList";
        public const string SearchBooking = "LB_SearchBooking";
        public const string PurgeExistingBookings = "PurgeExistingBookings";
        public const string RemoveFavouriteHotel = "LB_RemoveHotelFromFavourites";
        public const string GetAgentPerformanceDetails = "LB_GetAgentPerformance";
        public const string AddBookinginfo = "LB_AddBookingInfo";

        #endregion

        #region Affinion Membership provider
        public const string LastHistoryPasswordCountSetting = "LastHistoryPasswordCount";
        #endregion

        #region Regex Validations
        public const string PhoneNumberValidationRegex = @"^[0-9]{5,15}$"; //@"^([0-9\+]{10,15})+$";
        public const string EmailValidationRegex = @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
        #endregion

        #region Payment
        public const string PaymentModeCreditCard = "CreditCard";
        public const string PaymentModeCheque = "Cheque";
        public const string PaymentModeCash = "Cash";
        public const string PaymentModePostOffice = "PostOffice";
        public const string PaymentModeVoucher = "Voucher";
        public const string payPortionSessionKey = "payportion";
        public const string OfflinePortalPaymentType = "RealexOffline";
        public const string PaymentPortionType = "paymentportion";
        public const string PaymentPortionDeposit = "deposit";
        public const string PaymentPortionFull = "full";
        public const string ProceedToPaymentQueryString = "proceedtopayment";
        public const string OrderCheckoutAmountColumn = "CheckoutAmount";
        public const string PayableAtAccomodation = "PayableAtAccomodation";
        public const string OrderLineDeposit = "Deposit";
        public const string HardCodedClientId = "3D8A574F-933A-4AB8-BF61-370283B38AA2";
        public const string CustomerValidationItemId = "{5459E01C-6461-4088-851A-990BC24BCC82}";
        #endregion Payment

        #region Translations
        public const string InvalidSearchCritiriaErrorMessageKey = "InvalidSearchCritiriaErrorMessage";

        public const string PleaseSelectDictionaryKey = "PleaseSelect";
        #endregion

        #region Hardcoded Values Basket
        public const string HardCodedClientList = "loyaltybuild";
        public const string HarcodedMode = "Hotels";
        #endregion
        #region URL parameters
        public const string LocationUrlParameter = "Location";
        #endregion

        #region Subrate Item Codes
        public static readonly string CancellationFee = "CANFEE";
        public static readonly string CreditCardProcessingFee = "CCPROFEE";
        public static readonly string LBCommission = "LBCOM";
        public static readonly string TransferFee = "TRAFEE";
        #endregion

        public static class Client
        {
            public const string loyaltybuild = "loyaltybuild";
            public const string SuperValu = "SuperValu";
        }

        public static class PortalType
        {
            public const string Online = "Online";
            public const string Offline = "Offline";
        }

        public static class PaymentMethod
        {
            public const string FullPayment = "Full";
            public const string PartialPayment = "Partial";
            public const string DepositOnly = "Deposit";
        }

        public static class PaymentType
        {
            public const string CreditCard = "CreditCard";
            public const string Cheque = "Cheque";
            public const string Cash = "Cash";
            public const string PostOffice = "PostOffice";
            public const string Voucher = "Voucher";
        }

        public static class ProviderType
        {
            public const string HoldayHomes = "Holiday Homes";
            public const string Hotel = "Hotels";
            public const string BedBanks = "Bed Banks";
        }

        #region LWP-873
        public const string ClientConfig = "/sitecore/content/admin-portal/home/report-configuration/dailyoraclefinancereportconfig";
        public const string ProviderPath = "/sitecore/content/#admin-portal#/#supplier-setup#//*[@@templateid='{A7235C94-2EF5-4A3B-87DB-0D4DCF10B4DB}']";
        public const string UCommerceCategoryID = "UCommerceCategoryID";
        #endregion

        public static class SupplierPortal
        {
            public const string hotelRole = @"suppliers\hotel admin";
            public const string marketing = @"suppliers\marketing";
            public const string grpHotelRole = @"suppliers\hotel group";
            public const string lbAdminRole = @"suppliers\supply team";
            public const string superUser = @"suppliers\super user";
            public const string accountManagement = @"suppliers\account management";
            public const string finance = @"suppliers\finance";
            public const string IT = @"suppliers\it";
            public const string hotelSupervisor = @"suppliers\hotel supervisor";
            public const string DateMonthFormat = "dd/MM/yyyy";
            public const string suppliers = "suppliers";
            public static TimeSpan timeSpanToUnlockUserAccount = TimeSpan.FromMinutes(10);
            public static TimeSpan timeSpanToExpirePassword = TimeSpan.FromDays(90);

            public const string hotelPath = "/sitecore/content/#admin-portal#/#supplier-setup#//*[@@templatename='SupplierDetails']";
            public const string GroupHotelPath = "/sitecore/content/#admin-portal#/#supplier-setup#/#global#/#_supporting-content#/#groups#//*[@@templateid='{26507FB6-AFD2-4DCC-B56F-AD1BB5D60CFA}']";
            public const string supplireTypePath = "/sitecore/content/#admin-portal#/#supplier-setup#/#global#/#_supporting-content#/#supplier-types#//*[@@templateid='{3E85F816-1D87-4FFA-8261-820C92266305}']";

            public const string contactsList = "ContactsList";
            public const string userName = "UserName";
            public const string isActive = "IsActive";
            public const string group = "Group";
        }
    }
}
