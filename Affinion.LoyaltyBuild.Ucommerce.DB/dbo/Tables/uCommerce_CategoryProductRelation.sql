﻿CREATE TABLE [dbo].[uCommerce_CategoryProductRelation] (
    [CategoryProductRelationId] INT IDENTITY (1, 1) NOT NULL,
    [ProductId]                 INT NOT NULL,
    [CategoryId]                INT NOT NULL,
    [SortOrder]                 INT CONSTRAINT [DF_uCommerce_CategoryProductRelation_SortOrder] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [uCommerce_PK_CategoryProductRelation] PRIMARY KEY CLUSTERED ([CategoryProductRelationId] ASC),
    CONSTRAINT [uCommerce_FK_CategoryProductRelation_Category] FOREIGN KEY ([CategoryId]) REFERENCES [dbo].[uCommerce_Category] ([CategoryId]),
    CONSTRAINT [uCommerce_FK_CategoryProductRelation_Product] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[uCommerce_Product] ([ProductId]),
    CONSTRAINT [Unique_CategoryId_ProductId] UNIQUE NONCLUSTERED ([CategoryId] ASC, [ProductId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_CategoryProductRelation]
    ON [dbo].[uCommerce_CategoryProductRelation]([CategoryId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_uCommerce_CategoryProductRelation_ProductId]
    ON [dbo].[uCommerce_CategoryProductRelation]([ProductId] ASC)
    INCLUDE([CategoryProductRelationId], [CategoryId], [SortOrder]);

