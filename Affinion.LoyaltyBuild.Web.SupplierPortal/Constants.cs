﻿using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal
{
    public class Constants
    {
        #region sitecore Item path
        public const string hotelPath = "/sitecore/content/#admin-portal#/#supplier-setup#//*[@@templateid='{A7235C94-2EF5-4A3B-87DB-0D4DCF10B4DB}']";
        public const string newsItemPath = "/sitecore/content/admin-portal/supplier-setup/news/news/";
        public const string GroupHotelPath = "/sitecore/content/#admin-portal#/#supplier-setup#/#global#/#_supporting-content#/#groups#//*[@@templateid='{26507FB6-AFD2-4DCC-B56F-AD1BB5D60CFA}']";
        public const string supplireTypePath = "/sitecore/content/#admin-portal#/#supplier-setup#/#global#/#_supporting-content#/#supplier-types#//*[@@templateid='{3E85F816-1D87-4FFA-8261-820C92266305}']";
        public const string Partialpaymentdetails = "/sitecore/content/#admin-portal#/#client-setup#/{0}//*[@@templateid='{59B5739C-623B-46AE-9266-37BAEE2D1136}']";
        public const string AddOnsTemplateID = "{F56CBAA2-DA19-46B1-8474-6BCB27818303}";
        public const string AddOnsFolderTemplateID = "{04E030C4-E90E-418F-AE7D-9F797EA6399F}";
        #endregion
        #region Workflow IDs
        public const string workFlowFieldID = "{A4F985D9-98B3-4B52-AAAF-4344F6E747C6}";
        public const string workflowStateFieldID = "{3E431DE1-525E-47A3-B6B0-1CCBEC3A8C98}";
        public const string newWorkFlowFieldID = "{68A45159-F584-4DC9-A41F-8E40F890AB0C}";
        public const string newWorkflowStateFieldID = "{DE01104B-DF4D-4950-9CE4-0B85C5CB5AAA}";
        public const string newWorkFlowApprovedStateID = "{05746F7D-D13D-43A9-9A5F-744DA8F100AE}";
        #endregion
        #region Login
        
        public const string suppliers = "suppliers";
        public const string hotelRole = @"suppliers\hotel admin";
        public const string marketing = @"suppliers\marketing";
        public const string grpHotelRole = @"suppliers\hotel group";
        public const string lbAdminRole = @"suppliers\supply team";
        public const string superUser = @"suppliers\super user";
        public const string accountManagement = @"suppliers\account management";
        public const string finance = @"suppliers\finance";
        public const string IT = @"suppliers\it";
        public const string hotelSupervisor = @"suppliers\hotel supervisor";


        public const string forgotUserNameMailSubject = "Supply Management User Name Request";
        public const string msgUserNameDetails = "You will receive an email shortly to this address with the username details";
        public const string changePswdMailSubject = "Password Request";
        public const string userName = "UserName";
        public const string DateMonthFormat = "dd/MM/yyyy";
        #endregion

        #region Error Msg Sitecore Fields
        public static readonly string AuthenticationFailed = "Authentication Failed";
        public static readonly string InvalidCredentials = "Invalid Credentials";
        public static readonly string EmailShouldNotEmpty = "Email Should Not Empty";
        public static readonly string DoesNotExist = "Does Not Exist";
        public static readonly string EmailIdNotFound = "Email ID Not Found";
        public static readonly string UserInactive = "User Inactive";
        public static readonly string UserNotFound = "User Not Found";
        public static readonly string UserNamePasswordIncorrectMsg = "UserName Password Incorrect";
        public static readonly string ChangePasswordUrlField = "Change Password Url";
        public static readonly string MultipleUserMessage = "MultipleUserMessage";
        public static readonly string UserLocked = "User Locked";
        public static readonly string SomethingWentWrong = "Something Went Wrong";
        public static readonly string AlreadyChanged = "AlreadyChanged";

        public static readonly string PasswordExistsInHistory="Password Exists In History";
        public static readonly string PasswordChanged = "Password Changed";
        public static readonly string ProvideOldPasswordCorrectly = "Provide Old Password Correctly";
        public static readonly string NewPasswordDoesNotMatchWithRegex = "New Password does not match with the regular expressions";
        public static readonly string PasswordHasNotBeenChanged = "Password has not been changed";
        public static readonly string PasswordDoesNotMatch = "Password does not match";
        public static readonly string HomePageUrl = "HomePageUrl";
        public static readonly string ClickHereToContinue = "Click Here To Continue";
        #endregion

        #region sitecore field names
        public const string images = "Images";
        public const string isActive = "IsActive";
        public const string title = "Title";
        public const string description = "Description";
        public const string Priority = "Priority";
        public const string contactsList = "ContactsList";
        public const string group = "Group";
        public const string name = "Name";
        public const string addressLine1 = "AddressLine1";
        public const string phone = "Phone";
        public const string fax = "Fax";
        public const string emailID1 = "EmailID1";
        public const string firstName = "FirstName";
        public const string jobTitle = "JobTitle";
        public const string emailAddress = "EmailAddress";
        public const string directPhone = "DirectPhone";
        public const string email = "Email";
        public const string LogoLocation = "Logo Location";
        public const string Header = "Header";
        public const string BackgroundImage = "Background Image";
        public const string BackgroundColor = "Background Color";
        public const string LayoutStyle = "Layout Style";
        public const string ShowTopLine = "Show Top Line";
        public const string backgroundimage = "background-image";
        public const string patterns = "patterns";
        public const string backgroundpattern = "background-pattern";
        public const string backgroundcover = "background-cover";
        public const string backgroundcolor = "background-color";
        public const string uCommerceCategoryID = "UCommerceCategoryID";

        #endregion


        #region PropertyInformation
        public const string itemId = "itemId";
        #endregion

        #region redirect urls
        public const string redirectWelcomePage = @"~/home";
        public const string redirectToLogOn = @"/Login";
        #endregion

        #region UpdateSection
        public const string UpdatedMsg = "Updated Values Successfully";
        #endregion

        #region News Section

        public static readonly string Country = "Country";
        public static readonly string Location = "Location";
        public static readonly string MapLocation = "MapLocation";
        public static readonly string SupplierType = "SupplierType";
        public static readonly string Culture = "Culture";
       

        #endregion

        #region Login Templete

        public static readonly string LogOnPageItemId = "{3108821F-BC42-4A1F-BBB8-EC131964EEC3}";

        public static readonly string PasswordGuideFieldName = "PasswordGuidePdf";

        
        public static readonly string ContactUS = "ContactUS";
        public static readonly string ContactUsMessage = "ContactUsMessage";
        public static readonly string ContactUsTitle = "ContactUsTitle";

        #endregion

        #region Footer Links
        public static readonly string FooterPDFFieldName = "PDF";
        public static readonly string ChangePassFooterLinkID = "{3358DCC3-8404-4017-A0AB-0A4024CC61E3}";
        public static readonly string ChangePasswordPageId = "{2FC78011-E338-4F74-BA0B-FC0F3071A2DC}";
        public const string QandALink = "{AA4316DB-F0CE-485E-A4DB-8E7DA8E964E4}";

        #endregion


        #region EMail Sitecore Fields
        public static readonly string SenderID = "Sender Email ID";
        public static readonly string ForgotUserNameSubject = "ForgetUserNameMailSubject";
        public static readonly string ChangePaswordSubject = "ChangePasswordMailSubject";
        public static readonly string ForgotUserNameMailBody = "ForgetUserNameMailBody";
        public static readonly string ChangePaswordMailBody = "ChangePasswordMailBody";
        public static readonly string ReceiveAnEmailShortly = "ReceiveAnEMailShortly";
        public static readonly string EmailDoesntExist = "EmailDoesntExist";
        #endregion
    }
}