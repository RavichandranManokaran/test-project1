﻿IF NOT EXISTS (SELECT 1 FROM [dbo].[uCommerce_NoteTypes])
BEGIN
INSERT INTO [dbo].[uCommerce_NoteTypes] ([Description]) VALUES ('Special Request')
INSERT INTO [dbo].[uCommerce_NoteTypes] ([Description]) VALUES ('Cancel Reason')
END
GO