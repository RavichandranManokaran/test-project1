﻿//<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           StockSearchSublayout.ascx.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal
/// Description:           SubLayout for stock search box
/// </summary>
namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal
{
    #region Using Directives

    using Affinion.LoyaltyBuild.Common;
    using Affinion.LoyaltyBuild.Common.Exceptions;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.Search.Data;
    using Affinion.LoyaltyBuild.Search.Entities;   
    using Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.UserControls;
    using Sitecore.Data.Items;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    #endregion

    /// <summary>
    /// Sub-layout for stock information search box
    /// </summary>
    public partial class StockSearchSublayout : BaseSublayout
    {
        #region Fields
        protected bool useAjax = false;
        protected string submitUrl = string.Empty;
        protected string cssClass = string.Empty;
        protected string searchBoxId = string.Empty;
        protected string calendarStartDate = "0";
        protected PageMode pageMode;
        private string childAges = string.Empty;

        #endregion

        #region Properties
        //public string Messages
        //{
        //    get
        //    {
        //        try
        //        {

        //            Dictionary<string, string> errors = new Dictionary<string, string>();
        //            errors.Add("InvalidSearchCriteriaMessage", "Invalid Search Criteria");
        //            StringBuilder sb = new StringBuilder();

        //            sb.Append("var messages = {");

        //            foreach (KeyValuePair<string, string> error in errors)
        //            {
        //                sb.Append(error.Key);
        //                sb.Append(":'");
        //                sb.Append(error.Value);
        //                sb.Append("',");
        //            }

        //            sb.Append("};");

        //            string errorString = sb.ToString();
        //            int commaIndex = errorString.LastIndexOf(',');

        //            errorString = errorString.Remove(commaIndex, 1);

        //            return errorString;
        //        }
        //        catch (Exception ex)
        //        {
        //            Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, null);
        //            return string.Empty;
        //        }


        //        // var errorMessages = {type:"Fiat", model:"500", color:"white"};
        //    }
        //}


        #endregion

        #region Protected Methods

        protected string DatePickerContainerClass
        {
            get
            {
                if (this.DateSelectionCheckBox.Checked)
                {
                    return "hide-div";
                }

                return string.Empty;
            }
        }


        /// <summary>
        /// OnInit
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            try
            {
                //if (!IsPostBack)
                //{
                //CreateChildAgeDropdownLists();
                //}
                if (SearchKey.IsChildBedSelectionAvailable)
                {
                    CreatePeopleSelectionControls();
                }
                else
                {
                    CreateChildAgeDropdownLists();
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, this);
            }
        }

        public override void PreLoad(EventArgs e)
        {

            Item item = this.GetDataSource();

            Uri requestUrl = Request.Url;

            if (item == null)
            {
                return;
            }

            InitializeControlsPreload();

            //if (this.IsPostBack && this.Request != null && this.Request.Params["__EVENTTARGET"] != null && this.Request.Params["__EVENTTARGET"].Contains("Submit"))
            if (this.IsPostBack && this.Request != null)
            {
                string eventTarget = this.Request.Params["__EVENTTARGET"];

                if (eventTarget != null && eventTarget.Contains("Submit"))
                {

                    this.SetPageMode();
                    StringBuilder parameters;


                    if (SearchKey.IsChildBedSelectionAvailable)
                    {
                        if (!ValidateSearchCriteriaChildBedSelection(requestUrl, item))
                        {
                            return;
                        }

                        parameters = BuildParametersBedTypeSelection();

                    }
                    else
                    {
                        parameters = BuildParameters();

                    }


                    SetFormAction(item, requestUrl, parameters);
                }
            }
        }


        /// <summary>
        /// Code to execute on page load
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The parameter</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
            
                this.BindSitecoreTexts();

                //SubmitForm.ServerClick += SubmitForm_ServerClick;
                Item item = this.GetDataSource();
                calendarStartDate = SitecoreFieldsHelper.GetValue(item, "CalendarStartDate");

                AddJavascriptVariables(item);

                if (!ValidateChildAge(item))
                {
                    return;
                }

                Uri requestUrl = Request.Url;

                ///Initialize UI controls
                InitializeControls(item);

                childAges = string.Empty;

                if (item == null)
                {
                    return;
                }

                this.useAjax = SitecoreFieldsHelper.GetBoolean(item, "UseAjaxSubmit");
                this.cssClass = this.GetParameter("CssClass", "Title");
                this.searchBoxId = item.ID.ToString();

                // show hide controllers and bind data
                this.ShowHideControllers(item);
                this.BindData(item);
                Page.LoadComplete += new EventHandler(Page_LoadComplete);

                string requestQuery = requestUrl.Query;
                if (!this.IsPostBack && !string.IsNullOrEmpty(requestQuery))
                {
                    //Bind data to the text boxes in the result page
                    PrefillControls(item, requestUrl);
                }

                SetVisibilityOfControls();
                
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, this);
            }

        }


        /// <summary>
        /// Code execute after the page load completed successfully 
        /// </summary>
        /// <param name="sender">The Sender</param>
        /// <param name="e">The Parameter</param>
        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            try
            {
                Item item = this.GetDataSource();
                var querySession = Session["SearchKeyValues"];

                ///Check the session value is not empty
                if (querySession != null && querySession.ToString() != string.Empty)
                {
                    ///Get the request Url and append session Query to current Url.
                    UriBuilder urlBuilder = new UriBuilder(this.Request.Url);
                    urlBuilder.Query = querySession.ToString().Split('?')[1];

                    ///Fill the controllers according to the session query values
                    PrefillControls(item, urlBuilder.Uri);
                }
                else
                    return;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, this);
                throw;
            }
        }

        #endregion

        #region Private Methods

        private void CreatePeopleSelectionControls()
        {
            Item item = this.GetDataSource(); ;

            int maxNoOfRooms = SitecoreFieldsHelper.GetInteger(item, "MaxRooms", 5);


            for (int i = 0; i < maxNoOfRooms; i++)
            {
                HtmlGenericControl outerDiv = new HtmlGenericControl("div");

                outerDiv.Attributes.Add("class", "col-xs-12 col-md-6 roomResults roomPanels roomOn rooms panel-padding");
                outerDiv.Attributes.Add("id", string.Format("Room{0}Div", (i + 1).ToString()));

                RoomSelectionUserControl roomSelectionControl = (RoomSelectionUserControl)Page.LoadControl("../../UserControls/RoomSelectionUserControl.ascx");
                roomSelectionControl.DataSource = item;
                roomSelectionControl.RoomId = (i + 1).ToString();
                roomSelectionControl.ID = string.Format("peoplePanel{0}", i + 1);

                outerDiv.Controls.Add(roomSelectionControl);

                RoomDetailsPanel.Controls.Add(outerDiv);

            }

        }

        private void SetVisibilityOfControls()
        {
            if (SearchKey.IsChildBedSelectionAvailable)
            {
                RoomSelectionDiv.Visible = false;
                ChildBedSelectionDiv.Visible = true;
            }
            else
            {
                RoomSelectionDiv.Visible = true;

                ChildBedSelectionDiv.Visible = false;
            }
        }

        /// <summary>
        /// Initialize controls
        /// </summary>
        /// <param name="item">item</param>
        private void InitializeControls(Item item)
        {
            ServerSideErrorPanel.Visible = true;
            //ErrorPanel.InnerText = SitecoreFieldsHelper.GetValue(item, Constants.ChildAgeRequiredErrorMessageFieldName, "Please select age of children");
            ErrorPanel.ClientIDMode = ClientIDMode.Static;
            ErrorPanel.Style.Add("display", "none");
        }

        private void InitializeControlsPreload()
        {
            ServerSideErrorPanel.Visible = false;
        }

        /// <summary>
        /// Build parameter string
        /// </summary>
        /// <returns>Return URI parameters</returns>
        private StringBuilder BuildParameters()
        {
            Uri requestUrl = Request.Url;
            string requestQuery = requestUrl.Query;
            StringBuilder parameters = new StringBuilder();

            string noOfChildrens = NoOfChildren.Text;

            parameters.Append("?Destination=");
            parameters.Append((this.Destination.Visible) ? Server.UrlEncode(this.Destination.Text) : this.DestinationDropDown.SelectedValue);
            parameters.Append("&DestinationId=");
            parameters.Append(this.CategoryId.Value);
            parameters.Append("&DestinationType=");
            parameters.Append(this.DestinationType.Value);
            parameters.Append("&CheckinDate=");
            SetDate("CheckinDate", parameters);
            parameters.Append("&CheckoutDate=");
            SetDate("CheckoutDate", parameters);
            parameters.Append("&FlexibleDates=");
            parameters.Append(this.DateSelectionCheckBox.Checked.ToString());
            parameters.Append("&NoOfNights=");
            parameters.Append(this.NoOfNights.Text);
            parameters.Append("&NoOfRooms=");
            parameters.Append(this.NoOfRooms.Text);
            parameters.Append("&NoOfAdults=");
            parameters.Append(this.NoOfAdults.Text);
            parameters.Append("&NoOfChildren=");
            parameters.Append(noOfChildrens);
            parameters.Append("&OfferGroup=");
            parameters.Append(this.OfferGroup.Text);
            parameters.Append("&Mode=");
            parameters.Append(Enum.GetName(typeof(PageMode), this.pageMode));

            ///Check whether child ages are already filled when validated
            if (string.IsNullOrWhiteSpace(childAges))
            {
                childAges = GetChildAgesParameter(noOfChildrens);
            }

            parameters.Append(childAges);

            if (this.CountryDropDown.Visible)
            {
                parameters.Append("&Location=");
                parameters.Append(this.CountryDropDown.SelectedValue);
            }
            else
            {
                AppendLocation(requestUrl, requestQuery, parameters);
            }

            return parameters;
        }


        /// <summary>
        /// Build parameter string
        /// </summary>
        /// <returns>Return URI parameters</returns>
        private StringBuilder BuildParametersBedTypeSelection()
        {
            Uri requestUrl = Request.Url;
            string requestQuery = requestUrl.Query;
            StringBuilder parameters = new StringBuilder();

            parameters.Append("?Destination=");
            parameters.Append((this.Destination.Visible) ? Server.UrlEncode(this.Destination.Text) : this.DestinationDropDown.SelectedValue);
            parameters.Append("&DestinationId=");
            parameters.Append(this.CategoryId.Value);
            parameters.Append("&DestinationType=");
            parameters.Append(this.DestinationType.Value);
            parameters.Append("&CheckinDate=");
            SetDate("CheckinDate", parameters);
            parameters.Append("&CheckoutDate=");
            SetDate("CheckoutDate", parameters);
            parameters.Append("&FlexibleDates=");
            parameters.Append(this.DateSelectionCheckBox.Checked.ToString());
            parameters.Append("&NoOfNights=");
            parameters.Append(this.NoOfNights.Text);
            parameters.Append("&OfferGroup=");
            parameters.Append(this.OfferGroup.Text);
            parameters.Append("&Mode=");
            parameters.Append(Enum.GetName(typeof(PageMode), this.pageMode));

            if (this.CountryDropDown.Visible)
            {
                parameters.Append("&Location=");
                parameters.Append(this.CountryDropDown.SelectedValue);
            }
            else
            {
                AppendLocation(requestUrl, requestQuery, parameters);
            }

            int roomsCount;

            if (int.TryParse(ChildBedSelectionNoOfRoomsDropDownList.Text, out roomsCount))
            {
                // parameters.Append(GetPeopleCountParameter(roomsCount));

                string json = GetJson(requestUrl, roomsCount);

                var bytes = Encoding.UTF8.GetBytes(json);
                var base64 = Convert.ToBase64String(bytes);

                parameters.Append("&JSON=");
                parameters.Append(base64);
            }
            return parameters;
        }

        private string GetJson(Uri requestUrl, int roomsCount)
        {
            SearchKey searchKey = GetSearchKey(requestUrl, roomsCount);

            string json = Newtonsoft.Json.JsonConvert.SerializeObject(searchKey);

            return json;
        }

        private SearchKey GetSearchKey(Uri requestUrl, int roomsCount)
        {
            SearchKey searchKey = new SearchKey();

            //searchKey.UpdateDates(GetDate("CheckinDate"), GetDate("CheckinDate"), DateSelectionCheckBox.Checked.ToString());
            searchKey.UpdateDates(new QueryStringHelper(requestUrl));

            string destination = Destination.Text;

            //searchKey.Destination = destination;
            searchKey.Destination = !string.IsNullOrEmpty(destination) ? destination.ToLowerInvariant() : string.Empty;
            //searchKey.SetSortMode(helper.GetValue("sortmode"), helper.GetValue("sortvalue"));

            searchKey.NumberOfRooms = roomsCount;

            string query = requestUrl.Query;

            if (!String.IsNullOrEmpty(query))
            {
                if (query.IndexOf("Location") >= 0)
                {
                    QueryStringHelper stringHelper = new QueryStringHelper(requestUrl);
                    string location = stringHelper.GetValue("Location");

                    searchKey.Location = location;
                }

            }

            searchKey = GetSelectedRooms(requestUrl, roomsCount, searchKey);
            return searchKey;
        }

        private SearchKey GetSelectedRooms(Uri url, int noOfRooms, SearchKey searchKey)
        {
            IList<RoomRequest> roomList = new List<RoomRequest>();
            StringBuilder childAgesParameters = new StringBuilder();

            for (int i = 0; i < noOfRooms; i++)
            {
                RoomRequest room = new RoomRequest();
                RoomSelectionUserControl roomSelectionControl = RoomDetailsPanel.FindControl(string.Format("peoplePanel{0}", i + 1)) as RoomSelectionUserControl;

                //childAgesParameters.Append(string.Format("&Room{0}AdultsCount=", i + 1));
                //childAgesParameters.Append(roomSelectionControl.AdultsCount);
                int adultCount;

                if (int.TryParse(roomSelectionControl.AdultsCount, out adultCount))
                {
                    room.AdultCount = adultCount;
                }


                //childAgesParameters.Append(string.Format("&Room{0}ChildrenCount=", i + 1));
                //childAgesParameters.Append(roomSelectionControl.ChildrenCount);

                int childrenCount;

                if (int.TryParse(roomSelectionControl.ChildrenCount, out childrenCount))
                {
                    room.ChildrenCount = childrenCount;
                }


                if (SearchKey.IsChildBedSelectionAvailable)
                {
                    room.ChildrenBeds = roomSelectionControl.ChildBeds;
                }
                else
                {
                    room.ChildrenAges = roomSelectionControl.ChildrenAges;

                }

                searchKey.SelectedRooms.Add(room);

            }


            return searchKey;
        }

        /// <summary>
        /// Append location to query string parameters
        /// </summary>
        /// <param name="requestUrl"></param>
        /// <param name="requestQuery"></param>
        /// <param name="parameters"></param>
        private static void AppendLocation(Uri requestUrl, string requestQuery, StringBuilder parameters)
        {
            if (!String.IsNullOrEmpty(requestQuery))
            {
                if (requestQuery.IndexOf("Location") >= 0)
                {
                    QueryStringHelper stringHelper = new QueryStringHelper(requestUrl);
                    string location = stringHelper.GetValue("Location");
                    parameters.Append("&Location=");
                    parameters.Append(location);
                }

            }
        }

        /// <summary>
        /// Determine which page to load depending on the input
        /// </summary>
        private void SetPageMode()
        {
            bool flexibleDates = DateSelectionCheckBox.Checked; //string.IsNullOrEmpty(DateSelectionCheckBox.Text) || string.IsNullOrEmpty(CheckinDate.Text);
            bool noDestination = string.IsNullOrEmpty(Destination.Text);

            if (!noDestination && flexibleDates) // destination provided, no dates
            {
                pageMode = PageMode.Locations;
            }
            else if (!noDestination && !flexibleDates) // destination provided, date provided
            {
                pageMode = PageMode.Hotels;
            }
            else if (noDestination && !flexibleDates) // no destination, date provided
            {
                pageMode = PageMode.LocationFinder;
            }
            else
            {
                pageMode = PageMode.Locations;
            }
        }

        /// <summary>
        /// Get the correct results page to redirect for search results
        /// </summary>
        /// <param name="item">The searchbox item</param>
        /// <returns>Returns the url</returns>
        private AnchorTagHelperData GetResultsPage(Item item)
        {
            string fieldName = "HotelsPage";

            if (pageMode == PageMode.Hotels)
            {
                fieldName = "HotelsPage";
            }
            else if (pageMode == PageMode.Locations)
            {
                fieldName = "LocationsPage";
            }
            else if (pageMode == PageMode.LocationFinder)
            {
                fieldName = "LocationFinderPage";
            }

            AnchorTagHelperData resultsPage = SitecoreFieldsHelper.GetUrl(item, fieldName);
            return resultsPage;
        }

        /// <summary>
        /// Set date to parameters
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="parameters">Parameter string</param>
        private void SetDate(string controller, StringBuilder parameters)
        {
            string checkinDateText = CheckinDate.Text;

            if (checkinDateText.Length == 0)
                this.DateSelectionCheckBox.Checked = true;

            bool dateSelection = this.DateSelectionCheckBox.Checked;

            if (dateSelection == true)
            {
                if (controller == "CheckinDate")
                {
                    parameters.Append(DateTime.Today.AddDays(1).ToString(DateTimeHelper.DateFormat));
                }
                else
                {
                    string checkoutDate = DateTime.Today.AddDays(2).ToString(DateTimeHelper.DateFormat);
                    this.CheckoutDate.Value = checkoutDate;
                    parameters.Append(checkoutDate);
                }

            }
            else
            {
                if (controller == "CheckinDate")
                {
                    parameters.Append(checkinDateText);
                }
                else
                {
                    DateTime checkinDate = DateTimeHelper.ParseDate(checkinDateText);
                    int numberOfNights = int.Parse(this.NoOfNights.SelectedValue);
                    checkinDate = checkinDate.AddDays(numberOfNights);
                    string checkoutDate = checkinDate.ToString(DateTimeHelper.DateFormat);
                    this.CheckoutDate.Value = checkoutDate;
                    parameters.Append(checkoutDate);
                }

            }
        }

        /// <summary>
        /// Get child age parameter string
        /// </summary>
        /// <param name="noOfChildrens"></param>
        /// <returns>Formatted age parameters</returns>
        private string GetChildAgesParameter(string noOfChildrens)
        {
            string childAgesParameters = string.Empty;

            int childCount;

            if (int.TryParse(noOfChildrens, out childCount))
            {

                for (int i = 0; i < childCount; i++)
                {
                    DropDownList dropDownList = ChildAgePanel.FindControl(string.Format("AgeDropdownList{0}", i + 1)) as DropDownList;
                    childAgesParameters = string.Format("{0}{1}{2}", childAgesParameters, "&ChildAge=", dropDownList.SelectedValue);
                }
            }
            return childAgesParameters;
        }

        /// <summary>
        /// Dynamically create child age dropdowns
        /// </summary>
        private void CreateChildAgeDropdownLists()
        {
            Item item = this.GetDataSource();
            int maxChildren = SitecoreFieldsHelper.GetInteger(item, "MaxChildren", 7);
            int maxChildAge = SitecoreFieldsHelper.GetInteger(item, "MaxChildAge", 17);

            IList<string> childAgesCollection = new List<string>();

            if (!IsPostBack)
            {
                Uri requestUrl = Request.Url;
                QueryStringHelper helper = new QueryStringHelper(requestUrl);

                string childAgesParameter = helper.GetValue("ChildAge");

                if (childAgesParameter != null)
                {
                    childAgesCollection = childAgesParameter.Split(',');
                }
            }


            for (int i = 0; i < maxChildren; i++)
            {
                HtmlGenericControl outerDiv = new HtmlGenericControl("div");

                HtmlGenericControl innerDiv = new HtmlGenericControl("div");

                //innerDiv.Controls.Add(CreateDropDownList(string.Concat("AgeDropdownList", i + 1)));
                CreateDropDownList(string.Format("AgeDropdownList{0}", (i + 1)), innerDiv, maxChildAge, childAgesCollection.ElementAtOrDefault(i));

                innerDiv.Attributes.Add("role", "group");

                outerDiv.Controls.Add(innerDiv);
                outerDiv.Attributes.Add("class", "age col-xs-2");
                outerDiv.Attributes.Add("id", String.Format("AgeDropdownDiv{0}", (i + 1)));

                ChildAgePanel.Controls.Add(outerDiv);

            }

        }

        private static void CreateDropDownList(string id, Control parent, int maxChildAge, string selectedValue)
        {
            ArrayList listItems = new ArrayList();

            ///Add default option
            listItems.Add("Select");

            ///Add zero to twelve months option
            listItems.Add(LanguageReader.GetText(Constants.ZeroToTwelveMonthsDictionaryKey, "0"));


            for (int i = 0; i < maxChildAge; i++)
            {
                listItems.Add(i + 1);

            }

            DropDownList dropDownList = new DropDownList();
            dropDownList.ID = id;
            dropDownList.ClientIDMode = ClientIDMode.Static;

            dropDownList.Attributes.Add("class", "form-control childAgeDropdown");
            dropDownList.DataSource = listItems;
            dropDownList.DataBind();
            dropDownList.SelectedValue = selectedValue;


            parent.Controls.Add(dropDownList);
        }

        private void ShowHideControllers(Item item)
        {
            this.Destination.Visible = this.DestinationLabel.Visible = SitecoreFieldsHelper.GetBoolean(item, "Destination");
            this.CheckinDate.Visible = this.CheckinDateLabel.Visible = SitecoreFieldsHelper.GetBoolean(item, "CheckinDate");
            this.NoOfNights.Visible = this.CheckoutDateLabel.Visible = SitecoreFieldsHelper.GetBoolean(item, "NoOfNights");
            this.DateSelectionCheckBox.Visible = this.FlexibleDatePanel.Visible = SitecoreFieldsHelper.GetBoolean(item, "SpecificDateSelection");
            this.NoOfRooms.Visible = this.RoomsLabel.Visible = SitecoreFieldsHelper.GetBoolean(item, "Rooms");
            this.NoOfAdults.Visible = this.AdultsLabel.Visible = SitecoreFieldsHelper.GetBoolean(item, "Adults");
            this.NoOfChildren.Visible = this.ChildrenLabel.Visible = this.ChildrenAgeTitleLabel.Visible = SitecoreFieldsHelper.GetBoolean(item, "Children");
            this.OfferGroup.Visible = this.OfferGroupLabel.Visible = SitecoreFieldsHelper.GetBoolean(item, "OfferGroup");
            this.CountryDropDown.Visible = this.CountryPanel.Visible = this.CountryLabel.Visible = SitecoreFieldsHelper.GetBoolean(item, "Country");
        }

        /// <summary>
        /// pre-fill the text-boxes in the results page according to the values entered to perform search.
        /// </summary>
        /// <param name="requestUrl"></param>
        private void PrefillControls(Item item, Uri requestUrl)
        {
            QueryStringHelper helper = new QueryStringHelper(requestUrl);
            if (this.Destination.Visible)
            {
                bool showSingleDefaultValue = SitecoreFieldsHelper.GetDropListFieldValue(item, "DestinationDefaultValueMode") == "SingleValue";
                string destinationControlValidation = ValidateControlValue(item, "Destination", "DestinationDefaultValue", this.Destination, this.DestinationLabel);
                this.Destination.Text = (showSingleDefaultValue && !string.IsNullOrEmpty(destinationControlValidation)) ? destinationControlValidation : helper.GetValue("Destination");
                this.Destination.Enabled = !showSingleDefaultValue;
            }
            else
            {
                this.Destination.Text = this.DestinationDropDown.SelectedValue = helper.GetValue("Destination");
            }
            this.CategoryId.Value = helper.GetValue("DestinationId");
            this.DestinationType.Value = helper.GetValue("DestinationType");
            //this.Destination.Text = helper.GetValue("Destination");
            string checkinDateControlValidation = ValidateControlValue(item, "CheckinDate", "CheckinDateDefaultValue", this.CheckinDate, this.CheckinDateLabel);
            if (!string.IsNullOrEmpty(checkinDateControlValidation))
            {
                int startDatesFromToday = 0;
                int.TryParse(calendarStartDate, out startDatesFromToday);
                DateTime date = DateTime.ParseExact(checkinDateControlValidation, "yyyyMMddTHHmmssZ", CultureInfo.InvariantCulture);
                DateTime startDate = DateTime.Now.AddDays(startDatesFromToday);
                date = date < startDate ? startDate : date;
                string aa = date.ToString("dd/MM/yyyy");
                this.CheckinDate.Text = aa;
            }
            else
            {
                this.CheckinDate.Text = helper.GetValue("CheckinDate");
            }

            this.CheckoutDate.Value = helper.GetValue("CheckoutDate");
            string noOfNightsControlValidation = ValidateControlValue(item, "NoOfNights", "NoOfNightsDefaultValue", this.NoOfNights, this.CheckoutDateLabel);
            this.NoOfNights.SelectedValue = (!string.IsNullOrEmpty(noOfNightsControlValidation)) ? noOfNightsControlValidation : helper.GetValue("NoOfNights");
            //this.NoOfNights.SelectedValue = helper.GetValue("NoOfNights");

            DateTime checkinDate = DateTimeHelper.ParseDate(this.CheckinDate.Text);
            int numberOfNights = int.Parse(this.NoOfNights.SelectedValue);
            DateTime checkoutDate = checkinDate.AddDays(numberOfNights);
            this.CheckoutDate.Value = checkoutDate.ToString(DateTimeHelper.DateFormat);
            
            string SpecificDateControlValidation = ValidateCheckboxValue(item, "SpecificDateSelection", "SpecificDateSelectionDefaultValue", this.DateSelectionCheckBox);
            this.DateSelectionCheckBox.Checked = (SpecificDateControlValidation == "1") ? true : (helper.GetValue("FlexibleDates") == "True") ? true : false;
            //this.DateSelectionCheckBox.Checked = (helper.GetValue("SpecificDate") == "True") ? true : false;
            string roomsControlValidation = ValidateControlValue(item, "Rooms", "RoomsDefaultValue", this.NoOfRooms, this.RoomsLabel);
            this.NoOfRooms.SelectedValue = (!string.IsNullOrEmpty(roomsControlValidation)) ? roomsControlValidation : helper.GetValue("NoOfRooms");
            //this.NoOfRooms.SelectedValue = helper.GetValue("NoOfRooms");
            string adultsControlValidation = ValidateControlValue(item, "Adults", "AdultsDefaultValue", this.NoOfAdults, this.AdultsLabel);
            this.NoOfAdults.SelectedValue = (!string.IsNullOrEmpty(adultsControlValidation)) ? adultsControlValidation : helper.GetValue("NoOfAdults");
            //this.NoOfAdults.SelectedValue = helper.GetValue("NoOfAdults");
            string childrenControlValidation = ValidateControlValue(item, "Children", "ChildrenDefaultValue", this.NoOfChildren, this.ChildrenLabel);
            this.NoOfChildren.SelectedValue = (!string.IsNullOrEmpty(childrenControlValidation)) ? childrenControlValidation : helper.GetValue("NoOfChildren");
            this.ChildrenAgeTitleLabel.Visible = this.ChildAgePanel.Visible = this.NoOfChildren.Visible;
            //this.NoOfChildren.SelectedValue = helper.GetValue("NoOfChildren");
            string offerControlValidation = ValidateOfferGroupValue(item, "OfferGroup", "OfferGroupDefaultValue", this.OfferGroup, this.OfferGroupLabel);
            this.OfferGroup.SelectedValue = (!string.IsNullOrEmpty(offerControlValidation)) ? offerControlValidation : helper.GetValue("OfferGroup");

            if (this.CountryDropDown.Visible)
            {
                this.CountryDropDown.SelectedValue = helper.GetValue("Location");
            }

            string childAge = null;
            //getting children ages
            childAge = helper.GetValue("ChildAge");
            if (!string.IsNullOrEmpty(childAge))
            {
                List<string> ageList = new List<string>();

                foreach (string age in childAge.Split(','))
                {
                    ageList.Add(age);
                }

                for (int i = 0; i < ageList.Count; i++)
                {
                    DropDownList dropDownList = ChildAgePanel.FindControl(string.Format("AgeDropdownList{0}", i + 1)) as DropDownList;
                    //add value to dropdown
                    dropDownList.SelectedValue = ageList[i];
                }
            }

            SearchKey searchKey = new SearchKey(requestUrl, null);
            this.ChildBedSelectionNoOfRoomsDropDownList.SelectedValue = Convert.ToString(searchKey.NumberOfRooms);

        }

        private static string ValidateCheckboxValue(Item item, string checkboxField, string defaultValue, WebControl checkBox)
        {
            int checkBoxValue = SitecoreFieldsHelper.GetInteger(item, checkboxField, 0);
            string checkboxDefaultValue = SitecoreFieldsHelper.GetInteger(item, defaultValue, 0).ToString();
            
            if (checkboxDefaultValue == "1")
            {
                // checkBox.Enabled = false;
                return checkboxDefaultValue;
            }

            return checkboxDefaultValue;
        }

        /// <summary>
        /// offergroup value validation
        /// </summary>
        /// <param name="item">Data Source</param>
        /// <param name="fieldName"></param>
        /// <param name="selectedOfferGroup"></param>
        /// <param name="dropDownList"></param>
        /// <param name="dropListLabel"></param>
        /// <returns></returns>
        private static string ValidateOfferGroupValue(Item item, string fieldName, string selectedOfferGroup, WebControl dropDownList, Control dropListLabel)
        {
            //string defaultValue = item.Fields[defaultValueField].ToString();
            Item selectedOffer = SitecoreFieldsHelper.GetDropLinkFieldTargetItem(item, selectedOfferGroup);
            if (selectedOffer != null)
            {
                dropDownList.Enabled = false;
                string offerId = SitecoreFieldsHelper.GetValue(selectedOffer, "Title", string.Empty);
                return offerId;
            }
            else
            {
                return string.Empty;
            }


        }

        /// <summary>
        /// Binds data to controls
        /// </summary>
        private void BindData(Item item)
        {
            if (item != null)
            {
                this.DatasourceText.Visible = false;

                this.TitleLabel.Item =
                this.DestinationLabel.Item =
                this.CheckinDateLabel.Item =
                this.CheckoutDateLabel.Item =
                this.RoomsLabel.Item =
                this.AdultsLabel.Item =
                this.ChildrenLabel.Item =
                this.ChildrenAgeTitleLabel.Item =
                this.MobileVersionSearchLable.Item =
                this.OfferGroupLabel.Item = item;
                //this.SubmitLabel.Item = item;
                Submit.Text = SitecoreFieldsHelper.GetValue(item, "SubmitLabel");

                // read sitecore values
                string destinationText = SitecoreFieldsHelper.GetValue(item, "DestinationLabel");
                this.DateSelectionCheckBox.Text = SitecoreFieldsHelper.GetValue(item, "FlexibleDatesLabel");

                // set html5 attributes
                this.Destination.Attributes.Add("placeholder", destinationText);
                this.Destination.Attributes.Add("aria-describedby", destinationText);

                if(SitecoreFieldsHelper.GetValue(item, "DestinationDefaultValueMode") == "SingleValue")
                {
                    this.Destination.Text = SitecoreFieldsHelper.GetValue(item, "DestinationDefaultValue");
                    this.Destination.Enabled = false;
                }

                string defaultDate = SitecoreFieldsHelper.GetValue(item, "CheckinDateDefaultValue");
                this.CheckinDate.Attributes.Add("pattern", @"[0-9\/]*");

                if (string.IsNullOrEmpty(defaultDate))
                {
                    int startDatesFromToday = 0;
                    int.TryParse(calendarStartDate, out startDatesFromToday);
                    DateTime startDate = DateTime.Now.AddDays(startDatesFromToday);
                    this.CheckinDate.Text = startDate.ToString(DateTimeHelper.DateFormat);
                }
                else
                {
                    this.CheckinDate.Text = DateTime.ParseExact(defaultDate, "yyyyMMddTHHmmssZ", CultureInfo.InvariantCulture).ToString(DateTimeHelper.DateFormat);
                    this.CheckinDate.Enabled = false;
                }

                if (SitecoreFieldsHelper.GetBoolean(item, "SpecificDateSelectionDefaultValue"))
                {
                    this.DateSelectionCheckBox.Checked = true;
                    this.DateSelectionCheckBox.Attributes.Add("readonly", "readonly");
                }

                this.BindDropDowns(item);
            }
        }

        /// <summary>
        /// Bind data to drop downs
        /// </summary>
        /// <param name="item">The item to read data</param>
        private void BindDropDowns(Item item)
        {
            // read sitecore values
            int maxRooms = SitecoreFieldsHelper.GetInteger(item, "MaxRooms", 5);
            int maxAdults = SitecoreFieldsHelper.GetInteger(item, "MaxAdults", 8);
            int maxChildren = SitecoreFieldsHelper.GetInteger(item, "MaxChildren", 7);
            int maxNights = SitecoreFieldsHelper.GetInteger(item, "MaxNights", 10);
            string showAllText = SitecoreFieldsHelper.GetValue(item, "ShowAllLabel");
            string nightsDefaultValue = SitecoreFieldsHelper.GetValue(item, "NoOfNightsDefaultValue");

            List<int> defaultNights = new List<int>();
            if(!string.IsNullOrEmpty(nightsDefaultValue))
            {
                string[] splits = nightsDefaultValue.Split(',');
                foreach (string nightValue in splits)
                {
                    int tempNight;
                    if(int.TryParse(nightValue, out tempNight))
                    {
                        defaultNights.Add(tempNight);
                    }
                }
            }

            // bind data
            this.NoOfNights.DataSource = defaultNights.Count > 0 ? defaultNights : Enumerable.Range(1, maxNights);
            this.NoOfNights.DataBind();

            if (SearchKey.IsChildBedSelectionAvailable)
            {
                string selectedNoOfRooms = ChildBedSelectionNoOfRoomsDropDownList.SelectedValue;

                ChildBedSelectionNoOfRoomsDropDownList.DataSource = Enumerable.Range(1, maxRooms);
                ChildBedSelectionNoOfRoomsDropDownList.DataBind();
                ChildBedSelectionNoOfRoomsDropDownList.SelectedValue = selectedNoOfRooms;

            }
            else
            {
                string selectedNoOfRooms = SitecoreFieldsHelper.GetValue(item, "RoomsDefaultValue");
                this.NoOfRooms.DataSource = Enumerable.Range(1, maxRooms);
                this.NoOfRooms.DataBind();
                if (!string.IsNullOrEmpty(selectedNoOfRooms))
                {
                    this.NoOfRooms.SelectedValue = selectedNoOfRooms;
                    this.NoOfRooms.Enabled = false;
                }
            }

            BindDestinationDropdown(item);

            BindNoOfAdultsDropdown(item, maxAdults);

            BindNoOfChildrensDropdown(item, maxChildren);

            BindCountryDropdown(item);

            bool DisplayShowAll = SitecoreFieldsHelper.GetBoolean(item, "DisplayShowAllLabel");
            if (DisplayShowAll == true)
            {
                ListItem showAllItem = new ListItem(showAllText, "all");
                this.OfferGroup.AppendDataBoundItems = true;
                this.OfferGroup.Items.Insert(0, showAllItem);
            }

            this.OfferGroup.DataSource = SitecoreFieldsHelper.GetDropdownDataSource(item, "OfferGroups", "Title");
            this.OfferGroup.DataTextField = "Text";
            this.OfferGroup.DataValueField = "Text";
            this.OfferGroup.DataBind();

            Item selectedOffer = SitecoreFieldsHelper.GetDropLinkFieldTargetItem(item, "OfferGroupDefaultValue");
            if (selectedOffer != null)
            {
                string offerId = SitecoreFieldsHelper.GetValue(selectedOffer, "Title", string.Empty);
                this.OfferGroup.SelectedValue = offerId;
                this.OfferGroup.Enabled = false;
            }
        }

        private void BindDestinationDropdown(Item item)
        {
            if (item == null)
                return;

            string dataSourceFieldName;
            if (SitecoreFieldsHelper.GetDropListFieldValue(item, "DestinationDefaultValueMode") == "Suppliers")
            {
                dataSourceFieldName = "DestinationDefaultSuppliers";
            }
            else if (SitecoreFieldsHelper.GetDropListFieldValue(item, "DestinationDefaultValueMode") == "Locations")
            {
                dataSourceFieldName = "DestinationDefaultLocations";
            }
            else
            {
                return;
            }

            List<Item> dropDownItems = SitecoreFieldsHelper.GetMutiListItems(item, dataSourceFieldName).ToList();
            if (dropDownItems != null && dropDownItems.Count > 0)
            {
                this.Destination.Visible = false;
                this.DestinationDropDown.Visible = true;
                this.DestinationDropDown.DataSource = SitecoreFieldsHelper.GetDropdownDataSource(dropDownItems, "Name", "Name");
                this.DestinationDropDown.DataBind();
            }
        }

        private void BindCountryDropdown(Item item)
        {
            bool hideCountryList = !SitecoreFieldsHelper.GetBoolean(item, "Country");
            if(hideCountryList)
            {
                return;
            }

            ICollection<Item> countryList = SitecoreFieldsHelper.GetMutiListItems(item, "CountryDefaultValue");

            if (countryList.Count > 0)
            {
                string showAllLabel = SitecoreFieldsHelper.GetValue(item, "ShowAllLabel");
                ListItemCollection countryDataSource = SitecoreFieldsHelper.GetDropdownDataSource(countryList, "CountryName", "CountryName");
                countryDataSource.Insert(0, new ListItem(showAllLabel, "all"));

                this.CountryLabel.Item = item;
                this.CountryPanel.Visible = true;
                this.CountryDropDown.Visible = true;
                this.CountryDropDown.DataSource = countryDataSource;
                this.CountryDropDown.DataTextField = "Text";
                this.CountryDropDown.DataValueField = "Value";
                this.CountryDropDown.DataBind();
            }
        }

        private void BindNoOfChildrensDropdown(Item item, int maxChildren)
        {
            string selectedNoOfChildren = SitecoreFieldsHelper.GetValue(item, "ChildrenDefaultValue");
            this.NoOfChildren.DataSource = Enumerable.Range(0, maxChildren + 1);
            this.NoOfChildren.DataBind();
            if (!string.IsNullOrEmpty(selectedNoOfChildren))
            {
                this.NoOfChildren.SelectedValue = selectedNoOfChildren;
                this.NoOfChildren.Enabled = false;
            }
        }

        private void BindNoOfAdultsDropdown(Item item, int maxAdults)
        {
            string selectedNoOfAdults = SitecoreFieldsHelper.GetValue(item, "AdultsDefaultValue");
            this.NoOfAdults.DataSource = Enumerable.Range(1, maxAdults);
            this.NoOfAdults.DataBind();
            if (!string.IsNullOrEmpty(selectedNoOfAdults))
            {
                this.NoOfAdults.SelectedValue = selectedNoOfAdults;
                this.NoOfAdults.Enabled = false;
            }
        }

        private bool ValidateChildAge(Item source)
        {
            string allChildAges = string.Empty;
            bool isValid = true;
            int childCount;
            string noOfChildrens = NoOfChildren.Text;

            if (int.TryParse(noOfChildrens, out childCount))
            {

                for (int i = 0; i < childCount; i++)
                {
                    DropDownList dropDownList = ChildAgePanel.FindControl(string.Format("AgeDropdownList{0}", i + 1)) as DropDownList;

                    string selectedValue = dropDownList.SelectedValue;

                    if (selectedValue.Equals("Select"))
                    {
                        isValid = false;
                        break;
                    }
                    allChildAges = string.Format("{0}{1}{2}", allChildAges, "&ChildAge=", selectedValue);
                }
            }

            if (isValid)
            {
                childAges = allChildAges;
                return isValid;
            }
            else
            {
                ServerSideErrorPanel.Visible = true;
                //ServerSideErrorPanel.InnerText = SitecoreFieldsHelper.GetValue(source, Constants.ChildAgeRequiredErrorMessageFieldName, "Please select age of children");
                return isValid;
            }
        }

        private void ShowError(string errorMessage)
        {
            ServerSideErrorPanel.Visible = true;
            ServerSideErrorPanel.InnerText = errorMessage;
        }


        private bool ValidateSearchCriteriaChildBedSelection(Uri requestUrl, Item item)
        {
            int roomsCount;
            SearchKey searchKey;

            if (int.TryParse(ChildBedSelectionNoOfRoomsDropDownList.Text, out roomsCount))
            {
                searchKey = GetSearchKey(requestUrl, roomsCount);
            }
            else
            {
                return false;
            }

            foreach (RoomRequest room in searchKey.SelectedRooms)
            {

                if (room.ChildrenBeds.Count == 0)
                {
                    continue;
                }

                List<ChildBedRequest> LessThanTwoChildBeds = GetLessThanTwoChildrenBeds(room).ToList();

                List<ChildBedRequest> ThreeToFiveChildBedRequests = GetThreeToFiveBedRequest(room).ToList();

                int inParentsBedCountLessThanTwo = LessThanTwoChildBeds.Count(a => !a.IsExtraBed);


                if (LessThanTwoChildBeds.Count >= 1)
                {
                    //One child scenario 
                    //If child is aged 0-12 months, 1, 2: this child must sleep in the parent’s bed 
                    //If child is aged 3,4,5 : this child can sleep in either the parent’s bed or an extra bed in the room 
                    //If child is aged 6,7, 8, 9, 10, 11, and 12 : this child must sleep in the extra bed in the room 

                    ///If there is at least one child aged 0-2, must be on parents bed
                    if (inParentsBedCountLessThanTwo == 0)
                    {
                        ShowError(SitecoreFieldsHelper.GetValue(item, Constants.InvalidSearchCritiriaErrorMessageKey, "Invalid Search Criteria"));
                        return false;
                    }
                }

                int inParentsBedAllCount = inParentsBedCountLessThanTwo + ThreeToFiveChildBedRequests.Count(a => !a.IsExtraBed); ;

                ///Only one child can be in parents bed
                if (inParentsBedAllCount > 1)
                {
                    ShowError(SitecoreFieldsHelper.GetValue(item, Constants.InvalidSearchCritiriaErrorMessageKey, "Invalid Search Criteria"));
                    return false;
                }


                ///If older than five years, cannot be on parents bed
                if (!OlderThanFiveValidations(item, room))
                {
                    return false;
                }

            }

            return true;

        }

        private bool OlderThanFiveValidations(Item item, RoomRequest room)
        {
            int OlderThanFiveAndParentsBed = room.ChildrenBeds.Count(a =>
            {
                int childAge;

                if (a.IsExtraBed)
                {
                    return false;
                }

                ///if age is 0-12 months treat it as less than two years.
                if (a.Age.Equals(LanguageReader.GetText(Constants.ZeroToTwelveMonthsDictionaryKey, "0")))
                {
                    return false;
                }

                if (int.TryParse(a.Age, out childAge))
                {
                    return childAge > 5;
                }
                else
                    throw new AffinionException("Invalid Age");

            });

            if (OlderThanFiveAndParentsBed > 0)
            {
                ShowError(SitecoreFieldsHelper.GetValue(item, Constants.InvalidSearchCritiriaErrorMessageKey, "Invalid Search Criteria"));
                return false;
            }
            return true;
        }

        private static IEnumerable<ChildBedRequest> GetThreeToFiveBedRequest(RoomRequest room)
        {
            return room.ChildrenBeds.Where(a =>
            {
                int childAge;

                ///if age is 0-12 months treat it as less than two years.
                if (a.Age.Equals(LanguageReader.GetText(Constants.ZeroToTwelveMonthsDictionaryKey, "0")))
                {
                    return false;
                }

                if (int.TryParse(a.Age, out childAge))
                {
                    return childAge >= 3 && childAge <= 5;
                }
                else
                    throw new AffinionException("Invalid Age");
            });

        }

        private static IEnumerable<ChildBedRequest> GetLessThanTwoChildrenBeds(RoomRequest room)
        {
            return room.ChildrenBeds.Where(a =>
            {
                int childAge;

                ///if age is 0-12 months treat it as less than two years.
                if (a.Age.Equals(LanguageReader.GetText(Constants.ZeroToTwelveMonthsDictionaryKey, "0")))
                {
                    return true;
                }

                if (int.TryParse(a.Age, out childAge))
                {
                    return childAge <= 2;
                }
                else
                    throw new AffinionException("Invalid Age");

            });
        }


        /// <summary>
        /// validate checkbox values and show or hide the web controls in stock search box
        /// </summary>
        /// <param name="item"></param>
        /// <param name="checkBoxField"></param>
        /// <param name="defaultValueField"></param>
        /// <param name="control"></param>
        /// <param name="controlLabel"></param>
        private static string ValidateControlValue(Item item, string checkBoxField, string defaultValueField, WebControl control, Control controlLabel)
        {
            int controlValue = SitecoreFieldsHelper.GetInteger(item, checkBoxField, 0);
            string defaultValue = SitecoreFieldsHelper.GetValue(item, defaultValueField);
            if (controlValue == 1)
            {
                //control.Visible = true;
                //if (controlLabel != null)
                //{
                //    controlLabel.Visible = true;
                //}

                if (checkBoxField == "NoOfNights")
                {
                    return string.Empty;
                }
                else if (!string.IsNullOrEmpty(defaultValue))
                {
                    control.Enabled = false;
                }
            }

            return defaultValue;
        }

        private void SetFormAction(Item item, Uri requestUrl, StringBuilder parameters)
        {
            // set form action
            AnchorTagHelperData resultsPage = this.GetResultsPage(item);
            this.submitUrl = string.IsNullOrEmpty(resultsPage.Url) ? requestUrl.AbsolutePath : resultsPage.Url;
            this.submitUrl += parameters.ToString();
            Session["SearchKeyValues"] = submitUrl;

            Response.Redirect(this.submitUrl, true);
        }

        private void AddJavascriptVariables(Item item)
        {
            AddJavascriptVariable("ChildAgeRequiredErrorMessage42dc6be294574b13bc5d42f008087203", SitecoreFieldsHelper.GetValue(item, "ChildAgeRequiredErrorMessage", "Please select age of children"));

            AddJavascriptVariable("DropDownDefaultValue0ece8eb2eee54d44a57d45200d6edc5c", LanguageReader.GetText(Constants.DefaultDropDownTextDictionaryKey, "Select"));

            AddJavascriptVariable("ClientName", ItemHelper.RootItem.Name);

            AddJavascriptVariable("ClientId", ItemHelper.RootItem.ID);
        }

        private void BindSitecoreTexts()
        {
            Item conditionalMessageItem = this.GetItemFromRenderingParameterPath("Conditional Messages Link");
            string ageofChildrenRequiredMessageFieldName = "Age of Children Required Message";
            string numberofNightsRequiredMessageFieldName = "Number of Nights Required Message";
            string notValidCharacterMessageFieldName = "Not Valid Character Message";
            SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, ageofChildrenRequiredMessageFieldName, txtSelectChildAgeMessage);
            SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, numberofNightsRequiredMessageFieldName, numberOfNightsRequiredText);
            SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, notValidCharacterMessageFieldName, txtDestinationRegularText);
        }

        #endregion
    }
}