﻿using Affinion.LoyaltyBuild.Model.Provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Model.Product
{
    /// <summary>
    /// Bed bank product
    /// </summary>
    public class BedBankProduct : IProduct
    {
        /// <summary>
        /// Sku
        /// </summary>
        public string Sku { get; set; }

        /// <summary>
        /// Variant Sku
        /// </summary>
        public string VariantSku { get; set; }

        /// <summary>
        /// Product type
        /// </summary>
        public ProductType Type { get; set; }

        /// <summary>
        /// Ucommerce currency id
        /// </summary>
        public int CurrencyId { get; set; }

        // <summary>
        /// Gets or sets the Offer Price
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        ///PropertyRefernceId or Booking token for prebook Request 
        /// </summary>
        public string TokenForBooking { get; set; }

        /// <summary>
        /// margin percentage
        /// </summary>
        public decimal MarginPercentage { get; set; }

        /// <summary>
        /// margin Id
        /// </summary>
        public int MarginId { get; set; }

        /// <summary>
        /// margin amount
        /// </summary>
        public decimal MarginAmount { get; set; }

        /// <summary>
        /// final price after margin addition
        /// </summary>
        public decimal FinalPrice { get; set; }

        /// <summary>
        /// MealBasisID
        /// </summary>
        public string MealBasisID { get; set; }

        /// <summary>
        /// BedBanks Errata
        /// </summary>
        public List<BedBankErrata> Errata { get; set; }       

        /// <summary>
        /// ProviderId
        /// </summary>
        public string ProviderId { get; set; }

        /// <summary>
        /// BedBank Currency Id
        /// </summary>
        public string BBCurrencyId { get; set; }

        public string PropertyReferenceId { get; set; }
    }
}
