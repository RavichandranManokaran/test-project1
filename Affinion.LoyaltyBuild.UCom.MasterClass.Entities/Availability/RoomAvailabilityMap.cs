﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce;
using UCommerce.EntitiesV2.Maps;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Availability
{
    public class RoomAvailabilityMap : BaseClassMap<RoomAvailability>
    {
        public RoomAvailabilityMap()
        {
            //base.Cache.ReadWrite();
            //base.Cache.Region(Constants.Cache.Regions.MarketingFoundation);
            base.Table("LB_RoomAvailability");
            base.Id(x => x.RoomAvailibilityID).GeneratedBy.GuidNative();
            Map(x => x.Sku).Not.Nullable();
            Map(x => x.VariantSku).Not.Nullable();
            Map(x => x.NumberAllocated).Not.Nullable();
            Map(x => x.NumberBooked).Not.Nullable();
            Map(x => x.DateCounter).Not.Nullable();
            Map(x => x.CreatedBy).Nullable();
            Map(x => x.UpdateBy).Nullable();
            Map(x => x.DateCreated).Nullable();
            Map(x => x.DateLastUpdate).Nullable();
            Map(x => x.Price).Nullable();
            Map(x => x.PackagePrice).Nullable();
            Map(x => x.PriceBand).Nullable();

            Map(x => x.IsStopSellOn).Not.Nullable();
            Map(x => x.IsCloseOut).Not.Nullable();
        }
    }
}
