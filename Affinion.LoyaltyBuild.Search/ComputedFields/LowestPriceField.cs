﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SearchHelper.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Search
/// Description:           This field will generate and store the readable notation to indicate the lowest price of offers assigned to client
/// </summary>
namespace Affinion.LoyaltyBuild.Search.ComputedFields
{
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Search.Data;
    using Affinion.LoyaltyBuild.Search.Helpers;
    using Sitecore.ContentSearch;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// This field will generate and store the readable notation to indicate the lowest price of offers assigned to client
    /// </summary>
    public class LowestPriceField : ComputedField
    {
        /// <summary>
        /// Calculates the field value
        /// </summary>
        /// <param name="indexable">The indexable item</param>
        /// <returns>Returns the calculated value</returns>
        public override object ComputeFieldValue(IIndexable indexable)
        {
            try
            {
                Item supplier = ContentDatabase.GetItem(ID.Parse(indexable.Id));
                StringBuilder clientPrices = new StringBuilder();
                List<Item> clientsList = new List<Item>();
                Item clientsFolder = ContentDatabase.GetItemByKey("ClientSetupFolder");

                if (clientsFolder != null)
                {
                    clientsList = clientsFolder.Axes.GetDescendants().Where(i => i.TemplateName == "ClientDetails").ToList<Item>();
                }

                foreach (Item client in clientsList)
                {
                    bool isSupplierSelected = SitecoreFieldsHelper.MultiListContains(client, "AllowedSuppliers", supplier);

                    if (isSupplierSelected)
                    {
                        SearchKey searchTerm = new SearchKey();
                        searchTerm.CheckinDate = DateTime.Now;
                        searchTerm.CheckoutDate = DateTime.Now.AddDays(1);
                        searchTerm.Client = client;

                        SupplierData offersList = StoreHelper.GetProducts(supplier, client, searchTerm);

                        if (offersList != null)
                        {
                            clientPrices.Append(client.ID.ToString());
                            clientPrices.Append("=");
                            clientPrices.Append(offersList.LowestPrice.ToString());
                            clientPrices.Append(",");
                        }
                    }
                }

                return clientPrices.ToString();
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                return string.Empty;
            }
        }
    }
}
