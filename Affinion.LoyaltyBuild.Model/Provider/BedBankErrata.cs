﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Model.Provider
{
    /// <summary>
    /// Bed bank Errata Information
    /// </summary>
    [Serializable]
    [JsonObject(Title = "bbe")]
    public class BedBankErrata
    {
        /// <summary>
        /// Errata Subject
        /// </summary>
        [JsonProperty(PropertyName = "sub")]
        public string Subject { get; set; }

        /// <summary>
        /// Errata Description
        /// </summary>
        [JsonProperty(PropertyName = "desc")]
        public string Description { get; set; }
    }
}
