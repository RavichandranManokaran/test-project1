﻿CREATE TABLE [dbo].[uCommerce_SupplierFilter](
 [SupplierFilterId] [int] IDENTITY(1,1) NOT NULL,
 [SearchFilter] [varchar](max) NOT NULL,
 [CampaignItem] [int] NOT NULL,
 CONSTRAINT [PK_uCommerce_SupplierFilter] PRIMARY KEY CLUSTERED 
(
 [SupplierFilterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]