﻿using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Data;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Affinion.Loyaltybuild.BusinessLogic;
using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
using System.Data;
using System.Text;
using System.IO;
using Affinion.LoyaltyBuild.UCom.Common.Extension;
using System.Globalization;

namespace Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.Reports
{
    public partial class OverallRevenueByClientReport : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BindSitecoreText();
                
            if (!IsPostBack)
            {
                FillDropdown();
                //htmlReportGrid.Style.Add("display", "none");
            }
        }

        protected void ResultGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ResultGridView.PageIndex = e.NewPageIndex;
            BindGridData();

        }

        private void BindGridData()
        {
            DataSet ds = new DataSet();
            DateTime startdate = txtStartDate.Value.ParseFormatDateTime();
            DateTime endDate = txtEndDate.Value.ParseFormatDateTime();
            ds = uCommerceConnectionFactory.OverAllRevenueForClient(startdate, endDate, ddlBookingMethod.SelectedValue, ddlClientList.SelectedValue);
            // htmlReportGrid.Style.Add("display", "block");
            if (ds.Tables[0].Rows.Count != 0)
            {
                DataTable dtTable = ds.Tables[0];
                if (ds.Tables[0].Columns[2] != null)
                {
                    string value = string.Empty;
                    string providerName = string.Empty;

                    for (int i = 0; i < dtTable.Rows.Count; i++)
                    {
                        value = dtTable.Rows[i][2].ToString();
                        if (!string.IsNullOrEmpty(value))
                        {
                            var item = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(value));
                            if (item != null)
                            {
                                providerName = Sitecore.Context.Database.SelectItems(Constants.FilteredClientItems).Where(X => X.ID.ToString().Equals(value)).Select(y => y.DisplayName).FirstOrDefault().ToString();
                                if (!string.IsNullOrEmpty(providerName))
                                    dtTable.Rows[i][2] = providerName;
                            }
                        }
                    }
                }
                ResultGridView.DataSource = ds;
                ResultGridView.DataBind();
                ResultDiv.Visible = true;
                records.Visible = true;
                NoRecordFoundMsg.Visible = false;
                //scUnclaimedReceipts.Visible = true;
                records.Text = "Total of" + " " + ds.Tables[0].Rows.Count.ToString() + " " + "record(s) found.";
            }
            else
            {
                records.Visible = false;
                ResultDiv.Visible = false;
                NoRecordFoundMsg.Visible = true;
                NoRecordFoundMsg.Text = "No Records Found";
            }
        }
        /// <summary>
        /// Loading Client Name in dropdown
        /// </summary>
        private void FillDropdown()
        {

            Database contextDb = Sitecore.Context.Database;

            var ClientITems = new ListItemCollection();
            var items = contextDb.SelectItems(Constants.FilteredClientItems) != null ? contextDb.SelectItems(Constants.FilteredClientItems).ToList() : null;
            if (items != null)
            {
                foreach (var item in items)
                {
                    System.Web.UI.WebControls.ListItem liBox = new System.Web.UI.WebControls.ListItem();
                    liBox.Text = SitecoreFieldsHelper.GetValue(item, "Name");
                    liBox.Value = item.ID.ToString();
                    if (liBox != null)
                    {
                        ClientITems.Add(liBox);
                    }
                    //ClientITems.Add(SitecoreFieldsHelper.GetItemFieldValue(item, "Name"));

                }
                BindDropdown(ddlClientList, ClientITems, "Text", "Value");

                ListItem allClients = new ListItem("Select Client", string.Empty);
                ddlClientList.Items.Insert(0, allClients);

            }
        }

        /// <summary>
        /// Bind Drop down
        /// </summary>
        /// <param name="ddlControl"></param>
        /// <param name="dataSource"></param>
        /// <param name="text"></param>
        /// <param name="value"></param>
        private void BindDropdown(DropDownList ddlControl, object dataSource, string text, string value)
        {
            ddlControl.DataSource = dataSource;
            ddlControl.DataTextField = text;
            ddlControl.DataValueField = value;
            ddlControl.DataBind();
        }

        /// <summary>
        /// Click Event for Button Excel Report view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnExcelReport_ServerClick(object sender, EventArgs e)
        {
            ExportGridToExcel();

        }

        private void ExportGridToExcel()
        {
            DataSet ds = new DataSet();
            DateTime startdate = txtStartDate.Value.ParseFormatDateTime();
            DateTime endDate = txtEndDate.Value.ParseFormatDateTime();

            ds = uCommerceConnectionFactory.OverAllRevenueForClient(startdate, endDate, ddlBookingMethod.SelectedValue, ddlClientList.SelectedValue);
            if (ds.Tables[0].Rows.Count != 0)
            {
                Response.Clear();
                Response.Buffer = true;
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Charset = string.Empty;
                string FileName = CreateFileName();

                using (StringWriter strwritter = new StringWriter())
                {
                    HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);


                    //Temporary Grid
                    GridView tempGrid = ResultGridView;
                    tempGrid.AllowPaging = false;
                    tempGrid.CssClass = "table table-striped table-bordered";
                    tempGrid.AutoGenerateColumns = false;



                    //Hide select button column
                    tempGrid.DataSource = ds;
                    tempGrid.DataBind();

                    //Page.Header.Controls.Clear();
                    Page.Form.Controls.Clear();

                    // Page.Form.Controls.Add(totalCount);
                    //Item currentItem = Sitecore.Context.Item;
                    //SitecoreFieldsHelper.BindSitecoreText(currentItem, "NoOfRecords", noOfRecords);
                    //records.Text = ds.Tables[0].Rows.Count.ToString();
                    Page.Form.Controls.Add(tempGrid);

                    tempGrid.GridLines = GridLines.Both;
                    tempGrid.HeaderStyle.Font.Bold = true;
                    //ResultGridView.RenderControl(htmltextwrtter);
                    Page.Form.RenderControl(htmltextwrtter);
                    //System.Text.RegularExpressions.Regex rx = new System.Text.RegularExpressions.Regex(@".*<\s*body[^>]*>)|(<\s*/\s*body\s*\>.+");
                    //string str   = rx.Replace(strwritter.ToString(), "");

                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName + ".xls");
                    Response.Write(strwritter.ToString());
                    Response.End();

                }
            }
            else
            {
                NoRecordFoundMsg.Visible = true;
                NoRecordFoundMsg.Text = "No datas to bind";
            }



        }

        private string CreateFileName()
        {
            return "OverAllRevenueReport" + DateTime.Now.ToString();
        }



        /// <summary>
        /// Click Event for Button HTML Report view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnHtmlReport_ServerClick(object sender, EventArgs e)
        {
            BindGridData();
        }

        private void BindSitecoreText()
        {
            Item currentItem = Sitecore.Context.Item;
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Title", OverallRevenueText);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "DateFrom", StartDateText);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "DateTo", EndDateText);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Partner", ClientText);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Booking Method", BookingMethodText);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Export To Excel", ExcelReportText);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Export To Html", HtmlReportText);




        }




    }
}

