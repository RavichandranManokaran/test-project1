﻿using Affinion.Loyaltybuild.BusinessLogic.Helper;
using Affinion.LoyaltyBuild.Api.Booking.Data.Payment;
using Affinion.LoyaltyBuild.Api.Booking.Service;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.Model.Common;
using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic;
using Affinion.LoyaltyBuild.Web.ClientPortal.Helper;
using System;
using System.Web.UI;
using UCommerce.Infrastructure;
using service = Affinion.LoyaltyBuild.Api.Booking.Service;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    public partial class OfflinePortalDues : BaseSublayout
    {
        //private variables
        private service.IPaymentService _paymentService;

        protected ValidationResult ValidationResponse { get; private set; }

        public OfflinePortalDues()
        {
            _paymentService = ContainerFactory.Instance.GetInstance<service.IPaymentService>();
        }
        
        /// <summary>
        /// load method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void Page_Load(object sender, EventArgs e)
        {
            BindSiteoreText();
            if (!Page.IsPostBack)
            {
                int olid;
                int.TryParse(this.QueryString("olid"), out olid);
                orderlineid.Value = olid.ToString();
                IBookingService bookingservice = ObjectFactory.Instance.Resolve<IBookingService>();

                var booking = bookingservice.GetBooking(olid);
                var clientId = booking.ClientId;

                DrpLstCurrencyId.DataSource = _paymentService.GetCurrencyListDetails();
                DrpLstCurrencyId.DataTextField = "Name";
                DrpLstCurrencyId.DataValueField = "CurrencyID";
                DrpLstCurrencyId.DataBind();

                DrpLstPaymentMethod.DataSource = _paymentService.GetPaymentMethods(clientId);
                DrpLstPaymentMethod.DataTextField = "Name";
                DrpLstPaymentMethod.DataValueField = "Id";
                DrpLstPaymentMethod.DataBind();

                DrpLstPaymentReason.DataSource = _paymentService.GetPaymentReasons();
                DrpLstPaymentReason.DataTextField = "DueReason";
                DrpLstPaymentReason.DataValueField = "DueId";
                DrpLstPaymentReason.DataBind();

                int dueid;
                if (int.TryParse(this.QueryString("dueid"), out dueid))
                {
                    orderlinedueid.Value = dueid.ToString();
                    GetDueDetails(dueid);
                }
            }
        }

        /// <summary>
        /// add due event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

      
        protected void btnAddDue_Click(object sender, EventArgs e)
        {
            int olid;
            int.TryParse(this.QueryString("olid"), out olid);
            orderlineid.Value = olid.ToString();
            
            var due = new DueDetails()
            {
                OrderLineId = Convert.ToInt32(olid),
                CurrencyTypeId = Convert.ToInt32(DrpLstCurrencyId.SelectedValue),
                DueTypeId = Guid.Parse(DrpLstPaymentReason.SelectedValue),
                PaymentMethodId=Convert.ToInt32(DrpLstPaymentMethod.SelectedValue),
                Amount = Convert.ToDecimal(txtTotAmount.Text),
                CommissionAmount = Convert.ToDecimal(txtCommissionAmount.Text),
                ProviderAmount = Convert.ToDecimal(txtProviderAmount.Text),
                ProcessingFeeAmount=Convert.ToDecimal(txtProcessingFee.Text),
                Note = Convert.ToString(txtNote.Text),
             
                CreatedBy = SitecoreUserProfileHelper.GetItem(UserProfileKeys.CreatedBy)
            };
           
            //set due id for edit due

            int dueid;
            if (int.TryParse(orderlinedueid.Value, out dueid))
            {
                due.OrderLineDueId = dueid;
            }


            ValidationResponse = _paymentService.SaveDue(due);
            
            orderlinedueid.Value = ValidationResponse.Id.ToString();

            if (ValidationResponse.IsSuccess)
            {
                Response.Redirect(GetUrl("/paymenthistory?olid=" + QueryString("olid")));
            }
        }

        /// <summary>
        /// Get due details
        /// </summary>
        /// <param name="dueId"></param>
        public void GetDueDetails(int dueId)
        {
            DueDetails getDue = new DueDetails();
            getDue = _paymentService.GetDueDetail(dueId);

            if (getDue != null)
            {
                orderlinedueid.Value = getDue.OrderLineDueId.ToString();
                DrpLstCurrencyId.SelectedValue = getDue.CurrencyTypeId.ToString();
                DrpLstPaymentReason.SelectedValue = getDue.DueTypeId.ToString();
                DrpLstPaymentMethod.SelectedValue = getDue.PaymentMethodId.ToString();
                DrpLstCurrencyId.Enabled = false;
                DrpLstPaymentReason.Enabled = false;
                txtCommissionAmount.Text = getDue.CommissionAmount.ToString();
                txtTotAmount.Text = getDue.Amount.ToString();
                txtProviderAmount.Text = getDue.ProviderAmount.ToString();
                txtProcessingFee.Text = getDue.ProcessingFeeAmount.ToString();
                txtNote.Text = getDue.Note;
               
            }
            else
            {
                ValidationResponse = new  ValidationResult
                {
                    IsSuccess = false,
                    Message = "Invalid due details"
                };
                btnAddDue.Disabled = true;
            }
        }

        /// <summary>
        /// bind sitecore text 
        /// </summary>
        private void BindSiteoreText()
        {
            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "Title Label Text", titleTxt);
            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "Currency Label Text", currencyTxt);
            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "Payement Method Label Text", paymentMethodTxt);
            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "Mandatory Symbol", pmMandTxt);
            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "Payement Reason Label Text", paymentReasonTxt);
            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "Mandatory Symbol", payReasonMandTxt);
            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "Total Amount Label Text", totalAmountTxt);
            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "Relevant Commission Amount Label Text", commissionAmountTxt);
            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "Provider Amount Label Text", ProviderAmtTxt);
            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "Note Label Text", NoteTxt);
            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "Reset Button Text", btnResetTxt);
            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "Save Due Button Text", btnAddDueTxt);
            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "Cancel Button Text", btnCancelTxt);
            SitecoreFieldsHelper.BindSitecoreText(Sitecore.Context.Item, "Processing Fee Text", ProcessingFeeTxt);
        }


    }
}