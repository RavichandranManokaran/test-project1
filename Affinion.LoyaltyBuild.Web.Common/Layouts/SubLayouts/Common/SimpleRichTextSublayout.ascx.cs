﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SimpleRichTextSublayout.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.Common
/// Description:           Bind data to link block /// </summary>

namespace Affinion.LoyaltyBuild.Web.Common.Layouts.SubLayouts.Common
{
    #region Using Namespaces
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Sitecore.Data.Items;
    using System;
    #endregion

    public partial class SimpleRichTextSublayout : BaseSublayout
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Item item = this.GetDataSource();

                if (item != null)
                {
                    this.PlaceholderText.Visible = false;
                    this.Body.Item = item;                                       
                }
                else
                {
                    this.Body.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, ex.Message);
                throw;
            }
        }
    }
}