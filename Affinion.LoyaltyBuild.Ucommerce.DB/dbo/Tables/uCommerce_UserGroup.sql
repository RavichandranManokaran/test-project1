﻿CREATE TABLE [dbo].[uCommerce_UserGroup] (
    [UserGroupId] INT            IDENTITY (1, 1) NOT NULL,
    [ExternalId]  NVARCHAR (255) NOT NULL,
    PRIMARY KEY CLUSTERED ([UserGroupId] ASC)
);

