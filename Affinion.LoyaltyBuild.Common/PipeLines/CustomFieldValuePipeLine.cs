﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           CustomFieldValuePipeLine.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Set custom standard values by custom tokens at runtime
/// </summary>

#region Using Directives
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Sitecore.Pipelines.ExpandInitialFieldValue;
using System;
#endregion

namespace Affinion.LoyaltyBuild.Common.PipeLines
{
    public class CustomFieldValuePipeLine : ExpandInitialFieldValueProcessor
    {
        public override void Process(ExpandInitialFieldValueArgs args)
        {
            try
            {
                ///Set date field to a defined future date
                if (args.SourceField.Value.Contains("$activedate"))
                {
                    if (args.TargetItem != null && args.TargetItem.Parent != null && args.TargetItem.Children != null)
                    {
                        DateTime newDate = new DateTime();
                        ///Add 100years as the future date
                        newDate = DateTime.Now.AddYears(100);
                        ///Replace token with new date
                        args.Result = args.Result.Replace("$activedate", Sitecore.DateUtil.ToIsoDate(newDate));
                    }
                    else
                    {
                        ///Set today's date by replacing the token
                        args.Result = args.Result.Replace("$activedate", Sitecore.DateUtil.IsoNowDate);
                    }
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, this);
                throw;
            }
        }
    }
}
