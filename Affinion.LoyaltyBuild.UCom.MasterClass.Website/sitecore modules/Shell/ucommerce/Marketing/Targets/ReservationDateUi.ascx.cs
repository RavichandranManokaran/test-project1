﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.EntitiesV2;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using UCommerce.Presentation.Views.Marketing;
using UCommerce.Presentation.Web.Controls;
using UCommerce.Presentation.Web.Marketing;
using System.Text.RegularExpressions;
using System.Globalization;
using Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi;
using Affinion.LoyaltyBuild.UCom.Common.Extension;
using Affinion.LoyaltyBuild.Common.Instrumentation;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.ucommerce.Marketing.Targets
{
    public partial class ReservationDateUi : ViewEnabledControl<IEditCampaignItemView>, ITargetUi
    {
        //List<DataTypeEnum> datatypeList;

        protected void Page_Load(object sender, EventArgs e)
        {
            ReservationDateTarget dataSource = this.DataSource as ReservationDateTarget;
            this.DeleteButton.OnClientClick = string.Format("return confirm('Delete');");
            this.ReadOnlyMessage.Text = this.TargetToString(dataSource);
            this.txtFromDate.Text = dataSource.ReservationFromDate.ToShortDateString();
            this.txtToDate.Text = dataSource.ReservationToDate.ToShortDateString();
        }

        protected void SaveButton_Click(object sender, ImageClickEventArgs e)
        {
            if (Page.IsValid)
            {
                ReservationDateTarget dataSource = this.DataSource as ReservationDateTarget;
                dataSource.ReservationFromDate = txtFromDate.Text.ParseFormatDateTime();
                dataSource.ReservationToDate = txtToDate.Text.ParseFormatDateTime();
                dataSource.Save();
                this.ReadOnlyMessage.Text = this.TargetToString(dataSource);
                this.ToggleEditor();
            }
        }

        private void ToggleEditor()
        {
            this.NonEditModePlaceHolder.Visible = !this.NonEditModePlaceHolder.Visible;
            this.EditButton.Visible = !this.EditButton.Visible;
            this.EditModePlaceHolder.Visible = !this.EditModePlaceHolder.Visible;
            this.SaveButton.Visible = !this.SaveButton.Visible;

        }

        private string TargetToString(ReservationDateTarget ReservationDateTarget)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendFormat("Reservation Date ");
            stringBuilder.AppendFormat("From date {0} ", ReservationDateTarget.ReservationFromDate.ConvertDateToString());
            stringBuilder.AppendFormat("To date {0} ", ReservationDateTarget.ReservationToDate.ConvertDateToString());
            return stringBuilder.ToString();
        }



        protected void ValidateDate(object sender, ServerValidateEventArgs e)
        {
            string controlName = ((BaseValidator)sender).ControlToValidate;
            var control = FindControl(controlName) as TextBox;
            if (Regex.IsMatch(control.Text, @"((0[1-9]|1[0-2])\/((0|1)[0-9]|2[0-9]|3[0-1])\/((19|20)\d\d))$"))
            {
                DateTime dt;
                e.IsValid = DateTime.TryParseExact(e.Value, "MM/dd/yyyy", new CultureInfo("en-US"), DateTimeStyles.None, out dt);
            }
            else
            {
                e.IsValid = false;
            }
        }


        protected void DeleteButton_Click(object sender, ImageClickEventArgs e)
        {
            ReservationDateTarget dataSource = this.DataSource as ReservationDateTarget;
            if (dataSource != null)
            {
                dataSource.Delete();
            }
            this.Page.Response.Redirect(this.Page.Request.Url.ToString(), true);
        }

        protected void EditButton_Click(object sender, ImageClickEventArgs e)
        {
            this.ToggleEditor();
        }


        public bool EnabledForApply
        {
            get;
            set;
        }

        public bool EnabledForDisplay
        {
            get;
            set;
        }

        public object DataSource
        {
            get;
            set;

        }

        public object New()
        {
            var ReservationDateTarget = new ReservationDateTarget();
            try
            {
                ReservationDateTarget.CampaignItem = View.CampaignItem;
                ReservationDateTarget.EnabledForApply = true;
                ReservationDateTarget.ReservationFromDate = DateTime.Now;
                ReservationDateTarget.ReservationToDate = DateTime.Now;
                ReservationDateTarget.Save();
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.MasterClassBusinessLogic, ex, this);
                throw;
            }
            this.DataSource = ReservationDateTarget;
            return ReservationDateTarget;
        }
    }
}