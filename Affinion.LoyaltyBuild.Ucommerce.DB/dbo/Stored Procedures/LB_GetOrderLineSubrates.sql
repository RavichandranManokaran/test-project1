﻿--CREATE PROCEDURE [dbo].[LB_GetOrderLineSubrates] 
--(@orderLineId int)
--AS
--BEGIN
--	select SubrateItemCode, Amount
--	from LB_OrderLineSubrates UOL
--	where uol.OrderLineID = @orderLineId
--END

CREATE PROCEDURE [dbo].[LB_GetOrderLineSubrates] 
				(@orderLineId int)
		AS
		BEGIN
			Select _LBCommisionFee [CommissionFee], _CancellationFee [CancellationFee], _CreditCardProcessingFee [CreditCardProcessingFee] , _TransferFee [TransferFee],OrderLineId [OrderlineId]
			from
			(
				Select OrderId, OrderLineId, [KEY], [Value]
				from uCommerce_OrderProperty where OrderLineId=@orderLineId
			) Src
			pivot
			(
				max([Value])
				for [Key] in (_LBCommisionFee, _CancellationFee, _CreditCardProcessingFee , _TransferFee)
			) Piv;
		END
