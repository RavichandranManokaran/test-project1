﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           BeginingOfSessionNotificationRule.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Code is used to return rule conditions related results.
/// </summary>


#region Using Directives
using Affinion.LoyaltyBuild.Common.Instrumentation;
using System;
using System.Web;
#endregion

namespace Affinion.LoyaltyBuild.Common.Rules
{
    public class BeginingOfSessionNotificationRule : INotificfationRule
    {
        //Get the current HttpContext
        HttpContext context = HttpContext.Current;

        #region Public Methods

        /// <summary>
        ///  This method is implemented from INotification rule. Processes the conditions for the BeginingOfSessionNotificationRule.
        /// </summary>
        /// <returns>rule result</returns>
        public bool ExecuteDisplayRule()
        {
            try
            {
                if (context.Session["BeginOfSessionNotification"] == null)
                {
                    DisplayNotification();
                    return true;
                }
                else
                {

                    return false;
                }
            }
            catch (Exception ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, this);
                throw;
            }
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Assign the current system date time to the session.
        /// </summary>
        private void DisplayNotification()
        {
            DateTime currentDateTime = System.DateTime.Now;
            context.Session["BeginOfSessionNotification"] = currentDateTime;
        }
        #endregion
    }
}
