﻿CREATE TABLE [dbo].[LB_HotelBreakAward](
	[HotelBreakAwardId] [int] NOT NULL,
	[DiscountAmount] [decimal](18, 2) NOT NULL,
	[IsPercentage] [bit] NOT NULL,
	[OnOrderLine] [bit] NOT NULL,
 CONSTRAINT [PK_uCommerce_HotelBreakAward] PRIMARY KEY CLUSTERED 
(
	[HotelBreakAwardId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[LB_HotelBreakAward]  WITH CHECK ADD  CONSTRAINT [FK_uCommerce_HotelBreakAward_uCommerce_Award] FOREIGN KEY([HotelBreakAwardId])
REFERENCES [dbo].[uCommerce_Award] ([AwardId])
GO

ALTER TABLE [dbo].[LB_HotelBreakAward] CHECK CONSTRAINT [FK_uCommerce_HotelBreakAward_uCommerce_Award]
GO

