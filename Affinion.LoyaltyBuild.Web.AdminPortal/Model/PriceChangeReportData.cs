﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Affinion.LoyaltyBuild.AdminPortal.Model
{
    public class PriceChangeReportData
    {
        public virtual string RoomAvailibilityID { get; set; }
        public virtual string Sku { get; set; }
        public virtual string VariantSku { get; set; }
        public virtual int NumberAllocated { get; set; }
        public virtual int NumberBooked { get; set; }
        public virtual int DateCounter { get; set; }
        public virtual string CreatedBy { get; set; }
        public virtual string UpdateBy { get; set; }
        public virtual DateTime DateCreated { get; set; }
        public virtual DateTime DateLastUpdate { get; set; }
        public virtual bool IsStopSellOn { get; set; }
        public virtual bool IsCloseOut { get; set; }
        public virtual decimal Price { get; set; }
        public virtual decimal PackagePrice { get; set; }
        public virtual string PriceBand { get; set; }
        public virtual string ProviderName { get; set; }
        public virtual string Country { get; set; }
        public virtual string Location { get; set; }
        public virtual string MapLocation { get; set; }
        public virtual string Theme { get; set; }
        public virtual string RoomType { get; set; }
        public string PriceBandFrom { get; set; }
        public string PriceBandTo { get; set; }
        public string PriceFrom { get; set; }
        public string PriceTo { get; set; }  
    }
}