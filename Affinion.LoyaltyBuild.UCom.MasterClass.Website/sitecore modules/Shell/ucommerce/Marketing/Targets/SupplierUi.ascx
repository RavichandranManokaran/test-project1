﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SupplierUi.ascx.cs" Inherits="Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.ucommerce.Marketing.Targets.SupplierUi" %>
<table cellpadding="0" cellspacing="0" style="width: 100%;">
    <tr>
        <td>
            <asp:PlaceHolder runat="server" ID="NoCategorySelectedPlaceHolder">No Supplier Subscibe
            </asp:PlaceHolder>
            
            <asp:PlaceHolder runat="server" ID="CategoryInfoPlaceHolder" Visible="true">
            
                    <asp:ListView ID="lvSupplier" runat="server"
                    OnItemDeleting="lvSupplier_ItemDeleting">

                    <LayoutTemplate>

                        <table id="Table1" runat="server">

                            <tr id="Tr1" runat="server">

                                <td id="Td1" runat="server">

                                    <table id="itemPlaceholderContainer" runat="server" border="0" style="">

                                        <tr id="Tr2" runat="server" style="">
                                            <th id="Th1" runat="server"></th>
                                            <th id="Th3" runat="server">Supplier Name</th>
                                        </tr>
                                        <tr id="itemPlaceholder" runat="server">
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr id="Tr3" runat="server">
                                <td id="Td2" runat="server" style=""></td>
                            </tr>

                        </table>

                    </LayoutTemplate>

                    <ItemTemplate>

                        <tr style="">

                            <td>
                                <asp:ImageButton ID="DeleteButton" runat="server" ImageUrl="../../Images/ui/cross.png" meta:resourcekey="Delete" CommandName="Delete" />
                                <br />

                         
                            </td>

                            <td>
                                <asp:Label ID="SupplierInviteId" Visible="false" runat="server" Text='<%# Eval("SupplierInviteId") %>' />
                                <asp:Label ID="SupplierName" runat="server" Text='<%# Eval("Name") %>' />

                            </td>
                        </tr>

                    </ItemTemplate>

                </asp:ListView>
                <%--<asp:Localize ID="NameLabel" runat="server" meta:resourcekey="Name" />:
                <asp:Label runat="server" ID="CategoryNameLabel" /><br />--%>
            </asp:PlaceHolder>
        </td>
        <td style="width: 50px; text-align: right; vertical-align: top;">
            <asp:ImageButton ID="DeleteButton" runat="server" ImageUrl="../../Images/ui/cross.png" meta:resourcekey="Delete" OnClick="DeleteButton_Click" />

        </td>
       <%-- <td style="width: 50px; text-align: right; vertical-align: top;">
            <asp:ImageButton ID="SelectCategoryButton" runat="server" ImageUrl="../../Images/ui/add.png"  />
            
        </td>--%>
    </tr>
</table>
