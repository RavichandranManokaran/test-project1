﻿namespace Affinion.LoyaltyBuild.BedBanks.Troika.Model
{
    public class FacilityMapping
    {
        public int FeatureID { get; set; }
        public int FacilityID { get; set; }
        public string FeatureName { get; set; }
    }
}
