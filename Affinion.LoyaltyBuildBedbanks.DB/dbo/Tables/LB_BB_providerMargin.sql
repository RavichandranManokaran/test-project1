
/****** Object:  Table [dbo].[LB_BB_providerMargin]    Script Date: 7/19/2016 8:02:39 PM ******/

CREATE TABLE [dbo].[LB_BB_providerMargin](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[_ProviderID] [int] NOT NULL,
	[DateFrom] [datetime] NULL,
	[DateTo] [datetime] NULL,
	[marginPercentage] [decimal](18, 0) NULL,
	[isactive] [int] NULL,
	[createdBy] [int] NULL,
	[dateCreated] [datetime] NULL,
 CONSTRAINT [PK__providerMargin] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[LB_BB_providerMargin] ADD  DEFAULT ((1)) FOR [isactive]
GO

ALTER TABLE [dbo].[LB_BB_providerMargin] ADD  DEFAULT ((1)) FOR [createdBy]
GO

ALTER TABLE [dbo].[LB_BB_providerMargin] ADD  DEFAULT (getdate()) FOR [dateCreated]
GO

