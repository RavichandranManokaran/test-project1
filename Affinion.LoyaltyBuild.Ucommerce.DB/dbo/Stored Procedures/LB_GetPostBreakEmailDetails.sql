﻿Create PROCEDURE [dbo].[LB_GetPostBreakEmailDetails] ( @ClientId varchar(max))
AS
       SELECT BI.CheckOutDate, OA.EmailAddress , OA.FirstName, prod.Name
       FROM [dbo].uCommerce_OrderLine OL 
       INNER JOIN [dbo].uCommerce_PurchaseOrder P ON OL.OrderId=P.OrderId
       INNER JOIN [dbo].uCommerce_OrderAddress OA ON OA.OrderId=OL.OrderId
       INNER JOIN [dbo].LB_BookingInfo BI  ON OL.OrderLineId = BI.OrderLineId 
       INNER JOIN [dbo].uCommerce_product prod on prod.Sku = OL.Sku and prod.VariantSku is null 
       INNER JOIN (
                           select CustomerId ,_ClientId [ClientId]
                 from
                           (
                 select CustomerId, [key],[value]
                    from LB_CustomerProperty
                 )aa
                 pivot
                 (
                    max([value])
                           for [key] in ( _ClientId)
                           
                 )piv 
              )CP ON CP.CustomerId=P.CustomerId
       WHERE  BI.StatusId=6 and P.OrderStatusId=6 and CP.ClientId=@ClientId

