﻿//<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           RealexPaymentsSublayout.aspx.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal
/// Description:           Host iframe to load realex payment gateway
/// </summary>

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal
{
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.Loyaltybuild.BusinessLogic.Helper;
    using System;
    using System.Web.UI;
    using UCommerce.Api;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using System.Web;
    using UCommerce.EntitiesV2;
    using System.Globalization;
    using UCommerce.Transactions.Payments;
    using UCommerce;
    using System.Collections.Generic;

    public partial class RealexPaymentsSublayout : BaseSublayout
    {
        private void Page_Load(object sender, EventArgs e)
        {
            var fromUrl = Request.UrlReferrer; ///Get the from URL

            if (!Sitecore.Context.PageMode.IsPageEditor && (fromUrl == null || !string.Equals(fromUrl.LocalPath.ToLower(), "/basket")))
            {
                Response.Redirect("/basket");
            }
        }
    }
}