﻿/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/
namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    /// <summary>
    /// Entity to hold customer related information
    /// </summary>
    public class Customer
    {
        /// <summary>
        /// Customer id
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// First name of the customer
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Last name of the customer
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Customer Address
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Customer Phone Number
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Customer Email Address
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Customer ClientId
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// Customer AddressLine1
        /// </summary>
        public string AddressLine1 { get; set; }

        /// <summary>
        /// Customer AddressLine1
        /// </summary>
        public string AddressLine2 { get; set; }

        /// <summary>
        /// Customer BookingReferenceId
        /// </summary>
        public string BookingReferenceId { get; set; }

        /// <summary>
        /// Customer status
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Country Id
        /// </summary>
        public int CountryId { get; set; }

        /// <summary>
        /// Location Id
        /// </summary>
        public int LocationId { get; set; }

        /// <summary>
        /// Get Customer Specific Language
        /// </summary>
        public string LanguageName { get; set; }

    }
}
