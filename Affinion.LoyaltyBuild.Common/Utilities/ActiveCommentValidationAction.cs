﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           ActiveCommentValidationAction.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Used to item icon with validate workflow Active 
/// </summary>
#region Directives
using Sitecore.Data.Items;
using Affinion.LoyaltyBuild.Common.PipeLines;
#endregion
namespace Affinion.LoyaltyBuild.Common.Utilities
{
    public class ActiveCommentValidationAction
    {
        #region public method
        /// <summary>
        /// Set item icon as Active
        /// </summary>
        /// <param name="args"></param>
        public void Process(Sitecore.Workflows.Simple.WorkflowPipelineArgs args)
        {
            Item item = args.DataItem;
            using (new Sitecore.Security.Accounts.UserSwitcher(ItemHelper.ContentEditingUser))
            {
                item.Editing.BeginEdit();
                item.Fields["IsActive"].Value = "1";
                item.Fields["__icon"].Value = ItemHelper.UnapprovedIcon["__Icon"];
                if (item.Template.ID.ToString() == "{A7235C94-2EF5-4A3B-87DB-0D4DCF10B4DB}")
                {
                    item.Fields["StatusNote"].Value = string.Empty;
                }
                ItemHelper.ActivateSupplierType(item, "{3E85F816-1D87-4FFA-8261-820C92266305}", "IsActive");
                item.Editing.EndEdit();
            }
        }
        #endregion
    }
}
