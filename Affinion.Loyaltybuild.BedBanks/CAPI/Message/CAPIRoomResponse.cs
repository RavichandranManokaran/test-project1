﻿using System.Collections.Generic;
using Affinion.Loyaltybuild.BedBanks.CAPI.Model;

namespace Affinion.Loyaltybuild.BedBanks.CAPI.Message
{
    /// <summary>
    /// This is a class to hold the search Request Parameters.
    /// It will be passed to the API controller to perform the actual request.
    /// </summary>
    public class CapiRoomResponse
    {
        public List<Room> RoomsList;

        public CapiRoomResponse(List<Room> pRoomslist)
        {
            RoomsList = pRoomslist;
        }
    }
}