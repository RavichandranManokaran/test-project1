﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.BedBanks
{
    public class Constants
    {
        public static readonly string SearchRequest = "Search Request";
        public static readonly string PrebookRequest = "Pre book Request";
        public static readonly string BookRequest = "Book Request";
        public static readonly string CancelRequest = "Cancel Request";
        public static readonly string PrecancelRequest = "Pre Cancel Request";
        public static readonly string PropertyDetailsRequest = "Property Details Request";

        public static readonly string ExceptionMessage = "ExceptionMessage";
        public static readonly string ExceptionFlow = "ExceptionFlow";
    }
}
