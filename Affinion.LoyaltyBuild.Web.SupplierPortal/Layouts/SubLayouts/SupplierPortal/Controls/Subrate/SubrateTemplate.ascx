﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SubrateTemplate.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Subrate.SubrateTemplate" %>
<asp:UpdatePanel ID="updSubrate" runat="server">
    <ContentTemplate>
        <div>
            <div class="col-md-12">
                <asp:Label runat="server" ID="lblMessage" CssClass="alert-success"></asp:Label>
                <asp:Label runat="server" ID="lblError" CssClass="alert-danger"></asp:Label>
            </div>
            <div class="rate_tab">
                <div class="cl_rate">
                    <h3 class="rate_title">Templates</h3>

                    <asp:GridView ID="gvSubrateTemplates" runat="server" AutoGenerateColumns="false" CssClass="cl_tabl" OnRowCommand="gvSubrateTemplates_RowCommand">
                        <Columns>
                            <asp:BoundField ReadOnly="true" HeaderText="TemplateName" DataField="TemplateName" SortExpression="ItemName" />
                            <asp:TemplateField HeaderText="Edit">
                                <ItemTemplate>
                                    <asp:Button runat="server" ID="btnEdit" CausesValidation="false" CommandName="edittemplate" CommandArgument='<%# Eval("SubrateTemplateCode") + "," + Eval("TemplateName")  %>' CssClass="btn btn-info" Text="Edit"></asp:Button>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Delete">
                                <ItemTemplate>
                                    <asp:Button runat="server" ID="btnDelete" CausesValidation="false" CommandName="deletetemplate" CommandArgument='<%# Eval("SubrateTemplateID") %>' CssClass="btn btn-danger" Text="Delete"></asp:Button>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <div class="ofr_btns">
                        <asp:Button runat="server" CssClass="btn_save" ID="btnAddNew" OnClick="btnAddNew_Click" Text="Add New Template" />
                    </div>
                </div>
                <div class="sub_rate">
                    <h3 class="rate_title">Subrate Settings</h3>
                    <div class="subrate-pad-25">
                        <h1>Template Name:</h1>
                        <asp:TextBox CssClass="vat_value" runat="server" ID="txtItemName"></asp:TextBox>

                        <asp:RequiredFieldValidator runat="server" ID="reqItemName" ControlToValidate="txtItemName" CssClass="alert-danger" ErrorMessage="Please enter template name!" />
                        <asp:HiddenField runat="server" ID="ItemID" />
                    </div>
                    <asp:Repeater ID="rptSubrates" runat="server" OnItemDataBound="rptSubrates_ItemDataBound">

                        <HeaderTemplate>
                            <table class="cl_tabl">
                                <thead>
                                    <tr>
                                        <th>SubrateItem</th>
                                        <th>Code</th>
                                        <th>Ex.Vat</th>
                                        <th>VAT%</th>
                                        
                                        <th>VAT Amount</th>
                                        <th>Total Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <%#Eval("ItemName")%>                                       
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblSRCode" Text='<%#Eval("SubrateItemCode")%>'></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox runat="server" CssClass="vat_value" ID="txtAmount" onkeyup="ReCalculateVat(this)" onkeypress="return isNumberKey(event)" MaxLength="3"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:DropDownList CssClass="vat_value" runat="server" ID="ddlVatPercentage" onchange="ReCalculateVat(this)"></asp:DropDownList>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblVatAmount" Text='<%#Eval("VATAmount")%>'></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblTotalAmount" Text='<%#Eval("TotalAmount")%>'></asp:Label>
                                </td>

                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                                 </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <div class="ofr_btns">
                        <asp:Button runat="server" CssClass="btn_save" ID="btnSave" OnClick="btnSave_Click" Text="Save Template" />
                        <asp:Button runat="server" CausesValidation="false" CssClass="subrte_cancl" ID="btnCancle" OnClick="btnCancle_Click" Text="Cancel" />
                    </div>
                </div>
            </div>
        </div>
   </ContentTemplate>
</asp:UpdatePanel>
<script>
    function ReCalculateVat(obj) {
        var controlId = obj.id;
        if (controlId.indexOf("ddlVatPercentage") > -1) {
            controlId = controlId.replace("ddlVatPercentage", "##ChangeID##")
        }
        else if (controlId.indexOf("txtAmount") > -1) {
            controlId = controlId.replace("txtAmount", "##ChangeID##")
        }
        var vat = $("#" + controlId.replace("##ChangeID##", "ddlVatPercentage")).val();
        var price = $("#" + controlId.replace("##ChangeID##", "txtAmount")).val();

        var vatAmount = roundToTwo(((parseFloat(price) * parseFloat(vat)) / 100));
        var totalAmount = roundToTwo(parseFloat(price) + vatAmount);
        $("#" + controlId.replace("##ChangeID##", "lblVatAmount")).html(isNaN(vatAmount) ? "0" : vatAmount)
        $("#" + controlId.replace("##ChangeID##", "lblTotalAmount")).html(isNaN(totalAmount) ? "0" : totalAmount)

    }
    function roundToTwo(num) {
        return +(Math.round(num + "e+2") + "e-2");
    }
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
</script>
