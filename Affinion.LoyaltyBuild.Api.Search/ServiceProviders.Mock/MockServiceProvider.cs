﻿using Affinion.LoyaltyBuild.Api.Search.Data;
using Affinion.LoyaltyBuild.Model.Product;
using Affinion.LoyaltyBuild.Model.Provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Api.Search.ServiceProviders.Mock
{
    public class MockServiceProvider : IServiceProvider
    {
        public Data.ProviderCollection SearchProviders(IList<Filters.ISearchFilter> filters)
        {
            return new Data.ProviderCollection(Model.Provider.ProviderType.Mixed) {
                new SearchResult{
                    ProviderType = ProviderType.LoyaltyBuild,
                    Products = new List<IProduct>{
                        new Room {
                            Name = "My Room1",
                            Sku = "sku1",
                            VariantSku = "sku11",
                            Type =ProductType.Room
                        },
                        new Room {
                            Name = "My Room1",
                            Sku = "sku1",
                            VariantSku = "sku12",
                            Type =ProductType.Room
                        }
                    }
                },
                new SearchResult{
                    ProviderType = ProviderType.LoyaltyBuild,
                    Products = new List<IProduct>{
                        new Room {
                            Name = "My Room1",
                            Sku = "sku1",
                            VariantSku = "sku11",
                            Type =ProductType.Room
                        },
                        new Room {
                            Name = "My Room1",
                            Sku = "sku1",
                            VariantSku = "sku12",
                            Type =ProductType.Room
                        }
                    }
                },
                new SearchResult{
                    ProviderType = ProviderType.BedBanks,
                    Products = new List<IProduct>
                    {
                        new BedBankProduct
                        {
                            Name = "Bed banks hotel",
                            Sku = "bbsku",
                            VariantSku = "bbvsku",
                            Type= ProductType.BedBanks
                        }
                    }
                },
                new SearchResult{
                    ProviderType = ProviderType.BedBanks,
                    Products = new List<IProduct>
                    {
                        new BedBankProduct
                        {
                            Name = "Bed banks hote2",
                            Sku = "bbsku1",
                            VariantSku = "bbvsku1",
                            Type= ProductType.BedBanks
                        }
                    }
                }

            };
        }

        public Task<Data.ProviderCollection> SearchProvidersAsync(IList<Filters.ISearchFilter> filters)
        {
            throw new NotImplementedException();
        }

        public Model.Provider.IProvider GetProviderDetails(Model.Provider.IBaseProvider prodiverInfo)
        {
            if(prodiverInfo.ProviderType == ProviderType.BedBanks)
                return new BedBankHotel {
                    Name = "Bed banks hotel",
                    ProviderType = ProviderType.BedBanks
                };
            else
            return new Hotel
                {
                    AddressLine1 = "add1",
                    AddressLine2 ="add2",
                    Country ="india",
                    IntroductionImage = "http://10721-presscdn-0-80.pagely.netdna-cdn.com/wp-content/uploads/2008/08/winding-path.jpg",
                    Name ="Hotel 1",
                    StarRanking = 3,
                    ProviderType=ProviderType.LoyaltyBuild
                };
        }

        public IProvider GetProviderWithProduct(ProviderSearch provider)
        {
            return null;
        }
    }
}
