﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.EntitiesV2;
using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model.Payment;
using service = Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Payment;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Subrate;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.Web.ClientPortal.Helper;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    public partial class OfflinePortalPayments : System.Web.UI.UserControl
    {
        private service.IPaymentService _paymentService;
        public int orderLineId = 0;
        public int? currencyTypeID;
        public decimal refundAmount = 0;
        public bool IsCardPanelVisible = false;

        protected void Page_Load(object sender, EventArgs e)
        {

            divTotalAmt.Visible = false;
            cardPanelSelectDiv.Visible = false;
            _paymentService = ContainerFactory.Instance.GetInstance<service.IPaymentService>();
            if (!IsPostBack)
            {
                refundbycheque.Visible = true;
                refundbycard.Visible = false;
                ddlPaymentMethod.DataSource = _paymentService.GetPaymentMethods();
                ddlPaymentMethod.DataTextField = "Name";
                ddlPaymentMethod.DataValueField = "Id";
                ddlPaymentMethod.DataBind();
            }
            int.TryParse(Request.QueryString["olid"], out orderLineId);
            PopulateDefaultValues();
            ChangePaymentMethodDropdown();
        }

        public void PopulateDefaultValues()
        {

            var tmpStrCurrencyTypeID = Request.QueryString["cid"];
            if (tmpStrCurrencyTypeID != null)
            {
                currencyTypeID = Convert.ToInt32(Request.QueryString["cid"].ToString());
            }
            if (!IsPostBack)
            {
                //_paymentService = new service.PaymentService();
                List<Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model.CurrencyDetail> currencyList = new List<Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model.CurrencyDetail>();
                currencyList = _paymentService.GetCurrencyListDetails();
                ddlCurrency.DataSource = currencyList;
                ddlCurrency.DataTextField = "Name";
                ddlCurrency.DataValueField = "CurrencyID";
                ddlCurrency.DataBind();
            }

            List<TotalDueItem> tmpDueItem = new List<TotalDueItem>();
            tmpDueItem = _paymentService.GetDuesByOrderlineID(orderLineId, currencyTypeID);
            if (tmpDueItem != null)
            {
                if (tmpDueItem.Count > 0)
                {
                    refundAmount = tmpDueItem[0].Amount;
                    ChequeTotalAmountText.Text = Convert.ToString(refundAmount);
                    CreditCardTotalAmountTxt.Text = Convert.ToString(refundAmount);
                    currencyTypeID = tmpDueItem[0].CurrencyTypeId;
                }
            }

            ListItem listItem = ddlCurrency.Items.FindByValue(Convert.ToString(currencyTypeID));
            if (listItem != null)
            {
                ddlCurrency.ClearSelection();
                listItem.Selected = true;
            }
            ddlCurrency.Enabled = false;            
        }

        protected void BtnCreditCardRefund_Click(object sender, EventArgs e)
        {

            decimal TotalAmount = Convert.ToDecimal(CreditCardTotalAmountTxt.Text);
            decimal sumAmount = 0;
            if (ProviderAmountText.Text != string.Empty)
            {
                sumAmount = sumAmount + Convert.ToDecimal(ProviderAmountText.Text);
            }
            if (CommissionAmountText.Text != string.Empty)
            {
                sumAmount = sumAmount + Convert.ToDecimal(CommissionAmountText.Text);
            }
            if (ProcessingFeeAmountText.Text != string.Empty)
            {
                sumAmount = sumAmount + Convert.ToDecimal(ProcessingFeeAmountText.Text);
            }
            if (sumAmount == TotalAmount)
            {
                ProcessPaymentRefund();
            }
            else
            {
                divTotalAmt.Visible = true;
            }
        }
        protected void BtnCreditCardReset_Click(object sender, EventArgs e)
        {
            ProviderAmountText.Text = string.Empty;
            CommissionAmountText.Text = string.Empty;
            ProcessingFeeAmountText.Text = string.Empty;
        }
        protected void BtnChequeRefund_Click(object sender, EventArgs e)
        {
            ProcessPaymentRefund();
        }

        protected void ddlPaymentMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangePaymentMethodDropdown();
        }

        public void ProcessPaymentRefund()
        {
            
            string orderGuid = _paymentService.GetOrderGuidByOrderlineId(orderLineId);
            CardPaymentDetail pymtdetails = new CardPaymentDetail();
            pymtdetails.OrderLineID = orderLineId;
            pymtdetails.CurrencyTypeId = currencyTypeID.GetValueOrDefault();
            pymtdetails.Amount = refundAmount;
            pymtdetails.ProviderAmount = 0;
            pymtdetails.CommissionAmount = 0;
            pymtdetails.ProcessingFee = 0;
            if (ProviderAmountText.Text != string.Empty)
            {
                pymtdetails.ProviderAmount = Convert.ToDecimal(ProviderAmountText.Text);
            }
            if (CommissionAmountText.Text != string.Empty)
            {
                pymtdetails.CommissionAmount = Convert.ToDecimal(CommissionAmountText.Text);
            }
            if (ProcessingFeeAmountText.Text != string.Empty)
            {
                pymtdetails.ProcessingFee = Convert.ToDecimal(ProcessingFeeAmountText.Text);
            }
            pymtdetails.PaymentMethodID = Convert.ToInt32(ddlPaymentMethod.SelectedValue);
            //pymtdetails.CreatedBy = "test";
            pymtdetails.CreatedBy = SitecoreUserProfileHelper.GetItem(UserProfileKeys.CreatedBy);
            CheckIsCardPanelVisible(orderGuid, pymtdetails);
        }

        private void CheckIsCardPanelVisible(string orderGuid, CardPaymentDetail pymtdetails)
        {

            if (IsCardPanelVisible)
            {
                if (PaymentGridView.Rows.Count == 0)
                {
                    PaymentProcessing(pymtdetails, orderGuid);
                }

                else if (PaymentGridView.Rows.Count > 0 && this.HiddenFieldPaymentID.Value != string.Empty)
                {
                    PaymentProcessing(pymtdetails, this.HiddenFieldPaymentID.Value);
                }
                else if (PaymentGridView.Rows.Count > 0 && this.HiddenFieldPaymentID.Value == string.Empty)
                {
                    cardPanelSelectDiv.Visible = true;
                }
            }
            else
            {
                PaymentProcessing(pymtdetails, orderGuid);
            }
        }


        public void PaymentProcessing(CardPaymentDetail pymtdetails, string realexIDstring)
        {
            pymtdetails.ReferenceID = realexIDstring;
            ValidationResponse result = new ValidationResponse();
            result = _paymentService.AddPayment(pymtdetails);
            if (result.IsSuccess)
            {
                divErrorMsg.Visible = false;
                Response.Redirect("/PaymentHistory?olid=" + orderLineId);
            }
            else
            {
                divErrorMsg.Visible = true;
            }
        }
        protected void PaymentGridView_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                // Get the currently selected row using the SelectedRow property.
                GridViewRow row = PaymentGridView.SelectedRow;


                var rowCells = row.Cells;

                if (rowCells != null)
                {

                    this.HiddenFieldPaymentID.Value = rowCells[2].Text;
                    this.HiddenPaymentAmount.Value = rowCells[4].Text;
                }
            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw ex;
            }

        }
        public void ChangePaymentMethodDropdown()
        {
           
            List<RefundableItem> hst = new List<RefundableItem>();
            hst = _paymentService.GetAvailablePaymentsForRefund(orderLineId, Convert.ToInt32(ddlPaymentMethod.SelectedValue), Convert.ToInt32(currencyTypeID));
            PaymentGridView.DataSource = hst;
            PaymentGridView.DataBind();

            if (ddlPaymentMethod.SelectedItem.Text.ToLower().Contains("account") || ddlPaymentMethod.SelectedItem.Text.ToLower().Contains("cheque"))
            {
                refundbycheque.Visible = true;
                IsCardPanelVisible = false;
                refundbycard.Visible = false;
            }
            else
            {
                IsCardPanelVisible = true;
                refundbycard.Visible = true;
                refundbycheque.Visible = false;
            }
        }

        protected void DropDownListCommissionAmount_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownListCommissionAmount.SelectedIndex!=0)
            {

                ProviderAmountDiv.Visible = true;
                ProviderAmountDiv.Visible = false;
                CommissionAmountDiv.Visible = false;
                ProcessingFeeAmountDiv.Visible = false; 
               
            }
            else
            {
                ProviderAmountDiv.Visible = true;
                CommissionAmountDiv.Visible = true;
                ProcessingFeeAmountDiv.Visible = true; 
            }
        }
    }
}