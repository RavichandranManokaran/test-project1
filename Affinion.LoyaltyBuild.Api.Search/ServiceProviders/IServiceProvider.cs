﻿using Affinion.LoyaltyBuild.Api.Search.Data;
using Affinion.LoyaltyBuild.Api.Search.Filters;
using Affinion.LoyaltyBuild.Model.Provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Api.Search.ServiceProviders
{
    /// <summary>
    /// Serach provider base interface
    /// </summary>
    public interface IServiceProvider
    {
        /// <summary>
        /// Search providers based on filter
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        ProviderCollection SearchProviders(IList<ISearchFilter> filters);

        /// <summary>
        /// Search providers based on filter asyncronusly
        /// </summary>
        /// <param name="filters"></param>
        /// <returns></returns>
        Task<ProviderCollection> SearchProvidersAsync(IList<ISearchFilter> filters);

        /// <summary>
        /// Get provider details
        /// </summary>
        /// <param name="prodiverInfo"></param>
        /// <returns></returns>
        IProvider GetProviderDetails(IBaseProvider prodiverInfo);

        /// <summary>
        /// Get Provider Details with Products
        /// </summary>
        /// <returns></returns>
        IProvider GetProviderWithProduct(ProviderSearch provider);
    }
}
