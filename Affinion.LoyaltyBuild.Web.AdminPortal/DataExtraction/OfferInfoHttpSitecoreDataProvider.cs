﻿namespace Affinion.LoyaltyBuild.AdminPortal.DataExtraction
{
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using Sitecore.Security.Accounts;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    /// <summary>
    /// Implements ISitecoreExternalDataProvider to export offer specific information from a url in a mail
    /// </summary>
    public class OfferInfoHttpSitecoreDataProvider : ISitecoreExternalDataProvider
    {
        /// <summary>
        /// Getting database name
        /// </summary>
        private Database database = Database.GetDatabase("master");
        
        /// <summary>
        /// taking utilities object
        /// </summary>
        private QueryStringHelper queryString;

        /// <summary>
        /// Gets the database which data needs to be import
        /// </summary>
        public Database GetDatabase()
        {
            // always add offer specific information to master db
            return database;
        }

        /// <summary>
        /// Get the sitecore template to import data as sitecore items
        /// </summary>
        public TemplateItem GetTemplate()
        {
            // template - /sitecore/templates/Affinion/SupplierPortal/OfferSpecificInformation
            Item templateItem = database.GetItem(new ID("{12201B5E-C424-405D-814D-DEC7CC7C1E24}"));
            return new TemplateItem(templateItem);
        }

        /// <summary>
        /// Get the root folder item to decide where to add imported data
        /// </summary>
        public Item GetRootFolder()
        {
            Item item = database.GetItemByKey("OfferSpecificInformation");
            return item;
        }

        /// <summary>
        /// Gives the external party to decide on item name
        /// </summary>
        public string GetContextItemName()
        {
            string suffix = DateTime.Now.Ticks.ToString();

            if (this.queryString == null)
                return suffix;

            string supplierId = this.queryString.GetValue("supplier");
            Item supplierItem = database.GetItem(new ID(supplierId));
            string supplierName = supplierItem != null ? supplierItem.Name : supplierId;

            string offerId = this.queryString.GetValue("offerid");
            suffix = string.IsNullOrEmpty(offerId) ? suffix : offerId;

            return supplierName + "-" + suffix;
        }

        /// <summary>
        /// Presents the external data as a collection of SitecoreFieldData
        /// </summary>
        public IEnumerable<SitecoreFieldData> GetData()
        {
            if (this.queryString == null)
                return null;

            List<SitecoreFieldData> fieldData = new List<SitecoreFieldData>();

            fieldData.Add(new SitecoreFieldData()
            {
                Name = "OfferId",
                Value = this.queryString.GetValue("offerid"),
                FieldType = typeof(Sitecore.Data.Fields.TextField)
            });

            fieldData.Add(new SitecoreFieldData()
            {
                Name = "Supplier",
                Value = this.queryString.GetValue("supplier"),
                FieldType = typeof(Sitecore.Data.Fields.ReferenceField)
            });

            fieldData.Add(new SitecoreFieldData()
            {
                Name = "Message",
                Value = this.queryString.GetValue("message"),
                FieldType = typeof(Sitecore.Data.Fields.TextField)
            });

            fieldData.Add(new SitecoreFieldData()
            {
                Name = "Version",
                Value = this.queryString.GetValue("version"),
                FieldType = typeof(Sitecore.Data.Fields.TextField)
            });

            return fieldData;
        }

        public string GetExternalVersion()
        {
            return this.queryString.GetValue("version");
        }

        public string GetExternalVersionFieldName()
        {
            return "Version";
        }

        /// <summary>
        /// Set data using the url
        /// </summary>
        /// <param name="url">the url of the data extraction requst. should have the required query string</param>
        public void SetData(Uri url)
        {
            this.queryString = new QueryStringHelper(url);
        }
    }
}