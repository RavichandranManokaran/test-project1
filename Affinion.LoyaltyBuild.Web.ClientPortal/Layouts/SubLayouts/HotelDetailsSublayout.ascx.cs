﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           HotelDetailsSublayout.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.HotelDetailsSublayout
/// Description:           Renders hotel details

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts
{
    #region Using Statements

    using Affinion.Loyaltybuild.BusinessLogic.Helper;
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.Common.Exceptions;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.SessionHelper;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Sitecore.Data.Fields;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.WebControls;
    using Affinion.LoyaltyBuild.Search;
    using Affinion.LoyaltyBuild.Search.Data;
    using Affinion.LoyaltyBuild.Search.Helpers;
    using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
    using NewSearchApi = Affinion.LoyaltyBuild.Api.Search.Data;
    using System.Data;
    using Affinion.LoyaltyBuild.Api.Search.Helpers;
    using Affinion.Loyaltybuild.BusinessLogic;
    using System.Text;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
    using Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi;
    using Affinion.LoyaltyBuild.Web.ClientPortal.ViewModels;
    using Affinion.LoyaltyBuild.Model.Product;
    using Affinion.LoyaltyBuild.Web.ClientPortal.Helper;
    using Affinion.LoyaltyBuild.UCom.Common.Extension;
    using searchhelper = Affinion.LoyaltyBuild.Api.Search.Helpers;
    using Affinion.LoyaltyBuild.Model.Provider;
    using Affinion.LoyaltyBuild.BedBanks.Troika;
    using Affinion.LoyaltyBuild.BedBanks.General;
    using Affinion.LoyaltyBuild.BedBanks.JacTravel.Message;
    using Affinion.LoyaltyBuild.BedBanks.Helper;
    using Affinion.LoyaltyBuild.BedBanks.JacTravel.Model;
    using Affinion.LoyaltyBuild.BedBanks.JacTravel.Model.Room;
    using Affinion.LoyaltyBuild.Api.Booking.Data.Basket;



    #endregion

    public partial class HotelDetailsSublayout : BaseSublayout
    {
        #region Properties

        public string latitude { get; set; }
        public string lognitude { get; set; }
        public string PlaceHolderValue { get; set; }

        /// <summary>
        /// Class to store BasketData from multiple selection
        /// </summary>
        private class BasketData
        {
            public int Qty { get; set; }
            public string[] CommandArgument { get; set; }
        }


        #endregion

        #region Fields

        private string itemId;
        string path = "/sitecore/content/#admin-portal#/#client-setup#/#global#/#_supporting-content#/#bedbankprovidersettings#/#jactravel#/#jactravel-currency#//*[@@templateid='{730153DD-E48F-4F75-9BB1-CC16F8F8A432}']";
        private Item clientItem = SitecoreFieldsHelper.GetLookupFieldTargetItem(ItemHelper.RootItem, "ClientDetails");
        private Item supplierOfferItem;
        private SearchKey searchTerm;
        private IList<Affinion.LoyaltyBuild.Model.Provider.IProvider> searchResult;
        private string inputChildAges = string.Empty;
        NewSearchApi.SearchKey mySearchKey = new NewSearchApi.SearchKey(System.Web.HttpContext.Current);

        #endregion

        #region Public Methods

        /// <summary>
        /// Code to execute before page loads
        /// </summary>
        /// <param name="e">The argument</param>
        public override void PreLoad(EventArgs e)
        {
            Uri pageUri = Request.Url;
            string HotelDetailUrl = pageUri.ToString();
            QueryStringHelper helper = new QueryStringHelper(pageUri);
            itemId = helper.GetValue("id");
            searchTerm = new SearchKey(pageUri, clientItem);
            if (string.IsNullOrEmpty(itemId))
            {
                plcLBwrapper.Visible = false;
                return;
            }
            if (this.IsPostBack && this.Request != null && this.Request.Params["__EVENTTARGET"] != null && (this.Request.Params["__EVENTTARGET"].Contains("CheckAvailabilityButton") || this.Request.Params["__EVENTTARGET"].Contains("FlexibleDateRadioButton")))
            {

                string chekinDateValue = helper.GetValue("CheckinDate");
                int numberOfNights = 1;
                if (this.Request.Params["__EVENTTARGET"].Contains("FlexibleDateRadioButton"))
                {
                    chekinDateValue = Request["FlexibleDatesRadio"] != null ? Request["FlexibleDatesRadio"].ToString() : chekinDateValue;
                    numberOfNights = int.Parse(helper.GetValue("NoOfNights"));
                    helper.SetValue("FlexibleDates", "True");
                }
                else
                {
                    chekinDateValue = this.CheckInDate.Text;
                    numberOfNights = int.Parse(this.NoOfNights.SelectedValue);
                    helper.SetValue("FlexibleDates", "False");
                }

                DateTime checkinDate = DateTimeHelper.ParseDate(chekinDateValue);
                DateTime checkoutDate = checkinDate.AddDays(numberOfNights);

                helper.SetValue("CheckinDate", chekinDateValue);
                helper.SetValue("CheckoutDate", checkoutDate.ToString(DateTimeHelper.DateFormat));
                helper.SetValue("NoOfNights", numberOfNights.ToString());

                Response.Redirect(helper.GetUrl().ToString(), true);
                if (helper.GetValue("childAge") != null)
                {
                    inputChildAges = helper.GetValue("childAge");
                }
            }
            else
            {
                this.CheckInDate.Text = string.IsNullOrEmpty(helper.GetValue("CheckinDate")) ? Convert.ToString(DateTime.Now.AddDays(1).ToString("dd/MM/yyyy")) : helper.GetValue("CheckinDate");
                this.CheckingDateLabel.Text = string.IsNullOrEmpty(helper.GetValue("CheckinDate")) ? Convert.ToString(DateTime.Now.AddDays(1).ToString("dd/MM/yyyy")) : helper.GetValue("CheckinDate");
                this.NumberOfNightsLabel.Text = string.IsNullOrEmpty(helper.GetValue("NoOfNights")) ? Convert.ToString(1) : helper.GetValue("NoOfNights");
                this.NoOfNights.SelectedValue = string.IsNullOrEmpty(helper.GetValue("NoOfNights")) ? Convert.ToString(1) : helper.GetValue("NoOfNights");
                //this.CheckAvailabilityButton.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "CheckAvailabilityButton");
            }
        }

        #endregion

        #region Protected Methods
        /// <summary>
        /// Page load
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(itemId))
                {
                    if (!IsPostBack)
                    {
                        Item placeHolder = Sitecore.Context.Item;
                        this.PlaceHolderValue = SitecoreFieldsHelper.GetValue(placeHolder, "CheckinDateLabel");
                        CheckInDate.Attributes.Add("placeholder", this.PlaceHolderValue);
                        LoadHotelDetails(itemId);

                        supplierOfferItem = Sitecore.Context.Database.GetItem(itemId);

                        BindOfferDetails();

                        this.BindSitecoreTexts();
                    }
                    else
                    {
                        plcLBwrapper.Visible = false;
                        return;
                    }
                }
                BindList();
            }
            catch (FormatException ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, this);
                throw;
            }
            catch (OverflowException ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, this);
                throw;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, this);
                throw;
            }
        }


        /// <summary>
        /// Add to basket
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ButtonAddToBasket_Click(object sender, EventArgs e)
        {
            try
            {
                Button button = (sender as Button);
                int qty = 0;

                //Get the command argument
                string[] commandArgument = button.CommandArgument.Split(',');

                // TODO : Check the Qty
                foreach (var item in button.Parent.Controls)
                {
                    if (item.GetType().Name.Equals("DropDownList"))
                    {
                        //qty = (item as DropDownList).SelectedIndex;
                        bool isValid = Int32.TryParse((item as DropDownList).SelectedValue, out qty);
                    }
                }

                if (qty == 0)
                    return;

                // Create basket session
                //CreateBasketSession(commandArgument[0], commandArgument[2]);

                // Add product to basket, get necessary information form session and some are from button command arguement
                //BasketHelper.AddToBasket(commandArgument, qty, (BasketInfo)Session["BasketInfo"]);
                Response.Redirect("/Basket");
            }
            catch (Exception ex)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Add to basket in One click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ButtonAddToBasketOneClick_Click(object sender, EventArgs e)
        {
            Button btnAddToBasket = sender as Button;
            int qty = 0;
            List<BasketData> BasketDataList = new List<BasketData>();

            try
            {
                if (btnAddToBasket != null)
                {
                    foreach (RepeaterItem repeaterControls in this.OffersList.Controls)
                    {
                        //foreach (RepeaterItem repeaterControls in item.Parent.Controls)
                        //{
                        // Get the Item only from Item Template section (Header, Item and Footer are the section of ASP.Repeater)
                        if (repeaterControls.ItemType == ListItemType.Item || repeaterControls.ItemType == ListItemType.AlternatingItem)
                        {
                            DropDownList drpQty = repeaterControls.Controls[0].FindControl("drpAvailability") as DropDownList;
                            Button buttonAdd = repeaterControls.Controls[0].FindControl("ButtonAddToBasket1") as Button;

                            if (drpQty != null)
                            {
                                bool isValid = Int32.TryParse((drpQty).SelectedValue, out qty);
                            }

                            if (buttonAdd != null)
                            {
                                //Get the command argument
                                string[] commandArgument = buttonAdd.CommandArgument.Split(',');

                                if (qty > 0)
                                {
                                    BasketData basketData = new BasketData() { Qty = qty, CommandArgument = commandArgument };
                                    BasketDataList.Add(basketData);
                                }
                            }
                        }
                        // }
                    }

                    // Refresh the page when add to basket success (qty > 0 also ideal)
                    if (BasketDataList.Count > 0)
                    {
                        foreach (BasketData item in BasketDataList)
                        {
                            int currencyID = Convert.ToInt32(item.CommandArgument[1]);
                            IProduct outRoom = CreateBasketSession(item.CommandArgument[0], item.CommandArgument[2], item.CommandArgument[4]);
                            if (outRoom is Room)
                            {
                                for (int k = 0; k < item.Qty; k++)
                                {
                                    Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.AddToBasket(outRoom, 1, RoomInfo(item.CommandArgument[0], item.CommandArgument[3], currencyID));
                                }
                            }
                            else if (outRoom is BedBankProduct)
                            {
                                for (int k = 0; k < item.Qty; k++)
                                {
                                    int bbPropertyId = Convert.ToInt32(item.CommandArgument[2]);
                                    Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.AddToBasket(outRoom, 1, RoomInfo(item.CommandArgument[0], item.CommandArgument[3], currencyID, bbPropertyId, item.CommandArgument[4], item.CommandArgument[5], item.CommandArgument[6]));
                                }
                            }
                        }

                        BasketDataList.Clear();
                        //Refresh the page                                    
                        Response.Redirect("/Basket");
                    }

                }
            }
            catch (Exception ex)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Adding room Info
        /// </summary>
        /// <param name="variatSku"></param>
        /// <param name="roomName"></param>
        /// <param name="currencyID"></param>
        /// <returns></returns>
        private AddRoomRequest RoomInfo(string variatSku, string roomName, int currencyID)
        {

            AddRoomRequest roomre = new AddRoomRequest();
            roomre.CheckinDate = searchTerm.CheckinDate;
            roomre.CheckOutDate = searchTerm.CheckoutDate;
            roomre.VariantSku = variatSku;
            roomre.NoOfRooms = 1;
            roomre.NoOfAdults = searchTerm.NumberOfAdults;
            roomre.NoOfChildren = searchTerm.NumberOfChildren;
            roomre.NoofNights = searchTerm.NoOfNights;
            roomre.ChildrenAges = inputChildAges;
            roomre.BookingThrough = BookingMethod.Online.ToString();
            roomre.CreatedBy = SitecoreUserProfileHelper.GetItem(UserProfileKeys.CreatedBy);
            roomre.RoomType = roomName;
            roomre.CurrencyId = currencyID;
            roomre.ClientId = clientItem.ID.ToString();
            return roomre;

        }

        private AddBedBanksRequest RoomInfo(string variatSku, string roomName, int currencyID, int propertyId, string bookingToken, string marginId, string mealBasisId)
        {
            int temp = 0;
            AddBedBanksRequest roomReq = new AddBedBanksRequest();
            roomReq.PropertyID = propertyId;
            roomReq.ClientId = searchTerm.Client.ID.ToString();
            roomReq.CheckinDate = searchTerm.CheckinDate;
            roomReq.CheckoutDate = searchTerm.CheckoutDate;
            roomReq.NoofNights = searchTerm.NoOfNights;
            roomReq.NoOfAdults = searchTerm.NumberOfAdults;
            roomReq.NoOfChildren = searchTerm.NumberOfChildren;
            roomReq.ChildrenAges = inputChildAges;
            roomReq.RoomInfo = roomName;
            roomReq.PreBookingToken = bookingToken;
            roomReq.BookingThrough = BookingMethod.Online.ToString();
            roomReq.NoOfRooms = searchTerm.NumberOfRooms;
            roomReq.RoomType = ProductType.BedBanks.ToString();
            roomReq.NoofNights = searchTerm.NoOfNights;
            roomReq.CurrencyId = currencyID;
            roomReq.IsDirect = int.TryParse(bookingToken, out temp).ToString();
            int marginIdApplied = 0;
            int.TryParse(marginId, out marginIdApplied);
            roomReq.MarginIdApplied = marginIdApplied;
            roomReq.MealBasisID = mealBasisId;
            //roomReq.PropertyName = searchTerm.AllResults.Where(y => y.ProviderType == ProviderType.BedBanks && int.Parse(y.ProviderId) == propertyId).Select(x => x.Name).FirstOrDefault().ToString();
            //roomReq.ImageUrl = searchTerm.AllResults.Where(y => y.ProviderType == ProviderType.BedBanks && int.Parse(y.ProviderId) == propertyId).Select(x => x.IntroductionImage).FirstOrDefault().ToString();
            //roomReq.StarRanking = int.Parse(searchTerm.AllResults.Where(y => y.ProviderType == ProviderType.BedBanks && int.Parse(y.ProviderId) == propertyId).Select(x => x.StarRanking).FirstOrDefault().ToString());
            //roomReq.MaxAdults = int.Parse(searchTerm.AllResults.Where(y => y.ProviderType == ProviderType.BedBanks && int.Parse(y.ProviderId) == propertyId).Select(x => x.MaxAdults).FirstOrDefault().ToString());
            //roomReq.MaxChildren = int.Parse(searchTerm.AllResults.Where(y => y.ProviderType == ProviderType.BedBanks && int.Parse(y.ProviderId) == propertyId).Select(x => x.MaxChild).FirstOrDefault().ToString());
            //roomReq.MinAdults = int.Parse(searchTerm.AllResults.Where(y => y.ProviderType == ProviderType.BedBanks && int.Parse(y.ProviderId) == propertyId).Select(x => x.MinAdults).FirstOrDefault().ToString());
            //roomReq.OldPrice = searchTerm.AllResults.Where(y => y.ProviderType == ProviderType.BedBanks && int.Parse(y.ProviderId) == propertyId).Select(x => x.LowestPrice).FirstOrDefault().ToString();
            return roomReq;
            //ad price
        }



        /// <summary>
        /// ItemDataBound of OffersList Repeater
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OffersList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            ListItemType itemType = e.Item.ItemType;

            if (itemType == ListItemType.Item || itemType == ListItemType.AlternatingItem)
            {
                DropDownList selectList = e.Item.FindControl("drpAvailability") as DropDownList;

                int availability = (int)((ProviderInfo)e.Item.DataItem).RoomAvailability;


                if (availability > searchTerm.NumberOfRooms + 3)
                {
                    selectList.DataSource = Enumerable.Range(0, searchTerm.NumberOfRooms + 3);
                }
                else
                {
                    selectList.DataSource = Enumerable.Range(0, availability + 1);
                }
                selectList.DataBind();

                if (availability <= 0)
                {
                    selectList.Enabled = false;
                }


            }
            else if (itemType == ListItemType.Footer)
            {
                var addToBasket = e.Item.FindControl("ButtonAddToBasket") as Button;
                if (addToBasket != null)
                {
                    //addToBasket.Click += addToBasket_Click;
                }
            }
        }


        protected string GetVariantSkuAndProviderID(ProviderInfo roomDetails)
        {
            try
            {
                if (roomDetails == null)
                {
                    return string.Empty;
                }
                string name = roomDetails.VariatSku + "," + roomDetails.PriceCurrency + "," + roomDetails.ProviderID + "," + roomDetails.Name + "," + roomDetails.Sku + "," + roomDetails.BookingTokenForBB + "," + roomDetails.BBMealBasisId + "," + roomDetails.BBMarginId;
                return name;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, this);
                throw;
            }
        }


        protected string GetRoomName(ProviderInfo offer)
        {
            if (offer == null)
            {
                return string.Empty;
            }

            string name = "<span>" + offer.Name + "</span>";

            //if (!string.IsNullOrEmpty(offer.OfferName))
            //{
            //    name += "<span class='type-room'>(";
            //    name += offer.OfferName;
            //    name += ")</span>";
            //}

            return name;
        }

        protected string GetRoomPrice(ProviderInfo offer)
        {
            if (offer == null)
            {
                return string.Empty;
            }

            decimal price = offer.Price;
            return Convert.ToString(offer.Price);
        }

        protected string GetFlexibleDatesRowStartHtml(int rowIndex)
        {
            StringBuilder value = new StringBuilder();
            int rowNumber = rowIndex + 1;
            if (rowNumber % 7 == 1)
            {
                value.AppendLine();
                value.AppendLine("<div class='row'>");
                value.AppendLine("<div class='col-xs-12 col-md-1 calender-left-space'></div>");
                value.AppendLine("<div class='col-xs-12 col-md-1 col-sm-6'></div>");
                value.AppendLine();
            }

            return value.ToString();
        }

        protected string GetFlexibleDatesRowEndHtml(int rowIndex)
        {
            StringBuilder value = new StringBuilder();
            int rowNumber = rowIndex + 1;
            if (rowNumber % 7 == 0)
            {
                value.AppendLine();
                value.AppendLine("<div class='col-xs-12 col-md-1 col-sm-6'></div>");
                value.AppendLine("<div class='col-xs-12 col-md-1 col-sm-6'></div>");
                value.AppendLine("<div class='col-xs-12 col-md-1 col-sm-6'></div>");
                value.AppendLine("</div>");
                value.AppendLine();
            }

            return value.ToString();
        }

        protected Uri GetFlexibleDatesUrl(int days)
        {
            Uri pageUri = Request.Url;
            QueryStringHelper helper = new QueryStringHelper(pageUri);
            DateTime checkinDate = DateTimeHelper.ParseDate(helper.GetValue("CheckinDate"));
            helper.SetValue("CheckinDate", checkinDate.AddDays(days).ToString(DateTimeHelper.DateFormat));
            helper.SetValue("CheckoutDate", checkinDate.AddDays(days + searchTerm.NoOfNights).ToString(DateTimeHelper.DateFormat));
            helper.SetValue("FlexibleDates", "True");
            searchTerm.FlexibleDates = true;

            return helper.GetUrl(false);
        }

        protected string CheckFlexibleDateRadioButton(DateTime currentDate)
        {
            if (currentDate == searchTerm.CheckinDate)
            {
                return "checked='checked'";
            }

            return string.Empty;
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Load hotel related details
        /// </summary>
        /// <param name="itemID">Item id of the hotel</param>
        private void LoadHotelDetails(string itemID)
        {
            searchTerm.CurrencyCode = this.GetCurrencyInformation();
            Item item = Sitecore.Context.Database.GetItem(itemID);

            if (item == null)
            {
                return;
            }

            ///Bind hotel name
            HotelName.Item = item;
            HotelNameForAvailability.Item = item;

            BindStarRankings(item);

            ///Bind image gallery
            this.ImageGalleryRepeater.DataSource = SitecoreFieldsHelper.GetMutiListItems(item, "ProfileImages");
            this.ImageGalleryRepeater.DataBind();


            BindAddressFields(item);

            Item country = SitecoreFieldsHelper.GetDropLinkFieldTargetItem(item, "Country");
            if (country != null)
            {
                CountryName.Item = country;
            }
            //Phone.Item = item;
            //Fax.Item = item;

            ///Other info binding
            Overview.Item = item;
            Facilities.Item = item;
            GoodToKnow.Item = item;
            BindTimeFields(item);

            string latLong = item.Fields["GeoCoordinates"].Value;
            if (!string.IsNullOrWhiteSpace(latLong))
            {
                List<string> latLongList = latLong.Split(',').ToList<string>();
                latitude = latLongList[0];
                if (latLongList.Count() > 1)
                    lognitude = latLongList[1];
            }
            else
            {
                latitude = "0";
                lognitude = "0";
            }

            ///Bind facilities
            FacilitiesRepeater.DataSource = SitecoreFieldsHelper.GetMutiListItems(item, "Facilities");
            FacilitiesRepeater.DataBind();

            //LoadTripAdvisorDetails(item);

            BindNoOfNightsDropDownData(item);

            //Load Buttons
            //Item page = Sitecore.Context.Item;
            //Sitecore.Data.Fields.ImageField btnSavedLater = page.Fields["SavedForLaterButton"];
            //if(btnSavedLater != null && btnSavedLater.MediaItem != null)
            //{ 
            //    btnSaveForLater.ImageUrl = Sitecore.Resources.Media.MediaManager.GetMediaUrl(btnSavedLater.MediaItem);
            //}
            //Sitecore.Data.Fields.ImageField btnAddFav = page.Fields["AddToFavoriteButton"];
            //if (btnAddFav != null && btnAddFav.MediaItem != null )
            //{
            //    //btnAddToFav.Src = Sitecore.Resources.Media.MediaManager.GetMediaUrl(btnAddFav.MediaItem);
            //}
        }

        /// <summary>
        /// Bind data
        /// </summary>
        /// <param name="item">Hotel item</param>
        private void BindNoOfNightsDropDownData(Item item)
        {
            int maxNights = SitecoreFieldsHelper.GetInteger(item, "MaxNights", 10);
            string selectedNoOfNights = NoOfNights.SelectedValue;
            this.NoOfNights.DataSource = Enumerable.Range(1, maxNights);
            this.NoOfNights.DataBind();
            NoOfNights.SelectedValue = selectedNoOfNights;
        }

        /// <summary>
        /// Load TripAdvisor related details
        /// </summary>
        /// <param name="item"></param>
        //private void LoadTripAdvisorDetails(Item item)
        //{
        //    ///Set rating image
        //    LinkField ratingImageField = item.Fields["RatingImage"];
        //    if (!string.IsNullOrWhiteSpace(ratingImageField.Url))
        //    {
        //        hotelRating.ImageUrl = ratingImageField.Url;
        //    }
        //    else
        //    {
        //        hotelRating.Visible = false;
        //    }

        //    ///Set hotel URL and no of reviews
        //    LinkField hotelUrlField = item.Fields["HotelUrl"];
        //    noOfReviewsLink.NavigateUrl = hotelUrlField.Url;

        //    string reviewCount = item.Fields["NoOfReviews"].Value;
        //    //noOfReviews.Item = item;
        //    if (!string.IsNullOrWhiteSpace(reviewCount))
        //    {
        //        //noOfReviews.Text = reviewCount;
        //        noOfReviews.Item = item;
        //    }

        //    ///Hide TripAdvisor Traveller Rating title when details are not available.
        //    if (string.IsNullOrWhiteSpace(reviewCount) && string.IsNullOrWhiteSpace(ratingImageField.Url))
        //    {
        //        TravellerRatingLabel.Visible = false;
        //        ReviewsLabel.Visible = false;
        //    }


        //}

        /// <summary>
        /// Bind ranking related fields
        /// </summary>
        /// <param name="item">Sitecore item</param>
        private void BindStarRankings(Item item)
        {
            ///Bind star rank
            string ranking = SitecoreFieldsHelper.GetDropLinkFieldValue(item, "StarRanking", "Name");

            if (!string.IsNullOrWhiteSpace(ranking))
            {
                int starRank;

                bool result = Int32.TryParse(ranking, out starRank);

                if (result)
                {
                    //starRank = int.Parse(ranking);
                    List<int> listOfStars = Enumerable.Repeat(starRank, starRank).ToList();
                    this.StarRepeater.DataSource = listOfStars;
                    this.StarRepeater.DataBind();
                }
                else
                {
                    Diagnostics.WriteException(DiagnosticsCategory.ClientPortal,
                        new AffinionException("Unable to parse star rank:" + item.DisplayName), this);
                }


            }
        }

        /// <summary>
        /// Bind time related fields
        /// </summary>
        /// <param name="item">Sitecore item</param>
        private void BindTimeFields(Item item)
        {
            CheckinTime.Item = item;
            CheckoutTime.Item = item;
        }

        /// <summary>
        /// Bind address related fields
        /// </summary>
        /// <param name="item">Sitecore item</param>
        private void BindAddressFields(Item item)
        {
            /////Top address binding
            //TopAddressLine1.Item = item;
            //TopAddressLine2.Item = item;
            //TopAddressLine3.Item = item;
            //TopAddressLine4.Item = item;
            //TopTown.Item = item;

            ///Address binding
            string addresstext = string.Empty;
            if (!string.IsNullOrEmpty(item.Fields["AddressLine1"].Value))
            {
                addresstext += item.Fields["AddressLine1"].Value + ", ";
            }
            if (!string.IsNullOrEmpty(item.Fields["AddressLine2"].Value))
            {
                addresstext += item.Fields["AddressLine2"].Value + ", ";
            }
            if (!string.IsNullOrEmpty(item.Fields["AddressLine3"].Value))
            {
                addresstext += item.Fields["AddressLine3"].Value + ", ";
            }
            if (!string.IsNullOrEmpty(item.Fields["AddressLine4"].Value))
            {
                addresstext += item.Fields["AddressLine4"].Value + ", ";
            }
            if (!string.IsNullOrEmpty(item.Fields["Town"].Value))
            {
                addresstext += item.Fields["Town"].Value + ", ";
            }
            fulladdress.Text = addresstext;
        }


        /// <summary>
        /// Create basket information in session
        /// </summary>
        private IProduct CreateBasketSession(string variantSku, string providerid, string SKU)
        {
            // Create a session to store CheckinDate, CheckoutDate, NoOfRooms, NoOfAdults, NoOfChildren
            Uri pageUri = Request.Url;
            QueryStringHelper helper = new QueryStringHelper(pageUri);
            IProduct outRoom = new Room();

            ProviderSearch providerInfo = new ProviderSearch();
            providerInfo.ProviderId = SKU;
            providerInfo.SitecoreItemId = itemId;
            providerInfo.ProviderType = ProviderType.LoyaltyBuild;

            ///Getting Products of Hotel 
            var hotel = searchhelper.SearchHelper.GetProviderInfo(mySearchKey, providerInfo);

            //var OfferGroup = helper.GetValue("OfferGroup");
            foreach (var tmpItem in hotel.Products)
            {
                if (tmpItem.VariantSku == variantSku)
                {
                    outRoom = tmpItem;
                    break;
                }
            }

            BasketInfo basketInfo = new BasketInfo
            {
                VariantSku = variantSku,
                SupplierId = providerid,
                CheckinDate = helper.GetValue("CheckinDate"),
                CheckoutDate = helper.GetValue("CheckoutDate"),
                Nights = helper.GetValue("NoOfNights"),
                Location = helper.GetValue("Destination"),
                NoOfRooms = helper.GetValue("NoOfRooms"),
                NoOfAdults = helper.GetValue("NoOfAdults"),
                NoOfChildren = helper.GetValue("NoOfChildren"),
                OfferGroup = string.Empty,
                BookingThrough = BookingMethod.Online.ToString(),
                ChildrenAges = inputChildAges
            };

            Session["BasketInfo"] = basketInfo;

            List<BasketInfo> listBasketInfo = new List<BasketInfo>();

            listBasketInfo.Add(basketInfo);

            // bind the unique value to session
            //Session["listBasketInfo"] = listBasketInfo.Select(p => p.Sku).Distinct();
            listBasketInfo = listBasketInfo.Distinct().ToList();
            Session["listBasketInfo"] = listBasketInfo;

            return outRoom;
        }

        /// <summary>
        /// Bind offer details
        /// </summary>
        private void BindOfferDetails()
        {

            Item hotelitem = Sitecore.Context.Database.GetItem(itemId);
            string ProviderSKU = hotelitem.Fields["SKU"].Value;



            //If checkin date is null, set add a day with current date as checkindate 
            if (searchTerm.CheckinDate == DateTime.MinValue)
            {
                searchTerm.CheckinDate = DateTime.Now.AddDays(1);
                searchTerm.CheckoutDate = searchTerm.CheckinDate.AddDays(1);
            }
            //// temporary off flexible dates to load normal data set
            //bool tempFlexibleDateStatus = searchTerm.FlexibleDates;
            //searchTerm.FlexibleDates = false;

            ProviderSearch providerInfo = new ProviderSearch();
            providerInfo.ProviderId = ProviderSKU;
            providerInfo.SitecoreItemId = itemId;
            providerInfo.ProviderType = ProviderType.LoyaltyBuild;

            ///Getting Hotel Details


            var hotel = searchhelper.SearchHelper.GetProviderInfo(mySearchKey, providerInfo);

            var HotelRooms = LoadRoomsData(hotel);
            if (HotelRooms != null && HotelRooms.Count() > 0)
            {
                OffersList.DataSource = HotelRooms.OrderBy(y => y.Price);
                OffersList.DataBind();
                AvailableAlert.Visible = false;
            }

            IList<IProduct> products = new List<IProduct>();
            foreach (var item in HotelRooms)
            {
                Room room = new Room();
                room.Name = item.Name;
                room.Price = item.Price;
                room.Sku = item.Sku;
                room.VariantSku = item.VariatSku;
                room.Type = ProductType.Room;
                products.Add(room);
            }

            if (HotelRooms.Count > 0)
            {
                OffersList.DataSource = HotelRooms.OrderBy(y => y.Price);
                OffersList.DataBind();
                AvailableAlert.Visible = false;
            }
            else if (IsPostBack)
            {
                DataTable propRefId = TroikaDataController.ReverseMapLBtoBB(itemId.ToString());
                if (propRefId != null)
                {
                    string ReferenceId = propRefId.Rows[0][0].ToString();
                    string ProviderId = propRefId.Rows[0][1].ToString();
                    if (!string.IsNullOrEmpty(ReferenceId) && !(ReferenceId.Equals("0")))
                    {
                        var roomData = CheckBBRoomAvailability(ReferenceId, ProviderId);
                        if (roomData != null)
                        {
                            LoadBBRooms(roomData, mySearchKey.CheckInDate.ToString());
                        }
                    }
                }

            }


            if (searchTerm.FlexibleDates)
            {
                Uri pageUri = Request.Url;
                string HotelDetailUrl = pageUri.ToString();
                QueryStringHelper helper = new QueryStringHelper(pageUri);


                DateTime checkinDate = DateTimeHelper.ParseDate(helper.GetValue("CheckinDate"));

                DateTime startDate = checkinDate;
                DateTime endDate = startDate.AddDays(42);
                List<FlexibleDateData> flexibleDates = new List<FlexibleDateData>();

                List<string> dataValue = new List<string>();

                var RoomWithAvailability = HotelSearchHelper.GetAvailabilityAndPriceByDate(products, null, startDate, endDate, 1);

                var query = RoomWithAvailability.GroupBy(x => x.Date)
                                .Select(group => group.Where(x => x.Price == group.Min(y => y.Price))
                             .First());


                for (DateTime dt = startDate; dt < endDate; dt = dt.AddDays(1))
                {
                    FlexibleDateData data = new FlexibleDateData();
                    var RoomDataValue = query.Where(x => x.Date == dt).FirstOrDefault();
                    if (RoomDataValue != null)
                    {
                        data.Date = RoomDataValue.Date;
                        data.Price = Convert.ToString(RoomDataValue.Price);
                        data.Availability = Convert.ToBoolean(RoomDataValue.Availability);
                        dataValue.Add(data.ToString());
                        flexibleDates.Add(data);
                    }
                    else
                    {
                        data.Date = dt;
                        data.Price = string.Empty;
                        data.Availability = false;
                        dataValue.Add(data.ToString());
                        flexibleDates.Add(data);
                    }
                    rptFlexibleDates.DataSource = flexibleDates;
                    rptFlexibleDates.DataBind();
                    flexibleDatesContainer.Visible = true;
                }
            }
            else
            {
                flexibleDatesContainer.Visible = false;
            }
        }


        private void LoadBBRooms(PropertyResult hotelDetails, string arrDate)
        {
            JtRoomType[] rooms = null;
            decimal margin;
            int marginId;
            string[] marginInfo = GetMargin(hotelDetails.PropertyID, arrDate).Split(',');
            decimal.TryParse(marginInfo[1], out margin);
            int.TryParse(marginInfo[0], out marginId);
            CurrencyInfo ci = new CurrencyInfo();
            decimal finalPrice;

            int count = 0;
            if (hotelDetails.RoomTypes != null)
            {
                int avaiableRooms = hotelDetails.RoomTypes.Count(x => x != null);
                rooms = new JtRoomType[avaiableRooms];
                foreach (var roomtype in hotelDetails.RoomTypes)
                {
                    if (roomtype != null && roomtype.RoomType != null)
                    {
                        finalPrice = Math.Round(roomtype.Total + ((margin / 100) * roomtype.Total), 2);
                        roomtype.PriceToDisplay = ci.GetPriceWithCurrency(finalPrice);
                        rooms[count] = (JtRoomType)roomtype;
                        count++;
                    }
                }
            }

            if (rooms != null && rooms.Count() > 0)
            {
                roomTypeBBRepeater.DataSource = rooms;
                roomTypeBBRepeater.DataBind();
                OffersList.Visible = false;
                AvailableAlert.Visible = false;
            }
        }

        private static string GetMargin(string providerId, string arrivalDate)
        {
            DataTable marginData = TroikaDataController.GetMarginForEAchBBHotel(providerId, arrivalDate.ToString());
            string marginId = marginData.Rows[0][0].ToString();
            string marginPercentage = marginData.Rows[0][1].ToString();
            return marginId + "," + marginPercentage;
        }

        private PropertyResult CheckBBRoomAvailability(string ReferenceId, string ProviderId)
        {
            BedBanksSettings bbS = new BedBanksSettings(clientItem);
            BedBanksCurrency bb = new BedBanksCurrency();
            List<JtSearchResponse> property = new List<JtSearchResponse>();
            QueryStringHelper helper = new QueryStringHelper(Request.Url);

            string propertyRefID = ReferenceId;
            string providerID = ProviderId;
            string noOfRooms = helper.GetValue("NoOfRooms");
            string arrDate = helper.GetValue("checkinDate");
            string destination = helper.GetValue("Destination");
            string roomDetail = string.Empty;
            if (string.IsNullOrEmpty(providerID))
            {
                //plcBBwrapper.Visible = false;
                return null;
            }
            //DateTime date;
            //if (!string.IsNullOrEmpty(arrDate))
            //{
            //    date = DateTime.ParseExact(arrDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            //    arrDate = date.ToString("yyyy-MM-dd");
            //}
            int roomsTotal;
            int.TryParse(noOfRooms, out roomsTotal);
            int noOfAdults;
            int.TryParse(helper.GetValue("NoOfAdults"), out noOfAdults);
            int noOfChildren;
            int.TryParse(helper.GetValue("NoOfChildren"), out noOfChildren);
            string[] childAge = !string.IsNullOrEmpty(helper.GetValue("NoOfChildren")) ? helper.GetValue("NoOfChildren").Split(',').ToArray() : null;
            var roomInfo = new List<string>();

            if (!string.IsNullOrEmpty(propertyRefID) && (!string.IsNullOrEmpty(providerID)))
            {

                foreach (var location in bbS.BedBankLocation)
                {
                    if (!string.IsNullOrEmpty(destination) && destination.ToLower().Contains(location.LocationName.ToLower()))
                    {
                        Item locationItem = location.SitecoreLocationItem;
                        Item countryItem = (locationItem == null) ? null : SitecoreFieldsHelper.GetLookupFieldTargetItem(locationItem, "Country");
                        if (countryItem != null)
                        {
                            BedBanksCurrency actualCurr = GetActualCurrency(countryItem);
                            bbS.BedBanksActualCurrency = actualCurr;
                            break;
                        }
                    }
                }
                JacTravelApiWrapper jac = new JacTravelApiWrapper();
                List<string> propRef = new List<string>();
                propRef.Add(propertyRefID);
                JtSearchResponse propertyDetailsSearchResponse = jac.SearchOnPropRef("0", arrDate, helper.GetValue("NoOfNights"), helper.GetValue("NoOfRooms"), helper.GetValue("NoOfAdults"), helper.GetValue("NoOfChildren"), helper.GetValue("AgeOfChildren"), bbS, propRef);
                PropertyResult hotelDetails = propertyDetailsSearchResponse.PropertyResults.FirstOrDefault();
                return hotelDetails;
            }
            else
            {
                return null;
            }
        }

        private BedBanksCurrency GetActualCurrency(Item country)
        {

            Item currencyItem = SitecoreFieldsHelper.GetLookupFieldTargetItem(country, "Currency");
            var currencies = Sitecore.Context.Database.SelectItems(path);

            BedBanksCurrency actualCurr = null;
            foreach (var item in currencies)
            {
                Item ucomCurr = SitecoreFieldsHelper.GetLookupFieldTargetItem(item, "uCommerce Currency Id");
                if (ucomCurr != null && (ucomCurr.ID == currencyItem.ID))
                {
                    actualCurr = new BedBanksCurrency();
                    actualCurr.BedBanksCurrencyId = item.Fields["Bed Banks Currency Id"].Value;
                    actualCurr.CurrencyName = item.Fields["Currency Name"].Value;
                    actualCurr.uCommerceCurrencyId = item.Fields["uCommerce Currency Id"].Value;
                    break;
                }
            }
            return actualCurr;
        }


        /// <summary>
        /// Code for fetching matching rooms
        /// </summary>
        private List<ProviderInfo> LoadRoomsData(IProvider productdetails)
        {
            List<ProviderInfo> finallist = new List<ProviderInfo>();
            IList<IProduct> products = new List<IProduct>();
            List<PriceAvailabilityByDate> PriceAvailabilityList = new List<PriceAvailabilityByDate>();

            NewSearchApi.SearchKey mySearchKey = new NewSearchApi.SearchKey(System.Web.HttpContext.Current);
            products = productdetails.Products;
            Uri pageUri1 = Request.Url;
            QueryStringHelper helper1 = new QueryStringHelper(pageUri1);
            DateTime checkinDate = DateTimeHelper.ParseDate(helper1.GetValue("CheckinDate"));
            PriceAvailabilityList = HotelSearchHelper.GetAvailabilityAndPriceByDate(products, mySearchKey, checkinDate, checkinDate, mySearchKey.NoOfNights);
            if (PriceAvailabilityList != null && PriceAvailabilityList.Count > 0)
            {
                foreach (var priceLst in PriceAvailabilityList)
                {
                    ProviderInfo pr = new ProviderInfo();
                    pr.Price = decimal.Round(priceLst.Price, 2, MidpointRounding.AwayFromZero);
                    pr.Sku = priceLst.Sku;
                    if (priceLst.Availability > mySearchKey.NoOfRooms + 2)
                    {
                        pr.RoomAvailability = mySearchKey.NoOfRooms + 2;
                    }
                    else
                    {
                        pr.RoomAvailability = priceLst.Availability;
                    }
                    pr.VariatSku = priceLst.VariantSku;
                    pr.ProviderID = itemId;
                    pr.PriceCurrency = "5"; //need to change later
                    pr.NoOfNights = priceLst.NoOfNights;
                    pr.Name = products.First(w => w.VariantSku == priceLst.VariantSku).Name;
                    finallist.Add(pr);
                }
            }
            return finallist;
        }

        private void BindList()
        {
            string hotelId = Request.QueryString["id"];
            if (!string.IsNullOrWhiteSpace(hotelId))
            {
                DataSet ds = uCommerceConnectionFactory.GetFavoriteList(68);
                List<string> ListName = new List<string>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ListName.Add(ds.Tables[0].Rows[i]["ListName"].ToString());
                }
                chkFavList.DataSource = ds;
                chkFavList.DataTextField = ds.Tables[0].Columns["ListName"].ToString();
                chkFavList.DataValueField = ds.Tables[0].Columns["ListID"].ToString();
                chkFavList.DataBind();
                for (int i = 0; i < chkFavList.Items.Count; i++)
                {
                    chkFavList.Items[i].Text = "<span>" + chkFavList.Items[i].Text + " - </span>" + string.Format("<a href='/myfavouritelist?ListID={0}' target='_blank' title='Go To List'>Go to List</a>", ds.Tables[0].Rows.IndexOf(ds.Tables[0].Rows[i]));
                    if (ds.Tables[0].Rows[i]["HotelID"].ToString().Contains(hotelId))
                    {
                        chkFavList.Items[i].Selected = true;
                    }
                }
            }
        }

        protected void roomTypeBBRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e != null)
            {
                //if (!IsPostBack)
                //{
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    DropDownList ddl = e.Item.FindControl("availabilityDDL") as DropDownList;
                    QueryStringHelper helper = new QueryStringHelper(Request.Url);
                    int availability = int.Parse(helper.GetValue("NoOfRooms"));
                    ddl.DataSource = Enumerable.Range(0, availability + 1);
                    ddl.DataBind();

                    var addToBasket = e.Item.FindControl("ButtonBBAddToBasketOneClick") as Button;
                    if (addToBasket != null)
                    {
                        addToBasket.Click += ButtonBBAddToBasketOneClick_Click;
                    }
                }
                //}
            }

        }

        private void BindSitecoreTexts()
        {
            Item conditionalMessageItem = this.GetItemFromRenderingParameterPath("Conditional Messages Link");
            string checkinDateMessageFieldName = "Check in Date Message";
            string numberofNightsRequiredMessageFieldName = "Number of Nights Required Message";
            string selectRoomsMessageFieldName = "Select Rooms Message";

            SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, checkinDateMessageFieldName, txtCheckInDateRequiredText);
            SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, numberofNightsRequiredMessageFieldName, txtNumberOfNightsRequiredText);
            SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, selectRoomsMessageFieldName, txtSelectRoomText);

        }
        #endregion

        protected void savelater_ServerClick(object sender, EventArgs e)
        {
            string HotelID = Request.QueryString["id"];
            uCommerceConnectionFactory.SaveHotelForLater(68, HotelID);
            Response.Redirect(Request.RawUrl);

        }

        protected void ButtonBBAddToBasketOneClick_Click(object sender, EventArgs e)
        {
            Button btnAddToBasket = sender as Button;
            int qty = 0;
            List<BasketData> BasketDataList = new List<BasketData>();

            try
            {
                if (btnAddToBasket != null)
                {
                    foreach (RepeaterItem repeaterControls in this.OffersList.Controls)
                    {
                        //foreach (RepeaterItem repeaterControls in item.Parent.Controls)
                        //{
                        // Get the Item only from Item Template section (Header, Item and Footer are the section of ASP.Repeater)
                        if (repeaterControls.ItemType == ListItemType.Item || repeaterControls.ItemType == ListItemType.AlternatingItem)
                        {
                            DropDownList drpQty = repeaterControls.Controls[0].FindControl("availabilityDDL") as DropDownList;
                            Button buttonAdd = repeaterControls.Controls[0].FindControl("ButtonBBAddToBasket1") as Button;

                            if (drpQty != null)
                            {
                                bool isValid = Int32.TryParse((drpQty).SelectedValue, out qty);
                            }

                            if (buttonAdd != null)
                            {
                                //Get the command argument
                                string[] commandArgument = buttonAdd.CommandArgument.Split(',');

                                if (qty > 0)
                                {
                                    BasketData basketData = new BasketData() { Qty = qty, CommandArgument = commandArgument };
                                    BasketDataList.Add(basketData);
                                }
                            }
                        }
                        // }
                    }

                    // Refresh the page when add to basket success (qty > 0 also ideal)
                    if (BasketDataList.Count > 0)
                    {
                        foreach (BasketData item in BasketDataList)
                        {
                            int currencyID = Convert.ToInt32(item.CommandArgument[1]);
                            IProduct outRoom = CreateBasketSession(item.CommandArgument[0], item.CommandArgument[2], item.CommandArgument[4]);
                            if (outRoom is Room)
                            {
                                for (int k = 0; k < item.Qty; k++)
                                {
                                    Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.AddToBasket(outRoom, 1, RoomInfo(item.CommandArgument[0], item.CommandArgument[3], currencyID));
                                }
                            }
                            else if (outRoom is BedBankProduct)
                            {
                                for (int k = 0; k < item.Qty; k++)
                                {
                                    int bbPropertyId = Convert.ToInt32(item.CommandArgument[2]);
                                    Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.AddToBasket(outRoom, 1, RoomInfo(item.CommandArgument[0], item.CommandArgument[3], currencyID, bbPropertyId, item.CommandArgument[4], item.CommandArgument[5], item.CommandArgument[6]));
                                }
                            }
                        }

                        BasketDataList.Clear();
                        //Refresh the page                                    
                        Response.Redirect("/Basket");
                    }

                }
            }
            catch (Exception ex)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, ex.Message);
                throw;
            }

        }


    }

    /// <summary>
    /// Data class to store flexible dates data
    /// </summary>
    public class FlexibleDateData
    {
        public DateTime Date { get; set; }

        public string Price { get; set; }

        public bool Availability { get; set; }
    }
}