﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SimpleRichTextSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.Common.Layouts.SubLayouts.Common.SimpleRichTextSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Sitecore.Data.Items" %>
<p id="PlaceholderText" runat="server">Set the datasource: Required Fields: RichText</p>
<sc:FieldRenderer FieldName="Body" ID="Body" runat="server" />
