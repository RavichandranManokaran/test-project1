﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SuperValuBookingBarSublayout.ascx.cs
/// Sub-system/Module:     Web.ClientPortal(VS project name)
/// Description:           Sublayout for booking interface of supervalu site
/// </summary>
namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts
{
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Sitecore.Data.Items;
    using System;
    using System.Linq;

    /// <summary>
    /// Sublayout for booking interface of supervalu site
    /// </summary>
    public partial class SuperValuBookingBarSublayout : BaseSublayout
    {
        /// <summary>
        /// This function includes the code which needs to run on page load
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The parameter</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Item item = this.GetDataSource();

                if (item != null)
                {
                    SetLabelItems(item);

                    DataBindFields(item);
                }
            }
            catch(Exception ex)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Data bind UI elements
        /// </summary>
        /// <param name="item">Sitecore item</param>
        private void DataBindFields(Item item)
        {
            // Nights list
            int nights = 0;
            if (int.TryParse(SitecoreFieldsHelper.GetValue(item, "Nights", "5"), out nights))
            {
                this.NightsList.DataSource = Enumerable.Range(1, nights);
                this.NightsList.DataBind();
            }

            // Rooms list
            int rooms = 0;
            if (int.TryParse(SitecoreFieldsHelper.GetValue(item, "NoOfRooms", "5"), out rooms))
            {
                this.RoomsList.DataSource = Enumerable.Range(1, rooms);
                this.RoomsList.DataBind();
            }

            // Adults list
            int adults = 0;
            if (int.TryParse(SitecoreFieldsHelper.GetValue(item, "NoOfAdults", "5"), out adults))
            {
                this.AdultsList.DataSource = Enumerable.Range(1, adults);
                this.AdultsList.DataBind();
            }

            // Children list
            int children = 0;
            if (int.TryParse(SitecoreFieldsHelper.GetValue(item, "NoOfChildren", "5"), out children))
            {
                this.ChildrenList.DataSource = Enumerable.Range(1, children);
                this.ChildrenList.DataBind();
            }

            // Packages list
            this.PackageList.DataSource = SitecoreFieldsHelper.GetMutiListItems(item, "Packages");
            this.PackageList.DataBind();
        }

        private void SetLabelItems(Item item)
        {
            // Labels
            this.RoomSectionTitle.Item = item;
            this.RoomsLabel.Item = item;
            this.AdultsLabel.Item = item;
            this.ChildrenLabel.Item = item;
            this.PackageSectionTitle.Item = item;
            this.SearchLabel.Item = item;
            this.NightsLabel.Item = item;
            this.CheckinTitle.Item = item;
            this.DateSectionTitle.Item = item;
            this.BookNowLabel.Item = item;
        }
    }
}