﻿/****** Object:  StoredProcedure [dbo].[LB_GetOfferName]    Script Date: 3/10/2016 1:52:04 PM ******/
DROP PROCEDURE [dbo].[LB_GetOfferName]
GO

/****** Object:  StoredProcedure [dbo].[LB_GetOfferName]    Script Date: 3/10/2016 1:52:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[LB_GetOfferName]
(
@orderlineid int
)
AS
BEGIN
SELECT [CampaignItemName] [OfferName]
	FROM uCommerce_OrderLine OL
		INNER JOIN [dbo].[uCommerce_PurchaseOrder] PO ON PO.OrderId = OL.OrderId
		INNER JOIN [dbo].[uCommerce_OrderLineDiscountRelation] OLD ON OL.OrderLineId = OLD.OrderLineId
		INNER JOIN [dbo].[uCommerce_Discount] D ON OLD.DiscountId = D.DiscountId
	WHERE
		OL.OrderLineId=@orderlineid
END