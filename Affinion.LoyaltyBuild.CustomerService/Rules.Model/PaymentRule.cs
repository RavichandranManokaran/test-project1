﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/
namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Rules.Model
{
    /// <summary>
    /// Payment Rule Model
    /// </summary>
    public class PaymentRule
    {
        /// <summary>
        /// Provider Type
        /// </summary>
        public String ProviderType { get; set; }
        /// <summary>
        /// Portal
        /// </summary>
        public string Portal { get; set; }
        /// <summary>
        /// Payment Type
        /// </summary>
        public string PaymentType{ get; set; }
        /// <summary>
        /// Payment Method
        /// </summary>
        public string PaymentMethod { get; set; }
        /// <summary>
        /// Check OfferOverrides
        /// </summary>
        public bool OfferOverrides { get; set; }
    }
}
