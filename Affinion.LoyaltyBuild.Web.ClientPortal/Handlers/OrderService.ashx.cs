﻿using service = Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Web.Script.Serialization;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using System.IO;
using System.Text;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Handlers
{
    /// <summary>
    /// Summary description for Handler1
    /// </summary>
    public class OrderService : IHttpHandler, IRequiresSessionState
    {
        private service.IOrderService _orderService;

        /// <summary>
        /// Constructor
        /// </summary>
        public OrderService()
        {
            _orderService = ContainerFactory.Instance.GetInstance<service.IOrderService>();
        }

        /// <summary>
        /// Process request
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context)
        {
            string request = GetDocumentContents(context.Request);
            string response = string.Empty;
            
            context.Response.ContentType = "text/json";
            switch (context.Request.QueryString["action"].ToLower())
            {
                case "cancel":
                    response = CancelOrder(request);
                    break;
                case "transfer":
                    response = TransferOrder(request);
                    break;
            }
            context.Response.Write("Hello World");
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Cancel booking
        /// </summary>
        private string CancelOrder(string content)
        {
            var serializer = new JavaScriptSerializer();
            
            var cancelInfo = serializer.Deserialize<CancelBookingInfo>(content);

            return serializer.Serialize(_orderService.CancelBooking(cancelInfo));
        }

        /// <summary>
        /// Transfer booking
        /// </summary>
        private string TransferOrder(string content)
        {
            var serializer = new JavaScriptSerializer();

            var transferInfo = serializer.Deserialize<TransferBookingInfo>(content);

            return serializer.Serialize(_orderService.TranferBooking(transferInfo));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Request"></param>
        /// <returns></returns>
        private string GetDocumentContents(System.Web.HttpRequest Request)
        {
            string documentContents;
            
            using (Stream receiveStream = Request.InputStream)
            {
                using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
                {
                    documentContents = readStream.ReadToEnd();
                }
            }
            
            return documentContents;
        }
    }
}