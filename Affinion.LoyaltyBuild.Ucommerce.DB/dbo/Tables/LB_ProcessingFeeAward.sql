﻿
CREATE TABLE [dbo].[LB_ProcessingFeeAward](
	[ProcessingFeeAwardId] [int] NOT NULL,
	[DiscountAmount] [decimal](18, 2) NOT NULL,
	[IsPercentage] [bit] NOT NULL,
	[OnOrderLine] [bit] NOT NULL,
 CONSTRAINT [PK_uCommerce_ProcessingFeeAward] PRIMARY KEY CLUSTERED 
(
	[ProcessingFeeAwardId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[LB_ProcessingFeeAward]  WITH CHECK ADD  CONSTRAINT [FK_uCommerce_ProcessingFeeAward_uCommerce_Award] FOREIGN KEY([ProcessingFeeAwardId])
REFERENCES [dbo].[uCommerce_Award] ([AwardId])
GO

ALTER TABLE [dbo].[LB_ProcessingFeeAward] CHECK CONSTRAINT [FK_uCommerce_ProcessingFeeAward_uCommerce_Award]
GO


