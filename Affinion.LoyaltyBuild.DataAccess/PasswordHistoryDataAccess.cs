﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           PasswordHistoryDataAccess.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.DataAccess
/// Description:           Data Access layer for Pasword History.
/// </summary>

#region Using Directives
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
#endregion

namespace Affinion.LoyaltyBuild.DataAccess
{
    public class PasswordHistoryDataAccess
    {
        /// <summary>
        /// Record history password data
        /// </summary>
        /// <param name="userName">user name</param>
        /// <param name="password">password</param>
        /// <returns>Return true if success false otherwise</returns>
        public bool Add(string userName, string password)
        {
            try
            {
                if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(password))
                {
                    int result;
                    string hashedPassword;
                    string hashedUserName;
                    string salt;

                    /// Get Md5 hash of the username
                    hashedUserName = Cryptography.GetMd5Hash(userName);

                    /// Generate the salt value
                    salt = Cryptography.GenerateSalt();

                    /// Get hash for the new password
                    hashedPassword = Cryptography.GetHasedValue(password, salt);

                    /// Create Password data object
                    using (var context = new AffinionDbContext())
                    {
                        PasswordHistory data = new PasswordHistory()
                        {
                            UserName = hashedUserName,
                            Password = hashedPassword,
                            PasswordSalt = salt,
                            ChangedDate = DateTime.Now
                        };

                        /// Save data
                        context.PasswordHistory.Add(data);
                        result = context.SaveChanges();
                    }

                    return result > 0;
                }
                return false;
            }
            catch (SqlException ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.DataAccess, ex, this);
                throw;
            }
            catch (Exception ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.DataAccess, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Get history password data
        /// </summary>
        /// <param name="username">user name</param>
        /// <returns>List of password record</returns>
        private List<PasswordHistory> Get(string username)
        {
            try
            {
                /// Get Md5 hash for username
                /// Commented Code to Fix Bug  : LWD-789. Hashusername not working with comparing data.
                string hashedUserName = Cryptography.GetMd5Hash(username);

                //string hashedUserName = username;
                /// Get data rows for given username
                using (var context = new AffinionDbContext())
                {
                    /// Get data rows for given user
                    IQueryable<PasswordHistory> historyPasswordData = context.PasswordHistory
                                                                    .Where(data => data.UserName == hashedUserName);

                    return historyPasswordData.ToList<PasswordHistory>();
                }

                //Commenting unreachable code
                //return null;
            }
            catch (SqlException ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.DataAccess, ex, this);
                throw;
            }
            catch (Exception ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.DataAccess, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Get count of the records
        /// </summary>
        /// <param name="username">user name</param>
        /// <returns>Count of the records</returns>
        public int Count(string username)
        {
            try
            {
                /// Get Md5 hash for username
                string hashedUserName = Cryptography.GetMd5Hash(username);

                /// Get data rows for given username
                using (var context = new AffinionDbContext())
                {
                    /// Get data rows for given user
                    IQueryable<PasswordHistory> historyPasswordData = context.PasswordHistory
                                                                    .Where(data => data.UserName == hashedUserName);
                    if (historyPasswordData != null)
                    {
                        return historyPasswordData.Count();
                    }

                }

                return 0;
            }
            catch (SqlException ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.DataAccess, ex, this);
                throw;
            }
            catch (Exception ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.DataAccess, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Check if record exist with given username and password
        /// </summary>
        /// <param name="username">user name</param>
        /// <param name="password">password</param>
        /// <returns>True if record found false otherwise</returns>

        public bool Validate(string username, string password, bool defaultReturn = true)
        {
            try
            {

                /// Get password history record
                List<PasswordHistory> historyData = this.Get(username);

                /// Check if given password exist in the passwordhistory list
                if (historyData != null && historyData.Any())
                {
                    defaultReturn = historyData
                        .OrderBy(i => i.ChangedDate)

     .Where(record => Cryptography.VerifySaltedHash(password, record.Password, record.PasswordSalt))
                        .Count() > 0;
                }

                // return false;
                return defaultReturn;

            }
            catch (SqlException ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.DataAccess, ex, this);
                throw;
            }
            catch (Exception ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.DataAccess, ex, this);
                throw;
            }
        }
        /// <summary>
        /// Delete Password history data first(oldest) record
        /// </summary>
        /// <param name="username">username</param>
        /// <returns>Return true if success false otherwise</returns>
        public bool DeleteFirst(string username)
        {
            try
            {
                /// Get Md5 hash for username
                string hashedUserName = Cryptography.GetMd5Hash(username);

                using (var context = new AffinionDbContext())
                {
                    /// Get data rows for given user
                    var rows = context.PasswordHistory.Where(data => data.UserName == hashedUserName).OrderBy(o => o.ChangedDate).FirstOrDefault();

                    if (rows != null)
                    {
                        context.PasswordHistory.Remove(rows);
                        int result = context.SaveChanges();
                        return result > 0;
                    }

                }
                return false;
            }
            catch (SqlException ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.DataAccess, ex, this);
                throw;
            }
            catch (Exception ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.DataAccess, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Delete Password history of the user
        /// </summary>
        /// <param name="username">username</param>
        /// <returns>Return true if success false otherwise</returns>
        public bool DeleteAll(string username)
        {
            try
            {
                /// Get Md5 hash for username
                string hashedUserName = Cryptography.GetMd5Hash(username);

                using (var context = new AffinionDbContext())
                {
                    /// Get data rows for given user
                    var rows = context.PasswordHistory.Where(data => data.UserName == hashedUserName);

                    if (rows != null && rows.Count() > 0)
                    {
                        context.PasswordHistory.RemoveRange(rows);
                        int result = context.SaveChanges();
                        return result > 0;
                    }

                }
                return false;
            }
            catch (SqlException ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.DataAccess, ex, this);
                throw;
            }
            catch (Exception ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.DataAccess, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Login Audit
        /// </summary>
        /// <param name="userName">user name</param>
        /// <param name="isSuccess">isSuccess</param>
        public void LoginAudit(string userName, bool isSuccess)
        {
            try
            {
                if (!string.IsNullOrEmpty(userName))
                {


                    using (var context = new AffinionDbContext())
                    {
                        LoginAudit data = new LoginAudit()
                        {
                            UserName = userName,
                            IsLoginSucess = isSuccess,
                            LoggedInDate = DateTime.UtcNow,

                        };

                        /// Save data
                        context.LoginAudit.Add(data);
                        context.SaveChanges();
                    }


                }

            }
            catch (SqlException ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.DataAccess, ex, this);
                throw;
            }
            catch (Exception ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.DataAccess, ex, this);
                throw;
            }
        }
    }
}
