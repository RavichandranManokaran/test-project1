﻿CREATE PROCEDURE [dbo].[LB_Purge_CustomerQuery]
AS
BEGIN
      --Delete record from child table
DELETE FROM LB_CustomerQuery_Attachments 
       WHERE QueryId in 
       (
       SELECT QueryId FROM LB_CustomerQuery 
       WHERE CONVERT(VARCHAR(11),CreatedDate,111) <  CONVERT(VARCHAR(11), DATEADD(mm,-12,getdate()),111)
       )
       --Delete record from  parent table
DELETE FROM LB_CustomerQuery WHERE  CONVERT(VARCHAR(11),CreatedDate,111) <  CONVERT(VARCHAR(11), DATEADD(mm,-12,getdate()),111)
END
