﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
///Source File:           uCommerceConnectionFactory.cs
///Sub-system/Module:     Affinion.LoyaltyBuild.Common.uCommerceUtility
///Description:           Entity class for Country
///</summary>
#region usingDirectives
using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.Common.Exceptions;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.SessionHelper;
using Affinion.Loyaltybuild.BusinessLogic.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Affinion.LoyaltyBuild.DataAccess.Models;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using System.Text;


#endregion

namespace Affinion.LoyaltyBuild.DataAccess.uCommerceUtility
{
    public class uCommerceConnectionFactory
    {
        public static string GetuCommerceConnetionString()
        {
            try
            {
                return (ConfigurationManager.ConnectionStrings["UCommerce"].ConnectionString.Length != 0) ? ConfigurationManager.ConnectionStrings["UCommerce"].ConnectionString : string.Empty;
                //DataConnection dataConnection = new DataConnection("ClientPortal_uCommerce");
                //return dataConnection.ConnectionString;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// get core db connection string
        /// </summary>
        /// <param name="basketInfo"></param>
        public static string GetAffinionContextConnectionString()
        {
            try
            {
                return (ConfigurationManager.ConnectionStrings["AffinionContext"].ConnectionString.Length != 0) ? ConfigurationManager.ConnectionStrings["AffinionContext"].ConnectionString : string.Empty;

            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="basketInfo"></param>
        public static void InsertCustomOrderLineData(BasketInfo basketInfo)
        {
            try
            {
                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.UpdateCustomOrderDetails, con))
                        {

                            DateTime checkout, checkin;
                            DateTime.TryParse(basketInfo.CheckoutDate, out checkout);
                            DateTime.TryParse(basketInfo.CheckinDate, out checkin);

                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@CheckinDate", SqlDbType.DateTime)).Value = checkout;
                            command.Parameters.Add(new SqlParameter("@CheckOutDate", SqlDbType.DateTime)).Value = checkin;
                            command.Parameters.Add(new SqlParameter("@NofAdults", SqlDbType.Int)).Value = 1; // Int32.Parse(basketInfo.NoOfAdults);
                            command.Parameters.Add(new SqlParameter("@NoofChilderen", SqlDbType.Int)).Value = 1; //Int32.Parse(basketInfo.NoOfChildren);
                            con.Open();
                            command.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Insert Customer
        /// </summary>
        /// <param name="customer">Customer object</param>
        /// <returns></returns>
        /// <summary>
        /// Insert Customer
        /// </summary>
        /// <param name="customer">Customer object</param>
        /// <returns></returns>
        public static int AddCustomer(Customer customer)
        {
            try
            {
                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.AddCustomer, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@FirstName", SqlDbType.NVarChar)).Value = customer.FirstName;
                            command.Parameters.Add(new SqlParameter("@LastName", SqlDbType.NVarChar)).Value = customer.LastName;
                            command.Parameters.Add(new SqlParameter("@EmailAddress", SqlDbType.NVarChar)).Value = customer.EmailAddress;
                            command.Parameters.Add(new SqlParameter("@MobilePhone", SqlDbType.NVarChar)).Value = customer.MobilePhone;
                            command.Parameters.Add(new SqlParameter("@AddressLine1", SqlDbType.NVarChar)).Value = customer.AddressLine1;
                            command.Parameters.Add(new SqlParameter("@AddressLine2", SqlDbType.NVarChar)).Value = customer.AddressLine2;
                            command.Parameters.Add(new SqlParameter("@AddressLine3", SqlDbType.NVarChar)).Value = customer.AddressLine3;
                            command.Parameters.Add(new SqlParameter("@Phone", SqlDbType.NVarChar)).Value = customer.Phone;
                            command.Parameters.Add(new SqlParameter("@NewCustomerId", SqlDbType.Int)).Value = customer.CustomerId;
                            command.Parameters.Add(new SqlParameter("@LanguageName", SqlDbType.NVarChar)).Value = customer.Language;
                            command.Parameters.Add(new SqlParameter("@ClientId", SqlDbType.VarChar)).Value = customer.ClientId;
                            command.Parameters.Add(new SqlParameter("@Title", SqlDbType.NVarChar)).Value = customer.Title;
                            command.Parameters.Add(new SqlParameter("@CountryId", SqlDbType.Int)).Value = customer.CountryId;
                            command.Parameters.Add(new SqlParameter("@LocationId", SqlDbType.VarChar)).Value = customer.LocationId;
                            command.Parameters.Add(new SqlParameter("@BranchId", SqlDbType.VarChar)).Value = customer.BranchId;
                            command.Parameters.Add(new SqlParameter("@BookingReferenceId", SqlDbType.VarChar)).Value = GenerateBookingReferance();
                            command.Parameters.Add(new SqlParameter("@Subscribe", SqlDbType.Bit)).Value = customer.Subscribe;
                            command.Parameters.Add(new SqlParameter("@Active", SqlDbType.Bit)).Value = customer.Active;
                            command.Parameters.Add(new SqlParameter("@SubscribeType", SqlDbType.VarChar)).Value = customer.SubscribeType;
                            command.Parameters.Add(new SqlParameter("@Store", SqlDbType.VarChar)).Value = customer.Store;
                            command.Parameters.Add(new SqlParameter("@TermsandCondition", SqlDbType.Bit)).Value = customer.TermsandCondition;
                            con.Open();
                            int? customerId = (int?)command.ExecuteScalar();
                            return (customerId.HasValue) ? customerId.Value : 0;
                        }
                    }
                }
                return 0;
            }
            catch (Exception exception)
            {

                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }


        /// <summary>
        /// Generate Booking Referance - 16 digit Random number 
        /// </summary>
        /// <returns>Booking Referance Number</returns>
        public static string GenerateBookingReferance()
        {
            Random RNG = new Random();
            var builder = new StringBuilder();
            while (builder.Length < 16)
            {
                builder.Append(RNG.Next(10).ToString());
            }
            return builder.ToString();
        }


        public static void AddOrderLineSubrates(DataTable subrateTable, int orderId)
        {
            try
            {
                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand("LB_AddOrderLineSubrates", con))
                        {
                            command.CommandType = CommandType.StoredProcedure;

                            SqlParameter tvpParam = command.Parameters.AddWithValue("@SubrateTVP", subrateTable);
                            tvpParam.SqlDbType = SqlDbType.Structured;

                            SqlParameter orderIdParam = command.Parameters.AddWithValue("@orderId", orderId);
                            orderIdParam.SqlDbType = SqlDbType.Int;

                            con.Open();
                            command.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                throw new AffinionException("Error when adding subrates", exception);
            }
        }

        /// <summary>
        /// Update Customer
        /// </summary>
        /// <param name="customer">Customer Object</param>
        /// <returns></returns>
        public static int UpdateCustomer(Customer customer)
        {
            try
            {
                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.UpdateCustomer, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@FirstName", SqlDbType.NVarChar)).Value = customer.FirstName ?? string.Empty;
                            command.Parameters.Add(new SqlParameter("@LastName", SqlDbType.NVarChar)).Value = customer.LastName ?? string.Empty;
                            command.Parameters.Add(new SqlParameter("@EmailAddress", SqlDbType.NVarChar)).Value = customer.EmailAddress ?? string.Empty;
                            command.Parameters.Add(new SqlParameter("@Phone", SqlDbType.NVarChar)).Value = customer.Phone ?? string.Empty;
                            command.Parameters.Add(new SqlParameter("@MobilePhone", SqlDbType.NVarChar)).Value = customer.MobilePhone ?? string.Empty;
                            command.Parameters.Add(new SqlParameter("@AddressLine1", SqlDbType.NVarChar)).Value = customer.AddressLine1 ?? string.Empty;
                            command.Parameters.Add(new SqlParameter("@AddressLine2", SqlDbType.NVarChar)).Value = customer.AddressLine2 ?? string.Empty;
                            command.Parameters.Add(new SqlParameter("@AddressLine3", SqlDbType.NVarChar)).Value = customer.AddressLine3 ?? string.Empty;
                            command.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.NVarChar)).Value = customer.CustomerId;
                            command.Parameters.Add(new SqlParameter("@LanguageName", SqlDbType.NVarChar)).Value = customer.Language ?? string.Empty;
                            command.Parameters.Add(new SqlParameter("@ClientId", SqlDbType.VarChar)).Value = customer.ClientId ?? string.Empty;
                            command.Parameters.Add(new SqlParameter("@Title", SqlDbType.NVarChar)).Value = customer.Title ?? string.Empty;
                            command.Parameters.Add(new SqlParameter("@CountryId", SqlDbType.Int)).Value = customer.CountryId;
                            command.Parameters.Add(new SqlParameter("@LocationId", SqlDbType.VarChar)).Value = customer.LocationId;
                            command.Parameters.Add(new SqlParameter("@BranchId", SqlDbType.NVarChar)).Value = customer.BranchId ?? string.Empty;
                            command.Parameters.Add(new SqlParameter("@Subscribe", SqlDbType.Bit)).Value = customer.Subscribe;
                            command.Parameters.Add(new SqlParameter("@Active", SqlDbType.Bit)).Value = customer.Active;
                            command.Parameters.Add(new SqlParameter("@SubscribeType", SqlDbType.VarChar)).Value = customer.SubscribeType;
                            command.Parameters.Add(new SqlParameter("@Store", SqlDbType.VarChar)).Value = customer.Store;
                            command.Parameters.Add(new SqlParameter("@TermsandCondition", SqlDbType.Bit)).Value = customer.TermsandCondition;
                            con.Open();
                            return command.ExecuteNonQuery();
                        }
                    }
                }
                return 0;
            }
            catch (Exception exception)
            {

                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }


        /// <summary>
        /// UpdateCustomOrderLineData
        /// </summary>
        /// <param name="basketInfo"></param>
        /// <param name="id"></param>
        public static void UpdateCustomOrderLineData(BasketInfo basketInfo, int id)
        {
            try
            {
                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.UpdateCustomOrderDetails, con))
                        {

                            DateTime checkout, checkin;
                            DateTime.TryParse(basketInfo.CheckoutDate, out checkout);
                            DateTime.TryParse(basketInfo.CheckinDate, out checkin);
                            int NoOfAdults = 0;
                            int NoOfChildren = 0;

                            bool isValid = Int32.TryParse(basketInfo.NoOfAdults, out NoOfAdults);
                            isValid = Int32.TryParse(basketInfo.NoOfChildren, out NoOfChildren);

                            checkin = Affinion.LoyaltyBuild.Common.Utilities.DateTimeHelper.ParseDate(basketInfo.CheckinDate);
                            checkout = Affinion.LoyaltyBuild.Common.Utilities.DateTimeHelper.ParseDate(basketInfo.CheckoutDate);

                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@CheckinDate", SqlDbType.DateTime)).Value = checkin;
                            command.Parameters.Add(new SqlParameter("@CheckOutDate", SqlDbType.DateTime)).Value = checkout;
                            command.Parameters.Add(new SqlParameter("@NofAdults", SqlDbType.Int)).Value = NoOfAdults;
                            command.Parameters.Add(new SqlParameter("@NoofChilderen", SqlDbType.Int)).Value = NoOfChildren;
                            command.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int)).Value = id; //Int32.Parse(basketInfo.NoOfChildren);
                            command.Parameters.Add(new SqlParameter("@Sku", SqlDbType.VarChar)).Value = basketInfo.Sku; //Int32.Parse(basketInfo.NoOfChildren);
                            con.Open();
                            command.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        public static void AddRoomReservation(BasketInfo basketInfo, int id)
        {

            try
            {
                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.AddRoomReservation, con))
                        {

                            DateTime checkout, checkin;
                            DateTime.TryParse(basketInfo.CheckoutDate, out checkout);
                            DateTime.TryParse(basketInfo.CheckinDate, out checkin);
                            checkin = Affinion.LoyaltyBuild.Common.Utilities.DateTimeHelper.ParseDate(basketInfo.CheckinDate);
                            checkout = Affinion.LoyaltyBuild.Common.Utilities.DateTimeHelper.ParseDate(basketInfo.CheckoutDate);

                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@CheckinDate", SqlDbType.DateTime)).Value = checkin;
                            command.Parameters.Add(new SqlParameter("@CheckOutDate", SqlDbType.DateTime)).Value = checkout;
                            command.Parameters.Add(new SqlParameter("@OrderLineId", SqlDbType.Int)).Value = id;
                            command.Parameters.Add(new SqlParameter("@BookingThrough", SqlDbType.VarChar)).Value = basketInfo.BookingThrough;
                            command.Parameters.Add(new SqlParameter("@CreatedByName", SqlDbType.VarChar)).Value = string.IsNullOrEmpty(basketInfo.CreatedBy) ? string.Empty : basketInfo.CreatedBy;
                            con.Open();
                            command.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }

        }

        public static void AddBookingInfo(int orderLineId)
        {
            try
            {
                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.AddBookinginfo, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@OrderLineId", SqlDbType.Int)).Value = orderLineId;
                            con.Open();
                            command.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }

        }

        /// <summary>
        /// GetPurchaseOrderLines
        /// </summary>
        /// <param name="id"></param>
        public static DataSet GetPurchaseOrderLines(int id)
        {
            try
            {
                DataSet ds = new DataSet();

                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        string sql = "LB_GetPurchaseOrderDetails";

                        using (SqlCommand command = new SqlCommand(sql, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int)).Value = id; //Int32.Parse(basketInfo.NoOfChildren);
                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                        }
                    }
                }

                return ds;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }


        /// <summary>
        /// GetCategoryIDByCatalogIDAndMatchingText
        /// </summary>
        /// <param name="id"></param>
        public static int GetCategoryIDByCatalogIDAndMatchingText(int catalogID, string inputText)
        {
            try
            {
                int categoryid = 0;

                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        string sql = "LB_GetCategoryIDByCatalogIDAndMatchingText";
                        con.Open();
                        using (SqlCommand command = new SqlCommand(sql, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@CatalogID", SqlDbType.Int)).Value = catalogID;
                            command.Parameters.Add(new SqlParameter("@MatchingText", SqlDbType.VarChar, 100)).Value = inputText;

                            using (var reader = command.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        categoryid = reader.GetInt32(reader.GetOrdinal("CategoryId"));
                                    }
                                }
                            }
                            con.Close();
                        }
                    }
                }

                return categoryid;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }


        /// <summary>
        /// Get Existing Customer
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="email"></param>
        public static Customer GetCustomer(string clientId = null, string email = null, int CustomerId = 0)
        {
            try
            {
                Customer objCustomer = new Customer();
                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.GetCustomerDetailes, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;

                            if (clientId != null)
                                command.Parameters.Add(new SqlParameter("@ClientId", SqlDbType.VarChar)).Value = clientId;
                            if (email != null)
                                command.Parameters.Add(new SqlParameter("@Email", SqlDbType.VarChar)).Value = email;
                            if (CustomerId != 0)
                                command.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int)).Value = CustomerId;
                            con.Open();

                            using (var reader = command.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        objCustomer.CustomerId = reader["CustomerId"] != DBNull.Value ? Convert.ToInt32(reader["CustomerId"]) : default(int);
                                        objCustomer.FirstName = reader["FirstName"] != DBNull.Value ? Convert.ToString(reader["FirstName"]) : string.Empty;
                                        objCustomer.LastName = reader["LastName"] != DBNull.Value ? Convert.ToString(reader["LastName"]) : string.Empty;
                                        objCustomer.EmailAddress = reader["EmailAddress"] != DBNull.Value ? Convert.ToString(reader["EmailAddress"]) : string.Empty;
                                        objCustomer.Phone = reader["PhoneNumber"] != DBNull.Value ? Convert.ToString(reader["PhoneNumber"]) : string.Empty;
                                        objCustomer.MobilePhone = reader["MobilePhoneNumber"] != DBNull.Value ? Convert.ToString(reader["MobilePhoneNumber"]) : string.Empty;
                                        objCustomer.ClientId = reader["ClientId"] != DBNull.Value ? Convert.ToString(reader["ClientId"]) : string.Empty;
                                        objCustomer.Title = reader["Title"] != DBNull.Value ? Convert.ToString(reader["Title"]) : string.Empty;
                                        objCustomer.AddressLine1 = reader["AddressLine1"] != DBNull.Value ? Convert.ToString(reader["AddressLine1"]) : string.Empty;
                                        objCustomer.AddressLine2 = reader["AddressLine2"] != DBNull.Value ? Convert.ToString(reader["AddressLine2"]) : string.Empty;
                                        objCustomer.AddressLine3 = reader["AddressLine3"] != DBNull.Value ? Convert.ToString(reader["AddressLine3"]) : string.Empty;
                                        objCustomer.CountryId = reader["CountryId"] != DBNull.Value ? Convert.ToInt32(reader["CountryId"]) : default(int);
                                        objCustomer.LocationId = reader["LocationId"] != DBNull.Value ? Convert.ToString(reader["LocationId"]) : string.Empty;
                                        objCustomer.Language = reader["LanguageName"] != DBNull.Value ? Convert.ToString(reader["LanguageName"]) : string.Empty;
                                        objCustomer.BranchId = reader["BranchId"] != DBNull.Value ? Convert.ToString(reader["BranchId"]) : string.Empty;
                                        objCustomer.Subscribe = reader["Subscribe"] != DBNull.Value ? Convert.ToInt32(reader["Subscribe"]) : default(int);
                                        objCustomer.BookingReferenceId = reader["BookingReference"] != DBNull.Value ? Convert.ToString(reader["BookingReference"]) : string.Empty;
                                        objCustomer.SubscribeType = reader["SubscribeType"] != DBNull.Value ? Convert.ToString(reader["SubscribeType"]) : string.Empty;
                                        objCustomer.Store = reader["Store"] != DBNull.Value ? Convert.ToInt32(reader["Store"]) : default(int);
                                        objCustomer.TermsandCondition = reader["TermsandCondition"] != DBNull.Value ? Convert.ToInt32(reader["TermsandCondition"]) : default(int);
                                        return objCustomer;
                                    }
                                }
                                reader.Close();
                            }
                            con.Close();

                        }
                    }
                }
                return objCustomer;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }

        }

        public static BookingReference GetCustomerBookingReferenceDetails(int customerId)
        {
            try
            {
                BookingReference bookingReferenceDetails = new BookingReference();
                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.GetCustomerBookingReferenceDetails, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int)).Value = customerId;
                            con.Open();

                            using (var reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    bookingReferenceDetails.BookingReferenceId = reader.GetString(reader.GetOrdinal("BookingReferenceId"));
                                    bookingReferenceDetails.LastSequence = reader.GetInt32(reader.GetOrdinal("LastSequence")); ;

                                }
                            }
                        }
                    }
                }
                return bookingReferenceDetails;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        public static void SetBookingReferenceNumber(int orderLineId, string bookingReferenceNumber)
        {

            try
            {
                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.SetBookingReferenceNumber, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@OrderLineId", SqlDbType.Int)).Value = orderLineId;
                            command.Parameters.Add(new SqlParameter("@BookingReference", SqlDbType.VarChar)).Value = bookingReferenceNumber;
                            con.Open();
                            command.ExecuteNonQuery();
                            con.Close();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }

        }

        public static void UpdateBookingReferenceSequence(int customerId, int sequence)
        {
            try
            {
                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.UpdateCustomersBookingReferenceSequence, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int)).Value = customerId;
                            command.Parameters.Add(new SqlParameter("@LastSequence", SqlDbType.Int)).Value = sequence;

                            con.Open();
                            command.ExecuteNonQuery();
                            con.Close();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }

        }

        public static List<Subrates> GetOrderlineSubrates(int orderLineId)
        {
            List<Subrates> subratesList = new List<Subrates>();
            if (GetuCommerceConnetionString().Length > 0)
            {
                using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                {
                    using (SqlCommand command = new SqlCommand("LB_GetOrderLineSubrates", con))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new SqlParameter("@orderLineId", SqlDbType.Int)).Value = orderLineId;
                        con.Open();

                        using (var reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {

                                while (reader.Read())
                                {
                                    Subrates subrate = new Subrates();
                                    subrate.SubrateCode = reader.GetString(reader.GetOrdinal("SubrateItemCode"));
                                    subrate.TotalAmount = reader.GetDecimal(reader.GetOrdinal("Amount"));
                                    subratesList.Add(subrate);
                                }
                            }
                        }
                    }
                }
            }
            return subratesList;
        }

        /// <summary>
        /// Check IsExistingCustomer
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="bookingReferenceId"></param>
        public static bool IsExistingCustomer(string clientId, string email)
        {
            try
            {
                int? customerId = GetExistingCustomerId(clientId, email);
                return (customerId.HasValue) ? true : false;

            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }


        }

        /// <summary>
        /// Check Existing Customer Id
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="bookingReferenceId"></param>
        public static int? GetExistingCustomerId(string clientId, string email)
        {
            try
            {
                int? customerId = null;
                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.GetActiveCustomerIdForClient, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@ClientId", SqlDbType.VarChar)).Value = clientId;
                            command.Parameters.Add(new SqlParameter("@Email", SqlDbType.VarChar)).Value = email;
                            con.Open();
                            customerId = (int?)command.ExecuteScalar();
                            con.Close();


                        }
                    }
                }
                return customerId;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }


        }

        public static DataSet GetOrderLines(int orderId)
        {
            try
            {
                DataSet ds = new DataSet();

                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        string sql = "LB_GetOrderLines";

                        using (SqlCommand command = new SqlCommand(sql, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@OrderId", SqlDbType.Int)).Value = orderId; //Int32.Parse(basketInfo.NoOfChildren);
                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                        }
                    }
                }

                return ds;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }


        /// <summary>
        /// Get Customer Details
        /// </summary>
        /// <param name="customer"> Customer object</param>
        /// <param name="clientIds">Client Id or multiple "," separated Ids</param>
        /// <returns>Client List data set</returns>
        public static DataSet GetCustomerDetails(Customer customer)
        {
            try
            {
                DataSet ds = new DataSet();

                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.GetCustomerList, con))
                        {
                            if (customer != null)
                            {
                                //Since @clientIds expect a client Id or several "," separated Ids, use the clientIds parameter instead of customer.ClientId
                                command.CommandType = CommandType.StoredProcedure;
                                command.Parameters.Add(new SqlParameter("@ClientIds", SqlDbType.VarChar)).Value = string.IsNullOrEmpty(customer.ClientId) ? null : customer.ClientId;
                                command.Parameters.Add(new SqlParameter("@FirstName", SqlDbType.NVarChar)).Value = string.IsNullOrEmpty(customer.FirstName) ? null : customer.FirstName;
                                command.Parameters.Add(new SqlParameter("@LastName", SqlDbType.NVarChar)).Value = string.IsNullOrEmpty(customer.LastName) ? null : customer.LastName;
                                command.Parameters.Add(new SqlParameter("@EmailAddress", SqlDbType.NVarChar)).Value = string.IsNullOrEmpty(customer.EmailAddress) ? null : customer.EmailAddress;
                                command.Parameters.Add(new SqlParameter("@MobilePhone", SqlDbType.NVarChar)).Value = string.IsNullOrEmpty(customer.MobilePhone) ? null : customer.MobilePhone;
                                command.Parameters.Add(new SqlParameter("@Phone", SqlDbType.NVarChar)).Value = string.IsNullOrEmpty(customer.Phone) ? null : customer.Phone;
                                command.Parameters.Add(new SqlParameter("@Title", SqlDbType.NVarChar)).Value = string.IsNullOrEmpty(customer.Title) ? null : customer.Title;
                                command.Parameters.Add(new SqlParameter("@AddressLine1", SqlDbType.NVarChar)).Value = string.IsNullOrEmpty(customer.AddressLine1) ? null : customer.AddressLine1;
                                command.Parameters.Add(new SqlParameter("@AddressLine2", SqlDbType.NVarChar)).Value = string.IsNullOrEmpty(customer.AddressLine2) ? null : customer.AddressLine2;
                                command.Parameters.Add(new SqlParameter("@AddressLine3", SqlDbType.NVarChar)).Value = string.IsNullOrEmpty(customer.AddressLine3) ? null : customer.AddressLine3;
                                command.Parameters.Add(new SqlParameter("@CountryId", SqlDbType.Int)).Value = customer.CountryId > 0 ? customer.CountryId : (int?)null;
                                command.Parameters.Add(new SqlParameter("@LocationId", SqlDbType.Int)).Value =!string.IsNullOrEmpty(customer.LocationId)? customer.LocationId : null;
                                command.Parameters.Add(new SqlParameter("@BranchId", SqlDbType.VarChar)).Value = string.IsNullOrEmpty(customer.BranchId) ? null : customer.BranchId;
                            }

                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                        }
                    }
                }

                return ds;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }


        /// <summary>
        /// Get Customer Details
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public static List<Customer> GetCustomerList(Customer customer)
        {
            try
            {
                List<Customer> lstCustomer = new List<Customer>();

                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.GetCustomerList, con))
                        {
                            if (customer != null)
                            {
                                //Since @clientIds expect a client Id or several "," separated Ids, use the clientIds parameter instead of customer.ClientId
                                command.CommandType = CommandType.StoredProcedure;
                                command.Parameters.Add(new SqlParameter("@ClientIds", SqlDbType.VarChar)).Value = string.IsNullOrEmpty(customer.ClientId) ? null : customer.ClientId;
                                command.Parameters.Add(new SqlParameter("@FirstName", SqlDbType.NVarChar)).Value = string.IsNullOrEmpty(customer.FirstName) ? null : customer.FirstName;
                                command.Parameters.Add(new SqlParameter("@LastName", SqlDbType.NVarChar)).Value = string.IsNullOrEmpty(customer.LastName) ? null : customer.LastName;
                                command.Parameters.Add(new SqlParameter("@EmailAddress", SqlDbType.NVarChar)).Value = string.IsNullOrEmpty(customer.EmailAddress) ? null : customer.EmailAddress;
                                command.Parameters.Add(new SqlParameter("@MobilePhone", SqlDbType.NVarChar)).Value = string.IsNullOrEmpty(customer.MobilePhone) ? null : customer.MobilePhone;
                                command.Parameters.Add(new SqlParameter("@Phone", SqlDbType.NVarChar)).Value = string.IsNullOrEmpty(customer.Phone) ? null : customer.Phone;
                                command.Parameters.Add(new SqlParameter("@Title", SqlDbType.NVarChar)).Value = string.IsNullOrEmpty(customer.Title) ? null : customer.Title;
                                command.Parameters.Add(new SqlParameter("@AddressLine1", SqlDbType.NVarChar)).Value = string.IsNullOrEmpty(customer.AddressLine1) ? null : customer.AddressLine1;
                                command.Parameters.Add(new SqlParameter("@AddressLine2", SqlDbType.NVarChar)).Value = string.IsNullOrEmpty(customer.AddressLine2) ? null : customer.AddressLine2;
                                command.Parameters.Add(new SqlParameter("@AddressLine3", SqlDbType.NVarChar)).Value = string.IsNullOrEmpty(customer.AddressLine3) ? null : customer.AddressLine3;
                                command.Parameters.Add(new SqlParameter("@CountryId", SqlDbType.Int)).Value = customer.CountryId > 0 ? customer.CountryId : (int?)null;
                                command.Parameters.Add(new SqlParameter("@LocationId", SqlDbType.VarChar)).Value = !string.IsNullOrEmpty(customer.LocationId) ? customer.LocationId : null;
                                command.Parameters.Add(new SqlParameter("@BranchId", SqlDbType.VarChar)).Value = string.IsNullOrEmpty(customer.BranchId) ? null : customer.BranchId;
                            }

                            con.Open();
                            using (var reader = command.ExecuteReader())
                            {
                                if (reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        var objCustomer = new Customer();
                                        objCustomer.CustomerId = reader["CustomerId"] != DBNull.Value ? Convert.ToInt32(reader["CustomerId"]) : default(int);
                                        objCustomer.FullName = reader["FullName"] != DBNull.Value ? Convert.ToString(reader["FullName"]) : string.Empty;
                                        objCustomer.FullAddress = reader["FullAddress"] != DBNull.Value ? Convert.ToString(reader["FullAddress"]) : string.Empty;
                                        objCustomer.EmailAddress = reader["EmailAddress"] != DBNull.Value ? Convert.ToString(reader["EmailAddress"]) : string.Empty;
                                        objCustomer.MobilePhoneNumber = reader["MobilePhoneNumber"] != DBNull.Value ? Convert.ToString(reader["MobilePhoneNumber"]) : string.Empty;
                                        lstCustomer.Add(objCustomer);
                                    }
                                }
                                reader.Close();
                            }
                            con.Close();
                        }
                    }
                }

                return lstCustomer;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        //This is temporary  overload to avoid build break - will be deleted 
        public static DataSet GetCustomerDetailes(Customer customer, string clientIds)
        {
            customer.ClientId = clientIds;
            return GetCustomerDetails(customer);
        }

        public static List<Customer> GetCustomerList(Customer customer, string clientIds)
        {
            customer.ClientId = clientIds;
            return GetCustomerList(customer);
        }

        //TODO: Remove this method after correcting the UI
        /// <summary>
        /// 
        /// </summary>
        /// <param name="firstname"></param>
        /// <param name="lastname"></param>
        /// <param name="addressLine1"></param>
        /// <param name="primaryPhone"></param>
        /// <param name="countryId"></param>
        /// <param name="locationId"></param>
        /// <returns></returns>
        public static DataSet GetCustomerDetailes(string firstname, string lastname, string addressLine1, string primaryPhone, int? countryId, int? locationId, string clientIds)
        {
            try
            {
                DataSet ds = new DataSet();

                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.GetCustomerList, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@firstname", SqlDbType.NVarChar)).Value = firstname;
                            command.Parameters.Add(new SqlParameter("@lastname", SqlDbType.NVarChar)).Value = lastname;
                            command.Parameters.Add(new SqlParameter("@locationId", SqlDbType.Int)).Value = locationId;
                            command.Parameters.Add(new SqlParameter("@addressLine1", SqlDbType.NVarChar)).Value = string.IsNullOrEmpty(addressLine1) ? null : addressLine1;
                            command.Parameters.Add(new SqlParameter("@primaryPhone", SqlDbType.NVarChar)).Value = string.IsNullOrEmpty(primaryPhone) ? null : primaryPhone;
                            command.Parameters.Add(new SqlParameter("@countryId", SqlDbType.Int)).Value = countryId;
                            command.Parameters.Add(new SqlParameter("@clientIds", SqlDbType.VarChar)).Value = string.IsNullOrEmpty(clientIds) ? null : clientIds;

                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                        }
                    }
                }

                return ds;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        //TODO: Remove this after correcting the 


        /// <summary>
        /// 
        /// </summary>
        /// <param name="booking"></param>
        /// <returns></returns>
        public static DataSet GetBookingDetailes(SearchBooking booking, string userLoggedIn)
        {
            try
            {
                DataSet ds = new DataSet();

                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        string sql = Constants.GetExistingBookings;

                        using (SqlCommand command = new SqlCommand(sql, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            DateTime dateTimeDefault = new DateTime();

                            if (booking != null)
                            {
                                BookingDetails(booking, command, dateTimeDefault);

                                command.Parameters.Add(new SqlParameter("@CreatedByUser", SqlDbType.VarChar)).Value = userLoggedIn;
                                //Ignore order default(0) status 
                                if (booking.BookingStatus > 0)
                                {
                                    command.Parameters.Add(new SqlParameter("@orderStatusId", SqlDbType.Int)).Value = booking.BookingStatus;

                                }
                            }

                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                            adapter.Dispose();
                        }
                    }
                }

                return ds;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        private static void BookingDetails(SearchBooking booking, SqlCommand command, DateTime dateTimeDefault)
        {
            command.Parameters.Add(new SqlParameter("@clientIds", SqlDbType.NVarChar)).Value = string.IsNullOrEmpty(booking.Client) ? null : booking.Client;
            command.Parameters.Add(new SqlParameter("@firstname", SqlDbType.NVarChar)).Value = string.IsNullOrEmpty(booking.FirstName) ? null : booking.FirstName;
            command.Parameters.Add(new SqlParameter("@lastname", SqlDbType.NVarChar)).Value = string.IsNullOrEmpty(booking.LastName) ? null : booking.LastName;
            command.Parameters.Add(new SqlParameter("@providerName", SqlDbType.NVarChar)).Value = string.IsNullOrEmpty(booking.Provider) ? null : booking.Provider;

            //if DateTime is default(1/1/0001 12:00:00 AM) pass null
            command.Parameters.Add(new SqlParameter("@reservationDate", SqlDbType.DateTime)).Value = booking.ReservationDate == dateTimeDefault ? null : booking.ReservationDate.Date.ToString("yyyy-MM-dd HH:mm:ss");
            command.Parameters.Add(new SqlParameter("@reservationDateFrom", SqlDbType.DateTime)).Value = booking.ReservationFrom == dateTimeDefault ? null : booking.ReservationFrom.ToString("yyyy-MM-dd HH:mm:ss");
            command.Parameters.Add(new SqlParameter("@reservationDateTo", SqlDbType.DateTime)).Value = booking.ReservationTo == dateTimeDefault ? null : booking.ReservationTo.Date.ToString("yyyy-MM-dd HH:mm:ss");
            command.Parameters.Add(new SqlParameter("@arrivalDate", SqlDbType.DateTime)).Value = booking.ArrivalDate == dateTimeDefault ? null : booking.ArrivalDate.Date.ToString("yyyy-MM-dd HH:mm:ss");

            command.Parameters.Add(new SqlParameter("@bookingReference", SqlDbType.NVarChar)).Value = string.IsNullOrEmpty(booking.BookingReference) ? null : booking.BookingReference;
            command.Parameters.Add(new SqlParameter("@EmailValidation", SqlDbType.VarChar)).Value = string.IsNullOrEmpty(booking.EmailValidation) ? null : booking.EmailValidation;
            command.Parameters.Add(new SqlParameter("@NumericValidation", SqlDbType.VarChar)).Value = string.IsNullOrEmpty(booking.NumericValidation) ? null : booking.NumericValidation;

            command.Parameters.Add(new SqlParameter("@bookingMethod", SqlDbType.VarChar)).Value = string.IsNullOrEmpty(booking.BookingMethod) ? null : booking.BookingMethod;
        }

        /// <summary>
        /// Booking Status
        /// </summary>
        /// <returns></returns>
        public static DataSet GetBookingStatus()
        {
            try
            {
                DataSet ds = new DataSet();

                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        string sql = Constants.GetBookingStatusList;

                        using (SqlCommand command = new SqlCommand(sql, con))
                        {
                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                        }
                    }
                }

                return ds;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Get Customer Detailes by ID
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <returns></returns>
        public static DataSet GetCustomerDetailesById(int customerId)
        {
            try
            {
                DataSet ds = new DataSet();

                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        //string sql = "GetCustomerDetailesByCustomerId";

                        using (SqlCommand command = new SqlCommand(Constants.GetCustomerDetailes, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@customerId", SqlDbType.Int)).Value = customerId;
                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                        }
                    }
                }

                return ds;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static DataSet GetLocationList()
        {
            try
            {
                DataSet ds = new DataSet();

                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        string sql = "LB_GetLocationList";

                        using (SqlCommand command = new SqlCommand(sql, con))
                        {
                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                        }
                    }
                }

                return ds;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        public static void UpdateRoomAvailability(int orderLineId, int checkInCounter, int checkOutCounter, string sku, string variantSku, int campaignId, bool isBaseProduct)
        {
            if (GetuCommerceConnetionString().Length > 0)
            {
                using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                {
                    string sql = isBaseProduct ? "LB_UpdateAvailability" : "LB_UpdateOfferAvailability";

                    using (SqlCommand command = new SqlCommand(sql, con))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new SqlParameter("@OrderLineId", SqlDbType.Int)).Value = orderLineId;
                        command.Parameters.Add(new SqlParameter("@CheckInDateCount", SqlDbType.Int)).Value = checkInCounter;
                        command.Parameters.Add(new SqlParameter("@CheckOutDateCount", SqlDbType.Int)).Value = checkOutCounter;
                        command.Parameters.Add(new SqlParameter("@Sku", SqlDbType.NVarChar)).Value = sku;
                        command.Parameters.Add(new SqlParameter("@VariantSku", SqlDbType.NVarChar)).Value = variantSku;
                        //command.Parameters.Add(new SqlParameter("@Quantity", SqlDbType.Int)).Value = quantity;
                        if (!isBaseProduct)
                        {
                            command.Parameters.Add(new SqlParameter("@CampaignID", SqlDbType.Int)).Value = campaignId;
                        }
                        con.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
        }

        public static void UpdateDuesAndPayments(int orderId, bool isPaidInFull)
        {
            if (GetuCommerceConnetionString().Length > 0)
            {
                using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                {
                    string sql = "LB_ProcessOLDueAndOLPayments";

                    using (SqlCommand command = new SqlCommand(sql, con))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new SqlParameter("@OrderId", SqlDbType.Int)).Value = orderId;
                        command.Parameters.Add(new SqlParameter("@IsPartialPayment", SqlDbType.Bit)).Value = !isPaidInFull;
                        con.Open();
                        command.ExecuteNonQuery();
                    }
                }
            }
        }

        /// <summary>
        /// GetPurchaseOrderLines
        /// </summary>
        /// <param name="id"></param>
        public static DataSet GetOrderReservation(int id)
        {
            try
            {
                DataSet ds = new DataSet();

                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.GetBookingConfirmationDetails, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int)).Value = id; //Int32.Parse(basketInfo.NoOfChildren);
                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                        }
                    }
                }

                return ds;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// GetRoomReservationAgeInfos
        /// </summary>
        /// <param name="id"></param>
        public static DataSet GetRoomReservationAgeInfo(int id)
        {
            try
            {
                DataSet ds = new DataSet();

                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.GetRoomReservationAgeInfo, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int)).Value = id; //Int32.Parse(basketInfo.NoOfChildren);
                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                        }
                    }
                }

                return ds;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Get reciver mail
        /// </summary>
        /// <param name="id"></param>
        public static DataSet GetReciverMail(int id)
        {
            try
            {
                DataSet ds = new DataSet();

                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.GetReciverMail, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int)).Value = id; //Int32.Parse(basketInfo.NoOfChildren);
                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                        }
                    }
                }

                return ds;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// GetPurchaseOrderLines
        /// </summary>
        /// <param name="id"></param>
        public static bool ValidateBookingAvailability(int orderId, int orderLineId, int checkInDate, int checkOutDate, string sku, int campaignId)
        {
            bool flag = true;
            try
            {
                DataSet ds = new DataSet();

                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        string sql = campaignId == 0 ? "LB_ValidateAvailability" : "LB_ValidateOfferAvailability";

                        using (SqlCommand command = new SqlCommand(sql, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@OrderId", SqlDbType.Int)).Value = orderId;
                            command.Parameters.Add(new SqlParameter("@OrderLineId", SqlDbType.Int)).Value = orderLineId;
                            command.Parameters.Add(new SqlParameter("@CheckInDateCount", SqlDbType.Int)).Value = checkInDate;
                            command.Parameters.Add(new SqlParameter("@CheckOutDateCount", SqlDbType.Int)).Value = checkOutDate;
                            command.Parameters.Add(new SqlParameter("@Sku", SqlDbType.VarChar)).Value = sku;

                            if (campaignId > 0)
                            {
                                command.Parameters.Add(new SqlParameter("@CampaignID", SqlDbType.Int)).Value = campaignId;
                            }

                            command.Parameters.Add(new SqlParameter("@IsValid", SqlDbType.Int));
                            command.Parameters["@IsValid"].Direction = ParameterDirection.Output;

                            con.Open();
                            command.ExecuteNonQuery();
                            flag = string.Equals(command.Parameters["@IsValid"].Value.ToString(), "1") ? true : false;
                        }
                    }
                }

                return flag;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Add Personal Details
        /// </summary>
        public static void AddPersonalDetails(PersonalDetails details)
        {
            try
            {
                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.AddPerssonalDetails, con))
                        {
                            GetAddPersonalDetails(details, command);
                            con.Open();
                            command.ExecuteNonQuery();

                        }
                    }
                }

            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        private static void GetAddPersonalDetails(PersonalDetails details, SqlCommand command)
        {
            string countryToLowercase = details.Country.ToLowerInvariant();
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("@FirstName", SqlDbType.VarChar)).Value = details.FistName;
            command.Parameters.Add(new SqlParameter("@LastName", SqlDbType.VarChar)).Value = details.LastName;
            command.Parameters.Add(new SqlParameter("@Email", SqlDbType.VarChar)).Value = details.Email;
            command.Parameters.Add(new SqlParameter("@Phone", SqlDbType.VarChar)).Value = details.Phone;
            command.Parameters.Add(new SqlParameter("@MobilePhone", SqlDbType.VarChar)).Value = details.MobilePhone;
            command.Parameters.Add(new SqlParameter("@AddressLine1", SqlDbType.VarChar)).Value = details.Address1;
            command.Parameters.Add(new SqlParameter("@AddressLine2", SqlDbType.VarChar)).Value = details.Address2;
            command.Parameters.Add(new SqlParameter("@AddressLine3", SqlDbType.VarChar)).Value = details.Address3;
            command.Parameters.Add(new SqlParameter("@CityTown", SqlDbType.VarChar)).Value = details.Address1;
            command.Parameters.Add(new SqlParameter("@CountyRegion", SqlDbType.VarChar)).Value = details.CountyRegion;
            command.Parameters.Add(new SqlParameter("@Country", SqlDbType.VarChar)).Value = countryToLowercase;
            command.Parameters.Add(new SqlParameter("@ClientStore", SqlDbType.VarChar)).Value = details.Store;
            command.Parameters.Add(new SqlParameter("@SendEmail", SqlDbType.Bit)).Value = details.SendEmail;
            command.Parameters.Add(new SqlParameter("@TermsAndConditions", SqlDbType.Bit)).Value = details.TermsAndConditions;
            command.Parameters.Add(new SqlParameter("@OrderId", SqlDbType.Int)).Value = details.OrderId;

        }


        /// <summary>
        /// Add Guest User details
        /// </summary>
        public static void AddGuestDetails(List<RoomReservation> details)
        {
            try
            {
                bool successfullyParsed = false;
                //int roomReservationId = 0;
                int ConnectionStringLength = GetuCommerceConnetionString().Length;

                if (ConnectionStringLength > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.AddGuestDetails, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;

                            //command.Parameters.Add(new SqlParameter("@RoomReservationId", SqlDbType.Int));
                            command.Parameters.Add(new SqlParameter("@OrderLineId", SqlDbType.Int));
                            command.Parameters.Add(new SqlParameter("@NoOfGuest", SqlDbType.Int));

                            command.Parameters.Add(new SqlParameter("@NoOfAdult", SqlDbType.Int));
                            command.Parameters.Add(new SqlParameter("@NoOfChildren", SqlDbType.Int));

                            command.Parameters.Add(new SqlParameter("@GuestName", SqlDbType.VarChar));
                            command.Parameters.Add(new SqlParameter("@SpecialRequest", SqlDbType.VarChar));

                            con.Open();
                            foreach (RoomReservation item in details)
                            {
                                AddGuestDetailsRoomReservation(ref successfullyParsed, command, item);
                            }
                        }
                    }
                }

            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        private static void AddGuestDetailsRoomReservation(ref bool successfullyParsed, SqlCommand command, RoomReservation item)
        {
            //successfullyParsed = int.TryParse(item.RoomReservationId, out roomReservationId);

            //command.Parameters["@RoomReservationId"].Value = roomReservationId;
            command.Parameters["@OrderLineId"].Value = item.OrderLineId;
            command.Parameters["@NoOfGuest"].Value = item.NoOfGuest;
            command.Parameters["@NoOfAdult"].Value = item.NoOfAdults;
            command.Parameters["@NoOfChildren"].Value = item.NoOfChildren;
            command.Parameters["@GuestName"].Value = item.GuestName;
            command.Parameters["@SpecialRequest"].Value = item.SpecialRequest;
            command.ExecuteNonQuery();
        }


        /// <summary>
        /// Add Guest User details
        /// </summary>
        public static void AddGuestChildAgeDetails(string age, string OrderLineId)
        {
            try
            {
                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.AddGuestChildAgeDetails, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;

                            command.Parameters.Add(new SqlParameter("@OrderLineId", SqlDbType.Int)).Value = OrderLineId;
                            command.Parameters.Add(new SqlParameter("@ChildrenAge", SqlDbType.VarChar)).Value = age;

                            con.Open();
                            command.ExecuteNonQuery();
                            con.Close();
                            //foreach (RoomReservationAgeInfo item in details)
                            //{
                            //    command.Parameters["@OrderLineId"].Value = item.OrderLineId;
                            //    command.Parameters["@ChildrenAge"].Value = item.Age;
                            //    command.ExecuteNonQuery();
                            //}
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }


        /// <summary>
        /// Get the Room Reservation By Order Line Id
        /// </summary>
        /// <param name="OrderLineId"></param>
        public static DataSet GetRoomReservationByOrderLineId(string OrderLineId)
        {
            try
            {
                DataSet ds = new DataSet();

                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.GetRoomReservationByOrderLineId, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@OrderLineId", SqlDbType.Int)).Value = OrderLineId;
                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                        }
                    }
                }

                return ds;
            }

            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }


        /// <summary>
        /// Get ChildAge Details By OrderLineID
        /// </summary>
        /// <param name="OrderLineId"></param>
        /// <returns></returns>
        public static DataSet GetChildAgeDetailsByOrderLineID(string OrderLineId)
        {
            try
            {
                DataSet ds = new DataSet();

                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.GetChildAgeDetailsByOrderLineID, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@OrderLineId", SqlDbType.Int)).Value = OrderLineId;
                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                        }
                    }
                }

                return ds;
            }

            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }


        public static DataSet GetFavoriteList(int CustomerID)
        {
            DataSet ds = new DataSet();
            try
            {


                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.GetFavoriteHotelList, con))
                        {
                            con.Open();
                            command.CommandType = CommandType.StoredProcedure;
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                            con.Close();
                        }
                    }
                }
                return ds;

            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }
        public static void AddList(string username, string listname)
        {
            try
            {
                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.NewList, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@UserEmail", SqlDbType.VarChar)).Value = username;
                            command.Parameters.Add(new SqlParameter("@ListName", SqlDbType.VarChar)).Value = listname;
                            command.Parameters.Add(new SqlParameter("@date", SqlDbType.DateTime)).Value = DateTime.Now.ToString();
                            con.Open();
                            command.ExecuteNonQuery();
                            con.Close();

                        }
                    }
                }


            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        public static void SaveHotelForLater(int userID, string HotelID)
        {
            try
            {
                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.spSaveForLater, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@CustomerID", SqlDbType.Int)).Value = userID;
                            command.Parameters.Add(new SqlParameter("@HotelID", SqlDbType.VarChar)).Value = HotelID;
                            command.Parameters.Add(new SqlParameter("@Curdate", SqlDbType.DateTime)).Value = DateTime.Now.ToString();
                            con.Open();
                            command.ExecuteNonQuery();
                            con.Close();

                        }
                    }
                }


            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        public static DataSet SaveHotels(int userID)
        {
            DataSet ds = new DataSet();
            try
            {
                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.spShowSavedHotel, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@CustomerID", SqlDbType.Int)).Value = userID;
                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                            con.Close();

                        }
                    }
                }

                return ds;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        public static DataSet GetAccomodationDetailsByOrderLineId(string OrderLineId)
        {
            try
            {
                DataSet dataset = new DataSet();

                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.GetAccomodationDetailsByOrderLineId, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@OrderLineId", SqlDbType.Int)).Value = OrderLineId;
                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(dataset);
                        }
                    }
                }

                return dataset;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(ex.Message, ex);
            }
        }


        /// <summary>
        /// Get the Post Break Email Details
        /// </summary>
        /// <returns>Post Break Email related Data Set</returns>
        public static DataSet GetPostBreakEmailDetails(string clientId)
        {
            try
            {
                DataSet ds = new DataSet();

                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.GetPostBreakEmailDetails, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@ClientId", SqlDbType.VarChar)).Value = clientId;
                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                        }
                    }
                }

                return ds;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }


        public static DataSet ManageBooking(string bookingNumber)
        {
            try
            {
                DataSet ds = new DataSet();

                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.ManageBooking, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add("@BookingNumber", SqlDbType.Int).Value = bookingNumber;
                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                        }
                    }
                }

                return ds;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }



        public static void UpdatePurchaseOrder(int orderId, int customerId)
        {
            try
            {
                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.UpdateCustomerInOrder, con))
                        {
                            con.Open();
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@customerid", SqlDbType.Int)).Value = customerId;
                            command.Parameters.Add(new SqlParameter("@orderid", SqlDbType.Int)).Value = orderId;
                            command.ExecuteNonQuery();
                            con.Close();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Method to update Checkout Amount in the Purchase Order Table
        /// </summary>
        /// <param name="orderId">Order id for which checkout amount has to be updated</param>
        /// <param name="checkoutAmount">The checkout amount to be updated</param>
        public static void UpdateOrderCheckoutAmount(int orderId, decimal checkoutAmount)
        {
            try
            {
                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.UpdateCheckoutAmount, con))
                        {
                            con.Open();
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@OrderId", SqlDbType.Int)).Value = orderId;
                            command.Parameters.Add(new SqlParameter("@CheckoutAmount", SqlDbType.Money)).Value = checkoutAmount;
                            command.ExecuteNonQuery();
                            con.Close();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Method to update the OrderLine Table with Deposit and Payable at accomodation amount
        /// </summary>
        /// <param name="orderId">The order id for the orderline</param>
        /// <param name="orderLineId">The orderline to be updated</param>
        /// <param name="payableAtAccomodation">Amount to be updated for PayableAtAccomodation Column</param>
        /// <param name="deposit">Amount to be updated for Deposit Column</param>
        public static void UpdateOrderLineDepositAmount(int orderId, int orderLineId, decimal payableAtAccomodation, decimal deposit)
        {
            try
            {
                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.UpdateOrderLinePayabaleAtAccomodation, con))
                        {
                            con.Open();
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@OrderId", SqlDbType.Int)).Value = orderId;
                            command.Parameters.Add(new SqlParameter("@OrderLineId", SqlDbType.Int)).Value = orderLineId;
                            command.Parameters.Add(new SqlParameter("@PayableAtAccomodation", SqlDbType.Money)).Value = payableAtAccomodation;
                            command.Parameters.Add(new SqlParameter("@Deposit", SqlDbType.Money)).Value = deposit;
                            command.ExecuteNonQuery();
                            con.Close();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// set Recently Seen Hotels
        /// </summary>
        public static void setRecentlySeen(int CustomerID, string HotelID)
        {
            try
            {
                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.setRecentlySeen, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@CustomerID", SqlDbType.Int)).Value = CustomerID;
                            command.Parameters.Add(new SqlParameter("@HotelID", SqlDbType.VarChar)).Value = HotelID;
                            con.Open();
                            command.ExecuteNonQuery();
                            con.Close();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Get Recently Seen Hotels
        /// </summary>
        public static DataSet getRecentlySeen(int CustomerID)
        {
            DataSet ds = new DataSet();
            try
            {
                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.getRecentlySeen, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@CustomerID", SqlDbType.Int)).Value = CustomerID;
                            con.Open();
                            SqlDataAdapter da = new SqlDataAdapter(command);
                            da.Fill(ds);
                            con.Close();
                        }
                    }
                }
                return ds;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Returns the Number of nights for a booking
        /// </summary>
        /// <param name="OrderId">ucommerce order Id</param>
        /// <returns>Number of nights</returns>
        public static int GetNoOfNights(int orderLineId)
        {
            try
            {
                DataSet dataset = new DataSet();

                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.GetNoOfNightsByOrderId, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@OrderLineId", SqlDbType.Int)).Value = orderLineId;
                            con.Open();

                            int? days = (int?)command.ExecuteScalar();
                            con.Close();

                            return (days.HasValue) ? days.Value : 0;

                        }
                    }
                }

                return 0;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(ex.Message, ex);
            }
        }

        public static DataSet GetSavedHotel(int customerID)
        {
            try
            {
                DataSet ds = new DataSet();

                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        string sql = Constants.spShowSavedHotel;

                        using (SqlCommand command = new SqlCommand(sql, con))
                        {
                            con.Open();
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int)).Value = customerID;
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                            con.Close();
                        }
                    }
                }

                return ds;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        public static int AddListandSaveHotel(int customerID, string HotelId, string listname)
        {
            try
            {
                int returnListId = 0;

                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        string sql = "LB_AddListandSaveHotel";

                        using (SqlCommand command = new SqlCommand(sql, con))
                        {
                            con.Open();
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int)).Value = customerID;
                            command.Parameters.Add(new SqlParameter("@HotelId", SqlDbType.VarChar)).Value = HotelId;
                            command.Parameters.Add(new SqlParameter("@ListName", SqlDbType.VarChar)).Value = listname;
                            command.Parameters.Add(new SqlParameter("@date", SqlDbType.DateTime)).Value = DateTime.Now.ToString();
                            IDbDataParameter returnId = command.CreateParameter();
                            returnId.Direction = ParameterDirection.ReturnValue;
                            command.Parameters.Add(returnId);
                            command.ExecuteNonQuery();
                            int.TryParse(returnId.Value.ToString(), out returnListId);
                            con.Close();
                        }
                    }
                }

                return returnListId;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(null, exception.InnerException);
            }
        }

        public static void SaveHotelToList(int ListID, string HotelID)
        {
            try
            {
                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        string sql = "LB_AddHotelToList";

                        using (SqlCommand command = new SqlCommand(sql, con))
                        {
                            con.Open();
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@newAddHotel", SqlDbType.VarChar)).Value = HotelID;
                            command.Parameters.Add(new SqlParameter("@ListID", SqlDbType.Int)).Value = ListID;
                            command.ExecuteNonQuery();
                            con.Close();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(null, exception.InnerException);
            }
        }

        public static DataSet GetCustomerDetailByEmail(string email)
        {
            DataSet ds = new DataSet();
            try
            {
                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        string sql = "select * from uCommerce_Customer c join LB_CustomerAttribute a on c.CustomerId=a.CustomerId where c.EmailAddress='" + email + "'";

                        using (SqlCommand command = new SqlCommand(sql, con))
                        {
                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                            con.Close();
                        }
                    }
                }
                return ds;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(null, exception.InnerException);
            }
        }

        /// <summary>
        /// Search Booking
        /// </summary>
        public static int SearchBooking(string orderReference, string guestName, DateTime checkInDate)
        {
            int Orderid = 0;
            try
            {
                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.SearchBooking, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            SqlParameter parm = new SqlParameter("@orderLineID", SqlDbType.Int) { Direction = ParameterDirection.ReturnValue };
                            command.Parameters.Add(parm);
                            command.Parameters.Add(new SqlParameter("@OrderReference", SqlDbType.VarChar)).Value = orderReference;
                            command.Parameters.Add(new SqlParameter("@GuestName", SqlDbType.VarChar)).Value = guestName;
                            command.Parameters.Add(new SqlParameter("@CheckInDate", SqlDbType.Date)).Value = checkInDate.ToString("yyyy-MM-dd");
                            con.Open();
                            command.ExecuteNonQuery();
                            int.TryParse(parm.Value.ToString(), out Orderid);
                            con.Close();
                        }
                    }
                }
                return Orderid;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Get the Room Reservation By Order Line Id
        /// </summary>
        /// <param name="OrderLineId"></param>
        public static DataSet GetRoomReservationDetailsByOrderLineId(string OrderLineId)
        {
            try
            {
                DataSet ds = new DataSet();

                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.GetRoomReservationByOrderLineId, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@OrderLineId", SqlDbType.Int)).Value = OrderLineId;
                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                        }
                    }
                }

                return ds;
            }

            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }


        public static DataSet HourlyBookingData()
        {
            try
            {
                DataSet ds = new DataSet();

                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand("LB_GetHourlyBooking", con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@currentTime", SqlDbType.DateTime)).Value = DateTime.Now.AddMinutes(-DateTime.Now.Minute).AddSeconds(-DateTime.Now.Second).ToString();
                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                        }
                    }
                }

                return ds;
            }

            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }


        public static DataSet PrevDayBookingData()
        {
            try
            {
                DataSet ds = new DataSet();

                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand("LB_GetPreviousDayBooking", con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                        }
                    }
                }

                return ds;
            }

            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }


        public static DataSet EmailToCallCenterAgent()
        {
            try
            {
                DataSet ds = new DataSet();
                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand cmd = new SqlCommand("LB_EmailToCallCenterAgent", con))
                        {

                            cmd.CommandType = CommandType.StoredProcedure;
                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                            adapter.Fill(ds);
                            adapter.Dispose();

                        }
                    }
                }
                return ds;

            }


            catch (Exception e)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, e, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(e.Message, e);
            }
        }

        public static DataSet OverAllRevenueForClient(DateTime dateFrom, DateTime dateTo, string bookingMethod, string partner)
        {
            try
            {
                DataSet ds = new DataSet();
                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand cmd = new SqlCommand("LB_GetOverAllRevenueForClient", con))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@DateFrom", SqlDbType.Date)).Value = dateFrom.ToString("yyyy-MM-dd");
                            cmd.Parameters.Add(new SqlParameter("@DateTo", SqlDbType.Date)).Value = dateTo.ToString("yyyy-MM-dd");
                            cmd.Parameters.Add(new SqlParameter("@BookingThrough", SqlDbType.VarChar)).Value = bookingMethod.ToString();
                            cmd.Parameters.Add(new SqlParameter("@ClientId", SqlDbType.VarChar)).Value = partner.ToString();

                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                            adapter.Fill(ds);
                        }
                    }
                }
                return ds;
            }
            catch (Exception e)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, e, null);
                throw new AffinionException(e.Message, e);
            }
        }



        /// <summary>
        /// Remove Hotel From Favourite List
        /// </summary>
        /// <param name="ListId"></param>
        /// <param name="CustomerId"></param>
        /// <param name="HotelId"></param>
        public static void RemoveFavourite(int listId, int customerId, string hotelId)
        {
            try
            {
                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand(Constants.RemoveFavouriteHotel, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@ListID", SqlDbType.Int)).Value = listId;
                            command.Parameters.Add(new SqlParameter("@CustomerID", SqlDbType.Int)).Value = customerId;
                            command.Parameters.Add(new SqlParameter("@RemoveHotelId", SqlDbType.VarChar)).Value = hotelId;
                            con.Open();
                            command.ExecuteNonQuery();
                            con.Close();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// get invalid login attempt count
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public static int GetInvalidPasswordAttemptCount(string userid)
        {
            int invalidPasswordAttemptCount = 0;

            if (GetAffinionContextConnectionString().Length > 0)
            {
                using (SqlConnection con = new SqlConnection(GetAffinionContextConnectionString()))
                {
                    using (SqlCommand command = new SqlCommand("LB_SelectInvalidLogAttemptCount", con))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new SqlParameter("@UserId", SqlDbType.VarChar)).Value = userid;
                        if (con.State != ConnectionState.Open)
                            con.Open();
                        object result = command.ExecuteScalar();
                        if (result != null)
                        {
                            invalidPasswordAttemptCount = Convert.ToInt32(result);
                        }

                        con.Close();

                    }
                }
            }
            return invalidPasswordAttemptCount;
        }

        /// <summary>
        /// Get Invalid Password Attempt Time
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public static DateTime GetInvalidPasswordAttemptTime(string userid, string count)
        {
            DateTime invalidPasswordAttemptTime = DateTime.Now;

            if (GetAffinionContextConnectionString().Length > 0)
            {
                using (SqlConnection con = new SqlConnection(GetAffinionContextConnectionString()))
                {
                    using (SqlCommand command = new SqlCommand("LB_SelectInvalidLogAttemptTime", con))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new SqlParameter("@UserId", SqlDbType.VarChar)).Value = userid;
                        command.Parameters.Add(new SqlParameter("@Count", SqlDbType.NVarChar)).Value = count;
                        if (con.State != ConnectionState.Open)
                            con.Open();
                        object result = command.ExecuteScalar();
                        if (result != null)
                        {
                            invalidPasswordAttemptTime = Convert.ToDateTime(result);
                        }

                        con.Close();

                    }
                }
            }
            return invalidPasswordAttemptTime;
        }
        /// <summary>
        /// add unique id to user for chage password
        /// </summary>
        /// <returns></returns>
        /// <summary>
        /// AddUniqueId
        /// </summary>
        /// <param name="randomNumber">random Number</param>
        /// <param name="username">user name</param>
        /// <returns></returns>
        public static void AddUniqueId(string randomNumber, string username)
        {
            try
            {
                if (GetAffinionContextConnectionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetAffinionContextConnectionString()))
                    {
                        using (SqlCommand command = new SqlCommand("LB_InsertChangePasswordHistory", con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@UniqueId", SqlDbType.NVarChar)).Value = randomNumber;
                            command.Parameters.Add(new SqlParameter("@UserName", SqlDbType.NVarChar)).Value = username;
                            if (con.State != ConnectionState.Open)
                                con.Open();
                            command.ExecuteNonQuery();
                            con.Close();

                        }
                    }
                }

            }
            catch (Exception exception)
            {

                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Select ChangePassword History for chage password
        /// </summary>
        /// <returns></returns>
        /// <summary>
        /// Select Change Password History
        /// </summary>
        /// <param name="randomNumber">random Number</param>
        /// <returns></returns>

        public static DataSet SelectChangePasswordHistory(string randomNumber)
        {
            try
            {
                DataSet ds = new DataSet();
                if (GetAffinionContextConnectionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetAffinionContextConnectionString()))
                    {
                        using (SqlCommand command = new SqlCommand("LB_SelectChangePasswordHistory", con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            if (con.State != ConnectionState.Open)
                                con.Open();
                            command.Parameters.Add(new SqlParameter("@UniqueId", SqlDbType.NVarChar)).Value = randomNumber;
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);

                            con.Close();

                        }
                    }
                }
                return ds;

            }
            catch (Exception exception)
            {

                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }


        /// <summary>
        /// Select ChangePassword History for chage password
        /// </summary>
        /// <returns></returns>
        /// <summary>
        /// Select Change Password History
        /// </summary>
        /// <param name="randomNumber">random Number</param>
        /// <returns></returns>

        public static List<bool> SelectLoginAuditHistory(string username)
        {
            try
            {
                List<bool> isLoginSucess = new List<bool>();
                DataSet ds = new DataSet();
                if (GetAffinionContextConnectionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetAffinionContextConnectionString()))
                    {
                        using (SqlCommand command = new SqlCommand("LB_SelectLoginAudit", con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            if (con.State != ConnectionState.Open)
                                con.Open();
                            command.Parameters.Add(new SqlParameter("@UserName", SqlDbType.NVarChar)).Value = username;
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                            using (SqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    isLoginSucess.Add(Convert.ToBoolean(reader["IsLoginSucess"]));
                                }
                            }

                            con.Close();

                        }
                    }
                }
                //if (ds != null && ds.Tables[0].Rows.Count > 0)
                //{

                //    isLoginSucess = ds.Tables[0].Rows;
                //   // return isLoginSucess;
                //}
                return isLoginSucess;

            }
            catch (Exception exception)
            {

                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }
        /// <summary>
        /// Delete Change Password History for chage password
        /// </summary>
        /// <returns></returns>
        /// <summary>
        /// Delete Change Password History
        /// </summary>
        /// <param name="username">user name</param>
        /// <returns></returns>

        public static void DeleteChangePasswordHistory(string username)
        {
            try
            {
                if (GetAffinionContextConnectionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetAffinionContextConnectionString()))
                    {
                        using (SqlCommand command = new SqlCommand("LB_DeleteChangePasswordHistory", con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@UserName", SqlDbType.NVarChar)).Value = username;
                            if (con.State != ConnectionState.Open)
                                con.Open();
                            command.ExecuteNonQuery();
                            con.Close();

                        }
                    }
                }

            }
            catch (Exception exception)
            {

                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Add Customer for Subscription
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="customerId"></param>
        /// <param name="emailAddress"></param>
        /// <param name="subscribeType"></param>
        public static void AddSubscribedCustomer(string clientId, int customerId, string emailAddress, string subscribeType)
        {

            try
            {
                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand("LB_AddCustomerForSubscription", con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@ClientId", SqlDbType.VarChar)).Value = clientId;
                            command.Parameters.Add(new SqlParameter("@CustomerId", SqlDbType.Int)).Value = customerId;
                            command.Parameters.Add(new SqlParameter("@EmailAddress", SqlDbType.VarChar)).Value = emailAddress;
                            command.Parameters.Add(new SqlParameter("@SubscriptionType", SqlDbType.VarChar)).Value = subscribeType;

                            con.Open();
                            command.ExecuteNonQuery();
                            con.Close();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }

        }

        public static void StoreRealexReport(DataTable data)
        {
            try
            {
                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        con.Open();
                        using (SqlCommand command = new SqlCommand("delete from Realex_Report", con))
                        {
                            command.ExecuteNonQuery();
                        }
                        //creating object of SqlBulkCopy  
                        SqlBulkCopy objbulk = new SqlBulkCopy(con);
                        //assigning Destination table name  
                        objbulk.DestinationTableName = "Realex_Report";
                        //Mapping Table column  
                        objbulk.ColumnMappings.Add("BATCHID", "BATCHID");
                        objbulk.ColumnMappings.Add("DATE", "DATE");
                        objbulk.ColumnMappings.Add("ORDERID", "ORDERID");
                        objbulk.ColumnMappings.Add("CARDHOLDERSNAME", "CARDHOLDERSNAME");
                        objbulk.ColumnMappings.Add("CUSTOMERNUMBER", "CUSTOMERNUMBER");
                        objbulk.ColumnMappings.Add("PRODUCTCODE", "PRODUCTCODE");
                        objbulk.ColumnMappings.Add("VARIABLEREF", "VARIABLEREF");
                        objbulk.ColumnMappings.Add("AMOUNT", "AMOUNT");
                        objbulk.ColumnMappings.Add("CURRENCY", "CURRENCY");
                        objbulk.ColumnMappings.Add("REQUESTTIMESTAMP", "REQUESTTIMESTAMP");
                        objbulk.ColumnMappings.Add("CARDTYPE", "CARDTYPE");
                        objbulk.ColumnMappings.Add("PAYANDSHOPCOMREFERENCE", "PAYANDSHOPCOMREFERENCE");
                        objbulk.ColumnMappings.Add("AUTHCODE", "AUTHCODE");
                        objbulk.ColumnMappings.Add("AUTOSETTLEFLAG", "AUTOSETTLEFLAG");
                        objbulk.ColumnMappings.Add("TSSSCORE", "TSSSCORE");
                        objbulk.ColumnMappings.Add("TRANSACTIONSOURCE", "TRANSACTIONSOURCE");
                        objbulk.ColumnMappings.Add("COMMENT1", "COMMENT1");
                        objbulk.ColumnMappings.Add("COMMENT2", "COMMENT2");
                        objbulk.ColumnMappings.Add("ISSUINGBANK", "ISSUINGBANK");
                        objbulk.ColumnMappings.Add("ISSUERCOUNTRY", "ISSUERCOUNTRY");
                        objbulk.ColumnMappings.Add("ISSUERCOUNTRYCODE", "ISSUERCOUNTRYCODE");
                        objbulk.ColumnMappings.Add("SETTLETIME", "SETTLETIME");
                        objbulk.ColumnMappings.Add("VOIDCOMMENT", "VOIDCOMMENT");
                        objbulk.ColumnMappings.Add("FOREIGNCURRENCYAMOUNT", "FOREIGNCURRENCYAMOUNT");
                        objbulk.ColumnMappings.Add("CCPRATE", "CCPRATE");
                        objbulk.ColumnMappings.Add("CURRENCYPROCESSOR", "CURRENCYPROCESSOR");
                        objbulk.ColumnMappings.Add("MERCHANTID", "MERCHANTID");
                        objbulk.ColumnMappings.Add("ACCOUNT", "ACCOUNT");
                        objbulk.ColumnMappings.Add("STATUS", "STATUS");
                        objbulk.ColumnMappings.Add("SECURITYCODERESULT", "SECURITYCODERESULT");
                        objbulk.ColumnMappings.Add("AVSRESULT", "AVSRESULT");
                        //inserting bulk Records into DataBase   
                        objbulk.WriteToServer(data);
                    }
                }

            }
            catch (Exception exception)
            {

                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        public static DataSet paymentData()
        {
            try
            {
                DataSet ds = new DataSet();
                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand("getPaymentData", con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            if (con.State != ConnectionState.Open)
                                con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                            con.Close();

                        }
                    }
                }
                return ds;
            }
            catch (Exception exception)
            {

                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        public static DataSet GetCancellationMismatchReport()
        {
            try
            {
                DataSet ds = new DataSet();

                if (GetuCommerceConnetionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                    {
                        using (SqlCommand command = new SqlCommand("LB_GetCancellationMismatchReport", con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                        }
                    }
                }
                return ds;
            }

            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }


        /// <summary>
        /// OfflinePOrtal Confirmation Guestname and special request Sp
        /// </summary>
        /// <param name="Orderid"></param>
        /// <returns></returns>
        public static List<Customer> GetConfirmationInfo(int Orderid)
        {
            List<Customer> listCustomer = new List<Customer>();
            if (GetuCommerceConnetionString().Length > 0)
            {
                using (SqlConnection con = new SqlConnection(GetuCommerceConnetionString()))
                {
                    using (SqlCommand command = new SqlCommand("LB_GetConfirmationDetails ", con))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new SqlParameter("@orderid", SqlDbType.Int)).Value = Orderid;
                        con.Open();

                        using (var reader = command.ExecuteReader())
                        {
                            if (reader.HasRows)
                            {

                                while (reader.Read())
                                {
                                    Customer customer = new Customer();
                                    customer.GuestName = reader["GuestName"] != DBNull.Value ? Convert.ToString(reader["GuestName"]) : string.Empty;
                                    customer.OrderLineId = reader["OrderLineId"] != DBNull.Value ? Convert.ToInt32(reader["OrderLineId"]) : default(int);
                                    customer.Notes = reader["SpecialRequest"] != DBNull.Value ? Convert.ToString(reader["SpecialRequest"]) : string.Empty;

                                    listCustomer.Add(customer);
                                }
                            }
                        }
                    }
                }
            }
            return listCustomer;
        }
    }
}