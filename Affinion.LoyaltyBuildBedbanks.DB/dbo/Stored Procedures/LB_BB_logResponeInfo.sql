
/****** Object:  StoredProcedure [dbo].[LB_BB_logResponeInfo]    Script Date: 7/19/2016 7:28:34 PM ******/

---- ====================================================
---- 
---- RAISERROR ('mesage text %a', severity#, state#, @pi_); 
----			%s/%u is optional;	
----			severity < 10 The error is returned to the caller if RAISERROR is run
----			severity > 11 transfers control to the associated CATCH block.
----			severity > 20 terminates the database connection.
----			state#   set to 1
----			@pi_ optional, must be used if %s/%u is used in the message text
----
---- ====================================================

CREATE PROCEDURE [dbo].[LB_BB_logResponeInfo]
	--
	@pi_method varchar(Max),
	@pi_uuid varchar(max),
	@pi_TimeInMs int,
	
	--Return parameters
	@po_returnstatus INT OUTPUT,
	@po_returnmsg VARCHAR(200) OUTPUT
	

AS

BEGIN

	--Assume transaction will fail, set the return variable to -1.
	SET @po_returnstatus = -1;
	SET @po_returnmsg = null;

	
	BEGIN TRY
		INSERT INTO LB_BB_responseInfo(UUID,Method,TimeInMs) 
		Values (@pi_uuid,@pi_method,@pi_TimeInMs)
		
    END TRY
	
	BEGIN CATCH
		SET @po_returnstatus = error_number();
		SET @po_returnmsg = error_message();
		RETURN;
	END CATCH
	
	SET @po_returnstatus = 0;
	SET @po_returnmsg = 'Completed successfully.';
	
END











GO

