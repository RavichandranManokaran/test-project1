

/****** Object:  StoredProcedure [dbo].[LB_BB_checkDbPropertyCache]    Script Date: 7/19/2016 7:23:20 PM ******/
CREATE PROCEDURE [dbo].[LB_BB_checkDbPropertyCache]
	
	-- Input parameters 
	@pi_propertyid int = 1,
	@pi_availabilityProviderID int = 1,
	
	
	--Return parameters
	@po_xmlResponse VARCHAR(MAX) OUTPUT,
	@po_returnstatus INT OUTPUT,
	@po_returnmsg VARCHAR(200) OUTPUT

	
AS
set nocount on;
BEGIN

	--Assume transaction will fail, set the return variable to -1.
	SET @po_xmlResponse = '';
	SET @po_returnstatus = -1;
	SET @po_returnmsg = null;

	
	
	
	BEGIN TRY

	IF EXISTS (SELECT TOP 1 XmlResponse from LB_BB_providerDetails
					where availabilityProviderID = @pi_availabilityProviderID
					and _ProviderID = @pi_propertyid
					and ExpiryTime > GETDATE())
	BEGIN
	 SET @po_xmlResponse = (Select Top 1 XmlResponse from LB_BB_providerDetails
					where availabilityProviderID = @pi_availabilityProviderID
					and _ProviderID = @pi_propertyid
					and ExpiryTime > GETDATE())
	END
				                                		      
    END TRY	
	BEGIN CATCH	  
	SET @po_xmlResponse = ''; 
	SET @po_returnstatus = error_number();
		SET @po_returnmsg = error_message();
		RETURN;
	END CATCH
	SET @po_returnstatus = 0;
	SET @po_returnmsg = 'Completed successfully.';	
END









GO

