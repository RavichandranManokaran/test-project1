﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Sitecore.Links;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.UCom.Common.Extension;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Offer
{
    public partial class OfferAvailabilityView : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            btnbackavilability.Visible = false;
            DateTime startDate = DateTime.Now;
            DateTime endDate = new DateTime(startDate.Year, startDate.Month, 1).AddMonths(1).AddDays(-1);
            datepickerFrom.Value = startDate.ConvertDateToString();
            datepickerTo.Value = endDate.ConvertDateToString();
            serverdate.Value = startDate.ConvertDateToString();
            List<SupplierInviteDetail> supplierInvite = new List<SupplierInviteDetail>();
            string offerUrl = LinkManager.GetItemUrl(SiteConfiguration.GetManageAvailabilityItem());
            if (!string.IsNullOrEmpty(offerUrl))
            {
                offerurl.Value = offerUrl;
            }
            if (Request["fromdate"] != null)
            {
                startDate = Request["fromdate"].ToString().ParseFormatDateTime();
                datepickerFrom.Value = startDate.ConvertDateToString();
            }
            if (Request["todate"] != null)
            {
                endDate = Request["todate"].ToString().ParseFormatDateTime();
                datepickerTo.Value = endDate.ConvertDateToString();
                btnbackavilability.Visible = false;
            }
            AvailabilityHelper dataForOffers = new AvailabilityHelper(HttpContext.Current);
            supplierInvite = dataForOffers.GetCampaignDetailBySupplier(startDate, endDate);
            if (Request["offerid"] != null)
            {
                hdOfferId.Value = Request["offerid"].ToString();
                if (!supplierInvite.Any(x => x.CampaignItemId == Convert.ToInt32(hdOfferId.Value)))
                {
                    hdOfferId.Value = "-1";
                }
                else
                {
                    offername.InnerText = supplierInvite.FirstOrDefault(x => x.CampaignItemId == Convert.ToInt32(hdOfferId.Value)).CampaignName;
                }
            }
            else
            {
                if (supplierInvite != null && supplierInvite.Any())
                {
                    hdOfferId.Value = supplierInvite.First().CampaignItemId.ToString();
                    offername.InnerText = supplierInvite.FirstOrDefault().CampaignName;
                }
                else
                {
                    hdOfferId.Value = "-1";
                }
            }
            try
            {
                SupplierInfo data = dataForOffers.GetLoggedInSupplierDetailById();
                if (data != null)
                {
                    lblInfo.Text = string.Format("{0}{1}{2}{3}{4}{5}", data.SupplierName,
                        !string.IsNullOrEmpty(data.Group) ? string.Format(" /{0}", data.Group) : string.Empty,
                        !string.IsNullOrEmpty(data.SuperGroup) ? string.Format(" /{0}", data.SuperGroup) : string.Empty,
                        !string.IsNullOrEmpty(data.MapLocation) ? string.Format(", {0}", data.MapLocation) : string.Empty,
                        !string.IsNullOrEmpty(data.Country) ? string.Format(", {0}", data.Country) : string.Empty,
                        !string.IsNullOrEmpty(data.StarRating) ? string.Format(", Rating: {0}", data.StarRating) : string.Empty);
                }
            }
            catch (Exception er)
            {
                Sitecore.Diagnostics.Log.Error(er.Message, new object());
            }
            litHelp.Text = SiteConfiguration.GetHelpContentByType(2); 
            //this.checkRoles();

        }
    }
}