

/****** Object:  StoredProcedure [dbo].[LB_BB_getBedBankRegionsForClientv2]    Script Date: 7/19/2016 7:25:41 PM ******/
---- ====================================================
---- Description:	Tii Sp to return all countries for a voucher client
---- Written: Noreen
---- Created: 2011.08.30
---- ====================================================
CREATE PROCEDURE [dbo].[LB_BB_getBedBankRegionsForClientv2]
	
	-- Input parameters 
	@pi_ClientID int = 68001,
	@pi_Blended int = 1,

	--Return parameters
	@po_returnstatus INT OUTPUT,
	@po_returnmsg VARCHAR(200) OUTPUT,
	@po_scopeid NUMERIC OUTPUT
	
AS
set nocount on;
BEGIN

	--Assume transaction will fail, set the return variable to -1.
	SET @po_returnstatus = -1;
	SET @po_returnmsg = null;
	SET @po_scopeid = 0;	
	BEGIN TRY
	
	
		IF @pi_Blended = 1
		BEGIN
			IF @pi_ClientID = 68045
			BEGIN
				SELECT * FROM syn_emosLocation
			END 
			ELSE
			BEGIN
				SELECT * FROM syn_emosLocation EL
				INNER JOIN syn_emosCountryLingual ECL on EL.CountryID = ECL.CountryID
				AND ECL.LanguageID = 1
				AND ECL.CountryID in (96,213,231)
			END
		END
		ELSE
		BEGIN
			IF @pi_ClientID = 68045
			BEGIN
				SELECT DISTINCT Country,RegionID, REGION FROM Bedbank.dbo.Location
					where Country in ('England','Scotland','Wales','Republic of Ireland', 'Northern Ireland')
					Order by Region asc,Country desc
			END 
			ELSE
			BEGIN
				SELECT DISTINCT Country,RegionID, REGION FROM Bedbank.dbo.Location
				where Country in ('Republic of Ireland', 'Northern Ireland')
				Order by Region asc,Country desc
			END
		END
	

	
	
    END TRY	
	BEGIN CATCH	  
		SET @po_returnstatus = error_number();
		SET @po_returnmsg = error_message();
		RETURN;
	END CATCH
	
	SET @po_returnstatus = 0;
	SET @po_returnmsg = 'Completed successfully.';	
END








GO

