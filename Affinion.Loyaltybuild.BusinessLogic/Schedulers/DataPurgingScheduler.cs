﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           BasketHelper.cs
/// Sub-system/Module:     Affinion.Loyaltybuild.BusinessLogic.Schedulers
/// Description:           Scheduler Data Purging
/// </summary>

#region  Using Directives
using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.Common.Exceptions;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
using System;
using System.Data.SqlClient; 
#endregion

namespace Affinion.Loyaltybuild.BusinessLogic.Schedulers
{
    public class DataPurgingScheduler
    {
        public void Run()
        {
            try
            {
                Sitecore.Context.Job.Status.LogInfo("Data Purging Scheduler started");
                using (SqlConnection con = new SqlConnection(uCommerceConnectionFactory.GetuCommerceConnetionString()))
                {
                    using (SqlCommand command = new SqlCommand(Constants.PurgeExistingBookings, con))
                    {
                        con.Open();
                        command.ExecuteNonQuery();
                    }
                }
                Sitecore.Context.Job.Status.LogInfo("Data Purging Scheduler finished");
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, this);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

    }
}
