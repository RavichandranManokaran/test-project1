﻿using System.Configuration;

namespace Affinion.LoyaltyBuild.BedBanks.Troika.Config
{
    public class Currency : ConfigurationElement
    {
        [ConfigurationProperty("id", IsKey = true)]
        public string Id { get { return (string)this["id"]; } }

        [ConfigurationProperty("troikaId", IsKey = true)]
        public int TroikaId { get { return (int)this["troikaId"]; } }

        [ConfigurationProperty("name", IsKey = true)]
        public string Name { get { return (string)this["name"]; } }

        [ConfigurationProperty("symbol", IsKey = true)]
        public string Symbol { get { return (string)this["symbol"]; } }

        [ConfigurationProperty("rateId", IsKey = true)]
        public int RateId { get { return (int)this["rateId"]; } }

        [ConfigurationProperty("coicrId", IsKey = true)]
        public int CoicrId { get { return (int)this["coicrId"]; } }
    }
}