﻿
using Affinion.LoyaltyBuild.UCom.Common.Extension;
using Affinion.LoyaltyBuild.UCom.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;
using UCommerce.Marketing.Targets;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class ArrivalDayTarget : Target, IOrderLineTarget, IEntity, ITarget
    {
        private int? _hashCode;

        public virtual bool IsSatisfiedBy(OrderLine orderLine)
        {
            OrderProperty searchIgnor = orderLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.IgnorCampaignSearchCriteria);
            if (searchIgnor != null)
                return true;

            OrderProperty arrivalDateProperty = orderLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.ArrivalDateKey);
            if (arrivalDateProperty != null)
            {
                DateTime arrivalDate = arrivalDateProperty.Value.ParseFormatDateTime(true);
                string[] days = this.ArrivalDay.Split('|');

                if (days.Any(x => Convert.ToInt32(x) == (int)arrivalDate.DayOfWeek))
                {
                    return true;
                }
            }
            return false;

        }


        // Methods
        public new static IQueryable<ArrivalDayTarget> All()
        {
            return GetRepo().Select();
        }

        public override void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<ArrivalDayTarget, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            ArrivalDayTarget objA = obj as ArrivalDayTarget;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<ArrivalDayTarget, bool>> expression)
        {
            return All().Any<ArrivalDayTarget>(expression);
        }

        public static IList<ArrivalDayTarget> Find(Expression<Func<ArrivalDayTarget, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<ArrivalDayTarget>();
        }

        public static ArrivalDayTarget FirstOrDefault(Expression<Func<ArrivalDayTarget, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<ArrivalDayTarget>();
        }

        public new static ArrivalDayTarget Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<ArrivalDayTarget> GetRepo()
        {
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<ArrivalDayTarget>>();
        }


        public override void Save()
        {
            GetRepo().Save(this);
        }

        public static ArrivalDayTarget SingleOrDefault(Expression<Func<ArrivalDayTarget, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties
        public override int Id
        {
            get
            {
                return this.ArrivalDayTargetId;
            }
        }

        public virtual int ArrivalDayTargetId { get; protected set; }

        public virtual string ArrivalDay { get; set; }



    }
}
