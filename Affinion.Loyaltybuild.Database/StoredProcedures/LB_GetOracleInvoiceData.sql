CREATE PROCEDURE [dbo].[LB_GetOracleInvoiceData]
@OracleFileID INT=NULL,
@RequestedFrom VARCHAR(10)=NULL
AS
BEGIN

IF @RequestedFrom='AP'
BEGIN
        SELECT  
			SupplierPaymentId [InvoiceID] ,
			OrderDate [InvoiceDate],
			OracleId [VendorNumber],
			SupplierAddressCode [VendorSiteCode],
			OrderCurrencyIsoCode [CurrencyCode],
			SuggestedAmount [Amount],
			CAT.Guid [ProviderID],
			CONVERT(VARCHAR(10),FORMAT(CheckOutDate,'dd/MM/yyyy'))+' '+RR.OrderReference+' '+(C.FirstName+' '+C.LastName) [Description]
		FROM 
			LB_Supplier_Payments LSP 
			INNER JOIN ucommerce_OrderLine OL ON LSP.OrderId=OL.OrderId AND LSP.OrderLineId=OL.OrderLineId
			INNER JOIN LB_RoomReservation RR ON OL.OrderLineId=RR.OrderLineId
			INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId=PO.OrderId AND LSP.OrderId=OL.OrderId
			INNER JOIN LB_CustomerAttribute CA ON PO.CustomerId=CA.CustomerId
			INNER JOIN uCommerce_Customer C ON CA.CustomerId=C.CustomerId
			INNER JOIN Ucommerce_product P ON OL.Sku = P.Sku
			INNER JOIN uCommerce_CategoryProductRelation CPR ON P.Productid = CPR.Productid
			INNER JOIN uCommerce_Category CAT ON CPR.Categoryid = CAT.Categoryid
		WHERE 
			LSP.OracleFileId=@OracleFileID AND
			PO.CustomerId IS NOT NULL AND
			LSP.PostedAmount IS NOT NULL
		ORDER BY 
		    SupplierPaymentId ASC
END
ELSE IF @RequestedFrom='AR'
BEGIN
		SELECT  
			LVP.InvoiceId [InvoiceID] ,
			LVP.OrderPaymentDate [InvoiceDate],
			LVP.CustomerId [CustomerId],
			LVP.CustomerAddressCode [CustomerAddressCode],
			LVP.OrderCurrencyIsoCode [CurrencyCode],
			LVP.NetAmount [Amount],
			CONVERT(VARCHAR(10),FORMAT(CheckOutDate,'dd/MM/yyyy'))+' '+RR.OrderReference+' '+(C.FirstName+' '+C.LastName) [Description],
			CAT.Guid [ProviderID],
			LVP.SupplierVatPaymentId [SupplierVatPaymentId],
			LVP.LineNumber [LineNumber]
		FROM 
			LB_VAT_Payments LVP 
			INNER JOIN ucommerce_OrderLine OL ON LVP.OrderId=OL.OrderId AND LVP.OrderLineId=OL.OrderLineId
			INNER JOIN LB_RoomReservation RR ON OL.OrderLineId=RR.OrderLineId
			INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId=PO.OrderId AND LVP.OrderId=OL.OrderId
			INNER JOIN LB_CustomerAttribute CA ON PO.CustomerId=CA.CustomerId
			INNER JOIN uCommerce_Customer C ON CA.CustomerId=C.CustomerId
			INNER JOIN Ucommerce_product P ON OL.Sku = P.Sku
			INNER JOIN uCommerce_CategoryProductRelation CPR ON P.Productid = CPR.Productid
			INNER JOIN uCommerce_Category CAT ON CPR.Categoryid = CAT.Categoryid
		WHERE 
			LVP.OracleFileId=@OracleFileID AND
			PO.CustomerId IS NOT NULL AND
			LVP.PostedVatAmount IS NOT NULL
		ORDER BY 
		    SupplierVatPaymentId ASC
END						

END




