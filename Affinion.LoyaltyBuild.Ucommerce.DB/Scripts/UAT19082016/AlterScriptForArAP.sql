﻿ALTER TABLE dbo.LB_SupplierPaymentRequests
ALTER COLUMN SuggestedProviderAmount DECIMAL(18,2)

ALTER TABLE dbo.LB_Vat_Payments
ALTER COLUMN GrossAmount DECIMAL(18,2)

ALTER TABLE dbo.LB_Vat_Payments
ALTER COLUMN SuggestedVatAmount DECIMAL(18,2)

ALTER TABLE dbo.LB_Vat_Payments
ALTER COLUMN NetAmount DECIMAL(18,2)

ALTER TABLE dbo.LB_Vat_Payments
ALTER COLUMN PostedVatAmount DECIMAL(18,2)