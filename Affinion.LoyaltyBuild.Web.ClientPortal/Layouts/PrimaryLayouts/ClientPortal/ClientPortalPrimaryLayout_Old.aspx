﻿<%@ Page Language="c#" CodePage="65001" AutoEventWireup="true" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.PrimaryLayouts.ClientPortalPrimaryLayout" CodeBehind="ClientPortalPrimaryLayout_Old.aspx.cs" %>

<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ OutputCache Location="None" VaryByParam="none" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <%-- <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="CODE_LANGUAGE" content="C#" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="/default.css" rel="stylesheet" />--%>
    <sc:VisitorIdentification runat="server" />

    <!--  -->
    <script src="Resources/scripts/jquery.min.js" type="text/javascript"></script>
    <script src="Resources/bootstrap_assets/javascripts/bootstrap.min.js" type="text/javascript"></script>
    <script src="Resources/scripts/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="Resources/scripts/jquery.bxslider.min.js" type="text/javascript"></script>


    <link rel="stylesheet" href="Resources/styles/bootstrap.min.css"/>
    <link href="Resources/styles/jquery.bxslider.css" rel="stylesheet" />
    <link rel="stylesheet" href="Resources/styles/affinion.min.css"/>
    <link rel="stylesheet" href="Resources/styles/theme1/theme1.css"/>

    <!-- bxSlider Javascript file -->

    <!-- bxSlider CSS file -->

    <script type="text/javascript">
        $(function () {
            $("#collapse-button").click(function () {
                $("#accordion").toggle();
            });
        });
        $(function () {
            $("#collapse-button-tit").click(function () {
                $("#top-des").toggle();
            });
        });
    </script>


</head>
<body>
    <%--<form method="post" runat="server" id="mainform">-->
                       
               <%--  </form>--%>

    <div class="container-outer width-full overflow-hidden-att">


        <div class="container-outer-inner width-full header-top-bar">
            <div class="container">
                <!-- The justified navigation menu is meant for single line per list item.
           Multiple lines will require custom code not provided by Bootstrap. -->
                <div class="row coomon-outer-padding">

                    <div class="col-md-6 col-xs-6">
                        <div class="logo">
                            <a href="#">
                                <img src="Resources/images/logo.png" class="img-responsive" alt="logo"/></a>
                        </div>
                    </div>

                    <div class="col-md-6 col-xs-6">
                        <div class="header-right-content-nav width-full">
                            <div class="container-fluid">
                                <nav class="navbar navbar-default no-margin-bottom">
        <div class="container-fluid">
          <div class="navbar-header">
            <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>
          <div class="navbar-collapse collapse" id="navbar" aria-expanded="false" style="height: 1px;">
            <ul class="nav navbar-nav">
              <li><a href="#">HOME</a></li>
              <li><a href="#">REAL REWARDS</a></li>
              <li><a href="#">BACK TO SUPERVALU.IE</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
                            </div>
                        </div>
                        <div class="tp-number float-right">
                            <p>0818 220 088</p>
                        </div>
                    </div>

                </div>

            </div>
        </div>


        <!-- -->
        <div class="width-full">
            <!-- Jumbotron -->
            <div class="row slider margin-common">
                <div class="col-md-12">
                    <div class="slider-top">


                        <div id="bb1" class="LBranding">
                            <sc:Placeholder Key="PromoBox1" runat="server" />
                            <div class="container container-inner-relative">


                                <div class="book-top">
                                    <sc:Placeholder Key="PromoBox2" runat="server" />
                                </div>



                            </div>



                        </div>

                    </div>

                </div>
            </div>
            
        </div>

        <div class="container">
            
            <!-- Example row of columns -->
            <sc:Placeholder Key="PromoBox3" runat="server" />
            
            <div class="row footer-top midle-content-margin-top">
                <div class="col-md-6 col-sm-6">

                    <div class="adv-blog">
                        <sc:Placeholder Key="PromoBox4" runat="server" />
                    </div>

                    <div class="resource">
                        <sc:Placeholder Key="PromoBox5" runat="server" />
                    </div>
                </div>

                <div class="col-md-6 col-sm-6">
                    <div class="visible-xs">
                        <div class="top-desti-ti margin-bottom-common">
                            <a id="collapse-button-tit" href="#">
                                <span class="glyphicon glyphicon-road"></span>Show Top Destination
                            </a>
                        </div>
                    </div>
                    <div class="header-top-deatail-tit" id="top-des">
                        <h3 class="margin-bottom-common header-top-deatail">
                            <sc:Text Field="Des_Title" runat="server" />
                        </h3>

                        <sc:Placeholder Key="PromoBox6" runat="server" />


                        <div class="top-des">
                            <a href="#">More destinations &gt;&gt;</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        
        <!-- Site footer -->
        <div class="row footer-bottom margin-common">
            <footer class="footer">
        <div class="container-outer-inner width-full footer-bottom-bar">
         <div class="container">
        <div class="col-sm-4">
            <div class="footer-left-area">
            <h2>INFORMATION</h2>
            <ul class="no-padding">
            <li><a href="#">HOW TO BOOK </a></li>
            <li><a href="#">FAQ'S </a></li>
             <li><a href="#">MY BOOKINGS  </a></li>
            <li><a href="#">CONTACT US  </a></li>
             <li><a href="#">TERMS AND CONDITIONS</a></li>
            <li><a href="#">PRIVACY POLICY  </a></li>
              <li><a href="#">SUPERVALU GETAWAY BREAKS</a></li>
            <li><a href="#">COOKIE POLICY   </a></li>     
            </ul>
            </div>
        </div>
            <div class="col-sm-4">
            <div class="footer-middle-area">
            <h2>CHOOSE A BREAK</h2>
            <ul class="no-padding">
            <li><a href="#">HOME </a></li>
            <li><a href="#">B&amp;B + DINNER HOTEL BREAKS </a></li>
             <li><a href="#">B&amp;B HOTEL BREAKS</a></li>
            <li><a href="#">INSPIRED COLLECTION</a></li>
             <li><a href="#">LUXURY SPA HOTEL BREAKS</a></li>
            <li><a href="#">SLEF CATERING BREAKS</a></li>
              <li><a href="#">VOUCHERS</a></li>    
            </ul>
            </div>             
        </div> 
           <div class="col-sm-4">
            <div class="footer-middle-area">
            <h2>FIND US</h2>
            <div class="social-footer">
              <a href="#" class="ico-fb float-left social-icon-margin"></a>
                <a href="#" class="ico-tw float-left social-icon-margin"></a> 
                 <a href="#" class="ico-gg float-left"></a> 
            </div>
            <div class="clearfix">
                <h2>ONLINE SECURITY AND PAYMENT</h2>
                <div class="payment-icon">
                <img src="Resources/images/PAYMENT.png" alt="" class="img-responsive"/>
                </div>
            </div>
            </div> 
                
        </div>
             <div class="copyright-text">
             <p> © 2015 Loyaltybuild. All rights reserved.</p>
             </div>
             
          </div>    
        </div>
      </footer>
        </div>

    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.bxslider').bxSlider({ auto: true, autoControls: true });
        });
    </script>

</body>
</html>
