﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Provider
{
    /// <summary>
    /// Service to define all Provider related actions 
    /// </summary>
    public class ProviderService : IProviderService
    {
        //Path for basket page
        private const string BasketPageQueryPath = "/sitecore/content/#admin-portal#/#supplier-setup#//*[@@TemplateName='SupplierDetails' and @IsActive='1']"; // "/sitecore/content/#admin-portal#/#supplier-setup#/hotels/*[@@templatename='SupplierDetails' ]";

        /// <summary>
        /// Get Accomodation Details By Item ID
        /// </summary>
        /// <param name="itemId">itemId</param>
        /// <returns>Object of type IAccomodation</returns>
        public IAccomodation GetAccomodationDetailsById(string itemId)
        {
            Item item = Sitecore.Context.Database.GetItem(itemId);

            if (item == null)
                return null;

            return GetItem(item);
        }

        /// <summary>
        /// Get Accomodation Details By UCommerce ID
        /// </summary>
        /// <param name="uCommerceId">uCommerceId</param>
        /// <returns>Object of type IAccomodation</returns>
        public IAccomodation GetAccomodationDetailsByUcommerceId(string uCommerceId)
        {
            Item item = Sitecore.Context.Database.GetItem(uCommerceId);
            string providerSku = item.Fields["SKU"].Value;
            //var item = Sitecore.Context.Database.SelectItems(BasketPageQueryPath).Where(x => x.Fields["SKU"].Value.Equals(uCommerceId, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
            var hotelitem = Sitecore.Context.Database.SelectItems(BasketPageQueryPath).Where(x => x.Fields["SKU"].Value == providerSku).FirstOrDefault();
            if (hotelitem == null)
                return null;

            return GetItem(hotelitem); 
        }

        //Private methods
        #region private menthods
        /// <summary>
        /// Get Item Information
        /// </summary>
        /// <param name="item">item</param>
        /// <returns>Object of type IAccomodation</returns>
        private static IAccomodation GetItem(Item item)
        {
            string imageUrl = string.Empty;
            Item countryItem = (item.Fields["Country"] != null && !string.IsNullOrWhiteSpace(item.Fields["Country"].Value)) ? ((Sitecore.Data.Fields.LookupField)item.Fields["Country"]).TargetItem : null;
            string countryName = (countryItem != null) ? countryItem.Fields["CountryName"].Value : string.Empty;

            Sitecore.Data.Fields.ImageField hotelimgFld = item.Fields["IntroductionImage"];
            if (hotelimgFld != null && hotelimgFld.MediaItem != null)
            {
                Sitecore.Data.Items.MediaItem image = new Sitecore.Data.Items.MediaItem(hotelimgFld.MediaItem);
                imageUrl = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image));
            }

            var accomodation = new Accomodation
            {
                HotelItemId = item.ID.ToString(),
                Title = item.Fields["Name"].Value,
                Phone = item.Fields["Phone"].Value,
                EmailId = item.Fields["EmailID2"].Value,
                Pincode = item.Fields["Postcode"].Value,
                AddressLine1 = item.Fields["AddressLine1"].Value,
                AddressLine2 = item.Fields["AddressLine2"].Value,
                AddressLine3 = item.Fields["AddressLine3"].Value,
                AddressLine4 = item.Fields["AddressLine4"].Value,
                Town = item.Fields["Town"].Value,
                CountryName = countryName,
                GeoCoordinates = item.Fields["GeoCoordinates"].Value,
                ImageUrl = imageUrl,
            };

            List<string> addressItems = new List<string>{
                accomodation.AddressLine1,
                accomodation.AddressLine2,
                accomodation.AddressLine3,
                accomodation.AddressLine4,
                accomodation.Town,
                accomodation.CountryName
            };
            accomodation.Address = string.Join(", ", addressItems.Where(i => !string.IsNullOrWhiteSpace(i)).ToArray());

            return accomodation;
        }
        #endregion
    }
}
