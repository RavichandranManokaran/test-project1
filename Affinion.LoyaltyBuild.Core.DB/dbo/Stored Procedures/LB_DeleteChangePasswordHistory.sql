﻿CREATE PROCEDURE [dbo].[LB_DeleteChangePasswordHistory]
	   @UserName    varchar(200)       
AS
BEGIN 
SET NOCOUNT ON 

     Delete           
     FROM   [dbo].[LB_ChangePasswordHistory]  
     WHERE  
            UserName = @UserName

END 

