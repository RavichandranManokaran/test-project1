﻿/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           EmailSentITandCallCenterAgent.cs
/// Description:           Email Scheduler for IT and CallCenterAgent
/// </summary>

using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports;
using ClosedXML.Excel;
using Sitecore.Data;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;




namespace Affinion.LoyaltyBuild.Web.ClientPortal.EmailScheduler
{
    /// <summary>
    /// List of Email for IT and CallCenterAgent
    /// </summary>
    public class EmailSchedulerForCallCenterAgent
    {
        public string itemData { get; set; }

        /// <summary>
        /// To Execute sitecore Scheduler
        /// </summary>
        /// <param name="items">sitecore item</param>
        /// <param name="command">commandItem</param>
        /// <param name="schedule">SchedulerItem</param>
        public void Run(Sitecore.Data.Items.Item[] items, Sitecore.Tasks.CommandItem command, Sitecore.Tasks.ScheduleItem schedule)
        {
            itemData = "{32BB2160-B68F-4A98-9E56-869981EFF445}";
            //To make sure runs only once per day
            if (schedule.LastRun.Date != DateTime.Now.Date)
            {
                Sitecore.Data.Database db = Sitecore.Data.Database.GetDatabase("web");
                if (db != null)
                {
                    Item Data = db.GetItem(Sitecore.Data.ID.Parse(string.IsNullOrEmpty(itemData)?string.Empty :itemData));
                    if (Data != null)
                    {
                        string dReceiverEmail = !string.IsNullOrWhiteSpace(Data.Fields["Default Receiver Email Address"].ToString()) ? Data.Fields["Default Receiver Email Address"].ToString() : string.Empty;
                        if (dReceiverEmail != null)
                        {
                            mailGenarate(dReceiverEmail, Data);
                        }
                    }
                }
            }
            
        }
        /// <summary>
        /// Mail Generate Component and Body Part
        /// </summary>
        /// <param name="receiverId"></param>
        /// <param name="Details"></param>
        public void mailGenarate(string receiverId, Item Details)
        {
            try
            {
                Affinion.LoyaltyBuild.Communications.IEmailService emailService = new Affinion.LoyaltyBuild.Communications.Services.EmailService();
                string senderID = SitecoreFieldsHelper.GetValue(Details, "Sender Email Address");

                if (string.IsNullOrEmpty(senderID))
                    return;

                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("EmailSchedulerForCallCenterAgent: senderId-{0}", senderID));
                string multiEmail = SitecoreFieldsHelper.GetValue(Details, "Other Receivers Email Address");
                List<string> ccEmail = null;
                string mailSubject = !string.IsNullOrWhiteSpace(SitecoreFieldsHelper.GetValue(Details, "Email Subject")) ? SitecoreFieldsHelper.GetValue(Details, "Email Subject") : string.Empty;
                mailSubject = mailSubject.Replace("{Today's Date}", DateTime.Today.ToString("dd/MM/yyyy"));
                if (!string.IsNullOrWhiteSpace(multiEmail))
                {
                    ccEmail = multiEmail.TrimEnd(';').Split(';').Select(x => x.Trim()).ToList<string>();
                }
                string msgBodyTemplate = !string.IsNullOrWhiteSpace(SitecoreFieldsHelper.GetValue(Details, "Email Body")) ? SitecoreFieldsHelper.GetValue(Details, "Email Body") : string.Empty;
                string msgBody = GetMailText(msgBodyTemplate);
                DataTable table = new DataTable();
                DataSet ds = uCommerceConnectionFactory.EmailToCallCenterAgent();
                table = ds.Tables[0];
                //var bytes = Encoding.GetEncoding("iso-8859-1").GetBytes(ToCSV(table));
                //var attributes = new MemoryStream(bytes);
                if (table != null && table.Rows.Count > 0)
                {
                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        wb.Worksheets.Add(table, "EmailSchedulerForCallCenterAgent");
                        //wb.SaveAs(folderPath + "HotelDetails.xlsx");
                        //Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("workflowtest excel export// {0}", "ending"));
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            //Save the Excel Workbook to MemoryStream.
                            wb.SaveAs(memoryStream);

                            //Convert MemoryStream to Byte array.
                            byte[] bytes = memoryStream.ToArray();
                            var attributes = new MemoryStream(bytes);
                            memoryStream.Close();
                            if (ccEmail != null)
                            {
                                emailService.Send(senderID, receiverId, ccEmail, mailSubject, msgBody, true, attributes, "EmailSchedulerForCallCenterAgent.xls", "application/vnd.ms-excel");
                            }
                            else
                            {
                                emailService.Send(senderID, receiverId, mailSubject, msgBody, true, attributes, "EmailSchedulerForCallCenterAgent.xls", "application/vnd.ms-excel");
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("EmailSchedulerForCallCenterAgent: Exception-{0}", exception.ToString()));
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
            }
        }

        /// <summary>
        /// Mail Body Part
        /// </summary>
        /// <param name="bodyText"></param>
        /// <returns></returns>
        public string GetMailText(string bodyText)
        {
            if (bodyText != null)
            {
                bodyText = bodyText.Replace("{Today's Date}", DateTime.Today.ToString("dd/MM/yyyy"));
                return bodyText;
            }
            return string.Empty;
        }

        /// <summary>
        /// Converting DataTable Value to CSV
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public static string ToCSV(DataTable table)
        {
            if (table != null)
            {
                var result = new StringBuilder();
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    result.Append(table.Columns[i].ColumnName);
                    result.Append(i == table.Columns.Count - 1 ? "\n" : "");
                }

                foreach (DataRow row in table.Rows)
                {
                    for (int i = 0; i < table.Columns.Count; i++)
                    {
                        result.Append(row[i].ToString());
                        result.Append(i == table.Columns.Count - 1 ? "\n" : "");
                    }
                }


                return result.ToString();
            }
            return null;
        }
    }
}



