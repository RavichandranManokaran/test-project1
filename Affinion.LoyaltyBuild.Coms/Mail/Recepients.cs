﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Comms.Mail
{
    /// <summary>
    /// class for User information
    /// </summary>
    public class Recepient
    {
        /// <summary>
        /// User Name
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// EmailId
        /// </summary>
        public string EmailId { get; set; }
    }
}
