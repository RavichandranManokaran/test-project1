﻿CREATE TABLE [dbo].[uCommerce_SubrateItem](
	[SubRateItemCode] [nvarchar](50) NOT NULL,
	[DisplayName] [nvarchar](150) NULL,
 CONSTRAINT [PK_uCommerce_SubrateItem] PRIMARY KEY CLUSTERED 
(
	[SubRateItemCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
