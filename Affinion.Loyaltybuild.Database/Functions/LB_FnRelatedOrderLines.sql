﻿/****** Object:  UserDefinedFunction [dbo].[LB_FnRelatedOrderLines]    Script Date: 2/12/2016 11:47:02 AM ******/
DROP FUNCTION [dbo].[LB_FnRelatedOrderLines]
GO

/****** Object:  UserDefinedFunction [dbo].[LB_FnRelatedOrderLines]    Script Date: 2/12/2016 11:47:02 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Prasanna SR
-- Create date: 10 Feb 2016
-- Description:	Gets the list of releated orderlines for transfer order
-- =============================================
CREATE FUNCTION [dbo].[LB_FnRelatedOrderLines] 
(	
	@OrderLineId INT
)
RETURNS @RelatedOrderLinesIDTable TABLE
(
	OrderLineId INT
) 
AS
BEGIN
	DECLARE @OrderRef VARCHAR(50)
	DECLARE @NewOrderRef VARCHAR(50)

	SELECT @OrderRef = OrderReference, @NewOrderRef = NewOrderReference FROM LB_RoomReservation WHERE OrderLineId = @OrderLineId
	INSERT INTO @RelatedOrderLinesIDTable VALUES (@OrderLineId)
	
	WHILE EXISTS (SELECT 1 FROM [dbo].[LB_RoomReservation] WHERE NewOrderReference = @OrderRef)
	BEGIN
		SELECT @OrderLineId = OrderLineId, @OrderRef = OrderReference 
		FROM [dbo].[LB_RoomReservation] 
		WHERE NewOrderReference = @OrderRef	
		INSERT INTO @RelatedOrderLinesIDTable VALUES (@OrderLineId)
	END

	WHILE EXISTS (SELECT 1 FROM [dbo].[LB_RoomReservation] WHERE OrderReference =  @NewOrderRef)
	BEGIN
		SELECT @OrderLineId = OrderLineId, @NewOrderRef = NewOrderReference 
		FROM [dbo].[LB_RoomReservation] 
		WHERE OrderReference = @NewOrderRef	
		INSERT INTO @RelatedOrderLinesIDTable VALUES (@OrderLineId)
	END

	RETURN
END
GO




