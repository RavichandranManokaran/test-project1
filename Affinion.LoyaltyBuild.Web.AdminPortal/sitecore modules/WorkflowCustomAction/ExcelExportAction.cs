﻿using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.Communications;
using ClosedXML.Excel;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Workflows.Simple;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Affinion.LoyaltyBuild.AdminPortal.sitecore_modules.WorkflowCustomAction
{
    public class ExcelExportAction
    {
        private Database _currentDB;
        //Item Email1 = Sitecore.Context.Database.GetItemByKey("EmailContent");
        //string name = SitecoreFieldsHelper.GetValue(Email1, "");
        private string _mailContentPath = "/sitecore/content/admin-portal/global/_supporting-content/workflowemailcontent/emailcontent";
        public void Process(WorkflowPipelineArgs args)
        {
            try
            {
                // currentitem 
                Item workFlowItem = args.DataItem;
                if (workFlowItem.Template.ID.ToString() == "{A7235C94-2EF5-4A3B-87DB-0D4DCF10B4DB}")
                {
                    ExcelExport(workFlowItem);
                }
            }
            catch(Exception exception)
            {

                Diagnostics.Trace(DiagnosticsCategory.Common, string.Format("WorkFlow ExcelExportAction: Exception-{0}", exception.ToString()));
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
            }
        }
        private void ExcelExport(Item hotelItem)
        {
            string add1 = hotelItem.Fields["AddressLine1"].Value;
            DataTable dt = new DataTable();
            // excel sheet columns
            dt.Columns.Add("Address Line 1");
            dt.Columns.Add("Address Line 2");
            dt.Columns.Add("Address Line 3");
            dt.Columns.Add("Address Line 4");
            dt.Columns.Add("Country");
            dt.Columns.Add("Fax");
            dt.Columns.Add("Phone");
            dt.Columns.Add("Reservation Hotel Email Address");
            dt.Columns.Add("Communication Hotel Email Address");
            dt.Columns.Add("Supplier Name");
            dt.Columns.Add("Supplier Super Group");
            dt.Columns.Add("Supplier Group");
            dt.Columns.Add("Star Ranking");
            dt.Columns.Add("Number Of Rooms");
            dt.Columns.Add("Website URL");
            dt.Columns.Add("Do you want to receive Availability Alerts Yes / No");
            dt.Columns.Add("Overview");
            dt.Columns.Add("Facility");
            dt.Columns.Add("Good To Know");
            dt.Columns.Add("Checkin Time");
            dt.Columns.Add("Checkout Time");
            dt.Columns.Add("Directions");
            dt.Columns.Add("Contacts List");


            DataRow row = dt.NewRow();
            //row[""] = hotelItem.Fields[""].Value;
            row["Address Line 1"] = hotelItem.Fields["AddressLine1"].Value;
            row["Address Line 2"] = hotelItem.Fields["AddressLine2"].Value;
            row["Address Line 3"] = hotelItem.Fields["AddressLine3"].Value;
            row["Address Line 4"] = hotelItem.Fields["AddressLine4"].Value;
            string country = SitecoreFieldsHelper.GetDropLinkFieldValue(hotelItem, "Country", "CountryName");
            row["Country"] = country;// hotelItem.Fields["Country"].Name;
            row["Fax"] = hotelItem.Fields["Fax"].Value;
            row["Phone"] = hotelItem.Fields["Phone"].Value;
            row["Reservation Hotel Email Address"] = hotelItem.Fields["EmailID1"].Value;
            row["Communication Hotel Email Address"] = hotelItem.Fields["EmailID2"].Value;
            row["Supplier Name"] = hotelItem.Fields["Name"].Value;
            string SuperGroup = SitecoreFieldsHelper.GetDropLinkFieldValue(hotelItem, "SuperGroup", "Name");
            row["Supplier Super Group"] = SuperGroup;// hotelItem.Fields["SuperGroup"].Name;
            string Group = SitecoreFieldsHelper.GetDropLinkFieldValue(hotelItem, "Group", "Name");
            row["Supplier Group"] = Group;// hotelItem.Fields["Group"].Name;
            string StarRanking = SitecoreFieldsHelper.GetDropLinkFieldValue(hotelItem, "StarRanking", "Name");
            row["Star Ranking"] = StarRanking;// hotelItem.Fields["StarRanking"].Name;
            row["Number Of Rooms"] = hotelItem.Fields["NumberOfRooms"].Value;
            row["Website URL"] = hotelItem.Fields["WebSiteURL"].Value;
            row["Do you want to receive Availability Alerts Yes / No"] = hotelItem.Fields["IsAvailabilityUpdateEnabled"].Value;
            row["Overview"] = hotelItem.Fields["Overview"].Value;
            row["Facility"] = hotelItem.Fields["Facility"].Value;
            row["Good To Know"] = hotelItem.Fields["GoodToKnow"].Value;
            row["Checkin Time"] = hotelItem.Fields["CheckinTime"].Value;
            row["Checkout Time"] = hotelItem.Fields["CheckoutTime"].Value;
            row["Directions"] = hotelItem.Fields["Directions"].Value;
            //multilist
            Sitecore.Data.Fields.MultilistField obj = hotelItem.Fields["ContactsList"];
            var items = obj.GetItems();
            string contact = string.Empty;
            foreach (Item itm in items)
            {
                contact = contact + itm.Name + ',';
            }
            row["Contacts List"] = contact;
            dt.Rows.Add(row);

            string emailTo = hotelItem.Fields["EmailID2"].Value;
            _currentDB = Sitecore.Configuration.Factory.GetDatabase("web");
            Item mailitem = _currentDB.GetItem(_mailContentPath);
            string emailFrom = mailitem.Fields["FromAddress"].Value;
            string emailSubject = mailitem.Fields["Subject"].Value;
            string hotelname = hotelItem.Fields["Name"].Value;
            emailSubject = emailSubject.Replace("{hotel}", !string.IsNullOrWhiteSpace(hotelname) ? hotelname : string.Empty);

            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt, "SupplierDetails");
                //wb.SaveAs(folderPath + "HotelDetails.xlsx");
                //Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("workflowtest excel export// {0}", "ending"));
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    //Save the Excel Workbook to MemoryStream.
                    wb.SaveAs(memoryStream);

                    //Convert MemoryStream to Byte array.
                    byte[] bytes = memoryStream.ToArray();
                    var attributes = new MemoryStream(bytes);
                    memoryStream.Close();
                    Affinion.LoyaltyBuild.Communications.IEmailService emailService = new Affinion.LoyaltyBuild.Communications.Services.EmailService();
                    emailService.Send(emailFrom, emailTo, emailSubject, "", true, attributes, "SupplierDetails.xlsx", "application/vnd.ms-excel");
                }

            }
        }
    }
}