﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Affinion.LoyaltyBuild.DataAccess;
using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
using System.Data;
using Sitecore.Data.Items;
using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Data;
using Sitecore.Data.Items;


namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal
{
    public partial class ClientPortalAddToFavorite : System.Web.UI.UserControl
    {
        DataSet ds = uCommerceConnectionFactory.GetFavoriteList(68);
        protected void Page_Load(object sender, EventArgs e)
        {
            this.SetFavListDataSource();
            DisplayHotel(GetFavoriteHotel());
            getSitecoredata();
        }

        protected void btnSavFavList_Click(object sender, EventArgs e)
        {
            string ListName = txtNewFavList.Text;
            uCommerceConnectionFactory.AddList("leskil99@gmail.com", ListName);
            ds = uCommerceConnectionFactory.GetFavoriteList(68);
            txtNewFavList.Text = string.Empty;
            SetFavListDataSource();
        }

        protected void ddlFavList_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> HotelList = GetFavoriteHotel();
            if (HotelList != null && HotelList.Count > 0)
            {
                this.DisplayHotel(HotelList);
            }
        }

        private void SetFavListDataSource()
        {
            int selectedIndex;
            if (!IsPostBack)
            {
                selectedIndex = Request.QueryString["ListID"] != null ? int.Parse(Request.QueryString["ListID"]) : ddlFavList.SelectedIndex;
            }
            else
            {
                selectedIndex = ddlFavList.SelectedIndex;
            }
            ddlFavList.DataSource = ds.Tables[0].DefaultView;
            ddlFavList.DataValueField = "ListID";
            ddlFavList.DataTextField = "ListName";
            if (selectedIndex >= 0)
            {
                ddlFavList.SelectedIndex = selectedIndex;
            }
            ddlFavList.DataBind();
        }

        private List<String> GetFavoriteHotel()
        {
            List<string> HotelList = new List<string>();
            if (!string.IsNullOrWhiteSpace(ddlFavList.SelectedValue))
            {
                int ListId = Convert.ToInt32(ddlFavList.SelectedValue);
                DataRow[] rows = ds.Tables[0].Select("ListID=" + ListId + "");
                if (rows.Length > 0 && !string.IsNullOrWhiteSpace(rows[0]["HotelID"].ToString()))
                {
                    HotelList = rows[0]["HotelID"].ToString().Split(',').Select(x => x.Trim()).ToList<string>();
                }
            }
            return HotelList;
        }

        private void DisplayHotel(List<string> uCommerceHotelIds)
        {
            DataTable dt = new DataTable();
            string name, completeLocation, ImgUrl, link, hotelId, linkRowName = "link", TitleRowName = "Title", LocationsRowName = "Locations", ImgSrcRowName = "ImgSrc", getHotelID = "HotelId";
            dt.Columns.Add(TitleRowName);
            dt.Columns.Add(LocationsRowName);
            dt.Columns.Add(ImgSrcRowName);
            dt.Columns.Add(linkRowName);
            dt.Columns.Add(getHotelID);
            string hotelPath = Constants.BasketPageQueryPath;
            Item[] HotelItems = Sitecore.Context.Database.SelectItems(hotelPath);
            if (HotelItems != null && HotelItems.Length > 0)
            {
                foreach (string uCommerceHotel in uCommerceHotelIds)
                {
                    Item hotelItem = HotelItems.Where(item => item.ID.ToString().Equals(uCommerceHotel, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                    if (hotelItem != null)
                    {
                        link = "/hotelinformation?id=" + hotelItem.ID.ToString() + "&NoOfRooms=1&NoOfAdults=1&NoOfChildren=0";
                        name = hotelItem.Fields["Name"].Value;
                        hotelId = hotelItem.ID.ToString();

                        Item locationItem = (hotelItem.Fields["Location"] != null && !string.IsNullOrWhiteSpace(hotelItem.Fields["Location"].Value)) ? ((Sitecore.Data.Fields.LookupField)hotelItem.Fields["Location"]).TargetItem : null;
                        Item countryItem = (locationItem != null && locationItem.Fields["Country"] != null && !string.IsNullOrWhiteSpace(locationItem.Fields["Country"].Value)) ? ((Sitecore.Data.Fields.LookupField)locationItem.Fields["Country"]).TargetItem : null;
                        string location = locationItem != null ? locationItem.Fields["Name"].Value : string.Empty;
                        string country = countryItem != null ? countryItem.Fields["CountryName"].Value : string.Empty;
                        completeLocation = (!string.IsNullOrWhiteSpace(location) && !string.IsNullOrWhiteSpace(country)) ? string.Format("{0}, {1}", location, country) : string.Format("{0}{1}", location, country);

                        Sitecore.Data.Fields.ImageField imgField = hotelItem.Fields["IntroductionImage"];
                        ImgUrl = ((imgField.MediaItem) != null) ? Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField.MediaItem) : string.Empty;

                        DataRow row = dt.NewRow();
                        row[linkRowName] = link;
                        row[TitleRowName] = name;
                        row[LocationsRowName] = completeLocation;
                        row[ImgSrcRowName] = ImgUrl;
                        row[getHotelID] = hotelId;
                        dt.Rows.Add(row);
                    }
                }
            }

            rptHotelListing.DataSource = dt;
            rptHotelListing.DataBind();
        }

        protected void ImgClosebtn_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton imgButton = (ImageButton)(sender);
            string hotelId = imgButton.CommandArgument;
            int ListId;
            int.TryParse(ddlFavList.SelectedValue, out ListId);
            uCommerceConnectionFactory.RemoveFavourite(ListId, 68, hotelId);
            //Response.Redirect(Request.Url.AbsoluteUri + "?ListID=" + ddlFavList.SelectedIndex.ToString());

            string ListString = Request.QueryString["ListID"];
            if (string.IsNullOrWhiteSpace(ListString))
            {
                Response.Redirect(Request.Url.AbsoluteUri + "?ListID=" + ddlFavList.SelectedIndex.ToString());
            }
            else
            {
                Response.Redirect(Request.RawUrl);
            }
        }

        private void getSitecoredata()
        {
            Item currentitem = Sitecore.Context.Item;
            SelectText.Item = currentitem;
           btnCreateFavList.Value= SitecoreFieldsHelper.GetItemFieldValue(currentitem, "mybutton");
        }
    }
}