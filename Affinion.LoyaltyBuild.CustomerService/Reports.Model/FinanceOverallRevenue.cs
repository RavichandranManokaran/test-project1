﻿/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/// <summary>
/// Entity to hold finance overall revenue related information
/// </summary>

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model
{
    public class FinanceOverallRevenue
    {
        /// <summary>
        /// Bookings
        /// </summary>
        public int Bookings { get; set; }
        /// <summary>
        /// ClientName
        /// </summary>
        public string ClientName { get; set; }
        /// <summary>
        /// ClientName
        /// </summary>
        public string ClientID { get; set; }
        /// <summary>
        /// ClientOffer
        /// </summary>
        public string ClientOffer { get; set; }
        /// <summary>
        /// CurrencyName
        /// </summary>
        public string CurrencyName { get; set; }
        /// <summary>
        /// AccountingRates
        /// </summary>
        public decimal AccountingRates { get; set; }
        /// <summary>
        /// BudgetRates
        /// </summary>
        public decimal BudgetRates { get; set; }
        /// <summary>
        /// LoyaltyBuildRevenue
        /// </summary>
        public decimal LoyaltyBuildRevenue { get; set; }
        /// <summary>
        /// Count
        /// </summary>
        public int Count { get; set; }
        /// <summary>
        /// BasicPrice
        /// </summary>
        public decimal TotalPrice { get; set; }
        /// <summary>
        /// BasicPrice
        /// </summary>
        public decimal VAT { get; set; }
    }
}
