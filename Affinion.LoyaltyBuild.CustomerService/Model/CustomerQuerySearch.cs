﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    /// <summary>
    /// Service that provides methods to perform all order related actions
    /// </summary>
    public class CustomerQuerySearch
    {

        /// <summary>
        /// Gets or sets the client 
        /// </summary>
        public int? QueryTypeID { get; set; }

        /// <summary>
        /// Gets or sets the client 
        /// </summary>
        public string ClientID { get; set; }

        /// <summary>
        /// Check-in date
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Check-out date
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// First name of the customer
        /// </summary>
        public string FirstName { get; set; }


        /// <summary>
        /// Last name of the customer
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the CompaignGroupID 
        /// </summary>
        public string CampaignGroupID { get; set; }

        /// <summary>
        /// Gets or sets the ProviderName 
        /// </summary>
        public string ProviderId { get; set; }

        /// <summary>
        /// Gets or sets the BookingReference 
        /// </summary>
        public string BookingReference { get; set; }

        /// <summary>
        /// Gets or sets the QueryStatusId 
        /// </summary>
        public bool? IsResolved { get; set; }
    }
}
