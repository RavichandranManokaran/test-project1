﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientPortalBookingDetailsSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal.ClientPortalBookingDetailsSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Sitecore.Data.Items" %>
<%@ Import Namespace="Affinion.LoyaltyBuild.Common.SessionHelper" %>
<div class="booking-detail-info">
    <div class="content">
        <div class="content-top">
            <div class="col-sm-12">
                <div class="checkout-wrap">
                    <asp:Label ID="EmptyBasket" runat="server" Text="" ForeColor="Red"></asp:Label>
                </div>
            </div>
        </div>
        <div class="content-body">
            <div id="CustomerValidationSection" runat="server">
                <sc:placeholder runat="server" id="customervalidation" key="customervalidation" />
            </div>

            <div class="hide-div" id="PersonalDetails" runat="server">
                <div class="row">
                    
                    <div class="col-sm-12 summary-form">
                        <div class="panel-default martop10">
                            <div class="panel-heading bg-primary">
                                <h4 class="panel-title text-center">Validator</h4>
                            </div>
                        </div>
                        <div class="content_bg_inner_box alert-info no-margin">
                            <div class="col-sm-12">
                                <h2 class="no-margin">
                                    <sc:text id="TextPersonalDetails" runat="server" />
                                </h2>
                            </div>
                            <div class="col-sm-6">
                                <div class="row margin-row">
                                    <div class="col-sm-6">
                                       
                                        <sc:text id="Title" field="TitleLabel" runat="server" />
                                    </div>
                                    <div class="col-sm-6">
                                        <asp:DropDownList CssClass="form-control" ID="TitleDropDown" EnableViewState="true" runat="server" AppendDataBoundItems="false" AutoPostBack="false"/>
                                    </div>
                                </div>
                                <div class="row margin-row">
                                    <div class="col-sm-6">
                                        <sc:text id="FirstNameLabel" field="FirstName" runat="server" />
                                        <span class="accent58-f">*</span>
                                    </div>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="FirstName" CssClass="form-control" Text="" ClientIDMode="Static" runat="server" />
                                        <asp:RequiredFieldValidator  Display="Dynamic" runat="server" ID="FirstNameValidator" CssClass="accent58-f"  ControlToValidate="FirstName" ValidationGroup="CustomerDetailsValidation">
                                            <sc:text id="FirstNameRequiredMessage" runat="server" />
                                        </asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator Display="Dynamic" CssClass="accent58-f" runat="server" ControlToValidate="FirstName" ValidationExpression="^([\'\&quot;&quot;\,\%\&lt;\&gt;\/\;\\a-zA-ZäåöæâéèêøóòôÄÖÅÆØ0-9\s])*$" ValidationGroup="CustomerDetailsValidation">
                                            <sc:text id="revTextFirstName" field="FirstName" runat="server" />
                                        </asp:RegularExpressionValidator>
                                    </div>
                                    <div class="col-sm-6">
                                    </div>
                                </div>
                                <div class="row margin-row">
                                    <div class="col-sm-6">
                                        <sc:text id="LastNameLabel" field="LastName" runat="server" />
                                        <span class="accent58-f">*</span>
                                    </div>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="LastName" CssClass="form-control" Text="" ClientIDMode="Static" runat="server" />
                                        <asp:RequiredFieldValidator  Display="Dynamic" runat="server" ID="LastNameValidator" ControlToValidate="LastName" CssClass="accent58-f" ValidationGroup="CustomerDetailsValidation">
                                            <sc:text id="LastNameRequiredMessage" runat="server" />
                                        </asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator Display="Dynamic" CssClass="accent58-f" runat="server" ControlToValidate="LastName" ValidationExpression="^([\'\&quot;&quot;\,\%\&lt;\&gt;\/\;\\a-zA-ZäåöæâéèêøóòôÄÖÅÆØ0-9\s])*$" ValidationGroup="CustomerDetailsValidation">
                                            <sc:text id="revTextLastName" field="LastName" runat="server" />
                                        </asp:RegularExpressionValidator>
                                    </div>
                                    <div class="col-sm-6">
                                    </div>
                                </div>
                                <div class="row margin-row">
                                    <div class="col-sm-6">
                                        <sc:text id="Address1" field="AddressLine1" runat="server" />
                                        <span class="accent58-f">*</span>
                                    </div>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="AddressLine1" CssClass="form-control" Text="" ClientIDMode="Static" runat="server" />
                                        <asp:RequiredFieldValidator Display="Dynamic" runat="server" ID="AddressLine1Validator" CssClass="accent58-f" ControlToValidate="AddressLine1" ValidationGroup="CustomerDetailsValidation">
                                            <sc:text id="AddressLine1RequiredMessage" runat="server" />
                                        </asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator Display="Dynamic" CssClass="accent58-f" runat="server" ControlToValidate="AddressLine1" ValidationExpression="^[^<>'\/;`%?\&quot;]*$" ValidationGroup="CustomerDetailsValidation">
                                            <sc:text id="revTextAddressLine1" field="AddressLine1" runat="server" />
                                        </asp:RegularExpressionValidator>                                        
                                    </div>
                                    <div class="col-sm-6">
                                    </div>
                                </div>
                                <div class="row margin-row">
                                    <div class="col-sm-6">
                                        <sc:text id="Address2" field="AddressLine2" runat="server" />
                                    </div>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="AddressLine2" CssClass="form-control" Text="" ClientIDMode="Static" runat="server" />
                                        <asp:RegularExpressionValidator Display="Dynamic" CssClass="accent58-f" runat="server" ControlToValidate="AddressLine2" ValidationExpression="^[^<>'\/;`%?\&quot;]*$" ValidationGroup="CustomerDetailsValidation">
                                            <sc:text id="revTextAddressLine2" field="AddressLine2" runat="server" />
                                        </asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <div class="row margin-row">
                                    <div class="col-sm-6">
                                        <sc:text id="Address3" field="AddressLine3" runat="server" />
                                    </div>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="AddressLine3" CssClass="form-control" Text="" ClientIDMode="Static" runat="server" />
                                        <asp:RegularExpressionValidator Display="Dynamic" CssClass="accent58-f" runat="server" ControlToValidate="AddressLine3" ValidationExpression="^[^<>'\/;`%?\&quot;]*$" ValidationGroup="CustomerDetailsValidation">
                                            <sc:text id="revTextAddressLine3" field="AddressLine3" runat="server" />
                                        </asp:RegularExpressionValidator>
                                    </div>
                                </div>
                                <div class="row margin-row">
                                    <div class="col-sm-6">
                                        <sc:text id="CountyRegion" field="CountyRegion" runat="server" />
                                        <span class="accent58-f">*</span>
                                    </div>
                                    <div class="col-sm-6">
                                        <asp:DropDownList CssClass="form-control" ID="CountyRegionDropdown" EnableViewState="true" runat="server" />
                                        <asp:RequiredFieldValidator ID="RequiredCountyRegionvalidator" runat="server" CssClass="accent58-f" Display="Dynamic" InitialValue="Please Select" ControlToValidate="CountyRegionDropdown" ValidationGroup="CustomerDetailsValidation">
                                            <sc:text id="RequiredCountryRegionMessage" runat="server" />
                                        </asp:RequiredFieldValidator>                                        
                                    </div>
                                    <div class="col-sm-6">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="row margin-row">
                                    <div class="col-sm-6">
                                        <sc:text id="Country" field="Country" runat="server" />
                                        <span class="accent58-f">*</span>
                                    </div>
                                    <div class="col-sm-6">
                                        <asp:DropDownList CssClass="form-control" ID="CountriesDropdown" EnableViewState="true" runat="server" AppendDataBoundItems="false" AutoPostBack="false"/>
                                        <asp:RequiredFieldValidator ID="RequiredCountryvalidator" runat="server" InitialValue="Please Select" Display="Dynamic" CssClass="accent58-f" ControlToValidate="CountriesDropdown" ValidationGroup="CustomerDetailsValidation">
                                            <sc:text id="RequiredCountryMessage" runat="server" />
                                        </asp:RequiredFieldValidator>                                        
                                    </div>
                                    <div class="col-sm-6">
                                    </div>
                                </div>
                                <div class="row margin-row">
                                    <div class="col-sm-6">
                                        <sc:text id="Mobile" field="MobilePhone" runat="server" />
                                        <span class="accent58-f">*</span>
                                    </div>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="MobilePhone" CssClass="form-control" Text="" ClientIDMode="Static" runat="server" />
                                        <asp:RequiredFieldValidator Display="Dynamic" runat="server" ID="RequiredMobilePhoneValidator" CssClass="accent58-f" ControlToValidate="MobilePhone" ValidationGroup="CustomerDetailsValidation">
                                            <sc:text id="RequiredMobilePhoneMessage" runat="server" />
                                        </asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator Display="Dynamic" CssClass="accent58-f" runat="server" ControlToValidate="MobilePhone" ValidationExpression="^[0-9]{5,15}$" ValidationGroup="CustomerDetailsValidation">
                                            <sc:text id="MobilePhoneValidationMessage" runat="server" />
                                        </asp:RegularExpressionValidator>                                       
                                    </div>
                                    <div class="col-sm-6">
                                    </div>
                                </div>
                                <div class="row margin-row">
                                    <div class="col-sm-6">
                                        <sc:text id="Phonenumber" field="Phone" runat="server" />
                                        <span class="accent58-f">*</span>
                                    </div>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="Phone" CssClass="form-control" Text="" ClientIDMode="Static" runat="server" />
                                        <asp:RequiredFieldValidator Display="Dynamic" runat="server" ID="RequiredPhoneValidator" CssClass="accent58-f" ControlToValidate="Phone" ValidationGroup="CustomerDetailsValidation">
                                            <sc:text id="PhoneNumberRequiredMessage" runat="server" />
                                        </asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator Display="Dynamic" CssClass="accent58-f" runat="server" ControlToValidate="Phone" ValidationExpression="^[0-9]{5,15}$" ValidationGroup="CustomerDetailsValidation">
                                            <sc:text id="PhoneValidationMessage" runat="server" />
                                        </asp:RegularExpressionValidator>
                                       
                                    </div>
                                    <div class="col-sm-6">
                                    </div>
                                </div>
                                <div class="row margin-row">
                                    <div class="col-sm-6">
                                        <sc:text id="Email" field="Email" runat="server" />
                                        <span class="accent58-f">*</span>
                                    </div>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="PersonalEmailAddress" CssClass="form-control" Text="" ClientIDMode="Static" runat="server" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" placeholder="Enter Email Address" runat="server" CssClass="accent58-f" ControlToValidate="PersonalEmailAddress" ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$" Display="Dynamic" ValidationGroup="CustomerDetailsValidation">
                                            <sc:text id="EmailValidationFieldMessage" runat="server" />
                                        </asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator runat="server" CssClass="accent58-f" ControlToValidate="PersonalEmailAddress" Display="Dynamic" ValidationGroup="CustomerDetailsValidation">
                                            <sc:text id="EmailRequiredMessage" runat="server" />
                                        </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="row margin-row">
                                    <div class="col-sm-6">
                                        <sc:text id="ConfirmEmail" field="ConfirmEmail" runat="server" />
                                        <span class="accent58-f">*</span>
                                    </div>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="ConfirmPersonalEmailAddress" CssClass="form-control" Text="" ClientIDMode="Static" runat="server" />
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" CssClass="accent58-f" Display="Dynamic" placeholder="Confirm Email Address" ControlToCompare="ConfirmPersonalEmailAddress" ControlToValidate="PersonalEmailAddress" ForeColor="Red" ValidationGroup="EmailValidationGroup">
                                            <sc:text id="EmailMismatchMessage" runat="server" />
                                        </asp:CompareValidator>
                                        <asp:RequiredFieldValidator ID="cnfrmEmailValidator" CssClass="accent58-f" runat="server" Display="Dynamic" ControlToValidate="ConfirmPersonalEmailAddress" ForeColor="Red" ValidationGroup="CustomerDetailsValidation">
                                            <sc:text id="EmailRequiredMessage2" runat="server" />
                                        </asp:RequiredFieldValidator>                                        
                                    </div>
                                    <div class="col-sm-6">
                                    </div>
                                </div>
                                <div class="row margin-row">
                                    <div class="col-sm-6">
                                        <sc:text id="Store" field="Store" runat="server" />
                                        <span class="accent58-f">*</span>
                                    </div>
                                    <div class="col-sm-6">
                                        <asp:DropDownList CssClass="form-control" ID="ClientStoreDropdown" EnableViewState="true" AppendDataBoundItems="false" AutoPostBack="false" runat="server" />

                                        <asp:RequiredFieldValidator ID="RequiredClientStorevalidator1" CssClass="accent58-f" runat="server" Display="Dynamic" InitialValue="Please Select" ControlToValidate="ClientStoreDropdown" ForeColor="Red" ValidationGroup="CustomerDetailsValidation">
                                            <sc:text id="RequiredClientStoreMessage" runat="server" />
                                        </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="row margin-row" style="display: none;">
                                    <div class="col-sm-6">
                                        <%--Language:--%>
                                        <sc:text id="Language" field="Language" runat="server" />
                                        <span class="accent58-f">*</span>
                                    </div>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="LanguageDropDown" CssClass="form-control" EnableViewState="true" runat="server" AppendDataBoundItems="false" AutoPostBack="false"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row margin-row">
                                    <div class="col-sm-12 pay-chek">
                                        <div class="checkbox">
                                            <label>
                                                <asp:CheckBox ID="SendEmails" runat="server" />
                                                <sc:text id="SpecialOffersAndDetails" field="SpecialOffersAndDetails" runat="server" />
                                            </label>
                                            <label>
                                                <asp:CheckBox ID="TermsAndConditions" class="AcceptAgreement" runat="server" />
                                                <sc:text id="TermsAndConditionsLabel" field="TermsAndConditions" runat="server" />
                                                <asp:CustomValidator ID="TermsAndConditionsValidator" CssClass="accent58-f" runat="server" ClientValidationFunction="TermsAndConditionsValidator_Validate" ValidationGroup="CustomerDetailsValidation">
                                                    <sc:text id="RequiredTermsCondtionsMsg" runat="server" />
                                                </asp:CustomValidator>
                                            </label>
                                        </div>
                                        <div class="col-sm-12">
                                            <asp:Label class="accent58-f" ID="ValidateTermsAndConditions" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row margin-row">
                                    <div class="col-sm-12">
                                        <asp:Button CssClass="btn btn-default add-break pull-right ContinueToSecurePaymentButton" type="submit" runat="server" ID="ContinueToSecurePaymentButton" Text="" OnClick="ContinueToSecurePaymentButton_Click" ClientIDMode="Static" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        //jQuery(".sec-payment").click(function () {
        //    jQuery(".hide-div").show();
        //    jQuery(".vau-personal-card").hide();
        //});

        jQuery("#validate").click(function () {
            jQuery(".email-validate").slideToggle();
            //e.preventDefault();
        });
    });
    function TermsAndConditionsValidator_Validate(sender, e) {
        e.IsValid = jQuery(".AcceptAgreement input:checkbox").is(':checked');
    }

    function paymentFunction() {
        jQuery(".hide-div").show();
        //document.getElementById('PersonalDetails').scrollIntoView(true);
        jQuery('html, body').animate({ scrollTop: jQuery(".hide-div").offset().top - 500 }, 'slow');
        jQuery(".vau-personal-card").hide();
        jQuery("#validate").show();
        jQuery(".email-validate").hide();
    };

    function emptyBasketFocus() {
        jQuery('html, body').animate({ scrollTop: jQuery(".content").offset().top }, 'slow');
    };
</script>
