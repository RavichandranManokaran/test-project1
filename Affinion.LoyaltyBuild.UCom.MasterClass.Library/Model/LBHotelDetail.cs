﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    public class LBProductDetail
    {
        public string Name { get; set; }

        public string RoomType { get; set; }

        public string FoodType { get; set; }

        public string PackageType { get; set; }

        public string Description { get; set; }

        public string ParentSKU { get; set; }

        public string RelatedSku { get; set; }

        public string RelatedAddOnSku { get; set; }

        public bool IsUnlimited { get; set; }

        public bool IsSellableStaandalone { get; set; }

        public int Ranking { get; set; }

        public int MaxNumberOfAdults { get; set; }

        public int MaxNumberOfChildren { get; set; }

        public int MaxNumberOfPeople { get; set; }

        public int MaxNumberOfInfrant { get; set; }

        public string SKU { get; set; }

        public int UcommerceId { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsActive { get; set; }
    
    }

    public class LBHotelDetail
    {
        public string SKU { get; set; }

        public string SiteCoreItemId { get; set; }

        public string HotelPath { get; set; }

        public string HotelName { get; set; }

        public string HotelType { get; set; }

        public string Location { get; set; }

        public string HotelRating { get; set; }

        public string DisplayName { get; set; }

        public int UComId { get; set; }

        public string Country { get; set; }
        public string Region { get; set; }
        public string Client { get; set; }
        public string HotelFacility { get; set; }
        public string Theme { get; set; }
        public string Groups { get; set; }
        public string SuperGroups { get; set; }
    }

    public class CountryDetail
    {
        public string CountryName { get; set; }

        public string CultureCode { get; set; }
    }

    public class LBAssignSupplier
    {
        public LBAssignSupplier()
        {
            this.AddedHotel = new List<int>();
            this.RemoveHotel = new List<int>();
        }

        public int CatalogId { get; set; }

        public string ClientName { get; set; }

        public List<int> AddedHotel { get; set; }

        public List<int> RemoveHotel { get; set; }
    }

    public class LBProductInfo
    {
        public string SKU { get; set; }

        public string VSKU { get; set; }

        public int ProductId { get; set; }
    }
}
