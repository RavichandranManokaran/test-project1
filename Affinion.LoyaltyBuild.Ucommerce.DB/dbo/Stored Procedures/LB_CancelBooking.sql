﻿
create PROCEDURE [dbo].[LB_CancelBooking] @Note         NVARCHAR(max), 
                                          @CancelReason NVARCHAR(max), 
                                          @OrderLineId  INT, 
                                          @CreatedBy    VARCHAR(50) 
AS 
  BEGIN 
      DECLARE @cancelstatus INT = 7 
      DECLARE @bookingid UNIQUEIDENTIFIER 

      SELECT @bookingid = bookingid 
      FROM   lb_bookinginfo 
      WHERE  orderlineid = @OrderLineId 

      -- SET NOCOUNT ON added to prevent extra result sets from 
      -- interfering with SELECT statements. 
      SET nocount ON; 

      --Check If already cancelled 
      IF EXISTS (SELECT 1 
                 FROM   lb_bookinginfo 
                 WHERE  statusid = @cancelstatus 
                        AND orderlineid = @OrderlineId) 
        THROW 50000, 'ALREADY CANCELLED', 1 

      -- Insert statements for procedure here    
      INSERT INTO lb_reservationnotes 
                  (notetypeid, 
                   bookingid, 
                   orderlineid, 
                   note, 
                   createdby, 
                   createddate) 
      VALUES      ( 1, 
                    @bookingid, 
                    @OrderLineId, 
                    @Note, 
                    @CreatedBy, 
                    Getdate() ); 

      INSERT INTO lb_reservationnotes 
                  (notetypeid, 
                   bookingid, 
                   orderlineid, 
                   note, 
                   createdby, 
                   createddate) 
      VALUES      ( 2, 
                    @bookingid, 
                    @OrderLineId, 
                    @CancelReason, 
                    @CreatedBy, 
                    Getdate() ); 

      --UPDATE lb_bookinginfo 
      --SET    statusid = @cancelstatus 
      --WHERE  orderlineid = @OrderlineId 
  END 