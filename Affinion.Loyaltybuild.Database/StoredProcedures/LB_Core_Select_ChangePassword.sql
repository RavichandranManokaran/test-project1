﻿
CREATE PROCEDURE [dbo].[LB_SelectChangePasswordHistory]
       @UniqueId    varchar(200)       
AS
BEGIN 
     SET NOCOUNT ON 

     SELECT [UserName]           
     FROM   [dbo].[ChangePasswordHistory]  
     WHERE  
            UniqueId = @UniqueId

END 

GO