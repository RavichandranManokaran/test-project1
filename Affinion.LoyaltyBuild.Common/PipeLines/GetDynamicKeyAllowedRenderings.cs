﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           GetDynamicKeyAllowedRenderings.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Override allowed rendering Pipeline
/// </summary>
namespace Affinion.LoyaltyBuild.Common.PipeLines
{
    #region Using Directives
    using Affinion.LoyaltyBuild.Common.Exceptions;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Sitecore;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using Sitecore.Pipelines.GetPlaceholderRenderings;
    using System;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;
    #endregion

    /// <summary>
    /// Gets the allowed rendering for the dynamic placeholders
    /// </summary>
    public class GetDynamicKeyAllowedRenderings : GetAllowedRenderings
    {
        #region Public Methods
        /// <summary>
        /// Overrides the process methods to get placeholder renderings
        /// </summary>
        /// <param name="args">The arguments</param>
        public new void Process(GetPlaceholderRenderingsArgs args)
        {
            try
            {
                string placeholderKey = args.PlaceholderKey;
                var regex = new Regex("_Dynamic_");
                var match = regex.Match(placeholderKey);

                if (match.Success && match.Groups.Count <= 0)
                {
                    return;
                }

                placeholderKey = match.Groups[0].Value;

                List<Item> renderings = this.GetAllowedRendering(args, placeholderKey);
                if (renderings == null)
                {
                    return;
                }

                if (args.PlaceholderRenderings == null)
                {
                    args.PlaceholderRenderings = new List<Item>();
                }

                args.PlaceholderRenderings.AddRange(renderings);
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, new object());
                throw;
            }
        }
        
        #endregion

        #region private methods

        /// <summary>
        /// Get allowed renderings
        /// </summary>
        /// <param name="args">The arguments</param>
        /// <param name="placeholderKey">The placeholder key</param>
        /// <returns></returns>
        private List<Item> GetAllowedRendering(GetPlaceholderRenderingsArgs args, string placeholderKey)
        {
            Item placeholderItem = null;

            if (ID.IsNullOrEmpty(args.DeviceId))
            {
                placeholderItem = Client.Page.GetPlaceholderItem(placeholderKey, args.ContentDatabase,
                    args.LayoutDefinition);
            }
            else
            {
                using (new DeviceSwitcher(args.DeviceId, args.ContentDatabase))
                {
                    placeholderItem = Client.Page.GetPlaceholderItem(placeholderKey, args.ContentDatabase,
                    args.LayoutDefinition);
                }
            }

            List<Item> renderings = null;
            if (placeholderItem != null)
            {
                bool allowedControlsSpecified;
                args.HasPlaceholderSettings = true;
                renderings = this.GetRenderings(placeholderItem, out allowedControlsSpecified);

                if (allowedControlsSpecified)
                {
                    args.CustomData["allowedControlsSpecified"] = true;
                    args.Options.ShowTree = false; // Remove this line if using Sitecore 6.5 (see text)
                }
            }

            return renderings;
        }

        #endregion

    }
}
