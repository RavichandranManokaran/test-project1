﻿IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LB_SupplierPortal_ArrivalReportData]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[LB_SupplierPortal_ArrivalReportData]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[LB_SupplierPortal_ArrivalReportData]	
	@OrderLineId NVARCHAR(50),
	@ClientName       NVARCHAR(512),
	@DateFrom         DATE,
	@Offername        INT,
	@DateTo           DATE,
	@Provider         int,
	@ReportandOrderby NVARCHAR(50),
	@bookingstatus    Int
AS
BEGIN
SET nocount ON

     if isnull(@ReportandOrderby,'') =''
	begin
	select @ReportandOrderby = (isnull(@ReportandOrderby,'Arrival Date'))
    end

      if isnull(@ReportandOrderby,'') =''
	begin
	select @ReportandOrderby = (isnull(@ReportandOrderby,'Arrival Date'))
    end
	select * from(
      (SELECT distinct Roomreservation.OrderReference [Booking reference],
			 Campaignitem.Name [Offername],
			 Purchaseorder.CurrencyId [CurrencyId],
			 (select
				case when OrderProperty.Value='Deposit' THEN OrderLine.Price-OrderLineSubrates.Amount
				Else 0
				End) [Collectamount],
			 Orderaddress.Firstname [Firstname],
			 Orderaddress.Lastname [Lastname],
			 Orderaddress.Line1 [Address line1],
			 Orderaddress.Line2 [Address line2],
			 Orderaddress.City [City],
			 Orderaddress.[State] [Region],
			 Country.Name [Country],
			 Orderaddress.Mobilephonenumber [Mobilephonenumber],
			 Orderaddress.Phonenumber [Phonenumber],
             RoomReservation.CheckInDate [Arrival Date],
             RoomReservation.CheckOutDate [Departure Date],
             cast(Roomreservation.NoOfAdult as smallint) [Number of adults],
             cast(Roomreservation.NoOfChild as smallint) [Number of children],
			 (select STUFF((SELECT ', ' + cast(RoomReservationAgeInfo.Age as varchar(10)) 
              FROM LB_RoomReservationAgeInfo RoomReservationAgeInfo
              WHERE RoomReservationAgeInfo.OrderLineId=Orderline.OrderLineId 
              FOR XML PATH('')), 1, 2, '')) [Child Age],
             Orderline.CreatedOn [Reservationdate],
			 Roomreservation.BookingThrough [Bookingthrough],
			 OrderStatus.Name [Bookingstatus],
			 OrderLineStatus.createdDate [Bookingstatusdate],
		     Product.Name [Accomodation],
			 '' [Acknowledgement],
			 CASE WHEN NewOrderReference IS NOT NULL THEN '#819FF7' --Transferred Out
			 WHEN (SELECT COUNT(1) FROM LB_RoomReservation where neworderreference = Roomreservation.orderreference) > 1 THEN '#F781BE' --Transferred In
			 ELSE '#f3f3f3' END [Rowcolor],  --Not transferred [Rowcolor]
			 Category.Name [Hotelname],
			 (select STUFF((SELECT ', ' + RoomReservations.SpecialRequest
              FROM LB_RoomReservation RoomReservations
              WHERE RoomReservations.OrderLineId=Orderline.OrderLineId 
              FOR XML PATH('')), 1, 2, '')) [Note],
			   olcampain.Campaignitemid
      FROM   Ucommerce_orderline Orderline
		     INNER JOIN Ucommerce_product Product ON Orderline.Sku = Product.Sku
			 INNER JOIN Ucommerce_categoryproductrelation Categoryproductrelation ON Product.Productid = Categoryproductrelation.Productid
			 INNER JOIN Ucommerce_category Category ON Categoryproductrelation.Categoryid = Category.Categoryid			 
			 INNER JOIN Ucommerce_purchaseorder Purchaseorder ON Orderline.Orderid = Purchaseorder.Orderid
			 INNER JOIN LB_RoomReservation Roomreservation ON Orderline.Orderlineid = Roomreservation.Orderlineid
			 LEFT JOIN LB_orderaddress Orderaddress ON Purchaseorder.Orderid = Orderaddress.Orderid
			 LEFT JOIN Ucommerce_country Country ON Orderaddress.Countryid = Country.Countryid
			 LEFT JOIN uCommerce_OrderLineCampaignRelation olcampain ON orderline.OrderLineId=olcampain.OrderLineId  
			 LEFT JOIN ucommerce_campaignitem campaignitem ON olcampain.CampaignItemId=campaignitem.CampaignItemId
			 LEFT JOIN uCommerce_OrderLineStatus OrderLineStatus ON Roomreservation.orderlineId = OrderLineStatus.OrderlineId and OrderLineStatus.OrderLineStatusId = Roomreservation.OrderLineStatusID
			 LEFT JOIN LB_OrderStatus OrderStatus ON OrderLineStatus.StatusId=OrderStatus.OrderStatusId
			 INNER JOIN uCommerce_OrderProperty orderproperty ON orderproperty.OrderLineId=Orderline.OrderLineId
			 INNER JOIN LB_OrderLineSubrates OrderLineSubrates on OrderLineSubrates.OrderLineID=Orderline.OrderLineId
      WHERE  (@OrderLineId IS NULL	OR @OrderLineId = RoomReservation.orderreference)
	        AND (RoomReservation.orderreference IS NOT NULL)
			 AND (@ClientName IS NULL  OR @ClientName = Orderaddress.Firstname OR @ClientName = Orderaddress.Lastname)
			 AND @Provider = Category.Categoryid
		     AND ((@ReportandOrderby='Arrival Date' AND (RoomReservation.CheckInDate BETWEEN @DateFrom AND @DateTo)) OR (@ReportandOrderby='Reservation Date' AND (OrderLine.CreatedOn BETWEEN @DateFrom AND @DateTo)))
			 and (@Offername = 0 OR @Offername = Campaignitem.Campaignitemid)
             ANd((@bookingstatus =0 or @bookingstatus =OrderLineStatus.StatusId))
			 AND OrderLineSubrates.SubrateItemCode='LBCOM' AND  OrderProperty.[Key]='PaymentMethod'
			 )
			 )a
			 order by 
			 CASE 
				WHEN @ReportandOrderby='Arrival Date' THEN [Arrival Date]
				ELSE [Reservationdate]
			 END asc;


  END
