﻿--exec GetAllChildCategory '1078'
CREATE procedure GetAllChildCategory
@categoryid int
As
select distinct uc.categoryid as CategoryId from uCommerce_category uc
inner join  uCommerce_CategoryProductRelation cpr on  uc.CategoryId = cpr.CategoryId
where uc.categoryid = @categoryid or uc.ParentCategoryId = @categoryid
