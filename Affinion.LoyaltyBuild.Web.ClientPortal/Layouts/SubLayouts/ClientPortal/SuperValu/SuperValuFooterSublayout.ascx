﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SuperValuFooterSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.SuperValuFooterSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<!-- Site footer -->
<div class="row footer-bottom margin-common">
    <footer class="footer">
        <div class="container-outer-inner width-full footer-bottom-bar">
            <div class="container">
                <sc:Placeholder ID="footerlinkblock" Key="footerlinkblock" runat="server" />
                <div class="copyright-text">
                    <asp:Literal ID="PlaceholderText" Text="Set the datasource: Required Fields: Copyright" runat="server" />
                    <p><sc:Text Field="Copyright" ID="Copyright" runat="server" /></p>
                </div>

            </div>
        </div>
    </footer>
</div>
