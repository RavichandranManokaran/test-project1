﻿namespace Affinion.LoyaltyBuild.Common.Workflows
{
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Executes when the item saves on draft state
    /// </summary>
    public class DraftValidationAction
    {
        /// <summary>
        /// Set item's icon field to unaproved icon
        /// </summary>
        /// <param name="args">WorkflowPipelineArgs object</param>
        public void Process(Sitecore.Workflows.Simple.WorkflowPipelineArgs args)
        {
            Item item = args.DataItem;
            using (new Sitecore.Security.Accounts.UserSwitcher(ItemHelper.ContentEditingUser))
            {
                item.Editing.BeginEdit();
                item.Fields["__icon"].Value = ItemHelper.UnapprovedIcon["__icon"];
                item.Editing.EndEdit();
            }
        }
    }
}
