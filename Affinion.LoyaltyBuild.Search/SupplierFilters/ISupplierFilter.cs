﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           ISupplierFilter.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Search (VS project name)
/// Description:           Interface for include suppliers in client search results page
/// </summary>
namespace Affinion.LoyaltyBuild.Search.SupplierFilters
{

    public interface ISupplierFilter
    {
        /// <summary>
        /// Whether to ignore supplier
        /// </summary>
        /// <param name="filterData"></param>
        /// <returns></returns>
        bool IncludeSupplier(SupplierFilterData filterData);
    }
}
