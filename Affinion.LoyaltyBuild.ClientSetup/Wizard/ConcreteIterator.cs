﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion Inernational. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           ConcreteIterator.cs
/// Sub-system/Module:     ClientSetup(VS project name)
/// Description:           ConcreteIterator Class
/// </summary>


#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 
#endregion

namespace Affinion.LoyaltyBuild.ClientSetup.Wizard
{
    /// <summary>
    /// Class for Iterate functionality
    /// </summary>
    /// <typeparam name="T"></typeparam>
    sealed class ConcreteIterator<T> : IWizardIterator<T>
    {
        #region Class variables

        // private generic IWizardAggregate attribute
        private IWizardAggregate<T> aggregate;

        //the actual collection you are traversing
        private List<T> collection = new List<T>();

        //keeps track of the current position
        private int pointer = 0;   

        #endregion


        #region Public method implmentation

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="item"></param>
        public ConcreteIterator(IWizardAggregate<T> item)
        {
            aggregate = item;
        }

        /// <summary>
        /// get the first item
        /// </summary>
        /// <returns></returns>
        T IWizardIterator<T>.First()
        {
            //move pointer to the first element in the aggregate and return it
            pointer = 0;
            return collection[pointer];
        }

        /// <summary>
        /// iterate to next item
        /// </summary>
        /// <returns></returns>
        T IWizardIterator<T>.Next()
        {
            //move pointer to the next element in the aggregate and return it
            pointer++;
            return collection[pointer];
        }


        /// <summary>
        /// get the current item
        /// </summary>
        /// <returns></returns>
        T IWizardIterator<T>.CurrentItem()
        {
            //return the element that the pointer is pointing to
            return collection[pointer];
        }

        /// <summary>
        /// get the boolean value of completion
        /// </summary>
        /// <returns></returns>
        bool IWizardIterator<T>.IsDone()
        {
            //return true if pointer is pointing to the last element, else return false
            return pointer == collection.Count - 1;
        }

        /// <summary>
        /// Add item to colletion
        /// </summary>
        /// <param name="item"></param>
        void IWizardIterator<T>.AddItem(T item)
        {
            collection.Add(item);
        }
                
        #endregion
    }
}