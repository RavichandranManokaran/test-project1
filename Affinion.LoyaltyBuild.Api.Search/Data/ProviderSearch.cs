﻿
using Affinion.LoyaltyBuild.Model.Provider;
namespace Affinion.LoyaltyBuild.Api.Search.Data
{
    /// <summary>
    /// Provider class
    /// </summary>
    public class ProviderSearch : IBaseProvider
    {
        /// <summary>
        /// Provider Id
        /// </summary>
        public string ProviderId { get; set; }

        /// <summary>
        /// SitecoreItemId
        /// </summary>
        public string SitecoreItemId { get; set; }

        /// <summary>
        /// provider type
        /// </summary>
        public ProviderType ProviderType { get; set; }
    }
}
