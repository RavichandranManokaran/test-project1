﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

/*
Customer_Attribute Table FOREIGN KEYS
*/
ALTER TABLE [dbo].[uCommerce_CustomerAttribute]  WITH CHECK ADD  CONSTRAINT [FK_uCommerce_CustomerAttribute_uCommerce_Country] FOREIGN KEY([CountryId])
REFERENCES [dbo].[uCommerce_Country] ([CountryId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[uCommerce_CustomerAttribute] CHECK CONSTRAINT [FK_uCommerce_CustomerAttribute_uCommerce_Country]
GO

ALTER TABLE [dbo].[uCommerce_CustomerAttribute]  WITH CHECK ADD  CONSTRAINT [FK_uCommerce_CustomerAttribute_uCommerce_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[uCommerce_Customer] ([CustomerId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[uCommerce_CustomerAttribute] CHECK CONSTRAINT [FK_uCommerce_CustomerAttribute_uCommerce_Customer]
GO

ALTER TABLE [dbo].[uCommerce_CustomerAttribute]  WITH CHECK ADD  CONSTRAINT [FK_uCommerce_CustomerAttribute_uCommerce_Location] FOREIGN KEY([LocationId])
REFERENCES [dbo].[uCommerce_LocationTarget] ([LocationTargetId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[uCommerce_CustomerAttribute] CHECK CONSTRAINT [FK_uCommerce_CustomerAttribute_uCommerce_Location]
GO
