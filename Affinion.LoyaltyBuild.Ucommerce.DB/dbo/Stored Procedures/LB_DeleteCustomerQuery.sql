﻿CREATE PROCEDURE [dbo].[LB_DeleteCustomerQuery]
(
	@QueryId int
) 
AS 
BEGIN
	DELETE FROM [dbo].[LB_CustomerQuery] 
	WHERE QueryId = @QueryId	
END
