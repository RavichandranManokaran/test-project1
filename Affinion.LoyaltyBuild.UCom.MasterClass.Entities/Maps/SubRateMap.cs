﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2.Maps;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Maps
{
    public class SubRateMap : BaseClassMap<SubRate>
    {
        // Methods
        public SubRateMap()
        {
            base.Table("LB_Subrate");
            //base.Map(x => x.SubRateId).ReadOnly();
            //base.Map(x => x.SubrateItemCode).Not.Nullable();
            //base.Map(x => x.ItemID).Nullable();
            //            Id(x => x.SubrateItemCode).Column("SubrateItemCode");
            CompositeId().KeyProperty(x => x.SubrateItemCode)
            .KeyProperty(x => x.ItemID).KeyProperty(x => x.ClientID).KeyProperty(x => x.Currency);

            base.Map(x => x.VAT).Not.Nullable();
            //base.Map(x => x.ClientID).Not.Nullable();
            base.Map(x => x.IsPercentage).Not.Nullable();
            base.Map(x => x.CreatedOn).Not.Nullable();
            base.Map(x => x.UpdatedOn).Not.Nullable();
            base.Map(x => x.CreatedBy).Not.Nullable();
            base.Map(x => x.Price).Not.Nullable();
        }
    }
}

