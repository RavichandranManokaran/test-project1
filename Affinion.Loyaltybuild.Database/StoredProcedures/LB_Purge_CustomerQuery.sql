﻿IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LB_Purge_CustomerQuery]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[LB_Purge_CustomerQuery]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LB_Purge_CustomerQuery]
AS
BEGIN
      --Delete record from child table
DELETE FROM LBuCommerce_CustomerQuery_Attachments 
       WHERE QueryId in 
       (
       SELECT QueryId FROM LB_CustomerQuery 
       WHERE CONVERT(VARCHAR(11),CreatedDate,111) <  CONVERT(VARCHAR(11), DATEADD(mm,-12,getdate()),111)
       )
       --Delete record from  parent table
DELETE FROM LB_CustomerQuery WHERE  CONVERT(VARCHAR(11),CreatedDate,111) <  CONVERT(VARCHAR(11), DATEADD(mm,-12,getdate()),111)
END
GO