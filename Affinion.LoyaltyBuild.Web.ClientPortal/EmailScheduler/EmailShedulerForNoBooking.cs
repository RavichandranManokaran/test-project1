﻿using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Order;
using Sitecore.Data;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.EmailScheduler
{
    public class EmailShedulerForNoBooking
    {
        // get the parent folder of the clients from sitecore 
        public string clientitemPath = "/sitecore/content/admin-portal/client-setup";

        Database currentDatabase = Sitecore.Configuration.Factory.GetDatabase("web");

        /// scheduler job
        /// <summary>
        /// <param name="items"></param>
        /// <param name="command"></param>
        /// <param name="schedule"></param>
        /// </summary>
        public void Run(Sitecore.Data.Items.Item[] items, Sitecore.Tasks.CommandItem command, Sitecore.Tasks.ScheduleItem schedule)
        {
            double schedulerMinutes = schedule.Schedule.Interval.TotalMinutes;
            Executejob(schedulerMinutes);
        }

        /// <summary>
        /// Executejob for get the client,duration and profiletype for sending mail
        /// </summary>
        public void Executejob(double schedulerMinutes)
        {
            try
            {
                //get the clients
                List<Item> clients = currentDatabase.GetItem(clientitemPath).Children.Where(i => i.TemplateName == "ClientDetails").ToList();
                foreach (Item client in clients)
                {
                    string profileType = SitecoreFieldsHelper.GetDropLinkFieldValue(client, "ProfileType", "ProfileName");
                    IEnumerable<Item> schedulerdays = client.Axes.GetDescendants().Where(a => a.TemplateName.Equals("SchedulerDay"));

                    DateTime dtDateTime = DateTime.Now;
                    string currentDay = dtDateTime.ToString("dddd").ToUpper();

                    foreach (Item day in schedulerdays)
                    {
                        string nameofDay = day.Fields["NameOfDay"].Value.ToUpper();

                        //to check the currentDay and nameofDay(this is coming from sitecore) equal or not
                        if (currentDay == nameofDay)
                        {
                            Diagnostics.Trace(DiagnosticsCategory.ExternalServices, "NoBookingSchedulerEmailIfCondition1: started -- " + DateTime.Now);
                            foreach (Item interVal in day.GetChildren().ToList())
                            {
                                string timeOfDay = dtDateTime.ToString("hh:mm tt");

                                string fromTime = interVal.Fields["FromTime"].Value;
                                string toTime = interVal.Fields["ToTime"].Value;
                                string duraTion = interVal.Fields["Duration"].Value;

                                if (!string.IsNullOrEmpty(fromTime) || !string.IsNullOrEmpty(toTime) || !string.IsNullOrEmpty(duraTion))
                                {
                                    DateTime obCurrentTime = new DateTime();
                                    DateTime obDateTimeFrom = new DateTime();
                                    DateTime obDateTimeTo = new DateTime();

                                    obCurrentTime = DateTime.Parse(timeOfDay);
                                    obDateTimeFrom = DateTime.Parse(fromTime);
                                    obDateTimeTo = DateTime.Parse(toTime);
                                    TimeSpan interval = new TimeSpan();
                                    interval = TimeSpan.Parse(duraTion.Trim());
                                    double totalMinitues = interval.TotalMinutes;

                                    //to check the current time is grater than or equal to and lesser than or equal time from and time to
                                    if (obCurrentTime >= obDateTimeFrom && obCurrentTime <= obDateTimeTo)
                                    {

                                        var timeDiff = (obCurrentTime - obDateTimeFrom).TotalMinutes;
                                        int totalTimeRound = Convert.ToInt32(timeDiff / totalMinitues);

                                        var lastTimeTrigger = timeDiff % totalMinitues;
                                        //to check the job , already run with this interval or not                                   
                                        if (lastTimeTrigger == 0 || lastTimeTrigger < schedulerMinutes)
                                        {
                                            string clientName = Convert.ToString(client.ID);

                                            var currMin = obCurrentTime.Minute;
                                            //to check the scheduler mints time interval for each job
                                            var minVariation = currMin % schedulerMinutes;
                                            var fromTriggerTime = obDateTimeFrom.AddMinutes(((totalTimeRound - 1) * totalMinitues) + minVariation);

                                            var toTriggerTime = obCurrentTime;

                                            OrderService obOrderService = new OrderService(null, null);
                                            if (string.IsNullOrEmpty(obOrderService.CheckingNoBookingCount(clientName, fromTriggerTime, toTriggerTime)))
                                            {
                                                Diagnostics.Trace(DiagnosticsCategory.ExternalServices, string.Format("NoBookingSchedulerEmailIfCondition7: started {0}", DateTime.Now));
                                                EmailGenerate(client, duraTion, profileType);
                                            }
                                        }
                                    }
                                }

                                else
                                {
                                    Diagnostics.Trace(DiagnosticsCategory.ExternalServices, string.Format("NoBookingSchedulerEmail is not valid datetime {0}", DateTime.Now));
                                }
                            }
                        }

                    }

                }
            }
            catch (Exception exception)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("NoBookingSchedulerEmail: Exception-{0}", exception.ToString()));
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
            }
            Diagnostics.Trace(DiagnosticsCategory.ExternalServices, string.Format("NoBookingSchedulerEmail:ExecuteJob() method ended  ", DateTime.Now));
        }

        public void EmailGenerate(Item client, string duration, string profiletype)
        {
            IEnumerable<Item> currentItem = client.Axes.GetDescendants().Where(a => a.TemplateName.Equals("NoBookingScheduler"));
            foreach (Item item in currentItem)
            {
                string fromAddress = item.Fields["FromAddress"].Value;
                Diagnostics.Trace(DiagnosticsCategory.ExternalServices, string.Format("NoBookingSchedulerEmailfromAddress: started {0} {1}", DateTime.Now, fromAddress));
                string toAddress = item.Fields["ToAddress"].Value;
                Diagnostics.Trace(DiagnosticsCategory.ExternalServices, string.Format("NoBookingSchedulerEmailtoAddress: started {0} {1}", DateTime.Now, toAddress));
                IList<string> toAddressSplit = toAddress.Split(';');
                string msgSubject = item.Fields["Subject"].Value;

                string[] durationarr = duration.Split(':');
                string hourValue = durationarr[0];
                string minutesValue = durationarr[1];
                string hourandminutesValue = (hourValue != "00" ? hourValue + " hr" : "") + (minutesValue != "00" ? minutesValue + " mins" : "");

                msgSubject = msgSubject.Replace("{Client}", !string.IsNullOrWhiteSpace(client.Fields["Name"].Value) ? client.Fields["Name"].Value : string.Empty).
                    Replace("{Hour}", hourandminutesValue).Replace("{Type}", profiletype);

                Affinion.LoyaltyBuild.Communications.IEmailService emailService = new Affinion.LoyaltyBuild.Communications.Services.EmailService();
                // mail send method               
                emailService.Send(fromAddress, toAddressSplit, msgSubject, true);
            }
        }
    }
}