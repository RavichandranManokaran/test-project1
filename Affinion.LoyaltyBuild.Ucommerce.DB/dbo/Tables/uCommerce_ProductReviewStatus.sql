﻿CREATE TABLE [dbo].[uCommerce_ProductReviewStatus] (
    [ProductReviewStatusId] INT             NOT NULL,
    [Name]                  NVARCHAR (1024) NOT NULL,
    [Deleted]               BIT             NOT NULL,
    CONSTRAINT [PK_uCommerce_ProductReviewStatus] PRIMARY KEY CLUSTERED ([ProductReviewStatusId] ASC)
);

