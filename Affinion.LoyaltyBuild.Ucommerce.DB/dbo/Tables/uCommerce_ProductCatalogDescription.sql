﻿CREATE TABLE [dbo].[uCommerce_ProductCatalogDescription] (
    [ProductCatalogDescriptionId] INT            IDENTITY (1, 1) NOT NULL,
    [ProductCatalogId]            INT            NOT NULL,
    [CultureCode]                 NVARCHAR (60)  NOT NULL,
    [DisplayName]                 NVARCHAR (512) NOT NULL,
    CONSTRAINT [uCommerce_PK_ProductCatalogDescription] PRIMARY KEY CLUSTERED ([ProductCatalogDescriptionId] ASC),
    CONSTRAINT [uCommerce_FK_ProductCatalogDescription_ProductCatalog] FOREIGN KEY ([ProductCatalogId]) REFERENCES [dbo].[uCommerce_ProductCatalog] ([ProductCatalogId])
);

