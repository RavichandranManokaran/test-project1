﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SuperValuLocationFilterSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.SuperValuLocationFilterSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="detail-info">
    <div class="detail-info-header">
        <div class="col-xs-2">
            <h5 class="details-header">
                <span class="glyphicon glyphicon-map-marker"></span>
            </h5>
        </div>
        <div class="col-xs-10">
            <h5 class="details-header details-location">Bundoran 
								, Donegal 
            </h5>
        </div>

        <div class="col-xs-2">
            <h5 class="details-header details-date">
                <span class="glyphicon glyphicon-calendar"></span>
            </h5>
        </div>
        <div class="col-xs-10">
            <h5 class="details-header">03/09/2015 - 04/09/2015
            </h5>
        </div>
    </div>
    <div class="col-xs-6">
        <h6 class="details-label">Nights
        </h6>
    </div>
    <div class="col-xs-6">
        <h6 class="details-value">1
        </h6>
    </div>
    <div class="col-xs-6">
        <h6 class="details-label">Number of Rooms 
        </h6>
    </div>
    <div class="col-xs-6">
        <h6 class="details-value">3
        </h6>

    </div>



    <div class="col-xs-12">
        <h3>Select New Location</h3>
        <div role="group" aria-label="..." class="select-loc">
            <select class="form-control">
                <option>Select Location</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
            </select>
        </div>
    </div>
</div>
