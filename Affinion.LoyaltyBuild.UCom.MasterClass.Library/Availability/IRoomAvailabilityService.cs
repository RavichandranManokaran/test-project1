﻿using model = Affinion.LoyaltyBuild.Model.Pricing;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Availability
{
    /// <summary>
    /// Interface to define the roomavilabity related actions
    /// </summary>

    public interface IRoomAvailabilityService
    {
        /// <summary>
        /// FindRoomAvailability 
        /// </summary>
        /// <param name="providerId">providerId</param>
        ///<param name="productSku">productSku</param>
        ///<param name="startDate">startDate</param>
        ///<param name="endDate">endDate</param>
        ///<param name="includeOfferDetail">includeOfferDetail</param>
        ///<param name="campaignItemId">campaignItemId</param>
        /// <returns><list>RoomAvailabilityDetail</list>Object of RoomAvailabilityDetail</returns>
        List<RoomAvailabilityDetail> FindRoomAvailability(string providerId, string productSku, DateTime startDate, DateTime endDate, bool includeOfferDetail = false, int campaignItemId = 0);


        /// <summary>
        /// FindOfferAvailability 
        /// </summary>
        /// <param name="providerId">providerId</param>
        ///<param name="productSku">productSku</param>
        ///<param name="startDate">startDate</param>
        ///<param name="endDate">endDate</param>
        ///<param name="campaignItemId">campaignItemId</param>
        /// <returns><list>OfferAvailability</list>Object of OfferAvailability</returns>
        List<OfferAvailability> FindOfferAvailability(string providerId, string productSku, DateTime startDate, DateTime endDate, int campaignItemId);

        /// <summary>
        /// FindRoomAndOfferAvailabiltyByProvider 
        /// </summary>
        /// <param name="providerId">providerId</param>
        ///<param name="campaignItemId">campaignItemId</param>
        ///<param name="startDate">startDate</param>
        ///<param name="endDate">endDate</param>
        /// <returns><list>ProviderAvialbilityData</list>Object of ProviderAvialbilityData</returns>
        List<ProviderAvialbilityData> FindRoomAndOfferAvailabiltyByProvider(string providerId, int campaignItemId, DateTime startDate, DateTime endDate);


        /// <summary>
        /// SaveProviderRoomAvailability 
        /// </summary>
        /// <param name="providerId">providerId</param>
        ///<param name="roomAvailability"><list>ChangeRoomAvailabilityByDate</list>roomAvailability</param>
        /// <returns><list>ChangeRoomAvailabilityByDate</list>Object of ChangeRoomAvailabilityByDate</returns>

        List<ChangeRoomAvailabilityByDate> SaveProviderRoomAvailability(string providerId, List<ChangeRoomAvailabilityByDate> roomAvailability);


        /// <summary>
        /// SaveProviderOfferAvailability 
        /// </summary>
        /// <param name="providerId">providerId</param>
        ///<param name="ChangeOfferRoomAvailabilityByDate"><list>ChangeOfferRoomAvailabilityByDate</list>ChangeOfferRoomAvailabilityByDate</param>
        /// <returns><list>ChangeOfferRoomAvailabilityByDate</list>Object of ChangeOfferRoomAvailabilityByDate</returns>

        List<ChangeOfferRoomAvailabilityByDate> SaveProviderOfferAvailability(string providerId, List<ChangeOfferRoomAvailabilityByDate> roomAvailability);


        /// <summary>
        /// ChangeRoomRestriction 
        /// </summary>
        /// <param name="providerId">providerId</param>
        /// <param name="campaignItemId">campaignItemId</param>
        ///  <param name="isRestricted">isRestricted</param>
        /// <returns>returns true/false for ChangeRoomRestriction</returns>
        bool ChangeRoomRestriction(string providerId, int campaignItemId, bool isRestricted);


        /// <summary>
        /// GetHotelRoomAndOfferAvailabilityByProvider 
        /// </summary>
        /// <param name="providerId">providerId</param>
        /// <param name="startDate">startDate</param>
        ///  <param name="endDate">endDate</param>
        /// <returns> <list>RoomAndOfferAvaialbityShortage</list>GetHotelRoomAndOfferAvailabilityByProvider</returns>
        List<RoomAndOfferAvaialbityShortage> GetHotelRoomAndOfferAvailabilityByProvider(string providerId, DateTime startDate, DateTime endDate);

        /// <summary>
        /// GetOfferBookingByDateRange 
        /// </summary>
        /// <param name="providerId">providerId</param>
        /// <param name="campaignItemId">campaignItemId</param>
        /// <returns> <list>GetOfferBookingByDateRange</list>GetOfferBookingByDateRange</returns>
        List<int> GetOfferBookingByDateRange(string providerId, int campaignItemId);

        /// <summary>
        /// GetProductDetailByProviderId 
        /// </summary>
        /// <param name="providerId">providerId</param>
        /// <returns> <list>GetProductDetailByProviderId</list>GetProductDetailByProviderId</returns>
        List<ProductDetail> GetProductDetailByProviderId(string providerId);



        /// <summary>
        /// 
        /// </summary>
        /// <param name="providerId"></param>
        /// <param name="startDateCounter"></param>
        /// <param name="endDateCounter"></param>
        /// <param name="productSku"></param>
        /// <returns></returns>
        List<RoomAvailabilityByDate> GetRoomAvailabilityByProvider(string providerId, int startDateCounter, int endDateCounter, string productSku);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="variantSKU"></param>
        /// <param name="SKU"></param>
        /// <returns></returns>
        List<decimal> GetAddOnPrice(string variantSKU, string SKU);

        /// <summary>
        /// Get price by daterange
        /// </summary>
        /// <param name="sku"></param>
        /// <param name="variantSku"></param>
        /// <param name="startDateCounter"></param>
        /// <param name="endDateCounter"></param>
        /// <returns></returns>
        List<model.ProductPrice> GetProductPriceByDateRange(string sku, string variantSku, DateTime startDate, DateTime endDate);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sku"></param>
        /// <param name="variantSku"></param>
        /// <param name="date"></param>
        /// <param name="isSubItem"></param>
        /// <returns></returns>
        model.ProductPrice GetProductPriceByDate(string sku, string variantSku, DateTime date, bool isSubItem = false);


        /// <summary>
        /// Update Related Package Availability
        /// </summary>
        /// <param name="variantSku"></param>
        /// <returns>updated or not</returns>
        bool UpdateRelatedPackageAvailability(string variantSku, string dateCounter);
    }
}
