﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientPortalAddToFavoriteSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal.ClientPortalAddToFavorite" %>

<script type="text/javascript">
    jQuery.fx.speeds._default = 1000;
    jQuery(document).on('click', '.btnCreateFavList', function () {
        jQuery("#dialog").dialog({
            title: "Enter New List Name",
            buttons: {
                Ok: function () {
                    jQuery(this).parent().appendTo("form");
                    if (jQuery('.txtNewFavList').val() != null) {
                        jQuery(".btnSavFavList").click();
                        jQuery(this).dialog('close');
                    }
                    else {
                        alert("List Name cannot be Empty!");
                    }
                }
            }
        });
        $('.txtNewFavList').val("");
        return false;
    });
</script>
<style>
    .Panel {
        overflow-y: scroll;
    }
    h2 a 
    {
      font-size:17px;
    }
    .loc_txt
    {
      font-size:13px;
    }
</style>
<div class="col-xs-12">
    <div class="col-xs-12 breadcrumb list_bg">
       <span class="fav_list"><sc:text id="SelectText" runat="server" field="mylists" /></span>        
        <span class="col-md-3">
            <asp:DropDownList ID="ddlFavList" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlFavList_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
        </span>
        <div class="col-md-6 col-xs-12 no-padding">
            <span class="col-md-3 new_gap">
                <input type="button" id="btnCreateFavList" runat="server" class="btnCreateFavList btn btn-primary btn-bg pull-left" />
            </span>
            <div id="dialog" style="display: none">
                <asp:TextBox ID="txtNewFavList" CssClass="txtNewFavList" runat="server"></asp:TextBox>
                <asp:Button ID="btnSavFavList" CssClass="btnSavFavList" runat="server" Text="Save List" Style="display: none" ValidationGroup="newList" OnClick="btnSavFavList_Click" />
            </div>
        </div>
    </div>
</div>
<asp:Repeater ID="rptHotelListing" runat="server">
    <ItemTemplate>
        <div class="">
            <div class="content_bg_inner_box summary-detail alert-info">
                <div class="col-md-3 row col-sm-12">
                    <div class="col-xs-12 col-md-12">

                        <div class="basket-img">
                            <img alt="" src='<%#Eval("ImgSrc") %>' class="img-responsive img-thumbnail">
                        </div>
                    </div>
                </div>
                <div class="col-md-9 col-sm-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <span class="accent33-f">
                                <h2><strong><u>
                                    <asp:HyperLink ID="linkHotelName" runat="server" Text='<%#Eval("Title")%>' NavigateUrl='<%#Eval("link")%>'></asp:HyperLink></u></strong>
                                     <asp:ImageButton ID="imgClosebtn" runat="server" CssClass="pull-right" ImageUrl="~/Resources/images/CloseImage.png" Title="Remove Hotel From List" OnClick="ImgClosebtn_Click" CommandArgument='<%#Eval("HotelId") %>' />
                                </h2>
                                <asp:Label ID="lblLocation" CssClass="loc_txt" runat="server" Text='<%#Eval(&quot;Locations&quot;) %>'></asp:Label><br />
                               
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </ItemTemplate>
</asp:Repeater>
