﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalMapLocatorSublayout.ascx.cs" Inherits="Affinion.Loyaltybuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalMapLocatorSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&sensor=true"></script>

<link rel="stylesheet" href="/Resources/styles/responsiveNav.css" type="text/css" />

<script type="text/javascript">
    jQuery(document).ready(function () {
        if (typeof (MapLocations) === "undefined" || MapLocations === undefined || MapLocations == null || MapLocations == "undefined" || MapLocations.Locations == undefined || MapLocations.Locations.length < 1) {
            <%= (Sitecore.Context.PageMode.IsPageEditorEditing) ? "" : "jQuery('#gsmap').show();" %>
        }
        else {
            var mark = "markers=";
            var markAppend = "";
            var location = "markers=";
            var locationAppend = MapLocations.Locations[0].Latitude + "," + MapLocations.Locations[0].Longitute;
            var url = window.location.pathname;
            var PageName = url.substring(url.lastIndexOf('/') + 1);

            jQuery.each(MapLocations.Locations, function (i, item) {
                markAppend += item.Latitude + "," + item.Longitute + "|";
            });

            mark += markAppend;
            location += locationAppend;
            jQuery('#staticmap').attr('src', jQuery('#staticmap').attr('src') + location);
        }        
    });
</script>

<div class="map-sm-location margin-common-inner-box" title="Map view" id="gsmap">
    <a href="#" data-toggle="modal" data-target="#dialog-map">
        <img id="staticmap" src="http://maps.google.com/maps/api/staticmap?zoom=2&size=175x175&" alt="" class="img-responsive img-thumbnail img-enlarge" />
    </a>
</div>

<!-- Modal -->
<div class="modal fade" id="dialog-map" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><span class="fa fa-map-marker"></span>Map</h4>
            </div>
            <div class="modal-body">
                <div id="map_canvas" class="large-gmap"></div>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    // Store reference to the google map object
    var map;
    var newUrl;

    function initializeMap(location) {
        if (typeof(MapLocations) === "undefined" || MapLocations === undefined || MapLocations == null || MapLocations == "undefined" || MapLocations.Locations == undefined || MapLocations.Locations.length < 1) {
            return;
        }

        // Define the coordinates as a Google Maps LatLng Object
        var mapOptions = {
            center: new google.maps.LatLng(MapLocationsAll.Locations[0].Latitude, MapLocationsAll.Locations[0].Longitute),
            zoom: 6,
        };

        map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

        jQuery.each(MapLocationsAll.Locations, function (i, item) {
            // Map initialization variables
            var marker;

            var currentUrl = window.location.href;
            updateQueryStringParameterToVar(currentUrl, 'PageId', '1');
            var updatedUrl = updateQueryStringParameter(newUrl, 'Location', item.Location);
            
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(item.Latitude, item.Longitute),
                url: updatedUrl,
                map: map
            });
            google.maps.event.addListener(marker, 'click', function () {
                window.location.href = marker.url;
            });
        });
    }

    google.maps.event.addDomListener(window, 'load', initializeMap);

    jQuery(document).ready(function () {

        jQuery('#dialog-map').on('shown.bs.modal', function () {

            if (navigator.geolocation) {
                // Call getCurrentPosition with success and failure callbacks                
                initializeMap();
            }
            else {
                alert("Sorry, your browser does not support geolocation services.");
            }
        });
    });    

    /// Function used to update query string parameters and return
    function updateQueryStringParameter(uri, key, value) {
        var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
        var separator = uri.indexOf('?') !== -1 ? "&" : "?";
        if (uri.match(re)) {
            return uri.replace(re, '$1' + key + "=" + value + '$2');
        }
        else {
            return uri + separator + key + "=" + value;
        }
    }

    /// Function used to update query string parameters to variable
    function updateQueryStringParameterToVar(uri, key, value) {
        var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
        var separator = uri.indexOf('?') !== -1 ? "&" : "?";
        if (uri.match(re)) {
            newUrl = uri.replace(re, '$1' + key + "=" + value + '$2');
        }
        else {
            newUrl = uri + separator + key + "=" + value;
        }
    }    

</script>
