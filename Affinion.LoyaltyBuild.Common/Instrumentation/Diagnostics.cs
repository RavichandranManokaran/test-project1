﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           Diagnostics.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Used to log diagnostic data
/// </summary>


#region Using Directives

using Affinion.LoyaltyBuild.Common.Utilities;
using System;

#endregion

namespace Affinion.LoyaltyBuild.Common.Instrumentation
{
    /// <summary>
    /// Provides functionality for logging the exceptions. 
    /// </summary>
    public static class Diagnostics
    {
        #region Private Variables

        /// <summary>
        /// Default logger instance
        /// </summary>
        private static ILogger logger = new Logger();

        #endregion

        #region Public Methods

        /// <summary>
        /// Set new logger which is replace default logger instance
        /// </summary>
        /// <param name="newLogger">New logger object</param>
        public static void SetLogger(ILogger newLogger)
        {
            logger = newLogger;
        }

        /// <summary>
        /// Write the message to logs.
        /// </summary>
        /// <param name="categoryName">Name of the category.</param>
        /// <param name="message">The message to trace.</param>       
        /// <param name="data">The data.</param>
        public static void Trace(DiagnosticsCategory categoryName, string message)
        {

            //log the message to log
            logger.Trace(message, categoryName);


        }

        /// <summary>
        /// Writes the exception to log.
        /// </summary>
        /// <param name="categoryName">Name of the category.</param>
        /// <param name="exception">The exception.</param>
        /// <param name="owner">Owner</param>
        public static void WriteException(DiagnosticsCategory categoryName, Exception exception, object owner)
        {

            if (exception != null)
            {

                //Log the most recently occurred exception
                logger.WriteException(categoryName, exception, owner);

            }
            else
            {
                logger.WriteException(categoryName, new ArgumentNullException("exception"), "Diagnostics.cs");
            }

        }

        #endregion

    }
}
