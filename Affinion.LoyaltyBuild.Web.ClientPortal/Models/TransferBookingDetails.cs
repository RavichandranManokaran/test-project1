﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Affinion.LoyaltyBuild.Api.Booking.Data.Basket;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Models
{
    public class TransferBookingDetails:ITransferRequest
    {
        public int OrderLineid  { get; set; }

        public string NoteToProvider { get; set; }

        public int NoOdAdult { get; set; }

        public int NoOfChildren { get; set; }

        public string ChildrenAgeInfo { get; set; }

        public string BasketId { get; set; }

        public string CreatedBy { get; set; }

        public bool IsFreeTransfer { get; set; }

        public string BookingReferenceNumber { get; set; }
    }
}