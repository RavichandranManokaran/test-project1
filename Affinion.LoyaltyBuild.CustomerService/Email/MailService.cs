﻿using System;
using System.Linq;
using Affinion.LoyaltyBuild.Communications;
using Affinion.LoyaltyBuild.Communications.Services;
using System.Data;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Order;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Helpers;
using Sitecore.Data.Items;
using System.Collections.Specialized;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Customer;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Affinion.LoyaltyBuild.UCom.Common.Constants;
using Affinion.LoyaltyBuild.Common;
using System.Text.RegularExpressions;
using UCommerce.EntitiesV2;
using Affinion.LoyaltyBuild.Common.Utilities;
using data = Affinion.LoyaltyBuild.Api.Booking;
using Affinion.LoyaltyBuild.Api.Booking.Service;
using System.Globalization;
using Affinion.LoyaltyBuild.Model.Provider;
using System.Collections.Generic;
using Newtonsoft.Json;
using Affinion.LoyaltyBuild.Model.Product;
using UCommerce.Infrastructure;
//<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
/// </summary>

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Email
{


    public class MailService : IMailService
    {

        /// <summary>
        /// Private properties
        /// </summary>
        private IEmailService _emailService;
        private IOrderService _orderService;
        private string _clientId;
        private string _connectionString = "uCommerce";

        /// <summary>
        /// Constructor
        /// </summary>
        public MailService(string clientGUID)
        {
            _clientId = clientGUID;
            _emailService = new EmailService();
            _orderService = ContainerFactory.Instance.GetInstance<IOrderService>();
        }

        /// <summary>
        /// Get sitecore Item
        /// </summary>
        public Item MailItemSource
        {
            get
            {
                if (string.IsNullOrEmpty(LanguageCode))
                    return Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(_clientId));
                else
                {
                    // Get language specific mail Content
                    var language = Sitecore.Data.Managers.LanguageManager.GetLanguage(LanguageCode);
                    if (language == null)
                    {
                        return Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(_clientId));
                    }
                    else
                    {
                        var itemLanguage = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(_clientId), language);
                        if (itemLanguage == null)
                        {
                            return Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(_clientId));
                        }

                        return itemLanguage;
                    }
                }
            }
        }

        /// <summary>
        /// Gets Sender Email id
        /// </summary>
        public string EmailSender
        {
            get { return MailItemSource.Fields["SenderEmail"].Value; }
        }

        /// <summary>
        /// Gets Sender Customer Cancellation Mail Subject
        /// </summary>
        public string CustomerCancellationSubject
        {
            get { return MailItemSource.Fields["CustomerCancellationSubject"].Value; }
        }

        /// <summary>
        /// Gets Sender Provider Cancellation Mail Subject
        /// </summary>
        public string ProviderCancellationSubject
        {
            get { return MailItemSource.Fields["ProviderCancellationSubject"].Value; }
        }

        /// <summary>
        /// Gets Sender Customer Confirmation Mail Subject
        /// </summary>
        public string CustomerConfirmationSubject
        {
            get { return MailItemSource.Fields["CustomerConfirmationSubject"].Value; }
        }

        /// <summary>
        /// Gets Sender ManageBooking Customer Confirmation Mail Subject
        /// </summary>
        public string ManageBookingCustomerConfirmationSubject
        {
            get { return MailItemSource.Fields["ManageBookingCustomerConfirmationSubject"].Value; }
        }

        /// <summary>
        /// Gets Sender Provider Confirmation Mail Subject
        /// </summary>
        public string ProviderConfirmationSubject
        {
            get { return MailItemSource.Fields["ProviderConfirmationSubject"].Value; }
        }

        /// <summary>
        /// Gets Sender Provider Confirmation Mail Subject
        /// </summary>
        public string ProviderProvisionalConfirmationSubject
        {
            get { return MailItemSource.Fields["ProviderProvisionalConfirmationSubject"].Value; }
        }

        /// <summary>
        /// Gets Sender Client Provisional Confirmation Mail Subject
        /// </summary>
        public string ClientProvisionalConfirmationSubject
        {
            get { return MailItemSource.Fields["ClientProvisionalConfirmationSubject"].Value; }
        }

        /// <summary>
        /// Gets Sender Customer Cancellation Mail Body
        /// </summary>
        public string CustomerCancellationBody
        {
            get { return MailItemSource.Fields["CustomerCancellationBody"].Value; }
        }

        /// <summary>
        /// Gets Sender Provider Cancellation Mail Body
        /// </summary>
        public string ProviderCancellationBody
        {
            get { return MailItemSource.Fields["ProviderCancellationBody"].Value; }
        }

        /// <summary>
        /// Gets Sender Provider Confirmation Mail Body
        /// </summary>
        public string ProviderConfirmationBody
        {
            get { return MailItemSource.Fields["ProviderConfirmationBody"].Value; }
        }

        /// <summary>
        /// Gets Sender Customer Confirmation Mail Body
        /// </summary>
        public string CustomerConfirmationBody
        {
            get { return MailItemSource.Fields["CustomerConfirmationBody"].Value; }
        }

        /// <summary>
        /// Gets Sender Manage Booking Customer Confirmation Mail Body
        /// </summary>
        public string ManageBookingCustomerConfirmationBody
        {
            get { return MailItemSource.Fields["ManageBookingCustomerConfirmationBody"].Value; }
        }

        /// <summary>
        /// Gets Sender Provider Provisional Confirmation Mail Body
        /// </summary>
        public string ProviderProvisionalConfirmationBody
        {
            get { return MailItemSource.Fields["ProviderProvisionalConfirmationBody"].Value; }
        }

        /// <summary>
        /// Gets Sender Client Provisional Confirmation Mail Body
        /// </summary>
        public string ClientProvisionalConfirmationBody
        {
            get { return MailItemSource.Fields["ClientProvisionalConfirmationBody"].Value; }
        }


        /// <summary>
        /// gets partial payment confirmationRemaibnder subject
        /// </summary>
        public string PartialEmailConfirmationSubject
        {
            get { return MailItemSource.Fields["PartialEmailConfirmationSubject"].Value; }
        }
        /// <summary>
        /// Gets partial payment confirmationRemainder subject
        /// </summary>
        public string PartialEmailConfirmationBody
        {
            get { return MailItemSource.Fields["PartialEmailConfirmationBody"].Value; }
        }

        /// <summary>
        /// Gets Customer Confirmation Online Mail Subject
        /// </summary>
        public string CustomerConfirmationSubjectOnline
        {
            get { return MailItemSource.Fields["CustomerConfirmationSubjectOnline"].Value; }
        }

        /// <summary>
        /// Gets Customer Confirmation Online Mail Body
        /// </summary>
        public string CustomerConfirmationBodyOnline
        {
            get { return MailItemSource.Fields["CustomerConfirmationBodyOnline"].Value; }
        }

        /// <summary>
        /// Gets Provider Confirmation Online Mail Subject
        /// </summary>
        public string ProviderConfirmationSubjectOnline
        {
            get { return MailItemSource.Fields["ProviderConfirmationSubjectOnline"].Value; }
        }

        /// <summary>
        /// Gets Provider Confirmation Online Mail Body
        /// </summary>
        public string ProviderConfirmationBodyOnline
        {
            get { return MailItemSource.Fields["ProviderConfirmationBodyOnline"].Value; }
        }

        /// <summary>
        /// Gets Partial Customer Confirmation Online Mail Subject
        /// </summary>
        public string PartialCustomerConfirmationSubjectOnline
        {
            get { return MailItemSource.Fields["PartialCustomerConfirmationSubjectOnline"].Value; }
        }

        /// <summary>
        /// Gets Partial Customer Confirmation Online Mail Body
        /// </summary>
        public string PartialCustomerConfirmationBodyOnline
        {
            get { return MailItemSource.Fields["PartialCustomerConfirmationBodyOnline"].Value; }
        }

        /// <summary>
        /// Set And Get Language Code
        /// </summary>
        public string LanguageCode { get; set; }

        /// <summary>
        /// Generate email and send
        /// </summary>
        /// <param name="mailType"> Type of email </param>
        /// <param name="orderlineid"> Orderlineid </param>
        /// <param name="additionalInfo"> additionalInformation </param>
        /// <returns></returns>
        public void GenerateEmail(MailTypes mailType, int orderlineid, NameValueCollection additionalInfo = null)
        {
            CommunicationHistoryService historyService = new CommunicationHistoryService();
            int communicationTypeId = (int)mailType;
            try
            {
                CommunicationHistoryInfo historyInfo = null;
                switch (mailType)
                {
                    case MailTypes.ProviderCancellation:
                        historyInfo = SendProviderCancellationEmail(orderlineid);
                        break;
                    case MailTypes.CustomerCancellation:
                        historyInfo = SendCustomerCancellationEmail(orderlineid);
                        break;
                    case MailTypes.ProviderConfirmation:
                        historyInfo = SendProviderConfirmation(orderlineid);
                        break;
                    case MailTypes.CustomerConfirmation:
                        historyInfo = SendCustomerConfirmation(orderlineid);
                        break;
                    case MailTypes.ProviderProvisionalConfirmation:
                        historyInfo = SendProviderProvisionalConfirmationEmail(orderlineid);
                        break;
                    case MailTypes.ClientProvisionalConfirmation:
                        historyInfo = ClientProvisionalConfirmationEmail(orderlineid);
                        break;
                    case MailTypes.ManageBookingMailConfirmation:
                        historyInfo = ManageBookingEmailConfirmation(orderlineid);
                        break;
                    case MailTypes.PartialConfirmationEmailRemainder:
                        historyInfo = PartialConfirmationEmailRemainder(orderlineid);
                        break;
                    case MailTypes.CustomerConfirmationOnline:
                        historyInfo = SendOnlineCustomerConfiramtionEmail(orderlineid);
                        break;
                    case MailTypes.ProviderConfirmationOnline:
                        historyInfo = SendOnlineProviderConfirmationEmail(orderlineid);
                        break;
                    case MailTypes.PartialCustomerConfirmationOnline:
                        historyInfo = SendOnlinePartialCustomerConfirmationEmail(orderlineid);
                        break;
                    case MailTypes.BedBanksCustomerConfirmation:
                        historyInfo = SendBedBanksCustomerConfirmationEmail(orderlineid, additionalInfo);
                        break;
                    case MailTypes.BedBanksCustomerCancellaton:
                        historyInfo = SendBedBanksCustomerCancellationEmail(orderlineid);
                        break;
                    case MailTypes.BedBanksConfirmationIssue:
                        historyInfo = SendBedBanksConfirmationIssueEmail(orderlineid);
                        break;
                    case MailTypes.BedBanksCancellationIssue:
                        historyInfo = SendBedBanksCancellationIssueEmail(orderlineid);
                        break;
                    case MailTypes.BedBanksServiceIssue:
                        SendBedBanksServiceIssueEmail(additionalInfo);
                        break;
                    default:
                        break;
                }
                if (historyInfo != null)
                {
                    SendEmail(orderlineid, historyService, communicationTypeId, historyInfo);
                }
            }
            catch (Exception ex)
            {
                historyService.UpdateMailSentStatus(orderlineid, communicationTypeId, false);
                Diagnostics.WriteException(DiagnosticsCategory.BusinessLogic, ex, this);
            }
        }

        private void SendEmail(int orderlineid, ICommunicationHistoryService historyService, int communicationTypeId, CommunicationHistoryInfo historyInfo)
        {
            historyInfo.OrderlineId = orderlineid;
            historyInfo.CommunicationTypeID = communicationTypeId;
            ValidationResponse response = historyService.InsertCommunicationHistory(historyInfo);
            _emailService.Send(historyInfo.From, historyInfo.To, historyInfo.Subject, historyInfo.EmailHtmlText, true);
            historyService.UpdateMailSentStatus(orderlineid, communicationTypeId, true);
        }



        private CommunicationHistoryInfo PartialConfirmationEmailRemainder(int orderlineid)
        {
            Booking bookinginfo = _orderService.GetBooking(orderlineid);
            string mailSender = EmailSender;
            string mailReceiver = bookinginfo.Customer.Email;
            string mailSubject = PartialEmailConfirmationSubject;
            string mailBody = PartialEmailConfirmationBody;

            return new CommunicationHistoryInfo
            {
                From = mailSender,
                To = mailReceiver,
                Subject = GetMailText(mailSubject, bookinginfo),
                EmailHtmlText = GetMailText(mailBody, bookinginfo)
            };
        }



        ///<summary> 
        ///Send ProviderCancellationEmail 
        ///</summary>
        ///<param name="orderlineid"> Orderlineid </param>
        ///<returns>Email information as CommunicationHistory Object</returns>
        private CommunicationHistoryInfo SendProviderCancellationEmail(int orderlineid)
        {
            Booking bookinginfo = _orderService.GetBooking(orderlineid);
            string mailSender = EmailSender;
            string mailReceiver = bookinginfo.AccomodationInfo.EmailId;
            string mailSubject = ProviderCancellationSubject;
            string mailBody = ProviderCancellationBody;

            return new CommunicationHistoryInfo
            {
                From = mailSender,
                To = mailReceiver,
                Subject = GetMailText(mailSubject, bookinginfo),
                EmailHtmlText = GetMailText(mailBody, bookinginfo)
            };

        }

        ///<summary> 
        ///Send CustomerCancellationEmail 
        ///</summary>
        ///<param name="orderlineid"> Orderlineid </param>
        ///<returns>Email information as CommunicationHistory Object</returns>
        private CommunicationHistoryInfo SendCustomerCancellationEmail(int orderlineid)
        {
            Booking bookinginfo = _orderService.GetBooking(orderlineid);
            string mailSender = EmailSender;
            string mailReceiver = bookinginfo.Customer.Email;
            string mailSubject = CustomerCancellationSubject;
            LanguageCode = bookinginfo.Customer.LanguageName;
            string mailBody = CustomerCancellationBody;

            return new CommunicationHistoryInfo
            {
                From = mailSender,
                To = mailReceiver,
                Subject = GetMailText(mailSubject, bookinginfo),
                EmailHtmlText = GetMailText(mailBody, bookinginfo)
            };
        }

        ///<summary> 
        ///Send Provider Confirmation email 
        ///</summary>  
        ///<param name="orderlineid"> Orderlineid </param>
        ///<returns>Email information as CommunicationHistory Object</returns>
        private CommunicationHistoryInfo SendProviderConfirmation(int orderlineid)
        {
            Booking bookinginfo = _orderService.GetBooking(orderlineid);

            string mailSender = EmailSender;
            string mailReceiver = bookinginfo.AccomodationInfo.EmailId;
            string mailSubject = ProviderConfirmationSubject;
            string mailBody = ProviderConfirmationBody;


            return new CommunicationHistoryInfo
            {
                From = mailSender,
                To = mailReceiver,
                Subject = GetMailText(mailSubject, bookinginfo),
                EmailHtmlText = GetMailText(mailBody, bookinginfo)
            };

        }

        ///<summary> 
        ///Customer Confirmation email 
        ///</summary> 
        ///<param name="orderlineid"> Orderlineid </param>
        ///<returns>Email information as CommunicationHistory Object</returns>
        private CommunicationHistoryInfo SendCustomerConfirmation(int orderlineid)
        {
            Booking bookinginfo = _orderService.GetBooking(orderlineid);

            string mailSender = EmailSender;
            string mailReceiver = bookinginfo.Customer.Email;
            string mailSubject = CustomerConfirmationSubject;
            LanguageCode = bookinginfo.Customer.LanguageName;
            string mailBody = CustomerConfirmationBody;

            return new CommunicationHistoryInfo
            {
                From = mailSender,
                To = mailReceiver,
                Subject = GetMailText(mailSubject, bookinginfo),
                EmailHtmlText = GetMailText(mailBody, bookinginfo)
            };
        }

        ///<summary> 
        ///Send Provider Provisional Confirmation Email  
        ///</summary> 
        ///<param name="orderlineid"> Orderlineid </param>
        ///<returns>Email information as CommunicationHistory Object</returns>
        private CommunicationHistoryInfo SendProviderProvisionalConfirmationEmail(int orderlineid)
        {
            Booking bookinginfo = _orderService.GetBooking(orderlineid);
            if (bookinginfo.PayableAtHotel == 0)
            {
                string mailSender = EmailSender;
                string mailReceiver = bookinginfo.AccomodationInfo.EmailId;
                string mailSubject = ProviderProvisionalConfirmationSubject;
                string mailBody = ProviderProvisionalConfirmationBody;

                return new CommunicationHistoryInfo
                {
                    From = mailSender,
                    To = mailReceiver,
                    Subject = GetMailText(mailSubject, bookinginfo),
                    EmailHtmlText = GetMailText(mailBody, bookinginfo)
                };
            }
            else
            {
                return null;
            }

        }

        private CommunicationHistoryInfo ClientProvisionalConfirmationEmail(int orderlineid)
        {
            Booking bookinginfo = _orderService.GetBooking(orderlineid);
            if (bookinginfo.PayableAtHotel == 0)
            {
                string mailSender = EmailSender;
                string mailReceiver = bookinginfo.Customer.Email;
                string mailSubject = ClientProvisionalConfirmationSubject;
                string mailBody = ClientProvisionalConfirmationBody;


                return new CommunicationHistoryInfo
                {
                    From = mailSender,
                    To = mailReceiver,
                    Subject = GetMailText(mailSubject, bookinginfo),
                    EmailHtmlText = GetMailText(mailBody, bookinginfo)
                };
            }
            else
            {
                return null;
            }

        }

        /// <summary>
        ///  Send ManageBookingConfirmation Email
        /// </summary>
        /// <param name="orderlineid"></param>
        /// <returns>Email information as CommunicationHistory Object</returns>

        private CommunicationHistoryInfo ManageBookingEmailConfirmation(int orderlineid)
        {
            Booking bookinginfo = _orderService.GetBooking(orderlineid);

            if (bookinginfo.OrderLineId != 0)//multiple orderlineid as to be specified 
            {
                string mailSender = EmailSender;
                string mailReceiver = bookinginfo.Customer.Email;
                string mailBody = ManageBookingCustomerConfirmationBody;
                string mailSubject = ManageBookingCustomerConfirmationSubject;

                return new CommunicationHistoryInfo
                {
                    From = mailSender,
                    To = mailReceiver,
                    Subject = GetMailText(mailSubject, bookinginfo),
                    EmailHtmlText = GetMailText(mailBody, bookinginfo)
                };
            }
            else
            {
                return null;
            }

        }

        ///<summary> 
        ///Send CustomerConfirmationEmail Online
        ///</summary>
        ///<param name="orderlineid"> Orderlineid </param>
        ///<returns>Email information as CommunicationHistory Object</returns>
        private CommunicationHistoryInfo SendOnlineCustomerConfiramtionEmail(int orderlineid)
        {
            Diagnostics.Trace(DiagnosticsCategory.Communications, "GetBooking online customer start --> orderlineid -->" + Convert.ToString(orderlineid));
            Booking bookinginfo = _orderService.GetBooking(orderlineid);
            Diagnostics.Trace(DiagnosticsCategory.Communications, "GetBooking online customer completed --> customer firstname --> " + bookinginfo.Customer.FirstName);
            string mailSender = EmailSender;
            string mailReceiver = bookinginfo.Customer.Email;
            string mailSubject = CustomerConfirmationSubjectOnline;
            //LanguageCode = bookinginfo.Customer.LanguageName;
            string mailBody = CustomerConfirmationBodyOnline;

            return new CommunicationHistoryInfo
            {
                From = mailSender,
                To = mailReceiver,
                Subject = GetMailText(mailSubject, bookinginfo),
                EmailHtmlText = GetMailText(mailBody, bookinginfo)
            };
        }

        ///<summary> 
        ///Send ProviderConfirmationEmail Online
        ///</summary>
        ///<param name="orderlineid"> Orderlineid </param>
        ///<returns>Email information as CommunicationHistory Object</returns>
        private CommunicationHistoryInfo SendOnlineProviderConfirmationEmail(int orderlineid)
        {
            Booking bookinginfo = _orderService.GetBooking(orderlineid);
            string mailSender = EmailSender;
            string mailReceiver = bookinginfo.AccomodationInfo.EmailId;
            string mailSubject = ProviderConfirmationSubjectOnline;
            string mailBody = ProviderConfirmationBodyOnline;

            return new CommunicationHistoryInfo
            {
                From = mailSender,
                To = mailReceiver,
                Subject = GetMailText(mailSubject, bookinginfo),
                EmailHtmlText = GetMailText(mailBody, bookinginfo)
            };
        }

        ///<summary> 
        ///Partail Customer Confirmation email for online booking
        ///</summary> 
        ///<param name="orderlineid"> Orderlineid </param>
        ///<returns>Email information as CommunicationHistory Object</returns>
        private CommunicationHistoryInfo SendOnlinePartialCustomerConfirmationEmail(int orderlineid)
        {
            Booking bookinginfo = _orderService.GetBooking(orderlineid);

            string mailSender = EmailSender;
            string mailReceiver = bookinginfo.Customer.Email;
            string mailSubject = PartialCustomerConfirmationSubjectOnline;
            string mailBody = PartialCustomerConfirmationBodyOnline;


            return new CommunicationHistoryInfo
            {
                From = mailSender,
                To = mailReceiver,
                Subject = GetMailText(mailSubject, bookinginfo),
                EmailHtmlText = GetMailText(mailBody, bookinginfo)
            };
        }

        /// <summary>
        /// Bed banks Customer Confirmation Email
        /// </summary>
        /// <param name="orderlineid">The orderline for which the confirmation email has to be sent</param>
        /// <returns></returns>
        private CommunicationHistoryInfo SendBedBanksCustomerConfirmationEmail(int orderlineid, NameValueCollection additionalInfo)
        {
            Item emailItem = this.GetBedBanksEmailItem("Customer Confirmation Email");
            if (emailItem != null)
            {
                Booking bookinginfo = _orderService.GetBooking(orderlineid);
                bookinginfo.BedBanksCancellationMessage = SetBedBanksCancellationMessage(additionalInfo);
                return new CommunicationHistoryInfo
                {
                    From = SitecoreFieldsHelper.GetValue(emailItem, "From"),
                    To = bookinginfo.Customer.Email,
                    Subject = this.GetMailText(SitecoreFieldsHelper.GetValue(emailItem, "Subject"), bookinginfo),
                    EmailHtmlText = this.GetMailText(SitecoreFieldsHelper.GetValue(emailItem, "Body"), bookinginfo)
                };
            }
            else
                return null;

        }

        /// <summary>
        /// Bed banks Customer Cancellation Email
        /// </summary>
        /// <param name="orderlineid">The orderline for which the cancellation email has to be sent</param>
        /// <returns></returns>
        private CommunicationHistoryInfo SendBedBanksCustomerCancellationEmail(int orderlineid)
        {
            Item emailItem = this.GetBedBanksEmailItem("Customer Cancellation Email");
            if (emailItem != null)
            {
                Booking bookinginfo = _orderService.GetBooking(orderlineid);
                return new CommunicationHistoryInfo
                {
                    From = SitecoreFieldsHelper.GetValue(emailItem, "From"),
                    To = bookinginfo.Customer.Email,
                    Subject = this.GetMailText(SitecoreFieldsHelper.GetValue(emailItem, "Subject"), bookinginfo),
                    EmailHtmlText = this.GetMailText(SitecoreFieldsHelper.GetValue(emailItem, "Body"), bookinginfo)
                };
            }
            else
                return null;
        }

        /// <summary>
        /// Bed banks confirmation Issue Email
        /// </summary>
        /// <param name="orderlineid">The orderline for which the confirmation issue email has to be sent</param>
        /// <returns></returns>
        private CommunicationHistoryInfo SendBedBanksConfirmationIssueEmail(int orderlineid)
        {
            Item emailItem = this.GetBedBanksEmailItem("Confirmation Issue Email");
            if (emailItem != null)
            {
                Booking bookinginfo = _orderService.GetBooking(orderlineid);

                return new CommunicationHistoryInfo
                {
                    From = SitecoreFieldsHelper.GetValue(emailItem, "From"),
                    To = SitecoreFieldsHelper.GetValue(emailItem, "To"),
                    Subject = this.GetMailText(SitecoreFieldsHelper.GetValue(emailItem, "Subject"), bookinginfo),
                    EmailHtmlText = this.GetMailText(SitecoreFieldsHelper.GetValue(emailItem, "Body"), bookinginfo)
                };
            }
            else
                return null;
        }

        /// <summary>
        /// Bed banks cancellation issue Email
        /// </summary>
        /// <param name="orderlineid">The orderline for which the cancellation issue email has to be sent</param>
        /// <returns></returns>
        private CommunicationHistoryInfo SendBedBanksCancellationIssueEmail(int orderlineid)
        {
            Item emailItem = this.GetBedBanksEmailItem("Cancellation Issue Email");
            if (emailItem != null)
            {
                Booking bookinginfo = _orderService.GetBooking(orderlineid);

                return new CommunicationHistoryInfo
                {
                    From = SitecoreFieldsHelper.GetValue(emailItem, "From"),
                    To = SitecoreFieldsHelper.GetValue(emailItem, "To"),
                    Subject = this.GetMailText(SitecoreFieldsHelper.GetValue(emailItem, "Subject"), bookinginfo),
                    EmailHtmlText = this.GetMailText(SitecoreFieldsHelper.GetValue(emailItem, "Body"), bookinginfo)
                };
            }
            else
                return null;
        }

        /// <summary>
        /// Bed banks cancellation issue Email
        /// </summary>
        /// <param name="orderlineid">The orderline for which the cancellation issue email has to be sent</param>
        /// <returns></returns>
        private void SendBedBanksServiceIssueEmail(NameValueCollection additionalInfo)
        {
            Item emailItem = this.GetBedBanksEmailItem("Bed Banks Service Issue Email");
            if (emailItem != null)
            {
                string from = SitecoreFieldsHelper.GetValue(emailItem, "From");
                string to = SitecoreFieldsHelper.GetValue(emailItem, "To");
                string subject = SitecoreFieldsHelper.GetValue(emailItem, "Subject");
                string emailHtmlText = this.SetBedBanksExceptionMessage(SitecoreFieldsHelper.GetValue(emailItem, "Body"), additionalInfo);
                _emailService.Send(from, to, subject, emailHtmlText, true);
            }
        }

        /// <summary>
        /// Fill the placeholders in email template
        /// </summary>
        /// <param name="bodyText">Text need to be filled</param>
        /// <param name="bookinginfo">Booking information</param>
        /// <returns>Replaced string</returns>
        public string GetMailText(string bodyText, Booking bookinginfo)
        {

            var Orderline = UCommerce.EntitiesV2.OrderLine.FirstOrDefault(i => i.OrderLineId == bookinginfo.OrderLineId);
            //Product providerDetail = Product.FirstOrDefault(x => x.VariantSku == Orderline.VariantSku && x.ProductProperties != null && x.ProductProperties.Any(y => y.ProductDefinitionField.Name.Equals(ProductDefinationConstant.RoomType, StringComparison.InvariantCultureIgnoreCase) ));
            string roomType = Product.FirstOrDefault(x => x.VariantSku == Orderline.VariantSku).Name;



            if (bookinginfo != null)
            {
                bodyText = bodyText.Replace("{BookingReference}", !string.IsNullOrWhiteSpace(bookinginfo.BookingReference) ? bookinginfo.BookingReference : string.Empty);
                bodyText = bodyText.Replace("{GuestName}", !string.IsNullOrWhiteSpace(bookinginfo.GuestName) ? bookinginfo.GuestName : string.Empty + (!string.IsNullOrWhiteSpace(bookinginfo.GuestName) ? bookinginfo.GuestName : string.Empty));
                bodyText = bodyText.Replace("{ArrivalDate}", !string.IsNullOrWhiteSpace(bookinginfo.CheckInDate.ToString("dd/MM/yyyy")) ? bookinginfo.CheckInDate.ToString("dd/MM/yyyy") : string.Empty);
                bodyText = bodyText.Replace("{DepartureDate}", !string.IsNullOrWhiteSpace(bookinginfo.CheckOutDate.ToString("dd/MM/yyyy")) ? bookinginfo.CheckOutDate.ToString("dd/MM/yyyy") : string.Empty);
                bodyText = bodyText.Replace("{NumberOfNights}", !string.IsNullOrWhiteSpace(bookinginfo.NumberOfNights.ToString()) ? bookinginfo.NumberOfNights.ToString() : string.Empty);

                if (Orderline.GetOrderProperty(data.Constants.ProductType) != null && Orderline.GetOrderProperty(data.Constants.ProductType).Value.Equals(ProductType.BedBanks.ToString()))
                {
                    bodyText = bodyText.Replace("{OccupancyType}", (Orderline.GetOrderProperty(data.Constants.RoomInfo) == null || string.IsNullOrEmpty(Orderline.GetOrderProperty(data.Constants.RoomInfo).Value)) ? string.Empty : Orderline.GetOrderProperty(data.Constants.RoomInfo).Value);
                }
                else
                {
                    if (roomType != null)
                    {
                        //   bodyText = bodyText.Replace("{OccupancyType}", (!string.IsNullOrWhiteSpace(roomType) ? bookinginfo.OccupancyType : string.Empty));
                        bodyText = bodyText.Replace("{OccupancyType}", (roomType));

                    }
                }
                //bodyText = bodyText.Replace("{OccupancyType}", (!string.IsNullOrWhiteSpace(bookinginfo.OccupancyType) ? bookinginfo.OccupancyType : string.Empty));
                bodyText = bodyText.Replace("{BookingDetails}", (!string.IsNullOrWhiteSpace(bookinginfo.NumberOfRooms.ToString()) ? bookinginfo.NumberOfRooms.ToString() : string.Empty) + " room(s)/unit(s), " + (!string.IsNullOrWhiteSpace(bookinginfo.NumberOfAdults.ToString()) ? bookinginfo.NumberOfAdults.ToString() : "0") + " adult(s)/people, " + (!string.IsNullOrWhiteSpace(bookinginfo.NumberOfChildren.ToString()) ? bookinginfo.NumberOfChildren.ToString() : string.Empty) + " child/children");
                bodyText = bodyText.Replace("{NumberOfAdults}", !string.IsNullOrWhiteSpace(bookinginfo.NumberOfAdults.ToString()) ? bookinginfo.NumberOfAdults.ToString() : string.Empty);
                bodyText = bodyText.Replace("{NumberOfChildren}", !string.IsNullOrWhiteSpace(bookinginfo.NumberOfChildren.ToString()) ? bookinginfo.NumberOfChildren.ToString() : "0");

                //bodyText = bodyText.Replace("{ProcessingFee}", !string.IsNullOrWhiteSpace(bookinginfo.CreditCardProcessingFee.ToString()) ? bookinginfo.CreditCardProcessingFee.ToString("#.##") : "0");
                if (!string.IsNullOrWhiteSpace(bookinginfo.CreditCardProcessingFee.ToString()))
                {
                    bodyText = bodyText.Replace("{ProcessingFee}", bookinginfo.CreditCardProcessingFee.ToString("0.00"));
                }
                else
                {
                    bodyText = bodyText.Replace("{ProcessingFee}", "0.00");
                }
                //decimal paidtotaday=(bookinginfo.percentage + bookinginfo.ProcessingFee)
                if (bodyText.Contains("{SpecialRequest}"))
                {
                    NoteService noteService = new NoteService();
                    var notes = noteService.GetReservationNotes(bookinginfo.OrderLineId);
                    if (notes != null)
                    {
                        bodyText = bodyText.Replace("{SpecialRequest}", !string.IsNullOrWhiteSpace(notes.Where(i => i.TypeId == 3).Select(i => i.Note).FirstOrDefault()) ? (string.Join("\n", notes.Where(i => i.TypeId == 3).Select(i => i.Note).FirstOrDefault())) : string.Empty);
                    }
                    else
                    {
                        bodyText = bodyText.Replace("{SpecialRequest}", string.Empty);
                    }
                }

                if (bookinginfo.BedBanksCancellationMessage != null)
                {
                    BookingService book = new BookingService();
                    string finalString = string.Empty;
                    var bookingData = book.GetBooking(bookinginfo.OrderLineId);
                    if (bookingData != null)
                    {
                        var criteria = bookingData.OrderLine.GetOrderProperty(data.Constants.CancelCriteria).Value;
                        List<BedBankCancelCriteria> CancelCriterias = JsonConvert.DeserializeObject<List<BedBankCancelCriteria>>(criteria);
                        string cancelStatement = bookinginfo.BedBanksCancellationMessage;
                        List<string> statement = new List<string>();
                        foreach (var cancelCrietria in CancelCriterias)
                        {
                            string tempString;
                            tempString = cancelStatement.Replace("{0}", cancelCrietria.StartDate.Date.ToString());
                            if (cancelCrietria.EndDate <= bookinginfo.CheckInDate)
                                tempString = tempString.Replace("{1}", cancelCrietria.EndDate.Date.ToString());
                            else
                                tempString = tempString.Replace("{1}", bookinginfo.CheckInDate.Date.ToString());
                            tempString = tempString.Replace("{2}", cancelCrietria.Amount.ToString());
                            statement.Add(tempString);

                        }
                        finalString = string.Join("<br/>", statement);
                        bodyText = bodyText.Replace("{BedBanksCancelCriteria}", finalString);
                    }
                    //TODO: write code for cancellation
                }

                string offerName = GetOfferName(bookinginfo);

                bodyText = bodyText.Replace("{OfferName}", !string.IsNullOrWhiteSpace(offerName.ToString()) ? "( " + offerName + " )" : string.Empty);

                bodyText = bodyText.Replace("{PayableAtAccomodation}", !string.IsNullOrWhiteSpace(bookinginfo.PayableAtHotel.ToString()) ? bookinginfo.PayableAtHotel.ToString("f") : string.Empty);
                decimal BookingDepositPaidToday = (bookinginfo.Price - bookinginfo.PayableAtHotel);
                var orderline = UCommerce.EntitiesV2.OrderLine.FirstOrDefault(i => i.OrderLineId == bookinginfo.OrderLineId);
                bodyText = bodyText.Replace("{BookingDepositPaidToday}", !string.IsNullOrWhiteSpace(BookingDepositPaidToday.ToString()) ? BookingDepositPaidToday.ToString("f") : string.Empty);
                if (orderline != null && orderline.GetOrderProperty(data.Constants.ProductType) != null && orderline.GetOrderProperty(data.Constants.ProductType).Value.Equals(ProductType.BedBanks.ToString()))
                {
                    bodyText = bodyText.Replace("{BreakPrice}", !string.IsNullOrWhiteSpace(bookinginfo.TotalAmount.ToString()) ? (bookinginfo.TotalAmount - bookinginfo.CreditCardProcessingFee).ToString("f") : string.Empty);
                }
                else
                { 
                bodyText = bodyText.Replace("{BreakPrice}", !string.IsNullOrWhiteSpace(bookinginfo.TotalAmount.ToString()) ? bookinginfo.TotalAmount.ToString("f") : string.Empty);
                }
                bodyText = bodyText.Replace("{GateWay}", "ATG");
                bodyText = bodyText.Replace("{ClientID}", !string.IsNullOrWhiteSpace(bookinginfo.ClinetId) ? bookinginfo.ClinetId : string.Empty);
                bodyText = bodyText.Replace("{ClientName}", "ATG");
                bodyText = bodyText.Replace("{AgeOfChildren}", !string.IsNullOrWhiteSpace(bookinginfo.ChildAge) ? bookinginfo.ChildAge : "0");

                
                if (orderline != null)
                {
                    if (orderline[OrderPropertyConstants.PaymentMethod] == Constants.PaymentMethod.PartialPayment)
                    {
                        bodyText = bodyText.Replace("{NoOfDays}", orderline[OrderPropertyConstants.DaysBefore] ?? string.Empty);
                        decimal checkoutamount = 0;
                        decimal.TryParse(orderline[OrderPropertyConstants.ProviderAmountPaidCheckOut], out checkoutamount);
                        bodyText = bodyText.Replace("{PayableAtAccomodation}", (bookinginfo.Price + bookinginfo.CreditCardProcessingFee - checkoutamount).ToString("f"));
                        decimal totalprice = (bookinginfo.Price + bookinginfo.CreditCardProcessingFee);
                        var percentange = orderline[OrderPropertyConstants.PercentageOfAmount];
                        decimal paidtoday = ((Convert.ToDecimal(percentange) / 100) * bookinginfo.Price + bookinginfo.CreditCardProcessingFee);
                        bodyText = bodyText.Replace("{Totalprice}", (!string.IsNullOrWhiteSpace(totalprice.ToString()) ? totalprice.ToString("#.##") : "0"));
                        bodyText = bodyText.Replace("{PaidToday}", (!string.IsNullOrWhiteSpace(paidtoday.ToString()) ? paidtoday.ToString("#.##") : "0"));
                        bodyText = bodyText.Replace("{percentange}", (!string.IsNullOrWhiteSpace(percentange.ToString()) ? percentange.ToString() : "0"));
                    }
                    if (orderline.GetOrderProperty(data.Constants.ProductType) != null && orderline.GetOrderProperty(data.Constants.ProductType).Value.Equals(ProductType.BedBanks.ToString()))
                    {
                        IBookingService bookingService = ObjectFactory.Instance.Resolve<IBookingService>();
                        //get booking
                        var newBooking = bookingService.GetBooking(bookinginfo.OrderLineId);
                        if(newBooking!=null)
                        {
                            var provider = newBooking.GetProvider();
                            if(provider.ProviderType==ProviderType.BedBanks)
                            {
                                var bbHotel = (BedBankHotel)provider;
                                bodyText = bodyText.Replace("{AccomodationName}", !string.IsNullOrWhiteSpace(bbHotel.Name) ? bbHotel.Name : string.Empty);
                                bodyText = bodyText.Replace("{AccomodationPhone}", !string.IsNullOrWhiteSpace(bbHotel.Phone) ? bbHotel.Phone : string.Empty);
                                bodyText = bodyText.Replace("{AccomodationEmail}", !string.IsNullOrWhiteSpace(bbHotel.Email) ? bbHotel.Email : string.Empty);
                                bodyText = bodyText.Replace("{AccomodationAddress}", !string.IsNullOrWhiteSpace(bbHotel.Address) ? bbHotel.Address : string.Empty);
                                bodyText = bodyText.Replace("{AccomodationProvider}", !string.IsNullOrWhiteSpace(bbHotel.Name) ? bbHotel.Name : string.Empty);
                            }
                        }
                        string marginPercentage = (string.IsNullOrEmpty(orderline.GetOrderProperty(data.Constants.MarginPercentage).Value) ? string.Empty : orderline.GetOrderProperty(data.Constants.MarginPercentage).Value);
                        decimal marginPercent;
                        decimal.TryParse(marginPercentage, out marginPercent);
                        decimal bedBanksPrice = bookinginfo.Price / (1 + (marginPercent / 100));
                        decimal marginAmt = bedBanksPrice * (marginPercent / 100);
                        //TODO: Check for the bed bank product
                        bodyText = bodyText.Replace("{BedBanksMarginID}", (orderline.GetOrderProperty(data.Constants.MarginId) == null || string.IsNullOrEmpty(orderline.GetOrderProperty(data.Constants.MarginId).Value) ? string.Empty : orderline.GetOrderProperty(data.Constants.MarginId).Value));
                        bodyText = bodyText.Replace("{BedBanksMarginPercentage}", marginPercentage);
                        bodyText = bodyText.Replace("{BedBanksMarginAmount}", marginAmt.ToString());
                        bodyText = bodyText.Replace("{BedBanksBookingToken}", (orderline.GetOrderProperty(data.Constants.RoomToken) == null || string.IsNullOrEmpty(orderline.GetOrderProperty(data.Constants.RoomToken).Value) ? string.Empty : orderline.GetOrderProperty(data.Constants.RoomToken).Value));
                        bodyText = bodyText.Replace("{BedBanksPreBookingToken}", (orderline.GetOrderProperty(data.Constants.PreBookingToken) == null || string.IsNullOrEmpty(orderline.GetOrderProperty(data.Constants.PreBookingToken).Value) ? string.Empty : orderline.GetOrderProperty(data.Constants.PreBookingToken).Value));
                        bodyText = bodyText.Replace("{BedBanksBookingReference}", (orderline.GetOrderProperty(data.Constants.BBBookingReferenceId) == null || string.IsNullOrEmpty(orderline.GetOrderProperty(data.Constants.BBBookingReferenceId).Value) ? string.Empty : orderline.GetOrderProperty(data.Constants.BBBookingReferenceId).Value));
                        bodyText = bodyText.Replace("{BedBanksPrice}", bedBanksPrice.ToString());
                        
                    }
                }
                if (bookinginfo.AccomodationInfo != null)
                {
                    bodyText = bodyText.Replace("{AccomodationName}", !string.IsNullOrWhiteSpace(bookinginfo.AccomodationInfo.Title) ? bookinginfo.AccomodationInfo.Title : string.Empty);
                    bodyText = bodyText.Replace("{AccomodationPhone}", !string.IsNullOrWhiteSpace(bookinginfo.AccomodationInfo.Phone) ? bookinginfo.AccomodationInfo.Phone : string.Empty);
                    bodyText = bodyText.Replace("{AccomodationEmail}", !string.IsNullOrWhiteSpace(bookinginfo.AccomodationInfo.EmailId) ? bookinginfo.AccomodationInfo.EmailId : string.Empty);
                    bodyText = bodyText.Replace("{AccomodationAddress}", !string.IsNullOrWhiteSpace(bookinginfo.AccomodationInfo.Address) ? bookinginfo.AccomodationInfo.Address : string.Empty);
                    bodyText = bodyText.Replace("{AccomodationProvider}", !string.IsNullOrWhiteSpace(bookinginfo.AccomodationInfo.Title) ? bookinginfo.AccomodationInfo.Title : string.Empty);

                }
                if (bookinginfo.Customer != null)
                {
                    string customerAddress = bookinginfo.Customer.Address + " " + bookinginfo.Customer.AddressLine1 + " " + bookinginfo.Customer.AddressLine2;
                    bodyText = bodyText.Replace("{Address}", !string.IsNullOrWhiteSpace(customerAddress) ? customerAddress : string.Empty);
                    bodyText = bodyText.Replace("{GuestPhoneNumber}", !string.IsNullOrWhiteSpace(bookinginfo.Customer.Phone) ? bookinginfo.Customer.Phone : string.Empty);
                }

                if (bookinginfo.BookingNotes != null)
                {
                    bodyText = bodyText.Replace("{CancellationDetails}", !string.IsNullOrWhiteSpace(bookinginfo.BookingNotes.Where(x => x.TypeId == 1).Select(x => x.Note).FirstOrDefault()) ? bookinginfo.BookingNotes.Where(x => x.TypeId == 1).Select(x => x.Note).FirstOrDefault() : string.Empty);
                    bodyText = bodyText.Replace("{NoteToProvider}", !string.IsNullOrWhiteSpace(bookinginfo.BookingNotes.Where(x => x.TypeId == 2).Select(x => x.Note).FirstOrDefault()) ? bookinginfo.BookingNotes.Where(x => x.TypeId == 2).Select(x => x.Note).FirstOrDefault() : string.Empty);
                }

                if (bookinginfo.BookingSubrates != null)
                {
                    var provider = (bookinginfo.TotalAmount - bookinginfo.Deposit).ToString();
                    bodyText = bodyText.Replace("{ProviderRate}", !string.IsNullOrWhiteSpace(provider) ? provider : string.Empty);
                    //bodyText = bodyText.Replace("{Discount}", bookinginfo.BookingSubrates.Where(x => x.SubrateCode == "DISCOUNT").Select(x => x.Price).FirstOrDefault().ToString());
                }
            }
            bodyText = ReplaceTextWithEmpty(bodyText);
            return bodyText;
        }

        /// <summary>
        /// Replaces all places holders with empty string
        /// </summary>
        /// <param name="bodyText">Body text</param>
        /// <returns>Replaced text</returns>
        private static string ReplaceTextWithEmpty(string bodyText)
        {
            var regex = new Regex("{[^{}]+}");
            return regex.Replace(bodyText, "");
        }

        /// <summary>
        /// Gets the offer name
        /// </summary>
        /// <param name="bookinginfo">Booking information</param>
        /// <returns>Offer Name</returns>
        private string GetOfferName(Booking bookinginfo)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create(_connectionString);
            var sql = "dbo.LB_GetOfferName";
            string offerName = "";
            using (var command = db.GetStoredProcCommand(sql))
            {
                db.AddInParameter(command, "orderlineid", DbType.Int32, bookinginfo.OrderLineId);
                //execute reader
                var reader = db.ExecuteReader(command);
                while (reader.Read())
                {
                    var order = reader.Value<string>("OfferName");
                    offerName = order;
                }

            }
            return offerName;
        }

        private Item GetBedBanksEmailItem(string emailFieldName)
        {
            Item bedBanksEmailItem = null;
            Item clientItem = (!string.IsNullOrWhiteSpace(_clientId)) ? Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(_clientId)) : null;
            if (clientItem != null)
            {
                Item bedBanksSettingsItem = ((Sitecore.Data.Fields.MultilistField)clientItem.Fields["BedBanksProvider"]).GetItems().FirstOrDefault();
                bedBanksEmailItem = (bedBanksSettingsItem != null) ? SitecoreFieldsHelper.GetLookupFieldTargetItem(bedBanksSettingsItem, emailFieldName) : null;
            }
            return bedBanksEmailItem;
        }

        /// <summary>
        /// Method to set Bed banks cancellation Message
        /// </summary>
        /// <param name="data">Name value colleciton of data</param>
        /// <returns>Cancellation Message</returns>
        private string SetBedBanksCancellationMessage(NameValueCollection data)
        {
            string cancellationMessage = string.Empty;
            if (data != null)
            {
                string conditionalMessageItemIdString = data[Constants.ConditionalMessageItemId];
                Sitecore.Data.ID conditionalMessageItemId = new Sitecore.Data.ID();
                Sitecore.Data.ID.TryParse(conditionalMessageItemIdString, out conditionalMessageItemId);

                Item conditionalMessageItem = (!conditionalMessageItemId.IsNull) ? Sitecore.Context.Database.GetItem(conditionalMessageItemId) : null;
                cancellationMessage = conditionalMessageItem.Fields["Bed Banks Cancellation Message"].Value;
            }
            return cancellationMessage;
        }

        /// <summary>
        /// Method to set bed banks exception email body
        /// </summary>
        /// <param name="bodyContent">body content to be modified</param>
        /// <param name="data">modification data for bodycontent</param>
        /// <returns>modified body content</returns>
        private string SetBedBanksExceptionMessage(string bodyContent, NameValueCollection data)
        {
            if (data != null)
            {
                string exceptionFlow = data[Constants.ExceptionFlow];
                string exceptionMessage = data[Constants.ExceptionMessage];
                bodyContent = bodyContent.Replace("{BedBanksExceptionFlow}", exceptionFlow);
                bodyContent = bodyContent.Replace("{BedBanksExceptionMessage}", exceptionMessage);
            }

            return bodyContent;
        }
    }

    /// <summary>
    /// Enum of mail types
    /// </summary>
    public enum MailTypes
    {
        ProviderConfirmation = 1,
        CustomerConfirmation = 2,
        ProviderCancellation = 3,
        CustomerCancellation = 4,
        ProviderProvisionalConfirmation = 5,
        ClientProvisionalConfirmation = 6,
        ManageBookingMailConfirmation = 7,
        PartialConfirmationEmailRemainder = 8,
        CustomerConfirmationOnline = 9,
        ProviderConfirmationOnline = 10,
        PartialCustomerConfirmationOnline = 11,
        BedBanksCustomerConfirmation = 12,
        BedBanksCustomerCancellaton = 13,
        BedBanksConfirmationIssue = 14,
        BedBanksCancellationIssue = 15,
        BedBanksServiceIssue = 16
    }

}
