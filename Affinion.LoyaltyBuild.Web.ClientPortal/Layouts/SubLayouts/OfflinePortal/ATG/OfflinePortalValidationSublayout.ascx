﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalValidationSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.ATG.OfflinePortalValidationSublayout" %>

<div id="CustomerValidation"  clientidmode="Static" runat="server">
        <div class="booking-detail-info col-md-12 no-padding no-margin ">
            <div class="content">    
                <div class="content-body">
                        <a name="atgvalidation"></a>
                        <div class="row">
                            <div class="col-sm-12 martop15">
                                <div class="content_bg_inner_box no-margin alert-info">
                                    <div class="col-sm-6">
                                        <h3>
                                            <sc:text id="EmailValidationTitleText" field="EmailValidationTitle" runat="server" />
                                        </h3>
                                    </div>
                                    <div id="FirstDescriptionDiv" class="col-sm-12 margin-row" runat="server">
                                        <span>
                                            <sc:text id="FirstDescription" field="FirstDescription" runat="server" />
                                        </span>
                                    </div>
                                    <div id="SecondDescriptionDiv" class="col-sm-12 margin-row" runat="server">
                                        <span>
                                            <sc:text id="SecondDescription" field="SecondDescription" runat="server" />
                                        </span>
                                    </div>
                                    <div id="ThirdDescriptionDiv" class="col-sm-12 margin-row" runat="server">
                                        <span>
                                            <sc:text id="ThirdDescription" field="ThirdDescription" runat="server" />
                                        </span>
                                    </div>
                                    <div class="col-sm-12 margin-row">
                                        <div class="col-sm-4">
                                            <label><sc:text id="EmailTitleText" field="EmailTitle" runat="server"/> <span class="accent58-f">*</span></label>
                                        </div>
                                        <div class="col-sm-5">
                                            <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" ToolTip="Enter Email Address"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" placeholder="Enter Email Address" runat="server" ForeColor="Red" ControlToValidate="txtEmail" ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$" Display="Dynamic" ValidationGroup="PartnerValidation"><sc:text id="txtvalidEmailText" runat="server"/></asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator runat="server"  ForeColor="Red" ControlToValidate="txtEmail" Display="Dynamic" ValidationGroup="PartnerValidation"><sc:text id="txtEmailRequiredText" runat="server"/></asp:RequiredFieldValidator>
                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>
                                    <div class="col-sm-12 margin-row">
                                        <div class="col-sm-4">
                                            <label><sc:text id="ConfirmEmailTitleText" field="ConfirmEmailTitle" runat="server"/><span class="accent58-f">*</span></label>
                                        </div>
                                        <div class="col-sm-5">
                                            <asp:TextBox ID="txtConfirmEmail" runat="server" CssClass="form-control" ToolTip="Confirm Email Address"></asp:TextBox>
                                            <asp:CompareValidator ID="CompareValidator1" runat="server" placeholder="Confirm Email Address" ControlToCompare="txtEmail" ControlToValidate="txtConfirmEmail" Display="Dynamic" ForeColor="Red" ValidationGroup="PartnerValidation"><sc:text id="txtEmailMismatchText" runat="server"/></asp:CompareValidator>
                                            <asp:RequiredFieldValidator ID="cnfrmEmailValidator" runat="server" Display="Dynamic" ControlToValidate="txtConfirmEmail" ForeColor="Red" ValidationGroup="PartnerValidation"><sc:text id="txtConfirmEmailText" runat="server"/></asp:RequiredFieldValidator>
                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>
                                    <div id="EmailListConsent1Div" class="col-sm-12 margin-row" runat="server">
                                        <div class="col-sm-9">

                                            <sc:text id="EmailList1ConsentLabel" field="EmailList1Consent" runat="server" />
                                        </div>
                                        <div class="col-sm-2">

                                            <asp:CheckBox ID="EmailList1ConsentCheckBox" runat="server" />
                                        </div>
                                    </div>
                                    <div id="EmailListConsent2Div" class="col-sm-12 margin-row" runat="server">
                                        <div class="col-sm-9">

                                            <sc:text id="EmailList2ConsentLabel" field="EmailList2Consent" runat="server" />
                                        </div>
                                        <div class="col-sm-2">
                                            <asp:CheckBox ID="EmailList2ConsentCheckBox" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-sm-12 margin-row">
                                        <div class="col-sm-9">
                                            <span>
                                                <sc:text id="InstructionText" field="InstructionText" runat="server" />
                                            </span>
                                        </div>
                                        <div class="col-sm-2">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 margin-row">
                                        <div class="col-sm-4"></div>
                                        <div class="col-sm-5">
                                            <%--<asp:Button ID="btnToPersonalDetail" type="submit" ClientIDMode="Static" runat="server" CssClass="btn btn-default add-break pull-right validate" Text="Continue to Personal Detail" OnClick="btnToPersonalDetail_Click" ValidationGroup="emailValidation" />--%>
                                            <button type="submit" id="btnToPersonalDetail" runat="server" class="btn btn-default add-break pull-right personalDetailValidation validate" OnServerClick="btnToPersonalDetail_Click" validationgroup="PartnerValidation"><sc:text id="ContinueToPersonalDetailTitle" field="ContinueToPersonalDetailTitle" runat="server"/></button>
                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    
            </div>
            </div>
        </div>
    </div>







