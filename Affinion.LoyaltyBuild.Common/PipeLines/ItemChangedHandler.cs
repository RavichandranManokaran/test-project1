﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           ItemChangedHandler.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Handler to read saving items when changing items: using httprequest pipeline
/// </summary>

#region Using Directives
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Pipelines.HttpRequest;
using Sitecore.SecurityModel;
using System; 
#endregion

namespace Affinion.LoyaltyBuild.Common.PipeLines
{
    #region Public Methods
    public class ItemChangedHandler : HttpRequestProcessor
    {
        /// <summary>
        /// Override the process event of the pipeline
        /// </summary>
        /// <param name="args">Request Arguments</param>
        public override void Process(HttpRequestArgs args)
        {
            try
            {
                /// Get the cache if available
                var decryptCache = Sitecore.Caching.CacheManager.FindCacheByName("decryptCache");

                if (decryptCache != null)
                {
                    /// Read the item in the cache if available
                    if (decryptCache.GetValue("decryptKey") != null)
                    {
                        /// Refer master db to get the required item
                        Database masterDb = Sitecore.Configuration.Factory.GetDatabase("master");
                        /// Get the item to encrypt by item id stored in cache: (Item id should be store when decrypting)
                        Item item = masterDb.Items[decryptCache.GetValue("decryptKey") as Sitecore.Data.ID];
                        if (item != null)
                        {
                            /// Read the item field "Value"
                            string encryptValue = item.Fields["Value"].Value;
                            /// Start editing the item
                            using (new SecurityDisabler())
                            {
                                /// Clear the cache when decrypting
                                var tempCache = Sitecore.Caching.CacheManager.FindCacheByName("decryptCache");
                                if (tempCache != null)
                                {
                                    tempCache.Clear();
                                }

                                item.Editing.BeginEdit();
                                /// Encrypt and save the new value to the item
                                item.Fields["Value"].Value = Cryptography.Encrypt(encryptValue);
                                item.Editing.EndEdit();
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                // TODO: Handle in a general way(error page or a error label in layout)
                throw;
            }
        }
    } 
    #endregion
}
