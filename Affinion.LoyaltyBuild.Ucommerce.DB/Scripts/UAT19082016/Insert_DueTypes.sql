if not exists (select 1 from [LB_DueTypes] where DueTypeId = '9194B57C-E4D8-4FCB-B2C2-826351C029D4')
	insert into [dbo].[LB_DueTypes]([DueTypeId],[Description],[IsActive],[CanBeDeleted],[ShowInList],[Code]) values('9194B57C-E4D8-4FCB-B2C2-826351C029D4','Deposit as Cancellation Fee',1,1,1,'CANCELFEE')
if not exists (select 1 from [LB_DueTypes] where DueTypeId = '89456B16-A7A8-44E1-A499-4A3CF2FF6709')
	insert into [dbo].[LB_DueTypes]([DueTypeId],[Description],[IsActive],[CanBeDeleted],[ShowInList],[Code]) values('89456B16-A7A8-44E1-A499-4A3CF2FF6709','Commision of the Break Fee',1,0,0,'COMMISSION')
if not exists (select 1 from [LB_DueTypes] where DueTypeId = '20E1FE88-B81C-41D1-BDCA-9A80233FCF81')
	insert into [dbo].[LB_DueTypes]([DueTypeId],[Description],[IsActive],[CanBeDeleted],[ShowInList],[Code]) values('20E1FE88-B81C-41D1-BDCA-9A80233FCF81','Credit Card Processing Fee',1,1,1,'PROFEE')
if not exists (select 1 from [LB_DueTypes] where DueTypeId = 'DAB48539-B397-4009-86A3-15A4DAD5A6C9')
	insert into [dbo].[LB_DueTypes]([DueTypeId],[Description],[IsActive],[CanBeDeleted],[ShowInList],[Code]) values('DAB48539-B397-4009-86A3-15A4DAD5A6C9','Provider Amount',1,0,0,'PROVIDERAMOUNT')
if not exists (select 1 from [LB_DueTypes] where DueTypeId = '490503B8-5DD4-453A-A4DF-6EA4CC0A7A1D')
	insert into [dbo].[LB_DueTypes]([DueTypeId],[Description],[IsActive],[CanBeDeleted],[ShowInList],[Code]) values('490503B8-5DD4-453A-A4DF-6EA4CC0A7A1D','Deposit as Transfer Fee',1,1,1,'TRANSFERFEE')
if not exists (select 1 from [LB_DueTypes] where DueTypeId = '275390B7-05D6-4875-B501-F4C377444B86')
	insert into [dbo].[LB_DueTypes]([DueTypeId],[Description],[IsActive],[CanBeDeleted],[ShowInList],[Code]) values('275390B7-05D6-4875-B501-F4C377444B86','No Show Fee',1,1,1,'')
if not exists (select 1 from [LB_DueTypes] where DueTypeId = '97BF1444-49DC-44B7-AC98-ECEF52C1C904')
	insert into [dbo].[LB_DueTypes]([DueTypeId],[Description],[IsActive],[CanBeDeleted],[ShowInList],[Code]) values('97BF1444-49DC-44B7-AC98-ECEF52C1C904','Price Change Adjustment',1,1,1,'')
if not exists (select 1 from [LB_DueTypes] where DueTypeId = '378AD26A-9DDF-4973-9B85-689B69384188')
	insert into [dbo].[LB_DueTypes]([DueTypeId],[Description],[IsActive],[CanBeDeleted],[ShowInList],[Code]) values('378AD26A-9DDF-4973-9B85-689B69384188','Refund',1,1,1,'REFUND')
if not exists (select 1 from [LB_DueTypes] where DueTypeId = 'D0BE0235-879F-4E3B-A2EA-9C4D8F38AA22')
	insert into [dbo].[LB_DueTypes]([DueTypeId],[Description],[IsActive],[CanBeDeleted],[ShowInList],[Code]) values('D0BE0235-879F-4E3B-A2EA-9C4D8F38AA22','Incorrect Amount Charged',1,1,1,'')

Update [LB_DueTypes] set [Description]='Processing Fee' where [DueTypeId]='20E1FE88-B81C-41D1-BDCA-9A80233FCF81'
Update [LB_DueTypes] set [Description]='Commission of the Break Fee' where [DueTypeId]='89456B16-A7A8-44E1-A499-4A3CF2FF6709'

