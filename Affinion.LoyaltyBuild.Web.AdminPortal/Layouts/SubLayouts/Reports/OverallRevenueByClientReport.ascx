﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OverallRevenueByClientReport.ascx.cs" Inherits="Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.Reports.OverallRevenueByClientReport" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Sitecore.Data.Items" %>

<div class="OverallRevenueByClient">
    <div>
        <h3>
            <sc:Text ID="OverallRevenueText" runat="server" />
        </h3>
    </div>
    <div>
        <asp:ValidationSummary ID="validationErrorMsg" CssClass="col-sm-12 alert alert-danger" runat="server" ShowValidationErrors="true" EnableClientScript="true" DisplayMode="BulletList" ShowSummary="true" />
    </div>
    <div>
        <div class="row report-filter container-fluid marT20">
            <div class="col-xs-12 marT20">
                <div class="row report-filter">
                    <div class="col-md-3">
                        <label>
                            <sc:Text ID="StartDateText" runat="server" />
                        </label>
                        <span class="required">*</span>
                    </div>
                    <div class="col-md-3 ">
                        <div class="form-group">
                            <div class="input-group date">
                                <input type="text" id="txtStartDate" runat="server" clientidmode="Static" class="form-control datepicker width-85 pull-left font-size-12" />
                                <asp:RequiredFieldValidator ID="startDateRequired" runat="server" CssClass="highligh-error" ErrorMessage="Please Select Start Date" ControlToValidate="txtStartDate" Display="None" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label>
                            <sc:Text ID="ClientText" runat="server" />
                        </label>
                    </div>
                    <%--<div class="col-md-3">
                        <asp:DropDownList ID="ddlClient" runat="server" CssClass="form-control" AppendDataBoundItems="false">
                             <asp:ListItem Text="Clients" Selected="True" Value="ClientId" />
                         </asp:DropDownList>
                    </div>--%>
                    <div class="col-md-3">
                        <asp:DropDownList ID="ddlClientList" runat="server" CssClass="form-control" AppendDataBoundItems="false">
                            <asp:ListItem Text="All Clients" Selected="True" Value="ClientId"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-xs-12 no-padding">
                    <div class="row">
                        <div class="col-md-3">
                            <label>
                                <sc:Text ID="EndDateText" runat="server" />
                            </label>
                            <span class="required">*</span>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="input-group date">
                                    <input type="text" id="txtEndDate" runat="server" clientidmode="Static" class="form-control datepicker width-85 pull-left font-size-12" />
                                    <asp:RequiredFieldValidator ID="endDateRequired" runat="server" CssClass="highligh-error" ErrorMessage="Please Select End Date" ControlToValidate="txtEndDate" Display="None" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label>
                                <sc:Text ID="BookingMethodText" Field="BookingMethod" runat="server" />
                            </label>
                        </div>
                        <div class="col-md-3">
                            <asp:DropDownList ID="ddlBookingMethod" runat="server" CssClass="form-control" AppendDataBoundItems="false">
                                <asp:ListItem Text="Select Booking Method" Selected="True" Value="" />
                                <asp:ListItem Text="Online" Value="Online" />
                                <asp:ListItem Text="Offline" Value="Offline" />
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 no-padding">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" id="btnExcelReport" class="btn btn-primary pull-right" runat="server" onserverclick="btnExcelReport_ServerClick">
                                <sc:Text ID="ExcelReportText" runat="server" />
                            </button>
                            &nbsp;&nbsp;
                        <button type="submit" id="btnHtmlReport" class="btn btn-primary pull-right marR5" runat="server" onserverclick="btnHtmlReport_ServerClick">
                            <sc:Text ID="HtmlReportText" runat="server" />
                        </button>
                        </div>
                        <div>
                            <asp:Label ID="NoRecordFoundMsg" class="panel-title" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>


                <div class="row" id="htmlReportGrid" runat="server">
                    <div class="col-sm-12 marT20">
                        <div>
                        <h4 class="panel-title">
                            <sc:Text ID="scUnclaimedReceipts" Field="Report Title" runat="server" />
                            <asp:Label ID="records" class="pull-right" runat="server"></asp:Label></h4>
                           </div>
                        <div class="panel panel-default marT20">

                            <div class="panel-body">
                                <div class="receipt_body">
                                    <%-- Result Grid--%>
                                    <div runat="server" id="ResultDiv" clientidmode="Static">
                                        <div id="data-grid" class="table-responsive">
                                            <asp:GridView ID="ResultGridView" runat="server" AllowPaging="True" PageSize="10"
                                                AutoGenerateColumns="false"
                                                OnPageIndexChanging="ResultGridView_PageIndexChanging"
                                                SelectedRowStyle-Font-Bold="true" CssClass="table table-striped table-borderedr"
                                                PagerSettings-NextPageText=" Next " PagerSettings-PreviousPageText=" Previous " PagerSettings-Mode="NumericFirstLast" PagerSettings-FirstPageText=" First " PagerSettings-LastPageText=" Last " PagerSettings-Position="Bottom" PagerStyle-Font-Bold="true" PagerStyle-CssClass="pagination-grid">
                                                <Columns>
                                                    <asp:BoundField DataField="Bookings" HeaderText="Bookings" />
                                                    <asp:BoundField DataField="Booking Method" HeaderText="Booking Method" />
                                                    <asp:BoundField DataField="ParterName" HeaderText="ParterName" />

                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    $(document).ready(function () {
        jQuery(function () {
            jQuery("#txtStartDate").datepicker({
                defaultDate: new Date(),
                dateFormat: "<%=Affinion.LoyaltyBuild.Common.Utilities.DateTimeHelper.DateFormatJavaScript%>",
                    minDate: -180,
                    maxDate: 0,
                    numberOfMonths: 1,
                    showOn: "both",
                    buttonImage: "images/calendar.png",
                    buttonImageOnly: false,
                    buttonClass: "glyphicon-calendar"
                }).next(".ui-datepicker-trigger").addClass("btn btn-default glyphicon glyphicon-calendar calander-icon pull-left");

                jQuery("#txtEndDate").datepicker({
                    defaultDate: new Date(),
                    dateFormat: "<%=Affinion.LoyaltyBuild.Common.Utilities.DateTimeHelper.DateFormatJavaScript%>",
                    minDate: -180,
                    maxDate: 0,
                    numberOfMonths: 1,
                    showOn: "both",
                    buttonImage: "images/calendar.png",
                    buttonImageOnly: false,
                    buttonClass: "glyphicon-calendar"
                }).next(".ui-datepicker-trigger").addClass("btn btn-default glyphicon glyphicon-calendar calander-icon pull-left");
            });
        });
</script>
