﻿/*<summary>
Description: Stored procedure to retrieve customer details for the given combination Booking Reference ID,  
			Customer Id, Client Id	
<param name="@BookingReferenceId">The Booking Reference Id</param>
<param name="@ClientId">The Client Id</param>
<param name="@CustomerId">The Customer Id</param>
</summary>*/
CREATE PROCEDURE [dbo].[LB_GetCustomerDetails](
    @Email nvarchar(255) = NULL, 
	@ClientId varchar(50) = NULL, 
	@CustomerId int = NULL)
AS 

SELECT C.CustomerId, 
       C.FirstName, 
       C.LastName,   
       C.EmailAddress, 
       C.MobilePhoneNumber,
	   ISNULL(C.LanguageName,'0') AS LanguageName,
       C.PhoneNumber, 
       CA.ClientId,
	   CA.Title,
       CA.AddressLine1, 
       CA.AddressLine2, 
       CA.AddressLine3, 
       CA.CountryId, 
       CA.LocationId,
	   CA.BranchId, 
	   CA.Subscribe, 
	   CA.BookingReferenceId, 
	   CA.Active
FROM [dbo].uCommerce_Customer C
INNER JOIN [dbo].LB_CustomerAttribute CA
ON C.CustomerID=CA.CustomerID
WHERE C.EmailAddress = ISNULL(@Email,C.EmailAddress) 
AND CA.ClientId = ISNULL(@ClientId,CA.ClientId) 
AND CA.CustomerId = ISNULL(@CustomerId,CA.CustomerId)

GO
