﻿using Affinion.LoyaltyBuild.Model.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Api.Booking.Data.Basket
{
    /// <summary>
    /// Cancel request
    /// </summary>
    public interface ICancelRequest
    {
        /// <summary>
        /// int orderline id
        /// </summary>
        int OrderLineId { get; set; }

        /// <summary>
        /// cancel reason
        /// </summary>
        string CancelReason { get; set; }

        /// <summary>
        /// note to provider
        /// </summary>
        string NoteToProvider { get; set; }

        /// <summary>
        /// cancelled by
        /// </summary>
        string CancelledBy { get; set; }
    }
}
