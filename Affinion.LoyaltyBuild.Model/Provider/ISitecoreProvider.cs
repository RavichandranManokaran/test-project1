﻿using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Model.Provider
{
    /// <summary>
    /// Sitecore provider class
    /// </summary>
    public interface ISitecoreProvider
    {
        /// <summary>
        /// Sitecore item for that provider
        /// </summary>
        Item SitecoreItem { get; set; }

        /// <summary>
        /// Loyalty hotel type
        /// </summary>
        string HotelType { get; set; }
    }
}
