CREATE TYPE [dbo].[TVP_uCommerce_Subrates] AS TABLE(
	[OrderLineID] [int] NOT NULL,
	[SubrateItemCode] [varchar](50) NOT NULL,
	[Amount] [money] NOT NULL,
	[Discount] [money] NULL,
	[IsIncludedInUnitPrice] [bit] NOT NULL
)
GO


