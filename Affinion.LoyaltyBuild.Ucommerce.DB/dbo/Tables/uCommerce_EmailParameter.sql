﻿CREATE TABLE [dbo].[uCommerce_EmailParameter] (
    [EmailParameterId]  INT           IDENTITY (1, 1) NOT NULL,
    [Name]              NVARCHAR (50) NOT NULL,
    [GlobalResourceKey] NVARCHAR (50) NOT NULL,
    [QueryStringKey]    NVARCHAR (50) NOT NULL,
    CONSTRAINT [uCommerce_PK_EmailParameter] PRIMARY KEY CLUSTERED ([EmailParameterId] ASC)
);

