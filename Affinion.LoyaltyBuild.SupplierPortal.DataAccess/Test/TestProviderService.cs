﻿using Affinion.LoyaltyBuild.SupplierPortal.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.SupplierPortal.DataAccess.Test
{
    /// <summary>
    /// test provider service to return dummy data
    /// </summary>
    public class TestProviderService : IProviderService
    {
        /// <summary>
        /// Get provider info by provider id
        /// </summary>
        /// <param name="providerId"></param>
        /// <returns></returns>
        public Provider GetProviderById(int providerId)
        {
            return new Provider { Id = 1, Name = "Provider 1" };
        }

        /// <summary>
        /// Get all providers
        /// </summary>
        /// <returns></returns>
        public List<Provider> GetAllProviders()
        {
            var providers = new List<Provider>();

            providers.Add(new Provider { Id = 1, Name = string.Format("Group{0}Provider 1", 1) });
            providers.Add(new Provider { Id = 2, Name = string.Format("Group{0}Provider 2", 2) });
            providers.Add(new Provider { Id = 3, Name = string.Format("Group{0}Provider 3", 3) });

            return providers;
        }

        /// <summary>
        /// Get provider by the hotel group id
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public List<Provider> GetProvidersByHotelGroup(int groupId)
        {
            var providers = new List<Provider>();

            providers.Add(new Provider { Id = 1, Name = string.Format("Group{0}Provider 1", groupId) });
            providers.Add(new Provider { Id = 2, Name = string.Format("Group{0}Provider 2", groupId) });
            providers.Add(new Provider { Id = 3, Name = string.Format("Group{0}Provider 3", groupId) });

            return providers;
        }


        public List<Client> GetClientList()
        {
            var clients = new List<Client>();

            clients.Add(new Client { Id = "1", Name = "Client 1" });
            clients.Add(new Client { Id = "2", Name = "Client 2" });
            clients.Add(new Client { Id = "3", Name = "Client 3" });

            return clients;
        }

        public List<ProviderGroup> GetAllProviderGroups()
        {
            var groups = new List<ProviderGroup>();

            groups.Add(new ProviderGroup { Id = 1, Name = "Group 1" });
            groups.Add(new ProviderGroup { Id = 2, Name = "Group 2" });
            groups.Add(new ProviderGroup { Id = 3, Name = "Group 3" });

            return groups;
        }

        public ProviderGroup GetProviderGroupById(int provierGroupId)
        {
            return new ProviderGroup { Id = provierGroupId, Name = string.Format("Client {0}", provierGroupId) };
        }


        public Provider GetProviderById(string uniqueId)
        {
            return new Provider { Id = 1, Name = "Provider 1" };
        }

        public ProviderGroup GetProviderGroupById(string uniqueId)
        {
            return new ProviderGroup { Id = Convert.ToInt32(uniqueId), Name = string.Format("Client {0}", uniqueId) };
        }

        public List<Provider> GetProvidersByHotelGroup(string uniqueId)
        {
            var providers = new List<Provider>();

            providers.Add(new Provider { Id = 1, Name = string.Format("Group{0}Provider 1", uniqueId) });
            providers.Add(new Provider { Id = 2, Name = string.Format("Group{0}Provider 2", uniqueId) });
            providers.Add(new Provider { Id = 3, Name = string.Format("Group{0}Provider 3", uniqueId) });

            return providers;
        }
    }
}
