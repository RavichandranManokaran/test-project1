﻿/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           EmailService.cs
/// Sub-system/Module:     Communications(VS project name)
/// Description:           Email Service Implementation
/// </summary>

#region Using Statements
using Affinion.LoyaltyBuild.Common.Instrumentation;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Net;
using System.IO;
#endregion

namespace Affinion.LoyaltyBuild.Communications.Services
{
    public class EmailService : IEmailService
    {
        /// <summary>
        /// Email Send 
        /// </summary>
        /// <param name="from">From</param>
        /// <param name="to">To</param>
        /// <param name="subject">Subject</param>
        /// <param name="message">Email Body</param>
        /// <param name="isHtml">Is body Html</param>
        public void Send(string from, string to, string subject, string message, bool isHtml)
        {
            try
            {
                using (MailMessage mail = new MailMessage())
                {
                    Diagnostics.Trace(DiagnosticsCategory.Communications, "Line 1 : from text --> " + from + " to text --> " + to + "subject text --> " + subject);
                    Diagnostics.Trace(DiagnosticsCategory.Communications, "Line 2 : message text --> " + message);
                    mail.From = new MailAddress(from);
                    mail.To.Add(new MailAddress(to));
                    mail.Subject = subject;
                    Diagnostics.Trace(DiagnosticsCategory.Communications, "Assigned subject --> ");
                    mail.Body = message;
                    Diagnostics.Trace(DiagnosticsCategory.Communications, "Assigned message --> ");
                    mail.IsBodyHtml = isHtml;
                    Diagnostics.Trace(DiagnosticsCategory.Communications, "Assigned ishmtl & before send email --> ");

                    this.Send(mail);
                    Diagnostics.Trace(DiagnosticsCategory.Communications, "Assigned ishmtl & after send email --> ");

                }
            }
            catch (Exception ex)
            {
                Diagnostics.Trace(DiagnosticsCategory.Communications, "exception received --> " + ex.Message);
                Diagnostics.WriteException(DiagnosticsCategory.Communications, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Email send
        /// </summary>
        /// <param name="from">From</param>
        /// <param name="to">To</param>
        /// <param name="cc">Cc list</param>
        /// <param name="subject">Subject</param>
        /// <param name="message">Email Body</param>
        /// <param name="isHtml">Is body Html</param>
        public void Send(string from, string to, IList<string> cc, string subject, string message, bool isHtml)
        {
            try
            {
                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress(from);
                    mail.To.Add(new MailAddress(to));
                    mail.Subject = subject;
                    mail.Body = message;
                    mail.IsBodyHtml = isHtml;
                    if (cc != null)
                    {
                        foreach (string address in cc)
                        {
                            if (address.Length > 0)
                                mail.CC.Add(new MailAddress(address.ToString()));
                        }

                    }

                    this.Send(mail);

                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Communications, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Email send
        /// </summary>
        /// <param name="from">From</param>
        /// <param name="to">To</param>
        /// <param name="subject">Subject</param>
        /// <param name="message">Email Body</param>
        /// <param name="isHtml">Is body Html</param>
        /// <param name="stream">memorystream </param>
        /// <param name="filename">filename</param>
        /// <param name="mimetype">mimetype</param>
        public void Send(string from, string to, string subject, string message, bool isHtml, Stream stream, string filename, string mimetype)
        {
            try
            {
                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress(from);
                    mail.To.Add(new MailAddress(to));
                    mail.Subject = subject;
                    mail.Body = message;
                    mail.IsBodyHtml = isHtml;
                    mail.Attachments.Add(new Attachment(stream, filename, mimetype));
                    this.Send(mail);

                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Communications, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Send Email With CC And Attachment
        /// </summary>
        /// <param name="from">From</param>
        /// <param name="to">To</param>
        /// <param name="cc">CC</param>
        /// <param name="subject">Mail Subject</param>
        /// <param name="message">Mail Message</param>
        /// <param name="isHtml">Is body Html</param>
        /// <param name="stream">Stream</param>
        /// <param name="filename">FileName</param>
        /// <param name="mimetype">mimetype</param>
        public void Send(string from, string to, IList<string> cc, string subject, string message, bool isHtml, Stream stream, string filename, string mimetype)
        {
            try
            {
                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress(from);
                    mail.To.Add(new MailAddress(to));
                    if (cc != null)
                    {
                        foreach (string address in cc)
                        {
                            if (address.Length > 0)
                                mail.CC.Add(new MailAddress(address.ToString()));
                        }

                    }
                    mail.Subject = subject;
                    mail.Body = message;
                    mail.IsBodyHtml = isHtml;
                    mail.Attachments.Add(new Attachment(stream, filename, mimetype));
                    this.Send(mail);


                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Communications, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Email Send
        /// </summary>
        /// <param name="mail">MailMessage</param>
        public void Send(MailMessage mail)
        {
            try
            {
                if (mail != null)
                {
                    //Sitecore.MainUtil.SendMail(mail);
                    //mail.Dispose();

                    SmtpClient smtpClient = new SmtpClient();
                    Diagnostics.Trace(DiagnosticsCategory.Communications, "Email Sending: SMTP Details: " + string.Format("Host: {0}, Port: {1}, DeliveryMethod: {2}, DeliveryFormat: {3}, SSL: {4}", smtpClient.Host, smtpClient.Port, smtpClient.DeliveryMethod, smtpClient.DeliveryFormat, smtpClient.EnableSsl));
                    smtpClient.Send(mail);
                    Diagnostics.Trace(DiagnosticsCategory.Communications, "Email Sending: Mail Sending Successful");
                }
                else
                {
                    Diagnostics.Trace(DiagnosticsCategory.Communications, "Email Sending: Mail Sending Failed");
                    throw new System.ArgumentNullException("mail");
                }

            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Communications, ex, this);
                throw;
            }
        }

        /// <summary>
        /// Email Send 
        /// </summary>
        /// <param name="from">From</param>
        /// <param name="to">To</param>
        /// <param name="subject">Subject</param>       
        /// <param name="isHtml">Is body Html</param>
        public void Send(string from, IList<string> to, string subject, bool isHtml)
        {
            try
            {
                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress(from);
                    if (to != null)
                    {
                        foreach (string toAddress in to)
                        {
                            if (toAddress.Length > 0)
                                mail.To.Add(new MailAddress(toAddress.ToString()));
                        }
                    }
                    mail.Subject = subject;
                    mail.IsBodyHtml = isHtml;

                    this.Send(mail);

                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Communications, ex, this);
                throw;
            }
        }
    }
}
