﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProcessingFeeAwardUi.ascx.cs" Inherits="Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.Ucommerce.Marketing.Awards.ProcessingFeeAwardUi" %>
<table cellpadding="0" cellspacing="0" style="width:100%;">
	<tr>
		<td>
		    <asp:Localize ID="Localize1" runat="server" meta:resourcekey="DiscountOnProcessionFee" />: 
			<asp:PlaceHolder runat="server" id="NonEditModePlaceHolder">
				<asp:Label runat="server" id="ProcessingFeeLabel" />
			</asp:PlaceHolder>

			<asp:PlaceHolder runat="server" id="EditModePlaceHolder" visible="false"  >
				<asp:TextBox runat="server" id="DiscountAmount" /><br/>
				<asp:RegularExpressionValidator id="DecimalValidator" runat="server" ControlToValidate="DiscountAmount" 
								Style="color:Red;" ValidationExpression="^(\d|-)?(\d|,)*\.?\d*$" ValidationGroup="GrpFixPriceText"
								Display="Dynamic" />
				<asp:RequiredFieldValidator id="RequiredValidator" runat="server" ValidationGroup="GrpFixPriceText"
								ControlToValidate="DiscountAmount" Display="Dynamic" />
                <br/>
                <asp:Label runat="server" id="lblOnOrder" Text="Is Percentage" /> : 
                <asp:CheckBox runat="server" id="chkIsPercentage" /><br/>
			</asp:PlaceHolder>
		</td>
		<td style="width:50px; text-align:right; vertical-align:top;">
			<asp:ImageButton id="EditButton" runat="server" imageurl="../../Images/ui/pencil.png" meta:resourcekey="Edit" onclick="EditButton_Click" />
			<asp:ImageButton id="SaveButton" runat="server" imageurl="../../Images/ui/save.gif" meta:resourcekey="Save" visible="false" onclick="SaveButton_Click" ValidationGroup="GrpFixPriceText" />
			<asp:ImageButton id="DeleteButton" runat="server" imageurl="../../Images/ui/cross.png" meta:resourcekey="Delete" onclick="DeleteButton_Click" /><br />
		</td>
	</tr>
</table>
