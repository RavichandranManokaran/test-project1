﻿CREATE PROCEDURE [dbo].[LB_GetClientAndProviderID]
@OrderId INT=0,
@OrderLineId INT=0
AS
BEGIN

 IF @OrderId <> 0 AND @OrderId >0
	BEGIN  
		SET @OrderLineId=(SELECT TOP 1 OrderLineId FROM uCommerce_OrderLine WHERE OrderId=@OrderId order by OrderLineId desc)
	END
    SELECT 
        CP.Value [ClientId],
        PP.SiteCoreItemGuid [ProviderId],
        C.ISOCode [Currency]                                                    
	FROM [dbo].uCommerce_OrderLine OL 
					INNER JOIN [dbo].uCommerce_PurchaseOrder PO ON PO.OrderID =OL.OrderId
					INNER JOIN [dbo].LB_BookingInfo RR on RR.OrderLineId = OL.OrderLineId 
					INNER JOIN [dbo].[uCommerce_Product] P ON OL.Sku = P.Sku 
					INNER JOIN (
							   SELECT ProductId,SiteCoreItemGuid
								FROM 
								(
								  SELECT ProductId,PD.Name [key], PP.value
								  FROM uCommerce_ProductDefinitionField PD 
								  INNER JOIN uCommerce_ProductProperty PP ON PD.ProductDefinitionFieldId=PP.ProductDefinitionFieldId
								  WHERE PD.Name IN ('SiteCoreItemGuid')
								) src
								pivot
								(
								  MAX([value])
								  FOR [key] in (SiteCoreItemGuid)
								) piv
							) PP ON PP.ProductId=P.ProductId AND CONVERT(VARCHAR,PP.SiteCoreItemGuid)!=''
					INNER JOIN  [dbo].LB_CustomerProperty CP ON PO.CustomerId =CP.CustomerId AND CP.[Key]='_ClientId'
					INNER JOIN [dbo].uCommerce_Currency C ON PO.CurrencyId=C.CurrencyId
	WHERE 
		OL.OrderLineId=@OrderLineId                                                         
END  
