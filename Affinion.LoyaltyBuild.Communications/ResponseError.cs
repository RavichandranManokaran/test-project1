﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           ResponseError.cs
/// Sub-system/Module:     Communications(VS project name)
/// Description:           Response Error Class
/// </summary>
#region Using Directives

using System; 

#endregion

namespace Affinion.LoyaltyBuild.Communications
{

    public class ResponseError
    {
        private string owner;

        public string Error { get; set; }
        public string RawResponse { get; set; }
        public ResponseErrorType ErrorType { get; set; }
        public string Owner
        {
            get
            {
                return owner;
            }
            set
            {
                this.ErrorType = String.IsNullOrEmpty(value) ? ResponseErrorType.General : ResponseErrorType.ContactSpecific;
                owner = value;
            }
        }
    }

    //Enum for Error type
    public enum ResponseErrorType
    {
        General,
        ContactSpecific
    }
}
