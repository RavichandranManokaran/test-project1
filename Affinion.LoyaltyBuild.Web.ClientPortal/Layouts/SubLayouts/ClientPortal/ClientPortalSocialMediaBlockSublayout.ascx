﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientPortalSocialMediaBlockSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortalSocialMediaBlockSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Sitecore.Data.Items" %>

<div class="col-sm-4">
    <div class="footer-middle-area">
        <h2 class="no-margin">Follow Us</h2>
        <div class="social-footer">

            <asp:Repeater ID="MediaBlock" runat="server">

                <ItemTemplate>
                    <%--<a href="#" class="float-left "><span class="fa fa-facebook"></span></a>--%>
                    <sc:Link Field="Link" ID="Link" Item="<%#(Item)Container.DataItem %>" runat="server" class="float-left "><span class="<%# GetCSSClasses((Item)Container.DataItem) %>"></span></sc:Link>
                </ItemTemplate>

            </asp:Repeater>
            <%--<a href="#" class="float-left "><span class="fa fa-facebook"></span></a>
            <a href="#" class="float-left "><span class="fa fa-twitter"></span></a>
            <a href="#" class="float-left "><span class="fa fa-google-plus"></span></a>
            <a href="#" class="float-left no-margin"><span class="fa fa-instagram"></span></a>--%>
        </div>
    </div>
</div>
