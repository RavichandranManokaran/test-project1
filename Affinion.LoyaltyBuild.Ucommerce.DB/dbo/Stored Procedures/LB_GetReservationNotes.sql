﻿  CREATE PROCEDURE [dbo].[LB_GetReservationNotes]
	(
		@OrderLineId	INT
	)
AS
BEGIN
		SELECT   R.NotesId
		 ,R.[NoteTypeId]
      ,R.[OrderLineId]
      ,R.[BookingId]
      ,R.[Note]
      ,R.[CreatedDate]
      ,R.[CreatedBy]
      ,R.[SpecialRequest]
	  FROM LB_ReservationNotes R 
	  left join [LB_NoteTypes] N  on N.NoteTypeId = R.NoteTypeId
	  WHERE R.OrderLineId = @OrderLineId
	  Order by R.NoteTypeId , R.CreatedBy 
END