﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           IOfferFilter.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Search (VS project name)
/// Description:           Interface for include offers in client search results page
/// </summary>
namespace Affinion.LoyaltyBuild.Search.OfferFilters
{

    interface IOfferFilter
    {
        /// <summary>
        /// Returns true if the offer is valid and fine to show on client page
        /// </summary>
        /// <param name="filterData">Offer filter data</param>
        /// <returns>Returns true to include the offer</returns>
        bool IncludeOffer(OfferFilterData filterData);
    }
}
