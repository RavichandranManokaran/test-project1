﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientPortalCustomerValidation.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal.ClientPortalCustomerValidation" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>


<div class="row" id="ATGValidation" runat="server">
                <div class="col-sm-12">
                    <a class="btn btn-accent63 width-full col-sm-12" id="validate"><sc:text id="ValidatorText" runat="server" /></a>
                    <div class="email-validate collapsed">
                        <div class="content_bg_inner_box alert-info">
                            <div class="col-sm-12 margin-row">
                                <div class="col-sm-6">
                                    <h3>
                                        <sc:text id="EmailValidationTitleText" field="EmailValidationTitle" runat="server"/>

                                    </h3>
                                </div>
                                <div class="col-sm-6"></div>
                            </div>
                            <div id="FirstDescriptionDiv" class="col-sm-12 margin-row" runat="server">
                                <span>
                                    <sc:text id="FirstDescription" field="FirstDescription" runat="server" />
                                </span>
                            </div>
                            <div id="SecondDescriptionDiv" class="col-sm-12 margin-row" runat="server">
                                <span>
                                    <sc:text id="SecondDescription" field="SecondDescription" runat="server" />
                                </span>
                            </div>
                            <div id="ThirdDescriptionDiv" class="col-sm-12 margin-row" runat="server">
                                <span>
                                    <sc:text id="ThirdDescription" field="ThirdDescription" runat="server" />
                                </span>
                            </div>
                            <div class="col-sm-12 margin-row">
                                <div class="col-sm-4">
                                    <span>
                                        <sc:text id="EmailTitle" field="EmailTitle" runat="server" />
                                    </span>
                                    <span class="accent58-f">*</span>
                                </div>
                                <div class="col-sm-5">
                                    <asp:textbox id="EmailAddress" cssclass="form-control" text="" clientidmode="Static" runat="server" />
                                    <asp:regularexpressionvalidator id="RegularExpressionValidator1" placeholder="Enter Email Address" runat="server" forecolor="Red" controltovalidate="EmailAddress" validationexpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$" display="Dynamic" validationgroup="EmailValidationGroup">
                                        <sc:text id="EmailValidationFieldMessage" runat="server" />
                                    </asp:regularexpressionvalidator>
                                    <asp:requiredfieldvalidator runat="server" forecolor="Red" controltovalidate="EmailAddress" display="Dynamic" validationgroup="EmailValidationGroup">
                                        <sc:Text ID="RequiredFielsMessage1" runat="server" />
                                    </asp:requiredfieldvalidator>
                                </div>
                                <div class="col-sm-3"></div>
                            </div>
                            <div class="col-sm-12 margin-row">
                                <div class="col-sm-4">
                                    <span>
                                        <sc:text id="ConfirmEmailTitle" field="ConfirmEmailTitle" runat="server" />
                                    </span>
                                    <span class="accent58-f">*</span>
                                </div>
                                <div class="col-sm-5">
                                    <asp:textbox id="ConfirmEmailAddress" cssclass="form-control" text="" clientidmode="Static" runat="server" />
                                    <asp:comparevalidator id="CompareValidator1" runat="server" cssclass="error-msg" display="Dynamic" placeholder="Confirm Email Address" controltocompare="EmailAddress" controltovalidate="ConfirmEmailAddress" forecolor="Red" validationgroup="EmailValidationGroup">
                                        <sc:text id="EmailMismatchMessage" runat="server" />
                                    </asp:comparevalidator>
                                    <asp:requiredfieldvalidator id="cnfrmEmailValidator" runat="server" display="Dynamic" controltovalidate="ConfirmEmailAddress" forecolor="Red" validationgroup="EmailValidationGroup">
                                       <sc:Text id="RequiredFielsMessage2" runat="server" />
                                    </asp:requiredfieldvalidator>
                                </div>
                                <div class="col-sm-3"></div>
                            </div>

                            <div id="EmailListConsent1Div" class="col-sm-12 margin-row" runat="server">
                                <div class="col-sm-9">

                                    <sc:text id="EmailList1ConsentLabel" field="EmailList1Consent" runat="server" />
                                </div>
                                <div class="col-sm-2">

                                    <asp:checkbox id="EmailList1ConsentCheckBox" runat="server" />
                                </div>
                            </div>
                            <div id="EmailListConsent2Div" class="col-sm-12 margin-row" runat="server">
                                <div class="col-sm-9">

                                    <sc:text id="EmailList2ConsentLabel" field="EmailList2Consent" runat="server" />
                                </div>
                                <div class="col-sm-2">
                                    <asp:checkbox id="EmailList2ConsentCheckBox" runat="server" />
                                </div>
                            </div>
                            <div class="col-sm-12 margin-row">
                                <div class="col-sm-9">
                                    <span>
                                        <sc:text id="InstructionText" field="InstructionText" runat="server" />
                                    </span>
                                </div>
                                <div class="col-sm-2">
                                </div>
                            </div>
                            <div class="col-sm-12 margin-row">
                                <div class="col-sm-4">
                                    <asp:label class="accent58-f" id="Label1" runat="server" />
                                    <br />
                                    <asp:label class="accent58-f" id="Label2" runat="server" />
                                </div>
                                <div class="col-sm-5">
                                </div>
                                <div class="col-sm-3">
                                    <span class="accent58-f">*</span>
                                    <span>
                                        <sc:text id="RequiredFieldIndicator" field="RequiredFieldIndicatorText" runat="server" />
                                    </span>
                                </div>

                            </div>
                            <div class="col-sm-12 margin-row">
                                <div class="col-sm-9">
                                </div>
                                <div class="col-sm-3">
                                    <asp:button cssclass="btn btn-default add-break pull-right sec-payment" id="ContinueToDetailButton" runat="server" text="" onclick="ContinueToDetailButton_Click" validationgroup="EmailValidationGroup"/>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>