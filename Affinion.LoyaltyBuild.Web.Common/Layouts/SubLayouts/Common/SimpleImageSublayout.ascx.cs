﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SimpleImageSublayout.ascx.cs
/// Sub-system/Module:     Web.Common(VS project name)
/// Description:           Sublayout for simple image template
/// </summary>
namespace Affinion.LoyaltyBuild.Web.Common.Layouts.SubLayouts.Common
{
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Sitecore.Data.Items;
    using System;

    /// <summary>
    /// Sublayout for simple image template
    /// </summary>
    public partial class SimpleImageSublayout : BaseSublayout
    {
        /// <summary>
        /// This function includes the code which needs to run on page load
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The parameter</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Item item = this.GetDataSource();

                if (item != null)
                {
                    this.PlaceholderText.Visible = false;
                    this.Image.Item = item;
                    this.Link.Item = item;
                }
                else
                {
                    this.Image.Visible = this.Link.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, ex.Message);
                throw;
            }
        }
    }
}