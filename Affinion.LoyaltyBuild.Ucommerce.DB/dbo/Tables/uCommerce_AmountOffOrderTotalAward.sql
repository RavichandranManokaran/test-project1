﻿CREATE TABLE [dbo].[uCommerce_AmountOffOrderTotalAward] (
    [AmountOffOrderTotalAwardId] INT             NOT NULL,
    [AmountOff]                  DECIMAL (18, 2) NOT NULL,
    CONSTRAINT [PK_uCommerce_AmountOffOrderTotalAward_1] PRIMARY KEY CLUSTERED ([AmountOffOrderTotalAwardId] ASC),
    CONSTRAINT [FK_uCommerce_AmountOffOrderTotalAward_uCommerce_Award] FOREIGN KEY ([AmountOffOrderTotalAwardId]) REFERENCES [dbo].[uCommerce_Award] ([AwardId])
);

