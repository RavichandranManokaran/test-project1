﻿using Affinion.LoyaltyBuild.BedBanks.General;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Model.Auth;

namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Message.Interfaces
{
    public interface IJtRequest
    {
        JtLoginDetails LoginDetails { get; set; }

        IJtResponse Fire();

    }
}
