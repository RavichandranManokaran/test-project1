﻿using Affinion.LoyaltyBuild.Model.Product;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Model.Provider
{
    /// <summary>
    /// Hotel model
    /// </summary>
    public class Hotel : IProvider, ISitecoreProvider, IGenericProvider
    {
        /// <summary>
        /// Provider Id
        /// </summary>
        public string ProviderId { get; set; }

        /// <summary>
        /// SitecoreItemId
        /// </summary>
        public string SitecoreItemId { get; set; }
        
        /// <summary>
        /// check in time
        /// </summary>
        public string CheckInTime { get; set; }
        
        /// <summary>
        /// check out time
        /// </summary>
        public string CheckOutTime { get; set; }

        /// <summary>
        /// provider type
        /// </summary>
        public ProviderType ProviderType { get; set; }

        /// <summary>
        /// IntroductionImage
        /// </summary>
        public string IntroductionImage { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Town
        /// </summary>
        public string Town { get; set; }

        /// <summary>
        /// Overview
        /// </summary>
        public string Overview { get; set; }

        /// <summary>
        /// Star Ranking
        /// </summary>
        public int StarRanking { get; set; }

        /// <summary>
        /// Sitecore item for that provider
        /// </summary>
        public Item SitecoreItem { get; set; }

        /// <summary>
        /// Facilities
        /// </summary>
        public List<Item> Facilities { get; set; }

        /// <summary>
        /// Themes
        /// </summary>
        public string Themes { get; set; }

        // <summary>
        /// Experiences
        /// </summary>
        public string Experiences { get; set; }

        /// <summary>
        /// Address
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Address
        /// </summary>
        public string AddressLine1 { get; set; }

        /// <summary>
        /// Address
        /// </summary>
        public string AddressLine2 { get; set; }

        /// <summary>
        /// Address
        /// </summary>
        public string AddressLine3 { get; set; }

        /// <summary>
        /// Address
        /// </summary>
        public string AddressLine4 { get; set; }

        /// <summary>
        /// Address
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// UcommerceId
        /// </summary>
        public string UCommerceId { get; set; }

        /// <summary>
        /// Country
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Local ranking
        /// </summary>
        public string LocalRanking { get; set; }

        /// <summary>
        /// Lowest Price
        /// </summary>
        public string LowestPrice { get; set; }

        /// <summary>
        /// CurrencyId
        /// </summary>
        public int CurrencyID { get; set; }

        /// <summary>
        /// List of products for that provider
        /// </summary>
        public IList<IProduct> Products { get; set; }

        /// <summary>
        /// Loyalty hotel type
        /// </summary>
        public string HotelType { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Address
        /// </summary>
        public string Phone { get; set; }
    }
}
