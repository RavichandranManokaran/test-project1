﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           Security.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Code is used to encrypt and decrypt text.
/// </summary>

#region Using Directives
using Affinion.LoyaltyBuild.Common.Instrumentation;
using System;
using System.Security.Cryptography;
using System.Text;
#endregion

namespace Affinion.LoyaltyBuild.Common.Utilities
{
    public static class Cryptography
    {
        private const string cryptoKey = "cryptoKey";

        // The Initialization Vector for the DES encryption routine
        private static readonly byte[] IV =
            new byte[8] { 240, 3, 45, 29, 0, 76, 173, 59 };

        #region Public Methods

        /// <summary>
        /// Encrypts provided string parameter
        /// </summary>
        /// <param name="inputForEncryption">string to encrypt</param>
        /// <returns>encrypted string</returns>
        public static string Encrypt(string inputForEncryption)
        {
            try
            {
                //declare valiable to assign the return value
                string encryptedResult = string.Empty;
                //Check for null or empty string input
                if (String.IsNullOrEmpty(inputForEncryption))
                {
                    return encryptedResult;
                }

                //This will add trace information to the log for starting encryption process
                Diagnostics.Trace(DiagnosticsCategory.Common, "Encryption Started.");

                byte[] buffer = Encoding.ASCII.GetBytes(inputForEncryption);

                using (TripleDESCryptoServiceProvider des =
                    new TripleDESCryptoServiceProvider())

                using (MD5CryptoServiceProvider MD5 =
                new MD5CryptoServiceProvider())
                {
                    des.Key =
                   MD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(cryptoKey));

                    des.IV = IV;
                    //Convert the result to base 64 string
                    encryptedResult = Convert.ToBase64String(
                        des.CreateEncryptor().TransformFinalBlock(
                            buffer, 0, buffer.Length));

                    //This will add trace information to the log for ending encryption process
                    Diagnostics.Trace(DiagnosticsCategory.Common, "Encryption Ended.");

                    return encryptedResult;
                }
            }
            catch (Exception ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, typeof(Cryptography));
                throw;
            }
        }

        /// <summary>
        /// Decrypts provided string parameter
        /// </summary>
        /// <param name="inputForDecryption">string to decrypt</param>
        /// <returns>decrypted string</returns>
        public static string Decrypt(string inputForDecryption)
        {
            try
            {
                //declare valiable to assign the return value
                string DecryptedResult = string.Empty;
                //Check for null or empty string input
                if (String.IsNullOrEmpty(inputForDecryption))
                {
                    return DecryptedResult;
                }

                //This will add trace information to the log for starting decryption process
                Diagnostics.Trace(DiagnosticsCategory.Common, "Decryption Started.");

                byte[] buffer = Convert.FromBase64String(inputForDecryption);

                using (TripleDESCryptoServiceProvider des =
                    new TripleDESCryptoServiceProvider())
                {
                    using (MD5CryptoServiceProvider MD5 =
                     new MD5CryptoServiceProvider())
                    {
                        des.Key =
                         MD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(cryptoKey));

                        des.IV = IV;

                        DecryptedResult = Encoding.ASCII.GetString(
                            des.CreateDecryptor().TransformFinalBlock(
                            buffer, 0, buffer.Length));

                        //This will add trace information to the log for ending decryption process
                        Diagnostics.Trace(DiagnosticsCategory.Common, "Decryption Ended.");

                        return DecryptedResult;
                    }
                }
            }
            catch (Exception ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, typeof(Cryptography));
                throw;
            }
        }

        /// <summary>
        /// Create Md5 hash for given value
        /// </summary>
        /// <param name="md5Hash">Md5 hash object</param>
        /// <param name="value">Input value</param>
        /// <returns>Md5 hash string</returns>
        public static string GetMd5Hash(string value)
        {

            try
            {
                using (MD5 md5Hash = MD5.Create())
                {

                    // Convert the input string to a byte array and compute the hash.
                    byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(value));

                    // Create a new Stringbuilder to collect the bytes
                    // and create a string.
                    StringBuilder stringBuilder = new StringBuilder();

                    // Loop through each byte of the hashed data 
                    // and format each one as a hexadecimal string.
                    for (int i = 0; i < data.Length; i++)
                    {
                        stringBuilder.Append(data[i].ToString("x2"));
                    }

                    // Return the hexadecimal string.
                    return stringBuilder.ToString();
                }
            }
            catch (Exception ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, typeof(Cryptography));
                throw;
            }
        }

        /// <summary>
        /// Verify a hash against a string.
        /// </summary>
        /// <param name="md5Hash">Md5 hash object</param>
        /// <param name="value">Input value</param>
        /// <param name="hash">Hashed value</param>
        /// <returns>True if given value is equels to given hash otherwise false</returns>
        public static bool VerifyMd5Hash(string value, string hash)
        {
            try
            {
                // Hash the input.
                string hashOfInput = GetMd5Hash(value);

                // Create a StringComparer an compare the hashes.
                StringComparer comparer = StringComparer.OrdinalIgnoreCase;

                if (0 == comparer.Compare(hashOfInput, hash))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, typeof(Cryptography));
                throw;
            }
        }

        /// <summary>
        /// Generate a salt value
        /// </summary>
        /// <returns>Salt string</returns>
        public static string GenerateSalt()
        {
            try
            {
                var buf = new byte[16];
                (new RNGCryptoServiceProvider()).GetBytes(buf);
                return Convert.ToBase64String(buf);
            }
            catch (Exception ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, typeof(Cryptography));
                throw;
            }
        }

        /// <summary>
        /// Generate SHA1 salted hash
        /// </summary>
        /// <param name="value">Input value</param>
        /// <param name="salt">Salt value</param>
        /// <returns>Hashed string</returns>
        public static string GetHasedValue(string value, string salt)
        {
            try
            {
                byte[] bytes = Encoding.Unicode.GetBytes(value);
                byte[] src = Convert.FromBase64String(salt);
                byte[] dst = new byte[src.Length + bytes.Length];
                byte[] inArray = null;
                Buffer.BlockCopy(src, 0, dst, 0, src.Length);
                Buffer.BlockCopy(bytes, 0, dst, src.Length, bytes.Length);

                using (HashAlgorithm algorithm = HashAlgorithm.Create("SHA1"))
                {
                    inArray = algorithm.ComputeHash(dst);
                }
                
                return Convert.ToBase64String(inArray);
            }
            catch (Exception ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, typeof(Cryptography));
                throw;
            }
        }

        /// <summary>
        /// Verify a given value is equels to given salted hash
        /// </summary>
        /// <param name="value">Input value</param>
        /// <param name="hashedValue">Hashed value</param>
        /// <param name="salt">slat value of the hased value</param>
        /// <returns>True if given value valid otherwise false</returns>
        public static bool VerifySaltedHash(string value, string hashedValue, string salt)
        {
            try
            {
                // Hash the value.
                string hashOfValue = GetHasedValue(value, salt);

                // Create a StringComparer an compare the hashes.
                StringComparer comparer = StringComparer.Ordinal;

                return 0 == comparer.Compare(hashOfValue, hashedValue);
            }
            catch (Exception ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, typeof(Cryptography));
                throw;
            }
        }
        #endregion
    }
}