﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           BasePrimaryLayout.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Search (VS project name)
/// Description:           This field will generate and store the readable notation to indicate the offers assignment by client
/// </summary>
namespace Affinion.LoyaltyBuild.Search.ComputedFields
{
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Search.Data;
    using Affinion.LoyaltyBuild.Search.Helpers;
    using Newtonsoft.Json;
    using Sitecore.ContentSearch;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;

    /// <summary>
    /// This field will generate and store the readable notation to indicate the offers assignment by client
    /// </summary>
    public class ClientOfferAssignmentField : ComputedField
    {
        /// <summary>
        /// Calculates the field value
        /// </summary>
        /// <param name="indexable">The indexable item</param>
        /// <returns>Returns the calculated value</returns>
        public override object ComputeFieldValue(IIndexable indexable)
        {
            try
            {

                //var clientsList = SitecoreFieldsHelper.GetMutiListItems(supplier, "Clients");
                Item supplier = ContentDatabase.GetItem(ID.Parse(indexable.Id));
                Collection<SupplierData> offersDataSet = new Collection<SupplierData>();
                List<Item> clientsList = new List<Item>();
                Item clientsFolder = ContentDatabase.GetItemByKey("ClientSetupFolder");

                if (clientsFolder != null)
                {
                    clientsList = clientsFolder.Axes.GetDescendants().Where(i => i.TemplateName == "ClientDetails").ToList<Item>();
                }

                foreach (Item client in clientsList)
                {
                    bool isSupplierSelected = SitecoreFieldsHelper.MultiListContains(client, "AllowedSuppliers", supplier);
                    SupplierData offersList = new SupplierData();

                    if (isSupplierSelected)
                    {
                        offersList = StoreHelper.GetProducts(supplier, client);
                    }

                    offersDataSet.Add(offersList);
                }

                return JsonConvert.SerializeObject(offersDataSet);
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                return string.Empty;
            }
        }
    }
}
