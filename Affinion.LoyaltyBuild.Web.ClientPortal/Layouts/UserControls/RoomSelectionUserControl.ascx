﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RoomSelectionUserControl.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.UserControls.RoomSelectionUserControl" %>

<span class="accent50-f font-4 font-bold-600 col-md-12 roomOn" runat="server" id="RoomNumber"></span>
<div class="col-xs-12 col-md-6 roomOn">
    <h5 class="roomOn">
        <sc:Text Field="AdultsLabel" ID="AdultsLabel" runat="server" />
    </h5>
    <div role="group" aria-label="...">
        <asp:DropDownList CssClass="form-control roomOn" ID="NoOfAdults" ClientIDMode="Static" runat="server"></asp:DropDownList>
    </div>

</div>
<div class="col-xs-12 col-md-6 roomOn">
    <h5>
        <sc:Text Field="ChildrenLabel" ID="ChildrenLabel" runat="server" />
    </h5>
    <div role="group" aria-label="...">
        <asp:DropDownList CssClass="form-control bedSelectionChildCount" ID="NoOfChildrenDropDownList" ClientIDMode="Static" runat="server"></asp:DropDownList>
    </div>

</div>
<%--<div class="col-xs-12 no-padding ageGroupOuter col-md-6" style="display: none">--%>

    <%--<div class="col-xs-12"><span>Ages of children at check-out</span></div>--%>
    <asp:Panel ID="ChildAgePanel" runat="server">
    </asp:Panel>


<%--</div>--%>
