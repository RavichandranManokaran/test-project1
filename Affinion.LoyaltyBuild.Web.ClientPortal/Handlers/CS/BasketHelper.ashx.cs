﻿using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.UCom.Common.Constants;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Rules;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using helper = Affinion.Loyaltybuild.BusinessLogic.Helper;
using api = Affinion.LoyaltyBuild.Api.Booking.Helper;
using System.Text;
using Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi;
using Affinion.LoyaltyBuild.UCom.Common.Extension;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Availability;
using Affinion.LoyaltyBuild.Model.Common;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Handlers.CS
{
    /// <summary>
    /// Summary description for BasketHelper
    /// </summary>
    public class BasketHelper : IHttpHandler
    {
        private NameValueCollection _query = null;

        /// <summary>
        /// Process request method
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context)
        {
            _query = HttpUtility.ParseQueryString(context.Request.Url.Query.ToLower());

            switch (_query["action"])
            {
                case "calculatepayment":
                    CalculatePayment(context);
                    break;
                case "partialsettings":
                    GetPartialPaymentDetails(context);
                    break;
                case "removeitem":
                    RemoveItem(context);
                    break;
                case "checkavailability":
                    CheckRoomAvailabilty(context);
                    break;
                case "reset":
                    ResetBasket(context);
                    break;
            }

            context.Response.ContentType = "text/json";
        }

        /// <summary>
        /// Calculate payment
        /// </summary>
        /// <param name="context"></param>
        private void CalculatePayment(HttpContext context)
        {
            if (UCommerce.Api.TransactionLibrary.HasBasket())
            {
                var method = context.Request.QueryString["method"];
                var type = context.Request.QueryString["type"];

                api.BasketHelper.SetPaymentMethod(type, method);
                var basket = api.BasketHelper.GetBasket();

                var serializer = new JavaScriptSerializer();
                context.Response.Write(serializer.Serialize(
                    new
                    {
                        TotalPayableToday = CurrencyHelper.FormatCurrency(basket.TotalPayableToday, basket.CurrencyId),
                        TotalPayableAtAccommodation = CurrencyHelper.FormatCurrency(basket.TotalPayableAccomodation, basket.CurrencyId),
                        TotalPrice = CurrencyHelper.FormatCurrency(basket.TotalAmount, basket.CurrencyId),
                        ProcessingFee = CurrencyHelper.FormatCurrency(basket.ProcessingFee, basket.CurrencyId)
                    }));
            }
        }

        /// <summary>
        /// Remove item
        /// </summary>
        /// <param name="context"></param>
        private void RemoveItem(HttpContext context)
        {
            if (UCommerce.Api.TransactionLibrary.HasBasket())
            {
                var order = UCommerce.Api.TransactionLibrary.GetBasket(false).PurchaseOrder;
                var orderLineId = 0;
                int.TryParse(_query["olid"], out orderLineId);
                var isRemoved = api.BasketHelper.RemoveBasketItem(orderLineId);

                var serializer = new JavaScriptSerializer();
                context.Response.Write(serializer.Serialize(new { Success = isRemoved }));
            }
        }

        /// <summary>
        /// Get partial payment details
        /// </summary>
        /// <param name="context"></param>
        private void GetPartialPaymentDetails(HttpContext context)
        {
            if (UCommerce.Api.TransactionLibrary.HasBasket())
            {
                var order = UCommerce.Api.TransactionLibrary.GetBasket(false).PurchaseOrder;
                var clientId = helper.BasketHelper.GetOrderClientId(order.OrderId);

                var service = new SitecorePaymentService();
                var result = service.GetRulePaymentMethod(clientId, order.OrderId, Constants.PortalType.Online, null, null);

                var serializer = new JavaScriptSerializer();

                if (result.Count(i => i.Paymentmethod == Constants.PaymentMethod.PartialPayment) == result.Count)
                {
                    var orderLine = order.OrderLines.FirstOrDefault();
                    if (orderLine != null)
                    {
                        string daysBefore = orderLine[OrderPropertyConstants.DaysBefore];
                        string percent = orderLine[OrderPropertyConstants.PercentageOfAmount];

                        var data = new
                        {
                            IsPartial = true,
                            DefaultPayment = Constants.PaymentMethod.FullPayment,
                            AccomodationPaymentText = string.Format("Total Payable {0} Days Prior to Arrival:", daysBefore),
                            PartialPaymentText = string.Format("Approximately {0} % payable total with the balance due {1} days prior to arrival", percent, daysBefore)
                        };

                        context.Response.Write(serializer.Serialize(data));
                        return;
                    }
                }

                context.Response.Write(serializer.Serialize(new { IsPartial = false }));
            }
        }

        /// <summary>
        /// Get partial payment details
        /// </summary>
        /// <param name="context"></param>
        private void CheckRoomAvailabilty(HttpContext context)
        {
            if (UCommerce.Api.TransactionLibrary.HasBasket())
            {
                var basket = Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.GetBasket();
                List<string> lstVariantSku = new List<string>();

                bool availFlag = false;
                List<string> invalidBookings = new List<string>();

                foreach (var booking in basket.Bookings)
                {
                    if (booking.Type != Model.Product.ProductType.BedBanks)
                    {
                        lstVariantSku.Add(booking.OrderLine.VariantSku);
                        var availability = LoyaltyBuildCatalogLibrary.GetRoomAvailabilityWithPrice(lstVariantSku, booking.CheckinDate.ConvetDatetoDateCounter(), booking.CheckoutDate.ConvetDatetoDateCounter());
                        foreach (var availItem in availability)
                        {
                            if (availItem.AvailableRoom == 0)
                            {
                                availFlag = true;
                                invalidBookings.Add(booking.OrderLineId.ToString());
                                break;
                            }
                        }
                    }
                }

                var serializer = new JavaScriptSerializer();
                context.Response.Write(serializer.Serialize(
                new ValidationResult
                {
                    IsSuccess = !availFlag,
                    Id = string.Join(",", invalidBookings),
                    Message = availFlag ? "Some basket items not available" : string.Empty
                }));
            }
        }

        /// <summary>
        /// Reset Basket
        /// </summary>
        private void ResetBasket(HttpContext context)
        {
            var ClearFlag = false;
            if (Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.BasketCount > 0)
            {
                Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.ClearBasket();
                ClearFlag = true;
            }

            var serializer = new JavaScriptSerializer();
            context.Response.Write(serializer.Serialize(
            new ValidationResult
            {
                IsSuccess = ClearFlag,
            }));
        }

        /// <summary>
        /// is reusable flag
        /// </summary>
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}