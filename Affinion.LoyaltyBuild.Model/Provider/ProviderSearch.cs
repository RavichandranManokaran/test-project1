﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Model.Provider
{
    /// <summary>
    /// Provider search
    /// </summary>
    public class ProviderSearch : IBaseProvider
    {
        /// <summary>
        /// Provider Id
        /// </summary>
        public string ProviderId { get; set; }

        /// <summary>
        /// SitecoreItemId
        /// </summary>
        public string SitecoreItemId { get; set; }

        /// <summary>
        /// provider type
        /// </summary>
        public ProviderType ProviderType { get; set; }
    }
}
