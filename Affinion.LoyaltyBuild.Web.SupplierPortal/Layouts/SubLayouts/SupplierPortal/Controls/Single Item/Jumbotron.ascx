﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Jumbotron.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Single_Item.Jumbotron" %>

<div class="jumbotron">
 <div class="container">
  <h1><sc:FieldRenderer ID="FieldRenderer1" runat="server" FieldName="Title" /></h1>
  <sc:FieldRenderer ID="frBody" runat="server" FieldName="Body" />
  <p><asp:HyperLink ID="calltoaction" runat="server" class="btn btn-primary btn-lg" /></p>
 </div>
</div>
