﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SuperValuHomeSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.SuperValuHomeSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>


<div class="width-full conatiner-area">
    <div class="container">
        <div class="row container-top">

            <!-- Example row of columns -->
            <div class="col-sm-8 no-padding">
                
                <%--Promo box goes here--%>
                <sc:Placeholder ID="packagepreview" Key="packagepreview" runat="server" />

            </div>

            <div class="col-sm-4 no-padding">
                <div class="col-md-12 col-xs-12">

                    <%--Promo box goes here--%>
                    <sc:Placeholder ID="promobox" Key="promobox" runat="server" />

                </div>
            </div>

        </div>
        <div class="row">
            
            <%--Promo banner goes here--%>
            <sc:Placeholder ID="promobanner" Key="promobanner" runat="server" />

        </div>

    </div>
</div>
