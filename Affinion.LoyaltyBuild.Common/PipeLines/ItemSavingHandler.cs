﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           ItemSavingHandler.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Handler to read saving items and do operations (This class will not require if using event handlers)
/// </summary>

#region Using Directives
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Pipelines.Save;
using Sitecore.SecurityModel;
using System;
#endregion

namespace Affinion.LoyaltyBuild.Common.PipeLines
{
    public class ItemSavingHandler
    {
        #region Private Constants
        /// <summary>
        /// Name of the Secure template for sitecore configuration
        /// </summary>
        private const string SecureTemplateName = "SecureConfiguration";

        /// <summary>
        /// Field name of the sitecore item which stores configuration
        /// </summary>
        private const string ConfigurationFieldName = "Value";
        #endregion

        #region Public Methods
        /// <summary>
        /// Pipeline method to run when saving a item
        /// </summary>
        /// <param name="args">Saving Arguments</param>
        public void Process(SaveArgs args)
        {
            try
            {
                if (args.IsPostBack)
                {
                    if (args.Result.Equals("no") || args.Result.Equals("undefined"))
                    {
                        args.AbortPipeline();
                    }
                    return;
                }

                /// for each of the items the user attempts to save
                foreach (Sitecore.Pipelines.Save.SaveArgs.SaveItem savingItem in args.Items)
                {
                    ///Read item by item
                    Item item = Sitecore.Client.ContentDatabase.GetItem(savingItem.ID);
                    ///Change the icon according to active status
                    ItemHelper.ChangeIconField(item, null, "IsActive");
                    ///Activate supplier type when updating any item from two locations of supplier type
                    ActivateSupplierType(item, "{3E85F816-1D87-4FFA-8261-820C92266305}", "IsActive");
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                // TODO: Handle in a general way(error page or a error label in layout)
                throw;
            }
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Set active status according to change of any of the supplier type change
        /// </summary>
        /// <param name="currentItem">Currently saving item</param>
        /// <param name="supplierTypeTemplateID">Supplier Type template ID</param>
        /// <param name="fieldName">Active field name</param>
        private static void ActivateSupplierType(Item currentItem, string supplierTypeTemplateID, string fieldName)
        {
            if (string.IsNullOrEmpty(supplierTypeTemplateID) || !string.Equals(currentItem.TemplateID.ToString(), supplierTypeTemplateID))
            {
                return;
            }
            ///Get the new value of the IsActive Field
            string fieldValue = currentItem.Fields[fieldName].Value;
            ///Two locations of supplier types
            ///Path of the supplier type created online
            string supplierTypePathCreatedList = string.Format("/sitecore/content/admin-portal/supplier-setup/{0}", currentItem.Name);
            ///Path of the supplier type in master list
            string supplierTypePathMasterList = string.Format("/sitecore/content/admin-portal/supplier-setup/global/_supporting-content/supplier-types/{0}", currentItem.Name);
            ///Created supplier type item
            Item matchingSupplierTypeCreated = Sitecore.Configuration.Factory.GetDatabase("master").GetItem(supplierTypePathCreatedList);
            ///Supplier type of the master list
            Item matchingSupplierTypeMaster = Sitecore.Configuration.Factory.GetDatabase("master").GetItem(supplierTypePathMasterList);

            if (matchingSupplierTypeCreated != null)
            {
                ///Update supplier type IsActive field
                ItemHelper.UpdateFieldValue(matchingSupplierTypeCreated, fieldName, fieldValue);
                ///Update supplier type icon
                ItemHelper.ChangeIconField(matchingSupplierTypeCreated, null, fieldName); 
            }

            if (matchingSupplierTypeMaster != null)
            {
                ///Update supplier type IsActive field
                ItemHelper.UpdateFieldValue(matchingSupplierTypeMaster, fieldName, fieldValue);
                ///Update supplier type icon
                ItemHelper.ChangeIconField(matchingSupplierTypeMaster, null, fieldName); 
            }
        }

        #endregion
    }
}
