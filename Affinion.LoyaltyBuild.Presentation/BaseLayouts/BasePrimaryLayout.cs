﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           BasePrimaryLayout.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common (VS project name)
/// Description:           Base primary layout with common methods and attributes
/// </summary>
namespace Affinion.LoyaltyBuild.Presentation.BaseLayouts
{
    #region Using Statements

    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Communications.ExceptionServices;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using Sitecore.Web.UI.WebControls;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Text;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;

    #endregion

    /// <summary>
    /// Base primary layout with common methods and attributes
    /// </summary>
    public abstract class BasePrimaryLayout : Page
    {
        ExceptionMailHelper exceptionObject = new ExceptionMailHelper();
        #region Properties

        /// <summary>
        /// Gets or sets the last exception
        /// </summary>
        public Exception LastException { get; set; }

        /// <summary>
        /// Gets the main form
        /// </summary>
        public abstract HtmlForm MainForm { get; }

        /// <summary>
        /// Gets or sets the global javascript variables which may have to load values from back end
        /// </summary>
        public Dictionary<string, object> JavascriptVariables { get; set; }

        #endregion

        #region Public Methods

        /// <summary>
        /// Intialize the base primary layout
        /// </summary>
        public BasePrimaryLayout()
        {
            this.JavascriptVariables = new Dictionary<string, object>();
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Execute code on preload event
        /// This method will call the preload function on sublayouts which is attached to 2 levels deep in placeholders
        /// </summary>
        /// <param name="e">The argument</param>
        protected override void OnPreLoad(EventArgs e)
        {
            base.OnPreLoad(e);

            if (this.MainForm == null)
            {
                return;
            }

            foreach (Placeholder placeholderLevelOne in this.GetPlaceholders(this.MainForm.Controls))
            {
                foreach (Control layoutLevelOne in this.GetSubLayouts(placeholderLevelOne.Controls))
                {
                    BaseSublayout BaseLayoutLevelOne = layoutLevelOne as BaseSublayout;
                    if (BaseLayoutLevelOne != null)
                    {
                        BaseLayoutLevelOne.PreLoad(e);
                    }

                    ControlCollection childCollection = layoutLevelOne.Controls;
                    if (layoutLevelOne is Sublayout && layoutLevelOne.Controls.Count > 0)
                    {
                        childCollection = layoutLevelOne.Controls[0].Controls;
                    }

                    foreach (Placeholder placeholderLevelTwo in this.GetPlaceholders(childCollection))
                    {
                        foreach (Control layoutLevelTwo in this.GetSubLayouts(placeholderLevelTwo.Controls))
                        {
                            BaseSublayout BaseLayoutLevelTwo = layoutLevelTwo as BaseSublayout;
                            if (BaseLayoutLevelTwo != null)
                            {
                                BaseLayoutLevelTwo.PreLoad(e);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Render Javascript variables
        /// </summary>
        /// <returns>Returns a list of variables as a Javascript</returns>
        protected virtual string RenderJavascriptVariables()
        {
            StringBuilder variables = new StringBuilder();

            if (this.JavascriptVariables.Keys.Count > 0)
            {
                variables.AppendLine();
                variables.AppendLine("<script type=\"text/javascript\">");
                variables.AppendLine();
            }

            foreach (string key in this.JavascriptVariables.Keys)
            {
                object value = this.JavascriptVariables[key];

                variables.Append("var ");
                variables.Append(key);
                variables.Append(" = ");

                if (value is ValueType)
                {
                    variables.Append("\"");
                    variables.Append(value);
                    variables.Append("\"");
                }
                else
                {
                    variables.Append(Newtonsoft.Json.JsonConvert.SerializeObject(value));
                }

                variables.AppendLine(";");
            }

            if (this.JavascriptVariables.Keys.Count > 0)
            {
                variables.AppendLine();
                variables.AppendLine("</script>");
            }

            return variables.ToString();
        }

        /// <summary>
        /// Gets the error page
        /// </summary>
        /// <param name="exception">The generated exception</param>
        /// <returns>returns the error page url as a string</returns>
        protected virtual string GetErrorPage(Exception exception)
        {
            return "Pages/GenericErrorPage.aspx";
        }

        /// <summary>
        /// Captures the error in page level
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The parameters</param>
        protected void Page_Error(object sender, EventArgs e)
        {
            // Get last error from the server
            Exception exception = Server.GetLastError();
            this.LastException = exception;
            string errorPage = this.GetErrorPage(exception);

            //Database currentDb = Sitecore.Context.Database;
            //Item rootItem = Sitecore.Context.Database.GetItem(Sitecore.Context.Site.RootPath);
            //string rootItemPath = rootItem.Paths.FullPath;

            if (Sitecore.Context.Site.RootPath.Contains("client-portal"))
            {
                //send email to user if user is authenticated and email provided
                string userEmail = Sitecore.Context.User.Profile.Email;
                HttpCookie emailCookie = Request.Cookies["AffinionEmailCookie"];
                if (!Sitecore.Context.User.IsAuthenticated && string.IsNullOrEmpty(userEmail) && emailCookie != null && !string.IsNullOrEmpty(emailCookie.Value))
                {
                    userEmail = emailCookie.Value;
                }

                Diagnostics.Trace(DiagnosticsCategory.Communications, "Starting if condition");
                if (!string.IsNullOrEmpty(userEmail))
                {
                    Diagnostics.Trace(DiagnosticsCategory.Communications, "Before sending email");
                    exceptionObject.SendExceptionEmail(exception.Message, "clientexceptionnotificationemail", Sitecore.Context.User.Profile.FullName, userEmail);
                    Diagnostics.Trace(DiagnosticsCategory.Communications, "After sending email");
                }

                //send genaral fatal error email to IT team
                int orderId = Affinion.Loyaltybuild.BusinessLogic.Helper.BasketHelper.GetBasketOrderId();
                exceptionObject.SendExceptionEmail(exception.ToString(), orderId);
            }

            // Handle error pages
            if (!string.IsNullOrEmpty(errorPage))
            {
                Server.Transfer(errorPage, true);
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Get placeholders from a control collection
        /// </summary>
        /// <param name="controls">controls collection</param>
        /// <returns>Returns a list of placeholders</returns>
        private IEnumerable<Placeholder> GetPlaceholders(ControlCollection controls)
        {
            Collection<Placeholder> placeholders = new Collection<Placeholder>();

            foreach (Control control in controls)
            {
                Placeholder placeholder = control as Placeholder;

                if (placeholder != null)
                {
                    placeholders.Add(placeholder);
                }
            }

            return placeholders;
        }

        /// <summary>
        /// Get list of base sublayouts
        /// </summary>
        /// <param name="controls">control collection</param>
        /// <returns>Returns a list of base sublayouts</returns>
        private IEnumerable<Control> GetSubLayouts(ControlCollection controls)
        {
            Collection<Control> layouts = new Collection<Control>();

            foreach (Control control in controls)
            {
                Sublayout sublayout = control as Sublayout;
                if (sublayout != null)
                {
                    layouts.Add(sublayout.GetUserControl());
                }
            }

            return layouts;
        }

        #endregion
    }
}
