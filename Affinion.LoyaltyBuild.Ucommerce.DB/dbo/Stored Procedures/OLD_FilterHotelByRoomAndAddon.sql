﻿--exec FilterHotelByRoomAndAddon 'Single','Break fast'
CREATE PROCEDURE [dbo].[OLD_FilterHotelByRoomAndAddon]
@roomtype nvarchar(50)=NULL,
@addontype nvarchar(50)=NULL 
As
select distinct c.* from uCommerce_Category c
inner join uCommerce_CategoryProductRelation cp on  c.CategoryId = cp.CategoryId
inner join uCommerce_Product p on  cp.ProductId = p.ProductId
inner join uCommerce_ProductDefinition pd on p.ProductDefinitionId = pd.ProductDefinitionId
inner join uCommerce_ProductProperty pp on p.ProductId = pp.productid
inner join uCommerce_ProductDefinitionField pdf on pp.ProductDefinitionFieldId = pdf.ProductDefinitionFieldId 
where (pd.Name = 'HotelRoom' and pd.Deleted = 0 and pdf.Deleted = 0 and pdf.name='RoomType' and pp.Value=@roomtype)
or (pd.Name = 'Add On' and pd.Deleted = 0 and pdf.Deleted = 0 and pdf.name='ProductType' and pp.Value=@addontype)

