﻿<%@ page language="C#" autoeventwireup="true" codebehind="SetAccommodation.aspx.cs" inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SetAccommodation" %>

<%@ register tagprefix="sc" namespace="Sitecore.Web.UI.WebControls" assembly="Sitecore.Kernel" %>
<%@ outputcache location="None" varybyparam="none" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script type="text/javascript" src="/Resources/scripts/jquery.min.js"></script>
    <script type="text/javascript" src="/Resources/scripts/jquery-ui.js"></script>
    <script type="text/javascript" src="/Resources/bootstrap_assets/javascripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="/Resources/scripts/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="../Resources/scripts/CardCheck.js"></script>
    <link rel="stylesheet" href="/Resources/styles/jquery-ui.css" />
    <link rel="stylesheet" href="/Resources/styles/jquery-ui.theme.min.css" />
    <link rel="stylesheet" href="/Resources/styles/bootstrap.min.css" />
    <link rel="stylesheet" href="/Resources/styles/jquery.bxslider.css" />
    <link rel="stylesheet" href="/Resources/styles/affinion.min.css" />
    <link rel="stylesheet" href="/Resources/styles/affinion-dev.css" />
    <link rel="stylesheet" href="/Resources/styles/theme1/theme.css" />
    <link rel="stylesheet" href="/Resources/styles/affinion.custom.css" />
    <link rel="stylesheet" href="/Resources/styles/fonts.css" />
    <link rel="stylesheet" href="/Resources/styles/font-awesome.min.css" />
    <link rel="stylesheet" href="/Resources/styles/bootstrap-social.css" />

    <!-- theme css -->
    <!--<link rel="stylesheet" href="/Resources/styles/theme-red.css" />-->
    <link rel="stylesheet" href="/Resources/styles/<%= this.selectedTheme %>/theme.css" />

    <link rel="shortcut icon" type="image/x-icon" href="<%=this.Favicon %>" />
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&sensor=true"></script>
    <script type="text/javascript" src="/Resources/scripts/CardCheck.js"></script>

    <script lang="javascript" type="text/javascript">
        function saveAndCloseWin() {
            var olid = '<%= PassedOrderLineId.Trim()%>';
            if (window.opener != null && !window.opener.closed) {
                $('input[data-olid="' + olid + '"]', window.opener.document).prop('checked', true);
                $('button[data-olid="' + olid + '"]', window.opener.document).css("background-color", "#428243");
                $('span[data-olid="' + olid + '"]', window.opener.document).css('display', 'none');
                var labelAccomodation = window.opener.document.getElementById("LabelAccomodationValidation");
                if (labelAccomodation != null)
                    labelAccomodation.innerHTML = "";
            }
            window.close();
            return false;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <%--  <div class="modal fade" id="no-room" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">--%>
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><span class="fa fa-group"></span>
                <sc:text id="titleTxt" runat="server" field="Title Text" />
            </h4>
        </div>
        <div class="modal-body row">
            <div class="no-room inline-block width-full col-md-12">
                <asp:Repeater ID="RepeaterGuestAccordian" runat="server" OnItemDataBound="RepeaterGuestAccordian_ItemDataBound">
                    <ItemTemplate>
                        <div class="col-md-6 col-xs-12">
                            <div class="content_bg_inner_box alert-info ">
                                <div class="room-title">
                                    <h4 class="no-margin">
                                        <sc:text id="roomTxt" runat="server" field="Room Text" />
                                        <%#Container.ItemIndex + 1 %></h4>
                                </div>
                                <%--<span>No of Guests</span>  --%>
                                <asp:DropDownList CssClass="form-control" ID="DropdownGuest" runat="server" Visible="false">
                                </asp:DropDownList>
                                <div class="col-xs-12 no-padding">
                                    <span>
                                        <sc:text id="nameTxt" runat="server" field="Guest Name Text" />
                                    </span>

                                    <%--<input type="text" placeholder="Full Guest Name" class="form-control">--%>
                                    <span class="accent58-f">
                                        <sc:text id="mandSym" runat="server" field="Mandatory Symbol" />
                                    </span>

                                </div>
                                <div class="col-xs-12 no-padding">
                                    <div class="col-xs-4 no-padding">
                                        <asp:DropDownList ID="ddlTitle" CssClass="form-control width95" runat="server"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvTitle" ValidationGroup="submitGroup" InitialValue="0" ControlToValidate="ddlTitle" ForeColor="Red" CssClass="accrequired" runat="server" Display="Dynamic" ErrorMessage="*"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-xs-4">
                                        <asp:TextBox ID="txtFirstName" Text='<%#Eval("FirstName") %>' CssClass="form-control width95" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvFirstName" ValidationGroup="submitGroup" ForeColor="Red" ControlToValidate="txtFirstName" runat="server" CssClass="accrequired" Display="Dynamic" ErrorMessage="*"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="charFirstName" runat="server" ValidationGroup="submitGroup" ErrorMessage="<span class='accent58-f'>Not a valid character</span>" ControlToValidate="txtFirstName" ValidationExpression="^([\'\&quot;&quot;\,\%\&lt;\&gt;\/\;\\a-zA-ZäåöæâéèêøóòôÄÖÅÆØ0-9\s])*$"></asp:RegularExpressionValidator>
                                        <asp:CustomValidator ID="CustomFirstName" ControlToValidate="txtFirstName" ClientValidationFunction="ClientValidate" Display="Dynamic" ErrorMessage="*Dont Enter Card Number!" ForeColor="Red" Font-Name="verdana" Font-Size="10pt" runat="server" />
                                    </div>
                                    <div class="col-xs-4 no-padding">
                                        <asp:TextBox ID="txtLastName" CssClass="form-control width95" Text='<%#Eval("LastName") %>' runat="server"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="charLastName" runat="server" ValidationGroup="submitGroup" ErrorMessage="<span class='accent58-f'>Not a valid character</span>" ControlToValidate="txtLastName" ValidationExpression="^([\'\&quot;&quot;\,\%\&lt;\&gt;\/\;\\a-zA-ZäåöæâéèêøóòôÄÖÅÆØ0-9\s])*$"></asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="rfvLastName" ValidationGroup="submitGroup" ForeColor="Red" ControlToValidate="txtLastName" runat="server" CssClass="accrequired" Display="Dynamic" ErrorMessage="*"></asp:RequiredFieldValidator>
                                        <asp:CustomValidator ID="CustomLastName" ControlToValidate="txtLastName" ClientValidationFunction="ClientValidate" Display="Dynamic" ErrorMessage="*Dont Enter Card Number!" ForeColor="Red" Font-Name="verdana" Font-Size="10pt" runat="server" />
                                    </div>
                                </div>

                                <asp:TextBox CssClass="form-control" ID="TextGuestFullName" Text='<%#Eval("GuestName") %>' runat="server"></asp:TextBox>
                                <asp:CustomValidator ID="GuestName" ControlToValidate="TextGuestFullname" ClientValidationFunction="ClientValidate" Display="Dynamic" ErrorMessage="*Dont Enter Card Number!" ForeColor="Red" Font-Name="verdana" Font-Size="10pt" runat="server" />
                                <div class="col-xs-12 no-padding martop10">
                                    <span>
                                        <sc:text id="adultTxt" runat="server" field="Adult Text" />
                                    </span>
                                    <asp:DropDownList CssClass="form-control" ID="DropDownNoOfAdults" runat="server" ClientIDMode="Static">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="submitGroup" InitialValue="0" ControlToValidate="DropDownNoOfAdults" runat="server" CssClass="accrequired marRm10" Display="Dynamic" ErrorMessage="*"></asp:RequiredFieldValidator>
                                </div>
                                <div class="col-xs-12 no-padding martop10">
                                    <span>
                                        <sc:text id="childTxt" runat="server" field="Children Text" />
                                    </span>
                                    <asp:DropDownList CssClass="form-control" ID="DropDownNoOfChildren" runat="server" OnSelectedIndexChanged="DropDownNoOfChildren_SelectedIndexChanged" AutoPostBack="true" ClientIDMode="Static">
                                    </asp:DropDownList>
                                </div>
                                <%--<asp:HiddenField ID="HiddenRoomReservationId" Value='<%#Eval("RoomReservationId") %>' runat="server"></asp:HiddenField>--%>


                                <%--age selection code start--%>

                                <div class="col-xs-12 no-padding ageGroupOuter2" style="display: block">
                                    <div class="age-group">
                                        <div class="col-xs-12 row" id="ChildAgeTextDiv">
                                            <h5 id="ChildAgeTextHeading">
                                                <sc:text id="ageTxt" runat="server" field="Age of Children Text" />
                                            </h5>
                                        </div>
                                        <asp:Repeater ID="RepeaterChildAge" runat="server">
                                            <ItemTemplate>
                                                <div id="a1" class="age2 col-xs-4" style="display: block">
                                                    <div role="group" aria-label="...">
                                                        <%--<select class="form-control">
                                                            <option>1</option>
                                                            <option>2</option>
                                                            <option>3</option>
                                                            <option>4</option>
                                                            <option>5</option>
                                                        </select>--%>
                                                        <asp:DropDownList CssClass="form-control" ID="DropDownAge" runat="server">
                                                            <asp:ListItem Text="Please Select" Value=""></asp:ListItem>
                                                            <asp:ListItem Text="0" Value="0"></asp:ListItem>
                                                            <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                            <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                            <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                            <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" InitialValue="" ValidationGroup="submitGroup" ControlToValidate="DropDownAge" runat="server" CssClass="accrequired marRm10" Display="Dynamic" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                                <%--age selection code end--%>

                                <h4 class="accent34-f margin-bottom-15 martop10">
                                    <sc:text id="srTxt" runat="server" field="Special Request Text" />
                                </h4>
                                <div>
                                    <asp:CustomValidator ID="SpecialRequest" ForeColor="Red" ControlToValidate="TextSpecialRequest" ClientValidationFunction="ClientValidate" Display="Dynamic" ErrorMessage="*Dont Enter Card Number!" Font-Name="verdana" Font-Size="10pt" runat="server" />
                                    <asp:TextBox ID="TextSpecialRequest" TextMode="multiline" Columns="50" Rows="4" Text='<%#Eval("SpecialRequest") %>' class="width-full" cols="100" runat="server" />
                                    <asp:RegularExpressionValidator runat="server" ErrorMessage="<span class='accent58-f'>Problem adding special request</span>" ControlToValidate="TextSpecialRequest" ValidationExpression="^[^<>\&quot;/;`%?0-9]*$"> </asp:RegularExpressionValidator>
                                </div>

                            </div>
                        </div>

                        <%--Addon detail--%>

                        <%--<div class="col-md-6 col-xs-12">

                            <div class="addon pull-left col-md-12 width-full">
                                <h4 class="no-margin">Addons</h4>
                                <div class="col-md-12 margin-row">
                                    <div class="col-md-1 pull-left">
                                        <input type="checkbox" name="breakfast" value="breakfast">
                                    </div>
                                    <div class="col-md-3 pull-left">
                                        <span class="addon-free">FREE</span>
                                    </div>
                                    <div class="col-md-8"><span>Breakfast</span> <span class="accent50-f"></span></div>

                                </div>
                                <div class="col-md-12 margin-row">
                                    <div class="col-md-1 pull-left">
                                        <input type="checkbox" name="Golf" value="Golf">
                                    </div>
                                    <div class="col-md-3 pull-left">
                                        <span>€ 150</span>
                                    </div>
                                    <div class="col-md-7"><span>Golf</span><span class="accent50-f"> (Global)</span></div>

                                </div>
                                <div class="col-md-12 margin-row">
                                    <div class="col-md-1 pull-left">
                                        <input type="checkbox" name="Spa" value="Spa">
                                    </div>
                                    <div class="col-md-3 pull-left">
                                        <span class="addon-free">FREE</span>
                                    </div>
                                    <div class="col-md-8"><span>Spa </span><span class="accent50-f">(Global)</span></div>

                                </div>

                            </div>
                        </div>--%>
                    </ItemTemplate>
                </asp:Repeater>

                <%--<h4 class="accent34-f margin-bottom-15">Special Request</h4>
                <div>                    
                    <asp:TextBox ID="TextSpecialRequest" TextMode="multiline" Columns="50" Rows="4" class="width-full" cols="100" runat="server" />
                </div>--%>
            </div>
            <asp:Label ID="LabelValidateFieldErrorMsg" Class="accent58-f" runat="server" Text=""></asp:Label>
        </div>

        <div class="modal-footer">
            <button id="ButtonSave" runat="server" validationgroup="submitGroup" class="btn btn-default" onserverclick="ButtonSave_Click" usesubmitbehavior="false" enableviewstate="true">
                <sc:text id="btnSavetxt" runat="server" field="Submit Text" />
            </button>
            <button id="ButtonClose" runat="server" class="btn btn-default" onclick="closeWin()" usesubmitbehavior="false">
                <sc:text id="btnClosetxt" runat="server" field="Close Text" />
            </button>
        </div>
        <%-- </div>
            </div>
        </div>--%>
    </form>
</body>
</html>

<script lang="javascript" type="text/javascript">
    //jQuery("#ChildAgeDiv").hide();
    //jQuery("#DropDownNoOfChildren").change(function () {
    //    if (this.value > 0) {
    //        alert(this.value);
    //        jQuery("#ChildAgeDiv").hide();
    //    }
    //});   

    jQuery(document).ready(function () {
        jQuery('#ChildAgeTextDiv').hide();
        var ageVal = jQuery('#DropDownNoOfChildren').val();
        if (ageVal > 0) {
            jQuery('#ChildAgeTextDiv').show();
        }
        else {
            jQuery('#ChildAgeTextDiv').hide();
        }
    });

    function closeWin() {
        window.close();
        return false;
    }

    window.onunload = function (e) {
        opener.document.getElementById('ButtonSetAccomodation');
    };
</script>
