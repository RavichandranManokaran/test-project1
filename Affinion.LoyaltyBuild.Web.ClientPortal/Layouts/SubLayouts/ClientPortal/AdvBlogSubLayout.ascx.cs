﻿namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal
{
    //<summary>
    /// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
    /// PROPRIETARY INFORMATION The information contained herein (the 
    /// 'Proprietary Information') is highly confidential and proprietary to and 
    /// constitutes trade secrets of Affinion International. The Proprietary Information 
    /// is for Affinion International use only and shall not be published, 
    /// communicated, disclosed or divulged to any person, firm, corporation or 
    /// other legal entity, directly or indirectly, without the prior written 
    /// consent of Affinion International.
    ///
    /// Source File:           AdvBlogSubLayout.cs
    /// Sub-system/Module:     Affinion.LoyaltyBuild.Web
    /// Description:           Used to Bind data to AdvBlogSubLayout subLayout
    /// </summary>
    #region Using Directives
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using Sitecore.Web.UI.WebControls;
    using System;
    #endregion

    public partial class AdvBlogSubLayout : BaseSublayout
    {
        /// <summary>
        /// Bind  Item to image field of AdvBlogSubLayout sublayots 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                
                Item dataSource = this.GetDataSource();

                if (dataSource != null)
                {
                    this.PlaceholderText.Visible = false;
                    this.AdvSummary.Item = this.AdvLink.Item = dataSource;
                }
            }
            catch (Exception ex)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, ex.Message);
                throw;
            }
        }
    }
}