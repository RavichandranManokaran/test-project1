﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           GeoCordinate.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common (VS project name)
/// Description:           Data class for geo cordinates
/// </summary>
namespace Affinion.LoyaltyBuild.Common.Search
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Data class for geo cordinates
    /// </summary>
    public class GeoCordinate
    {
        public string SupplierId { get; set; }

        public string Location { get; set; }

        public string Longitute { get; set; }

        public string Latitude { get; set; }
    }
}
