﻿CREATE TABLE [dbo].[uCommerce_DataTypeEnum] (
    [DataTypeEnumId] INT              IDENTITY (1, 1) NOT NULL,
    [DataTypeId]     INT              NOT NULL,
    [Value]          NVARCHAR (1024)  NOT NULL,
    [Deleted]        BIT              CONSTRAINT [DF__uCommerce__Delet__4959E263] DEFAULT ((0)) NOT NULL,
    [Guid]           UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [SortOrder]      INT              DEFAULT ((0)) NOT NULL,
    CONSTRAINT [uCommerce_PK_DataTypeEnum] PRIMARY KEY CLUSTERED ([DataTypeEnumId] ASC),
    CONSTRAINT [uCommerce_FK_DataTypeEnum_DataType] FOREIGN KEY ([DataTypeId]) REFERENCES [dbo].[uCommerce_DataType] ([DataTypeId])
);

