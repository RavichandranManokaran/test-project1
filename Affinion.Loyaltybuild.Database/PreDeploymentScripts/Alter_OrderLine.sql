﻿/*
 Pre-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be executed before the build script.	
 Use SQLCMD syntax to include a file in the pre-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the pre-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/


IF  EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[uCommerce_OrderLine]') 
         AND name = 'NoOfAdults'
)
begin
ALTER TABLE [dbo].[uCommerce_OrderLine] DROP CONSTRAINT DF__uCommerce__NoOfA__0B7CAB7B
ALTER TABLE [dbo].[uCommerce_OrderLine]
DROP COLUMN [NoOfAdults] 

end


IF  EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[uCommerce_OrderLine]') 
         AND name = 'NoOfChilderen'
)
begin
ALTER TABLE [dbo].[uCommerce_OrderLine] DROP CONSTRAINT DF__uCommerce__NoOfC__0C70CFB4
ALTER TABLE [dbo].[uCommerce_OrderLine]
DROP  COLUMN [NoOfChilderen] 
END




IF  EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[uCommerce_OrderLine]') 
         AND name = 'CheckInDate'
)
begin
ALTER TABLE [dbo].[uCommerce_OrderLine] DROP CONSTRAINT DF__uCommerce__Check__0D64F3ED
ALTER TABLE [dbo].[uCommerce_OrderLine]
DROP COLUMN [CheckInDate] 
End

IF  EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[uCommerce_OrderLine]') 
         AND name = 'CheckOutDate'
)
Begin
ALTER TABLE [dbo].[uCommerce_OrderLine] DROP CONSTRAINT  DF__uCommerce__Check__0E591826
ALTER TABLE [dbo].[uCommerce_OrderLine]
DROP COLUMN [CheckOutDate]
End


--Alter scripts for columns PayableAtAccomodation and Deposit

IF not EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[uCommerce_OrderLine]') 
         AND name = 'PayableAtAccomodation'
)
ALTER TABLE [dbo].[uCommerce_OrderLine]
ADD [PayableAtAccomodation] DECIMAL NOT NULL 


IF not EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[uCommerce_OrderLine]') 
         AND name = 'Deposit'
)

ALTER TABLE [dbo].[uCommerce_OrderLine]
ADD [Deposit] DECIMAL NOT NULL 

ALTER TABLE [dbo].[uCommerce_OrderLine]
ADD [PaidInFull] BIT NULL