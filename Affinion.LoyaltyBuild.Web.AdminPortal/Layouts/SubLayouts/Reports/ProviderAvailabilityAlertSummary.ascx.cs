﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model;
using Sitecore.Data.Items;
using Affinion.LoyaltyBuild.Web.SupplierPortal;
using System.IO;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration;
using Affinion.LoyaltyBuild.Presentation.BaseLayouts;

namespace Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.Reports
{
    public partial class ProviderAvailabilityAlertSummary : BaseSublayout
    {
        IReportsService iReportsService = new ReportsService();
        SearchCriteria objSearchCriteria = new SearchCriteria();
        string ProviderPath = Affinion.LoyaltyBuild.Common.Constants.ProviderPath;
        Sitecore.Data.Database sitecoreDatabase = Sitecore.Configuration.Factory.GetDatabase("master");
        Item[] providers = null;
        Item providerConfiguration = null;
        protected int TotalOfRecords { get; private set; }
        ListItem listItem = null;
        public static List<ProviderAvailabilityAlert> ProviderAvailabilityAlertList =  new List<ProviderAvailabilityAlert>();
        protected void Page_Load(object sender, EventArgs e)
        {
            BindSiteCoreText();
            if(!IsPostBack)
            {
                BindSupplierType();
                BindOccupancyTypes();
                ddlSupplier.Items.Insert(0, new ListItem { Text = "Select Supplier", Value = string.Empty });
            }
           
        }

        private void BindSiteCoreText()
        {
            Item currentItem = Sitecore.Context.Item;
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "PanelTitleText", scPanelTitle);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "ParameterText", scParameters);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "DateRangeText", scDateRange);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "SupplierTypeText", scSupplierType);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "SupplierText", scSupplier);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "OccupancyTypeText", scOccupencyType);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "GridPanelTitleText", scGridPanelTile);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "GridPanelTotalText", scTototalContText);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "GridRecordsFoundText", scGridRecordsFound);
            
        }

        protected void Search_Click(object sender, EventArgs e)
        {
            GetProviderAvailability();
        }


        private void GetProviderAvailability()
        {
            try
            {
                iReportsService = new ReportsService();
                objSearchCriteria = new SearchCriteria();

                if (!string.IsNullOrEmpty(this.DateRange.Value))
                {
                    string[] dates = this.DateRange.Value.ToString().Split('-');
                    objSearchCriteria.DateRange = InputDateFormatting(dates[0]);
                    objSearchCriteria.DateUpTo = InputDateFormatting(dates[1]);
                }
                else
                {
                    objSearchCriteria.DateRange = null;
                    objSearchCriteria.DateUpTo = null;
                }

                if (!string.IsNullOrEmpty(this.ddlSupplierType.SelectedValue))
                    objSearchCriteria.SupplierType = this.ddlSupplierType.SelectedItem.Text;

                if (!string.IsNullOrEmpty(this.ddlSupplier.SelectedValue))
                    objSearchCriteria.Provider = this.ddlSupplier.SelectedValue;

                if (!string.IsNullOrEmpty(this.ddlOccupancyType.SelectedValue))
                    objSearchCriteria.OccupancyType = this.ddlOccupancyType.SelectedValue;
                
                ProviderAvailabilityAlertList = iReportsService.GetProviderAvailabilityAlertReport(objSearchCriteria);

                SetRecordCount();

                if (ProviderAvailabilityAlertList != null && ProviderAvailabilityAlertList.Count > 0)
                {
                    ProviderAvailabilityAlertList.ToList().ForEach(lst =>
                    {
                        providers = sitecoreDatabase.SelectItems(ProviderPath);
                        providerConfiguration = providers.Where(x => Guid.Parse(x.ID.ToString()).ToString().Equals(Guid.Parse(lst.ProviderID.ToString()).ToString(), StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                        if (providerConfiguration != null)
                        {
                            SupplierInfo supplierInfo = SiteConfiguration.GetSupplierDetailById(providerConfiguration.ID.ToString());

                            if (supplierInfo != null)
                            {
                                if (!string.IsNullOrEmpty(supplierInfo.MapLocation))
                                    lst.MapLocationName = supplierInfo.MapLocation;

                                if (!string.IsNullOrEmpty(supplierInfo.StarRating))
                                    lst.StarRanking = supplierInfo.StarRating;

                                if (!string.IsNullOrEmpty(supplierInfo.Country))
                                    lst.CountryName = supplierInfo.Country;

                                if (!string.IsNullOrEmpty(supplierInfo.Email))
                                    lst.ProviderEmail = supplierInfo.Email;

                                if (!string.IsNullOrEmpty(supplierInfo.SupplierType))
                                    lst.ProviderTypeName = supplierInfo.SupplierType;
                            }
                        }
                    });

                }


                this.ResultGridView.DataSource = ProviderAvailabilityAlertList;
                this.ResultGridView.DataBind();

            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.AdminPortal, ex, null);
                throw;
            }

        }

        private void SetRecordCount()
        {
            if (ProviderAvailabilityAlertList != null && ProviderAvailabilityAlertList.Count > 0)
            {
                TotalOfRecords = ProviderAvailabilityAlertList.Count;
            }
            else
            {
                ProviderAvailabilityAlertList = new List<ProviderAvailabilityAlert>();
                
            }

        }

        private DateTime InputDateFormatting(string date)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(date))
                {
                    return default(DateTime);
                }
                else
                {
                    DateTime tmpDateTime;
                    try
                    {
                        tmpDateTime = DateTime.Parse(date);
                        return tmpDateTime;
                    }
                    catch
                    {
                        string[] words = date.Split('/');
                        string newstring = words[1] + "/" + words[0] + "/" + words[2];
                        DateTime dt = Convert.ToDateTime(newstring);
                        return dt;
                    }
                }
            }
            catch
            {
                return default(DateTime);
            }

        }

        protected void Export_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Clear();
                Response.Buffer = true;
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Charset = string.Empty;
                string FileName = CreateFileName();

                using (StringWriter strwritter = new StringWriter())
                {
                    HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);


                    GridView tempGrid = ResultGridView;
                    tempGrid.AllowPaging = false;
                    tempGrid.CssClass = "table table-striped table-bordered";
                    tempGrid.AutoGenerateColumns = false;
                    if (ProviderAvailabilityAlertList == null || ProviderAvailabilityAlertList.Count == 0)
                        GetProviderAvailability();

                    tempGrid.DataSource = ProviderAvailabilityAlertList;
                    tempGrid.DataBind();

                    Page.Form.Controls.Clear();

                    Page.Form.Controls.Add(tempGrid);

                    tempGrid.GridLines = GridLines.Both;
                    tempGrid.HeaderStyle.Font.Bold = true;
                    Page.Form.RenderControl(htmltextwrtter);
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName + ".xls");
                    Response.Write(strwritter.ToString());
                    Response.End();
                }

            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        private void BindSupplierType()
        {
            try
            {
                var supplierType = new ListItemCollection();
                Item[] supplierTypeList = Sitecore.Context.Database.SelectItems(Constants.supplireTypePath) != null ? Sitecore.Context.Database.SelectItems(Constants.supplireTypePath) : null;
                if (supplierTypeList != null)
                {
                    foreach (var suplierType in supplierTypeList)
                    {
                        listItem = new ListItem();
                        listItem.Text = SitecoreFieldsHelper.GetValue(suplierType, "Name");
                        listItem.Value = suplierType.ID.ToString();
                        if (listItem != null)
                        {
                            supplierType.Add(listItem);
                        }
                    }

                    BindDropdown(ddlSupplierType, supplierType, "Text", "Value");

                    ddlSupplierType.Items.Insert(0, new ListItem { Text = "Select Supplier Type", Value = string.Empty });
                }
            }
            catch(Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.AdminPortal, ex, null);
                throw;
            }

        }

        private void BindDropdown(DropDownList ddlControl, object dataSource, string text, string value)
        {
            ddlControl.DataSource = dataSource;
            ddlControl.DataTextField = text;
            ddlControl.DataValueField = value;
            ddlControl.DataBind();
        }

        protected void ddlSupplierType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddlSupplier.Items.Clear();
                Item[] hotelList = Sitecore.Context.Database.SelectItems(Constants.hotelPath);
                var supplierList = new ListItemCollection();

                if (ddlSupplierType.SelectedValue != null)
                {
                    if (hotelList != null)
                    {
                        foreach (var hotel in hotelList)
                        {
                            Sitecore.Data.Fields.ReferenceField droplinkFld = hotel.Fields["SupplierType"];
                            if (droplinkFld != null)
                            {
                                if (droplinkFld.TargetItem != null)
                                {
                                    Sitecore.Data.Items.Item targetItem2 = droplinkFld.TargetItem;
                                    if (targetItem2 != null && targetItem2.ID.ToString().Equals(ddlSupplierType.SelectedValue, StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        ListItem liBox = new ListItem();
                                        liBox.Text = SitecoreFieldsHelper.GetValue(hotel, "Name");
                                        liBox.Value =Guid.Parse(hotel.ID.ToString()).ToString();
                                        if (liBox != null)
                                        {
                                            supplierList.Add(liBox);
                                        }
                                    }
                                }



                            }
                        }
                        BindDropdown(ddlSupplier, supplierList, "Text", "Value");
                        ddlSupplier.Items.Insert(0, new ListItem { Text = "Select Supplier", Value = string.Empty });
                    }
                }
            }
            catch(Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.AdminPortal, ex, null);
                throw;
            }
        }

        private void BindOccupancyTypes()
        {
            try
            {
                ddlOccupancyType.Items.Clear();
                var occupancyType = new ListItemCollection();
                List<string> dicRoomType = new List<string>();
                Item[] roomTypes = Sitecore.Context.Database.SelectItems(string.Format("fast:{0}", Sitecore.Configuration.Settings.GetSetting("SiteCoreOccupancyType")));
                foreach (Item roomType in roomTypes)
                {
                    listItem = new ListItem();
                    listItem.Text = roomType.Fields["Name"].Value;
                    listItem.Value = roomType.Fields["Name"].Value;
                    if (listItem != null)
                    {
                        occupancyType.Add(listItem);
                    }
                }

                BindDropdown(ddlOccupancyType, occupancyType, "Text", "Value");

                ddlOccupancyType.Items.Insert(0, new ListItem { Text = "Select Occupancy Type", Value = string.Empty });
            }
            catch(Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.AdminPortal, ex, null);
                throw;
            }
        }

        private string CreateFileName()
        {
            return "ProviderAvailabilityAlertSummary " + DateTime.Now.Date.ToString("d");
        }

        
        protected void ResultGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ResultGridView.PageIndex = e.NewPageIndex;
            this.ResultGridView.DataSource = ProviderAvailabilityAlertList;
            this.ResultGridView.DataBind();
            SetRecordCount();
        }


    }
}