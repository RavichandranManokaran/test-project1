﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion Inernational. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           WizardDialog.cs
/// Sub-system/Module:     ClientSetup(VS project name)
/// Description:           WizardDialog Class contain more information about the wizard
/// </summary>


#region Using Directives

using System.Collections.Generic;

#endregion

namespace Affinion.LoyaltyBuild.Common.Wizard
{
    /// <summary>
    /// WizardDialog stores the basic information about the wizard
    /// </summary>
    public class WizardDialog : WizardCommon
    {
        #region Class Properties
        /// <summary>
        /// use to check for mandatory wizard step
        /// </summary>
        public bool isRequiredToFill { get; set; }

        /// <summary>
        /// use to store html elements
        /// </summary>
        public Dictionary<string, string> htmlTags { get; set; }

        /// <summary>
        /// 
        /// </summary>         
        public string SampleDescription { get; set; }

        /// <summary>
        /// store the list of htmlelements
        /// </summary>
        public HtmlElements HtmlElements { get; set; }

        #endregion
    }

    #region HtmlElement class
    /// <summary>
    /// Class for list of HtmlElements
    /// </summary>
    public class HtmlElements
    {
        internal List<Element> ListElements = new List<Element>();
    }

    #endregion

    /// <summary>
    /// Class for individual html Element
    /// </summary>
    public class Element
    {
        #region Class Properties
        /// <summary>
        /// ID of html element
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// Display text of html element
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Type of html element
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Placeholder
        /// </summary>
        public string Placeholder { get; set; }

        /// <summary>
        /// Class
        /// </summary>
        public string Class { get; set; }


        /// <summary>
        /// Value
        /// </summary>
        public string Value { get; set; }
        #endregion
    }
}
