﻿using Affinion.LoyaltyBuild.UCom.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;
using Affinion.LoyaltyBuild.UCom.Common.Extension;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class SeasonUnitDiscountAward : Award, IEntity
    {
        public SeasonUnitDiscountAward()
        {
            this.Details = new List<SeasonUnitDiscountAwardDetail>();
        }
        // Fields
        private int? _hashCode;

        // Methods

        public override void Apply(PurchaseOrder order, IList<OrderLine> orderLines)
        {
            if (orderLines != null && orderLines.Any())
            {
                List<SeasonUnitDiscountAwardDetail> seasonAward = this.Details.ToList();
                foreach (OrderLine line in orderLines)
                {
                    OrderProperty arrivalDateProperty = line.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.ArrivalDateKey);
                    if (arrivalDateProperty != null)
                    {
                        DateTime arrivalDate = arrivalDateProperty.Value.ParseFormatDateTime(true);
                        OrderProperty nights = line.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.NightKey);
                        if (nights != null)
                        {
                            int noOfNights = 0;
                            if (int.TryParse(nights.Value, out noOfNights))
                            {
                                arrivalDate = arrivalDate.AddDays(noOfNights);
                            }
                        }
                        SeasonUnitDiscountAwardDetail applyAward = seasonAward.FirstOrDefault(x => arrivalDate >= x.StartDate && arrivalDate <= x.EndDate);
                        if (applyAward != null)
                        {
                            Discount discount = this.CreateDiscount(order, orderLines);
                            if (applyAward.IsPercentage)
                            {
                                discount.AmountOffTotal = ((line.Quantity * line.Price) / 100M) * applyAward.AmountOff;
                            }
                            else
                            {
                                if (applyAward.OnOrderLine)
                                {
                                    discount.AmountOffTotal = applyAward.AmountOff;
                                }
                                else
                                {
                                    discount.AmountOffTotal = applyAward.AmountOff * line.Quantity;
                                }
                            }
                            order.AddDiscount(discount);
                            discount.AddOrderLine(line);
                            line.AddDiscount(discount);
                        }
                    }
                }
            }
        }

        // Methods
        public new static IQueryable<SeasonUnitDiscountAward> All()
        {
            return GetRepo().Select();
        }

        public override void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<SeasonUnitDiscountAward, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            SeasonUnitDiscountAward objA = obj as SeasonUnitDiscountAward;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<SeasonUnitDiscountAward, bool>> expression)
        {
            return All().Any<SeasonUnitDiscountAward>(expression);
        }

        public static IList<SeasonUnitDiscountAward> Find(Expression<Func<SeasonUnitDiscountAward, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<SeasonUnitDiscountAward>();
        }

        public static SeasonUnitDiscountAward FirstOrDefault(Expression<Func<SeasonUnitDiscountAward, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<SeasonUnitDiscountAward>();
        }

        public new static SeasonUnitDiscountAward Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<SeasonUnitDiscountAward> GetRepo()
        {
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<SeasonUnitDiscountAward>>();

        }


        public override void Save()
        {
            try
            {
                GetRepo().Save(this);
            }
            catch
            {

            }
        }

        public static SeasonUnitDiscountAward SingleOrDefault(Expression<Func<SeasonUnitDiscountAward, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties
        public virtual IList<SeasonUnitDiscountAwardDetail> Details { get; set; }

        public virtual int SeasonUnitDiscountAwardId { get; protected set; }

        public override int Id
        {
            get
            {
                return this.SeasonUnitDiscountAwardId;
            }
        }

        public virtual string Content { get; set; }
    }




}
