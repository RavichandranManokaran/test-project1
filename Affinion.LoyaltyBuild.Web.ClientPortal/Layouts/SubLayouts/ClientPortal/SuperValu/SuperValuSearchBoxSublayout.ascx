﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SuperValuSearchBoxSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.SuperValuSearchBoxSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="hotel-search-box">
    <h3>Quick Search</h3>

    <div class="booking-top width-full">
    </div>
    <div class="col-xs-12">
        <h5>Destination</h5>
        <input type="text" class="form-control" placeholder="Detaile Info" aria-describedby="basic-addon1">
    </div>
    <div class="col-xs-12">
        <h5>Check-in date</h5>
        <div class="input-group">
            <span class="input-group-btn">
                <button class="btn btn-default glyphicon glyphicon-calendar calander-icon" type="button"></button>
            </span>
            <input type="text" class="form-control" placeholder="When to order..." id="datepicker">
        </div>
    </div>
    <div class="col-xs-12">
        <h5>Check-in date</h5>
        <div role="group" aria-label="...">
            <select class="form-control">
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
            </select>
        </div>
    </div>
    <div class="col-xs-12">
        Check-out-date: 03/08/2015 
    </div>
    <div class="booking-deatail-order-room width-full inline-block">
        <div class="col-xs-12 col-md-4">
            <h5>Rooms</h5>
            <div role="group" aria-label="...">
                <select class="form-control">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                </select>
            </div>

        </div>
        <div class="col-xs-12 col-md-4">
            <h5>Adults</h5>
            <div role="group" aria-label="...">
                <select class="form-control">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                </select>
            </div>

        </div>
        <div class="col-xs-12 col-md-4">
            <h5>Children</h5>
            <div role="group" aria-label="...">
                <select class="form-control">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                </select>
            </div>

        </div>
    </div>
    <div class="col-xs-12">

        <h5>Choose Your Package </h5>
        <select class="form-control styled-select">
            <option selected="selected" value="746">Show me everything</option>
            <option value="205">Breakfast</option>
            <option value="252">Breakfast + 1 Dinner</option>
            <option value="748">Breakfast  - Inspired Collection</option>
            <option value="248">Breakfast + 1 Dinner + Spa</option>
            <option value="238">Self Catering Accommodation</option>
            n>
        </select>
    </div>
    <div class="col-xs-12">
        <button type="submit" class="btn btn-primary booking-margin-top float-right btn-bg">Submit</button>
    </div>


</div>
