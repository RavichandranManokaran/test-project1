CREATE PROCEDURE LB_ApSqlJob
AS
BEGIN
INSERT INTO LB_SupplierPaymentRequests
(OrderId,OrderLineId,SuggestedProviderAmount,IsProviderPaymentRequested,dateProviderPaymentRequested)
SELECT DISTINCT OL.OrderId,OL.OrderLineId,SUM(OLP.Amount),1,GETDATE()
FROM LB_RoomReservation RS
INNER JOIN uCommerce_OrderLine OL ON RS.OrderLineId=OL.OrderLineId
INNER JOIN LB_OrderLinePayment OLP ON OL.OrderLineId=OLP.OrderLineId AND OLP.DueTypeId=1
INNER JOIN LB_OrderLineDue OLD ON OLP.OrderLineId=OLD.OrderLineId AND OLD.IsDeleted=0 AND OLD.SubrateItemCode IS NOT NULL 
INNER JOIN LB_DueTypes DT ON DT.DueTypeId=OLD.DueTypeId 
INNER JOIN uCommerce_OrderProperty OP ON OLD.OrderLineId=OP.OrderLineId AND OP.OrderLineId IS NOT NULL AND OP.OrderId=OL.OrderId
WHERE
 OP.Value='Full' AND RS.OrderReference IS NOT NULL AND
 DATEDIFF(mm, RS.CheckOutDate,GETDATE())<=6 AND
 OL.OrderId NOT IN (SELECT PR.OrderId FROM LB_SupplierPaymentRequests PR) AND
 OL.OrderLineId NOT IN (SELECT PR.OrderLineId FROM LB_SupplierPaymentRequests PR) AND
 DT.DueTypeId=1 AND DT.IsActive=1
 
GROUP BY
OL.OrderLineId,
OL.OrderId
 
END
