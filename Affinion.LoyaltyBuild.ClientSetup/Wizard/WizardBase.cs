﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion Inernational. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           WizardBase.cs
/// Sub-system/Module:     ClientSetup(VS project name)
/// Description:           WizardBase Class contain base information about the wizard
/// </summary>


namespace Affinion.LoyaltyBuild.ClientSetup.Wizard
{
    public class WizardBase
    {
        /// <summary>
        #region Class Properties
        /// imitalize the wizard steps
        /// </summary>
        public int NumberOfSteps { get; set; }

        /// <summary>
        /// use to get the percentage of completion
        /// </summary>
        public int CompletedSteps { get; set; } 

        #endregion
    }
}
