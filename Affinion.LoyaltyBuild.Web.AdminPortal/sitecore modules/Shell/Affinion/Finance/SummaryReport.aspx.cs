﻿using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.AdminPortal.sitecore_modules.Shell.Affinion.Finance
{
    public partial class SummaryReport : System.Web.UI.Page
    {
        IReportsService iReportsService = new ReportsService();
        SearchCriteria objSearchCriteria = new SearchCriteria();
        protected int ProviderTotalOfRecords { get; private set; }
        protected int CurrencyTotalOfRecords { get; private set; }
        static List<FinanceSummaryReport> _providerSummaryReport = new List<FinanceSummaryReport>();
        Sitecore.Data.Database sitecoreDatabase = Sitecore.Configuration.Factory.GetDatabase("master");
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (ProviderSummaryReport != null)
                    ProviderSummaryReport.Clear();
                BindProvider();
                BindClient();
                BindOracleFile();
            }
        }

        public static List<FinanceSummaryReport> ProviderSummaryReport
        {
            get { return _providerSummaryReport; }
            set { _providerSummaryReport=value; }
        }
  
        private void BindProvider()
        {
            Dictionary<string, string> providerDatasource = new Dictionary<string, string>();

            Item[] hotels = sitecoreDatabase.SelectItems(Constants.BasketPageQueryPath);
            foreach (var hotel in hotels)
            {
                if (hotel != null)
                {
                    providerDatasource.Add(Guid.Parse(hotel.ID.ToString()).ToString(), hotel.Fields["Name"].Value);
                }
            }

            this.Provider.DataSource = providerDatasource;
            this.Provider.DataTextField = "Value";
            this.Provider.DataValueField = "Key";
            this.Provider.DataBind();
            this.Provider.Items.Insert(0, new ListItem { Text = "Select Supplier", Value = "" });
        }

        private void BindClient()
        {
            var ClientITems = new ListItemCollection();
            var items = sitecoreDatabase.SelectItems(Constants.FilteredClientItems) != null ? sitecoreDatabase.SelectItems(Constants.FilteredClientItems).ToList() : null;
            if (items != null)
            {
                foreach (var item in items)
                {
                    ListItem liBox = new ListItem();
                    liBox.Text = SitecoreFieldsHelper.GetValue(item, "Name");
                    liBox.Value = Guid.Parse(item.ID.ToString()).ToString();
                    if (liBox != null)
                    {
                        ClientITems.Add(liBox);
                    }

                }

                Partner.DataSource = ClientITems;
                Partner.DataTextField = "Text";
                Partner.DataValueField = "Value";
                Partner.DataBind();
                Partner.Items.Insert(0, new ListItem { Text = "Select Client", Value = "" });

            }

        }

        protected void btnSummaryReport_Click(object sender, EventArgs e)
        {
            GetSummaryReport();
        }

        private void GetSummaryReport()
        {
            try
            {
                iReportsService = new ReportsService();
                objSearchCriteria = new SearchCriteria();

                if (!string.IsNullOrEmpty(this.DateRange.Value))
                {
                    string[] dates = this.DateRange.Value.ToString().Split('-');
                    objSearchCriteria.DateRange = InputDateFormatting(dates[0]);
                    objSearchCriteria.DateUpTo = InputDateFormatting(dates[1]);
                }
                else
                {
                    objSearchCriteria.DateRange = null;
                    objSearchCriteria.DateUpTo = null;
                }

                if (!string.IsNullOrEmpty(this.ddlOracleFile.SelectedValue))
                    objSearchCriteria.OracleFileID =int.Parse(this.ddlOracleFile.SelectedValue.ToString());

                if (!string.IsNullOrEmpty(this.Provider.SelectedValue))
                    objSearchCriteria.Provider = this.Provider.SelectedValue;

                if (!string.IsNullOrEmpty(this.Partner.SelectedValue))
                    objSearchCriteria.Partner = this.Partner.SelectedValue;

                ProviderSummaryReport = iReportsService.GetARSummaryReport(objSearchCriteria);

                BindGrid();

            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.AdminPortal, ex, null);
                throw;
            }

        }

        private void BindGrid()
        {
            if (ProviderSummaryReport != null && ProviderSummaryReport.Count > 0)
            {
                ProviderTotalOfRecords = ProviderSummaryReport.Count;
            }
            else
            {
                ProviderSummaryReport = new List<FinanceSummaryReport>();
                ProviderTotalOfRecords = 0;
            }

            this.ProviderResultGridView.DataSource = ProviderSummaryReport;
            this.ProviderResultGridView.DataBind();


            var currencySummaryReport = ProviderSummaryReport.GroupBy(u => u.CurrencyCode)
                .Select(group => new { CurrencyCode = group.Key, Amount = group.Sum(lst => lst.Amount) })
                .ToList();

            CurrencyTotalOfRecords = currencySummaryReport.Count;

            this.CurrencyResultGridView.DataSource = currencySummaryReport;
            this.CurrencyResultGridView.DataBind();

        }

        private DateTime InputDateFormatting(string date)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(date))
                {
                    return default(DateTime);
                }
                else
                {
                    DateTime tmpDateTime;
                    try
                    {
                        tmpDateTime = DateTime.Parse(date);
                        return tmpDateTime;
                    }
                    catch
                    {
                        string[] words = date.Split('/');
                        string newstring = words[1] + "/" + words[0] + "/" + words[2];
                        DateTime dt = Convert.ToDateTime(newstring);
                        return dt;
                    }
                }
            }
            catch
            {
                return default(DateTime);
            }

        }

        private void BindOracleFile()
        {
            try
            {
                iReportsService = new ReportsService();

                List<OracleFileInfo> lstOracleFileInfo = iReportsService.GetOracleFileInfo();

                if (lstOracleFileInfo == null || lstOracleFileInfo.Count == 0)
                    lstOracleFileInfo = new List<OracleFileInfo>();

                ddlOracleFile.DataSource = lstOracleFileInfo.Where(lst=>lst.OracleFileType=="AR");
                ddlOracleFile.DataTextField = "OracleFileName";
                ddlOracleFile.DataValueField = "SequenceNo";
                ddlOracleFile.DataBind();
                ddlOracleFile.Items.Insert(0, new ListItem { Text = "Select File Name", Value = "" });

               

            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.AdminPortal, ex, null);
                throw;
            }
        }

        protected void ProviderResultGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ProviderResultGridView.PageIndex = e.NewPageIndex;
            BindGrid();
        }

        protected void CurrencyResultGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CurrencyResultGridView.PageIndex = e.NewPageIndex;
            BindGrid();

        }
    }
}