﻿
using Affinion.LoyaltyBuild.Model.Provider;
namespace Affinion.LoyaltyBuild.Api.Search.Data
{
    /// <summary>
    /// Provider info
    /// </summary>
    public class ProviderInfo : IProvider
    {
        /// <summary>
        /// Provider Id
        /// </summary>
        public string ProviderId { get; set; }

        /// <summary>
        /// SitecoreItemId
        /// </summary>
        public string SitecoreItemId { get; set; }

        /// <summary>
        /// provider type
        /// </summary>
        public ProviderType ProviderType { get; set; }

        /// <summary>
        /// IntroductionImage
        /// </summary>
        public string IntroductionImage { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Town
        /// </summary>
        public string Town { get; set; }

        /// <summary>
        /// Overview
        /// </summary>
        public string Overview { get; set; }

        /// <summary>
        /// Star Ranking
        /// </summary>
        public string StarRanking { get; set; }

        
    }
}
