﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.EntitiesV2;
using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.Common.Utilities;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Pages
{
    public partial class TransferPaymentPage : System.Web.UI.Page
    {
        private string autoSettleFlag;

        #region Public properties
        public string GatewayUrl { get; set; }
        public string MerchantId { get; set; }
        public string Secret { get; set; }
        public string CurrentTimeStamp { get; set; }
        public string OrderGuidId { get; set; }
        public string Account { get; set; }
        public string PaymentCurrency { get; set; }
        public string AmountToPay { get; set; }
        public string ShaHash { get; set; }
        public string ResponseUrl { get; set; }
        public string AnythingElse { get; set; }
        public string AutoSettleFlag
        {
            get
            {
                return autoSettleFlag;
            }
            set
            {
                autoSettleFlag = string.Equals(value, "True") ? "1" : "0";
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            InitializeSettings();
        }

        /// <summary>
        /// Initialize POST parameters to load payment gateway
        /// </summary>
        /// <param name="paymentRequest">Current payment request with order details</param>
        private void InitializeSettings()
        {
            PaymentMethod paymentMethod = PaymentMethod.SingleOrDefault(x => x.Name == Constants.OfflinePortalPaymentType);
            var paymentMethodProperties = paymentMethod.PaymentMethodProperties;
            GatewayUrl = paymentMethodProperties.FirstOrDefault(x => x.DefinitionField.Name == "GatewayUrl").Value;
            MerchantId = paymentMethodProperties.FirstOrDefault(x => x.DefinitionField.Name == "MerchantId").Value;
            Secret = paymentMethodProperties.FirstOrDefault(x => x.DefinitionField.Name == "Secret").Value;
            ResponseUrl = paymentMethodProperties.FirstOrDefault(x => x.DefinitionField.Name == "ResponseUrl").Value;
            Account = paymentMethodProperties.FirstOrDefault(x => x.DefinitionField.Name == "Account").Value;
            AutoSettleFlag = paymentMethodProperties.FirstOrDefault(x => x.DefinitionField.Name == "AutoSettleFlag").Value;
            CurrentTimeStamp = PaymentGatewayHelper.GetNowTimestamp();
            OrderGuidId = Request.QueryString["guid"];
            PaymentCurrency = Request.QueryString["cid"];
            ///Amount in cents as per Realex guide. Removed unwanted decimal points
            decimal tmpAmount;
            decimal.TryParse(Request.QueryString["amt"], out tmpAmount);
            AmountToPay = (tmpAmount * 100).ToString().Split('.')[0];
            ShaHash = PaymentGatewayHelper.GetHash(string.Format("{0}.{1}", PaymentGatewayHelper.GetHash(string.Format("{0}.{1}.{2}.{3}.{4}", CurrentTimeStamp, MerchantId, OrderGuidId, AmountToPay, PaymentCurrency)), Secret));
            AnythingElse = "TransferSubPayment_" + Request.QueryString["olid"] + "_" + OrderGuidId;
            SetSession(OrderGuidId, AmountToPay, AnythingElse);
        }

        private void SetSession(string oderId, string amount, string responseOther)
        {
            Session["ResponseOrderId"] = oderId;
            Session["ResponseAmount"] = amount;
            Session["ResponseOther"] = responseOther;
        }

    }
}