﻿namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal
{
    //<summary>
    /// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
    /// PROPRIETARY INFORMATION The information contained herein (the 
    /// 'Proprietary Information') is highly confidential and proprietary to and 
    /// constitutes trade secrets of Affinion International. The Proprietary Information 
    /// is for Affinion International use only and shall not be published, 
    /// communicated, disclosed or divulged to any person, firm, corporation or 
    /// other legal entity, directly or indirectly, without the prior written 
    /// consent of Affinion International.
    ///
    /// Source File:           ResourceLinkSubLayout.cs
    /// Sub-system/Module:     Affinion.LoyaltyBuild.Web
    /// Description:           Used to Bind data to ResourceLinkSubLayout subLayout
    /// </summary>
    #region Using Directives
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    #endregion

    public partial class ResourceLinkSubLayout : BaseSublayout
    {
        private List<Item> list;

        /// <summary>
        /// Bind destination items to  datasource of ResourceRepeater
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            try
            {
                Item item = Sitecore.Context.Item;
                Sitecore.Data.Fields.MultilistField multilistField = item.Fields["ResourceField"];
                if (multilistField != null)
                {
                    this.list = new List<Item>();
                    foreach (Item ResourceItem in multilistField.GetItems())
                    {
                        list.Add(ResourceItem);
                    }
                    ResourceRepeater.DataSource = list;
                    ResourceRepeater.DataBind();
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, null);
                throw;
            }
        }

        /// <summary>
        /// Gets the css class depend on the row index
        /// </summary>
        /// <param name="index">The row index</param>
        /// <returns>Returns the css class name</returns>
        protected string GetStyleClass(int index)
        {
            if(list != null && index == 0)
            {
                return "no-margin";
            }
            else if(list != null && index == (list.Count - 1))
            {
                return "border-none";
            }
            else
            {
                return string.Empty;
            }
        }
    }
}