﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.ViewModels
{
    public class PromotionInfo
    {
        /// <summary>
        /// CampaignItemId
        /// </summary>
        public int CampaignItemId { get; set; }

        /// <summary>
        /// CampaignName
        /// </summary>
        public string CampaignName { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Applicable Variant Skus
        /// </summary>
        public List<PromotionProduct> VariantSkus { get; set; }
    }

    public class PromotionProduct
    {
        /// <summary>
        /// Promotion Product Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Variant Sku of the product
        /// </summary>
        public string VariantSku { get; set; }
    }
}