﻿
/*<summary>
Description: Stored procedure to retrieve invalid Login count
</summary>*/



CREATE PROCEDURE [dbo].[LB_SelectInvalidLogAttemptCount]
       @UserId    varchar(200)       
AS
BEGIN 
     SET NOCOUNT ON 

     SELECT [FailedPasswordAttemptCount]            
     FROM   [dbo].[aspnet_Membership]  
     WHERE  
            UserId = @UserId

END 

