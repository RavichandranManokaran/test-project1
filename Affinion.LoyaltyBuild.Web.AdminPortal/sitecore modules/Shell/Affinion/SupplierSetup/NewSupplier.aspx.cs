﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           NewSupplier.aspx.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.AdminPortal
/// Description:           WebForm for SupplierSetup.Master
/// </summary>

#region Using Directives

using Affinion.Loyaltybuild.Security;
using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Security.AccessControl;
using Sitecore.SecurityModel;
using System;
using System.Linq;
using System.Text.RegularExpressions;

#endregion

namespace Affinion.LoyaltyBuild.AdminPortal.sitecore_modules.Shell.Affinion.SupplierSetup
{
    public partial class NewSupplier : System.Web.UI.Page
    {
        #region private variables

        /// <summary>
        /// master database
        /// </summary>
        private Database masterDb = Sitecore.Configuration.Factory.GetDatabase(Constants.MasterDatabase);

        /// <summary>
        /// fetch the latest parent item path
        /// </summary>
        private string latestPath = string.Empty;
        #endregion

        #region private constant variable

        /// <summary>
        /// Query to get Supplier Types
        /// </summary>
        private const string SupplierTypesQuery = @"fast:/sitecore/content/#admin-portal#/#supplier-setup#/global/#_supporting-content#/#supplier-types#//*[@@templatename='SupplierType' and @IsActive='1']";

        /// <summary>
        /// Query to get Supplier Groups
        /// </summary>
        private const string SupplierGroupsQuery = @"fast:/sitecore/content/#admin-portal#/#supplier-setup#/global/#_supporting-content#/groups//*[@@templatename='SupplierGroup']";

        /// <summary>
        /// Query to get Filtered Supplier Groups
        /// </summary>
        private const string SupplierGroupsFilterQuery = @"/sitecore/content/#admin-portal#/#supplier-setup#/global/#_supporting-content#/groups//*[@@templatename='SupplierGroup' and @SuperGroup='{0}']";

        /// <summary>
        /// Query to get Supplier Super Groups
        /// </summary>
        private const string SupplierSuperGroupsQuery = @"fast:/sitecore/content/#admin-portal#/#supplier-setup#/global/#_supporting-content#/#super-groups#//*[@@templatename='SupplierSuperGroup']";

        /// <summary>
        /// Path of Supplier Setup Item (Suppliers created here under supplier type)
        /// </summary>
        private const string SupplierSetupPath = "/sitecore/content/admin-portal/supplier-setup";

        /// <summary>
        /// Path of Supplier Type Items
        /// </summary>
        private const string SupplierTypesPath = "/sitecore/content/admin-portal/supplier-setup/global/_supporting-content/supplier-types";

        /// <summary>
        /// Path of Supplier Type Branch Template
        /// </summary>
        private const string BranchTemplatePathSuppllierType = "/sitecore/templates/Branches/Affinion/Supplier Setup/SupplierType";

        /// <summary>
        /// Path of Supplier Branch Template
        /// </summary>
        private const string BranchTemplatePathSupplier = "/sitecore/templates/Branches/Affinion/Supplier Setup/Supplier";

        /// <summary>
        /// Path of Supplier Setup Media Library folder location
        /// </summary>
        private const string SupplierSetupMediaFolder = "/sitecore/media library/Affinion/AdminPortal/Supplier Setup";

        /// <summary>
        /// Super group folder path
        /// </summary>
        private const string SuperGroupPath = @"/sitecore/content/admin-portal/supplier-setup/global/_supporting-content/super-groups/{0}";

        /// <summary>
        /// Group folder path
        /// </summary>
        private const string GroupPath = @"/sitecore/content/admin-portal/supplier-setup/global/_supporting-content/groups/{0}";

        /// <summary>
        /// Supplier type folder path
        /// </summary>
        private const string SupplierTypeItemPath = @"/sitecore/content/admin-portal/supplier-setup/global/_supporting-content/supplier-types/{0}";

        /// <summary>
        /// Partial contacts folder path
        /// </summary>
        private const string PartialContactsPath = "/_supporting-content/contacts";

        /// <summary>
        /// Supplier required validation key
        /// </summary>
        private const string SupplierRequiredValidationKey = "SupplierRequiredValidationKey";

        /// <summary>
        /// Supplier name is required validation message
        /// </summary>
        private const string SupplierRequiredValidationText = "Supplier name is required!";

        /// <summary>
        /// Special characters validation key
        /// </summary>
        private const string SpecialCharactersValidationKey = "SpecialCharactersValidationKey";

        /// <summary>
        /// Special characters validation message
        /// </summary>
        private const string SpecialCharactersValidationText = @"Special characters except '&', '(', ')', '+', '-' are not allowed. Please enter a valid name!";

        /// <summary>
        /// Supplier type existence validation key
        /// </summary>
        private const string SupplierTypeExistenceValidationKey = "SupplierTypeExistenceValidationKey";

        /// <summary>
        /// Supplier type existence validation message
        /// </summary>
        private const string SupplierTypeExistenceValidationText = "Please create the supplier type first before setting up a supplier!";

        /// <summary>
        /// Supplier type required validation key
        /// </summary>
        private const string SupplierTypeRequiredValidationKey = "SupplierTypeRequiredValidationKey";

        /// <summary>
        /// Supplier group required validation key
        /// </summary>
        private const string SupplierGroupRequiredValidationKey = "SupplierGroupRequiredValidationKey";

        /// <summary>
        /// Supplier type is required validation message
        /// </summary>
        private const string SupplierTypeRequiredValidationText = "Supplier type is required!";

        /// <summary>
        /// Supplier group is required validation message
        /// </summary>
        private const string SupplierGroupRequiredValidationText = "Supplier group is required!";

        /// <summary>
        /// Supplier name existence validation message
        /// </summary>
        private const string SupplierNameExistenceValidationText = "Supplier name \"{0}\" already exist for the \"{1}\" supplier type. Please enter a different supplier name!";

        /// <summary>
        /// Super group text
        /// </summary>
        private const string SuperGroupText = "SuperGroup";

        /// <summary>
        /// Group text
        /// </summary>
        private const string GroupText = "Group";

        /// <summary>
        /// Is active text
        /// </summary>
        private const string IsActiveText = "IsActive";

        /// <summary>
        /// Number one text
        /// </summary>
        private const string OneText = "1";

        /// <summary>
        /// ID text
        /// </summary>
        private const string IdText = "ID";

        /// <summary>
        /// Name text
        /// </summary>
        private const string NameText = "Name";

        /// <summary>
        /// "Group" Folder Name
        /// </summary>
        private const string GroupFolderName = "Group";

        /// <summary>
        /// "Independent" Folder Name
        /// </summary>
        private const string IndependentFolderName = "Independent";

        private const string AtoIFolder = "a---i";

        private const string JtoRFolder = "j---r";

        private const string StoZFolder = "s---z";

        #endregion

        #region Protected Methods

        /// <summary>
        /// Page load function
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    /// Load supplier setup page dropdown data
                    LoadSupplierSetupPageDropdown();
                }

                var user = Sitecore.Context.User;

                if (!IsAuthorized(user))
                {
                    /// Unauthorized Access Exception
                    throw new UnauthorizedAccessException();
                }
            }
            catch (UnauthorizedAccessException exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                Response.Redirect(Sitecore.Context.Site.LoginPage);
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                throw;
            }
        }

        /// <summary>
        /// Method use to load Supplier Groups by selected Supplier Super Group Dropdownlist
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DropDownListSupplierSuperGroupSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                /// Get Sitecore Master DB
                //Database masterDb = Sitecore.Configuration.Factory.GetDatabase(Constants.MasterDatabase);

                /// local variables
                string supplierSuperGroupId = DropDownListSupplierSuperGroup.SelectedValue.Trim();
                /// Get Supplier Group items
                var supplierGroupsItems = masterDb.SelectItems(BuildQuery(SupplierGroupsFilterQuery, supplierSuperGroupId));

                /// Extract Supplier Group Filtered Items
                var supplierGroupsFilterList = (from item in supplierGroupsItems
                                                orderby item.DisplayName
                                                select item.DisplayName).ToList();

                /// Set Datasource for Dropdown list
                DropDownListSupplierGroup.DataSource = supplierGroupsFilterList;
                /// Do data binding of Dropdown list
                DropDownListSupplierGroup.DataBind();
                /// Set Default display name for Dropdown list
                DropDownListSupplierGroup.Items.Insert(0, Constants.PleaseSelectText);
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, this);
                throw;
            }
        }

        /// <summary>
        /// Calls when button clicks and create Supplier portal and related information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ButtonCreateClick(object sender, EventArgs e)
        {
            try
            {
                /// Create item in sitecore
                CreateItem();
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, this);
                throw;
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Method use to load Supplier Setup Page Dropdownlists
        /// </summary>
        private void LoadSupplierSetupPageDropdown()
        {
            /// Get Sitecore Master DB
            //Database masterDb = Sitecore.Configuration.Factory.GetDatabase(Constants.MasterDatabase);

            /// Extract Supplier Type Items
            var supplierTypeItems = masterDb.SelectItems(SupplierTypesQuery);
            /// Extract Supplier Super Group Items
            var supplierSuperGroupItems = masterDb.SelectItems(SupplierSuperGroupsQuery);

            /// Populate Supplier Type List
            var supplierTypeNameList = (from item in supplierTypeItems
                                        orderby item.DisplayName
                                        select item.DisplayName).ToList();

            /// Populate Supplier Super Group List
            var supplierSuperGroupNameList = (from item in supplierSuperGroupItems
                                              orderby item.DisplayName
                                              select new { Name = item.DisplayName, ID = item.ID }).ToList();

            /// Set Datasource for Dropdown lists
            DropDownListSuppliertype.DataSource = supplierTypeNameList;
            DropDownListSupplierSuperGroup.DataSource = supplierSuperGroupNameList;

            /// Set data value and data text fields
            DropDownListSupplierSuperGroup.DataValueField = IdText;
            DropDownListSupplierSuperGroup.DataTextField = NameText;

            /// Do data binding of Dropdown lists
            DropDownListSuppliertype.DataBind();
            DropDownListSupplierSuperGroup.DataBind();

            /// Set Default display name for Dropdown lists
            DropDownListSuppliertype.Items.Insert(0, Constants.PleaseSelectText);
            DropDownListSupplierSuperGroup.Items.Insert(0, Constants.PleaseSelectText);
            DropDownListSupplierGroup.Items.Insert(0, Constants.PleaseSelectText);

            /// loading supplier group Dropdown list
            LoadSupplierGroup();
            ///


        }
        /// <summary>
        /// Method use to load Supplier Group Dropdownlists 
        /// </summary>
        private void LoadSupplierGroup()
        {
            /// supplier  group dropdown loding.
            string supplierSuperGroupId = DropDownListSupplierSuperGroup.SelectedValue.Trim();
            var supplierGroupsItems = masterDb.SelectItems(BuildQuery(SupplierGroupsFilterQuery, supplierSuperGroupId));

            /// Extract Supplier Group Filtered Items
            var supplierGroupsFilterList = (from item in supplierGroupsItems
                                            orderby item.DisplayName
                                            select item.DisplayName).ToList();

            DropDownListSupplierGroup.DataSource = supplierGroupsFilterList;
            /// Do data binding of Dropdown list
            DropDownListSupplierGroup.DataBind();
            /// Set Default display name for Dropdown list
            DropDownListSupplierGroup.Items.Insert(0, Constants.PleaseSelectText);
            ////
        }

        /// <summary>
        /// Will create Group and Independance folder structure
        /// </summary>
        private void CreateBaseFolderStructure(Database database,string supplierName)
        {
            try
            {
                Item groupPathItem = database.GetItem(string.Concat(SupplierSetupPath, Constants.SlashText, supplierName, Constants.SlashText, GroupFolderName));

                ///Create new group folder if group foler does not exist
                if (groupPathItem == null)
                {
                    ItemHelper.AddFolder(masterDb, string.Concat(SupplierSetupPath, Constants.SlashText, supplierName, Constants.SlashText), GroupFolderName);
                }


                groupPathItem = database.GetItem(string.Concat(SupplierSetupPath, Constants.SlashText, supplierName, Constants.SlashText, GroupFolderName, Constants.SlashText, AtoIFolder));
                // Create individual folders (A-I, J-R, S-Z)
                if (groupPathItem == null)
                {
                    ItemHelper.AddFolder(masterDb, string.Concat(SupplierSetupPath, Constants.SlashText, supplierName, Constants.SlashText, GroupFolderName, Constants.SlashText), AtoIFolder);
                }

                groupPathItem.Editing.BeginEdit();
                groupPathItem.Appearance.DisplayName = "A - I";
                groupPathItem.Editing.EndEdit();

                groupPathItem = database.GetItem(string.Concat(SupplierSetupPath, Constants.SlashText, supplierName, Constants.SlashText, GroupFolderName, Constants.SlashText, JtoRFolder));
                // Create individual folders (A-I, J-R, S-Z)
                if (groupPathItem == null)
                {
                    ItemHelper.AddFolder(masterDb, string.Concat(SupplierSetupPath, Constants.SlashText, supplierName, Constants.SlashText, GroupFolderName, Constants.SlashText), JtoRFolder);                    
                }

                groupPathItem.Editing.BeginEdit();
                groupPathItem.Appearance.DisplayName = "J - R";
                groupPathItem.Editing.EndEdit();

                groupPathItem = database.GetItem(string.Concat(SupplierSetupPath, Constants.SlashText, supplierName, Constants.SlashText, GroupFolderName, Constants.SlashText, StoZFolder));
                // Create individual folders (A-I, J-R, S-Z)
                if (groupPathItem == null)
                {
                    ItemHelper.AddFolder(masterDb, string.Concat(SupplierSetupPath, Constants.SlashText, supplierName, Constants.SlashText, GroupFolderName, Constants.SlashText), StoZFolder);
                }

                groupPathItem.Editing.BeginEdit();
                groupPathItem.Appearance.DisplayName = "S - Z";
                groupPathItem.Editing.EndEdit();

                // Check IndependentFolderName
                groupPathItem = database.GetItem(string.Concat(SupplierSetupPath, Constants.SlashText, supplierName, Constants.SlashText, IndependentFolderName));

                ///Create new group folder if group foler does not exist
                if (groupPathItem == null)
                {
                    ItemHelper.AddFolder(masterDb, string.Concat(SupplierSetupPath, Constants.SlashText, supplierName, Constants.SlashText), IndependentFolderName);
                }
            }
            catch (Exception)
            {

            }
        }
        /// <summary>
        /// Method use to create item in sitecore
        /// </summary>
        private void CreateItem()
        {
            using (new SecurityDisabler())
            {
                LabelValidation.InnerText = string.Empty;

                /// local variables            
                string supplierDisplayName = TextBoxSupplierName.Text.Trim();
                string suppllierTypeDisplayName = DropDownListSuppliertype.SelectedValue.Trim();
                Item supplierSuperGroup = masterDb.GetItem(DropDownListSupplierSuperGroup.SelectedValue);
                string supplierSuperGroupDisplayName = ((supplierSuperGroup == null) ? string.Empty : supplierSuperGroup.DisplayName);
                string supplierGroupDisplayName = DropDownListSupplierGroup.SelectedValue.Trim();
                
                /// Validation for Create Supplier Setup                    
                if (!ValidateNewSupplier(supplierDisplayName))
                    return;

                CreateBaseFolderStructure(masterDb, suppllierTypeDisplayName);

                /// Get master database
                //Database masterDb = Sitecore.Configuration.Factory.GetDatabase(Constants.MasterDatabase);

                /// Get supplier type item name
                string supplierTypeItemName = GetSupplierTypeItemName(masterDb, suppllierTypeDisplayName);

                /// Supplier type path
                string newSupplierTypePath = string.Concat(SupplierSetupPath, Constants.SlashText, supplierTypeItemName);

                ///set the supplier type path as the latest path
                latestPath = newSupplierTypePath;

                ///Supplier group path
                //string newSupplierGroupPath = string.Concat(newSupplierTypePath, Constants.SlashText, );

                if (IsNewSupplier(masterDb, supplierDisplayName, newSupplierTypePath))
                {
                    LabelValidation.InnerText = string.Format(SupplierNameExistenceValidationText, supplierDisplayName, suppllierTypeDisplayName);
                    return;
                }

                /// Create supplier with supplier type and supplier
                CreateSupplierSetup(masterDb, newSupplierTypePath, suppllierTypeDisplayName, supplierSuperGroupDisplayName, supplierGroupDisplayName, supplierDisplayName);

                /// Create media items for supplier setup
                CreateMediaItems(string.Concat(SupplierSetupMediaFolder, Constants.SlashText, suppllierTypeDisplayName), supplierDisplayName);

                /// Update the newly added supplier with selected items
                UpdateSupplierItem(masterDb, latestPath);

                /// Update supplier type icon: Set to active at begining
                UpdateSupplierTypeIcon(masterDb, newSupplierTypePath);

                /// call CheckReadAccess method to set delete access rigts 
                string itemPath = newSupplierTypePath + Constants.SlashText + SitecoreItemHelper.SetItemName(supplierDisplayName) + PartialContactsPath;
                SetAccessRights.CheckDeleteAccess(itemPath);

                Response.Redirect(Constants.LaunchpadPath);
            }
        }

        /// <summary>
        /// Method use to validates whether all requirements are there for creating supplier
        /// </summary>
        /// <param name="supplierDisplayName"></param>
        /// <returns>Returns True or False</returns>
        private bool ValidateNewSupplier(string supplierDisplayName)
        {
            /// Validation for white spaces
            if (supplierDisplayName.Length == 0)
            {
                LabelValidation.InnerText = LanguageReader.GetText(SupplierRequiredValidationKey, SupplierRequiredValidationText);
                return false;
            }

            /// Validation which allows only alphanumeric and space
            Regex regex = new Regex(Constants.SpecialCharactorValidationRegex);

            if (!regex.IsMatch(supplierDisplayName))
            {
                LabelValidation.InnerText = LanguageReader.GetText(SpecialCharactersValidationKey, SpecialCharactersValidationText);
                return false;
            }
            /// Validation whether the supplier type dropdownlist contain data                  
            if (DropDownListSuppliertype.Items.Count < 2)
            {
                LabelValidation.InnerText = LanguageReader.GetText(SupplierTypeExistenceValidationKey, SupplierTypeExistenceValidationText);
                return false;
            }
            /// Validate whether the Supplier Type is selected
            if (DropDownListSuppliertype.SelectedIndex == 0)
            {
                LabelValidation.InnerText = LanguageReader.GetText(SupplierTypeRequiredValidationKey, SupplierTypeRequiredValidationText);
                return false;
            }

            if (DropDownListSupplierSuperGroup.SelectedIndex > 0 && DropDownListSupplierGroup.SelectedIndex == 0)
            {
                LabelValidation.InnerText = LanguageReader.GetText(SupplierGroupRequiredValidationKey, SupplierGroupRequiredValidationText);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Method use to convert supplier type display name to item name
        /// </summary>
        /// <param name="database"></param>
        /// <param name="suppllierTypeDisplayName"></param>
        /// <returns>Returns supplier type item name</returns>
        private string GetSupplierTypeItemName(Database database, string suppllierTypeDisplayName)
        {
            /// Convert suppllier type display name to item name
            var SupplierTypeChildren = database.Items[SupplierTypesPath].GetChildren();
            var supplierTypeItemName = (from item in SupplierTypeChildren
                                        where item.DisplayName.Equals(suppllierTypeDisplayName)
                                        select item.Name).FirstOrDefault().ToString();

            return supplierTypeItemName;
        }

        /// <summary>
        /// Method use to convert supplier display name to item name
        /// </summary>
        /// <param name="database"></param>
        /// <param name="supplierDisplayName"></param>
        /// <param name="newSupplierTypePath"></param>
        /// <returns>Returns supplier item name</returns>
        private bool IsNewSupplier(Database database, string supplierDisplayName, string newSupplierTypePath)
        {
            /// Convert suppllier display name to item name
            var supplierTypePath = database.Items[newSupplierTypePath];
            if (supplierTypePath == null)
            {
                return false;
            }
            else
            {
                var SupplierChildren = supplierTypePath.GetChildren();
                var supplierItemNameList = (from item in SupplierChildren
                                            where item.DisplayName.Equals(supplierDisplayName)
                                            select item.Name).ToList();

                /// Format suppllier name for first time only
                if (supplierItemNameList.Count == 0)
                {
                    return false;
                }
                else
                    return true;
            }
        }

        /// <summary>
        /// Method use to create supplier setup with supplier type and supplier
        /// </summary>
        /// <param name="database"></param>
        /// <param name="supplierTypePath"></param>
        /// <param name="suppllierType"></param>
        /// <param name="supplier"></param>
        private void CreateSupplierSetup(Database database, string supplierTypePath, string suppllierType, string supplierSuperGroup, string supplierGroup, string supplier)
        {
            Item parentItem = database.GetItem(SupplierSetupPath);
            BranchItem branchItem = database.GetItem(BranchTemplatePathSuppllierType);
            string groupPath = null;
            /// Create suppllier type item if suppllier type is not exist
            Item supplierTypePathItem = database.GetItem(supplierTypePath);
            if (supplierTypePathItem == null)
                parentItem.Add(suppllierType, branchItem);

            if (!string.IsNullOrEmpty(supplierSuperGroup))
            {
                 groupPath = string.Concat(supplierTypePath, Constants.SlashText, GroupFolderName);
                Item groupPathItem = database.GetItem(groupPath);
                ///Create new group folder if group foler does not exist
                if (groupPathItem == null)
                {
                    ItemHelper.AddFolder(masterDb, supplierTypePath, GroupFolderName);
                }

                ///Convert the super group name to the required format
                string supplierSuperGroupName = supplierSuperGroup.ToLower().Replace(" ", "-");

                // Get Folder (A---I)
                string groupParentFolder = GetGroupNameForSupplier(supplierGroup, groupPath, database);

                string supplierSuperGroupPath = string.Concat(groupPath, Constants.SlashText,groupParentFolder,Constants.SlashText, supplierSuperGroupName);
               
                Item superGroupPathItem = database.GetItem(supplierSuperGroupPath);

                ///Create new super group if super group does not exist
                //if (superGroupPathItem == null)
                //{
                //    ItemHelper.AddFolder(masterDb, string.Concat(groupPath, Constants.SlashText, groupParentFolder), supplierSuperGroup);
                //}

                if (!string.IsNullOrEmpty(supplierGroup))
                {
                    ///convert group name to the required format
                    string supplierGroupName = supplierGroup.ToLower().Replace(" ", "-");
                    //string suppliergroupPath = string.Concat(supplierSuperGroupPath, Constants.SlashText, supplierGroupName);
                    string suppliergroupPath = string.Concat(supplierSuperGroupPath.Replace(supplierSuperGroupName, ""), supplierGroupName);
                    Item supplierGroupItem = database.GetItem(suppliergroupPath);

                    ///Create new supplier group if supplier group does not exist
                    if (supplierGroupItem == null)
                    {
                        //ItemHelper.AddFolder(masterDb, supplierSuperGroupPath, supplierGroup);
                        ItemHelper.AddFolder(masterDb, suppliergroupPath.Replace(supplierGroupName, ""), supplierGroupName);
                        supplierGroupItem = database.GetItem(suppliergroupPath);
                    }

                    ///Create the supplier from branch template
                    parentItem = supplierGroupItem;
                    latestPath = suppliergroupPath;
                }
            }
            else
            {

                if (!supplierGroup.Contains("Please"))
                {
                    string tempPath = string.Concat(supplierTypePath, Constants.SlashText, GroupFolderName, Constants.SlashText, supplierGroup);
                    string groupParentFolder = GetGroupNameForSupplier(supplierGroup, tempPath.Replace(supplierGroup, ""), database);

                    ///convert group name to the required format
                    string supplierGroupName = supplierGroup.ToLower().Replace(" ", "-");
                    //string suppliergroupPath = string.Concat(supplierTypePath, Constants.SlashText, GroupFolderName, Constants.SlashText, supplierGroup);
                    string suppliergroupPath = string.Concat(supplierTypePath, Constants.SlashText, GroupFolderName, Constants.SlashText, groupParentFolder, Constants.SlashText, supplierGroupName);
                    //string.Concat(supplierTypePath, Constants.SlashText, GroupFolderName); 
                    Item supplierGroupItem = database.GetItem(suppliergroupPath);

                    ///Create new supplier group if supplier group does not exist
                    if (supplierGroupItem == null)
                    {
                        ItemHelper.AddFolder(masterDb, suppliergroupPath.Replace(supplierGroupName, ""), supplierGroupName);
                        supplierGroupItem = database.GetItem(suppliergroupPath);

                        // Rename the Folder which newly created
                        supplierGroupItem.Editing.BeginEdit();
                        supplierGroupItem.Appearance.DisplayName = supplierGroup;
                        supplierGroupItem.Editing.EndEdit();
                    }

                    ///Create the supplier from branch template
                    parentItem = supplierGroupItem;
                    latestPath = suppliergroupPath;
                }
                else
                {
                    ///if supplier does not have a group or super group supplier is created under independent category
                    string supplierIndependentPath = string.Concat(supplierTypePath, Constants.SlashText, IndependentFolderName, Constants.SlashText, supplier[0]); 
                    //string.Concat(supplierTypePath, Constants.SlashText, IndependentFolderName);
                    Item supplierIndependentItem = database.GetItem(supplierIndependentPath);

                    ///Creat independent folder if folder does not exist
                    if (supplierIndependentItem == null)
                    {
                        ItemHelper.AddFolder(masterDb, supplierTypePath + "/" + IndependentFolderName, supplier[0].ToString().ToUpper());
                        supplierIndependentItem = database.GetItem(supplierIndependentPath);
                    }

                    //Create the supplier from branch template
                    parentItem = supplierIndependentItem;
                    latestPath = supplierIndependentPath;
                }
            }

            /// Create supplier item
            //parentItem = database.Items[SupplierTypePath];
            branchItem = database.GetItem(BranchTemplatePathSupplier);
            parentItem.Add(supplier, branchItem);

        }


        /// <summary>
        /// Will return the parent group foler name of a supplier
        /// </summary>
        private string GetGroupNameForSupplier(string supplier, string parentFolderPath, Database database)
        {
            string path = string.Empty;
            string firstLetter, lastLetter;

            try
            {
                int groupNameLenght = 0;
                foreach (Item item in database.GetItem(parentFolderPath).Children)
                {
                    groupNameLenght = item.DisplayName.Length;

                    // Check the first letter of item and group
                    // A - I   G
                    
                    firstLetter = item.DisplayName.Substring(0,1).ToString();
                    lastLetter =  item.DisplayName.Substring(groupNameLenght - 1, 1).ToString();

                    //if (string.Compare(firstLetter, supplier[0].ToString(), true) >= 0 && string.Compare(lastLetter, supplier[0].ToString(), true) <= 1)

                    // A-I  (A)
                    if (string.Compare(firstLetter, supplier[0].ToString(), true) == 0 || string.Compare(lastLetter, supplier[0].ToString(), true) == 0)
                    {
                        path = item.Name.ToString();
                    }
                    
                    if (string.Compare(firstLetter, supplier[0].ToString(), true) == -1 ) // A-I (B) -1
                    {
                        if(string.Compare(lastLetter, supplier[0].ToString(), true) >= 0 )
                        {
                            path = item.Name.ToString();
                        }                        
                    }

                    // A-I (B)    I B == 1
                    //if (string.Compare(firstLetter, supplier[0].ToString(), true) == -1 && string.Compare(lastLetter, supplier[0].ToString(), true) <= 1)
                    //{
                    //    path = item.Name.ToString();
                    //}                    
                }
            }
            catch (Exception)
            {
                
            }

            return path;
        }

        /// <summary>
        /// Method use to create media item in sitecore and renames display name
        /// </summary>
        /// <param name="parentFolderPath"></param>
        /// <param name="supplierDisplayName"></param>
        private static void CreateMediaItems(string parentFolderPath, string supplierDisplayName)
        {
            /// Create respective media folders
            ItemHelper.AddMeidiaFolder(parentFolderPath, supplierDisplayName);
        }

        /// <summary>
        /// Update the suppliers with selected items
        /// </summary>
        /// <param name="database">Database</param>
        /// <param name="parentPath">Parent item i.e. supplier type</param>
        private void UpdateSupplierItem(Database database, string parentPath)
        {
            /// Selected group name
            string groupName = SitecoreItemHelper.SetItemName(DropDownListSupplierGroup.SelectedItem.ToString().ToLower());
            /// Selected super group name
            string superGroupName = SitecoreItemHelper.SetItemName(DropDownListSupplierSuperGroup.SelectedItem.ToString().ToLower());
            /// Selected supplier type name
            string supplierTypeName = SitecoreItemHelper.SetItemName(DropDownListSuppliertype.SelectedItem.ToString().ToLower());

            /// Selected super group item
            Item superGroupItem = database.GetItem(BuildQuery(SuperGroupPath, superGroupName));
            /// Selected group item
            Item groupItem = database.GetItem(BuildQuery(GroupPath, groupName));
            /// Selected supplier type item
            Item supplierTypeItem = database.GetItem(BuildQuery(SupplierTypeItemPath, supplierTypeName));

            /// Newly added supplier to edit
            Item supplierItem = database.GetItem(parentPath + Constants.SlashText + SitecoreItemHelper.SetItemName(TextBoxSupplierName.Text.Trim().ToLower()));

            supplierItem.Editing.BeginEdit();

            /// Update supplier with selected super group
            if (superGroupItem != null)
            {
                Sitecore.Data.Fields.LookupField superGroupLookup = supplierItem.Fields[SuperGroupText];
                superGroupLookup.Value = superGroupItem.ID.ToString();
            }

            /// Update supplier with selected group
            if (groupItem != null)
            {
                Sitecore.Data.Fields.LookupField groupLookup = supplierItem.Fields[GroupText];
                groupLookup.Value = groupItem.ID.ToString();
            }

            /// Update supplier with selected supplier type
            if (supplierTypeItem != null)
            {
                Sitecore.Data.Fields.LookupField supplierTypeLookup = supplierItem.Fields[Constants.SupplierTypeText];
                supplierTypeLookup.Value = supplierTypeItem.ID.ToString();
            }

            supplierItem.Editing.EndEdit();
        }

        /// <summary>
        /// Build query to get items
        /// </summary>
        /// <param name="beginValue">Begining value</param>
        /// <param name="valueToAppend">Value to append</param>
        /// <returns>Returns concat query</returns>
        private string BuildQuery(string beginValue, string valueToAppend)
        {
            return string.Format(beginValue, valueToAppend);
        }

        /// <summary>
        /// Update supplier type icon as active at the begining
        /// </summary>
        /// <param name="database">Database</param>
        /// <param name="newSupplierTypePath">Path of the newly added supplier type path</param>
        private static void UpdateSupplierTypeIcon(Database database, string newSupplierTypePath)
        {
            /// Field name of the active status
            string activeFieldName = IsActiveText;
            /// Field value to set
            string defaultFieldValue = OneText;
            /// Get the newly added item
            Item supplierType = database.GetItem(newSupplierTypePath);
            /// Update the status field as active
            ItemHelper.UpdateFieldValue(supplierType, activeFieldName, defaultFieldValue);
            /// Update the icon of the item
            ItemHelper.ChangeIconField(supplierType, null, activeFieldName);
        }

        /// <summary>
        /// User Authentication
        /// </summary>
        /// <param name="user">Context User</param>
        /// <returns>Returns true if authenticated false otherwise</returns>
        private static bool IsAuthorized(Sitecore.Security.Accounts.User user)
        {
            /// Check if user authorized to access this page
            return SitecoreAccess.IsAllowed(user, AccessRight.ItemRead, Constants.SupplierSetupButtonPath, Constants.CoreDatabase);
        }

        #endregion
    }
}