﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model
{
    public class APInvoice:Invoice
    {
        public string AdultsCount { get; set; }
        public string ChildrenCount { get; set; }
        public string BookingStatus { get; set; }
        public string AccommodationType { get; set; }
        public string HotelName { get; set; }
        public DateTime RequestedDate { get; set; }
        public string RequestedBy { get; set; }
        public string OracleID { get; set; }
        public string OfferName { get; set; }
        public string ProviderName { get; set; }
        public DateTime FirstBookingConfDate { get; set; }
        public DateTime PaymentPaidDate { get; set; }
        public string OfferBased { get; set; }
        public int SupplierPaymentId { get; set; }
        public DateTime PaymentRequestedDate { get; set; }
        public int PaymentRequestStatus { get; set; }
        public int RequestWithinSixMonth { get; set; }
        public bool IsChecked { get; set; }

    }
}
