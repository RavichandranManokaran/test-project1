﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BreadcrumbSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal.BreadcrumbSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Sitecore.Data.Items" %>

<asp:Repeater ID="Breadcrumb" runat="server">

    <HeaderTemplate>
        <div class="col-md-12">
            <div class="breadcrumb-top">
                <ol class="breadcrumb">
    </HeaderTemplate>

    <ItemTemplate>
                <li>
                    <a href="<%#this.GetBreadcrumbLink((Item)Container.DataItem) %>">
                        <%# this.GetBreadcrumbTitle((Item)Container.DataItem) %>
                    </a>
                </li>
    </ItemTemplate>

    <FooterTemplate>
                <li class="active">
                    <%# this.GetBreadcrumbTitle(Sitecore.Context.Item) %>
                </li>
                </ol>  
            </div>
        </div>
    </FooterTemplate>

</asp:Repeater>
