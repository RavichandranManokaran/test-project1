﻿<%@ control language="C#" autoeventwireup="true" codebehind="OfflinePortalCommunicationHistroySublayout.ascx.cs" inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalCommunicationHistroySublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<%if(ValidationResponse != null){%> 
<div class="alert alert-<%= ValidationResponse.IsSuccess ? "success" : "danger" %>"> 
    <%= ValidationResponse.Message %> 
</div> 
<%} %>
<div class="col-sm-12">
    <div class="accordian-outer">
        <div class="panel-group" id="accordion">
            <div class="panel panel-default">
                <div class="panel-heading bg-primary">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse0" class="show-hide-list"><sc:text id="CommunicationHistoryLabel" field="CommunicationHistory" runat="server"/></a>
                    </h4>
                </div>
                <div id="collapse0" class="panel-collapse collapse in">
                    <div class="panel-body alert-info">
                        <div class="row">
                            <div class="col-sm-12">
                                <input type ="hidden" runat="server" ClientIdMode="Static" id="hdnSelectedIds" />
                                <button type="submit" ID="btn_regenerateClientEmail" onclick="SelectIds();" class="btn btn-default add-break font-size-12" runat="server" OnServerClick="btn_regenerateClientEmail_Click"><span class="glyphicon glyphicon-envelope"></span>&nbsp;&nbsp;<sc:text id="RegenerateclientEmailText" field="RegenerateclientEmailText" runat="server"/></button>
                                <button type="submit" ID="btn_regenerateProviderEmail" onclick="SelectIds();" class="btn btn-default add-break font-size-12" runat="server" OnServerClick="btn_regenerateProviderEmail_Click"><span class="glyphicon glyphicon-envelope"></span>&nbsp;&nbsp;<sc:text id="RegenerateProviderEmailText" field="RegenerateProviderEmailText" runat="server"/></button>
                                <button type="submit" class="btn btn-default add-break font-size-12"><span class="glyphicon glyphicon-phone"></span>&nbsp;&nbsp;<sc:text id="RegenerateSMSText" field="RegenerateSMSText" runat="server"/></button>
                            </div>
                        </div>
                        <div class="row margin-row">
                            <div class="table-responsive responsive-tb">
                                <table class="table tbl-calendar">
                                    <thead>
                                        <tr>
                                            <%--<th></th>--%>
                                            <th><sc:text id="MailTypeText" field="MailType" runat="server"/></th>
                                            <th><sc:text id="FromToText" field="FromTo" runat="server"/></th>
                                            <th><sc:text id="SentText" field="Sent" runat="server"/></th>
                                            <th><sc:text id="EmailSubjectText" field="EmailSubject" runat="server"/></th>
                                            <th><sc:text id="ResendText" field="Resend" runat="server"/></th>
                                        </tr>
                                    </thead>
                                    <%if(CommunicationHistoryDetails!=null){ %>
                                    <%foreach(var item in CommunicationHistoryDetails) {%>
                                    <tbody>
                                        <tr style="background-color: rgb(255, 255, 255);">
                                           <%-- <td> <input type="checkbox" class="commhistoryselect"/> </td>--%>
                                            <td data-label="Mail Type"><span><%=item.CommunicationType%></span></td>
                                            <td data-label="From To"><span><%=item.From %><br />
                                                <%=item.To %></span></td>
                                            <td data-label="Sent"><span><%=item.CreatedDate%></span></td>
                                            <td data-label="Email Subject"><span><%=item.Subject%></span></td>
                                            <td><a href="javascript:void(0)" onclick="CS.MailService.ReSendMail('<%=item.HistoryID %>')"> <span><sc:text id="Resend" field="Resend" runat="server"/></span></a></td>
                                           <%-- <td><asp:button Text="Resend" ID="btn_resend" runat="server" OnClick="btn_resend_Click" /></td>--%>
                                             
                                        </tr>
                                    </tbody>
                                    <%} %>
                                    <%} %>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 <div class="modal fade bs-example-modal-sm" id="dvmessage" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header  alert-info">
                    <h4 class="modal-title"><sc:text id="InformationText" field="InformationText" runat="server"/></h4>
                </div>
                <div class="modal-body">
                    <p id="msgcontent"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="Button3" class="btn btn-info" data-dismiss="modal"><sc:text id="OkButtonText" field="OkButtonText" runat="server"/></button>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
    $(function () {
        $("#btn_resend").click(function () {
            $("#hdnSelectedIds").val($(this).data("commid"))
        });
    })();
    function SelectIds() {
        $(".commhistoryselect:selected").each(function () {
            $("#hdnSelectedIds").val($("#hdnSelectedIds").val() + $(this).data("commid") + ",");
        });
        //alert($("#hdnSelectedIds").val());
    }
</script>