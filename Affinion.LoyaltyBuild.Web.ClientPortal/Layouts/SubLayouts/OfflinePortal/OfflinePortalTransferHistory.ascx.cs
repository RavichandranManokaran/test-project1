﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Order;
using Affinion.Loyaltybuild.BusinessLogic.Helper;
using Affinion.LoyaltyBuild.Common.SessionHelper;
using Affinion.LoyaltyBuild.Common;
using Sitecore.Data.Items;
using UCommerce.Api;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic;


namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    public partial class OfflinePortalTransferHistory : BaseSublayout
    {
        /// <summary>
        /// Page_Init
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Init(Object sender, EventArgs e)
        {
            BindGridViewHeaders(TransferHistoryGridView);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            OrderService orderService = ContainerFactory.Instance.GetInstance<OrderService>();
            List<TransferHistoryItem> transferhistoryData = new List<TransferHistoryItem>();

            int orderLineId;
            int.TryParse(this.QueryString("olid"), out orderLineId);
            transferhistoryData = orderService.GetBookingTransferHistory(orderLineId);
            TransferHistoryGridView.DataSource = transferhistoryData;
            TransferHistoryGridView.DataBind();
        }


        /// <summary>
        /// Bind Table Header
        /// </summary>
        private void BindGridViewHeaders(GridView gridView)
        {
            string supplierName = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "SupplierName", "Supplier Name");
            string arrivalDate = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "ArrivalDate", "Arrival Date");
            string departureDate = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "DepartureDate", "Departure Date");
            string noOfRooms = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "NoOfRooms", "No Of Rooms");
            string noOfNights = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "NoOfNights", "No Of Nights");
            string noOfPeople = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "NoOfPeople", "No Of People");
            string reservationDate = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "ReservationDate", "Reservation Date");

            BindBoundField(supplierName, "SupplierName", gridView);
            BindBoundField(arrivalDate, "ArrivalDate", gridView);
            BindBoundField(departureDate, "DepartureDate", gridView);
            BindBoundField(noOfRooms, "NoOfRooms", gridView);
            BindBoundField(noOfNights, "NoOfNights", gridView);
            BindBoundField(noOfPeople, "NoOfPeople", gridView);
            BindBoundField(reservationDate, "ReservationDate", gridView);

        }

        /// <summary>
        /// Bind Bound Field to grid
        /// </summary>
        /// <param name="headerText">Header Text</param>
        /// <param name="dataField">Field Value<Data Field/param>
        /// <param name="grid">Grid</param>
        private void BindBoundField(string headerText, string dataField, GridView grid)
        {
            BoundField boundField = new BoundField();
            boundField.HeaderText = headerText;
            boundField.DataField = dataField;
            grid.Columns.Add(boundField);
        }
    }
}