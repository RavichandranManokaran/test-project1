﻿CREATE TABLE [dbo].[LB_ArrivalDateTarget] (
    [ArrivalDateTargetId] INT      NOT NULL,
    [ArrivalFromDate]     DATETIME NOT NULL,
    [ArrivalToDate]       DATETIME NOT NULL,
    CONSTRAINT [PK_uCommerce_ArrivalDate] PRIMARY KEY CLUSTERED ([ArrivalDateTargetId] ASC),
    CONSTRAINT [FK_uCommerce_ArrivalDateTarget_uCommerce_Target] FOREIGN KEY ([ArrivalDateTargetId]) REFERENCES [dbo].[uCommerce_Target] ([TargetId])
);

