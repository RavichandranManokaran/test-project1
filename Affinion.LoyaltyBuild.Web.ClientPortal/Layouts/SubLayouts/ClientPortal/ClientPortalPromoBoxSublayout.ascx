﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientPortalPromoBoxSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal.ClientPortalPromoBoxSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Sitecore.Data.Items" %>


<div class="col-xs-12 col-sm-4">

    <div class="offers view view-first">
        <asp:Literal ID="Literal1" Text="Set the datasource: Required Fields: Title, Subtitle, Image" runat="server" />
       
            <sc:Link Field="Link" ID="Link" runat="server">

            <sc:Image Field="Image" alt="" CssClass="img-responsive img-enlarge" ID="Image" runat="server" />

            <div class="offers-inner">
                <h3 class="no-margin">
                    <sc:Text Field="Heading" ID="Text5" runat="server" />
                </h3>
                <h4 class="no-margin">
                    <sc:Text Field="SubHeading" ID="Text4" runat="server" />
                </h4>
            </div>

            <div class="mask">
                <h2>
                    <sc:Text Field="HoverHeading" ID="Text1" runat="server" />
                </h2>
                <p>
                    <sc:Text Field="HoverDescription1" ID="Text2" runat="server" />
                </p>
                <p>
                    <sc:Text Field="HoverDescription2" ID="Text3" runat="server" />
                </p>
            </div>
       </sc:Link>
    </div>
</div>

