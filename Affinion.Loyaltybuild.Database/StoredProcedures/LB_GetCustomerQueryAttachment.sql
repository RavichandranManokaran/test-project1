
Create PROCEDURE [dbo].[LB_GetCustomerQueryAttachment]
(
	@QueryId int
) 
AS
BEGIN
	SELECT	AttachmentId
			,QueryId
			,FileType
			,FileName
			,FileContent
	FROM [dbo].[LB_CustomerQuery_Attachments] 
	WHERE QueryId = @QueryId
END



GO


