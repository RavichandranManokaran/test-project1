﻿using Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal;
using Affinion.LoyaltyBuild.Web.ClientPortal.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using helper = Affinion.LoyaltyBuild.Web.ClientPortal.Helper;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Handlers
{
    /// <summary>
    /// Summary description for OfferAvailabilityAndPrice
    /// </summary>
    public class OfferAvailabilityAndPrice : IHttpHandler
    {
        private NameValueCollection _query;
        /// <summary>
        /// Process request
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context)
        {
            //Get Query String
            _query = HttpUtility.ParseQueryString(context.Request.Url.Query.ToLower());

            //Get Values from Query String
            string variantSkuList = _query["VariantSku"];
            List<string> variantSku = variantSkuList.Split(',').ToList<string>();
            DateTime dateFrom= Convert.ToDateTime(_query["CheckInDate"]);

            int tempVal = 0;
            if (Int32.TryParse(_query["Week"], out tempVal) && tempVal > 0)
                dateFrom = dateFrom.AddDays(7 * tempVal);

            int noOfNights=Convert.ToInt32(_query["NoOfNoghts"]);
            DateTime dateTo = dateFrom.AddDays(noOfNights);
            int currencyId;
            int.TryParse(_query["Currency"], out currencyId);

            List<PromotionProduct> filteredVariantSkus = new List<PromotionProduct>(); 
            //Filter bedbanks variant Skus
            foreach (var variant in variantSku)
            {
                var product = UCommerce.EntitiesV2.Product.FirstOrDefault(i => i.VariantSku == variant);
                if (product != null)
                    filteredVariantSkus.Add(new PromotionProduct { VariantSku = product.VariantSku, Name = product.Name });
            }

            //Get Offer availability
            var offersList = helper.HotelSearchHelper.GetOfferAvailabilityAndPriceByDate(variantSku, dateFrom.AddDays(-7), dateTo.AddDays(7), noOfNights, currencyId);

            context.Response.Write(LoadUserControl(offersList, filteredVariantSkus, dateFrom));
        }


        protected static string LoadUserControl(List<PromotionAvailabilityByDate> promotionAvailability, List<PromotionProduct> filteredVariantSkus,DateTime checkInDate)
        {
            using (Page page = new Page())
            {
                StringBuilder s = new StringBuilder();
                PlaceHolder f = new PlaceHolder();
                using (StringWriter writer = new StringWriter(CultureInfo.CurrentCulture))
                {
                    
                    var promotions = new List<PromotionInfo>();
                    foreach(var promotionId in promotionAvailability.Select(i => i.PromotionId).Distinct())
                    {
                        var campaign = UCommerce.EntitiesV2.CampaignItem.FirstOrDefault(i => i.CampaignItemId == promotionId);
                        var promotion = new PromotionInfo()
                        {
                            CampaignItemId = campaign.CampaignItemId,
                            CampaignName = campaign.Name,
                            Description = campaign.Name
                        };
                        promotion.VariantSkus = filteredVariantSkus
                            .Where(i => promotionAvailability.Any(j => j.PromotionId == campaign.CampaignItemId &&  i.VariantSku == j.VariantSku))
                            .ToList();
                        promotions.Add(promotion);
                    }

                    OfflinePortalPromotions promotionControl = (OfflinePortalPromotions)page.LoadControl("~/Layouts/SubLayouts/OfflinePortal/OfflinePortalPromotions.ascx");
                    promotionControl.PromotionAvailability = promotionAvailability;
                    promotionControl.CheckinDate = checkInDate;
                    promotionControl.Promotions = promotions;

                    f.Controls.Add(promotionControl);
                    page.Controls.Add(f);
                    HttpContext.Current.Server.Execute(page, writer, false);
                    s.Clear();
                    s.Append(writer.ToString());
                }
                return s.ToString();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}