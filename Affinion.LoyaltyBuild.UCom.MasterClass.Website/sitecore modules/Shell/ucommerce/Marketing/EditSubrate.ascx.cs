﻿using Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi;
using Affinion.LoyaltyBuild.UCom.Common.Constants;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;

using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Subrate;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.UCom.MasterClass.Website.Configuration;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.EntitiesV2;
using UCommerce.Presentation.Views.Catalog;
using UCommerce.Presentation.Views.Marketing;
using UCommerce.Presentation.Web.Controls;
using UCommerce.Presentation.Web.Pages;
using Affinion.LoyaltyBuild.Common;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.Ucommerce.Marketing
{
    public partial class EditSubrate : ViewEnabledControl<IEditCampaignItemView>
    {
        // Fields

        #region Global Var


        public List<int> lstVatRange = SiteConfiguration.GetVatRangeItem();
        public SubrateService _subrateservice = new SubrateService();

        #endregion

        #region EVENTS

        public IList<ICommand> GetCommands()
        {
            return null;
        }


        public bool Show
        {
            get { return true; }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                divCommand.Visible = false;
                LoadClientList();
                LoadTemplateList();
                LoadCurrency();
                dvEditRate.Visible = false;
                
                //LoadSubratesItem();
            }

        }

        protected void drpTemplateList_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindSubratesItem();
        }

        protected void gvClientList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            ClearMessage();
            ClearErrorMessage();
            if (e.CommandName == "edittemplate")
            {
                string[] Args = Convert.ToString(e.CommandArgument).Split(new char[] { ',' });

                drpTemplateList.SelectedIndex = 0;
                ItemID.Value = CampaignItemId.ToString();
                HiddenClientID.Value = Args[0];
                lblClientName.Text = Args[1];
                //txtItemName.Text = Args[1];
                //txtItemName.Enabled = false;
                BindSubratesItem();
                //LoadSubratesItem(GetSubratesBySingleItem(Args[0]));

            }
            else if (e.CommandName == "deletetemplate")
            {
                int itemID = Convert.ToInt32(e.CommandArgument.ToString());
            }
        }

        protected void rptSubrates_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                try
                {
                    DropDownList ddlVat = (DropDownList)e.Item.FindControl("ddlVatPercentage");
                    Label lblVat = (Label)e.Item.FindControl("lblVatAmount");
                    Label lblTotalAmount = (Label)e.Item.FindControl("lblTotalAmount");
                    TextBox txtVat = (TextBox)e.Item.FindControl("txtAmount");
                    TextBox txtPercentage = (TextBox)e.Item.FindControl("txtPercentageAmount");
                    ddlVat.DataSource = lstVatRange;
                    ddlVat.DataBind();


                    var data = (SubratesDetail)e.Item.DataItem;
                    if (data != null)
                    {
                        if (data.VAT != 0 && ddlVat != null)
                            ddlVat.Items.FindByText(data.VAT.ToString()).Selected = true;
                        if (data.Price != 0 && !data.IsPercentage && txtVat != null)
                            txtVat.Text = data.Price.ToString();
                        if (data.Price != 0 && data.IsPercentage && txtPercentage != null)
                            txtPercentage.Text = data.Price.ToString();
                        if (lblVat != null)
                        {
                            lblVat.Text = data.IsPercentage ? string.Format("{0} % Of Ex.Vat (%) Amount", data.VAT) : data.VATAmount.ToString();
                        }
                        if (lblTotalAmount != null)
                        {
                            lblTotalAmount.Text = data.IsPercentage ? string.Format("{0} % Of Room Price  & {1} % Of Ex.Vat(%) Amount", data.Price, data.VAT) : data.TotalAmount.ToString();
                        }
                    }

                }
                catch (Exception ex)
                {
                    lblError.Text = ex.Message;
                    Sitecore.Diagnostics.Log.Error(ex.Message, new object());
                }
            }

        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            dvEditRate.Visible = true;
            Reload();
            ClearErrorMessage();
            ClearMessage();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            List<SubratesDetail> objSubrates = PreparesubratebojectList();

            int result = _subrateservice.SaveSubrates(objSubrates, string.Empty);

            if (result == 1)
            {
                lblMessage.Text = "Client Subrate Saved Successfully";
                ClearErrorMessage();
            }

            Reload();
        }

        protected void btnCancle_Click(object sender, EventArgs e)
        {
            Reload();
            dvEditRate.Visible = false;
        }

        #endregion

        #region METHODS

        protected void LoadTemplateList()
        {
            try
            {
                List<SubrateTemplatesDetail> tempLateList = new List<SubrateTemplatesDetail>();

                tempLateList = _subrateservice.GetAllSubRateTemplate();
                tempLateList.Insert(0, new SubrateTemplatesDetail() { SubrateTemplateCode = string.Empty, SubrateTemplateID = 0, TemplateName = "Select Template" });
                drpTemplateList.DataSource = tempLateList;
                drpTemplateList.DataTextField = "TemplateName";
                drpTemplateList.DataValueField = "SubrateTemplateCode";
                drpTemplateList.DataBind();
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
                Sitecore.Diagnostics.Log.Error(ex.Message, new object());
            }
        }

        protected void LoadCurrency()
        {
            try
            {
                List<CurrencyDetail> currencyList = new List<CurrencyDetail>();
                Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
                currencyList = _subrateservice.GetAllCurrency(workingdb);
                currencyList.Insert(0, new CurrencyDetail() { CurrencyGUID = string.Empty, Name = "Select Currency" });
                drpCurrency.DataSource = currencyList;
                drpCurrency.DataTextField = "Name";
                drpCurrency.DataValueField = "CurrencyGUID";
                drpCurrency.DataBind();
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
                Sitecore.Diagnostics.Log.Error(ex.Message, new object());
            }
        }

        protected void LoadClientList()
        {
            try
            {
                List<ClientDetail> clientList = new List<ClientDetail>();
                Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);


                clientList = _subrateservice.GetAllClient(CampaignItemId.ToString(), workingdb);
                gvClientList.DataSource = clientList;
                gvClientList.DataBind();
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
                Sitecore.Diagnostics.Log.Error(ex.Message, new object());
            }
        }

        //protected void LoadSubratesItem()
        //{
        //    try
        //    {
        //        List<SubrateItemDetail> objSubrateItems = _subrateservice.GetAllSubRateItems();
        //        rptSubrates.DataSource = objSubrateItems;
        //        rptSubrates.DataBind();
        //    }
        //    catch (Exception ex)
        //    {
        //        lblError.Text = ex.Message;
        //        Sitecore.Diagnostics.Log.Error(ex.Message, new object());
        //    }
        //}

        protected void BindSubratesItem()
        {
            try
            {
                dvEditRate.Visible = true;
                rptSubrates.Visible = false;
                List<SubratesDetail> objSubratesData = new List<SubratesDetail>();
                if (!string.IsNullOrEmpty(drpCurrency.SelectedValue))
                {
                    divCommand.Visible = true;
                    rptSubrates.Visible = true;
                    List<SubratesDetail> objSubrates = null;

                    List<SubrateItemDetail> objSubrateItems = _subrateservice.GetAllSubRateItems();
                    if (string.IsNullOrEmpty(drpTemplateList.SelectedValue))
                    {
                        objSubrates = _subrateservice.GetSubRateByClientAndItemId(CampaignItemId.ToString(), HiddenClientID.Value, drpCurrency.SelectedValue);
                    }
                    else
                    {
                        objSubrates = _subrateservice.GetSubRateByItemId(drpTemplateList.SelectedValue.ToString());
                    }
                    foreach (var item in objSubrateItems)
                    {
                        SubratesDetail rateDetail = new SubratesDetail();
                        SubratesDetail itemDetail = objSubrates.FirstOrDefault(x => x.SubrateItemCode == item.SubrateItemCode) ?? new SubratesDetail();
                        rateDetail.ClientID = HiddenClientID.Value;
                        rateDetail.ItemID = CampaignItemId.ToString();
                        rateDetail.ItemName = item.ItemName;
                        rateDetail.SubrateItemCode = item.SubrateItemCode;
                        rateDetail.Price = itemDetail.Price;
                        rateDetail.IsPercentage = itemDetail.IsPercentage;
                        rateDetail.VAT = itemDetail.VAT;

                        objSubratesData.Add(rateDetail);
                    }
                }
                rptSubrates.DataSource = objSubratesData;
                rptSubrates.DataBind();
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
                Sitecore.Diagnostics.Log.Error(ex.Message, new object());
            }
        }

        //protected void LoadSubratesItem(List<SubratesDetail> objSubrates)
        //{
        //    try
        //    {
        //        rptSubrates.DataSource = objSubrates;
        //        rptSubrates.DataBind();
        //    }
        //    catch (Exception ex)
        //    {
        //        lblError.Text = ex.Message;
        //        Sitecore.Diagnostics.Log.Error(ex.Message, new object());
        //    }
        //}

        protected void Reload()
        {
            ItemID.Value = "";
            //ClientID.Value = "";
        }

        protected void ClearErrorMessage()
        {
            lblError.Text = "";
        }

        protected void ClearMessage()
        {
            lblMessage.Text = "";
        }


        protected List<SubratesDetail> PreparesubratebojectList()
        {
            try
            {
                List<SubratesDetail> objSubrate = new List<SubratesDetail>();

                foreach (RepeaterItem item in rptSubrates.Items)
                {
                    objSubrate.Add(Preparesubrateboject(item));
                }
                return objSubrate;
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
                Sitecore.Diagnostics.Log.Error(ex.Message, new object());
                return null;
            }
        }

        protected SubratesDetail Preparesubrateboject(RepeaterItem item)
        {
            try
            {
                SubratesDetail objSubrate = new SubratesDetail();

                objSubrate.ItemID = CampaignItemId.ToString();
                objSubrate.SubrateItemCode = ((Label)item.FindControl("lblSRCode")).Text; ;
                objSubrate.UpdtedOn = DateTime.Now;

                var price = ((TextBox)item.FindControl("txtAmount")).Text;
                var percentageAmount = ((TextBox)item.FindControl("txtPercentageAmount")).Text;

                if (!String.IsNullOrEmpty(price))
                {
                    objSubrate.Price = Convert.ToDecimal(price);
                    objSubrate.IsPercentage = false;

                }

                if (!String.IsNullOrEmpty(percentageAmount))
                {
                    objSubrate.Price = Convert.ToDecimal(percentageAmount);
                    objSubrate.IsPercentage = true;
                }

                var VatAmountPercentage = ((DropDownList)item.FindControl("ddlVatPercentage")).SelectedItem.Text;

                objSubrate.VAT = Convert.ToDecimal(VatAmountPercentage);

                objSubrate.CreatedBy = "Admin";
                objSubrate.ClientID = HiddenClientID.Value;
                objSubrate.Currency = drpCurrency.SelectedValue;
                return objSubrate;

            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
                Sitecore.Diagnostics.Log.Error(ex.Message, new object());
                return null;
            }
        }

        public int CampaignItemId
        {
            get
            {

                return View.CampaignItem.CampaignItemId;
            }
        }

        #endregion

        protected void drpCurrency_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindSubratesItem();
        }
    }
}