﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.UCom.Common.Extension;
using Sitecore.Data.Items;
using Affinion.LoyaltyBuild.Common.Utilities;
namespace Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.Reports
{
    public partial class UnClaimedReceiptsReport : System.Web.UI.UserControl
    {
        private List<UnClaimedReceipts> _unClaimedReceipts;
        private ReportsService _reportsService = new ReportsService();
        private const string errorMsg = " No records found for this receipt date";
        private double sTG_UCR = 0.0, sEK_UCR = 0.0, nOK_UCR = 0.0, dKK_UCR = 0.0, cHF_UCR = 0.0, uSD_UCR = 0.0;
        protected int TotalOfRecords { get; private set; }
        protected void Page_Load(object sender, EventArgs e)
        {
          if(Page.IsPostBack)
          { 
            htmlReportGrid.Style.Add("display", "none");
          }
         //Binding Sitecore Text Fields
          BindSiteCoreText();
        }

        protected void ResultGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ResultGridView.PageIndex = e.NewPageIndex;
            BindGridData();
        }

        protected void HtmlReport_Click(object sender, EventArgs e)
        {
            BindGridData();
        }
        protected void ExcelReport_Click(object sender, EventArgs e)
        {
            ExportGridToExcel();
        }
        private void BindGridData()
        {
            _unClaimedReceipts = GetUnClaimedReceipts();
            if (_unClaimedReceipts.Any())
            {
                htmlReportGrid.Style.Add("display", "block");
                errMsg.Style.Add("display", "none");
                 errMsg.InnerHtml = string.Empty;
            }
            else
            {
                htmlReportGrid.Style.Add("display", "none");
                errMsg.Style.Add("display", "block");
                errMsg.InnerHtml = errorMsg;
            }
            ResultGridView.DataSource = _unClaimedReceipts;
            ResultGridView.DataBind();
        }

        private List<UnClaimedReceipts> GetUnClaimedReceipts()
        {
            DateTime receiptDate = ReceiptDate.Value.ParseFormatDateTime();

            string recDate = receiptDate.ToString("yyyy-MM-dd");
            if(!DateTime.TryParse(recDate, out  receiptDate))
            {
                errMsg.Style.Add("display", "block");
                errMsg.InnerHtml = "Enter the valid receipt date";
                return null;
            }

            errMsg.Style.Add("display", "none");
            TotalOfRecords = 0;
            var unClaimedReceipts = _reportsService.GetUnClaimedReceipts(receiptDate);
            sTG_UCR = Convert.ToDouble(txtSTG_UCR.Value);
            sEK_UCR = Convert.ToDouble(txtSEK_UCR.Value);
            nOK_UCR = Convert.ToDouble(txtNOK_UCR.Value);
            dKK_UCR = Convert.ToDouble(txtDKK_UCR.Value);

            if (unClaimedReceipts.Any())
            {
                foreach (var item in unClaimedReceipts)
                {
                    Currency currency;
                    if (Enum.TryParse(item.ISOCode, out currency))
                    {
                        switch (currency)
                        {
                            case Currency.SEK:
                                item.AmountInEuro = item.Amount * sEK_UCR;
                                break;
                            case Currency.DKK:
                                item.AmountInEuro = item.Amount * dKK_UCR;
                                break;
                            case Currency.STC:
                                item.AmountInEuro = item.Amount * sTG_UCR;
                                break;
                            case Currency.NOK:
                                item.AmountInEuro = item.Amount * nOK_UCR;
                                break;
                            case Currency.EUR:
                                item.AmountInEuro = item.Amount;
                                break;

                            default:
                                item.AmountInEuro = 0;
                                break;
                        }
                    }
                }

                TotalOfRecords = (from item in unClaimedReceipts
                                  select (int)item.Count)
                                  .Sum();
            }
            return unClaimedReceipts;
        }


        /// <summary>
        ///  Export Grid To Excel
        /// </summary>
        private void ExportGridToExcel()
        {
            try
            {
                _unClaimedReceipts = GetUnClaimedReceipts();

                if (!_unClaimedReceipts.Any())
                {
                    htmlReportGrid.Style.Add("display", "none");
                    errMsg.Style.Add("display", "block");
                    errMsg.InnerHtml = errorMsg;
                    return;
                }
                errMsg.Style.Add("display","none");
                Response.Clear();
                Response.Buffer = true;
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Charset = string.Empty;
                string FileName = CreateFileName();

                using (StringWriter strwritter = new StringWriter())
                {
                    HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);


                    //Temporary Grid
                    GridView tempGrid = ResultGridView;
                    tempGrid.AllowPaging = false;
                    tempGrid.CssClass = "table table-striped table-bordered";
                    tempGrid.AutoGenerateColumns = false;



                    //Hide select button column
                    tempGrid.DataSource = _unClaimedReceipts;
                    tempGrid.DataBind();

                    //Page.Header.Controls.Clear();
                    Page.Form.Controls.Clear();

                    Page.Form.Controls.Add(totalCount);
                    Page.Form.Controls.Add(tempGrid);

                    tempGrid.GridLines = GridLines.Both;
                    tempGrid.HeaderStyle.Font.Bold = true;
                    //ResultGridView.RenderControl(htmltextwrtter);
                    Page.Form.RenderControl(htmltextwrtter);
                    //System.Text.RegularExpressions.Regex rx = new System.Text.RegularExpressions.Regex(@".*<\s*body[^>]*>)|(<\s*/\s*body\s*\>.+");
                    //string str   = rx.Replace(strwritter.ToString(), "");

                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName + ".xls");
                    Response.Write(strwritter.ToString());
                    Response.End();
                }

            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }

        }

        /// <summary>
        /// Create File Name
        /// </summary>
        /// <returns>file name</returns>
        private string CreateFileName()
        {
            return "UnClaimedReceiptsReport" + DateTime.Now.ToString();
        }

        private enum Currency
        {
            STC,
            SEK,
            EUR,
            GBP,
            USD,
            NOK,
            DKK
        }

        
        /// <summary>
        /// SitecoreTextFields
        /// </summary>
        private void BindSiteCoreText()
        {
            Item currentItem = Sitecore.Context.Item;
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Error Message", scError);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Fill in the parameters for your UCR Report", scFill);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Enter a value for STG_UCR", scStg);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Enter a value for SEK_UCR", scSEK);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Enter a value for NOK_UCR", scNok);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Enter a value for DKK_UCR", scDKK);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Enter a value for CHF_UCR", scCHF);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Enter a value for USD_UCR", scUSD);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Unclaimed receipts up-to", scUnclaimed);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Unclaimed Receipts In HTML", scUnclaimedReceipts);
            
        }
    }
}