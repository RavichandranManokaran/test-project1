﻿<%@ control language="C#" autoeventwireup="true" codebehind="ClientPortalSetAccommodation.ascx.cs" inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal.ClientPortalSetAccommodation" %>
<%@ register tagprefix="sc" namespace="Sitecore.Web.UI.WebControls" assembly="Sitecore.Kernel" %>
<%@ import namespace="Sitecore.Data.Items" %>
<%@ import namespace="Affinion.LoyaltyBuild.Common.SessionHelper" %>
<%@ import namespace="Affinion.LoyaltyBuild.Common.SessionHelper" %>

<div class="modal-header" id="divMemoInfo">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title"><span class="fa fa-group"></span>Great Choice</h4>
</div>
<div class="modal-body row">
    <div class="no-room inline-block width-full col-md-12">

        <asp:Repeater ID="RepeaterGuestAccordian" runat="server" OnItemDataBound="RepeaterGuestAccordian_ItemDataBound">
            <ItemTemplate>
                <div class="col-md-6 col-xs-12">
                    <div class="content_bg_inner_box alert-info ">
                        <div class="room-title">
                            <h4 class="no-margin">Room <%#Container.ItemIndex + 1 %></h4>
                        </div>
                        <%--<span>No of Guests</span>  --%>
                        <asp:DropDownList CssClass="form-control" ID="DropdownGuest" runat="server" Visible="false">
                        </asp:DropDownList>
                        <span>Full Guest Name:</span>
                        <%--<input type="text" placeholder="Full Guest Name" class="form-control">--%>
                        <span class="accent58-f">*</span><asp:RegularExpressionValidator runat="server" ControlToValidate="TextGuestFullName" ValidationExpression="^([\'\&quot;&quot;\,\%\&lt;\&gt;\/\;\\a-zA-ZäöåéÄÖÅÆæØø0-9\s])*$">
                            <sc:text id="txtGuestNameRegularText" runat="server" />
                        </asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator ID="rfvBookingNumber" runat="server" ForeColor="Red" ControlToValidate="TextGuestFullName" Display="Dynamic">
                            <sc:text id="txtGuestNameRequiredText" runat="server" visible="false" />
                        </asp:RequiredFieldValidator>
                        <asp:TextBox CssClass="form-control" ID="TextGuestFullName" runat="server"></asp:TextBox>
                        <span>No of Adults</span>
                        <asp:DropDownList CssClass="form-control" ID="DropDownNoOfAdults" runat="server" ClientIDMode="Static">
                        </asp:DropDownList>

                        <span>No of Children</span>
                        <asp:DropDownList CssClass="form-control" ID="DropDownNoOfChildren" runat="server" OnSelectedIndexChanged="DropDownNoOfChildren_SelectedIndexChanged" AutoPostBack="true" ClientIDMode="Static">
                        </asp:DropDownList>
                        <asp:HiddenField ID="HiddenRoomReservationId" Value='<%#Eval("RoomReservationId") %>' runat="server"></asp:HiddenField>


                        <%--age selection code start--%>

                        <div class="col-xs-12 no-padding ageGroupOuter2" style="display: block">
                            <div class="age-group">
                                <div class="col-xs-12 row" id="ChildAgeTextDiv">
                                    <h5 id="ChildAgeTextHeading">Ages of children at check-out</h5>
                                </div>
                                <asp:Repeater ID="RepeaterChildAge" runat="server">
                                    <ItemTemplate>
                                        <div id="a1" class="age2 col-xs-4" style="display: block">
                                            <div role="group" aria-label="...">
                                                <%--<select class="form-control">
                                                            <option>1</option>
                                                            <option>2</option>
                                                            <option>3</option>
                                                            <option>4</option>
                                                            <option>5</option>
                                                        </select>--%>
                                                <asp:DropDownList CssClass="form-control" ID="DropDownAge" runat="server">
                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                        <%--age selection code end--%>

                        <h4 class="accent34-f margin-bottom-15">Special Request</h4>
                        <div>
                            <asp:TextBox ID="TextSpecialRequest" TextMode="multiline" Columns="50" Rows="4" Text='<%#Eval("SpecialRequest") %>' class="width-full" cols="100" runat="server" />
                        </div>

                    </div>
                </div>

                <%--Addon detail--%>

                <%--<div class="col-md-6 col-xs-12">

                            <div class="addon pull-left col-md-12 width-full">
                                <h4 class="no-margin">Addons</h4>
                                <div class="col-md-12 margin-row">
                                    <div class="col-md-1 pull-left">
                                        <input type="checkbox" name="breakfast" value="breakfast">
                                    </div>
                                    <div class="col-md-3 pull-left">
                                        <span class="addon-free">FREE</span>
                                    </div>
                                    <div class="col-md-8"><span>Breakfast</span> <span class="accent50-f"></span></div>

                                </div>
                                <div class="col-md-12 margin-row">
                                    <div class="col-md-1 pull-left">
                                        <input type="checkbox" name="Golf" value="Golf">
                                    </div>
                                    <div class="col-md-3 pull-left">
                                        <span>€ 150</span>
                                    </div>
                                    <div class="col-md-7"><span>Golf</span><span class="accent50-f"> (Global)</span></div>

                                </div>
                                <div class="col-md-12 margin-row">
                                    <div class="col-md-1 pull-left">
                                        <input type="checkbox" name="Spa" value="Spa">
                                    </div>
                                    <div class="col-md-3 pull-left">
                                        <span class="addon-free">FREE</span>
                                    </div>
                                    <div class="col-md-8"><span>Spa </span><span class="accent50-f">(Global)</span></div>

                                </div>

                            </div>
                        </div>--%>
            </ItemTemplate>
        </asp:Repeater>

        <%--<h4 class="accent34-f margin-bottom-15">Special Request</h4>
                <div>                    
                    <asp:TextBox ID="TextSpecialRequest" TextMode="multiline" Columns="50" Rows="4" class="width-full" cols="100" runat="server" />
                </div>--%>
    </div>
    <sc:text id="txtSelectChildAgeMessage" runat="server" visible="false" />
    <sc:text id="txtGuestNameRequiredMessage" runat="server" visible="false" />
    <!--<asp:Label ID="LabelValidateFieldErrorMsg" Class="accent58-f" runat="server" Text="Label"></asp:Label>-->
</div>

<div class="modal-footer">
    <asp:Button ID="ButtonSave" runat="server" Text="Save" CssClass="btn btn-default" OnClick="ButtonSave_Click" UseSubmitBehavior="false" EnableViewState="true" />
    <asp:Button ID="ButtonClose" runat="server" Text="Close" CssClass="btn btn-default" OnClientClick="closeWin()" UseSubmitBehavior="false" />
</div>

<script lang="javascript" type="text/javascript">
    //jQuery("#ChildAgeDiv").hide();
    //jQuery("#DropDownNoOfChildren").change(function () {
    //    if (this.value > 0) {
    //        alert(this.value);
    //        jQuery("#ChildAgeDiv").hide();
    //    }
    //});
    jQuery(document).ready(function () {


        jQuery('#ChildAgeTextDiv').hide();
        var ageVal = jQuery('#DropDownNoOfChildren').val();
        if (ageVal > 0) {
            jQuery('#ChildAgeTextDiv').show();
        }
        else {
            jQuery('#ChildAgeTextDiv').hide();
        }
    });

    function closeWin() {
        window.close();
        return false;
    }

    window.onunload = function (e) {
        window.opener.location.reload();
        opener.document.getElementById('ButtonSetAccomodation');
    };
</script>
