﻿using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using Affinion.LoyaltyBuild.UCom.MasterClass.Website.Configuration;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.EntitiesV2;
using UCommerce.Presentation.Views.Marketing;
using UCommerce.Presentation.Web;
using UCommerce.Presentation.Web.Controls;
using UCommerce.Presentation.Web.Marketing;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.ucommerce.Marketing.Targets
{
    public class SupplierData
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }


    public partial class SupplierUi : ViewEnabledControl<IEditCampaignItemView>, ITargetUi, IConfigurable
    {
        
        public object New()
        {
            this.DataSource = SupplierTarget.All().FirstOrDefault(x => x.CampaignItem.CampaignItemId == View.CampaignItem.CampaignItemId);
            if (this.DataSource == null)
            {
                this.DataSource = new SupplierTarget();
            }
            ((SupplierTarget)this.DataSource).EnabledForDisplay = this.EnabledForDisplay;
            ((SupplierTarget)this.DataSource).EnabledForApply = this.EnabledForApply;
            ((SupplierTarget)this.DataSource).CampaignItem = base.View.CampaignItem;
            if (string.IsNullOrEmpty(((SupplierTarget)this.DataSource).Name))
            {
                ((SupplierTarget)this.DataSource).Name = "";
                ((SupplierTarget)this.DataSource).SupplierId = "";
            }
            ((SupplierTarget)this.DataSource).Save();
            return this.DataSource;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            BindData();

            this.DeleteButton.OnClientClick = string.Format("return confirm('Delete');");

            // this.DeleteTargetButton.OnClientClick = string.Format("return confirm('{0}');", base.GetLocalResourceObject("AreYouSureDeleteTarget.Text"));
        }

        private void BindData()
        {
            List<SupplierInvite> supplierInvite = SupplierInvite.All().Where(x => x.CampaignItem == CampaignItemId && x.IsSubscribe).ToList();
            if (supplierInvite != null && supplierInvite.Any())
            {

                this.NoCategorySelectedPlaceHolder.Visible = false;
                this.CategoryInfoPlaceHolder.Visible = true;
                lvSupplier.DataSource = supplierInvite;
                lvSupplier.DataBind();

            }
            else
            {
                lvSupplier.DataSource = null;
                this.NoCategorySelectedPlaceHolder.Visible = true;
                this.CategoryInfoPlaceHolder.Visible = false;
            }
        }

        protected void lvSupplier_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {
            string empid = "";

            Label lbl = (lvSupplier.Items[e.ItemIndex].FindControl("SupplierInviteId")) as Label;
            lvSupplier.Enabled = false;
            if (lbl != null)
            {
                empid = lbl.Text;
                SupplierInvite invite = SupplierInvite.Get(Convert.ToInt32(lbl.Text));
                if (invite != null)
                {
                    try
                    {
                        invite.Delete();
                    }
                    catch (Exception ex)
                    {
                        Diagnostics.WriteException(DiagnosticsCategory.MasterClassWebsite, ex, this);
                        throw;
                    }
                }
                BindData();
                /*SupplierTarget SupplierTarget = (SupplierTarget)this.DataSource;
                List<string> ids = SupplierTarget.SupplierId.Split('|').ToList();
                int selectedIndex = ids.FindIndex(x => x.Equals(empid));
                if (selectedIndex != -1)
                {
                    List<string> names = SupplierTarget.Name.Split('|').ToList();
                    ids[selectedIndex] = string.Empty;
                    names[selectedIndex] = string.Empty;
                    SupplierTarget.SupplierId = string.Join("|", ids.Where(x => !string.IsNullOrEmpty(x))) + "|";
                    SupplierTarget.Name = string.Join("|", names.Where(x => !string.IsNullOrEmpty(x))) + "|";
                    SupplierTarget.Save();
                    BindData();
                }*/

            }
            lvSupplier.Enabled = true;

        }

        protected void DeleteButton_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                List<SupplierInvite> supplierInvite = SupplierInvite.All().Where(x => x.CampaignItem == View.CampaignItem.CampaignItemId).ToList();
                foreach (var item in supplierInvite)
                {
                    item.Delete();
                }
                SupplierTarget target = this.DataSource as SupplierTarget;
                if (target != null)
                {
                    target.Delete();
                }
            }
            catch (Exception er)
            {
                SiteConfiguration.LogError(er.Message, new object());
            }
            this.Page.Response.Redirect(this.Page.Request.Url.ToString(), true);
        }
        // Properties
        public int CampaignItemId
        {
            get
            {
                return base.View.CampaignItem.CampaignItemId;
            }
        }

        public object DataSource { get; set; }

        public bool EnabledForApply { get; set; }

        public bool EnabledForDisplay { get; set; }

        public int TargetId
        {
            get
            {
                if (this.DataSource == null)
                {
                    return 0;
                }
                return ((Target)this.DataSource).TargetId;
            }
        }
    }




}
