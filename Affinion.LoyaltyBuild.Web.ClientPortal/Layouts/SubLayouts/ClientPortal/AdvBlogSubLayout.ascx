﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdvBlogSubLayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal.AdvBlogSubLayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<asp:Literal ID="PlaceholderText" Text="Set datasource to view banner" runat="server" />
<sc:Link Field="Link" ID="AdvLink" runat="server">
    <%--<sc:Image Field="Adv_Image" ID="AdvImg" alt="adv" class=" img-responsive position-center img-enlarge" runat="server" />--%>
    <sc:Text Field="Summary" ID="AdvSummary" alt="adv" runat="server" />
</sc:Link>