﻿using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Linq;
using UCommerce.EntitiesV2;
using UCommerce.EntitiesV2.Queries;
using System;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Queries
{
    public class LatestOrderQuery : ICannedQuery<PurchaseOrder>
    {
        /// <summary>
        /// IEnumerable Type PurchaseOrder
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public IEnumerable<PurchaseOrder> Execute(ISession session)
        {
            var purchaseOrder = session.Query<PurchaseOrder>()
                .OrderByDescending(x => x.CompletedDate)
                .First();

            return new[] { purchaseOrder };
        }
    }

   
}
