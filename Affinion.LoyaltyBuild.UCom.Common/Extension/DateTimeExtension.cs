﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.Common.Extension
{
    public static class DateTimeExtension
    {
        //in config set date in any format
        static string defaultFormat = System.Configuration.ConfigurationManager.AppSettings["defaultdateformat"] ?? "dd/MM/yyyy";
        static string defaultFormatWithTime = System.Configuration.ConfigurationManager.AppSettings["defaultdatetimeformat"] ?? "dd/MM/yyyy HH:mm:ss tt";
        public static string ConvertDateToString(this DateTime date, bool withTime = false)
        {
            if (withTime)
                return date.ToString(defaultFormatWithTime);
            return date.ToString(defaultFormat);

            //String.Format("{0:MM/dd/yyyy}", date);
            //return date.ToString("MM/dd/yyyy");
        }

        public static bool ValidDate(this string s)
        {
            string otherformate = defaultFormat.IndexOf("/") > -1 ? defaultFormat.Replace("/", "-") : defaultFormat.Replace("-", "/");
            string[] formats = { defaultFormat, otherformate }; //Support date in 21-12-2015 format. 
            var culture = System.Globalization.CultureInfo.CurrentCulture;
            DateTime convetedDate;
            if (DateTime.TryParseExact(s, formats, culture, System.Globalization.DateTimeStyles.None, out convetedDate))
            {
                return true;
            }
            return false;
        }

        public static DateTime ParseFormatDateTime(this string s, bool withTime = false)
        {
            DateTime convertDate = DateTime.Now;
            if (withTime)
            {
                string otherformate = defaultFormatWithTime.IndexOf("/") > -1 ? defaultFormatWithTime.Replace("/", "-") : defaultFormatWithTime.Replace("-", "/");
                string[] formats = { defaultFormatWithTime, otherformate }; //Support date in 21-12-2015 format. 
                var culture = System.Globalization.CultureInfo.CurrentCulture;
                DateTime.TryParseExact(s, formats, culture, System.Globalization.DateTimeStyles.None, out convertDate);
            }
            else
            {
                string otherformate = defaultFormat.IndexOf("/") > -1 ? defaultFormat.Replace("/", "-") : defaultFormat.Replace("-", "/");
                string[] formats = { defaultFormat, otherformate }; //Support date in 21-12-2015 format. 
                var culture = System.Globalization.CultureInfo.CurrentCulture;
                DateTime.TryParseExact(s, formats, culture, System.Globalization.DateTimeStyles.None, out convertDate);
            }
            return convertDate;
        }

        public static DateTime GetDateFromDateCount(this int dateCount)
        {
            DateTime julianFixed = new DateTime(1990, 1, 1);
            return julianFixed.AddDays(dateCount);

        }

        public static int ConvetDatetoDateCounter(this DateTime time)
        {
            DateTime julianFixed = new DateTime(1990, 1, 1);

            // Difference in days, hours, and minutes.
            TimeSpan ts = time - julianFixed;
            // Difference in days.
            return ts.Days;

        }

        public static int GetMonthFromJulianFixed(this DateTime currentDate)
        {
            DateTime julianFixed = new DateTime(1990, 1, 1);
            int monthdiff = ((currentDate.Year - julianFixed.Year) * 12) + currentDate.Month - julianFixed.Month;

            // Difference in days, hours, and minutes.
            // Difference in days.
            return monthdiff;

        }
    }
}
