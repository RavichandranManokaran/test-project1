﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion Inernational. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           IWizardAggregate.cs
/// Sub-system/Module:     ClientSetup(VS project name)
/// Description:           IWizardAggregate interface
/// </summary>


#region Using Directives

using System.Collections.Generic;

#endregion

namespace Affinion.LoyaltyBuild.ClientSetup.Wizard
{
    public interface IWizardAggregate<T>
    {
        #region Interface Functions 

        IWizardIterator<T> CreateIterator();
        List<T> GetAll();
        void AddItem(T item); 
        
        #endregion
    }
}
