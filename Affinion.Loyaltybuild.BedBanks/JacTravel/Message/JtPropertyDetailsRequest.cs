﻿using System.Xml.Serialization;
using System.IO;
using System.Text;
using System;
using System.Diagnostics;
using Affinion.LoyaltyBuild.BedBanks.Troika;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Message.Interfaces;
using Affinion.LoyaltyBuild.BedBanks.General;
using Affinion.LoyaltyBuild.Common.Utilities;
using System.Web;
using System.Collections.Specialized;

namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Message
{
    /// <summary>
    /// This is a class to hold the Property Details Request Parameters.
    /// It will be passed to the API controller to perform the actual request.
    /// </summary>
    [XmlType(TypeName = "PropertyDetailsRequest")]
    public class JtPropertyDetailsRequest : JtRequest, IJtRequest
    {
        [XmlElement(ElementName = "PropertyID")]
        public string PropertyId { get; set; }

        //Parameterless Constructor - Needed for XML Serialization
        private JtPropertyDetailsRequest() { }
        BedBanksSettings BedBanks;
        //The real constructor
        public JtPropertyDetailsRequest(string pPropertyId, BedBanksSettings bbs)
        {
            this.BedBanks = bbs;
            LoginDetails = JtController.GenerateLoginDetails(bbs.BedBanksUserName, bbs.BedBanksPassword);
            PropertyId = pPropertyId;
        }

        public IJtResponse Fire()
        {
            Stopwatch apiTimer = Stopwatch.StartNew();
            string response = JtController.PerformApiRequest(this);
            apiTimer.Stop();

            //READ THE RESULTS
            XmlRootAttribute xRoot = new XmlRootAttribute
            {
                ElementName = "PropertyDetailsResponse",
                IsNullable = true
            };

            XmlSerializer serializer = new XmlSerializer(typeof(JtPropertyDetailsResponse), xRoot);

            JtPropertyDetailsResponse pdRes;

            try
            {
                MemoryStream memStream = new MemoryStream(Encoding.UTF8.GetBytes(response));
                pdRes = (JtPropertyDetailsResponse)serializer.Deserialize(memStream);
                string sitecoreId = TroikaDataController.InsertDbPropertyCache(BedBanks.BedBanksProviderId, PropertyId, response, pdRes.PropertyName, pdRes.Latitude, pdRes.Longitude, pdRes.Address1, pdRes.Address2, pdRes.Region, pdRes.TownCity, pdRes.Country, pdRes.Telephone, pdRes.Fax);
                if (!string.IsNullOrEmpty(sitecoreId))
                {
                    pdRes.SitecoreHotelId = sitecoreId;
                }
            }
            catch (Exception exc)
            {
                NameValueCollection exception = new NameValueCollection();
                exception.Add(Constants.ExceptionMessage, exc.Message);
                exception.Add(Constants.ExceptionFlow, Constants.PropertyDetailsRequest);
                SendExceptionMail(exception, BedBanks.ClientItemId);
                throw new Exception(exc.Message);
            }

            pdRes.ResponseTime = apiTimer.ElapsedMilliseconds;
            TroikaDataController.InsertApiResponseTime(1, "PropertyDetails", pdRes.ResponseTime);
            //Cache the list of mappings so we don't need the Database again
            HttpRuntime.Cache[pdRes.PropertyId] = (JtPropertyDetailsResponse)pdRes;
            return pdRes;
        }
    }
}
