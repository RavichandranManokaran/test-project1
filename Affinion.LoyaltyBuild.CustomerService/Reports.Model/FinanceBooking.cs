﻿/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Entity to hold finance booking related information
/// </summary>

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model
{
    public class FinanceBooking
    {
        /// <summary>
        /// Bookings
        /// </summary>
        public int Bookings { get; set; }
        /// <summary>
        /// BookingMethodID
        /// </summary>
        public int BookingMethodID { get; set; }
        /// <summary>
        /// BookingMethod
        /// </summary>
        public string BookingMethod { get; set; }
        /// <summary>
        /// PartnerName
        /// </summary>
        public string PartnerName { get; set; }
        /// <summary>
        /// Count
        /// </summary>
        public string Count { get; set; }
    }
}
