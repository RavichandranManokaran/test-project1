﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalSearchCustomerQuery.ascx.cs"  Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalCustomerSearchResultData" %>
<%if(SearchQueryResultItems != null && SearchQueryResultItems.Count > 0){ %>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 class="panel-title">Query Chart</h4>
    </div>
    <div class="panel-body" id="querychart">
    </div>
</div>
 <script type="text/javascript">
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
              ['Query', 'Count'],
              ['Resolved - <%=SearchQueryResultItems.Where(r => r.IsResolved).Count()%>', <%=SearchQueryResultItems.Where(r => r.IsResolved).Count()%>],
              ['Not Resolved - <%=SearchQueryResultItems.Where(r => !r.IsResolved).Count()%>', <%=SearchQueryResultItems.Where(r => !r.IsResolved).Count()%>],
            ]);
            var options = {
                title: ''
            };
            var chart = new google.visualization.PieChart(document.getElementById('querychart'));
            chart.draw(data, options);
        }
    </script>

<%}else {%>
    <div class="alert alert-info">
      No matching records found
    </div>
<%} %>
