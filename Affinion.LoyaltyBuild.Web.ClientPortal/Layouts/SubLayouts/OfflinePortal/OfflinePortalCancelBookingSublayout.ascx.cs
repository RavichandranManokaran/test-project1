﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Order;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.Web.ClientPortal.Helper;
using Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts;
using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Email;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic;
using Sitecore.Data.Items;
using Affinion.LoyaltyBuild.Common.CardValidationCheck;
using Affinion.LoyaltyBuild.Api.Booking.Service;
using Affinion.LoyaltyBuild.Model.Common;
using Affinion.LoyaltyBuild.UCom.Common.Constants;


namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    public partial class OfflinePortalCancelBookingSublayout : BaseSublayout
    {
        //variable declaration
        private IOrderService _orderService;
        private IBookingService _bookingService;
        private int _orderLineId;
        private IMailService _mailService;

        //response object
        protected ValidationResponse ValidationResponse { get; private set; }

        /// <summary>
        /// page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void page_Init(Object sender, EventArgs e)
        {
            BindSitecoreText();
            BindSitecoreErrormsgs();

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _orderService = ContainerFactory.Instance.GetInstance<IOrderService>();
                _bookingService = ContainerFactory.Instance.GetInstance<BookingService>();
                int olid;
                int.TryParse(this.QueryString("olid"), out olid);
                _orderLineId = olid;

                var booking = _orderService.GetBaseBooking(_orderLineId);
                //disable cancel on invalid booking
                if (booking == null || booking.IsCancelled || booking.CheckInDate < DateTime.Now.AddDays(-1))
                {
                    ShowError(booking);
                    DisableCancel();
                }

                //set booking reference text
                if (booking != null)
                {
                    lblBookingReference.Text = booking.BookingReference;
                    Affinion.LoyaltyBuild.Api.Booking.Data.Booking orderlineInfo = _bookingService.GetBooking(_orderLineId);
                    //if (orderlineInfo.Type == Model.Product.ProductType.BedBanks)
                    //{

                    //}

                    Api.Booking.Data.PreCancelBookingResonse response = _bookingService.ValidateCancelBooking(booking.OrderLineId);

                    SessionHelper.AddItem(response.CancellationCost, BookingSessionConstant.CancellationCharge);
                    decimal cancellationCost = 0;
                    decimal refundAmount = 0;
                    if (orderlineInfo.Type == Model.Product.ProductType.BedBanks)
                    {
                        cancellationCost = (decimal)response.CancellationCost;
                    }
                    else
                    {
                        cancellationCost = orderlineInfo.CommissionAmount;
                    }
                    refundAmount = orderlineInfo.ProviderAmount - cancellationCost;
                    lblCancellationCost.Text = cancellationCost.ToString();
                    lblRefundAmount.Text = refundAmount.ToString();
                }
            }

        }

        private void ShowError(BaseBooking booking)
        {
            ValidationResponse = new ValidationResponse
            {
                IsSuccess = false
            };
            if (booking == null)
                ValidationResponse.Message = "Invalid booking details";
            else if (booking.IsCancelled)
                ValidationResponse.Message = "Booking already cancelled";
            else if (booking.CheckInDate < DateTime.Now.AddDays(-1))
                ValidationResponse.Message = "Booking past cancellation time";
        }

        /// <summary>
        /// Complete cancel click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CompleteCancel_Click(object sender, EventArgs e)
        {

            string noteToProvider = CredictCardCheck.CredictCard(ta_NoteToProvider.InnerText);
            string cancelReason = CredictCardCheck.CredictCard(ta_CancelReason.InnerText);
            
            ValidationResult cancelFlag = _bookingService.CancelBooking(new Api.Booking.Data.Basket.CancelRequest()
            {
                OrderLineId = _orderLineId,
                NoteToProvider = noteToProvider,
                CancelReason = cancelReason,
                CancelledBy = SitecoreUserProfileHelper.GetItem(UserProfileKeys.CreatedBy)
            });

            if (cancelFlag.IsSuccess)
            {
                Booking booking = _orderService.GetBooking(_orderLineId);
                SendEmail(booking);
                DisableCancel();
            }
        }

        /// <summary>
        /// Send cancellation email
        /// </summary>
        /// <param name="booking"></param>
        private void SendEmail(Booking booking)
        {
            _mailService = new MailService(booking.ClinetId);

            _mailService.GenerateEmail(MailTypes.CustomerCancellation, _orderLineId);
            _mailService.GenerateEmail(MailTypes.ProviderCancellation, _orderLineId);
        }

        private void DisableCancel()
        {
            //CompleteCancel.CssClass += " diabled";
            CompleteCancel.Attributes["class"] += "diabled";
            CompleteCancel.Attributes.Add("disabled", "disabled");
        }

        private void BindSitecoreText()
        {
            Item currentPageItem = Sitecore.Context.Item;
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Title", CancelBookingText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Booking Id", BookingIdText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Note To Provider", NoteToProviderText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Internal Note", InternalNoteText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Complete Cancellation", CompleteCancellationText);

            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Cancel Cancellation", CancelCancellationText);



        }
        #region binderrormsgs
        private void BindSitecoreErrormsgs()
        {
            Item conditionalMessageItem = this.GetItemFromRenderingParameterPath("Conditional Messages Link");
            string dontentercardFieldMessageFieldName = "Not Enter Card Number Message";
            SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, dontentercardFieldMessageFieldName, txtnoteToProviderText);
            SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, dontentercardFieldMessageFieldName, txtcancelreasonText);
        }
        #endregion
    }

}
