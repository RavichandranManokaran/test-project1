CREATE PROCEDURE [dbo].[LB_ARSqlJob]
AS
BEGIN

/* Step1
The following SQL will write a vat element on Commission of Break Free and Break Free transactions occuring on or before first
confirmation date for all irish providers. Vat is at 23%
*/
INSERT INTO LB_Vat_Payments
(
	OrderPaymentId,OrderPaymentDate,OrderId,OrderLineId,SupplierId,CampaignId,ClientId,
	OrderCurrencyId,OrderCurrencyIsoCode,CustomerId,GrossAmount,SuggestedVatAmount,
	NetAmount,CreatedBy,DateCreated
)
SELECT DISTINCT 

	OLP.OrderLinePaymentID,OLP.CreatedDate,OL.OrderId,OL.OrderLineId,CAT.CategoryId,CI.CampaignId,
	CA.ClientId,CU.CurrencyId,CU.ISOCode,C.CustomerId,OLD.Amount [GrossAmount],
	((OLD.Amount /123) * 23) [SuggestedVatAmount],(OLD.Amount - ((OLD.Amount /123) * 23)) [NetAmount],
	'Admin' [CreatedBy],GETDATE() [DateCreated]

FROM LB_RoomReservation RS

	INNER JOIN uCommerce_OrderLine OL ON RS.OrderLineId=OL.OrderLineId
	INNER JOIN uCommerce_OrderLineStatus OLS ON OL.OrderLineId=OLS.OrderLineID 
	INNER JOIN uCommerce_OrderStatus OS ON OLS.StatusId=OS.OrderStatusId AND OS.OrderStatusId=6
	INNER JOIN LB_OrderLinePayment OLP ON OL.OrderLineId=OLP.OrderLineId AND OLP.DueTypeId=2
	INNER JOIN LB_OrderLineDue OLD ON OLP.OrderLineId=OLD.OrderLineId AND OLD.IsDeleted=0 AND OLD.SubrateItemCode IS NOT NULL
	INNER JOIN LB_DueTypes DT ON DT.DueTypeId=OLD.DueTypeId 
	INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId=PO.OrderId
    INNER JOIN Ucommerce_orderaddress Orderaddress ON PO.Orderid = Orderaddress.Orderid
    INNER JOIN Ucommerce_country Country ON Orderaddress.Countryid = Country.Countryid 
	INNER JOIN LB_CustomerAttribute CA ON PO.CustomerId=CA.CustomerId
	INNER JOIN uCommerce_Customer C ON CA.CustomerId=C.CustomerId
	INNER JOIN uCommerce_Currency CU ON PO.CurrencyId=CU.CurrencyId
	INNER JOIN Ucommerce_product P ON OL.Sku = P.Sku
	INNER JOIN uCommerce_CategoryProductRelation CPR ON P.Productid = CPR.Productid
	INNER JOIN uCommerce_Category CAT ON CPR.Categoryid = CAT.Categoryid	
	LEFT JOIN uCommerce_OrderLineCampaignRelation OLCR ON OL.OrderLineId=OLCR.OrderLineId  
	LEFT JOIN ucommerce_campaignitem CI ON OLCR.CampaignItemId=CI.CampaignItemId	

WHERE
	 RS.OrderReference IS NOT NULL AND
	 OLS.CreatedDate IS NOT NULL AND
	 DATEDIFF(DD, OLS.CreatedDate,GETDATE())=1 AND
	 DT.DueTypeId=2 AND 
	 DT.IsActive=1 AND
	 Country.CountryId=24


 /* Step2
The following SQL will write a vat element on Commission of Break Free and Break Free transactions 
occuring on or before first confirmation date for all non irish providers. Vat is at 0%                                   
*/
INSERT INTO LB_Vat_Payments
(
	OrderPaymentId,OrderPaymentDate,OrderId,OrderLineId,SupplierId,CampaignId,ClientId,
	OrderCurrencyId,OrderCurrencyIsoCode,CustomerId,GrossAmount,SuggestedVatAmount,
	NetAmount,CreatedBy,DateCreated
)
SELECT DISTINCT 

	OLP.OrderLinePaymentID,OLP.CreatedDate,OL.OrderId,OL.OrderLineId,CAT.CategoryId,CI.CampaignId,
	CA.ClientId,CU.CurrencyId,CU.ISOCode,C.CustomerId,OLD.Amount [GrossAmount],
	0 [SuggestedVatAmount],OLD.Amount [NetAmount],
	'Admin' [CreatedBy],GETDATE() [DateCreated]

FROM LB_RoomReservation RS
	INNER JOIN uCommerce_OrderLine OL ON RS.OrderLineId=OL.OrderLineId
	INNER JOIN uCommerce_OrderLineStatus OLS ON OL.OrderLineId=OLS.OrderLineID 
	INNER JOIN uCommerce_OrderStatus OS ON OLS.StatusId=OS.OrderStatusId AND OS.OrderStatusId=6
	INNER JOIN LB_OrderLinePayment OLP ON OL.OrderLineId=OLP.OrderLineId AND OLP.DueTypeId=2
	INNER JOIN LB_OrderLineDue OLD ON OLP.OrderLineId=OLD.OrderLineId AND OLD.IsDeleted=0 AND OLD.SubrateItemCode IS NOT NULL
	INNER JOIN LB_DueTypes DT ON DT.DueTypeId=OLD.DueTypeId 
	INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId=PO.OrderId
    INNER JOIN Ucommerce_orderaddress Orderaddress ON PO.Orderid = Orderaddress.Orderid
    INNER JOIN Ucommerce_country Country ON Orderaddress.Countryid = Country.Countryid 
	INNER JOIN LB_CustomerAttribute CA ON PO.CustomerId=CA.CustomerId
	INNER JOIN uCommerce_Customer C ON CA.CustomerId=C.CustomerId
	INNER JOIN uCommerce_Currency CU ON PO.CurrencyId=CU.CurrencyId
	INNER JOIN Ucommerce_product P ON OL.Sku = P.Sku
	INNER JOIN uCommerce_CategoryProductRelation CPR ON P.Productid = CPR.Productid
	INNER JOIN uCommerce_Category CAT ON CPR.Categoryid = CAT.Categoryid	
	LEFT JOIN uCommerce_OrderLineCampaignRelation OLCR ON OL.OrderLineId=OLCR.OrderLineId  
	LEFT JOIN ucommerce_campaignitem CI ON OLCR.CampaignItemId=CI.CampaignItemId	

WHERE
	 RS.OrderReference IS NOT NULL AND
	 OLS.CreatedDate IS NOT NULL AND
	 DATEDIFF(DD, OLS.CreatedDate,GETDATE())=1 AND
	 DT.DueTypeId=2 AND 
	 DT.IsActive=1 AND
	 Country.CountryId!=24

/* Step3
The following SQL will write a vat element on all commission of the break fee or break fee
transactions occuring after the first confirmation date for all irish providers. The vat element 
will be recorded at 23%
*/
INSERT INTO LB_Vat_Payments
(
	OrderPaymentId,OrderPaymentDate,OrderId,OrderLineId,SupplierId,CampaignId,ClientId,
	OrderCurrencyId,OrderCurrencyIsoCode,CustomerId,GrossAmount,SuggestedVatAmount,
	NetAmount,CreatedBy,DateCreated
)
SELECT DISTINCT 

	OLP.OrderLinePaymentID,OLP.CreatedDate,OL.OrderId,OL.OrderLineId,CAT.CategoryId,CI.CampaignId,
	CA.ClientId,CU.CurrencyId,CU.ISOCode,C.CustomerId,OLD.Amount [GrossAmount],
	((OLD.Amount /123) * 23) [SuggestedVatAmount],(OLD.Amount - ((OLD.Amount /123) * 23)) [NetAmount],
	'Admin' [CreatedBy],GETDATE() [DateCreated]

FROM LB_RoomReservation RS

	INNER JOIN uCommerce_OrderLine OL ON RS.OrderLineId=OL.OrderLineId
	INNER JOIN uCommerce_OrderLineStatus OLS ON OL.OrderLineId=OLS.OrderLineID 
	INNER JOIN uCommerce_OrderStatus OS ON OLS.StatusId=OS.OrderStatusId AND OS.OrderStatusId=6
	INNER JOIN LB_OrderLinePayment OLP ON OL.OrderLineId=OLP.OrderLineId AND OLP.DueTypeId=2
	INNER JOIN LB_OrderLineDue OLD ON OLP.OrderLineId=OLD.OrderLineId AND OLD.IsDeleted=0 AND OLD.SubrateItemCode IS NOT NULL
	INNER JOIN LB_DueTypes DT ON DT.DueTypeId=OLD.DueTypeId 
	INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId=PO.OrderId
    INNER JOIN Ucommerce_orderaddress Orderaddress ON PO.Orderid = Orderaddress.Orderid
    INNER JOIN Ucommerce_country Country ON Orderaddress.Countryid = Country.Countryid 
	INNER JOIN LB_CustomerAttribute CA ON PO.CustomerId=CA.CustomerId
	INNER JOIN uCommerce_Customer C ON CA.CustomerId=C.CustomerId
	INNER JOIN uCommerce_Currency CU ON PO.CurrencyId=CU.CurrencyId
	INNER JOIN Ucommerce_product P ON OL.Sku = P.Sku
	INNER JOIN uCommerce_CategoryProductRelation CPR ON P.Productid = CPR.Productid
	INNER JOIN uCommerce_Category CAT ON CPR.Categoryid = CAT.Categoryid	
	LEFT JOIN uCommerce_OrderLineCampaignRelation OLCR ON OL.OrderLineId=OLCR.OrderLineId  
	LEFT JOIN ucommerce_campaignitem CI ON OLCR.CampaignItemId=CI.CampaignItemId	

WHERE
	 RS.OrderReference IS NOT NULL AND
	 OLS.CreatedDate IS NOT NULL AND
	 DATEDIFF(DD, OLS.CreatedDate,GETDATE())=1 AND
	 DATEDIFF(DD, OLS.CreatedDate,OLD.CreatedDate) > 1 AND
	 DT.DueTypeId=2 AND 
	 DT.IsActive=1 AND
	 Country.CountryId=24



/* Step4
The following SQL will write a vat element on all commision of the break fee and break fee 
transactions occuring after the first confirmation date for all non irish providers. 
The vat element will be recorded at 0% 
*/
INSERT INTO LB_Vat_Payments
(
	OrderPaymentId,OrderPaymentDate,OrderId,OrderLineId,SupplierId,CampaignId,ClientId,
	OrderCurrencyId,OrderCurrencyIsoCode,CustomerId,GrossAmount,SuggestedVatAmount,
	NetAmount,CreatedBy,DateCreated
)
SELECT DISTINCT 

	OLP.OrderLinePaymentID,OLP.CreatedDate,OL.OrderId,OL.OrderLineId,CAT.CategoryId,CI.CampaignId,
	CA.ClientId,CU.CurrencyId,CU.ISOCode,C.CustomerId,OLD.Amount [GrossAmount],
	0 [SuggestedVatAmount],OLD.Amount [NetAmount],
	'Admin' [CreatedBy],GETDATE() [DateCreated]

FROM LB_RoomReservation RS

	INNER JOIN uCommerce_OrderLine OL ON RS.OrderLineId=OL.OrderLineId
	INNER JOIN uCommerce_OrderLineStatus OLS ON OL.OrderLineId=OLS.OrderLineID 
	INNER JOIN uCommerce_OrderStatus OS ON OLS.StatusId=OS.OrderStatusId AND OS.OrderStatusId=6
	INNER JOIN LB_OrderLinePayment OLP ON OL.OrderLineId=OLP.OrderLineId AND OLP.DueTypeId=2
	INNER JOIN LB_OrderLineDue OLD ON OLP.OrderLineId=OLD.OrderLineId AND OLD.IsDeleted=0 AND OLD.SubrateItemCode IS NOT NULL
	INNER JOIN LB_DueTypes DT ON DT.DueTypeId=OLD.DueTypeId 
	INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId=PO.OrderId
    INNER JOIN Ucommerce_orderaddress Orderaddress ON PO.Orderid = Orderaddress.Orderid
    INNER JOIN Ucommerce_country Country ON Orderaddress.Countryid = Country.Countryid 
	INNER JOIN LB_CustomerAttribute CA ON PO.CustomerId=CA.CustomerId
	INNER JOIN uCommerce_Customer C ON CA.CustomerId=C.CustomerId
	INNER JOIN uCommerce_Currency CU ON PO.CurrencyId=CU.CurrencyId
	INNER JOIN Ucommerce_product P ON OL.Sku = P.Sku
	INNER JOIN uCommerce_CategoryProductRelation CPR ON P.Productid = CPR.Productid
	INNER JOIN uCommerce_Category CAT ON CPR.Categoryid = CAT.Categoryid	
	LEFT JOIN uCommerce_OrderLineCampaignRelation OLCR ON OL.OrderLineId=OLCR.OrderLineId  
	LEFT JOIN ucommerce_campaignitem CI ON OLCR.CampaignItemId=CI.CampaignItemId	

WHERE
	 RS.OrderReference IS NOT NULL AND
	 OLS.CreatedDate IS NOT NULL AND
	 DATEDIFF(DD, OLS.CreatedDate,GETDATE())=1 AND
	 DATEDIFF(DD, OLS.CreatedDate,OLD.CreatedDate) > 1 AND
	 DT.DueTypeId=2 AND 
	 DT.IsActive=1 AND
	 Country.CountryId!=24

/* Step5
The following SQL will write a negative vat element on all cancellation & Deposit as 
Transfer Fee transaction fee ( written to transfer record provider)occuring after first 
confirmation date for all non irish providers.The vat element will be recorded at 0%*
*/
INSERT INTO LB_Vat_Payments
(
	OrderPaymentId,OrderPaymentDate,OrderId,OrderLineId,SupplierId,CampaignId,ClientId,
	OrderCurrencyId,OrderCurrencyIsoCode,CustomerId,GrossAmount,SuggestedVatAmount,
	NetAmount,CreatedBy,DateCreated
)
SELECT DISTINCT 

	OLP.OrderLinePaymentID,OLP.CreatedDate,OL.OrderId,OL.OrderLineId,CAT.CategoryId,CI.CampaignId,
	CA.ClientId,CU.CurrencyId,CU.ISOCode,C.CustomerId,(OLD.Amount *-1) [GrossAmount],
	0 [SuggestedVatAmount],(OLD.Amount*-1) [NetAmount],
	'Admin' [CreatedBy],GETDATE() [DateCreated]

FROM LB_RoomReservation RS

	INNER JOIN uCommerce_OrderLine OL ON RS.OrderLineId=OL.OrderLineId
	INNER JOIN uCommerce_OrderLineStatus OLS ON OL.OrderLineId=OLS.OrderLineID 
	INNER JOIN uCommerce_OrderStatus OS ON OLS.StatusId=OS.OrderStatusId AND OS.OrderStatusId=6
	INNER JOIN LB_OrderLinePayment OLP ON OL.OrderLineId=OLP.OrderLineId AND OLP.DueTypeId IN (4,5)
	INNER JOIN LB_OrderLineDue OLD ON OLP.OrderLineId=OLD.OrderLineId AND OLD.IsDeleted=0 AND OLD.SubrateItemCode IS NOT NULL
	INNER JOIN LB_DueTypes DT ON DT.DueTypeId=OLD.DueTypeId 
	INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId=PO.OrderId
    INNER JOIN Ucommerce_orderaddress Orderaddress ON PO.Orderid = Orderaddress.Orderid
    INNER JOIN Ucommerce_country Country ON Orderaddress.Countryid = Country.Countryid 
	INNER JOIN LB_CustomerAttribute CA ON PO.CustomerId=CA.CustomerId
	INNER JOIN uCommerce_Customer C ON CA.CustomerId=C.CustomerId
	INNER JOIN uCommerce_Currency CU ON PO.CurrencyId=CU.CurrencyId
	INNER JOIN Ucommerce_product P ON OL.Sku = P.Sku
	INNER JOIN uCommerce_CategoryProductRelation CPR ON P.Productid = CPR.Productid
	INNER JOIN uCommerce_Category CAT ON CPR.Categoryid = CAT.Categoryid	
	LEFT JOIN uCommerce_OrderLineCampaignRelation OLCR ON OL.OrderLineId=OLCR.OrderLineId  
	LEFT JOIN ucommerce_campaignitem CI ON OLCR.CampaignItemId=CI.CampaignItemId	

WHERE
     RS.OrderReference IS NOT NULL AND
	 OLS.CreatedDate IS NOT NULL AND
	 DATEDIFF(DD, OLS.CreatedDate,GETDATE())=1 AND
	 DATEDIFF(DD, OLS.CreatedDate,OLD.CreatedDate) > 1 AND
	 DT.DueTypeId IN (4,5) AND 
	 DT.IsActive=1 AND
	 Country.CountryId!=24

END




GO


