﻿CREATE TABLE [dbo].[Blobs] (
    [Id]        UNIQUEIDENTIFIER CONSTRAINT [DF_Blobs_Id] DEFAULT (newid()) NOT NULL,
    [BlobId]    UNIQUEIDENTIFIER NOT NULL,
    [Index]     INT              NOT NULL,
    [Data]      IMAGE            NOT NULL,
    [Created]   DATETIME         NOT NULL,
    [DAC_Index] INT              NULL
);


GO
CREATE CLUSTERED INDEX [DAC_Clustered]
    ON [dbo].[Blobs]([DAC_Index] ASC);


GO
CREATE NONCLUSTERED INDEX [ndxBlobId]
    ON [dbo].[Blobs]([BlobId] ASC);

