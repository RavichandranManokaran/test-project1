﻿namespace Affinion.LoyaltyBuild.Search.Data
{

    /// <summary>
    /// Stores data related to affinion product price
    /// </summary>
    public class ProductPrice
    {
        public string ClientId { get; set; }

        public string OccupancyTypeId { get; set; }

        public decimal Price { get; set; }

        public OfferTypes offerType { get; set; }

        public int CampaignItemId { get; set; }

    }
}
