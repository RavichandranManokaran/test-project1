﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model.Payment;
using System;
using System.Data;
using System.Collections.Generic;
/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/
namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Order
{
    /// <summary>
    /// Interface to define the purchase order related actions
    /// </summary>
    public interface IOrderService
    {
        /// <summary>
        /// Gets the purchase order information based on the provided order id
        /// </summary>
        /// <param name="orderId">Order id to fetch</param>
        /// <returns>Purchase order details</returns>
        PurchaseOrder GetOrder(int orderId);

        /// <summary>
        /// Gets the purchase order information based on the provided order number/booking reeference
        /// </summary>
        /// <param name="orderId">order number/booking reeference</param>
        /// <returns>Purchase order details</returns>
        PurchaseOrder GetOrder(string orderNumber);

        /// <summary>
        /// Gets the Booking information along with the order line info for just the provided order id
        /// </summary>
        /// <param name="orderlineId">Order line id to fetch</param>
        /// <returns>Purchase orer details with order line info</returns>
        Booking GetBooking(int orderlineId);

        /// <summary>
        /// Gets the Booking information along with the order line info for just the provided order id
        /// </summary>
        /// <param name="orderlineId">Order line id to fetch</param>
        /// <returns>Purchase orer details with order line info</returns>
        BaseBooking GetBaseBooking(int orderlineId);

        /// <summary>
        /// Cancels a item in the order
        /// </summary>
        /// <param name="cancelBookingInfo">Booking info</param>
        /// <returns>Cancel success flag</returns>
        ValidationResponse CancelBooking(CancelBookingInfo cancelBookingInfo);

        /// <summary>
        /// Transfer a booking in an order to another booking
        /// </summary>
        /// <param name="orderLineToTransfer">info on order line item to tranfer<param>
        /// <returns>Success flag</returns>
        ValidationResponse TranferBooking(TransferBookingInfo orderLineToTransfer);

        /// <summary>
        /// Get the transfer history details for the item
        /// </summary>
        /// <param name="orderLineId">order line id</param>
        /// <returns>list of transfer history</returns>
        List<TransferHistoryItem> GetBookingTransferHistory(int orderLineId);

         /// <summary>
        /// Get the Holiday home partial payment Details
        /// </summary>
        /// <returns> Holiday home partial payment related Data Set</returns>
        DataSet ProvisionalHolidayHomesBookings(string clientId);
       

        
    }
}
