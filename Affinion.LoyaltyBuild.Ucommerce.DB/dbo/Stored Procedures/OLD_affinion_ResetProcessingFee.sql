﻿
-- =============================================
-- Author:		Prasanna SR
-- Create date: 12 Feb 2016
-- Description:	Reset processing fee
-- =============================================
CREATE PROCEDURE [dbo].[OLD_affinion_ResetProcessingFee] 
	@OrderId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @OrderLineSubrateID INT
	SELECT TOP 1 @OrderLineSubrateID = OrderLineSubrateID FROM LB_OrderLineSubrates SR
		INNER JOIN uCommerce_OrderLine OL ON SR.[OrderLineID] = OL.[OrderLineId]
	WHERE OL.[OrderId] = @OrderId AND 
		SR.[SubRateItemCode] = 'PROFEE'
	ORDER BY Amount DESC

    UPDATE SR
	SET Discount = Amount
	FROM LB_OrderLineSubrates SR
		INNER JOIN uCommerce_OrderLine OL ON SR.[OrderLineID] = OL.[OrderLineId]
	WHERE OL.[OrderId] = @OrderId AND 
		SR.[SubRateItemCode] = 'PROFEE' AND
		SR.OrderLineSubrateID <> @OrderLineSubrateID
					
END


