﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    /// <summary>
    ///offer based revenge
    /// </summary>
    public class CampaignRevenue
    {
        /// <summary>
        ///booking date
        /// </summary>
        public virtual DateTime BookingDate { get; set; }
        /// <summary>
        ///Total number of bookings
        /// </summary>
        public virtual int TotalBooking { get; set; }

        /// <summary>
        ///ProductSKU
        /// </summary>
        public virtual string ProductSKU { get; set; }

        /// <summary>
        ///TotalSale
        /// </summary>
        public virtual decimal TotalSale { get; set; }
    }
}
