﻿using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Data;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Affinion.Loyaltybuild.BusinessLogic.Helper;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Order;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports;
using System.Net.Mail;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model;
using System.IO;
using ClosedXML.Excel;
using Sitecore.Data.Fields;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.EmailScheduler
{
    public class DailyOracleFinanceReport
    {
        #region Class variables
        public Item JobSchedulingItem { get; set; }
        private IReportsService _reportsService = ContainerFactory.Instance.GetInstance<ReportsService>();
        private string emailFileName = string.Empty;
        private string emailTo = string.Empty;
        private string emailFrom = string.Empty;
        private string emailSubject = string.Empty;
        private string emailBody = string.Empty;
        private List<DailyOracleFinance> lstDailyOracleFinanceReport = null; 
        private DateTime dailyOracleFinanceReportDate = DateTime.Now.Date;
        #endregion

        /// <summary>
        /// Run the Email scheduler
        /// </summary>
        public void Run()
        {
            try
            {

                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("DailyOracleFinanceReport: Service Start"));
                Sitecore.Context.Job.Status.LogInfo("Scheduler started");

                Database SitecoreContextDatabase = Sitecore.Configuration.Factory.GetDatabase("web");
                Item jobSchedulingItems = SitecoreContextDatabase.GetItem(Constants.ClientConfig);
                Diagnostics.Trace(DiagnosticsCategory.AdminPortal, string.Format("GetItem From Web: Service Start"));
                Diagnostics.Trace(DiagnosticsCategory.AdminPortal, string.Format("JobSchedulingItem: GetItem From Web"));

                if (jobSchedulingItems != null)
                {
                    /* if (jobSchedulingItems.Fields["Date"] != null && jobSchedulingItems.Fields["Date"].Value != null)
                    {
                        DateField dateField = (DateField)jobSchedulingItems.Fields["Date"];
                        DateTime date=DateTime.Now;
                        if (DateTime.TryParse(dateField.DateTime.ToString(), out date))
                        {
                            dailyOracleFinanceReportDate = date.AddDays(1);
                        }
                    }*/
                    dailyOracleFinanceReportDate = DateTime.Now;
                    if (jobSchedulingItems.Fields["File Name"] != null && jobSchedulingItems.Fields["File Name"].Value !=null)
                        emailFileName = jobSchedulingItems.Fields["File Name"].ToString() + "_" + dailyOracleFinanceReportDate.ToString("MMddyyy");

                    if (jobSchedulingItems.Fields["From"] != null && jobSchedulingItems.Fields["From"].Value != null)
                    emailFrom = jobSchedulingItems.Fields["From"].ToString();
                    if (jobSchedulingItems.Fields["To"] != null && jobSchedulingItems.Fields["To"].Value != null)
                    emailTo = jobSchedulingItems.Fields["To"].ToString();
                    if (jobSchedulingItems.Fields["Subject"] != null && jobSchedulingItems.Fields["Subject"].Value != null)
                    emailSubject = jobSchedulingItems.Fields["Subject"].ToString();
                    if (jobSchedulingItems.Fields["Body"] != null && jobSchedulingItems.Fields["Body"].Value != null)
                    emailBody = jobSchedulingItems.Fields["Body"].ToString();

                   
                    Affinion.LoyaltyBuild.Communications.IEmailService emailService = new Affinion.LoyaltyBuild.Communications.Services.EmailService();

                    //Get the GridView Data from database.
                    DataTable dt = BindSearchData();

                    if (dt != null && dt.Rows.Count > 0 && !string.IsNullOrEmpty(emailFrom) && !string.IsNullOrEmpty(emailTo))
                    {
                        //Set DataTable Name which will be the name of Excel Sheet.
                        dt.TableName = "DailyOracleFinanceReport";

                        //Create a New Workbook.
                        using (XLWorkbook wb = new XLWorkbook())
                        {
                            //Add the DataTable as Excel Worksheet.
                            wb.Worksheets.Add(dt);

                                #region Adding color in xl rows
                                    int count = 1;
                                    decimal amountDebit=0;
                                    var ws = wb.Worksheets.Worksheet(1);

                                    var range = ws.RangeUsed();
                                    var table = range.AsTable();

                                    string amountDebitCol = GetColumnName(table, "AMOUNTDEBIT");

                                    foreach (var row in table.Rows())
                                    {
                                        if (count > 1)
                                        {
                                            string value = row.Cell(amountDebitCol).Value.ToString();

                                            if (!string.IsNullOrEmpty(value))
                                            {
                                                amountDebit = decimal.Parse(value.ToString());
                                                if (amountDebit > 0)
                                                {
                                                    row.Style.Fill.BackgroundColor = XLColor.LightBlue;
                                                }
                                                else
                                                {
                                                    row.Style.Fill.BackgroundColor = XLColor.LightGray;
                                                }
                                            }
                                        }
                                        count++;
                                    }
                               #endregion

                            using (MemoryStream memoryStream = new MemoryStream())
                            {
                                //Save the Excel Workbook to MemoryStream.
                                wb.SaveAs(memoryStream);

                                //Convert MemoryStream to Byte array.
                                byte[] bytes = memoryStream.ToArray();
                                var attributes = new MemoryStream(bytes);
                                memoryStream.Close();
                                emailService.Send(emailFrom, emailTo, emailSubject, emailBody, true, attributes, emailFileName + ".xlsx", "application/vnd.ms-excel");
                                //Send Email with Excel attachment.
                                //using (MailMessage mail = new MailMessage(emailFrom, emailTo))
                                //{
                                //    mail.Subject = emailSubject;
                                //    mail.Body = emailBody;

                                //    //Add Byte array as Attachment.
                                //    mail.Attachments.Add(new Attachment(new MemoryStream(bytes), emailFileName+".xlsx"));
                                //    mail.IsBodyHtml = true;
                                //    emailService.Send(mail);
                                //}
                            }
                        }
                    }

                }
                              
                Sitecore.Context.Job.Status.LogInfo("Scheduler finished");
            }
            catch (Exception exception)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("DailyOracleFinanceReport: Exception-{0}", exception.ToString()));
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
            }

        }

        public string GetColumnName(IXLTable table, string columnHeader)
        {
            var cell = table.HeadersRow().CellsUsed(c => c.Value.ToString() == columnHeader).FirstOrDefault();
            if (cell != null)
            {
                return cell.WorksheetColumn().ColumnLetter();
            }
            return null;
        }

        private DataTable BindSearchData()
        {
            IReportsService iReportsService = new ReportsService();
            SearchCriteria objSearchCriteria = new SearchCriteria();
            objSearchCriteria.StartDate = dailyOracleFinanceReportDate;
            lstDailyOracleFinanceReport = iReportsService.GetDailyOracleFinanceReport(objSearchCriteria);
            DataTable dtDailyOracelFinanceReport = GetDataTable();
            if (lstDailyOracleFinanceReport != null && lstDailyOracleFinanceReport.Count > 0)
            {
                foreach (var objDOFR in lstDailyOracleFinanceReport)
                {
                    dtDailyOracelFinanceReport.Rows.Add
                        (
                          objDOFR.Currency,
                          objDOFR.Company,
                          objDOFR.MerchantIdentification,
                          objDOFR.Partner,
                          objDOFR.Department,
                          objDOFR.Product,
                          objDOFR.GlCode,
                          objDOFR.Client,
                          objDOFR.Project,
                          objDOFR.Spare,
                          objDOFR.AmountDebit,
                          objDOFR.AmountCredit,
                          objDOFR.Description
                        );
                }
            }
            return dtDailyOracelFinanceReport;
        }

        private DataTable GetDataTable()
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("CURRENCYID");
            dataTable.Columns.Add("COMPANY");
            dataTable.Columns.Add("MERCHANTIDENTIFICATION");
            dataTable.Columns.Add("PARTNER");
            dataTable.Columns.Add("DEPARTMENT");
            dataTable.Columns.Add("PRODUCT");
            dataTable.Columns.Add("GLCODE");
            dataTable.Columns.Add("CLIENT");
            dataTable.Columns.Add("PROJECT");
            dataTable.Columns.Add("SPARE");
            dataTable.Columns.Add("AMOUNTDEBIT");
            dataTable.Columns.Add("AMOUNTCREDIT");
            dataTable.Columns.Add("DESCRIPTION");
            return dataTable;
        }
      
    }
}