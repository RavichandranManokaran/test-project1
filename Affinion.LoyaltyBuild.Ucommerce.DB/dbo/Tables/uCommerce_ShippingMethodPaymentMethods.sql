﻿CREATE TABLE [dbo].[uCommerce_ShippingMethodPaymentMethods] (
    [ShippingMethodId] INT NOT NULL,
    [PaymentMethodId]  INT NOT NULL,
    CONSTRAINT [uCommerce_PK_ShippingMethodPaymentMethods] PRIMARY KEY CLUSTERED ([ShippingMethodId] ASC, [PaymentMethodId] ASC),
    CONSTRAINT [uCommerce_FK_ShippingMethodPaymentMethods_PaymentMethod] FOREIGN KEY ([PaymentMethodId]) REFERENCES [dbo].[uCommerce_PaymentMethod] ([PaymentMethodId]),
    CONSTRAINT [uCommerce_FK_ShippingMethodPaymentMethods_ShippingMethod] FOREIGN KEY ([ShippingMethodId]) REFERENCES [dbo].[uCommerce_ShippingMethod] ([ShippingMethodId])
);

