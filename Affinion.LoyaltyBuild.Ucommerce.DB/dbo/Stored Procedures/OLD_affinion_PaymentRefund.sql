﻿CREATE PROCEDURE[dbo].[OLD_affinion_PaymentRefund]
@orderlineid INT,
@currencytypeid INT

AS
BEGIN

SELECT SUM(OLD.Amount) - Sum(OLP.Amount) [Total]

FROM uCommerce_OrderLine  OL
		INNER JOIN LB_OrderLineDue OLD ON OLD.OrderLineID = OL.OrderLineId 
		INNER JOIN LB_OrderLinePayment OLP ON OLD.CurrencyTypeId = OLP.CurrencyTypeId
	WHERE
		ISNULL(OLD.IsDeleted,0) = 0 AND  OLD.CurrencyTypeId =@currencytypeid AND
		OLD.OrderLineID IN (SELECT OrderLineID FROM [dbo].[affinion_RelatedOrderLines](@OrderLineId))

		HAVING
		(SUM(OLD.Amount) - Sum(OLP.Amount)) <> 0
		END

