﻿create PROCEDURE [dbo].[LB_GetPostBreakEmailDetails]
AS
	SELECT RR.CheckOutDate, OA.EmailAddress 
	FROM [dbo].uCommerce_OrderLine O
	INNER JOIN [dbo].uCommerce_PurchaseOrder P ON O.OrderId=P.OrderId
	INNER JOIN [dbo].LB_OrderAddress OA ON OA.OrderId=O.OrderId
	INNER JOIN [dbo].LB_RoomReservation  RR ON O.OrderLineId = RR.OrderLineId 
	WHERE P.OrderStatusId=6

GO