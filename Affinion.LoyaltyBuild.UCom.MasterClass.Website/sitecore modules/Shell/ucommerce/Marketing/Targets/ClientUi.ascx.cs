﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Subrate;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.Presentation.Views.Marketing;
using UCommerce.Presentation.Web.Controls;
using UCommerce.Presentation.Web.Marketing;
using Affinion.LoyaltyBuild.Common;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.Ucommerce.Marketing.Targets
{
    public partial class ClientUi : ViewEnabledControl<IEditCampaignItemView>, ITargetUi
    {
        public List<ClientDetail> ClientList;
        private ISubrate subrateService = new SubrateService();
        public Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);


        protected void Page_Load(object sender, EventArgs e)
        {
            ClientTarget clientData = this.DataSource as ClientTarget;
            this.DeleteButton.OnClientClick = string.Format("return confirm('Delete');");
            string[] clientSubscribe = clientData.ClientGUID.Split('|');
            chkClientList.Items.Clear();
            ClientList = subrateService.GetAllClient(string.Empty, workingdb);
            foreach (var item in ClientList)
            {
                ListItem items = new ListItem();
                items.Text = item.Name.ToString();
                items.Value = item.ClientGUID;
                items.Selected = clientSubscribe.Any(x => x.ToString() == (item.ClientGUID).ToString());
                chkClientList.Items.Add(items);
            }

            this.ReadOnlyMessage.Text = this.TargetToString(clientData);
        }

        protected void SaveButton_Click(object sender, ImageClickEventArgs e)
        {
            ClientTarget dataSource = this.DataSource as ClientTarget;
            string selectedClient = string.Empty;
            foreach (ListItem item in chkClientList.Items)
            {
                if (item.Selected)
                {
                    selectedClient += item.Value + "|";
                }
            }
            if (selectedClient.Length > 0)
            {
                selectedClient = selectedClient.Substring(0, selectedClient.Length - 1);
            }
            dataSource.ClientGUID = selectedClient;
            dataSource.Save();
            this.ReadOnlyMessage.Text = this.TargetToString(dataSource);
            this.ToggleEditor();
        }

        private void ToggleEditor()
        {
            this.NonEditModePlaceHolder.Visible = !this.NonEditModePlaceHolder.Visible;
            this.EditButton.Visible = !this.EditButton.Visible;
            this.EditModePlaceHolder.Visible = !this.EditModePlaceHolder.Visible;
            this.SaveButton.Visible = !this.SaveButton.Visible;
        }

        private string TargetToString(ClientTarget ClientTarget)
        {
            string[] subscribeClient = ClientTarget.ClientGUID.Split('|').Where(x => !string.IsNullOrEmpty(x)).ToArray();
            string clientList = string.Empty;
            foreach (var item in subscribeClient)
            {
                var data = ClientList.FirstOrDefault(x => x.ClientGUID == item);
                if (data != null)
                {
                    clientList += data.Name;
                }
            }
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendFormat("Client List");
            stringBuilder.AppendFormat("{0} ", clientList);
            return stringBuilder.ToString();
        }

        protected void DeleteButton_Click(object sender, ImageClickEventArgs e)
        {
            ClientTarget dataSource = this.DataSource as ClientTarget;
            if (dataSource != null)
            {
                dataSource.Delete();
            }
            this.Page.Response.Redirect(this.Page.Request.Url.ToString(), true);
        }

        protected void EditButton_Click(object sender, ImageClickEventArgs e)
        {
            this.ToggleEditor();
        }


        public bool EnabledForApply
        {
            get;
            set;
        }

        public bool EnabledForDisplay
        {
            get;
            set;
        }

        public object DataSource
        {
            get;
            set;

        }

        public object New()
        {
            var clientTarget = ClientTarget.All().FirstOrDefault(x => x.CampaignItem.CampaignItemId == View.CampaignItem.CampaignItemId);
            if (clientTarget == null)
            {
                clientTarget = new ClientTarget();
            }
            try
            {
                clientTarget.CampaignItem = View.CampaignItem;
                clientTarget.EnabledForApply = true;
                clientTarget.ClientGUID = string.Empty;
                clientTarget.CampaginItem = View.CampaignItem.Id;
                clientTarget.Save();
            }
            catch (Exception ex )
            {
                Diagnostics.WriteException(DiagnosticsCategory.MasterClassWebsite, ex, this);
                throw;
            }
            this.DataSource = clientTarget;
            return clientTarget;
        }
    }
}