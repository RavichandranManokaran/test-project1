﻿CREATE TABLE [dbo].[uCommerce_DiscountSpecificOrderLineAward] (
    [DiscountSpecificOrderLineAwardId] INT             NOT NULL,
    [AmountOff]                        DECIMAL (18, 2) NOT NULL,
    [AmountType]                       INT             NOT NULL,
    [DiscountTarget]                   INT             NOT NULL,
    [Sku]                              NVARCHAR (255)  NULL,
    [VariantSku]                       NVARCHAR (255)  NULL,
    CONSTRAINT [PK_DiscountSpecificOrderLineAward] PRIMARY KEY CLUSTERED ([DiscountSpecificOrderLineAwardId] ASC)
);

