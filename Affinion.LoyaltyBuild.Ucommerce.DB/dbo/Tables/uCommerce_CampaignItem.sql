﻿CREATE TABLE [dbo].[uCommerce_CampaignItem] (
    [CampaignItemId]         INT            IDENTITY (1, 1) NOT NULL,
    [CampaignId]             INT            NOT NULL,
    [DefinitionId]           INT            NOT NULL,
    [Name]                   NVARCHAR (512) NULL,
    [Enabled]                BIT            NOT NULL,
    [Priority]               INT            NULL,
    [AllowNextCampaignItems] BIT            NOT NULL,
    [CreatedBy]              NVARCHAR (50)  NULL,
    [ModifiedBy]             NVARCHAR (50)  NULL,
    [CreatedOn]              DATETIME       NOT NULL,
    [ModifiedOn]             DATETIME       NOT NULL,
    [Deleted]                BIT            NOT NULL,
    [AnyTargetAppliesAwards] BIT            DEFAULT ((0)) NOT NULL,
    [AnyTargetAdvertises]    BIT            DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_uCommerce_CampaignItem] PRIMARY KEY CLUSTERED ([CampaignItemId] ASC),
    CONSTRAINT [FK_uCommerce_CampaignItem_uCommerce_Campaign] FOREIGN KEY ([CampaignId]) REFERENCES [dbo].[uCommerce_Campaign] ([CampaignId]),
    CONSTRAINT [FK_uCommerce_CampaignItem_uCommerce_Definition] FOREIGN KEY ([DefinitionId]) REFERENCES [dbo].[uCommerce_Definition] ([DefinitionId])
);

