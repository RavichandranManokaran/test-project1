﻿using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Security.Accounts;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace Affinion.LoyaltyBuild.AdminPortal.DataExtraction
{
    public class SupplierInformationAcceptanceHttpSitecoreDataProvider : ISitecoreExternalDataProvider
    {

        /// <summary>
        /// Getting database name
        /// </summary>
        private Database database = Database.GetDatabase("master");
        private Item currentItem = null;

        private Item CurrentItem 
        { 
            get
            {
                // Current item is going to be same for one instance
                // If it is initialized, don't bother to calculate again
                if (this.currentItem != null || this.queryString == null)
                {
                    return this.currentItem;
                }

                string itemIdValue = this.queryString.GetValue("itemId");
                ID itemId;

                if (ID.TryParse(itemIdValue, out itemId))
                {
                    this.currentItem = database.GetItem(itemId);
                }

                return this.currentItem;
            }
        }

        /// <summary>
        /// taking utilities object
        /// </summary>
        private QueryStringHelper queryString;

        /// <summary>
        /// Gets the database which data needs to be import
        /// </summary>
        public Database GetDatabase()
        {
            // always add offer specific information to master db
            return database;
        }

        /// <summary>
        /// Get the sitecore template to import data as sitecore items
        /// </summary>
        public TemplateItem GetTemplate()
        {
            if(this.CurrentItem == null)
            {
                return null;
            }

            // supplier item template
            return this.CurrentItem.Template;
        }

        /// <summary>
        /// Get the root folder item to decide where to add imported data
        /// </summary>
        public Item GetRootFolder()
        {
            if (this.CurrentItem == null)
            {
                return null;
            }

            // supplier's parent folder
            return this.CurrentItem.Parent;
        }

        /// <summary>
        /// Gives the external party to decide on item name
        /// </summary>
        public string GetContextItemName()
        {
            if (this.CurrentItem == null)
            {
                return string.Empty;
            }

            // supplier item name
            return this.CurrentItem.Name;
        }

        /// <summary>
        /// Presents the external data as a collection of SitecoreFieldData
        /// </summary>
        public IEnumerable<SitecoreFieldData> GetData()
        {
            if (this.queryString == null)
                return null;

            List<SitecoreFieldData> fieldData = new List<SitecoreFieldData>();

            NameValueCollection keyValueParams = this.queryString.QueryStringParameters;

            foreach (String key in keyValueParams)
            {
                fieldData.Add(new SitecoreFieldData()
                {
                    Name = key,
                    Value = keyValueParams[key],
                });
            }

            return fieldData;
        }

        public string GetExternalVersion()
        {
            return this.queryString.GetValue("ExternalVersion");
        }

        public string GetExternalVersionFieldName()
        {
            return "ExternalVersion";
        }

        /// <summary>
        /// Set data using the url
        /// </summary>
        /// <param name="url">the url of the data extraction requst. should have the required query string</param>
        public void SetData(Uri url)
        {
            this.queryString = new QueryStringHelper(url);
        }
    }
}