
/****** Object:  Table [dbo].[LB_BB_Location]    Script Date: 7/19/2016 7:59:40 PM ******/

CREATE TABLE [dbo].[LB_BB_Location](
	[Country] [varchar](50) NULL,
	[RegionID] [varchar](50) NULL,
	[Region] [varchar](50) NULL,
	[ResortID] [varchar](50) NULL,
	[Resort] [varchar](50) NULL,
	[isMatched] [int] NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[LB_BB_Location] ADD  DEFAULT ((0)) FOR [isMatched]
GO

