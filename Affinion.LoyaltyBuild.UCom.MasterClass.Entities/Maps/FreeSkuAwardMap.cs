﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class FreeSkuAwardMap : SubclassMap<FreeSkuAward>
    {
        // Methods
        public FreeSkuAwardMap()
        {
            base.Table("LB_FreeSkuAward");
            base.KeyColumn("FreeSkuAwardId");
            base.Map(x => x.FreeSkuAwardId).ReadOnly();
            base.Map(x => x.FreeSkuType).Not.Nullable();
        }

    }
}
