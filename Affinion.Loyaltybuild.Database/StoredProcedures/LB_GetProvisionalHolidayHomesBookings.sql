

Create Procedure [dbo].[LB_GetProvisionalHolidayHomesBookings] 
	   @clientId as varchar(50) = null
AS
Begin
Select	Distinct   OL.OrderLineId,
		   RR.OrderReference,
		   CUS.FirstName +' ' + CUS.LastName CustomerName,
		   OA.EmailAddress,
		   RR.GuestName,
		   C.CategoryId,
		   C.Name ProviderName,
		   PO.CurrencyId,
		   CU.ISOCode,
		   RR.CheckInDate,
		  isnull((select top 1  ISNULL(Total,0) from LB_FnGetDuesByOrderLineID(OL.OrderLineId,null)),0) AmountDue		   
	FROM 	
	[dbo].uCommerce_OrderLine OL 
	INNER JOIN [dbo].uCommerce_PurchaseOrder PO ON OL.OrderId = PO.OrderId
	INNER JOIN [dbo].LB_OrderAddress  OA ON OA.OrderId = PO.OrderId 
	INNER JOIN [dbo].LB_RoomReservation RR ON OL.OrderLineId = RR.OrderLineId 
	INNER JOIN [dbo].uCommerce_Product P ON OL.Sku = P.Sku
	INNER JOIN [dbo].uCommerce_CategoryProductRelation CPR ON P.ProductId = CPR.ProductId 
	INNER JOIN [dbo].uCommerce_Category C ON CPR.CategoryId = C.CategoryId
	INNER JOIN [dbo].uCommerce_Customer CUS ON PO.CustomerId = CUS.CustomerId 
	INNER JOIN [dbo].LB_CustomerAttribute CA ON PO.CustomerId = CA.CustomerId 
	INNER JOIN [dbo].uCommerce_OrderLineStatus OLS ON RR.OrderLineStatusID = OLS.OrderLineStatusId
	INNER JOIN [dbo].LB_OrderStatus OS ON OLS.StatusId = OS.OrderStatusId
	INNER JOIN [dbo].uCommerce_Currency CU ON CU.CurrencyId = PO.CurrencyId
	INNER JOIN [dbo].uCommerce_OrderProperty OP ON OL.OrderLineId = OP.OrderLineId AND OP.[Key] = 'PaymentMethod' AND Value = 'Partial'
	WHERE 
	OS.OrderStatusId = 5 and CA.ClientId = @clientId
	 and DateDiff(dd,getdate(),RR.CheckInDate) >0 
end
