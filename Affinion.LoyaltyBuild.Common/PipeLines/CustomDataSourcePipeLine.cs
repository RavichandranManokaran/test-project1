﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           MediaDataSourceSetter.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Customize data source queries at runtime
/// </summary>

#region Using Directives
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Sitecore.Data.Fields;
using Sitecore.Pipelines.GetLookupSourceItems;
using System;
#endregion

namespace Affinion.LoyaltyBuild.Common.PipeLines
{
    public class CustomDataSourcePipeLine
    {
        public void Process(GetLookupSourceItemsArgs args)
        {
            try
            {
                ///Replace the tokens by supplier type and the supplier
                if (args.Source.Contains("$supplierType"))
                {
                    ///Replace the token by Supplier Type
                    args.Source = args.Source.Replace("$supplierType", ((ReferenceField)args.Item.Fields["SupplierType"]).TargetItem.DisplayName);//Supplier type is a droplink field so it is a reference field

                    if (args.Source.Contains("$supplier"))
                    {
                        ///Replace the token by Supplier name
                        args.Source = args.Source.Replace("$supplier", args.Item["Name"]);
                    }
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, this);
                throw;
            }
        }
    }
}
