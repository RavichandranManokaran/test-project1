﻿//<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           OfflinePortalNewsSublayout.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal
/// Description:           Used to Bind data to OfflinePortalNewsSublayout subLayout
/// </summary>

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    #region Using Directives
    using Affinion.LoyaltyBuild.Common;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.Search.Helpers;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    #endregion

    public partial class OfflinePortalNewsSublayout : BaseSublayout
    {
        private string currentDate { get; set; }
        private string NewsListQuery { get; set; }
        public Item Settings { get; set; }

        private void Page_Load(object sender, EventArgs e)
        {

            NewsListQuery = @"fast:/sitecore/content/#offline-portal#/news/*[@IsActive='1' and @FromDate < '" + Sitecore.DateUtil.ToIsoDate(DateTime.Now, false, true) + "' and @ToDate > '" + Sitecore.DateUtil.ToIsoDate(DateTime.Now, false, true) + "' ]";

            Settings = Sitecore.Context.Database.GetItem(Constants.OfflinePortalSettings);

            if (!IsPostBack)
            {
                NewsTitle.Text = SitecoreFieldsHelper.GetValue(Settings, "NewTitle", "News");
            }

            this.News.DataSource = GetNewItems(NewsListQuery);
            this.News.DataBind();

        }

        /// <summary>
        /// Get sorted news items
        /// </summary>
        /// <returns></returns>
        private List<Item> GetNewItems(string query)
        {
            Sitecore.Data.Database currentDb = Sitecore.Context.Database;

            var newsItems = currentDb.SelectItems(query);

            if (newsItems != null)
            {
                //Get user call centre
                Item callcentre = SearchHelper.GetUserCallCentre();
                List<Item> itemList = new List<Item>();
                if (callcentre != null)
                {
                    //Sort new data by priority and updated date filtering by call centre
                    var sortedItems = newsItems.Where(c => c.Fields["CallCentre"].Value.Contains(callcentre.ID.ToString()))
                        .OrderBy(o => o.Fields["Priority"].Value)
                        .ThenByDescending(o => SitecoreFieldsHelper.GetValue(o, "__Updated"));

                    if (sortedItems.Any<Item>())
                    {
                        Errormessage.Visible = false;
                        return sortedItems.ToList<Item>();
                    }
                    else
                    {
                        literalErrorMessage.Text = SitecoreFieldsHelper.GetValue(Settings, "NoNewsFound", "No New Found");
                        Errormessage.Visible = true;
                    }
                }
                else
                {
                    literalErrorMessage.Text = SitecoreFieldsHelper.GetValue(Settings, "NoCallCentre", "No Call Centre Found");
                    Errormessage.Visible = true;
                }

            }
            return null;
        }
    }
}