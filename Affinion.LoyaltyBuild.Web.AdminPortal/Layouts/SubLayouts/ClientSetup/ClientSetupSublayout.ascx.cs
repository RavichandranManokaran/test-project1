﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           ClientSetupSublayout.ascx.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.AdminPortal(VS project name)
/// Description:           Sublayout for simple image template
/// </summary>
namespace Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.ClientSetup
{
    #region Using Directives
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using Sitecore.SecurityModel;
    using System;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.ClientSetup.ClientSetup;
    using System.Web;
    #endregion

    /// <summary>
    /// Sublayout for Client Setup
    /// </summary>
    public partial class ClientSetupSublayout : System.Web.UI.UserControl
    {

        #region Constant variable
        //private const string folderTemplatePath = "/sitecore/templates/System/Templates/Template Folder";
        private const string clientPath = "/sitecore/content/AdminPortal/ClientSetup";
        //private const string clientTemplatePath = "/sitecore/templates/Affinion/AdminPortal/Client Setup/ClientDetails"; 
        #endregion

        /// <summary>
        /// Page load function
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //ButtonCreate.Enabled = true;
            if (Sitecore.Context.User.IsAuthenticated)
            {
                //Sitecore.Data.ID id;
                //Database db = Sitecore.Configuration.Factory.GetDatabase("core");
                //bool result = Sitecore.Data.ID.TryParse("{F013C4A5-8E13-4079-8C64-C622809A0DFC}", out id);

                //if (result)
                //{
                //    Item item = db.GetItem(id);

                //    if (!item.Access.CanRead())
                //    {
                //        HttpContext.Current.Request.Abort();
                //    }

                //}

            }
            else
            {
                HttpContext.Current.Request.Abort();
            }

        }

        /// <summary>
        /// Calls when button clicks and create admin portal and uCommerce store and related information
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ButtonCreate_Click(object sender, EventArgs e)
        {
            // call create item function to create new item in sitecore and uCommerce
            CreateItem();
        }

        /// <summary>
        /// This is use to create item in sitecore and uCommerce end
        /// </summary>
        public void CreateItem()
        {
            //Again we need to handle security, in this example we just disable it
            using (new SecurityDisabler())
            {
                try
                {
                    // local variables
                    string clientName = TextBoxClientName.Text.Trim();

                    //First get the parent item from the master database
                    Database masterDb = Sitecore.Configuration.Factory.GetDatabase("master");

                    // Removed due to introduce new branch template.

                    /*
                    CreateItem(clientPath, folderTemplatePath, clientName);

                    // creates client item using ClientDetails template   
                    CreateItem(string.Concat(clientPath, "/", clientName), clientTemplatePath, clientName);

                    //creates _supporting-content-content folder 
                    CreateItem(string.Concat(clientPath, "/", clientName), folderTemplatePath, "_supporting-content-content");

                    //creates Contacts folder 
                    CreateItem(string.Concat(clientPath, "/", clientName, "/_supporting-content-content"), folderTemplatePath, "ContactDetails");
                    
                    //creates FinancialDetails folder 
                    CreateItem(string.Concat(clientPath, "/", clientName, "/_supporting-content-content"), folderTemplatePath, "FinancialInformation");

                    //creates FinancialDetails folder 
                    CreateItem(string.Concat(clientPath, "/", clientName, "/_supporting-content-content"), folderTemplatePath, "Branches");

                    //creates FinancialDetails folder 
                    CreateItem(string.Concat(clientPath, "/", clientName, "/_supporting-content-content"), folderTemplatePath, "ClientRules");

                     */

                    //Create new Client portal for new client
                    Item parentItem = masterDb.Items["/sitecore/content"];
                    BranchItem branchItem = masterDb.GetItem("/sitecore/templates/Branches/Affinion/ClientPortalBranchTemplate");
                    parentItem.Add(clientName, branchItem);

                    //Creating admin poral item useing branch template
                    parentItem = masterDb.Items[clientPath];
                    branchItem = masterDb.GetItem("/sitecore/templates/Branches/Affinion/ClientSetup");
                    parentItem.Add(clientName, branchItem);

                    // Creates uCommerce store
                    //UCommerceInstaller uInstaller = new UCommerceInstaller(clientName, "Suppliers");
                    UCommerceInstaller uInstaller = new UCommerceInstaller(clientName, string.Concat(clientName, "Hotels"));
                    uInstaller.Configure();

                    Response.Redirect("/sitecore/shell/sitecore/client/Applications/Launchpad/");
                }
                catch (Exception exception)
                {
                    Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                    throw;
                }
            }
        }

        /// <summary>
        /// Use to create Items in Sitecore
        /// </summary>
        /// <param name="parentItemPath">path of parent Item </param>
        /// <param name="templatePath">Path of template</param>
        /// <param name="childName">childName</param>
        public static void CreateItem(string parentItemPath, string templatePath, string childName)
        {
            try
            {
                //Get the master database
                Database masterDb = Sitecore.Configuration.Factory.GetDatabase("master");

                //Get the patent item path
                Item parentItem = masterDb.Items[parentItemPath];

                //Retrive the templare from given path
                TemplateItem template = masterDb.GetItem(templatePath);

                //Add new item to parent item.
                parentItem.Add(childName, template);
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                throw;
            }
        }
    }
}