﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           AffinionDbContext.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.DataAccess
/// Description:           AffinionDbContext class for DbContext.
/// </summary>

#region Using Directives
using Affinion.LoyaltyBuild.DataAccess.Models;
using System.Data.Entity;

#endregion

namespace Affinion.LoyaltyBuild.DataAccess
{
    class AffinionDbContext : DbContext
    {
        /// <summary>
        /// Affinion DB Context
        /// </summary>
        public AffinionDbContext()
            : base("AffinionContext")
        {
           
        }

        /// <summary>
        /// PasswordHistory DbSet
        /// </summary>
        public DbSet<PasswordHistory> PasswordHistory { get; set; }

        /// <summary>
        /// Login Audit DbSet
        /// </summary>
        public DbSet<LoginAudit> LoginAudit { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<AffinionDbContext>(null);
        }
    }
}
