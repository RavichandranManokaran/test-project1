﻿CREATE PROCEDURE LB_UpdatePackageAvailability 

@VariantSku varchar(max),
@DateCounter varchar(max)
AS
DECLARE @packages TABLE (packagesku varchar(100))
DECLARE @packagesrelation TABLE (packagesku varchar(100),itemsku varchar(100))

DECLARE @xml AS xml,
        @str AS varchar(100),
        @delimiter AS varchar(10);
		SET @str = @DateCounter
		SET @delimiter = ',';
		SET @xml = CAST('<X>' + REPLACE(@str, @delimiter, '</X><X>') + '</X>' AS xml);
BEGIN
  INSERT INTO @packages
    SELECT DISTINCT
      P.VariantSku
    FROM uCommerce_Product P
    INNER JOIN uCommerce_ProductDefinition PD	
      ON P.ProductDefinitionId = PD.ProductDefinitionId
    INNER JOIN uCommerce_ProductDefinitionField PDF
      ON PD.ProductDefinitionId = PDF.ProductDefinitionId
      AND PDF.Name IN ('RelatedSKU', 'relatedaddonsku')
    INNER JOIN uCommerce_ProductProperty PP
      ON PDF.ProductDefinitionFieldId = PP.ProductDefinitionFieldId
      AND P.ProductId = PP.ProductId
    WHERE PP.Value LIKE ('%' + @VariantSku + '%')

  INSERT INTO @packagesrelation
    SELECT
      A.packagesku,
      Split.a.value('.', 'VARCHAR(100)') AS Data
    FROM (SELECT
      PC.packagesku,
      CAST('<M>' + REPLACE(pp.Value, ',', '</M><M>') + '</M>' AS xml) Data
    FROM @packages PC
    INNER JOIN uCommerce_Product P
      ON PC.packagesku = P.VariantSku
    INNER JOIN uCommerce_ProductDefinition PD
      ON P.ProductDefinitionId = PD.ProductDefinitionId
    INNER JOIN uCommerce_ProductDefinitionField PDF
      ON PD.ProductDefinitionId = PDF.ProductDefinitionId
      AND PDF.Name IN ('RelatedSKU', 'relatedaddonsku')
    INNER JOIN uCommerce_ProductProperty PP
      ON PDF.ProductDefinitionFieldId = PP.ProductDefinitionFieldId
      AND P.ProductId = PP.ProductId) AS A
    CROSS APPLY Data.nodes('/M') AS Split (a);

  UPDATE LB_RoomAvailability
  SET NumberAllocated = temp.NumberAllocated,
      IsStopSellOn = temp.IsStopSellOn,
      IsCloseOut = temp.IsCloseOut
  FROM (SELECT
    RA.DateCounter,
    PR.packagesku,
    MIN(RA.NumberAllocated) NumberAllocated,
    MAX(CAST(IsCloseOut AS tinyint)) IsCloseOut,
    MAX(CAST(IsStopSellOn AS tinyint)) IsStopSellOn
  FROM LB_RoomAvailability RA
  INNER JOIN @packagesrelation PR
    ON PR.itemsku = RA.VariantSku and RA.DateCounter in (SELECT [N].value( '.', 'varchar(50)' ) AS value
FROM @xml.nodes( 'X' ) AS [T]( [N] ))
  GROUP BY RA.DateCounter,
           PR.packagesku) temp
  WHERE LB_RoomAvailability.VariantSku = temp.packagesku
  AND LB_RoomAvailability.DateCounter = temp.DateCounter

END	