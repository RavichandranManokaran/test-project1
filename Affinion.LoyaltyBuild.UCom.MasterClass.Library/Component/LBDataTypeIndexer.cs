﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using Documents = UCommerce.Documents.Definitions;
using UCommerce.Search;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Component
{
    public class LBDataTypeIndexer : IIndexer<DataType>, IDeindexer<DataType>, IIndexer<IEnumerable<DataType>>, IDeindexer<IEnumerable<DataType>>
    {
        // Fields
        private readonly IRepository _ravenDbRepository;

        // Methods
        public LBDataTypeIndexer(IRepository ravenDbRepository)
        {
            this._ravenDbRepository = ravenDbRepository;
        }

        public void Deindex(DataType subjects)
        {
            this.Deindex(new List<DataType> { subjects });
        }

        public void Deindex(IEnumerable<DataType> subjects)
        {
            List<string> list = (from x in subjects select "datatypes/" + x.Guid.ToString()).ToList<string>();
            this._ravenDbRepository.Delete<DataType>((IEnumerable<object>)list);
        }

        public void Index(DataType subjects)
        {
            this.Index(new List<DataType> { subjects });
        }

        public void Index(IEnumerable<DataType> subjects)
        {
            List<Documents.DataType> items = new List<Documents.DataType>();
            foreach (DataType type in subjects)
            {
                Documents.DataType item = new Documents.DataType
                {
                    Id = type.Guid,
                    Name = type.Name,
                    DefinitionName = type.DefinitionName
                };
                foreach (DataTypeEnum enum2 in type.DataTypeEnums)
                {
                    Documents.PreValue value2 = new Documents.PreValue
                    {
                        Name = enum2.Name,
                        Value = enum2.Value
                    };
                    foreach (DataTypeEnumDescription description in enum2.DataTypeEnumDescriptions)
                    {
                        value2.Properties.AddProperty(description.CultureCode, "DisplayName", description.DisplayName);
                        value2.Properties.AddProperty(description.CultureCode, "Description", description.Description);
                    }
                    item.PreValues.Add(value2);
                }
                items.Add(item);
            }
            this._ravenDbRepository.SaveMany<Documents.DataType>(items);
        }
    }
}