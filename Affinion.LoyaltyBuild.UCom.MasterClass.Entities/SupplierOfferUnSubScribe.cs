﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class SupplierOfferUnSubScribe : IEntity
    {
        private int? _hashCode;


        // Methods
        public static IQueryable<SupplierOfferUnSubScribe> All()
        {
            return GetRepo().Select();
        }

        public virtual void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<SupplierOfferUnSubScribe, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            SupplierOfferUnSubScribe objA = obj as SupplierOfferUnSubScribe;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<SupplierOfferUnSubScribe, bool>> expression)
        {
            return All().Any<SupplierOfferUnSubScribe>(expression);
        }

        public static IList<SupplierOfferUnSubScribe> Find(Expression<Func<SupplierOfferUnSubScribe, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<SupplierOfferUnSubScribe>();
        }

        public static SupplierOfferUnSubScribe FirstOrDefault(Expression<Func<SupplierOfferUnSubScribe, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<SupplierOfferUnSubScribe>();
        }

        public static SupplierOfferUnSubScribe Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<SupplierOfferUnSubScribe> GetRepo()
        {
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<SupplierOfferUnSubScribe>>();
            
        }

        public virtual void Save()
        {
            try
            {
                GetRepo().Save(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static SupplierOfferUnSubScribe SingleOrDefault(Expression<Func<SupplierOfferUnSubScribe, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties

        public virtual int SupplierOfferUnSubScribeId { get; protected set; }

        public virtual int Id
        {
            get
            {
                return this.SupplierOfferUnSubScribeId;
            }
        }

        public virtual string Reason { get; set; }

        public virtual string CategoryGuid { get; set; }

        public virtual int CampaignItem { get; set; }

        public virtual DateTime? CreatedDate { get; set; }

        public virtual string CreatedBy { get; set; }

        public virtual string Sku { get; set; }

    }
}
