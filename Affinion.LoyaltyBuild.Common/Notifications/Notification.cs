﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           Notification.cs
/// Sub-system/Module:     Client Setup(VS project name)
/// Description:           Code is used to read conditions and return corresponding notification details.
/// </summary>


#region Using  Directives
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Rules;
using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using System;
using System.Runtime.Remoting;
#endregion

namespace Affinion.LoyaltyBuild.Common.Notifications
{
    public class Notification
    {

        #region Private Properties
        //Properties of the Notification
        private string title;
        private string message;
        private NotificationType type;//enum for notification types
        private INotificfationRule rule;

        ////Declare a boolean variable to read the returned value from rule.ExecuteDisplayRule().
        private bool showMessage = true;

        #endregion

        //enumirator NotificationTypes
        public enum NotificationType
        {
            LightBox = 0,
            Timed,
            Repeater
        };


        //Constructor
        public Notification(Item item)
        {
            try
            {
                if (item != null)
                {
                    ExtractNotificationParameters(item);
                }
            }
            catch (Exception ex)
            {
                //log the error and throw 
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, this);
                throw;
            }

        }


        public Notification(string key)
        {
            Sitecore.Data.Database currentDB = Sitecore.Context.Database;

            try
            {
                if (key != null)
                {
                    Item item = currentDB.GetItemByKey(key);
                    ExtractNotificationParameters(item);
                }
            }
            catch (Exception ex)
            {
                //log the error and throw 
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, this);
                throw;
            }

        }



        #region Private Methods
        /// <summary>
        /// Extracts the requestes parameters of the notification item.
        /// </summary>
        /// <param name="item"></param>
        private void ExtractNotificationParameters(Item item)
        {
            //Assigning values to properties.
            //Assigning value to title.
            title = item.Fields["Title"].ToString();
            //Assigning value to message.
            message = item.Fields["Message"].ToString();

            //comparing notification type with the value of field "Category"
            string category = item.Fields["Category"].ToString();
            type = (NotificationType)Enum.Parse(typeof(NotificationType), category);


            //Calling the rule
            MultilistField rulesField = (MultilistField)item.Fields["Rule"];
            if (rulesField != null)
            {
                //Iterate over all the selected items and read the field values.
                foreach (ID id in rulesField.TargetIDs)
                {
                    Item targetRule = Sitecore.Context.Database.Items[id];

                    //extracting rule assembly and class name
                    string assemblyName = targetRule.Fields["Assembly"].ToString();
                    string className = targetRule.Fields["Class"].ToString();

                    //create instance of the corresponding rule
                    Type classType = Type.GetType(className);
                    ObjectHandle extractedRule = (ObjectHandle)Activator.CreateInstance(assemblyName, classType.ToString());
                    rule = (INotificfationRule)extractedRule.Unwrap();
                    //assign the returnvalue of the rule.ExecuteDisplayRule() to showMessage
                    showMessage = rule.ExecuteDisplayRule();
                }
            }
        }

        /// <summary>
        /// Return the item field value according to that rule validations.
        /// </summary>
        /// <param name="messageResult">Item field value</param>
        /// <returns>result</returns>
        private string ReturnMessageResult(string messageResult) 
        {
            try
            {
                return (showMessage == true) ? messageResult : null;
            }
            catch (Exception ex)
            {
                //log the error and throw 
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, this);
                throw;
            }
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Returns the extracted value of the title field.
        /// </summary>
        /// <returns>title</returns>
        public virtual string DisplayNotificationTitle()
        {
            return ReturnMessageResult(title);
        }

        /// <summary>
        /// Returns the extracted value of the message field.
        /// </summary>
        /// <returns>message</returns>
        public virtual string DisplayNotificationMessage()
        {
            return ReturnMessageResult(message);
        }

        /// <summary>
        /// Returns the extracted and parsed value of the type field.
        /// </summary>
        /// <returns>type</returns>
        public virtual NotificationType DisplayNotificationType()
        {
            try
            {
                return type;
            }
            catch (Exception ex)
            {
                //log the error and throw 
               Diagnostics.WriteException(DiagnosticsCategory.Common, ex, this);
                throw;
            }

        }
        #endregion



    }


}
