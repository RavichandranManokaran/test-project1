﻿/****** Object:  StoredProcedure [dbo].[LB_AddCancelDue]    Script Date: 2/12/2016 11:41:52 AM ******/
DROP PROCEDURE [dbo].[LB_AddCancelDue]
GO

/****** Object:  StoredProcedure [dbo].[LB_AddCancelDue]    Script Date: 2/12/2016 11:41:52 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Prasanna SR
-- Create date: 10 Jan 2016
-- Description:	Calculate and enter due on cancel 
-- =============================================
CREATE PROCEDURE [dbo].[LB_AddCancelDue]
	@OrderLineId INT,
	@CreatedBy VARCHAR(50)
	AS
	BEGIN
		-- SET NOCOUNT ON added to prevent extra result sets from
		-- interfering with SELECT statements.
		SET NOCOUNT ON;

		-- Insert statements for procedure here
		DECLARE @CancelFee MONEY,
			@SubrateCode VARCHAR(50),
			@CurrencyId INT,
			@CreatedDate DATETIME,
			@PaymentMethodId INT

		SELECT TOP 1 
			@CreatedDate = GETDATE(),
			@SubrateCode = [SubRateItemCode] ,
			@CancelFee = OS.[Amount] - OS.[Discount],
			@CurrencyId = PO.[CurrencyId]
		FROM  [LB_OrderLineSubrates] OS
			INNER JOIN [uCommerce_OrderLine] OL ON OS.[OrderLineId] = OL.[OrderLineId]
			INNER JOIN [uCommerce_PurchaseOrder] PO ON OL.[OrderId] = PO.[OrderId]
		WHERE OS.OrderLineId = @OrderLineId 
			AND [SubRateItemCode] IN ('CANFEE','LBCOM')
		ORDER BY [SubRateItemCode]
	
		--Set payment method id
		SELECT TOP 1
			@PaymentMethodId = PaymentMethodId
		FROM [uCommerce_OrderLine] OL
			INNER JOIN  [LB_OrderLinePayment] OLP ON OL.[OrderLineId] = OLP.[OrderLineId]
		WHERE OL.OrderLineId = @OrderLineId

		--Set the commision amount due as delete
		UPDATE [dbo]. [LB_OrderLineDue]
		SET [SubRateItemCode] = 'CANFEE',
			[DueTypeId] = 4,
			[Amount] = @CancelFee,
			[UpdatedBy] = @CreatedBy,
			[UpdatedDate] = @CreatedDate
		WHERE [OrderLineId] = @OrderLineId
			AND [SubRateItemCode] IN ('LBCOM','TRAFEE')
		
		
		--Add Refund for provider Amount
		IF EXISTS (SELECT 1 WHERE 1 = [dbo].[LB_FnIsPaidInFull](@OrderLineId))
		BEGIN
			DECLARE @ProviderAmt MONEY
			SELECT TOP 1 @ProviderAmt = -(OL.Total - OS.Amount + ISNULL(OS.Discount, 0))
			FROM  [LB_OrderLineSubrates] OS
				INNER JOIN [uCommerce_OrderLine] OL ON OS.[OrderLineId] = OL.[OrderLineId]
			WHERE OS.OrderLineId = @OrderLineId 
				AND [SubRateItemCode] = 'LBCOM'

			EXEC [LB_SaveOrderLineDue]
				NULL
				,@OrderLineId
				,''
				,NULL
				,@CurrencyId
				,6
				,@ProviderAmt
				,0
				,0
				,0
				,0
				,0
				,0
				,''
				,@CreatedBy
				,@CreatedDate
				,@PaymentMethodId
		END
	END

GO

