﻿using System.Xml.Serialization;

namespace Affinion.LoyaltyBuild.BedBanks.Troika.Model.Coldfusion
{
    public class ColdFusionBit
    {
        [XmlAttribute("cftype")]
        public string CfType = "BIT";

        [XmlText]
        public string Value { get; set; }
    }
}
