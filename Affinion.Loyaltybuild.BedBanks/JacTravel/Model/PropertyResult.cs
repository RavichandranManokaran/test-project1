﻿using System;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Message;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Model.Room;

namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Model
{
    public enum DataLocation
    {
        Api = 1,
        Database = 2,
        Cache = 3
    }

    /// <summary>
    /// Name:           PropertyResult
    /// Author:         Adam Berry
    /// Date:           26/02/2016
    /// 
    /// Description:
    /// 
    /// Main Data Class for PropertyResult Object. Instances of this class hold all key information 
    /// about available PropertyResult from JacTravel.
    /// 
    /// Instances of this class are created directly from the XML feed, and bound to
    /// the results page.
    /// </summary>
    [Serializable]
    public class PropertyResult
    {
        public string TotalProperties { get; set; }
        public string PropertyID { get; set; }
        public string PropertyReferenceID { get; set; }
        public string PropertyName { get; set; }
        public string Rating { get; set; }
        public string OurRating { get; set; }
        public string Country { get; set; }
        public string Region { get; set; }
        public string Resort { get; set; }
        public string SearchURL { get; set; }
        public JtRoomType[] RoomTypes { get; set; }
        public JtPropertyDetailsResponse PropertyDetails { get; set; }
    }
}