﻿namespace Affinion.LoyaltyBuild.AdminPortal.DataExtraction
{
    using Affinion.LoyaltyBuild.Common.Exceptions;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using Sitecore.Security.Accounts;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web;

    /// <summary>
    /// Static helper class for data extraction
    /// Always use this with a specific data provider
    /// </summary>
    public static class SitecoreExternalDataExtraction
    {
        /// <summary>
        /// Saves external data to the draft state
        /// </summary>
        /// <param name="dataProvider">Implemented external data provider</param>
        /// <param name="runAsAdmin">Run as admin when context user is not having write permission. This is only for write permission</param>
        /// <returns>Returns the status of the extraction</returns>
        public static SitecoreExternalDataExtractionResult SaveToDraftState(ISitecoreExternalDataProvider dataProvider, bool runAsAdmin = false)
        {
            SitecoreExternalDataExtractionResult result = new SitecoreExternalDataExtractionResult();

            try
            {
                if (dataProvider == null)
                {
                    throw new AffinionException("Data provider is null");
                }

                Database database = dataProvider.GetDatabase();
                Item rootFolder = dataProvider.GetRootFolder();
                string contextItemName = dataProvider.GetContextItemName();
                TemplateItem template = dataProvider.GetTemplate();
                IEnumerable<SitecoreFieldData> data = dataProvider.GetData();
                User contentEditingUser = runAsAdmin ? User.FromName(@"sitecore\admin", true) : Sitecore.Context.User;

                if (rootFolder == null)
                {
                    throw new AffinionException("Affinion.LoyaltyBuild.AdminPortal.DataExtraction.SitecoreExternalDataExtraction : SaveToDraftState: Parent item not found.");
                }

                List<Item> items = rootFolder.Children.Where(i => i.Name == contextItemName).ToList();
                Item contextItem = items != null && items.Count() > 0 ? items[0] : null;

                if (contextItem != null)
                {
                    if (contextItem[dataProvider.GetExternalVersionFieldName()] == dataProvider.GetExternalVersion())
                        throw new AffinionException(string.Format("This request is already processed. go to '{0}' to view content", contextItem.Paths.ContentPath));

                    contextItem.Editing.BeginEdit();
                    contextItem.Versions.AddVersion();
                    contextItem.Editing.EndEdit();
                    contextItem = contextItem.Versions.GetLatestVersion();
                }
                else
                {
                    if (rootFolder == null || template == null)
                    {
                        throw new AffinionException("Affinion.LoyaltyBuild.AdminPortal.DataExtraction.SitecoreExternalDataExtraction : SaveToDraftState: root item or template null.");
                    }

                    if (contentEditingUser == null)
                    {
                        throw new AffinionException("Affinion.LoyaltyBuild.AdminPortal.DataExtraction.SitecoreExternalDataExtraction : SaveToDraftState: Conent Editing User not found.");
                    }

                    using (new Sitecore.Security.Accounts.UserSwitcher(contentEditingUser))
                    {
                        rootFolder.Editing.BeginEdit();
                        rootFolder.Add(contextItemName, template);
                        rootFolder.Editing.EndEdit();

                        items = rootFolder.Children.Where(i => i.Name == contextItemName).ToList();
                        contextItem = items != null && items.Count() > 0 ? items[0] : null;

                        if (contextItem == null)
                        {
                            throw new AffinionException("Affinion.LoyaltyBuild.AdminPortal.DataExtraction.SitecoreExternalDataExtraction : SaveToDraftState: item can not save.");
                        }

                        contextItem.Editing.BeginEdit();
                    }
                }

                contextItem.Editing.BeginEdit();
                StringBuilder errors = new StringBuilder();

                foreach (SitecoreFieldData fieldData in data)
                {
                    try
                    {
                        contextItem[fieldData.Name] = fieldData.Value;
                    }
                    catch (Exception ex)
                    {
                        errors.AppendLine(ex.Message);
                        continue;
                    }
                }

                result.ContextItem = contextItem;
                if (errors.Length > 0)
                {
                    result.ErrorType = SitecoreExternalDataExtractionErrorType.SuceededWithErrors;
                    result.Exception = new AffinionException(errors.ToString());
                }

                contextItem.Editing.EndEdit();
                return result;
            }
            catch(AffinionException ex)
            {
                result.ErrorType = SitecoreExternalDataExtractionErrorType.Failed;
                result.Exception = ex;
                return result;
            }
        }

        /// <summary>
        /// Saves external data to the final state
        /// This operation is depends on user permission
        /// </summary>
        /// <param name="dataProvider">Implemented external data provider</param>
        /// <param name="runAsAdmin">Run as admin when context user is not having write permission. This is only for write permission</param>
        /// <returns>Returns the status of the extraction</returns>
        public static void SaveToFinalState(ISitecoreExternalDataProvider dataProvider, bool runAsAdmin = false)
        {

        }
    }
}