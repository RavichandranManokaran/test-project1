﻿//<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           BookingConfirmationSublayout.ascx.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal
/// Description:           SubLayout for BookingConfirmationSublayout generation
/// </summary>
namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal
{
    #region Using Directives
    using Affinion.Loyaltybuild.BusinessLogic.Helper;
    using Affinion.LoyaltyBuild.Common;
    using Affinion.LoyaltyBuild.Common.Exceptions;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.SessionHelper;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Communications;
    using Affinion.LoyaltyBuild.Communications.Services;
    using Affinion.LoyaltyBuild.Search.Helpers;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Subrate;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Payment;
    using Sitecore.Data.Items;
    using Sitecore.Resources.Media;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Web.UI.WebControls;
    using Affinion.LoyaltyBuild.UCom.Common.Constants;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model.Payment;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Email;
    using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
    using UCommerce.EntitiesV2;
    using UCommerce.Api;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Rules;
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using service = Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Order;
    using Affinion.LoyaltyBuild.Web.ClientPortal.Helper;
    using Affinion.LoyaltyBuild.BedBanks.JacTravel.Message;
    using Affinion.LoyaltyBuild.BedBanks.JacTravel;
    using System.Web.UI.HtmlControls;
    using Affinion.LoyaltyBuild.Model.Product;
    using apiBasket = Affinion.LoyaltyBuild.Api.Booking.Helper;
    using System.Collections.Specialized;
    #endregion

    /// <summary>
    /// SubLayout for BookingConfirmation generation
    /// </summary>
    public partial class BookingConfirmationSublayout : System.Web.UI.UserControl
    {
        List<Item> hotels = new List<Item>();
        List<BasketInfo> listBasketInfo = null;
        List<OrderLineSubrate> OrderLineSubrates = null;
        private service.IOrderService _orderService;
        List<JtBookResponse> book = new List<JtBookResponse>();
        public string Secret { get; set; } // Hold the shared secret of the Merchant
        public string ResponseResult { get; set; }
        public string ResponseMerchantId { get; set; }
        public string ResponseOrderId { get; set; }
        public string ResponseAmount { get; set; }
        public string ResponseMessage { get; set; }
        public string ResponsePasRef { get; set; }
        public string ResponseAuthCode { get; set; }
        public string ResponseTimestamp { get; set; }
        public string ResponseSha1Hash { get; set; }
        public string ResponseOther { get; set; }
        public string CurrentSiteUrl { get; set; }
        public string CurrencySymbol { get; set; }
        public string CurrencyCode { get; set; }
        public string BookedBy { get; set; }
        public string BookingReference { get; set; }
        public string GoBackText { get; set; }
        public string GetHostUrl { get; set; }
        public string SenderEmail
        {
            get { return Sitecore.Context.Item["SenderEmail"]; }
        }
        public string CustomerConfirmationSubject
        {
            get { return Sitecore.Context.Item["CustomerConfirmationSubject"]; }
        }
        public string ProviderConfirmationSubject
        {
            get { return Sitecore.Context.Item["ProviderConfirmationSubject"]; }
        }
        public string PartialPaymentConfirmationSubject
        {
            get { return Sitecore.Context.Item["PartialPaymentConfirmationSubject"]; }
        }

        protected string ConfirmationFooterContent
        {
            get { return Sitecore.Context.Item["ConfirmationFooter"]; }
        }
        protected string BookingRejectedMessage
        {
            get { return Sitecore.Context.Item["BookingRejected"]; }
        }
        protected string BookingConfirmedMessage
        {
            get { return Sitecore.Context.Item["BookingConfirmed"]; }
        }
        protected string BookingPrintMessage
        {
            get { return Sitecore.Context.Item["Print"]; }
        }

        public bool IgnoreHash { get; set; }

        public int orderId { get; set; }
        #region LWP-873
        private static bool IsPageLoaded = false;
        #endregion

        /// <summary>
        /// Page_Init
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Init(Object sender, EventArgs e)
        {
            BindSiteoreText();


        }
        private void SetSession()
        {
            if (Request.QueryString["islocal"] != null)
            {
                ResponseOrderId = Session["ResponseOrderId"].ToString();
                ResponseResult = "00";
                ResponseAmount = Session["ResponseAmount"].ToString();
                ResponseOther = Session["ResponseOther"].ToString();
                IgnoreHash = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, "REALEX: Confirmation page");
                CurrentSiteUrl = "https://" + Sitecore.Context.Site.HostName;
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, "REALEX: cur site url in sub: " + CurrentSiteUrl);

                siteurlfield.Value = CurrentSiteUrl;
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, "REALEX: hiddenfield value: " + siteurlfield.Value);
                ///Read response POST data
                ResponseResult = Request.Form["RESULT"];

                Item PageItem = Sitecore.Context.Item;
                string messageFailureText = (PageItem != null && PageItem.Fields["MailSendingFailureMessage"] != null) ? PageItem.Fields["MailSendingFailureMessage"].Value : string.Empty; ;

                GetHostUrl = System.Web.HttpContext.Current.Request.IsSecureConnection ? "https://" + System.Web.HttpContext.Current.Request.Url.Host : "http://" + System.Web.HttpContext.Current.Request.Url.Host;


                ///Initialize properties using the response parameters
                InitializeResponseProperties();

                if (ResponseResult == null)
                {
                    return;
                }


                // BindSitecoreErrorMessages();

                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("REALEX: anything_else: orderid: {0}", ResponseOther));
                ///Validate returning hash code for security purpose
                if (!IgnoreHash)
                {
                    ValidateHash();
                }

                if (string.Equals(ResponseResult, "00"))
                {

                    Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("REALEX: {0}", ResponseResult));
                    ///Show only success div
                    successDiv.Visible = true;
                    dangerDiv.Visible = false;
                    ///Show booking summary
                    orderId = Convert.ToInt32(ResponseOther);

                    //TODO:Send Bed Banks Book Request

                    if (!IsPostBack)
                    {
                        apiBasket.BbBookRequestHelper bookHelper = new apiBasket.BbBookRequestHelper();
                        book = bookHelper.GetBedBanksBookResponse(orderId);
                    }



                    ///Set currency labels (symbol or sign) in ascx.
                    SetCurrency(orderId);
                    Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("REALEX: converted to int: {0}", orderId));
                    ///Update Availability table
                    UpdateAvailability(orderId);
                    //update dues and payments
                    UpdateDuesAndPayments(orderId);
                    ///Update status of the order
                    UpdateOrderStatus(orderId);
                    ///Show booking summary for each orderline
                    ShowSummary(orderId);
                    //send confirmation mail


                    // UpdateTable();

                    if (!MailGenarate(orderId))
                    {
                        MailErrorDiv.Visible = true;
                        MailErrorLiteral.Text = messageFailureText;
                    }

                    #region LWP-873
                    if (Session["IsClientPortalLoaded"] == null)
                    {
                        AddFinanceCode(orderId);
                        Session["IsClientPortalLoaded"] = "true";
                    }
                    #endregion
                }
                else
                {
                    Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("REALEX: get response order id: ", ResponseOther));
                    orderId = Convert.ToInt32(ResponseOther);
                    BasketHelper.ClearBasket(orderId);
                    ///Show error div
                    dangerDiv.Visible = true;
                    ///Show success div
                    successDiv.Visible = false;

                    if (!SendPaymentExceptionEmail())
                    {
                        MailErrorDiv.Visible = true;
                        MailErrorLiteral.Text = messageFailureText;
                    }
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException("Error is occured in Page_Load " + exception.Message, exception.InnerException);
            }
        }


        //private void UpdateTable()
        //{
        //    Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("REALEX: {0}", ResponseResult));
        //    ///Show only success div
        //    successDiv.Visible = true;
        //    dangerDiv.Visible = false;
        //    ///Show booking summary
        //    orderId = Convert.ToInt32(ResponseOther);

        //    ///Set currency labels (symbol or sign) in ascx.
        //    SetCurrency(orderId);
        //    Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("REALEX: converted to int: {0}", orderId));
        //    ///Update Availability table
        //    UpdateAvailability(orderId);
        //    //update dues and payments
        //    UpdateDuesAndPayments(orderId);
        //    ///Update status of the order
        //    UpdateOrderStatus(orderId);
        //    ///Show booking summary for each orderline
        //    ShowSummary(orderId);
        //    //send confirmation mail
        //    //partial confirmation email
        //    //PartialEmail(orderId);
        //}


        private bool SendPaymentExceptionEmail()
        {
            try
            {
                Item clientItem = SitecoreFieldsHelper.GetLookupFieldTargetItem(ItemHelper.RootItem, "ClientDetails");
                string PaymentExceptionEmailPath = string.Concat("/sitecore/content/client-portal/", clientItem.Name, "/global/_supporting-content/paymentexceptionemail");
                Item PaymentExceptionEmailItem = Sitecore.Context.Database.GetItem(PaymentExceptionEmailPath).Children.FirstOrDefault(i => i.TemplateName == "BaseEmail");
                Affinion.LoyaltyBuild.Communications.IEmailService emailService = new Affinion.LoyaltyBuild.Communications.Services.EmailService();

                string senderID = SitecoreFieldsHelper.GetValue(PaymentExceptionEmailItem, "SenderEmail");
                string mailSubject = SitecoreFieldsHelper.GetValue(PaymentExceptionEmailItem, "Subject");
                string msgBody = SitecoreFieldsHelper.GetValue(PaymentExceptionEmailItem, "EmailBody");
                string receiver = SitecoreFieldsHelper.GetValue(PaymentExceptionEmailItem, "ReceiverEmail");

                msgBody = string.Format(msgBody, ResponseResult, ResponseMessage);

                emailService.Send(senderID, receiver, mailSubject, msgBody, true);
                return true;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                return false;
            }
        }

        private void UpdateOrderlineSubrates(int orderId)
        {
            BasketHelper.UpdateOrderLineSubrate(OrderLineSubrates, orderId);
        }

        /// <summary>
        /// Set currency in ascx
        /// </summary>
        /// <param name="orderId">Current order id</param>
        private void SetCurrency(int orderId)
        {
            string currency = UCommerce.EntitiesV2.PurchaseOrder.Get(orderId).BillingCurrency.ISOCode;

            string symbol = StoreHelper.GetCurrencySymbol(currency);

            CurrencySymbol = string.IsNullOrEmpty(symbol) ? string.Empty : string.Format("{0} ", symbol);

            ///Set the currency code if a symbol is not available
            if (string.Equals(symbol, currency))
            {
                CurrencyCode = string.Format(" {0}", currency);
                CurrencySymbol = string.Empty;
            }
        }

        /// <summary>
        /// Initialize response properties
        /// </summary>
        private void InitializeResponseProperties()
        {
            ResponseMerchantId = Request.Form["MERCHANT_ID"];
            ResponseOrderId = Request.Form["ORDER_ID"];
            ResponseAmount = Request.Form["AMOUNT"];
            ResponseMessage = Request.Form["MESSAGE"];
            ResponsePasRef = Request.Form["PASREF"];
            ResponseAuthCode = Request.Form["AUTHCODE"];
            ResponseTimestamp = Request.Form["TIMESTAMP"];
            ResponseSha1Hash = Request.Form["SHA1HASH"];
            ResponseOther = Request.Form["ANYTHING ELSE"];
            SetSession();
        }

        /// <summary>
        /// Update availability table
        /// </summary>
        private void UpdateAvailability(int orderId)
        {
            Diagnostics.Trace(DiagnosticsCategory.ClientPortal, "REALEX: inside update availability method");
            BasketHelper.UpdateAvailability(orderId);
        }

        /// <summary>
        /// Update availability table
        /// </summary>
        private void UpdateDuesAndPayments(int orderId)
        {
            Diagnostics.Trace(DiagnosticsCategory.ClientPortal, "REALEX: inside update dues and payments method");
            BasketHelper.UpdateDuesAndPayments(orderId);
        }

        /// <summary>
        /// Show booking summary
        /// </summary>
        private void ShowSummary(int orderId)
        {
            try
            {


                /// Put user code to initialize the page here
                hotels = Sitecore.Context.Database.SelectItems(Constants.BasketPageQueryPath).ToList();

                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("REALEX: hotel count: {0}", hotels.Count()));



                listBasketInfo = new List<BasketInfo>();
                listBasketInfo = BasketHelper.PopulateConfirmationDataForClientPortal(orderId);
                Booking booking = new Booking();


                //Query to get the supplier which has the same UCommerceCategoryID 
                var supplierList = (from itm in listBasketInfo
                                    join supplier in hotels on itm.Sku equals StoreHelper.GetUcommerceProductSku(supplier).ToString()
                                    select supplier).ToList();

                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("REALEX: supplierList count: {0}", supplierList.Count()));

                Item clientItem = SitecoreFieldsHelper.GetLookupFieldTargetItem(ItemHelper.RootItem, "ClientDetails");
                BookedBy = clientItem.DisplayName;

                foreach (var item in listBasketInfo)
                {
                    Affinion.LoyaltyBuild.Api.Booking.Service.BookingService bookingService = new Api.Booking.Service.BookingService();
                    var bookingDetails = bookingService.GetBooking(Convert.ToInt32(item.OrderLineId));

                    if (!string.IsNullOrEmpty(bookingDetails.Type.ToString()) && !bookingDetails.Type.ToString().ToLower().Equals("bedbanks"))
                    {
                        item.ProductType = bookingDetails.Type.ToString();
                        item.SitecoreItem = supplierList.FirstOrDefault();
                        supplierList.RemoveAt(0);// remove first item
                    }
                }
                PanelRepeater.DataSource = listBasketInfo;
                PanelRepeater.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Update status of the purchase order
        /// </summary>
        private void UpdateOrderStatus(int orderId)
        {
            try
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, "REALEX: inside updateorderstatus method");
                var currentOrder = BasketHelper.GetPurchaseOrder(orderId);
                BasketHelper.UpdateOrderStatus(currentOrder, 6); //6 is the order status ID of "Paid" status
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException("Error ocuured in a method(UpdateOrderStatus()). More Details" + exception, exception.InnerException);
            }
        }

        /// <summary>
        /// Validate the response hash
        /// </summary>
        private void ValidateHash()
        {
            Diagnostics.Trace(DiagnosticsCategory.ClientPortal, "REALEX: inside ValidateHash method");
            ///Load the portal specific payment method. TODO: Read from sitecore configuration
            Item clientPaymentMethodItem = SitecoreFieldsHelper.GetDropLinkFieldTargetItem(ItemHelper.RootItem, "ServiceName");
            UCommerce.EntitiesV2.PaymentMethod paymentMethod = UCommerce.EntitiesV2.PaymentMethod.SingleOrDefault(x => x.Name == clientPaymentMethodItem.Fields["ServiceName"].Value);
            var paymentMethodProperties = paymentMethod.PaymentMethodProperties;
            Secret = paymentMethodProperties.FirstOrDefault(x => x.DefinitionField.Name == "Secret").Value;
            Diagnostics.Trace(DiagnosticsCategory.ClientPortal, "REALEX: retrieved ucom realex payment method");

            ///Generate the hash using SHA1 algorithm
            var sha1Hash = PaymentGatewayHelper.GetHash(string.Format("{0}.{1}", PaymentGatewayHelper.GetHash(string.Format("{0}.{1}.{2}.{3}.{4}.{5}.{6}", ResponseTimestamp, ResponseMerchantId, ResponseOrderId, ResponseResult, ResponseMessage, ResponsePasRef, ResponseAuthCode)), Secret));

            ///Validate the response HASH
            if (!string.Equals(ResponseSha1Hash, sha1Hash))
            {
                throw new AffinionException("Invalid payment request. Response hash not matched");//TODO: Exception or any other solution ?
            }
        }

        protected void PanelRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Repeater starRepeater = (Repeater)e.Item.FindControl("StarRepeater");
                Image hotelImage = (Image)e.Item.FindControl("hotelImage");
                BasketInfo dataItem = ((BasketInfo)e.Item.DataItem);
                var dataItemSitecore = ((BasketInfo)e.Item.DataItem).SitecoreItem;
                string ranking = string.Empty;

                string BillingCurrency = UCommerce.EntitiesV2.PurchaseOrder.Get(orderId).BillingCurrency.ISOCode;
                OrderLine orderLine = UCommerce.EntitiesV2.OrderLine.FirstOrDefault(i => i.OrderLineId == int.Parse(dataItem.OrderLineId));
                if (orderLine != null)
                {

                    if (orderLine[OrderPropertyConstants.PaymentMethod] == Constants.PaymentMethod.PartialPayment)
                    {
                        string daysBefore = orderLine[OrderPropertyConstants.DaysBefore];

                        Literal pricePartial = e.Item.FindControl("PriceAccommodation") as Literal;
                        pricePartial.Text = string.Format("Total Payable {0} Days Prior to Arrival:", daysBefore);
                        pricePartial.Text = CurrencyHelper.GetCurrency(BillingCurrency) + dataItem.PayableAtAccommodation;

                    }

                    Literal priceTotalLiteral = (Literal)e.Item.FindControl("PriceAccommodation");
                    priceTotalLiteral.Text = StoreHelper.GetPriceWithCurrency(Convert.ToDecimal(dataItem.PayableAtAccommodation), null, BillingCurrency);

                    Literal processingPriceLiteral = (Literal)e.Item.FindControl("DepositPayableToday");
                    processingPriceLiteral.Text = StoreHelper.GetPriceWithCurrency(Convert.ToDecimal(dataItem.DepositPayableToday), null, BillingCurrency);

                    Literal ProcessingFeeLiteral = (Literal)e.Item.FindControl("ProcessingFee");
                    ProcessingFeeLiteral.Text = StoreHelper.GetPriceWithCurrency(Convert.ToDecimal(dataItem.ProcessingFee), null, BillingCurrency);
                }

                if (dataItem.ProductType != null && dataItem.ProductType.ToLower().Equals("bedbanks"))
                {
                    if (dataItem != null)
                    {
                        string propertyId = string.IsNullOrEmpty(dataItem.SupplierId) ? string.Empty : dataItem.SupplierId;
                        JtPropertyDetailsResponse propertyDetailsRes = JtController.GetSinglePropertyDetails(propertyId);
                        hotelImage.ImageUrl = string.IsNullOrEmpty(dataItem.BBImageUrl) ? string.Empty : dataItem.BBImageUrl;
                        ranking = string.IsNullOrEmpty(dataItem.BBStarRanking) ? string.Empty : dataItem.BBStarRanking;
                        if (propertyDetailsRes != null)
                        {
                            HtmlGenericControl nameDiv = e.Item.FindControl("divLBName") as HtmlGenericControl;
                            HtmlGenericControl lBaddDiv = e.Item.FindControl("addLB") as HtmlGenericControl;
                            HtmlGenericControl lBphDiv = e.Item.FindControl("phLB") as HtmlGenericControl;
                            HtmlGenericControl lBemailDiv = e.Item.FindControl("emailLB") as HtmlGenericControl;
                            Literal hotelName = e.Item.FindControl("litBBHotelName") as Literal;
                            Literal add1 = e.Item.FindControl("litBBAdd1") as Literal;
                            Literal add2 = e.Item.FindControl("litBBAdd2") as Literal;
                            Literal add3 = e.Item.FindControl("litBBAdd3") as Literal;
                            Literal add4 = e.Item.FindControl("litBBAdd4") as Literal;
                            Literal phone = e.Item.FindControl("litBBph") as Literal;
                            Literal email = e.Item.FindControl("litBBEmail") as Literal;
                            if (hotelName != null)
                                hotelName.Text = propertyDetailsRes.PropertyName;
                            nameDiv.Visible = false;
                            if (add1 != null && add2 != null && add3 != null & add4 != null)
                            {
                                add1.Text = propertyDetailsRes.Address1;
                                add2.Text = propertyDetailsRes.Address2;
                                add3.Text = propertyDetailsRes.Region;
                                add4.Text = propertyDetailsRes.TownCity;
                            }
                            lBaddDiv.Visible = false;
                            if (phone != null)
                                phone.Text = propertyDetailsRes.Telephone;
                            lBphDiv.Visible = false;
                            if (email != null)
                            {
                                email.Text = string.Empty;
                            }
                            lBemailDiv.Visible = false;
                        }
                    }

                }
                else
                {
                    MediaUrlOptions muo = new MediaUrlOptions();
                    muo.AlwaysIncludeServerUrl = true;
                    if (((BasketInfo)e.Item.DataItem).SitecoreItem != null)
                    {

                        Sitecore.Data.Fields.FileField fileField = (((BasketInfo)e.Item.DataItem).SitecoreItem).Fields["IntroductionImage"];
                        if (fileField.MediaItem != null)
                        {
                            string url = MediaManager.GetMediaUrl(fileField.MediaItem, muo);
                            hotelImage.ImageUrl = url;
                        }
                    }
                    ranking = SitecoreFieldsHelper.GetDropLinkFieldValue(dataItemSitecore, "StarRanking", "Name");
                    HtmlGenericControl nameDiv = e.Item.FindControl("divBBName") as HtmlGenericControl;
                    HtmlGenericControl bBaddDiv = e.Item.FindControl("addBB") as HtmlGenericControl;
                    HtmlGenericControl bBphDiv = e.Item.FindControl("phBB") as HtmlGenericControl;
                    HtmlGenericControl bBemailDiv = e.Item.FindControl("emailBB") as HtmlGenericControl;
                    nameDiv.Visible = false;
                    bBaddDiv.Visible = false;
                    bBphDiv.Visible = false;
                    bBemailDiv.Visible = false;
                }

                if (!string.IsNullOrWhiteSpace(ranking))
                {
                    int starRank;
                    bool result = Int32.TryParse(ranking, out starRank);
                    if (result)
                    {
                        List<int> listOfStars = Enumerable.Repeat(starRank, starRank).ToList();
                        starRepeater.DataSource = listOfStars;
                        starRepeater.DataBind();
                    }
                    else
                    {
                        Diagnostics.WriteException(DiagnosticsCategory.ClientPortal,
                        new AffinionException("Unable to parse star rank:" + dataItemSitecore.DisplayName), this);
                    }
                }
            }
        }

        private string GetImageURL(Item currentItem, string fieldName)
        {
            string imageURL = string.Empty;
            Sitecore.Data.Fields.ImageField imageField = currentItem.Fields[fieldName];
            if (imageField != null && imageField.MediaItem != null)
            {
                Sitecore.Data.Items.MediaItem image = new Sitecore.Data.Items.MediaItem(imageField.MediaItem);
                imageURL = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image));
            }
            return imageURL;
        }

        /// <summary>
        /// Send Booking Confirmation Email
        /// </summary>
        private bool MailGenarate(int orderID)
        {
            try
            {
                ISitecorePaymentService sitecorePaymentService = new SitecorePaymentService();
                //Affinion.LoyaltyBuild.Api.Booking.Data.BasketInfo newBasketData = Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.GetBasket();
                Affinion.LoyaltyBuild.Api.Booking.Service.BookingService bs = new Api.Booking.Service.BookingService();
                Affinion.LoyaltyBuild.Api.Booking.Data.BasketInfo bi = bs.GetPurchaseOrder(orderId);
                var pType = bi.Bookings.Where(x => x.Type == ProductType.BedBanks);
                var ClientId = bi.Bookings.First().ClientId;
                MailService mailService = new MailService(ClientId);
                DataSet orderLineDetails = BasketHelper.GetOrderLines(orderId);
                int bbOrderLineId = 0;
                int iOrderLineId=0;
                for (int i = 0; i < orderLineDetails.Tables[0].Rows.Count; i++)
                {
                    string orderlineid = orderLineDetails.Tables[0].Rows[i][0].ToString();
                    if (!string.IsNullOrWhiteSpace(orderlineid))
                    {
                        
                        if (int.TryParse(orderlineid, out iOrderLineId))
                        {
                            bbOrderLineId = pType.Where(x => x.OrderLineId.Equals(iOrderLineId)).Select(y => y.OrderLineId).FirstOrDefault();
                            var paymentMethod = sitecorePaymentService.GetPaymentMethod(orderId);
                            var selectedPayment = paymentMethod.FirstOrDefault(x => x.OrderLineId == iOrderLineId);
                            string paymentMethodName = string.Empty;
                            if (selectedPayment != null)
                                paymentMethodName = selectedPayment.Paymentmethod;
                            if (paymentMethodName == Constants.PaymentMethod.PartialPayment)
                                mailService.GenerateEmail(MailTypes.PartialCustomerConfirmationOnline, iOrderLineId);
                            else
                            {
                                if (bbOrderLineId > 0)
                                {
                                    NameValueCollection addInfo = new NameValueCollection();
                                    addInfo[Constants.ConditionalMessageItemId] = "{056B5DFF-ED1E-4C1D-96B2-014F7621C1AE}"; //Need to change
                                    // Bed banks Booking confirmation----
                                    mailService.GenerateEmail(MailTypes.BedBanksCustomerConfirmation, iOrderLineId, addInfo);
                                }
                                else
                                {
                                    mailService.GenerateEmail(MailTypes.CustomerConfirmationOnline, iOrderLineId);
                                    mailService.GenerateEmail(MailTypes.ProviderConfirmationOnline, iOrderLineId);
                                }
                            }
                        }
                    }
                }

                if (book != null && book.Count > 0)
                {
                    foreach (JtBookResponse response in book)
                    {
                        if (!response.ReturnStatus.Success.ToLower().Equals("true"))
                        {
                            if (response.ReturnStatus.Success.ToLower().Equals("false") && response.ReturnStatus.Exception == "API Exception")
                            {
                                mailService.GenerateEmail(MailTypes.BedBanksConfirmationIssue, iOrderLineId);
                                //API Exception
                            }
                            else if (response.ReturnStatus.Success.ToLower().Equals("false"))
                            {
                                mailService.GenerateEmail(MailTypes.BedBanksConfirmationIssue, iOrderLineId);
                                //Booking failure
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                return false;
            }

        }


        /// <summary>
        ///  send provider confirmation mail
        /// </summary>
        /// <param name="orderID"></param>
        private void SendProviderConfirmationEmail(int orderId)
        {
            try
            {
                if (string.Equals(SenderEmail, null))
                {
                    throw new AffinionException("SenderEmail that  Field of Booking Confirmation is not configured");
                }

                IEmailService emailService = new EmailService();
                DataSet ds = BasketHelper.GetOrderReservation(orderId);
                string senderID = SenderEmail;
                List<string> receiverList = new List<string>();
                receiverList = BasketHelper.getProviderEmail(orderId, ds);
                string receiver = null;

                List<string> msgList = BasketHelper.GetPopulateProviderMailBody(orderId, ds, OrderLineSubrates);
                int mailIndex = 0;

                for (int i = 0; i <= receiverList.Count - 2; i = i + 2)
                {
                    receiver = receiverList[i];
                    if (string.IsNullOrEmpty(receiver))
                    {
                        throw new AffinionException("Email ID 2 that  Field of" + receiverList[i + 1] + "  hotel is not configured");
                    }
                    string msgBody = msgList[mailIndex];
                    mailIndex = mailIndex + 1;
                    int number = mailIndex;
                    string mailSubject = string.Format("{0} [Booking {1} of {2} ]", ProviderConfirmationSubject, number.ToString(), msgList.Count.ToString());
                    emailService.Send(senderID, receiver, mailSubject, msgBody, true);
                }
            }
            catch (Exception exception)
            {
                throw new AffinionException("Error occured in a method (SendCustomerConfirmationEmail). More Details:" + exception.Message, exception);
            }
        }

        /// <summary>
        /// send customer confirmation mail
        /// </summary>
        /// <param name="orderID"></param>
        private void SendCustomerConfirmationEmail(int orderId)
        {
            try
            {
                if (string.Equals(CustomerConfirmationSubject, null))
                {
                    throw new AffinionException("CustomerConfirmationSubject that  Field of Booking Confirmation is not configured ");
                }

                IEmailService emailService = new EmailService();
                DataSet ds = BasketHelper.GetOrderReservation(orderId);

                string receiver = BasketHelper.getCustomerEmail(orderId, ds);

                List<string> msgList = BasketHelper.GetPopulateCustomerMailBody(orderId, ds, OrderLineSubrates);

                for (int i = 0; i < msgList.Count; i++)
                {
                    string msgBody = msgList[i];
                    int number = i + 1;
                    string mailSubject = string.Format("{0} [Booking {1} of {2} ]", CustomerConfirmationSubject, number.ToString(), msgList.Count.ToString());
                    emailService.Send(SenderEmail, receiver, mailSubject, msgBody, true);
                }
            }
            catch (Exception exception)
            {
                throw new AffinionException("Error occured in a method (SendCustomerConfirmationEmail). More Details:" + exception.Message, exception);
            }

        }

        #region LWP-873
        private void AddFinanceCode(int purchaseOrderId)
        {
            try
            {
                IPaymentService _paymentService = ContainerFactory.Instance.GetInstance<IPaymentService>();
                FinanceCode objFinanceCode = _paymentService.GetClientAndProviderID(purchaseOrderId, 0);

                string ProviderPath = Constants.ProviderPath;
                string productCode = string.Empty;
                long fourDigitMerchantID = 0;

                Item ClientConfigItems = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(objFinanceCode.ClientId));
                Item[] providers = Sitecore.Context.Database.SelectItems(ProviderPath);
                Item providersItem = providers.Where(x => Guid.Parse(x.ID.ToString()).ToString().Equals(objFinanceCode.ProviderId, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                Item providertype = SitecoreFieldsHelper.GetDropLinkFieldTargetItem(providersItem, "SupplierType");
                Dictionary<string, string> FieldsWithInSection = new Dictionary<string, string>();
                objFinanceCode.OrderId = purchaseOrderId;
                objFinanceCode.OrderLineId = 0;
                if (ClientConfigItems != null && ClientConfigItems.Fields != null)
                {
                    if (ClientConfigItems.Fields["Company"] != null && ClientConfigItems.Fields["Company"].Value != string.Empty)
                        objFinanceCode.Company = ClientConfigItems.Fields["Company"].Value;
                    else
                        objFinanceCode.Company = "0";

                    var currencySection = ClientConfigItems.Fields.Where(name => name.Section == objFinanceCode.Currency).ToDictionary(field => field.Name, field => field.Value);
                    if (currencySection != null && currencySection.Count > 0 && !string.IsNullOrEmpty(currencySection["Online MID Value"]))
                        objFinanceCode.MerchantIdentificationNo = Int32.Parse(currencySection["Online MID Value"]);
                    else
                        objFinanceCode.MerchantIdentificationNo = 0;

                    if (ClientConfigItems.Fields["Department"] != null && ClientConfigItems.Fields["Department"].Value != string.Empty)
                        objFinanceCode.Department = ClientConfigItems.Fields["Department"].Value;
                    else
                        objFinanceCode.Department = "0";

                    if (ClientConfigItems.Fields["Client Code"] != null && ClientConfigItems.Fields["Client Code"].Value != string.Empty)
                        objFinanceCode.ClientCode = ClientConfigItems.Fields["Client Code"].Value;
                    else
                        objFinanceCode.ClientCode = string.Empty;

                    if (ClientConfigItems.Fields["Processing Fee"] != null && ClientConfigItems.Fields["Processing Fee"].Value != string.Empty)
                        objFinanceCode.GlCode = ClientConfigItems.Fields["Processing Fee"].Value;
                    else
                        objFinanceCode.GlCode = string.Empty;

                    if (providertype != null)
                    {
                        if (providertype.Fields != null && providertype.Fields["Product"].Value != string.Empty)
                        {
                            productCode = providertype.Fields["Product"].Value;
                        }
                    }
                }

                objFinanceCode.ClientName = ClientConfigItems.DisplayName;
                objFinanceCode.ProductCode = productCode;

                fourDigitMerchantID = objFinanceCode.MerchantIdentificationNo;
                objFinanceCode.Description = "EMSXXX" + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + "XXXX" + fourDigitMerchantID.ToString().Substring(fourDigitMerchantID.ToString().Length - 4);

                ValidationResponse financeCodeResult = _paymentService.AddFinaceCode(objFinanceCode);
                IsPageLoaded = true;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, this);

            }

        }
        #endregion

        private void BindSiteoreText()
        {
            Item currentPageItem = Sitecore.Context.Item;
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "BookingRejected", BookingRejectedMessageLabel);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "BookingConfirmed", BookingConfirmedMessageLabel);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "GoBackButton", GoBackButtonText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "Print", BookingPrintMessageLabel);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "ConfirmationFooter", ConfirmationFooterLabel);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "PrintButton", PrintButtonLabel);
            SitecoreFieldsHelper.BindSitecoreText(currentPageItem, "BookingSummaryText", BookingSummaryText);

            //this.GoBackText = SitecoreFieldsHelper.GetValue(currentPageItem, "GoBackButton");
            //btnGoBack.Attributes.Add("value", this.GoBackText);
        }


    }


}




