﻿CREATE PROCEDURE [dbo].[OLD_HasActiveCustomerForClient]
(
	@ClientId VARCHAR(50),
	@BookingReferenceId VARCHAR(50)
)
AS
BEGIN
    IF EXISTS (SELECT * FROM uCommerce_Customer_Attribute as CA WHERE (CA.BookingReferenceId = @BookingReferenceId) AND (CA.ClientId = @ClientId) AND  (CA.Active=1))
        SELECT 1
    ELSE
        SELECT 0  
END

