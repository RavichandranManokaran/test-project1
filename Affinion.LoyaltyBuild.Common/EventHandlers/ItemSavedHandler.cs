﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           ItemSavedHandler.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Handling event when a item is saved
/// </summary>

using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Data.Items;
using Sitecore.Events;
using Sitecore.SecurityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Common.EventHandlers
{
    public class ItemSavedHandler
    {
        #region Private Constants
        /// <summary>
        /// Template name to store secure configuration
        /// </summary>
        private const string SecureConfigurationTemplateName = "SecureConfiguration";
        /// <summary>
        /// Custom Error message to show when encryption error occured
        /// </summary>
        private const string EncryptExceptionMessage = "Error occured when encrypting item data";
        /// <summary>
        /// Custom error message responce
        /// </summary>
        private const string EncryptExceptionMessageResponce = "Encryption Error Occured";
        /// <summary>
        /// Custom error message header of the popup
        /// </summary>
        private const string EncryptExceptionMessageHeader = "Encryption Error";
        /// <summary>
        /// Field name of the encrypting sitecore item
        /// </summary>
        private const string ItemFieldName = "Value";
        /// <summary>
        /// Master database name
        /// </summary>
        private const string MasterDatabaseName = "master";
        #endregion

        public void OnItemSaved(object sender, EventArgs args)
        {
            try
            {
                /// Extract the currently editing item from the event Arguments
                SitecoreEventArgs eventArgs = args as SitecoreEventArgs;
                Item item = eventArgs.Parameters[0] as Item;
                ///Change icon when saving the item by checking is active
                ChangeIconOnActive(item);
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Show custom Error message 
                // TODO: Handle in a general way(error page or a error label in layout)
                //Sitecore.Context.ClientPage.ClientResponse.Alert(EncryptExceptionMessage, EncryptExceptionMessageResponce, EncryptExceptionMessageHeader);
                throw;
            }
        }

        /// <summary>
        /// Change the item when is active set to true
        /// </summary>
        /// <param name="item">Item to change the item</param>
        public void ChangeIconOnActive(Item item)
        {
            ItemHelper.ChangeIconField(item, null, "IsActive");
        }

        /// <summary>
        /// Encrypt an item when saved
        /// </summary>
        /// <param name="savingItem">Item to encrypt</param>
        public void EncryptItemOnSaved(Item savingItem)
        {
            /// Allow only non null items and allow only items from the master database
            if (savingItem != null && savingItem.Database.Name.ToLower().Equals(MasterDatabaseName))
            {
                /// Encrypt only items with predefined template
                if (savingItem.TemplateName == SecureConfigurationTemplateName)
                {
                    /// Get the current item value to encrypt
                    string fieldValue = savingItem.Fields[ItemFieldName].Value;

                    /// Start Editing the Item
                    using (new SecurityDisabler())
                    {
                        /// Encrypt the current item value and save
                        savingItem.Fields[ItemFieldName].Value = Cryptography.Encrypt(fieldValue);
                    }

                    ///If the current item is not in the cache, add it to the cache: This is to prevent recursively occuring of item saving event handler
                    if (Sitecore.Context.Database.Caches.ItemCache.GetItem(savingItem.ID, Sitecore.Globalization.Language.Invariant, Sitecore.Data.Version.First) == null)
                    {
                        ///Add current item to item cache
                        Sitecore.Context.Database.Caches.ItemCache.AddItem(savingItem.ID, Sitecore.Globalization.Language.Invariant, Sitecore.Data.Version.First, savingItem);
                    }
                }
            }
        }
    }
}
