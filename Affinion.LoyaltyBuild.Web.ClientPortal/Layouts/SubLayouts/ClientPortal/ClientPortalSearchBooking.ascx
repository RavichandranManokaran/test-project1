﻿<%@ control language="C#" autoeventwireup="true" codebehind="ClientPortalSearchBooking.ascx.cs" inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal.ClientPortalSearchBooking" %>

<div class="content">
    <div class="content-body">
        <div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="accordian-outer">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading bg-primary">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="show-hide-list">
                                           <sc:text id="heading" field="Heading" runat="server" /> <asp:Literal ID="litHeading" runat="server" /></a>
                                    </h4>
                                </div>
                                <div id="collapse1" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div>
                                            <div class="content_bg_inner_box summary-detail alert-info">
                                                <div class="col-xs-12 col-md-12">
                                                    <div class="hotel-name-detail ">
                                                        <h2 class="no-margin accent54-f">
                                                           <sc:text id="subHeading" field="Sub Heading" runat="server" /></h2>
                                                        <div class="row margin-row">
                                                            <div class="col-sm-5">
                                                                <span class="bask-form-titl font-size-12">
                                                                  <sc:text id="refNo" field="Booking Reference Number Text" runat="server" /></span>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <asp:TextBox ID="txtReferenceNumber" runat="server" MaxLength="75" CssClass="form-control font-size-12" />
                                                                <asp:RequiredFieldValidator ID="rfvBookingNumber" runat="server"
                                                                    ForeColor="Red" ControlToValidate="txtReferenceNumber" Display="Dynamic" ValidationGroup="rtvBooking">
                                                                    <sc:text id="txtReferenceNumberRequiredText" runat="server" visible="false" />
                                                                </asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revBookingNumber" runat="server" ValidationGroup="rtvBooking" ControlToValidate="txtReferenceNumber" ForeColor="Red" ValidationExpression="^[A-Z,a-z]{3}[0-9]{10}$" Display="Dynamic">
                                                                    <sc:text id="txtReferenceNumberRegularText" runat="server" />
                                                                </asp:RegularExpressionValidator>
                                                            </div>
                                                        </div>
                                                        <div class="row margin-row">
                                                            <div class="col-sm-5">
                                                                <span class="bask-form-titl font-size-12">
                                                                     <sc:text id="nameLabel" field="Name Text" runat="server" /></span>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <asp:TextBox ID="txtName" runat="server" MaxLength="75" CssClass="form-control font-size-12" />
                                                                <asp:RequiredFieldValidator ID="rfvName" runat="server" ForeColor="Red"
                                                                    ControlToValidate="txtName" Display="Dynamic" ValidationGroup="rtvBooking">
                                                                    <sc:text id="txtNameRequiredText" runat="server" />
                                                                </asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revName" runat="server" ValidationGroup="rtvBooking" ControlToValidate="txtName" ForeColor="Red" ValidationExpression="^[^<>\\&quot;/;`%?0-9]*$" Display="Dynamic">
                                                                    <sc:text id="txtNameRegularText" runat="server" />
                                                                </asp:RegularExpressionValidator>
                                                            </div>
                                                        </div>
                                                        <div class="row margin-row">
                                                            <div class="col-sm-5">
                                                                <span class="bask-form-titl font-size-12">
                                                                   <sc:text id="checkin" field="Check In Date Text" runat="server" /></span>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <div class="form-group">
                                                                    <div class="input-group date" id="mybookingcal">
                                                                        <asp:TextBox ID="txtCheckInDate" runat="server" ReadOnly="false" CssClass="form-control" />
                                                                        <asp:RegularExpressionValidator runat="server" id="monthValidator" ControlToValidate="txtCheckInDate" ErrorMessage="Wrong Date Format" ForeColor="Red"  ValidationExpression="^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$" />
                                                                      
                                                                        <span class="input-group-addon">
                                                                           <button id="btnShowCalnedar" type="submit" class="btn btn-link pad0 btnShowCalnedar" causesvalidation="false">
                                                                               <span>&nbsp;</span>
                                                                           </button>
                                                                        </span>
                                                                        
                                                                    </div>
                                                                      <asp:RequiredFieldValidator ID="rfvCheckInDate" runat="server" ForeColor="Red"
                                                                            ControlToValidate="txtCheckInDate" Display="Dynamic" ValidationGroup="rtvBooking">
                                                                            <sc:text id="txtCheckInDateRequiredText" runat="server" visible="false" />
                                                                        </asp:RequiredFieldValidator>
                                                                    <span class="pull-right">
                                                                           <%-- <asp:Calendar ID="calCheckinDate" runat="server" BackColor="White" BorderColor="#3366CC" BorderWidth="1px" CellPadding="1" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="#003399" Height="200px" Width="220px" OnSelectionChanged="calCheckinDate_SelectionChanged" Visible="false">
                                                                                <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" Font-Underline="False" />
                                                                                <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
                                                                                <OtherMonthDayStyle ForeColor="#999999" />
                                                                                <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                                                                                <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
                                                                                <TitleStyle BackColor="#003399" BorderColor="#3366CC" BorderWidth="1px" Font-Bold="True" Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
                                                                                <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
                                                                                <WeekendDayStyle BackColor="#CCCCFF" />
                                                                            </asp:Calendar>--%>
                                                                        </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row margin-row">
                                                            <div class="col-sm-3"></div>
                                                            <div class="col-sm-9">
                                                                <button type="button" runat="server" class="btn btn-default add-break font-size-12 pull-right" runat="server" onserverclick="btnSubmit_Click" validationgroup="rtvBooking" ><sc:text id="button1" field="Search Button Text" runat="server" /></button>
                                                            </div>
                                                        </div>
                                                        <div class="alert alert-danger margin-row" role="alert" id="errorMessageWrapper" runat="server" visible="false">
                                                            <sc:text id="txtErrorText" runat="server" visible="false" />
                                                            <%--<asp:Label ID="lblError" runat="server" Visible="false" Text="Data not found. Please try again." />--%>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="content_bg_inner_box summary-detail alert-info">
                                                <div class="col-xs-12 col-md-12">
                                                    <div class="hotel-name-detail ">
                                                        <h2 class="no-margin accent54-f"><sc:text id="subHeading1" field="Sub Heading 1 Text" runat="server" /></h2>
                                                        <div class="hotel-name-detail-bottom">
                                                            <p class="no-margin margin-bottom-15"><span><sc:text id="instTxt" field="Instruction Text" runat="server" /></span></p>
                                                        </div>
                                                        <div class="row margin-row">
                                                            <div class="col-sm-5"><span class="bask-form-titl font-size-12"><sc:text id="emailTxt" field="Email Text" runat="server" /></span></div>
                                                            <div class="col-sm-7">
                                                                <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control font-size-12" />
                                                                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ForeColor="Red" ControlToValidate="txtEmail" Display="Dynamic" ValidationGroup="rtvEmail">
                                                                    <sc:text id="txtEmailRequiredText" runat="server" visible="false" />
                                                                </asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="Red" Display="Dynamic" ValidationGroup="rtvEmail">
                                                                    <sc:text id="txtEmailRegularText" runat="server" />
                                                                </asp:RegularExpressionValidator>
                                                                <!--<input type="text" placeholder="" id="textEmail" runat="server" class="form-control font-size-12">-->

                                                            </div>
                                                        </div>
                                                        <div class="row margin-row">
                                                            <div class="col-sm-5"><span class="bask-form-titl font-size-12"><sc:text id="lastNameTxt" field="Last Name Text" runat="server" /></span></div>
                                                            <div class="col-sm-7">
                                                                <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control font-size-12" />
                                                                <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ForeColor="Red" ControlToValidate="txtLastName" Display="Dynamic" ValidationGroup="rtvEmail">
                                                                    <sc:text id="txtLastNameRequiredText" runat="server" visible="false" />
                                                                </asp:RequiredFieldValidator>
                                                                <asp:RegularExpressionValidator ID="revLastName" runat="server" ValidationGroup="rtvEmail" ControlToValidate="txtLastName" ForeColor="Red" ValidationExpression="^[^<>\\&quot;/;`%?0-9]*$" Display="Dynamic">
                                                                    <sc:text id="txtLastNameRegularText" runat="server" />
                                                                </asp:RegularExpressionValidator>
                                                                <!--<input type="text" placeholder="" class="form-control font-size-12">-->
                                                            </div>
                                                        </div>
                                                        <div class="row margin-row">
                                                            <div class="col-sm-3"></div>
                                                            <div class="col-sm-9">
                                                                <button type="button" id="btnRtr" class="btn btn-default add-break font-size-12 pull-right" runat="server" onserverclick="btnRetrive_Click" validationgroup="rtvEmail"><sc:text id="btnTxt" field="Search Button 1 Text" runat="server" /></button>
                                                                <!--<asp:Button ID="btnRetrieve" Text="Retrive" CssClass="btn btn-default add-break font-size-12 pull-right" OnClick="btnRetrive_Click" runat="server" ValidationGroup="rtvEmail"></asp:Button>
                                                                --><div id="emailMessageWrapper" runat="server" visible="false">
                                                                    <%--<asp:Literal id="mailSentMessage" runat="server" text="Booking Reference is sent to your registered email" visible="false"></asp:Literal>--%>
                                                                    <sc:text id="txtMailSentMessageText" runat="server" visible="false" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 <script type="text/javascript">
     $(function () {
         jQuery('#maincontent_0_txtCheckInDate').datepicker({
             defaultDate: new Date(),
             dateFormat: "<%=Affinion.LoyaltyBuild.Common.Utilities.DateTimeHelper.MonthDateFormat%>",
             numberOfMonths: 1,
             showOn: "both",
            // buttonImage: "images/calendar.png",
             buttonImageOnly: false,
             buttonClass: "btnShowCalnedar"
         }).next(".ui-datepicker-trigger").addClass("btn btn-link glyphicon glyphicon-calendar calander-icon");
     }
     );
 </script>

