﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Maps
{
    public class SeasonUnitDiscountAwardDetailMap : ClassMap<SeasonUnitDiscountAwardDetail>
    {
        // Methods
        public SeasonUnitDiscountAwardDetailMap()
        {
            base.Table("uCommerce_SeasonUnitDiscountAwardDetail");
            base.Id(x => x.SeasonUnitDiscountAwardDetailId, "SeasonUnitDiscountAwardDetailId").GeneratedBy.Identity();
            base.Map(x => x.AmountOff).Not.Nullable();
            base.Map(x => x.StartDate).Not.Nullable();
            base.Map(x => x.EndDate).Not.Nullable();
            base.Map(x => x.IsPercentage).Not.Nullable();
            base.Map(x => x.OnOrderLine).Not.Nullable();
            base.References<SeasonUnitDiscountAward>(x => x.SeasonUnitDiscountAward).Column("SeasonUnitDiscountAwardId");
        }
    }
}