﻿
CREATE PROCEDURE [dbo].[LB_GetPaymentMethodsByClientID]
	@ClientID varchar(50) 	
AS
BEGIN				
	SELECT
	PaymentMethodId,
	[Description]
	FROM
	[dbo].[LB_FnClientPaymentMethods](@ClientId)
	WHERE AvailableOffline = 1
END
