﻿CREATE PROCEDURE [dbo].[OLD_affinion_GetCustomerQueryAttachments]
(
	@AttachmentId int
) 
AS 
BEGIN
	
SELECT	AttachmentId
		,QueryId
		,FileType
		,FileName
		,FileContent
FROM [dbo].[LB_CustomerQuery_Attachments] 
WHERE AttachmentId = @AttachmentId
	 
END


