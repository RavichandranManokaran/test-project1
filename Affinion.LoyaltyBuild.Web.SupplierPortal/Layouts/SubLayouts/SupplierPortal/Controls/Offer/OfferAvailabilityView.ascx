﻿<%@ control language="C#" autoeventwireup="true" codebehind="OfferAvailabilityView.ascx.cs" inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Offer.OfferAvailabilityView" %>

<style type="text/css">
    .tit_pric {
        width: 100%;
        padding-left: 15px;
    }

    .offerfullwidth {
        width: 100%;
    }

    .ofr_subtitl {
        float: left;
        clear: left;
        color: #4c91cd;
        font-weight: bold;
    }

    .view_btn {
        margin-bottom: 9px;
        margin-left: 2%;
    }

    .collapse_btn {
        border: 1px solid #bcd5e9;
        text-align: right;
        color: #4c91cd;
        background: #fff;
        padding: 0 0 2px;
    }

    .collapse_btnbg {
        background: #eaf5ff;
        width: 100%;
        padding: 4px;
        color: #f77324;
        font-weight: 700;
        border-bottom: 1px solid #bcd5e9;
        text-decoration: none;
    }

    .toggled {
        display: none;
    }
</style>
<div class="page header no-padding">
    <div class="row">
        <div class="col-md-12 no-padding">
            <h4 class="cal-header">
                <asp:Label id="lblInfo" runat="server"></asp:Label>
            </h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 no-padding">
            <h4 class="cal-header">Manage Promotions </h4>
        </div>

    </div>
</div>
<div class="container-outer width-full overflow-hidden-att app-bg no-padding" id="mainWrapper1">
    <%--<div class="row">
        <div class="col-md-2">
            <div class="tit_pric">
                <h4 class="cal-header" style="font-weight: bold; padding-bottom: 0px ! important;" id="dvHotelName" clientidmode="Static" runat="server"></h4>
                <div class="ofr_subtitl" clientidmode="Static" runat="server" id="dvHotelName"></div>
            </div>
        </div>
        <div class="col-md-10">
            
        </div>
    </div>--%>
    <div class="middle-body-container no-padding">

        <div class="row">

            <div class="col-md-12 no-padding">
                <div class="table-cal-header">

                    <div class="tit_pric">
                        <button id="btnbackavilability" clientidmode="Static" runat="server" class="btn btn-link font-size-12 view_btn" type="button" onclick="Supplier.ManageOfferAvailability.RedirectToBacktoAvailability()">
                            <span aria-hidden="true" class="glyphicon glyphicon-backward"></span>&nbsp;Back to Manage Availbility 
                        </button>
                    </div>
                    <div class="col-md-3 no-padding-left" id="leftbar">
                        <div class="col-md-7 no-padding">
                            <div class="ofr_subtitl" clientidmode="Static" runat="server" id="offername"></div>
                        </div>
                        <div class="col-md-5 no-padding-right">
                            <button type="button" class="btn btn-info font-size-12 view_btn pull-right" onclick="Supplier.ManageOfferAvailability.ViewPrice()">View Price</button>
                        </div>
                        <div class="offers-top date_bg marT3">
                            <div class="offers datePickerPanel">
                                <h2 class="no-margin">Choose Date Range</h2>
                                <input readonly="true" type="hidden" id="hdOfferId" runat="server" class="form-control" clientidmode="Static" />
                                <div class="col-md-12 cal-dates padding-top12 no-padding-right clear">
                                    <div class="col-md-2 col-xs-2  datepickerTitle">From</div>
                                    <div class="col-md-10 col-xs-10  float-right no-padding-right">
                                        <input readonly="true" type="text" id="datepickerFrom" placeholder="DD/MM/YYYY" runat="server" class="form-control" clientidmode="Static" />

                                    </div>
                                </div>
                                <div class="col-md-12 cal-dates padding-top12 no-padding-right clear">
                                    <div class="col-md-2 col-xs-2 datepickerTitle">To</div>
                                    <div class="col-md-10 col-xs-10 float-right no-padding-right">
                                        <input readonly="true" type="text" id="datepickerTo" placeholder="DD/MM/YYYY" runat="server" class="form-control" clientidmode="Static" />
                                        <input readonly="true" type="hidden" id="serverdate" placeholder="DD/MM/YYYY" runat="server" class="form-control" clientidmode="Static" />
                                    </div>
                                </div>
                            </div>
                            <div class="dropdown padding-15 float-right clear">
                                <button type="button" class="btn  btn-info" onclick="Supplier.ManageOfferAvailability.ViewByDateRange(0)">View <span class="glyphicon glyphicon-right-arrow" aria-hidden="true"></span></button>

                                <%--<button type="button" class="btn  btn-info font-size-12" data-toggle="modal" data-target="#error">View <span class="glyphicon glyphicon-right-arrow" aria-hidden="true"></span></button>--%>
                            </div>
                        </div>
                        <div class="panel-collapse collapse in" id="sidebar-wrappernew">
                            <div class="offers-content">
                                <input type="hidden" name="offerurl" id="offerurl" runat="server" clientidmode="Static" />
                                <input type="hidden" name="pagetype" id="pagetype" value="offeravailability" clientidmode="Static" />

                                <asp:panel id="dvOfferContent" runat="server" clientidmode="Static">
                                </asp:panel>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 no-padding" id="page-content-wrapper1">
                        <div class="cal-table-top no-padding">
                            <div class="col-md-12 no-padding">
                                <div>
                                    <a href="#menu-toggle" class="btn btn-info font-size-12" id="menu-toggle"><span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>Toggle Menu</a>
                                    <button class="btn btn-info font-size-12" id="printofferavailability">Print</button>
                                    <span class="cal-table" style="display: inline">
                                        <span class="unallocate colorguidewidth"></span>&nbsp;Unallocated
                                    <span class="bookable colorguidewidth"></span>&nbsp;Bookable
                                    <span class="bookable limits colorguidewidth"></span>&nbsp;Bookable with limits
                                    <span class="closeOut colorguidewidth"></span>&nbsp;Closed out
                                    <span class="soldOut colorguidewidth"></span>&nbsp;Sold out
                                    </span>
                                </div>
                            </div>

                            <div class="col-md-12 padding0">
                                <div class="cal-table-top no-padding btnpanel" style="display: none">
                                    <div class="col-md-12 padding0">
                                        <div class="col-md-3 no-padding">
                                            <button id="Button4" clientidmode="Static" runat="server" class="btn btn-link font-size-12 view_btn" type="button" onclick="Supplier.ManageOfferAvailability.RedirectToBacktoAvailability()">
                                                <span aria-hidden="true" class="glyphicon glyphicon-backward"></span>&nbsp;Back to Manage Availbility 
                                            </button>
                                        </div>
                                        <div class="col-md-9 no-padding">
                                            <div class="cal-table-top-right float-right">
                                                <button type="button" class="btn btn-info font-size-12 noprint" data-toggle="modal" data-target="#help"><span class="glyphicon glyphicon-question-sign"></span>&nbsp;&nbsp;Help</button>
                                                <button class="btn btn-info font-size-12" type="button" onclick="Supplier.ManageOfferAvailability.GetReport()">Annual Reports</button>
                                                <button type="button" class="btn btn-info noprint" onclick="Supplier.ManageOfferAvailability.ClearChanges(0)">Clear All Changes </button>
                                                <button id="btn_0" type="button" class="btn btn-orange noprint" onclick="Supplier.ManageOfferAvailability.UpdateChanges(0)">Update and Save Changes </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="offerprintable" class="offerinput">
                                    <asp:panel id="dvContent" runat="server" clientidmode="Static">
                                    </asp:panel>
                                </div>
                                <div class="cal-table-top no-padding btnpanel" style="display: none">
                                    <div class="col-md-12 padding0">
                                        <div class="col-md-3 no-padding">
                                            <button id="Button6" clientidmode="Static" runat="server" class="btn btn-link font-size-12 view_btn" type="button" onclick="Supplier.ManageOfferAvailability.RedirectToBacktoAvailability()">
                                                <span aria-hidden="true" class="glyphicon glyphicon-backward"></span>&nbsp;Back to Manage Availbility 
                                            </button>
                                        </div>
                                        <div class="col-md-9 no-padding">
                                            <div class="cal-table-top-right float-right">
                                                <button type="button" class="btn btn-info font-size-12 noprint" data-toggle="modal" data-target="#help"><span class="glyphicon glyphicon-question-sign"></span>&nbsp;&nbsp;Help</button>
                                                <button class="btn btn-info font-size-12" type="button" onclick="Supplier.ManageOfferAvailability.GetReport()">Annual Reports</button>
                                                <button type="button" class="btn btn-info noprint" onclick="Supplier.ManageOfferAvailability.ClearChanges(0)">Clear All Changes </button>
                                                <button id="btn_1" type="button" class="btn btn-orange noprint" onclick="Supplier.ManageOfferAvailability.UpdateChanges(0)">Update and Save Changes </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bs-example-modal-sm" id="myModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header  alert-info">
                    <h4 class="modal-title">Clear Changes</h4>
                </div>
                <div class="modal-body">
                    <p>Your changes will not be saved. Would you like to Save Changes Now?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnUpdateModalPopup" class="btn btn-info" data-dismiss="modal" disabled>Update & Save Changes</button>
                    <button type="button" id="btnNoModalPopup" class="btn btn-info" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bs-example-modal-sm sigin-modal" id="removal-offer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog modal-md-8 modal-sm-12" role="document">
            <div class="modal-content">
                <div class="modal-header alert-info">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="removal-offer-label">Removal of Promotion</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="removeOfferId" />
                    <input type="hidden" id="removeSupplierInviteId" />
                    <div id="removeOfferContent"></div>



                </div>
                <div class="modal-footer ">
                    <button type="button" class="btn btn-info" onclick="Supplier.ManageOfferAvailability.UnSubScribeOffer()">Update & Save  </button>
                    <button type="button" class="btn btn-info" data-dismiss="modal" id="btncancelremoval">Cancel</button>
                </div>
            </div>
        </div>
    </div>
       <div class="modal fade bs-example-modal-sm sigin-modal" id="dvSubscribePromotion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog modal-md-8 modal-sm-12" role="document">
              <div class="modal-content">
                <div class="modal-header alert-info">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;
                    </span>
                  </button>
                  <h4 class="modal-title" id="subscribeheading">
                  </h4>
                </div>
                <div class="modal-body">
                  <input type="hidden" id="subscribeOfferId" />
                    <input type="hidden" id="hdcontenttype" />
                  <input type="hidden" id="subscribeSupplierInviteId" />
                  <div id="subscribeOfferContent">
                      <textarea id="txtContent" rows="200" cols="300" class="ckeditor"></textarea>
                  </div>
                </div>
                <div class="modal-footer ">
                  <button type="button" class="btn btn-info" 
                           onclick="Supplier.ManageOfferAvailability.ChangeCampaignSubscribe()" id="btnsubscribecontent">Subscribe for Promotion  
                  </button>
                <button type="button" class="btn btn-info" 
                        data-dismiss="modal" id="btncancelsubscribe">Cancel
                </button>
            </div>
          </div>
        </div>
      </div>
    <div class="modal fade bs-example-modal-sm sigin-modal" id="dvviewprice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog modal-md-8 modal-sm-12" role="document">
            <div class="modal-content">
                <div class="modal-header alert-info">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">View Price</h4>
                </div>
                <div class="modal-body">
                    <div id="dvviewpricecontent"></div>

                </div>
                <div class="modal-footer ">
                    <button type="button" class="btn btn-info" data-dismiss="modal">Ok</button>

                </div>
            </div>
        </div>
    </div>



    <div class="modal fade bs-example-modal-sm" id="dvOfferUnsubscribe" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header  alert-info">
                    <h4 class="modal-title">Reason For Unsubscribe</h4>
                </div>
                <div class="modal-body">
                    <p>Please select valid reason for unsubscribe</p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="Button1" class="btn btn-info" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bs-example-modal-sm" id="messageModal" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header  alert-info">
                    <h4 class="modal-title">Save</h4>
                </div>
                <div class="modal-body">
                    <p>Your change have been successfully saved</p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="Button2" class="btn btn-info" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bs-example-modal-sm" id="dvmoreallocated" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header alert-info">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">
                        <span id="sproommsg">Room Allocation</span>
                        <span id="spofferallocated">Offer Subscribe</span>
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="roommsg">
                        You are unable to set a higher number of rooms to sell on this promotion as it is greater than the total of unsold Rooms. Please allocate additional Rooms for this Offer/Room Type
                    </div>
                    <div id="offerallocated">
                        <div class="alert alert-success">
                            This promotion can now be booked from your general availability
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade bs-example-modal-sm" id="dvOfferContentalert" role="dialog">
            <div class="modal-dialog modal-md">
              <div class="modal-content">
                <div class="modal-header alert-info">
                  <button type="button" class="close" data-dismiss="modal">&times;
                  </button>
                  <h4 class="modal-title">
                    <span id="Span1">Offer information
                    </span>
                  </h4>
                </div>
                <div class="modal-body">
                  <div id="Div2">
                    <div class="alert alert-success">
                      Offer information updated successfully
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-info" data-dismiss="modal">Ok
                  </button>
                </div>
              </div>
            </div>
          </div>

    <div class="modal fade bs-example-modal-sm" id="help" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header alert-info">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><span class="glyphicon glyphicon-question-sign"></span>Help</h4>
                </div>
                <div class="modal-body">
    <asp:Literal runat="server" ID="litHelp"></asp:Literal>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bs-example-modal-sm" id="dvEmptyMsg" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header  alert-info">
                    <h4 class="modal-title">Validation</h4>
                </div>
                <div class="modal-body">
                    <p>This field cannot be blank as rooms have been booked, please input a number</p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="Button5" class="btn btn-info" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>
     <div class="modal fade bs-example-modal-sm" id="dvShowEmptyMsg" role="dialog">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            <div class="modal-header  alert-info">
              <h4 class="modal-title">Validation
              </h4>
            </div>
            <div class="modal-body">
              <p>This field cannot be blank, please input a number
              </p>
            </div>
            <div class="modal-footer">
              <button type="button" id="Button6" class="btn btn-info" data-dismiss="modal">Ok
              </button>
            </div>
          </div>
        </div>
      </div>

    <div class="modal fade bs-example-modal-sm" id="dvLessRoomAllocationMsg" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header  alert-info">
                    <h4 class="modal-title">Validation</h4>
                </div>
                <div class="modal-body">
                    <p>Allocated is less than booked, therefore system has updated allocated to match the booked figure</p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="Button3" class="btn btn-info" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Anual ReportModal pop-up -->
    <div class="modal fade" id="myanualModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Annual Reports</h4>
                </div>
                <div class="modal-body" id="dvanualcontainer">
                    <div class="content_bg_inner_box alert-info">
                        <div class="col-sm-12">
                            <strong>Offer Name: </strong>
                        </div>
                        <div class="col-sm-12 padding-top12">
                            <div class="row">
                                <div class="col-sm-8">Bookings received over last 12 months by promotion</div>
                                <div class="col-sm-4"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-8">Revenue generated over last 12 months by promotion</div>
                                <div class="col-sm-4"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-8">Future bookings by promotion</div>
                                <div class="col-sm-4"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-8">Future Revenue by promotion</div>
                                <div class="col-sm-4"></div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>




</div>

<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-header">
        <h1>Processing...</h1>
    </div>
    <div class="modal-body">
        <div class="progress progress-striped active">
            <div class="bar" style="width: 100%;"></div>
        </div>
    </div>
</div>



