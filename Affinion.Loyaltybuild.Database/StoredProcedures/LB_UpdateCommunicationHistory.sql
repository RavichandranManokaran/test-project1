﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LB_UpdateCommunicationHistory] 
	-- Add the parameters for the stored procedure here
	@orderlineId int, 
	@status bit 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE uCommerce_CommunicationHistory SET [status]=@status WHERE OrderLineId=@orderlineId
END
