﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           UCommerceHelper.cs
/// Sub-system/Module:     Affinion.Loyaltybuild.BusinessLogic.Helper (VS project name)
/// Description:           UCommerce related helper methods
/// </summary>

using Affinion.LoyaltyBuild.Common.Instrumentation;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using UCommerce.EntitiesV2;
using UCommerce.EntitiesV2.Definitions;

namespace Affinion.Loyaltybuild.BusinessLogic.Helper
{
    public static class UCommerceHelper
    {
        /// <summary>
        /// Get integer value
        /// </summary>
        /// <param name="product">UCommerce product</param>
        /// <param name="fieldName">Field name</param>
        /// <param name="defaultValue">Value to return when failed</param>
        /// <returns>Integer field value</returns>
        public static int GetIntergerProductFieldValue(Product product, string fieldName, int defaultValue)
        {
            try
            {
                int value;
                IProperty property = product.GetProperty(fieldName);

                if (property == null)
                {
                    return defaultValue;
                }

                if (int.TryParse(Convert.ToString(property.GetValue()), out value))
                {
                    return value;
                }

                return defaultValue;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.BusinessLogic, ex, null);
                throw;
            }
        }

        /// <summary>
        /// Ucoomerce Item list
        /// </summary>
        /// <param name="items"></param>
        /// <returns>Returns a list item collection with text of the given field and value of the item ID</returns>
        public static ListItemCollection GetDropdownDataSource(IEnumerable<INamedEntity> items)
        {
            try
            {

                ListItemCollection listItemCollection = new ListItemCollection();

                foreach (INamedEntity currentItem in items)
                {
                    ListItem listItem = new ListItem();

                    listItem.Value = currentItem.Id.ToString();
                    listItem.Text = currentItem.Name;

                    listItemCollection.Add(listItem);
                }

                return listItemCollection;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                throw;
            }
        }
    }
}
