﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Helpers
{
    public static class GenericExtensionMethods
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="reader"></param>
        /// <param name="columnName"></param>
        /// <returns></returns>
        public static Nullable<T> NullValue<T>(this IDataReader reader, string columnName)
            where T : struct
        {
            if (reader[columnName] == DBNull.Value)
                return null;

            var t = typeof(T);
            t = Nullable.GetUnderlyingType(t) ?? t;

            return (T)Convert.ChangeType(reader[columnName], t);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="reader"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        public static Nullable<T> NullValue<T>(this IDataReader reader, int column)
            where T : struct
        {
            if (reader[column] == DBNull.Value)
                return null;

            var t = typeof(T);
            t = Nullable.GetUnderlyingType(t) ?? t;

            return (T)Convert.ChangeType(reader[column], t);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="reader"></param>
        /// <param name="columnName"></param>
        /// <returns></returns>
        public static T Value<T>(this IDataReader reader, string columnName)
        {
            var t = typeof(T);
            t = Nullable.GetUnderlyingType(t) ?? t;

            if (reader[columnName] == DBNull.Value)
                return default(T);

            return (T)Convert.ChangeType(reader[columnName], t);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="reader"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        public static T Value<T>(this IDataReader reader, int column)
        {
            var t = typeof(T);
            t = Nullable.GetUnderlyingType(t) ?? t;

            if (reader[column] == DBNull.Value)
                return default(T);

            return (T)Convert.ChangeType(reader[column], t);
        }

        public static T ParseIntoEnum<T>(this String input, bool canRemoveSpaces = false, bool ignoreCasing = true) where T : struct
        {
            return (T)Enum.Parse(typeof(T), (canRemoveSpaces) ? input = Convert.ToString(input).Replace(" ", "").Trim() : input, ignoreCasing);
        }

        public static string GetRightNCharacters(this string input, int NoOfCharacters)
        {
            return (NoOfCharacters >= input.Length)
                ? input
                : input.Substring(input.Length - NoOfCharacters);
        }

        public static bool FieldNameFound(this Sitecore.Collections.FieldCollection input, string fieldName)
        {
            return (input[fieldName] != null);
        }

        public static long ConvertToLong(this string input)
        {
            long inputAsLong = 0;
            long.TryParse(input, out inputAsLong);
            return inputAsLong;
        }
    }
}