﻿CREATE TABLE [dbo].[uCommerce_DefinitionField] (
    [DefinitionFieldId] INT              IDENTITY (1, 1) NOT NULL,
    [DataTypeId]        INT              NOT NULL,
    [DefinitionId]      INT              NOT NULL,
    [Name]              NVARCHAR (512)   NOT NULL,
    [DisplayOnSite]     BIT              NOT NULL,
    [Multilingual]      BIT              NOT NULL,
    [RenderInEditor]    BIT              NOT NULL,
    [Searchable]        BIT              NOT NULL,
    [SortOrder]         INT              CONSTRAINT [DF_uCommerce_DefinitionField_SortOrder] DEFAULT ((0)) NOT NULL,
    [Deleted]           BIT              NOT NULL,
    [BuiltIn]           BIT              DEFAULT ((0)) NOT NULL,
    [DefaultValue]      NVARCHAR (MAX)   NULL,
    [Guid]              UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    CONSTRAINT [PK_DefinitionField] PRIMARY KEY CLUSTERED ([DefinitionFieldId] ASC),
    CONSTRAINT [FK_uCommerce_DefinitionField_uCommerce_DataType] FOREIGN KEY ([DataTypeId]) REFERENCES [dbo].[uCommerce_DataType] ([DataTypeId]),
    CONSTRAINT [FK_uCommerce_DefinitionField_uCommerce_Definition] FOREIGN KEY ([DefinitionId]) REFERENCES [dbo].[uCommerce_Definition] ([DefinitionId])
);

