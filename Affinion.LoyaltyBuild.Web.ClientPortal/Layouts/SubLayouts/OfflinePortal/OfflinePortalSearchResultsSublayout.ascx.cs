﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           OfflinePortalSearchResultsSublayout.ascx.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal
/// Description:           SubLayout for offline portal search Results
/// </summary>

namespace Affinion.Loyaltybuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    #region Using Directives

    using Affinion.Loyaltybuild.BusinessLogic;
    using Affinion.Loyaltybuild.BusinessLogic.Helper;
    using Affinion.LoyaltyBuild.Api.Booking.Helper;
    using helpers = Affinion.LoyaltyBuild.Api.Search.Helpers;
    using Affinion.LoyaltyBuild.Common.Exceptions;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.SessionHelper;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Model.Product;
    using Affinion.LoyaltyBuild.Model.Provider;
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.Search.Data;
    using Affinion.LoyaltyBuild.Search.Helpers;
    using Affinion.LoyaltyBuild.Search.SupplierFilters;
    using Affinion.LoyaltyBuild.UCom.Common.Extension;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
    using Affinion.LoyaltyBuild.Web.ClientPortal.Helper;
    using Affinion.LoyaltyBuild.Web.ClientPortal.ViewModels;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Web.UI.WebControls;

    #endregion

    public partial class OfflinePortalSearchResultsSublayout : BaseSublayout
    {
        #region private constant variable

        /// <summary>
        /// Client item path
        /// </summary>
        private const string ClientItemPath = @"/sitecore/content/admin-portal/client-setup/{0}";

        /// <summary>
        /// Hotel Information path
        /// </summary>
        private const string HotelInformationPath = @"/sitecore/content/client-portal/{0}/hotelinformation";

        /// <summary>
        /// Locations path
        /// </summary>
        private const string LocationsPath = @"/sitecore/content/client-portal/{0}/locations";

        /// <summary>
        /// Select All Text
        /// </summary>
        private const string selectAllText = "Select All";

        #endregion

        #region Global Variables

        /// <summary>
        /// Global variable for offerLists
        /// </summary>
        private Dictionary<string, List<OfferDataItem>> offerLists = new Dictionary<string, List<OfferDataItem>>();

        /// <summary>
        /// Intizialize global count variable for accordion
        /// </summary>
        protected int numberCount = 0;

        #endregion

        #region Fields

        private int currentPage;
        private int pageCount;
        private SearchKey keywords;
        private QueryStringHelper helper;

        #endregion

        #region LWP-1195
        private int DefaultHotelCount = 10;
        IList<Affinion.LoyaltyBuild.Model.Provider.IProvider> searchResult = null;
        int PageResultCount = 0;
        Repeater rptDateHeaders = null;
        List<ProviderInfo> roomAvailablityDetails = null;
        #endregion

        #region public methods



        public string CurrentSiteUrl { get; set; }
        public string pageTitle { get; set; }

        /// <summary>
        /// Executes on page pre load event
        /// </summary>
        /// <param name="e">The parameter</param>
        public override void PreLoad(EventArgs e)
        {
            try
            {
                this.PreviousWeeks.Visible = true;
                helper = new QueryStringHelper(Request.Url);
                this.SetSearchKeywords();

                this.SetDates(keywords.FlexibleDates, helper);

                DateTime checkIn = keywords.CheckinDate;
                if (checkIn < DateTime.Today.AddDays(7))
                {
                    this.PreviousWeeks.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, null);
                throw;
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Code to execute on page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                this.AddJavascriptVariable("MapLocations", string.Empty);
                this.NoResults.Text = string.Empty;
                this.SearchResultsAll.Visible = false;

                this.btnTransferBooking.Visible = false;
                this.btnAddtoCart.Visible = true;
                this.btnCheckOut.Visible = true;

                basketPopUp.Visible = true;
                btnCheckOut.Visible = false;

                this.ValidateCountyDropDown();


                CurrentSiteUrl = "https://" + Sitecore.Context.Site.HostName;
                //Diagnostics.Trace(DiagnosticsCategory.ClientPortal, "REALEX: cur site url in sub: " + CurrentSiteUrl);
                //pageTitle = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "BreadcrumbTitle");
                siteurlfield.Value = CurrentSiteUrl;
                //Diagnostics.Trace(DiagnosticsCategory.ClientPortal, "REALEX: hiddenfield value: " + siteurlfield.Value);

                if (!IsPostBack)
                {
                    this.LoadSearchResults();
                   
                }

                this.ValidateBasketPopUp();
                #region LWP-873
                Session["IsOfflinePortalLoaded"] = null;
                #endregion

                LoadTextFromSitecore();
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, null);
                throw;
            }
        }

        /// <summary>
        /// Sort Link
        /// </summary>
        /// <param name="sortMode">Sort mode</param>
        /// <param name="sortValue">Sort value</param>
        /// <returns></returns>
        protected string SortLink(SortMode sortMode, string sortValue)
        {
            try
            {
                QueryStringHelper stringHelper = new QueryStringHelper(this.Request.Url);
                stringHelper.SetValue("sortmode", sortMode.ToString());

                if (!string.IsNullOrEmpty(sortValue))
                {
                    stringHelper.SetValue("sortValue", sortValue);
                    stringHelper.SetValue("PageId", "1");
                }

                return stringHelper.GetUrl(false).ToString();
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, null);
                throw;
            }
        }

        /// <summary>
        /// Method used to nevigate to previous page
        /// </summary>
        /// <returns></returns>
        protected string PreviousLink()
        {
            try
            {
                if (currentPage != 1)
                {                    
                    string previous = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Previous", "");
                    return "<a href=" + this.PaginationLink(currentPage - 1) + ">" + previous + "</a>";
                }

                return string.Empty;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, null);
                throw;
            }
        }

        /// <summary>
        /// Method used to check whether page is active or not
        /// </summary>
        /// <param name="pageNumebr"></param>
        /// <returns></returns>
        protected string SelectedPageClass(int pageNumebr)
        {
            try
            {
                if (this.currentPage == pageNumebr)
                {
                    return "active";
                }

                return string.Empty;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, null);
                throw;
            }
        }

        /// <summary>
        /// Method used to add page details on query string
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        protected string PaginationLink(int pageNumber)
        {
            try
            {
                if (pageNumber == 0)
                {
                    return @"javascript:void(0)";
                }

                QueryStringHelper stringHelper = new QueryStringHelper(this.Request.Url);
                if (!string.IsNullOrEmpty(stringHelper.GetValue(QueryStringHelper.encryptParamName)))
                {
                    stringHelper.SetValue("PageId", Cryptography.Encrypt(pageNumber.ToString()));
                }
                else
                {
                    stringHelper.SetValue("PageId", pageNumber.ToString());
                }

                return stringHelper.GetUrl(false).ToString();
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, null);
                throw;
            }
        }

        /// <summary>
        /// Method used to nevigate to next page
        /// </summary>
        /// <returns></returns>
        protected string NextLink()
        {
            try
            {
                if (currentPage != pageCount)
                {                    
                    string next = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Next", "");
                    return "<a href=" + this.PaginationLink(currentPage + 1) + ">" + next + "</a>";
                }

                return string.Empty;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, null);
                throw;
            }
        }

        /// <summary>
        /// Button click event for Add to basket button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ButtonAddToBasket_Click(object sender, EventArgs e)
        {
            try
            {
                this.AddToBasket();
                Response.Redirect(helper.GetUrl().ToString(), true);
            }
            catch (Exception ex)
            {
                Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Button click event for Process to checkout button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ButtonProcessToCheckout_Click(object sender, EventArgs e)
        {
            try
            {
                this.AddToBasket();
                Response.Redirect("/Basket");
            }
            catch (Exception ex)
            {
                Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Button click event for Transfer booking button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ButtonTransferBooking_Click(object sender, EventArgs e)
        {
            try
            {
              Affinion.Loyaltybuild.BusinessLogic.Helper.BasketHelper.ClearBasket();
                this.AddToBasket();
                Response.Redirect(this.GetUrl("/transferbooking?action=transfer&olid=" + this.QueryString("olid")));
            }
            catch (Exception ex)
            {
                Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Event fired when county drop down selected index changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CountyDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.IsPostBack && this.Request != null && !string.IsNullOrEmpty(this.Request.Params["__EVENTTARGET"]) && this.Request.Params["__EVENTTARGET"].Contains("CountyDropDown"))
                {
                    /// Get the URL of the current page                    
                    QueryStringHelper helper = new QueryStringHelper(Request.Url);

                    /// Get the selected index of the drop down
                    string location = CountyDropDown.SelectedValue;

                    /// Append the selected index value to query string and return
                    if (string.Equals(location, selectAllText))
                    {
                        helper.SetValue("Location", string.Empty);
                    }
                    else
                    {
                        helper.SetValue("Location", location);
                    }

                    if (!string.IsNullOrEmpty(helper.GetValue("PageId")))
                    {
                        helper.SetValue("PageId", "1");
                    }

                    Response.Redirect(helper.GetUrl().ToString(), true);
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, null);
                throw;
            }
        }

        /// <summary>
        /// Method used to increment numberCount by 1
        /// </summary>
        /// <returns></returns>
        protected string GetJavascriptID()
        {
            this.numberCount = numberCount + 1;
            return numberCount.ToString();
        }

        /// <summary>
        /// Method used to get offer for non-base products
        /// </summary>
        /// <param name="availability"></param>
        /// <returns></returns>
        protected int GetOfferData(RoomAvailabilityDetail availability)
        {
            var availabilityRoom = availability.OfferAvailability.FirstOrDefault();
            if (availability != null && availability.OfferAvailability != null && availabilityRoom != null && availabilityRoom.OfferAvailableRoom > 0)
            {
                return availabilityRoom.OfferAvailableRoom;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Method used to get OfferDataItem as string
        /// </summary>
        /// <param name="offer"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        protected string GetOfferDataItem(OfferDataItem offer, string parameter)
        {
            if (offer != null && !string.IsNullOrEmpty(parameter))
            {
                switch (parameter)
                {
                    case "OfferType":
                        return offer.OfferType.ToString();
                    case "SupplierId":
                        return offer.SupplierId;
                    case "CampaignItemId":
                        return offer.CampaignItemId.ToString();
                    case "CurrencyId":
                        return offer.UcommerceCurrencyDetails.CurrencyId.ToString();
                    default:
                        return string.Empty;
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// Method used to get offer availability status css
        /// </summary>
        /// <param name="results"></param>
        /// <returns></returns>
        protected string GetAvailabilityStatusCss(SupplierSearchResultItem results)
        {
            SupplierData offersDataSet = results.GetOffers(results.GetItem(), GetClientItem(), keywords.CheckinDate, keywords.CheckoutDate);
            List<OfferDataItem> baseProducts = offersDataSet.Offers.Where(o => o.OfferType == OfferTypes.BaseProduct).ToList();
            List<OfferDataItem> offers = offersDataSet.Offers.Where(o => o.OfferType != OfferTypes.BaseProduct).ToList();

            bool baseProductCondition = baseProducts != null && baseProducts.Count > 0;
            bool offersCondition = offers != null && offers.Count > 0;

            /// Base Product Condition
            //if (baseProducts != null)
            //{
            //    foreach (OfferDataItem offerItem in baseProducts)
            //    {
            //        foreach (RoomAvailabilityDetail roomAvailabilityDetail in offerItem.RoomAvailability)
            //        {
            //            if (roomAvailabilityDetail.AvailabilityDate > DateTime.Today && roomAvailabilityDetail.AvailableRoom > 0)
            //            {
            //                baseProductCondition = true;
            //            }
            //        }
            //    }
            //}

            ///// Offers Condition
            //if (offers != null)
            //{
            //    foreach (OfferDataItem offerItem in offers)
            //    {
            //        foreach (RoomAvailabilityDetail roomAvailabilityDetail in offerItem.RoomAvailability)
            //        {
            //            if (roomAvailabilityDetail.AvailabilityDate > DateTime.Today)
            //            {
            //                foreach (OfferAvailabilityDetail offerAvailabilityDetail in roomAvailabilityDetail.OfferAvailability)
            //                {
            //                    if (offerAvailabilityDetail.OfferAvailableRoom > 0)
            //                    {
            //                        offersCondition = true;
            //                    }
            //                }
            //            }
            //        }
            //    }
            //}            

            if (!baseProductCondition && !offersCondition)
            {
                return " empty-panel";
            }
            return string.Empty;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Get the client item from query string
        /// </summary>
        /// <returns></returns>
        private Item GetClientItem()
        {
            return Sitecore.Context.Database.GetItem(string.Format(ClientItemPath, GetClientName()));
        }

        /// <summary>
        /// Get the client name from query string
        /// </summary>
        /// <returns></returns>
        private string GetClientName()
        {
            return helper.GetValue("ClientList");
        }

        /// <summary>
        /// Load search results
        /// </summary>
        private void LoadSearchResults()
        {
            //if (BasketHelper.GetBasketCount() > 0)
            //{
            //    btnCheckOut.Visible = true;
            //}
            helper = new QueryStringHelper(Request.Url);
            SetSearchKeywords();

            if (!int.TryParse((helper.GetValue("PageId")), out this.currentPage))
            {
                this.currentPage = 1;
            }

            this.GenerateFilters();

            // Use a collection to fetch results 
            //SupplierSearchResults results = SearchHelper.SearchContent(keywords);

            BindSearchResultToRepeater();
        }
        /// <summary>
        /// setting map details and bind search reasult to searchResultsRepater
        /// </summary>
        /// <returns></returns>

        private void BindSearchResultToRepeater()
        {
            try
            {
                #region LWP-1195

                if (keywords != null && keywords.Client != null && !string.IsNullOrEmpty(keywords.Client.ID.ToString()) && !string.IsNullOrEmpty(keywords.Destination))
                {
                Affinion.LoyaltyBuild.Api.Search.Data.SearchKey mySearchKey = new Affinion.LoyaltyBuild.Api.Search.Data.SearchKey(System.Web.HttpContext.Current, keywords.Client.ID.ToString());

                #region Search Key
                mySearchKey.ClientId = keywords.Client.ID.ToString();
                mySearchKey.Location = new LoyaltyBuild.Api.Search.Data.SearchLocation();
                mySearchKey.Location.Text = keywords.Destination;
                mySearchKey.CheckInDate = keywords.CheckinDate;
                mySearchKey.CheckOutDate = keywords.CheckoutDate;
                mySearchKey.NoOfRooms = keywords.NumberOfRooms;
                mySearchKey.NoOfNights = keywords.NoOfNights;
                mySearchKey.NoOfAdults = keywords.NumberOfAdults;
                mySearchKey.NoOfChildren = keywords.NumberOfChildren;
                mySearchKey.Location.Type = Affinion.LoyaltyBuild.Api.Search.Data.LocationType.City;
                #endregion


                Affinion.LoyaltyBuild.Api.Search.Data.PagedProviderCollection providerCollection = Affinion.LoyaltyBuild.Api.Search.Helpers.SearchHelper.SearchHotels(mySearchKey, SetSortOptions(helper));

                searchResult = new List<Affinion.LoyaltyBuild.Model.Provider.IProvider>();
                if (providerCollection != null && providerCollection.Count > 0)
                {
                    searchResult = Affinion.LoyaltyBuild.Api.Search.Helpers.SearchHelper.GetPageData(providerCollection, this.currentPage);
                }

                if (searchResult != null && searchResult.Count > 0)
                {
                    HotelSupplierResults results = new HotelSupplierResults();
                    results.AllResults = FillData(searchResult).ToArray();
                    if (results != null && results.AllResults.Length > 0)
                    {
                        this.SearchResultsAll.Visible = true;

                        int itemsPerPage = SitecoreFieldsHelper.GetInteger(Sitecore.Context.Item, "ItemsPerPage", 10);
                        PagedDataSource resultsWithPaging = GetPagedDataSource(results.AllResults, this.currentPage, itemsPerPage);

                        /// Setting map details
                            this.SetMapDetails(resultsWithPaging, results);

                        Dictionary<string, string> mustacheContext = new Dictionary<string, string>();
                        mustacheContext.Add("{{Keyword}}", keywords.Destination);
                        mustacheContext.Add("{{ResultCount}}", results.AllResults.Length.ToString());

                        ResultsSummary.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "ResultsTitle", mustacheContext);

                        this.CustomizePageOnSearchMode(keywords.SearchResultsMode);

                        // Bind the search results to SearchResultsRepeater
                        SearchResultsRepeater.DataSource = resultsWithPaging;
                        SearchResultsRepeater.ItemDataBound += SearchResultsRepeaterItemDataBound;
                        SearchResultsRepeater.DataBind();

                        this.pageCount = PageResultCount;

                        ICollection<int> pageRange = PagingHelper.CreatePages(this.pageCount, this.currentPage);

                        if (this.pageCount > 1)
                        {
                            rptPagination.DataSource = pageRange;
                            rptPagination.DataBind();
                        }
                    }
                    else if (helper.QueryStringParameters.Count != 0)
                    {
                        this.NoResults.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "NoResultsLabel", "Sorry, No Hotels matched with your search.");
                    }
                }
                }
                #endregion
            }
            catch(Exception ex)
            {
                Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, ex.Message);
                throw;

            }
        }

        private Affinion.LoyaltyBuild.Api.Search.Data.SearchOptions SetSortOptions(QueryStringHelper helper)
        {
            Affinion.LoyaltyBuild.Api.Search.Data.SearchOptions tmpSearchOptions = new Affinion.LoyaltyBuild.Api.Search.Data.SearchOptions();
            tmpSearchOptions.SortDirection = Affinion.LoyaltyBuild.Api.Search.Data.SortDirection.Ascending;
            tmpSearchOptions.SortOn = Affinion.LoyaltyBuild.Api.Search.Data.SortField.Default;
            if (helper.GetValue("sortmode") != null)
            {
                switch (helper.GetValue("sortmode"))
                {
                    case "Price":
                        tmpSearchOptions.SortOn = Affinion.LoyaltyBuild.Api.Search.Data.SortField.Price;
                        break;
                    case "Name":
                        tmpSearchOptions.SortOn = Affinion.LoyaltyBuild.Api.Search.Data.SortField.Name;
                        break;
                    case "Rate":
                        tmpSearchOptions.SortOn = Affinion.LoyaltyBuild.Api.Search.Data.SortField.Rate;
                        break;
                    case "Recommended":
                        tmpSearchOptions.SortOn = Affinion.LoyaltyBuild.Api.Search.Data.SortField.Recommended;
                        break;
                }
            }
            if (helper.GetValue("sortValue") != null)
            {
                switch (helper.GetValue("sortValue"))
                {
                    case "0":
                        tmpSearchOptions.SortDirection = Affinion.LoyaltyBuild.Api.Search.Data.SortDirection.Ascending;
                        break;
                    case "1":
                        tmpSearchOptions.SortDirection = Affinion.LoyaltyBuild.Api.Search.Data.SortDirection.Descending;
                        break;
                }
            }
            return tmpSearchOptions;
        }

        public PagedDataSource GetPagedDataSource(HotelSearchResultItem[] list, int pageNumber, int itemsPerPage)
        {
            if (list == null || pageNumber == 0 || itemsPerPage == 0)
            {
                return null;
            }

            PagedDataSource pagedDataSource = new PagedDataSource();
            PageResultCount = (int)Math.Ceiling(list.Length / (double)itemsPerPage);

            pagedDataSource.DataSource = list;
            pagedDataSource.AllowPaging = true;
            pagedDataSource.CurrentPageIndex = pageNumber > 0 ? pageNumber - 1 : 0;
            pagedDataSource.PageSize = itemsPerPage;

            return pagedDataSource;
        }

        private List<HotelSearchResultItem> FillData(IList<IProvider> searchResult)
        {
            List<HotelSearchResultItem> hotelSearchResultItem = new List<HotelSearchResultItem>();
            foreach (var searchResultItem in searchResult)
            {
                HotelSearchResultItem hotelOutputItem = new HotelSearchResultItem();
                Hotel hotelInputItem = (Hotel)searchResultItem;
                hotelOutputItem.ProviderId = hotelInputItem.ProviderId;
                hotelOutputItem.SitecoreItemId = hotelInputItem.SitecoreItemId;
                hotelOutputItem.ProviderType = hotelInputItem.ProviderType;
                hotelOutputItem.IntroductionImage = hotelInputItem.IntroductionImage;
                hotelOutputItem.Name = hotelInputItem.Name;
                hotelOutputItem.Town = hotelInputItem.Town;
                hotelOutputItem.Overview = hotelInputItem.Overview;
                hotelOutputItem.StarRanking = hotelInputItem.StarRanking.ToString();
                hotelOutputItem.SitecoreItem = hotelInputItem.SitecoreItem;
                hotelOutputItem.Facilities = hotelInputItem.Facilities;
                hotelOutputItem.Themes = hotelInputItem.Themes;
                hotelOutputItem.Experiences = hotelInputItem.Experiences;
                hotelOutputItem.AddressLine1 = hotelInputItem.AddressLine1;
                hotelOutputItem.AddressLine2 = hotelInputItem.AddressLine2;
                hotelOutputItem.AddressLine3 = hotelInputItem.AddressLine3;
                hotelOutputItem.AddressLine4 = hotelInputItem.AddressLine4;
                hotelOutputItem.Location = hotelInputItem.Location;
                hotelOutputItem.UCommerceId = hotelInputItem.UCommerceId;
                hotelOutputItem.Country = hotelInputItem.Country;
                hotelOutputItem.LocalRanking = hotelInputItem.LocalRanking;
                hotelOutputItem.LowestPrice = hotelInputItem.LowestPrice;
                hotelOutputItem.CurrencyID = hotelInputItem.CurrencyID;
                hotelOutputItem.RoomCollection = LoadRoomsData(hotelInputItem);
                hotelSearchResultItem.Add(hotelOutputItem);
            }
            return hotelSearchResultItem;
        }


        private List<ProviderInfo> LoadRoomsData(Hotel supplierItem)
        {
            List<ProviderInfo> lstProviderInfo = new List<ProviderInfo>();
            ProviderInfo objProviderInfo = null;
            if (supplierItem != null && supplierItem.Products != null && supplierItem.Products.Count > 0)
            {
                List<PriceAvailabilityByDate> PriceAvailabilityList = new List<PriceAvailabilityByDate>();
                DateTime ToDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
                PriceAvailabilityList = HotelSearchHelper.GetAvailabilityAndPriceByDate(supplierItem.Products, null, DateTime.Now.Date, ToDate.Date, keywords.NoOfNights);
                if (PriceAvailabilityList != null && PriceAvailabilityList.Count > 0)
                {
                    foreach (var priceLst in PriceAvailabilityList)
                    {
                        objProviderInfo = new ProviderInfo();
                        objProviderInfo.ProviderID = supplierItem.ProviderId;
                        objProviderInfo.Sku = priceLst.Sku;
                        objProviderInfo.VariatSku = priceLst.VariantSku;
                        objProviderInfo.Price = priceLst.Price;
                        //objProviderInfo.PriceCurrency = keywords.CurrencyCode;
                        objProviderInfo.RoomAvailability = priceLst.Availability;
                        //objProviderInfo.PriceCurrency = keywords.CurrencyCode;
                       // objProviderInfo.NoOfNights = priceLst.NoOfNights;
                        objProviderInfo.Name = (supplierItem.Products.Where(x => x.Sku == priceLst.Sku).FirstOrDefault()).Name;
                        objProviderInfo.AvailabilityDate = priceLst.Date;
                        objProviderInfo.DateCounter = priceLst.Date.ConvetDatetoDateCounter();
                        objProviderInfo.ProviderType = supplierItem.ProviderType;
                        lstProviderInfo.Add(objProviderInfo);
                    }
                }
                else
                {
                    foreach (var product in supplierItem.Products)
                    {
                        objProviderInfo = new ProviderInfo();
                        objProviderInfo.ProviderID = supplierItem.ProviderId;
                        objProviderInfo.Sku = product.Sku;
                        objProviderInfo.VariatSku = product.VariantSku;
                        objProviderInfo.Price = product.Price == 0 ? 100 : product.Price;
                       // objProviderInfo.PriceCurrency = keywords.CurrencyCode;
                        objProviderInfo.RoomAvailability =100;
                       // objProviderInfo.NoOfNights = 10;
                        objProviderInfo.Name = product.Name;
                        objProviderInfo.AvailabilityDate = DateTime.Now.Date;
                        objProviderInfo.DateCounter = DateTime.Now.Date.ConvetDatetoDateCounter();
                        objProviderInfo.ProviderType = supplierItem.ProviderType;
                        lstProviderInfo.Add(objProviderInfo);
                    }
                }
            }
            return lstProviderInfo;
        }

        /// <summary>
        /// Method used to assign map details to javascript variables
        /// </summary>
        /// <param name="resultsWithPaging"></param>
        private void SetMapDetails(PagedDataSource resultsWithPaging, HotelSupplierResults results)
        {
            SearchKey mapDataKeywords = keywords;
            mapDataKeywords.Location = string.Empty;

            HotelSearchResultItem[] newResults = results.AllResults;
            PagedDataSource allResults = new PagedDataSource();
            allResults.DataSource = newResults;
            allResults.AllowPaging = false;

            /// Coordinates used to populate resultsWithPaging datasource
            MapData coordinates = GetCordinates(resultsWithPaging, keywords);

            /// Coordinates used to populate all results datasource
            MapData coordinatesAll = GetCordinates(allResults, keywords);

            /// Create respective MapLocations javascript variable
            this.AddJavascriptVariable("MapLocations", coordinates);
            this.AddJavascriptVariable("MapLocationsAll", coordinatesAll);
        }

        public MapData GetCordinates(PagedDataSource dataSource, SearchKey searchKeywords)
        {
            MapData mapData = new MapData();
            string locationData=string.Empty;
            foreach (var item in dataSource)
            {
                HotelSearchResultItem resultsItem = item as HotelSearchResultItem;
                GeoCordinate cordinate = new GeoCordinate();

                if (resultsItem == null)
                {
                    continue;
                }

                if (resultsItem != null && resultsItem.SitecoreItem != null)
                {
                    Item location = SitecoreFieldsHelper.GetDropLinkFieldTargetItem(resultsItem.SitecoreItem, "MapLocation");

                    if (location == null)
                    {
                        continue;
                    }

                    locationData = SitecoreFieldsHelper.GetValue(location, "MapLocation");
                    cordinate.Location = SitecoreFieldsHelper.GetValue(location, "Name");
                    cordinate.SupplierId = resultsItem.SitecoreItem.ID.ToString();
                }
                else
                {
                    cordinate.Location = resultsItem.Location;
                    cordinate.SupplierId = resultsItem.ProviderId.ToString();

                }
                cordinate.Latitude = "0";
                cordinate.Longitute = "0";


                if (!string.IsNullOrWhiteSpace(locationData))
                {
                    List<string> latLongList = locationData.Split(',').ToList<string>();
                    if (latLongList.Count == 2)
                    {
                        cordinate.Latitude = latLongList[0];
                        cordinate.Longitute = latLongList[1];
                    }
                }

                mapData.Locations.Add(cordinate);
            }

            mapData.ContextItemId = Sitecore.Context.Item.ID.ToString();
            mapData.ResultsType = searchKeywords.SearchResultsMode;

            return mapData;
        }

        /// <summary>
        /// Method used to generate Filters Content
        /// </summary>
        private void GenerateFilters()
        {
            /// Add default filters
            List<ISupplierFilter> supplierFilters = new List<ISupplierFilter>();
            supplierFilters.Add(new FilterByClient());
            supplierFilters.Add(new FilterByLocation());
            supplierFilters.Add(new FilterByStarRanking());
            supplierFilters.Add(new FilterByFacilities());
            supplierFilters.Add(new FilterByThemes());
            supplierFilters.Add(new FilterByExperience());
            supplierFilters.Add(new FilterByPackage());

            /// Add aditional filters depending on the filter criteria
            keywords.SupplierFilters = supplierFilters;
            keywords.Facilities = Server.UrlDecode(keywords.Facilities);
            keywords.Themes = Server.UrlDecode(keywords.Themes);
            keywords.Experiences = Server.UrlDecode(keywords.Experiences);
            keywords.StarRanking = Server.UrlDecode(keywords.StarRanking);
            keywords.CurrencyCode = this.GetCurrencyInformation();
        }

        /// <summary>
        /// Define search mode
        /// </summary>
        /// <param name="resultsMode"></param>
        /// <param name="results"></param>
        private void CustomizePageOnSearchMode(SearchResultsMode resultsMode)
        {
            if (resultsMode == SearchResultsMode.Location)
            {                               
                ResultsSummary.Text += SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "ResultsSummaryLocation", "");
            }
            else if (resultsMode == SearchResultsMode.SupplierName)
            {               
                ResultsSummary.Text += SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "ResultsSummaryHotel", "");
            }
            else if (resultsMode == SearchResultsMode.Mixed)
            {                
                ResultsSummary.Text += SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "ResultsSummaryLocationAndHotel", "");
            }
        }

        /// <summary>
        /// Set search keywords using query string parameters
        /// </summary>       
        private void SetSearchKeywords()
        {
            if (string.Equals(helper.GetValue("TransferBooking"), "true"))
            {
                this.btnTransferBooking.Visible = true;
                this.btnAddtoCart.Visible = false;
                this.btnCheckOut.Visible = false;
            }

            keywords = new SearchKey();
            keywords.Client = GetClientItem();

            keywords.Destination = helper.GetValue("Destination");
            keywords.Destination = !string.IsNullOrEmpty(keywords.Destination) ? keywords.Destination.ToLowerInvariant() : string.Empty;

            keywords.Location = helper.GetValue("Location");
            keywords.Location = !string.IsNullOrEmpty(keywords.Location) ? keywords.Location.ToLowerInvariant() : string.Empty;

            keywords.SetSortMode(helper.GetValue("sortmode"), helper.GetValue("sortvalue"));

            this.SetFilterData(helper);

            this.UpdateDates(helper);
            this.UpdateOcupancyDetails(helper);
        }

        /// <summary>
        /// Method used to set filter sublayout data from query string
        /// </summary>
        /// <param name="helper"></param>
        private void SetFilterData(QueryStringHelper helper)
        {
            keywords.SupplierFilters = null;
            keywords.StarRanking = helper.GetValue("StarRanking");
            keywords.Facilities = helper.GetValue("Facilities");
            keywords.Experiences = helper.GetValue("Experiences");
            keywords.Themes = helper.GetValue("Themes");
        }

        /// <summary>
        /// Update date fields
        /// </summary>
        /// <param name="helper">Query helper</param>
        private void UpdateDates(QueryStringHelper helper)
        {
            string checkinDateValue = helper.GetValue("CheckinDate");
            string checkoutDateValue = helper.GetValue("CheckoutDate");
            bool specificDates = false;

            if (!string.IsNullOrEmpty(checkinDateValue) && !string.IsNullOrEmpty(checkoutDateValue))
            {
                keywords.CheckinDate = DateTimeHelper.ParseDate(checkinDateValue);
                keywords.CheckoutDate = DateTimeHelper.ParseDate(checkoutDateValue);
            }

            if (Boolean.TryParse(helper.GetValue("FlexibleDates"), out specificDates))
            {
                keywords.FlexibleDates = specificDates;
            }
        }

        /// <summary>
        /// Update occupancy details
        /// </summary>
        /// <param name="helper">Query helper</param>
        private void UpdateOcupancyDetails(QueryStringHelper helper)
        {
            int noOfAdults = 0;
            if (int.TryParse(helper.GetValue("NoOfAdults"), out noOfAdults))
            {
                keywords.NumberOfAdults = noOfAdults;
            }

            int noOfChildren = 0;
            if (int.TryParse(helper.GetValue("NoOfChildren"), out noOfChildren))
            {
                keywords.NumberOfChildren = noOfChildren;
            }

            string childrenAgesString = helper.GetValue("ChildAge");
            if (!string.IsNullOrWhiteSpace(childrenAgesString))
            {
                keywords.ChildrenAges = childrenAgesString.Split(',');
            }

            int noOfRooms = 0;

            if (int.TryParse(helper.GetValue("NoOfRooms"), out noOfRooms))
            {
                keywords.NumberOfRooms = noOfRooms;
            }

            keywords.OfferGroup = helper.GetValue("OfferGroup");
        }

        /// <summary>
        /// Event handler for binding search result repeater
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchResultsRepeaterItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            try
            {
                HotelSearchResultItem supplierItem = e.Item.DataItem as HotelSearchResultItem;
                Repeater facilities = e.Item.FindControl("HotelFacilities") as Repeater;
                Repeater starRepeater = e.Item.FindControl("StarRepeater") as Repeater;
                Literal country = e.Item.FindControl("CountryName") as Literal;
                //Repeater OfferTypeRepeater = e.Item.FindControl("rptOfferType") as Repeater;
                //Image hotelRating = e.Item.FindControl("hotelRating") as Image;
                Repeater RoomAvailablityRepeater = e.Item.FindControl("RoomAvailablity") as Repeater;

                if (supplierItem == null)
                {
                    return;
                }
                Item item = null;
                if (supplierItem == null && supplierItem.SitecoreItem != null)
                {

                    item = supplierItem.SitecoreItem;

                    if (item == null)
                    {
                        return;
                    }

                    if (facilities != null)
                    {
                        facilities.DataSource = SitecoreFieldsHelper.GetMutiListItems(item, "Facilities");
                        facilities.DataBind();
                    }

                    if (country != null)
                    {
                        country.Text = SitecoreFieldsHelper.GetLookupFieldTargetItemValue(item, "Country", "CountryName");
                    }
                }
                else
                {
                    if (facilities != null)
                    {
                        facilities.DataSource = supplierItem.Facilities;
                        facilities.DataBind();
                    }

                    if (country != null)
                    {
                        country.Text = supplierItem.Country;
                    }
                    
                }

                //BindHotelRating(hotelRating, item);
                BindStarRankings(item, starRepeater, supplierItem.StarRanking);

                //BindOfferTypeRepeater(e, supplierItem, OfferTypeRepeater, item);
                BindRoomDetails(e, supplierItem, RoomAvailablityRepeater, item);
            }
            catch (Exception ex)
            {
                Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, ex.Message);
                throw;
            }
        }

        private void BindRoomDetails(RepeaterItemEventArgs e, HotelSearchResultItem supplierItem, Repeater RoomAvailablityRepeater, Item item)
        {
            roomAvailablityDetails = new List<ProviderInfo>();
            roomAvailablityDetails = supplierItem.RoomCollection;
            RoomAvailablityRepeater.DataSource = supplierItem.RoomCollection;
            RoomAvailablityRepeater.ItemDataBound += RoomAvailablityRepeaterItemDataBound;
            RoomAvailablityRepeater.DataBind();
        }

        private void RoomAvailablityRepeaterItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
            {
                rptDateHeaders = e.Item.FindControl("rptDateHeaders") as Repeater;
            }
            if (e.Item.ItemType == ListItemType.Item)
            {
                ProviderInfo roomAvailability = e.Item.DataItem as ProviderInfo;
                Repeater rptDates = e.Item.FindControl("rptDates") as Repeater;
                Literal occupancyName = e.Item.FindControl("occupancyName") as Literal;
                //Literal price = e.Item.FindControl("price") as Literal;

                if (rptDateHeaders != null)
                {
                    DateTime startDate = keywords.CheckinDate.AddDays(-7);
                    DateTime endDate = keywords.CheckinDate.AddDays(7);
                    List<DateTime> dateRange = new List<DateTime>();

                    for (DateTime date = startDate; date <= endDate; date = date.AddDays(1))
                        dateRange.Add(date);

                    rptDateHeaders.DataSource = dateRange;
                    rptDateHeaders.ItemDataBound += rptDateHeaders_ItemDataBound;
                    rptDateHeaders.DataBind();
                }

                if (occupancyName != null)
                {
                    occupancyName.Text = roomAvailability.Name;
                }

                /*if (price != null)
                {
                    price.Text = roomAvailability.Price.ToString();
                }*/

                if (roomAvailability != null && rptDates != null)
                {
                    rptDates.DataSource = roomAvailablityDetails;
                    rptDates.ItemDataBound += rptDates_ItemDataBound;
                    rptDates.DataBind();
                }
            }
        }

        /// <summary>
        /// Method used to bind offer type
        /// </summary>
        /// <param name="e"></param>
        /// <param name="supplierItem"></param>
        /// <param name="OfferTypeRepeater"></param>
        /// <param name="item"></param>
        private void BindOfferTypeRepeater(RepeaterItemEventArgs e, SupplierSearchResultItem supplierItem, Repeater OfferTypeRepeater, Item item)
        {
            SupplierData offersDataSet = supplierItem.GetOffers(item, GetClientItem(), keywords.CheckinDate, keywords.CheckoutDate);

            List<OfferDataItem> baseProducts = offersDataSet.Offers.Where(o => o.OfferType == OfferTypes.BaseProduct).ToList();
            List<OfferDataItem> offers = offersDataSet.Offers.Where(o => o.OfferType != OfferTypes.BaseProduct).ToList();
            offerLists = new Dictionary<string, List<OfferDataItem>>();

            /// Base Product Condition
            if (baseProducts != null)
            {
                string baseOfferTitle = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "RoomOnlyAvailabilityTitle", "Room Only");

                foreach (OfferDataItem baseItem in baseProducts)
                {
                    //foreach (RoomAvailabilityDetail roomAvailabilityDetail in baseItem.RoomAvailability)
                    //{
                    //    if (roomAvailabilityDetail.AvailabilityDate > DateTime.Today && roomAvailabilityDetail.AvailableRoom > 0)
                    //    {
                    //        string baseOfferTitle = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "RoomOnlyAvailabilityTitle", "Room Only");
                    //        offerLists[baseOfferTitle] = new List<OfferDataItem>();
                    //        foreach (OfferDataItem offerItem in baseProducts)
                    //        {
                    //            offerLists[baseOfferTitle].Add(offerItem);
                    //        }
                    //    }
                    //}

                    offerLists[baseOfferTitle] = new List<OfferDataItem>();
                    foreach (OfferDataItem offerItem in baseProducts)
                    {
                        offerLists[baseOfferTitle].Add(offerItem);
                    }
                }
            }

            /// Offers Condition
            if (offers != null)
            {
                foreach (OfferDataItem offerItem in offers)
                {
                    //var roomAvailabilityRecords = offerItem.RoomAvailability.Where(r => r.AvailabilityDate > DateTime.Today);
                    //foreach (RoomAvailabilityDetail roomAvailabilityDetail in roomAvailabilityRecords)
                    //{
                    //    foreach (OfferAvailabilityDetail offerAvailabilityDetail in roomAvailabilityDetail.OfferAvailability)
                    //    {
                    //        if (offerAvailabilityDetail.OfferAvailableRoom > 0)
                    //        {
                    //            if (offerLists.Keys.Contains(offerItem.OfferName))
                    //            {
                    //                offerLists[offerItem.OfferName].Add(offerItem);
                    //            }
                    //            else
                    //            {
                    //                offerLists[offerItem.OfferName] = new List<OfferDataItem>();
                    //                offerLists[offerItem.OfferName].Add(offerItem);
                    //            }
                    //        }
                    //    }
                    //}

                    if (offerLists.Keys.Contains(offerItem.OfferName))
                    {
                        offerLists[offerItem.OfferName].Add(offerItem);
                    }
                    else
                    {
                        offerLists[offerItem.OfferName] = new List<OfferDataItem>();
                        offerLists[offerItem.OfferName].Add(offerItem);
                    }
                }
            }

            if (offersDataSet.Offers.Any())
            {
                OfferTypeRepeater.DataSource = offerLists.Keys;
                OfferTypeRepeater.ItemDataBound += OfferTypeRepeaterItemDataBound;
                OfferTypeRepeater.DataBind();
            }
        }

        /// <summary>
        /// Event handler for binding offer typerepeater
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OfferTypeRepeaterItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Repeater offersListRepeater = e.Item.FindControl("OffersList") as Repeater;
            Literal offerTypeName = e.Item.FindControl("OfferTypeName") as Literal;

            string offerGroupName = e.Item.DataItem as string;

            if (!string.IsNullOrEmpty(offerGroupName) && offerLists[offerGroupName] != null)
            {
                List<OfferDataItem> offers = offerLists[offerGroupName];

                offerTypeName.Text = e.Item.DataItem.ToString();

                BindOfferDetails(e, offers, offersListRepeater);
            }
        }

        /// <summary>
        /// Event handler for binding date header repeater
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rptDateHeaders_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            DateTime item = (DateTime)e.Item.DataItem;
            Literal date = e.Item.FindControl("Date") as Literal;
            Literal day = e.Item.FindControl("Day") as Literal;

            if (item != null && date != null && day != null)
            {
                date.Text = item.ToString("dd/MM");
                day.Text = item.ToString("ddd");
            }
        }

        /// <summary>
        /// Method used to bind hotel rating
        /// </summary>
        /// <param name="hotelRating">Image field</parram>
        /// <param name="item">Hotel rating item</param>
        //private static void BindHotelRating(Image hotelRating, Item item)
        //{
        //    if (hotelRating != null)
        //    {
        //        LinkField ratingImageField = item.Fields["RatingImage"];
        //        if (!string.IsNullOrWhiteSpace(ratingImageField.Url))
        //        {
        //            hotelRating.ImageUrl = ratingImageField.Url;
        //        }
        //        else
        //        {
        //            hotelRating.Visible = false;
        //        }
        //    }
        //}

        /// <summary>
        /// Method used to bind offer details
        /// </summary>
        /// <param name="e"></param>
        /// <param name="supplierItem"></param>
        /// <param name="offersList"></param>
        /// <param name="item"></param>
        private void BindOfferDetails(RepeaterItemEventArgs e, List<OfferDataItem> Offers, Repeater offersListRepeater)
        {
            List<OfferDataItem> filteredOffers = new List<OfferDataItem>();

            //foreach (OfferDataItem offer in Offers)
            //{
            //    int availability = 0;
            //    if (offer.OfferType == OfferTypes.BaseProduct)
            //    {
            //        foreach (RoomAvailabilityDetail roomAvailabilityDetail in offer.RoomAvailability)
            //        {
            //            availability = offer.RoomAvailability.Where(r => r.AvailabilityDate > DateTime.Today && r.AvailableRoom > 0).Count();
            //            if (availability > 0)
            //            {
            //                filteredOffers.Add(offer);
            //                break;
            //            }
            //        }
            //        //availability = offer.RoomAvailability.Where(r => r.AvailableRoom > 0).Count();
            //    }
            //    else
            //    {
            //        foreach (RoomAvailabilityDetail roomAvailabilityDetail in offer.RoomAvailability)
            //        {
            //            availability = offer.RoomAvailability.Where(r => r.AvailabilityDate > DateTime.Today && r.AvailableRoom > 0 && r.OfferAvailability.Count > 0).Count();
            //            if (availability > 0)
            //            {
            //                filteredOffers.Add(offer);
            //                break;
            //            }
            //        }
            //        //availability = offer.RoomAvailability.Where(r => r.AvailableRoom > 0 && r.OfferAvailability.Count > 0).Count();
            //    }                
            //}

            // disabled availaibility filter
            filteredOffers = Offers;

            if (filteredOffers != null && offersListRepeater != null)
            {
                offersListRepeater.DataSource = filteredOffers.GroupBy(x => x.OccupancyTypeId).Select(x => x.OrderBy(y => y.Price)).Select(x => x.First());
                offersListRepeater.ItemDataBound += offersListRepeaterItemDataBound;
                offersListRepeater.DataBind();
            }
        }

        /// <summary>
        /// Event handler for binding offers list repeater
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void offersListRepeaterItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            OfferDataItem offers = e.Item.DataItem as OfferDataItem;
            Repeater rptDates = e.Item.FindControl("rptDates") as Repeater;
            Literal occupancyName = e.Item.FindControl("occupancyName") as Literal;
            Literal price = e.Item.FindControl("price") as Literal;
            Repeater rptDateHeaders = e.Item.FindControl("rptDateHeaders") as Repeater;

            if (rptDateHeaders != null)
            {
                DateTime startDate = keywords.CheckinDate.AddDays(-7);
                DateTime endDate = keywords.CheckinDate.AddDays(7);
                List<DateTime> dateRange = new List<DateTime>();

                for (DateTime date = startDate; date <= endDate; date = date.AddDays(1))
                    dateRange.Add(date);

                rptDateHeaders.DataSource = dateRange;
                rptDateHeaders.ItemDataBound += rptDateHeaders_ItemDataBound;
                rptDateHeaders.DataBind();
            }

            if (occupancyName != null)
            {
                occupancyName.Text = offers.OccupancyType.Name;
            }

            if (price != null)
            {
                price.Text = GetOfferPrice(offers);
            }

            if (offers != null && rptDates != null)
            {
                rptDates.DataSource = offers.RoomAvailability;
                if (offers.OfferType == OfferTypes.BaseProduct)
                {
                    rptDates.ItemDataBound += rptDates_ItemDataBound;
                }
                else
                {
                    rptDates.ItemDataBound += rptOffers_ItemDataBound;
                }
                rptDates.DataBind();
            }
        }

        /// <summary>
        /// Event handler for binding search result repeater for base products
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rptDates_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            ProviderInfo availability = e.Item.DataItem as ProviderInfo;
            Literal roomAvailability = e.Item.FindControl("Date") as Literal;
            Literal cssPlaceholder = e.Item.FindControl("ClassPlaceholder") as Literal;

            if (roomAvailability != null && cssPlaceholder != null)
            {
                if (availability != null && availability.RoomAvailability > 0)
                {
                    roomAvailability.Text = availability.RoomAvailability.ToString()+ "(" + availability.Price.ToString("#")+")";

                    /// cssPlaceholder validation
                    if (availability.AvailabilityDate == keywords.CheckinDate)
                    {
                        cssPlaceholder.Text = " accent54-bg accent36-f body-text";
                    }
                    else if (availability.AvailabilityDate >= DateTime.Today)
                    {
                        cssPlaceholder.Text = " body-text";
                    }
                    else if (availability.AvailabilityDate < DateTime.Today)
                    {
                        cssPlaceholder.Text = " disable-box";
                    }
                }
                else
                {
                    roomAvailability.Text = "0";

                    /// cssPlaceholder validation
                    if (availability.AvailabilityDate == keywords.CheckinDate)
                    {
                        cssPlaceholder.Text = " availability-zero";
                    }
                    else if (availability.AvailabilityDate < DateTime.Today)
                    {
                        cssPlaceholder.Text = " disable-box";
                    }
                }
            }
        }

        /// <summary>
        /// Event handler for binding search result repeater for non-base products
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rptOffers_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            RoomAvailabilityDetail availability = e.Item.DataItem as RoomAvailabilityDetail;
            Literal roomAvailability = e.Item.FindControl("Date") as Literal;
            Literal cssPlaceholder = e.Item.FindControl("ClassPlaceholder") as Literal;

            if (roomAvailability != null && cssPlaceholder != null)
            {
                var availabilityRoom = availability.OfferAvailability.FirstOrDefault();

                if (availability != null && availability.OfferAvailability != null && availabilityRoom != null && availabilityRoom.OfferAvailableRoom > 0)
                {
                    roomAvailability.Text = availabilityRoom.OfferAvailableRoom.ToString();

                    /// cssPlaceholder validation
                    if (availability.AvailabilityDate == keywords.CheckinDate)
                    {
                        cssPlaceholder.Text = " accent54-bg accent36-f body-text";
                    }
                    else if (availability.AvailabilityDate >= DateTime.Today)
                    {
                        cssPlaceholder.Text = " body-text";
                    }
                    else if (availability.AvailabilityDate < DateTime.Today)
                    {
                        cssPlaceholder.Text = " disable-box";
                    }
                }
                else
                {
                    roomAvailability.Text = "0";

                    /// cssPlaceholder validation
                    if (availability.AvailabilityDate == keywords.CheckinDate)
                    {
                        cssPlaceholder.Text = " availability-zero";
                    }
                    else if (availability.AvailabilityDate < DateTime.Today)
                    {
                        cssPlaceholder.Text = " disable-box";
                    }
                }
            }
        }

        /// <summary>
        /// Bind ranking related fields
        /// </summary>
        /// <param name="item"></param>
        /// <param name="starRepeater"></param>
        private void BindStarRankings(Item item, Repeater starRepeater,string starRanking=" ")
        {
            string ranking = string.Empty;

            if (item != null)
                ranking = SitecoreFieldsHelper.GetDropLinkFieldValue(item, "StarRanking", "Name");
            else
                ranking = starRanking;

            if (!string.IsNullOrWhiteSpace(ranking))
            {
                int starRank;
                if (Int32.TryParse(ranking, out starRank))
                {
                    List<int> listOfStars = Enumerable.Repeat(starRank, starRank).ToList();
                    starRepeater.DataSource = listOfStars;
                    starRepeater.DataBind();
                }
                else
                {
                    Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal,
                        new AffinionException("Unable to parse star rank:" + item.DisplayName), this);
                }
            }
        }

        /// <summary>
        /// Gets last price for offer
        /// </summary>
        /// <param name="offer"></param>
        /// <returns></returns>
        private string GetOfferPrice(OfferDataItem offer)
        {
            if (offer == null)
            {
                return string.Empty;
            }

            decimal price = offer.IsDiscounted ? offer.DiscountedPrice : offer.Price;
            return offer.GetPriceWithCurrency(price);
        }

        /// <summary>
        /// Get the given event target or return null if not found.
        /// </summary>
        /// <param name="key">The key to match the event target</param>
        /// <returns>Returns true if event target found</returns>
        private bool GetEventTarget(string key)
        {
            if (this.IsPostBack && this.Request != null)
            {
                string eventTarget = this.Request.Params["__EVENTTARGET"];

                if (eventTarget != null && eventTarget.Contains(key))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Method to validate Basket popup visibility
        /// </summary>
        private void ValidateBasketPopUp()
        {
            // Get locations item ID
            string itemId = Sitecore.Context.Item.ID.ToString();
            QueryStringHelper helper = new QueryStringHelper(Request.Url);

            if (string.Equals(itemId, "{6837514C-EDF8-40E2-94AA-807C03961CC1}") || string.Equals(helper.GetValue("mode"), "Location"))
            {
                basketPopUp.Visible = false;
            }
        }

        /// <summary>
        /// Method used to set check In and check Out Dates
        /// </summary>
        /// <param name="specificDate"></param>
        /// <param name="helper"></param>
        private void SetDates(bool specificDate, QueryStringHelper helper)
        {
            if (!specificDate) // false
            {
                // Considered no of nights as 1 if string to int conversion fails
                int noOfNights = 1;
                if (Int32.TryParse(helper.GetValue("NoOfNights"), out noOfNights))
                {
                    if (this.GetEventTarget("PreviousWeeks"))
                    {
                        SetDateHelper(-7, noOfNights);
                    }
                    else if (this.GetEventTarget("NextWeeks"))
                    {
                        SetDateHelper(7, noOfNights);
                    }
                }
            }
            else // true
            {
                // Considered no of nights as 1
                if (this.GetEventTarget("PreviousWeeks"))
                {
                    SetDateHelper(-7, 1);
                }
                else if (this.GetEventTarget("NextWeeks"))
                {
                    SetDateHelper(7, 1);
                }
            }
        }

        /// <summary>
        /// Method helper used to set dates and redirect
        /// </summary>
        /// <param name="checkIn"></param>
        /// <param name="checkOut"></param>
        private void SetDateHelper(int checkIn, int checkOut)
        {
            DateTime checkInDate = keywords.CheckinDate.AddDays(checkIn);
            DateTime checkOutDate = checkInDate.AddDays(checkOut);
            helper.SetValue("CheckinDate", checkInDate.ToString(DateTimeHelper.DateFormat));
            helper.SetValue("CheckoutDate", checkOutDate.ToString(DateTimeHelper.DateFormat));
            Response.Redirect(helper.GetUrl().ToString(), true);
        }

        /// <summary>
        /// Method used to add to basket
        /// </summary>
        private void AddToBasket()
        {
            string Sku = this.hiddenFieldSku.Value;
            string variantssku = string.Empty;
            string productType = "";

            int room = 0;
            if (Int32.TryParse(this.hiddenFieldRooms.Value, out room))
            {
                // Populate custom basketInfo data
                var basketInfo = GetBasketInfo(Sku);
                //basketInfo.OldOrderLineId = string.IsNullOrEmpty(this.QueryString("olid")) ? "0" : this.QueryString("olid");

                if (room != -1)
                {
                    IProduct iPro = new Package();
                   

                    switch (productType)
                    {
                        case "Pacakge":
                            iPro = new Package();
                            iPro.Type = ProductType.Package;
                            break;
                        case "Room":
                            iPro = new Room();
                            iPro.Type = ProductType.Room;
                            break;
                        case "Addon":
                            iPro = new Addon();
                            iPro.Type = ProductType.Addon;
                            break;
                        case "BB":
                            iPro = new BedBankProduct();
                            iPro.Type = ProductType.BedBanks;
                            break;
                        default:
                            break;
                    }

                    iPro.Sku = Sku;
                    iPro.VariantSku = variantssku;
                    Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.AddToBasket(iPro, room, basketInfo);
                }
            }
        }

        /// <summary>
        /// Populate custom basketInfo data
        /// </summary>
        /// <param name="sku"></param>
        /// <returns></returns>
        private Dictionary<string,string> GetBasketInfo(string sku)
        {
            Uri pageUri = Request.Url;
            QueryStringHelper helper = new QueryStringHelper(pageUri);
            Dictionary<string, string> dicBasket = new Dictionary<string, string>();

            int dateCounter = Convert.ToInt32(this.hdDateCounter.Value);
            DateTime selectedDate = dateCounter.GetDateFromDateCount();
            string numberOfNights = helper.GetValue("NoOfNights");

            dicBasket.Add("Destination", helper.GetValue("Destination"));
            dicBasket.Add("NoOfAdults", helper.GetValue("NoOfAdults"));
            dicBasket.Add("NoOfChildren", helper.GetValue("NoOfChildren"));
            dicBasket.Add("NoOfRooms", helper.GetValue("NoOfRooms"));
            dicBasket.Add("BookingThrough", BookingMethod.Offline.ToString());
            dicBasket.Add("CheckinDate", selectedDate.ConvertDateToString());
            dicBasket.Add("CheckoutDate", selectedDate.AddDays(Convert.ToInt32(numberOfNights)).ConvertDateToString());
            dicBasket.Add("CreatedBy", SitecoreUserProfileHelper.GetItem(UserProfileKeys.CreatedBy));
           



            //BasketInfo basketInfo = new BasketInfo
            //{
            //    Sku = sku,
            //    Location = helper.GetValue("Destination"),
            //    NoOfRooms = helper.GetValue("NoOfRooms"),
            //    NoOfAdults = helper.GetValue("NoOfAdults"),
            //    NoOfChildren = helper.GetValue("NoOfChildren"),
            //    Nights = numberOfNights,
            //    OfferGroup = helper.GetValue("OfferGroup"),
            //    BookingThrough = BookingMethod.Offline.ToString(),
            //    CheckinDate = selectedDate.ConvertDateToString(),
            //    CheckoutDate = selectedDate.AddDays(Convert.ToInt32(numberOfNights)).ConvertDateToString(),
            //    CreatedBy = SitecoreUserProfileHelper.GetItem(UserProfileKeys.CreatedBy)
            //};

            return dicBasket;
        }

        /// <summary>
        /// Method used to validate CountyDropDown
        /// </summary>
        private void ValidateCountyDropDown()
        {
            QueryStringHelper helper = new QueryStringHelper(Request.Url);
            string pageMode = helper.GetValue("mode");
            CountySection.Visible = false;
            if (!string.IsNullOrEmpty(pageMode) && string.Equals(pageMode, "LocationFinder"))
            {
                if (!IsPostBack)
                {
                    CountySection.Visible = true;
                    this.LoadCountyDropDown(helper);
                }
            }
        }

        /// <summary>
        /// Load data to the county drop down list
        /// </summary>
        private void LoadCountyDropDown(QueryStringHelper helper)
        {
            string location = helper.GetValue("Location");

            if (GetClientItem() != null)
            {
                /// Bind data to dropdown
                Collection<Item> items = SitecoreFieldsHelper.GetMutiListItems(GetClientItem(), "Regions");
                CountyDropDown.DataSource = SitecoreFieldsHelper.GetDropdownDataSource(items, "Name", "ID");
                CountyDropDown.DataTextField = "Text";
                CountyDropDown.DataValueField = "Text";
                CountyDropDown.DataBind();

                /// Set the default list item
                CountyDropDown.Items.Insert(0, selectAllText);

                if (!string.IsNullOrEmpty(location))
                {
                    CountyDropDown.SelectedValue = location;
                }
            }
        }

        /// <summary>
        /// LoadTextFromSitecore
        /// </summary>

        private void LoadTextFromSitecore()
        {
            PreviousWeeks.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "PreviousWeek", "");
            NextWeeks.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "NextWeek", "");
            btnAddtoCart.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "AddtoBasket", "");
            btnCheckOut.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Checkout", "");
            btnCancel.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "Cancel", "");
            btnTransferBooking.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "TransferBooking", "");
        }

        #endregion
    }
}