﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2.Maps;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Maps
{
    public class SubrateTemplatesMap : BaseClassMap<SubrateTemplates>
    {
        // Methods
        public SubrateTemplatesMap()
        {
            base.Table("LB_SubrateTemplates");
            Id(x => x.SubrateTemplateID).Column("SubrateTemplateID");
            base.Map(x => x.SubrateTemplateGuid).Column("SubrateTemplate").Not.Nullable();
            base.Map(x => x.SubrateTemplateName).Column("SubrateTemplateCode").Not.Nullable();
            base.Map(x => x.CreatedOn).Not.Nullable();
            base.Map(x => x.UpdateOn).Not.Nullable();
            base.Map(x => x.CreatedBy).Not.Nullable();
        }
    }
}
