﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientPortalTwoColumnSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortalTwoColumnSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="col-md-12">
    <div class="float-right">
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="col-xs-12 col-md-4">
            <sc:Placeholder ID="leftsidebar" Key="leftsidebar" runat="server" />
            <sc:Placeholder ID="leftsidebarbottom" Key="leftsidebarbottom" runat="server" />
              <sc:Placeholder ID="leftsidebarbottomFilter" Key="leftsidebarbottomFilter" runat="server" />

        </div>
        <div class="col-xs-12 col-md-8">
            <sc:Placeholder ID="rightcontentbar" Key="rightcontentbar" runat="server" />
        </div>
    </div>
</div>
