﻿CREATE PROCEDURE [dbo].[LB_BookingCounterReport]
	AS

BEGIN


-- for today booking(s)

SELECT 
	count(Value)as 'Booking(s)',
	Value as [ClientId] 
FROM
	uCommerce_OrderProperty AS OP
Join
	LB_OrderLineStatusAudit AS OS
On OP.OrderLineId = OS.OrderLineId
WHERE [KEY]  = 'clientid' and
	CAST(OS.CreatedOn AS DATE)=CAST(getdate()as date)
	and (OS.StatusId = 6 or -- If the booking is confirmed
		OS.StatusId = 5) --If the booking is provisonal
Group By 
Value

-- for yesterday booking(s)



SELECT 
	count(Value)as 'Booking(s)',
	Value as [ClientId] 
FROM
	uCommerce_OrderProperty AS OP
Join
	LB_OrderLineStatusAudit AS OS
On OP.OrderLineId = OS.OrderLineId
WHERE [KEY]  = 'clientid' and
	CAST(OS.CreatedOn AS DATE)=CAST(getdate()-1 as date)
	and (OS.StatusId = 6 or -- If the booking is confirmed
			OS.StatusId = 5) --If the booking is provisonal
Group By
	Value

-- for week booking(s)

SELECT 
	count(Value)as 'Booking(s)',
	Value as [ClientId] 
FROM
	uCommerce_OrderProperty AS OP
Join
	LB_OrderLineStatusAudit AS OS
On OP.OrderLineId = OS.OrderLineId
WHERE [KEY]  = 'clientid' and
	CAST(OS.CreatedOn AS DATE) between DATEADD(wk, DATEDIFF(wk, 6, GETDATE()), 6) and getdate()
	and (OS.StatusId = 6 or -- If the booking is confirmed
			OS.StatusId = 5) --If the booking is provisonal
Group By
	Value 


-- for month booking(s)

SELECT 
	count(Value)as 'Booking(s)',
	Value as [ClientId] 
FROM
	uCommerce_OrderProperty AS OP
Join
	LB_OrderLineStatusAudit AS OS
On OP.OrderLineId = OS.OrderLineId
WHERE [KEY]  = 'clientid' and
	CAST(OS.CreatedOn AS DATE) between DATEADD(DAY, 1-DAY(GETDATE()), DATEDIFF(DAY, 0, GETDATE())) and getdate()
	and (OS.StatusId = 6 or -- If the booking is confirmed
			OS.StatusId = 5) --If the booking is provisonal
Group By
	Value


-- for year booking(s)


SELECT 
	count(Value)as 'Booking(s)',
	Value as [ClientId] 
FROM
	uCommerce_OrderProperty AS OP
Join
	LB_OrderLineStatusAudit AS OS
On OP.OrderLineId = OS.OrderLineId
WHERE [KEY]  = 'clientid' and
	CAST(OS.CreatedOn AS DATE) between   DATEADD(yy, DATEDIFF(yy,0,getdate()), 0)
and DATEADD(yy, DATEDIFF(yy,0,getdate()) + 1, -1) 
	and (OS.StatusId = 6 or -- If the booking is confirmed
			OS.StatusId = 5) --If the booking is provisonal
Group By
	Value 


END