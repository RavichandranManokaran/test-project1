﻿CREATE TABLE [dbo].[uCommerce_UserWidgetSetting] (
    [UserWidgetSettingId] INT            IDENTITY (1, 1) NOT NULL,
    [Section]             NVARCHAR (MAX) NULL,
    [WidgetName]          NVARCHAR (MAX) NULL,
    [Width]               NVARCHAR (MAX) NULL,
    [Height]              NVARCHAR (MAX) NULL,
    [PositionX]           NVARCHAR (MAX) NULL,
    [PositionY]           NVARCHAR (MAX) NULL,
    [UserId]              INT            NULL,
    [DisplayName]         NVARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([UserWidgetSettingId] ASC),
    FOREIGN KEY ([UserId]) REFERENCES [dbo].[uCommerce_User] ([UserId])
);

