Go
update uCommerce_DataType
set DefinitionName = 'SiteCoreSupplierType'
where TypeName = 'HotelType'
Go

update uCommerce_DataType
set DefinitionName = 'SiteCoreLocation'
where TypeName = 'Location'
Go
update uCommerce_DataType
set DefinitionName = 'SiteCoreHotelRanking'
where TypeName = 'HotelRating'
Go
update uCommerce_DataType
set DefinitionName = 'SiteCoreOccupancyTypes'
where TypeName = 'RoomType'
Go
update uCommerce_DataType
set DefinitionName = 'SiteCoreAddOn'
where TypeName = 'ProductType'
Go


insert into uCommerce_DataType (TypeName,Nullable,ValidationExpression,BuiltIn,DefinitionName,Deleted,Guid,CreatedOn,
CreatedBy,ModifiedOn,ModifiedBy)
values ('Country',1,'',0,'SiteCoreCountry',0,NEWID(),GETDATE(),'admin',GETDATE(),'admin'),
('Region',1,'',0,'SiteCoreRegion',0,NEWID(),GETDATE(),'admin',GETDATE(),'admin'),
('Theme',1,'',0,'SiteCoreHotelTheme',0,NEWID(),GETDATE(),'admin',GETDATE(),'admin'),
('HotelFacility',1,'',0,'SiteCoreHotelFacility',0,NEWID(),GETDATE(),'admin',GETDATE(),'admin'),
('Client',1,'',0,'SiteCoreClient',0,NEWID(),GETDATE(),'admin',GETDATE(),'admin'),
('OfferType',1,'',0,'SiteCoreOfferType',0,NEWID(),GETDATE(),'admin',GETDATE(),'admin'),
('Groups',1,'',0,'SiteCoreGroups',0,NEWID(),GETDATE(),'admin',GETDATE(),'admin'),
('SuperGroups',1,'',0,'SiteCoreSuperGroups',0,NEWID(),GETDATE(),'admin',GETDATE(),'admin')
Go