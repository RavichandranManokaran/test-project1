﻿using Affinion.LoyaltyBuild.Search.Helpers;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using service = Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Customer;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Handlers.CS
{
    /// <summary>
    /// Summary description for SearchCustomerQuery
    /// </summary>
    public class SearchCustomerQuery : IHttpHandler
    {
        private NameValueCollection _query = null;
        private service.QueryService _queryService;

        public void ProcessRequest(HttpContext context)
        {
            _query = HttpUtility.ParseQueryString(context.Request.Url.Query.ToLower());
            _queryService = new service.QueryService();
            
            var serializer = new JavaScriptSerializer();

            CustomerQuerySearch search = serializer.Deserialize<CustomerQuerySearch>(GetDocumentContents(context.Request));
            var searchResult = _queryService.SearchCustomerQuery(search);
            var clients = SearchHelper.GetUserSpecificClientItems();

            var result = from searchItem in searchResult
                         join client in clients on searchItem.ClientId.ToLower() equals (client.ID.Guid.ToString().ToLower())
                         select new SearchQueryResultItem
                         {
                             ID = searchItem.ID,
                             CustomerName = searchItem.CustomerName,
                             Phone        = searchItem.Phone,
                             Created      = searchItem.Created,
                             Updated      = searchItem.Updated,
                             Content       = searchItem.Content.Replace("\\r\\n", Environment.NewLine).Replace("//r//n", Environment.NewLine),
                             HasAttachments = searchItem.HasAttachments,
                             ClientId       = searchItem.ClientId,
                             Client              = client.DisplayName,
                             BookingReference = searchItem.BookingReference,
                             FirstName          = searchItem.FirstName,
                             LastName           = searchItem.LastName,
                             Email              = searchItem.Email,
                             QueryId            = searchItem.QueryId,
                             IsResolved         = searchItem.IsResolved,
                             ProviderName       = searchItem.ProviderName
                         };

            context.Response.Write(LoadUserControl(context, result.ToList()));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        protected string LoadUserControl(HttpContext context, List<SearchQueryResultItem> data)
        {
            using (Page page = new Page())
            {
                StringBuilder s = new StringBuilder();
                PlaceHolder f = new PlaceHolder();

                string url = string.Format("~/Layouts/SubLayouts/OfflinePortal/{0}.ascx", string.IsNullOrEmpty(_query["graph"]) ? "OfflinePortalSearchCustomerQueryResultData" : "OfflinePortalSearchCustomerQueryGraphData");

                using (StringWriter writer = new StringWriter(CultureInfo.CurrentCulture))
                {

                    OfflinePortalCustomerSearchResultData avp = (OfflinePortalCustomerSearchResultData)page.LoadControl(url);
                    avp.SearchQueryResultItems = data;
                    f.Controls.Add(avp);
                    page.Controls.Add(f);
                    HttpContext.Current.Server.Execute(page, writer, false);
                    s.Clear();
                    s.Append(writer.ToString());
                }
                return s.ToString();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Request"></param>
        /// <returns></returns>
        private string GetDocumentContents(System.Web.HttpRequest Request)
        {
            string documentContents;

            using (Stream receiveStream = Request.InputStream)
            {
                using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
                {
                    documentContents = readStream.ReadToEnd();
                }
            }

            return documentContents;
        }
    }
}