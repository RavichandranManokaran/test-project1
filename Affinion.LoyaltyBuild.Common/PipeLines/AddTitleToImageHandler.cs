﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           AddTitleToImageHandler.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Extented of AddTitleToImage Pipeline
/// </summary>


using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using Sitecore.Pipelines.RenderField;
using Sitecore.Data.Fields;
using Sitecore.Xml.Xsl;

namespace Affinion.LoyaltyBuild.Common.PipeLines
{
    /// <summary>
    /// Pipeline to handle user login interaction
    /// </summary>
    public class AddTitleToImageHandler
    {
      
        /// <summary>
        /// Proccess method of AddTitleToImage pipeline
        /// </summary>
        /// <param name="args">Arguments</param>
        public void Process(RenderFieldArgs args)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(args.Result.FirstPart);
            HtmlAgilityPack.HtmlNodeCollection missingTitles = doc.DocumentNode.SelectNodes("//img[not(@title)]");

            if (missingTitles == null || missingTitles.Count < 1)
            {
                return;
            }

            foreach (HtmlAgilityPack.HtmlNode img in missingTitles)
            {
                string title = img.GetAttributeValue("alt", String.Empty);

                if (String.IsNullOrEmpty(title))
                {
                    title = img.GetAttributeValue("longdesc", String.Empty);
                }

                if (!String.IsNullOrEmpty(title))
                {
                    img.SetAttributeValue("title", title);
                }
            }

            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            doc.Save(sw);
            sw.Close();
            args.Result.FirstPart = sb.ToString();
        }
    }



}
