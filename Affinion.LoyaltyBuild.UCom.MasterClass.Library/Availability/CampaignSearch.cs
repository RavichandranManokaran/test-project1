﻿using Affinion.LoyaltyBuild.UCom.Common.Constants;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Component;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Queries;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UCommerce;
using UCommerce.Catalog;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Marketing;
using UCommerce.Marketing.Awards.AwardResolvers;
using UCommerce.Marketing.TargetingContextAggregators;
using UCommerce.Marketing.Targets.TargetResolvers;
using UCommerce.Runtime;
using Affinion.LoyaltyBuild.UCom.Common.Extension;
using Affinion.LoyaltyBuild.Common.Instrumentation;
/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/


namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Availability
{
    /// <summary>
    /// Service that provides methods to perform all Availabilty related actions
    /// </summary>

    public class CampaignSearch : ICampaignSearch
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="supplierId"></param>
        /// <param name="startDate"></param>
        /// <param name="startDate"></param>
        /// <param name="includepricing"></param>
        /// <returns>list of supplierCampaign</returns>

        public List<Model.SupplierInviteDetail> GetCampaignDetailBySupplier(string supplierId, DateTime startDate, DateTime endDate, string clientId = null, bool includepricing = false)
        {
            List<Model.SupplierInviteDetail> supplierCampaign = new List<Model.SupplierInviteDetail>();
            List<CampaignItem> campaignItem = ObjectFactory
                .Instance
                .Resolve<IRepository<CampaignItem>>()
                .Select(new CampaignItemByDateQuery(startDate, endDate)).ToList();

            List<SupplierInvite> supplierInvite = SupplierInvite.All().Where(x => x.Sku == supplierId).ToList();
            if (supplierInvite != null && supplierInvite.Any())
            {
                foreach (var item in supplierInvite)
                {
                    CampaignItem cItem = campaignItem.FirstOrDefault(x => x.Id.Equals(item.CampaignItem));
                    CampaignOfferDetail offerDetail = null;
                    if (cItem != null)
                    {
                        CampaignItemProperty cProperty = cItem.CampaignItemProperties.FirstOrDefault(x => x.DefinitionField.Name.Equals("OfferManageBySupplier"));
                        bool manageBySupplier = false;
                        if (cProperty != null && !string.IsNullOrEmpty(cProperty.Value))
                        {
                            manageBySupplier = Convert.ToBoolean(cProperty.Value);
                        }
                        string offerDescription = "Promotion Description Not Avialable";
                        CampaignItemProperty cDescriptionProperty = cItem.CampaignItemProperties.FirstOrDefault(x => x.DefinitionField.Name.Equals("OfferSpecificInformation"));
                        if (string.IsNullOrEmpty(item.Offerinformation))
                        {
                            if (cDescriptionProperty != null && !string.IsNullOrEmpty(cDescriptionProperty.Value))
                            {
                                offerDescription = cDescriptionProperty.Value;
                                offerDescription = Regex.Replace(offerDescription, @"\r\n?|\n", "<br />");
                            }
                        }
                        else
                        {
                            offerDescription = item.Offerinformation;
                        }
                        //price calculation needs to be implemented later
                        //if (!item.IsSubscribe && includepricing)
                        //{
                        //    offerDetail = this.GetOfferPriceByCampaignIds(supplierId, new List<int>() { item.CampaignItem }, clientId).FirstOrDefault();
                        //}
                        //if (offerDetail != null || item.IsSubscribe)
                        //{
                        supplierCampaign.Add(new Model.SupplierInviteDetail()
                        {
                            CampaignName = cItem.Name,
                            SupplierInviteId = item.SupplierInviteId,
                            IsSubscibe = item.IsSubscribe,
                            OfferDescription = offerDescription,
                            ManageBySupplier = manageBySupplier,
                            CampaignItemId = item.CampaignItem,
                            OfferPrice = null//(offerDetail == null ? null : offerDetail.OfferPrice)
                        });
                        //}
                    }
                }
            }
            supplierCampaign = supplierCampaign.OrderBy(x => x.CampaignName).ToList();
            return supplierCampaign;
            //ObjectFactory.Instance.Resolve<CampaignItem>()
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="supplierId"></param>
        /// <param name="supplierInviteId"></param>
        /// <param name="isSubscribe"></param>
        /// <returns>bool if Campaign is subscribed</returns>

        public bool ChangeCampaignSubscribe(string supplierId, int supplierInviteId, bool isSubscribe, string content)
        {
            SupplierInvite supplierInvite = SupplierInvite.All().Where(x => x.Sku == supplierId && x.SupplierInviteId == supplierInviteId).FirstOrDefault();
            if (supplierInvite != null)
            {
                supplierInvite.IsSubscribe = isSubscribe;
                if (isSubscribe)
                {
                    supplierInvite.Offerinformation = content;
                }
                supplierInvite.Save();
                return true;
            }
            return false;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="supplierInviteId"></param>
        /// <returns>CampaignItem</returns>
        public int GetCampaignItemIdBySupplierId(int supplierInviteId)
        {
            SupplierInvite supplierInvite = SupplierInvite.All().Where(x => x.SupplierInviteId == supplierInviteId).FirstOrDefault();
            if (supplierInvite != null)
            {
                return supplierInvite.CampaignItem;
            }
            return 0;
            //throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="supplierId"></param>
        /// <returns><list> participating CampaignItem</list>CampaignItem</returns>
        public List<CampaignItem> GetCampaignItemsBySupplier(string supplierId)
        {
            List<SupplierInvite> supplierInvite = SupplierInvite.All().Where(x => x.Sku == supplierId && x.IsSubscribe).ToList();
            if (supplierInvite != null)
            {
                List<int> campaignItemIds = supplierInvite.Select(invite => invite.CampaignItem).ToList();
                List<CampaignItem> cItems = CampaignItem.All().Where(x => campaignItemIds.Contains(x.CampaignItemId) && x.Campaign.Enabled && x.Enabled).ToList();
                return cItems;
            }

            return new List<CampaignItem>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="supplierId"></param>
        /// <param name="campaignItemId"></param>
        /// <param name="supplierInviteId"></param>
        /// <param name="calculatediscountprice"></param>
        /// <param name="calculateoneyear"></param>
        /// <returns>offerDetail</returns>
        public OfferDetail GetCampaignById(string supplierId, int campaignItemId, int supplierInviteId, bool calculatediscountprice = false, bool calculateoneyear = false)
        {
            OfferDetail offerDetail = null;
            SupplierInvite supplierInvite = SupplierInvite.All().Where(x => x.Sku == supplierId && x.CampaignItem == campaignItemId && x.SupplierInviteId == supplierInviteId).FirstOrDefault();
            if (supplierInvite != null)
            {
                CampaignItem cItem = CampaignItem.All().Where(x => x.CampaignItemId == campaignItemId).FirstOrDefault();
                offerDetail = new OfferDetail();
                if (cItem != null)
                {
                    offerDetail.OfferName = cItem.Name;

                    try
                    {
                        Category supplierCategory = Category.All().Where(x => x.Guid.ToString() == supplierId).FirstOrDefault();
                        if (supplierCategory != null)
                        {
                            List<Product> supplierProduct = supplierCategory.Products.ToList();
                            IRepository<CampaignRevenue> campaignRevenue = ObjectFactory.Instance.Resolve<IRepository<CampaignRevenue>>();
                            List<CampaignRevenue> campaingRevenueList = campaignRevenue.Select(new GetCampaignRevenueQuery(campaignItemId)).ToList();
                            campaingRevenueList = campaingRevenueList.Where(x => supplierProduct.Any(y => y.Sku == x.ProductSKU)).ToList();
                            DateTime todayDate = DateTime.Now.ConvertDateToString().ParseFormatDateTime().AddDays(1).AddSeconds(-1);
                            List<CampaignRevenue> bookedhistroy = campaingRevenueList.Where(x => x.BookingDate <= todayDate).ToList();
                            if (calculateoneyear)
                            {
                                bookedhistroy = bookedhistroy.Where(x => x.BookingDate >= DateTime.Now.AddYears(-1).ConvertDateToString().ParseFormatDateTime()).ToList();
                            }
                            List<CampaignRevenue> futurehistory = campaingRevenueList.Where(x => x.BookingDate > todayDate).ToList();

                            offerDetail.BookingReceived = bookedhistroy.Sum(x => x.TotalBooking);
                            offerDetail.RevenueGenerated = bookedhistroy.Sum(x => x.TotalSale);

                            offerDetail.FutureBooking = futurehistory.Sum(x => x.TotalBooking);
                            offerDetail.FutureArrival = futurehistory.Sum(x => x.TotalSale);
                            if (calculatediscountprice)
                            {
                                List<CampaignOfferDetail> offerPriceDetails = this.GetOfferPriceByCampaignIds(supplierId, new List<int>() { campaignItemId }, string.Empty);
                                if (offerPriceDetails != null && offerPriceDetails.Count == 1)
                                {
                                    CampaignOfferDetail offerPriceDetail = offerPriceDetails.FirstOrDefault();
                                    offerDetail.HotelRate = offerPriceDetail.OfferPrice.Min(x => x.price);
                                    offerDetail.HotelDiscountedRate = offerPriceDetail.OfferPrice.Min(x => x.PriceIncluVat);
                                }
                                else
                                {
                                    //TODO : Chnages for multi room price
                                    offerDetail.HotelRate = 0;
                                    offerDetail.HotelDiscountedRate = 0;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Diagnostics.WriteException(DiagnosticsCategory.MasterClassBusinessLogic, ex, this);
                        throw;
                    }
                }
            }
            return offerDetail;
            //throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="supplierId"></param>
        /// <param name="campaignItemId"></param>
        /// <param name="supplierInviteId"></param>
        /// <param name="reason"></param>
        /// <returns>UnSubScribeForOffer</returns>
        public bool UnSubScribeForOffer(string supplierId, int campaignItemId, int supplierInviteId, string reason)
        {
            SupplierInvite supplierInvite = SupplierInvite.All().Where(x => x.Sku == supplierId && x.CampaignItem == campaignItemId && x.SupplierInviteId == supplierInviteId).FirstOrDefault();
            if (supplierInvite != null)
            {
                try
                {
                    SupplierOfferUnSubScribe offerUnsubsribe = new SupplierOfferUnSubScribe();
                    offerUnsubsribe.CampaignItem = supplierInvite.CampaignItem;
                    offerUnsubsribe.Sku = supplierInvite.Sku;
                    offerUnsubsribe.Reason = reason;
                    offerUnsubsribe.CreatedDate = DateTime.Now;
                    offerUnsubsribe.CreatedBy = string.Empty;
                    offerUnsubsribe.CategoryGuid = supplierInvite.Sku;
                    offerUnsubsribe.Save();
                    supplierInvite.IsSubscribe = false;
                    supplierInvite.Save();
                    return true;
                }
                catch (Exception ex)
                {
                    Diagnostics.WriteException(DiagnosticsCategory.MasterClassBusinessLogic, ex, this);
                    return false;
                }
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="campaignItemId"></param>
        /// <returns>CampaignDetail</returns>
        public CampaignDetail GetCampaignItemById(int campaignItemId)
        {
            CampaignItem cItem = CampaignItem.All().Where(x => x.CampaignItemId == campaignItemId).ToList().FirstOrDefault();
            if (cItem != null)
            {
                return new CampaignDetail() { CampaignName = cItem.Name };
            }
            return new CampaignDetail();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="supplierId"></param>
        /// <param name="campaignId"></param>
        /// <param name="clientId"></param>
        /// <returns>OfferPrice based on campaignid</returns>
        public List<CampaignOfferDetail> GetOfferPriceByCampaignIds(string supplierId, List<int> campaignId, string clientId)
        {
            ICatalogContext catalogContext = ObjectFactory.Instance.Resolve<ICatalogContext>();
            IDiscountService discountService = ObjectFactory.Instance.Resolve<IDiscountService>();
            IPricingService pricingService = ObjectFactory.Instance.Resolve<IPricingService>();
            ITaxService taxService = ObjectFactory.Instance.Resolve<ITaxService>();
            PriceGroup priceGroup = catalogContext.CurrentPriceGroup;
            List<CampaignOfferDetail> offerDetail = new List<CampaignOfferDetail>();
            Product hotel = Product.All().FirstOrDefault(x => x.Sku.ToString() == supplierId);
            if (hotel != null)
            {
                List<Product> products = hotel.Variants.ToList();//.Where(x => x.DisplayOnSite && x.AllowOrdering && x.ProductDefinition.Name.Equals(ProductDefinationConstant.HotelRoom, StringComparison.InvariantCultureIgnoreCase)).ToList();
                List<CampaignItem> cItems = CampaignItem.All().Where(x => campaignId.Contains(x.CampaignItemId)).ToList();
                LBMarketingService service = new LBMarketingService(ObjectFactory.Instance.Resolve<IRepository<CampaignItem>>(), ObjectFactory.Instance.Resolve<ITargetingContextAggregator>(), ObjectFactory.Instance.Resolve<ITargetAggregator>(), ObjectFactory.Instance.Resolve<IAwardAggregator>());
                List<string> productSku = products.Select(x => x.VariantSku).ToList();
                List<string> strcampaignId = campaignId.Select(x => x.ToString()).ToList();
                List<SubRate> supplierSubrate = SubRate.All().Where(x => strcampaignId.Contains(x.ItemID) || productSku.Contains(x.ItemID)).ToList();
                foreach (var item in cItems)
                {
                    RoomTypeTarget roomTarget = RoomTypeTarget.FirstOrDefault(x => x.CampaignItem.CampaignItemId == item.CampaignItemId);
                    List<Product> campaignProduct = new List<Product>();
                    if (roomTarget != null)
                    {
                        List<Product> filterProduct = products.Where(x => x.ProductProperties != null && x.ProductProperties.Any(y => y.ProductDefinitionField.Name.Equals(ProductDefinationConstant.RoomType, StringComparison.InvariantCultureIgnoreCase) && y.Value.Equals(roomTarget.RoomType, StringComparison.InvariantCultureIgnoreCase))).ToList();
                        campaignProduct.AddRange(filterProduct);
                    }
                    else
                    {
                        campaignProduct.AddRange(products);
                    }
                    List<OfferPrice> offerPrice = new List<OfferPrice>();
                    OfferPrice price = new OfferPrice();
                    ArrivalDateTarget arrivalDate = ArrivalDateTarget.All().FirstOrDefault(x => x.CampaignItem.CampaignItemId == item.CampaignItemId);
                    if (arrivalDate != null)
                    {
                        price.ArrivalDate = new CampaginItemDateRange() { EndDate = arrivalDate.ArrivalToDate.ConvertDateToString(), StartDate = arrivalDate.ArrivalFromDate.ConvertDateToString() };
                    }
                    ReservationDateTarget restarget = ReservationDateTarget.All().FirstOrDefault(x => x.CampaignItem.CampaignItemId == item.CampaignItemId);
                    if (restarget != null)
                    {
                        price.ReservationDate = new CampaginItemDateRange() { EndDate = restarget.ReservationToDate.ConvertDateToString(), StartDate = restarget.ReservationFromDate.ConvertDateToString() };
                    }
                    int night = -1;
                    NightTarget nightTarget = NightTarget.All().FirstOrDefault(x => x.CampaignItem.CampaignItemId == item.CampaignItemId);
                    if (nightTarget != null)
                    {
                        night = nightTarget.Night;
                    }
                    bool isvalidCampaign = false;
                    if (campaignProduct.Any())
                    {

                        foreach (var product in campaignProduct)
                        {
                            PurchaseOrder purchaseOrder = new PurchaseOrder
                            {
                                BillingCurrency = priceGroup.Currency,
                                ProductCatalogGroup = catalogContext.CurrentCatalogGroup
                            };

                            if (product.PriceGroupPrices.Count == 0)
                                continue;
                            purchaseOrder.AddOrderLine(AddProductToOrderLineInMemory(pricingService, taxService, priceGroup, product, false, night));
                            while (purchaseOrder.Discounts.Count > 0)
                            {
                                Discount discount = purchaseOrder.Discounts.First<Discount>();
                                purchaseOrder.RemoveDiscount(discount);
                            }

                            if (service.Apply(purchaseOrder, item))
                            {
                                isvalidCampaign = true;
                                decimal discountOrderLine = 0;
                                foreach (var orderLines in purchaseOrder.OrderLines)
                                {
                                    if (orderLines.Discounts != null && orderLines.Discounts.Any())
                                    {
                                        discountOrderLine = orderLines.Discounts.Sum(x => x.AmountOffTotal);
                                    }
                                }
                                bool campaingSubrate = true;
                                SubRate campaignRate = supplierSubrate.FirstOrDefault(x => x.ItemID == item.CampaignItemId.ToString() && x.SubrateItemCode == SUBRateConstant.LBCommision);
                                if (campaignRate == null)
                                {
                                    campaingSubrate = false;
                                    campaignRate = supplierSubrate.FirstOrDefault(x => x.ItemID == product.Sku.ToString() && x.SubrateItemCode == SUBRateConstant.LBCommision);
                                }

                                decimal totalPrice = (purchaseOrder.OrderLines.Sum(x => x.Price * x.Quantity) - discountOrderLine);
                                decimal lbCommision = 0;
                                decimal hotelSellablePrice = totalPrice;
                                decimal hotelSellablePriceVat = totalPrice;
                                if (campaignRate != null)
                                {
                                    if (campaingSubrate)
                                    {
                                        if (campaignRate.IsPercentage)
                                        {
                                            lbCommision = Math.Round(((totalPrice * campaignRate.Price) / 100), 2);
                                        }
                                        else
                                        {
                                            lbCommision = campaignRate.Price;
                                        }
                                    }
                                    else
                                    {
                                        if (campaignRate.IsPercentage)
                                        {
                                            lbCommision = Math.Round(((totalPrice * campaignRate.Price) / 100), 2);
                                        }
                                        else
                                        {
                                            lbCommision = (campaignRate.Price * (night == -1 ? 1 : night));
                                        }
                                    }
                                    hotelSellablePrice = totalPrice - lbCommision;
                                    hotelSellablePriceVat = hotelSellablePrice + Math.Round(((lbCommision * campaignRate.VAT) / 100), 2);
                                }
                                decimal vat = totalPrice + purchaseOrder.OrderLines.Sum(x => x.VAT);
                                offerPrice.Add(new OfferPrice() { price = hotelSellablePrice, ReservationDate = price.ReservationDate, ArrivalDate = price.ArrivalDate, productSKU = product.Sku, RoomName = product.Name, PriceIncluVat = hotelSellablePriceVat });
                            }
                        }
                    }
                    else
                    {
                        offerPrice.Add(new OfferPrice() { price = -1, ReservationDate = price.ReservationDate, ArrivalDate = price.ArrivalDate });
                    }
                    if (isvalidCampaign)
                        offerDetail.Add(new CampaignOfferDetail() { CampaignItemId = item.CampaignItemId, OfferPrice = offerPrice });
                }
            }
            return offerDetail;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pricingService"></param>
        /// <param name="taxService"></param
        /// <param name="priceGroup"></param>
        /// <param name="product"></param>
        /// <param name="addOnProduct"></param>
        /// <returns>orderLine properties</returns>
        private OrderLine AddProductToOrderLineInMemory(IPricingService pricingService, ITaxService taxService, PriceGroup priceGroup, Product product, bool addOnProduct = false, int quanitty = -1)
        {
            List<OrderProperty> orderProperties = new List<OrderProperty>(); ;
            if (addOnProduct)
            {
                orderProperties.Add(new OrderProperty() { Key = OrderPropertyConstants.IgnorInSearch, Value = "1" });
            }
            orderProperties.Add(new OrderProperty() { Key = OrderPropertyConstants.IgnorCampaignSearchCriteria, Value = "1" });
            orderProperties.Add(new OrderProperty() { Key = OrderPropertyConstants.ForSearch, Value = "1" });
            Money productPrice = pricingService.GetProductPrice(product, priceGroup);
            OrderLine orderLine = new OrderLine
            {
                Sku = product.Sku,
                VariantSku = product.VariantSku,
                Price = productPrice.Value,
                VAT = taxService.CalculateTax(priceGroup, productPrice).Value,
                VATRate = priceGroup.VATRate,
                Quantity = (quanitty != -1 ? quanitty : 1),
                OrderProperties = orderProperties
            };
            return orderLine;

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="campaignItemId"></param>
        /// <param name="supplierId"></param
        /// <returns>campaignitem</returns>
        public int GetSupplierInviteIdByCampaign(int campaignItemId, string supplierId)
        {
            SupplierInvite supplierInviteId = SupplierInvite.All().Where(x => x.CampaignItem == campaignItemId && x.Sku == supplierId).FirstOrDefault();
            if (supplierInviteId != null)
            {
                return supplierInviteId.SupplierInviteId;
            }
            return 0;

        }


        public CampaignDetail GetCampaignDescriptionById(string supplierId, int campaignItemId, int supplierInviteId)
        {
            CampaignDetail detail = new CampaignDetail();
            CampaignItem cItem = CampaignItem.All().FirstOrDefault(x => x.CampaignItemId == campaignItemId);
            SupplierInvite invite = SupplierInvite.All().FirstOrDefault(x => x.SupplierInviteId == supplierInviteId);
            if (cItem == null)
                return detail;
            CampaignItemProperty cDescriptionProperty = cItem.CampaignItemProperties.FirstOrDefault(x => x.DefinitionField.Name.Equals("OfferSpecificInformation"));
            detail.Content = "Promotion Description Not Avialable";
            if (invite != null && !string.IsNullOrEmpty(invite.Offerinformation))
            {
                detail.Content = invite.Offerinformation;
            }
            else if (cDescriptionProperty != null && !string.IsNullOrEmpty(cDescriptionProperty.Value))
            {
                detail.Content = cDescriptionProperty.Value;
                detail.Content = Regex.Replace(detail.Content, @"\r\n?|\n", "<br />");
            }
            return detail;
        }
    }
}
