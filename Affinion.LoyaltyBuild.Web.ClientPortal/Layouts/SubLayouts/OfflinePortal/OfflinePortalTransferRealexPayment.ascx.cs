﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    public partial class OfflinePortalTransferRealexPayment : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            paymentIframe.Src = string.Format("../../../Pages/TransferPaymentPage.aspx?olid={0}&cid={1}&amt={2}&guid={3}", Request.QueryString["olid"], Request.QueryString["cid"], Request.QueryString["amt"],Request.QueryString["guid"]);
        }
    }
}