﻿CREATE TABLE [dbo].[LB_UpgradeProductSkuAward] (
    [UpgradeProductSkuAwardId] INT            NOT NULL,
    [UpgradedRoomType]         NVARCHAR (500) NOT NULL,
    CONSTRAINT [PK_uCommerce_UpgradeProductSkuAward] PRIMARY KEY CLUSTERED ([UpgradeProductSkuAwardId] ASC),
    CONSTRAINT [FK_uCommerce_UpgradeProductSkuAward_uCommerce_Award] FOREIGN KEY ([UpgradeProductSkuAwardId]) REFERENCES [dbo].[uCommerce_Award] ([AwardId])
);

