﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.PrimaryLayouts.SupplierPortal.Login" %>

<%@ Import Namespace="Sitecore.Configuration" %>
<%@ Import Namespace="Sitecore.SecurityModel.License" %>

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="CODE_LANGUAGE" content="C#" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <link href="/default.css" rel="stylesheet" />

    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="description" content="Bootstrap, a sleek, intuitive, and powerful mobile first front-end framework for faster and easier web development."/>
    <meta name="keywords" content="HTML, CSS, JS, JavaScript, framework, bootstrap, front-end, frontend, web development"/>
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors"/>
    <script src="/Resources/scripts/jquery.min.js"></script>
    <script src="/Resources/bootstrap_assets/javascripts/bootstrap.min.js"></script>
    <link rel="stylesheet" href="/Resources/styles/bootstrapsupplier.min.css"/>
    <link rel="stylesheet" href="/Resources/styles/affinion-supplieradmin.min.css"/>
    <link rel="stylesheet" href="/Resources/styles/themesupplier.css"/>
    <link rel="shortcut icon" type="image/x-icon" href="/Resources/images/favicon.ico" />
    <script>
        $(document).ready(function () {
            $("table tr:even").css("background-color", "#f4f1ee");
            $("table tr:odd").css("background-color", "#fff");
            $("#myTab a").click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });

        });
    </script>

</head>
    <div class="container-outer width-full overflow-hidden-att app-bg" id="ctl1">
    <div class="top-header-login width-full" id="ctl2">
        <div class="login-form-logo" id="ctl3">
            <sc:FieldRenderer runat="server" ID="FieldRenderer1" FieldName="HeaderLogo"></sc:FieldRenderer>
        </div>
    </div>
    <div class="login-form-tagline-outer" id="ctl6">
        <div class="login-form-tagline" id="ctl7">
            <div class="login-form-tagline-inner" id="ctl8">
                <span>Share our success</span>
                <div class="login-form-tagline-bottom" id="ctl9">
                </div>
            </div>

        </div>
    </div>
        </div>
<body class="cbp-spmenu-push">
    <form id="mainform" runat="server">
            <sc:Placeholder runat="server" Key="main"></sc:Placeholder>
    </form>

    <script src="scripts/tdm-navigation.js" type="text/javaScript"></script>
    <!-- Bootstrap core And jQuery JavaScript-->
    <script src="scripts/jquery.2.1.0.min.js" type="text/javaScript"></script>
    <!--jQuery UI-->
    <script src="scripts/jquery-ui.custom.min.js" type="text/javaScript"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="scripts/jquery.1.11.0.min.js"></script>
    <script src="scripts/html5shiv.js"></script>
    <script src="scripts/respond.js"></script>
    <![endif]-->

    <!-- Bootsrap -->
    <script src="bootstrap_assets/javascripts/bootstrap.min.js" type="text/javaScript"></script>

    <!-- ================================================== -->

    <script src="scripts/affinion-framework.js" type="text/javascript"></script>
    <script src="scripts/affinion-plugins.js" type="text/javascript"></script>

    <!--Language Links-->
    <script src="scripts/languages/datepicker-zh-CN.js" type="text/javascript"></script>
    <script src="scripts/languages/datepicker-fr.js" type="text/javascript"></script>
    <script src="scripts/languages/datepicker-en-US.js" type="text/javascript"></script>

    <!-- ================================================== -->
    <script type="text/javascript">
        $(document).ready(function () {

            //Init All DatePickers in the page
            $("#datepicker1,#datepicker2").datepicker({
                showOn: "both",
                buttonImage: Rgen.Config.DatePicker.Image,
                buttonImageOnly: true

            });

            var tdmlanguage = document.querySelectorAll("[data-tdm-language]");

            setLanguage(getSelected);
            var dt = tdmlanguage[0].dataset;

            var getSelected;
            var currentLanguage_db = "English";

            function setLanguage(getSelected) {
                if (getSelected !== undefined) {
                    currentLanguage_db = getSelected;
                    $(tdmlanguage[0]).text(currentLanguage_db);
                } else {
                    $(tdmlanguage[0]).text(currentLanguage_db);
                }
            }
            setLanguage(currentLanguage_db);
            $('#lang-globe .dropdown-menu li a').click(function (e) {
                var getSelected = $(this).text();
                setLanguage(getSelected);
            });

            /*Carousel: Init : data-ride='carousel':removed */
            $(".carousel").carousel();
        });
    </script>
</body>
       <div class="login-form-footer" id="ctl35">
            <div class="container" id="ctl36">
                <div class="col-md-3" id="ctl37">

                    <sc:FieldRenderer runat="server" ID="FieldRenderer4" FieldName="PciLogo" class="img-responsive"></sc:FieldRenderer>
                    <!--<img src="images/logos-pci-trustware.png" alt="" class="img-responsive">-->
                </div>
                <div class="col-md-6" id="ctl40">
                    <div class="login-form-copyright-text" id="ctl41">
                        <span>Copyright © 2015 </span>Loyaltybuild | <a href="http://www.loyaltybuild.com/">www.loyaltybuild.com</a>
                    </div>
                </div>
                <div class="col-md-3" id="ctl42">
                    <!--<img src="images/happiness-earned.png" alt="" class="img-responsive">-->
                    <sc:FieldRenderer runat="server" ID="FieldRenderer3" FieldName="HappinessEarnedLogo" class="img-responsive"></sc:FieldRenderer>
                </div>
            </div>
        </div>
</html>
