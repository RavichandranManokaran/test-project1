

CREATE TABLE [dbo].[uCommerce_RecentlySeenHotels](
	[RSeenID] [int] IDENTITY(1,10) NOT NULL,
	[CustomerID] [int] NULL,
	[HotelID] [varchar](50) NULL,
	[CreatedBy] [varchar](100) NULL,
	[UpdatedBy] [varchar](100) NULL,
	[RSeenDate] [datetime] NULL
) ON [PRIMARY]

GO
