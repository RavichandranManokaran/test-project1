﻿using Affinion.LoyaltyBuild.AdminPortal.DataExtraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.AdminPortal.sitecore_modules.Shell.Affinion.ExternalDataHandling
{
    public partial class AddOfferData : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!User.Identity.IsAuthenticated)
            {
                Response.Redirect("/sitecore/login?returnUrl=" + HttpUtility.UrlEncode(this.Request.Url.PathAndQuery));
            }

            OfferInfoHttpSitecoreDataProvider dataProvider = new OfferInfoHttpSitecoreDataProvider();
            dataProvider.SetData(this.Request.Url);

            SitecoreExternalDataExtractionResult result = SitecoreExternalDataExtraction.SaveToDraftState(dataProvider);

            if (result.ErrorType != SitecoreExternalDataExtractionErrorType.Failed)
            {
                this.Response.Redirect(string.Format("http://{0}:{1}/sitecore/shell/Applications/Content%20Editor?id={2}&fo={2}&sc_bw=1&sc_lang=en", this.Request.Url.Host, this.Request.Url.Port, result.ContextItem.ID.ToString()));
            }
            else
            {
                this.ErrorMessage.InnerText = result.Exception.Message;
            }
        }
    }
}