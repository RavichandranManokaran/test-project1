﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalSearchResultsSublayout.ascx.cs" Inherits="Affinion.Loyaltybuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalSearchResultsSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Affinion.LoyaltyBuild.UCom.Common.Extension" %>
<%@ Import Namespace="Sitecore.Data.Items" %>
<%@ Import Namespace="Affinion.LoyaltyBuild.Search" %>
<%@ Import Namespace="Affinion.LoyaltyBuild.Search.Data" %>
<%@ Import Namespace="Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model" %>
<%@ import namespace="Affinion.LoyaltyBuild.Model.Provider" %>

<link href="/Resources/styles/theme1/cstheme.css" rel="stylesheet" />

<div class="col-md-12">
    <div class="county-filter col-xs-12 alert alert-info accent6-bg" id="CountySection" runat="server" visible="false">
        <div class="col-xs-12 col-md-6">
            <h4 class="details-header no-margin padding-top5"><sc:text field="CountyFilter" id="CountyFilterLabel" runat="server" /></h4>
        </div>
        <div class="col-xs-12 col-md-6">
            <div class="select-loc" aria-label="..." role="group">
                <asp:DropDownList CssClass="form-control" ID="CountyDropDown" ClientIDMode="Static" runat="server" OnSelectedIndexChanged="CountyDropDown_SelectedIndexChanged" AutoPostBack="true" EnableViewState="true" />
            </div>
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="search-result-top">
        <p>
            <asp:Label ID="NoResults" runat="server" />
        </p>
    </div>
</div>

<div id="SearchResultsAll" runat="server">

    <div class="col-md-12">
        <div class="search-result-top">
            <p>
                <asp:Label ID="ResultsSummary" runat="server" />
            </p>
        </div>
    </div>

    <%--sort section--%>
    <div class="col-md-12" id="SortSection" runat="server">

        <div class="search-nav">

            <ul class="nav nav-tabs">
                <li class="sort-by"> <sc:text field="SortBy" id="SortByLabel" runat="server" /></li>
                <li role="presentation" class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="<%=this.SortLink(SortMode.Price, "0") %>" role="button" aria-haspopup="true" aria-expanded="false" id="Price">
                        <sc:text field="SortByPriceLabel" runat="server" />
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<%=this.SortLink(SortMode.Price, "0") %>" id="PriceLH"><sc:text field="PriceLowestToHighest" id="PriceLowestToHighestLabel" runat="server" /></a></li>
                        <li><a href="<%=this.SortLink(SortMode.Price, "1") %>" id="PriceHL"><sc:text field="PriceHighestToLowest" id="PriceHighestToLowestLabel" runat="server" /></a></li>
                    </ul>
                </li>

                <li role="presentation" class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="<%=this.SortLink(SortMode.Stars, "0") %>" role="button" aria-haspopup="true" aria-expanded="false" id="Stars">
                        <sc:text field="SortByRateLabel" runat="server" />
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="<%=this.SortLink(SortMode.Stars, "0") %>" id="StarLH"><sc:text field="StarsLowestToHighest" id="StarsLowestToHighestLabel" runat="server" /></a></li>
                        <li><a href="<%=this.SortLink(SortMode.Stars, "1") %>" id="StarHL"><sc:text field="StarsHighestToLowest" id="StarsHighestToLowestLabel" runat="server" /></a></li>
                    </ul>
                </li>

                <li role="presentation" class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="<%=this.SortLink(SortMode.Recommend, "0") %>" role="button" aria-haspopup="true" aria-expanded="false" id="Recommend">
                        <sc:text field="SortByRecommendLabel" runat="server" />
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<%=this.SortLink(SortMode.Recommend, "0") %>" id="RecommendLH"><sc:text field="PriceLowestToHighest" id="RecommendLowestToHighestLabel" runat="server" /></a></li>
                        <li><a href="<%=this.SortLink(SortMode.Recommend, "1") %>" id="RecommendHL"><sc:text field="PriceHighestToLowest" id="RecommendHighestToLowestLabel" runat="server" /></a></li>
                    </ul>
                </li>

                <li role="presentation" class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="<%=this.SortLink(SortMode.SupplierName, "0") %>" role="button" aria-haspopup="true" aria-expanded="false" id="SupplierName">
                        <sc:text field="SortByHotelNameLabel" runat="server" />
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="<%=this.SortLink(SortMode.SupplierName, "0") %>" id="SupplierNameAsc"><sc:text field="SupplierNameAscendingOrder" id="SupplierNameAscendingOrderLabel" runat="server" /></a></li>
                        <li><a href="<%=this.SortLink(SortMode.SupplierName, "1") %>" id="SupplierNameDes"><sc:text field="SupplierNameDescendingOrder" id="SupplierNameDescendingOrderLabel" runat="server" /></a></li>
                    </ul>
                </li>

                <%--<li role="presentation" class="dropdown">
                    <a href="<%=this.SortLink(SortMode.DistanceFromCityCentre, "0") %>" role="button" aria-haspopup="true" aria-expanded="false">
                        <sc:text field="SortByDistanceLabel" runat="server" />
                    </a>
                </li>--%>
            </ul>

        </div>

        <div class="col-md-12 no-padding  margin-row">
            <div class="no-padding-left col-md-6 padding-top-1">
                <asp:Button ID="PreviousWeeks" runat="server" CssClass="btn btn-secondry booking-margin-top  pull-left btn-bg" OnClientClick="javascript:__doPostBack('PreviousWeeks')" />
            </div>

            <div class="no-padding-right col-md-6">
                <asp:Button ID="NextWeeks" runat="server" CssClass="btn btn-secondry booking-margin-top pull-right  btn-bg" OnClientClick="javascript:__doPostBack('NextWeeks')" />
            </div>
        </div>

    </div>

    <div class="col-md-12">
        <div class="content-body">
            <div class="accordian-outer">
                <div class="panel-group tbl-accordion" id="accordion">

                    <asp:Repeater ID="SearchResultsRepeater" runat="server">

                        <ItemTemplate>

                            <div class="panel panel-default">
                                <%--Accordion header--%>
                                <div class="panel-heading bg-primary>

                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse<%#Container.ItemIndex %>" class="collapsed">
                                        <div class="star-rank pull-left width-full">

                                            <asp:Repeater ID="StarRepeater" runat="server">
                                                <ItemTemplate>
                                                    <span class="fa fa-star"></span>
                                                </ItemTemplate>
                                            </asp:Repeater>

                                            <span class="panel-title">
                                                <span data-toggle="collapse" data-parent="#accordion" href="#collapse<%#Container.ItemIndex %>" class="show-hide-list collapsed">
                                                    <sc:text field="Name" item="<%#((HotelSearchResultItem)Container.DataItem).SitecoreItem %>" runat="server"   visible="<%#(((HotelSearchResultItem)Container.DataItem).ProviderType == ProviderType.LoyaltyBuild) %>"   />
                                                    <asp:Literal id="Name" Text="<%#((HotelSearchResultItem)Container.DataItem).Name %>" runat="server" visible="<%#(((HotelSearchResultItem)Container.DataItem).ProviderType != ProviderType.LoyaltyBuild) %>"   />
                                                </span>
                                            </span>
                                        </div>
                                    </a>
                                </div>

                                <%--Accordion Body--%>
                                <div id="collapse<%#Container.ItemIndex %>" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div>
                                            <%--Inner content head section--%>
                                            <div class="row cs-search-result-bg1">
                                                <span class="padding-right-5">
                                                    <sc:text field="Town" item="<%#((HotelSearchResultItem)Container.DataItem).SitecoreItem %>" runat="server" visible="<%#(((HotelSearchResultItem)Container.DataItem).ProviderType == ProviderType.LoyaltyBuild) %>"  />
                                                    <asp:Literal id="Town" Text="<%#((HotelSearchResultItem)Container.DataItem).Town %>" runat="server" visible="<%#(((HotelSearchResultItem)Container.DataItem).ProviderType != ProviderType.LoyaltyBuild) %>"  />
                                                    ,
                                                    <asp:Literal ID="CountryName" runat="server" />
                                                </span>

                                                <%--<span class="accent34-f padding-right-5">TripAdvisor Traveller Rating</span>
                                                <span>                                                    
                                                    <asp:Image ID="hotelRating" runat="server" />
                                                </span>

                                                <span>
                                                    <span class="accent34-f">                                                       
                                                         <sc:text field="Reviews" runat="server" id="ReviewsLabel" visible="<%#(((HotelSearchResultItem)Container.DataItem).ProviderType == ProviderType.LoyaltyBuild) %>"  />
                                                         <asp:Literal runat="server" Text="Reviews" id="Reviews" visible="<%#(((HotelSearchResultItem)Container.DataItem).ProviderType != ProviderType.LoyaltyBuild) %>"  />
                                                    </span>
                                                    <span class="padding-right-5">
                                                        <sc:text field="NoOfReviews" runat="server" item="<%#((HotelSearchResultItem)Container.DataItem).SitecoreItem %>" visible="<%#(((HotelSearchResultItem)Container.DataItem).ProviderType == ProviderType.LoyaltyBuild) %>"  />
                                                        <asp:Literal id="NoOfReviews" Text="<%#((HotelSearchResultItem)Container.DataItem).NoOfReviews %>" runat="server" visible="<%#(((HotelSearchResultItem)Container.DataItem).ProviderType != ProviderType.LoyaltyBuild) %>"  />
                                                    </span>
                                                </span>--%>

                                                <span>
                                                    <asp:Repeater ID="HotelFacilities" runat="server">

                                                        <ItemTemplate>
                                                            <sc:link field="Link" id="FacilityLink" runat="server" item="<%#(Item)Container.DataItem %>">
                                                                <sc:Image Field="Icon" ID="FacilityImage" runat="server" Item="<%#(Item)Container.DataItem %>" />
                                                            </sc:link>
                                                        </ItemTemplate>

                                                    </asp:Repeater>

                                                </span>

                                                <span>
                                                    <button type="button" id="SitecoreProviderDetails" runat="server" onclick="OpenPopup(this)" visible="<%#(((HotelSearchResultItem)Container.DataItem).ProviderType == ProviderType.LoyaltyBuild) %>"   class="btn btn-primary booking-margin-top float-right btn-bg"
                                                        data-supplierid="<%#((HotelSearchResultItem)Container.DataItem).SitecoreItem.ID.ToString()%>"> 
                                                        <sc:text field="GetProviderDetails" runat="server" id="GetProviderDetailsLabel"/></button>

                                                     <button type="button" id="NonSitecoreProviderDetails" runat="server" onclick="OpenPopup(this)"  visible="<%#(((HotelSearchResultItem)Container.DataItem).ProviderType != ProviderType.LoyaltyBuild) %>"  class="btn btn-primary booking-margin-top float-right btn-bg" 
                                                        data-supplierid="<%#((HotelSearchResultItem)Container.DataItem).ProviderId.ToString()%>">
                                                         <asp:Literal Text="GetProviderDetails" runat="server" ID="lblGetProviderDetails"/></button>
                                                </span>
                                            </div>

                                            <%--Inner Table Content--%>
                                            <div class="panel-group margin-row" id="accordion_sub">

                                                
                                                    <div class="panel panel-default">
                                                            <div id="collapse_sub_<%#numberCount.ToString() %>" class="panel-collapse">
                                                                <div class="panel-body">
                                                                    <div class="detail-outer">
                                                                        <div class="row cs-search-result-bg2 "></div>

                                                                        <div class="table-responsive no-margin">
                                                                            <asp:Repeater ID="RoomAvailablity" runat="server">
                                                                                <HeaderTemplate>
                                                                                    <table class="table table-striped no-margin tbl-calendar font-5">
                                                                                        <thead>
                                                                                            <tr class="">
                                                                                                <th class="text-center font-6 width115 nowrap"><sc:text field="OccupancyType" runat="server" /></th>

                                                                                                <asp:Repeater ID="rptDateHeaders" runat="server">

                                                                                                    <ItemTemplate>
                                                                                                        <th class="text-center font-6">
                                                                                                            <asp:Literal ID="Day" runat="server" />
                                                                                                            <span class="accent34-f">
                                                                                                                <asp:Literal ID="Date" runat="server" /></span>
                                                                                                        </th>
                                                                                                    </ItemTemplate>

                                                                                                </asp:Repeater>
                                                                                            </tr>
                                                                                        </thead>
                                                                                        <tbody>
                                                                                </HeaderTemplate>

                                                                                <ItemTemplate>

                                                                                    <tr>
                                                                                        <td class="text-left accent34-f nowrap">
                                                                                            <asp:Literal ID="occupancyName" runat="server" />
                                                                                        </td>

                                                                                        <asp:Repeater ID="rptDates" runat="server">

                                                                                            <ItemTemplate>                                                                                                
                                                                                                <td class="text-center no-padding<asp:Literal ID="ClassPlaceholder" runat="server" />">
                                                                                                    <span class="padding-5 width-full inline-block" data-toggle="modal" data-target="#myModal"
                                                                                                        data-supplierid="<%#Eval("ProviderID")%>"
                                                                                                        data-sku="<%#Eval("Sku")%>"
                                                                                                        data-variatsku="<%#Eval("VariatSku")%>"
                                                                                                        data-price="<%#Eval("Price")%>" 
                                                                                                        data-currencyid="<%#Eval("PriceCurrency")%>"
                                                                                                        data-base="<%#Eval("RoomAvailability")%>"
                                                                                                        data-availabilitydate="<%#Eval("AvailabilityDate")%>"
                                                                                                        data-counter="<%#Eval("DateCounter")%>"
                                                                                                        data-rooms="<%#Eval("RoomAvailability")%>"
                                                                                                        data-providertype="<%#Eval("ProviderType")%>"
                                                                                                        onclick="LoadDropList(this);">

                                                                                                        <asp:Literal ID="Date" runat="server" />
                                                                                                    </span>
                                                                                                </td>
                                                                                            </ItemTemplate>

                                                                                        </asp:Repeater>
                                                                                    </tr>

                                                                                </ItemTemplate>

                                                                                <FooterTemplate>
                                                                                    </tbody>
                                                                            </table>
                                                                                </FooterTemplate>

                                                                            </asp:Repeater> 

                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                 <%--End RoomAvailablity Table Content--%>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    <%--Pagination Tab--%>
                    <div class="col-xs-12 no-padding">

                        <asp:Repeater ID="rptPagination" runat="server">

                            <HeaderTemplate>
                                <ul class="pagination">
                                    <li>
                                        <%# this.PreviousLink() %>
                                    </li>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li class="<%#this.SelectedPageClass((int)Container.DataItem) %>">
                                    <a id="PageNumber" href="<%#this.PaginationLink((int)Container.DataItem) %>"><%#(int)Container.DataItem == 0 ? "..." : Container.DataItem.ToString() %></a>
                                </li>
                            </ItemTemplate>
                            <FooterTemplate>
                                <li>
                                    <%# this.NextLink() %>
                                </li>
                                </ul>
                            </FooterTemplate>

                        </asp:Repeater>

                    </div>

                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript" language="javascript">
    
    // Load the sort filter styles based on the selection
    jQuery(document).ready(function () {
        console.log("Loading filter style!");

        if (location.search.split('sortmode=').length > 1) {
            SortMode = location.search.split('sortmode=')[1].split('&')[0];
            isAscndingOrder = location.search.split('sortmode=')[1].split('&')[1].split('=')[1]

            if (SortMode != null) {
                // Price
                if ((SortMode.split('&')[0] == "Price") && location.search.split('sortValue=')[1] == "0") {
                    jQuery("#PriceLH").addClass("active-price");
                    jQuery("a#Price").addClass("active-price");
                }

                if ((SortMode.split('&')[0] == "Price") && location.search.split('sortValue=')[1] == "1") {
                    jQuery("#PriceHL").addClass("active-price");
                    jQuery("#Price").addClass("active-price");
                }

                // Rate
                if ((SortMode.split('&')[0] == "Stars") && location.search.split('sortValue=')[1] == "0") {
                    jQuery("#StarLH").addClass("active-price");
                    jQuery("#Stars").addClass("active-price");
                }

                if ((SortMode.split('&')[0] == "Stars") && location.search.split('sortValue=')[1] == "1") {
                    jQuery("#StarHL").addClass("active-price");
                    jQuery("#Stars").addClass("active-price");
                }

                //Recommend
                if ((SortMode.split('&')[0] == "Recommend") && location.search.split('sortValue=')[1] == "0") {
                    jQuery("#RecommendLH").addClass("active-price");
                    jQuery("#Recommend").addClass("active-price");
                }

                if ((SortMode.split('&')[0] == "Recommend") && location.search.split('sortValue=')[1] == "1") {
                    jQuery("#RecommendHL").addClass("active-price");
                    jQuery("#Recommend").addClass("active-price");
                }

                //Supplier
                if ((SortMode.split('&')[0] == "SupplierName") && location.search.split('sortValue=')[1] == "0") {
                    jQuery("#SupplierNameAsc").addClass("active-price");
                    jQuery("#SupplierName").addClass("active-price");
                }

                if ((SortMode.split('&')[0] == "SupplierName") && location.search.split('sortValue=')[1] == "1") {
                    jQuery("#SupplierNameDes").addClass("active-price");
                    jQuery("#SupplierName").addClass("active-price");
                }
                console.log("SortMode: " + SortMode + "  isAscndingOrder " + isAscndingOrder);
            }
        }

    });

</script>

<!-- Popup content for availability-->
<div id="basketPopUp" runat="server">
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-md add-basket-popup">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                </div>
                <div class="modal-body" style="padding:8px;">
                 <div class="col-xs-12">
                    <span class="room_titl"><sc:text field="Noofroom" id="NoofroomLabel" runat="server" /></span>
                    <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>

                    
                        

                        <select id="ddlNoOfRooms" class="form-control midle-content-margin-top margin-bottom-small"></select>

                        <div class="paddng_btns">

                        <div class="col-xs-4">
                            <asp:Button ID="btnAddtoCart" runat="server" Text="Add To Basket"
                                CssClass="btn-co1 pop_paddng" ClientIDMode="Static" />
                        </div>
                         

                        <div class="col-xs-4">
                            <asp:Button ID="btnCheckOut" runat="server" Visible="false" Text=""
                                CssClass="btn-co1 pop_paddng" ClientIDMode="Static" OnClick="ButtonProcessToCheckout_Click" />
                        </div>
                        <div class="col-xs-4"> 
                            <asp:Button ID="btnCancel" runat="server" Text=""
                                CssClass="btn-co1 pop_paddng" ClientIDMode="Static" aria-label="Close" data-dismiss="modal" />
                       
                            <asp:Button ID="btnTransferBooking" runat="server" Text=""
                                CssClass="btn-co1 pop_paddng" ClientIDMode="Static" OnClick="ButtonTransferBooking_Click" />
                        </div>

                        </div>

                        <asp:HiddenField ID="hiddenFieldSku" ClientIDMode="Static" runat="server" />
                        <asp:HiddenField ID="hiddenFieldRooms" ClientIDMode="Static" runat="server" />
                        <asp:HiddenField ID="hiddenFieldOfferType" ClientIDMode="Static" runat="server" />

                        <asp:HiddenField ID="hiddenFieldSupplierId" ClientIDMode="Static" runat="server" />
                        <asp:HiddenField ID="hiddenFieldCampaignItemId" ClientIDMode="Static" runat="server" />
                        <asp:HiddenField ID="hiddenFieldCurrencyId" ClientIDMode="Static" runat="server" />
                         <asp:HiddenField ID="siteurlfield" runat="server"></asp:HiddenField>

                    </div>
                    <input type="hidden" id="hdDateCounter" clientidmode="Static" runat="server" />
                </div>
                <div class="modal-footer">
                </div>
            </div>

        </div>
    </div>
</div>

<!-- Script Section for popups-->
<script type="text/javascript">

    /// Script for supplier details popup
    function OpenPopup(obj) {
        if (obj) {
            var supplierid = obj.getAttribute('data-supplierid');
            window.open("/Layouts/ProviderDetails.aspx?supplierid=" + supplierid, "List", "toolbar=no, location=no,status=yes,menubar=no,scrollbars=yes,resizable=no, width=1000,height=500,left=200,top=100");
            return false;
        }
    }

    /// Script for availability popup
    function LoadDropList(obj) {
        $('#dumbId').attr('id', 'myModal');

        if (obj) {
            var baseRooms = obj.getAttribute('data-base');
            //var offersRooms = obj.getAttribute('data-offers');
            //var offerType = obj.getAttribute('data-offertype');
            var supplierId = obj.getAttribute('data-supplierid');
            // var campaignItemId = obj.getAttribute('data-campaignitemid');
            var currencyId = obj.getAttribute('data-currencyid');
            var datecounter = obj.getAttribute('data-counter');
            var rooms = 0;

            /*if (offerType == "BaseProduct") {
                rooms = baseRooms;
            }
            else {
                rooms = offersRooms;
            }*/
            rooms = obj.getAttribute('data-rooms');
            var sku = obj.getAttribute('data-sku');
            var availabilityDate = obj.getAttribute('data-availabilityDate');

            var ddlrooms = $('#ddlNoOfRooms');
            ddlrooms.empty();
            ddlrooms.append($('<option></option>').val(-1).html("Select room"));

            if (new Date(availabilityDate).setHours(0, 0, 0, 0) >= new Date().setHours(0, 0, 0, 0) && rooms != null && rooms > 0) {
                for (i = 1; i <= rooms; i++) {
                    ddlrooms.append($('<option></option>').val(i).html(i))
                }
            }
            else {
                $('#myModal').attr('id', 'dumbId');
            }
            var hfSku = document.getElementById('hiddenFieldSku');
            var hfRooms = document.getElementById('hiddenFieldRooms');
            //var hfOfferType = document.getElementById('hiddenFieldOfferType');

            var hfSupplierId = document.getElementById('hiddenFieldSupplierId');
            //var hfCampaignItemId = document.getElementById('hiddenFieldCampaignItemId');
            var hfCurrencyId = document.getElementById('hiddenFieldCurrencyId');
            var hdDateCounter = document.getElementById('hdDateCounter');
            hdDateCounter.value = datecounter;
            if (hdDateCounter) {
                hdDateCounter.value = datecounter;
            }
            if (hfSku) {
                hfSku.value = sku;
            }
            if (hfRooms) {
                hfRooms.value = ddlrooms.val();
            }
            /*if (hfOfferType) {
                hfOfferType.value = offerType;
            }*/

            if (hfSupplierId) {
                hfSupplierId.value = supplierId;
            }
            /*if (hfCampaignItemId) {
                hfCampaignItemId.value = campaignItemId;
            }*/
            if (hfCurrencyId) {
                hfCurrencyId.value = currencyId;
            }

            IsSubmitButtonDisabled();
        }
    }

    $("#ddlNoOfRooms").change(function () {
        var ddlrooms = $('#ddlNoOfRooms');
        var hfRooms = document.getElementById('hiddenFieldRooms');
        if (hfRooms) {
            hfRooms.value = ddlrooms.val();
        }
        IsSubmitButtonDisabled();
    });

    function IsSubmitButtonDisabled() {
        if (($("#ddlNoOfRooms option:selected").index()) == 0) {
            $('#btnAddtoCart').prop('disabled', true);
           // $('#btnCheckOut').prop('disabled', true);
            $('#btnTransferBooking').prop('disabled', true);
        }
        else {
            $('#btnAddtoCart').removeAttr("disabled");
           // $('#btnCheckOut').removeAttr("disabled");
            $('#btnTransferBooking').removeAttr("disabled");
        }
    }



    function getParameterByName(name) {
        var url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }






    //$("#btnAddtoCart").on('click', function (e) {

    //    // Initialize the object, before adding data to it.
    //    //  { } is declarative shorthand for new Object()
    //    var obj = {};
    //    obj.Destination = getParameterByName("Destination");
    //    obj.NoOfAdults = getParameterByName("NoOfAdults");
    //    obj.CheckinDate = getParameterByName("CheckinDate");
    //    obj.CheckoutDate = getParameterByName("CheckOutDate");
    //    obj.NoOfChildren = getParameterByName("NoOfChildren");
    //    obj.NoOfNights = getParameterByName("NoOfNights");
    //    obj.BookingThrough = getParameterByName("");
    //    obj.CreatedBy = getParameterByName("");


    //obj.RoomAvailability = $("#hiddenFieldRooms").val();
    //obj.offertype = $("#hiddenFieldOfferType").val();
    //obj.supplierid = $("#hiddenFieldSupplierId").val();
    //obj.campaign = $("#hiddenFieldCampaignItemId").val();
    //obj.currency = $("#hiddenFieldCurrencyId").val();
    //obj.campaignitemid = $("#hiddenFieldCampaignItemId").val();

    //obj.age = $("#txtAge").val();

    //In order to proper pass a json string, you have to use function JSON.stringfy
    //var jsonData = JSON.stringify(obj);



    jQuery('#btnAddtoCart').click(function () {

        var objCommonInfo = {};
        objCommonInfo.Destination = getParameterByName("Destination");
        objCommonInfo.NoOfAdults = getParameterByName("NoOfAdults");
        objCommonInfo.CheckinDate = getParameterByName("CheckinDate");
        objCommonInfo.CheckoutDate = getParameterByName("CheckOutDate");
        objCommonInfo.NoOfChildren = getParameterByName("NoOfChildren");
        objCommonInfo.NoOfNights = getParameterByName("NoOfNights");
        objCommonInfo.DateCounter = $("#hdDateCounter").val(); // checkindate- int datecounter

        objCommonInfo.BookingThrough = getParameterByName("");
        objCommonInfo.CreatedBy = getParameterByName("");

        //var objuCommerceInfo = {};
        objCommonInfo.Sku = 'ki_5139';//$("#hiddenFieldSku").val();
        
        //objCommonInfo.varientsku =obj.getAttribute('data-variatsku');

        objCommonInfo.VarianceSku = 'do_5140';//obj.getAttribute('data-variatsku');
        //$("#hiddenFieldSku").val();
        objCommonInfo.Price = $("#hiddenFieldSku").val();
        objCommonInfo.ProductType = $("#hiddenFieldSku").val();
        objCommonInfo.RoomCount = $("#hiddenFieldRooms").val();




        //var objModel = {};
        //objModel["CommonInfo"] = objCommonInfo;
        //objModel["uCommerceInfo"] = objuCommerceInfo;

        var jsonData = JSON.stringify(objCommonInfo);


        var URLString = "/Handlers/CS/AddtoBasket.ashx";
        jQuery.ajax({
            url: URLString,
            type: 'post',
            data: jsonData,
            contenttype: "application/json;charset=utf-8",
            traditional: true,
            success: function (results) {
               
                alert("Success :" + data);

                <%--var url = document.getElementById('<%= siteurlfield.ClientID %>').value;
                var ClientUrl = url;
                window.parent.location.href = ClientUrl + "/basket";--%>
                window.location.href = "http://lbofflineportal/basket";
                
            },
            error: function (errorText) {
                alert(errorText);
            }
        });
        e.preventDefault();
    });


</script>
