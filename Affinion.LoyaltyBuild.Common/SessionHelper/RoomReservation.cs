﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           RoomReservation.cs
/// Sub-system/Module:     Common(VS project name)
/// Description:           Contains the data class for link field functions
/// </summary>
#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#endregion
namespace Affinion.LoyaltyBuild.Common.SessionHelper
{
    /// <summary>
    /// Class to store the guest accoommodation
    /// </summary>
    public class RoomReservation
    {
        //public string RoomReservationId { get; set; }
        public string OrderLineId { get; set; }
        public int NoOfGuest { get; set; }
        public string GuestName { get; set; }
        public int NoOfAdults { get; set; }
        public int NoOfChildren { get; set; }
        public string SpecialRequest { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime CheckinDate { get; set; }
        
        public DateTime CheckOutDate { get; set; }
    }

    /// <summary>
    /// Age related information will be used this class
    /// </summary>
    public class RoomReservationAgeInfo
    {
        //public int ReservationAgeId { get; set; }
        //public string RoomReservationId { get; set; }
        public int OrderLineId { get; set; }
        public int Age { get; set; }
    }
}
