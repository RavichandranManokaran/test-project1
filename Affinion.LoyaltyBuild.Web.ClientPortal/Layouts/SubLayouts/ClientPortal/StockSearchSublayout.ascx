﻿<%@ control language="C#" autoeventwireup="true" codebehind="StockSearchSublayout.ascx.cs" inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal.StockSearchSublayout" %>
<%@ register tagprefix="sc" namespace="Sitecore.Web.UI.WebControls" assembly="Sitecore.Kernel" %>
<%@ import namespace="Affinion.LoyaltyBuild.Common.Utilities" %>
<%@ import namespace="Sitecore.Data.Items" %>
<script type="text/javascript" src="/Resources/scripts/affinionCustom.js"></script>
<script type="text/javascript" src="/Resources/scripts/affinionSearch.js"></script>
<link href="../../../Resources/styles/affinion.custom.css" rel="stylesheet" />


<% if (this.useAjax)
   {
%>


<script type="text/javascript">

    jQuery(function () {
        jQuery("#collapse-button").click(function () {
            jQuery("#show-hide").toggle();
        });
    })

    jQuery("#mainform").submit(function (event) {

        event.preventDefault();

        if (ValidateInputs()) {
            var parameters =
            {
                Destination: jQuery("#Destination").val(),
                CheckinDate: jQuery("#CheckinDate").val(),
                CheckoutDate: jQuery("#CheckoutDate").val(),
                NoOfRooms: jQuery("#NoOfRooms").val(),
                NoOfAdults: jQuery("#NoOfAdults").val(),
                NoOfChildren: jQuery("#NoOfChildren").val(),
                OfferGroup: jQuery("#OfferGroup").val()
            };

            jQuery.ajax({

                url: "<%=this.submitUrl %>",
                data: parameters,
                context: document.body

            }).done(function (data) {

                if (typeof RenderResults == 'function') {
                    RenderResults(data);
                }

            });
            }

    });

        var ValidateInputs = function () {
            var result = true;

            var destination = jQuery("#Destination").val();
            var checkinDate = jQuery("#CheckinDate").val();
            // var checkoutDate = jQuery("#CheckoutDate").val();

            if (destination == "") {
                result = false;
                alert("Please fill required fields.");
            }
            return result;
        }
</script>

<%       
   }
%>
<asp:panel id="StockSearchPanel" runat="server" defaultbutton="Submit" cssclass="stockSearchPanel">
    <div class="col-lg-12 visible-sm visible-xs no-padding">
        <div class="book-detail">
            <a id="collapse-button" href="#">
                <span class="fa fa-calendar"></span>
                <sc:text field="MobileVersionSearchLable" id="MobileVersionSearchLable" runat="server" />
            </a>
        </div>
    </div>
    <div class="<%=this.cssClass %> width-full" id="show-hide">
        <div class="search-inner width-full">
            <asp:Label ID="DatasourceText" runat="server">Set datasource to view values</asp:Label>

            <h3 class="no-margin">
                <sc:text field="TitleLabel" id="TitleLabel" runat="server" />
            </h3>

            <div class="booking-top width-full divider"></div>

            <div class="col-xs-12">
                <h5>
                    <sc:text field="DestinationLabel" id="DestinationLabel" runat="server" />
                </h5>
                <asp:TextBox ID="Destination" CssClass="form-control" Text="" ClientIDMode="Static" runat="server" />
                <asp:hiddenfield id="CategoryId" value="" ClientIDMode="Static" runat="server"/>
                <asp:hiddenfield id="DestinationType" value="" ClientIDMode="Static" runat="server"/>
                <asp:DropDownList ID="DestinationDropDown" CssClass="form-control" ClientIDMode="Static" runat="server" Visible="False"></asp:DropDownList>
                <asp:RegularExpressionValidator ID="revDestination" runat="server" ControlToValidate="Destination" ValidationExpression="^[^<>\\&quot;/;`%?0-9]*$" ForeColor="Red" Display="Dynamic" ValidationGroup="submitBtnValidation" >
                      <sc:text id="txtDestinationRegularText" runat="server" />
                </asp:RegularExpressionValidator>
                <%--Start LWD-1022--%>
                <div id="divErrorMsg" style="color:red;font-weight:400">Please enter the value in Destination/hotel name.</div>
                <%--End LWD-1022--%>
            </div>

            <div class="ondate">
                <div class="col-xs-6">
                    <h5>
                        <sc:text field="CheckinDateLabel" id="CheckinDateLabel" runat="server" />
                    </h5>
                    <div class="width-full">
                        <span class="input-group-btn">
                            <%--<button class="btn btn-default glyphicon glyphicon-calendar calander-icon" type="button"></button>--%>
                        </span>
                        <asp:TextBox ID="CheckinDate" CssClass="form-control datepicker width-85 pull-right" Text="" ClientIDMode="Static" runat="server" data-provide="datepicker" />
                    </div>
                </div>

                <div class="col-xs-6">
                    <h5>
                        <%--<sc:Text Field="CheckoutDateLabel" ID="CheckoutDateLabel" runat="server" />--%>
                        <sc:text field="NoOfNightsLabel" id="CheckoutDateLabel" runat="server" />
                    </h5>
                    <div class="width-full">
                        <%--<span class="input-group-btn">--%>
                        <%--<button class="btn btn-default glyphicon glyphicon-calendar calander-icon" type="button"></button>--%>
                        <%--</span>
                        <asp:TextBox ID="CheckoutDate" CssClass="form-control datepicker width-85 pull-right" Text="" ClientIDMode="Static"  runat="server" data-provide="datepicker" />--%>
                        <asp:HiddenField ID="CheckoutDate" runat="server" Value="" />
                        <div role="group" aria-label="...">
                            <asp:DropDownList CssClass="form-control" ID="NoOfNights" ClientIDMode="Static" runat="server"></asp:DropDownList>
                            <asp:RequiredFieldValidator ID="rfvNumberOfNights" runat="server" ForeColor="Red" ControlToValidate="NoOfNights" Display="Dynamic" ValidationGroup="submitBtnValidation">
                                    <sc:text id="numberOfNightsRequiredText" runat="server" visible="false" />
                            </asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12" runat="server" id="FlexibleDatePanel">
                <div class="checkbox">
                    <label>
                        <asp:CheckBox ID="DateSelectionCheckBox" Text="I don't have specific dates yet" ClientIDMode="Static" runat="server" />
                    </label>
                </div>
            </div>
            <div class="booking-deatail-order-room width-full inline-block" runat="server" id="ChildBedSelectionDiv">
                <div class="col-xs-12 col-md-12   no-padding" id="roomPanel">

                    <div class="col-xs-12 col-md-6 col-btn-list no-padding">
                        <button type="button" class=" room-btn btn collapsed " data-toggle="collapse" data-target="#room-availability">Show/ Hide Rooms</button>
                    </div>
                </div>
                <div class="roomOn room-panel panel-padding" id="room-availability">
                    <div class="roomOn col-xs-12 col-md-12 roomResults no-padding-left">

                        <span class="roomOn col-xs-12 col-md-4 accent50-f font-4 font-bold-600 padding-top5">No of Rooms</span>
                        <div role="group" aria-label="..." class="col-xs-12 col-md-2 roomOn">
                            <asp:DropDownList CssClass="form-control roomOn" ID="ChildBedSelectionNoOfRoomsDropDownList" ClientIDMode="Static" runat="server"></asp:DropDownList>
                        </div>

                    </div>
                    <asp:Panel ID="RoomDetailsPanel" runat="server">
                    </asp:Panel>

                </div>

            </div>
            <div class="booking-deatail-order-room width-full inline-block" id="RoomSelectionDiv" runat="server">
                <div class="col-xs-12 col-md-4">
                    <h5>
                        <sc:text field="RoomsLabel" id="RoomsLabel" runat="server" />
                    </h5>
                    <div role="group" aria-label="...">
                        <asp:DropDownList CssClass="form-control roomOn" ID="NoOfRooms" ClientIDMode="Static" runat="server"></asp:DropDownList>
                    </div>

                </div>
                <div class="col-xs-12 col-md-4">
                    <h5>
                        <sc:text field="AdultsLabel" id="AdultsLabel" runat="server" />
                    </h5>
                    <div role="group" aria-label="...">
                        <asp:DropDownList CssClass="form-control" ID="NoOfAdults" ClientIDMode="Static" runat="server"></asp:DropDownList>
                    </div>

                </div>
                <div class="col-xs-12 col-md-4">
                    <h5>
                        <sc:text field="ChildrenLabel" id="ChildrenLabel" runat="server" />
                    </h5>
                    <div role="group" aria-label="...">
                        <asp:DropDownList CssClass="form-control" ID="NoOfChildren" ClientIDMode="Static" runat="server"></asp:DropDownList>
                    </div>

                </div>
                <div class="col-xs-12 no-padding ageGroupOuter" style="display: none">
                    <div class="age-group">
                        <div class="col-xs-12">
                            <span>
                                <sc:text field="ChildrenAgeTitleLabel" id="ChildrenAgeTitleLabel" runat="server" />
                            </span>
                        </div>
                        <asp:Panel ID="ChildAgePanel" runat="server">
                        </asp:Panel>

                    </div>
                </div>

                <div class="col-xs-2">
                </div>
            </div>
            <div runat="server" id="ErrorPanel" class="col-xs-12 error-msg" visible="true">
                <sc:text id="txtSelectChildAgeMessage" runat="server" visible="false" />
            </div>

            <div class="col-xs-12" id="CountryPanel" runat="server" Visible="False">

                <h5>
                    <sc:text field="CountryLabel" id="CountryLabel" runat="server" />
                </h5>
                <asp:DropDownList CssClass="form-control styled-select" ID="CountryDropDown" ClientIDMode="Static" runat="server" Visible="False"></asp:DropDownList>
            </div>

            <div class="col-xs-12 hide">

                <h5>
                    <sc:text field="OfferGroupLabel" id="OfferGroupLabel" runat="server" />
                </h5>
                <asp:DropDownList CssClass="form-control styled-select" ID="OfferGroup" ClientIDMode="Static" runat="server"></asp:DropDownList>
            </div>
            <div id="ServerSideErrorPanel" runat="server" class="col-xs-12 error-msg"></div>
            <div id="ClientSideErrorPanel" class="col-xs-12 error-msg">
            </div>
            <div class="col-xs-12">

                <asp:Button ID="Submit" runat="server" Text="" class="btn btn-primary booking-margin-top float-right btn-bg" OnClientClick="javascript:SubmitSearchButton();" ValidationGroup="submitBtnValidation" />
            </div>

        </div>
    </div>
</asp:panel>
<script type="text/javascript">
    <%--<%=this.Messages %>--%>

    jQuery(document).ready(function () {



        //Start LWD-1022//
        $("#divErrorMsg").hide();
        //End LWD-1022//

        var searchBox = jQuery('#Destination');

        searchBox.autocomplete({
            source: function (request, response) {
                $("#CategoryId").val("");
                $("#DestinationType").val("");
                // LWD LWD-336 fixes
                if (searchBox.val().length > 1) {

                    jQuery.ajax({
                        type: "POST",
                        url: "/webservices/autocomplete.asmx/GetSuggestions",
                        data: JSON.stringify({ clientId: ClientId, term: searchBox.val() }),
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            var d = eval(data);
                            response($.map(eval(d.d), function (item) {
                                return {
                                    label: item.Text,
                                    value: item.Text,
                                    itemId: item.ItemId,
                                    destinationType: item.Type
                                }
                            }));
                        },
                        error: function () {
                            response([]);
                        }
                    });
                }
            },
            select: function (event, ui) {
                event.preventDefault();
                searchBox.val(ui.item.label);
                $("#CategoryId").val(ui.item.itemId);
                $("#DestinationType").val(ui.item.destinationType);
                return false;
            }
        });



        jQuery(function () {
            jQuery("#CheckinDate").datepicker({
                defaultDate: "+1w",
                dateFormat: "<%=DateTimeHelper.DateFormatJavaScript%>",
                minDate: "<%=calendarStartDate%>",
                numberOfMonths: 1,
                showOn: "both",
                buttonImage: "images/calendar.png",
                buttonImageOnly: false,
                buttonClass: "glyphicon-calendar"


                //dateFormat: 'dd/mm/yy'
                //onClose: function (selectedDate) {

                //    var checkoutDate = new Date(selectedDate);
                //    if (checkoutDate.toString() != "Invalid Date")
                //    {
                //        checkoutDate.setDate(checkoutDate.getDate() + 1);
                //    }

                //    jQuery("#CheckoutDate").datepicker("option", "minDate", checkoutDate);
                //}
            }).next(".ui-datepicker-trigger").addClass("btn btn-default glyphicon glyphicon-calendar calander-icon pull-right");
            //jQuery("#CheckoutDate").datepicker({
            //    defaultDate: "+1w",
            //    minDate: 0,
            //    numberOfMonths: 1,
            //    showOn: "both",
            //    buttonImage: "datepicker/images/calendar.gif",
            //    buttonImageOnly: false,
            //    buttonClass: "glyphicon-calendar",
            //dateFormat: 'dd/mm/yy',
            //    onClose: function (selectedDate) {
            //        //jQuery("#CheckinDate").datepicker("option", "maxDate", selectedDate);
            //    }
            //});

            jQuery("form").submit(function (e) {

                if (SubmitClientClick() == false) {
                    e.preventDefault();
                }

            });
        });

        jQuery('#DateSelectionCheckBox').change(function () {
            if (this.checked)
                $('.ondate').addClass("greyout");
            else
                $('.ondate').removeClass("greyout");
        });

        if ($('#DateSelectionCheckBox').is(':checked')) {
            $('.ondate').addClass("greyout");
        }
        else
            $('.ondate').removeClass("greyout");
    });



    function SubmitClientClick() {

        jQuery("#ErrorPanel").hide();
        var isValid = true;
        var noOfChildren = jQuery('#NoOfChildren').val();
        for (var i = 0; i < noOfChildren; i++) {
            var age = jQuery('#AgeDropdownList' + (i + 1)).val();

            if (age === 'Select') {

                jQuery("#ErrorPanel").show();
                return false;
            }
        }

        if ($('#room-availability').length)         // use this if you are using id to check
        {
            return validateFieldsForSubmit();
        }

        return true;
    }
    jQuery('#NoOfChildren').change(function () {
        var childCount = "";
        jQuery("select option:selected").each(function () {
            childCount = $('#NoOfChildren').val();
            if (childCount >= '0')
                jQuery("#ErrorPanel").hide();
        });

    })



    function SubmitSearchButton() {
        //Start LWD-1022//
        $("#divErrorMsg").hide();

        if ($("#Destination").val() != "") {
            if (SubmitClientClick()) {
                if (Page_ClientValidate('submitBtnValidation')) {
                    __doPostBack('Submit');
                }
            }
        }
        else {
            $("#divErrorMsg").show();
            event.preventDefault();
        }
        //End LWD-1022//
    }

    // Makes the date selection field readonly
    jQuery(document).ready(function () {
        jQuery('#CheckinDate').attr('readonly', true);
    });

    //Start LWD-1022//
    $("#Destination").on("keypress keyup blur", function (event) {
        if ($("#Destination").val() != "") {
            $("#divErrorMsg").hide();
        }
        else {
            $("#divErrorMsg").show();
        }
    });
    //End LWD-1022//

</script>
