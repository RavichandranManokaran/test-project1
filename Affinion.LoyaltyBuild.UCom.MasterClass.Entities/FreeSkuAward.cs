﻿
using Affinion.LoyaltyBuild.UCom.Common.Constants;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;
using UCommerce.Runtime;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class FreeSkuAward : Award, IEntity
    {
        // Fields
        private int? _hashCode;
        // Methods
        public new static IQueryable<FreeSkuAward> All()
        {
            return GetRepo().Select();
        }

        public override void Apply(PurchaseOrder order, IList<OrderLine> orderLines)
        {
            bool skuAdded = false;
            List<string> data = this.FreeSkuType.Split('|').Where(y => !string.IsNullOrEmpty(y)).ToList();
            foreach (OrderLine line in orderLines)
            {
                foreach (var item in line.OrderProperties)
                {
                    int index = data.FindIndex(x => x.Equals(item.Key));
                    if (index != -1)
                    {
                        OrderLine oldOrderLine = OrderLine.All().Where(x => x.PurchaseOrder.OrderId == order.OrderId
                         && x.VariantSku.Equals(item.Value) && x.OrderProperties.Any(y => y.Key == OrderPropertyConstants.RefOrderId && y.Value == line.OrderLineId.ToString())).FirstOrDefault();
                        if (oldOrderLine != null)
                        {
                            var discount = new Discount
                            {
                                CampaignName = CampaignItem.Campaign.Name,
                                CampaignItemName = CampaignItem.Name,
                                Description = Name,
                                AmountOffTotal = oldOrderLine.VAT + oldOrderLine.Price
                            };
                            order.AddDiscount(discount);
                            if (!oldOrderLine.OrderProperties.Any(x => x.Key == OrderPropertyConstants.FreeSku))
                            {
                                oldOrderLine.AddOrderProperty(new OrderProperty() { Key = OrderPropertyConstants.FreeSku, Value = "true" });
                            }
                            oldOrderLine.AddDiscount(discount);
                            data[index] = string.Empty;
                            skuAdded = true;
                        }
                    }
                }
                if (data.Any(x => !string.IsNullOrEmpty(x)))
                {
                    int categoryId = ProductLibrary.GetCategoryIdFromSku(line.Sku);
                    //TODO : Change Code For AddOn Product
                    List<Product> addonProduct = ProductLibrary.GetAddonProudct(categoryId);
                    if (!addonProduct.Any(x => x.VariantSku.Equals(line.VariantSku) && x.Sku.Equals(line.Sku)))
                    {
                        foreach (var item in addonProduct)
                        {
                            ProductProperty productProperty = item[ProductDefinationVairantConstant.FoodType];
                            if (productProperty != null && !string.IsNullOrEmpty(productProperty.Value))
                            {
                                int index = data.FindIndex(y => y.Equals(productProperty.Value));
                                var priceGroup = SiteContext.Current.CatalogContext.CurrentPriceGroup;
                                var generatedOrderline = order.AddProduct(priceGroup, item, quantity: 1, addToExistingLine: false);

                                line.AddOrderProperty(new OrderProperty() { Key = productProperty.Value, Value = item.VariantSku });

                                // Indicate the order line is generated.

                                // Now add a discount to new orderline to
                                // make it free.
                                var discount = new Discount
                                {
                                    CampaignName = CampaignItem.Campaign.Name,
                                    CampaignItemName = CampaignItem.Name,
                                    Description = Name,
                                    AmountOffTotal = generatedOrderline.Price + generatedOrderline.VAT
                                };
                                generatedOrderline.AddOrderProperty(new OrderProperty() { Key = OrderPropertyConstants.FreeSku, Value = "true" });
                                generatedOrderline.AddOrderProperty(new OrderProperty() { Key = OrderPropertyConstants.RefOrderId, Value = line.OrderLineId.ToString() });
                                generatedOrderline.AddDiscount(discount);
                                order.AddDiscount(discount);
                                data[index] = string.Empty;
                                skuAdded = true;
                            }
                        }
                    }
                }
                if (skuAdded)
                {
                    if (!line.OrderProperties.Any(x => x.Key == OrderPropertyConstants.FreeSkuApplied))
                    {
                        line.AddOrderProperty(new OrderProperty() { Key = OrderPropertyConstants.FreeSkuApplied, Value = "true" });
                    }
                }
            }


        }

        private bool FreeSKUAwarded(OrderLine orderLines)
        {
            return orderLines.OrderProperties.Any(y => (y.Key == OrderPropertyConstants.FreeSku || y.Key == OrderPropertyConstants.FreeSkuApplied) && y.Value == "true");
        }

        public override void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<FreeSkuAward, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            FreeSkuAward objA = obj as FreeSkuAward;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<FreeSkuAward, bool>> expression)
        {
            return All().Any<FreeSkuAward>(expression);
        }

        public static IList<FreeSkuAward> Find(Expression<Func<FreeSkuAward, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<FreeSkuAward>();
        }

        public static FreeSkuAward FirstOrDefault(Expression<Func<FreeSkuAward, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<FreeSkuAward>();
        }

        public new static FreeSkuAward Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<FreeSkuAward> GetRepo()
        {
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<FreeSkuAward>>();
        }


        public override void Save()
        {
            GetRepo().Save(this);
        }

        public static FreeSkuAward SingleOrDefault(Expression<Func<FreeSkuAward, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties
        public virtual string FreeSkuType { get; set; }

        public virtual int FreeSkuAwardId { get; protected set; }

        public override int Id
        {
            get
            {
                return this.FreeSkuAwardId;
            }
        }
    }




}
