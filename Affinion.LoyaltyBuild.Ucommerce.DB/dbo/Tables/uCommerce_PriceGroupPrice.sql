﻿CREATE TABLE [dbo].[uCommerce_PriceGroupPrice] (
    [PriceGroupPriceId] INT   IDENTITY (1, 1) NOT NULL,
    [ProductId]         INT   NOT NULL,
    [Price]             MONEY NULL,
    [DiscountPrice]     MONEY NULL,
    [PriceGroupId]      INT   NOT NULL,
    CONSTRAINT [uCommerce_PK_PriceGroupPrice] PRIMARY KEY CLUSTERED ([PriceGroupPriceId] ASC),
    CONSTRAINT [uCommerce_FK_PriceGroupPrice_PriceGroup] FOREIGN KEY ([PriceGroupId]) REFERENCES [dbo].[uCommerce_PriceGroup] ([PriceGroupId]),
    CONSTRAINT [uCommerce_FK_PriceGroupPrice_Product] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[uCommerce_Product] ([ProductId]),
    CONSTRAINT [Unique_ProductId_PriceGroupId_PriceGroupPrice] UNIQUE NONCLUSTERED ([ProductId] ASC, [PriceGroupId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_uCommerce_PriceGroupPrice_ProductId]
    ON [dbo].[uCommerce_PriceGroupPrice]([ProductId] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_uCommerce_PriceGroupPrice_PriceGroupId]
    ON [dbo].[uCommerce_PriceGroupPrice]([PriceGroupId] ASC)
    INCLUDE([Price]);

