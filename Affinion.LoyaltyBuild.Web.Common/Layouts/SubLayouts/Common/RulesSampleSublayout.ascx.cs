﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           RulesSampleSublayout.aspx.cs
/// Sub-system/Module:     Web.Common(VS project name)
/// Description:           Sublayout for rules pages
/// </summary>

namespace Affinion.LoyaltyBuild.Web.Common.Layouts.SubLayouts.Common
{
    using System;

    public partial class RulesSampleSublayout : System.Web.UI.UserControl
    {
    }
}