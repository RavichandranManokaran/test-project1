﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Offer
{
    public partial class ViewPrice : System.Web.UI.UserControl
    {
        public CampaginItemDateRange ArriValDate { get; set; }
        public CampaginItemDateRange ResDate { get; set; }

        public List<OfferPrice> OfferPrice { get; set; }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.DataBind();
            }
        }
    }
}