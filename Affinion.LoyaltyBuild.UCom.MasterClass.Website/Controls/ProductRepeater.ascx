﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductRepeater.ascx.cs" Inherits="Affinion.LoyaltyBuild.UCom.MasterClass.Website.Controls.ProductRepeater" %>
<%@ Register Src="~/Controls/ProductDetails.ascx" TagPrefix="uc1" TagName="ProductDetails" %>

<asp:Repeater runat="server" ID="ProductRepeaterInternal" ItemType="UCommerce.EntitiesV2.Product">
    <ItemTemplate>
        <uc1:ProductDetails Product="<%# Item %>" runat="server"></uc1:ProductDetails>
    </ItemTemplate>
</asp:Repeater>