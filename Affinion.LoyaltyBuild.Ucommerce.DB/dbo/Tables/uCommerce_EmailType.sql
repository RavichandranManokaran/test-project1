﻿CREATE TABLE [dbo].[uCommerce_EmailType] (
    [EmailTypeId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (128) NOT NULL,
    [Description] NTEXT          NULL,
    [CreatedOn]   DATETIME       NOT NULL,
    [CreatedBy]   NVARCHAR (50)  NOT NULL,
    [ModifiedOn]  DATETIME       NOT NULL,
    [ModifiedBy]  NVARCHAR (50)  NOT NULL,
    [Deleted]     BIT            CONSTRAINT [uCommerce_DF_EmailType_Deleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [uCommerce_PK_EmailType] PRIMARY KEY CLUSTERED ([EmailTypeId] ASC)
);

