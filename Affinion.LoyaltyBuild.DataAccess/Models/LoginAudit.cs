﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.DataAccess.Models
{
    [Table("LoginAudit")]
    public class LoginAudit
    {
        /// <summary>
        /// User name
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int  ID { get; set; }
        
        /// <summary>
        /// User name
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Is Login Sucess
        /// </summary>
        public bool IsLoginSucess{ get; set; }

        /// <summary>
        /// Logged In Date
        /// </summary>
        public DateTime LoggedInDate { get; set; }
    }
}
