﻿namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Model
{
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class ReturnStatus
    {
        public string Success { get; set; }
        public string Exception { get; set; }
    }
}
