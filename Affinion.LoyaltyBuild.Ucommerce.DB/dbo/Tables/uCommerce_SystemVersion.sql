﻿CREATE TABLE [dbo].[uCommerce_SystemVersion] (
    [SystemVersionId] INT            IDENTITY (1, 1) NOT NULL,
    [SchemaVersion]   INT            NOT NULL,
    [AssemblyVersion] NVARCHAR (MAX) DEFAULT ('0.0.0.00000') NOT NULL,
    CONSTRAINT [uCommerce_PK_SystemVersion] PRIMARY KEY CLUSTERED ([SystemVersionId] ASC)
);



