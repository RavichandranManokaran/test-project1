﻿CREATE TABLE [dbo].[uCommerce_QuantityTarget] (
    [QuantityTargetId] INT NOT NULL,
    [MinQuantity]      INT CONSTRAINT [DF_uCommerce_Quantity_Target_MinAmount] DEFAULT ((0)) NOT NULL,
    [TargetOrderLine]  BIT DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_uCommerce_QuantityTarget] PRIMARY KEY CLUSTERED ([QuantityTargetId] ASC)
);

