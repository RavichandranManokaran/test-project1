﻿using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Handler.Offer
{
    /// <summary>
    /// Summary description for UnsubscribeOffer
    /// </summary>
    public class UnsubscribeOffer : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                // context.Response.ContentType = "text/plain";
                string offerId = context.Request.Form["offerId"];
                string supplierInviteId = context.Request.Form["supplierInviteId"];
                string reason = context.Request.Form["Reason"];
                AvailabilityHelper staticDataForRoom = new AvailabilityHelper(context);
                int campaignItemId = Convert.ToInt32(offerId);
                bool unSubscribe = staticDataForRoom.UnsubscribeForOffer(Convert.ToInt32(campaignItemId), Convert.ToInt32(supplierInviteId), reason);
                if (unSubscribe)
                    context.Response.Write("SUCCESS");
                else
                    context.Response.Write("FAIL");
            }
            catch (Exception er)
            {
                context.Response.Write("ERROR" + er.Message.ToString());
                Sitecore.Diagnostics.Log.Error(er.Message, new object());
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}