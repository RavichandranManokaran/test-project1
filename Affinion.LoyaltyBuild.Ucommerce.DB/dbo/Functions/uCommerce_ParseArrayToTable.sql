﻿
CREATE FUNCTION [dbo].[uCommerce_ParseArrayToTable]
(
	@Array NVARCHAR(MAX), 		-- String to parse (ie: '1,2,6,4,12')
	@Separator CHAR(1) = ',',	-- Seperator to use, default to ',' (comma)
	@ReturnAsNumeric BIT = 0	-- If true, returns numeric values in stead of varchars
)
RETURNS @table TABLE
(
	[Id] INT IDENTITY(1, 1),
	stringvalue NVARCHAR(MAX),
	numericvalue INT
)
AS
BEGIN

	DECLARE @Separator_Position int 		-- This is used to locate each separator character
	DECLARE @Array_Value NVARCHAR(MAX) 	-- This holds each array value as it is returned

	-- For my loop to work I need an extra separator at the end.  I always look to the
	-- left of the separator character for each array value
	SET @Array = @Array + @Separator

	WHILE Patindex('%' + @Separator + '%' , @Array) <> 0 
	BEGIN			
		-- Patindex matches the a pattern against a string
		SELECT @Separator_position =  Patindex('%' + @Separator + '%' , @Array)
		SELECT @Array_value = Left(@Array, @Separator_Position - 1)

		-- This is where you process the values passed.
		-- Replace this select statement with your processing
		-- @Array_Value holds the value of this element of the array
		-- If the value is not numeric, insert as a string (using '')
		IF @ReturnAsNumeric = 1
			INSERT @table (numericvalue) VALUES (CONVERT(INT, @Array_Value))
		ELSE
			INSERT @table (stringvalue) VALUES (CONVERT(NVARCHAR(MAX), @Array_Value))

		-- This replaces what we just processed with an empty string
		SELECT @Array = Stuff(@Array, 1, @Separator_Position, '')

	END

	RETURN

END