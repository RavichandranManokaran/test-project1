﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/

namespace Affinion.LoyaltyBuild.Api.Booking.Data.Payment
{
    /// <summary>
    /// Entity containing Card payment detail implementing PaymentDetail
    /// </summary>
    public class CardPaymentDetail : PaymentDetail
    {
        /// <summary>
        /// Processing fee
        /// </summary>
        public decimal CommissionAmount { get; set; }

        /// <summary>
        /// Transferring fee
        /// </summary>
        public decimal ProcessingFee { get; set; }

        /// <summary>
        /// Provider Fee
        /// </summary>
        public decimal ProviderAmount { get; set; }

    }
}
