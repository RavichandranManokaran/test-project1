﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BBProviderDetails.aspx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.BBProviderDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Hotel Details</title>
    
    <script type="text/javascript" src="/Resources/scripts/jquery.min.js"></script>    
    <script type="text/javascript" src="/Resources/bootstrap_assets/javascripts/bootstrap.min.js"></script>
    <%--<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false"> </script>--%>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&sensor=true"></script>

    <link rel="stylesheet" href="/Resources/styles/responsiveNav.css" type="text/css" />
    <link rel="stylesheet" href="/Resources/styles/bootstrap.min.css" />
    <link rel="stylesheet" href="/Resources/styles/affinionOfflinePortal.min.css" />
    <link rel="stylesheet" href="/Resources/styles/font-awesome.min.css" />
    <link href="/Resources/styles/theme1/cstheme.css" rel="stylesheet" />
    <link href="/Resources/styles/offlineportal.css" rel="stylesheet" />

</head>
<body>
    <form id="bbproviderform" runat="server">
    <div>
      <div>
                <div class="modal-header">
                    <h4 class="modal-title">
                        <asp:Literal ID="litProviderNameHead" runat="server"></asp:Literal>
                    </h4>
                </div>

                <div class="modal-body">
                <div>
                    <div>                        
                        <%--Navigations--%>
                        <div class="row">
                            <div class="col-xs-12 margin-common-inner-box">
                                <ul class="info-top-link list-group">
                                    <li class="display-block-val pull-left"><a href="#facilities" class="list-group-item alert-info" title="Facilities">Facilities </a></li>
                                    <li class="display-block-val pull-left"><a href="#goodToKnow" class="list-group-item alert-info" title="GoodToKnow">GoodToKnow</a> </li>
                                    <li class="display-block-val pull-left"><a href="#checkInCheckOutTime" class="list-group-item alert-info" title="CheckInCheckOutTime">Check In CheckOut Time</a> </li>                                    
                                </ul>
                            </div>
                        </div>
                        
                        <%--Contents--%>
                        <div class="row">
                            <div class="col-md-12 no-padding">
                                <div class="col-xs-12 col-md-7">
                                    <div class="hotel-image">
                                        <asp:Image ID="imageIntroduction" runat="server" cssclass="img-responsive img-thumbnail" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-5">
                                    <div class="hotel-search-box-detail">
                                        <div class="hotel-name-detail">
                                            <h2 class="no-margin pull-left">
                                                <asp:Literal ID="litProviderName" runat="server"></asp:Literal>
                                            </h2>

                                            <div class="star-rank pull-left padding-left-small padding-top-small">
                                                <asp:Repeater ID="rptBBStarRepeater" runat="server">
                                                    <ItemTemplate>
                                                        <span class="fa fa-star"></span>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>

                                            <div class="hotel-name-detail-bottom width-full pull-left">
                                                <p style="color: #3c763d;">
                                                    <asp:Literal ID="litAddress" runat="server"></asp:Literal>
                                                </p>
                                            </div>
                                        </div>

                                        <ul class="event">
                                            <asp:Repeater ID="rptBBFacilities" runat="server" OnItemDataBound="rptBBFacilities_ItemDataBound">
                                                <ItemTemplate>
                                                    <li>
                                                        <asp:Image ID="imgFac" runat="server" />
                                                    </li>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </ul>
                                    </div>
                                    <div class="col-xs-12 no-padding">
                                        <div class="col-xs-12">
                                            <div class="info-map">
                                                <a href="#">
                                                    <div id="bbmap_canvas" class="gmap gmap-off"></div>
                                                </a>
                                            </div>
                                       </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12">

                                <div class="content_bg_inner_box alert-info">
                                    <p>
                                        <asp:Literal ID="litOverview" runat="server"></asp:Literal>
                                    </p>
                                </div>                                
                            </div>

                            <div class="col-xs-12">
                                <div class="detail-hotel margin-bottom-15">

                                    <div class="content_bg_inner_box light-box" id="bbfacilities">
                                        <h2>Facilities </h2>
                                        <p>                                            
                                            <asp:Literal ID="litFacility" runat="server" />
                                        </p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer width-full inline-block">
                <div class="col-xs-12">
                    <asp:Button runat="server" Text="Close" CssClass="btn btn-default" OnClientClick="closeWin()" UseSubmitBehavior="false" />
                </div>
            </div>
        </div>
    </div>
        <script type="text/javascript">

            //Store reference to the google map object
            var map;

            function initialize(location) {

                //Set the required Geo Information
                var latitude = '<%=latitude%>';
        var lognitude = '<%=longitude%>';

        // Define the coordinates as a Google Maps LatLng Object
        var coords = new google.maps.LatLng(latitude, lognitude);

        //Map initialization variables
        var mapOptions = {
            center: new google.maps.LatLng(latitude, lognitude),
            zoom: 9,
        };

        map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

        // Place the initial marker
        var marker = new google.maps.Marker({
            position: coords,
            draggable: true,
            map: map,
        });
    }

    $(document).ready(function () {
        if (navigator.geolocation) {
            // Call getCurrentPosition with success and failure callbacks
            initialize();
        }
        else {
            alert("Sorry, your browser does not support geolocation services.");
        }
    });

    function closeWin() {
        window.close();
        return false;
    }

</script>
    </form>
</body>
</html>
