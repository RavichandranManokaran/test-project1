﻿CREATE TABLE [dbo].[AccessControl] (
    [Id]          UNIQUEIDENTIFIER NOT NULL,
    [EntityId]    NVARCHAR (255)   NOT NULL,
    [TypeName]    VARCHAR (255)    NOT NULL,
    [AccessRules] NVARCHAR (MAX)   NOT NULL,
    [DAC_Index]   INT              NULL,
    CONSTRAINT [PK_AccessControl] PRIMARY KEY NONCLUSTERED ([Id] ASC)
);


GO
CREATE CLUSTERED INDEX [DAC_Clustered]
    ON [dbo].[AccessControl]([DAC_Index] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_AccessControl]
    ON [dbo].[AccessControl]([EntityId] ASC, [TypeName] ASC);

