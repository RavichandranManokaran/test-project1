﻿using Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration.SiteUI.Base;
using System;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Single_Item
{
    public partial class SmallStickyNote : SitecoreSingleItemUserControl
    {
        private void Page_Load(object sender, EventArgs e)
        {
            WriteAlertsIfNeeded();
            HideIfNoVersionUnlessPageEditing();

            if (!IsDataSourceItemNull)
            {
                pnlStickyNote.CssClass = String.Format("{0}-sidestick sidebar-stick", DataSourceItem.Fields["Note Type"]);
            }
            else
            {
                pnlStickyNote.Visible = false;
            }
        }
    }
}