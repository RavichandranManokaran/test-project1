﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Helper
{ /// <summary> 
    /// News section Fields 
    /// </summary> 
    public class NewsFields
    {
        /// <summary> 
        /// Title of news 
        /// </summary> 
        public string NewsTitle { get; set; }
        /// <summary> 
        /// Description of news 
        /// </summary> 
        public string NewsDescription { get; set; }

        /// <summary> 
        /// priority of news 
        /// </summary> 
        public int NewsPriority { get; set; }

        /// <summary> 
        /// from date 
        /// </summary> 
        public DateTime? FromDate { get; set; }

        /// <summary> 
        /// to date 
        /// </summary> 
        public DateTime? ToDate { get; set; }

        /// <summary> 
        /// Published Date
        /// </summary> 
        public string PublishDate { get; set; }
    } 



}