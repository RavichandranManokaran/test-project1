﻿if not exists (select 1 from uCommerce_EntityUi where 
	[Type] = 'Affinion.LoyaltyBuild.UCom.MasterClass.Entities.BookingDateThresoldTarget, Affinion.LoyaltyBuild.UCom.MasterClass.Entities')
	insert into uCommerce_EntityUi values ('Affinion.LoyaltyBuild.UCom.MasterClass.Entities.BookingDateThresoldTarget, Affinion.LoyaltyBuild.UCom.MasterClass.Entities','Targets/BookingThresoldUi.ascx',1)
go

declare @uiid int
select top 1 @uiid = EntityUiId from uCommerce_EntityUi
	where [Type] = 'Affinion.LoyaltyBuild.UCom.MasterClass.Entities.BookingDateThresoldTarget, Affinion.LoyaltyBuild.UCom.MasterClass.Entities'
if not exists (select 1 from uCommerce_EntityUidescription where 
	EntityUiId = @uiid)
	insert into uCommerce_EntityUidescription values (@uiid, 'Booking Date Thresold', 'Booking Date Thresold', 'en')
go