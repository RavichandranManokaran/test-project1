﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SitecoreFieldsHelper.cs
/// Sub-system/Module:     Common(VS project name)
/// Description:           Contains helper methods to validate and get values from sitecore fields
/// </summary>

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts
{
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public partial class ClientPortalSliderSublayout : BaseSublayout
    {
        

        private string interval;
        private int timeinterval;

        #region public Properties
        public string Interval
        {
            get
            {
                return interval;
            }
            set
            {
                interval = value;
            }
        }

        public int Timeinterval
        {
            get
            {
                return timeinterval;
            }
            set
            {
                timeinterval = value;
            }
        } 
        #endregion
      

        /// <summary>
        /// Page load
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Item item = this.GetDataSource();
               
                if (item != null)
                {
                    List<Item> images = SitecoreFieldsHelper.GetMutiListItems(item, "Items").ToList();
                    foreach (var img in images)
                    {
                        interval = SitecoreFieldsHelper.GetValue(img, "Interval", "4");
                        timeinterval = 1000 * Convert.ToInt32(interval);

                    }
                    this.ImageSlider.DataSource = SitecoreFieldsHelper.GetMutiListItems(item, "Items");
                    this.ImageSlider.DataBind();
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, this);
                throw;
            }
        }
    }
}