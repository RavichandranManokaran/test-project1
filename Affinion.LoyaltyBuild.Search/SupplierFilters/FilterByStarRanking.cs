﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           FilterByStarRanking.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Search (VS project name)
/// Description:           Filter by StarRanking
/// </summary>
namespace Affinion.LoyaltyBuild.Search.SupplierFilters
{
    public class FilterByStarRanking : ISupplierFilter
    {
        /// <summary>
        /// Returns true to include the supplier in search results
        /// </summary>
        /// <param name="filterData">he filter data</param>
        /// <returns>Returns true to include the supplier in search results</returns>
        public bool IncludeSupplier(SupplierFilterData filterData)
        {
            if (filterData != null && filterData.SearchTerm != null && filterData.ResultsItem == null || string.IsNullOrEmpty(filterData.ResultsItem.StarRanking) || string.IsNullOrEmpty(filterData.SearchTerm.StarRanking))
            {
                return true;
            }

            string resultItemStarRanking = filterData.ResultsItem.StarRanking.ToLowerInvariant();
            string keywordStarRanking = filterData.SearchTerm.StarRanking.ToLowerInvariant();

            if (keywordStarRanking == "all")
            {
                return true;
            }

            return FilterResultContent(keywordStarRanking, resultItemStarRanking);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="keywordValue"></param>
        /// <param name="resultItemValue"></param>
        /// <returns></returns>
        private bool FilterResultContent(string keywordValue, string resultItemValue)
        {
            foreach (var value in keywordValue.Split(','))
            {
                string currentkeywordFacilities;
                currentkeywordFacilities = value;
                bool flag = (resultItemValue.Contains(currentkeywordFacilities));

                if (flag)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
