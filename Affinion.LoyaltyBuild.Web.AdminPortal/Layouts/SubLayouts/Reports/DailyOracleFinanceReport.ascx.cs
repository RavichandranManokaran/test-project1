﻿using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.Reports
{
    public partial class DailyOracleFinanceReport : System.Web.UI.UserControl
    {
        static List<DailyOracleFinance> lstDailyOracleFinanceReport = null;
        protected int TotalOfRecords { get; private set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            BindCoreText();
        }

        protected void Search_Click(object sender, EventArgs e)
        {
            try
            {
                BindSearchData();
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.AdminPortal, ex, null);
                throw;
            }
        }

        private void BindSearchData()
        {
            IReportsService iReportsService = new ReportsService();
            SearchCriteria objSearchCriteria = new SearchCriteria();
            DateTime startDate = new DateTime();
            if (DateTime.TryParse(this.CurrentDate.Value, out startDate))
                objSearchCriteria.StartDate = startDate;
            lstDailyOracleFinanceReport = iReportsService.GetDailyOracleFinanceReport(objSearchCriteria);

            if (lstDailyOracleFinanceReport != null && lstDailyOracleFinanceReport.Count > 0)
            {
                TotalOfRecords = lstDailyOracleFinanceReport.Count;
                this.Export.Attributes.Remove("disabled");
            }
            else
            {
                lstDailyOracleFinanceReport = new List<DailyOracleFinance>();
                this.Export.Attributes.Add("disabled", "true");
            }

            this.ResultGridView.DataSource = lstDailyOracleFinanceReport;
            this.ResultGridView.DataBind();
        }

        protected void Export_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Clear();
                Response.Buffer = true;
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Charset = string.Empty;
                string FileName = CreateFileName();

                using (StringWriter strwritter = new StringWriter())
                {
                    HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);


                    GridView tempGrid = ResultGridView;
                    tempGrid.AllowPaging = false;
                    tempGrid.CssClass = "table table-striped table-bordered";
                    tempGrid.AutoGenerateColumns = false;

                    tempGrid.DataSource = lstDailyOracleFinanceReport;
                    tempGrid.DataBind();

                    Page.Form.Controls.Clear();

                    Page.Form.Controls.Add(tempGrid);

                    tempGrid.GridLines = GridLines.Both;
                    tempGrid.HeaderStyle.Font.Bold = true;
                    Page.Form.RenderControl(htmltextwrtter);
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName + ".xls");
                    Response.Write(strwritter.ToString());
                    Response.End();
                }

            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        private string CreateFileName()
        {
            return "DailyOracleFinanceReport" + DateTime.Now.ToString();
        }
        protected void ResultGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ResultGridView.PageIndex = e.NewPageIndex;
            BindSearchData();
        }

        protected void ResultGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                decimal AmountDebit = 0;
                Label lblAmountDebit = (Label)e.Row.FindControl("lblAmountDebit");

                if (!string.IsNullOrEmpty(lblAmountDebit.Text))
                {
                    AmountDebit = decimal.Parse(lblAmountDebit.Text);
                }

                if (AmountDebit > 0)
                {
                    e.Row.BackColor = Color.LightBlue;
                }
                else
                {
                    e.Row.BackColor = Color.LightGray;
                }
            } 
        }


        /// <summary>
        /// Binding SitecoreTexts
        /// </summary>
        private void BindCoreText()
        {
            Item currentItem = Sitecore.Context.Item;
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Oracle - Daily Finance Report", scOracleReport);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Fill in the parameters for your report", scParmeters);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Oracle - Daily Finance Reports Details", scOracleDailyFinance);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Oracle Daily Finance Reports Total of", scOracleDaily);
           // SitecoreFieldsHelper.BindSitecoreText(currentItem, "DisplayNo Records", scNoRecords);
           
        }

       
    }
}