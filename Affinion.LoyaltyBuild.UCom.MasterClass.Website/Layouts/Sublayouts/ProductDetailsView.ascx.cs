﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.Api;
using UCommerce.Runtime;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.Layouts.Sublayouts
{
    public partial class ProductDetailsView : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var product = SiteContext.Current.CatalogContext.CurrentProduct;
                ProductSkuLabel.Text = product.Sku;

                ProductVariantsDropDownList.DataSource = product.Variants;
                ProductVariantsDropDownList.DataBind();

                ProductVariantsDropDownList.Visible = product.Variants.Any();
            }
        }

        protected void AddToBasketButton_OnClick(object sender, EventArgs e)
        {
            var product = SiteContext.Current.CatalogContext.CurrentProduct;
            string sku = product.Sku;

            string variantSku = null;
            if (product.Variants.Any())
                variantSku = ProductVariantsDropDownList.SelectedValue;

            TransactionLibrary.AddToBasket(1, sku, variantSku);
        }
    }
}