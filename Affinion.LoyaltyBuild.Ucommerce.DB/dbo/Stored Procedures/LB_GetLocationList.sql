﻿/*<summary>
Description: Stored procedure to retrieve Location list
</summary>*/

CREATE PROCEDURE [dbo].[LB_GetLocationList]
AS 

SELECT LocationTargetId, 
	Location
FROM [dbo].LB_LocationTarget


