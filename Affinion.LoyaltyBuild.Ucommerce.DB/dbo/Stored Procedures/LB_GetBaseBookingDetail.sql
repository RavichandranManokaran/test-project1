﻿

CREATE PROCEDURE [dbo].[LB_GetBaseBookingDetail]
(
	@OrderLineID int
) 
AS 
BEGIN
	SELECT 
		OL.OrderLineId,
		BI.BookingNo [BookingReferance],
		BI.StatusId,
		BI.CheckInDate
	FROM 
		[dbo].uCommerce_OrderLine OL 
		INNER JOIN [dbo].LB_BookingInfo BI on BI.OrderLineId = OL.OrderLineId 
	WHERE 
		OL.OrderLineID= @OrderLineID
END


