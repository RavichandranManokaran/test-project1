﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model
{
    public class ARInvoice : Invoice
    {
        public int SupplierVatPaymentId { get; set; }
        public int OrderPaymentId { get; set; }
        public DateTime OrderPaymentDate { get; set; }
        public string CampaignId { get; set; }
        public string ProviderName { get; set; }
        public string CustomerAddressCode { get; set; }
        public int InvoiceId { get; set; }
        public bool IsChecked { get; set; }
        public string OracleID { get; set; }
    }
}
