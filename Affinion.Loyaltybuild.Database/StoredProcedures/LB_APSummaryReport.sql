CREATE PROCEDURE [dbo].[LB_APSummaryReport]
@OracleFileID INT=NULL,
@DateRange DATETIME=NULL,
@DateUpTo DATETIME=NULL,
@Provider VARCHAR(MAX)=NULL,
@Partner VARCHAR(MAX)=NULL
AS
BEGIN

	SELECT CAT.Name [HotelName],COUNT(CAT.Name) [NumberOfSales],SUM(PostedAmount) [ProviderAmount],CU.ISOCode [CurrencyCode]

	FROM LB_Supplier_Payments SP
		INNER JOIN uCommerce_OrderLine OL ON SP.OrderLineId=OL.OrderLineId AND SP.OrderId=OL.OrderId
		INNER JOIN LB_RoomReservation RR ON OL.OrderLineId=RR.OrderLineId
		INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId=PO.OrderId
		INNER JOIN uCommerce_Currency CU ON PO.CurrencyId=CU.CurrencyId
		INNER JOIN LB_CustomerAttribute CA ON PO.CustomerId=CA.CustomerId
		INNER JOIN uCommerce_Customer C ON CA.CustomerId=C.CustomerId
		INNER JOIN Ucommerce_product P ON OL.Sku = P.Sku
		INNER JOIN uCommerce_CategoryProductRelation CPR ON P.Productid = CPR.Productid
		INNER JOIN uCommerce_Category CAT ON CPR.Categoryid = CAT.Categoryid

	WHERE

		(@OracleFileID IS NULL OR  SP.OracleFileId=@OracleFileID) AND 
		SP.PostedAmount IS NOT NULL AND
		(@Partner IS NULL OR   CA.ClientId = @Partner) AND
		(@Provider IS NULL OR  CAT.Guid = @Provider) AND
		(@DateRange IS NULL OR (CAST(RR.CheckInDate AS DATE) <= CAST(@DateUpTo AS DATE)  AND  CAST(RR.CheckOutDate AS DATE) >= CAST(@DateRange AS DATE)))

	GROUP BY
		CU.ISOCode,
		CAT.NAME
			

END






GO


