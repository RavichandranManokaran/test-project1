﻿CREATE TABLE [dbo].[uCommerce_GiftCard] (
    [GiftCardId]      INT            IDENTITY (1, 1) NOT NULL,
    [PaymentMethodId] INT            NOT NULL,
    [CurrencyId]      INT            NOT NULL,
    [Amount]          MONEY          NOT NULL,
    [AmountUsed]      MONEY          DEFAULT ((0)) NOT NULL,
    [Enabled]         BIT            DEFAULT ((1)) NOT NULL,
    [CreatedOn]       DATETIME       NOT NULL,
    [CreatedBy]       NVARCHAR (50)  NOT NULL,
    [ModifiedBy]      NVARCHAR (50)  NULL,
    [ModifiedOn]      DATETIME       DEFAULT (getdate()) NOT NULL,
    [ExpiresOn]       DATETIME       NOT NULL,
    [Code]            NVARCHAR (512) NOT NULL,
    [OrderNumber]     NVARCHAR (50)  NULL,
    [Note]            NVARCHAR (MAX) NOT NULL,
    PRIMARY KEY CLUSTERED ([GiftCardId] ASC),
    FOREIGN KEY ([CurrencyId]) REFERENCES [dbo].[uCommerce_Currency] ([CurrencyId]),
    FOREIGN KEY ([PaymentMethodId]) REFERENCES [dbo].[uCommerce_PaymentMethod] ([PaymentMethodId])
);

