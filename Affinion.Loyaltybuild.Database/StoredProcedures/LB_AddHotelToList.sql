/****** Object:  StoredProcedure [dbo].[LB_AddHotelToList]    Script Date: 10-Feb-16 11:49:06 AM ******/
CREATE procedure [dbo].[LB_AddHotelToList](@ListID int, @newAddHotel varchar(50))
as
Declare
@hotelId varchar(50),
@ifExistHotel varchar(50)
begin
select @hotelId=HotelID from LB_FavoriteList where ListID=@ListID
if (@hotelId IS NULL)
	begin
		update LB_FavoriteList set HotelID=@newAddHotel where ListID=@ListID
	end
Else
	begin
		select @ifExistHotel=HotelID from LB_FavoriteList where ListID=@ListID AND HotelID LIKE '%'+@newAddHotel+'%'
		if (@ifExistHotel IS NULL)
		begin
			update LB_FavoriteList set HotelID=((select HotelID from LB_FavoriteList where ListID=@ListID)+', '+ @newAddHotel) where ListID=@ListID
		end
		else
		begin
			update LB_FavoriteList set HotelID=Replace((select HotelID from LB_FavoriteList where ListID=@ListID), @newAddHotel,'') where ListID=@ListID
		end
	end 
end

GO
