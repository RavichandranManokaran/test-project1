﻿create PROCEDURE [dbo].[LB_GetBookingInfo] (@OrderLineID int)
AS
BEGIN
	--Get Basic Booking Info details based on orderline id

	SELECT
		BI.BookingId,
		BI.BookingNo,
		BI.CheckInDate,
		BI.CheckOutDate,
		BI.CreatedBy,
		BI.CreatedDate,
		BI.StatusId,
		BI.UpdatedBy,
		BI.UpdatedDate,
		(SELECT MAX(CreatedOn) FROM LB_OrderLineStatusAudit WHERE OrderLineId = @OrderLineID) CurrentStatusDate,
		(SELECT MIN(CreatedOn) FROM LB_OrderLineStatusAudit WHERE OrderLineId = @OrderLineID AND StatusId = 6) ConfirmedStatusDate
	FROM
		LB_BookingInfo BI 
	WHERE
		OrderLineId = @OrderLineID
END


