﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model.Payment;
using System.Data.SqlClient;
using service = Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Payment;
using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.Web.ClientPortal.Helper;
using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic;
using Sitecore.Data.Items;
using Affinion.LoyaltyBuild.Common.Utilities;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    public partial class OfflinePortalPaymentHistory : BaseSublayout
    {
        //private variables
        private service.IPaymentService _paymentService;
        private OrderLinePaymentInfo _paymentInfo;

        protected ValidationResponse ValidationResponse { get; private set; }

        /// <summary>
        /// gets the payment info object to render
        /// </summary>
        protected OrderLinePaymentInfo PaymentInfo
        {
            get
            {
                return _paymentInfo;
            }
        }


        public OfflinePortalPaymentHistory()
        {
            _paymentService = ContainerFactory.Instance.GetInstance<service.IPaymentService>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        int _orderLineId;
        protected void Page_Load(object sender, EventArgs e)
        {
            int olid;
            int.TryParse(this.QueryString("olid"), out olid);
            _orderLineId = olid;

            if (_orderLineId > 0)
            {
                _paymentInfo = _paymentService.GetOrderLinePaymentInfo(_orderLineId);
            }

            if (_paymentInfo == null)
            {
                ValidationResponse = new ValidationResponse()
                {
                    IsSuccess = false,
                    Message = "Invalid booking details"
                };
            }
            this.checkRoles();



            ///SitecoreEditableText
            BindSiteCoreText();
        }

        private void BindSiteCoreText()
        {
            Item currentItem = Sitecore.Context.Item;
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "PaymentInformation", scPaymentInformation);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "TotalAmount", scPaymentHistoryTotalAmount);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Payment", scPayment);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "PayNow", scNewPayNow);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "PayNow", scOldPayNow);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "NumericValue", scNumeric);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "AddDue", scAddDue);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "List of Booking Due Records", scListOfBooking);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Date", scDate);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Payment Method", scPaymentMethod);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Payment Reason", scPaymentReason);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "TotalAmount", scTotalAmount);

            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Delete", scDelete);

            SitecoreFieldsHelper.BindSitecoreText(currentItem, "List of Booking Payment Records", scListPaymentRecords);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Date", scBookingDate);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Payment Method", scBookingPaymentMethod);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Payment Reason", scBookingPaymentReason);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "TotalAmount", scBookingTotalAmount);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "RealEx Id", scRealExID);


            SitecoreFieldsHelper.BindSitecoreText(currentItem, "List of Discount Records", scListOfDiscount);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Description", scDescription);

            SitecoreFieldsHelper.BindSitecoreText(currentItem, "DiscAmount", scDiscAmount);

        }


        public void checkRoles()
        {
            string currentUserRole = SitecoreUserProfileHelper.GetItem(UserProfileKeys.LoggedInUserRoles);
            if (currentUserRole.ToLower().Contains(Constants.OfflinePortalRoleCallCenterSupervisor.ToLower()) || currentUserRole.ToLower().Contains(Constants.OfflinePortalRoleIrishCallCenterSupervisor.ToLower()) || currentUserRole.ToLower().Contains(Constants.OfflinePortalRoleSwedishCallCenterSupervisor.ToLower()))
            {
                disableAddDueButton.Value = "false";
            }
            else
            {
                disableAddDueButton.Value = "true";
            }
        }



    }
}







