﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalMenuSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalMenuSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Sitecore.Data.Items" %>
<script type="text/javascript">
    jQuery(document).on('click', '.aReset', function () {
        jQuery("#dialog").dialog({
            title: "Message:",
            buttons: {
                Yes: function () {
                    //jQuery(this).parent().appendTo("form");
                    //jQuery(".btnReset").click();
                    $.get("/handlers/cs/baskethelper.ashx?action=reset", function (data) {
                        window.location.href = "/welcome";

                    });                 
                },
                No: function () {
                    jQuery(this).dialog('close');
                }
            }
        });
        return false;
    });
    </script>
<div runat="server" id="Menu" visible="false">
    <p id="PlaceholderText" runat="server">Set the Main Menu Datasource</p>
    <div id='cssmenu'>
        <ul>
            <asp:repeater id="MainMenu" runat="server" onitemdatabound="ItemBound">
                <ItemTemplate>
                    <li>

                        <sc:Link Field="Link" ID="Link" Item="<%#(Item)Container.DataItem %>" runat="server">                           
                            <sc:Text Field="Body" Item="<%#(Item)Container.DataItem %>" runat="server" />
                        </sc:Link>
                        
                        <asp:Repeater ID="SubMenu" runat="server" >
                            <HeaderTemplate>
                                <ul>
                                    <li>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%-- <sc:Link Field="Link" ID="Link1" Item="<%#(Item)Container.DataItem %>" runat="server">
                                    <sc:Text Field="Body" Item="<%#(Item)Container.DataItem %>" runat="server" />
                                </sc:Link> --%>
                                  <asp:HyperLink id="hyperlink1" NavigateUrl='<%# Eval("RedirectUrl") %>' Text='<%# Eval("MenuText") %>' Target='<%# Eval("Target") %>' runat="server"/>                                 
                            </ItemTemplate>
                            <FooterTemplate>
                                    </li>
                                </ul>
                            </FooterTemplate>
                        </asp:repeater>

                    </li>
                </ItemTemplate>
            </asp:Repeater>
        
            <%--Basket Item section--%>
            <li>
                <a href="<%=this.BasketPageNavigation %>" title="Basket" id="basketLink">
                    <div id="basketico" runat="server">
                        <i class="fa fa-briefcase"></i>
                        <span class="counter" data-basketcount="<asp:Literal ID="basketCount1" runat="server" />">
                            <asp:Literal ID="basketCount" runat="server" />
                        </span>
                        <span>Basket</span>
                    </div>
                </a>
            </li>

            <%--reset section--%>
            <li>
                <a href="" id="aReset" class="aReset">
                    <div id="Div1">
                        <i class="fa fa-repeat"></i>
                        <span>Reset</span>
                    </div>
                </a>
            </li>

        </ul>
    </div>
     <div id="dialog" style="display: none">
         <p>Are you sure you want to reset?</p>
        <asp:Button ID="btnReset" CssClass="btnReset" runat="server" Style="display: none" OnClick="btnReset_Click" />
      </div>
</div>

<script>
    function UpdateBasketCount(value) {
        var count = parseInt(jQuery("span.counter").data("basketcount"), 10);
        jQuery("span.counter").html(count + value);
        jQuery("span.counter").data("basketcount", count + value);
        if (count + value <= 0)
            $("#basketLink").attr("href", "#");
        else
            $("#basketLink").attr("href", "/basket");
    }
</script>
