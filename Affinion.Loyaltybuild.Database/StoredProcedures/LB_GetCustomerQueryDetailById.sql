--exec [dbo].[LB_GetCustomerQueryDetailById] ' 2'
CREATE PROCEDURE [dbo].[LB_GetCustomerQueryDetailById]
(
	@QueryId int
) 
AS 
BEGIN

SELECT isnull(cq.OrderLineId,0) OrderLineId ,cq.PartnerId,cq.ProviderId,cq.QueryId,cq.querytypeid,cq.CampaignId,cq.Content,cq.CreatedBy,cq.CreatedDate,cq.CustomerId,ISNULL(cq.issolved,0) as issolved,
 case when ISNULL(cqa.AttachmentId,0) > 0 then 'true' else 'false' end as HasAttachment ,
ISNULL(CA.BookingReferenceId,'') BookingReference
FROM  LB_CustomerQuery cq
LEFT JOIN [dbo].LB_CustomerAttribute CA ON cq.CustomerId = CA.CustomerId 
LEFT join [dbo].[LB_CustomerQuery_Attachments] cqa on  cq.QueryId = cqa.QueryId
WHERE cq.QueryId = @QueryId

END
