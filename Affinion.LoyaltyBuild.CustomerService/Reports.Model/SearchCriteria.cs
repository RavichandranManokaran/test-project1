﻿/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Entity to hold search criteria related information
/// </summary>

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model
{
    public class SearchCriteria
    {
        /// <summary>
        /// StartDate
        /// </summary>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// EndDate
        /// </summary>
        public DateTime EndDate { get; set; }
        /// <summary>
        /// BookingMethod
        /// </summary>
        public int BookingMethodID { get; set; }
        /// <summary>
        /// SubTypeReport
        /// </summary>
        public int SubTypeReportID { get; set; }

        //Ap-Ar Invoice Search Criteria
        public DateTime? DateRange { get; set; }
        public DateTime? DateUpTo { get; set; }
        public string Provider { get; set; }
        public string Partner { get; set; }
        public string Spa { get; set; }
        public int? PaymentStatus { get; set; }
        public string BookingRefNumber { get; set; }
        public string Currency { get; set; }
        public string StartEndDate { get; set; }
        public string SearchText { get; set; }
        public int? OracleFileID { get; set; }

        public string OccupancyType { get; set; }
        public string SupplierType { get; set; }

    }
}
