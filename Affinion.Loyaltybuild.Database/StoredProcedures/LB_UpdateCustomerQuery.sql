Create procedure [dbo].[LB_UpdateCustomerQuery]
@Queryid int,
@IsSolved bit,
@Content nvarchar(max),
@QueryTypeId int
As
BEGIN
	update LB_CustomerQuery
	set QueryTypeId = @QueryTypeId,
	IsSolved = @IsSolved,
	Content = Content +'\r\n'+ CONVERT(VARCHAR(100),GETDATE()) + ':\r\n' +  @Content,
	UpdatedDate = GETDATE()
	where QueryId = @Queryid
END