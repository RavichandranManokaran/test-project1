﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SuperValuTopCartLinkSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.SuperValuTopCartLinkSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<asp:Literal ID="PlaceholderText" Text="Set the datasource: Required Fields: DesktopImage, MobileImage" runat="server" />

<div class="visible-xs" id="ico-basket">
    <sc:Image Field="DesktopImage" CssClass="img-responsive"  ID="DesktopImage" runat="server" />
</div>
<div class="basket float-right header-right hidden-xs" id="basket-info">
    <div id="basket-ico">
        <sc:Image Field="MobileImage" CssClass="img-responsive"  ID="MobileImage" runat="server" />
    </div>
    <div id="basket-link">
        <!-- Can't check out if nothing in basket -->
        <asp:Literal ID="CartStatus" Text="Your basket is empty!" runat="server" />
    </div>
</div>
