﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TransferPaymentPage.aspx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Pages.TransferPaymentPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form method="POST" action="<%= GatewayUrl %>" id="realexPayment" name="realex">
	<input type="hidden" name="MERCHANT_ID" value="<%= MerchantId %>" />
	<input type="hidden" name="ORDER_ID" value="<%= OrderGuidId %>" />
	<input type="hidden" name="ACCOUNT" value="<%= Account %>" />
	<input type="hidden" name="CURRENCY" value="<%= PaymentCurrency %>" />
	<input type="hidden" name="AMOUNT" value="<%= AmountToPay %>" />
	<input type="hidden" name="TIMESTAMP" value="<%= CurrentTimeStamp %>" />
	<input type="hidden" name="SHA1HASH" value="<%= ShaHash %>" />
	<input type="hidden" name="MERCHANT_RESPONSE_URL" value="<%= ResponseUrl %>" />
	<input type="hidden" name="ANYTHING ELSE" value="<%= AnythingElse %>" />
	<input type="hidden" name="AUTO_SETTLE_FLAG" value="<%= AutoSettleFlag %>" />
	
	<script type="text/javascript">
	    document.getElementById('realexPayment').submit();
	</script>
</form>
</body>
</html>