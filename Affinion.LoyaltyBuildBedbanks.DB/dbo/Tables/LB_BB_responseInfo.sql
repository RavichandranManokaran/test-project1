
/****** Object:  Table [dbo].[LB_BB_responseInfo]    Script Date: 7/19/2016 8:03:02 PM ******/

CREATE TABLE [dbo].[LB_BB_responseInfo](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UUID] [varchar](250) NOT NULL,
	[Method] [varchar](250) NOT NULL,
	[TimeinMs] [int] NOT NULL,
	[Datecreated] [datetime] NOT NULL,
 CONSTRAINT [PK__responseInfo] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[LB_BB_responseInfo] ADD  DEFAULT (getdate()) FOR [Datecreated]
GO

