﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Payment;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic;

namespace Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.Reports
{
    public partial class OracleSalesInvoicesReport : System.Web.UI.UserControl
    {
        IReportsService iReportsService = new ReportsService();
        SearchCriteria objSearchCriteria = new SearchCriteria();
        protected int TotalOfRecords { get; private set; }
        public const string SELECTED_ORACLE_SALES_INVOICE_INDEX = "SelectedOracleSalesInvoiceIndex";
        private IPaymentService _paymentService;


        private List<Int32> SelectedOracleSalesInvoiceIndex
        {
            get
            {
                if (ViewState[SELECTED_ORACLE_SALES_INVOICE_INDEX] == null)
                {
                    ViewState[SELECTED_ORACLE_SALES_INVOICE_INDEX] = new List<Int32>();
                }

                return (List<Int32>)ViewState[SELECTED_ORACLE_SALES_INVOICE_INDEX];
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindCurrency();
            }
        }

        private void GetOracleSalesInvoice()
        {
            try
            {
                iReportsService = new ReportsService();
                objSearchCriteria = new SearchCriteria();

                string[] dates = this.DateRange.Value.ToString().Split('-');
                objSearchCriteria.DateRange = InputDateFormatting(dates[0]);
                objSearchCriteria.DateUpTo = InputDateFormatting(dates[1]);
                if (!string.IsNullOrEmpty(this.Currency.SelectedValue))
                    objSearchCriteria.Currency = this.Currency.SelectedValue;
                if (!string.IsNullOrEmpty(this.PaymentStatus.SelectedValue))
                    objSearchCriteria.PaymentStatus = int.Parse(this.PaymentStatus.SelectedValue);


                List<APInvoice> lstAPInvoice = iReportsService.GetDemands(objSearchCriteria);

                if (lstAPInvoice != null && lstAPInvoice.Count > 0)
                {
                    TotalOfRecords = lstAPInvoice.Count;
                }
                else
                {
                    lstAPInvoice = new List<APInvoice>();
                }

                this.ResultGridView.DataSource = lstAPInvoice;
                this.ResultGridView.DataBind();

                RePopulateCheckBoxes();
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.AdminPortal, ex, null);
                throw;
            }

        }

        private void RePopulateCheckBoxes()
        {
            foreach (GridViewRow row in ResultGridView.Rows)
            {
                var chkBox = row.FindControl("chkSelect") as CheckBox;

                IDataItemContainer container = (IDataItemContainer)chkBox.NamingContainer;

                if (SelectedOracleSalesInvoiceIndex != null)
                {
                    if (SelectedOracleSalesInvoiceIndex.Exists(i => i == container.DataItemIndex))
                    {
                        chkBox.Checked = true;
                    }
                }
            }
        }

        private void RemoveRowIndex(int index)
        {
            SelectedOracleSalesInvoiceIndex.Remove(index);
        }

        private void PersistRowIndex(int index)
        {
            if (!SelectedOracleSalesInvoiceIndex.Exists(i => i == index))
            {
                SelectedOracleSalesInvoiceIndex.Add(index);
            }
        }

        protected void OracleSalesInvoice_Click(object sender, EventArgs e)
        {
            GetOracleSalesInvoice();
        }

        private DateTime InputDateFormatting(string date)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(date))
                {
                    return default(DateTime);
                }
                else
                {
                    DateTime tmpDateTime;
                    if (DateTime.TryParseExact(date,
                                       "yyyy-dd-MM",
                                       CultureInfo.InvariantCulture,
                                       DateTimeStyles.None,
                                       out tmpDateTime))
                    {
                        return tmpDateTime;
                    }
                    else
                    {
                        string[] words = date.Split('/');
                        string newstring = words[1] + "/" + words[0] + "/" + words[2];
                        DateTime dt = Convert.ToDateTime(newstring);
                        return dt;
                    }
                }
            }
            catch
            {
                return default(DateTime);
            }

        }

        protected void PostToOracle_Click(object sender, EventArgs e)
        {
            try
            {
                iReportsService = new ReportsService();
                List<AROraclePayment> lstAROraclePayment = new List<AROraclePayment>();
                ResultGridView.AllowPaging = false;
                foreach (GridViewRow gvRow in this.ResultGridView.Rows)
                {
                    AROraclePayment objAROraclePayment = new AROraclePayment();
                    Label lblBookingRefNo = (Label)gvRow.FindControl("lblBookingRefNo");
                    Label lblBookingID = (Label)gvRow.FindControl("lblBookingID");
                    Label lblArrivalDate = (Label)gvRow.FindControl("lblArrivalDate");
                    Label lblDepartureDate = (Label)gvRow.FindControl("lblDepartureDate");
                    Label lblCustomerName = (Label)gvRow.FindControl("lblCustomerName");
                    Label lblPostedAmount = (Label)gvRow.FindControl("lblPostedAmount");
                    CheckBox chkSelect = (CheckBox)gvRow.FindControl("chkSelect");


                    if (chkSelect.Checked)
                    {
                        lstAROraclePayment.Add(objAROraclePayment);
                    }
                }
                ResultGridView.AllowPaging = true;

                if (lstAROraclePayment != null && lstAROraclePayment.Count > 0)
                {
                    ValidationResponse objValidationResponse = iReportsService.ARPaymentRequest(lstAROraclePayment);
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.AdminPortal, ex, null);
                throw;
            }

        }

        private void BindCurrency()
        {
            _paymentService = ContainerFactory.Instance.GetInstance<IPaymentService>();
            List<CurrencyDetail> currencyList = new List<CurrencyDetail>();
            currencyList = _paymentService.GetCurrencyListDetails();
            Currency.DataSource = currencyList;
            Currency.DataTextField = "Name";
            Currency.DataValueField = "CurrencyID";
            Currency.DataBind();
        }

    }
}