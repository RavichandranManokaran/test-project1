﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           RebuildSearchIndex.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Handling scheduler tasks for search index rebuild
/// </summary>
namespace Affinion.LoyaltyBuild.Common.EventHandlers
{
    #region Using Directives
    using Sitecore.Data;
    using System;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Sitecore.Data.Items;
    using System.Collections.Generic;
    using Sitecore.Collections;
    using System.Linq;
    using Sitecore.ContentSearch.Maintenance;
    using Sitecore.ContentSearch;
    #endregion

    /// <summary>
    /// Rebuild the lucene search index for supplier search
    /// </summary>
    public class RebuildSearchIndex
    {
        /// <summary>
        /// Run the scheduler
        /// </summary>
        public void Run()
        {
            try
            {
                Sitecore.Context.Job.Status.LogInfo("Scheduler started - Rebuilding supplier search index.");

                IndexCustodian.FullRebuild(ContentSearchManager.GetIndex("affinion_suppliers_index"));

                Sitecore.Context.Job.Status.LogInfo("Scheduler finished - Rebuilding supplier search index completed.");

            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                throw;
            }
        }
    }
}
