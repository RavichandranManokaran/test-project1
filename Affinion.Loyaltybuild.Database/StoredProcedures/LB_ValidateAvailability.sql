﻿CREATE PROCEDURE [dbo].[LB_ValidateAvailability]
(@OrderId int,
@OrderLineId int,
@CheckInDateCount int, 
@CheckOutDateCount int,
@Sku NVARCHAR(512),
@IsValid BIT OUTPUT)
AS
SET @IsValid = 1;

DECLARE @Quantity INT;

SELECT @Quantity = SUM(OL.Quantity)
FROM uCommerce_OrderLine OL
WHERE OL.OrderId = @OrderId AND OL.Sku = @Sku

WHILE (@CheckInDateCount < @CheckOutDateCount)
BEGIN

DECLARE @Balance int;
SELECT @Balance = (BA.NumberAllocated - BA.NumberBooked)
FROM [LB_RoomAvailability] BA
WHERE BA.DateCounter = @CheckInDateCount AND
	BA.ProviderOccupancyTypeID = @Sku

	IF (@Balance < @Quantity)
	BEGIN
		SET @IsValid = 0
	END

SET @CheckInDateCount = @CheckInDateCount + 1
END

