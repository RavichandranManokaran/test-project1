/****** Object:  StoredProcedure [dbo].[LB_SearchCustomerQuery]    Script Date: 2/14/2016 2:32:41 PM ******/
DROP PROCEDURE [dbo].[LB_SearchCustomerQuery]
GO

/****** Object:  StoredProcedure [dbo].[LB_SearchCustomerQuery]    Script Date: 2/14/2016 2:32:41 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
/*<summary>
Description: Stored procedure to retrieve Existing Booking details for the given search criteria
<param name="@queryTypeID">querytype id</param>
<param name="@clientIds">Client Id or ',' seperated Id list</param>
<param name="@checkinDate">Booking Created Date</param>
<param name="@checkoutDate">Customer Arrival Date</param>
<param name="@firstname">Customer First Name</param>
<param name="@lastname">Customer Last Name</param>
<param name="@compaignGroupId"> Compaign Group Id</param>
<param name="@providerName">Hotel Name</param>
<param name="@bookingReference">Customer Booking reference Id</param>
<param name="@QueryStatusId">Current Order Status Id</param>
</summary>*/
CREATE PROCEDURE [dbo].[LB_SearchCustomerQuery]
(	   
	   @queryTypeID int =NULL,
	   @clientId varchar(50) =NULL, 
	   @startDate Date=NULL, 
       @endDate Date=NULL,
	   @firstname nvarchar(512)=NULL, 
       @lastname nvarchar(512)=NULL, 
	   @campaignGroupId nvarchar(50) = NULL,
       @providerId nvarchar(50)=NULL, 
       @bookingReference nvarchar(50)=NULL,
	   @isSolved bit=NULL)
AS 
BEGIN
	SELECT 
	    CQ.QueryId,
	    CQ.Content,
		CA.ClientId,
		C.Name [ProviderName], -- provider name 
		CUS.FirstName,
		CUS.LastName, 
		CUS.EmailAddress,
		isnull(CUS.FirstName,'')+ ' ' + isnull(CUS.LastName,'') +'  ' + isnull(CA.AddressLine1,'') + ' ' + isnull(CA.AddressLine2,'') [Name],
		isnull(CUS.PhoneNumber,MobilePhoneNumber) PhoneNumber,
		RR.OrderReference BookingReference,
		CQ.IsSolved,
		CQ.CreatedDate,
		CQ.UpdatedDate,
	    ( Select Count(1) from LB_CustomerQuery_Attachments where QueryId = CQ.QueryId) HasAttachments
	FROM [dbo].LB_CustomerQuery CQ
		INNER JOIN [dbo].uCommerce_OrderLine OL ON OL.OrderLineId = CQ.OrderLineId
		INNER JOIN [dbo].uCommerce_PurchaseOrder PO ON OL.OrderId = PO.OrderId
		INNER JOIN [dbo].LB_RoomReservation RR ON OL.OrderLineId = RR.OrderLineId 
		INNER JOIN [dbo].uCommerce_Product P ON OL.Sku = P.Sku
		INNER JOIN [dbo].uCommerce_CategoryProductRelation CPR ON P.ProductId = CPR.ProductId 
		INNER JOIN [dbo].uCommerce_Category C ON CPR.CategoryId = C.CategoryId
		INNER JOIN [dbo].uCommerce_Customer CUS ON PO.CustomerId = CUS.CustomerId 
		INNER JOIN [dbo].LB_CustomerAttribute CA ON PO.CustomerId = CA.CustomerId 	
	WHERE (@firstname IS NULL OR CUS.FirstName Like @firstname + '%')
		AND (@lastname IS NULL OR CUS.LastName Like @lastname + '%') 
		AND (@clientId IS NULL OR CA.ClientId = @clientId)
		AND (@providerId IS NULL OR c.[Guid] = @providerId)
		AND (CONVERT(VARCHAR(11),CQ.CreatedDate,111) BETWEEN ISNULL(@startDate,'1900-1-1') AND ISNULL(@endDate, '2099-1-1'))
		AND (@bookingReference IS NULL OR RR.OrderReference = @bookingReference)
		AND (@IsSolved IS NULL OR @IsSolved = CQ.IsSolved)
		AND (@campaignGroupId IS NULL OR @campaignGroupId = CQ.CampaignId)
		AND (@queryTypeID IS NULL OR @queryTypeID = CQ.QueryTypeId)
		AND CQ.OrderLineId > 0

union
SELECT  		
		CQ.QueryId,
	    CQ.Content,
		CA.ClientId,
		'' [ProviderName], -- provider name 
		CUS.FirstName,
		CUS.LastName, 
		CUS.EmailAddress,
		isnull(CUS.FirstName,'')+ ' ' + isnull(CUS.LastName,'') +'  ' + isnull(CA.AddressLine1,'') + ' ' + isnull(CA.AddressLine2,'') [Name],
		isnull(CUS.PhoneNumber,MobilePhoneNumber) PhoneNumber,
		CA.BookingReferenceId BookingReference,
		CQ.IsSolved,
		CQ.CreatedDate,
		CQ.UpdatedDate,
	    ( Select Count(1) from LB_CustomerQuery_Attachments where QueryId = CQ.QueryId) HasAttachments
	FROM [dbo]. LB_CustomerQuery CQ		
		INNER JOIN [dbo].uCommerce_Customer CUS ON CQ.CustomerId = CUS.CustomerId 
		INNER JOIN [dbo].LB_CustomerAttribute CA ON CUS.CustomerId = CA.CustomerId 	
	WHERE (@firstname IS NULL OR CUS.FirstName Like @firstname + '%')
		AND (@lastname IS NULL OR CUS.LastName Like @lastname + '%') 
		AND (@clientId IS NULL OR CQ.PartnerId = @clientId)
		AND (@providerId IS NULL OR CQ.ProviderId = @providerId)
		AND (CONVERT(VARCHAR(11),CQ.CreatedDate,111) BETWEEN ISNULL(@startDate,'1900-1-1') AND ISNULL(@endDate, '2099-1-1'))
		AND (@bookingReference IS NULL OR CA.BookingReferenceId = @bookingReference)
		AND (@IsSolved IS NULL OR @IsSolved = CQ.IsSolved)
		AND (@campaignGroupId IS NULL OR @campaignGroupId = CQ.CampaignId)
		AND (@queryTypeID IS NULL OR @queryTypeID = CQ.QueryTypeId)
		AND  CQ.CustomerId > 0
END

GO
