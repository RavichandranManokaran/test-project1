CREATE PROCEDURE [dbo].[LB_GetAPDemands]
@DateRange DATETIME=NULL,
@DateUpTo DATETIME=NULL,
@Currency VARCHAR(10) =NULL,
@PaymentStatus VARCHAR(10) =NULL,
@SearchText VARCHAR(MAX)=NULL
AS
BEGIN

DECLARE @SearchData VARCHAR(MAX)=NULL
IF @SearchText IS NOT NULL
BEGIN
SET @SearchData = '%' + @SearchText + '%'
END
        SELECT * FROM LB_APDemandsRequestView
		WHERE
		(@Currency IS NULL OR  Currency = @Currency) AND
		(@PaymentStatus IS NULL OR  PaymentStatus = @PaymentStatus) AND
		(
		         @SearchData IS NULL OR 
				 ( 
				       CONVERT(VARCHAR,OrderId) LIKE  @SearchData OR
					   CONVERT(VARCHAR,HotelID) LIKE  @SearchData OR
					   BookingRefNo LIKE  @SearchData OR
					   CustomerName LIKE  @SearchData 
				 )
		 ) AND
		 CAST(CheckInDate AS DATE) <= CAST(@DateUpTo AS DATE)  AND  CAST(CheckOutDate AS DATE) >= CAST(@DateRange AS DATE)

END

