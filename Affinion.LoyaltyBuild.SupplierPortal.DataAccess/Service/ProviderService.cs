﻿using Affinion.LoyaltyBuild.SupplierPortal.DataAccess.Data;
using Affinion.LoyaltyBuild.SupplierPortal.DataAccess;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Exceptions;
using Affinion.LoyaltyBuild.Common.Utilities;

namespace Affinion.LoyaltyBuild.SupplierPortal.DataAccess.Service
{
    /// <summary>
    /// provider service
    /// </summary>
    public class ProviderService : IProviderService
    {
        //private variables
        private Sitecore.Data.Database context;


        ///// <summary>
        ///// Get provider info by provider id
        ///// </summary>
        ///// <param name="providerId"></param>
        ///// <returns></returns>
        //public Provider GetProviderById(int providerId)
        //{
        //    try
        //    {
        //        Diagnostics.Trace(DiagnosticsCategory.SupplierPortal, "GetProviderById" + providerId);
        //        var category = UCommerce.EntitiesV2.Category.FirstOrDefault(x => x.CategoryId == providerId);

        //        return GetProviderByCategory(category);
        //    }
        //    catch (Exception exception)
        //    {
        //        Diagnostics.WriteException(DiagnosticsCategory.SupplierPortal, exception, null);
        //        throw;
        //    }
        //}

        /// <summary>
        /// Get provider info by provider guid
        /// </summary>
        /// <param name="providerId"></param>
        /// <returns></returns>
        public Provider GetProviderById(string productSku)
        {
            Diagnostics.Trace(DiagnosticsCategory.SupplierPortal, "GetProviderById" + productSku);
            var product = UCommerce.EntitiesV2.Product.FirstOrDefault(x => x.Sku == productSku);

            return GetProviderByCategory(product);
        }


        /// <summary>
        /// Get all providers
        /// </summary>
        /// <returns></returns>
        public List<Provider> GetAllProviders()
        {
            try
            {
                Diagnostics.Trace(DiagnosticsCategory.SupplierPortal, "GetAllProviders");
                return GetAllProvidersFromSitecore();
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.SupplierPortal, exception, null);
                throw;
            }

        }


        /// <summary>
        /// Get provider by the hotel group id
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public List<Provider> GetProvidersByHotelGroup(string groupId)
        {
            try
            {
                Diagnostics.Trace(DiagnosticsCategory.SupplierPortal, "GetProvidersByHotelGroup" + groupId);
                // var product = UCommerce.EntitiesV2.Product.FirstOrDefault(x => x.Guid == groupId);

                return GetAllGroupProvidersFromSitecore(groupId);
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.SupplierPortal, exception, groupId);
                throw;
            }
        }

        //public List<Provider> GetProvidersByHotelGroup(string uniqueId)
        //{
        //    try
        //    {
        //        Diagnostics.Trace(DiagnosticsCategory.SupplierPortal, "GetProvidersByHotelGroup" + uniqueId);
        //        var category = UCommerce.EntitiesV2.Category.FirstOrDefault(x => x.Guid == new Guid(uniqueId));
        //        return GetAllGroupProvidersFromSitecore(category);
        //    }
        //    catch (Exception exception)
        //    {
        //        Diagnostics.WriteException(DiagnosticsCategory.SupplierPortal, exception, uniqueId);
        //        throw;
        //    }
        //}

        //public ProviderGroup GetProviderGroupById(int provierGroupId)
        //{
        //    try
        //    {
        //        Diagnostics.Trace(DiagnosticsCategory.SupplierPortal, "GetProviderGroupById" + provierGroupId);
        //        var groupCategory = UCommerce.EntitiesV2.Category.FirstOrDefault(x => x.Definition.DefinitionId == 1 && x.CategoryId == provierGroupId);
        //        return GetProviderByGroupCategory(groupCategory);
        //    }
        //    catch (Exception exception)
        //    {
        //        Diagnostics.WriteException(DiagnosticsCategory.SupplierPortal, exception, provierGroupId);
        //        throw;
        //    }
        //}

        public ProviderGroup GetProviderGroupById(string uniqueId)
        {
            try
            {
                Diagnostics.Trace(DiagnosticsCategory.SupplierPortal, "GetProviderGroupById" + uniqueId);
                //  var groupCategory = UCommerce.EntitiesV2.Category.FirstOrDefault(x => x.Guid == new Guid(uniqueId));
                return GetProviderByGroupCategory(uniqueId);
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.SupplierPortal, exception, uniqueId);
                throw;
            }
        }

        public List<Client> GetClientList()
        {
            try
            {
                Diagnostics.Trace(DiagnosticsCategory.SupplierPortal, "GetClientList");
                return Sitecore.Context.Database.SelectItems(ServiceConstants.ClientPath)
                 .Select(x => new Client
                 {
                     Id = x.ID.ToString(),
                     Name = x.DisplayName
                 }).ToList();
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.SupplierPortal, exception, exception.Message);
                throw;
            }
        }

        public List<ProviderGroup> GetAllProviderGroups()
        {
            try
            {
                Diagnostics.Trace(DiagnosticsCategory.SupplierPortal, "GetAllProviderGroups");
                return GetAllGroups();
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.SupplierPortal, exception, exception.Message);
                throw;
            }
        }



        #region private methods
        /// <summary>
        /// get all providers from sitecore
        /// </summary>
        /// <returns></returns>
        private List<Provider> GetAllProvidersFromSitecore()
        {
            try
            {
                Diagnostics.Trace(DiagnosticsCategory.SupplierPortal, "GetAllProvidersFromSitecore");
                context = Sitecore.Context.Database;
                List<Item> providers = context.SelectItems(ServiceConstants.ProviderPath).ToList();
                List<Provider> listProviderInfo = new List<Provider>();
                if (providers != null)
                {
                    foreach (var item in providers)
                    {

                        var productSku = item.Fields[ServiceConstants.Sku].Value;
                        if (!string.IsNullOrEmpty(productSku))
                        {
                            Provider providerInfo = new Provider();

                            providerInfo.Name = SitecoreFieldsHelper.GetValue(item, "Name");
                            providerInfo.ProductSKU = productSku;
                            var product = UCommerce.EntitiesV2.Product.All().FirstOrDefault(x => x.Sku == productSku);
                            if (product != null)
                            {
                                providerInfo.Id = product.Id;
                                providerInfo.SitecoreId = item.ID.ToString();
                                listProviderInfo.Add(providerInfo);
                            }
                        }
                    }
                }

                return listProviderInfo;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.SupplierPortal, exception, exception.Message);
                throw;
            }
        }

        /// <summary>
        /// get providers by category
        /// </summary>
        /// <returns></returns>
        private Provider GetProviderByCategory(UCommerce.EntitiesV2.Product product)
        {
            try
            {
                Diagnostics.Trace(DiagnosticsCategory.SupplierPortal, "GetProviderByCategory" + product);
                if (product != null)
                {
                    context = Sitecore.Context.Database;
                    //string guid = category.Guid.ToString();
                    Item[] Hotels = context.SelectItems(ServiceConstants.ProviderPath);
                    Item providerItem = Hotels.Where(x => x.Fields[ServiceConstants.Sku].Value.Equals(product.Sku, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();

                    if (providerItem != null)
                    {
                        return new Provider { SitecoreId = providerItem.ID.ToString(), Id = product.Id, Name = SitecoreFieldsHelper.GetValue(providerItem, "Name"), ProductSKU = product.Sku };
                    }
                }

                return null;
            }

            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.SupplierPortal, exception, product);
                throw;
            }
        }


        /// <summary>
        /// get providers by group category
        /// </summary>
        /// <returns></returns>
        private ProviderGroup GetProviderByGroupCategory(string groupID)
        {
            try
            {
                Diagnostics.Trace(DiagnosticsCategory.SupplierPortal, "GetProviderByGroupCategory" + groupID);
                if (!string.IsNullOrWhiteSpace(groupID))
                {
                    context = Sitecore.Context.Database;
                    Item[] Groups = context.SelectItems(ServiceConstants.GroupProviderPath);
                    Item groupItem = Groups.Where(x => x.ID.ToString().Equals(groupID, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                    if (groupItem != null)
                    {
                        return new ProviderGroup { SitecoreId = groupItem.ID.ToString(), Name = SitecoreFieldsHelper.GetValue(groupItem, "Name") };

                    }
                }

                return null;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.SupplierPortal, exception, groupID);
                throw;
            }
        }


        /// <summary>
        /// get all providers by hotel group from sitecore
        /// </summary>
        /// <returns></returns>
        private List<Provider> GetAllGroupProvidersFromSitecore(string groupId)
        {
            try
            {
                Diagnostics.Trace(DiagnosticsCategory.SupplierPortal, "GetAllGroupProvidersFromSitecore" + groupId);
                if (!String.IsNullOrWhiteSpace(groupId))
                {
                    context = Sitecore.Context.Database;
                    // string guid = category.Guid.ToString();
                    List<Item> Groups = context.SelectItems(ServiceConstants.GroupProviderPath).ToList();
                    Item groupItem = Groups.Where(x => x.ID.ToString().Equals(groupId, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                    Item[] Providers = context.SelectItems(ServiceConstants.ProviderPath);
                    List<Provider> listProviderInfo = new List<Provider>();
                    if (Providers != null)
                    {
                        foreach (var provider in Providers)
                        {
                            var productSku = provider.Fields[ServiceConstants.Sku].Value;

                            if (!string.IsNullOrEmpty(productSku))
                            {
                                Sitecore.Data.Fields.ReferenceField droplinkFld = provider.Fields[ServiceConstants.Group];
                                if (droplinkFld != null)
                                {
                                    if (droplinkFld.TargetItem != null)
                                    {
                                        Sitecore.Data.Items.Item targetItem2 = droplinkFld.TargetItem;
                                        if (targetItem2 != null && targetItem2.DisplayName != null && targetItem2.Name != null && targetItem2.ID.Equals(groupItem.ID))
                                        {
                                            Provider providerInfo = new Provider();
                                            providerInfo.ProductSKU = productSku;
                                            var product = UCommerce.EntitiesV2.Product.All().FirstOrDefault(x => x.Sku == providerInfo.ProductSKU);
                                            if (product != null)
                                            {
                                                providerInfo.Id = product.Id;
                                                providerInfo.SitecoreId = provider.ID.ToString();
                                                providerInfo.Name = SitecoreFieldsHelper.GetValue(provider, "Name");
                                                listProviderInfo.Add(providerInfo);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    return listProviderInfo;
                }
                return null;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.SupplierPortal, exception, groupId);
                throw;
            }
        }

        /// <summary>
        /// get all groups from sitecore
        /// </summary>
        /// <returns></returns>
        private List<ProviderGroup> GetAllGroups()
        {
            try
            {
                Diagnostics.Trace(DiagnosticsCategory.SupplierPortal, "GetAllGroups");
                context = Sitecore.Context.Database;
                List<Item> groups = context.SelectItems(ServiceConstants.GroupProviderPath).ToList();
                List<ProviderGroup> listProviderInfo = new List<ProviderGroup>();
                if (groups != null)
                {
                    foreach (var item in groups)
                    {
                        ProviderGroup providerInfo = new ProviderGroup();
                        providerInfo.Name = SitecoreFieldsHelper.GetValue(item, "Name");

                        // providerInfo.Id = providerCategory.CategoryId;
                        providerInfo.SitecoreId = item.ID.ToString();
                        listProviderInfo.Add(providerInfo);

                    }

                }

                return listProviderInfo;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.SupplierPortal, exception, exception.Message);
                throw;
            }
        }

        #endregion


    }
}
