﻿CREATE PROCEDURE [dbo].[OLD_Gettransferbooking]
(
@OrderLineID int
) 
As 
Begin
Select 
PO.CustomerID,
RR.CheckIndate [ArrivalDate],
RR.CheckOutdate [Departure],
RR.CreatedDate [Confirmation],
RR.CreatedDate [Reservation], 
OLS.CreatedDate [Statusdate],
DateDiff(day, RR.CheckInDate,RR.checkOutdate) as [NoOfNights], 
OA.FirstName [Name],
OA.AddressName [Address],
OA.PhoneNumber [Phonenumber],
OA.EmailAddress [Email],
OL.Price[TotalPrice],
1 [No.ofRooms],
RR.NoOfAdult[No.Of Adults],
RR.NoOfChild[No.Of Children],
OL.Sku Sku ,
OL.ProductName [OccupancyType],
OL.CreatedBy [CreateBy],
RR.BookingThrough [Method],
OL.CreatedBy [LastUpdate],
RR.OrderReference [BookingReferance]
from 
	[dbo].uCommerce_OrderLine OL 
	left Join [dbo].uCommerce_PurchaseOrder PO ON PO.OrderID =OL.OrderId
	left Join [dbo].uCommerce_OrderAddress OA ON OA.OrderId = OL.OrderId 
	left Join [dbo].LB_OrderLinePayment  OLP on OLP.OrderLineId=OL.OrderLineId 
	left Join [dbo].LB_RoomReservation RR on RR.OrderLineId = OL.OrderLineId 
	left join [dbo].uCommerce_OrderLineStatus OLS on OLS.OrderLineId=OL.OrderLineId

where OL.OrderLineID= @OrderLineID

--Subrate Items
SELECT 
		OrderLineSubrateID 
		,OrderLineID
		,OLS.SubrateItemCode
		,Amount
		,Discount
		,DisplayName
FROM LB_OrderLineSubrates OLS
	left join LB_SubrateItem SI on OLS.SubrateItemCode = SI.SubrateItemCode
WHERE OrderLineID = @OrderLineID

End 

