﻿CREATE TABLE [dbo].[uCommerce_SupplierFilter] (
    [SupplierFilterId] INT           IDENTITY (1, 1) NOT NULL,
    [SearchFilter]     VARCHAR (MAX) NOT NULL,
    [CampaignItem]     INT           NOT NULL,
    CONSTRAINT [PK_uCommerce_SupplierFilter] PRIMARY KEY CLUSTERED ([SupplierFilterId] ASC)
);

