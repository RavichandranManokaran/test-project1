// JavaScript Document
var Photos = {
    unsplash: {
      by: {
        href: 'http://unsplash.com/',
        title: 'Unsplash'
      },
      uuids: [
        '05e7ff61-c1d5-4d96-ae79-c381956cca2e',
        'cd8dfa25-2bc5-4546-995a-f3fd23809e1d',
        '382a5139-6712-4418-b25e-cc8ba69ab07f',
        '3ed25902-4a51-4628-a057-1e55fbca7856',
        '5b0b329d-050e-4143-bc92-7f40cdde46f5',
        '464f96db-6ae3-4875-ac6a-cbede40c4a51',
        '4facbe78-b4e8-4b7d-8fb0-d3659f46f1b4',
        '379c6c28-f726-48a3-b59e-1248e1e30443',
        '631479df-27a8-4047-ae59-63f9167001f2',
        '8e1e4402-84f0-4d78-b7d8-c48ec437b5af',
        'f55e6755-198a-408d-8e82-a50370527aed',
        '5264c896-cf01-4ad9-9216-114c20a388cc',
        'c6284eae-9be4-4811-b45b-17a5b6e99ad2',
        '40ff508f-01e5-4417-bee0-20633efc6147',
        'eaaee377-f1b5-49d7-a7db-d7a1f86b2805',
        '584c29c8-b521-48ee-8104-6656d4faac97',
        '798aa641-01fe-4ed2-886b-bac818c5fdfc',
        'f82be8f5-d517-4642-8fe1-8987b4e530d0',
        '23b818d0-07c3-40de-a070-c999c1323ff3',
        '7ca0e7f6-90eb-4254-82ea-58c77e74f6a0',
        '42dc8c54-2315-453f-9b40-07e332b8ee39',
        '8e62227c-5acb-4603-abb9-ac0643b7b478',
        '80713821-5d54-4819-810a-19991502ca56',
        '35ce83fa-eac1-4326-83e9-e445450b35ce',
        '3df9ac37-4e86-49e5-9095-28679ab37718',
        '9e7211c0-b73b-4b1d-8b47-4b1700f9a80f',
        '1cc3c44b-e4a9-4e37-96cf-afafeb3eb748',
        'ab52465c-b3d8-4bf6-986a-a4bf815dfaed',
        '69e43c1d-9fac-4278-bec5-52291c1b1c2b',
        '0627c11f-522d-48b9-9f17-9ea05b769aaa'
      ]
    },
    birdwatcher: {
      by: {
        href: 'http://birdwatcher.ru/en/',
        title: 'Artem Sapegin'
      },
      uuids: [
        'fdb7745a-78c9-418e-a853-5e95d8d2069d',
        '4c232085-935f-4869-b9f3-4b5e2f254445',
        '2341b9d3-4d02-482e-afff-cc607325f40b',
        '155e039d-00fc-4525-9c68-531a133cf7b7',
        'aa11819e-f614-46bd-8858-752b6fcb0ca3',
        'ac2c2fe6-30fe-4695-876b-ce185ed408cf',
        'ad853163-2c07-4e2a-b6be-8880377c14a6',
        '6e8273f9-25a9-4a68-b620-8a9a3892c731',
        'a5c42bf0-0160-4b3e-867a-f9dfd7ad70d6',
        '69c723d8-ceb5-4126-b289-6473a7490078',
        '5c22d9ee-a631-44bd-b848-363e5f2695cd',
        '6dda3ad1-ce52-4baa-b6dd-3adb4a019154',
        '03b41653-31ea-49a0-bf27-79ce08645726',
        'cd24ae35-b0df-483b-8ae4-ed215a0175ad',
        '1994668f-75fc-48b9-ad75-588cc78c9826',
        '0f67f8ce-5ff9-4288-a9da-f56d6c810337',
        'fcee6f7e-2e3b-4a0e-840f-672c0624d6e7',
        'f30f4bbd-5feb-46f1-b0e7-138c239b65e1',
        'ac31a815-fef6-427b-b036-44c61901b844',
        '388746db-f84e-4800-a199-318d77ac6f12',
        '6b24c79a-1d08-4ad5-a04b-3086cd311f23',
        '37b5454f-5f12-404e-b1e0-fde70ac76890'
      ]
    }
  }