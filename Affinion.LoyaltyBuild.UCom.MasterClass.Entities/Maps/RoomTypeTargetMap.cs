﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Maps
{
    public class RoomTypeTargetMap : SubclassMap<RoomTypeTarget>
    {
        // Methods
        public RoomTypeTargetMap()
        {
            base.Table("LB_RoomTypeTarget");
            base.KeyColumn("RoomTypeTargetId");
            base.Map(x => x.RoomTypeTargetId).ReadOnly();
            base.Map(x => x.RoomType).Not.Nullable();
        }

    }

}
