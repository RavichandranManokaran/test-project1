﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Model
{
    public class LBOfferAvailability
    {
        public virtual int CampaignItem { get; set; }

        public virtual int AvaialbileRoom { get; set; }

        public virtual string Sku { get; set; }

        public virtual string VariantSku { get; set; }

        public virtual int DateCounter { get; set; }

        public virtual decimal Price { get; set; }
        
        public virtual bool IsRestricted { get; set; }
    }
}
