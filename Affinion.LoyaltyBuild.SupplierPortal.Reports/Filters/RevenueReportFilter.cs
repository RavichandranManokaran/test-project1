﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.SupplierPortal.Reports.Filters
{
    /// <summary>
    /// Base Revenue report filters used by two revenue report items
    /// </summary>
    public class RevenueReportFilter
    {
        /// <summary>
        /// From date
        /// </summary>
        public DateTime? DateFrom { get; set; }

        /// <summary>
        /// To date
        /// </summary>
        public DateTime? DateTo { get; set; }

        /// <summary>
        /// Partner name
        /// </summary>
        public string Partner { get; set; }

        /// <summary>
        /// Provider
        /// </summary>
        public int? Provider { get; set; }

        /// <summary>
        /// Offer name
        /// </summary>
        public int? OfferName { get; set; }

        /// <summary>
        /// Date Type
        /// </summary>
        public string DateType { get; set; }

        /// <summary>
        /// Provider group
        /// </summary>
        public string ProviderGroup { get; set; }

        /// <summary>
        /// Break Down by Campaign
        /// </summary>
        public bool BreakdownbyCampaign { get; set; }

        /// <summary>
        /// Break Down by Month/Week
        /// </summary>
        public string Breakdown { get; set; }
    }
}

