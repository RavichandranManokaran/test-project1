﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           LoginSublayout.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.LoginSublayout
/// Description:           Handle user Login

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts
{
    #region Using Directives
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Sitecore.Data.Items;
    using Sitecore.Security.Authentication;
    using System;
    using Affinion.LoyaltyBuild.Web.ClientPortal.Helper;
    #endregion

    public partial class LoginSublayout : BaseSublayout
    {

        private Item loginItem;

        /// <summary>
        /// Page load
        /// </summary>
        private void Page_Load(object sender, EventArgs e)
        {
            ///Add button click eent for the ButtonLogin
            ButtonLogin.ServerClick += new EventHandler(this.ButtonLogin_Click);

            ///Bind the data source
            loginItem = this.GetDataSource();

            ///Check the datasource 
            if (loginItem != null)
            {
                LoginLiteral.Visible = false;
            }
            else
            {
                LoginLiteral.Visible = true;
            }

            ///Check the user is logged in
            if (!Sitecore.Context.IsLoggedIn)
            {
                DivWelcome.Visible = false;
                DivLogOff.Visible = false;
                SignIn.Visible = true;
            }
            else
            {
                DivWelcome.Visible = true;
                DivLogOff.Visible = true;
                SignIn.Visible = false;

                ///Get the value of the title field in the binded item
                var fieldValue = SitecoreFieldsHelper.GetValue(loginItem, "Title", "Welcome");

                ///Set the welcome message
                DivWelcome.InnerHtml = string.Concat(@"<i class=\"" fa fa-user \""></i>", fieldValue, " ", Sitecore.Context.User.Profile.FullName);
            }
        }

        /// <summary>
        /// Login button event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonLogin_Click(object sender, EventArgs e)
        {
            try
            {

                string userName = TextUsername.Text;
                string domainName = string.Concat(Sitecore.Context.Domain.Name, "\\", userName.Trim());
                string password = TextPassword.Text.Trim();

               
                ///If the user logged in
                if ((userName.Trim().Length != 0) && AuthenticationManager.Login(domainName, password))
                {
                    // success!
                    Sitecore.Web.WebUtil.Redirect("/");
                    DivWelcome.Visible = true;
                    SignIn.Visible = false;
                    DivLogOff.Visible = true;
                    SitecoreUserProfileHelper.AddItem(userName, UserProfileKeys.CreatedBy);
                    Item changePasswordPageItem = Sitecore.Context.Database.GetItem(UserProfileKeys.CreatedBy);
                    var fieldValue = SitecoreFieldsHelper.GetValue(loginItem, "Title", "Welcome");

                    ///Set the welcome message
                    DivWelcome.InnerHtml = string.Concat(@"<i class=\"" fa fa-user \""></i>", fieldValue, " ", userName);
                    
                }
                else
                {
                    throw new System.Security.Authentication.AuthenticationException(
                    "Invalid username or password.");
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, this);
                throw;
            }
        }

        /// <summary>
        /// Logout button event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LblLogOut_Click(object sender, EventArgs e)
        {
            ///Log out the current logged in user
            Sitecore.Context.Logout();
            SitecoreUserProfileHelper.ClearCustomProperty();
            Response.Redirect(Request.Url.AbsoluteUri);
        }
    }
}