﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Customer
{
    /// <summary>
    /// Interface to define the Communication related actions
    /// </summary>
    public interface ICommunicationHistoryService
    {
        /// <summary>
        /// To get the list of Communication history info by providing OrderlineID
        /// </summary>
        /// <param name="OrderLineID">OrderLineID</param>
        /// <returns> List of Communication History info  </returns>
        List<CommunicationHistoryInfo> GetCommunicationHistoryByOrderLineID(int orderLineID);

        /// <summary>
        /// Add communication histroy info
        /// </summary>
        /// <param name="communicationHistoryInfo">communicationHistoryInfo</param>        
        /// <returns>Insert success flag</returns>
        ValidationResponse InsertCommunicationHistory(CommunicationHistoryInfo communicationHistoryInfo);

        /// <summary>
        /// To get the Communication history info by providing HistoryID
        /// </summary>
        /// <param name="communicationHistoryID">history id</param>
        /// <returns>Communication history info</returns>
        CommunicationHistoryInfo GetCommucationHistoryByHistoryID(int communicationHistoryID);

        /// <summary>
        /// Update the Mail status
        /// </summary>
        /// <param name="orderlineId">orderline id</param>
        /// <param name="communicationTypeId">communicationTypeId</param>
        /// <param name="status">status</param>
        /// <returns>Mail status flag</returns>
        ValidationResponse UpdateMailSentStatus(int orderlineId,int communicationTypeId, bool status);
    }
}
