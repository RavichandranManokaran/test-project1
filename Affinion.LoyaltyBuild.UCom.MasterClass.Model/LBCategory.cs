﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    public class LBSearhResult
    {
        public LBSearhResult() 
        {
            this.OfferAvailability = new List<RoomOfferAvailability>();
            this.RoomAvailability = new List<RoomOfferAvailability>();
            this.ProductVariantProperty = new List<AdditionalInfo>();
            this.ProductProperty = new List<AdditionalInfo>();
            this.Category = new List<LBProduct>();
        }

        public List<LBProduct> Category { get; set; }
        public List<AdditionalInfo> ProductProperty { get; set; }
        public List<LBProductVariant> Product { get; set; }
        public List<AdditionalInfo> ProductVariantProperty { get; set; }
        public List<RoomOfferAvailability> RoomAvailability { get; set; }
        public List<RoomOfferAvailability> OfferAvailability { get; set; }

    }

    public class RoomOfferAvailability
    {
        public int Availability { get; set; }

        public int DateCounter { get; set; }

        public int OfferId { get; set; }

        public string VSKU { get; set; }

        public string SKU { get; set; }

        public decimal Price { get; set; }

        public Guid PriceBand { get; set; }
    }

    public class LBProduct
    {
        public string SKU { get; set; }
    }
    public class LBProductVariant
    {
        public string SKU { get; set; }

        public int ProductId { get; set; }

        public string Name { get; set; }

        public string VSKU { get; set; }

        public string DisplayName { get; set; }

        public bool IsVariant { get; set; }

        
    }

    //public class LBAvailability
    //{
    //    public string CategoryGuid { get; set; }

    //    public string SKU { get; set; }

    //    public int Availability { get; set; }

    //    public int OfferId { get; set; }
    //}
    public class AdditionalInfo
    {
        public string SKU { get; set; }

        public string VSKU { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }

        public int ProductId { get; set; }
    }


}
