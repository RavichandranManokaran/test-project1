﻿using System;
using System.IO;
using System.Xml;
using System.Web;
using System.Net;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Message;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Message.Interfaces;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Model;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Model.Auth;
using Affinion.LoyaltyBuild.BedBanks.Troika;
using System.Xml.Serialization;
using System.Text;
using System.Collections.Generic;
using System.Web.Configuration;
using Affinion.LoyaltyBuild.BedBanks.General;
using Affinion.LoyaltyBuild.BedBanks.Troika.Model;

namespace Affinion.LoyaltyBuild.BedBanks.JacTravel
{
    /// <summary>
    /// This is the main Controller class for communication between CAPI and the Loyaltybuild System.
    /// 
    /// The class is used to create API requests via the 'Model' and 'Message' objects within
    /// this namespace. 
    /// 
    /// The results can be bound to asp repeaters on the presentation layer.
    /// </summary>
    public static class JtController
    {
        //public static string Endpoint = "https://totalstaytestxml.ivector.co.uk/xml/book.aspx";

        /// <summary>
        /// Publically exposed method to perform a Search Request on JacTravel
        /// </summary>
        /// <param name="pRegionId"></param>
        /// <param name="pArrivalDate"></param>
        /// <param name="pDuration"></param>
        /// <param name="pRoomInfo"></param>
        /// <returns></returns>
        /// 
        public static BedBanksSettings bedBankSettings;

        public static JtSearchResponse Search(string pRegionId, string pArrivalDate, string pDuration, string pRoomInfo, BedBanksSettings bbs)
        {
            bedBankSettings = bbs;
            JtSearchRequest searchReq = new JtSearchRequest(pRegionId, pArrivalDate, pDuration, pRoomInfo, bbs);
            return (JtSearchResponse)searchReq.Fire();
        }

        public static JtSearchResponse SearchOnPropReferenceId(string pRegionId, string pArrivalDate, string pDuration, string pRoomInfo, BedBanksSettings bbs, List<string> refIds)
        {
            bedBankSettings = bbs;
            JtSearchRequest searchReq = new JtSearchRequest(pRegionId, pArrivalDate, pDuration, pRoomInfo, bbs,"0", refIds);
            return (JtSearchResponse)searchReq.Fire();
        }

        public static JtPreBookResponse PreBook(string pPropertyID, string pArrivalDate, string pDuration, string pRoomToken, bool pIsDirect, string pMealBasisID, string pAdults, BedBanksSettings bbs)
        {
            bedBankSettings = bbs;
            JtPreBookRequest preBookReq = new JtPreBookRequest(pPropertyID, pArrivalDate, pDuration, pRoomToken, pIsDirect, pMealBasisID, pAdults, bbs);
            return (JtPreBookResponse) preBookReq.Fire();
        }

        public static JtBookResponse Book(string pPropertyID, string pPreBookingToken, string pArrivalDate, string pDuration, string pTitle, string pFirstName, string pLastName, string pAddressLine1, string pPostcode, string pBookingCountryID, string pPhone, string pEmail, string pRequest, string pRoomToken, bool pIsDirect, string pMealBasisID, string pRoomInfo, BedBanksSettings bbs)
        {
            bedBankSettings = bbs;
            JtBookRequest bookReq = new JtBookRequest(pPropertyID, pPreBookingToken, pArrivalDate, pDuration, pTitle, pFirstName, pLastName, pAddressLine1, pPostcode, pBookingCountryID, pPhone, pEmail, pRequest, pRoomToken, pIsDirect, pMealBasisID, pRoomInfo, bbs);
            return (JtBookResponse) bookReq.Fire();
        }

        public static JtPreCancelResponse PreCancel(string pBookingReferenceID, BedBanksSettings bbs)
        {
            bedBankSettings = bbs;
            JtPreCancelRequest preCancelReq = new JtPreCancelRequest(pBookingReferenceID, bbs);
            return (JtPreCancelResponse) preCancelReq.Fire();
        }

        public static JtCancelResponse Cancel(string pBookingReferenceID, double pCancellationCost, string pCancellationToken, BedBanksSettings bbs)
        {
            bedBankSettings = bbs;
            JtCancelRequest cancelReq = new JtCancelRequest(pBookingReferenceID, pCancellationCost, pCancellationToken, bbs);
            return (JtCancelResponse) cancelReq.Fire();
        }

        public static JtPropertyDetailsResponse GetPropertyDetailsFromCache(string pPropertyId)
        {
            JtPropertyDetailsResponse pdRes = null;

            if (HttpRuntime.Cache[pPropertyId] != null)
            {
                pdRes = (JtPropertyDetailsResponse)HttpRuntime.Cache[pPropertyId];

                //The Data came from the Cache
                pdRes.DataOrigin = DataLocation.Cache;
            }
            return pdRes;
        }

        public static JtPropertyDetailsResponse GetPropertyDetailsFromAPI(string pPropertyId, BedBanksSettings bbs)
        {
            bedBankSettings = bbs;
            JtPropertyDetailsRequest pdReq = new JtPropertyDetailsRequest(pPropertyId, bbs);
            return (JtPropertyDetailsResponse)pdReq.Fire();
        }

        public static JtPropertyDetailsResponse GetSinglePropertyDetails(string pPropertyIdList)
        {
            JtPropertyDetailsResponse pdRes;

            pdRes = TroikaDataController.CheckDbPropertyCacheOld(pPropertyIdList, "1");

            return pdRes;
        }

        public static List<JtPropertyDetailsResponse> GetPropertyDetailsFromDB(string pPropertyIdList)
        {
            List<JtPropertyDetailsResponse> pdRes = TroikaDataController.CheckDbPropertyCache(pPropertyIdList, "1");
                           
            return pdRes;
        }


        /// <summary>
        /// Return Login Details - Could get these from a config file or something in the future.
        /// They are hard-coded values to a test environment for now.
        /// </summary>
        /// <returns>JtLoginDetails Object</returns>
        public static JtLoginDetails GenerateLoginDetails(int pCurrencyID,string userName,string password)
        {
            JtLoginDetails loginDetails = new JtLoginDetails
            {
                Login = userName,
                Password = password,
                Locale = "",
                CurrencyID = pCurrencyID,
                AgentReference = ""
            };
            return loginDetails;
        }

        /// <summary>
        /// Return Login Details - Could get these from a config file or something in the future.
        /// They are hard-coded values to a test environment for now.
        /// </summary>
        /// <returns>JtLoginDetails Object</returns>
        public static JtLoginDetails GenerateLoginDetails(string username, string password)
        {
            JtLoginDetails loginDetails = new JtLoginDetails
            {
                Login = username,
                Password = password,
                Locale = "",
                AgentReference = ""
            };
            return loginDetails;
        }

        /// <summary>
        /// This function will eventually call JacTravel to perform a PropertyDetails Request.
        /// Currently I am reading sample XML from a .js file to mimic the response.
        /// </summary>
        /// <returns>A JtSearchResponse Object - Comprising of a ReturnStatus Object and List of PropertyResult Objects</returns>
        public static string PerformApiRequest(IJtRequest ApiRequest)
        {
            string response = "";
            
            try
            {
                XmlSerializer xsSubmit = new XmlSerializer(ApiRequest.GetType());

                XmlWriterSettings xmlWs = new XmlWriterSettings()
                {
                    OmitXmlDeclaration = true,
                    ConformanceLevel = ConformanceLevel.Auto,
                    Indent = true
                };

                string xml;

                using (StringWriter sw = new StringWriter())
                using (XmlWriter writer = XmlWriter.Create(sw, xmlWs))
                {
                    XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                    ns.Add("", "");
                    xsSubmit.Serialize(writer, ApiRequest, ns);
                    xml = sw.ToString(); // Your XML
                }

                StringBuilder postData = new StringBuilder();
                postData.Append("Data=" + xml);

                string aaa = postData.ToString();

                ASCIIEncoding ascii = new ASCIIEncoding();
                byte[] postBytes = ascii.GetBytes(postData.ToString());

                postBytes = ascii.GetBytes(aaa);

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(bedBankSettings.BedBanksApiUrl);
                httpWebRequest.Method = "POST";
                httpWebRequest.ContentType = "application/x-www-form-urlencoded";
                httpWebRequest.Headers.Add("Accept-Encoding", "gzip");
                httpWebRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                httpWebRequest.ContentLength = postBytes.Length;

                // add post data to request
                Stream postStream = httpWebRequest.GetRequestStream();
                postStream.Write(postBytes, 0, postBytes.Length);
                postStream.Flush();
                postStream.Close();

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    response = streamReader.ReadToEnd();
                }
                return response;
            }
            catch (Exception exc)
            {
                //LOG ERROR

                return response;
            }
        }
    }
}