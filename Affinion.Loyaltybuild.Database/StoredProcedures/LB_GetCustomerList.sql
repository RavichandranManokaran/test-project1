﻿/*<summary>
Description: Stored procedure to retrieve customer details for the given search criteria
<param name="@clientIds">Selected Client Id or ',' seperated list of client Ids Agent has access to</param>
<param name="@firstname">Customer First Name</param>
<param name="@lastname">Customer Last Name</param>
<param name="@EmailAddress">Customer email address</param>
<param name="@Phone">Customer Phone</param>
<param name="@mobilePhone">Customer Mobile Phone</param>
<param name="@Title">Customer Title</param>
<param name="@AddressLine1">Customer Address lLine 1</param>
<param name="@AddressLine2">Customer Address lLine 2</param>
<param name="@AddressLine3">Customer Address lLine 3</param>
<param name="@countyId">Customer Country Id</param>
<param name="@locationId">Customer Location Id</param>
<param name="@BranchId">Client Branch Id</param>
</summary>*/
CREATE PROCEDURE [dbo].[LB_GetCustomerList](
	@ClientIds varchar(500),
	@FirstName nvarchar(512),
	@LastName nvarchar(512),
	@EmailAddress nvarchar(255)=NULL,
	@Phone nvarchar(50)=NULL,
	@MobilePhone nvarchar(50)=NULL,
	@Title varchar(50)=NULL,
	@AddressLine1 nvarchar(50)=NULL,
	@AddressLine2 nvarchar(50)=NULL,
	@AddressLine3 nvarchar(50)=NULL,
	@CountryId int=NULL,
	@LocationId int=NULL,
	@BranchId VARCHAR(50)=NULL)
AS 
/*ClientIds, First Name and Last Name are mandatory Fields. Search excluded Acive and Subscribe fields since they have free defined values*/
BEGIN
	SELECT CA.CustomerId,
	concat(C.FirstName, ' ', C.LastName) as FullName , 
	RTRIM(concat(CA.AddressLine1, IIF(CA.AddressLine2 IS NULL OR CA.AddressLine2='','',concat(', ', CA.AddressLine2)), IIF(CA.AddressLine3 IS NULL OR CA.AddressLine3='','',concat(', ', CA.AddressLine3)))) as FullAddress,
	C.EmailAddress,
	C.MobilePhoneNumber
	FROM [dbo].uCommerce_Customer as C 
	INNER JOIN [dbo].LB_CustomerAttribute as CA 
	ON C.CustomerID=CA.CustomerID 
	WHERE C.FirstName Like @firstname + '%' 
	AND C.LastName Like @lastname + '%' 
	AND CHARINDEX(CA.ClientId, @clientIds) > 0
	AND C.EmailAddress=ISNULL(@EmailAddress,C.EmailAddress)  
	AND (@Phone IS NULL OR C.PhoneNumber = @Phone) 
	AND C.MobilePhoneNumber =ISNULL(@mobilePhone,C.MobilePhoneNumber) 
	AND (@Title IS NULL OR CA.Title=@Title) 
	AND (@addressLine1 IS NULL OR CA.AddressLine1 Like '%' + @addressLine1 + '%') 
	AND (@addressLine2 IS NULL OR CA.AddressLine2 Like '%' + @addressLine2 + '%') 
	AND (@addressLine3 IS NULL OR CA.AddressLine3 Like '%' + @addressLine3 + '%') 
	AND CA.CountryId=ISNULL(@countryId,CA.CountryId)  
	AND CA.LocationId=ISNULL(@locationId,CA.LocationId)  
	AND (@BranchId IS NULL OR CA.BranchId=@BranchId);
END

GO
