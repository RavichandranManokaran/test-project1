
CREATE TABLE [dbo].[uCommerce_Subscription](
	[SubscriptionId] [int] NOT NULL,
	[SubscriptionType] [varchar](100) NULL,
	[DateAdded] [date] NULL,
 CONSTRAINT [PK_uCommerce_Subscription] PRIMARY KEY CLUSTERED 
(
	[SubscriptionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


