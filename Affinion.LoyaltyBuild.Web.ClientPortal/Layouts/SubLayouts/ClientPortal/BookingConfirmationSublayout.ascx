﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BookingConfirmationSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal.BookingConfirmationSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Sitecore.Data.Items" %>
<%@ Import Namespace="Affinion.LoyaltyBuild.Common.SessionHelper" %>

<link href="styles/bootstrap.min.css" rel="stylesheet" type="text/css" />

<asp:HiddenField ID="siteurlfield" runat="server" Value="" Visible="true"></asp:HiddenField>
<div class="alert alert-danger" id="dangerDiv" runat="server">
    <sc:text id="BookingRejectedMessageLabel" runat="server" />
    <br />
    <div id="backbuttonDiv" class="alert col-md-2">
        <%--<input type="button" id="btnGoBack" runat="server" class="btn btn-primary booking-margin-top btn-bg pull-left" onclick="goback()" value=""/>--%>
        <a href="" class="btn btn-primary booking-margin-top btn-bg pull-left" role="button" onclick="goback()" runat="server">
            <sc:text id="GoBackButtonText" runat="server" />
        </a>
    </div>
</div>

<div id="successDiv" runat="server">
    <div class="alert alert-success">
        <sc:text id="BookingConfirmedMessageLabel" runat="server" />
    </div>
    <div class="alert col-md-2">
        <%--<asp:Button type="submit" class="btnprint btn btn-primary booking-margin-top btn-bg pull-left" OnClientClick="javascript:window.print(); return false;" runat="server" Text="Print"/>--%>
        <a href="" class="btnprint btn btn-primary booking-margin-top btn-bg pull-left" runat="server" onclick="javascript:window.print(); return false;">
            <sc:text id="PrintButtonLabel" runat="server" />
        </a>
    </div>
    <div class="alert col-md-10">
        <%--<%=BookingPrintMessage%>--%>
        <sc:text id="BookingPrintMessageLabel" runat="server" />
    </div>
    <div class="content booking-confirmation">

        <div class="content-body">
            <div class="row">
                <div class="col-sm-12 vau-personal-card">
                </div>
            </div>
            <div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="accordian-outer">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading bg-primary">
                                        <h4 class="panel-title">
                                            <sc:text id="BookingSummaryText" runat="server" />
                                        </h4>
                                    </div>
                                    <div id="collapse1" class="panel-collapse collapse in">
                                        <div class="panel-body">

                                            <asp:Repeater ID="PanelRepeater" runat="server" OnItemDataBound="PanelRepeater_ItemDataBound">
                                                <ItemTemplate>
                                                    <div>
                                                        <div class="content_bg_inner_box summary-detail alert-info">

                                                            <div class="col-sm-12 bask-title">
                                                                <div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-12">
                                                                <div class="hotel-name-detail ">
                                                                    <%--<h2 class="no-margin accent54-f">Booking Reference No : <%= this.BookingReference %></h2>--%>
                                                                    <h2 class="no-margin accent54-f">
                                                                        <sc:text id="ReferenceNumberLabel" field="Booking Reference" runat="server" />
                                                                        <%#((BasketInfo)Container.DataItem).OrderLineReference %></h2>
                                                                    <div class="hotel-name-detail-bottom">
                                                                        <p class="no-margin accent54-f ">
                                                                            <span>
                                                                                <sc:text id="EmailLabel" field="E-mailText" runat="server" />
                                                                                <%#((BasketInfo)Container.DataItem).Email %></span>
                                                                        </p>
                                                                        <p class="no-margin accent50-f margin-bottom-15">
                                                                            <span>
                                                                                <sc:text id="BookingReferenceQuoteLabel" field="BookingReferenceQuote" runat="server" />
                                                                            </span>
                                                                        </p>
                                                                    </div>

                                                                </div>

                                                            </div>
                                                            <div class="col-md-5 row col-sm-12">



                                                                <div class="col-xs-12 col-md-12">
                                                                    <div class="hotel-name-detail">
                                                                        <h2 class="no-margin">
                                                                            <div id="divLBName" runat="server">
                                                                            <sc:text item="<%#(Item) ((BasketInfo)Container.DataItem).SitecoreItem %>"
                                                                                id="HotelName" field="Name" runat="server" />

                                                                            <%--       <span class="fa fa-star"></span><span class="fa fa-star">
                                                                </span><span class="fa fa-star"></span><span class="fa fa-star">
                                                               </span><span class="fa fa-star"></span>
                                                                            --%>
                                                                            <sc:text item="<%#(Item) ((BasketInfo)Container.DataItem).SitecoreItem %>" id="StarRanking" field="StarRanking" runat="server" visible="False" />
                                                                                </div>
                                                                            <div id="divBBName" runat="server">
                                                                                <asp:Literal ID="litBBHotelName" runat="server"></asp:Literal>
                                                                            </div>
                                                                            <asp:Repeater ID="StarRepeater" runat="server">
                                                                                <ItemTemplate>
                                                                                    <span class="fa fa-star"></span>
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                        </h2>




                                                                        <div class="hotel-name-detail-bottom">
                                                                            <div id="addLB" runat="server">
                                                                            <p class="no-margin" >
                                                                                <sc:text item="<%# (Item) ((BasketInfo)Container.DataItem).SitecoreItem %>" id="AddressLine1" field="AddressLine1" runat="server" />
                                                                                ,
                                                                            <sc:text item="<%# (Item) ((BasketInfo)Container.DataItem).SitecoreItem %>" id="AddressLine2" field="AddressLine2" runat="server" />
                                                                                ,
                                                                            <sc:text item="<%# (Item) ((BasketInfo)Container.DataItem).SitecoreItem %>" id="AddressLine3" field="AddressLine3" runat="server" />
                                                                                ,
                                                                            <sc:text item="<%# (Item) ((BasketInfo)Container.DataItem).SitecoreItem %>" id="AddressLine4" field="AddressLine4" runat="server" />
                                                                            </p>
                                                                                </div>
                                                                            <div id="addBB" runat="server">
                                                                            <p>
                                                                                <asp:Literal ID="litBBAdd1" runat="server"></asp:Literal>
                                                                                ,
                                                                                <asp:Literal ID="litBBAdd2" runat="server"></asp:Literal>
                                                                                ,
                                                                                <asp:Literal ID="litBBAdd3" runat="server"></asp:Literal>
                                                                                ,
                                                                                <asp:Literal ID="litBBAdd4" runat="server"></asp:Literal>
                                                                            </p>
                                                                            </div>
                                                                            <div id="phLB" runat="server">
                                                                            <p class="no-margin" >
                                                                                <span>
                                                                                    <sc:text id="PhoneTextLabel" field="PhoneText" runat="server" />
                                                                                    <sc:text item="<%# (Item) ((BasketInfo)Container.DataItem).SitecoreItem %>" id="Phone" field="Phone" runat="server" />
                                                                                </span>
                                                                            </p>
                                                                                </div>
                                                                            <div id="phBB" runat="server">
                                                                            <p class="no-margin">
                                                                                <span>
                                                                                    <sc:text id="ph" field="PhoneText" runat="server" />
                                                                                    <asp:Literal ID="litBBph" runat="server"></asp:Literal>
                                                                                </span>
                                                                            </p>
                                                                                </div>
                                                                            <div id="emailLB" runat="server">
                                                                            <p class="no-margin">
                                                                                <span>
                                                                                    <sc:text id="EmailTextLabel" field="E-mailText" runat="server" />
                                                                                    <sc:text item="<%# (Item) ((BasketInfo)Container.DataItem).SitecoreItem %>" id="EmailID" field="EmailID1" runat="server" />
                                                                                </span>
                                                                            </p>
                                                                                </div>
                                                                            <div id="emailBB" runat="server">
                                                                            <p class="no-margin">
                                                                                <span>
                                                                                    <sc:text id="emailBBtxt" field="E-mailText" runat="server" />
                                                                                    <asp:Literal ID="litBBEmail" runat="server"></asp:Literal>
                                                                                </span>
                                                                            </p>
                                                                                </div>
                                                                        </div>

                                                                    </div>
                                                                    <div class="basket-img margin-common-top">
                                                                        <%--<sc:Image Item="<%# (Item) ((BasketInfo)Container.DataItem).SitecoreItem %>" ID="HotelImage" Field="IntroductionImage" runat="server" CssClass="img-responsive img-thumbnail" />--%>
                                                                        <asp:Image ID="hotelImage" runat="server" CssClass="img-responsive img-thumbnail" />

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-7 row col-sm-12">
                                                                <div class="col-sm-12">
                                                                    <h2 class="no-margin">
                                                                        <sc:text id="ReservationDetailsLabel" field="ReservationDetails" runat="server" />
                                                                    </h2>
                                                                </div>
                                                                <div class="row margin-row">
                                                                    <div class="col-sm-7">
                                                                        <span class="accent33-f">
                                                                            <sc:text id="RoomDescriptionLabel" field="RoomDescription" runat="server" />
                                                                        </span>
                                                                    </div>
                                                                    <div class="col-sm-5"><span><%#((BasketInfo)Container.DataItem).RoomType %></span></div>
                                                                </div>
                                                                <div class="row margin-row">
                                                                    <div class="col-sm-7">
                                                                        <span class="accent33-f">
                                                                            <sc:text id="GuestNameLabel" field="GuestName" runat="server" />
                                                                        </span>
                                                                    </div>
                                                                    <div class="col-sm-5"><span><%#((BasketInfo)Container.DataItem).GuestName %></span></div>
                                                                </div>
                                                                <div class="row margin-row">
                                                                    <div class="col-sm-7">
                                                                        <span class="accent33-f">
                                                                            <sc:text id="PeopleTravelingLabel" field="PeopleTraveling" runat="server" />
                                                                        </span>
                                                                    </div>
                                                                    <div class="col-sm-5">
                                                                        <span><%#((BasketInfo)Container.DataItem).Adults %>
                                                                            <sc:text id="AdultLabel" field="AdultText" runat="server" />
                                                                            ,<%#((BasketInfo)Container.DataItem).NoOfChildren %>
                                                                            <sc:text id="ChildrenLabel" field="ChildrenText" runat="server" />
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                <div class="row margin-row">
                                                                    <div class="col-sm-7">
                                                                        <span class="accent33-f">
                                                                            <sc:text id="CheckInDateLabel" field="CheckInDate" runat="server" />
                                                                        </span>
                                                                    </div>
                                                                    <div class="col-sm-5"><span><%#((BasketInfo)Container.DataItem).CheckinDate %></span></div>
                                                                </div>
                                                                <div class="row margin-row">
                                                                    <div class="col-sm-7">
                                                                        <span class="accent33-f">
                                                                            <sc:text id="CheckOutDateLabel" field="CheckOutDate" runat="server" />
                                                                        </span>
                                                                    </div>
                                                                    <div class="col-sm-5"><span><%#((BasketInfo)Container.DataItem).CheckoutDate %></span></div>
                                                                </div>
                                                                <div class="row margin-row">
                                                                    <div class="col-sm-7">
                                                                        <span class="accent33-f">
                                                                            <sc:text id="SpecialRequestLabel" field="SpecialRequest" runat="server" />
                                                                        </span>
                                                                    </div>
                                                                    <div class="col-sm-5"><span><%#((BasketInfo)Container.DataItem).SpecialRequest %></span></div>
                                                                </div>
                                                                <div class="row margin-row">
                                                                    <div class="col-sm-7">
                                                                        <span class="accent33-f">
                                                                            <sc:text id="AccomodationPayableLabel" field="AccomodationPayable" runat="server" />
                                                                        </span>
                                                                    </div>
                                                                    <div class="col-sm-5">
                                                                        <span>
                                                                            <asp:Literal ID="PriceAccommodation" runat="server" /></span>
                                                                    </div>
                                                                </div>
                                                                <div class="row margin-row">
                                                                    <div class="col-sm-7">
                                                                        <span class="accent33-f">
                                                                            <sc:text id="PaidTodayTextLabel" field="PaidTodayText" runat="server" />
                                                                            </span>
                                                                    </div>
                                                                    <div class="col-sm-5">
                                                                        <span>
                                                                             <asp:Literal ID="DepositPayableToday" runat="server" /></span>
                                                                          
                                                                    </div>
                                                                    <div class="col-sm-5">
                                                                        <span>
                                                                            <asp:Literal ID="ProcessingFee" runat="server" /></span>
                                                                    </div>
                                                                </div>
                                                                <%-- <div class="row margin-row">
                                                                    <div class="col-sm-7"><span class="accent33-f">Discount:</span></div>
                                                                    <div class="col-sm-5"><sp/an><%#((BasketInfo)Container.DataItem).Discount %></sp/an></div>
                                                                </div>--%>
                                                                <%--<div class="row margin-row">
                                                                    <div class="col-sm-8"><span class="accent33-f">Including processing fee of € 304.00</span></div>

                                                                </div>--%>
                                                                <%--<div class="row margin-row">
                                                                    <div class="col-sm-7"><span class="accent33-f">Total Points/Stickers Used:</span></div>
                                                                    <div class="col-sm-5"><span>0</span></div>
                                                                </div>--%>
                                                                <%-- <div class="row margin-row">
                                                                    <div class="col-sm-7"><span class="accent33-f">Booked By:</span></div>
                                                                    <div class="col-sm-5"><span><%= BookedBy %></span></div>
                                                                </div>--%>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </ItemTemplate>
                                            </asp:Repeater>

                                        </div>
                                        <div class="alert alert-danger" id="MailErrorDiv" runat="server" visible="false">
                                            <asp:Literal runat="server" ID="MailErrorLiteral"></asp:Literal>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <%--<div class="col-md-12 alert alert-warning margin-bottom-15">
                <p>
                    To view or cancel you booking online, please use
                    <asp:literal id="bookingLink" runat="server" />
                    <a class="accent54-f font-weight4" href="#">MY BOOKINGS</a>
                </p>
                <p>Contact customer service with any queries on <a href="#" class="accent54-f font-weight4">info@supervalugetaway.com</a> or by calling 0818 220 088. Our office hours are Monday to Friday, from 9am to 5pm (excluding bank holidays)</p>
            </div>--%>

            <div>
                <input id="hiddenFieldHost" class="HiddenFieldClass" type="hidden" value='<%=GetHostUrl %>' />
            </div>
            
            <div class="col-md-12 alert alert-warning margin-bottom-15 ConfirmationFooter">
                <%--<%=ConfirmationFooterContent%>--%>
                <sc:text id="ConfirmationFooterLabel" runat="server" />
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">

    $(document).ready(function () {
        var serverhost = $(".HiddenFieldClass").val();
        $(".ConfirmationFooter a").each(function () {
            var currentAnchor = $(this);
            var hrefValue = $(currentAnchor).attr("href");
            if (hrefValue.indexOf("http") > -1 || hrefValue.indexOf("https") > -1) {
            }
            else {
                $(this).attr("href", serverhost + hrefValue);
            }
            var anchorTarget = $(this).attr("target");
            $(this).attr("target", "_parent");

            //if (anchorTarget.consists("_blank") || anchorTarget.consists("_parent") || anchorTarget.consists("_self")) {
            //    $(this).attr("_blank", "_top");
            //    $(this).attr("_parent", "_top");
            //    $(this).attr("_self", "_top");
            //}
        });
    });

    function goback() {
        var url = document.getElementById('<%= siteurlfield.ClientID %>').value;
        var ClientUrl = url;
        window.parent.location.href = ClientUrl + "/basket";
    }
    var el = document.getElementById('mybooking');
    el[window.addEventListener ? 'addEventListener' : 'attachEvent'](window.addEventListener ? 'click' : 'onclick', myClickFunc, false);
    function myClickFunc() {
        var url = document.getElementById('<%= siteurlfield.ClientID %>').value;
        var ClientUrl = url;
        window.parent.location.href = ClientUrl + "/searchbooking";
    }

</script>

