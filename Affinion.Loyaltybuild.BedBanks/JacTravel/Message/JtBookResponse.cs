﻿using System;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Model;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Message.Interfaces;

namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Message
{
    [Serializable]
    public class JtBookResponse : JtResponse, IJtResponse
    { 
        public string BookingReference { get; set; }
        public string TradeReference { get; set; }
        public double TotalPrice { get; set; }
        public double TotalCommission { get; set; }
        public double CustomerTotalPrice { get; set; }
        public string PaymentsDue { get; set; } //Presume null all the time as we are always @ full payment
        
        public JtPropertyBooking[] PropertyBookings { get; set; }
    }
}
