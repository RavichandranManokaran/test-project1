﻿using Affinion.LoyaltyBuild.BedBanks.General;
using Affinion.LoyaltyBuild.BedBanks.Helper;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Message;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Model;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Model.Room;
using Affinion.LoyaltyBuild.BedBanks.Troika;
using Affinion.LoyaltyBuild.BedBanks.Troika.Model;
using Affinion.LoyaltyBuild.Common.Exceptions;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.Model.Product;
using Affinion.LoyaltyBuild.Model.Provider;
using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
using Affinion.LoyaltyBuild.Search.Data;
using Sitecore.Data;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using NewSearchApi = Affinion.LoyaltyBuild.Api.Search.Data;
using searchhelper = Affinion.LoyaltyBuild.Api.Search.Helpers;
using System.Web.UI.WebControls;
using Affinion.LoyaltyBuild.Web.ClientPortal.ViewModels;
using Affinion.LoyaltyBuild.Web.ClientPortal.Helper;
using Affinion.LoyaltyBuild.Common.SessionHelper;
using Affinion.Loyaltybuild.BusinessLogic;
using Affinion.LoyaltyBuild.Api.Booking.Data.Basket;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts
{
    public partial class BBHotelDetailsSublayout : BaseSublayout
    {
        public string latitude;
        public string lognitude;
        private string inputChildAges = string.Empty;
        string path = "/sitecore/content/#admin-portal#/#client-setup#/#global#/#_supporting-content#/#bedbankprovidersettings#/#jactravel#/#jactravel-currency#//*[@@templateid='{730153DD-E48F-4F75-9BB1-CC16F8F8A432}']";
        private Item clientItem = SitecoreFieldsHelper.GetLookupFieldTargetItem(ItemHelper.RootItem, "ClientDetails");
        private SearchKey searchTerm;
        NewSearchApi.SearchKey mySearchKey = new NewSearchApi.SearchKey(System.Web.HttpContext.Current);
        static PropertyResult hotelDetails;
        public string propertyRefID = string.Empty;

        /// <summary>
        /// Class to store BasketData from multiple selection
        /// </summary>
        private class BasketData
        {
            public int Qty { get; set; }
            public string[] CommandArgument { get; set; }
        }
        
        /// <summary>
        /// Code to execute before page loads
        /// </summary>
        /// <param name="e">The argument</param>
        public override void PreLoad(EventArgs e)
        {
            QueryStringHelper helper = new QueryStringHelper(Request.Url);
            string hotelItemId = helper.GetValue("id");

            if (!string.IsNullOrEmpty(hotelItemId))
            {
                plcBBwrapper.Visible = false;
                return;
            }
            AvailableAlert.Visible = false;
           
            string checkinDateValue = helper.GetValue("checkinDate");
            string noOfNights = helper.GetValue("NoOfNights");
            propertyRefID = helper.GetValue("propertyRefId");
            searchTerm = new SearchKey(Request.Url, clientItem);
            if (string.IsNullOrEmpty(propertyRefID))
            {
                plcBBwrapper.Visible = false;
                return;
            }
            if (this.IsPostBack && this.Request != null && this.Request.Params["__EVENTTARGET"] != null && (this.Request.Params["__EVENTTARGET"].Contains("CheckAvailabilityButton")))
            {
                checkinDateValue = this.CheckInDate.Text;
                noOfNights = this.NoOfNights.SelectedValue;
                DateTime checkinDate = DateTimeHelper.ParseDate(checkinDateValue);
                DateTime checkoutDate = checkinDate.AddDays(double.Parse(noOfNights));

                helper.SetValue("checkinDate", checkinDateValue);
                helper.SetValue("checkoutDate", checkoutDate.ToString(DateTimeHelper.DateFormat));
                helper.SetValue("NoOfNights", noOfNights);

                Response.Redirect(helper.GetUrl().ToString(), true);
            }
            this.CheckInDate.Text = string.IsNullOrEmpty(helper.GetValue("checkinDate")) ? Convert.ToString(DateTime.Now.AddDays(1).ToString("dd/MM/yyyy")) : helper.GetValue("checkinDate");
            this.CheckingDateLabel.Text = string.IsNullOrEmpty(helper.GetValue("checkinDate")) ? Convert.ToString(DateTime.Now.AddDays(1).ToString("dd/MM/yyyy")) : helper.GetValue("checkinDate");
            this.NumberOfNightsLabel.Text = string.IsNullOrEmpty(helper.GetValue("NoOfNights")) ? Convert.ToString(1) : helper.GetValue("NoOfNights");
            this.NoOfNights.DataSource = Enumerable.Range(1, int.Parse(noOfNights));
            NoOfNights.DataBind();
            this.NoOfNights.SelectedValue = string.IsNullOrEmpty(helper.GetValue("NoOfNights")) ? Convert.ToString(1) : helper.GetValue("NoOfNights");
            if (helper.GetValue("childAge") != null)
            {
                inputChildAges = helper.GetValue("childAge");
            }
        }

        /// <summary>
        /// Page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            QueryStringHelper helper = new QueryStringHelper(Request.Url);
            string propertyRefID = helper.GetValue("propertyRefId");
            string providerID = helper.GetValue("providerId");
            string noOfRooms = helper.GetValue("NoOfRooms");          
            string arrDate = helper.GetValue("checkinDate");
            string destination = helper.GetValue("Destination");
            string roomDetail = string.Empty;

            string hotelItemId = helper.GetValue("id");

            if (!string.IsNullOrEmpty(hotelItemId))
            {
                plcBBwrapper.Visible = false;
                return;
            }

            Item clientItem = SitecoreFieldsHelper.GetLookupFieldTargetItem(ItemHelper.RootItem, "ClientDetails");
            BedBanksSettings bbS = new BedBanksSettings(clientItem);
            DateTime date;
            if (!string.IsNullOrEmpty(arrDate))
            {
                date = DateTime.ParseExact(arrDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                arrDate = date.ToString("yyyy-MM-dd");
            }

            JacTravelApiWrapper jac = new JacTravelApiWrapper();
            List<string> propRef = new List<string>();
            propRef.Add(propertyRefID);
            JtSearchResponse propertyDetailsSearchResponse = jac.SearchOnPropRef("0", arrDate, helper.GetValue("NoOfNights"), helper.GetValue("NoOfRooms"), helper.GetValue("NoOfAdults"), helper.GetValue("NoOfChildren"), helper.GetValue("AgeOfChildren"), bbS, propRef);
            hotelDetails = new PropertyResult();
            hotelDetails = propertyDetailsSearchResponse.PropertyResults.FirstOrDefault();
            foreach (var room in hotelDetails.RoomTypes)
            {
                room.Sku = "BedBanksProduct";
                room.VariantSku = "DefaultRoomType";
                decimal margin = GetMarginAmount(hotelDetails, arrDate);
                room.PriceToDisplay = CalculateBBMargin(margin,room.Total).ToString();
                if (bbS.BedBanksActualCurrency != null)
                    room.CurrencyId = ReverseMapCurrency(bbS.BedBanksActualCurrency).ToString();
                else
                    room.CurrencyId = ReverseMapCurrency(bbS.BedBanksDefaultCurrency).ToString();
            }

            if (!IsPostBack)
            {
                BedBanksCurrency bb = new BedBanksCurrency();
                List<JtSearchResponse> property = new List<JtSearchResponse>();

                if (string.IsNullOrEmpty(providerID))
                {
                    plcBBwrapper.Visible = false;
                    return;
                }

                int roomsTotal;
                int.TryParse(noOfRooms, out roomsTotal);
                int noOfAdults;
                int.TryParse(helper.GetValue("NoOfAdults"), out noOfAdults);
                int noOfChildren;
                int.TryParse(helper.GetValue("NoOfChildren"), out noOfChildren);
                string[] childAge = !string.IsNullOrEmpty(helper.GetValue("NoOfChildren")) ? helper.GetValue("NoOfChildren").Split(',').ToArray() : null;
                var roomInfo = new List<string>();


                if (!string.IsNullOrEmpty(propertyRefID) && (!string.IsNullOrEmpty(providerID)))
                {

                    foreach (var location in bbS.BedBankLocation)
                    {
                        if (!string.IsNullOrEmpty(destination) && destination.ToLower().Contains(location.LocationName.ToLower()))
                        {
                            Item locationItem = location.SitecoreLocationItem;
                            Item countryItem = (locationItem == null) ? null : SitecoreFieldsHelper.GetLookupFieldTargetItem(locationItem, "Country");
                            if (countryItem != null)
                            {
                                BedBanksCurrency actualCurr = GetActualCurrency(countryItem);
                                bbS.BedBanksActualCurrency = actualCurr;
                                break;
                            }
                        }
                    }

                    //Check If BB Property has availability
                    if (hotelDetails != null)
                    {
                        property.Add(propertyDetailsSearchResponse);

                        providerName.Text = hotelDetails.PropertyName;
                        providerNameText.Text = hotelDetails.PropertyName;
                        providerAddress.Text = hotelDetails.PropertyDetails.Address1 + ", " + hotelDetails.PropertyDetails.Address2 + ", " + hotelDetails.PropertyDetails.Region + ", " + hotelDetails.PropertyDetails.Country;
                        lognitude = hotelDetails.PropertyDetails.Longitude;
                        latitude = hotelDetails.PropertyDetails.Latitude;
                        string fac = string.Empty;
                        foreach (var facility in hotelDetails.PropertyDetails.Facilities)
                        {
                            fac = fac + facility.Facility + ", ";
                        }
                        lblFacilities.Text = fac;
                        litOverview.Text = hotelDetails.PropertyDetails.Strapline;
                        ImageGalleryRepeater.DataSource = hotelDetails.PropertyDetails.Images;
                        ImageGalleryRepeater.DataBind();
                        BindBBStarRankings(BBStarRepeater, hotelDetails.Rating);
                        //Check if we have any matching LB Hotel 
                        if (!string.IsNullOrEmpty(hotelDetails.PropertyDetails.SitecoreHotelId) && IsPostBack)
                        {
                            ID itemId = Sitecore.Data.ID.Parse(hotelDetails.PropertyDetails.SitecoreHotelId);
                            Item hotelitem = Sitecore.Context.Database.GetItem(itemId);
                            string ProviderSKU = hotelitem.Fields["SKU"].Value;


                            //If checkin date is null, set add a day with current date as checkindate 

                            NewSearchApi.SearchKey mySearchKey = new NewSearchApi.SearchKey(System.Web.HttpContext.Current);

                            ProviderSearch providerInfo = new ProviderSearch();
                            providerInfo.ProviderId = ProviderSKU;
                            providerInfo.SitecoreItemId = itemId.ToString();
                            providerInfo.ProviderType = ProviderType.LoyaltyBuild;

                            ///Getting Hotel Details
                            var hotel = searchhelper.SearchHelper.GetProviderInfo(mySearchKey, providerInfo);
                            var HotelRooms = LoadRoomsData(hotel, itemId);
                            if (HotelRooms != null && HotelRooms.Count() > 0)
                            {
                                IList<IProduct> products = new List<IProduct>();
                                foreach (var item in HotelRooms)
                                {
                                    IProduct room =  new Room();
                                    room.Name = item.Name;
                                    room.Price = item.Price;
                                    room.Sku = item.Sku;
                                    room.VariantSku = item.VariatSku;
                                    room.Type = ProductType.Room;
                                    products.Add(room);
                                }

                                if (HotelRooms.Count > 0)
                                {
                                    OffersList.DataSource = HotelRooms.OrderBy(y => y.Price);
                                    OffersList.DataBind();
                                    AvailableAlert.Visible = false;
                                }
                            }
                            else if (hotelDetails.RoomTypes != null && hotelDetails.RoomTypes.Count() > 0)
                            {
                                LoadBBRooms(hotelDetails, arrDate);
                            }
                        }
                        else
                        {
                            LoadBBRooms(hotelDetails, arrDate);
                        }
                    }
                }
            }
        }
        private decimal CalculateBBMargin(decimal margin,decimal price)
        {
            return Math.Round(price + ((margin / 100) * price), 2);
        }

        private void LoadBBRooms(PropertyResult hotelDetails, string arrDate)
        {            
            JtRoomType[] rooms = null;
            decimal margin = GetMarginAmount(hotelDetails, arrDate);
            CurrencyInfo ci = new CurrencyInfo();
            decimal finalPrice;

            int count = 0;
            if (hotelDetails.RoomTypes != null)
            {
                int avaiableRooms = hotelDetails.RoomTypes.Count(x => x != null);
                rooms = new JtRoomType[avaiableRooms];
               
                foreach (var roomtype in hotelDetails.RoomTypes)
                {
                    if (roomtype != null && roomtype.RoomType != null)
                    {
                        finalPrice = CalculateBBMargin(margin, roomtype.Total);
                        roomtype.PriceToDisplay = finalPrice.ToString();
                        rooms[count] = (JtRoomType)roomtype;                      
                        count++;
                    }
                }
            }
            
            List<ProviderInfo> providerDetails = new List<ProviderInfo>();
            foreach(var room in rooms)
            {
                ProviderInfo objProviderInfo = new ProviderInfo();
                objProviderInfo.ProviderID = hotelDetails.PropertyID;
                objProviderInfo.Sku = room.Sku;
                objProviderInfo.VariatSku = room.VariantSku;
               // objProviderInfo.BBMarginId = room.Margin;
                objProviderInfo.BookingTokenForBB = room.BookingToken;
                objProviderInfo.BBMealBasisId = room.MealBasisID;
                objProviderInfo.Name = room.RoomType;
                decimal temp = 0;
                decimal.TryParse(room.PriceToDisplay, out temp);
                objProviderInfo.Price = temp;
                objProviderInfo.BBMealBasis = room.MealBasis;
                objProviderInfo.UComCurrencyId = room.CurrencyId;
                objProviderInfo.ProviderType = ProviderType.BedBanks;
                objProviderInfo.BBMarginPercentage = margin;
                objProviderInfo.PriceCurrency = ci.GetPriceWithCurrency(temp);
                providerDetails.Add(objProviderInfo);
                
            }
           
            if (rooms != null && rooms.Count() > 0)
            {
                roomTypeBBRepeater.DataSource = providerDetails;
                roomTypeBBRepeater.DataBind();
                OffersList.Visible = false;
                AvailableAlert.Visible = false;
            }
        }

        private static decimal GetMarginAmount(PropertyResult hotelDetails, string arrDate)
        {
            decimal margin;
            int marginId;
            string[] marginInfo = GetMargin(hotelDetails.PropertyID, arrDate).Split(',');
            decimal.TryParse(marginInfo[1], out margin);
            int.TryParse(marginInfo[0], out marginId);
            return margin;
        }


        /// <summary>
        /// ItemDataBound of OffersList Repeater
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OffersList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            ListItemType itemType = e.Item.ItemType;

            if (itemType == ListItemType.Item || itemType == ListItemType.AlternatingItem)
            {
                DropDownList selectList = e.Item.FindControl("drpAvailability") as DropDownList;

                int availability = (int)((ProviderInfo)e.Item.DataItem).RoomAvailability;

                selectList.DataSource = Enumerable.Range(0, availability + 1); ;
                selectList.DataBind();

                if (availability <= 0)
                {
                    selectList.Enabled = false;
                }
            }
            else if (itemType == ListItemType.Footer)
            {
                var addToBasket = e.Item.FindControl("ButtonAddToBasket") as Button;
                if (addToBasket != null)
                {
                    //addToBasket.Click += addToBasket_Click;
                }
            }
        }


        /// <summary>
        /// Event handler for binding room types
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void roomTypeBBRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e != null)
            {

                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    DropDownList ddl = e.Item.FindControl("availabilityDDL") as DropDownList;
                    QueryStringHelper helper = new QueryStringHelper(Request.Url);
                    int availability = int.Parse(helper.GetValue("NoOfRooms"));
                    ddl.DataSource = Enumerable.Range(0, availability + 1);
                    ddl.DataBind();
                    //var addToBasket = e.Item.FindControl("ButtonBBAddToBasketOneClick") as Button;
                    //if (addToBasket != null)
                    //{
                    //    addToBasket.Click += ButtonBBAddToBasketOneClick_Click;
                    //}

                }
            }
        }

        private List<ProviderInfo> LoadRoomsData(IProvider productdetails, ID itemId)
        {
            List<ProviderInfo> finallist = new List<ProviderInfo>();
            IList<IProduct> products = new List<IProduct>();
            List<PriceAvailabilityByDate> PriceAvailabilityList = new List<PriceAvailabilityByDate>();
            NewSearchApi.SearchKey mySearchKey = new NewSearchApi.SearchKey(System.Web.HttpContext.Current);
            products = productdetails.Products;

            PriceAvailabilityList = HotelSearchHelper.GetAvailabilityAndPriceByDate(products, mySearchKey, DateTime.Parse(mySearchKey.CheckInDate.ToString()), DateTime.Parse(mySearchKey.CheckInDate.ToString()), mySearchKey.NoOfNights);
            if (PriceAvailabilityList != null && PriceAvailabilityList.Count > 0)
            {
                foreach (var priceLst in PriceAvailabilityList)
                {
                    ProviderInfo pr = new ProviderInfo();
                    pr.Price = decimal.Round(priceLst.Price, 2, MidpointRounding.AwayFromZero);
                    pr.Sku = priceLst.Sku;
                    pr.RoomAvailability = priceLst.Availability;
                    pr.VariatSku = priceLst.VariantSku;
                    pr.ProviderID = itemId.ToString();
                    pr.PriceCurrency = GetCurrencyInformation();
                    pr.NoOfNights = priceLst.NoOfNights;
                    pr.Name = products.First(w => w.VariantSku == priceLst.VariantSku).Name;
                    pr.ProviderType = ProviderType.LoyaltyBuild;
                    finallist.Add(pr);
                }
            }
            return finallist;
        }

        private static int ReverseMapCurrency(BedBanksCurrency curr)
        {
            int currencyId = 0;
            if (curr != null)
            {
                Item ucomCurrencyItem = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(curr.uCommerceCurrencyId));
                if (ucomCurrencyItem != null)
                {
                    int.TryParse(ucomCurrencyItem.Fields["UCommerceId"].Value, out currencyId);
                }
            }
            return currencyId;
        }

        /// <summary>
        /// Get the user selected currency information or default
        /// </summary>
        /// <returns>Returns currency information</returns>
        public string GetCurrencyInformation()
        {
            string currencyCode = Request.QueryString["cur"] != null ? Request.QueryString["cur"].ToString() : null;

            if (Request.Cookies["affcurcode"] == null)
            {
                HttpCookie newCookie = new System.Web.HttpCookie("affcurcode");
                Response.Cookies.Add(newCookie);
            }

            HttpCookie currencyCodeCookie = Request.Cookies["affcurcode"];

            if (!string.IsNullOrEmpty(currencyCode))
            {
                currencyCodeCookie.Value = currencyCode;
                return currencyCode;
            }
            else
            {
                if (string.IsNullOrEmpty(currencyCodeCookie.Value))
                {
                    return string.Empty;
                }

                return currencyCodeCookie.Value;
            }
        }

        protected string GetRoomName(ProviderInfo offer)
        {
            if (offer == null)
            {
                return string.Empty;
            }

            string name = "<span>" + offer.Name + "</span>";

            //if (!string.IsNullOrEmpty(offer.OfferName))
            //{
            //    name += "<span class='type-room'>(";
            //    name += offer.OfferName;
            //    name += ")</span>";
            //}

            return name;
        }

        protected string GetRoomPrice(ProviderInfo offer)
        {
            if (offer == null)
            {
                return string.Empty;
            }

            decimal price = offer.Price;
            return Convert.ToString(offer.Price);
        }

        protected string GetVariantSkuAndProviderID(ProviderInfo roomDetails)
        {
            try
            {
                if (roomDetails == null)
                {
                    return string.Empty;
                }
                string name = roomDetails.VariatSku + "," + roomDetails.PriceCurrency + "," + roomDetails.ProviderID + "," + roomDetails.Name + "," + roomDetails.Sku + "," + roomDetails.BookingTokenForBB + "," + roomDetails.BBMealBasisId + "," + roomDetails.BBMarginId + "," + roomDetails.ProviderType + "," + roomDetails.UComCurrencyId;
                return name;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, this);
                throw;
            }
        }
        /// <summary>
        /// Bind star ranking for Hotels
        /// </summary>
        /// <param name="starRepeater"></param>
        /// <param name="ranking"></param>
        private void BindBBStarRankings(Repeater starRepeater, string ranking)
        {
            ///Bind star rank
            if (!string.IsNullOrWhiteSpace(ranking))
            {
                decimal starRank;

                bool result = decimal.TryParse(ranking, out starRank);

                if (result)
                {
                    int count = Convert.ToInt32(starRank);
                    List<int> listOfStars = Enumerable.Repeat(count, count).ToList();
                    starRepeater.DataSource = listOfStars;
                    starRepeater.DataBind();
                }
                else
                {
                    Diagnostics.WriteException(DiagnosticsCategory.ClientPortal,
                        new AffinionException("Unable to parse star rank: BB star Rating"), this);
                }
            }
        }


        private static string GetMargin(string providerId, string arrivalDate)
        {
            DataTable marginData = TroikaDataController.GetMarginForEAchBBHotel(providerId, arrivalDate.ToString());
            string marginId = marginData.Rows[0][0].ToString();
            string marginPercentage = marginData.Rows[0][1].ToString();
            return marginId + "," + marginPercentage;
        }

        private BedBanksCurrency GetActualCurrency(Item country)
        {

            Item currencyItem = SitecoreFieldsHelper.GetLookupFieldTargetItem(country, "Currency");
            var currencies = Sitecore.Context.Database.SelectItems(path);

            BedBanksCurrency actualCurr = null;
            foreach (var item in currencies)
            {
                Item ucomCurr = SitecoreFieldsHelper.GetLookupFieldTargetItem(item, "uCommerce Currency Id");
                if (ucomCurr != null && (ucomCurr.ID == currencyItem.ID))
                {
                    actualCurr = new BedBanksCurrency();
                    actualCurr.BedBanksCurrencyId = item.Fields["Bed Banks Currency Id"].Value;
                    actualCurr.CurrencyName = item.Fields["Currency Name"].Value;
                    actualCurr.uCommerceCurrencyId = item.Fields["uCommerce Currency Id"].Value;
                    break;
                }
            }
            return actualCurr;
        }

        /// <summary>
        /// Adding room Info
        /// </summary>
        /// <param name="variatSku"></param>
        /// <param name="roomName"></param>
        /// <param name="currencyID"></param>
        /// <returns></returns>
        private AddRoomRequest RoomInfo(string variatSku, string roomName, int currencyID)
        {

            AddRoomRequest roomre = new AddRoomRequest();
            roomre.CheckinDate = searchTerm.CheckinDate;
            roomre.CheckOutDate = searchTerm.CheckoutDate;
            roomre.VariantSku = variatSku;
            roomre.NoOfRooms = 1;
            roomre.NoOfAdults = searchTerm.NumberOfAdults;
            roomre.NoOfChildren = searchTerm.NumberOfChildren;
            roomre.NoofNights = searchTerm.NoOfNights;
            roomre.ChildrenAges = inputChildAges;
            roomre.BookingThrough = BookingMethod.Online.ToString();
            roomre.CreatedBy = SitecoreUserProfileHelper.GetItem(UserProfileKeys.CreatedBy);
            roomre.RoomType = roomName;
            roomre.CurrencyId = currencyID;
            roomre.ClientId = clientItem.ID.ToString();
            return roomre;

        }

        private AddBedBanksRequest RoomInfo(string variatSku, string roomName, int currencyID, int propertyId, string bookingToken, string marginId, string mealBasisId)
        {
            int temp = 0;
            AddBedBanksRequest roomReq = new AddBedBanksRequest();
            roomReq.PropertyID = propertyId;
            roomReq.ClientId = searchTerm.Client.ID.ToString();
            roomReq.CheckinDate = searchTerm.CheckinDate;
            roomReq.CheckoutDate = searchTerm.CheckoutDate;
            roomReq.NoofNights = searchTerm.NoOfNights;
            roomReq.NoOfAdults = searchTerm.NumberOfAdults;
            roomReq.NoOfChildren = searchTerm.NumberOfChildren;
            roomReq.ChildrenAges = inputChildAges;
            roomReq.RoomInfo = roomName;
            roomReq.PreBookingToken = bookingToken;
            roomReq.BookingThrough = BookingMethod.Online.ToString();
            roomReq.NoOfRooms = searchTerm.NumberOfRooms;
            roomReq.RoomType = ProductType.BedBanks.ToString();
            roomReq.NoofNights = searchTerm.NoOfNights;
            roomReq.CurrencyId = currencyID;
            roomReq.IsDirect = int.TryParse(bookingToken, out temp).ToString();
            int marginIdApplied = 0;
            int.TryParse(marginId, out marginIdApplied);
            roomReq.MarginIdApplied = marginIdApplied;
            roomReq.MealBasisID = mealBasisId;
            roomReq.PropertyName = hotelDetails.PropertyName;
            roomReq.ImageUrl = hotelDetails.PropertyDetails.CMSBaseURL + hotelDetails.PropertyDetails.MainImage;
            double tempRanking = 0;
            int ranking = 0;
            double.TryParse(hotelDetails.Rating, out tempRanking);
            int.TryParse(Math.Round(tempRanking).ToString(), out ranking);
            roomReq.StarRanking = ranking;
            roomReq.MaxAdults = int.Parse(hotelDetails.PropertyDetails.MaxAdults);
            roomReq.MaxChildren = int.Parse(hotelDetails.PropertyDetails.MaxChildren);
            roomReq.MinAdults = int.Parse(hotelDetails.PropertyDetails.MinAdults);
            roomReq.OldPrice = hotelDetails.RoomTypes.Select(x=>x.PriceToDisplay).FirstOrDefault();
            roomReq.BBMarginPercentage = GetMarginAmount(hotelDetails, searchTerm.CheckinDate.ToString());
            roomReq.PropertyReferenceId = propertyRefID;
            return roomReq;
            //ad price
        }

        /// <summary>
        /// Create basket information in session
        /// </summary>
        private IProduct CreateBasketSession(string variantSku, string providerid, string SKU,string provider)
        {
            // Create a session to store CheckinDate, CheckoutDate, NoOfRooms, NoOfAdults, NoOfChildren
            Uri pageUri = Request.Url;
            QueryStringHelper helper = new QueryStringHelper(pageUri);
            IProduct outRoom = null;

            ProviderSearch providerInfo = new ProviderSearch();
            providerInfo.ProviderId = SKU;
            providerInfo.SitecoreItemId = null;
            if (provider.Equals(ProviderType.LoyaltyBuild.ToString()))
            {
                providerInfo.ProviderType = ProviderType.LoyaltyBuild;

                ///Getting Products of Hotel 
                var hotel = searchhelper.SearchHelper.GetProviderInfo(mySearchKey, providerInfo);

                //var OfferGroup = helper.GetValue("OfferGroup");
                foreach (var tmpItem in hotel.Products)
                {
                    if (tmpItem.VariantSku == variantSku)
                    {
                        outRoom = tmpItem;
                        break;
                    }
                }
            }
            else if(provider.Equals(ProviderType.BedBanks.ToString()))
            {
                providerInfo.ProviderType = ProviderType.BedBanks;

                //Getting Product details of Bedbank hotel
                outRoom = new BedBankProduct();
                //outRoom.CurrencyId=0;
                outRoom.Name = hotelDetails.RoomTypes.Select(x => x.RoomType).FirstOrDefault();               
                outRoom.Price=decimal.Parse(hotelDetails.RoomTypes.Select(x=>x.PriceToDisplay).FirstOrDefault());
                outRoom.Sku = hotelDetails.RoomTypes.Select(x => x.Sku).FirstOrDefault();
                outRoom.VariantSku = hotelDetails.RoomTypes.Select(x => x.VariantSku).FirstOrDefault();
                outRoom.Type = ProductType.BedBanks;
            }
            
            BasketInfo basketInfo = new BasketInfo
            {
                VariantSku = variantSku,
                SupplierId = providerid,
                CheckinDate = helper.GetValue("CheckinDate"),
                CheckoutDate = helper.GetValue("CheckoutDate"),
                Nights = helper.GetValue("NoOfNights"),
                Location = helper.GetValue("Destination"),
                NoOfRooms = helper.GetValue("NoOfRooms"),
                NoOfAdults = helper.GetValue("NoOfAdults"),
                NoOfChildren = helper.GetValue("NoOfChildren"),
                OfferGroup = string.Empty,
                BookingThrough = BookingMethod.Online.ToString(),
                ChildrenAges = inputChildAges
            };

            Session["BasketInfo"] = basketInfo;

            List<BasketInfo> listBasketInfo = new List<BasketInfo>();

            listBasketInfo.Add(basketInfo);

            // bind the unique value to session
            //Session["listBasketInfo"] = listBasketInfo.Select(p => p.Sku).Distinct();
            listBasketInfo = listBasketInfo.Distinct().ToList();
            Session["listBasketInfo"] = listBasketInfo;

            return outRoom;
        }

        protected void ButtonAddToBasket1_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Add to basket
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ButtonAddToBasket_Click(object sender, EventArgs e)
        {
            try
            {
                Button button = (sender as Button);
                int qty = 0;

                //Get the command argument
                string[] commandArgument = button.CommandArgument.Split(',');

                // TODO : Check the Qty
                foreach (var item in button.Parent.Controls)
                {
                    if (item.GetType().Name.Equals("DropDownList"))
                    {
                        //qty = (item as DropDownList).SelectedIndex;
                        bool isValid = Int32.TryParse((item as DropDownList).SelectedValue, out qty);
                    }
                }

                if (qty == 0)
                    return;

                // Create basket session
                //CreateBasketSession(commandArgument[0], commandArgument[2]);

                // Add product to basket, get necessary information form session and some are from button command arguement
                //BasketHelper.AddToBasket(commandArgument, qty, (BasketInfo)Session["BasketInfo"]);
                Response.Redirect("/Basket");
            }
            catch (Exception ex)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Add to basket in One click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ButtonAddToBasketOneClick_Click(object sender, EventArgs e)
        {
            Button btnAddToBasket = sender as Button;
            int qty = 0;
            List<BasketData> BasketDataList = new List<BasketData>();

            try
            {
                if (btnAddToBasket != null)
                {
                    foreach (RepeaterItem repeaterControls in this.OffersList.Controls)
                    {
                        //foreach (RepeaterItem repeaterControls in item.Parent.Controls)
                        //{
                        // Get the Item only from Item Template section (Header, Item and Footer are the section of ASP.Repeater)
                        if (repeaterControls.ItemType == ListItemType.Item || repeaterControls.ItemType == ListItemType.AlternatingItem)
                        {
                            DropDownList drpQty = repeaterControls.Controls[0].FindControl("drpAvailability") as DropDownList;
                            Button buttonAdd = repeaterControls.Controls[0].FindControl("ButtonAddToBasket1") as Button;

                            if (drpQty != null)
                            {
                                bool isValid = Int32.TryParse((drpQty).SelectedValue, out qty);
                            }

                            if (buttonAdd != null)
                            {
                                //Get the command argument
                                string[] commandArgument = buttonAdd.CommandArgument.Split(',');

                                if (qty > 0)
                                {
                                    BasketData basketData = new BasketData() { Qty = qty, CommandArgument = commandArgument };
                                    BasketDataList.Add(basketData);
                                }
                            }
                        }
                        // }
                    }

                    // Refresh the page when add to basket success (qty > 0 also ideal)
                    if (BasketDataList.Count > 0)
                    {
                        foreach (BasketData item in BasketDataList)
                        {
                            int currencyID = Convert.ToInt32(item.CommandArgument[1]);
                            IProduct outRoom = CreateBasketSession(item.CommandArgument[0], item.CommandArgument[2], item.CommandArgument[4],item.CommandArgument[8]);
                            if (outRoom is Room)
                            {
                                for (int k = 0; k < item.Qty; k++)
                                {
                                    Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.AddToBasket(outRoom, 1, RoomInfo(item.CommandArgument[0], item.CommandArgument[3], currencyID));
                                }
                            }
                            else if (outRoom is BedBankProduct)
                            {
                                for (int k = 0; k < item.Qty; k++)
                                {
                                    int bbPropertyId = Convert.ToInt32(item.CommandArgument[2]);
                                    Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.AddToBasket(outRoom, 1, RoomInfo(item.CommandArgument[0], item.CommandArgument[3], currencyID, bbPropertyId, item.CommandArgument[4], item.CommandArgument[5], item.CommandArgument[6]));
                                }
                            }
                        }

                        BasketDataList.Clear();
                        //Refresh the page                                    
                        Response.Redirect("/Basket");
                    }

                }
            }
            catch (Exception ex)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, ex.Message);
                throw;
            }
        }

        protected void ButtonBBAddToBasketOneClick_Click(object sender, EventArgs e)
        {
            Button btnAddToBasket = sender as Button;
            int qty = 0;
            List<BasketData> BasketDataList = new List<BasketData>();

            try
            {
                if (btnAddToBasket != null)
                {
                    foreach (RepeaterItem repeaterControls in this.roomTypeBBRepeater.Controls)
                    {
                        //foreach (RepeaterItem repeaterControls in item.Parent.Controls)
                        //{
                        // Get the Item only from Item Template section (Header, Item and Footer are the section of ASP.Repeater)
                        if (repeaterControls.ItemType == ListItemType.Item || repeaterControls.ItemType == ListItemType.AlternatingItem)
                        {
                            DropDownList drpQty = repeaterControls.Controls[0].FindControl("availabilityDDL") as DropDownList;
                            Button buttonAdd = repeaterControls.Controls[0].FindControl("ButtonBBAddToBasket1") as Button;

                            if (drpQty != null)
                            {
                                bool isValid = Int32.TryParse((drpQty).SelectedValue, out qty);
                            }

                            if (buttonAdd != null)
                            {
                                //Get the command argument
                                string[] commandArgument = buttonAdd.CommandArgument.Split(',');

                                if (qty > 0)
                                {
                                    BasketData basketData = new BasketData() { Qty = qty, CommandArgument = commandArgument };
                                    BasketDataList.Add(basketData);
                                }
                            }
                        }
                        // }
                    }

                    // Refresh the page when add to basket success (qty > 0 also ideal)
                    if (BasketDataList.Count > 0)
                    {
                        foreach (BasketData item in BasketDataList)
                        {
                            int currencyID = 0; 
                            
                           
                            IProduct outRoom = CreateBasketSession(item.CommandArgument[0], item.CommandArgument[2], item.CommandArgument[4],item.CommandArgument[8]);
                            if (outRoom is Room)
                            {
                                int.TryParse(item.CommandArgument[1], out currencyID);
                                for (int k = 0; k < item.Qty; k++)
                                {
                                    Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.AddToBasket(outRoom, 1, RoomInfo(item.CommandArgument[0], item.CommandArgument[3], currencyID));
                                }
                            }
                            else if (outRoom is BedBankProduct)
                            {
                                int.TryParse(item.CommandArgument[9], out currencyID);
                                for (int k = 0; k < item.Qty; k++)
                                {
                                    int bbPropertyId = Convert.ToInt32(item.CommandArgument[2]);                                                                                      
                                    Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.AddToBasket(outRoom, 1, RoomInfo(item.CommandArgument[0], item.CommandArgument[3], currencyID, bbPropertyId, item.CommandArgument[5], item.CommandArgument[7], item.CommandArgument[6]));
                                }
                            }
                        }

                        BasketDataList.Clear();
                        //Refresh the page                                    
                        Response.Redirect("/Basket");
                    }

                }
            }
            catch (Exception ex)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, ex.Message);
                throw;
            }
        }
    }
}