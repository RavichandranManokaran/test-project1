﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           ILogger.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Used to log diagnostic data
/// </summary>
#region Using Directives

using System;

#endregion

namespace Affinion.LoyaltyBuild.Common.Instrumentation
{
    /// <summary>
    /// Interface which should be implemented by Logger class. 
    /// </summary>
    public interface ILogger
    {
        #region Public Methods


        /// <summary>
        /// Add item to write the message to logs.
        /// </summary>
        /// <param name="message">The message.</param>     
        /// <param name="owner">Owner</param>
        void Trace(string message, object owner);

        /// <summary>
        /// Add item to write the exception to log.
        /// </summary>
        /// <param name="categoryName">Name of the category.</param>
        /// <param name="exception">The exception.</param>      
        void WriteException(DiagnosticsCategory categoryName, Exception exception, object owner);

        #endregion
    }
}
