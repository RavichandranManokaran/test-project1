﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.SessionState;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Handler.Offer
{
    /// <summary>
    /// Summary description for OfferRoomAvailibilityData
    /// </summary>
    public class OfferRoomAvailibilityData : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                context.Response.ContentType = "text/plain";
                var jss = new JavaScriptSerializer();
                AvailabilityHelper provider = new AvailabilityHelper(context);
                string json = new StreamReader(context.Request.InputStream).ReadToEnd();
                List<ChangeOfferRoomAvailabilityByDate> sData = jss.Deserialize<List<ChangeOfferRoomAvailabilityByDate>>(json);
                provider.SaveOfferRoomAvailibility(sData);

                context.Response.Write("SUCCESS");

            }
            catch (Exception er)
            {
                context.Response.Write("ERROR" + er.Message.ToString());
                Sitecore.Diagnostics.Log.Error(er.Message, new object());
            }
        }




        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}