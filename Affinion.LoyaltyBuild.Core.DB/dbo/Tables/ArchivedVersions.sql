﻿CREATE TABLE [dbo].[ArchivedVersions] (
    [VersionId]    UNIQUEIDENTIFIER NOT NULL,
    [ArchivalId]   UNIQUEIDENTIFIER NOT NULL,
    [ItemId]       UNIQUEIDENTIFIER NOT NULL,
    [Language]     NVARCHAR (50)    NULL,
    [Version]      INT              NULL,
    [ArchivedDate] DATETIME         NOT NULL,
    [ArchivedBy]   NVARCHAR (50)    NULL,
    [DAC_Index]    INT              NULL,
    CONSTRAINT [PK_ArchivedVersions] PRIMARY KEY NONCLUSTERED ([VersionId] ASC)
);


GO
CREATE CLUSTERED INDEX [DAC_Clustered]
    ON [dbo].[ArchivedVersions]([DAC_Index] ASC);

