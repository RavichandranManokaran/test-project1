﻿CREATE PROC dbo.ReplaceTableNamesInRelatedProcedure
(
	@OldTableName VARCHAR(255)
	, @NewTableName VARCHAR(255)
)
/*   -- Comments --
To rename a Table_Name in all the Stored Procedures
*/
AS
SET NOCOUNT ON;
BEGIN
	IF ISNULL(@OldTableName, '') = ''
	BEGIN
		SELECT 'Please provide value for @OldTableName' AS [Message]
		RETURN -1 
	END
	IF ISNULL(@NewTableName, '') = ''
	BEGIN
		SELECT 'Please provide value for @NewTableName' AS [Message]
		RETURN -1 
	END

	--Play with Cursor
	DECLARE @ProcedureText VARCHAR(MAX)
		, @ProcedureName VARCHAR(255)
		, @Table_Name VARCHAR(255)
		, @SQL NVARCHAR(MAX)
	SET @Table_Name = '%' + @OldTableName + '%'
	DECLARE MyCur CURSOR
	FAST_FORWARD FOR 
	SELECT SO.name 
		, SC.text
	FROM sys.syscomments SC 
		INNER JOIN sys.objects SO
			ON SO.object_id = SC.id
	WHERE SO.type = 'P'
		AND SC.text LIKE @Table_Name
	OPEN MyCur
	FETCH NEXT FROM MyCur INTO @ProcedureName, @ProcedureText 

	WHILE @@FETCH_STATUS = 0
	BEGIN
		BEGIN TRAN
			BEGIN TRY
				SET @SQL = 'DROP PROC '+ @ProcedureName				
				EXECUTE SP_EXECUTESQL @SQL
				SET @SQL = REPLACE(@ProcedureText, @OldTableName, @NewTableName)				
				EXECUTE SP_EXECUTESQL @SQL				
				--SELECT @OldTableName as 'Old_Table_Name', @ProcedureName as 'Procedure_Name', 'Success'
			END TRY
			BEGIN CATCH
				ROLLBACK TRAN
				CLOSE MyCur
				DEALLOCATE MyCur
				RETURN -1
			END CATCH
		COMMIT TRAN		
		FETCH NEXT FROM MyCur INTO @ProcedureName, @ProcedureText 
	END
	CLOSE MyCur 
	DEALLOCATE MyCur 
	
END
