﻿CREATE PROCEDURE [dbo].[LB_GetHourlyBooking]
@currentTime as datetime
as
SELECT 
   Value as [Provider Name],
   count(Value) as 'Booking(s)'
FROM
     uCommerce_OrderProperty AS OP
Join
     LB_OrderLineStatusAudit AS OS
On OP.OrderLineId = OS.OrderLineId
WHERE [KEY]  = '_clientid' and
     CAST(os.CreatedOn AS DATE)=CAST(@currentTime as date) AND  CAST(OS.CreatedOn AS time)<=CAST(@currentTime as time) and CAST(OS.CreatedOn AS TIME)>CAST(DATEADD(HOUR, -1, @currentTime) AS TIME)
     and
	  (OS.StatusId = 6 or -- If the booking is confirmed
              OS.StatusId = 5) --If the booking is provisonal
Group By
     Value

