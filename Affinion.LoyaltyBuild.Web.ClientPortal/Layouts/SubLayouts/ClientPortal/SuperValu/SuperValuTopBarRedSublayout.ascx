﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SuperValuTopBarRedSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.SuperValuTopBarRedSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="container-outer-inner width-full header-top-bar">
    <div class="container">
        <div class="row coomon-outer-padding">

            <div class="col-md-6 col-xs-6">
                <div class="logo">
                    <a href="#">
                        <sc:Placeholder ID="logo" Key="logo" runat="server" />
                    </a>
                </div>
            </div>

            <div class="col-md-6 col-xs-6">
                <div class="header-right-content-nav width-full">
                    <div class="container-fluid">
                        <nav class="navbar navbar-default no-margin-bottom">
                            <div class="container-fluid">
                                <div class="navbar-header">
                                    <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>
                                <div class="navbar-collapse collapse" id="navbar" aria-expanded="false" style="height: 1px;">
                                    <ul class="nav navbar-nav">
                                        <sc:Placeholder ID="toplinks" Key="toplinks" runat="server" />
                                    </ul>
                                </div>
                                <!--/.nav-collapse -->
                            </div>
                            <!--/.container-fluid -->
                        </nav>
                    </div>
                </div>
                <div class="tp-number float-right">
                    <p>
                        <sc:Placeholder ID="topcallnowlink" Key="topcallnowlink" runat="server" />
                    </p>
                </div>
            </div>

        </div>
    </div>
</div>
