﻿CREATE FUNCTION [dbo].[LB_FnIsPaidInFull]
(
	@OrderLineId int
)
RETURNS BIT
AS
BEGIN
	DECLARE @IspaidInFull BIT
	SET @IspaidInFull = 0
	SELECT TOP 1 @IspaidInFull = 1 FROM uCommerce_OrderProperty OP
		INNER JOIN uCommerce_OrderLine OL ON OL.OrderId = OP.OrderId
	WHERE  
		OL.OrderLineId = @OrderLineId
		AND OP.[Key]='PaymentMethod'
		AND OP.Value IN ('Full','Partial')

	RETURN @IspaidInFull
END
