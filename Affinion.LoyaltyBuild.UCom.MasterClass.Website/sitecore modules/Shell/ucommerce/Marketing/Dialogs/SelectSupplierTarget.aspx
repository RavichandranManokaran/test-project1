﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelectSupplierTarget.aspx.cs" MasterPageFile="../../masterpages/Dialog.master" Inherits="UCommerce.Web.UI.Marketing.Dialogs.SelectSupplierTarget" %>

<%@ Import Namespace="UCommerce.Web.UI.Catalog.Dialogs" %>
<%@ Register Namespace="UCommerce.Web.UI.Catalog.Dialogs" Assembly="UCommerce.Admin" TagPrefix="uc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeaderLabel" runat="server">
    <asp:Localize ID="Localize1" runat="server" meta:resourceKey="Header" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentArea" runat="server">
    <div class="propertyPane dialog-header">
        <h3>Select Supplier
        </h3>
        <p class="guiDialogTiny">
            Supplier List
        </p>
    </div>
    <div class="propertyPane contentCatalogItems">
        <div class="propertyItems">
            <asp:ListBox
                ID="listSupplier"
                Rows="20"
                runat="server" SelectionMode="Single"  />
            <%--<% Response.Write(CatalogItemType.Category); %>--%>
            <%--<uc:CatalogItemSelector runat="server" id="catalogItemSelector" SelectType="Category"
				MultipleSelect="false" />--%>
        </div>
    </div>
    <div class="propertyPane dialog-actions">
        <div class="footerOkCancel">
            <asp:Button ID="SaveButton" CssClass="dialog-saveButton" runat="server" meta:resourcekey="SaveButton" OnClick="SaveButton_Clicked" />
            <em>Cancel </em><a href="#" class="dialog-cancelButton" onclick="UCommerceClientMgr.closeModalWindow()">Cancel Button
            </a>
        </div>
    </div>
</asp:Content>
