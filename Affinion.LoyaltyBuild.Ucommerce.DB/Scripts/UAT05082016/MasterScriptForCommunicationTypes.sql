﻿SET IDENTITY_INSERT [dbo].[LB_CommunicationTypes] ON 

INSERT [dbo].[LB_CommunicationTypes] ([CommunicationTypeId], [Name]) VALUES (1, N'ProviderConfirmation')
INSERT [dbo].[LB_CommunicationTypes] ([CommunicationTypeId], [Name]) VALUES (2, N'CustomerConfirmation')
INSERT [dbo].[LB_CommunicationTypes] ([CommunicationTypeId], [Name]) VALUES (3, N'ProviderCancellation')
INSERT [dbo].[LB_CommunicationTypes] ([CommunicationTypeId], [Name]) VALUES (4, N'CustomerCancellation')
INSERT [dbo].[LB_CommunicationTypes] ([CommunicationTypeId], [Name]) VALUES (5, N'ProviderProvisionalConfirmation')
INSERT [dbo].[LB_CommunicationTypes] ([CommunicationTypeId], [Name]) VALUES (6, N'ClientProvisionalConfirmation')
INSERT [dbo].[LB_CommunicationTypes] ([CommunicationTypeId], [Name]) VALUES (7, N'ManageBookingMailConfirmation')
INSERT [dbo].[LB_CommunicationTypes] ([CommunicationTypeId], [Name]) VALUES (8, N'PartialConfirmationEmailRemainder')
INSERT [dbo].[LB_CommunicationTypes] ([CommunicationTypeId], [Name]) VALUES (9, N'CustomerConfirmationOnline')
INSERT [dbo].[LB_CommunicationTypes] ([CommunicationTypeId], [Name]) VALUES (10, N'ProviderConfirmationOnline')
INSERT [dbo].[LB_CommunicationTypes] ([CommunicationTypeId], [Name]) VALUES (11, N'PartialCustomerConfirmationOnline')
SET IDENTITY_INSERT [dbo].[LB_CommunicationTypes] OFF