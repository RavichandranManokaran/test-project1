﻿<%@ Page Language="c#" CodePage="65001" AutoEventWireup="true" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.PrimaryLayouts.ClientPortal.ClientPortalPrimaryLayout" CodeBehind="ClientPortalPrimaryLayout.aspx.cs" %>

<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ OutputCache Location="None" VaryByParam="none" %>
<!DOCTYPE html>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title id="pageTitle" runat="server"></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="CODE_LANGUAGE" content="C#" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5" />
    <%--  <link href="/default.css" rel="stylesheet" />--%>
    <sc:VisitorIdentification runat="server" />

    <%--  --%>

    <!-- Meta, title, CSS, favicons, etc. -->
    <%--<meta charset="utf-8">--%>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="Bootstrap, a sleek, intuitive, and powerful mobile first front-end framework for faster and easier web development." />
    <meta name="keywords" content="HTML, CSS, JS, JavaScript, framework, bootstrap, front-end, frontend, web development" />
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors" />

    <%=this.RenderJavascriptVariables()%>

    <script type="text/javascript" src="/Resources/scripts/jquery.min.js"></script>
    <script type="text/javascript" src="/Resources/scripts/jquery-ui.js"></script>
    <script type="text/javascript" src="/Resources/bootstrap_assets/javascripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="/Resources/scripts/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="/Resources/scripts/jquery.simplePagination.js"></script>
        <script type="text/javascript" src="/Resources/scripts/jquery.nanoscroller.js"></script>
    <%--<script type="text/javascript" src="/Resources/scripts/callscroller.js"></script>--%>

    

    <link rel="stylesheet" href="/Resources/styles/jquery-ui.css" />
    <link rel="stylesheet" href="/Resources/styles/jquery-ui.theme.min.css" />
    <link rel="stylesheet" href="/Resources/styles/bootstrap.min.css" />
    <link rel="stylesheet" href="/Resources/styles/jquery.bxslider.css" />
    <link rel="stylesheet" href="/Resources/styles/affinion.min.css" />
    <link rel="stylesheet" href="/Resources/styles/affinion-dev.css" />
     <link rel="stylesheet" href="/Resources/styles/simplePagination.css"  />
   <link rel="stylesheet" href="/Resources/styles/<%= this.selectedTheme %>/theme.css" />
  <link rel="stylesheet" href="/Resources/styles/affinion.custom.css" />

    <!-- theme css -->
    <!--<link rel="stylesheet" href="/Resources/styles/theme-red.css" />-->


    <link rel="stylesheet" href="/Resources/styles/fonts.css" />
    <link rel="stylesheet" href="/Resources/styles/font-awesome.min.css" />
    <link rel="stylesheet" href="/Resources/styles/bootstrap-social.css" />
    <link rel="shortcut icon" type="image/x-icon" href="/Resources/images/favicon.ico" />
   <%-- <link rel="shortcut icon" type="image/x-icon" href="<%=this.Favicon %>" />--%>
 
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&sensor=true"></script>
    <script type="text/javascript" src="/Resources/scripts/CardCheck.js"></script>
    <script type="text/javascript">

        jQuery(function () {

            jQuery(document).on('scroll', function () {

                if (jQuery(window).scrollTop() > 100) {
                    jQuery('.scroll-top-wrapper').addClass('show');
                } else {
                    jQuery('.scroll-top-wrapper').removeClass('show');
                }
            });

            jQuery('.scroll-top-wrapper').on('click', scrollToTop);

            // $('body').on('click', '[href^=#]', function (e) { e.preventDefault() });
        });

        function scrollToTop() {
            verticalOffset = typeof (verticalOffset) != 'undefined' ? verticalOffset : 0;
            element = jQuery('body');
            offset = element.offset();
            offsetTop = offset.top;
            jQuery('html, body').animate({ scrollTop: offsetTop }, 500, 'linear');
        }

        jQuery(function () {
            jQuery("#hide-show").click(function () {
                jQuery("#hide-show-expe").toggle();
            });
        });



        function check_box_hide() {
            jQuery("#expandr").hide();
            jQuery("#expand").click(function () {
                if (jQuery(this).is(":checked")) {
                    jQuery("#hide-show-expe").show();
                } else {
                    jQuery("#hide-show-expe").hide();
                }
            });
        }

        jQuery(document).ready(function () {
            check_box_hide();
        });

        //function load() {
        //    var y = document.getElementsByClassName("ui-datepicker-trigger")[0].classList.add("btn", "btn-default", "glyphicon", "glyphicon-calendar", "calander-icon", "pull-right");
        //    var x = document.getElementsByClassName("ui-datepicker-trigger")[1].classList.add("btn", "btn-default", "glyphicon", "glyphicon-calendar", "calander-icon", "pull-right");
        //}

    </script>
   

</head>

<body class="cbp-spmenu-push">

    <form method="post" runat="server" id="mainform">

        <div class="container-main container-outer width-full overflow-hidden-att">
            <div class="container-outer-inner width-full header-top-bar">

                <sc:Placeholder ID="topbar" Key="topbar" runat="server" />

            </div>
            <div class="body-container">
                <div class="width-full">
                    <sc:Placeholder ID="fullwidthcontent" Key="fullwidthcontent" runat="server" />
                </div>
                <div class="container">
                    <sc:Placeholder ID="breadcrumb" Key="breadcrumb" runat="server" />
                    <sc:Placeholder ID="maincontent" Key="maincontent" runat="server" />
                    <sc:Placeholder ID="advertisment" Key="advertismentarea" runat="server" />
                </div>

            </div>
        </div>

        <sc:Placeholder ID="footer" Key="footer" runat="server" />
    </form>





    <!-- ================================================== -->
    <script type="text/javascript">
        jQuery(document).ready(function () {



            jQuery('#lang-globe .dropdown-menu li a').click(function (e) {

                var getSelected = jQuery(this).text();
                setLanguage(getSelected);

            });

            /*Carousel: Init : data-ride='carousel':removed */
            jQuery(".carousel").carousel();


        });
    </script>
    <script lang="javascript" type="text/javascript">
        function fnCancelBooking() {
           
    }


</script>
</body>

</html>

