﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Tags.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Lists.Tags" %>

<asp:Repeater ID="rptTags" runat="server" OnItemDataBound="rptTags_ItemDataBound">
 <HeaderTemplate><div class="tags-widget sidebar-block"><h2><asp:Literal ID="SectionTitle" runat="server" /></h2><ul></HeaderTemplate>
 <ItemTemplate><li><p><i class="icon-tags"></i><asp:LinkButton ID="RelatedLink" runat="server" OnClick="SearchByTag" /></p></li></ItemTemplate>
 <FooterTemplate></ul></div></FooterTemplate>
</asp:Repeater>
