﻿//<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           PaymentGatewayHelper.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Helper methods related to payment gateways
/// </summary>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Common.Utilities
{
    public static class PaymentGatewayHelper
    {
        /// <summary>
        /// Generate hash using SHA1 algorithm for given string
        /// </summary>
        /// <param name="input">String to convert to hash</param>
        /// <returns>Returns hash</returns>
        public static string GetHash(string input)
        {
            using (SHA1 sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(input));
                return string.Join("", hash.Select(b => b.ToString("x2")).ToArray());
            }
        }

        /// <summary>
        /// Generate current time-stamp. Format: YYYYMMDDHHMMSS
        /// </summary>
        /// <returns>Returns current time-stamp</returns>
        public static string GetNowTimestamp()
        {
            DateTime TodaysDay = DateTime.Now;
            int DayNumber = TodaysDay.Day;
            int MonthNumber = TodaysDay.Month;
            string YearNumber = TodaysDay.Year.ToString();
            int HourNumber = TodaysDay.Hour;
            int MinNumber = TodaysDay.Minute;
            int SecNumber = TodaysDay.Second;
            string timestamp, DayNumberString, MonthNumberString, HourNumberString, MinNumberString, SecNumberString;

            if (DayNumber < 10)
            {
                DayNumberString = "0" + DayNumber.ToString();
            }
            else
            {
                DayNumberString = DayNumber.ToString();
            }

            if (MonthNumber < 10)
            {
                MonthNumberString = "0" + MonthNumber.ToString();
            }
            else
            {
                MonthNumberString = MonthNumber.ToString();
            }

            if (HourNumber < 10)
            {
                HourNumberString = "0" + HourNumber.ToString();
            }
            else
            {
                HourNumberString = HourNumber.ToString();
            }

            if (MinNumber < 10)
            {
                MinNumberString = "0" + MinNumber.ToString();
            }
            else
            {
                MinNumberString = MinNumber.ToString();
            }

            if (SecNumber < 10)
            {
                SecNumberString = "0" + SecNumber.ToString();
            }
            else
            {
                SecNumberString = SecNumber.ToString();
            }

            return timestamp = YearNumber + MonthNumberString + DayNumberString + HourNumberString + MinNumberString + SecNumberString;
        }

    }
}
