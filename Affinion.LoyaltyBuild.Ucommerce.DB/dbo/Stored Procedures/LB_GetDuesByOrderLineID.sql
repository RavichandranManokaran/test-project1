﻿
/* changes in sp with latest Table Structures*/
CREATE PROCEDURE [dbo].[LB_GetDuesByOrderLineID]
	@OrderLineId INT,
	@CurrencyTypeId INT=NULL
AS
BEGIN
	SELECT SUM([Total]) [Total], [Currency], MAX(OrderLineID)[OrderLineId] FROM
		(SELECT SUM(OLD.TotalAmount) [Total],
				OLD.CurrencyTypeId [Currency],
				MAX(OLD.OrderLineID) [OrderLineId]
		FROM uCommerce_OrderLine  OL
			INNER JOIN LB_OrderLineDue OLD ON OLD.OrderLineID = OL.OrderLineId 
		WHERE
			ISNULL(OLD.IsDeleted,0) = 0 AND 			
			OLD.CurrencyTypeId = (CASE WHEN @CurrencyTypeId IS NOT NULL THEN @CurrencyTypeId ELSE OLD.CurrencyTypeId END) AND			
			OLD.OrderLineID IN (SELECT OrderLineID FROM [dbo].[LB_FnRelatedOrderLines](@OrderLineId))
		GROUP BY 
			OLD.CurrencyTypeId
		UNION ALL
		SELECT -SUM(OLP.TotalAmount) [Total],
				OLP.CurrencyTypeId [Currency],
				MAX(OLP.OrderLineID) [OrderLineId]
		FROM uCommerce_OrderLine  OL
			INNER JOIN LB_OrderLinePayment OLP ON OLP.OrderLineID = OL.OrderLineId 
		WHERE
			OLP.CurrencyTypeId = (CASE WHEN @CurrencyTypeId IS NOT NULL THEN @CurrencyTypeId ELSE OLP.CurrencyTypeId END) AND			
			OLP.OrderLineID IN (SELECT OrderLineID FROM [dbo].[LB_FnRelatedOrderLines](@OrderLineId))
		GROUP BY 
			OLP.CurrencyTypeId) P
	GROUP BY [Currency]
	HAVING
		SUM([Total]) <> 0
END
