﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
 © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
 PROPRIETARY INFORMATION The information contained herein (the 
 'Proprietary Information') is highly confidential and proprietary to and 
 constitutes trade secrets of Affinion International. The Proprietary Information 
 is for Affinion International use only and shall not be published, 
 communicated, disclosed or divulged to any person, firm, corporation or 
 other legal entity, directly or indirectly, without the prior written 
 consent of Affinion International.
 */
namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    /// <summary>
    /// Entity to hold the accomodation information like supplier and offers
    /// This data comes from the sitecore
    /// </summary>
    public class SupplierInviteDetail
    {

        /// <summary>
        /// SupplierInviteId
        /// </summary>
        public int SupplierInviteId { get; set; }

        /// <summary>
        /// offer Name
        /// </summary>
        public string CampaignName { get; set; }


        /// <summary>
        /// Offer Description
        /// </summary>
        public string OfferDescription { get; set; }

        /// <summary>
        /// CampaignItemId
        /// </summary>
        public int CampaignItemId { get; set; }

        /// <summary>
        /// IsSubscibe
        /// </summary>
        public bool IsSubscibe { get; set; }

        /// <summary>
        /// ManageBySupplier
        /// </summary>
        public bool ManageBySupplier { get; set; }


        /// <summary>
        /// <list>OfferPrice</list>
        /// </summary>
        public List<OfferPrice> OfferPrice { get; set; }

    }

    public class CampaignOfferDetail
    {

        /// <summary>
        /// CampaignItemId
        /// </summary>
        public int CampaignItemId { get; set; }

        /// <summary>
        /// <list>OfferPrice</list>
        /// </summary>
        public List<OfferPrice> OfferPrice { get; set; }
    }

    public class OfferPrice
    {
        public OfferPrice()
        {
            this.ArrivalDate = new CampaginItemDateRange();
            this.ReservationDate = new CampaginItemDateRange();
            this.price = -1;
        }

        /// <summary>
        /// price
        /// </summary>
        public decimal price { get; set; }

        /// <summary>
        ///PriceIncluVat
        /// </summary>
        public decimal PriceIncluVat { get; set; }

        /// <summary>
        ///ArrivalDate
        /// </summary>
        public CampaginItemDateRange ArrivalDate { get; set; }

        /// <summary>
        ///ReservationDate
        /// </summary>
        public CampaginItemDateRange ReservationDate { get; set; }

        /// <summary>
        ///productSKU
        /// </summary>
        public string productSKU { get; set; }

        /// <summary>
        ///RoomName
        /// </summary>
        public string RoomName { get; set; }
    }
}
