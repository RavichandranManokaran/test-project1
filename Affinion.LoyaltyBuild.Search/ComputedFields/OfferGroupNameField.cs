﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SearchHelper.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Search
/// Description:           This field will generate and store the offer group/ package name
/// </summary>
namespace Affinion.LoyaltyBuild.Search.ComputedFields
{
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Search.Data;
    using Affinion.LoyaltyBuild.Search.Helpers;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Availability;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.CampaignTarget;
    using Newtonsoft.Json;
    using Sitecore.ContentSearch;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using UCommerce.EntitiesV2;
    using UCommerce.EntitiesV2.Definitions;

    /// <summary>
    /// This field will generate and store the offer group/ package name
    /// </summary>
    public class OfferGroupNameField : ComputedField
    {
        /// <summary>
        /// Calculates the field value
        /// </summary>
        /// <param name="indexable">The indexable item</param>
        /// <returns>Returns the calculated value</returns>
        public override object ComputeFieldValue(IIndexable indexable)
        {
            try
            {
                Item supplier = ContentDatabase.GetItem(ID.Parse(indexable.Id));
                List<CampaignOfferData> packages = new List<CampaignOfferData>();

                if (supplier == null)
                {
                    return JsonConvert.SerializeObject(packages);
                }

                List<Item> clientsList = new List<Item>();
                Item clientsFolder = ContentDatabase.GetItemByKey("ClientSetupFolder");

                if (clientsFolder != null)
                {
                    clientsList = clientsFolder.Axes.GetDescendants().Where(i => i.TemplateName == "ClientDetails").ToList<Item>();
                }

                foreach (Item client in clientsList)
                {
                    bool isSupplierSelected = SitecoreFieldsHelper.MultiListContains(client, "AllowedSuppliers", supplier);

                    if (isSupplierSelected)
                    {
                        Category supplierCategory = StoreHelper.GetUcommerceCategory(supplier);

                        if (supplierCategory != null)
                        {
                            packages.AddRange(GetCampaignOfferData(supplierCategory));
                        }

                        // index only if atleast one client has selected the supplier
                        break;
                    }
                }

                return JsonConvert.SerializeObject(packages);
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                return string.Empty;
            }
        }

        private static List<CampaignOfferData> GetCampaignOfferData(Category supplierCategory)
        {
            CampaignSearch campaignSearch = new CampaignSearch();
            CampaignTargets campaignTargets = new CampaignTargets();
            List<CampaignOfferData> packages = new List<CampaignOfferData>();
            List<CampaignItem> campaignItems = campaignSearch.GetCampaignItemsBySupplier(supplierCategory.Guid.ToString());

            foreach (CampaignItem campaignDetail in campaignItems)
            {
                CampaignOfferData tempData = new CampaignOfferData();
                // tempData.Name = campaignDetail.Name;
                IProperty offerTypeProperty = campaignDetail.GetProperty("OfferType");
                string offerType = offerTypeProperty != null ? offerTypeProperty.GetValue().ToString() : string.Empty;
                tempData.Name = offerType;

                // store applicable arrival dates
                foreach (var dateRange in campaignTargets.GetArrivalDateTargetByCampaignId(campaignDetail.CampaignItemId))
                {
                    tempData.ArrivalDates.Add(dateRange);
                }

                tempData.StartDate = campaignDetail.Campaign.StartsOn;
                tempData.EndDate = campaignDetail.Campaign.EndsOn;
                tempData.Enabled = campaignDetail.Campaign.Enabled && campaignDetail.Enabled;
                tempData.CampaignItemId = campaignDetail.CampaignItemId;

                tempData.NoOfNights = campaignTargets.GetNightTargetByCampaignId(campaignDetail.CampaignItemId);

                packages.Add(tempData);
            }

            return packages;
        }
    }
}
