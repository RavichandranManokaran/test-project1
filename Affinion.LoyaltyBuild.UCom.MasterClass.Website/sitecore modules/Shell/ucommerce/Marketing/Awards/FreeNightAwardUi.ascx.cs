﻿using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.EntitiesV2;
using UCommerce.Presentation.Views.Marketing;
using UCommerce.Presentation.Web;
using UCommerce.Presentation.Web.Controls;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.Ucommerce.Marketing.Awards
{
    public partial class FreeNightAwardUi : ViewEnabledControl<IEditCampaignItemView>, IConfigurable
    {


        protected void Page_Load(object sender, EventArgs e)
        {
            FreeNightAward dataSource = (FreeNightAward)this.DataSource;
            this.FreeNightLabel.Text = string.Format("Night Free: {0}", dataSource.NoOfNight.ToString());
            this.FreeNighText.Text = dataSource.NoOfNight.ToString();
            this.DecimalValidator.ErrorMessage = string.Format("{0}", "Please Enter Only Number");
            this.RequiredValidator.ErrorMessage = string.Format("{0}", "No Of Nights Required");
            this.DeleteButton.OnClientClick = string.Format("return confirm('{0}');", "Are you want to delete award");
        }

        // Methods
        protected void DeleteButton_Click(object sender, ImageClickEventArgs e)
        {
            if (this.AwardId != 0)
            {
                (from x in UCommerce.EntitiesV2.Award.All()
                 where x.AwardId == this.AwardId
                 select x).SingleOrDefault<UCommerce.EntitiesV2.Award>().Delete();
            }
            this.Page.Response.Redirect(this.Page.Request.Url.ToString(), true);
        }

        protected void EditButton_Click(object sender, ImageClickEventArgs e)
        {
            this.ToggleEditor();
        }

        public object New()
        {
            FreeNightAward freNightAward = FreeNightAward.All().FirstOrDefault(x => x.CampaignItem.CampaignItemId == View.CampaignItem.CampaignItemId) ?? new FreeNightAward();
            this.DataSource = freNightAward;
            ((FreeNightAward)this.DataSource).CampaignItem = base.View.CampaignItem;
            ((FreeNightAward)this.DataSource).NoOfNight = 0;
            if (string.IsNullOrEmpty(((FreeNightAward)this.DataSource).Name))
            {
                ((FreeNightAward)this.DataSource).Name = "Night Free";
            }
            ((FreeNightAward)this.DataSource).Save();
            return this.DataSource;
        }

        protected void SaveButton_Click(object sender, ImageClickEventArgs e)
        {
            FreeNightAward award = (this.DataSource == null) ? new FreeNightAward() : ((FreeNightAward)this.DataSource);
            CampaignItem campaignItem = base.View.CampaignItem;
            award.NoOfNight = Convert.ToInt32(this.FreeNighText.Text);
            award.CampaignItem = campaignItem;
            award.Save();
            this.FreeNightLabel.Text = string.Format("No. Of Night(s) Free: {0}", FreeNighText.Text.ToString());
            this.ToggleEditor();
        }

        private void ToggleEditor()
        {
            this.NonEditModePlaceHolder.Visible = !this.NonEditModePlaceHolder.Visible;
            this.EditButton.Visible = !this.EditButton.Visible;
            this.EditModePlaceHolder.Visible = !this.EditModePlaceHolder.Visible;
            this.SaveButton.Visible = !this.SaveButton.Visible;
        }

        // Properties
        public int AwardId
        {
            get
            {
                if (this.DataSource == null)
                {
                    return 0;
                }
                return ((UCommerce.EntitiesV2.Award)this.DataSource).AwardId;
            }
        }

        public int CampaignItemId
        {
            get
            {
                return base.View.CampaignItem.CampaignItemId;
            }
        }

        public object DataSource { get; set; }

    }



}