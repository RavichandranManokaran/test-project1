CREATE TABLE [dbo].[uCommerce_SeasonUnitDiscountAwardDetail](
	[SeasonUnitDiscountAwardDetailId] [int] IDENTITY(1,1) NOT NULL,
	[SeasonUnitDiscountAwardId] [int] NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[AmountOff] [decimal](18, 0) NOT NULL,
	[IsPercentage] [bit] NOT NULL,
	[OnOrderLine] [bit] NOT NULL,
 CONSTRAINT [PK_uCommerce_SeasonUnitDiscountAwardDetail] PRIMARY KEY CLUSTERED 
(
	[SeasonUnitDiscountAwardDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[uCommerce_SeasonUnitDiscountAwardDetail]  WITH CHECK ADD  CONSTRAINT [FK_uCommerce_SeasonUnitDiscountAwardDetail_uCommerce_SeasonUnitDiscountAward] FOREIGN KEY([SeasonUnitDiscountAwardId])
REFERENCES [dbo].[uCommerce_SeasonUnitDiscountAward] ([SeasonUnitDiscountAwardId])
GO

ALTER TABLE [dbo].[uCommerce_SeasonUnitDiscountAwardDetail] CHECK CONSTRAINT [FK_uCommerce_SeasonUnitDiscountAwardDetail_uCommerce_SeasonUnitDiscountAward]
GO


