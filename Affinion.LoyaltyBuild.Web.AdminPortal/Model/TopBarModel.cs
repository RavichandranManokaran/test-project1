﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Affinion.LoyaltyBuild.AdminPortal.Model
{
    public class TopBarModel
    {
        /// <summary>
        /// Text
        /// </summary>
        public string Text{ get; set; }

        /// <summary>
        /// LinkPath
        /// </summary>
        public string Link { get; set; }

    }
}