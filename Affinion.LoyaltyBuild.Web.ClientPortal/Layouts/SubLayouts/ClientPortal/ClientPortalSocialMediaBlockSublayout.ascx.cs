﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           ClientPortalSocialMediaBlockSublayout.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.ClientPortal
/// Description:           Bind data to media block 
/// </summary>

using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Data.Items;
using System;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts
{

    public partial class ClientPortalSocialMediaBlockSublayout : BaseSublayout
    {

        /// <summary>
        /// Page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                ///Get current data source
                Item item = this.GetDataSource();

                if (item != null)
                {

                    this.MediaBlock.DataSource = SitecoreFieldsHelper.GetMutiListItems(item, "Items");
                    this.MediaBlock.DataBind();
                }
            }
            catch (Exception ex)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Get CSS classes from field
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected static string GetCSSClasses(Item item)
        {
            try
            {
                string cssClasses = string.Empty;

                cssClasses = SitecoreFieldsHelper.GetDropLinkFieldValue(item, "MediaType", "CSS");

                return cssClasses;
            }
            catch (Exception ex)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, ex.Message);
                throw;
            }
        }
    }
}