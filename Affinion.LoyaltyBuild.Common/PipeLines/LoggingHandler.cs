﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           LoggedInHandler.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Extented of LoggingIn Pipeline
/// </summary>

using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Pipelines.LoggingIn;
using Sitecore.Web;
using System;
using System.Web;
using System.Web.Security;

namespace Affinion.LoyaltyBuild.Common.PipeLines
{
    /// <summary>
    /// Pipeline to handle user login interaction
    /// </summary>
    public class LoggingHandler
    {

        #region Private Variables
        private TimeSpan timeSpanToExpirePassword { get; set; }
        private TimeSpan timeSpanToUnlockUserAccount { get; set; }
        private string changePasswordPageUrl { get; set; }
        #endregion

        #region Private Constants
        private const string generalExceptionMessage = "Error occured";
        private const string userAccountLockExceptionMessage = "Message from LoyaltyBuild Administrator: Your account has been locked out because of too many invalid login attempts.";
        private const string queryStringUserParam = "user";
        #endregion

        #region Public Methods
        /// <summary>
        /// Proccess method of Logging pipeline
        /// </summary>
        /// <param name="args">Arguments</param>
        public void Process(LoggingInArgs args)
        {
            try
            {
                var currentUser = Membership.GetUser(args.Username);
                LogUser(args);
                HandleUnlockAccount(args);
                HandleExpiredPassword(currentUser);
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                throw;
            }
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Handle expired password of the currentUser.
        /// </summary>
        /// <param name="currentUser">Current User</param>
        private void HandleExpiredPassword(MembershipUser currentUser)
        {
            if (HasPasswordExpired(currentUser))
            {
                try
                {
                    string host = HttpContext.Current.Request.Url.Host;
                    UriBuilder uribuilder = new UriBuilder(host + changePasswordPageUrl);
                    QueryStringHelper queryHelper = new QueryStringHelper(uribuilder.Uri);
                    queryHelper.SetValue(queryStringUserParam, currentUser.UserName);

                    WebUtil.Redirect(queryHelper.GetUrl(true).AbsoluteUri);
                }
                catch (Exception ex)
                {
                    Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                    throw;
                }
            }
        }

        /// <summary>
        /// Check wheather password expired for current MembershipUser.
        /// </summary>
        /// <param name="user">User</param>
        /// <returns></returns>
        private bool HasPasswordExpired(MembershipUser user)
        {
            return user.LastPasswordChangedDate.ToLocalTime().Add(timeSpanToExpirePassword) <= DateTime.Now;
        }

        /// <summary>
        /// Logs the user logon attempts.
        /// </summary>
        /// <param name="args">Arguments</param>
        private static void LogUser(LoggingInArgs args)
        {
            var currentUser = Membership.GetUser(args.Username);
            if (currentUser.IsApproved && currentUser.IsOnline && !currentUser.IsLockedOut)
            {
                //TODO: Log the message for successful login

            }
            else
            {
                //TODO: Log the message for unsuccessful login
            }
        }

        /// <summary>
        /// Unlock the locked out user account.
        /// </summary>
        /// <param name="args">Arguments</param>
        private void HandleUnlockAccount(LoggingInArgs args)
        {
            var currentUser = Membership.GetUser(args.Username);

            if (currentUser != null)
            {
                if (currentUser.IsLockedOut)
                {
                    DateTime lastLockout = currentUser.LastLockoutDate.ToLocalTime();
                    DateTime unlockDate = lastLockout.Add(timeSpanToUnlockUserAccount);
                    if (DateTime.Now > unlockDate)
                    {
                        currentUser.UnlockUser();
                    }
                    else
                    {
                        args.Success = false;
                        args.AddMessage(userAccountLockExceptionMessage);
                    }
                }
            }
        }
        #endregion
    }
}
