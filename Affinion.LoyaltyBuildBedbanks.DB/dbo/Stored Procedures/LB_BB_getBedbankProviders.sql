

/****** Object:  StoredProcedure [dbo].[LB_BB_getBedbankProviders]    Script Date: 7/19/2016 7:25:07 PM ******/
---- ====================================================
---- Description:	RETURNS MARGIN FOR A PROPERTY BASED ON AVAILABILITY SOURCE
---- Written: RICHARD
---- Created: 2016.04.19
---- ====================================================

CREATE PROCEDURE [dbo].[LB_BB_getBedbankProviders]
	
	-- Input parameters 

	--Return parameters
	@po_returnstatus INT OUTPUT,
	@po_returnmsg VARCHAR(200) OUTPUT

	
AS
set nocount on;
BEGIN

	--Assume transaction will fail, set the return variable to -1.
	SET @po_returnstatus = -1;
	SET @po_returnmsg = null;

	
	
	BEGIN TRY
	
	Select ID, source from dbo.LB_BB_availabilityProvider
	
	

		      
    END TRY	
	BEGIN CATCH	  
		SET @po_returnstatus = error_number();
		SET @po_returnmsg = error_message();
		RETURN;
	END CATCH
	
	SET @po_returnstatus = 0;
	SET @po_returnmsg = 'Completed successfully.';	
END













GO

