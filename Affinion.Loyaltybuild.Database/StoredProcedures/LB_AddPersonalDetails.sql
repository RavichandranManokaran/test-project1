﻿CREATE  PROCEDURE [dbo].[LB_AddPersonalDetails]
	@FirstName as varchar(50),
	@LastName as varchar(50),
	@AddressLine1 as varchar(50),
	@AddressLine2 as varchar(50),
	@AddressLine3 as varchar(50),
	@CityTown as varchar(50),
	@CountyRegion as varchar(50),	
	@Country as varchar(50),
	@MobilePhone as varchar(50),
	@Phone as varchar(50),
	@Email as varchar(50),
	@ClientStore as varchar(50),
	@PostalCode as varchar(50) = 8000,
	@OrderId as int,
	@SendEmail as bit,
	@TermsAndConditions as bit
AS
BEGIN
DECLARE @CountryId as int
SELECT @CountryId = CountryId FROM uCommerce_Country WHERE LOWER(Name) = @Country

IF EXISTS (SELECT 1 FROM [dbo].LB_OrderAddress  WHERE OrderID = @OrderID)
	DELETE FROM [dbo].LB_OrderAddress  WHERE OrderID = @OrderID

INSERT INTO [dbo].LB_OrderAddress  
(FirstName,
LastName,
EmailAddress,
PhoneNumber,
MobilePhoneNumber,
AddressName,
Line1,
Line2,
Line3,
PostalCode,
City,
County,
CountryId,
Branch,
OrderId,
IsEmail,
IsTerms
)
 VALUES
  (@FirstName,
  @LastName,
  @Email,
  @Phone,
  @MobilePhone,
  @AddressLine1,
  @AddressLine1,
  @AddressLine2,
  @AddressLine3,
  @PostalCode,
  @CityTown,
  @CountyRegion,
  @CountryId,
  @ClientStore,
  @OrderId,
  @SendEmail,
  @TermsAndConditions
  )
END