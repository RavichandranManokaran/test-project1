﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           DateTimeHelper.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common.Utilities
/// Description:           Search helper
/// </summary>

#region Using Statements

using Affinion.LoyaltyBuild.Common.Instrumentation;
using System;
using System.Globalization;

#endregion

namespace Affinion.LoyaltyBuild.Common.Utilities
{
    public static class DateTimeHelper
    {
        #region Fields

        private static DateTime julianFixed = new DateTime(1990, 1, 1);

        public static string DateFormat
        {
            get
            {
                return "dd/MM/yyyy";
            }
        }

        public static string TimeFormat
        {
            get
            {
                return "hh:mm:ss";
            }
        }

        public static string DateFormatJavaScript
        {
            get
            {
                return "dd/mm/yy";
            }
        }

        public static string MonthDateFormat
        {
            get
            {
                return "mm/dd/yy";
            }
        }

        #endregion

        /// <summary>
        /// Get julian date count
        /// </summary>
        /// <param name="date"></param>
        /// <returns>Julian date count</returns>
        public static int GetJulianDateCount(DateTime date)
        {
            try
            {
                int julianDateCount = (int)(date - julianFixed).TotalDays;

                return julianDateCount;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.BusinessLogic, ex, null);
                throw;
            }
        }

        /// <summary>
        /// Parse the datetime to the format specified in this class
        /// </summary>
        /// <param name="textDate">date in text form. Format needs to be match with the format specified in the class</param>
        /// <returns>Returns a DateTime object</returns>
        public static DateTime ParseDate(string textDate)
        {
            DateTime date;
            if (!DateTime.TryParseExact(textDate, DateFormat, CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault, out date))
            {
                return DateTime.Now;
            }

            return date;
        }

        /// <summary>
        /// Parse the time to the format specified in this class
        /// </summary>
        /// <param name="textDate">time in text form. Format needs to be match with the format specified in the class</param>
        /// <returns>Returns a DateTime object</returns>
        public static DateTime ParseTime(string textDate)
        {
            DateTime date;
            if (!DateTime.TryParseExact(textDate, TimeFormat, CultureInfo.InvariantCulture, DateTimeStyles.NoCurrentDateDefault, out date))
            {
                return DateTime.Now;
            }

            return date;
        }
    }
}
