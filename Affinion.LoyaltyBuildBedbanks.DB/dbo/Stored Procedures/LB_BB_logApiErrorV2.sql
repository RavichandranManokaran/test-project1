

/****** Object:  StoredProcedure [dbo].[LB_BB_logApiErrorV2]    Script Date: 7/19/2016 7:28:15 PM ******/

---- ====================================================
---- Description:	RETURNS MARGIN FOR A PROPERTY BASED ON AVAILABILITY SOURCE
---- Written: RICHARD
---- Created: 2016.04.19
---- ====================================================
CREATE PROCEDURE [dbo].[LB_BB_logApiErrorV2]
	
	-- Input parameters 
	@pi_method varchar(100),
	@pi_source int,
	@pi_severity int,
	@pi_system int,
	@pi_detail varchar(MAX),
	@pi_type varchar(MAX),
	@pi_code varchar(MAX),
	@pi_errorCode varchar(MAX),
	@pi_extendedInfo varchar(MAX),
	@pi_message varchar(MAX),
	@pi_bbResponse varchar(MAX),
	@pi_stackTrace varchar(MAX),
	
	--Return parameters
	@po_returnstatus INT OUTPUT,
	@po_returnmsg VARCHAR(200) OUTPUT
AS
set nocount on;
BEGIN

	--Assume transaction will fail, set the return variable to -1.
	SET @po_returnstatus = -1;
	SET @po_returnmsg = null;
    
	BEGIN TRY                               
		INSERT INTO LB_BB_apiErrorLog
		(
			Method,
			Source,
			Severity,
			System,
			Detail,
			Type,
			Code,
			ErrorCode,
			ExtendedInfo,
			Message,
			StackTrace,
			datecreated,
			bbResponse
		)
		VALUES
		(
			@pi_method,
			@pi_source,
			@pi_severity,
			@pi_system,
			@pi_detail,
			@pi_type,
			@pi_code,
			@pi_errorCode,
			@pi_extendedInfo,
			@pi_message,
			@pi_stackTrace,
			GETDATE(),
			@pi_bbResponse
		) 
    END TRY	
	BEGIN CATCH	  
		SET @po_returnstatus = error_number();
		SET @po_returnmsg = error_message();
		RETURN;
	END CATCH
	
	SET @po_returnstatus = 0;
	SET @po_returnmsg = 'Completed successfully.';	
END

GO

