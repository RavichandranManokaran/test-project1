﻿using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.BedBanks.General
{
    public class BedBanksSettings
    {
        #region Properties
        /// <summary>
        ///  Client Item Id
        /// </summary>
        public string ClientItemId { get; set; }

        /// <summary>
        /// User Name for the Api call
        /// </summary>
        public string BedBanksUserName { get; set; }

        /// <summary>
        /// Password for the Api call
        /// </summary>
        public string BedBanksPassword { get; set; }

        /// <summary>
        /// Api Url to call
        /// </summary>
        public string BedBanksApiUrl { get; set; }

        /// <summary>
        /// Bed banks provider Name
        /// </summary>
        public string BedBanksProviderName { get; set; }

        /// <summary>
        /// Bed banks Provider Id
        /// </summary>
        public string BedBanksProviderId { get; set; }

        /// <summary>
        /// Minimum Star Ranking of hotels
        /// </summary>
        public string BedBanksMinimumStarRating { get; set; }

        /// <summary>
        /// Fallback Currency for Api call
        /// </summary>
        public BedBanksCurrency BedBanksDefaultCurrency { get; set; }

        /// <summary>
        /// Actual Currency for Api call
        /// </summary>
        public BedBanksCurrency BedBanksActualCurrency { get; set; }

        /// <summary>
        /// List of Processing fee for each currency at client level
        /// </summary>
        public List<BedBanksProcessingFee> BedBanksProcessingFees { get; set; }

        /// <summary>
        /// Default Locale for Api call
        /// </summary>
        public string BedBanksDefaultLocale { get; set; }

        /// <summary>
        /// List of facility to include
        /// </summary>
        public List<BedBanksFacility> BedBanksFacility { get; set; }

        /// <summary>
        /// List of Bed Bank Locations currently associated with provider settings
        /// </summary>
        public List<BedBankLocations> BedBankLocation { get; set; }

        /// <summary>
        /// Product SKU Id of uCommerce product to use for shopping related operations
        /// </summary>
        public string UcommerceProductSku { get; set; }

        /// <summary>
        /// Variant SKU Id of uCommerce product to use for shopping related operations
        /// </summary>
        public string UcommerceVariantSku { get; set; }

        /// <summary>
        /// Vat percentage applicable for current Bed Banks provider
        /// </summary>
        public float VatPercentage { get; set; }

        /// <summary>
        /// Bed Banks Oracle Configurations
        /// </summary>
        public BedBanksOracleConfigurations BedBanksOracleConfigurations { get; set; }

        #endregion Properties

        #region Constructor
        /// <summary>
        /// Get all the data for BB provider from Sitecore Client Item
        /// </summary>
        /// <param name="clientSetuptItem"></param>
        public BedBanksSettings(Item clientSetupItem)
        {
            Item bedBanksSettingsItem = ((MultilistField)clientSetupItem.Fields["BedBanksProvider"]).GetItems().FirstOrDefault();
            if (bedBanksSettingsItem != null)
            {
                this.ClientItemId = clientSetupItem.ID.ToString();
                Item bedBankProvider = SitecoreFieldsHelper.GetLookupFieldTargetItem(bedBanksSettingsItem, "Bed Banks Provider");
                Item bedBankCurrency = SitecoreFieldsHelper.GetLookupFieldTargetItem(bedBanksSettingsItem, "Currency");
                Item bedBankLocale = SitecoreFieldsHelper.GetLookupFieldTargetItem(bedBanksSettingsItem, "Locale");
                List<Item> bedBankLocation = ((MultilistField)bedBanksSettingsItem.Fields["Locations"]).GetItems().ToList<Item>();
                List<Item> bedBankFacilities = ((MultilistField)bedBanksSettingsItem.Fields["Facility"]).GetItems().ToList<Item>();
                List<Item> bedBanksProcessingFees = ((MultilistField)bedBanksSettingsItem.Fields["Processing Fee"]).GetItems().ToList<Item>();

                this.BedBanksUserName = SitecoreFieldsHelper.GetItemFieldValue(bedBanksSettingsItem, "Username");
                this.BedBanksPassword = SitecoreFieldsHelper.GetItemFieldValue(bedBanksSettingsItem, "Password");
                this.BedBanksApiUrl = SitecoreFieldsHelper.GetItemFieldValue(bedBanksSettingsItem, "Base API");
                this.BedBanksMinimumStarRating = SitecoreFieldsHelper.GetItemFieldValue(bedBanksSettingsItem, "Minimum Star Ranking");
                this.UcommerceProductSku = SitecoreFieldsHelper.GetItemFieldValue(bedBanksSettingsItem, "uCommerce Product SKU");
                this.UcommerceVariantSku = SitecoreFieldsHelper.GetItemFieldValue(bedBanksSettingsItem, "uCommerce Variant SKU");
                this.BedBanksDefaultLocale = SitecoreFieldsHelper.GetItemFieldValue(bedBankLocale, "Locale Code");

                if (bedBankProvider != null)
                {
                    this.BedBanksProviderName = SitecoreFieldsHelper.GetItemFieldValue(bedBankProvider, "Bed Banks Provider Name");
                    this.BedBanksProviderId = SitecoreFieldsHelper.GetItemFieldValue(bedBankProvider, "Bed Banks Provider Id");
                }

                if (bedBankCurrency != null)
                {
                    BedBanksCurrency currency = new BedBanksCurrency();
                    currency.BedBankCurrencyItem = bedBankCurrency;
                    currency.CurrencyName = SitecoreFieldsHelper.GetItemFieldValue(bedBankCurrency, "Currency Name");
                    currency.BedBanksCurrencyId = SitecoreFieldsHelper.GetItemFieldValue(bedBankCurrency, "Bed Banks Currency Id");
                    currency.uCommerceCurrencyId = SitecoreFieldsHelper.GetItemFieldValue(bedBankCurrency, "uCommerce Currency Id");
                    currency.MaxPriceForHotels = SitecoreFieldsHelper.GetItemFieldValue(bedBankCurrency, "Maximum Price for Hotels");
                    this.BedBanksDefaultCurrency = currency;
                }

                if (bedBankLocation != null)
                {
                    List<BedBankLocations> LocationList = new List<BedBankLocations>();
                    foreach (var location in bedBankLocation)
                    {
                        BedBankLocations bbLocation = new BedBankLocations();
                        bbLocation.BedBanksLocationId = SitecoreFieldsHelper.GetItemFieldValue(location, "Bed Banks Location Id");
                        bbLocation.LocationName = SitecoreFieldsHelper.GetItemFieldValue(location, "Location Name");
                        bbLocation.SitecoreLocationItem = (SitecoreFieldsHelper.GetLookupFieldTargetItem(location, "Sitecore Location Item") != null) ? SitecoreFieldsHelper.GetLookupFieldTargetItem(location, "Sitecore Location Item") : null;
                        LocationList.Add(bbLocation);
                    }
                    this.BedBankLocation = LocationList;
                }
                if (bedBankFacilities != null)
                {
                    List<BedBanksFacility> FacilityList = new List<BedBanksFacility>();
                    foreach (var facility in bedBankFacilities)
                    {
                        BedBanksFacility bbFacility = new BedBanksFacility();
                        bbFacility.FacilityName = SitecoreFieldsHelper.GetItemFieldValue(facility, "Facility Name");
                        bbFacility.FacilityId = SitecoreFieldsHelper.GetItemFieldValue(facility, "Bed Banks Facility Id");
                        bbFacility.SitecoreFacilityItem = SitecoreFieldsHelper.GetLookupFieldTargetItem(facility, "Sitecore Facility Item");
                        bbFacility.SitecoreFacilityName = SitecoreFieldsHelper.GetItemFieldValue(bbFacility.SitecoreFacilityItem, "Name");
                        FacilityList.Add(bbFacility);
                    }
                    this.BedBanksFacility = FacilityList;
                }

                string vatPercentage = SitecoreFieldsHelper.GetItemFieldValue(bedBanksSettingsItem, "Vat Percentage");
                float vatPercent = 0.0f;
                float.TryParse(vatPercentage, out vatPercent);
                this.VatPercentage = vatPercent;

                if (bedBanksProcessingFees != null && bedBanksProcessingFees.Count > 0)
                {
                    List<BedBanksProcessingFee> bbProcFee = new List<BedBanksProcessingFee>();
                    foreach (Item processingFeeItem in bedBanksProcessingFees)
                    {
                        BedBanksProcessingFee bedBanksProcessingFee = new BedBanksProcessingFee();
                        bedBanksProcessingFee.BedBanksCurrencyItem = SitecoreFieldsHelper.GetLookupFieldTargetItem(processingFeeItem, "Currency Item");
                        string processingFee = SitecoreFieldsHelper.GetItemFieldValue(processingFeeItem, "Processing Fee");
                        float processingFeeAmount = 0.0f;
                        float.TryParse(processingFee, out processingFeeAmount);
                        bedBanksProcessingFee.ProccesingFee = processingFeeAmount;
                        bbProcFee.Add(bedBanksProcessingFee);
                    }
                    this.BedBanksProcessingFees = bbProcFee;
                }

                this.BedBanksOracleConfigurations = new BedBanksOracleConfigurations();
                this.BedBanksOracleConfigurations.OracleId = SitecoreFieldsHelper.GetItemFieldValue(bedBanksSettingsItem, "OracleID");
                this.BedBanksOracleConfigurations.OracleIDforVAT = SitecoreFieldsHelper.GetItemFieldValue(bedBanksSettingsItem, "OracleIDforVAT");
                this.BedBanksOracleConfigurations.VatNumber = SitecoreFieldsHelper.GetItemFieldValue(bedBanksSettingsItem, "VAT Number");
                this.BedBanksOracleConfigurations.CustomerAddressCode = SitecoreFieldsHelper.GetItemFieldValue(bedBanksSettingsItem, "Customer Address Code");
                this.BedBanksOracleConfigurations.SupplierAddresCode = SitecoreFieldsHelper.GetItemFieldValue(bedBanksSettingsItem, "Supplier Address Code");
                this.BedBanksOracleConfigurations.ApPaymentTerms = SitecoreFieldsHelper.GetItemFieldValue(bedBanksSettingsItem, "AP Payment Terms");
                this.BedBanksOracleConfigurations.ApLineNumber = SitecoreFieldsHelper.GetItemFieldValue(bedBanksSettingsItem, "AP Line Number");
                this.BedBanksOracleConfigurations.ApTaxCode = SitecoreFieldsHelper.GetItemFieldValue(bedBanksSettingsItem, "AP Tax Code");
                this.BedBanksOracleConfigurations.ApNominalCode = SitecoreFieldsHelper.GetItemFieldValue(bedBanksSettingsItem, "AP Nominal Code");
                this.BedBanksOracleConfigurations.ApCampaignAccountingId = SitecoreFieldsHelper.GetItemFieldValue(bedBanksSettingsItem, "AP Campaign Accounting ID");
                this.BedBanksOracleConfigurations.ArNominalCode = SitecoreFieldsHelper.GetItemFieldValue(bedBanksSettingsItem, "AR Nominal Code");
                this.BedBanksOracleConfigurations.ArCampaignAccountingId = SitecoreFieldsHelper.GetItemFieldValue(bedBanksSettingsItem, "AR Campaign Accounting ID");
                this.BedBanksOracleConfigurations.ArReceiptTerms = SitecoreFieldsHelper.GetItemFieldValue(bedBanksSettingsItem, "AR Receipt Terms");
                this.BedBanksOracleConfigurations.ArTaxCode = SitecoreFieldsHelper.GetItemFieldValue(bedBanksSettingsItem, "AR Tax Code");
                this.BedBanksOracleConfigurations.ArInvoiceId = SitecoreFieldsHelper.GetItemFieldValue(bedBanksSettingsItem, "AR Invoice ID");
            }
        }

        #endregion Constructor
    }
}
