﻿using Affinion.LoyaltyBuild.Api.Booking.Data.Payment;
using Affinion.LoyaltyBuild.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/
namespace Affinion.LoyaltyBuild.Api.Booking.Service
{
    /// <summary>
    /// Interface to define the payment related actions
    /// </summary>
    public interface IPaymentService
    {
        /// <summary>
        /// Gets the OrderlinePayment Information 
        /// </summary>
        /// <param name="orderLineId">orderLineId</param>
        /// <returns>Object of OrderLinePaymentInfo</returns>
        OrderLinePaymentInfo GetOrderLinePaymentInfo(int orderLineId);
                
        /// <summary>
        /// Gets the dues information 
        /// </summary>
        /// <param name="orderLineId">orderLineId</param>
        /// <param name="currencyTypeId">currencyTypeId</param>          
        List<TotalDueItem> GetDuesByOrderlineID(int orderLineId, int? currencyTypeId);

        /// <summary>
        /// Saves the due details
        /// </summary>
        /// <param name="dueDetails">dueDetails</param>
        /// <returns>ValidationResponse object with success/failure flag</returns>
        ValidationResult SaveDue(DueDetails dueDetails);

        /// <summary>
        /// Deletes a due item with given DueId
        /// </summary>
        /// <param name="dueId">dueId</param>
        /// <returns>ValidationResponse object with success/failure flag</returns>
        ValidationResult DeleteDue(int dueId, string reason);
        
        /// <summary>
        /// Get the Due Details for the particular DueId
        /// </summary>
        /// <param name="dueId">dueId</param>
        /// <returns>DueDetails object</returns>
        DueDetails GetDueDetail(int dueId);

        /// <summary>
        /// Add a payment 
        /// </summary>
        /// <param name="payment">payment</param>
        /// <returns>ValidationResponse object with success/failure flag</returns>
        ValidationResult AddPayment(PaymentDetail payment);

        /// <summary>
        /// Gets the list of payment methods
        /// </summary>
        /// <returns> List of Payment menthods</returns>
        List<PaymentMethod> GetPaymentMethods(string clientId);

        /// <summary>
        /// Gets the payment reasons
        /// </summary>
        /// <returns> List of Payment Reasons</returns>
        List<PaymentReason> GetPaymentReasons();
        
        /// <summary>
        /// Gets the available payment list for refund
        /// </summary>
        /// <param name="orderLineId">orderLineId</param>
        /// <param name="paymentType">paymentType</param>
        /// <param name="currencyType">currencyType</param>
        /// <returns>List of Payment History Item</returns>
        List<RefundableItem> GetAvailablePaymentsForRefund(int orderLineId, int paymentType, int currencyType);


        /// <summary>
        /// Gets the list of currency details
        /// </summary>
        /// <returns> List of currency details</returns>
        List<Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model.CurrencyDetail> GetCurrencyListDetails();
    }
}
