﻿CREATE PROCEDURE [dbo].[LB_GetOrderLineSubrates] 
(@orderLineId int)
AS
BEGIN
	select SubrateItemCode, Amount
	from LB_OrderLineSubrates UOL
	where uol.OrderLineID = @orderLineId
END
