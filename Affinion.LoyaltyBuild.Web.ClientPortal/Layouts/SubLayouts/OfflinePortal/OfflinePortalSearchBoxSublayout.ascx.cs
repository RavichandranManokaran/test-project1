﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           OfflinePortalSearchBoxSublayout.ascx.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal
/// Description:           SubLayout for offline portal search box
/// </summary>

namespace Affinion.Loyaltybuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    #region Using Directives

    using Affinion.LoyaltyBuild.Common;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.Search.Data;
    using Affinion.LoyaltyBuild.Search.Helpers;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;

    #endregion

    public partial class OfflinePortalSearchBoxSublayout : BaseSublayout
    {
        #region private constant variable

        /// <summary>
        /// Client item path
        /// </summary>
        private const string ClientItemPath = "/sitecore/content/admin-portal/client-setup/{0}";

        #endregion

        #region Fields

        /// Get Sitecore Context DB
        Database contextDb = Sitecore.Context.Database;

        protected bool useAjax = false;
        protected string submitUrl = string.Empty;
        protected string cssClass = string.Empty;
        protected string searchBoxId = string.Empty;
        protected PageMode pageMode;
        private string childAges = string.Empty;

        #endregion

        #region Public Methods

        /// <summary>
        /// Code to execute on before page load
        /// </summary>
        /// <param name="e"></param>
        public override void PreLoad(EventArgs e)
        {
            try
            {
                Item item = this.GetDataSource();

                if (item == null)
                {
                    return;
                }

                this.PostAction(item);
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, null);
                throw;
            }
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Overriding OnInit function
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);
                CreateChildAgeDropdownLists();
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, null);
                throw;
            }
        }

        /// <summary>
        /// Code to execute on page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Item item = this.GetDataSource();

                if (!ValidateChildAge(item))
                {
                    return;
                }

                InitializeControls(item);

                Uri requestUrl = Request.Url;
                childAges = string.Empty;

                if (item == null)
                {
                    return;
                }

                this.useAjax = SitecoreFieldsHelper.GetBoolean(item, "UseAjaxSubmit");
                this.cssClass = this.GetParameter("CssClass", "Title");
                this.searchBoxId = item.ID.ToString();

                /// Bind data
                this.BindData(item);

                QueryStringHelper helper = new QueryStringHelper(Request.Url);
                if (string.Equals(helper.GetValue("mode"), "Transfer", StringComparison.InvariantCultureIgnoreCase) || string.Equals(helper.GetValue("EnableClient"), "false"))
                {
                    this.ClientList.Enabled = false;
                }

                string requestQuery = requestUrl.Query;
                if (!this.IsPostBack && !string.IsNullOrEmpty(requestQuery))
                {
                    /// Bind data to the text boxes in the result page
                    PrefillControls(requestUrl);
                }
                else
                {
                    this.CheckinDate.Text = DateTime.Now.ToString(DateTimeHelper.DateFormat);
                }

                if (Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.HasBasket)
                {
                    if (SearchHelper.GetUserSpecificClientItems() != null)
                    {
                        /// Extract Clients Filtered Items
                        var clientFilterList = (from client in SearchHelper.GetUserSpecificClientItems()
                                                orderby client.DisplayName
                                                select new { ClientId = client.ID, DiplayName = client.DisplayName, ItemName = client.Name }).ToList();

                        var getClient = Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.GetBasket().Bookings[0].ClientId;
                        var ClientItem = clientFilterList.FirstOrDefault(w => w.ClientId.ToString() == getClient).ItemName;

                        ClientList.SelectedIndex = ClientList.Items.IndexOf(ClientList.Items.FindByValue(ClientItem));
                        this.ClientList.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, null);
                throw;
            }
        }

        /// <summary>
        /// Returns whether Date selection controller to apper or hide
        /// </summary>
        protected string DatePickerContainerClass
        {
            get
            {
                if (this.DateSelectionCheckBox.Checked)
                {
                    //return "hide-div";
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Code execute after the page load completed successfully 
        /// </summary>
        /// <param name="sender">The Sender</param>
        /// <param name="e">The Parameter</param>
        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            try
            {
                Item item = this.GetDataSource();
                var querySession = Session["SearchKeyValues"];

                ///Check the session value is not empty
                if (querySession != null && querySession.ToString() != string.Empty)
                {
                    ///Get the request Url and append session Query to current Url.
                    UriBuilder urlBuilder = new UriBuilder(this.Request.Url);
                    urlBuilder.Query = querySession.ToString().Split('?')[1];

                    ///Fill the controllers according to the session query values
                    PrefillControls(Request.Url);
                }
                else
                    return;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Method used to create child age dropdownlist dynamically
        /// </summary>
        private void CreateChildAgeDropdownLists()
        {
            Item item = this.GetDataSource();
            int maxChildren = SitecoreFieldsHelper.GetInteger(item, "MaxChildren", 7);
            int maxChildAge = SitecoreFieldsHelper.GetInteger(item, "MaxChildAge", 17);

            IList<string> childAgesCollection = new List<string>();

            if (!IsPostBack)
            {
                Uri requestUrl = Request.Url;
                QueryStringHelper helper = new QueryStringHelper(requestUrl);

                string childAgesParameter = helper.GetValue("ChildAge");

                if (childAgesParameter != null)
                {
                    childAgesCollection = childAgesParameter.Split(',');
                }
            }

            for (int i = 0; i < maxChildren; i++)
            {
                HtmlGenericControl outerDiv = new HtmlGenericControl("div");

                HtmlGenericControl innerDiv = new HtmlGenericControl("div");

                CreateDropDownList(string.Format("AgeDropdownList{0}", (i + 1)), innerDiv, maxChildAge, childAgesCollection.ElementAtOrDefault(i));

                innerDiv.Attributes.Add("role", "group");

                outerDiv.Controls.Add(innerDiv);
                outerDiv.Attributes.Add("class", "age col-xs-3");
                outerDiv.Attributes.Add("id", String.Format("AgeDropdownDiv{0}", (i + 1)));

                ChildAgePanel.Controls.Add(outerDiv);
            }
        }

        /// <summary>
        /// Method used to create dropdownlist dynamically
        /// </summary>
        /// <param name="id"></param>
        /// <param name="parent"></param>
        /// <param name="maxChildAge"></param>
        /// <param name="selectedValue"></param>
        private static void CreateDropDownList(string id, Control parent, int maxChildAge, string selectedValue)
        {
            ArrayList listItems = new ArrayList();

            /// Add default option
            listItems.Add("Select");

            /// Add zero to twelve months option            
            listItems.Add(LanguageReader.GetText(Constants.ZeroToTwelveMonthsDictionaryKey, "0"));

            for (int i = 0; i < maxChildAge; i++)
            {
                listItems.Add(i + 1);
            }

            DropDownList dropDownList = new DropDownList();
            dropDownList.ID = id;
            dropDownList.ClientIDMode = ClientIDMode.Static;

            dropDownList.Attributes.Add("class", "form-control");
            dropDownList.DataSource = listItems;
            dropDownList.DataBind();
            dropDownList.SelectedValue = selectedValue;

            parent.Controls.Add(dropDownList);
        }

        /// <summary>
        /// Method to validate child age
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        private bool ValidateChildAge(Item source)
        {
            string allChildAges = string.Empty;
            bool isValid = true;
            int childCount;
            string noOfChildrens = NoOfChildren.Text;

            if (int.TryParse(noOfChildrens, out childCount))
            {
                for (int i = 0; i < childCount; i++)
                {
                    DropDownList dropDownList = ChildAgePanel.FindControl(string.Format("AgeDropdownList{0}", i + 1)) as DropDownList;

                    string selectedValue = dropDownList.SelectedValue;

                    if (selectedValue.Equals("Select"))
                    {
                        isValid = false;
                        break;
                    }
                    allChildAges = string.Format("{0}{1}{2}", allChildAges, "&ChildAge=", selectedValue);
                }
            }

            if (isValid)
            {
                childAges = allChildAges;
                return isValid;
            }
            else
            {
                ServerSideErrorPanel.Visible = true;
                ServerSideErrorPanel.InnerText = SitecoreFieldsHelper.GetValue(source, Constants.ChildAgeRequiredErrorMessageFieldName, "Please select age of children");
                return isValid;
            }
        }

        /// <summary>
        /// Method used to intizialize ErrorPanel panels
        /// </summary>
        /// <param name="item"></param>
        private void InitializeControls(Item item)
        {
            ServerSideErrorPanel.Visible = false;
            ErrorPanel.InnerText = SitecoreFieldsHelper.GetValue(item, Constants.ChildAgeRequiredErrorMessageFieldName, "Please select age of children");
            ErrorPanel.ClientIDMode = ClientIDMode.Static;
            ErrorPanel.Style.Add("display", "none");
        }

        /// <summary>
        /// Submit post back method
        /// </summary>
        /// <param name="item"></param>
        private void PostAction(Item item)
        {
            string eventTarget = this.Request.Params["__EVENTTARGET"];

            if (eventTarget != null && eventTarget.Contains("filterButton"))
            {
                return;
            }

            Uri requestUrl = Request.Url;

            //if (this.IsPostBack && this.Request != null && this.Request.Params["__EVENTTARGET"] != null && this.Request.Params["__EVENTTARGET"].Contains("Submit"))
            //{

            if (!string.IsNullOrEmpty(this.ClientList.SelectedValue) && !string.IsNullOrEmpty(this.Destination.Text))
            {
                this.SetPageMode();
                StringBuilder parameters = BuildParameters();
                /// Set form action
                AnchorTagHelperData resultsPage = this.GetResultsPage(item);
                this.submitUrl = string.IsNullOrEmpty(resultsPage.Url) ? requestUrl.AbsolutePath : resultsPage.Url;
                this.submitUrl += parameters.ToString();
                Session["SearchKeyValues"] = submitUrl;
                Response.Redirect(this.submitUrl, true);
            }
            //}
        }

        /// <summary>
        /// Binds data to controls
        /// </summary>
        private void BindData(Item item)
        {
            if (item != null)
            {
                this.DatasourceText.Visible = false;

                this.TitleLabel.Item =
                this.DestinationLabel.Item =
                this.CheckinDateLabel.Item =
                this.CheckoutDateLabel.Item =
                this.RoomsLabel.Item =
                this.AdultsLabel.Item =
                this.ChildrenLabel.Item =
                this.ChildrenAgeTitleLabel.Item =
                this.MobileVersionSearchLable.Item =
                    //this.ExperienceLabel.Item =
                this.ClientListLabel.Item =
                this.OfferGroupLabel.Item = item;
                Submit.Text = item.Fields["SubmitLabel"].Value;

                /// Read sitecore values
                string destinationText = SitecoreFieldsHelper.GetValue(item, "DestinationLabel");
                this.DateSelectionCheckBox.Text = SitecoreFieldsHelper.GetValue(item, "FlexibleDatesLabel");

                /// Set html5 attributes
                this.Destination.Attributes.Add("placeholder", destinationText);
                this.Destination.Attributes.Add("aria-describedby", destinationText);

                this.CheckinDate.Attributes.Add("pattern", @"[0-9\/]*");

                this.BindDropDowns(item);
            }
        }

        /// <summary>
        /// Bind data to drop downs
        /// </summary>
        /// <param name="item">The item to read data</param>
        private void BindDropDowns(Item item)
        {
            /// Read sitecore values
            int maxRooms = SitecoreFieldsHelper.GetInteger(item, "MaxRooms", 5);
            int maxAdults = SitecoreFieldsHelper.GetInteger(item, "MaxAdults", 8);
            int maxChildren = SitecoreFieldsHelper.GetInteger(item, "MaxChildren", 7);
            int maxNights = SitecoreFieldsHelper.GetInteger(item, "MaxNights", 10);
            string showAllText = SitecoreFieldsHelper.GetValue(item, "ShowAllLabel");

            string selectedNoOfNights = NoOfNights.SelectedValue;
            this.NoOfNights.DataSource = Enumerable.Range(1, maxNights);
            this.NoOfNights.DataBind();
            NoOfNights.SelectedValue = selectedNoOfNights;

            string selectedNoOfRooms = NoOfRooms.SelectedValue;
            this.NoOfRooms.DataSource = Enumerable.Range(1, maxRooms);
            this.NoOfRooms.DataBind();
            NoOfRooms.SelectedValue = selectedNoOfRooms;

            string selectedNoOfAdults = NoOfAdults.SelectedValue;
            this.NoOfAdults.DataSource = Enumerable.Range(1, maxAdults);
            this.NoOfAdults.DataBind();
            NoOfAdults.SelectedValue = selectedNoOfAdults;

            string selectedNoOfChildren = NoOfChildren.SelectedValue;
            this.NoOfChildren.DataSource = Enumerable.Range(0, maxChildren + 1);
            this.NoOfChildren.DataBind();
            NoOfChildren.SelectedValue = selectedNoOfChildren;

            if (SitecoreFieldsHelper.GetBoolean(item, "DisplayShowAllLabel"))
            {
                ListItem showAllItem = new ListItem(showAllText, "all");
                this.OfferGroup.AppendDataBoundItems = true;
                this.OfferGroup.Items.Insert(0, showAllItem);
            }

            /// Load Package Dropdown
            this.OfferGroup.DataSource = SitecoreFieldsHelper.GetDropdownDataSource(item, "OfferGroups", "Title");
            this.OfferGroup.DataTextField = "Text";
            this.OfferGroup.DataValueField = "Text";
            this.OfferGroup.DataBind();

            this.BindClientDropDown();
        }

        /// <summary>
        /// Bind Client data to drop downs
        /// </summary>
        private void BindClientDropDown()
        {
            try
            {
                if (SearchHelper.GetUserSpecificClientItems() != null)
                {
                    /// Extract Clients Filtered Items
                    var clientFilterList = (from client in SearchHelper.GetUserSpecificClientItems()
                                            orderby client.DisplayName
                                            select new { ClientId = client.ID, DiplayName = client.DisplayName, ItemName = client.Name }).ToList();

                    /// Load Client List Dropdown
                    this.ClientList.DataSource = clientFilterList;

                    /// Set data text and data value fields
                    ClientList.DataTextField = "DiplayName";
                    ClientList.DataValueField = "ItemName";

                    foreach (var item in clientFilterList)
                    {
                        AddJavascriptVariable(item.ItemName, item.ClientId);
                    }

                    this.ClientList.DataBind();
                }

                this.ClientList.Items.Insert(0, Constants.PleaseSelectText);

                /// Assign respective client images to dropdown
                foreach (ListItem clientListItem in ClientList.Items)
                {
                    if (!string.Equals(clientListItem.Text, Constants.PleaseSelectText))
                    {
                        Item clientItem = contextDb.GetItem(string.Format(ClientItemPath, clientListItem.Value));
                        clientListItem.Attributes.Add("data-image", GetImageURL(clientItem, "Customer Service Image"));
                        clientListItem.Attributes.Add("data-clientid", clientItem.ID.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, null);
                throw;
            }
        }

        /// <summary>
        /// Method used to Get Image URL of Sitecore Image field
        /// </summary>
        /// <param name="currentItem"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        private string GetImageURL(Item currentItem, string fieldName)
        {
            string imageURL = string.Empty;
            Sitecore.Data.Fields.ImageField imageField = currentItem.Fields[fieldName];
            if (imageField != null && imageField.MediaItem != null)
            {
                Sitecore.Data.Items.MediaItem image = new Sitecore.Data.Items.MediaItem(imageField.MediaItem);
                imageURL = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image));
            }
            return imageURL;
        }

        /// <summary>
        /// Prefill the textboxes in the results page according to the values entered to perform search.
        /// </summary>
        /// <param name="requestUrl"></param>
        private void PrefillControls(Uri requestUrl)
        {
            QueryStringHelper helper = new QueryStringHelper(requestUrl);

            this.ClientList.SelectedValue = helper.GetValue("ClientList");
            this.Destination.Text = helper.GetValue("Destination");
            this.CategoryId.Value = helper.GetValue("DestinationId");
            this.DestinationType.Value = helper.GetValue("DestinationType");
            this.CheckinDate.Text = helper.GetValue("CheckinDate");
            this.CheckoutDate.Value = helper.GetValue("CheckoutDate");
            this.NoOfNights.SelectedValue = helper.GetValue("NoOfNights");
            //this.DateSelectionCheckBox.Checked = (helper.GetValue("SpecificDate") == "True") ? true : false;
            this.NoOfRooms.SelectedValue = helper.GetValue("NoOfRooms");
            this.NoOfAdults.SelectedValue = helper.GetValue("NoOfAdults");
            this.NoOfChildren.SelectedValue = helper.GetValue("NoOfChildren");
            this.OfferGroup.SelectedValue = helper.GetValue("OfferGroup");
        }

        /// <summary>
        /// Determine which page to load depending on the input
        /// </summary>
        private void SetPageMode()
        {
            bool flexibleDates = string.IsNullOrEmpty(DateSelectionCheckBox.Text) || string.IsNullOrEmpty(CheckinDate.Text);
            bool noDestination = string.IsNullOrEmpty(Destination.Text);

            if (!noDestination && flexibleDates) // destination provided, no dates
            {
                pageMode = PageMode.Locations;
            }
            else if (!noDestination && !flexibleDates) // destination provided, date provided
            {
                pageMode = PageMode.Hotels;
            }
            else if (noDestination && !flexibleDates) // no destination, date provided
            {
                pageMode = PageMode.LocationFinder;
            }
            else
            {
                pageMode = PageMode.Locations;
            }
        }

        /// <summary>
        /// Get the correct results page to redirect for search results
        /// </summary>
        /// <param name="item">The searchbox item</param>
        /// <returns>Returns the url</returns>
        private AnchorTagHelperData GetResultsPage(Item item)
        {
            string fieldName = "HotelsPage";

            if (pageMode == PageMode.Hotels)
            {
                fieldName = "HotelsPage";
            }
            else if (pageMode == PageMode.Locations)
            {
                fieldName = "LocationsPage";
            }
            else if (pageMode == PageMode.LocationFinder)
            {
                fieldName = "LocationFinderPage";
            }

            AnchorTagHelperData resultsPage = SitecoreFieldsHelper.GetUrl(item, fieldName);
            return resultsPage;
        }

        /// <summary>
        /// Build parameter string
        /// </summary>
        /// <returns>Return URI parameters</returns>
        private StringBuilder BuildParameters()
        {
            StringBuilder parameters = AppendParameters();
            return ValidateAppendParameters(parameters);
        }

        /// <summary>
        /// Method used to append basic query string parameters
        /// </summary>
        /// <returns></returns>
        private StringBuilder AppendParameters()
        {
            StringBuilder parameters = new StringBuilder();

            parameters.Append("?ClientList=");
            parameters.Append(this.ClientList.SelectedValue);
            parameters.Append("&Destination=");
            parameters.Append(this.Destination.Text);
            parameters.Append("&DestinationId=");
            parameters.Append(this.CategoryId.Value);
            parameters.Append("&DestinationType=");
            parameters.Append(this.DestinationType.Value);
            parameters.Append("&CheckinDate=");
            SetDate("CheckinDate", parameters);
            parameters.Append("&CheckoutDate=");
            SetDate("CheckoutDate", parameters);
            //parameters.Append("&SpecificDate=");
            //parameters.Append(this.DateSelectionCheckBox.Checked.ToString());
            parameters.Append("&NoOfNights=");
            parameters.Append(this.NoOfNights.Text);
            parameters.Append("&NoOfRooms=");
            parameters.Append(this.NoOfRooms.Text);
            parameters.Append("&NoOfAdults=");
            parameters.Append(this.NoOfAdults.Text);
            parameters.Append("&NoOfChildren=");
            parameters.Append(this.NoOfChildren.Text);
            parameters.Append("&OfferGroup=");
            parameters.Append(this.OfferGroup.Text);
            //parameters.Append("&Mode=");
            //parameters.Append(Enum.GetName(typeof(PageMode), this.pageMode));

            return parameters;
        }

        /// <summary>
        /// Method used to validate and append query string parameters
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private StringBuilder ValidateAppendParameters(StringBuilder parameters)
        {
            Uri requestUrl = Request.Url;
            QueryStringHelper helper = new QueryStringHelper(requestUrl);
            string requestQuery = requestUrl.Query;

            /// Check whether child ages are already filled when validated
            if (string.IsNullOrWhiteSpace(childAges))
            {
                childAges = GetChildAgesParameter(NoOfChildren.Text);
            }

            parameters.Append(childAges);

            if (!String.IsNullOrEmpty(requestQuery))
            {
                if (requestQuery.IndexOf("Location") >= 0)
                {
                    string location = helper.GetValue("Location");
                    parameters.Append("&Location=");
                    parameters.Append(location);
                }
            }

            if (!string.IsNullOrEmpty(helper.GetValue("mode")))
            {
                parameters.Append("&mode=" + helper.GetValue("mode"));
            }

            if (!string.IsNullOrEmpty(helper.GetValue("olid")))
            {
                parameters.Append("&olid=" + helper.GetValue("olid"));
            }

            if (string.Equals(helper.GetValue("EnableClient"), "false"))
            {
                parameters.Append("&EnableClient=false");
            }

            return parameters;
        }

        /// <summary>
        /// Method used to get child ages for query string
        /// </summary>
        /// <param name="noOfChildrens"></param>
        /// <returns></returns>
        private string GetChildAgesParameter(string noOfChildrens)
        {
            string childAgesParameters = string.Empty;

            int childCount;

            if (int.TryParse(noOfChildrens, out childCount))
            {

                for (int i = 0; i < childCount; i++)
                {
                    DropDownList dropDownList = ChildAgePanel.FindControl(string.Format("AgeDropdownList{0}", i + 1)) as DropDownList;
                    childAgesParameters = string.Format("{0}{1}{2}", childAgesParameters, "&ChildAge=", dropDownList.SelectedValue);
                }
            }
            return childAgesParameters;
        }

        /// <summary>
        /// Set date to requested controller
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="parameters"></param>
        private void SetDate(string controller, StringBuilder parameters)
        {
            //if (CheckinDate.Text.Length == 0)
            //{
            //    this.DateSelectionCheckBox.Checked = true;
            //}

            if (this.DateSelectionCheckBox.Checked)
            {
                if (string.Equals(controller, "CheckinDate"))
                {
                    parameters.Append(DateTime.Now.ToString(DateTimeHelper.DateFormat));
                }
                else
                {
                    // Considered no of nights as 1
                    string checkoutDate = DateTime.Now.AddDays(1).ToString(DateTimeHelper.DateFormat);
                    this.CheckoutDate.Value = checkoutDate;
                    parameters.Append(checkoutDate);
                    this.NoOfNights.SelectedIndex = 0;
                }
            }
            else
            {
                if (string.Equals(controller, "CheckinDate"))
                {
                    parameters.Append(this.CheckinDate.Text);
                }
                else
                {
                    DateTime checkinDate = DateTimeHelper.ParseDate(this.CheckinDate.Text);
                    int numberOfNights = int.Parse(this.NoOfNights.SelectedValue);
                    checkinDate = checkinDate.AddDays(numberOfNights);
                    string checkoutDate = checkinDate.ToString(DateTimeHelper.DateFormat);
                    this.CheckoutDate.Value = checkoutDate;
                    parameters.Append(checkoutDate);
                }
            }
        }

        #endregion
    }
}