﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SuperValuTopNavigationLinkSublayout.ascx.cs
/// Sub-system/Module:     Web.ClientPortal(VS project name)
/// Description:           Sublayout for top main menu navigation links
/// </summary>
namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts
{
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Sitecore.Data.Items;
    using System;

    /// <summary>
    /// Sublayout for top main menu navigation links
    /// </summary>
    public partial class TopNavigationLinkSublayout : BaseSublayout
    {
        /// <summary>
        /// Gets or sets the css class of the list item
        /// </summary>
        public string ListCss { get; set; }

        /// <summary>
        /// Gets or sets the css class of the anchor tag
        /// </summary>
        public string LinkCss { get; set; }

        /// <summary>
        /// This function includes the code which needs to run on page load
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The parameter</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                this.ListCss = "divider";
                Item item = this.GetDataSource();

                if (item != null)
                {
                    this.PlaceholderText.Visible = false;
                    this.ListCss = item == Sitecore.Context.Item ? this.ListCss + " current" : this.ListCss;
                    this.Link.Item = item;
                }
            }
            catch (Exception ex)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, ex.Message);
                throw;
            }
        }
    }
}