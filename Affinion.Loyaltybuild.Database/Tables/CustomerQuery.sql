
CREATE TABLE [dbo].[uCommerce_CustomerQuery](
	[QueryId] [int] IDENTITY(1,1) NOT NULL,
	[QueryTypeId] [int] NOT NULL,
	[OrderLineId] [int] NULL,
	[CustomerId]  [int] NULL,
	[Content]     [nvarchar](max) NOT NULL,
	[IsSolved]    [Bit] ,
	[PartnerId]   [nvarchar](100) NULL,
	[CampaignId] [nvarchar](100) NULL,
	[ProviderId] [nvarchar](100) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50),
	[UpdatedDate] [datetime],
	[UpdatedBy] [nvarchar](50)
PRIMARY KEY CLUSTERED 
(
	[QueryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[uCommerce_CustomerQuery] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO

ALTER TABLE [dbo].[uCommerce_CustomerQuery] ADD  DEFAULT (getdate()) FOR [UpdatedDate]
GO

