﻿using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts
{
    /// <summary>
    /// OfflinePortal Applicable Promotions class
    /// </summary>
    public partial class OfflinePortalApplicablePromotions : System.Web.UI.UserControl
    {
        /// <summary>
        /// List Hotel Product Detail
        /// </summary>
        public List<HotelProductDetail> ListHotelProductDetail { get; set; }

        public int CurrencyID { get; set; }

        public int Availability
        {
            get
            {
                
                if (ListHotelProductDetail.Count > 0)
                {
                    return ListHotelProductDetail.FirstOrDefault().Availability;
                }
                return 0;
            }

        }

        /// <summary>
        /// OfflinePortal Applicable Promotions
        /// </summary>
        public OfflinePortalApplicablePromotions()
        {

        }
        /// <summary>
        /// OfflinePortal Applicable Promotions
        /// </summary>
        /// <param name="listHotelProductDetail"></param>
        public OfflinePortalApplicablePromotions(List<HotelProductDetail> listHotelProductDetail)
            : this()
        {
            this.ListHotelProductDetail = listHotelProductDetail;
        }

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                if (ListHotelProductDetail.Count > 0)
                {
                    promoPriceLabel.Text = CurrencyHelper.FormatCurrency((decimal)(ListHotelProductDetail.FirstOrDefault().Price), CurrencyID);
                    int availability = Availability;
                    availabilityDropDown.DataSource = Enumerable.Range(1, availability);
                    availabilityDropDown.DataBind();
                    availabilitysection.Visible = true;    

                }
                else
                {
                    ZeroavailabilitySection.Visible = true;
                }
            }
            
        }
    }
}