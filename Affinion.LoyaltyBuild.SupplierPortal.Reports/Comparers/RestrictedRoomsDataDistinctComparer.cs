﻿using Affinion.LoyaltyBuild.SupplierPortal.Reports.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.SupplierPortal.Reports.Comparers
{
    class RestrictedRoomsDataDistinctComparer : IEqualityComparer<RestrictedRoomsData>
    {
        public bool Equals(RestrictedRoomsData x, RestrictedRoomsData y)
        {
            return (x.HotelId == y.HotelId) && (x.Date.AddDays(-x.Date.Day) == y.Date.AddDays(-y.Date.Day)) && (x.OfferId == y.OfferId);
        }

        public int GetHashCode(RestrictedRoomsData obj)
        {
            return obj.HotelId + (obj.Date.Day * obj.Date.Month * (obj.Date.Year - 2000));
        }
    }
}
