﻿CREATE PROCEDURE [dbo].[LB_GetAccomodationDetailsByOrderLineId](@OrderLineId int)
AS
BEGIN
   select [GuestName] from 
  [dbo].[LB_RoomReservation]
  where [OrderLineId] = @OrderLineId
END
