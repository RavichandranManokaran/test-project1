﻿using Affinion.LoyaltyBuild.BedBanks.JacTravel.Model.Auth;
using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Data.Items;
using System.Collections.Specialized;
using System.Linq;
using Affinion.LoyaltyBuild.Comms.Mail;
using Affinion.LoyaltyBuild.Comms.Mail.SMTP;
using System;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Sitecore.Data;

namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Message
{
    /// <summary>
    /// Base Class for a JacTravel Request Object
    /// </summary>
    public class JtRequest
    {
        private JtLoginDetails _loginDetails;
        public JtLoginDetails LoginDetails
        {
            get { return _loginDetails; }
            set { _loginDetails = value; }
        }
        public void SendExceptionMail(NameValueCollection exceptionInfo,string clientId)
        {
            //generate email
            try
            {
                Item emailItem = this.GetBedBanksEmailItem("Bed Banks Service Issue Email", clientId);
                if (emailItem != null)
                {
                    string from = SitecoreFieldsHelper.GetValue(emailItem, "From");
                    string to = SitecoreFieldsHelper.GetValue(emailItem, "To");
                    string subject = SitecoreFieldsHelper.GetValue(emailItem, "Subject");
                    string emailHtmlText = this.SetBedBanksExceptionMessage(SitecoreFieldsHelper.GetValue(emailItem, "Body"), exceptionInfo);
                    IMail mailObj = new SmtpMail(emailItem.ID.ToString(), "Body", "Subject");
                    mailObj.FromAddress = from;
                    mailObj.AddRecipient(new Recepient() { EmailId = to });
                    mailObj.Subject = subject;
                    mailObj.Body = emailHtmlText;
                    var mailService = MailService.CreateMailer(MailType.SMTP);
                    mailService.SendMail(mailObj);
                }
            }
            catch(Exception ex)
            {
                //supress exception
                Diagnostics.Trace(DiagnosticsCategory.Common, "BedBanks Api Fail Email Exception:"+ex);
            }
        }
        private Item GetBedBanksEmailItem(string emailFieldName,string clientId)
        {
            Item bedBanksEmailItem = null;
            ID newId = new ID();
            Sitecore.Data.ID.TryParse(clientId, out newId);
            Item clientItem = (!string.IsNullOrWhiteSpace(clientId)) ? Sitecore.Context.Database.GetItem(newId) : null;
            if (clientItem != null)
            {
                Item bedBanksSettingsItem = ((Sitecore.Data.Fields.MultilistField)clientItem.Fields["BedBanksProvider"]).GetItems().FirstOrDefault();
                bedBanksEmailItem = (bedBanksSettingsItem != null) ? SitecoreFieldsHelper.GetLookupFieldTargetItem(bedBanksSettingsItem, emailFieldName) : null;
            }
            return bedBanksEmailItem;
        }
        private string SetBedBanksExceptionMessage(string bodyContent, NameValueCollection data)
        {
            if (data != null)
            {
                string exceptionFlow = data["ExceptionFlow"];
                string exceptionMessage = data["ExceptionMessage"];
                bodyContent = bodyContent.Replace("{BedBanksExceptionFlow}", exceptionFlow);
                bodyContent = bodyContent.Replace("{BedBanksExceptionMessage}", exceptionMessage);
            }

            return bodyContent;
        }
    }
}
