﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Model
{
    [Serializable()]
    [XmlType(TypeName = "PropertyReferenceID")]
    public class JtPropertyReferenceId
    {
        public string PropertyReferenceID { get; set; }
    }
}
