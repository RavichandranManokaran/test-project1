

/****** Object:  StoredProcedure [dbo].[LB_BB_importBedbankProvider]    Script Date: 7/19/2016 7:27:15 PM ******/

---- ====================================================
---- Description:	RETURNS MARGIN FOR A PROPERTY BASED ON AVAILABILITY SOURCE
---- Written: RICHARD
---- Created: 2016.04.19
---- ====================================================
CREATE PROCEDURE [dbo].[LB_BB_importBedbankProvider]
	
	-- Input parameters 	
	--Return parameters
	@po_lbproviderid INT OUTPUT,
	@po_returnstatus INT OUTPUT,
	@po_returnmsg VARCHAR(200) OUTPUT

	
AS
set nocount on;
BEGIN

	--Assume transaction will fail, set the return variable to -1.
	SET @po_returnstatus = -1;
	SET @po_returnmsg = null;

	Declare @PropertyName varchar(500)
	, @PropertyReferenceID int
	, @Addressline1 varchar(500)
	, @Addressline2 varchar(500)
	,@Addressline3 varchar(500)
	,@Addressline4 varchar(500)
	,@Addressline5 varchar(500)
	,@phone varchar(500)
	,@fax varchar(500)
	,@lat varchar(50)
	,@Long varchar(50)
	,@RC int
	
	
	
	BEGIN TRY

		Declare my_cur cursor for
		Select PropertyName,PropertyReferenceID,isnull(Address1,'') as addressline1,isnull(Address2,'') as addressline2,isnull(TownCity,'')as addressline3,Case when PostCodeZip <> '0' THEN ltrim(isnull(County,'') + ' ' + Cast(isnull(PostCodeZip,'') as nvarchar)) else isnull(County,'') end as addressline4 ,isnull(Country,''),isnull(Stuff(Telephone, 1, 1, ''),''),isnull(Stuff(Fax, 1, 1, ''),''),isnull(Latitude,''),isnull(Longitude,'') from LB_BB_JAC
		where PropertyReferenceID in (Select PropertyReferenceID from LB_BB_JAC where PropertyReferenceID not in (Select availabilityProviderRecordID from _provider))

		Open my_cur
		Fetch next from my_cur into @PropertyName, @PropertyReferenceID, @Addressline1, @Addressline2,@Addressline3,
		@Addressline4,@Addressline5,@phone, @fax, @lat, @Long

		While @@FETCH_STATUS = 0
		BEGIN


		EXECUTE @RC = [Bedbank].[dbo].[uspTii_bedbankDAO_createBedbankProvider] 
		   @PropertyReferenceID
		  ,'1'
		  ,@PropertyName
		  ,@addressline1
		  ,@addressline2
		  ,@addressline3
		  ,@addressline4
		  ,@addressline5
		  ,@phone
		  ,''
		  ,@lat
		  ,@Long
		  ,@po_lbproviderid OUTPUT
		  ,@po_returnstatus OUTPUT
		  ,@po_returnmsg OUTPUT


		Fetch next from my_cur into @PropertyName, @PropertyReferenceID, @Addressline1, @Addressline2,@Addressline3,
		@Addressline4,@Addressline5,@phone, @fax, @lat, @Long
		END

		Close my_cur
		Deallocate my_cur
    
    END TRY	
	BEGIN CATCH	  
		SET @po_returnstatus = error_number();
		SET @po_returnmsg = error_message();
		RETURN;
	END CATCH
	
	SET @po_returnstatus = 0;
	SET @po_returnmsg = 'Completed successfully.';	
END








GO

