﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
///Source File:           Country.cs
///Sub-system/Module:     Affinion.LoyaltyBuild.Common
///Description:           Entity class for Country
///</summary>
#region usingDirectives
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Sitecore.Globalization;
using System;

#endregion

namespace Affinion.LoyaltyBuild.Common.Languages
{
    /// <summary>
    /// contains the country details
    /// </summary>
    public class Country
    {
        #region public properties
        /// <summary>
        /// Gets or sets a value for the country name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a value for the ImageUrl
        /// </summary>
        public string ImageUrl { get; set; }

        /// <summary>
        /// Gets or sets a value for the Navigation Url
        /// </summary>
        public string Url { get; set; }

        #endregion

        #region public constructors
        /// <summary>
        /// sets the language details
        /// </summary>
        public Country()
        {
            /// Set default language
            Language lang;
            bool result = Language.TryParse(Sitecore.Context.Site.Language,out lang);
            if(result)
            {
                this.UpdateLanguageDetails(lang, string.Empty);
            }
                
        }

        /// <summary>
        /// sets the langauge details
        /// </summary>
        /// <param name="language">Pass the current language</param>
        /// <param name="itemUrl">pass the current item path</param>
        public Country(Language language, string itemUrl)
        {
            /// Set default language
            this.UpdateLanguageDetails(language, itemUrl);
        }
        #endregion 

        #region private methods
        /// <summary>
        /// sets the langauge details
        /// </summary>
        /// <param name="language">Pass the current language</param>
        /// <param name="itemUrl">pass the current item path</param>
        private void UpdateLanguageDetails(Language language, string itemUrl)
        {
                itemUrl = string.IsNullOrEmpty(itemUrl) ? string.Empty : itemUrl;
                this.Name = language.CultureInfo.DisplayName;
                this.Url = "/" + language.CultureInfo.Name + itemUrl;
                this.ImageUrl = "/~/icon/" + language.GetIcon(Sitecore.Context.Database);
        }
        #endregion
    }
}
