﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model
{
    public class APOraclePayment:OraclePayment
    {
        public int SupplierPaymentId { get; set; }
        public DateTime OrderDate { get; set; }
        public string SupplierAddrCode { get; set; }
    }
}
