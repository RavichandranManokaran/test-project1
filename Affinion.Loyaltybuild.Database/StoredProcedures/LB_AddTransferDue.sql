﻿/****** Object:  StoredProcedure [dbo].[LB_AddTransferDue]    Script Date: 2/12/2016 11:41:34 AM ******/
DROP PROCEDURE [dbo].[LB_AddTransferDue]
GO

/****** Object:  StoredProcedure [dbo].[LB_AddTransferDue]    Script Date: 2/12/2016 11:41:34 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Prasanna SR
-- Create date: 10 Jan 2016
-- Description:	Calculate and enter due on transfer
-- =============================================
CREATE PROCEDURE [dbo].[LB_AddTransferDue] 
	@OrderLineId INT,
	@CreatedBy VARCHAR(50),
	@IsFreeTransfer BIT = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE 
		@OrderId INT,
		@BookingRef VARCHAR(50),
		@OldOrderLineId INT,
		@OldDepositFee INT,
		@OldCurrencyId INT,
		@OldProviderAmt MONEY,
		@TransferFee MONEY,
		@SubrateCode VARCHAR(50),
		@CurrencyId INT,
		@CreatedDate DATETIME,
		@CCProFee MONEY,
		@PaymentMethodId INT

	SELECT TOP 1 
		@CreatedDate = GETDATE(),
		@SubrateCode = [SubRateItemCode] ,
		@TransferFee = OS.[Amount] - OS.[Discount],
		@CurrencyId = PO.[CurrencyId],
		@BookingRef = RR.[OrderReference],
		@OrderId = OL.[OrderId]
	FROM [LB_OrderLineSubrates] OS
		INNER JOIN [uCommerce_OrderLine] OL ON OS.[OrderLineId] = OL.[OrderLineId]
		INNER JOIN [LB_RoomReservation] RR ON OL.[OrderLineId] = RR.[OrderLineId]
		INNER JOIN [uCommerce_PurchaseOrder] PO ON OL.[OrderId] = PO.[OrderId]
	WHERE OS.OrderLineId = @OrderLineId 
		AND [SubRateItemCode] IN ('LBCOM','TRAFEE')
	ORDER BY [SubRateItemCode] DESC
	
	--Set payment method id
	SELECT TOP 1
		@PaymentMethodId = PaymentMethodId
	FROM [uCommerce_OrderLine] OL
		INNER JOIN [LB_OrderLinePayment] OLP ON OL.[OrderLineId] = OLP.[OrderLineId]
	WHERE OL.OrderLineId IN (SELECT OrderLineID FROM [dbo].[LB_RelatedOrderLines](@OrderLineId)) 
	ORDER BY OrderLinePaymentID


	--Insert CC processing fee if exists
	IF EXISTS (SELECT 1 FROM [LB_OrderLineSubrates] WHERE OrderLineID = @OrderLineId AND [SubRateItemCode] ='CCPROFEE' AND @IsFreeTransfer = 0) 
	BEGIN
		SELECT TOP 1 @CCProFee = ([Amount] - [Discount]) FROM [LB_OrderLineSubrates] WHERE OrderLineID = @OrderLineId AND [SubRateItemCode] ='CCPROFEE'

		EXECUTE [LB_SaveOrderLineDue]
			NULL
			,@OrderLineId
			,'CCPROFEE'
			,NULL
			,@CurrencyId
			,3
			,@CCProFee
			,0
			,0
			,0
			,0
			,0
			,0
			,''
			,@CreatedBy
			,@CreatedDate
			,@PaymentMethodId
	END

	--Get the deposit fee info for previous item
	SELECT TOP 1 
		@OldOrderLineId = OL.[OrderLineID],
		@OldCurrencyId = PO.[CurrencyId],
		@OldProviderAmt = OL.Total - OS.[Amount] + OS.[Discount],
		@OldDepositFee = OS.[Amount] - OS.[Discount]
	FROM [LB_OrderLineSubrates] OS
		INNER JOIN [uCommerce_OrderLine] OL ON OS.[OrderLineId] = OL.[OrderLineId]
		INNER JOIN [LB_RoomReservation] RR ON OL.[OrderLineId] = RR.[OrderLineId]
		INNER JOIN [uCommerce_PurchaseOrder] PO ON OL.[OrderId] = PO.[OrderId]
	WHERE [SubRateItemCode] ='LBCOM'
		AND RR.[NewOrderReference] = @BookingRef
	
	--inset paid in full status
	INSERT INTO [dbo].[uCommerce_OrderProperty]
		([OrderId]
		,[OrderLineId]
		,[Key]
		,[Value])
	SELECT TOP 1 
		@OrderId
		,NULL
		,[Key]
		,[Value]
	FROM
		uCommerce_OrderLine OL
		INNER JOIN [uCommerce_PurchaseOrder] PO ON OL.OrderId = PO.OrderId
		INNER JOIN [uCommerce_OrderProperty] OP ON OL.OrderId = OP.OrderId
	WHERE 
		OL.[OrderLineId] = @OldOrderLineId AND
		OP.[Key] = '_isPaidinfull'

	--Insert trensfer fee if not free transfer
	IF (@IsFreeTransfer <> 1 AND @TransferFee > 0)
	BEGIN
		UPDATE [dbo].[LB_OrderLineDue]
		SET [SubRateItemCode] = 'TRAFEE',
			[DueTypeId] = 5,
			[UpdatedBy] = @CreatedBy,
			[UpdatedDate] = @CreatedDate
		WHERE [OrderLineId] = @OldOrderLineId
		AND [SubRateItemCode] IN ('LBCOM','TRAFEE')

		EXEC [LB_SaveOrderLineDue]
			NULL
			,@OrderLineId
			,'LBCOM'
			,NULL
			,@CurrencyId
			,2
			,@TransferFee
			,0
			,0
			,0
			,0
			,0
			,0
			,''
			,@CreatedBy
			,@CreatedDate
			,@PaymentMethodId
	END

	--if paid in full reund/change provider amount
	IF EXISTS (SELECT 1 WHERE 1 = [dbo].[LB_FnIsPaidInFull](@OrderLineId))
	BEGIN 
		--Entry for refund due
		DECLARE @ProviderAmt MONEY
		SELECT TOP 1 @ProviderAmt = (OL.Total- OS.Amount + OS.Discount)
		FROM [LB_OrderLineSubrates] OS
			INNER JOIN [uCommerce_OrderLine] OL ON OS.[OrderLineId] = OL.[OrderLineId]
		WHERE OS.OrderLineId = @OrderLineId 
			AND [SubRateItemCode] = 'LBCOM'
		
		DECLARE 
			@CalculatedAmount MONEY,
			@DueType INT

		SELECT @CalculatedAmount = @ProviderAmt - @OldProviderAmt,
			@DueType = CASE WHEN (@ProviderAmt - @OldProviderAmt) > 0 THEN 1 ELSE 6 END

		--set due if calulated amount is not 0
		IF (@CalculatedAmount <> 0)
		BEGIN
			EXEC [LB_SaveOrderLineDue]
				NULL
				,@OrderLineId
				,''
				,NULL
				,@CurrencyId
				,@DueType
				,@CalculatedAmount
				,0
				,0
				,0
				,0
				,0
				,0
				,''
				,@CreatedBy
				,@CreatedDate
				,@PaymentMethodId
		END
	END
END
