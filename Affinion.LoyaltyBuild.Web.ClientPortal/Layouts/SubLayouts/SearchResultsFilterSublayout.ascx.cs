﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SearchResultsFilterSublayout.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.HotelDetailsSublayout
/// Description:           filter the search results from the facets


namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts
{
    using System;
    using System.Web.UI.WebControls;

    public partial class SearchResultsFilterSublayout : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Filter.Text = "Filter";
            //Uri currentUrl = Request.Url;
            // Put user code to initialize the page here
        }

        //protected void Filter_Click(object sender, EventArgs e)
        //{
            
        //}

        //protected void Category_CheckedChanged(object sender, EventArgs e)
        //{
        //    CheckBox chk = (CheckBox)sender;
        //    if(chk.Checked==true)
        //    {
                
        //    }
           
        //}
    }
}