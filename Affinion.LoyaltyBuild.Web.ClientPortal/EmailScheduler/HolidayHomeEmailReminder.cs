﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           HolidayHomeReminder.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal
/// Description:           Handling Post Break Email scheduler
/// </summary>

using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Data;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Affinion.Loyaltybuild.BusinessLogic.Helper;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Order;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic;
namespace Affinion.LoyaltyBuild.Web.ClientPortal.EmailScheduler
{
    public class HolidayHomeEmailReminder
    {
        #region Class variables
        public Item HolidayHomeReminderEmailItem { get; set; }
        private IOrderService _orderService = ContainerFactory.Instance.GetInstance<IOrderService>();
        #endregion

        public void Run(Sitecore.Data.Items.Item[] items, Sitecore.Tasks.CommandItem command, Sitecore.Tasks.ScheduleItem schedule)
        {
            Sitecore.Context.Job.Status.LogInfo("Holiday Home Scheduler");
            if (schedule.LastRun.Date != DateTime.Now.Date)
                ExecuteJob();
        }
        
        /// <summary>
        /// Run the Email scheduler
        /// </summary>
        public void ExecuteJob()
        {
            try
            {
                List<Item> partialPaymentReminderEmail = new List<Item>();

                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("HolidayHomeEmailReminder: Service Start"));
                Sitecore.Context.Job.Status.LogInfo("Scheduler started");

                Database currentDB = Sitecore.Configuration.Factory.GetDatabase("web");
                // To get all clients
                List<Item> clients = currentDB.SelectItems("/sitecore/content/#admin-portal#/#client-setup#//*[@@TemplateName='ClientDetails']").ToList();
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("GetItem From Web: Service Start"));
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("HolidayHomeEmailReminder: GetItem From Web"));
                foreach (Item client in clients)
                {
                    string clientItem = client.Name;
                    // To get all Partial Payment Reminder mail
                    string partialPaymentReminderEmailPath = string.Concat("/sitecore/content/admin-portal/client-setup/", clientItem, "/_supporting-content/partialpaymentreminders");
                    if (currentDB.GetItem(partialPaymentReminderEmailPath) != null)
                    {
                        partialPaymentReminderEmail = currentDB.GetItem(partialPaymentReminderEmailPath).Children.Where(i => i.TemplateName == "PartialPaymentReminder").ToList<Item>();
                    }
                    else
                    {       continue;
                    }
                    string clientId = client.ID.Guid.ToString();
                    //Get the partial payment bookings
                    DataSet ds = _orderService.ProvisionalHolidayHomesBookings(clientId);

                    if (ds != null && ds.Tables.Count > 0)
                    {
                        foreach (Item reminderEmail in partialPaymentReminderEmail)
                        {
                            HolidayHomeReminderEmailItem = reminderEmail;
                            string providerType = SitecoreFieldsHelper.GetValue(reminderEmail, "ProviderType");
                            string noOfDays = SitecoreFieldsHelper.GetValue(reminderEmail, "DaysBefore");
                            string isActive = SitecoreFieldsHelper.GetValue(reminderEmail, "IsActive");
                            string msgBody = SitecoreFieldsHelper.GetValue(reminderEmail, "MailBody");
                            if (isActive == "0" || string.IsNullOrEmpty(msgBody))
                                continue;

                            foreach (DataRow row in ds.Tables[0].Rows)
                            {
                                
                                string receiver = row["EmailAddress"].ToString();

                                if (string.IsNullOrEmpty(receiver))
                                    continue;
                                DateTime checkin;
                                DateTime.TryParse(row["CheckInDate"].ToString(), out checkin);
                                Double nodays;
                                Double.TryParse(noOfDays, out nodays);
                                string amountDue = row["AmountDue"].ToString() + ' ' + row["ISOCode"].ToString();
                                string bookingRefNo = row["OrderReference"].ToString();
                                string customerName = row["CustomerName"].ToString();
                                string providerName = row["ProviderName"].ToString();

                                if ((checkin - DateTime.Today).Days == nodays)
                                {
                                   string msgBodyToSent = PrepareMailBody(msgBody, bookingRefNo, customerName, checkin, nodays, providerName, amountDue);
                                   mailGenarate(receiver, msgBodyToSent);
                                }
                            }
                        }
                    }
                }
                Sitecore.Context.Job.Status.LogInfo("Scheduler finished");
            }
            catch (Exception exception)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("HolidayHomeEmailReminder: Exception-{0}", exception.ToString()));
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
            }

        }

        /// <summary>
        /// Generate Holiday Home Reminder Email 
        /// </summary>
        private void mailGenarate(string receiverId, string msgBody)
        {
            try
            {
                Affinion.LoyaltyBuild.Communications.IEmailService emailService = new Affinion.LoyaltyBuild.Communications.Services.EmailService();

                string senderID = SitecoreFieldsHelper.GetValue(HolidayHomeReminderEmailItem, "FromAddress");

                if (string.IsNullOrEmpty(senderID))
                    return;

                if (string.IsNullOrEmpty(senderID))
                    return;
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("HolidayHomeEmailReminder: SenderId-{0}", senderID));
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("HolidayHomeEmailReminder: ReceiverId-{0}", receiverId));

                string mailSubject = SitecoreFieldsHelper.GetValue(HolidayHomeReminderEmailItem, "Subject");
                emailService.Send(senderID, receiverId, mailSubject, msgBody, true);
            }
            catch (Exception exception)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("HolidayHomeEmailReminder: Exception-{0}", exception.ToString()));
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
            }
        }

        /// <summary>
        /// Prepare the Email body
        /// Replace the content with actual data
        /// </summary>
        private string PrepareMailBody(string bodyText, string bookingRefNo, string customerName, DateTime checkin, Double nodays, string providerName, string amountDue)
        {
            bodyText = bodyText.Replace("{BookingReference}", !string.IsNullOrWhiteSpace(bookingRefNo) ? bookingRefNo : string.Empty);
            bodyText = bodyText.Replace("{GuestName}", !string.IsNullOrWhiteSpace(customerName) ? customerName : string.Empty + (!string.IsNullOrWhiteSpace(customerName) ? customerName : string.Empty));
            bodyText = bodyText.Replace("{ArrivalDate}", !string.IsNullOrWhiteSpace(checkin.ToString("dd/MM/yyyy")) ? checkin.ToString("dd/MM/yyyy") : string.Empty);
            bodyText = bodyText.Replace("{Amount}", !string.IsNullOrWhiteSpace(amountDue.ToString()) ? amountDue : "0");
            bodyText = bodyText.Replace("{PropertyName}", !string.IsNullOrWhiteSpace(customerName) ? providerName : string.Empty + (!string.IsNullOrWhiteSpace(providerName) ? customerName : string.Empty));
            bodyText = bodyText.Replace("{Days}", !string.IsNullOrWhiteSpace(amountDue.ToString()) ? nodays.ToString() : "0");
            return bodyText;
        }

    }

}