﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Api.Booking
{
    public static class Constants
    {
        internal const string ClientId = "_ClientId";
        internal const string PropertyId = "PropertyId";
        public const string PropertyName = "PropertyName";
        internal const string StarRanking = "StarRanking";
        internal const string ImageUrl = "ImageUrl";
        public const string RoomToken = "RoomToken";
        internal const string IsDirect = "IsDirect";
        internal const string MealBasisId = "MealBasisId";
        public const string RoomInfo = "RoomInfo";
        internal const string PropertyReferenceId = "PropertyReferenceId";

        public const string CancelCriteria = "CancelCriteria";
        internal const string ErrataInformation = "ErrataInformation";
        public const string PreBookingToken = "PreBookingToken";
        internal const string Totalcommission = "Totalcommission";
        internal const string UnitPrice = "UnitPrice";
        internal const string Vat = "Vat";

        public const string LeadGuestTitle = "LeadGuestTitle";
        public const string LeadGuestFirstName = "LeadGuestFirstName";
        public const string LeadGuestLastName = "LeadGuestLastName";
        public const string LeadGuestAddress1 = "LeadGuestAddress1";
        public const string LeadGuestPostcode = "LeadGuestPostcode";
        internal const string LeadGuestBookingCountryID = "LeadGuestBookingCountryID";
        public const string LeadGuestPhone = "LeadGuestPhone";
        public const string LeadGuestEmail = "LeadGuestEmail";
        internal const string ContractArrangementID = "ContractArrangementID";
        public const string Request = "Request";
        internal const string TradeReference = "TradeReference";
        internal const string OldPrice = "OldPrice";
        internal const string BookingReferenceNo = "_BookingReference";
        internal const string MaxAdults = "MaxAdults";
        internal const string MaxChildren = "MaxChildren";
        internal const string MinAdults = "MinAdults";
        public const string MarginId = "MarginId";
        public const string MarginPercentage = "MarginPercentage";
        public const string BBCurrencyId = "BBCurrencyId";
        internal const string Destination = "Destination";
        internal const string OrderLineId = "OrderLineId";
        internal const string BookingThrough = "_BookingThrough";
        internal const string CheckinDate = "_CheckinDate";
        internal const string CheckoutDate = "_CheckoutDate";
        internal const string CreatedBy = "CreatedBy";
        public const string ProductType = "_ProductType";
        internal const string GuestName = "_GuestName";
        internal const string CategoryId = "CategoryId";
        internal const string Price = "Price";
        internal const string CommissionAmount = "_LBCommissionFee";
        internal const string Cancellationfee = "_CancellationFee";
        internal const string CalculatedProcessingFee = "CalculatedProcessingFee";
        internal const string ProcessingFee = "ProcessingFee";
        internal const string ProviderAmount = "_ProviderAmount";
        internal const string ProviderAmountPaidOnCheckOut = "ProviderAmountPaidOnCheckOut";
        internal const string Nights = "_Nights";
        internal const string NoOfAdults = "_NoOfAdult";
        internal const string NoOfChildren = "_NoOfChildren";
        internal const string NoOfRooms = "_NoOfRooms";
        internal const string RoomType = "_RoomType";
        internal const string CurrencyId = "_CurrencyID";
        internal const string ChildrenAges = "_ChildrenAge";
        internal const string PaymentMethod = "PaymentMethod";
        internal const string PaymentType = "PaymentType";
        internal const string ProviderType = "ProviderType";
        internal const string AppliedCampaign = "AppliedCampaign";
        internal const string ChildItems = "ChildItems";
        internal const string ParentItem = "ParentItem";
        internal const string BBCancellationToken = "_BBCancellationToken";
        internal const string BBCancellationCharges = "_BBCancellationCharges";
        public const string BBBookingReferenceId = "BBBookingReferenceId";
        internal const string UserSelectedCampaign = "_userselectedcampaign";
        //Store Room SKU & Add On SKU (',') Seprated
        internal const string PackageSKU = "_PackageSKU";
        public const string CurrItemPath = "/sitecore/content/#admin-portal#/#client-setup#/#global#/#_supporting-content#/#bedbankprovidersettings#/#jactravel#/#jactravel-currency#//*[@@templateid='{730153DD-E48F-4F75-9BB1-CC16F8F8A432}']";
        public const string TitleListPath = "/sitecore/content/offline-portal/global/_supporting-content/customer/title";
    }
}
