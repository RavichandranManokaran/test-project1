﻿CREATE TABLE [dbo].[uCommerce_EmailTypeParameter] (
    [EmailTypeId]      INT NOT NULL,
    [EmailParameterId] INT NOT NULL,
    CONSTRAINT [uCommerce_PK_EmailTypeParameter] PRIMARY KEY CLUSTERED ([EmailTypeId] ASC, [EmailParameterId] ASC),
    CONSTRAINT [uCommerce_FK_EmailTypeParameter_EmailParameter] FOREIGN KEY ([EmailParameterId]) REFERENCES [dbo].[uCommerce_EmailParameter] ([EmailParameterId]),
    CONSTRAINT [uCommerce_FK_EmailTypeParameter_EmailType] FOREIGN KEY ([EmailTypeId]) REFERENCES [dbo].[uCommerce_EmailType] ([EmailTypeId])
);

