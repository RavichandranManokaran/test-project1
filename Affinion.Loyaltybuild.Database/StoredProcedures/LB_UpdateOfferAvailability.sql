CREATE PROCEDURE [dbo].[LB_UpdateOfferAvailability]
(@OrderLineId INT,
@CheckInDateCount INT, 
@CheckOutDateCount INT,
@Sku NVARCHAR(512),
@CampaignID int)
AS

WHILE (@CheckInDateCount < @CheckOutDateCount)
BEGIN

UPDATE [LB_OfferAvailability]
SET
	NumberBooked = NumberBooked + 1
WHERE DateCounter = @CheckInDateCount 
	AND ProviderOccupancyTypeID = @Sku
	AND CampaignItem = @CampaignID

UPDATE [LB_RoomAvailability]
SET
	NumberBooked = NumberBooked + 1
WHERE DateCounter = @CheckInDateCount 
	AND ProviderOccupancyTypeID = @Sku

SET @CheckInDateCount = @CheckInDateCount + 1
END