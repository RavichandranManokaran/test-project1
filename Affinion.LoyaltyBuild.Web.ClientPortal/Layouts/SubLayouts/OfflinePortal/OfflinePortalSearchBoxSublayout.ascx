﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalSearchBoxSublayout.ascx.cs" Inherits="Affinion.Loyaltybuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalSearchBoxSublayout" %>
<%@ Register Assembly="Affinion.LoyaltyBuild.Web.Common" Namespace="Affinion.LoyaltyBuild.Web.Common.CustomFields.GoogleMapLocationPicker" TagPrefix="cc1" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Affinion.LoyaltyBuild.Common.Utilities" %>

<%--<script type="text/javascript" src="/Resources/scripts/jquery.min.js"></script>
<script type="text/javascript" src="/Resources/scripts/jquery-ui.js"></script>--%>
<script type="text/javascript" src="/Resources/scripts/affinionCustom.js"></script>

<script src="/Resources/scripts/jquery.nanoscroller.js"></script>
<script src="/Resources/scripts/callscroller.js"></script>

<script src="/Resources/scripts/jquery.dd.min.js"></script>
<link href="/Resources/styles/dd.css" rel="stylesheet" />

<% if (this.useAjax)
   {
%>

<script type="text/javascript">
    jQuery(function () {
        jQuery("#collapse-button").click(function () {
            jQuery("#show-hide").toggle();
        });
    })

    jQuery("#mainform").submit(function (event) {

        event.preventDefault();

        if (ValidateInputs()) {
            var parameters =
            {
                ClientList: jQuery("#ClientList").val(),
                Destination: jQuery("#Destination").val(),
                CheckinDate: jQuery("#CheckinDate").val(),
                CheckoutDate: jQuery("#CheckoutDate").val(),
                NoOfRooms: jQuery("#NoOfRooms").val(),
                NoOfAdults: jQuery("#NoOfAdults").val(),
                NoOfChildren: jQuery("#NoOfChildren").val(),
                OfferGroup: jQuery("#OfferGroup").val()
            };

            jQuery.ajax({

                url: "<%=this.submitUrl %>",
                data: parameters,
                context: document.body

            }).done(function (data) {

                if (typeof RenderResults == 'function') {
                    RenderResults(data);
                }
            });
            }
    });

        var ValidateInputs = function () {
            var result = true;

            var destination = jQuery("#Destination").val();
            var checkinDate = jQuery("#CheckinDate").val();

            if (destination == "") {
                result = false;
                alert("Please fill required fields.");
            }
            return result;
        }

</script>

<%       
   }
%>

<asp:Panel ID="StockSearchPanel" runat="server" DefaultButton="Submit">
    <div class="col-lg-12 visible-sm visible-xs no-padding">
        <div class="book-detail">
            <a id="collapse-button" href="#">
                <span class="fa fa-calendar"></span>
                <sc:text field="MobileVersionSearchLable" id="MobileVersionSearchLable" runat="server" />
            </a>
        </div>
    </div>
    <div class="<%=this.cssClass %>">

        <div class="hotel-search-box">
            <asp:Label ID="DatasourceText" runat="server">Set datasource to view values</asp:Label>
            <h3>
                <sc:text field="TitleLabel" id="TitleLabel" runat="server" />
            </h3>

            <div class="booking-top width-full divider"></div>

            <%--Clients dropdown function--%>
            <div class="col-xs-12">
                <h5>
                    <sc:text field="ClientListLabel" id="ClientListLabel" runat="server" />
                </h5>
                <asp:DropDownList CssClass="form-control styled-select tech ClientList" ID="ClientList" ClientIDMode="Static" runat="server"></asp:DropDownList>
            </div>

            <div class="col-xs-12">
                <h5>
                    <sc:text field="DestinationLabel" id="DestinationLabel" runat="server" />
                </h5>
                <asp:TextBox ID="Destination" CssClass="form-control" Text="" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="CategoryId" Value="" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="DestinationType" Value="" ClientIDMode="Static" runat="server" />
                <div id="divErrorMsg" style="color: red; font-weight: 400">
                    Please enter the value in Destination/hotel name.

                </div>
            </div>

            <div class="ondate <%=DatePickerContainerClass %>">
                <div class="col-xs-7 ">
                    <h5>
                        <sc:text field="CheckinDateLabel" id="CheckinDateLabel" runat="server" />
                    </h5>

                    <div class="width-full">
                        <span class="input-group-btn"></span>
                        <asp:TextBox ID="CheckinDate" CssClass="form-control datepicker width-85 pull-right" Text="" ClientIDMode="Static" runat="server" />
                    </div>
                </div>

                <div class="col-xs-5 ">
                    <h5>
                        <sc:text field="NoOfNightsLabel" id="CheckoutDateLabel" runat="server" />
                    </h5>

                    <div class="width-full">
                        <asp:HiddenField ID="CheckoutDate" runat="server" Value='' />
                        <div role="group" aria-label="...">
                            <asp:DropDownList CssClass="form-control" ID="NoOfNights" ClientIDMode="Static" runat="server"></asp:DropDownList>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-xs-12" runat="server" visible="false">
                <div class="checkbox">
                    <label>
                        <asp:CheckBox ID="DateSelectionCheckBox" Text="I don't have specific dates yet" ClientIDMode="Static" runat="server" />
                    </label>
                </div>
            </div>

            <div class="booking-deatail-order-room width-full inline-block">

                <div class="col-xs-12 col-md-4">
                    <h5>
                        <sc:text field="RoomsLabel" id="RoomsLabel" runat="server" />
                    </h5>
                    <div role="group" aria-label="...">
                        <asp:DropDownList CssClass="form-control" ID="NoOfRooms" ClientIDMode="Static" runat="server"></asp:DropDownList>
                    </div>
                </div>

                <div class="col-xs-12 col-md-4">
                    <h5>
                        <sc:text field="AdultsLabel" id="AdultsLabel" runat="server" />
                    </h5>
                    <div role="group" aria-label="...">
                        <asp:DropDownList CssClass="form-control" ID="NoOfAdults" ClientIDMode="Static" runat="server"></asp:DropDownList>
                    </div>
                </div>

                <div class="col-xs-12 col-md-4">
                    <h5>
                        <sc:text field="ChildrenLabel" id="ChildrenLabel" runat="server" />
                    </h5>
                    <div role="group" aria-label="...">
                        <asp:DropDownList CssClass="form-control" ID="NoOfChildren" ClientIDMode="Static" runat="server"></asp:DropDownList>
                    </div>
                </div>

                <div class="col-xs-12 no-padding ageGroupOuter" style="display: none">
                    <div class="age-group">
                        <div class="col-xs-12">
                            <span>
                                <sc:text field="ChildrenAgeTitleLabel" id="ChildrenAgeTitleLabel" runat="server" />
                            </span>
                        </div>
                        <asp:Panel ID="ChildAgePanel" runat="server">
                        </asp:Panel>

                    </div>
                </div>
                <div class="col-xs-2"></div>
            </div>

            <div runat="server" id="ErrorPanel" class="col-xs-12 error-msg">
            </div>

            <%--Offer Group dropdown--%>
            <div class="col-xs-12 hide">
                <h5>
                    <sc:text field="OfferGroupLabel" id="OfferGroupLabel" runat="server" />
                </h5>
                <asp:DropDownList CssClass="form-control styled-select" ID="OfferGroup" ClientIDMode="Static" runat="server"></asp:DropDownList>
            </div>

            <div id="ServerSideErrorPanel" runat="server" class="col-xs-12 error-msg"></div>

            <div class="col-xs-12">
                <asp:Button ID="Submit" runat="server" Text="" class="btn btn-primary booking-margin-top float-right btn-bg searchBtn" OnClientClick="javascript:SubmitSearchButton();" />
            </div>
        </div>

    </div>
</asp:Panel>

<div class="col-xs-12 no-padding"></div>


<script type="text/javascript">
    var clientname;

    $(document).ready(function () {


        $("#divErrorMsg").hide();

        $("#CheckinDate").prop("readonly", true);

        $("#ClientList").msDropdown({ roundedCorner: false });

        IsSubmitSearchButtonDisabled();

        $("#ClientList").change(function () {
            IsSubmitSearchButtonDisabled();
        });

        function IsSubmitSearchButtonDisabled() {
            if (($("#ClientList option:selected").index()) == 0) {

                $('.searchBtn').prop('disabled', true);
            }
            else {
                $('.searchBtn').prop('disabled', false);
            }
        }

        jQuery('#DateSelectionCheckBox').change(function () {
            if (this.checked)
                jQuery('.ondate').slideUp("slow");
            else
                jQuery('.ondate').slideDown("slow");
        });
    });

    jQuery(function ($) {
        $("#ClientList").change(function () {
            var client = $("#ClientList option:selected").data("clientid");
            if (client != null && client != undefined) {
                $.get("/handlers/cs/ServerSideEventHandler.ashx?action=SetClientId&value=" + client, function (data) {

                });
                //document.cookie = "_clientid=" + client;
            }
        });
        jQuery("#CheckinDate").datepicker({
            defaultDate: "+1w",
            dateFormat: "<%=DateTimeHelper.DateFormatJavaScript%>",
            minDate: 0,
            numberOfMonths: 1,
            showOn: "both",
            buttonImage: "images/calendar.png",
            buttonImageOnly: false,
            buttonClass: "glyphicon-calendar"
        }).next(".ui-datepicker-trigger").addClass("btn btn-default glyphicon glyphicon-calendar calander-icon pull-right");

        /*date picker position script*/
        $(".datepicker").focus(function (event) {
            var dim = $(this).offset();
            $("#ui-datepicker-div").offset({
                top: dim.top - -30,
            });
        });

        jQuery("form").submit(function (e) {

            if (SubmitClientClick() == false) {
                e.preventDefault();
            }
        });
    });

    var searchBox = jQuery('#Destination');

    searchBox.autocomplete({
        source: function (request, response) {
            var clientSetupItemId = window[$("#ClientList").val()];
            $("#CategoryId").val("");
            $("#DestinationType").val("");
            if ($("#ClientList").val()) {
                loadSpinner = false;
                $.ajax({
                    type: "POST",
                    url: "/webservices/autocomplete.asmx/GetSuggestionsOfflinePortal",
                    data: JSON.stringify({ clientSetupId: clientSetupItemId, term: searchBox.val() }),
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var d = eval(data);
                        response($.map(eval(d.d), function (item) {
                            return {
                                label: item.Text,
                                value: item.Text,
                                itemId: item.ItemId,
                                destinationType: item.Type
                            }
                        }));
                    },
                    error: function () {
                        response([]);
                    }
                });
                loadSpinner = true;
            }
        },
        select: function (event, ui) {
            event.preventDefault();
            searchBox.val(ui.item.label);
            $("#CategoryId").val(ui.item.itemId);
            $("#DestinationType").val(ui.item.destinationType);
            return false;
        }
    });

    jQuery("select[id^='AgeDropdownList']").change(function () {
        
        var isValid = true;
        var noOfChildren = $('#NoOfChildren').val();
        for (var i = 0; i < noOfChildren; i++) {
            var age = $('#AgeDropdownList' + (i + 1)).val();
            console.log($('#AgeDropdownList' + (i + 1)));
            if (age === 'Select') {
                $("#ErrorPanel").show();
                return false;
            }
        }

        if (isValid)
            $("#ErrorPanel").hide();
    });

    function SubmitClientClick() {

        $("#ErrorPanel").hide();
        var isValid = true;
        var noOfChildren = $('#NoOfChildren').val();
        for (var i = 0; i < noOfChildren; i++) {
            var age = $('#AgeDropdownList' + (i + 1)).val();
            console.log($('#AgeDropdownList' + (i + 1)));
            if (age === 'Select') {
                $("#ErrorPanel").show();
                return false;
            }
        }

        return true;
    }

    jQuery('#NoOfChildren').change(function () {
        var childCount = "";
        jQuery("select option:selected").each(function () {
            childCount = $('#NoOfChildren').val();
            if (childCount >= '0')
                jQuery("#ErrorPanel").hide();
        });

    })

    function SubmitSearchButton() {
        $("#divErrorMsg").hide();

        if ($("#Destination").val() != "") {
            if (SubmitClientClick()) {
                if (Page_ClientValidate('submitBtnValidation')) {
                    __doPostBack('Submit');
                }
            }
        }
        else {
            $("#divErrorMsg").show();
            event.preventDefault();
        }
    }

    // making destination as default value in search results
    $("#Destination").on("keypress keyup blur", function (event) {
        if ($("#Destination").val() != "") {
            $("#divErrorMsg").hide();
        }
        else {
            $("#divErrorMsg").show();
        }
    });

</script>
