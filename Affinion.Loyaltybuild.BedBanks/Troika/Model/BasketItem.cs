﻿using System;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Message;

namespace Affinion.LoyaltyBuild.BedBanks.Troika.Model
{
    [Serializable]
    public class BasketItem : JtResponse
    {
        public string AvailabilitySourceId;
        public string BedBankProviderId;
        public string ClientOfferID;
        public string ItemID;
        public string ItemName;
        public string ItemRanking;
        public string ItemTypeId;
        public string Latitude;
        public string Longitude;
        public string MaxNumberofAdults;
        public string MaxNumberofChildren;
        public string MaxNumberTotal;
        public string PictureFileName;
        public string CountryId;
        public string ProviderEmail;
        public string ProviderLanguageId;
        public string ProviderId;
        public string ProviderName;
        public string StarRanking;
        public string LocalRanking;
        public string MapLocationName;
        public string LocationId;
        public string LocationName;
        public string OfferId;
        public string WebIntroduction;
        public string CoicrId;
        public string ItemPaymentLevel;
        public string RateId;
        public string RateTypeId;
        public string Priority;
        public string Rate;
        public int CurrencyCode;
        public string CurrencyName;
        public string RateTypeDescription;
        public string NumberofNights;
        public string ProviderTypeId;
        public string NumberofAdults;
        public string NumberofChildren;
        public string ItemExtraDescription;
    }
}
