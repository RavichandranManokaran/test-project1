﻿var loadSpinner = true;
(function () {
    "use strict";
    jQuery(document).ajaxStart(function () {
        if (loadSpinner)
            jQuery.LoadingOverlay("show", { image: "/resources/styles/cs/loading.gif", zIndex: 100000 });
    });
    jQuery(document).ajaxStop(function () {
        jQuery.LoadingOverlay("hide");
    });
}());


var CS = {};

(function ($) {
    "use strict";
    //order service
    function OrderService(common) {
        var order = this;
        order.CancelOrder = function () {
            var form = $("#CancelForm");
            form.validate();

            if (form.valid()) {
                $.post("",
                form.serialize(),
                function (data) {
                    common.ShowResult(data);
                })
             .fail(function () {
                 common.ShowGenericError();
             });
            }
        };
        order.TrasferOrder = function () {
        };
    }
    //payment service
    function PaymentService(common) {
        var payment = this;
        payment.GetRefundablePayments = function (orderlineid, paymenttype, currencyid) {
            $("input#refundreference").val("");
            $("input#availablerefund").val("0");

            $.get("/handlers/cs/paymentservice.ashx?action=getpaymenttypehistory&olid=" + orderlineid + "&ptid=" + paymenttype + "&cid=currencyid",
            function (data) {
                $("#refundpayment").html(data);
                $('#refundablepayments td').off('click').on('click', function (event) {
                    if ($(this).hasClass('active')) {
                        $(this).removeClass('active');
                    } else {
                        $(this).addClass('active').siblings().removeClass('active');
                        $("input#refundreference").val($(this).data("ref"));
                        $("input#availablerefund").val($(this).data("availamt"));
                    }
                });
            }
        ).fail(function () {
            common.ShowGenericError();
        });
        };
        payment.SaveDue = function () {
            alert('test');
        };
        payment.DeleteDue = function (dueid, reason) {
            $.post("/handlers/cs/paymentservice.ashx?action=deletedue", JSON.stringify({ DueId: dueid, Reason: reason }),
            function (data) {
                if (data.IsSuccess) {
                    $("tr[data-dueid=" + dueid + "]").remove();
                    payment.RefreshDueByCurrencies();
                }
                common.ShowResult(data);
            }
        ).fail(function () {
            common.ShowGenericError();
        });
        };
        payment.RefreshDueByCurrencies = function () {
            var olid = $("#paymenthistory").data("orderlineid");
            $.get("/handlers/cs/paymentservice.ashx?action=getduebycurrencies&olid=" + olid,
        function (data) {
            $("#duebycurrencies").html(data);
        });
        };
        payment.Refund = function (refund) {
            return $.post("/handlers/cs/paymentservice.ashx?action=refund", JSON.stringify(refund));
        };
        payment.SetRefundMode = function (mode) {
            alert(mode);
        };
    }
    //common service
    function Common() {
        var common = this;
        common.ShowResult = function (result) {
            common.ShowAlert(result.Message, '');
            return result.IsSuccess;
        };
        common.ShowGenericError = function () {
            common.ShowAlert(Alerts.AlertMessagesCustomerQuery.GeneralErrorMessage, ''); //'Your request cannot be processed now'
        };
        common.ShowAlert = function (message, title, alertType, buttns) {
            if (buttns === undefined) {
                buttns = {
                    "Ok": function () {
                        $(this).dialog("close");
                    }
                };
            }
            if (title == "" || title == undefined) {
                title = "Alert!";
            }
            $("#dialog-confirm").html("<div>" + message + "</div>");
            $("#dialog-confirm").dialog({
                title: title,
                resizable: true,
                width: 400,
                modal: true,
                close: function () {
                    $("#dialog-confirm").html("");
                },
                buttons: buttns
            });
        };
    }
    //communication service
    function CommunicationService(common) {
        var comms = this;
        comms.LoadCustomerQueries = function (data, queryStr) {
            var url = "/handlers/cs/searchcustomerquery.ashx" + ((queryStr == null) ? "" : ("?" + queryStr));

            if ($("#custqueryresult").length) {
                $.ajax({
                    type: "POST",
                    url: url,
                    data: JSON.stringify(data)
                }).done(function (data) {
                    $("#custqueryresult").html(data);
                });
            }

        };
        comms.LoadQueryDetail = function (queryId) {
            var url = "/handlers/cs/customerquerydetail.ashx?queryid=" + queryId;
            $.ajax({
                type: "GET",
                url: url
            }).success(function (data) {
                $("#hdselectedqueryid").val(queryId);
                $("#QueryDetail").html(data);
                $("#dvQuery").modal();
                comms.BindFileUpload();
            }).error(function (data) {
                $("#hdselectedqueryid").val(0);
                $("#QueryDetail").html(data);
                $("#dvQuery").modal();
            });
        };


        /*  Code for Cancelling a Booking - start */
        comms.LoadCancellationDetails = function (queryId) {
            comms.LoadCancelDetail(queryId);
        }

        comms.CancelBookingList = function () {
            $("#btnCompleteCancel").unbind("click");

            $("#btnCompleteCancel").click(function () {
                comms.CompleteCancellation();
            });
        }

        comms.CompleteCancellation = function () {
            var url = "/handlers/cs/UpdateBookingCancellation.ashx";
            $.ajax({
                type: "POST",
                data: { "CancelReason": $("#ta_CancelReason").val(), "NoteToProvider": $("#ta_NoteToProvider").val(), "orderlineid": $("#HiddenOrderlineid").val() },
                url: url
            }).success(function (data) {
                comms.ShowCancelMsg(data);
                $("#dvCancelDialog").modal('hide');

            }).error(function (data) {
                comms.ShowCancelMsg(data);
                $("#dvCancelDialog").modal('hide');
            });
        }

        comms.ShowCancelMsg = function (msg) {
            $("#cancelyesno").html(msg);
            $("#dvmessage1").modal();
        }

        comms.LoadCancelDetail = function (queryId) {
            var url = "/handlers/cs/BookingCancellation.ashx?orderlineid=" + queryId;
            $.ajax({
                type: "GET",
                url: url
            }).success(function (data) {
                if (data == "Booking past cancellation time" || data == "Booking already cancelled" || data == "Invalid booking details") {
                    comms.ShowCancelMsg(data);
                    $("#dvCancelDialog").modal('hide');
                }
                else {
                    $("#CancelDetail").html(data);
                    $("#HiddenOrderlineid").val(queryId);
                    $("#dvCancelDialog").modal();
                    comms.CancelBookingList();
                }
            }).error(function (data) {
                comms.ShowCancelMsg(data);
                $("#dvCancelDialog").modal('hide');
            });
        }
        /*  Code for Cancelling a Booking - end */

        comms.DeleteFile = function () {
            var url = "/handlers/cs/deletefile.ashx?queryid=" + $("#hdselectedqueryid").val();
            $.ajax({
                type: "GET",
                url: url
            }).success(function (data) {
                if (data == "1") {
                    comms.ShowMsg(Alerts.AlertMessagesCustomerQuery.FileDeleteSuccess);//'file deleted successfully'
                    $("#btnViewFile").hide();
                    $("#btnDelete").hide();
                }
            }).error(function (data) {

            });
        };
        comms.ViewUploadFile = function (queryid) {
            if (queryid === null || queryid == undefined) {
                queryid = $("#hdselectedqueryid").val();
            }
            window.location.href = "/handlers/cs/viewuploadfile.ashx?queryid=" + queryid;
            return false;
        };
        comms.ResetNote = function () {
            $("#txtAddNotes").val('');
            $("#ddlEnquiryType").val($("#hdquerytype").val());
            var isresolved = $("#hdquerysolved").val();
            if (isresolved == "1") {
                $("#rdryes").prop("checked", true);
            }
            else {
                $("#rdryno").prop("checked", true);
            }
        };
        comms.UpdateNote = function () {
            var notes = $("#txtAddNotes").val();
            if (notes !== '') {
                var url = "/handlers/cs/UpdateCustomerQueryDetail.ashx";
                $.ajax({
                    type: "POST",
                    data: { "ID": $("#hdselectedqueryid").val(), "QueryTypeId": $("#ddlEnquiryType").val(), "IsSolved": $("#rdryes").is(":checked"), "Content": $("#txtAddNotes").val() },
                    url: url
                }).success(function (data) {
                    if (data == "1") {
                        comms.ShowMsg(Alerts.AlertMessagesCustomerQuery.QueryUpdateSuccess); //'Query updated successfully'
                        $("#hdselectedqueryid").val(0);
                        $("#dvQuery").modal('hide');
                    }
                    else if (data == "2") { comms.ShowMsg(Alerts.AlertMessagesCustomerQuery.NotesCreditCardNo); }
                    else {
                        comms.ShowMsg(Alerts.AlertMessagesCustomerQuery.QueryUpdateFailure);
                    }

                }).error(function (data) {

                });
            }
            else {
                comms.ShowMsg(Alerts.AlertMessagesCustomerQuery.PlsEnterNotes);
            }
        };
        comms.LoadCustomerQuery = function (queryId) {
            comms.LoadQueryDetail(queryId);
        };
        comms.ShowMsg = function (msg) {
            $("#msgcontent").html(msg);
            $("#dvmessage").modal();

        };
        comms.BindFileUpload = function () {
            $("#btnFileUpload").unbind("click");
            $("#btnViewFile").unbind("click");
            $("#btnDelete").unbind("click");
            $("#btnUpdate").unbind("click");
            $("#btnReset").unbind("click");

            $("#btnUpdate").click(function () {
                comms.UpdateNote();
            });


            $("#btnReset").click(function () {
                comms.ResetNote();
            });


            $("#btnFileUpload").click(function () {
                $("#btnselectfile").click();
            });

            $("#btnViewFile").click(function () {
                comms.ViewUploadFile();
            });

            $("#btnDelete").click(function () {
                comms.DeleteFile();
            });

            $('input[id=btnselectfile]').on('change', function (e) {
                if (e.target.files == undefined) {
                    alert(Alerts.AlertMessagesCustomerQuery.FileTypeBrowser); //File upload not Support for this browser
                    return;
                }
                var selectFile = e.target.files[0],
                    ext = selectFile.name.match(/\.(.+)$/)[1],
                    validfile = false;
                switch (ext) {
                    case 'doc':
                    case 'zip':
                    case 'docx':
                    case 'rar':
                    case 'pdf':
                    case 'xls':
                    case 'xlsx':
                        validfile = true;
                        break;
                    default:
                        alert(Alerts.AlertMessagesCustomerQuery.FileTypeMessage); //'This is not an allowed file type.'
                }
                if (validfile) {

                    var test = new FormData();
                    test.append(selectFile.name, selectFile);
                    $.ajax({
                        url: "/handlers/cs/UploadHandler.ashx?queryId=" + $("#hdselectedqueryid").val(),
                        type: "POST",
                        contentType: false,
                        processData: false,
                        data: test,
                        // dataType: "json",
                        success: function (result) {
                            if (result == "1") {
                                $("#btnViewFile").show();
                                $("#btnDelete").show();
                                comms.ShowMsg(Alerts.AlertMessagesCustomerQuery.FileUploadSuccess); //file uploaded successfully'
                            }
                        },
                        error: function (err) {
                            alert(err.statusText);
                        }
                    });

                }
            });


        };
    }
    //mail service
    function MailService(common) {
        var mail = this;
        mail.ReSendMail = function (historyId) {
            var url = "/handlers/cs/CommunicationHistoryHandler.ashx?historyId=" + historyId;
            $.ajax({
                type: "GET",
                url: url
            }).success(function (data) {
                if (data == "1") {
                    common.ShowAlert('Mail sent successfully', "");
                }
                else {
                    common.ShowAlert('Mail sending failed', "");
                }
            }).error(function (data) {
            });
        };
        mail.ShowMsg = function (msg) {
            $("#msgcontent").html(msg);
            $("#dvmessage").modal();
        };
    }
    //basket service
    function BasketService(common) {
        var basket = this;
        var addToBasketItem;

        var PrepareBasketRequest = function () {
            var childAge = "";
            $("select[id^='AgeDropdownList']").each(function () {
                if ($(this).val() != 'Select')
                    childAge = $(this).val() + "," + childAge;
            });
            var spn = $(addToBasketItem);
            var ddl = $('#ddlNoOfRooms');
        
            var basketRequest = {};
            basketRequest.Sku = spn.data("sku");
            basketRequest.VariantSku = spn.data("variatsku");
            basketRequest.CheckInDate = spn.data("date");
            basketRequest.RoomType = spn.data("roomtype");
            basketRequest.CurrencyId = spn.data("currencyid");
            basketRequest.NoOfNights = spn.data("noofnights");
            basketRequest.CheckOutDate = spn.data("checkoutdate");
            basketRequest.Destination = "";
            basketRequest.ChildrenAges = childAge;
            basketRequest.NoOfRooms = ddl.val();
            basketRequest.PreBookingToken = spn.data("prebookingtoken");
            basketRequest.MealBasisID = spn.data("mealbasisid");
            basketRequest.PropertyReferenceId = spn.data("propertyrefid");
            basketRequest.RoomInfo = spn.data("roominfo");
            basketRequest.BBCurrencyId = spn.data("bbcurrencyid");
            basketRequest.IsDirect = spn.data("isdirect");
            basketRequest.PropertyName = $(addToBasketItem).closest("table").data("hotelname");
            if (spn.data("marginid") != null && spn.data("marginid") != ""){
                basketRequest.MarginIdApplied = spn.data("marginid");
            }
            else {
                basketRequest.MarginId = 0;
            }
            if (spn.data("marginpercentage") != null && spn.data("marginpercentage") != "") {
                basketRequest.BBMarginPercentage = spn.data("marginpercentage");
            }
            else {
                basketRequest.MarginPercentage = 0;
            }
            basketRequest.CreatedBy = '<%=CreatedBy%>';
            if (spn.data("oldprice") != null && spn.data("oldprice") != "") {
                basketRequest.OldPrice = spn.data("oldprice");
            } else {
                basketRequest.OldPrice = 0;
            }
            if (spn.data("providerid") != null && spn.data("providerid") != "") {
                basketRequest.PropertyID = parseInt(spn.data("providerid"));
            } else {
                basketRequest.PropertyID = 0;
            }
            if (spn.data("noofadults") != null && spn.data("noofadults") != "") {
                basketRequest.NoOfAdults = parseInt(spn.data("noofadults"));
            } else {
                basketRequest.NoOfAdults = 0;
            }
            if (spn.data("noofchildren") != null && spn.data("noofchildren") != "") {
                basketRequest.NoOfChildren = parseInt(spn.data("noofchildren"));
            } else {
                basketRequest.NoOfChildren = 0;
            }
            if (spn.data("promotionid") != null && spn.data("promotionid") != "") {
                basketRequest.CampaignId = spn.data("promotionid");
            }

            basketRequest.BookingThrough = "Offline";
            basketRequest.ClientId = $("#hdnClientID").val();
            basketRequest.NoOfRooms = parseInt($('#ddlNoOfRooms').val(), 10);//basketRequest.NoOfRooms =1;
            

            return basketRequest;
        }

        this.CreateBasketItem = function(item)
        {
            addToBasketItem = item;
        }

        this.AddToBasket = function(mode)
        {
            var req = PrepareBasketRequest();
            $.ajax({
                url: "/Handlers/CS/AddtoBasket.ashx",
                type: 'post',
                data: JSON.stringify(req),
                contenttype: "application/json;charset=utf-8",
                traditional: true,
                success: function (results) {
                    if (results.IsSuccess) {
                        if (mode === "transfer") {
                            $.LoadingOverlay("show", { image: "/resources/styles/cs/loading.gif", zIndex: 100000 });
                            window.location.href = "/transferbooking?action=transfer&olid=" + $("#hdnOldOrderLineID").val();
                        }
                        else if (mode === "checkout") {
                            $.LoadingOverlay("show", { image: "/resources/styles/cs/loading.gif", zIndex: 100000 });
                            window.location.href = "/basket";
                        }
                        else {
                            var avail = $(addToBasketItem).data("availability");
                            avail = avail - req.NoOfRooms;
                            if (avail == 0) {
                                var parentItem = $(addToBasketItem).parent();
                                if (parentItem.hasClass('accent54-bg'))
                                    parentItem.removeClass('accent54-bg').addClass('availability-zero');
                                else
                                    parentItem.addClass('disable-box');
                                parentItem.removeClass('body-text');
                                parentItem.html("N/A");
                            }
                            else {
                                $(addToBasketItem).data("availability", avail);
                                $(addToBasketItem).find("span").html(avail);
                            }
                            UpdateBasketCount(req.NoOfRooms);
                            common.ShowAlert("Item added to basket.")
                        }
                    }
                    else {
                        common.ShowResult(results);
                    }
                },
                error: function (errorText) {
                    common.ShowAlert(errorText);
                }
            });
        }
    }

    CS.Common = new Common();
    CS.OrderService = new OrderService(CS.Common);
    CS.PaymentService = new PaymentService(CS.Common);
    CS.CommunicationService = new CommunicationService(CS.Common);
    CS.MailService = new MailService(CS.Common);
    CS.BasketService = new BasketService(CS.Common);
})(jQuery);