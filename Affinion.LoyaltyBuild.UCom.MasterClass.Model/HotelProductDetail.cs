﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    public class HotelDetail
    {
        public string SKU { get; set; }

        public string Name { get; set; }

        public string ItemId { get; set; }

        public int StarRanking { get; set; }

        public int LocalRanking { get; set; }

        public List<HotelProductDetail> HotelProductDetail { get; set; }
    }

    public class HotelProductDetail
    {
        /// <summary>
        /// Gets or sets the offer name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the offer sku
        /// </summary>
        public string Sku { get; set; }

        /// <summary>
        /// Gets or sets the offer sku
        /// </summary>
        public string VariatSku { get; set; }

        /// <summary>
        /// Max adult count
        /// </summary>
        public int MaxAdults { get; set; }

        /// <summary>
        /// Max children count
        /// </summary>
        public int MaxChildren { get; set; }

        /// <summary>
        /// Max People count
        /// </summary>
        public int MaxPeople { get; set; }

        /// <summary>
        /// Max infant count
        /// </summary>
        public int MaxInfants { get; set; }

        /// <summary>
        /// Gets or sets the offer group name
        /// </summary>
        public string PackageName { get; set; }

        /// <summary>
        /// Gets or sets the Offer Price
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Gets or sets the Offer Price
        /// </summary>
        public decimal DiscountedPrice { get; set; }

        /// <summary>
        /// Gets or sets value indicating weather the price is discounted
        /// </summary>
        public bool IsDiscounted { get; set; }

        /// <summary>
        /// Gets or sets the currency
        /// </summary>
        public Currency PriceCurrency { get; set; }

        /// <summary>
        /// Gets or sets the offer's occupancy type availability
        /// </summary>
        public int Availability { get; set; }
        
        public List<DayWiseAvaialbility> RoomAvailability { get; set; }

        public int CampaignItemId { get; set; }

        public bool SubrateApplied { get; set; }

    }
}
