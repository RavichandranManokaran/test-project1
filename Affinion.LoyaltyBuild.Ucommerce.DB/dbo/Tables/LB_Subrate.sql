﻿CREATE TABLE [dbo].[LB_Subrate] (
    [SubrateItemCode] NVARCHAR (50)  NOT NULL,
    [ItemID]          NVARCHAR (50)  NOT NULL,
    [VAT]             FLOAT (53)     CONSTRAINT [DF_uCommerce_Subrate_VAT] DEFAULT ((0)) NOT NULL,
    [ClientID]        NVARCHAR (200) NOT NULL,
    [IsPercentage]    BIT            CONSTRAINT [DF_uCommerce_Subrate_IsPercentage] DEFAULT ((0)) NOT NULL,
    [CreatedOn]       DATETIME       CONSTRAINT [DF_uCommerce_Subrate_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [UpdatedOn]       DATETIME       CONSTRAINT [DF_uCommerce_Subrate_UpdatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]       NVARCHAR (50)  CONSTRAINT [DF_uCommerce_Subrate_CreatedBy] DEFAULT (N'Admin') NOT NULL,
    [Price]           FLOAT (53)     NULL,
    [currency]        NVARCHAR (100) DEFAULT ('global') NOT NULL,
    CONSTRAINT [PK_uCommerce_Subrate_1] PRIMARY KEY CLUSTERED ([SubrateItemCode] ASC, [ItemID] ASC, [ClientID] ASC, [currency] ASC)
);

