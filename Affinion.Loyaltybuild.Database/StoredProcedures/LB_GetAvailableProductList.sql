﻿CREATE PROCEDURE [dbo].[LB_GetAvailableProductList](@CheckInDateCount int, @CheckOutDateCount int, @ProductSku int)
AS

DECLARE @NoOfDays int;


SET @NoOfDays=  @CheckOutDateCount-@CheckInDateCount;

SELECT [AvailabilityID]
      --,[ProviderID]
      
      --,[AvailabilityDate]
      ,[DateCounter]
      ,[ProviderOccupancyTypeID]
	  ,[NumberAllocated]
      ,[NumberBooked]
  FROM [dbo].[uCommerce_Booking_Availability]
WHERE [ProviderOccupancyTypeID]=@ProductSku
AND [DateCounter]<@CheckOutDateCount 
AND [DateCounter]>=@CheckInDateCount
AND [NumberBooked]<[NumberAllocated]
AND IsStopSellOn=0 
AND IsCloseOut=0

GO


