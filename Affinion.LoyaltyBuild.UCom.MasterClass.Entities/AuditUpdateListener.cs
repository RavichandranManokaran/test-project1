﻿using Affinion.LoyaltyBuild.Audit;
using NHibernate;
using NHibernate.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Marketing.Awards;
using UCommerce.Marketing.Targets;
using UCommerce.Security;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class AuditUpdateListener : IPostUpdateEventListener
    {
        public IUserService UserService { get; set; }
        
        public AuditUpdateListener(IUserService userService)
        {
            this.UserService = userService;
        }

        private const string _noValueString = "*No Value*";

        private static string getStringValueFromStateArray(object[] stateArray, int position)
        {
            try
            {
                var value = stateArray[position];

                return value == null || value.ToString() == string.Empty
                           ? _noValueString
                           : value.ToString();
            }
            catch (Exception)
            {
                return _noValueString;

            }

        }

        public void OnPostUpdate(PostUpdateEvent updateEvent)
        {
            //if (updateEvent.Entity is AuditLogEntry)
            //{
            //    return;
            //}
            if (updateEvent.Entity is ITarget || updateEvent.Entity is IAward || updateEvent.Entity is CampaignItem || updateEvent.Entity is CampaignItemProperty)
            {
                StringBuilder stroldValue = new StringBuilder();
                StringBuilder strnewValue = new StringBuilder();
                var entityFullName = updateEvent.Entity.GetType().FullName;

                if (updateEvent.OldState == null)
                {
                    throw new ArgumentNullException("No old state available for entity type '" + entityFullName +
                                                    "'. Make sure you're loading it into Session before modifying and saving it.");
                }

                var dirtyFieldIndexes = updateEvent.Persister.FindDirty(updateEvent.State, updateEvent.OldState, updateEvent.Entity, updateEvent.Session);

                var session = updateEvent.Session.GetSession(EntityMode.Poco);

                foreach (var dirtyFieldIndex in dirtyFieldIndexes)
                {
                    //For component types, check:
                    //	updateEvent.Persister.PropertyTypes[dirtyFieldIndex] is ComponentType

                    var oldValue = getStringValueFromStateArray(updateEvent.OldState, dirtyFieldIndex);
                    var newValue = getStringValueFromStateArray(updateEvent.State, dirtyFieldIndex);

                    if (oldValue == newValue)
                    {
                        continue;
                    }
                    else
                    {
                        stroldValue.Append(string.Format("\"{0}\":\"{1}\",", updateEvent.Persister.PropertyNames[dirtyFieldIndex], oldValue));
                        strnewValue.Append(string.Format("\"{0}\":\"{1}\",", updateEvent.Persister.PropertyNames[dirtyFieldIndex], newValue));
                    }
                }
                if (stroldValue.Length > 0 || strnewValue.Length > 0)
                {
                    AuditTrail.Instance.Log(new AuditData()
                    {
                        AuditName = AuditNames.Promotion,
                        Date = DateTime.Now,
                        Message = string.Empty,
                        NewValue = string.Format("{{{0}}}", strnewValue.ToString()),
                        ObjectId = updateEvent.Id.ToString().ToString(),
                        ObjectType = AuditObjectType.Ucommerce,
                        Operation = AuditOperations.Edit,
                        OldValue = string.Format("{{{0}}}", stroldValue.ToString()),
                        Remarks = strnewValue.ToString(),
                        Section = AuditSection.AdminPortalUcommerce,
                        Username = this.UserService.GetCurrentUserName(),
                        Time = DateTime.Now

                    });
                }
                session.Flush();
            }
        }


    }
}