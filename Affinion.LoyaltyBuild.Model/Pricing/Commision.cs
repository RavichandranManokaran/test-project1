﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Affinion.LoyaltyBuild.Model.Pricing
{
    /// <summary>
    /// Commission details
    /// </summary>
    public class Commision
    {
        /// <summary>
        /// Common commission if something is not set
        /// </summary>
        public decimal Common { get; set; }

        /// <summary>
        /// Day 1 commission
        /// </summary>
        public decimal Day1 { get; set; }

        /// <summary>
        /// Day 2 commission
        /// </summary>
        public decimal Day2 { get; set; }

        /// <summary>
        /// Day 3 commission
        /// </summary>
        public decimal Day3 { get; set; }

        /// <summary>
        /// Day 4 commission
        /// </summary>
        public decimal Day4 { get; set; }

        /// <summary>
        /// Day 5 commission
        /// </summary>
        public decimal Day5 { get; set; }

        /// <summary>
        /// Day 6 commission
        /// </summary>
        public decimal Day6 { get; set; }

        /// <summary>
        /// Day 7 commission
        /// </summary>
        public decimal Day7 { get; set; }

        /// <summary>
        /// Processing fee
        /// </summary>
        public decimal ProcessingFee { get; set; }
    }
}
