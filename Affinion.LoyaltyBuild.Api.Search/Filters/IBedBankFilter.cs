﻿

using Affinion.LoyaltyBuild.BedBanks.General;
namespace Affinion.LoyaltyBuild.Api.Search.Filters
{
    /// <summary>
    /// Interface to get BedBank Filters
    /// </summary>
    public interface IBedBankFilter
    {
        /// <summary>
        /// Bed bank Search Criteria
        /// </summary>
        /// <param name="criteria"></param>
        void BuildbbSearch(ref BBsearchCriteria criteria);
    }
}
