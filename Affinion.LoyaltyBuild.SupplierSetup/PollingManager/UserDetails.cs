﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           UserDetails.cs
/// Sub-system/Module:     Supplier Setup
/// Description:           Supplier Setup Polling
/// </summary>

#region Using Directives
using System.Collections.Generic;
#endregion


namespace Affinion.LoyaltyBuild.SupplierSetup.PollingManager
{
    #region Polling Manager UserDetails
    /// <summary>
    /// Polling Manager details
    /// </summary>
    public class UserDetails
    {
        /// <summary>
        /// Gets or sets Item ID
        /// </summary>
        public string itemID { get; set; }

        /// <summary>
        /// Gets or sets User Information
        /// </summary>
        public string userInfo { get; set; }
    }
    #endregion

    public class Line
    {
        public List<UserDetails> PollingManager_UserDetails { get; set; }
    }

    public class RootObject
    {
        public List<Line> Lines { get; set; }
    }
}
