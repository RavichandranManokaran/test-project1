﻿CREATE PROCEDURE [dbo].[LB_AddGuestChildAgeDetails]
	
	@RoomReservationId as int,
	@OrderLineId as int,
	@Age as int

AS
BEGIN
-- Delete all records for particular orderline id
--DELETE FROM [dbo].[LB_RoomReservationAgeInfo] WHERE [OrderLineId] = @OrderLineId

	INSERT INTO [dbo].[LB_RoomReservationAgeInfo]
           (
			[RoomReservationId], [OrderLineId], [Age])
     VALUES
           (@RoomReservationId, @OrderLineId, @Age)
	END