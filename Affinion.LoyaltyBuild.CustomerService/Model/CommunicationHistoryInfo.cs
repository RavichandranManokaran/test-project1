﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    /// <summary>
    /// Entity to hold communication related information
    /// </summary>
    public class CommunicationHistoryInfo
    {
        /// <summary>
        /// Communication history ID
        /// </summary>
        public int HistoryID { get; set; }

        /// <summary>
        /// Communication type of the communication history
        /// </summary>
        public int CommunicationTypeID { get; set; }

        /// <summary>
        /// Communication type name
        /// </summary>
        public String CommunicationType { get; set; }

        /// <summary>
        /// Communication history ID
        /// </summary>
        public int OrderlineId { get; set; }
       

        /// <summary>
        /// Created date
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Email plain text
        /// </summary>
        public String EmailPlainText { get; set; }

        /// <summary>
        /// Email html Text
        /// </summary>
        public String EmailHtmlText { get; set; }

        /// <summary>
        /// Email Subject
        /// </summary>
        public String Subject { get; set; }

        /// <summary>
        /// From Address
        /// </summary>
        public String From { get; set; }

        /// <summary>
        /// To Address
        /// </summary>
        public String To { get; set; }

        
    }
}
