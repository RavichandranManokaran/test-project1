﻿namespace Affinion.LoyaltyBuild.Web.Tests.Layouts.SubLayouts.ClientSetup.Wizard
{
    using Affinion.LoyaltyBuild.Common.Wizard;
    using System;

    public partial class Wizard2 : System.Web.UI.UserControl
    {
        private void Page_Load(object sender, EventArgs e)
        {
            DataTransfer dataTransfer = DataTransfer.Instance;

            // fetch data from DictionaryData and load           
            try
            {
                txtTwitter.Text = dataTransfer.DictionaryData["twitter"];
            }
            catch (Exception)
            {
                txtTwitter.Text = "Key Not found!";
            }

        }
    }
}