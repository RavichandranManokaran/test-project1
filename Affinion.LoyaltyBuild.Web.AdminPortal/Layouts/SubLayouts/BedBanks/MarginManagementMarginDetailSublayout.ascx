﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MarginManagementMarginDetailSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.BedBanks.MarginManagementMarginDetail" %>
<div class="col-sm-10 no-padding bedbank-rightpanel">
						<div class="row no-margin ">
							<div class="col-sm-12">
								<h3><sc:text id="title" field="Title Text" runat="server"/></h3>
							</div>   
						</div>
						<div class="row no-margin">
							<div class="col-sm-12 marB15">
								<div class="col-md-10 no-padding"></div>
								<div class="col-md-2 no-padding">
									<button type="button" runat="server" id="backBtn" onserverclick="backBtn_ServerClick" class="btn btn-default font-size-12 pull-right marL" ><sc:text id="back" field="Back Button Text" runat="server"/></button> 
									<%--<select class="form-control font-size-12 pull-right Wtd60">
										<option>Is Active</option>
										<option>Is Inactive</option>
										<option>No Filter</option>
									</select>--%>
                                    <asp:DropDownList ID="ddlFilters" runat="server" CssClass="form-control font-size-12 pull-right Wtd60" OnSelectedIndexChanged="ddlFilters_SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem Selected Text="Is Active" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Is InActive" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="No Filter" Value="-1"></asp:ListItem>
                                    </asp:DropDownList>
								</div>
							</div>
						</div>
						<div class="row no-margin">
							<div class="col-sm-12 marB15">   
								<div class="table-responsive table-bordered">
									<%--<table class="table table-striped">
										<thead>
											<tr>
												<th>Date From</th>
												<th>Date To</th>
												<th>Margin</th> 
												<th>Is Active</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td><span>Default Margin‎</span></td>
												<td><span>Default Margin</span></td>
												<td><span>10,0000</span></td>
												<td><span>Yes</span></td>
												<td><span><a href="#">Edit</a></span></td>
											</tr>
											<tr>
												<td class="RedL"><span>23/06/2016</span></td>
												<td class="RedL"><span>30/06/2016</span></td>
												<td class="RedL"><span>5,0000</span></td>
												<td class="RedL"><span>Yes</span></td>
												<td><span><a href="#">Edit</a></span></td>
											</tr>																						
										</tbody>
									</table>--%>
                                    <asp:GridView ID="gvMarginData" runat="server" CssClass="table table-striped" EmptyDataText="No Records Found" AutoGenerateEditButton="false" OnRowDataBound="gvMarginData_RowDataBound" AutoGenerateColumns="false" OnRowCommand="gvMarginData_RowCommand" >
                                         <Columns>
                <asp:BoundField DataField="DateFrom" HeaderText="Date From" />
                <asp:BoundField DataField="DateTo" HeaderText="Date To" />
                <asp:BoundField DataField="Margin" HeaderText="Margin" />
                <asp:TemplateField HeaderText="Is Active">
                    <ItemTemplate>
                       <asp:literal ID="litIsActive" runat="server" Text='<%# (bool)(Eval("IsActive"))? "Yes":"No" %>'></asp:literal>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Actions">
                    <ItemTemplate>
                        <asp:Button ID="btnEdit" runat="server" CommandName="editMargin" CssClass="editButton" CommandArgument='<%# Eval("ID") %>' Text="Edit" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
                                    </asp:GridView>
								</div>	
							</div>
							<div class="col-sm-12 marB15"> 
								<button type="button" id="addMarginBtn" runat="server" onserverclick="addMarginBtn_ServerClick" class="btn btn-default font-size-12 pull-right marL"><sc:text id="addMargin" field="Add New Margin Text" runat="server"/></button> 
							</div>
							<div class="col-sm-12 marB15"> 
								<div class="alert-info row no-margin pad10">
									<div class="col-md-12 no-padding">
										<h5><sc:text id="legend" field="Legend Text" runat="server"/></h5>
									</div>
									<div class="col-md-6 no-padding">
										<p>
										<sc:text id="baseTxt" field="Base Margin Text" runat="server"/>: <sc:text id="baseClr" field="Base Margin Color" runat="server"/> [Background]<br>
										<sc:text id="presentTxt" field="Present Margin Text" runat="server"/>: <sc:text id="presentClr" field="Present Margin Color" runat="server"/> [Background]<br>
										<sc:text id="isactiveTxt" field="Is Active Margin Text" runat="server"/>: <sc:text id="isactiveClr" field="Is Active Margin Color" runat="server"/> [Foreground]
										</p>
									</div>
									<div class="col-md-6 no-padding">
										<p>
										<sc:text id="expiredTxt" field="Expired Margin Text" runat="server"/>: <sc:text id="expiredClr" field="Expired Margin Color" runat="server"/> [Background]<br>
										<sc:text id="futureTxt" field="Future Margin Text" runat="server"/>: <sc:text id="futureClr" field="Future Margin Color" runat="server"/> [Background]<br>
										<sc:text id="inactiveTxt" field="In Active Margin Text" runat="server"/>: <sc:text id="inactiveClr" field="In Active Margin Color" runat="server"/>  [Foreground]
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				