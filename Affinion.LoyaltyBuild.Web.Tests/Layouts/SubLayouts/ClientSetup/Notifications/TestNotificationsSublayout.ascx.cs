﻿#region Using Directives
using System;
using Sitecore.Data.Items;
using Affinion.LoyaltyBuild.Common.Notifications;
#endregion

namespace Affinion.LoyaltyBuild.Web.Tests.Layouts.SubLayouts.ClientSetup.Notifications
{
    using System;

    public partial class TestNotificationsSublayout : System.Web.UI.UserControl
    {
        private void Page_Load(object sender, EventArgs e)
        {
            this.GetNotificationDetails();

        }


        private void GetNotificationDetails()
        {
            Item item = Sitecore.Context.Database.SelectSingleItem("fast:/sitecore/content/Home/Notifications/TestNotification");

            Notification timedNotification = new Notification(item);
            Label1.Text = timedNotification.DisplayNotificationTitle();


        }
    }
}