﻿using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Rules.Model;
using Sitecore.Security.Accounts;
using UCommerce.EntitiesV2;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;

/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Rules
{
    /// <summary>
    /// Interface to get paymentrule
    /// </summary>
   public interface ISitecorePaymentService
    {
        /// <summary>
        /// Gets the Payment Rule Information 
        /// </summary>
        /// <param name="Clientid">Clientid</param>
        /// <returns>Object of PaymentRuleModel list</returns>
       
         List<PaymentRule> GetPaymentRule(string Clientid);
       
        /// <summary>
        /// updates Paymentmethod
        /// </summary>
        /// <param name="Clientid">clientId</param>
        ///<param name="orderId">orderId</param>
        ///<param name="portal">portal</param>
        ///<param name="paymentType">paymentType</param>
        /// <returns>Object of update Paymentmethod list</returns>
         void UpdatePaymentMethod(string clientId, int orderId, string portal, string paymentType = null, string orderPaymentMethod = null);

         /// <summary>
         /// get rule Paymentmethod
         /// </summary>
         /// <param name="Clientid">clientId</param>
         ///<param name="orderId">orderId</param>
         ///<param name="portal">portal</param>
         ///<param name="paymentType">paymentType</param>
         /// <returns>Object of update Paymentmethod list</returns>
         List<OrderLinePaymentmethod> GetRulePaymentMethod(string clientId, int orderId, string portal, string paymentType = null, string orderPaymentMethod = null);

        /// <summary>
        /// Gets the list of orderline paymentmethod
        /// </summary>
        ///<param name="portal">orderId</param>
        /// <returns>Object of OrderLinePaymentmethod list</returns>
         List<OrderLinePaymentmethod> GetPaymentMethod(int orderId);
   }
}
