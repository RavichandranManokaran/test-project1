﻿using Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration.SiteUI.Base;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Links;
using System;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Single_Item
{
    public partial class Promo_with_Thumbnail : SitecoreSingleItemUserControl
    {
        private void Page_Load(object sender, EventArgs e)
        {
            WriteAlertsIfNeeded();
            HideIfNoVersionUnlessPageEditing();

            if (IsDataSourceItemNull)
            {
                pnl.Visible = false;
            }
            else
            {
                if (DataSourceItem["Link To"] != String.Empty && Sitecore.Context.Database.GetItem(new ID(DataSourceItem["Link To"])) != null)
                {
                    LinkTo.NavigateUrl = LinkManager.GetItemUrl(Sitecore.Context.Database.GetItem(new ID(DataSourceItem["Link To"])));
                }
                else
                {
                    LinkTo.Visible = false;
                }
            }
        }
    }
}