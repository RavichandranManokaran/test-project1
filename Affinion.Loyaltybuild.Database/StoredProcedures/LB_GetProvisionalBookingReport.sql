

--[dbo].[LB_GetProvisionalBookings] '3/1/2016','3/31/2016','LoyaltyBuild',null,'ArrivalDate'
CREATE PROCEDURE [dbo].[LB_GetProvisionalBookings]
(
	@datefrom Date=Null,
	@dateto Date=Null,	
	@partner nvarchar(50)=Null,
	@partialpaymentprovisional int = Null, 
	@reportandorderby nvarchar(50)= Null
)
AS 
BEGIN
	--SET NOCOUNT ON
    IF ISNULL(@ReportandOrderby,'') =''
    BEGIN
       SELECT @ReportandOrderby = (ISNULL(@ReportandOrderby,'ArrivalDate'))
    END
	      
	IF ISNULL(@partialpaymentprovisional,'') <> ''
    BEGIN
       SELECT @datefrom = CONVERT(DATE,GETDATE())
	   SELECT @dateto = CONVERT(DATE,DATEADD(DAY,@partialpaymentprovisional,GETDATE()))
    END

	SELECT Distinct RR.OrderReference [BookingReference],
	C.Name [ProviderName],			
	RR.CreatedDate [ReservationDate],
	RR.CheckIndate [ArrivalDate],
	CONCAT(Cus.FirstName,'',CUS.LastName)[CustomerName],
	Cus.MobilePhoneNumber [MobileNumber],
	Cus.PhoneNumber [PhoneNumber],
	OS.Name [BookingStatus],			
	Cus.EmailAddress [Emailid]
	FROM  [dbo].uCommerce_OrderLine OL
	INNER JOIN [dbo].LB_OrderAddress OA ON OA.OrderId=OL.OrderId
	INNER JOIN [dbo].uCommerce_PurchaseOrder PO ON OL.OrderId = PO.OrderId
	INNER JOIN [dbo].LB_RoomReservation RR ON OL.OrderLineId = RR.OrderLineId 
	INNER JOIN [dbo].uCommerce_Product P ON OL.Sku = P.Sku
	INNER JOIN [dbo].uCommerce_CategoryProductRelation CPR ON P.ProductId = CPR.ProductId 
	INNER JOIN [dbo].uCommerce_Category C ON CPR.CategoryId = C.CategoryId
	INNER JOIN [dbo].uCommerce_Customer CUS ON PO.CustomerId = CUS.CustomerId 
	INNER JOIN [dbo].LB_CustomerAttribute CA ON PO.CustomerId = CA.CustomerId 
	INNER JOIN [dbo].uCommerce_OrderLineStatus OLS ON RR.OrderLineStatusID = OLS.OrderLineStatusId
	INNER JOIN [dbo].LB_OrderStatus OS ON OLS.StatusId = OS.OrderStatusId
	INNER JOIN [dbo].uCommerce_OrderProperty OP ON OL.OrderLineId = OP.OrderLineId                 
	WHERE 	
	case when @reportandorderby='ArrivalDate' then CONVERT(date,RR.CheckInDate) else CONVERT(date,RR.CreatedDate) end between @datefrom and @dateto
	AND (OS.Name='Provisional' or OS.Name='Partial' AND OS.OrderStatusId=5)	
END


GO

