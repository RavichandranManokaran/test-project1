﻿CREATE TABLE [dbo].[LB_PromotionAuditTrail](
	[PromotionAuditTrailId] [int] IDENTITY(1,1) NOT NULL,
	[Record] [int] NOT NULL,
	[EntityName] [varchar](100) NULL,
	[Message] [varchar](500) NULL,
	[AuditedBy] [varchar](50) NOT NULL,
	[AuditedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_LB_PromotionAuditTrail] PRIMARY KEY CLUSTERED 
(
	[PromotionAuditTrailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

