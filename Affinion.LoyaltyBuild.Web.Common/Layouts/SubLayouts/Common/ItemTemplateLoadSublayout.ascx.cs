﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           RulesSampleSublayout.aspx.cs
/// Sub-system/Module:      Affinion.LoyaltyBuild.Web.Common(VS project name)
/// Description:           Sublayout for retrive fields of item
/// </summary>
namespace Affinion.LoyaltyBuild.Web.Common.Layouts.SubLayouts.Common
{
    #region Using directives
    using Affinion.LoyaltyBuild.Common.Utilities;
    using System;
    using System.Collections;
    using System.Web.UI.WebControls; 
    #endregion

    public partial class ItemTemplateLoadSublayout : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Put user code to initialize the page here
            //var listTemplateFieldItem = GetFieldsCollection.GetItemFieldWithInSection();
            //TemplateTable.DataSource = listTemplateFieldItem;
            //TemplateTable.DataBind();
            //GroupGridView(TemplateTable.Rows, 0, 3);

        }
        
        /// <summary>
        /// GroupGridView method for grouping  the gridview by template section value
        /// </summary>
        /// <param name="gridViewRows"> collection of gridview records</param>
        /// <param name="startIndex">start grid view column number</param>
        /// <param name="total number of columns that want to display from gridview"></param>
        //void GroupGridView(GridViewRowCollection gridViewRows, int startIndex, int total)
        //{
        //    if (total == 0) return;
        //    int i, count = 1;
        //    ArrayList lst = new ArrayList();
        //    lst.Add(gridViewRows[0]);
        //    var ctrl = gridViewRows[0].Cells[startIndex];
        //    for (i = 1; i < gridViewRows.Count; i++)
        //    {
        //        TableCell nextCell = gridViewRows[i].Cells[startIndex];
        //        if (ctrl.Text == nextCell.Text)
        //        {
        //            count++;
        //            nextCell.Visible = false;
        //            lst.Add(gridViewRows[i]);
        //        }
        //        else
        //        {
        //            if (count > 1)
        //            {
        //                ctrl.RowSpan = count;
        //                GroupGridView(new GridViewRowCollection(lst), startIndex + 1, total - 1);
        //            }
        //            count = 1;
        //            lst.Clear();
        //            ctrl = gridViewRows[i].Cells[startIndex];
        //            lst.Add(gridViewRows[i]);
        //        }
        //    }
        //    if (count > 1)
        //    {
        //        ctrl.RowSpan = count;
        //        GroupGridView(new GridViewRowCollection(lst), startIndex + 1, total - 1);
        //    }
        //    count = 1;
        //    lst.Clear();
        //}
    }
}