﻿using Affinion.LoyaltyBuild.Model.Pricing;
using Affinion.LoyaltyBuild.UCom.Common.Constants;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.Availability.SupplierPortal.Controls;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Availability;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.EntitiesV2;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Handler
{
    /// <summary>
    /// Summary description for Packages
    /// </summary>
    public class Packages : IHttpHandler
    {
        /// <summary>
        /// Process Request
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context)
        {
            string roomSKu = context.Request.Params["sku"];
            string[] skus = roomSKu.Split('@');

            string selectedDate = context.Request.Params["selecteddate"];
            DateTime packageDate = DateTime.Now;
            if (!string.IsNullOrWhiteSpace(selectedDate))
            {
                packageDate = DateTime.ParseExact(selectedDate, "dd/MM/yyyy", null);
            }
            context.Response.ContentType = "text/plain";
            
            AvailabilityHelper provider = new AvailabilityHelper(context);
            if (skus != null && skus.Count() > 0)
            {
                string sku = skus[0];
                string variantSku = skus[1];
                try
                {
                    //write html response
                    context.Response.Write(LoadUserControl(packageDate, provider, sku, variantSku));
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

        }

        /// <summary>
        /// Load User Control
        /// </summary>
        /// <param name="packageDate"></param>
        /// <param name="provider"></param>
        /// <param name="sku"></param>
        /// <param name="variantSku"></param>
        /// <returns></returns>
        protected static string LoadUserControl(DateTime packageDate, AvailabilityHelper provider, string sku, string variantSku)
        {
            using (Page page = new Page())
            {
                StringBuilder s = new StringBuilder();

                PlaceHolder f = new PlaceHolder();
                var relatedProducts = Product.All().Where(i => i.ProductProperties.Any(x => x.ProductDefinitionField.Name.Equals(ProductDefinationVairantConstant.RelatedSKU) && x.Value == variantSku)).ToList();
                List<ProductPrice> productPriceList = new List<ProductPrice>();

                if (relatedProducts != null)
                {
                    foreach (var item in relatedProducts)
                    {
                        string relatedSku = item.VariantSku;
                        if (!string.IsNullOrWhiteSpace(relatedSku))
                        {
                            var priceDetils = provider.GetProductPriceByDate(sku, relatedSku, packageDate);
                            productPriceList.Add(priceDetils);
                        }
                    }
                }
                if (productPriceList.Count > 0)
                {

                        using (StringWriter writer = new StringWriter(CultureInfo.CurrentCulture))
                        {
                            AvailabilityPackageView avp = (AvailabilityPackageView)page.LoadControl("~/Layouts/SubLayouts/SupplierPortal/Controls/Availability/AvailabilityPackageView.ascx");
                            avp.ProductPrice = productPriceList;
                            f.Controls.Add(avp);
                            page.Controls.Add(f);
                            HttpContext.Current.Server.Execute(page, writer, false);
                            s.Clear();
                            s.Append(writer.ToString());
                        }

                }
                return s.ToString();
            }
        }



        /// <summary>
        /// IsReusable flag
        /// </summary>
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

    }
}