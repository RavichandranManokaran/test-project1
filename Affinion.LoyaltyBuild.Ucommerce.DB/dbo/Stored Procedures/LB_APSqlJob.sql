CREATE PROCEDURE [dbo].[LB_APSqlJob]
AS
BEGIN
	INSERT INTO LB_SupplierPaymentRequests
		(OrderId,OrderLineId,SuggestedProviderAmount,IsProviderPaymentRequested,dateProviderPaymentRequested)
		SELECT DISTINCT OL.OrderId,OL.OrderLineId,SUM(OLD.ProviderAmount),1,GETDATE()
		FROM LB_BookingInfo BI
		INNER JOIN uCommerce_OrderLine OL ON BI.OrderLineId=OL.OrderLineId
		INNER JOIN LB_OrderLineDue OLD ON BI.OrderLineId=OLD.OrderLineId AND BI.BookingId=OLD.BookingId
		INNER JOIN uCommerce_OrderProperty OP ON OL.OrderLineId=OP.OrderLineId AND OP.OrderLineId IS NOT NULL AND OP.OrderId=OL.OrderId
	WHERE
		BI.StatusId=6  AND
		OP.Value='Full' AND BI.BookingNo IS NOT NULL AND
		DATEDIFF(mm, BI.CheckOutDate,GETDATE())<=6 AND
		(SELECT count(*) FROM LB_SupplierPaymentRequests PR WHERE PR.OrderId=OL.OrderId AND PR.OrderLineId=OL.OrderLineId)=0 
	GROUP BY
		OL.OrderLineId,
		OL.OrderId
	 
END



