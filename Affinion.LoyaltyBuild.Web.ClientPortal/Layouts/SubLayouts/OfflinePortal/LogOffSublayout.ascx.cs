﻿//<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           LogoutSublayout.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal
/// Description:           Used to Bind data to LogoutSublayout subLayout
/// </summary>

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    using Affinion.Loyaltybuild.Security;
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.Web.ClientPortal.Helper;
    using Sitecore.Security.Authentication;
    using Sitecore.Web;
    using System;

    public partial class LogoutSublayout : BaseSublayout
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SitecoreUserProfileHelper.ClearCustomProperty();
            SitecoreAccess.LogOff();
        }
    }
}