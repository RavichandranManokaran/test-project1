﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.Api;
using UCommerce.Runtime;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.Layouts.Sublayouts
{
    public partial class ProductListing : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var category = SiteContext.Current.CatalogContext.CurrentCategory;
            var products = CatalogLibrary.GetProducts(category);

            ProductRepeater.Products = products;
        }
    }
}