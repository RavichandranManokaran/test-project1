﻿using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.DataAccess.Helpers;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.CategoryImport;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Affinion.LoyaltyBuild.UCom.Common.Extension;
using Sitecore.Workflows;

namespace Affinion.LoyaltyBuild.AdminPortal.UCommSync
{
    public class ItemSavedHandler
    {
        public void OnItemSaved(object sender, EventArgs args)
        {
            // Extract the item from the event Arguments
            try
            {
                Item savedItem = Event.ExtractParameter(args, 0) as Item;
                if (savedItem.Database.Name == "master")
                {
                    Database masterDatabase = Sitecore.Configuration.Factory.GetDatabase("master");
                    IWorkflow workflow = masterDatabase.WorkflowProvider.GetWorkflow(savedItem);
                    if (workflow != null)
                    {
                        WorkflowState state = workflow.GetState(savedItem);
                        if (state != null && state.FinalState)
                        {
                            ISiteCoreHotel dataImport = new SiteCoreHotel();

                            // Allow only non null items and allow only items from the master database
                            if (savedItem != null && savedItem.Name != "__Standard Values")
                            {

                                if (savedItem.TemplateName.Equals("Country", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    CountryDetail detail = new CountryDetail();
                                    if (savedItem.Fields["CountryName"] != null)
                                    {
                                        detail.CountryName = savedItem.Fields["CountryName"].Value;
                                    }
                                    if (savedItem.Fields["Culture"] != null)
                                    {
                                        detail.CultureCode = savedItem.Fields["Culture"].Value;
                                    }
                                    int countryId = dataImport.SaveSiteCountry(detail);
                                    int storeCountryId = 0;
                                    int.TryParse(savedItem.Fields["UCommerceCountryID"].Value, out storeCountryId);
                                    if (storeCountryId != countryId)
                                    {

                                        savedItem.Editing.BeginEdit();
                                        savedItem.Fields["UCommerceCountryID"].Value = countryId.ToString();
                                        savedItem.Editing.EndEdit();
                                    }
                                }
                                else if (savedItem.TemplateName.Equals("PriceBand", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    decimal currentPrice = 0;
                                    //if (existingItem != null && existingItem.Fields["Rate"] != null)
                                    //{
                                    //    decimal.TryParse(existingItem.Fields["Rate"].Value, out oldPrice);
                                    //}
                                    if (savedItem.Fields["Rate"] != null)
                                    {
                                        decimal.TryParse(savedItem.Fields["Rate"].Value, out currentPrice);
                                    }

                                    SearchResultHelper.UpdatePrice(DateTime.Now.ConvetDatetoDateCounter(), false, currentPrice, 0, savedItem.ID.Guid, string.Empty);

                                    //Rate
                                }
                                else if (savedItem.TemplateName.Equals("SupplierDetails", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    LBHotelDetail detail = new LBHotelDetail();
                                    detail.HotelPath = savedItem.Paths.FullPath;
                                    detail.SiteCoreItemId = savedItem.ID.Guid.ToString();
                                    detail.HotelName = savedItem.DisplayName;


                                    detail.HotelPath = savedItem.Paths.FullPath;
                                    //if (savedItem.Fields["Clients"] != null)
                                    //{
                                    //    detail.Client = GetMultiListData(savedItem, "Clients", "Name");
                                    //}
                                    if (savedItem.Fields["MapLocation"] != null)
                                    {
                                        detail.Region = GetDropListData(savedItem, "MapLocation", "Name");
                                    }

                                    if (savedItem.Fields["Country"] != null)//droplink
                                    {
                                        detail.Country = GetDropListData(savedItem, "Country", "CountryName");
                                    }

                                    if (savedItem.Fields["Group"] != null)//droplink
                                    {
                                        detail.Groups = GetDropListData(savedItem, "Group", "Name"); //savedItem.Fields[""].Value;
                                    }

                                    if (savedItem.Fields["Experiences"] != null)// multilist
                                    {
                                        detail.HotelExperience = GetMultiListData(savedItem, "Experiences", "Name");
                                    }

                                    if (savedItem.Fields["Facilities"] != null)// multilist
                                    {
                                        detail.HotelFacility = GetMultiListData(savedItem, "Facilities", "Name");
                                    }

                                    if (savedItem.Fields["StarRanking"] != null) //droplink
                                    {
                                        detail.HotelRating = GetDropListData(savedItem, "StarRanking", "Name");
                                    }

                                    if (savedItem.Fields["SupplierType"] != null)//droplink
                                    {
                                        detail.HotelType = GetDropListData(savedItem, "SupplierType", "Name");
                                    }

                                    if (savedItem.Fields["Location"] != null)//droplink
                                    {
                                        detail.Location = GetDropListData(savedItem, "Location", "Name");
                                    }
                                    if (savedItem.Fields["GeoCoordinates"] != null)//droplink
                                    {
                                        detail.GeoCoOrdinate = savedItem.Fields["GeoCoordinates"].Value;
                                        if (detail.GeoCoOrdinate != null)
                                        {
                                            string[] lat = detail.GeoCoOrdinate.Split(',');
                                            if (lat.Length == 2)
                                            {
                                                detail.Latitude = lat[0];
                                                detail.Longitude = lat[1];
                                            }
                                        }
                                    }
                                    //
                                    if (savedItem.Fields["SuperGroup"] != null)//droplink
                                    {
                                        detail.SuperGroups = GetDropListData(savedItem, "SuperGroup", "Name");
                                    }

                                    if (savedItem.Fields["Themes"] != null) // multilist
                                    {
                                        detail.Theme = GetMultiListData(savedItem, "Themes", "Name");
                                    }
                                    if (savedItem.Fields["SKU"] != null) // multilist
                                    {
                                        detail.SKU = savedItem.Fields["SKU"].Value;
                                    }
                                    int searchRestriction = 0;
                                    if (savedItem.Fields["OneNightWeekEndRestriction"] != null)
                                    {
                                        int.TryParse(savedItem.Fields["OneNightWeekEndRestriction"].Value, out searchRestriction);
                                        detail.WeekendSerchRestriction = (searchRestriction == 1 ? true : false);
                                    }
                                    if (savedItem.Fields["Clients"] != null) // multilist
                                    {
                                        string catalogId = GetMultiListData(savedItem, "Clients", "ProductCatalogID");
                                        int id = 0;
                                        detail.CatalogId = catalogId.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries).Where(x => int.TryParse(x, out id)).Select(x => Convert.ToInt32(x)).ToList();
                                    }

                                    if (savedItem.Fields["UcommerceID"] != null) // multilist
                                    {
                                        int ucomId;
                                        if (int.TryParse(savedItem.Fields["UcommerceID"].Value, out ucomId))
                                        {
                                            detail.UComId = ucomId;
                                        }
                                    }
                                    detail.ConvertToLower();
                                    if (!string.IsNullOrEmpty(detail.HotelName) && detail.HotelName != "$name" && !string.IsNullOrEmpty(detail.Country)
                                        && !string.IsNullOrEmpty(detail.Location))
                                    {
                                        LBProductInfo productDetail = dataImport.SaveSiteCoreHotel(detail);
                                        int savedUcomId = 0;
                                        int.TryParse(savedItem.Fields["UcommerceID"].Value, out savedUcomId);
                                        if (productDetail.ProductId != savedUcomId)
                                        {
                                            using (new Sitecore.SecurityModel.SecurityDisabler())
                                            {
                                                savedItem.Editing.BeginEdit();
                                                savedItem.Fields["UcommerceID"].Value = productDetail.ProductId.ToString();
                                                savedItem.Fields["SKU"].Value = productDetail.SKU.ToString();
                                                savedItem.Editing.EndEdit();
                                            }
                                        }
                                    }

                                    try
                                    {
                                        var path = savedItem.Paths.Path;
                                        List<string> newPath = new List<string>();
                                        //string.Empty;

                                        foreach (var item in path.Split('/'))
                                        {
                                            if (item.Contains("-"))
                                            {
                                                newPath.Add(string.Format("#{0}#", item));
                                            }
                                            else
                                            {
                                                newPath.Add(item);
                                            }
                                        }

                                        var query = String.Format("fast:{0}//*[@@templatename='{1}' or @@templatename='{2}' or @@templatename='{3}']",
                                                    string.Join("/", newPath), "Supplier Room", "Supplier Addon", "Supplier Package");
                                        var items = masterDatabase.SelectItems(query);
                                        foreach (var item in items)
                                        {
                                            WorkflowState itemState = workflow.GetState(item);
                                            if (itemState != null && itemState.FinalState)
                                            {
                                                string sku = item.Fields["SKU"] != null ? item.Fields["SKU"].Value : string.Empty;
                                                if (string.IsNullOrEmpty(sku))
                                                {
                                                    SaveSitecoreHotelProduct(item);
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception er)
                                    {
                                        Diagnostics.WriteException(DiagnosticsCategory.SyncData, er, null);
                                    }
                                    //int storeCountryId = 0;
                                    //int.TryParse(savedItem.Fields["UCommerceCountryID"].Value, out storeCountryId);
                                    //if (storeCountryId != countryId)
                                    //{

                                    //    savedItem.Editing.BeginEdit();
                                    //    savedItem.Fields["UCommerceCountryID"].Value = countryId.ToString();
                                    //    savedItem.Editing.EndEdit();
                                    //}
                                }
                                else if (savedItem.TemplateName.Equals("Supplier Room", StringComparison.InvariantCultureIgnoreCase) ||
                                    savedItem.TemplateName.Equals("Supplier Addon", StringComparison.InvariantCultureIgnoreCase) ||
                                    savedItem.TemplateName.Equals("Supplier Package", StringComparison.InvariantCultureIgnoreCase)
                                    )
                                {
                                    SaveSitecoreHotelProduct(savedItem);
                                }


                            }
                        }
                    }
                }
            }
            catch (Exception er)
            {
                Diagnostics.WriteException(DiagnosticsCategory.SyncData, er, null);
            }
        }

        public void SaveSitecoreHotelProduct(Item savedItem)
        {
            ISiteCoreHotel dataImport = new SiteCoreHotel();
            Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
            LBProductDetail detail = new LBProductDetail();
            if (savedItem.Fields["Title"] != null)
            {
                detail.Name = savedItem.Fields["Title"].Value;
            }
            Item parentHotelItem = savedItem.Axes.SelectSingleItem("./ancestor-or-self::*[@@templatename= 'SupplierDetails']");
            if (parentHotelItem != null)
            {
                if (parentHotelItem.Fields["SKU"] != null)
                {
                    detail.ParentSKU = parentHotelItem.Fields["SKU"].Value;
                }
            }
            if (string.IsNullOrEmpty(detail.ParentSKU))
            {
                return;
            }
            if (savedItem.TemplateName.Equals("Supplier Room", StringComparison.InvariantCultureIgnoreCase) && savedItem.Fields["RoomType"] != null)
            {
                LookupField lookupField = savedItem.Fields["RoomType"];
                if (lookupField.TargetItem != null)
                {
                    Item sitecoreRoom = lookupField.TargetItem;
                    if (sitecoreRoom.Fields["Name"] != null)
                    {
                        detail.RoomType = sitecoreRoom.Fields["Name"].Value;
                    }
                    int value = 0;
                    if (sitecoreRoom.Fields["MaxNumberOfAdults"] != null && int.TryParse(sitecoreRoom.Fields["MaxNumberOfAdults"].Value, out value))
                    {
                        detail.MaxNumberOfAdults = value;
                    }
                    if (sitecoreRoom.Fields["MaxNumberOfChildren"] != null && int.TryParse(sitecoreRoom.Fields["MaxNumberOfChildren"].Value, out value))
                    {
                        detail.MaxNumberOfChildren = value;
                    }
                    if (sitecoreRoom.Fields["MaxNumberOfPeople"] != null && int.TryParse(sitecoreRoom.Fields["MaxNumberOfPeople"].Value, out value))
                    {
                        detail.MaxNumberOfPeople = value;
                    }
                    if (sitecoreRoom.Fields["MaxNumberOfInfants"] != null && int.TryParse(sitecoreRoom.Fields["MaxNumberOfInfants"].Value, out value))
                    {
                        detail.MaxNumberOfInfrant = value;
                    }
                    if (sitecoreRoom.Fields["Ranking"] != null)
                    {
                        int ranking = 0;
                        if (int.TryParse(sitecoreRoom.Fields["Ranking"].Value, out ranking))
                        {
                            detail.Ranking = ranking;
                        }
                    }
                }
            }

            else if (savedItem.TemplateName.Equals("Supplier Addon", StringComparison.InvariantCultureIgnoreCase) && savedItem.Fields["AddonType"] != null && !string.IsNullOrEmpty(savedItem.Fields["AddonType"].Value))//droplink
            {
                decimal packagePrice = 0;
                decimal price = 0;
                if (savedItem.Fields["SKU"] != null && !string.IsNullOrEmpty(savedItem.Fields["SKU"].Value))
                {
                    string SKU = savedItem.Fields["SKU"].Value.ToString();
                    if (savedItem.Fields["PackagePrice"] != null)
                    {
                        decimal.TryParse(savedItem.Fields["PackagePrice"].Value, out packagePrice);
                    }
                    if (savedItem.Fields["Price"] != null)
                    {
                        decimal.TryParse(savedItem.Fields["Price"].Value, out packagePrice);
                    }
                    SearchResultHelper.UpdatePrice(DateTime.Now.ConvetDatetoDateCounter(), true, price, packagePrice, default(Guid), SKU);
                }
                LookupField lookupField = savedItem.Fields["AddonType"];
                if (lookupField.TargetItem != null)
                {
                    Item sitecoreRoom = lookupField.TargetItem;
                    if (sitecoreRoom.Fields["Name"] != null)
                    {
                        detail.FoodType = sitecoreRoom.Fields["Name"].Value;
                    }
                }
            }
            else if (savedItem.TemplateName.Equals("Supplier Package", StringComparison.InvariantCultureIgnoreCase))//droplink
            {
                LookupField lookupField = savedItem.Fields["PackageType"];
                if (lookupField.TargetItem != null)
                {
                    Item sitecoreRoom = lookupField.TargetItem;
                    if (sitecoreRoom.Fields["Title"] != null)
                    {
                        detail.PackageType = sitecoreRoom.Fields["Title"].Value;
                    }
                }

                LookupField supplierlookupField = savedItem.Fields["SupplierRoom"];
                if (supplierlookupField.TargetItem != null)
                {
                    Item sitecoreHotelRoom = supplierlookupField.TargetItem;
                    if (sitecoreHotelRoom.Fields["SKU"] != null)
                    {
                        detail.RelatedSku = sitecoreHotelRoom.Fields["SKU"].Value;
                    }
                    if (sitecoreHotelRoom.Fields["RoomType"] != null)
                    {
                        LookupField mstRoomItem = sitecoreHotelRoom.Fields["RoomType"];
                        if (mstRoomItem.TargetItem != null)
                        {
                            int value = 0;
                            Item sitecoreRoom = mstRoomItem.TargetItem;
                            if (sitecoreRoom.Fields["Name"] != null)
                            {
                                detail.RoomType = sitecoreRoom.Fields["Name"].Value;
                            }
                            if (sitecoreRoom.Fields["MaxNumberOfAdults"] != null && int.TryParse(sitecoreRoom.Fields["MaxNumberOfAdults"].Value, out value))
                            {
                                detail.MaxNumberOfAdults = value;
                            }
                            if (sitecoreRoom.Fields["MaxNumberOfChildren"] != null && int.TryParse(sitecoreRoom.Fields["MaxNumberOfChildren"].Value, out value))
                            {
                                detail.MaxNumberOfChildren = value;
                            }
                            if (sitecoreRoom.Fields["MaxNumberOfPeople"] != null && int.TryParse(sitecoreRoom.Fields["MaxNumberOfPeople"].Value, out value))
                            {
                                detail.MaxNumberOfPeople = value;
                            }
                            if (sitecoreRoom.Fields["MaxNumberOfInfants"] != null && int.TryParse(sitecoreRoom.Fields["MaxNumberOfInfants"].Value, out value))
                            {
                                detail.MaxNumberOfInfrant = value;
                            }
                            if (sitecoreRoom.Fields["Ranking"] != null)
                            {
                                int ranking = 0;
                                if (int.TryParse(sitecoreRoom.Fields["Ranking"].Value, out ranking))
                                {
                                    detail.Ranking = ranking;
                                }
                            }
                        }
                    }
                }

                Sitecore.Data.Fields.MultilistField multilistField = savedItem.Fields["Addon"];
                if (multilistField != null)
                {
                    List<string> packageSku = new List<string>();
                    List<string> foodType = new List<string>();
                    foreach (ID id in multilistField.TargetIDs)
                    {
                        Item targetItem = workingdb.Items[id];
                        if (targetItem != null)
                        {
                            if (targetItem.Fields["SKU"] != null)
                            {
                                packageSku.Add(targetItem.Fields["SKU"].Value);
                            }

                            LookupField addOnlookupField = targetItem.Fields["AddonType"];
                            if (addOnlookupField != null && addOnlookupField.TargetItem != null)
                            {
                                Item sitecoreAddon = addOnlookupField.TargetItem;
                                if (sitecoreAddon.Fields["Name"] != null)
                                {
                                    foodType.Add(sitecoreAddon.Fields["Name"].Value);
                                }
                            }
                        }
                        //data.Add(id.Guid.ToString());
                    }
                    if (packageSku.Any())
                    {
                        detail.RelatedAddOnSku = string.Join(",", packageSku);
                    }
                    if (foodType.Any())
                    {
                        detail.FoodType = string.Join(",", foodType);
                    }
                }
            }


            if (savedItem.Fields["Desciription"] != null)
            {
                detail.Description = savedItem.Fields["Desciription"].Value;
            }

            if (savedItem.Fields["SKU"] != null)
            {
                detail.SKU = savedItem.Fields["SKU"].Value;
            }

            if (savedItem.Fields["IsDeleted"] != null && !string.IsNullOrEmpty(savedItem.Fields["IsDeleted"].Value))//droplink
            {
                detail.IsDeleted = savedItem.Fields["IsDeleted"].Value == "1" ? true : false;
            }
            if (savedItem.Fields["IsActive"] != null && !string.IsNullOrEmpty(savedItem.Fields["IsActive"].Value))//droplink
            {
                detail.IsActive = savedItem.Fields["IsActive"].Value == "1" ? true : false;
            }


            if (savedItem.Fields["IsUnlimited"] != null && !string.IsNullOrEmpty(savedItem.Fields["IsUnlimited"].Value))//droplink
            {
                detail.IsUnlimited = savedItem.Fields["IsUnlimited"].Value == "1" ? true : false;
            }
            if (savedItem.Fields["IsSellableStandAlone"] != null && !string.IsNullOrEmpty(savedItem.Fields["IsSellableStandAlone"].Value))//droplink
            {
                detail.IsSellableStaandalone = savedItem.Fields["IsSellableStandAlone"].Value == "1" ? true : false;
            }



            if (savedItem.Fields["UcommerceID"] != null && !string.IsNullOrEmpty(savedItem.Fields["UcommerceID"].Value))//droplink
            {
                detail.UcommerceId = Convert.ToInt32(savedItem.Fields["UcommerceID"].Value);
            }
            detail.ConvertToLower();
            LBProductInfo productInfo = dataImport.SaveProducVariant(detail);
            if (productInfo.ProductId > 0)
            {
                if (detail.UcommerceId != productInfo.ProductId)
                {
                    using (new Sitecore.SecurityModel.SecurityDisabler())
                    {
                        savedItem.Editing.BeginEdit();
                        savedItem.Fields["SKU"].Value = productInfo.VSKU.ToString();
                        savedItem.Fields["UcommerceID"].Value = productInfo.ProductId.ToString();
                        savedItem.Editing.EndEdit();
                    }
                }
            }
        }

        public void OnItemSaving(object sender, EventArgs args)
        {
            try
            {
                Item savedItem = Event.ExtractParameter(args, 0) as Item;
                if (savedItem.Database.Name == "master")
                {
                    Database masterDatabase = Sitecore.Configuration.Factory.GetDatabase("master");
                    IWorkflow workflow = masterDatabase.WorkflowProvider.GetWorkflow(savedItem);
                    if (workflow != null)
                    {
                        WorkflowState state = workflow.GetState(savedItem);
                        if (state != null && state.FinalState)
                        {
                            Sitecore.Events.SitecoreEventArgs eventArgs = args as Sitecore.Events.SitecoreEventArgs;
                            Sitecore.Data.Items.Item updatedItem = eventArgs.Parameters[0] as Sitecore.Data.Items.Item;
                            Sitecore.Data.Items.Item existingItem = null;
                            if (updatedItem.Version.Number > 1)
                            {
                                existingItem = updatedItem.Database.GetItem(
                                updatedItem.ID,
                                updatedItem.Language,
                                Sitecore.Data.Version.Parse(updatedItem.Version.Number - 1));
                            }
                            if (updatedItem != null)
                            {

                                ISiteCoreHotel dataImport = new SiteCoreHotel();
                                LBAssignSupplier assignSupplier = new LBAssignSupplier();
                                if (updatedItem.TemplateName.Equals("ClientDetails", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
                                    //var supplierTypeItems = workingdb.SelectItems(@"fast:/sitecore/content/#admin-portal#/#supplier-setup#//*[@@templatename='SupplierDetails' and {0} ]");
                                    List<string> newCategory = GetMultiListDataIds(updatedItem, "AllowedSuppliers"); ;
                                    List<string> oldCategory = GetMultiListDataIds(existingItem, "AllowedSuppliers"); ;
                                    int productCatalogId = 0;
                                    if (updatedItem.Fields["ProductCatalogID"] != null)
                                    {
                                        int.TryParse(updatedItem.Fields["ProductCatalogID"].Value, out productCatalogId);
                                        if (productCatalogId != 0)
                                        {
                                            assignSupplier.CatalogId = productCatalogId;
                                        }
                                        else
                                        {
                                            int id = dataImport.CreateCataLog(updatedItem.Name);
                                            if (id > 0)
                                            {
                                                using (new Sitecore.SecurityModel.SecurityDisabler())
                                                {
                                                    updatedItem.Editing.BeginEdit();
                                                    updatedItem.Fields["ProductCatalogID"].Value = id.ToString();
                                                    updatedItem.Editing.EndEdit();
                                                    assignSupplier.CatalogId = id;
                                                }
                                            }
                                        }

                                    }
                                    //ProductCatalogID
                                    List<string> addedHotel = newCategory.Where(x => !oldCategory.Any(y => y == x)).ToList();
                                    List<string> removeHotel = oldCategory.Where(x => !newCategory.Any(y => y == x)).ToList();
                                    string content = string.Empty;
                                    for (int i = 0; i < addedHotel.Count; i++)
                                    {
                                        content += string.Format("@@id='{0}' {1}", addedHotel[i], (i == addedHotel.Count - 1) ? "" : "or");
                                    }
                                    List<int> addhotel = new List<int>();
                                    List<int> removehotel = new List<int>();
                                    if (!string.IsNullOrEmpty(content))
                                    {
                                        var addedsupplierItems = workingdb.SelectItems(string.Format(@"fast:/sitecore/content/#admin-portal#/#supplier-setup#//*[@@templatename='SupplierDetails' and {0} ]", content));
                                        if (addedsupplierItems.Length > 0)
                                        {
                                            foreach (var item in addedsupplierItems)
                                            {
                                                int id = 0;
                                                if (item.Fields["UcommerceID"] != null)
                                                {
                                                    if (int.TryParse(item.Fields["UcommerceID"].Value, out id))
                                                    {
                                                        if (id > 0)
                                                            addhotel.Add(id);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    content = string.Empty;
                                    for (int i = 0; i < removeHotel.Count; i++)
                                    {
                                        content += string.Format("@@id='{0}' {1}", removeHotel[i], (i == removeHotel.Count - 1) ? "" : "or");
                                    }
                                    if (!string.IsNullOrEmpty(content))
                                    {
                                        var removesupplierItems = workingdb.SelectItems(string.Format(@"fast:/sitecore/content/#admin-portal#/#supplier-setup#//*[@@templatename='SupplierDetails' and {0} ]", content));
                                        if (removesupplierItems.Length > 0)
                                        {
                                            foreach (var item in removesupplierItems)
                                            {
                                                int id = 0;
                                                if (item.Fields["UcommerceID"] != null)
                                                {
                                                    if (int.TryParse(item.Fields["UcommerceID"].Value, out id))
                                                    {
                                                        if (id > 0)
                                                            removehotel.Add(id);
                                                    }
                                                }
                                            }
                                        }

                                    }

                                    assignSupplier.AddedHotel = addhotel;
                                    assignSupplier.RemoveHotel = removehotel;
                                    assignSupplier.ClientName = updatedItem.Name;
                                    if (assignSupplier.AddedHotel.Any() || assignSupplier.RemoveHotel.Any())
                                    {
                                        try
                                        {
                                            dataImport.UpdateSupplierClientDetail(assignSupplier);
                                        }
                                        catch (Exception er)
                                        {
                                            Diagnostics.WriteException(DiagnosticsCategory.SyncData, er, null);
                                        }
                                    }

                                }

                            }
                        }
                    }
                }
            }
            catch (Exception er)
            { }
        }

        private string GetDropListData(Item selectedItem, string dropLinkFieldName, string dropLinkFieldProperty)
        {
            string selectedValue = string.Empty;
            try
            {
                if (selectedItem == null || string.IsNullOrEmpty(dropLinkFieldName))
                {
                    return null;
                }

                LookupField lookupField = selectedItem.Fields[dropLinkFieldName];

                if (lookupField == null)
                {
                    return null;
                }
                if (lookupField.TargetItem != null && lookupField.TargetItem.Fields[dropLinkFieldProperty] != null)
                {
                    selectedValue = lookupField.TargetItem.Fields[dropLinkFieldProperty].Value;
                }

            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
            }
            return selectedValue;
        }

        private List<string> GetMultiListDataIds(Item selectedItem, string multiListFieldName)
        {
            List<string> data = new List<string>();
            Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
            Sitecore.Data.Fields.MultilistField multilistField = selectedItem.Fields[multiListFieldName];
            if (multilistField != null)
            {
                foreach (ID id in multilistField.TargetIDs)
                {
                    data.Add(id.Guid.ToString());
                }
            }
            return data;
        }
        private string GetMultiListData(Item selectedItem, string multiListFieldName, string multiListProperty)
        {
            string selectedValue = string.Empty;
            List<string> data = new List<string>();
            if (selectedItem == null)
                return string.Join("|", data);
            Sitecore.Data.Database workingdb = Sitecore.Configuration.Factory.GetDatabase(Constants.CurrentSitecoreDatabase);
            Sitecore.Data.Fields.MultilistField multilistField = selectedItem.Fields[multiListFieldName];
            if (multilistField != null)
            {
                foreach (ID id in multilistField.TargetIDs)
                {
                    Item targetItem = workingdb.Items[id];
                    if (targetItem != null)
                    {
                        if (targetItem.Fields[multiListProperty] != null)
                        {
                            data.Add(targetItem.Fields[multiListProperty].Value);
                        }
                    }
                }
            }
            selectedValue = string.Join("|", data);
            return selectedValue;
        }
    }
}