CREATE PROCEDURE [dbo].[LB_ARSqlJob]
AS
BEGIN

/* Step1
The following SQL will write a vat element on Commission of Break Free and Break Free transactions occuring on or before first
confirmation date for all irish providers. Vat is at 23%
*/
INSERT INTO LB_Vat_Payments
(
	OrderId,OrderLineId,OrderDate,SupplierId,CampaignId,ClientId,
	OrderCurrencyId,OrderCurrencyIsoCode,CustomerId,GrossAmount,SuggestedVatAmount,
	NetAmount,CreatedBy,DateCreated
)
SELECT DISTINCT 

	OL.OrderId,OL.OrderLineId,GETDATE(),P.ProductId,CI.CampaignId,
	CP.ClientId,CU.CurrencyId,CU.ISOCode,C.CustomerId,SUM(OLD.CommissionAmount) [GrossAmount],
	((SUM(OLD.CommissionAmount) /123) * SUM(ISNULL(OLD.VatAmount,0))) [SuggestedVatAmount],(SUM(OLD.CommissionAmount) - ((SUM(OLD.CommissionAmount) /123) * SUM(ISNULL(OLD.VatAmount,0)))) [NetAmount],
	'Admin' [CreatedBy],GETDATE() [DateCreated]

FROM LB_BookingInfo BI

	INNER JOIN uCommerce_OrderLine OL ON BI.OrderLineId=OL.OrderLineId
	INNER JOIN LB_OrderLineStatusAudit OLS ON OL.OrderLineId=OLS.OrderLineID AND OLS.StatusId=6
	INNER JOIN LB_OrderLineDue OLD ON BI.OrderLineId=OLD.OrderLineId AND BI.BookingId=OLD.BookingId
	INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId=PO.OrderId
    INNER JOIN Ucommerce_orderaddress Orderaddress ON PO.Orderid = Orderaddress.Orderid
    INNER JOIN Ucommerce_country Country ON Orderaddress.Countryid = Country.Countryid 
	INNER JOIN (
		   SELECT CustomerId,_ClientId [ClientId]
			FROM 
			(
			  SELECT CustomerId, [key], [value]
			  FROM LB_CustomerProperty 
			) src
			pivot
			(
			  MAX([value])
			  FOR [key] in (_ClientId)
			) piv
		) CP ON PO.CustomerId=CP.CustomerId 
	INNER JOIN uCommerce_Customer C ON CP.CustomerId=C.CustomerId
	INNER JOIN uCommerce_Currency CU ON PO.CurrencyId=CU.CurrencyId
	INNER JOIN Ucommerce_product P ON OL.Sku = P.Sku AND P.ParentProductId IS NULL AND P.VariantSku IS NULL
	INNER JOIN uCommerce_ProductDefinition PD ON P.ProductDefinitionId=PD.ProductDefinitionId AND PD.Name='Hotel'	
	LEFT JOIN uCommerce_OrderLineCampaignRelation OLCR ON OL.OrderLineId=OLCR.OrderLineId  
	LEFT JOIN ucommerce_campaignitem CI ON OLCR.CampaignItemId=CI.CampaignItemId	

WHERE
	 BI.BookingNo IS NOT NULL AND
	 OLS.CreatedOn IS NOT NULL AND
	 DATEDIFF(DD, OLS.CreatedOn,GETDATE())=1 AND
	 Country.CountryId=24 AND
	 (SELECT count(*) FROM LB_Vat_Payments VP WHERE VP.OrderId=OL.OrderId AND VP.OrderLineId=OL.OrderLineId)=0 

GROUP BY

	CI.CampaignId,
	CP.ClientId,
	CU.CurrencyId,
	CU.ISOCode,
	C.CustomerId,
	P.ProductId,
	OLD.CommissionAmount,
	OL.OrderLineId,
	OL.OrderId

HAVING 
	SUM(OLD.CommissionAmount) > 0


 

     


 /* Step2
The following SQL will write a vat element on Commission of Break Free and Break Free transactions 
occuring on or before first confirmation date for all non irish providers. Vat is at 0%                                   
*/
INSERT INTO LB_Vat_Payments
(
	OrderId,OrderLineId,OrderDate,SupplierId,CampaignId,ClientId,
	OrderCurrencyId,OrderCurrencyIsoCode,CustomerId,GrossAmount,SuggestedVatAmount,
	NetAmount,CreatedBy,DateCreated
)
SELECT DISTINCT 

	OL.OrderId,OL.OrderLineId,GETDATE(),P.ProductId,CI.CampaignId,
	CP.ClientId,CU.CurrencyId,CU.ISOCode,C.CustomerId,SUM(OLD.CommissionAmount) [GrossAmount],
	0 [SuggestedVatAmount],SUM(OLD.CommissionAmount) [NetAmount],
	'Admin' [CreatedBy],GETDATE() [DateCreated]

FROM LB_BookingInfo BI

	INNER JOIN uCommerce_OrderLine OL ON BI.OrderLineId=OL.OrderLineId
	INNER JOIN LB_OrderLineStatusAudit OLS ON OL.OrderLineId=OLS.OrderLineID AND OLS.StatusId=6
	INNER JOIN LB_OrderLineDue OLD ON BI.OrderLineId=OLD.OrderLineId AND BI.BookingId=OLD.BookingId
	INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId=PO.OrderId
    INNER JOIN Ucommerce_orderaddress Orderaddress ON PO.Orderid = Orderaddress.Orderid
    INNER JOIN Ucommerce_country Country ON Orderaddress.Countryid = Country.Countryid 
	INNER JOIN (
		   SELECT CustomerId,_ClientId [ClientId]
			FROM 
			(
			  SELECT CustomerId, [key], [value]
			  FROM LB_CustomerProperty 
			) src
			pivot
			(
			  MAX([value])
			  FOR [key] in (_ClientId)
			) piv
		) CP ON PO.CustomerId=CP.CustomerId 
	INNER JOIN uCommerce_Customer C ON CP.CustomerId=C.CustomerId
	INNER JOIN uCommerce_Currency CU ON PO.CurrencyId=CU.CurrencyId
	INNER JOIN Ucommerce_product P ON OL.Sku = P.Sku AND P.ParentProductId IS NULL AND P.VariantSku IS NULL
	INNER JOIN uCommerce_ProductDefinition PD ON P.ProductDefinitionId=PD.ProductDefinitionId AND PD.Name='Hotel'	
	LEFT JOIN uCommerce_OrderLineCampaignRelation OLCR ON OL.OrderLineId=OLCR.OrderLineId  
	LEFT JOIN ucommerce_campaignitem CI ON OLCR.CampaignItemId=CI.CampaignItemId	

WHERE
	 BI.BookingNo IS NOT NULL AND
	 OLS.CreatedOn IS NOT NULL AND
	 DATEDIFF(DD, OLS.CreatedOn,GETDATE())=1 AND
	 Country.CountryId!=24 AND
	 (SELECT count(*) FROM LB_Vat_Payments VP WHERE VP.OrderId=OL.OrderId AND VP.OrderLineId=OL.OrderLineId)=0 

GROUP BY

	CI.CampaignId,
	CP.ClientId,
	CU.CurrencyId,
	CU.ISOCode,
	C.CustomerId,
	P.ProductId,
	OLD.CommissionAmount,
	OL.OrderLineId,
	OL.OrderId

HAVING 
	SUM(OLD.CommissionAmount) > 0



/* Step3
The following SQL will write a vat element on all commission of the break fee or break fee
transactions occuring after the first confirmation date for all irish providers. The vat element 
will be recorded at 23%
*/
INSERT INTO LB_Vat_Payments
(
	OrderId,OrderLineId,OrderDate,SupplierId,CampaignId,ClientId,
	OrderCurrencyId,OrderCurrencyIsoCode,CustomerId,GrossAmount,SuggestedVatAmount,
	NetAmount,CreatedBy,DateCreated
)
SELECT DISTINCT 

	OL.OrderId,OL.OrderLineId,GETDATE(),P.ProductId,CI.CampaignId,
	CP.ClientId,CU.CurrencyId,CU.ISOCode,C.CustomerId,OLD.CommissionAmount [GrossAmount],
	((SUM(OLD.CommissionAmount) /123) * SUM(ISNULL(OLD.VatAmount,0))) [SuggestedVatAmount],(SUM(OLD.CommissionAmount) - ((SUM(OLD.CommissionAmount) /123) * SUM(ISNULL(OLD.VatAmount,0)))) [NetAmount],
	'Admin' [CreatedBy],GETDATE() [DateCreated]

FROM LB_BookingInfo BI

	INNER JOIN uCommerce_OrderLine OL ON BI.OrderLineId=OL.OrderLineId
	INNER JOIN LB_OrderLineStatusAudit OLS ON OL.OrderLineId=OLS.OrderLineID AND OLS.StatusId=6
	INNER JOIN LB_OrderLineDue OLD ON BI.OrderLineId=OLD.OrderLineId AND BI.BookingId=OLD.BookingId
	INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId=PO.OrderId
    INNER JOIN Ucommerce_orderaddress Orderaddress ON PO.Orderid = Orderaddress.Orderid
    INNER JOIN Ucommerce_country Country ON Orderaddress.Countryid = Country.Countryid 
	INNER JOIN (
		   SELECT CustomerId,_ClientId [ClientId]
			FROM 
			(
			  SELECT CustomerId, [key], [value]
			  FROM LB_CustomerProperty 
			) src
			pivot
			(
			  MAX([value])
			  FOR [key] in (_ClientId)
			) piv
		) CP ON PO.CustomerId=CP.CustomerId 
	INNER JOIN uCommerce_Customer C ON CP.CustomerId=C.CustomerId
	INNER JOIN uCommerce_Currency CU ON PO.CurrencyId=CU.CurrencyId
	INNER JOIN Ucommerce_product P ON OL.Sku = P.Sku AND P.ParentProductId IS NULL AND P.VariantSku IS NULL
	INNER JOIN uCommerce_ProductDefinition PD ON P.ProductDefinitionId=PD.ProductDefinitionId AND PD.Name='Hotel'	
	LEFT JOIN uCommerce_OrderLineCampaignRelation OLCR ON OL.OrderLineId=OLCR.OrderLineId  
	LEFT JOIN ucommerce_campaignitem CI ON OLCR.CampaignItemId=CI.CampaignItemId	

WHERE
	 BI.BookingNo IS NOT NULL AND
	 OLS.CreatedOn IS NOT NULL AND
	 DATEDIFF(DD, OLS.CreatedOn,GETDATE())=1 AND
	 DATEDIFF(DD, OLS.CreatedOn,OLD.CreatedDate) > 1 AND
	 Country.CountryId=24 AND
	 (SELECT count(*) FROM LB_Vat_Payments VP WHERE VP.OrderId=OL.OrderId AND VP.OrderLineId=OL.OrderLineId)=0 

GROUP BY

	CI.CampaignId,
	CP.ClientId,
	CU.CurrencyId,
	CU.ISOCode,
	C.CustomerId,
	P.ProductId,
	OLD.CommissionAmount,
	OL.OrderLineId,
	OL.OrderId
	
HAVING 
	SUM(OLD.CommissionAmount) > 0


/* Step4
The following SQL will write a vat element on all commision of the break fee and break fee 
transactions occuring after the first confirmation date for all non irish providers. 
The vat element will be recorded at 0% 
*/
INSERT INTO LB_Vat_Payments
(
	OrderId,OrderLineId,OrderDate,SupplierId,CampaignId,ClientId,
	OrderCurrencyId,OrderCurrencyIsoCode,CustomerId,GrossAmount,SuggestedVatAmount,
	NetAmount,CreatedBy,DateCreated
)
SELECT DISTINCT 

	OL.OrderId,OL.OrderLineId,GETDATE(),P.ProductId,CI.CampaignId,
	CP.ClientId,CU.CurrencyId,CU.ISOCode,C.CustomerId,SUM(OLD.CommissionAmount) [GrossAmount],
	0 [SuggestedVatAmount],SUM(OLD.CommissionAmount) [NetAmount],
	'Admin' [CreatedBy],GETDATE() [DateCreated]

FROM LB_BookingInfo BI

	INNER JOIN uCommerce_OrderLine OL ON BI.OrderLineId=OL.OrderLineId
	INNER JOIN LB_OrderLineStatusAudit OLS ON OL.OrderLineId=OLS.OrderLineID AND OLS.StatusId=6
	INNER JOIN LB_OrderLineDue OLD ON BI.OrderLineId=OLD.OrderLineId AND BI.BookingId=OLD.BookingId
	INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId=PO.OrderId
    INNER JOIN Ucommerce_orderaddress Orderaddress ON PO.Orderid = Orderaddress.Orderid
    INNER JOIN Ucommerce_country Country ON Orderaddress.Countryid = Country.Countryid 
	INNER JOIN (
		   SELECT CustomerId,_ClientId [ClientId]
			FROM 
			(
			  SELECT CustomerId, [key], [value]
			  FROM LB_CustomerProperty 
			) src
			pivot
			(
			  MAX([value])
			  FOR [key] in (_ClientId)
			) piv
		) CP ON PO.CustomerId=CP.CustomerId 
	INNER JOIN uCommerce_Customer C ON CP.CustomerId=C.CustomerId
	INNER JOIN uCommerce_Currency CU ON PO.CurrencyId=CU.CurrencyId
	INNER JOIN Ucommerce_product P ON OL.Sku = P.Sku AND P.ParentProductId IS NULL AND P.VariantSku IS NULL
	INNER JOIN uCommerce_ProductDefinition PD ON P.ProductDefinitionId=PD.ProductDefinitionId AND PD.Name='Hotel'	
	LEFT JOIN uCommerce_OrderLineCampaignRelation OLCR ON OL.OrderLineId=OLCR.OrderLineId  
	LEFT JOIN ucommerce_campaignitem CI ON OLCR.CampaignItemId=CI.CampaignItemId	

WHERE
	 BI.BookingNo IS NOT NULL AND
	 OLS.CreatedOn IS NOT NULL AND
	 DATEDIFF(DD, OLS.CreatedOn,GETDATE())=1 AND
	 DATEDIFF(DD, OLS.CreatedOn,OLD.CreatedDate) > 1 AND
	 Country.CountryId!=24 AND
	 (SELECT count(*) FROM LB_Vat_Payments VP WHERE VP.OrderId=OL.OrderId AND VP.OrderLineId=OL.OrderLineId)=0 

GROUP BY

	CI.CampaignId,
	CP.ClientId,
	CU.CurrencyId,
	CU.ISOCode,
	C.CustomerId,
	P.ProductId,
	OLD.CommissionAmount,
	OL.OrderLineId,
	OL.OrderId

HAVING 
	SUM(OLD.CommissionAmount) > 0



/* Step5
The following SQL will write a negative vat element on all cancellation & Deposit as 
Transfer Fee transaction fee ( written to transfer record provider)occuring after first 
confirmation date for all non irish providers.The vat element will be recorded at 0%*
*/
INSERT INTO LB_Vat_Payments
(
	OrderId,OrderLineId,OrderDate,SupplierId,CampaignId,ClientId,
	OrderCurrencyId,OrderCurrencyIsoCode,CustomerId,GrossAmount,SuggestedVatAmount,
	NetAmount,CreatedBy,DateCreated
)
SELECT DISTINCT 

	OL.OrderId,OL.OrderLineId,GETDATE(),P.ProductId,CI.CampaignId,
	CP.ClientId,CU.CurrencyId,CU.ISOCode,C.CustomerId,(SUM(OLD.CommissionAmount) *-1) [GrossAmount],
	0 [SuggestedVatAmount],(SUM(OLD.CommissionAmount)*-1) [NetAmount],
	'Admin' [CreatedBy],GETDATE() [DateCreated]

FROM LB_BookingInfo BI

	INNER JOIN uCommerce_OrderLine OL ON BI.OrderLineId=OL.OrderLineId
	INNER JOIN LB_OrderLineStatusAudit OLS ON OL.OrderLineId=OLS.OrderLineID AND OLS.StatusId=6
	INNER JOIN LB_OrderLineDue OLD ON BI.OrderLineId=OLD.OrderLineId AND BI.BookingId=OLD.BookingId
	INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId=PO.OrderId
    INNER JOIN Ucommerce_orderaddress Orderaddress ON PO.Orderid = Orderaddress.Orderid
    INNER JOIN Ucommerce_country Country ON Orderaddress.Countryid = Country.Countryid 
	INNER JOIN (
		   SELECT CustomerId,_ClientId [ClientId]
			FROM 
			(
			  SELECT CustomerId, [key], [value]
			  FROM LB_CustomerProperty 
			) src
			pivot
			(
			  MAX([value])
			  FOR [key] in (_ClientId)
			) piv
		) CP ON PO.CustomerId=CP.CustomerId 
	INNER JOIN uCommerce_Customer C ON CP.CustomerId=C.CustomerId
	INNER JOIN uCommerce_Currency CU ON PO.CurrencyId=CU.CurrencyId
	INNER JOIN Ucommerce_product P ON OL.Sku = P.Sku AND P.ParentProductId IS NULL AND P.VariantSku IS NULL
	INNER JOIN uCommerce_ProductDefinition PD ON P.ProductDefinitionId=PD.ProductDefinitionId AND PD.Name='Hotel'	
	LEFT JOIN uCommerce_OrderLineCampaignRelation OLCR ON OL.OrderLineId=OLCR.OrderLineId  
	LEFT JOIN ucommerce_campaignitem CI ON OLCR.CampaignItemId=CI.CampaignItemId	

WHERE
     BI.BookingNo IS NOT NULL AND
	 OLS.CreatedOn IS NOT NULL AND
	 DATEDIFF(DD, OLS.CreatedOn,GETDATE())=1 AND
	 DATEDIFF(DD, OLS.CreatedOn,OLD.CreatedDate) > 1 AND
	 Country.CountryId!=24 AND
	 (SELECT count(*) FROM LB_Vat_Payments VP WHERE VP.OrderId=OL.OrderId AND VP.OrderLineId=OL.OrderLineId)=0 

GROUP BY

	CI.CampaignId,
	CP.ClientId,
	CU.CurrencyId,
	CU.ISOCode,
	C.CustomerId,
	P.ProductId,
	OLD.CommissionAmount,
	OL.OrderLineId,
	OL.OrderId

HAVING 
	SUM(OLD.CommissionAmount) > 0


END