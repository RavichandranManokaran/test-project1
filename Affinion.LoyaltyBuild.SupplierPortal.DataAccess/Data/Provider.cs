﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.SupplierPortal.DataAccess.Data
{
    /// <summary>
    /// Contains provider (Supplier/Hotel) info 
    /// </summary>
    public class Provider
    {
        /// <summary>
        /// provider id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// provider name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// ucommerce guid
        /// </summary>
        public string UCommerceGuid { get; set; }

        /// <summary>
        /// provider id
        /// </summary>
        public string ProductSKU { get; set; }

        /// <summary>
        ///Sitecore Id
        /// </summary>
        public string SitecoreId { get; set; }
    }
}
