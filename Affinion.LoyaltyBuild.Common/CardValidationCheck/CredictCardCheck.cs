﻿using Affinion.LoyaltyBuild.Common.Instrumentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Common.CardValidationCheck
{
    public static class CredictCardCheck
    {

         /// <summary>
        /// CredictCard Check ServerSide
        /// </summary>
        /// <param name="textboxvalue"></param>
        /// <returns></returns>
        public static string CredictCard(string textboxvalue)
        {
            string result = string.Empty;
            
                string pattern = @"(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})";
                //string replacement = " ";
                Regex rgx = new Regex(pattern);
                result = rgx.Replace(textboxvalue, new string('*', 19));
                return result;
        }

    }
}
