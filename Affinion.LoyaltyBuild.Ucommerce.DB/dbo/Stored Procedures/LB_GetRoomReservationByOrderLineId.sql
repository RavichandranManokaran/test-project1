﻿CREATE PROCEDURE [dbo].[LB_GetRoomReservationByOrderLineId]
	(	@OrderLineId int	)
AS
BEGIN
  SELECT OP.NoOfGuest , OP.NoOfAdult, OP.NoOfChild, OP.GuestName, 
		 BI.OrderLineId,BI.CheckInDate,BI.CheckOutDate,
		 RN.Note [SpecialRequest]
  FROM LB_BookingInfo BI
  INNER JOIN (
			   Select OrderLineId, _GuestName [GuestName], _NoOfGuest [NoOfGuest], _NoOfAdult [NoOfAdult], _NoOfChildren [NoOfChild]
			   from
				(
					Select OrderLineId, [KEY], [Value]
					from uCommerce_OrderProperty
				) Src
				pivot
				(
					max([Value])
					for [Key] in (_GuestName, _NoOfGuest, _NoOfAdult, _NoOfChildren)
				) Piv
			) OP ON BI.OrderLineId=OP.OrderLineId
	INNER JOIN LB_ReservationNotes RN ON BI.OrderLineId=RN.OrderLineId
  
  WHERE BI.OrderLineId = @OrderLineId AND RN.NoteTypeId=3
END