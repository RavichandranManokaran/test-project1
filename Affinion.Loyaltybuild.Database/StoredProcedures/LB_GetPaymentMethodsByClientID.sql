﻿/****** Object:  StoredProcedure [dbo].[LB_GetPaymentMethodsByClientID]    Script Date: 2/12/2016 11:40:56 AM ******/
DROP PROCEDURE [dbo].[LB_GetPaymentMethodsByClientID]
GO

/****** Object:  StoredProcedure [dbo].[LB_GetPaymentMethodsByClientID]    Script Date: 2/12/2016 11:40:56 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[LB_GetPaymentMethodsByClientID]
	@ClientID varchar(500) 	
AS
BEGIN				
	if (@ClientID is not null)
	begin
		select paymentmethodid, description from uCommerce_ClientIDPaymentMethodMapping where ClientId = @ClientID and showinlist = 1
		order by description
	end	
END
