﻿using System.Data.SqlClient;
using System.Data;
using System;
using Affinion.LoyaltyBuild.BedBanks.Troika.Model;
using Affinion.LoyaltyBuild.BedBanks.General;
using System.Collections.Generic;
using System.Web;
using System.Configuration;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Message;
using System.Xml.Serialization;
using System.IO;
using System.Text;
using System.Linq;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Exceptions;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Model;

namespace Affinion.LoyaltyBuild.BedBanks.Troika
{
    public static class TroikaDataController
    {
        public static string GetbedBanksConnectionString()
        {
            try
            {
                return (ConfigurationManager.ConnectionStrings["Bedbank"].ConnectionString.Length != 0) ? ConfigurationManager.ConnectionStrings["Bedbank"].ConnectionString : string.Empty;
                //DataConnection dataConnection = new DataConnection("ClientPortal_uCommerce");
                //return dataConnection.ConnectionString;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        public static void InsertBedBankBookingInfo(string pBookingId, int AvailabilitySourceId, string pPropertyId, string pRoomType, string pMealBasis, int pExternalBookingRef, decimal pWholesaleAmount, int pMarginIdApplied, string lbBookReference, decimal marginAmt, decimal marginPercent)
        {
            var connStr = GetbedBanksConnectionString();

            using (SqlConnection myConn = new SqlConnection(connStr))
            {
                try
                {
                    myConn.Open();
                    SqlCommand cmd = new SqlCommand("LB_BB_insertBedBankBookingInfo", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pi_availabilityProviderID", 1));
                    cmd.Parameters.Add(new SqlParameter("@pi_availabilityproviderRecordID", int.Parse(pPropertyId)));
                    cmd.Parameters.Add(new SqlParameter("@pi_bookingId", int.Parse(pBookingId)));
                    cmd.Parameters.Add(new SqlParameter("@pi_roomType", pRoomType));
                    cmd.Parameters.Add(new SqlParameter("@pi_mealBasis", pMealBasis));
                    cmd.Parameters.Add(new SqlParameter("@pi_externalBookingReference", pExternalBookingRef));
                    cmd.Parameters.Add(new SqlParameter("@pi_WholeSaleAmount", pWholesaleAmount));
                    cmd.Parameters.Add(new SqlParameter("@pi_propertyMarginID", pMarginIdApplied));
                    cmd.Parameters.Add(new SqlParameter("@pi_lbBookingReference", lbBookReference));
                    cmd.Parameters.Add(new SqlParameter("@pi_marginAmount", marginAmt));
                    cmd.Parameters.Add(new SqlParameter("@pi_marginPercentage", marginPercent));
                    cmd.Parameters.Add(new SqlParameter("@po_returnstatus", SqlDbType.Int) { Direction = ParameterDirection.Output });
                    cmd.Parameters.Add(new SqlParameter("@po_returnmsg", SqlDbType.VarChar, 200) { Direction = ParameterDirection.Output });
                    cmd.Parameters.Add(new SqlParameter("@po_scopeid", SqlDbType.Int) { Direction = ParameterDirection.Output });
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException sqlEx)
                {
                    throw;
                }
                catch (Exception Ex)
                {
                    throw;
                }
            }
        }

        private static List<BedBankMargin> GetBedBankPropertyMargins()
        {
            List<BedBankMargin> bbMargins;

            // Check to see if the days margins are already are already in cache
            if (HttpRuntime.Cache["Margins"] != null)
            {
                bbMargins = (List<BedBankMargin>)HttpRuntime.Cache["Margins"];
            }
            else
            {
                //Create a new list object for the margins
                bbMargins = new List<BedBankMargin>();

                var connStr = GetbedBanksConnectionString();

                using (SqlConnection myConn = new SqlConnection(connStr))
                {
                    try
                    {
                        myConn.Open();
                        SqlCommand cmd = new SqlCommand("LB_BB_getBedBankPropertyMarginForCache", myConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@po_returnstatus", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new SqlParameter("@po_returnmsg", SqlDbType.VarChar, 200) { Direction = ParameterDirection.Output });

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Call Read before accessing data.
                            while (reader.Read())
                            {
                                BedBankMargin bbMargin = new BedBankMargin
                                {
                                    PropertyId = (int)reader[0],
                                    MarginId = (int)reader[3],
                                    MarginPercentage = (decimal)reader[4],
                                    DateFromCounter = (int)reader[5],
                                    DateToCounter = (int)reader[6]
                                };

                                bbMargins.Add(bbMargin);
                            }
                        }
                    }
                    catch (SqlException sqlEx)
                    {
                        throw;
                    }
                    catch (Exception Ex)
                    {
                        throw;
                    }

                    //Add the Margins to the Cache with a cache time of 2 hours
                    HttpRuntime.Cache.Insert("Margins", bbMargins, null, DateTime.Now.AddHours(2.0), System.Web.Caching.Cache.NoSlidingExpiration);
                }
            }

            return bbMargins;
        }

        public static List<JtPropertyDetailsResponse> CheckDbPropertyCache(string pPropertyIdList, string pAvailabilityProviderId)
        {
            var connStr = GetbedBanksConnectionString();

            List<JtPropertyDetailsResponse> facilityMappings = new List<JtPropertyDetailsResponse>();

            using (SqlConnection myConn = new SqlConnection(connStr))
            {
                try
                {
                    myConn.Open();
                    SqlCommand cmd = new SqlCommand("LB_BB_checkDbMultiPropertyCache", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pi_availabilityProviderId", pAvailabilityProviderId));
                    cmd.Parameters.Add(new SqlParameter("@pi_propertyIdList", pPropertyIdList));
                    cmd.Parameters.Add(new SqlParameter("@po_returnstatus", SqlDbType.Int) { Direction = ParameterDirection.Output });
                    cmd.Parameters.Add(new SqlParameter("@po_returnmsg", SqlDbType.VarChar, 200) { Direction = ParameterDirection.Output });

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        // Call Read before accessing data.
                        while (reader.Read())
                        {
                            int propertyID = (int)reader[0];
                            string contentResponseString = (string)reader[1];
                            string sitecoreItemId = (string)reader[2];

                            if (!contentResponseString.Equals(""))
                            {
                                //Serialize the result
                                XmlRootAttribute xRoot = new XmlRootAttribute
                                {
                                    ElementName = "PropertyDetailsResponse",
                                    IsNullable = true
                                };

                                XmlSerializer serializer = new XmlSerializer(typeof(JtPropertyDetailsResponse), xRoot);

                                JtPropertyDetailsResponse cachedContent = null;

                                try
                                {
                                    MemoryStream memStream = new MemoryStream(Encoding.UTF8.GetBytes(contentResponseString));
                                    cachedContent = (JtPropertyDetailsResponse)serializer.Deserialize(memStream);
                                    if (!string.IsNullOrEmpty(sitecoreItemId))
                                    {
                                        cachedContent.SitecoreHotelId = sitecoreItemId;
                                    }
                                }
                                catch (Exception exc)
                                {
                                    throw new Exception(exc.Message);
                                }

                                if (cachedContent != null)
                                {
                                    facilityMappings.Add(cachedContent);

                                    //Cache the list of mappings so we don't need the Database again
                                    HttpRuntime.Cache[propertyID.ToString()] = cachedContent;
                                }
                            }
                        }
                    }
                }
                catch (SqlException sqlEx)
                {
                    //Need to log something here
                    var str = sqlEx.Message;
                }


            }

            return facilityMappings;
        }


        //Check the Database for the property details response
        public static JtPropertyDetailsResponse CheckDbPropertyCacheOld(string pPropertyId, string pAvailabilityProviderId)
        {
            var connStr = GetbedBanksConnectionString();

            JtPropertyDetailsResponse cachedContent = null;

            using (SqlConnection myConn = new SqlConnection(connStr))
            {
                try
                {
                    SqlParameter outXmlResponse = new SqlParameter("@po_xmlResponse", SqlDbType.VarChar, -1) { Direction = ParameterDirection.Output };

                    myConn.Open();
                    SqlCommand cmd = new SqlCommand("LB_BB_checkDbPropertyCache", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pi_availabilityProviderId", pAvailabilityProviderId));
                    cmd.Parameters.Add(new SqlParameter("@pi_propertyId", pPropertyId));
                    cmd.Parameters.Add(new SqlParameter("@po_returnstatus", SqlDbType.Int) { Direction = ParameterDirection.Output });
                    cmd.Parameters.Add(new SqlParameter("@po_returnmsg", SqlDbType.VarChar, 200) { Direction = ParameterDirection.Output });
                    cmd.Parameters.Add(outXmlResponse);
                    cmd.ExecuteNonQuery();

                    string contentResponseString = (string)outXmlResponse.Value;

                    if (!contentResponseString.Equals(""))
                    {
                        //Serialize the result
                        XmlRootAttribute xRoot = new XmlRootAttribute
                        {
                            ElementName = "PropertyDetailsResponse",
                            IsNullable = true
                        };

                        XmlSerializer serializer = new XmlSerializer(typeof(JtPropertyDetailsResponse), xRoot);

                        try
                        {
                            MemoryStream memStream = new MemoryStream(Encoding.UTF8.GetBytes(contentResponseString));
                            cachedContent = (JtPropertyDetailsResponse)serializer.Deserialize(memStream);
                        }
                        catch (Exception exc)
                        {
                            throw new Exception(exc.Message);
                        }
                    }
                }
                catch (SqlException sqlEx)
                {
                    throw;
                }
                catch (Exception Ex)
                {
                    throw;
                }
            }

            return cachedContent;
        }


        //Insert a record to the Database for the property details response
        public static string InsertDbPropertyCache(string pAvailabilityProviderId, string pPropertyId, string pXmlResponse, string pPropertyName, string pLat, string pLong, string pAddressLine1, string pAddressLine2, string pAddressLine3, string pAddressLine4, string pAddressLine5, string pPhone, string pFax)
        {
            var connStr = GetbedBanksConnectionString();

            using (SqlConnection myConn = new SqlConnection(connStr))
            {
                try
                {
                    //Work out Expiry Time - Uses a random hour seed so all records don;t expire at the same time
                    //(+30 Days from Now + 0-24 hours)
                    string format = "yyyy-MM-dd HH:MM:ss";
                    Random rnd = new Random();
                    DateTime expiryTime = DateTime.Now.AddDays(30).AddHours(rnd.Next(25));
                    string uCommerceConnString = ConfigurationManager.ConnectionStrings["UCommerce"].ConnectionString.Length != 0 ? ConfigurationManager.ConnectionStrings["UCommerce"].ConnectionString : string.Empty;
                    System.Data.SqlClient.SqlConnectionStringBuilder builder = new System.Data.SqlClient.SqlConnectionStringBuilder();
                    builder.ConnectionString = uCommerceConnString;
                    string database = builder.InitialCatalog;
                    myConn.Open();
                    SqlCommand cmd = new SqlCommand("LB_BB_insertDBPropertyCache", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlParameter SitecoreId = new SqlParameter("@lbProviderId", SqlDbType.VarChar) { Direction = ParameterDirection.Output, Size = 200 };
                    cmd.Parameters.Add(new SqlParameter("@pi_availabilityProviderId", pAvailabilityProviderId));
                    cmd.Parameters.Add(new SqlParameter("@pi_propertyId", pPropertyId));
                    cmd.Parameters.Add(new SqlParameter("@pi_xmlResponse", pXmlResponse));
                    cmd.Parameters.Add(new SqlParameter("@pi_expiryTime", expiryTime.ToString(format)));
                    cmd.Parameters.Add(new SqlParameter("@pi_providerName", pPropertyName));
                    cmd.Parameters.Add(new SqlParameter("@pi_lat", pLat));
                    cmd.Parameters.Add(new SqlParameter("@pi_long", pLong));
                    cmd.Parameters.Add(new SqlParameter("@pi_addressline1", pAddressLine1));
                    cmd.Parameters.Add(new SqlParameter("@pi_addressline2", pAddressLine2));
                    cmd.Parameters.Add(new SqlParameter("@pi_addressline3", pAddressLine3));
                    cmd.Parameters.Add(new SqlParameter("@pi_addressline4", pAddressLine4));
                    cmd.Parameters.Add(new SqlParameter("@pi_addressline5", pAddressLine5));
                    cmd.Parameters.Add(new SqlParameter("@pi_phone", pPhone));
                    cmd.Parameters.Add(new SqlParameter("@pi_fax", pFax));
                    cmd.Parameters.Add(new SqlParameter("@pi_dbname", database));
                    cmd.Parameters.Add(new SqlParameter("@po_returnstatus", SqlDbType.Int) { Direction = ParameterDirection.Output });
                    cmd.Parameters.Add(new SqlParameter("@po_returnmsg", SqlDbType.VarChar, 200) { Direction = ParameterDirection.Output });
                    cmd.Parameters.Add(new SqlParameter("@po_scopeid", SqlDbType.Int) { Direction = ParameterDirection.Output });
                    cmd.Parameters.Add(SitecoreId);
                    cmd.ExecuteNonQuery();
                    return SitecoreId.Value.ToString();
                }
                catch (SqlException sqlEx)
                {
                    throw;
                }
                catch (Exception Ex)
                {
                    throw;
                }
            }
        }

        public static Tuple<decimal, int> CalculateMargin(string pPropertyId, int pArrivalDate, int pDepartureDate, int pAvailabilityProviderSourceId)
        {
            //DEFAULT THE MARGIN - WE SHOULD ALWAYS GET A VALUE AS THERES A DEFAULT RETURNED FROM SP
            //THIS IS ONLY IN THE CASE THAT THE SP FAILS
            decimal margin = 10.0M;
            int marginId = 34120;

            //Get the list of up to date margins
            List<BedBankMargin> bbMargins = GetBedBankPropertyMargins();

            //Query the list using Linq to get the most relevant margin
            BedBankMargin bbMarginResult = bbMargins.OrderBy(x => x.PropertyId).ThenByDescending(x => x.DateFromCounter).FirstOrDefault(x => x.PropertyId == int.Parse(pPropertyId) && x.DateFromCounter <= pArrivalDate && x.DateToCounter >= pDepartureDate);

            //Set the values - otherwise it will use the defaults
            if (bbMarginResult != null)
            {
                margin = bbMarginResult.MarginPercentage;
                marginId = bbMarginResult.MarginId;
            }

            //Return as a tuple
            return Tuple.Create(margin, marginId);
        }

        public static string InsertProviderRecord(int pPropertyId, string pPropertyName, string pLat, string pLong, string pAddressLine1, string pAddressLine2, string pAddressLine3, string pAddressLine4, string pAddressLine5, string pPhone, string pFax)
        {
            var connStr = GetbedBanksConnectionString();

            if (HttpRuntime.Cache['_' + pPropertyId.ToString()] == null)
            {
                using (SqlConnection myConn = new SqlConnection(connStr))
                {
                    try
                    {
                        SqlParameter outLbProviderId = new SqlParameter("@po_lbproviderid", SqlDbType.VarChar) { Direction = ParameterDirection.Output };

                        myConn.Open();
                        SqlCommand cmd = new SqlCommand("LB_BB_createBedbankProvider", myConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@pi_propertyid", pPropertyId));
                        cmd.Parameters.Add(new SqlParameter("@pi_availabilitySourceId", 1));
                        cmd.Parameters.Add(new SqlParameter("@pi_providerName", pPropertyName));
                        cmd.Parameters.Add(new SqlParameter("@pi_lat", pLat));
                        cmd.Parameters.Add(new SqlParameter("@pi_long", pLong));
                        cmd.Parameters.Add(new SqlParameter("@pi_addressline1", pAddressLine1));
                        cmd.Parameters.Add(new SqlParameter("@pi_addressline2", pAddressLine2));
                        cmd.Parameters.Add(new SqlParameter("@pi_addressline3", pAddressLine3));
                        cmd.Parameters.Add(new SqlParameter("@pi_addressline4", pAddressLine4));
                        cmd.Parameters.Add(new SqlParameter("@pi_addressline5", pAddressLine5));
                        cmd.Parameters.Add(new SqlParameter("@pi_phone", pPhone));
                        cmd.Parameters.Add(new SqlParameter("@pi_fax", pFax));
                        cmd.Parameters.Add(outLbProviderId);
                        cmd.Parameters.Add(new SqlParameter("@po_returnstatus", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new SqlParameter("@po_returnmsg", SqlDbType.VarChar, 200) { Direction = ParameterDirection.Output });
                        cmd.ExecuteNonQuery();
                        //myConn.Close();
                        HttpRuntime.Cache['_' + pPropertyId.ToString()] = (string)outLbProviderId.Value;
                    }
                    catch (SqlException sqlEx)
                    {
                        throw;
                    }
                    catch (Exception Ex)
                    {
                        throw;
                    }
                }
            }

            return (string)HttpRuntime.Cache['_' + pPropertyId.ToString()];
        }

        public static List<FacilityMapping> GetFacilityMappings(int AvailabilitySourceId)
        {
            var connStr = GetbedBanksConnectionString();

            List<FacilityMapping> facilityMappings;

            if (HttpRuntime.Cache["FacilityMappings"] == null)
            {
                facilityMappings = new List<FacilityMapping>();

                using (SqlConnection myConn = new SqlConnection(connStr))
                {
                    try
                    {
                        myConn.Open();
                        SqlCommand cmd = new SqlCommand("LB_BB_getFacilityMappings", myConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@pi_availabilityProviderID", AvailabilitySourceId));
                        cmd.Parameters.Add(new SqlParameter("@po_returnstatus", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        cmd.Parameters.Add(new SqlParameter("@po_returnmsg", SqlDbType.VarChar, 200) { Direction = ParameterDirection.Output });

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Call Read before accessing data.
                            while (reader.Read())
                            {
                                FacilityMapping fm = new FacilityMapping()
                                {
                                    FeatureID = (int)reader[0],
                                    FacilityID = (int)reader[1],
                                    FeatureName = (string)reader[2]
                                };

                                facilityMappings.Add(fm);
                            }
                        }
                    }
                    catch (SqlException sqlEx)
                    {
                        //Need to log something here
                        var str = sqlEx.Message;
                    }

                    //Cache the list of mappings so we don't need the Database again
                    HttpRuntime.Cache["FacilityMappings"] = facilityMappings;
                }
            }
            else
            {
                facilityMappings = (List<FacilityMapping>)HttpRuntime.Cache["FacilityMappings"];
            }

            return facilityMappings;
        }

        public static void InsertApiResponseTime(int pAvailabilityProviderId, string pMethod, double pResponseTime)
        {
            var connStr = GetbedBanksConnectionString();

            using (SqlConnection myConn = new SqlConnection(connStr))
            {
                try
                {
                    myConn.Open();
                    SqlCommand cmd = new SqlCommand("LB_BB_insertApiResponseTime", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pi_availabilityProviderID", pAvailabilityProviderId));
                    cmd.Parameters.Add(new SqlParameter("@pi_method", pMethod));
                    cmd.Parameters.Add(new SqlParameter("@pi_responseTime", pResponseTime));
                    cmd.Parameters.Add(new SqlParameter("@po_returnstatus", SqlDbType.Int) { Direction = ParameterDirection.Output });
                    cmd.Parameters.Add(new SqlParameter("@po_returnmsg", SqlDbType.VarChar, 200) { Direction = ParameterDirection.Output });
                    cmd.Parameters.Add(new SqlParameter("@po_scopeid", SqlDbType.Int) { Direction = ParameterDirection.Output });
                    cmd.ExecuteNonQuery();
                    myConn.Close();
                }
                catch (SqlException sqlEx)
                {
                    throw;
                }
                catch (Exception Ex)
                {
                    throw;
                }
            }
        }

        public static DataTable providerDetailForMargin(string country, string location, string maplocation, string providerName)
        {
            try
            {
                DataSet ds = new DataSet();

                if (GetbedBanksConnectionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetbedBanksConnectionString()))
                    {
                        string sql = "LB_BB_GetProviderDetails";

                        using (SqlCommand command = new SqlCommand(sql, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@country", SqlDbType.VarChar)).Value = country;
                            command.Parameters.Add(new SqlParameter("@location", SqlDbType.VarChar)).Value = location;
                            command.Parameters.Add(new SqlParameter("@mapLocation", SqlDbType.VarChar)).Value = maplocation;
                            command.Parameters.Add(new SqlParameter("@providerName", SqlDbType.VarChar)).Value = providerName;
                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                        }
                    }
                }

                return ds.Tables[0];
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        public static DataSet GetProviderMargin(string propertyId, string isactive)
        {
            try
            {
                DataSet ds = new DataSet();

                if (GetbedBanksConnectionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetbedBanksConnectionString()))
                    {
                        string sql = "LB_BB_GetProviderMargin";

                        using (SqlCommand command = new SqlCommand(sql, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@providerId", SqlDbType.Int)).Value = int.Parse(propertyId);
                            command.Parameters.Add(new SqlParameter("@isactive", SqlDbType.Int)).Value = int.Parse(isactive);
                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                        }
                    }
                }

                return ds;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        public static int AddProviderMargin(string providerId, string marginPercentage, string startDate, string endDate, string marginId = "0", string isglobal = "0", string isactive = "1", int ErrorListId=0)
        {
            var connStr = GetbedBanksConnectionString();

            using (SqlConnection myConn = new SqlConnection(connStr))
            {
                try
                {
                    myConn.Open();
                    SqlCommand cmd = new SqlCommand("LB_BB_AddMargin", myConn);
                    SqlParameter IsExist = new SqlParameter("@exists", SqlDbType.Int) { Direction = ParameterDirection.Output };
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@providerId", providerId));
                    cmd.Parameters.Add(new SqlParameter("@marginPercentage", marginPercentage));
                    cmd.Parameters.Add(new SqlParameter("@startDate", startDate));
                    cmd.Parameters.Add(new SqlParameter("@endDate", endDate));
                    cmd.Parameters.Add(new SqlParameter("@marginId", int.Parse(marginId)));
                    cmd.Parameters.Add(new SqlParameter("@isactive", int.Parse(isactive)));
                    if (ErrorListId > 0)
                    {
                        cmd.Parameters.Add(new SqlParameter("@ErrorListID", ErrorListId));
                    }
                    if ((int.Parse(isglobal)) > 0)
                    {
                        cmd.Parameters.Add(new SqlParameter("@isglobal", int.Parse(marginId)));
                    }
                    cmd.Parameters.Add(IsExist);
                    cmd.ExecuteNonQuery();
                    int value;
                    int.TryParse(IsExist.Value.ToString(), out value);
                    if (value != null)
                    {
                        return value;
                    }
                    else
                    {
                        return -1;
                    }
                }
                catch (SqlException sqlEx)
                {
                    throw;
                }
                catch (Exception Ex)
                {
                    throw;
                }
            }
        }

        public static DataTable GetMarginForUpdate(string marginId)
        {
            try
            {
                DataSet ds = new DataSet();
                int margin;
                int.TryParse(marginId, out margin);
                if (GetbedBanksConnectionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetbedBanksConnectionString()))
                    {
                        string sql = "LB_BB_GetMarginForEdit";

                        using (SqlCommand command = new SqlCommand(sql, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@marginId", SqlDbType.Int)).Value = margin;
                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                        }
                    }
                }

                return ds.Tables[0];
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        public static DataTable GetMarginForEAchBBHotel(string providerId, string arrivalDate)
        {
            try
            {
                DataSet ds = new DataSet();
                if (GetbedBanksConnectionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetbedBanksConnectionString()))
                    {
                        string sql = "LB_BB_getMarginForEachBBHotel";

                        using (SqlCommand command = new SqlCommand(sql, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@providerID", SqlDbType.VarChar)).Value = providerId;
                            command.Parameters.Add(new SqlParameter("@arrivalDate", SqlDbType.Date)).Value = arrivalDate;
                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                        }
                    }
                }

                return ds.Tables[0];
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }
        public static DataTable ReverseMapLBtoBB(string itemId)
        {
            try
            {
                DataSet ds = new DataSet();
                if (GetbedBanksConnectionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetbedBanksConnectionString()))
                    {
                        string sql = "LB_BB_ReverseMapLBtoBB";

                        using (SqlCommand command = new SqlCommand(sql, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@SitecoreItemId", SqlDbType.VarChar)).Value = itemId;
                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                        }
                    }
                }

                return ds.Tables[0];
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        public static DataTable SetMarginForMultipleProvider(string info, decimal marginPercentage, string startDate, string endDate)
        {
            try
            {
                DataSet ds = new DataSet();
                if (GetbedBanksConnectionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetbedBanksConnectionString()))
                    {
                        string sql = "LB_BB_UpdateMarginForAll";

                        using (SqlCommand command = new SqlCommand(sql, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@marginDetails", info));
                            command.Parameters.Add(new SqlParameter("@marginPercentage", marginPercentage));
                            command.Parameters.Add(new SqlParameter("@startDate", startDate));
                            command.Parameters.Add(new SqlParameter("@endDate", endDate));
                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                        }
                    }
                }

                return ds.Tables[0];
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        public static DataTable GetProviderDetailsFromErrorList(int listId)
        {
            try
            {
                DataSet ds = new DataSet();
                if (GetbedBanksConnectionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetbedBanksConnectionString()))
                    {
                        string sql = "LB_BB_GetProvidersInErrorList";

                        using (SqlCommand command = new SqlCommand(sql, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@listID", listId));
                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                        }
                    }
                }
                return ds.Tables[0];
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        public static DataTable GetMarginErrorList()
        {
            try
            {
                DataSet ds = new DataSet();
                if (GetbedBanksConnectionString().Length > 0)
                {
                    using (SqlConnection con = new SqlConnection(GetbedBanksConnectionString()))
                    {
                        string sql = "LB_BB_GetErrorList";

                        using (SqlCommand command = new SqlCommand(sql, con))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(command);
                            adapter.Fill(ds);
                        }
                    }
                }
                return ds.Tables[0];
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        public static void InsertErrataInfo(int propertyId,string propertyRoomTypeId,int orderLineId,string errata)
        {
            try
            {
                if(GetbedBanksConnectionString().Length>0)
                {
                    using (SqlConnection con=new SqlConnection(GetbedBanksConnectionString()))
                    {
                        string sql = "LB_BB_InsertErrataFeedForOrderLine";
                        using(SqlCommand cmd=new SqlCommand(sql,con))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            
                            cmd.Parameters.Add(new SqlParameter("@propertyId", propertyId));
                            cmd.Parameters.Add(new SqlParameter("@propertyRoomTypeId", propertyRoomTypeId));
                            cmd.Parameters.Add(new SqlParameter("@orderlineId", orderLineId));
                            cmd.Parameters.Add(new SqlParameter("@errata", errata));
                            con.Open();
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch(Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        public static string GetErrataInfo(int orderLineId)
        {
            try
            {
                if (GetbedBanksConnectionString().Length > 0)
                {
                    DataSet ds = new DataSet();
                    using (SqlConnection con = new SqlConnection(GetbedBanksConnectionString()))
                    {
                        string sql = "LB_BB_GetErrataInfo";
                        string errata = string.Empty;
                        using (SqlCommand cmd = new SqlCommand(sql, con))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;                            
                            cmd.Parameters.Add(new SqlParameter("@OrderlineId", orderLineId));
                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                            adapter.Fill(ds);
                            if (ds != null && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                            {
                                errata = ds.Tables[0].Rows[0][0].ToString();
                            }
                            adapter.Dispose();
                            return errata;
                        }
                    }                   
                }
                return string.Empty;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }


        public static DataTable GetBookingDetailsForCancellation(int orderLineId)
        {
            try
            {
                DataSet ds = new DataSet();
                if (GetbedBanksConnectionString().Length > 0)
                {
                    
                    using (SqlConnection con = new SqlConnection(GetbedBanksConnectionString()))
                    {
                        string sql = "LB_BB_GetBookingDetailsForCancellation";
                        using (SqlCommand cmd = new SqlCommand(sql, con))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@pi_lbOrderLineID", orderLineId));
                            con.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                            adapter.Fill(ds);
                        }
                    }
                }
                return ds.Tables[0];
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

    }
}
