﻿<%@ control language="C#" autoeventwireup="true" codebehind="OfflinePortalRefund.ascx.cs" inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalRefund" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
        <h4 class="panel-title"><sc:text id="Refund" field="RefundText" runat="server"/></h4>
    </div>
    <div class="panel-body alert-info">
        <input type="hidden" id="hdnRefundRefId" />
        <input type="hidden" id="hdnRefundableProviderAmount" />
        <input type="hidden" id="hdnRefundableCommissionAmount" />
        <input type="hidden" id="hdnRefundableProcessingFee" />
        <div class="col-sm-12 no-padding-m">
            <div class="content_bg_inner_box alert-info">
                <div class="col-sm-6 no-padding-m">
                    <div class="row margin-row">
                        <div class="col-sm-5"><span class="bask-form-titl font-size-12"><sc:text id="CurrencyText" field="CurrencyText" runat="server"/><span class="accent58-f"></span></span></div>
                        <div class="col-sm-7">
                            <asp:dropdownlist enableviewstate="false" clientidmode="static" id="ddlCurrency" runat="server" enabled="False" cssclass="form-control font-size-12" />
                        </div>
                    </div>
                    <div class="row margin-row">
                        <div class="col-sm-5"><span class="bask-form-titl font-size-12"><sc:text id="PaymentMethodText" field="PaymentMethod" runat="server"/><span class="accent58-f"> </span></span></div>
                        <div class="col-sm-7">
                            <asp:dropdownlist enableviewstate="false" clientidmode="static" id="ddlPaymentMethod" runat="server" cssclass="form-control font-size-12" />
                        </div>
                    </div>
                    <div class="row margin-row">
                        <div class="col-sm-5"><span class="bask-form-titl font-size-12"><sc:text id="TotalAmountText" field="TotalAmount" runat="server"/></span></div>
                        <div class="col-sm-7">
                            <asp:textbox id="txtAmount" enableviewstate="false" clientidmode="static" runat="server" enabled="false" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
<div class="col-sm-12">

    <div class="panel panel-default" id="refundbycard" style="display:none;">
        <div class="panel-heading">
            <h4 class="panel-title"><sc:text id="CreditCardRefundText" field="CreditCardRefund" runat="server"/></h4>
        </div>
        <div class="panel-body alert-info">
            <div class="col-sm-12 no-padding-m">
                <div class="content_bg_inner_box alert-info">
                    <div class="col-sm-6 no-padding-m">
                        <div class="row margin-row">
                            <div class="col-sm-5"><span class="bask-form-titl font-size-12"><sc:text id="CommissionAmountText" field="CommissionAmount" runat="server"/></span></div>
                            <div class="col-sm-7">
                                <asp:dropdownlist enableviewstate="false" clientidmode="static" id="ddlCommissionAmount" runat="server" cssclass="form-control font-size-12" runat="server">
                                    <asp:ListItem Selected="True" Text="Ordinary Refund-Default" Value="0"></asp:ListItem> 
                                    <asp:ListItem Text="Mismatch" Value="1"></asp:ListItem>  
                                    <asp:ListItem Text="Double Booking" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Hotel Overbooked" Value="3"></asp:ListItem>
                                    <asp:ListItem  Text="24HR Rule/Med Cert" Value="4"></asp:ListItem> 
                                    <asp:ListItem Text="Voucher" Value="5"></asp:ListItem>
                                    <asp:ListItem  Text="CustomerBooked Wrongbreak" Value="6"></asp:ListItem>
                                    <asp:ListItem Text="CustomerService Issue" Value="7"></asp:ListItem>   
                                </asp:dropdownlist>
                            </div>
                        </div>
                        <div class="row margin-row" id="divProviderAmount">
                            <div class="col-sm-5"><span class="bask-form-titl font-size-12"><sc:text id="ProviderAmountText" field="ProviderAmount" runat="server"/></span></div>
                            <div class="col-sm-7">
                                <input type="text" id="txtProviderAmountText" class="form-control font-size-12" />
                            </div>
                        </div>
                        <div class="row margin-row" id="divCommissionAmount" style="display:none;">
                            <div class="col-sm-5"><span class="bask-form-titl font-size-12"><sc:text id="CommissionAmount" field="CommissionAmount" runat="server"/></span></div>
                            <div class="col-sm-7">
                                <input type="text" id="txtCommissionAmountText" class="form-control font-size-12" />
                            </div>
                        </div>
                        <div class="row margin-row" id="divProcessingFeeAmount" style="display:none;">
                            <div class="col-sm-5"><span class="bask-form-titl font-size-12"><sc:text id="ProcessingFeeAmount" field="ProcessingFeeAmount" runat="server"/></span></div>
                            <div class="col-sm-7">
                                <input type="text" id="txtProcessingFeeAmountText" class="form-control font-size-12" />
                            </div>
                        </div>
                         <div class="row margin-row">
                            <div class="table-responsive">
                                <table ID="selectablePayments" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th><sc:text id="CreatedDateText" field="CreatedDate" runat="server"/></th>
                                            <th><sc:text id="RealexIdText" field="RealexId" runat="server"/></th>
                                            <th><sc:text id="AmountText" field="Amount" runat="server"/></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%foreach(var item in AvailableCardPayments){ %>
                                        <tr data-refcode="<%=item.ReferenceId%>" data-provideramt="<%=item.ProviderAmount.ToString("f")%>"
                                            data-processingfee="<%=item.ProcessingFee.ToString("f")%>" data-commissionamt="<%=item.CommissionAmount.ToString("f")%>" >
                                            <td><%=item.CreatedDate.ToString("dd/MM/yyyy") %></td>
                                            <td><%=item.ReferenceId%></td>
                                            <td><%=item.Amount.ToString("f")%></td>
                                        </tr>    
                                        <%} %>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row margin-row">
                            <div class="col-sm-6">
                                <button class="btn btn-default pro-check" type="button" id="btnCardrefund"><sc:text id="RefundText" field="Refund" runat="server"/></button>
                                <button class="btn btn-default pro-check" type="reset"><sc:text id="ResetText" field="Reset" runat="server"/></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default" id="refundbycheque">
        <div class="panel-heading">
            <h4 class="panel-title"><sc:text id="ChequeRefundText" field="ChequeRefund" runat="server"/></h4>
        </div>
        <div class="panel-body alert-info">
            <div class="row margin-row">
                <div class="col-sm-6">
                    <button class="btn btn-default pro-check" type="button" id="btnChequerefund"><sc:text id="RefundLabel" field="Refund" runat="server"/></button>
                    <button class="btn btn-default pro-check" type="reset"><sc:text id="ResetLabel" field="Reset" runat="server"/></button>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(function ($) {
        function SetPayment() {
            var method = $("#ddlPaymentMethod option:selected").text().toLowerCase();
            $("#hdnRefundRefId").val("");
            $("#hdnRefundableProviderAmount").val("");
            $("#hdnRefundableCommissionAmount").val("");
            $("#hdnRefundableProcessingFee").val("");

            if (method.indexOf("card") >= 0) {
                $("#refundbycard").css("display",'');
                $("#refundbycheque").css("display", "none");
                $('#selectablePayments .ui-selected').removeClass('ui-selected');
            }
            else if (method.indexOf("cheque") >= 0) {
                $("#refundbycheque").css("display",'');
                $("#refundbycard").css("display", "none");
            }
            else{
                $("#refundbycard").css("display",'');
                $("#refundbycheque").css("display", "none");
                $('#selectablePayments .ui-selected').removeClass('ui-selected');
            }
        }
        SetPayment();
        $("#ddlPaymentMethod").change(function () {
            SetPayment()
        });
        $("button[type='reset']").click(function(event){
            event.preventDefault();
            // stops the form from resetting after this function
            $(this).closest('form').get(0).reset();
            setTimeout(function(){ 
                $("#ddlCommissionAmount").trigger("change");
                $("#ddlPaymentMethod").trigger("change");
            }, 100);
        });
        $("#ddlCommissionAmount").change(function () {
            var val = $(this).val();
            if (val != "0") {
                $("#divCommissionAmount").css("display",'');
                $("#divProcessingFeeAmount").css("display",'');
            }
            else {
                $("#txtCommissionAmountText").val("");
                $("#txtProcessingFeeAmountText").val("");
                $("#divCommissionAmount").css("display", "none");
                $("#divProcessingFeeAmount").css("display", "none");
            }
        });
        $("#selectablePayments>tbody").selectable({
            filter: 'tr',
            selected: function (e, ui) {
                $("#hdnRefundRefId").val($(ui.selected).data("refcode"));
                $("#hdnRefundableProviderAmount").val($(ui.selected).data("provideramt"));
                $("#hdnRefundableCommissionAmount").val($(ui.selected).data("commissionamt"));
                $("#hdnRefundableProcessingFee").val($(ui.selected).data("processingfee"));
            }
        });
        $("#btnCardrefund").click(function () {
            var refProviderAmt = parseFloat($("#hdnRefundableProviderAmount").val());
            var refCommissionAmt = parseFloat($("#hdnRefundableCommissionAmount").val());
            var refProcessingAmt = parseFloat($("#hdnRefundableProcessingFee").val());

            var amt = parseFloat($("#txtAmount").val());
            var providerAmt = parseFloat($("#txtProviderAmountText").val());
            var commAmt = parseFloat($("#txtCommissionAmountText").val());
            var proFee = parseFloat($("#txtProcessingFeeAmountText").val());
            var currid = "<%= QueryString("CID")%>";

            refProviderAmt = !isNaN(refProviderAmt) ? (refProviderAmt) : 0.00;
            refCommissionAmt = !isNaN(refCommissionAmt) ? (refCommissionAmt) : 0.00;
            refProcessingAmt = !isNaN(refProcessingAmt) ? (refProcessingAmt) : 0.00;

            amt = !isNaN(amt) ? (amt) : 0.00;
            providerAmt = (!isNaN(providerAmt) ? (providerAmt) : 0.00);
            commAmt = (!isNaN(commAmt) ? (commAmt) : 0.00);
            proFee = (!isNaN(proFee) ? (proFee) : 0.00);

            if (amt == 0){
                CS.Common.ShowAlert('No refundable amount available', '');
            }
            else if (providerAmt > 0 || commAmt > 0 || proFee > 0){
                CS.Common.ShowAlert('Amount needs to be negative', '');
            }
            else if ((providerAmt + commAmt + proFee) == 0){
                CS.Common.ShowAlert('Refund amount cannot be 0.00', '');
            }
            else if (-providerAmt > refProviderAmt){
                CS.Common.ShowAlert('Provider amount cannot be greater than <span class="cur-' + currid +'">' + refProviderAmt + '</span>');
            }
            else if (-commAmt > refCommissionAmt){
                CS.Common.ShowAlert('Commission amount cannot be greater than <span class="cur-' + currid +'">' + refCommissionAmt + '</span>');
            }
            else if (-proFee > refProcessingAmt){
                CS.Common.ShowAlert('Processing fee cannot be greater than <span class="cur-' + currid +'">' + refProcessingAmt + '</span>');
            }
            else if (-(providerAmt + commAmt + proFee) > -amt) {
                CS.Common.ShowAlert('Impossible to refund. Total Refundable Amount is too big ', '');
            }
            else {
                ProcessRefund({
                    OrderLineId: <%=QueryString("olid")%>,
                    ProviderAmount: providerAmt,
                    CommissionAmount: commAmt,
                    ProcessingFee: proFee,
                    CurrencyTypeId: $("#ddlCurrency").val(),
                    PaymentMethodID: $("#ddlPaymentMethod").val(),
                    ReferenceID: $("#hdnRefundRefId").val(),
                    CreatedBy: 'test'
                });
            }
        });
        $("#btnChequerefund").click(function () {
            var amt = parseFloat($("#txtAmount").val());

            amt = !isNaN(amt) ? (amt) : 0.00;

            ProcessRefund({
                OrderLineId: <%=QueryString("olid")%>,
                Amount: amt,
                CurrencyTypeId: $("#ddlCurrency").val(),
                PaymentMethodID: $("#ddlPaymentMethod").val(),
                ReferenceID: '',
                CreatedBy: 'test'
            });
        });
        function ProcessRefund(refund){
            CS.PaymentService
                .Refund(refund)
                .done(function (data) {
                    if (data.IsSuccess) {
                        CS.Common.ShowAlert(data.Message, "", null, {
                            "Ok": function () {
                                window.location ='<%=GetUrl("/paymenthistory?olid=" + QueryString("olid"))%>';
                            }
                        });
                    }
                    else {
                        CS.Common.ShowResult(data);
                    }
                })
                .fail(function () {
                    CS.Common.ShowGenericError();
                });
            }
    });
</script>
