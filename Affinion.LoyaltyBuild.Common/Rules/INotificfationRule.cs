﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           INotificfationRule.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Interface is used to implement rules for notifications.
/// </summary>

namespace Affinion.LoyaltyBuild.Common.Rules
{
    interface INotificfationRule
    {
        bool ExecuteDisplayRule();
    }
}
