﻿using System;
using UCommerce.EntitiesV2;

namespace Affinion.LoyaltyBuild.BedBanks.General
{
    public static class UCommerceController
    {
        public static Boolean IsHotelInUcommerce(string name)
        {
            Product prod = Product.SingleOrDefault(x => x.Name.Equals(name));
                
            return !ReferenceEquals(null, prod);
        }
    }
}
