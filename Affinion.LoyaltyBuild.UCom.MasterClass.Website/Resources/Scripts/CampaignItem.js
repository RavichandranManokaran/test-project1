﻿/// <reference path="../sitecore modules/Shell/Ucommerce/Marketing/EditCampaignItemInviteSupplierNew.ascx" />
/// <reference path="../sitecore modules/Shell/Ucommerce/Marketing/EditCampaignItemInviteSupplierNew.ascx" />
function CheckValue(dd, ui) {
    var value = $("#" + dd).val();
    if ($("#" + ui + ' li:contains("' + value + '")').length > 0) {
    }
    else {
        if ($("#" + dd).get(0).selectedIndex != 0) {
            $("#" + ui).append('<li class="list-group-item">' + $("#" + dd).val() + '<a href="javascript:void(0)" id="removeitem" ><span class="pull-right glyphicon glyphicon-minus-sign red"></span></a></li>')
        }
        else {
            alert("Select other Value")
        }
    }
}
$(document).on("click", "#removeitem", function () {
    $(this).parent('li').remove();
});

function DisplayJSONData() {
    var d = $("#hdQuery").val();
    d = "[{" + d + "}]";
    var a = JSON.parse(d);
    if (a[0].Country != null && a[0].Country != 'undefined') {
        $.each(a[0].Country, function (index, d) {
            $('#UlCountry').append('<li class="list-group-item">' + d + '<a href="#" id="removeitem" ><span class="pull-right glyphicon glyphicon-minus-sign red"></span></a></li>');
        });
    }
    if (a[0].Region != null && a[0].Region != 'undefined') {
        $.each(a[0].Region, function (index, d) {
            $('#UIRegion').append('<li class="list-group-item">' + d + '<a href="#" id="removeitem" ><span class="pull-right glyphicon glyphicon-minus-sign red"></span></a></li>');
        });
    }
    if (a[0].Location != null && a[0].Location != 'undefined') {
        $.each(a[0].Location, function (index, d) {
            $('#UILocation').append('<li class="list-group-item">' + d + '<a href="#" id="removeitem" ><span class="pull-right glyphicon glyphicon-minus-sign red"></span></a></li>');
        });
    }
    if (a[0].HotelType != null && a[0].HotelType != 'undefined') {
        $.each(a[0].HotelType, function (index, d) {
            $('#UIHotelType').append('<li class="list-group-item">' + d + '<a href="#" id="removeitem" ><span class="pull-right glyphicon glyphicon-minus-sign red"></span></a></li>');
        });
    }
    if (a[0].Theme != null && a[0].Theme != 'undefined') {
        $.each(a[0].Theme, function (index, d) {
            $('#UITheme').append('<li class="list-group-item">' + d + '<a href="#" id="removeitem" ><span class="pull-right glyphicon glyphicon-minus-sign red"></span></a></li>');
        });
    }
    if (a[0].Group != null && a[0].Group != 'undefined') {
        $.each(a[0].Group, function (index, d) {
            $('#UIGroup').append('<li class="list-group-item">' + d + '<a href="#" id="removeitem" ><span class="pull-right glyphicon glyphicon-minus-sign red"></span></a></li>');
        });
    }
    if (a[0].Experience != null && a[0].Experience != 'undefined') {
        $.each(a[0].Experience, function (index, d) {
            $('#UIExperience').append('<li class="list-group-item">' + d + '<a href="#" id="removeitem" ><span class="pull-right glyphicon glyphicon-minus-sign red"></span></a></li>');
        });
    }
    if (a[0].Ratting != null && a[0].Ratting != 'undefined') {
        $.each(a[0].Ratting, function (index, d) {
            $('#UIRating').append('<li class="list-group-item">' + d + '<a href="#" id="removeitem" ><span class="pull-right glyphicon glyphicon-minus-sign red"></span></a></li>');
        });
    }
}
function SetJsonData() {
    var Country = [];
    $("#UlCountry li").each(function (i, v) { Country.push($(v).text()); })
    var CountryData = "\"Country\":" + JSON.stringify(Country);

    var Region = [];
    $("#UIRegion li").each(function (i, v) { Region.push($(v).text()); })
    var RegionData = "\"Region\":" + JSON.stringify(Region);

    var Location = [];
    $("#UILocation li").each(function (i, v) { Location.push($(v).text()); })
    var LocationData = "\"Location\":" + JSON.stringify(Location);

    var Hotel_Type = [];
    $("#UIHotelType li").each(function (i, v) { Hotel_Type.push($(v).text()); })
    var Hotel_TypeData = "\"HotelType\":" + JSON.stringify(Hotel_Type);

    var Theme = [];
    $("#UITheme li").each(function (i, v) { Theme.push($(v).text()); })
    var ThemeData = "\"Theme\":" + JSON.stringify(Theme);

    var Group = [];
    $("#UIGroup li").each(function (i, v) { Group.push($(v).text()); })
    var GroupData = "\"Group\":" + JSON.stringify(Group);

    var Experience = [];
    $("#UIExperience li").each(function (i, v) { Experience.push($(v).text()); })
    var ExperienceData = "\"Experience\":" + JSON.stringify(Experience);

    var Hotel_Rating = [];
    $("#UIRating li").each(function (i, v) { Hotel_Rating.push($(v).text()); })
    var Hotel_RatingData = "\"Ratting\":" + JSON.stringify(Hotel_Rating);
    var abc = [CountryData, RegionData, LocationData, Hotel_TypeData, ThemeData, GroupData, ExperienceData, Hotel_RatingData];
    var Data = [CountryData, RegionData, LocationData, Hotel_TypeData, ThemeData, GroupData, ExperienceData, Hotel_RatingData];

    $("#hdQuery").val(Data);

    //$.ajax({
    //    type: 'POST',
    //    url: '../Shell/Ucommerce/Marketing/EditCampaignItemInviteSupplierNew.ascx/SaveSupplierInvite',
    //    data: { Data: Data },
    //    dataType: 'json',
    //    success: function () {
    //        alert("hiiii");
    //    },
    //    error: function (XMLHttpRequest, textStatus, errorThrown) {
    //        alert(textStatus);
    //    }
    //});
}


function setsearch() {
    $("#filterTable-input").keyup(function () {
        filterData(this.value, "tblserachsupplier");
    });

    $("#filterTable-InviteSupplier").keyup(function () {
        filterData(this.value, "tblInvitesupplier");
    });

    $("#filterTable-Subscribe").keyup(function () {
        filterData(this.value, "tblsearchSubscribe");
    });
}

function filterData(value, tableId) {
    var rows = $("#" + tableId).find("tr").not(':first').hide();
    if (value.length) {
        var data = value.split(" ");
        $.each(data, function (i, v) {
            rows.filter(":contains('" + v + "')").show();
        });
    } else rows.show();
}


