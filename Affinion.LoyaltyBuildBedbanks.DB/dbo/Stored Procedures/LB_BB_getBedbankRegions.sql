

/****** Object:  StoredProcedure [dbo].[LB_BB_getBedbankRegions]    Script Date: 7/19/2016 7:25:16 PM ******/

---- ====================================================
---- Description:	RETURNS MARGIN FOR A PROPERTY BASED ON AVAILABILITY SOURCE
---- Written: RICHARD
---- Created: 2016.04.19
---- ====================================================

CREATE PROCEDURE [dbo].[LB_BB_getBedbankRegions]
	
	-- Input parameters 
	@pi_availabilitysourceid int, 
	--Return parameters
	@po_returnstatus INT OUTPUT,
	@po_returnmsg VARCHAR(200) OUTPUT

	
AS
set nocount on;
BEGIN

	--Assume transaction will fail, set the return variable to -1.
	SET @po_returnstatus = -1;
	SET @po_returnmsg = null;

	
	
	BEGIN TRY
	
	Select Distinct Country,mapFromRecordID,RegionID,Region,Count(distinct LB_BB_mappings.ID) as Mappings,isMatched from LB_BB_Location
	left join LB_BB_mappings on LB_BB_Location.Regionid = LB_BB_mappings.mapfromRecordID
	and MapFrom = 'location'
	and Mapsto = 'Maplocation'
	-- WHERE CLAUSE IS A FUDGE TO RETURN BLANK WHERE NO AVAILABILITY SOURCE SELECTED NEED TO BE REVISITED AT A LATER DATE
	where LB_BB_Location.isMatched = case when @pi_availabilitysourceid = -1 then @pi_availabilitysourceid else LB_BB_Location.isMatched end
	Group by Country,RegionID,Region,mapFromRecordID,isMatched
	Order by Count(distinct LB_BB_mappings.ID) desc, Country,mapFromRecordID,RegionID,Region,isMatched
	
      
    END TRY	
	BEGIN CATCH	  
		SET @po_returnstatus = error_number();
		SET @po_returnmsg = error_message();
		RETURN;
	END CATCH
	
	SET @po_returnstatus = 0;
	SET @po_returnmsg = 'Completed successfully.';	
END
















GO

