﻿using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Models
{
    public class SubMenu
    {
        public String MenuText { get; set; }
        public String RedirectUrl { get; set; }
        public String Target { get; set; }
        public Item CurrentItem { get; set; }
        public String CssClass { get; set; }
        public Boolean Disabled { get; set;}
        public List<SubMenu> Children { get; set; }
    }
}