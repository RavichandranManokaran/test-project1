﻿using System;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration.SiteUI.Base;
using Sitecore.Data.Items;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Sitecore.Links;
using Sitecore.Web.UI.WebControls;
using Sitecore.Data.Managers;
using System.Linq;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration;
using System.Web;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Navigation
{
    public partial class Tertiary_Nav : SitecoreUserControlBase
    {
      
     private void Page_Load(object sender, EventArgs e)
      {
          //Label2.Text = Session["UserName"].ToString();
          //var userName = HttpContext.Current.User.Identity.Name;
         // var userName = SessionHelper.GetItem<string>(SessionKeys.loggedInUserName);
          var userName = SitecoreUserProfile.GetItem(UserProfileKeys.loggedInUserName);
          Label2.Text = userName;
         
          
      }
      protected void btnLogout_Click(object sender, EventArgs e)
      {
          SitecoreUserProfile.ClearCustomProperty();
          Sitecore.Security.Authentication.AuthenticationManager.Logout();
          Sitecore.Web.WebUtil.Redirect(Constants.redirectToLogOn);
      }
    }
}