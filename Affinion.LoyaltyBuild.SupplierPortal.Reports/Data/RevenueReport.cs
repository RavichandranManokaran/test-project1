﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.SupplierPortal.Reports.Data
{
    /// <summary>
    /// Base Revenue report item used by two revenue report items
    /// </summary>
    public class RevenueReport
    {
        /// <summary>
        /// Offer id
        /// </summary>
        public int OfferId { get; set; }

        /// <summary>
        /// Offer name
        /// </summary>
        public string OfferName { get; set; }

        /// <summary>
        /// Number Of Bookings
        /// </summary>
        public int? NumberOfBookings { get; set; }

        /// <summary>
        /// Total Revenue
        /// </summary>
        public int? TotalRevenue { get; set; }

        /// <summary>
        /// Date
        /// </summary>
        public DateTime? Date { get; set; }
    }

    /// <summary>
    /// Collection to manage Revenue Report
    /// </summary>
    public class RevenueReportCollection : IEnumerable<RevenueReport>
    {
        //Private variables declaration
        private List<RevenueReport> _items;

        //To display row count
        public int Count
        {
            get { return _items.Count; }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public RevenueReportCollection()
        {
            _items = new List<RevenueReport>();
        }

        /// <summary>
        /// Add method to add item
        /// </summary>
        /// <param name="item"></param>
        public void Add(RevenueReport item)
        {
            _items.Add(item);
        }

        /// <summary>
        /// Override method IEnumenrable class
        /// </summary>
        /// <returns></returns>
        public IEnumerator<RevenueReport> GetEnumerator()
        {
            foreach (var item in _items)
                yield return item;
        }

        /// <summary>
        /// Override method IEnumenrable class
        /// </summary>
        /// <returns></returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            foreach (var item in _items)
                yield return item;
        }

    }
}
