﻿CREATE PROCEDURE [dbo].[LB_ValidateOfferAvailability]
(@OrderId int,
@OrderLineId int,
@CheckInDateCount int, 
@CheckOutDateCount int,
@Sku NVARCHAR(512),
@CampaignID int,
@IsValid BIT OUTPUT)
AS
SET @IsValid = 1;

DECLARE @Quantity INT;

SELECT @Quantity = COUNT(OL.OrderLineId)
FROM uCommerce_OrderLine OL
WHERE OL.OrderId = @OrderId AND OL.Sku = @Sku

WHILE (@CheckInDateCount < @CheckOutDateCount)
BEGIN

DECLARE @Balance int;
SELECT @Balance = (OA.NumberAllocated - OA.NumberBooked)
FROM [LB_OfferAvailability] OA
WHERE OA.DateCounter = @CheckInDateCount AND
	OA.ProviderOccupancyTypeID = @Sku AND
	OA.CampaignItem = @CampaignID

	IF (@Balance < @Quantity)
	BEGIN
		SET @IsValid = 0
	END

SET @CheckInDateCount = @CheckInDateCount + 1
END