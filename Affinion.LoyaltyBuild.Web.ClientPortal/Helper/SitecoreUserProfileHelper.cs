﻿using Sitecore.Security;
using Sitecore.Security.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Helper
{
    public class SitecoreUserProfileHelper
    {
          public static UserProfile CurrentUserProfile
        {
            get
            {
                return User.Current.Profile;
            }
        }
        
        ///// <summary>
        ///// Logged in agent name
        ///// </summary>
        //  public string CreatedBy { set; get; }

        public static void AddItem(string objectToSession, string profileName)
        {

            CurrentUserProfile.SetCustomProperty(profileName, objectToSession);
            CurrentUserProfile.Save();
        }      
        /// <summary>
        /// Retrive cached item
        /// </summary>
        /// <typeparam name="T">Type of Session Item</typeparam>
        /// <param name="sessionName">Name of Session Item</param>
        /// <returns>Session Item as type</returns>
        public static string GetItem(string profileName)
        {
            try
            {
                return CurrentUserProfile.GetCustomProperty(profileName);
            }
            catch
            {
                return null;
            }
        }

        public static void ClearCustomProperty()
        {
            var allCustomProperties = CurrentUserProfile.GetCustomPropertyNames();

            foreach (var item in allCustomProperties)
            {
                CurrentUserProfile.RemoveCustomProperty(item);
                CurrentUserProfile.Save();

            }

        }
        /// <summary>
        /// createdby agent name
        /// </summary>
        public static string GetCreatedBy
        {
            get
            {                
                SitecoreUserProfileHelper.GetItem(UserProfileKeys.CreatedBy);

                return " ";   

            }
        }



    }

    public abstract class UserProfileKeys
    {

        public const string paymentPortionSelectedValue = "paymentPortionSelectedValue";
        public const string paymentMethodSelectedValue = "paymentPortionSelectedValue";
        public const string CreatedBy = "loggedInAgent";
        public const string LoggedInUserRoles = "loggedInUserRoles";
        public const string SetAccommodationDetails = "setAccommodationDetails";
    }

}