﻿CREATE FUNCTION [dbo].[GetDateByCounter]
(
	@counter int 	 
)
RETURNS DateTime
AS
BEGIN
	DECLARE @Date DateTime;
	SELECT @Date=(CAST('1/1/1990' AS Datetime) + @counter)
	RETURN @Date
END