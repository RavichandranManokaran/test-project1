﻿CREATE TABLE [dbo].[LB_CommissionAward](
	[CommissionAwardId] [int] NOT NULL,
	[DiscountAmount] [decimal](18, 2) NOT NULL,
	[IsPercentage] [bit] NOT NULL,
	[OnOrderLine] [bit] NOT NULL,
 CONSTRAINT [PK_uCommerce_CommissionAward] PRIMARY KEY CLUSTERED 
(
	[CommissionAwardId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[LB_CommissionAward]  WITH CHECK ADD  CONSTRAINT [FK_uCommerce_CommissionAward_uCommerce_Award] FOREIGN KEY([CommissionAwardId])
REFERENCES [dbo].[uCommerce_Award] ([AwardId])
GO

ALTER TABLE [dbo].[LB_CommissionAward] CHECK CONSTRAINT [FK_uCommerce_CommissionAward_uCommerce_Award]
GO


