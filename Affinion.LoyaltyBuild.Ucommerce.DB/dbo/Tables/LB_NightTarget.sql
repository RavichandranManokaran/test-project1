﻿
CREATE TABLE [dbo].[LB_NightTarget](
	[NightTargetId] [int] NOT NULL,
	[Night] [int] NOT NULL,
	[LastArrivalDay] [varchar](50) NULL,
 CONSTRAINT [PK_uCommerce_NightTarget] PRIMARY KEY CLUSTERED 
(
	[NightTargetId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[LB_NightTarget]  WITH CHECK ADD  CONSTRAINT [FK_uCommerce_NightTarget_uCommerce_Target] FOREIGN KEY([NightTargetId])
REFERENCES [dbo].[uCommerce_Target] ([TargetId])
GO

ALTER TABLE [dbo].[LB_NightTarget] CHECK CONSTRAINT [FK_uCommerce_NightTarget_uCommerce_Target]
GO


