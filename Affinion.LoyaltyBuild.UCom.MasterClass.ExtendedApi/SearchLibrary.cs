﻿using Affinion.LoyaltyBuild.UCom.Common.Constants;
using Affinion.LoyaltyBuild.UCom.Common.Extension;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi
{
    public static class SearchLibrary
    {
        public static List<Product> FilterData1(FilterSupplier obj)
        {
            List<Product> product = new List<Product>();
            bool filterRecord = false;
            bool filterLater = false;

            var hotelCategory = Product.All().Where(x => x.ProductDefinition.Name.Equals(ProductDefinationConstant.Hotel));
            if (obj.Country != null && obj.Country.Any())
            {
                filterRecord = true;
                hotelCategory = hotelCategory.Where(x => x.ProductProperties.Any(y => !y.ProductDefinitionField.Deleted && y.ProductDefinitionField.Name == ProductDefinationConstant.Country && obj.Country.Contains(y.Value)));
            }
            if (obj.Region != null && obj.Region.Any())
            {
                filterRecord = true;
                hotelCategory = hotelCategory.Where(x => x.ProductProperties.Any(y => !y.ProductDefinitionField.Deleted && y.ProductDefinitionField.Name == ProductDefinationConstant.Region && obj.Region.Contains(y.Value)));
            }
            if (obj.Location != null && obj.Location.Any())
            {
                filterRecord = true;
                hotelCategory = hotelCategory.Where(x => x.ProductProperties.Any(y => !y.ProductDefinitionField.Deleted && y.ProductDefinitionField.Name == ProductDefinationConstant.Location && obj.Location.Contains(y.Value)));
            }
            if (obj.Ratting != null && obj.Ratting.Any())
            {
                filterRecord = true;
                hotelCategory = hotelCategory.Where(x => x.ProductProperties.Any(y => !y.ProductDefinitionField.Deleted && y.ProductDefinitionField.Name == ProductDefinationConstant.HotelRating && obj.Ratting.Contains(y.Value)));
            }
            if (obj.HotelType != null && obj.HotelType.Any())
            {
                filterRecord = true;
                hotelCategory = hotelCategory.Where(x => x.ProductProperties.Any(y => !y.ProductDefinitionField.Deleted && y.ProductDefinitionField.Name == ProductDefinationConstant.HotelType && obj.HotelType.Contains(y.Value)));
            }
            if (obj.Theme != null && obj.Theme.Any())
            {
                filterRecord = true;
                filterLater = true;
                //   hotelCategory = hotelCategory.Where(x => x.CategoryProperties.Any(y => !y.DefinitionField.Deleted && y.DefinitionField.Name == DefinationConstant.Theme && obj.Theme.Contains(y.Value)));
            }
            if (obj.Group != null && obj.Group.Any())
            {
                filterRecord = true;
                hotelCategory = hotelCategory.Where(x => x.ProductProperties.Any(y => !y.ProductDefinitionField.Deleted && y.ProductDefinitionField.Name == ProductDefinationConstant.Groups && obj.Group.Contains(y.Value)));
            }
            if (obj.Experience != null && obj.Experience.Any())
            {
                filterRecord = true;
                filterLater = true;
                //   hotelCategory = hotelCategory.Where(x => x.CategoryProperties.Any(y => !y.DefinitionField.Deleted && y.DefinitionField.Name == DefinationConstant.HotelFacility && obj.Experience.Any(z => y.Value.Contains(z))));
            }
            if (filterRecord)
            {
                product = hotelCategory.ToList();
            }
            if (filterLater)
            {
                if (obj.Experience != null && obj.Experience.Any())
                {
                    product = product.Where(x => x.ProductProperties.Any(y => !y.ProductDefinitionField.Deleted && y.ProductDefinitionField.Name == ProductDefinationConstant.Experience && obj.Experience.Any(z => y.Value.ToLower().Contains(z.ToLower())))).ToList();
                }
                if (obj.Theme != null && obj.Theme.Any())
                {
                    product = product.Where(x => x.ProductProperties.Any(y => !y.ProductDefinitionField.Deleted && y.ProductDefinitionField.Name == ProductDefinationConstant.Theme && obj.Theme.Any(z => y.Value.ToLower().Contains(z.ToLower())))).ToList();
                }
            }

            return product;
        }


        public static List<Product> FilterProduct(string region, string city, string hotelRating, string hotelType, string theme, string group, string facility, string roomType, string addonItem)
        {
            List<Product> product = new List<Product>();
            var hotelCategory = Product.All().Where(x => x.ProductDefinition.Name.Equals(ProductDefinationConstant.Hotel));
            if (!string.IsNullOrEmpty(region))
            {
                hotelCategory = hotelCategory.Where(x => x.ProductProperties.Any(y => !y.ProductDefinitionField.Deleted && y.ProductDefinitionField.Name == ProductDefinationConstant.Region && y.Value == region));
            }
            if (!string.IsNullOrEmpty(city))
            {
                hotelCategory = hotelCategory.Where(x => x.ProductProperties.Any(y => !y.ProductDefinitionField.Deleted && y.ProductDefinitionField.Name == ProductDefinationConstant.Location && y.Value == city));
            }
            if (!string.IsNullOrEmpty(hotelRating))
            {
                hotelCategory = hotelCategory.Where(x => x.ProductProperties.Any(y => !y.ProductDefinitionField.Deleted && y.ProductDefinitionField.Name == ProductDefinationConstant.HotelRating && y.Value == hotelRating));
            }
            if (!string.IsNullOrEmpty(hotelType))
            {
                hotelCategory = hotelCategory.Where(x => x.ProductProperties.Any(y => !y.ProductDefinitionField.Deleted && y.ProductDefinitionField.Name == ProductDefinationConstant.HotelType && y.Value == hotelType));
            }
            if (!string.IsNullOrEmpty(theme))
            {
                hotelCategory = hotelCategory.Where(x => x.ProductProperties.Any(y => !y.ProductDefinitionField.Deleted && y.ProductDefinitionField.Name == ProductDefinationConstant.Theme && y.Value.Contains(theme)));
            }
            if (!string.IsNullOrEmpty(group))
            {
                hotelCategory = hotelCategory.Where(x => x.ProductProperties.Any(y => !y.ProductDefinitionField.Deleted && y.ProductDefinitionField.Name == ProductDefinationConstant.Groups && y.Value == group));
            }
            if (!string.IsNullOrEmpty(facility))
            {
                hotelCategory = hotelCategory.Where(x => x.ProductProperties.Any(y => !y.ProductDefinitionField.Deleted && y.ProductDefinitionField.Name == ProductDefinationConstant.Experience && y.Value.Contains(facility)));
            }
            product = hotelCategory.ToList();

            //if (!string.IsNullOrEmpty(roomType))
            //{

            //    product = product.Where(x => x.Products.Any(y => !y.ProductDefinition.Deleted
            //                                                            && y.ProductDefinition.Name.Equals(ProductDefinationConstant.HotelRoom)
            //                                                            && y.ProductProperties.Any(z => !z.ProductDefinitionField.Deleted && z.ProductDefinitionField.Name.Equals(ProductDefinationConstant.RoomType) && z.Value.Equals(roomType)))).ToList();
            //}

            //if (!string.IsNullOrEmpty(roomType))
            //{

            //    product = product.Where(x => x.Products.Any(y => !y.ProductDefinition.Deleted
            //                                                            && y.ProductDefinition.Name.Equals(ProductDefinationConstant.HotelRoom)
            //                                                            && y.ProductProperties.Any(z => !z.ProductDefinitionField.Deleted && z.ProductDefinitionField.Name.Equals(ProductDefinationConstant.RoomType) && z.Value.Equals(roomType)))).ToList();
            //}

            //if (!string.IsNullOrEmpty(addonItem))
            //{
            //    List<Category> pcategory = Product.All().Where(y => !y.ProductDefinition.Deleted
            //                                                              && y.ProductDefinition.Name.Equals(ProductDefinationConstant.AddOn)
            //                                                              && y.ProductProperties.Any(z => !z.ProductDefinitionField.Deleted && z.ProductDefinitionField.Name.Equals(ProductDefinationConstant.ProductType) && z.Value.Equals(roomType))
            //                                                              ).Select(x => x.GetCategories().FirstOrDefault()).ToList();
            //    if (product != null && product.Any())
            //    {
            //        product = product.Intersect(pcategory).ToList();
            //    }
            //    else
            //    {
            //        product = pcategory;
            //    }

            //}

           // product = hotelCategory.ToList();
            return product;
        }



        //public static List<Category> FilterData(FilterSupplier obj)
        //{
        //    List<Category> category = new List<Category>();
        //    bool filterRecord = false;
        //    bool filterLater = false;
        //    var hotelCategory = Category.All().Where(x => !x.Deleted && x.Definition.Name.Equals(DefinationConstant.HotelCategory));
        //    if (obj.Country != null && obj.Country.Any())
        //    {
        //        filterRecord = true;
        //        hotelCategory = hotelCategory.Where(x => x.CategoryProperties.Any(y => !y.DefinitionField.Deleted && y.DefinitionField.Name == DefinationConstant.Country && obj.Country.Contains(y.Value)));
        //    }
        //    if (obj.Region != null && obj.Region.Any())
        //    {
        //        filterRecord = true;
        //        hotelCategory = hotelCategory.Where(x => x.CategoryProperties.Any(y => !y.DefinitionField.Deleted && y.DefinitionField.Name == DefinationConstant.Region && obj.Region.Contains(y.Value)));
        //    }
        //    if (obj.Location != null && obj.Location.Any())
        //    {
        //        filterRecord = true;
        //        hotelCategory = hotelCategory.Where(x => x.CategoryProperties.Any(y => !y.DefinitionField.Deleted && y.DefinitionField.Name == DefinationConstant.Location && obj.Location.Contains(y.Value)));
        //    }
        //    if (obj.Ratting != null && obj.Ratting.Any())
        //    {
        //        filterRecord = true;
        //        hotelCategory = hotelCategory.Where(x => x.CategoryProperties.Any(y => !y.DefinitionField.Deleted && y.DefinitionField.Name == DefinationConstant.HotelRating && obj.Ratting.Contains(y.Value)));
        //    }
        //    if (obj.HotelType != null && obj.HotelType.Any())
        //    {
        //        filterRecord = true;
        //        hotelCategory = hotelCategory.Where(x => x.CategoryProperties.Any(y => !y.DefinitionField.Deleted && y.DefinitionField.Name == DefinationConstant.HotelType && obj.HotelType.Contains(y.Value)));
        //    }
        //    if (obj.Theme != null && obj.Theme.Any())
        //    {
        //        filterRecord = true;
        //        filterLater = true;
        //        //   hotelCategory = hotelCategory.Where(x => x.CategoryProperties.Any(y => !y.DefinitionField.Deleted && y.DefinitionField.Name == DefinationConstant.Theme && obj.Theme.Contains(y.Value)));
        //    }
        //    if (obj.Group != null && obj.Group.Any())
        //    {
        //        filterRecord = true;
        //        hotelCategory = hotelCategory.Where(x => x.CategoryProperties.Any(y => !y.DefinitionField.Deleted && y.DefinitionField.Name == DefinationConstant.Groups && obj.Group.Contains(y.Value)));
        //    }
        //    if (obj.Experience != null && obj.Experience.Any())
        //    {
        //        filterRecord = true;
        //        filterLater = true;
        //        //   hotelCategory = hotelCategory.Where(x => x.CategoryProperties.Any(y => !y.DefinitionField.Deleted && y.DefinitionField.Name == DefinationConstant.HotelFacility && obj.Experience.Any(z => y.Value.Contains(z))));
        //    }
        //    if (filterRecord)
        //    {
        //        category = hotelCategory.ToList();
        //    }
        //    if (filterLater)
        //    {
        //        if (obj.Experience != null && obj.Experience.Any())
        //        {
        //            category = category.Where(x => x.CategoryProperties.Any(y => !y.DefinitionField.Deleted && y.DefinitionField.Name == DefinationConstant.HotelFacility && obj.Experience.Any(z => y.Value.ToLower().Contains(z.ToLower())))).ToList();
        //        }
        //        if (obj.Theme != null && obj.Theme.Any())
        //        {
        //            category = category.Where(x => x.CategoryProperties.Any(y => !y.DefinitionField.Deleted && y.DefinitionField.Name == DefinationConstant.Theme && obj.Theme.Any(z => y.Value.ToLower().Contains(z.ToLower())))).ToList();
        //        }
        //    }

        //    return category;
        //}







        //public static List<Category> FilterProduct(string region, string city, string hotelRating, string hotelType, string theme, string group, string facility, string roomType, string addonItem)
        //{
        //    List<Category> category = new List<Category>();
        //    var hotelCategory = Category.All().Where(x => !x.Deleted && x.Definition.Name.Equals(DefinationConstant.HotelCategory));
        //    if (!string.IsNullOrEmpty(region))
        //    {

        //        hotelCategory = hotelCategory.Where(x => x.CategoryProperties.Any(y => !y.DefinitionField.Deleted && y.DefinitionField.Name == DefinationConstant.Region && y.Value == region));
        //    }
        //    if (!string.IsNullOrEmpty(city))
        //    {
        //        hotelCategory = hotelCategory.Where(x => x.CategoryProperties.Any(y => !y.DefinitionField.Deleted && y.DefinitionField.Name == DefinationConstant.Location && y.Value == city));
        //    }
        //    if (!string.IsNullOrEmpty(hotelRating))
        //    {
        //        hotelCategory = hotelCategory.Where(x => x.CategoryProperties.Any(y => !y.DefinitionField.Deleted && y.DefinitionField.Name == DefinationConstant.HotelRating && y.Value == hotelRating));
        //    }
        //    if (!string.IsNullOrEmpty(hotelType))
        //    {
        //        hotelCategory = hotelCategory.Where(x => x.CategoryProperties.Any(y => !y.DefinitionField.Deleted && y.DefinitionField.Name == DefinationConstant.HotelType && y.Value == hotelType));
        //    }
        //    if (!string.IsNullOrEmpty(theme))
        //    {
        //        hotelCategory = hotelCategory.Where(x => x.CategoryProperties.Any(y => !y.DefinitionField.Deleted && y.DefinitionField.Name == DefinationConstant.Theme && y.Value.Contains(theme)));
        //    }
        //    if (!string.IsNullOrEmpty(group))
        //    {
        //        hotelCategory = hotelCategory.Where(x => x.CategoryProperties.Any(y => !y.DefinitionField.Deleted && y.DefinitionField.Name == DefinationConstant.Groups && y.Value == group));
        //    }
        //    if (!string.IsNullOrEmpty(facility))
        //    {
        //        hotelCategory = hotelCategory.Where(x => x.CategoryProperties.Any(y => !y.DefinitionField.Deleted && y.DefinitionField.Name == DefinationConstant.HotelFacility && y.Value.Contains(facility)));
        //    }
        //    category = hotelCategory.ToList();

        //    if (!string.IsNullOrEmpty(roomType))
        //    {

        //        category = category.Where(x => x.Products.Any(y => !y.ProductDefinition.Deleted
        //                                                                && y.ProductDefinition.Name.Equals(ProductDefinationConstant.HotelRoom)
        //                                                                && y.ProductProperties.Any(z => !z.ProductDefinitionField.Deleted && z.ProductDefinitionField.Name.Equals(ProductDefinationConstant.RoomType) && z.Value.Equals(roomType)))).ToList();
        //    }

        //    if (!string.IsNullOrEmpty(roomType))
        //    {
               
        //        category = category.Where(x => x.Products.Any(y => !y.ProductDefinition.Deleted
        //                                                                && y.ProductDefinition.Name.Equals(ProductDefinationConstant.HotelRoom)
        //                                                                && y.ProductProperties.Any(z => !z.ProductDefinitionField.Deleted && z.ProductDefinitionField.Name.Equals(ProductDefinationConstant.RoomType) && z.Value.Equals(roomType)))).ToList();
        //    }

        //    if (!string.IsNullOrEmpty(addonItem))
        //    {
        //        List<Category> pcategory = Product.All().Where(y => !y.ProductDefinition.Deleted
        //                                                                  && y.ProductDefinition.Name.Equals(ProductDefinationConstant.AddOn)
        //                                                                  && y.ProductProperties.Any(z => !z.ProductDefinitionField.Deleted && z.ProductDefinitionField.Name.Equals(ProductDefinationConstant.ProductType) && z.Value.Equals(roomType))
        //                                                                  ).Select(x => x.GetCategories().FirstOrDefault()).ToList();
        //        if (category != null && category.Any())
        //        {
        //            category = category.Intersect(pcategory).ToList();
        //        }
        //        else
        //        {
        //            category = pcategory;
        //        }
                
        //    }

        //    category = hotelCategory.ToList();
        //    return category;
        //}
    }
}
