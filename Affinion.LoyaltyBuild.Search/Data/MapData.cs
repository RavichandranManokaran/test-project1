﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           MapData.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Search (VS project name)
/// Description:           Data class for map data
/// </summary>
namespace Affinion.LoyaltyBuild.Search.Data
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Data class for map data
    /// </summary>
    public class MapData
    {
        public string ContextItemId { get; set; }

        public Collection<GeoCordinate> Locations { get; set; }

        public SearchResultsMode ResultsType { get; set; }

        public MapData()
        {
            this.Locations = new Collection<GeoCordinate>();
        }
    }
}
