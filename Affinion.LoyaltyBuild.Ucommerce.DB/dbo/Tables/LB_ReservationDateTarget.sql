﻿CREATE TABLE [dbo].[LB_ReservationDateTarget] (
    [ReservationDateTargetId] INT      NOT NULL,
    [ReservationFromDate]     DATETIME NOT NULL,
    [ReservationToDate]       DATETIME NOT NULL,
    CONSTRAINT [PK_uCommerce_ReservationDate] PRIMARY KEY CLUSTERED ([ReservationDateTargetId] ASC),
    CONSTRAINT [FK_uCommerce_ReservationDateTarget_uCommerce_Target] FOREIGN KEY ([ReservationDateTargetId]) REFERENCES [dbo].[uCommerce_Target] ([TargetId])
);

