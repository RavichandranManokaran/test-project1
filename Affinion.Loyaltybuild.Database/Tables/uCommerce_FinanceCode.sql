CREATE TABLE [dbo].[uCommerce_FinanceCode](
	[FinanceCodeId] [int] IDENTITY(1,1) NOT NULL,
	[OrderLineId] [int] NULL,
	[Company] [int] NULL,
	[MerchantIdentificationNo] [bigint] NULL,
	[ClientName] [varchar](100) NULL,
	[Department] [int] NULL,
	[ProductCode] [varchar](100) NULL,
	[GlCode] [varchar](100) NULL,
	[ClientCode] [varchar](100) NULL,
	[Description] [varchar](100) NULL,
 CONSTRAINT [PK_dbo.uCommerce_FinanceCode] PRIMARY KEY CLUSTERED 
(
	[FinanceCodeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]



