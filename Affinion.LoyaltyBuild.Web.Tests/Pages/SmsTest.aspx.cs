﻿#region Using Directives
using Affinion.LoyaltyBuild.Communications;
using Affinion.LoyaltyBuild.Communications.SmsService;
using System;
using System.Collections.Generic;
using System.Linq;

#endregion

namespace Affinion.LoyaltyBuild.Web
{
    public partial class SmsTest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Label2.Text = null;
            
            
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            IMessageService smsService = new TelesignMobileSmsService();

            //TelesignMobileSmsServeice smsService1 = TelesignMobileSmsServeice.Instance;

            string number = "947731245698";

            ResponseStatus stat = smsService.Send(number, "Test Massage");
            //string response1 = smsService1.sendSmsHttp(numbers, "Test Massage1");
            var r = smsService.GetErrors().Select(err => err.Error);
            Label2.Text = stat.ToString() + "<br/>" + String.Join("<br/>", r.ToArray());
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            IMessageService smsService = new TelesignMobileSmsService();

            List<string> numbers = new List<string>();

            //TelesignMobileSmsServeice smsService1 = TelesignMobileSmsServeice.Instance;

            numbers.Add("947731245691");
            numbers.Add("947731245692");
            numbers.Add("947731245693");
            numbers.Add("947731245694");
            numbers.Add("947731245695");
            numbers.Add("947731245696");
            numbers.Add("947731245697");
            numbers.Add("947731245698");
            numbers.Add("947731245699");
            numbers.Add("947731245610");

            ResponseStatus stat = smsService.Send(numbers, "Test Massage");
            //string response1 = smsService1.sendSmsHttp(numbers, "Test Massage1");
            var r = smsService.GetErrors().Select(err => err.Error);
            Label2.Text = stat.ToString() + "<br/>" + String.Join("<br/>", r.ToArray()); 
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            IMessageService smsService = new TelesignMobileSmsService();

            List<string> numbers = new List<string>();

            //TelesignMobileSmsServeice smsService1 = TelesignMobileSmsServeice.Instance;

            numbers.Add("947731245691");
            numbers.Add("947731245692");
            numbers.Add("947731245693");
            numbers.Add("947731245694");
            numbers.Add("947731245695");
            numbers.Add("947731245696");
            numbers.Add("947731245697");
            numbers.Add("947731245698");
            numbers.Add("947731245699");
            numbers.Add("947731245610");
            numbers.Add("94773124561");
            ResponseStatus stat = smsService.Send(numbers, "Test Massage");
            //string response1 = smsService1.sendSmsHttp(numbers, "Test Massage1");
            var r = smsService.GetErrors().Select(err => err.Error);
            Label2.Text = stat.ToString() + "<br/>" + String.Join("<br/>", r.ToArray());
        }       
    }
}