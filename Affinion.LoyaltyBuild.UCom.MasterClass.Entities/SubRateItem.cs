﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class SubRateItem : IEntity
    {
        private int? _hashCode;

        // Methods
        public static IQueryable<SubRateItem> All()
        {
            return GetRepo().Select();
        }

        public virtual void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<SubRateItem, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            SubRateItem objA = obj as SubRateItem;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<SubRateItem, bool>> expression)
        {
            return All().Any<SubRateItem>(expression);
        }

        public static IList<SubRateItem> Find(Expression<Func<SubRateItem, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<SubRateItem>();
        }

        public static SubRateItem FirstOrDefault(Expression<Func<SubRateItem, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<SubRateItem>();
        }

        public static SubRateItem Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<SubRateItem> GetRepo()
        {
            //if (_repo != null)
            //{
            //    return _repo;
            //}
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<SubRateItem>>();
            
        }

        public virtual void Save()
        {
            try
            {
                GetRepo().Save(this);
            }
            catch (Exception ex )
            {
                throw ex;
            }
        }

        public static SubRateItem SingleOrDefault(Expression<Func<SubRateItem, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties

        public virtual int Id
        {
            get
            {
                return 0;
            }
        }

        public virtual string SubRateItemCode { get; set; }

        public virtual string DisplayName { get; set; }

    }
}
