﻿CREATE TABLE [dbo].[uCommerce_ShippingMethodCountry] (
    [ShippingMethodId] INT NOT NULL,
    [CountryId]        INT NOT NULL,
    CONSTRAINT [uCommerce_PK_ShippingMethodCountry] PRIMARY KEY CLUSTERED ([ShippingMethodId] ASC, [CountryId] ASC),
    CONSTRAINT [uCommerce_FK_ShippingMethodCountry_Country] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[uCommerce_Country] ([CountryId]),
    CONSTRAINT [uCommerce_FK_ShippingMethodCountry_ShippingMethod] FOREIGN KEY ([ShippingMethodId]) REFERENCES [dbo].[uCommerce_ShippingMethod] ([ShippingMethodId])
);

