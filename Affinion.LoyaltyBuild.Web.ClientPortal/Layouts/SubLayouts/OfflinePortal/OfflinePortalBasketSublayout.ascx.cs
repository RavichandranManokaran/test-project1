﻿
namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Services;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Affinion.Loyaltybuild.BusinessLogic.Helper;
    using Affinion.LoyaltyBuild.Common;
    using Affinion.LoyaltyBuild.Common.Exceptions;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Subrate;
    using Affinion.LoyaltyBuild.Web.ClientPortal.Helper;
    using Sitecore.Data.Items;
    using System.ComponentModel;
    using Affinion.LoyaltyBuild.Search.Helpers;
    using Affinion.LoyaltyBuild.UCom.Common;
    using Affinion.LoyaltyBuild.UCom.Common.Constants;
    //using Affinion.LoyaltyBuild.Common.SessionHelper;
    using UCommerce.EntitiesV2;
    using System.Data;
    using UCommerce.Api;
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Rules;
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.Api.Booking.Data;
    using Affinion.LoyaltyBuild.Web.ClientPortal.Models;
    using Affinion.LoyaltyBuild.Model.Provider;
    //using Affinion.LoyaltyBuild.Api.Booking.Helper;

    public partial class OfflinePortalBasketSublayout : BaseSublayout
    {
        private Item clientItem = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(Constants.HardCodedClientId));
        //SitecoreFieldsHelper.GetLookupFieldTargetItem(ItemHelper.RootItem, "ClientDetails");
        public string voucherRuleId = "{E9FDD77D-55DF-4E6A-82AE-E2EA1EDE04CB}";
        List<Item> hotels = null;
        public string qty = string.Empty;
        public string CurrencySymbol { get; set; }
        public string CurrencyCode { get; set; }
        public string FormattedCurrency { get; set; }
        public string BillingCurrency { get; set; }
        public List<Affinion.LoyaltyBuild.Common.SessionHelper.BasketInfo> ListBasketInfo { get; set; }
       // public RoomReservation roomReservation { get; set; }


        protected String LabelAccomodation { get; set; }
        protected String ConfirmationMessage { get; set; }
        protected String GuestCount { get; set; }
        protected String RemoveBasketText { get; set; }
        protected String ProcessingFeeText { get; set; }
        protected String TotalPayableTodayText { get; set; }
        protected String TotalPriceText { get; set; }
        protected String SetAccommodationText { get; set; }
        protected String AccomodationDetailsRequiredMsg { get; set; }
        protected String SelectPaymentMethodMsg { get; set; }
        protected String SelectPaymentPortionMsg { get; set; }
        protected String PaymentText { get; set; }
        protected String PaymentMethodText { get; set; }
        protected String PaymentPortionText { get; set; }

        Literal CheckInLiteral = null;
        Literal CheckOutLiteral = null;
        Literal RoomsLiteral = null;
        Literal PeopleLiteral = null;
        Literal PriceLiteral = null;
        public string orderLineId = string.Empty;


        ISitecorePaymentService sitecorePaymentService { get; set; }


        // Guest Availability popup page dropdown loading range


        protected void Page_Init(object sender, EventArgs e)
        {
            BindSitecoreText();
        }
        /// <summary>
        /// Page load related events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Sitecore.Events.Event.Subscribe("PartnerDetailsSuccess", ClientValidationSuccess);

            // Put user code to initialize the page here
            if (!IsPostBack)
            {

                hotels = Sitecore.Context.Database.SelectItems(Constants.BasketPageQueryPath).ToList();
                Item supplierOfferItems = Sitecore.Context.Database.GetItem(Constants.SupplierOfferPath);

                if (UCommerce.Api.TransactionLibrary.HasBasket())
                {
                    if (UCommerce.Api.TransactionLibrary.GetBasket(false).PurchaseOrder != null)
                    {

                       int orderId = BasketHelper.GetBasketOrderId();
                      // PopulateBasket(orderId);

                       var order = TransactionLibrary.GetBasket(false).PurchaseOrder;
                       order[OrderPropertyConstants.PaymentMethod] = string.Empty;
                       order.Save();
                       var list = NewPopulatedDataSourceForBasket();
                       PanelRepeater.DataSource = list;
                       PanelRepeater.DataBind();
                     if (list.Any(i => i.ProviderType.Equals(Constants.ProviderType.HoldayHomes, StringComparison.InvariantCultureIgnoreCase)))
                              depositCheckBox.Enabled = false;
                     }
                    else
                    {
                        DisplayEmptyMessage();
                    }
                }
                else
                {
                    DisplayEmptyMessage();
                }

            }

            #region LWD-924

            #endregion
        }

        private List<Affinion.LoyaltyBuild.Common.SessionHelper.BasketInfo> NewPopulatedDataSourceForBasket()
        {

            var basket = Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.GetBasket();
            List<Affinion.LoyaltyBuild.Common.SessionHelper.BasketInfo> listBasketInfo = new List<Affinion.LoyaltyBuild.Common.SessionHelper.BasketInfo>();
            string noOfAdults = string.Empty, noOfChilderen = string.Empty,sku=string.Empty, noofRooms = string.Empty, roomtype = string.Empty, providerstype = string.Empty;
            string guestName = string.Empty;

            foreach (var item in basket.Bookings)
            {
                var providers = item.GetProvider();
                 sku = item.OrderLine.Sku;

                Item hotelItem = null;
                if (providers.ProviderType == Model.Provider.ProviderType.LoyaltyBuild)
                {
                    var hotel = (Hotel)providers;
                    //hotel.Town
                    hotelItem = hotel.SitecoreItem;
                    providerstype = hotel.ProviderType.ToString();
                    //sku= hotel

                }

                DateTime checkout, checkin;

                checkout = item.CheckoutDate;
                checkin = item.CheckinDate;
                string coDate = String.Format("{0:dd/MM/yyyy}", checkout);
                string ciDate = String.Format("{0:dd/MM/yyyy}", checkin);
                string coTime = String.Format("{0:HH:mm}", checkout);
                string ciTime = String.Format("{0:HH:mm}", checkin);

                //int orderLineId = Convert.ToInt32(item.OrderLineId);
                 //sku = Convert.ToString(item.GetProduct().Sku);
               
                //string OrderId = Convert.ToString(item.OrderLineId);
                if (item.Type == Model.Product.ProductType.Room)
                {
                    RoomBooking roomBooking = (RoomBooking)item;
                    noOfAdults = Convert.ToString(roomBooking.NoOfAdults);
                    noOfChilderen = Convert.ToString(roomBooking.NoOfChildren);
                    noofRooms = Convert.ToString(roomBooking.Quantity);
                    roomtype = roomBooking.Type.ToString();
                    guestName = roomBooking.GuestName;
                }

                Common.SessionHelper.BasketInfo bookingobject = new Common.SessionHelper.BasketInfo();
                bookingobject.Sku = sku;
                bookingobject.ProductType = Convert.ToString(item.Type);
                bookingobject.CheckinDate = ciDate;
                bookingobject.CheckoutDate = coDate;
                bookingobject.Nights = (checkout - checkin).ToString("dd");

                bookingobject.Price =item.OrderLine.Total.Value.ToString("#.##");

                //bookingobject.Price = Convert.ToString(item.Price);
                //if (item!=null && item.OrderLine != null)
                //bookingobject.Price = item.OrderLine.Total.Value.ToString("#.##");

                bookingobject.NoOfRooms = noofRooms;
                bookingobject.NoOfAdults = noOfAdults;
                bookingobject.NoOfChildren = noOfChilderen;
                bookingobject.RoomType = roomtype;
                bookingobject.GuestName = Convert.ToString(guestName);
                bookingobject.Adults = noOfAdults;
                bookingobject.People = string.Concat(noOfAdults, " ", "Adults", " ", noOfChilderen, " ", "Children");
                //bookingobject.CategoryId = Convert.ToString(item.CategoryId);
                bookingobject.OrderLineId = Convert.ToString(item.OrderLineId);
                bookingobject.SitecoreItem = hotelItem;
                bookingobject.ProviderType = providerstype;
                GetSitecoreProviderAddress(bookingobject);
                listBasketInfo.Add(bookingobject);

            }

            if (listBasketInfo != null && listBasketInfo.Count > 0)
            {
                decimal processingFee = listBasketInfo.Sum(lst => lst.ProcessingFee == string.Empty ? 0 : Convert.ToDecimal(lst.TotalPrice));
                decimal bookingDeposit = listBasketInfo.Sum(lst => lst.BookingDeposit == string.Empty ? 0 : Convert.ToDecimal(lst.BookingDeposit));
                decimal totalPayableAcc = listBasketInfo.Sum(lst => lst.TotalPayableAtAccommodation == string.Empty ? 0 : Convert.ToDecimal(lst.BookingDeposit)); 
                decimal totalPrice = listBasketInfo.Sum(lst => lst.TotalPrice == string.Empty ? 0 : Convert.ToDecimal(lst.TotalPrice));

                ProcessingFee.Text = processingFee.ToString("#.##");
                BookingDeposit.Text = bookingDeposit.ToString("#.##");
                TotalPayableAccommodation.Text = totalPayableAcc.ToString("#.##");
                TotalPrice.Text = totalPrice.ToString("#.##");
            }

            return listBasketInfo;
        }


        /// <summary>
        /// Event raised on client validation success
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClientValidationSuccess(object sender, EventArgs e)
        {
            if (e != null)
            {
                var customerRefernce = Sitecore.Events.Event.ExtractParameter(e, 0).ToString();

                //set order property for "CustomerReference"
                if (customerRefernce != null && UCommerce.Api.TransactionLibrary.HasBasket())
                {
                    PaymentSection.Visible = true;

                }
            }
        }


        /// <summary>
        /// populate basket payment details
        /// </summary>
        private void PopulateBasket(int orderId)
        {
            if (!PopulateBasketPaymentDetails())
            {

                
                BasketHelper.ClearBasket();
                DisplayEmptyMessage();
                throw new AffinionException(string.Format("Subrates are not configured for selected hotels. OrderID: {0}", orderId));
            }
            sitecorePaymentService = new SitecorePaymentService();
            var paymentMethod = sitecorePaymentService.GetPaymentMethod(orderId);

            //show partial payment option when all items in basket have partial payment applicable
            if (paymentMethod != null && paymentMethod.Count(i => i.Paymentmethod == Constants.PaymentMethod.PartialPayment) == paymentMethod.Count)
            {
                OrderLine orderLine = UCommerce.Api.TransactionLibrary.GetBasket(false).PurchaseOrder.OrderLines.FirstOrDefault();
                if (orderLine != null)
                {
                    string daysBefore = orderLine[OrderPropertyConstants.DaysBefore];
                    string percent = orderLine[OrderPropertyConstants.PercentageOfAmount];
                    // showPaymentMethod.Visible = true;
                    partialCheckBox.Visible = true;
                    //fullCheckbox.Enabled = false;
                    PayableAtAccommodationText.Text = string.Format("Total Payable {0} Days Prior to Arrival:", daysBefore);
                    LitInst.Text = string.Format("Approximately {0} % payable total with the balance due {1} days prior to arrival", percent, daysBefore);
                }

            }
            else
            {

                PayableAtAccommodationText.Text = "Total Payable at Accommodation:";
                partialCheckBox.Visible = false;
                //fullCheckbox.Enabled = true;

            }
        }


        /// <summary>
        /// Displays a Message when Basket is Empty 
        /// </summary>
        private void DisplayEmptyMessage()
        {
            DivEmptyBasket.Visible = true;
            EmptyBasket.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "EmptyBasketMessage");
            showAdditionalDetails.Visible = false;
            subrateSection.Visible = false;

        }

        /// <summary>
        /// populate datasource for basket
        /// </summary>
        private List<Affinion.LoyaltyBuild.Common.SessionHelper.BasketInfo> PopulatedDataSourceForBasket()
        {
            try
            {
                ListBasketInfo = new List<Affinion.LoyaltyBuild.Common.SessionHelper.BasketInfo>();

                ListBasketInfo = BasketHelper.GetPopulateBastketListFromDB();

                List<Item> supplierList = Sitecore.Context.Database.SelectItems(Constants.BasketPageQueryPath).ToList();
                supplierList = (from itm in ListBasketInfo
                                join supplier in hotels on itm.CategoryId equals StoreHelper.GetUcommerceCategoryId(supplier).ToString()
                                select supplier).ToList();

                foreach (var item in ListBasketInfo)
                {

                    item.SitecoreItem = supplierList.FirstOrDefault();

                    // Set the country name from Item ID
                    GetSitecoreProviderAddress(supplierList, item);
                    // remove first item
                }



                return ListBasketInfo;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }


        /// <summary>
        ///set countryid,name and address from ItemID
        /// </summary>
        private static void GetSitecoreProviderAddress(Affinion.LoyaltyBuild.Common.SessionHelper.BasketInfo item)
        {
            if (item != null && item.SitecoreItem != null)
            {
                if (item.SitecoreItem.Fields["Country"] != null)
                {
                    item.Country = Sitecore.Context.Database.GetItem(item.SitecoreItem.Fields["Country"].Value).DisplayName;

                    if (item.SitecoreItem.Fields["AddressLine1"].Value.Length > 0)
                        item.AddressInfo = string.Concat(item.SitecoreItem.Fields["AddressLine1"].Value, ",");

                    if (item.SitecoreItem.Fields["AddressLine3"].Value.Length > 0)
                        item.AddressInfo = string.Concat(item.AddressInfo, item.SitecoreItem.Fields["AddressLine3"].Value, ",");

                    if (item.SitecoreItem.Fields["AddressLine4"].Value.Length > 0)
                        item.AddressInfo = string.Concat(item.AddressInfo, item.SitecoreItem.Fields["AddressLine4"].Value, ",");

                    if (item.SitecoreItem.Fields["AddressLine2"].Value.Length > 0)
                        item.AddressInfo = string.Concat(item.AddressInfo, item.SitecoreItem.Fields["AddressLine2"].Value, ",");

                    if (item.SitecoreItem.Fields["Town"].Value.Length > 0)
                        item.AddressInfo = string.Concat(item.AddressInfo, item.SitecoreItem.Fields["Town"].Value, ",");
                }
                if (item.SitecoreItem.Fields["SupplierType"] != null)
                    item.ProviderType = SitecoreFieldsHelper.GetDropLinkFieldValue(item.SitecoreItem, "SupplierType", "__display name");

            }
        }




        /// <summary>
        ///set countryid,name and address from ItemID
        /// </summary>
        private static void GetSitecoreProviderAddress(List<Item> supplierList, Affinion.LoyaltyBuild.Common.SessionHelper.BasketInfo item)
        {
            if (item != null && item.SitecoreItem != null)
            {
                if (item.SitecoreItem.Fields["Country"] != null)
                {
                    item.Country = Sitecore.Context.Database.GetItem(item.SitecoreItem.Fields["Country"].Value).DisplayName;

                    if (item.SitecoreItem.Fields["AddressLine1"].Value.Length > 0)
                        item.AddressInfo = string.Concat(item.SitecoreItem.Fields["AddressLine1"].Value, ",");

                    if (item.SitecoreItem.Fields["AddressLine3"].Value.Length > 0)
                        item.AddressInfo = string.Concat(item.AddressInfo, item.SitecoreItem.Fields["AddressLine3"].Value, ",");

                    if (item.SitecoreItem.Fields["AddressLine4"].Value.Length > 0)
                        item.AddressInfo = string.Concat(item.AddressInfo, item.SitecoreItem.Fields["AddressLine4"].Value, ",");

                    if (item.SitecoreItem.Fields["AddressLine2"].Value.Length > 0)
                        item.AddressInfo = string.Concat(item.AddressInfo, item.SitecoreItem.Fields["AddressLine2"].Value, ",");

                    if (item.SitecoreItem.Fields["Town"].Value.Length > 0)
                        item.AddressInfo = string.Concat(item.AddressInfo, item.SitecoreItem.Fields["Town"].Value, ",");
                }
                if (item.SitecoreItem.Fields["SupplierType"] != null)
                    item.ProviderType = SitecoreFieldsHelper.GetDropLinkFieldValue(item.SitecoreItem, "SupplierType", "__display name");

                supplierList.RemoveAt(0);
                OrderLine orderLine = UCommerce.Api.TransactionLibrary.GetBasket(false).PurchaseOrder.OrderLines.Where(i => Convert.ToString(i.OrderLineId).Equals(item.OrderLineId)).FirstOrDefault();

                if (orderLine != null && orderLine.Discounts != null && orderLine.Discounts.Count != 0)
                {
                    item.RoomType = string.Concat(item.RoomType, "(", orderLine.Discounts.First().CampaignItemName, ")");
                }
            }
        }


        /// <summary>
        /// bind repeater 
        /// </summary>
        protected void PanelRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Affinion.LoyaltyBuild.Common.SessionHelper.BasketInfo basketInfo = e.Item.DataItem as Affinion.LoyaltyBuild.Common.SessionHelper.BasketInfo;
                Repeater starRepeater = (Repeater)e.Item.FindControl("StarRepeater");
                Label test = e.Item.FindControl("lblhotelname") as Label;
                var dataItem = ((Affinion.LoyaltyBuild.Common.SessionHelper.BasketInfo)e.Item.DataItem).SitecoreItem;
                string ranking = SitecoreFieldsHelper.GetDropLinkFieldValue(dataItem, "StarRanking", "Name");
                test.Text = SitecoreFieldsHelper.GetValue(dataItem, "Name");
                //Label lblAccomodation = e.Item.FindControl("LabelAccomodationValidation") as Label;

                //Bind ranking related fields


                if (e != null)
                {
                    if (basketInfo != null)
                        //Room reservation and Ranking field information
                        GetRoomReservationAndRankingFields(e, basketInfo);

                    if (!string.IsNullOrWhiteSpace(ranking))
                    {
                        int starRank;
                        bool result = Int32.TryParse(ranking, out starRank);
                        if (result)
                        {
                            List<int> listOfStars = Enumerable.Repeat(starRank, starRank).ToList();
                            starRepeater.DataSource = listOfStars;
                            starRepeater.DataBind();
                        }
                        else
                        {
                            Diagnostics.WriteException(DiagnosticsCategory.ClientPortal,
                            new AffinionException("Unable to parse star rank:" + dataItem.DisplayName), this);
                        }
                    }
                }
            }


        }



        //protected void PanelRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //    {
        //        Repeater starRepeater = (Repeater)e.Item.FindControl("StarRepeater");
        //        //Repeater bookingDetailsRepeater = (Repeater)e.Item.FindControl("BookingDetailsRepeater");
        //        var dataItem = ((BasketInfo)e.Item.DataItem).;// (Sitecore.Data.Items.Item)e.Item.DataItem;
        //        var supplierItem = ((BasketInfo)e.Item.DataItem);

        //        // Booking infor controls
        //        //Checkin Checkout Rooms People Price
        //        CheckInLiteral = (Literal)e.Item.FindControl("Checkin");
        //        CheckOutLiteral = (Literal)e.Item.FindControl("Checkout");
        //        RoomsLiteral = (Literal)e.Item.FindControl("Rooms");
        //        PeopleLiteral = (Literal)e.Item.FindControl("People");
        //        PriceLiteral = (Literal)e.Item.FindControl("Price");


        //        //Bind ranking related fields
        //        string ranking = SitecoreFieldsHelper.GetDropLinkFieldValue(dataItem, "StarRanking", "Name");

        //        //string supplierType = dataItem.Fields["SupplierType"].Value;
        //        //int advanceDays = Convert.ToInt32(dataItem.Fields["No of Days in Advance"].Value);
        //        //int partialPercentage = Convert.ToInt32(dataItem.Fields["Payment Percentage"].Value);
        //        //DateTime checkinDate;
        //        //DateTime.TryParse(supplierItem.CheckinDate, out checkinDate);

        //        if (e != null)
        //        {
        //            int count = 0;
        //            var isValid = Int32.TryParse((((BasketInfo)e.Item.DataItem).), out count);
        //            qty = count.ToString();

        //            orderlineid = ((BasketInfo)e.Item.DataItem).OrderLineId.ToString();
        //        }

        //        if (!string.IsNullOrWhiteSpace(ranking))
        //        {
        //            int starRank;
        //            bool result = Int32.TryParse(ranking, out starRank);
        //            if (result)
        //            {
        //                List<int> listOfStars = Enumerable.Repeat(starRank, starRank).ToList();
        //                starRepeater.DataSource = listOfStars;
        //                starRepeater.DataBind();
        //            }
        //            else
        //            {
        //                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal,
        //                new AffinionException("Unable to parse star rank:" + dataItem.DisplayName), this);
        //            }
        //        }

        //        //if (IsPatialPayment(supplierType, advanceDays, checkinDate))
        //        //{
        //        //    //paymentType.Visible = false;
        //        //    System.Web.UI.HtmlControls.HtmlContainerControl paymentType=(System.Web.UI.HtmlControls.HtmlContainerControl)(e.Item.FindControl("paymentType"));
        //        //    paymentType.Visible=true;
        //        //}

        //        //bind Roomwise booking details 
        //        bookingDetailsRepeater.DataSource = PopulateBasket(dataItem);
        //        bookingDetailsRepeater.DataBind();
        //    }
        //}


        /// <summary>u
        /// page unload event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Unload(object sender, EventArgs e)
        {

            Sitecore.Events.Event.Unsubscribe("PartnerValidationSuccess", ClientValidationSuccess);

        }
        private void GetRoomReservationAndRankingFields(RepeaterItemEventArgs e, Affinion.LoyaltyBuild.Common.SessionHelper.BasketInfo basketInfo)
        {
            int count = 0;
            string checkinDate = string.Empty;
            string checkoutDate = string.Empty;
            var isValid = Int32.TryParse((((Affinion.LoyaltyBuild.Common.SessionHelper.BasketInfo)e.Item.DataItem).NoOfRooms), out count);
            qty = count.ToString();

            LinkButton lnkButton = e.Item.FindControl("EditLinkButton") as LinkButton;

            //RoomReservation 
           // roomReservation = BasketHelper.GetRoomReservationDetailsByOrderLineId(basketInfo.OrderLineId);
            if (basketInfo.CheckinDate != null)
            {
                if (basketInfo.CheckinDate.Contains("from"))
                {
                    int startIndexInDate = basketInfo.CheckinDate.IndexOf("from");
                    string startDate = basketInfo.CheckinDate.Remove(startIndexInDate);
                    DateTime inDate = DateTime.Parse(startDate);
                    checkinDate = inDate.ToString("dd/MM/yyyy");
                }
                else
                {
                    checkinDate = basketInfo.CheckinDate;

                }
            }
            if (basketInfo.CheckoutDate != null)
            {
                if (basketInfo.CheckoutDate.Contains("from"))
                {
                    int endIndexInDate = basketInfo.CheckoutDate.IndexOf("from");
                    string endDate = basketInfo.CheckoutDate.Remove(endIndexInDate);
                    DateTime outDate = DateTime.Parse(endDate);
                    checkoutDate = outDate.ToString("dd/MM/yyyy");
                }
                else
                {
                    checkoutDate = basketInfo.CheckoutDate;

                }
            }
            string clientList = Constants.HardCodedClientList;
            string destination = basketInfo.Location != null ? basketInfo.Location : string.Empty;
            string noOfNights = basketInfo.Nights != null ? basketInfo.Nights : string.Empty;
            string NoOfRooms = basketInfo.NoOfRooms != null ? basketInfo.NoOfRooms : string.Empty;
            string offerGroup = basketInfo.OfferGroup != null ? basketInfo.OfferGroup : string.Empty;
            string mode = Constants.HarcodedMode;
            string olid = basketInfo.OrderLineId;
            


            string noOfAdults = basketInfo.NoOfAdults.ToString(); //roomReservation != null ? roomReservation.NoOfAdults.ToString() : string.Empty;
            string noOfChildrens = basketInfo.NoOfChildren.ToString();
            string Price = basketInfo.Price;
          
            

            //roomReservation != null ? roomReservation.NoOfChildren.ToString() : string.Empty;
            //lblAccomodation.Text = ValidateAccomodation().ToString();
            lnkButton.PostBackUrl = "~/location-finder" + "?ClientList=" + clientList + "&Destination=" + destination + "&CheckinDate=" + checkinDate + "&CheckoutDate=" + checkoutDate + "&NoOfNights=" + noOfNights + "&NoOfRooms=" + NoOfRooms + "&NoOfAdults=" + noOfAdults + "&NoOfChildren=" + noOfChildrens + "&Mode=" + mode + "&olid=" + olid;
        }


        /// <summary>
        /// Populates Basket Order Price Details 
        /// </summary>
        private bool PopulateBasketPaymentDetails()
        {





            int? orderId = BasketHelper.GetBasketOrderId();
            var clientId = BasketHelper.GetOrderClientId(orderId.Value);
            //string clientId = clientItem.ID.Guid.ToString();
            if (orderId.HasValue && clientId != null)
            {
                try
                {
                    Affinion.LoyaltyBuild.Common.SessionHelper.BasketInfo basketInfo = BasketHelper.GetPopulateBasketPaymentDetails(orderId.Value, clientId);
                    if (basketInfo == null)
                    {
                        return false;
                    }

                    ProcessingFee.Text = StoreHelper.GetPriceWithCurrency(Convert.ToDecimal(basketInfo.ProcessingFee), null, BillingCurrency);
                    BookingDeposit.Text = StoreHelper.GetPriceWithCurrency(Convert.ToDecimal(basketInfo.TotalPayableToday), null, BillingCurrency);
                    TotalPayableAccommodation.Text = StoreHelper.GetPriceWithCurrency(Convert.ToDecimal(basketInfo.TotalPayableAtAccommodation), null, BillingCurrency);
                    TotalPrice.Text = StoreHelper.GetPriceWithCurrency(Convert.ToDecimal(basketInfo.TotalPrice), null, BillingCurrency);
                }
                catch (Exception)
                {
                    BasketHelper.ClearBasket();
                    DisplayEmptyMessage();
                    throw new AffinionException(string.Format("Subrates are not configured for selected hotels. OrderID: {0}", orderId));
                }


            }
            return false;
        }


        /// <summary>
        /// Formating price 
        /// </summary>
        private string FormatTotalPrice(string price)
        {
            if (price == string.Empty || price == "")
                return "0.00";
            else if (price.Split('.')[1].Length == 0)
                return (string.Format("{0}.00", price));
            else if (price.Split('.')[1].Length == 1)
                return (string.Format("{0}0", price));
            else
                return price;
        }

        /// <summary>
        /// Remove item click event
        /// </summary>
        protected void lblRemove_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)(sender);
            string skuId = btn.CommandArgument;
            BasketHelper.RemoveCartItemBySkuId(skuId);

            //Rfresh the page
            Response.Redirect(Request.RawUrl);
        }

        /// <summary>
        /// customer Validation Onclick event
        /// </summary>
        protected void reCalculateOnclick(object sender, EventArgs e)
        {
            string paymentPortion = string.Empty;
            string paymentMode = string.Empty;
            if (fullCheckbox.Checked)
            {
                paymentPortion = Constants.PaymentMethod.FullPayment;
            }
            else if (depositCheckBox.Checked)
            {
                paymentPortion = Constants.PaymentMethod.DepositOnly;
            }
            else if (partialCheckBox.Checked)
            {
                paymentPortion = Constants.PaymentMethod.PartialPayment;
            }

            if (creditDebitCheckBox.Checked)
            {
                paymentMode = Constants.PaymentType.CreditCard;
            }
            else if (chequeCheckBox.Checked)
            {
                paymentMode = Constants.PaymentType.Cheque;
            }
            else if (cashCheckBox.Checked)
            {
                paymentMode = Constants.PaymentType.Cash;
            }
            else if (poCheckBox.Checked)
            {
                paymentMode = Constants.PaymentType.PostOffice;
            }
            else if (voucherCheckBox.Checked)
            {
                paymentMode = Constants.PaymentType.Voucher;
            }

            this.SetCheckoutAmount(paymentPortion, paymentMode);
            this.PopulateBasketPaymentDetails();
        }

        /// <summary>
        /// customer Validation Onclick event
        /// </summary>
        protected void customerValidationOnclick(object sender, EventArgs e)
        {
            string paymentPortion = string.Empty;
            string paymentMode = string.Empty;

            SetPaymentMethod(ref paymentPortion, ref paymentMode);
            if ((paymentPortion.Equals(Constants.PaymentMethod.FullPayment) || paymentPortion.Equals(Constants.PaymentMethod.DepositOnly) || paymentPortion.Equals(Constants.PaymentMethod.PartialPayment)) && paymentMode.Equals(Constants.PaymentType.CreditCard))
            {
                this.SetCheckoutAmount(paymentPortion, paymentMode);
                #region LWD-924
                this.showAdditionalDetails.Visible = false;
                string productName = BasketHelper.GetInvalidReservation();
                Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "REALEX: invalid offline product: " + productName + "");
                if (string.IsNullOrEmpty(productName))
                {
                    string redirectUrl = Request.Url.ToString().Replace("http:", "https:");
                    redirectUrl = redirectUrl.Replace(Request.RawUrl, "/paymentgateway");
                    Response.Redirect(redirectUrl, false);
                }

                #endregion
            }

        }

        private void SetPaymentMethod(ref string paymentPortion, ref string paymentMode)
        {
            if (fullCheckbox.Checked)
            {
                paymentPortion = Constants.PaymentMethod.FullPayment;
            }
            else if (depositCheckBox.Checked)
            {
                paymentPortion = Constants.PaymentMethod.DepositOnly;
            }
            else if (partialCheckBox.Checked)
            {
                paymentPortion = Constants.PaymentMethod.PartialPayment;
            }
            if (creditDebitCheckBox.Checked)
            {
                paymentMode = Constants.PaymentType.CreditCard;
            }
            else if (chequeCheckBox.Checked)
            {
                paymentMode = Constants.PaymentType.Cheque;
            }
            else if (cashCheckBox.Checked)
            {
                paymentMode = Constants.PaymentType.Cash;
            }
            else if (poCheckBox.Checked)
            {
                paymentMode = Constants.PaymentType.PostOffice;
            }
            else if (voucherCheckBox.Checked)
            {
                paymentMode = Constants.PaymentType.Voucher;
            }
        }

        /// <summary>
        /// Method to set the checkout amount in Purchase Order and also in Database
        /// </summary>
        /// <param name="paymentPortion">string to determine if the payment portion is deposit or full</param>
        private void SetCheckoutAmount(string paymentPortion, string paymentType)
        {
            UCommerce.EntitiesV2.PurchaseOrder order = UCommerce.Runtime.SiteContext.Current.OrderContext.GetBasket().PurchaseOrder;
            

            int orderId = BasketHelper.GetBasketOrderId();

            if (!string.IsNullOrEmpty(paymentPortion))
                order[OrderPropertyConstants.PaymentMethod] = paymentPortion;
            if (!string.IsNullOrEmpty(paymentType))
                order[OrderPropertyConstants.PaymentType] = paymentType;

            order.Save();
            string clientId = Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.GetBasket().Bookings.FirstOrDefault().ClientId;
            clientId = Guid.Parse(clientId.ToString()).ToString();
            //var clientId = "3d8a574f-933a-4ab8-bf61-370283b38aa2";           
            // var clientId = BasketHelper.GetOrderClientId(orderId);
            var paymentDetails = Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.GetBasket().Bookings;
           // Affinion.LoyaltyBuild.Common.SessionHelper.BasketInfo paymentDetails = BasketHelper.GetPopulateBasketPaymentDetails(orderId, clientId);

            //SubrateService subrateService = new SubrateService();
           // List<OrderLineSubrate> orderLineSubrateList = subrateService.GetOrderSubrate(orderId, clientId);

            decimal checkoutAmount = 0.00m;

            //decimal.TryParse(paymentDetails.TotalPayableToday, out checkoutAmount);

            if (checkoutAmount > 0)
            {
                //Add dynamic order property to an order line. This will be used in Payment Page
                order[Constants.OrderCheckoutAmountColumn] = checkoutAmount.ToString("0.00");
                order.Save();
            }
        }

        /// <summary>
        /// Method to calculate and update the amounts Payable At Accomodation and Booking Deposit at each Orderline level in Database
        /// </summary>
        /// <param name="order">The Purchase Order</param>
        /// <param name="orderId">The order id corresponding to the order line id</param>
        /// <param name="orderLineSubrateList">The subrate list for the order line</param>
        /// <param name="isFullPayement">Boolean value to specify if the payment mode is partial or full</param>
        private void UpdateOrderLineAmounts(UCommerce.EntitiesV2.PurchaseOrder order, int orderId, List<OrderLineSubrate> orderLineSubrateList, bool isFullPayement)
        {
            List<Affinion.LoyaltyBuild.Common.SessionHelper.BasketInfo> basketInfoList = BasketHelper.GetPopulateConfirmationListFromDB(orderId);
            List<int> orderLineIds = order.OrderLines.Select(x => x.OrderLineId).ToList<int>();

            foreach (int orderLineId in orderLineIds)
            {
                Affinion.LoyaltyBuild.Common.SessionHelper.BasketInfo basketInfo = basketInfoList.Where(x => x.OrderLineId.Equals(orderLineId.ToString())).FirstOrDefault();

                decimal orderLineBookingDeposit;
                decimal.TryParse(basketInfo.BookingDeposit, out orderLineBookingDeposit);
                orderLineBookingDeposit = decimal.Round(orderLineBookingDeposit, 2);

                decimal orderlinePayableAtAccomodation;
                decimal.TryParse(basketInfo.PayableAtAccommodation, out orderlinePayableAtAccomodation);
                orderlinePayableAtAccomodation = decimal.Round(orderlinePayableAtAccomodation, 2);

                decimal orderlineProcessingFee;
                decimal.TryParse(basketInfo.ProcessingFee, out orderlineProcessingFee);
                orderlineProcessingFee = decimal.Round(orderlineProcessingFee, 2);

                //Update the subrate amounts in uCommerce_OrderLine Table.
                //uCommerceConnectionFactory.UpdateOrderLineDepositAmount(orderId, orderLineId, orderlinePayableAtAccomodation, orderLineBookingDeposit);
            }

        }

        /// <summary>
        /// Validate the accomodation page data
        /// </summary>
        /// <returns>return true if validation successfull</returns>
        private bool ValidateAccomodation()
        {
            try
            {
                int orderID = BasketHelper.GetBasketOrderId();
                DataSet ordeLineIDs = uCommerceConnectionFactory.GetOrderLines(orderID);

                for (int i = 0; i < ordeLineIDs.Tables[0].Rows.Count; i++)
                {
                    string orderLineID = ordeLineIDs.Tables[0].Rows[i]["OrderLineId"].ToString();
                    DataSet reservationCount = uCommerceConnectionFactory.GetAccomodationDetailsByOrderLineId(orderLineID);
                    string guestName = reservationCount.Tables[0].Rows[0]["GuestName"].ToString();
                    if (guestName.Length <= 0)
                    {
                        //  LabelAccomodation = "Accomodation details are required";
                        return false;
                    }
                }
                return true;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException("Error in validating accomadation", exception.InnerException);
            }
        }


        private void BindSitecoreText()
        {
            Item currentPageItem = Sitecore.Context.Item;
            if (currentPageItem != null)
            {
                LabelAccomodation = SitecoreFieldsHelper.GetItemFieldValue(currentPageItem, "Accomodation Details Required");
                ConfirmationMessage = SitecoreFieldsHelper.GetItemFieldValue(currentPageItem, "ConfirmationMessage");
                GuestCount = SitecoreFieldsHelper.GetItemFieldValue(currentPageItem, "GuestCount");
                RemoveBasketText = SitecoreFieldsHelper.GetItemFieldValue(currentPageItem, "RemoveBasketLabel");
                ProcessingFeeText = SitecoreFieldsHelper.GetItemFieldValue(currentPageItem, "Processing Fee Text");
                TotalPayableTodayText = SitecoreFieldsHelper.GetItemFieldValue(currentPageItem, "Total Payable Today Text");
                TotalPriceText = SitecoreFieldsHelper.GetItemFieldValue(currentPageItem, "Total Price Text");
                AccomodationDetailsRequiredMsg = SitecoreFieldsHelper.GetItemFieldValue(currentPageItem, "Accomodation Details Required");
                SelectPaymentMethodMsg = SitecoreFieldsHelper.GetItemFieldValue(currentPageItem, "Select Payment Method");
                SelectPaymentPortionMsg = SitecoreFieldsHelper.GetItemFieldValue(currentPageItem, "Select  Payment Portion");
                SetAccommodationText = SitecoreFieldsHelper.GetItemFieldValue(currentPageItem, "Set Accommodation Text");
                PaymentText = SitecoreFieldsHelper.GetItemFieldValue(currentPageItem, "Payment Text");
                PaymentMethodText = SitecoreFieldsHelper.GetItemFieldValue(currentPageItem, "Payment Method Text");
                PaymentPortionText = SitecoreFieldsHelper.GetItemFieldValue(currentPageItem, "Payment Portion Text");
            }

        }



    }
}

