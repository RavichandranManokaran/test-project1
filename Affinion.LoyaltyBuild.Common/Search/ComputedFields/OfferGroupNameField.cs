﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SearchHelper.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common.Search
/// Description:           This field will generate and store the offer group/ package name
/// </summary>
namespace Affinion.LoyaltyBuild.Common.Search.ComputedFields
{
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Sitecore.ContentSearch;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using System.Collections.Generic;
    using System.Text;
    using System.Linq;
    using UCommerce.Api;
    using UCommerce.EntitiesV2;
    using System;
    using Affinion.LoyaltyBuild.Common.Instrumentation;

    /// <summary>
    /// This field will generate and store the offer group/ package name
    /// </summary>
    public class OfferGroupNameField : ComputedField
    {
        /// <summary>
        /// Calculates the field value
        /// </summary>
        /// <param name="indexable">The indexable item</param>
        /// <returns>Returns the calculated value</returns>
        public override object ComputeFieldValue(IIndexable indexable)
        {
            try
            {
                Item supplier = ContentDatabase.GetItem(ID.Parse(indexable.Id));
                List<string> offerGroups = new List<string>();
                StringBuilder clients = new StringBuilder();
                List<Item> clientsList = new List<Item>();
                Item clientsFolder = ContentDatabase.GetItemByKey("ClientSetupFolder");

                if (clientsFolder != null)
                {
                    clientsList = clientsFolder.Axes.GetDescendants().Where(i => i.TemplateName == "ClientDetails").ToList<Item>();
                }

                foreach (Item client in clientsList)
                {
                    bool isSupplierSelected = SitecoreFieldsHelper.MultiListContains(client, "AllowedSuppliers", supplier);
                    SupplierData offersList = new SupplierData();

                    if (isSupplierSelected)
                    {
                        int categoryId = SitecoreFieldsHelper.GetInteger(supplier, "UCommerceCategoryID", 0);
                        if (categoryId == 0)
                        {
                            continue;
                        }

                        Category hotel = CatalogLibrary.GetCategory(categoryId);

                        foreach (Product product in hotel.Products)
                        {
                            // TODO: Implement the logic once the ucommerce model finalized
                            // add offer group
                            //if (!offerGroups.Contains(offerGroupName))
                            //{
                            //    offerGroups.Add(offerGroupName);
                            //}
                        }
                    }
                }

                foreach (string offerGroupName in offerGroups)
                {
                    clients.Append(offerGroupName);
                    clients.Append(",");
                }

                return clients.ToString();
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                return string.Empty;
            }
        }
    }
}
