﻿declare @id int
insert into ucommerce_entityui values('Affinion.LoyaltyBuild.UCom.MasterClass.Entities.SeasonFixPriceAward, Affinion.LoyaltyBuild.UCom.MasterClass.Entities','Awards/SeasonFixPriceAwardUi.ascx',1)
set @id = @@identity
insert into uCommerce_EntityUiDescription values(@id,'Season FixPrice Award','Season FixPrice Award','en')

insert into ucommerce_entityui values('Affinion.LoyaltyBuild.UCom.MasterClass.Entities.SeasonUnitDiscountAward, Affinion.LoyaltyBuild.UCom.MasterClass.Entities','Awards/SeasonUnitDiscountAwardUi.ascx',1)
set @id = @@identity
insert into uCommerce_EntityUiDescription values(@id,'Season Unit Discount Award','Season Unit Discount Award','en')

insert into ucommerce_entityui values('Affinion.LoyaltyBuild.UCom.MasterClass.Entities.FreeNightAward, Affinion.LoyaltyBuild.UCom.MasterClass.Entities','Awards/FreeNightAwardUi.ascx',1)
set @id = @@identity
insert into uCommerce_EntityUiDescription values(@id,'Free Night(s) Discount','Free Night(s) Discount','en')

