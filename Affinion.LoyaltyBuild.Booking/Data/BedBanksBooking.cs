﻿using Affinion.LoyaltyBuild.BedBanks.Troika;
using Affinion.LoyaltyBuild.Model.Provider;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using UCommerce.Infrastructure;
using service = Affinion.LoyaltyBuild.Api.Search.ServiceProviders;

namespace Affinion.LoyaltyBuild.Api.Booking.Data
{
    /// <summary>
    /// Bed banks booking
    /// </summary>
    public class BedBanksBooking : Booking
    {
        private List<BedBankCancelCriteria> _cancelCriteria;
        
        /// <summary>
        /// Constructor to load booking info
        /// </summary>
        /// <param name="orderLineId"></param>
        public BedBanksBooking(int orderLineId)
            : base(orderLineId)
        {
        }

        /// <summary>
        /// Property Id
        /// </summary>
        public string PropertyId
        {
            get
            {
                if (this[Constants.PropertyId] != null)
                    return this[Constants.PropertyId];
                else
                    return string.Empty;
            }
        }

        /// <summary>
        /// PreBooking Token
        /// </summary>
        public string PreBookingToken
        {
            get
            {
                if (this[Constants.PreBookingToken] != null)
                    return this[Constants.PreBookingToken];
                else
                    return string.Empty;
            }
        }

        /// <summary>
        /// Arrival Date
        /// </summary>
        public string ArrivalDate
        {
            get
            {
                if (this[Constants.CheckinDate] != null)
                    return this[Constants.CheckinDate];
                else
                    return string.Empty;
            }
        }

        /// <summary>
        /// Duration
        /// </summary>
        public string Duration
        {
            get
            {
                if (this[Constants.Nights] != null)
                    return this[Constants.Nights];
                else
                    return string.Empty;
            }
        }

        /// <summary>
        /// Lead Guest Title
        /// </summary>
        public string LeadGuestTitle
        {
            get
            {
                if (this[Constants.LeadGuestTitle] != null)
                    return this[Constants.LeadGuestTitle];
                else
                    return string.Empty;
            }
        }

        /// <summary>
        /// LeadGuestFirstName
        /// </summary>
        public string LeadGuestFirstName
        {
            get
            {
                if (this[Constants.LeadGuestFirstName] != null)
                    return this[Constants.LeadGuestFirstName];
                else
                    return string.Empty;
            }
        }

        /// <summary>
        /// Duration
        /// </summary>
        public string LeadGuestLastName
        {
            get
            {
                if (this[Constants.LeadGuestLastName] != null)
                    return this[Constants.LeadGuestLastName];
                else
                    return string.Empty;
            }
        }

        /// <summary>
        /// LeadGuestAddress1
        /// </summary>
        public string LeadGuestAddress1
        {
            get
            {
                if (this[Constants.LeadGuestAddress1] != null)
                    return this[Constants.LeadGuestAddress1];
                else
                    return string.Empty;
            }
        }

        /// <summary>
        /// LeadGuestPostcode
        /// </summary>
        public string LeadGuestPostcode
        {
            get
            {
                if (this[Constants.LeadGuestPostcode] != null)
                    return this[Constants.LeadGuestPostcode];
                else
                    return string.Empty;
            }
        }

        /// <summary>
        /// LeadGuestBookingCountryID
        /// </summary>
        public string LeadGuestBookingCountryID
        {
            get
            {
                if (this[Constants.LeadGuestBookingCountryID] != null)
                    return this[Constants.LeadGuestBookingCountryID];
                else
                    return string.Empty;
            }
        }

        /// <summary>
        /// LeadGuestPhone
        /// </summary>
        public string LeadGuestPhone
        {
            get
            {
                if (this[Constants.LeadGuestPhone] != null)
                    return this[Constants.LeadGuestPhone];
                else
                    return string.Empty;
            }
        }

        /// <summary>
        /// LeadGuestEmail
        /// </summary>
        public string LeadGuestEmail
        {
            get
            {
                if (this[Constants.LeadGuestEmail] != null)
                    return this[Constants.LeadGuestEmail];
                else
                    return string.Empty;
            }
        }

        /// <summary>
        /// Special Request
        /// </summary>
        public string Request
        {
            get
            {
                if (this[Constants.Request] != null)
                    return this[Constants.Request];
                else
                    return string.Empty;
            }
        }

        /// <summary>
        /// RoomToken
        /// </summary>
        public string RoomToken
        {
            get
            {
                if (this[Constants.RoomToken] != null)
                    return this[Constants.RoomToken];
                else
                    return string.Empty;
            }
        }

        /// <summary>
        /// IsDirect
        /// </summary>
        public string IsDirect
        {
            get
            {
                if (this[Constants.IsDirect] != null)
                    return this[Constants.IsDirect];
                else
                    return string.Empty;
            }
        }

        /// <summary>
        /// MealBasisId
        /// </summary>
        public string MealBasisId
        {
            get
            {
                if (this[Constants.MealBasisId] != null)
                    return this[Constants.MealBasisId];
                else
                    return string.Empty;
            }
        }

        //Check if this is needed
        /// <summary>
        /// RoomInfo
        /// </summary>
        public string RoomInfo
        {
            get
            {
                if (this[Constants.RoomInfo] != null)
                    return this[Constants.RoomInfo];
                else
                    return string.Empty;
            }
        }

        /// <summary>
        /// OldPrice
        /// </summary>
        public double OldPrice
        {
            get
            {
                if (this[Constants.OldPrice] != null)
                {
                    double old = 0.0;
                    Double.TryParse(this[Constants.OldPrice], out old);
                    return old;
                }
                else
                    return 0.00;
            }
        }

        /// <summary>
        /// Cancel criteria
        /// </summary>
        public List<BedBankCancelCriteria> CancelCriterias
        {
            get
            {
                if (_cancelCriteria == null)
                {
                    var criteria = this[Constants.CancelCriteria];
                    if (criteria != null)
                    {
                        _cancelCriteria = JsonConvert.DeserializeObject<List<BedBankCancelCriteria>>(criteria);
                    }
                }

                return _cancelCriteria;
            }
        }       

        /// <summary>
        /// NoOfAdults Property
        /// </summary>
        public int NoOfAdults
        {
            get
            {
                int noOfAdults = 0;
                int.TryParse(this[Constants.NoOfAdults], out noOfAdults);
                return noOfAdults;
            }
            set
            {
                this[Constants.NoOfAdults] = Convert.ToString(value);
            }
        }

        /// <summary>
        /// NoOfChildren Property
        /// </summary>
        public int NoOfChildren
        {
            get
            {
                int noOfChildren = 0;
                int.TryParse(this[Constants.NoOfChildren], out noOfChildren);
                return noOfChildren;
            }
            set
            {
                this[Constants.NoOfChildren] = Convert.ToString(value);
            }
        }

        /// <summary>
        /// Property Name
        /// </summary>
        public string PropertyName
        {
            get
            {
                if (this[Constants.PropertyName] != null)
                    return this[Constants.PropertyName];
                else
                    return string.Empty;
            }
        }

        /// <summary>
        /// StarRanking Property
        /// </summary>
        public int StarRanking
        {
            get
            {
                int starRanking = 0;
                int.TryParse(this[Constants.StarRanking], out starRanking);
                return starRanking;
            }
            set
            {
                this[Constants.StarRanking] = Convert.ToString(value);
            }
        }

        /// <summary>
        /// ImageUrl Property
        /// </summary>
        public string ImageUrl
        {
            get
            {
                if (this[Constants.ImageUrl] != null)
                    return this[Constants.ImageUrl];
                else
                    return string.Empty;
            }
        }

        /// <summary>
        /// bedbanks provider id
        /// </summary>
        public override string ProviderId
        {
            get
            {
                return this[Constants.PropertyId];
            }
        }

        public List<BedBankErrata> GetErrataFromDB(int orderlineId)
        {
           string errata= TroikaDataController.GetErrataInfo(orderlineId);
           return JsonConvert.DeserializeObject<List<BedBankErrata>>(errata);
        }

        public string BookingReferenceNo
        {
            get
            {
                if (!string.IsNullOrEmpty(this[Constants.BBBookingReferenceId]))
                {
                    return this[Constants.BBBookingReferenceId];
                }
                else
                {
                    return string.Empty;
                }
            }
            set
            {
                this[Constants.BBBookingReferenceId] = value;
            }
        }

        #region Implement Abstract Methods

        /// <summary>
        /// Get product details
        /// </summary>
        /// <returns></returns>
        public override Model.Product.IProduct GetProduct()
        {
           // throw new NotImplementedException();
            return null;
        }

        /// <summary>
        /// Get product details
        /// </summary>
        /// <returns></returns>
        public override Model.Provider.IProvider GetProvider()
        {
            var providerService = ObjectFactory.Instance.Resolve<service.IProviderService>();

            ProviderSearch bbProviderDetails = new ProviderSearch() { ProviderId = this[Constants.PropertyId], ProviderType = ProviderType.BedBanks };
            service.IServiceProvider bbProvider = providerService.GetServiceProvider(ProviderType.BedBanks);
            return bbProvider.GetProviderDetails(bbProviderDetails);
        }

        #endregion
    }
}
