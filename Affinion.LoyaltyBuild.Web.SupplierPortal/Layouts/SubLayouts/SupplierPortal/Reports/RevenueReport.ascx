﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RevenueReport.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Reports.RevenueReport" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register TagPrefix="ajaxToolkit" Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" %>
<script language="javascript" type="text/javascript">
    //No.of Days Difference
    function DateDifference(sender, args) {
        var startDate = Date.parse(document.getElementById('tbStartDate').value);
        var endDate = Date.parse(document.getElementById('tbEndDate').value);
        var timeDiff = endDate - startDate;

        daysDiff = Math.floor(timeDiff / (1000 * 60 * 60 * 24));

        if (daysDiff > 366) {
            args.IsValid = false;
        }
        else {
            args.IsValid = true;
        }
    }
</script>

<style type="text/css">

     #maincontent_0_rvShowRecord_ctl09
       {

           max-height:350px;

       }     


</style>
<div class="supplier-report">
    <div>
        <h3 class="title-text font-weight-bold"> <sc:Text id="RevenueReportTitle" runat="server" /></h3>
    </div>
    <div>
        <sc:Text id="AvailabityReportInformation" runat="server" />
        <asp:Label ID="ReportInstruction" AssociatedControlID="ReportInstruction" runat="server"></asp:Label><br />
    </div>
    <div class="col-xs-12">
        <asp:ValidationSummary ID="valsum" CssClass="alert alert-danger validation-summary" runat="server" ShowValidationErrors="true"  DisplayMode="BulletList" ShowSummary="true" />
    </div>
    <div>
        <asp:UpdatePanel ID="uplRevenueByOffer" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <ajaxToolkit:CalendarExtender ID="dateFromCalenderExtender"
                    runat="server"
                    TargetControlID="txtDatefrom"
                    Animated="false"
                    PopupPosition="BottomLeft"
                    Format="dd/MM/yyyy"
                    PopupButtonID="btnDateFrom"></ajaxToolkit:CalendarExtender>
                <ajaxToolkit:CalendarExtender ID="dateToCalenderExtender"
                    runat="server"
                    TargetControlID="txtDateTo"
                    Animated="false"
                    PopupPosition="BottomLeft"
                    Format="dd/MM/yyyy"
                    PopupButtonID="btnDateTo"></ajaxToolkit:CalendarExtender>
                <div class="row report-filter">
                    <div class="col-xs-10">
                        <div class="row ">
                            <div class="col-xs-3">
                                <sc:Text id="datefromText" runat="server" />
                                <asp:Label ID="lblDateFrom" AssociatedControlID="txtDateFrom" runat="server"></asp:Label><span class="required">*</span>
                            </div>
                            <div class="col-xs-3">
                                <div class="input-group">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Date from is required" CssClass="highligh-error" ControlToValidate="txtDatefrom" runat="server" />
                                    <asp:TextBox CssClass="form-control" ID="txtDateFrom" runat="server"></asp:TextBox>
                                    <div class="input-group-addon">
                                        <asp:LinkButton ID="btnDateFrom" runat="server" CssClass="calendar glyphicon glyphicon-calendar" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <sc:Text id="dateToText" runat="server" />
                                <asp:Label ID="lblDateTo" runat="server"  AssociatedControlID="txtDateTo"></asp:Label><span class="required">*</span>
                            </div>
                            <div class="col-xs-3">
                                <div class="input-group">
                                    <asp:CompareValidator ID="cmpVal2" ControlToCompare="txtDatefrom"  CssClass="highligh-error"
									 ControlToValidate="txtDateTo" Type="Date" Operator="GreaterThanEqual"   
									 ErrorMessage="Start date should be greater than or equal to end date" runat="server"></asp:CompareValidator>
                                    <asp:RequiredFieldValidator ID="RequiredDateTo" runat="server" CssClass="highligh-error" ControlToValidate="txtDateTo" ErrorMessage="Date to is required" ></asp:RequiredFieldValidator>
                                    <asp:CustomValidator ID="cusValDateTo" ErrorMessage="Report can only be viewed for 12 months at a time" CssClass="highligh-error" runat="server" ControlToValidate="txtDateTo" Display="None"></asp:CustomValidator>
                                    <asp:TextBox CssClass="form-control" ID="txtDateTo" runat="server"></asp:TextBox>
                                    <div class="input-group-addon">
                                        <asp:LinkButton ID="btnDateTo" runat="server" CssClass="calendar glyphicon glyphicon-calendar" />
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-xs-3">
                                <sc:Text id="ParterText" runat="server" />
                                <asp:Label ID="lblPartner" runat="server"  AssociatedControlID="ddlPartner"></asp:Label><span class="required">*</span>
                            </div>
                            <div class="col-xs-3">
                                <asp:RequiredFieldValidator ID="partnerRequiredFieldValidator" CssClass="highligh-error" runat="server" ControlToValidate="ddlPartner" 
                                    ErrorMessage="Partner is required" InitialValue="0" ></asp:RequiredFieldValidator>
                                <asp:DropDownList CssClass="form-control" ID="ddlPartner" runat="server" AppendDataBoundItems="false" >
                                    <asp:ListItem Value="0">-Select-</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-xs-3">
                                <sc:Text id="OfferNameText" runat="server" />
                                <asp:Label ID="lblOfferName" runat="server"  AssociatedControlID="ddlOfferName"></asp:Label>

                            </div>
                            <div class="col-xs-3">
                                <asp:DropDownList CssClass="form-control" ID="ddlOfferName" runat="server" AppendDataBoundItems="false">
                                    <asp:ListItem Selected="True" Text="All Offers" Value="0"></asp:ListItem>
                                </asp:DropDownList>

                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-xs-3">

                                <sc:Text id="ProviderText" runat="server" />
                                <asp:Label ID="lblProvider" runat="server" AssociatedControlID="ddlProvider"></asp:Label>
                            </div>
                            <div class="col-xs-3">
                                <asp:DropDownList CssClass="form-control" ID="ddlProvider" runat="server" AutoPostBack="true" AppendDataBoundItems="false" OnSelectedIndexChanged="ddlProvider_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                            <div class="col-xs-3">
                                <sc:Text id="ProviderGroupText" runat="server" />
                                <asp:Label ID="lblProviderGroup" runat="server" AssociatedControlID="ddlProviderGroup"></asp:Label>
                            </div>
                            <div class="col-xs-3">
                                <asp:DropDownList CssClass="form-control" ID="ddlProviderGroup" runat="server" AutoPostBack="true" AppendDataBoundItems="false" OnSelectedIndexChanged="ddlProviderGroup_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-xs-3">
                                <sc:Text id="DateTypeText" runat="server" />
                                <asp:Label ID="lblDateType" runat="server"  AssociatedControlID="ddlDateType"></asp:Label>
                            </div>
                            <div class="col-xs-3">
                                <asp:DropDownList CssClass="form-control" ID="ddlDateType" runat="server">
                                    <asp:ListItem Text="Arrival Date" Value="Arrival" Selected="True">Based on Arrival Date</asp:ListItem>
                                    <asp:ListItem Text="Reservation Date" Value="Reserve" >Based on Reservation Date</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                            <div class="col-xs-3">
                                <sc:Text id="CampaignText" runat="server" />
                                <asp:Label ID="lblCheck" runat="server" AssociatedControlID="BreakdownCheckbox"></asp:Label>
                            </div>
                            <div class="col-xs-3">
                                <asp:CheckBox ID="BreakdownCheckbox" runat="server" />
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-xs-3">
                                <sc:Text id="breakdownRequired" runat="server" />
                                <asp:Label ID="lblBreakdown" runat="server"  AssociatedControlID="rbBreakDown"></asp:Label>
                            </div>
                            <div class="col-xs-3">
                                <asp:RadioButtonList ID="rbBreakDown" runat="server" RepeatDirection="Horizontal" CssClass="radio-list">
                                    <asp:ListItem Text="Overall" Enabled="true" Selected="True"> </asp:ListItem>
                                    <asp:ListItem Text="Monthly" Enabled="true"></asp:ListItem>
                                    <asp:ListItem Text="Weekly" Enabled="true"></asp:ListItem>
                                </asp:RadioButtonList>
                               
                            </div>
                            <div class="col-xs-3">
                               <%-- <asp:Button ID="btnListRevenue" runat="server" Text="List Revenue"  CssClass="btn btn-default" OnClick="btnListRevenue_Click" />--%>
                                <button id="btnListRevenue" runat="server"  CssClass="btn btn-default" OnClick="btnListRevenue_Click">
                                    <sc:Text id="btnLstRevenue" runat="server" />
                                </button>
                            </div>
                            <div class="col-xs-3">
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnListRevenue" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <br />
    <div>
        <rsweb:ReportViewer ID="rvTotalRevenue" runat="server" Width="100%" AsyncRendering="false"></rsweb:ReportViewer>
    </div>

    <div>
        <rsweb:ReportViewer ID="rvListOfRevenue" runat="server" Width="100%" AsyncRendering="false"></rsweb:ReportViewer>
    </div>
</div>
