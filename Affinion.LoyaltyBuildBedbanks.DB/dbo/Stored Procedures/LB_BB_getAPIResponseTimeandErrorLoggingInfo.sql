
/****** Object:  StoredProcedure [dbo].[LB_BB_getAPIResponseTimeandErrorLoggingInfo]    Script Date: 7/19/2016 7:24:17 PM ******/


---- ====================================================
---- Description:	RETURNS MARGIN FOR A PROPERTY BASED ON AVAILABILITY SOURCE
---- Written: RICHARD
---- Created: 2016.04.19
---- ====================================================

CREATE PROCEDURE [dbo].[LB_BB_getAPIResponseTimeandErrorLoggingInfo]
	
	-- Input parameters 
	@pi_datefrom int = 9628,
	@pi_dateto int = 9629,
	@pi_reqType numeric = 1,
	
	--Return parameters
	@po_returnstatus INT OUTPUT,
	@po_returnmsg VARCHAR(200) OUTPUT

	
AS
set nocount on;
BEGIN

	--Assume transaction will fail, set the return variable to -1.
	SET @po_returnstatus = -1;
	SET @po_returnmsg = null;

	
	
	BEGIN TRY
	
	IF (@pi_reqType = 2)
	BEGIN
	Select * from dbo.LB_BB_apiErrorLog
	where DATEDIFF(d,'01/01/1990',datecreated) >= @pi_datefrom 
	and DATEDIFF(d,'01/01/1990',datecreated) < @pi_dateto
	Order by 1 desc
	END 
	ELSE
	BEGIN
	Select * from dbo.LB_BB_apiResponseTime ART
	join LB_BB_availabilityProvider AP on  ART.availabilityProviderID = AP.ID
	where DATEDIFF(d,'01/01/1990',ART.datecreated) >= @pi_datefrom 
	and DATEDIFF(d,'01/01/1990',ART.datecreated) < @pi_dateto
	Order by 1 desc
	END

		      
    END TRY	
	BEGIN CATCH	  
		SET @po_returnstatus = error_number();
		SET @po_returnmsg = error_message();
		RETURN;
	END CATCH
	
	SET @po_returnstatus = 0;
	SET @po_returnmsg = 'Completed successfully.';	
END












GO

