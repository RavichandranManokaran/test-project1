﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Model
{
    public class PromotionUser
    {
        public string UserName { get; set; }

        public string Name { get; set; }

        public string EmailAddress { get; set; }
    }
}
