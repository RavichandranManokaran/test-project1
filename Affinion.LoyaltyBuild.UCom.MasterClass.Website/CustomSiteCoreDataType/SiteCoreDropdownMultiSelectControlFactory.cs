﻿using Affinion.LoyaltyBuild.UCom.MasterClass.Website.Configuration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UCommerce.EntitiesV2;
using UCommerce.EntitiesV2.Definitions;
using UCommerce.Infrastructure.Globalization;
using UCommerce.Presentation.Web.Controls;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.CustomSiteCoreDataType
{
    public class SiteCoreDropdownMultiSelectControlFactory : IControlFactory, IRequireScriptBlock
    {
        private readonly List<string> supportedDataType = new List<string>() {
         CustomDataTypeConstant.HotelTheme,CustomDataTypeConstant.Experience,CustomDataTypeConstant.Client,
         CustomDataTypeConstant.AddOnMultiSelect, CustomDataTypeConstant.Facility,
         CustomDataTypeConstant.ClientMulti, CustomDataTypeConstant.PortalTypeMulti
         
        };
        private readonly ILocalizationContext _localizationContext;


        private string CurrentDataType { get; set; }
        public SiteCoreDropdownMultiSelectControlFactory(ILocalizationContext localizationContext)
        {
            this._localizationContext = localizationContext;
        }
        public string GetScript()
        {
            return "$(function() { $('.uc-multi-select-list').multiselect();});";
        }

        private static string BuildId(IProperty property)
        {
            return (property.GetDefinitionField().Guid + property.CultureCode);
        }

        public System.Web.UI.Control GetControl(IProperty property)
        {
            string str = (property.GetValue() != null) ? property.GetValue().ToString() : "";
            string id = BuildId(property);
            str = str.ToLower();
            List<KeyValuePair<string, string>> values = null;
            string[] selectedClient = str.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);

            switch (CurrentDataType)
            {
                case CustomDataTypeConstant.ClientMulti:
                    values = SiteConfiguration.GetSiteCoreItemWithId(CurrentDataType);
                    break;
                default:
                    var locations = SiteConfiguration.GetSiteCoreItem(CurrentDataType);
                    values = (from enumValue in locations
                              orderby enumValue
                              select new KeyValuePair<string, string>(enumValue.ToLower(), enumValue)).ToList<KeyValuePair<string, string>>();
                    break;
            }            

            return new MultiSelectDropDownList(id, values, selectedClient) { EnableViewState = true, ID = id };
        }

        private string GetDisplayName(DataTypeEnum enumValue)
        {
            string displayName = enumValue.GetDescription(this._localizationContext.CurrentCultureCode).DisplayName;
            if (string.IsNullOrWhiteSpace(displayName))
            {
                displayName = enumValue.GetDescription("en-US").DisplayName;
            }
            if (string.IsNullOrWhiteSpace(displayName))
            {
                displayName = enumValue.Value;
            }
            return displayName;
        }

        public bool Supports(UCommerce.EntitiesV2.DataType dataType)
        {
            CurrentDataType = dataType.DefinitionName;
            return supportedDataType.Any(x => (x.Equals(dataType.DefinitionName)));
        }


    }
}