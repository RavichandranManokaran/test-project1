﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SupplierSetup.Master.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.AdminPortal
/// Description:           Mater layout for NewSupplier.aspx
/// </summary>

using System;
using System.Web;

namespace Affinion.LoyaltyBuild.AdminPortal.sitecore_modules.Shell.Affinion.SupplierSetup
{
    public partial class SupplierSetup : System.Web.UI.MasterPage
    {
        /// <summary>
        /// Page load function 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Response.Redirect("/sitecore/login?returnUrl=" + HttpContext.Current.Request.Path);
            }
        }
    }
}