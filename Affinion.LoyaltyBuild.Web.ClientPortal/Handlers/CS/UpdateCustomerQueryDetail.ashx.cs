﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Customer;
using model = Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;
using Affinion.LoyaltyBuild.Common.Instrumentation;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Handlers.CS
{
    /// <summary>
    /// Summary description for UpdateCustomerQueryDetail
    /// </summary>
    public class UpdateCustomerQueryDetail : IHttpHandler
    {
        private NameValueCollection _query = null;
        private QueryService _queryService;

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                _query = HttpUtility.ParseQueryString(context.Request.Url.Query.ToLower());
                _queryService = new QueryService();

                var serializer = new JavaScriptSerializer();

                var query = new model.CustomerQuery();

                if (HasCreditCardNumber(Convert.ToString(context.Request.Form["Content"])))
                {
                    context.Response.Write("2");
                    return;
                }

                // {ID=2&QueryTypeId=3&IsSolved=true&Content=asd}
                if (context.Request.Form["ID"] != null)
                {
                    query.ID = Convert.ToInt32(context.Request.Form["ID"]);
                }
                if (context.Request.Form["QueryTypeId"] != null)
                {
                    query.QueryTypeId = Convert.ToInt32(context.Request.Form["QueryTypeId"]);
                }
                if (context.Request.Form["IsSolved"] != null)
                {
                    query.IsSolved = Convert.ToBoolean(context.Request.Form["IsSolved"]); 
                }
                if (context.Request.Form["Content"] != null)
                {
                    query.Content = Convert.ToString(context.Request.Form["Content"]); 
                }
                var searchResult = _queryService.UpdateCustomerQuery(query);
                if (searchResult.IsSuccess)
                {
                    context.Response.Write("1");
                }
                else
                {
                    context.Response.Write("0");
                }
            }
            catch (Exception ex)
            {
                context.Response.Write("0");
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, this);
                throw;

            }


        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }


         /// <summary>
        /// Check the credit card no in the text
        /// </summary>
        private bool HasCreditCardNumber(string inputCardNumber)
        {
            bool result = false;

            var input = inputCardNumber.Replace("-", string.Empty).Replace(" ", string.Empty);
            Regex regex = new Regex(@"\d{16}");
            Match match = regex.Match(input);
            if (match.Success)
            {
                return true;
            }
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Request"></param>
        /// <returns></returns>
        private string GetDocumentContents(System.Web.HttpRequest Request)
        {
            string documentContents;

            using (Stream receiveStream = Request.InputStream)
            {
                using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
                {
                    documentContents = readStream.ReadToEnd();
                }
            }

            return documentContents;
        }
    }
}