﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           AffinionException.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Used to log diagnostic data
/// </summary>
#region Using Directives

using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

#endregion

namespace Affinion.LoyaltyBuild.Common.Exceptions
{
    /// <summary>
    /// Depicts an exception from Foundation module
    /// </summary>
    [Serializable]
    public class AffinionException : Exception
    {
        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public AffinionException()
        {
        }

        /// <summary>
        /// Constructor with message parameter  
        /// </summary>
        /// <param name="message">Message of the exception</param>
        public AffinionException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Constructor with message and inner exception
        /// </summary>
        /// <param name="message">Message</param>
        /// <param name="innerException">Inner exception</param>
        public AffinionException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Constructor for serialization
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected AffinionException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
        #endregion

        #region Overriden Methods
        /// <summary>
        /// Returns serialized exception
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
        #endregion
    }
}
