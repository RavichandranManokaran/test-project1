﻿--CREATE PROCEDURE [dbo].[LB_GetPurchaseOrderDetails]( @Id int )
--AS
--BEGIN
--	SELECT TOP 1000 P.PRODUCTID,CR.CATEGORYID, L.* , RR.CheckInDate, RR.CheckOutDate, RR.GuestName
--	FROM [DBO].[UCOMMERCE_ORDERLINE] L
--	INNER JOIN UCOMMERCE_PRODUCT P ON L.SKU = P.SKU
--	INNER JOIN [UCOMMERCE_CATEGORYPRODUCTRELATION] CR ON P.PRODUCTID = CR.PRODUCTID
--	LEFT JOIN  [LB_RoomReservation] RR ON L.OrderLineId = RR.OrderLineId
--	WHERE /*L.SKU = '001001' AND */ L.ORDERID = @Id
--END


CREATE PROCEDURE [dbo].[LB_GetPurchaseOrderDetails]
	( @OrderId int )
AS
BEGIN
	SELECT TOP 1000 P.PRODUCTID,
					CR.CATEGORYID, 
					L.* , 
					BI.CheckInDate, BI.CheckOutDate, 
					OP.[GuestName]
	FROM [DBO].[UCOMMERCE_ORDERLINE] L
	INNER JOIN UCOMMERCE_PRODUCT P ON L.SKU = P.SKU
	INNER JOIN [UCOMMERCE_CATEGORYPRODUCTRELATION] CR ON P.PRODUCTID = CR.PRODUCTID
	--LEFT JOIN  [LB_RoomReservation] RR ON L.OrderLineId = RR.OrderLineId
	LEFT JOIN [LB_BookingInfo] BI ON L.OrderLineId=BI.OrderLineId
	INNER JOIN (
					Select OrderLineId, _GuestName [GuestName]
					from
					(
						Select OrderLineId, [KEY], [Value]
						from uCommerce_OrderProperty
					) Src
					pivot
					(
						max([Value])
						for [Key] in (_GuestName)
					) Piv
				) OP ON BI.OrderLineId=OP.OrderLineId
	WHERE 
	/*L.SKU = '001001' AND */ L.ORDERID = @OrderId
END
