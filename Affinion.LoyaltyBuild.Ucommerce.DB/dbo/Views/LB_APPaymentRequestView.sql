﻿CREATE VIEW [dbo].[LB_APPaymentRequestView]
AS
SELECT DISTINCT
		OP.ClientId,
		PPD.SiteCoreItemGuid [ProviderID],
		'(Booking ID : '+CONVERT(VARCHAR,OL.OrderId)+')' AS BookingID, 
		OL.OrderId,
		SPR.OrderLineId,
		BI.BookingNo AS BookingRefNo,
		OS.Name [BookingStatus],
		CI.Name [Offername],
		BI.CheckInDate,
		BI.CheckOutDate,
		OLS.CreatedOn [BookingDate],
		(C.FirstName+' '+C.LastName) [CustomerName],
		CONVERT(VARCHAR,OP.NoOfAdult) +' Adults' AS [Adults],
		CONVERT(VARCHAR,OP.NoOfChild)+' Children' AS [Children],
		CU.ISOCode [Currency],
		CU.CurrencyId AS [CurrencyId],
		(CASE WHEN OP.RoomType IS NOT NULL THEN OP.RoomType ELSE OP.RoomInfo END) [AccommodationType],
		P.Name [HotelName],
		(CASE WHEN ISNULL(SupplierPaymentId,0)=0 THEN ISNULL(SPR.SuggestedProviderAmount,0) ELSE SP.SuggestedAmount END) AS [SuggestedAmount] ,
		(CASE WHEN CI.Name IS NULL THEN '' ELSE 'Offer Based' END)  [OfferBased],
		PO.CustomerId,
		SPR.DateProviderPaymentRequested [RequestedDate],
		ISNULL(SP.IsPaidToAccounts,0) [PaymentStatus],
		ISNULL(SP.SupplierPaymentId,0) [SupplierPaymentId]
FROM LB_BookingInfo BI
		INNER JOIN ucommerce_OrderLine OL ON BI.OrderLineId=OL.OrderLineId 
		INNER JOIN LB_SupplierPaymentRequests SPR ON OL.OrderLineId=SPR.OrderLineId AND OL.OrderId=SPR.OrderId
		INNER JOIN (
		   SELECT OrderLineId,OrderId, _NoOfAdult [NoOfAdult], _NoOfChild [NoOfChild],_RoomType [RoomType],_ClientId [ClientId],RoomInfo
			FROM 
			(
			  SELECT OrderLineId,OrderId, [key], [value]
			  FROM uCommerce_OrderProperty 
			) src
			PIVOT
			(
			  MAX([value])
			  FOR [key] IN (_NoOfAdult, _NoOfChild,_RoomType,_ClientId,RoomInfo)
			) piv
		) OP ON SPR.OrderId=OP.OrderId AND SPR.OrderLineId=OP.OrderLineId
		INNER JOIN LB_OrderLineStatusAudit OLS ON SPR.OrderLineId=OLS.OrderLineId
		INNER JOIN uCommerce_OrderStatus OS ON OLS.StatusId=OS.OrderStatusId AND OS.OrderStatusId=6
		INNER JOIN uCommerce_PurchaseOrder PO ON OL.OrderId=PO.OrderId
		INNER JOIN uCommerce_Customer C ON PO.CustomerId=C.CustomerId
		INNER JOIN uCommerce_Currency CU ON PO.CurrencyId=CU.CurrencyId
		INNER JOIN Ucommerce_product P ON OL.Sku = P.Sku AND P.ParentProductId IS NULL AND P.VariantSku IS NULL
		INNER JOIN uCommerce_ProductDefinition PD ON P.ProductDefinitionId=PD.ProductDefinitionId AND PD.Name='Hotel'
		INNER JOIN (
		   SELECT ProductId,SiteCoreItemGuid
			FROM 
			(
			  SELECT ProductId,PD.Name [key], PP.value
			  FROM uCommerce_ProductDefinitionField PD 
			  INNER JOIN uCommerce_ProductProperty PP ON PD.ProductDefinitionFieldId=PP.ProductDefinitionFieldId
			  WHERE PD.Name IN ('SiteCoreItemGuid')
			) src
			PIVOT
			(
			  MAX([value])
			  FOR [key] IN (SiteCoreItemGuid)
			) piv
		) PPD ON PPD.ProductId=P.ProductId 
		LEFT JOIN uCommerce_OrderLineCampaignRelation OLCR ON OL.OrderLineId=OLCR.OrderLineId  
		LEFT JOIN ucommerce_campaignitem CI ON OLCR.CampaignItemId=CI.CampaignItemId	
		LEFT JOIN LB_Supplier_Payments SP ON SPR.OrderId=SP.OrderId	AND SPR.OrderLineId=SP.OrderLineId	 
WHERE 
	    BI.BookingNo IS NOT NULL AND
	    PO.CustomerId IS NOT NULL AND
		DATEDIFF(mm, BI.CheckOutDate,GETDATE())<=6 



