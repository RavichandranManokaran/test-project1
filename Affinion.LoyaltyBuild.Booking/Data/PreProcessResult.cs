﻿using System.Collections.Generic;
using UCommerce;

namespace Affinion.LoyaltyBuild.Api.Booking.Data
{
    /// <summary>
    /// Pre process booking result
    /// </summary>
    class PreProcessResult : Dictionary<string, string>
    {
        /// <summary>
        /// Unit price Info
        /// </summary>
        public Money UnitPrice { get; set; }

        /// <summary>
        /// Vat info
        /// </summary>
        public Money Vat { get; set; }
    }
}
