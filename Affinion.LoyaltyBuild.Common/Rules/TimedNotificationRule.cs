﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           TimedNotification.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Code is used to return rule conditions related results.
/// </summary>


#region Using Directives
using Affinion.LoyaltyBuild.Common.Instrumentation;
using System;
using System.Web;
#endregion

namespace Affinion.LoyaltyBuild.Common.Rules
{
    public class TimedNotificationRule : INotificfationRule
    {

        //Get the current HttpContext
        HttpContext context = HttpContext.Current;

        /// <summary>
        /// ths method is implemented from INotification rule. Processes the conditions for the TimedNotificationRule.
        /// </summary>
        /// <returns>rule result</returns>
        public bool ExecuteDisplayRule()
        {

            try
            {
                //Declare a boolean variableto read the returned value from NotificationDisplayed().
                bool timeoutResult = true;

                if (context.Session["NotificationTimeout"] == null)
                {
                    NotificationTimeout();
                    return true;
                }
                else
                {
                    timeoutResult = NotificationDisplayed();

                    return timeoutResult = true ? true : false;
                }
            }
            catch (Exception ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, this);
                throw;
            }
        }


        #region Private Methods
        /// <summary>
        /// Assign the current system date time to the session.
        /// </summary>
        private void NotificationTimeout()
        {
            DateTime currentDateTime = System.DateTime.Now;
            context.Session["NotificationTimeout"] = currentDateTime;

        }

        /// <summary>
        /// Compares the current system date with the session started date + timeout.
        /// </summary>
        /// <returns>result</returns>
        private bool NotificationDisplayed()
        {
            DateTime refreshedTime = System.DateTime.Now;
            DateTime sessionStartingTime = (DateTime)(context.Session["NotificationTimeout"]);
            DateTime sessionStartingTimeWithTimeout = sessionStartingTime.AddMinutes(2);
            int result = DateTime.Compare(sessionStartingTimeWithTimeout, refreshedTime);
            return (result > 0) ? true : false;

        }
        #endregion
    }
}
