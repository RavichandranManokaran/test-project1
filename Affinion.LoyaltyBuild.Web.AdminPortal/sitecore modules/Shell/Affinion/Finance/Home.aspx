﻿<%@ Page Title="" Language="C#" MasterPageFile="~/sitecore modules/Shell/Affinion/Finance/MainLayout.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Affinion.LoyaltyBuild.AdminPortal.sitecore_modules.Shell.Affinion.Finance.Home" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MenuItems" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Title" runat="server">
    Welcome to the Finance Module
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Body" runat="server">
    <a href="/sitecore modules/Shell/Affinion/Finance/HotelPaymentRequest.aspx" >Payment Request</a> <br />
    <a href="/sitecore modules/Shell/Affinion/Finance/ManualHotelPaymentRequest.aspx" >Manual Payment Request</a> <br />
    <a href="/sitecore modules/Shell/Affinion/Finance/HotelDemands.aspx" >Process Payment Requests</a> <br />
    <a href="/sitecore modules/Shell/Affinion/Finance/VATPaymentProcess.aspx" >VAT Payment Process</a> <br />
    <a href="/sitecore modules/Shell/Affinion/Finance/FileManager.aspx" >File Manager</a> <br />
    <a href="/sitecore modules/Shell/Affinion/Finance/SummaryReport.aspx" >Summary Report - AR</a> <br />
    
</asp:Content>
