﻿using Sitecore.Modules.EmailCampaign;
using Sitecore.Modules.EmailCampaign.Recipients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Comms.Mail.EXM
{

    /// <summary>
    /// implemention for abstract class 
    /// </summary>
    public class EXMService : MailService
    {

        /// <summary>
        /// implemention for send mail for EXM
        /// </summary>
        /// <param name="mail">Imail </param>
        public override void SendMail(IMail mail)
        {
            var exmMail = mail as EXMMail;

            RecipientId recipient = null;

            if (exmMail.IsSitecoreUser)
                recipient = GetSiteCoreUser(mail);
            else
                recipient = GetSiteCoreContact(mail);

            new AsyncSendingManager(exmMail.Message).SendStandardMessage(recipient);
        }

        /// <summary>
        /// get Sitecore Contact Id
        /// </summary>
        /// <param name="mail">Imail</param>
        /// <returns>RecipentId</returns>
        private RecipientId GetSiteCoreContact(IMail mail)
        {
            RecipientId recipient = new SitecoreUserName(string.Join(",", mail.Recepients));

            return recipient;
        }

        /// <summary>
        /// Get Sitecore UserId
        /// </summary>
        /// <param name="mail">Imail</param>
        /// <returns>Recepient Id</returns>
        private RecipientId GetSiteCoreUser(IMail mail)
        {
            RecipientId recipient = RecipientRepository.GetDefaultInstance().ResolveRecipientId("xdb:" + (string.Join(",", mail.Recepients)));

            return recipient;
        }
    }
}
