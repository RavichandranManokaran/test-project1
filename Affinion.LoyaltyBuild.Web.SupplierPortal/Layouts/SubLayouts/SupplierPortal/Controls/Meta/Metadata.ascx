﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Metadata.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Meta.Metadata" %>

<meta name="title" content="<%= GetTitle() %>" />
<meta name="description" content="<%= GetDescription() %>" />
