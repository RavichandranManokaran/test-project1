﻿using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using SelectPdf;
using Sitecore.Data;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;


namespace Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.Reports
{
    public partial class ProvisionalBookingReport : System.Web.UI.UserControl
    {
        static List<ProvisionalBooking> result = null;

        
        /// <summary>
        /// PageLoad EVENT
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            BindSiteCoreText();

            //calling BindDropDown
            if (!IsPostBack)
            {
                FillDropdown();
            }

        }





        /// <summary>
        /// Provisional booking button 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        public void btnListProvisional_Click(object sender, EventArgs e)
        {
            ReportsService objBooking = new ReportsService();
            ProvisionalBookingFilter Filters = new ProvisionalBookingFilter();
           
            showBooking.Visible = true;

            if (Filters != null && !String.IsNullOrWhiteSpace(txtDateFrom.Text) && !String.IsNullOrWhiteSpace(txtPartialPayment.Text) && !String.IsNullOrWhiteSpace(txtDateTo.Text))
            {
                Filters.DateFrom = DateTime.Parse(txtDateFrom.Text);
                Filters.DateTo = DateTime.Parse(txtDateTo.Text);
                Filters.partialpayment = Int32.Parse(txtPartialPayment.Text);
                
                Filters.partner = ddlPartner.SelectedValue;
                Filters.reportandorderby = ddlReportAndOrderBy.SelectedValue;

                result = objBooking.GetProvisionalBooking(Filters);

                grdvwProvisional.DataSource = result;
                lblCount.Text = "Total of" + " " + result.Count.ToString() + " " + "record(s) found.";

                grdvwProvisional.DataBind();
            }


        }

        /// <summary>
        /// GridView To Excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void buttonConvertExcel_Click(object sender, EventArgs e)
        {
            ExportGridToExcel();
        }


        /// </summary>
        /// Print To PDF Content
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PrintButton_Click(object sender, EventArgs e)
        {
            //using iTextsharp print method
            //printpdftextsharp();
            //using SelectToPDF print method
            pdfprint();
        }


        /// <summary>
        /// Loading Client Name in dropdown
        /// </summary>
        private void FillDropdown()
        {
            Database contextDb = Sitecore.Context.Database;
           // List<string> ClientITems = new List<string>();
            var ClientITems = new ListItemCollection();
            var items = contextDb.SelectItems(Constants.FilteredClientItems) != null ? contextDb.SelectItems(Constants.FilteredClientItems).ToList() : null;
            if (items != null)
            {
                foreach (var item in items)
                {
                    System.Web.UI.WebControls.ListItem liBox = new System.Web.UI.WebControls.ListItem();
                    liBox.Text = SitecoreFieldsHelper.GetValue(item, "Name");
                    liBox.Value = item.ID.ToString();
                    if (liBox != null)
                    {
                        ClientITems.Add(liBox);
                    }
                    //ClientITems.Add(SitecoreFieldsHelper.GetItemFieldValue(item, "Name"));

                }
                BindDropdown(ddlPartner, ClientITems, "Text", "Value");
                //if (ClientITems != null && ClientITems.Count > 0)
                //{
                //    ddlPartner.DataSource = ClientITems;
                //    ddlPartner.DataBind();
                //}

            }


        }

        /// <summary>
        /// Bind Drop down
        /// </summary>
        /// <param name="ddlControl"></param>
        /// <param name="dataSource"></param>
        /// <param name="text"></param>
        /// <param name="value"></param>
        private void BindDropdown(DropDownList ddlControl, object dataSource, string text, string value)
        {
            ddlControl.DataSource = dataSource;
            ddlControl.DataTextField = text;
            ddlControl.DataValueField = value;
            ddlControl.DataBind();
        }

        /// <summary>
        /// Print Using ItextSharp
        /// </summary>
        private void printpdftextsharp()
        {

            //ExportGridToPdf();

            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=ProvisionalReport.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            HtmlForm hf = new HtmlForm();
            grdvwProvisional.Parent.Controls.Add(hf);
            hf.Attributes["runat"] = "server";
            hf.Controls.Add(grdvwProvisional);
            hf.RenderControl(hw);
            StringReader sr = new StringReader(sw.ToString());
            iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(PageSize.A4, 10f, 250f, 10f, -10f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);

            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();


        }

        /// <summary>
        /// Printing in PDF using SelectToPDF
        /// </summary>
        private void pdfprint()
        {
            //WITHOUT USING THIRD PARTY TOOLS
            /// get html of the page
            TextWriter myWriter = new StringWriter();
            HtmlTextWriter htmlWriter = new HtmlTextWriter(myWriter);
            HtmlForm hf = new HtmlForm();
            grdvwProvisional.Parent.Controls.Add(hf);
            hf.Attributes["runat"] = "server";
            hf.Controls.Add(grdvwProvisional);
            hf.RenderControl(htmlWriter);

            // instantiate a html to pdf converter object
            HtmlToPdf converter = new HtmlToPdf();

            // create a new pdf document converting the html string of the page
            SelectPdf.PdfDocument doc = converter.ConvertHtmlString(myWriter.ToString(), Request.Url.AbsoluteUri);

            // save pdf document
            doc.Save(Response, true, "ProvisionalBookingReport.pdf");


            // close pdf document
            doc.Close();
            Response.Redirect("ProvisionalBookingReport.pdf");

        }


        /// <summary>
        /// Binding HardCoded Values Using Sitecore
        /// </summary>

        private void BindSiteCoreText()
        {

            Item currentItem = Sitecore.Context.Item;
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "ProvisionalBookingText", scProvisional);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "DateFromText", scDateFrom);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "DateToText", scDateTo);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "PartnerText", scPartner);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "ShowBookingConfirmationDate", scShowbookingconfirmation);

            SitecoreFieldsHelper.BindSitecoreText(currentItem, "PartialpaymentDetails", scPartialpayment);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "ListProvisionalButton", scListProvisionalBooking);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "ReportAndOrderBy", scReportorderby);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Please Select", scPleaseselect);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Date From is Required", scDateFromRequired);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Date To is Required", scDateToRequired);

            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Please Select Partner", scPartnerRequired);

            SitecoreFieldsHelper.BindSitecoreText(currentItem, "ListProvisionalButton", scListProvisionalText);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "ListExcelButton", scConvertToExcel);

            SitecoreFieldsHelper.BindSitecoreText(currentItem, "ListPrintButton", scPrint);
            SitecoreFieldsHelper.BindSitecoreText(currentItem, "Greater than Equal", CompareDatesLabel);
        }

        /// <summary>
        /// Creating FileName For Excel
        /// </summary>
        /// <returns></returns>
        private string CreateFileName()
        {
            return "ProvisionalBookingReport" + DateTime.Now.ToString();
        }


        /// <summary>
        /// ExportGridToExcel
        /// </summary>
        private void ExportGridToExcel()
        {
            try
            {
                Response.Clear();
                Response.Buffer = true;
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Charset = string.Empty;
                string FileName = CreateFileName();

                using (StringWriter strwritter = new StringWriter())
                {
                    HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);


                    GridView tempGrid = grdvwProvisional;
                    tempGrid.AllowPaging = false;
                    tempGrid.CssClass = "table table-striped table-bordered";
                    tempGrid.AutoGenerateColumns = false;

                    tempGrid.DataSource = result;
                    tempGrid.DataBind();

                    Page.Form.Controls.Clear();

                    Page.Form.Controls.Add(tempGrid);

                    tempGrid.GridLines = GridLines.Both;
                    tempGrid.HeaderStyle.Font.Bold = true;
                    Page.Form.RenderControl(htmltextwrtter);
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName + ".xls");
                    Response.Write(strwritter.ToString());
                    Response.End();
                }

            }
            catch (Exception ex)
            {
                //Log 
                Diagnostics.WriteException(DiagnosticsCategory.OfflinePortal, ex, this);
                throw;
            }

        }



    }
}




        
        
        





        


        
        
  













        
 