﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Maps
{
    public class FreeNightAwardMap : SubclassMap<FreeNightAward>
    {
        // Methods
        public FreeNightAwardMap()
        {
            base.Table("LB_FreeNightAward");
            base.KeyColumn("FreeNightAwardId");
            base.Map(x => x.FreeNightAwardId).ReadOnly();
            base.Map(x => x.NoOfNight).Not.Nullable();
        }

    }

}
