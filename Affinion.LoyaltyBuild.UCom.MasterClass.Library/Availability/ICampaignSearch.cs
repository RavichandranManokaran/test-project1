﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;

/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Availability
{
    /// <summary>
    /// Interface to define the availabity related actions
    /// </summary>

    public interface ICampaignSearch
    {
        /// <summary>
        /// Gets the GetCampaignDetailBySupplier Information 
        /// </summary>
        /// <param name="supplierId">supplierId</param>
        /// <param name="startDate">startDate</param>
        /// <param name="startDate">endDate</param>
        /// <param name="startDate">clientId</param>
        /// <param name="startDate">includepricing</param>
        /// <returns><list>GetCampaignDetailBySupplier</list></returns>
        List<SupplierInviteDetail> GetCampaignDetailBySupplier(string supplierId, DateTime startDate, DateTime endDate, string clientId = null, bool includepricing = false);


        /// <summary>
        /// ChangeCampaignSubscribe 
        /// </summary>
        /// <param name="supplierId">supplierId</param>
        /// <param name="supplierInviteId">supplierInviteIdk</param>
        /// <param name="isSubscribe">isSubscribe</param>
        /// <returns>bool if Campaign is subscribed</returns>
        bool ChangeCampaignSubscribe(string supplierId, int supplierInviteId, bool isSubscribe,string content);


        /// <summary>
        /// GetCampaignById 
        /// </summary>
        /// <param name="supplierId">supplierId</param>
        /// <param name="campaignItemId">campaignItemId</param>
        /// <param name="supplierInviteId">supplierInviteId</param>
        /// <param name="calculatediscountprice">calculatediscountprice</param>
        /// <param name="calculateoneyear">calculateoneyear</param>
        /// <returns>get offerbyid</returns>
        OfferDetail GetCampaignById(string supplierId, int campaignItemId, int supplierInviteId, bool calculatediscountprice = false, bool calculateoneyear = false);


        /// <summary>
        /// UnSubScribeForOffer 
        /// </summary>
        /// <param name="supplierId">supplierId</param>
        /// <param name="campaignItemId">campaignItemId</param>
        /// <param name="supplierInviteId">supplierInviteId</param>
        /// <param name="reason">reason</param>
        /// <returns>UnSubScribeForOffer</returns>
        bool UnSubScribeForOffer(string supplierId, int campaignItemId, int supplierInviteId, string reason);

        /// <summary>
        /// GetCampaignItemIdBySupplierId 
        /// </summary>
        /// <param name="supplierInviteId">supplierInviteId</param>
        /// <returns>GetCampaignItemIdBySupplierId</returns>
        int GetCampaignItemIdBySupplierId(int supplierInviteId);

        /// <summary>
        /// GetCampaignItemById 
        /// </summary>
        /// <param name="supplierInviteId">campaignItemId</param>
        /// <returns>GetCampaignItemById</returns>
        CampaignDetail GetCampaignItemById(int campaignItemId);


        /// <summary>
        /// GetCampaignItemsBySupplier 
        /// </summary>
        /// <param name="supplierId">supplierId</param>
        /// <returns>GetCampaignItemsBySupplier</returns>
        List<CampaignItem> GetCampaignItemsBySupplier(string supplierId);

        /// <summary>
        /// GetSupplierInviteIdByCampaign 
        /// </summary>
        /// <param name="campaignItemId">campaignItemId</param>
        /// <param name="supplierId">supplierId</param>
        /// <returns>GetSupplierInviteIdByCampaign</returns>

        int GetSupplierInviteIdByCampaign(int campaignItemId, string supplierId);


        /// <summary>
        /// GetOfferPriceByCampaignIds 
        /// </summary>
        /// <param name="supplierId">supplierId</param>
        /// <param name="campaignId"><lsit>campaignId</lsit>campaignId</param>
        /// <returns>GetOfferPriceByCampaignIds</returns>
        List<CampaignOfferDetail> GetOfferPriceByCampaignIds(string supplierId, List<int> campaignId, string clientId);


        CampaignDetail GetCampaignDescriptionById(string supplierId, int campaignItemId, int supplierInviteId);
    }
}
