﻿/// <summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           FilterByExperience.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Search (VS project name)
/// Description:           Filter by Experiences
/// </summary>
namespace Affinion.LoyaltyBuild.Search.SupplierFilters
{
    public class FilterByExperience : ISupplierFilter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterData"></param>
        /// <returns></returns>
        public bool IncludeSupplier(SupplierFilterData filterData)
        {
            if (filterData != null && filterData.SearchTerm != null && filterData.ResultsItem == null || string.IsNullOrEmpty(filterData.ResultsItem.Experiences) || string.IsNullOrEmpty(filterData.SearchTerm.Experiences))
            {
                return true;
            }

            string resultItemExperiences = filterData.ResultsItem.Experiences.ToLowerInvariant();
            string keywordExperiences = filterData.SearchTerm.Experiences.ToLowerInvariant();

            if (keywordExperiences == "all")
            {
                return true;
            }

            return FilterResultContent(keywordExperiences, resultItemExperiences);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="keywordValue"></param>
        /// <param name="resultItemValue"></param>
        /// <returns></returns>
        private bool FilterResultContent(string keywordValue, string resultItemValue)
        {
            foreach (var value in keywordValue.Split(','))
            {
                string currentkeywordFacilities;
                currentkeywordFacilities = value;
                bool flag = (resultItemValue.Contains(currentkeywordFacilities));

                if (!flag)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
