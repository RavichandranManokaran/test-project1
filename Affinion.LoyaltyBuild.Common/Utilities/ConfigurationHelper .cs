﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           ConfigurationHelper.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Configuration Helper Class to read configuration settings in Web.config file
/// </summary>

#region Using Directives
using Affinion.LoyaltyBuild.Common.Exceptions;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using System;
using System.Configuration;
using System.Linq;
using System.Xml;
#endregion

namespace Affinion.LoyaltyBuild.Common.Utilities
{
    public static class ConfigurationHelper
    {
        #region Private Constants

        /// <summary>
        /// Error message for null parameters
        /// </summary>
        private const string ArgumentExceptionMessage = "Invalid arguments provided, key cannot be empty or null";
        /// <summary>
        /// Error message for general exception
        /// </summary>
        private const string GeneralExceptionMessage = "Error occured when reading configuration values";
        /// <summary>
        /// Query to retrieve secure template based items
        /// </summary>
        private const string SecureTemplateQuery = "//*[@@templatename = 'SecureConfiguration']";
        /// <summary>
        /// Query to retrieve non-secure template based items
        /// </summary>
        private const string NonSecureTemplateQuery = "//*[@@templatename = 'GeneralConfiguration']";
        /// <summary>
        /// Config template Name Value List field name
        /// </summary>
        private const string TemplateFieldName = "Value";
        /// <summary>
        /// Master database name
        /// </summary>
        private const string MasterDatabase = "master";

        #endregion

        #region Public Methods

        /// <summary>
        /// Read configuration values from affinion config
        /// </summary>
        /// <param name="key">The name of the <key> node</param>
        /// <returns>Returns the value in value attribute</returns>
        public static string GetConfigValue(string key)
        {
            XmlNode node = Sitecore.Configuration.Factory.GetConfigNode("affinion/key[@name='" + key +"']");
            if (node == null)
            {
                Diagnostics.Trace(DiagnosticsCategory.Common, "Configuration key '" + key + "' was not found in AffinionSettings.config");
                return string.Empty;
            }

            XmlAttribute value = node.Attributes["value"];
            if (value == null)
            {
                Diagnostics.Trace(DiagnosticsCategory.Common, "Attribute 'value' was not found in key '" + key + "' in AffinionSettings.config");
                return string.Empty;
            }

            return value.InnerText;
        }

        /// <summary>
        /// Get configuration values from web.config file or 
        /// returning a default value if the requested value is not available in web.config file
        /// </summary>
        /// <param name="key">Key to get the value</param>
        /// <param name="defaultValue">Value to return if the requested key is not available</param>
        /// <returns>Returns the requested config value or default value</returns>
        public static string GetWebConfigAppSettingValue(string key, string defaultValue)
        {
            /// Throw exception if the provided key is null
            if (String.IsNullOrEmpty(key))
            {
                throw new ArgumentException(ArgumentExceptionMessage);
            }

            try
            {
                /// Read the configuration entry
                string configurationEntry = ConfigurationManager.AppSettings[key];
                /// Return configuration value for the provided key or if the value is unavailable return the default value
                return String.IsNullOrEmpty(configurationEntry) ? defaultValue : configurationEntry.Trim();
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(GeneralExceptionMessage, exception);
            }
        }

        /// <summary>
        /// Get configuration values form Sitecore items
        /// </summary>
        /// <param name="key">Key of the config value</param>
        /// <param name="defaultValue">Value to return if the requested item is not available</param>
        /// <returns>Returns the requested config value or default value</returns>
        public static string GetSitecoreAppSettingValue(string key, string defaultValue)
        {
            try
            {
                /// Get configuration value by providing key
                string ConfigurationSetting = GetConfigurationValue(key);
                /// If requested configuration setting is empty, return default value
                return string.IsNullOrEmpty(ConfigurationSetting) ? defaultValue : ConfigurationSetting;
            }
            catch (ArgumentNullException exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(GeneralExceptionMessage, exception);
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(GeneralExceptionMessage, exception);
            }
        }

        /// <summary>
        /// Get configuration values form Sitecore items
        /// </summary>
        /// <param name="key">Key of the config value</param>
        /// <returns>Returns the requested config value</returns>
        public static string GetSitecoreAppSettingValue(string key)
        {
            try
            {
                /// Get configuration value by providing key
                return GetConfigurationValue(key);
            }
            catch (ArgumentNullException exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(GeneralExceptionMessage, exception);
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(GeneralExceptionMessage, exception);
            }
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Get secure or non-secure configuration value for provided key
        /// </summary>
        /// <param name="key">Key to get config value</param>
        /// <returns>Returns configuration value</returns>
        private static string GetConfigurationValue(string key)
        {
            /// Throw exception if the provided key is null
            if (String.IsNullOrEmpty(key))
            {
                throw new ArgumentException(ArgumentExceptionMessage);
            }

            /// Get the secured configuration value first for the given key
            string configurationSetting = GetSiteCoreConfigurationValue(SecureTemplateQuery, key);

            if (!string.IsNullOrEmpty(configurationSetting))
            {
                /// Decrypt the secured configuration entry and return
                return Cryptography.Decrypt(configurationSetting);
            }
            /// If a secured configuration is not available search for non-secure configuration for given key
            return GetSiteCoreConfigurationValue(NonSecureTemplateQuery, key);
        }

        /// <summary>
        /// Get configuration value from sitecore item
        /// </summary>
        /// <param name="query">Query to get the get items related to configuration template</param>
        /// <param name="key">Key of the configuration value</param>
        /// <returns>Returns configuration value for the given key</returns>
        private static string GetSiteCoreConfigurationValue(string query, string key)
        {
            string configurationValue = string.Empty;
            /// Get the master database reference
            Database masterDB = Factory.GetDatabase(MasterDatabase);

            /// Get the items filtered by template
            Item[] secureTemplateItems = masterDB.SelectItems(query);

            /// If items form configuration template available,
            if (secureTemplateItems.Length > 0)
            {
                /// Get the configuration item using given key
                Item configurationItem = secureTemplateItems.Where(s => s.Name == key).FirstOrDefault();
                if (configurationItem != null)
                {
                    /// Get the configuration value from the item
                    configurationValue = configurationItem.Fields[TemplateFieldName].Value;
                }
            }
            return configurationValue;
        }

        #endregion
    }
}
