﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion Inernational. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           IWizardIterator.cs
/// Sub-system/Module:     ClientSetup(VS project name)
/// Description:           IWizardIterator interface
/// </summary>


#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 
#endregion

namespace Affinion.LoyaltyBuild.Common.Wizard
{
    /// <summary>
    /// Interface defines all the methods needed to traverse the collection. 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IWizardIterator<T>
    {
        #region Interface Functions

        /// <summary>
        /// get the first element
        /// </summary>
        /// <returns></returns>
        T First();

        /// <summary>
        /// move to next element
        /// </summary>
        /// <returns></returns>
        T Next();

        /// <summary>
        /// get the current element
        /// </summary>
        /// <returns></returns>
        T CurrentItem();

        /// <summary>
        /// boolean flag to represent the stateus of completion
        /// </summary>
        /// <returns></returns>
        bool IsDone();

        /// <summary>
        /// use to add new item
        /// </summary>
        /// <param name="item"></param>
        void AddItem(T item); 

        #endregion
    }
}
