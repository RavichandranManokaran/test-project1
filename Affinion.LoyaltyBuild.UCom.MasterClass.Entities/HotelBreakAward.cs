﻿using Affinion.LoyaltyBuild.UCom.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;
using Affinion.LoyaltyBuild.UCom.Common.Extension;


namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class HotelBreakAward : Award, IEntity
    {
        public HotelBreakAward()
        {
        }
        // Fields
        private int? _hashCode;

        // Methods

        public override void Apply(PurchaseOrder order, IList<OrderLine> orderLines)
        {
            if (orderLines != null && orderLines.Any())
            {
                foreach (OrderLine line in orderLines)
                {
                    decimal providerAmount = 0;
                    if (decimal.TryParse(line[BookingOrderConstant.ProviderAmount], out providerAmount))
                    {
                        decimal discountedAmount = this.DiscountAmount;
                        if (this.IsPercentage)
                        {
                            discountedAmount = Math.Round((decimal)(providerAmount * this.DiscountAmount) / 100);
                        }
                        if (line.OrderLineId > 0)
                            order[BookingOrderConstant.ProviderAmountDiscount] = discountedAmount.ToString();
                        Discount discount = this.CreateDiscount(order, orderLines);
                        discount.AmountOffTotal = discountedAmount;
                        order.AddDiscount(discount);
                        discount.AddOrderLine(line);
                        line.AddDiscount(discount);
                    }
                }
            }
        }

        // Methods
        public new static IQueryable<HotelBreakAward> All()
        {
            return GetRepo().Select();
        }

        public override void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<HotelBreakAward, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            HotelBreakAward objA = obj as HotelBreakAward;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<HotelBreakAward, bool>> expression)
        {
            return All().Any<HotelBreakAward>(expression);
        }

        public static IList<HotelBreakAward> Find(Expression<Func<HotelBreakAward, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<HotelBreakAward>();
        }

        public static HotelBreakAward FirstOrDefault(Expression<Func<HotelBreakAward, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<HotelBreakAward>();
        }

        public new static HotelBreakAward Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<HotelBreakAward> GetRepo()
        {
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<HotelBreakAward>>();

        }


        public override void Save()
        {
            try
            {
                GetRepo().Save(this);
            }
            catch
            {

            }
        }

        public static HotelBreakAward SingleOrDefault(Expression<Func<HotelBreakAward, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties

        public virtual int HotelBreakAwardId { get; protected set; }

        public virtual decimal DiscountAmount { get; set; }

        public virtual bool IsPercentage { get; set; }

        public virtual bool OnOrderLine { get; set; }

        public override int Id
        {
            get
            {
                return this.HotelBreakAwardId;
            }
        }

    }

}
