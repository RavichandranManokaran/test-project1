﻿using Affinion.LoyaltyBuild.Api.Search.Data;
using Affinion.LoyaltyBuild.BedBanks.General;
using Sitecore.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Affinion.LoyaltyBuild.Api.Search.Filters
{
    public class FacilitiesFilter : ILoyaltyFilter, ISearchFilter, IBedBankFilter
    {
        private readonly SearchKey _key;

        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="key"></param>
        public FacilitiesFilter(SearchKey key)
        {
            _key = key;
        }

        public void BuildSearch(ref UCom.MasterClass.ExtendedApi.SearchCriteria criteria)
        {
            if (_key.Facilities != null)
            {
                criteria.Facility = _key.Facilities.Replace(",", "|");
            }
        }

        public void BuildbbSearch(ref BBsearchCriteria criteria)
        {
            if (!string.IsNullOrWhiteSpace(_key.Facilities))
            {
                BedBanksSettings bbs = new BedBanksSettings(Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(_key.ClientId)));
                if (bbs!=null)
                {
                    List<BedBanksFacility> bbFacilities = new List<BedBanksFacility>();
                    List<string> facilities = HttpUtility.UrlDecode(_key.Facilities).Split(',').ToList<string>();
                    bbFacilities = bbs.BedBanksFacility.Where(bbfac => facilities.Contains(bbfac.SitecoreFacilityName)).ToList<BedBanksFacility>();
                    List<string> bedBankFacilityId = new List<string>();
                    foreach (var item in bbFacilities)
                    {
                        bedBankFacilityId.Add(item.FacilityId);
                    }
                    criteria.Facility = string.Join("|", bedBankFacilityId.Distinct());    
                }
            }
        }
    }
}
