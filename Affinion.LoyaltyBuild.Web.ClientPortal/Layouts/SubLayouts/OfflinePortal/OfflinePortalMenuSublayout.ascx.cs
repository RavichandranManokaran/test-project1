﻿//<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           OfflinePortalMenuSublayout.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal
/// Description:           Used to Bind data to OfflinePortalMenuSublayout subLayout
/// </summary>


namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    #region Using Directives

    using Affinion.Loyaltybuild.BusinessLogic.Helper;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.Web.ClientPortal.Models;
    using Sitecore.Data.Fields;
    using Sitecore.Data.Items;
    using Sitecore.Links;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Web;
    using System.Web.UI.WebControls;
    using Affinion.LoyaltyBuild.Api.Booking.Helper;

    #endregion

    public partial class OfflinePortalMenuSublayout : BaseSublayout
    {
        #region Public Properties

        private Item currentPageItem;
        List<SubMenu> listSubMenu;//= new List<SubMenu>();
        Sitecore.Data.Fields.CheckboxField checkbox = null;

        /// <summary>
        /// Property used to get navigation item for basket page
        /// </summary>
        public string BasketPageNavigation
        {
            get
            {
                if (Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.BasketCount > 0)
                    return "/Basket";
                else
                    return "#";
            }
        }


        #endregion

        #region Protected Methods

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                currentPageItem = Sitecore.Context.Item;
                /// Clear basket validation
                this.ValidateClearBasket();

                if (this.IsPostBack && this.Request == null &&
                    this.Request.Params["__EVENTTARGET"] == null &&
                    this.Request.Params["__EVENTTARGET"].Contains("Submit"))
                {
                    /// Do not reload the menu bar
                }
                else
                {
                    Item menuItem = this.GetDataSource();
                    if (currentPageItem != null && currentPageItem.Fields["ShowBookingInMenu"] != null)
                    {
                        checkbox = currentPageItem.Fields["ShowBookingInMenu"];
                    }

                    if (menuItem != null)
                    {
                        this.PlaceholderText.Visible = false;
                        var mainMenuItems = SitecoreFieldsHelper.GetMutiListItems(menuItem, "MenuItems");
                        var litsOfMainMenu = new List<Item>();
                        foreach (var item in mainMenuItems)
                        {
                            if (item.Name.ToString().Equals("booking"))
                            {
                                if (checkbox != null && checkbox.Checked)
                                    litsOfMainMenu.Add(item);
                                continue;
                            }
                            else
                            {
                                litsOfMainMenu.Add(item);
                            }
                        }
                        this.MainMenu.DataSource = litsOfMainMenu;
                        MainMenu.ItemDataBound += ItemBound;
                        this.MainMenu.DataBind();
                    }
                }

                /// Load basket item
                this.LoadBasket();

                /// Validate menu visibility
                this.MenuVisibility();
            }
            catch (Exception ex)
            {
                Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Child Repeater Item bound 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        protected void ItemBound(object sender, RepeaterItemEventArgs args)
        {
            try
            {

                if (args != null && (args.Item.ItemType == ListItemType.Item || args.Item.ItemType == ListItemType.AlternatingItem))
                {

                    Item menuItem = args.Item.DataItem as Item;
                    if (menuItem != null)
                    {
                        listSubMenu = new List<SubMenu>();
                        Collection<Item> subMenuItems = SitecoreFieldsHelper.GetMutiListItems(menuItem as Item, "SubMenuItems");
                        Repeater subMenuRepeater = args.Item.FindControl("SubMenu") as Repeater;
                        foreach (Item subMenuItem in subMenuItems)
                        {
                            subMenuFieldsMapping(subMenuItem);

                        }
                        if (listSubMenu != null && listSubMenu.Count > 0)
                        {
                            subMenuRepeater.DataSource = listSubMenu;
                            subMenuRepeater.DataBind();
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, ex.Message);
                throw;
            }
        }



        #endregion

        #region Private Methods
        /// <summary>
        /// Method to load sub menus
        /// </summary>
        protected void subMenuFieldsMapping(Item targetItem)
        {

            if (targetItem != null)
            {

                SubMenu subMenu = new SubMenu();
                subMenu.MenuText = targetItem.Fields["Body"] != null ? targetItem.Fields["Body"].Value : string.Empty;
                Sitecore.Data.Fields.LinkField lnkFld = targetItem.Fields["Link"];
                if (lnkFld != null && lnkFld.TargetItem != null)
                {


                    string lnkUrl = LinkManager.GetItemUrl(lnkFld.TargetItem);


                    if (!String.IsNullOrWhiteSpace(lnkFld.QueryString))
                    {
                        var decodedParameter = HttpUtility.UrlDecode(HttpUtility.UrlDecode(lnkFld.QueryString));
                        //var formattedString= string.Format(decodedParameter, orderLineId);
                        var query = HttpUtility.ParseQueryString(decodedParameter);
                        var queryValues = new List<string>();
                        foreach (var item in query.AllKeys)
                        {
                            queryValues.Add(QueryString(item));
                        }
                        var queryParameters = string.Format(decodedParameter, queryValues.ToArray());

                        var url = string.Concat(lnkUrl + "?", queryParameters);
                        subMenu.RedirectUrl = this.GetUrl(url);// string.Format(lnkUrl, orderLineId);
                    }
                    else
                    {
                        subMenu.RedirectUrl = lnkUrl;
                    }
                    subMenu.Target = lnkFld.Target;
                }
                listSubMenu.Add(subMenu);
            }
        }

        /// <summary>
        /// Method used to load basket item
        /// </summary>
        private void LoadBasket()
        {
            int basketCount = 0;
            Uri pageUri = Request.Url;

            basketCount = Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.BasketCount;

            /// Hide basket count when payment page loads
            if (pageUri.ToString().Contains("paymentgateway") || pageUri.ToString().Contains("BookingConfirmation"))
            {
                basketico.Visible = false;
            }
            else
            {
                basketico.Visible = true;
            }

            if (basketCount > 0)
            {
                this.basketCount.Text = basketCount.ToString();
                this.basketCount1.Text = basketCount.ToString();
            }
            else
            {
                /// Clear the cart when count is 0
                Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.ClearBasket();
                this.basketCount.Text = "0";
                this.basketCount1.Text = "0";
            }
        }

        /// <summary>
        /// Method used to validate clear basket
        /// </summary>
        private void ValidateClearBasket()
        {
            /// Clear basket if user clicks on new booking or trying to navigate to locations item
            string path = Request.Url.AbsolutePath;
            if (string.Equals(path, "/locations"))
            {
                Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.ClearBasket();
            }
        }

        /// <summary>
        /// Method used to validate menu visibility
        /// </summary>
        private void MenuVisibility()
        {
            /// Menu Visibility
            if (Sitecore.Context.User.IsAuthenticated)
            {
                /// Hide menu in password reset
                if (Request.QueryString.HasKey("passreset") && Request.QueryString.Get("passreset").Equals("1"))
                {
                    Menu.Visible = false;
                }
                else
                {
                    Menu.Visible = true;
                }
            }
            else if (Request.QueryString.HasKey("id"))
            {
                Menu.Visible = false;
            }
            else 
            {
                Response.Redirect("/logout");
            }
        }

        #endregion

        protected void btnReset_Click(object sender, EventArgs e)
        {
            if (Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.BasketCount > 0)
            {
                Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.ClearBasket();
            }
            Response.Redirect("/welcome");
        }
    }
}