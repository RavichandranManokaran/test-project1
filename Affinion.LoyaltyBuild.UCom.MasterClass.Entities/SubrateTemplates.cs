﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class SubrateTemplates : IEntity
    {
        private int? _hashCode;

        // Methods
        public static IQueryable<SubrateTemplates> All()
        {
            return GetRepo().Select();
        }

        public virtual void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<SubrateTemplates, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            SubrateTemplates objA = obj as SubrateTemplates;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<SubrateTemplates, bool>> expression)
        {
            return All().Any<SubrateTemplates>(expression);
        }

        public static IList<SubrateTemplates> Find(Expression<Func<SubrateTemplates, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<SubrateTemplates>();
        }

        public static SubrateTemplates FirstOrDefault(Expression<Func<SubrateTemplates, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<SubrateTemplates>();
        }

        public static SubrateTemplates Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<SubrateTemplates> GetRepo()
        {
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<SubrateTemplates>>();
            
        }

        public virtual void Save()
        {
            try
            {
                GetRepo().Save(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static SubrateTemplates SingleOrDefault(Expression<Func<SubrateTemplates, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties

        public virtual int Id
        {
            get
            {
                return this.SubrateTemplateID;
            }
        }



        public virtual int SubrateTemplateID { get; set; }
        public virtual string SubrateTemplateGuid { get; set; }
        public virtual string SubrateTemplateName { get; set; }
        public virtual DateTime CreatedOn { get; set; }
        public virtual DateTime UpdateOn { get; set; }
        public virtual string CreatedBy { get; set; }


        
    }
}
