﻿using Affinion.LoyaltyBuild.SupplierPortal.Reports.Service;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Reports
{
    public partial class RevenueByOffer : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!Page.IsPostBack && !Page.IsCallback)
            //{
            //    BindDropdown1(ddlProvider, ProviderList, "Name", "Id");
            //}
        //    if (Page.IsPostBack)
        //        return;
        //    SqlConnection con;

        //    string Constr = @"Data Source=MD-TRNGEUP09\SAMPLE;Initial Catalog=ReportSitecore_Ucommerce;User ID=sa;Password=1234qwer$";

        //    con = new SqlConnection(Constr);
        //    con.Open();
        //    string Query = "Select * from Ucommerce_Category";
        //    SqlDataAdapter da = new SqlDataAdapter(Query, con);
        //    DataTable dt = new DataTable();
        //    da.Fill(dt);
        //    ddlProvider.DataSource = dt;
        //    ddlProvider.DataBind();
        //    ddlProvider.DataTextField = "Name";
        //    ddlProvider.DataValueField = "Name";
        //    ddlProvider.DataBind();



        //    string Query1 = "Select * from Ucommerce_ProductCatalogGroup";
        //    SqlDataAdapter da1 = new SqlDataAdapter(Query1, con);
        //    DataTable dt1 = new DataTable();
        //    da1.Fill(dt1);
        //    ddlProviderGroup.DataSource = dt1;
        //    ddlProviderGroup.DataBind();
        //    ddlProviderGroup.DataTextField = "Name";
        //    ddlProviderGroup.DataValueField = "Name";
        //    ddlProviderGroup.DataBind();

        //    String Query2 = "Select Name from Ucommerce_CampaignItem";
        //    SqlDataAdapter da2 = new SqlDataAdapter(Query2, con);
        //    DataTable dt2 = new DataTable();
        //    da2.Fill(dt2);
        //    ddlOfferName.DataSource = dt2;
        //    ddlOfferName.DataBind();
        //    ddlOfferName.DataTextField = "Name";
        //    ddlOfferName.DataValueField = "Name";
        //    ddlOfferName.DataBind();

        //}

        //protected void CalenderImageDateFrom_Click(object sender, ImageClickEventArgs e)
        //{

        //    CalendarDateFrom.Visible = true;
        //    this.CalendarDateFrom.Attributes.Add("style", "POSITION: absolute; background:#ffffff ; margin-left:125px;");
        //}

        //protected void CalendarDateFrom_SelectionChanged(object sender, EventArgs e)
        //{
        //    txtDateFrom.Text = CalendarDateFrom.SelectedDate.ToString().Substring(0, 10);
        //    CalendarDateFrom.Visible = false;
        //}

        //protected void CalenderImageDateTo_Click(object sender, ImageClickEventArgs e)
        //{

        //    CalendarDateTo.Visible = true;
        //    this.CalendarDateTo.Attributes.Add("style", "POSITION: absolute; background:#ffffff; margin-left:135px;");
        //}

        //protected void CalendarDateTo_SelectionChanged(object sender, EventArgs e)
        //{
        //    txtDateTo.Text = CalendarDateTo.SelectedDate.ToString().Substring(0, 10);
        //    CalendarDateTo.Visible = false;
        //}

        //protected void ListRevenue_Button_Click(object sender, EventArgs e)
        //{



        //    SqlConnection con;
        //    SqlCommand cmd;
        //    SqlDataAdapter da = new SqlDataAdapter();

        //    DataSet ds = new DataSet(); ;
        //    string Constr = @"Data Source=MD-TRNGEUP09\SAMPLE;Initial Catalog=ReportSitecore_Ucommerce;User ID=sa;Password=1234qwer$";

        //    con = new SqlConnection(Constr);
        //    con.Open();
        //    cmd = new SqlCommand("SupplierReportRevenueByOffer", con);
        //    //cmd.Parameters.AddWithValue("@DateFrom", txtDateFrom.Text);
        //    //cmd.Parameters.AddWithValue("@DateTo", txtDateTo.Text); 
        //    cmd.Parameters.AddWithValue("@Partner", ddlPartner.SelectedValue);
        //    cmd.Parameters.AddWithValue("@OfferName", ddlOfferName.SelectedValue);
        //    cmd.Parameters.AddWithValue("@Provider", ddlProvider.SelectedValue);
        //    cmd.Parameters.AddWithValue("@ProviderGroup", ddlProviderGroup.SelectedValue);
        //    cmd.Parameters.AddWithValue("@DateType", ddlDateType.SelectedValue);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    da.SelectCommand = cmd;
        //    using (SqlDataAdapter da1 = new SqlDataAdapter(cmd))
        //    {

        //        da.Fill(ds);
        //        gvTotalRevenue.DataSource = ds.Tables[0];
        //        gvTotalRevenue.DataBind();


        //        gvListRevenue.DataSource = ds.Tables[1];
        //        gvListRevenue.DataBind();

        //    }
        //    string ddl1 = ddlProvider.SelectedValue;
        //    //ddlProvider.Text = ddlProvider.SelectedValue;
        //    //ddlProviderGroup.Text = ddlProviderGroup.SelectedValue;

        //    divTotalRevenue.Visible = true;
        //    String Total = "Total of {0} record(s) found";
        //    int RecordCount = ds.Tables[0].Rows.Count;
        //    String Records = String.Format(Total, RecordCount);
        //    lblRevenueTotals.Visible = true;
        //    lblTotal.Text = Records;

        //    divListOfRevenue.Visible = true;
        //    String TotalRevenue = "Total of {0} record(s) found";
        //    int Count = ds.Tables[1].Rows.Count;
        //    String TotalRevenueRecords = String.Format(TotalRevenue, Count);
        //    lblListofRevenue.Visible = true;
        //    lblTotalListOfRevenue.Text = TotalRevenueRecords;

        //}

        //protected void linkbtnExportTotalRevenue_Click(object sender, EventArgs e)
        //{
        //    ////Response.ContentType = "application/pdf";
        //    ////Response.AddHeader("content-disposition", "attachment;filename=Total Revenue.pdf");
        //    ////Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //    ////StringWriter sw = new StringWriter();
        //    ////HtmlTextWriter hw = new HtmlTextWriter(sw);
        //    ////HtmlForm frm = new HtmlForm();
        //    ////gvTotalRevenue.Parent.Controls.Add(frm);
        //    ////// .Parent.Controls.Add(frm);
        //    ////frm.Attributes["runat"] = "server";
        //    ////frm.Controls.Add(gvTotalRevenue);
        //    ////frm.RenderControl(hw);
        //    ////StringReader sr = new StringReader(sw.ToString());
        //    ////Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
        //    ////HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
        //    ////PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        //    ////pdfDoc.Open();
        //    ////htmlparser.Parse(sr);
        //    ////pdfDoc.Close();
        //    ////Response.Write(pdfDoc);
        //    ////Response.End(); 


        //    //string attachment = "attachment; filename=Total Revenue.xls";
        //    //Response.ClearContent();
        //    //Response.AddHeader("content-disposition", attachment);
        //    //Response.ContentType = "application/ms-excel";
        //    //StringWriter sw = new StringWriter();
        //    //HtmlTextWriter htw = new HtmlTextWriter(sw);
        //    //HtmlForm frm = new HtmlForm();
        //    //gvTotalRevenue.Parent.Controls.Add(frm);
        //    //frm.Attributes["runat"] = "server";
        //    //frm.Controls.Add(gvTotalRevenue);
        //    //frm.RenderControl(htw);
        //    //Response.Write(sw.ToString());
        //    //Response.End();
        }

        //protected void linkbtnListOfRevenue_Click(object sender, EventArgs e)
        //{

        //}

        protected void btnListRevenue_Click(object sender, EventArgs e)
        {
            var service = new ReportService();
            Assembly assembly = Assembly.GetAssembly(typeof(Affinion.LoyaltyBuild.SupplierPortal.Reports.IReportService));
            DateTime? dateFrom = null;
            DateTime tmpDateFrom;
            if (DateTime.TryParse(txtDateFrom.Text, out tmpDateFrom))
                dateFrom = tmpDateFrom;

            //get date to
            DateTime? dateTo = null;
            DateTime tmpDateTo;
            if (DateTime.TryParse(txtDateTo.Text, out tmpDateTo))
                dateTo = tmpDateTo;

            //get offer id
            int? offerId = null;
            int tmpOfferId;
            if (int.TryParse(ddlOfferName.SelectedValue, out tmpOfferId))
                offerId = tmpOfferId;
            //get provider id
            int? provider = null;
            int tmpProvider;
            if (int.TryParse(ddlProvider.SelectedValue, out tmpProvider))
                provider = tmpProvider;
            //get partner
            string partner = string.IsNullOrWhiteSpace(ddlPartner.SelectedValue) ? null : ddlPartner.SelectedValue;
           



            var objShowRecord = service.GetRevenueByOfferTotalRevenue(dateFrom, dateTo,partner,provider,offerId);
            rvTotalRevenue.ProcessingMode = ProcessingMode.Local;
            rvTotalRevenue.LocalReport.LoadReportDefinition(assembly.GetManifestResourceStream("Affinion.LoyaltyBuild.SupplierPortal.Reports.Reports.RevenueByOfferTotalRevenue.rdlc"));
            var datasource = new ReportDataSource("RevenueByOfferTotalRevenue", objShowRecord);
            rvTotalRevenue.LocalReport.SetParameters(new ReportParameter("RowCount", "10"));
            rvTotalRevenue.LocalReport.DataSources.Clear();
            rvTotalRevenue.LocalReport.DataSources.Add(datasource);

            var objShowRecord1 = service.GetRevenueByOfferListRevenue(dateFrom, dateTo, partner,provider,offerId);
            rvListOfRevenue.ProcessingMode = ProcessingMode.Local;
            rvListOfRevenue.LocalReport.LoadReportDefinition(assembly.GetManifestResourceStream("Affinion.LoyaltyBuild.SupplierPortal.Reports.Reports.RevenueByOfferListRevenue.rdlc"));
            var datasource1 = new ReportDataSource("RevenueByOfferListRevenue", objShowRecord1);
            rvListOfRevenue.LocalReport.SetParameters(new ReportParameter("RowCount", "10"));
            rvListOfRevenue.LocalReport.DataSources.Clear();
            rvListOfRevenue.LocalReport.DataSources.Add(datasource1);
        
        }
        //protected void ddlProvider_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    var offerService = new TestOfferService();

        //    var offers = offerService.GetParticipatingOffersByProvider(int.Parse(ddlProvider.SelectedValue));
        //    BindDropdown1(ddlOfferName, offers, "Name", "Id");
        //    ddlOfferName.Items.Insert(0, new ListItem { Text = "-All Offers-", Value = "0" });
        //}
    }
}