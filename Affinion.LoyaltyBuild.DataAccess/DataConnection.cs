﻿namespace Affinion.LoyaltyBuild.DataAccess
{
    using Affinion.LoyaltyBuild.DataAccess.DatabaseScripts;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web;

    public class DataConnection
    {
        private string coonectionString;

        public virtual string ConnectionString
        {
            get
            {
                return coonectionString;
            }
        }

        public DataConnection()
        {
            coonectionString = ConfigurationManager.ConnectionStrings["UCommerce"].ConnectionString;
        }

        public DataConnection(string connectionStringName)
        {
            coonectionString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
        }

        /// <summary>
        /// Read data from database
        /// </summary>
        /// <param name="data">The data object</param>
        /// <returns>Returns the data object with filled data</returns>
        public DataObject Read(DataObject data)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(this.ConnectionString))
                {
                    SqlCommand command = this.GetCommand(connection, data);

                    data.DataAdapter.SelectCommand = command;
                    data.DataAdapter.Fill(data.Data);

                    command.Connection.Close();
                    command.Dispose();
                }

                HttpContext.Current.Session["SQLErrorCode"] = 0;
                data.Exception = null;
            }
            catch (SqlException sqlException)
            {
                if (data.DatabaseScript != null)
                {
                    if (this.ExecuteDatabaseScripts(sqlException, data.DatabaseScript))
                    {
                        data.Exception = null;
                        this.Read(data);
                    }
                    else
                    {
                        data.Exception = sqlException;
                    }
                }
                else
                {
                    data.Exception = sqlException;
                }
            }
            catch(Exception exception)
            {
                data.Exception = exception;
            }

            return data;
        }

        /// <summary>
        /// Read data from database
        /// </summary>
        /// <param name="data">The data object</param>
        /// <returns>Returns the data object with filled data</returns>
        public DataObject Write(DataObject data)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(this.ConnectionString))
                {
                    SqlCommand command = this.GetCommand(connection, data);

                    command.ExecuteNonQuery();

                    command.Connection.Close();
                    command.Dispose();
                }

                HttpContext.Current.Session["SQLErrorCode"] = 0;
                data.Exception = null;
            }
            catch (SqlException sqlException)
            {
                // Try database scripts on debug mode only
                // this is just to ease the development of developers, so no need to worry about running scripts manually
                if (data.DatabaseScript != null && HttpContext.Current.IsDebuggingEnabled)
                {
                    if(this.ExecuteDatabaseScripts(sqlException, data.DatabaseScript))
                    {
                        data.Exception = null;
                        this.Write(data);
                    }
                    else
                    {
                        data.Exception = sqlException;
                    }
                }
                else
                {
                    data.Exception = sqlException;
                }
            }
            catch (Exception exception)
            {
                data.Exception = exception;
            }

            return data;
        }

        /// <summary>
        /// Get the sql command to be executed
        /// </summary>
        /// <param name="connection">The sql connection</param>
        /// <param name="data">The data to build the command</param>
        /// <returns>Returns the sql command</returns>
        private SqlCommand GetCommand(SqlConnection connection, DataObject data)
        {
            SqlCommand command = new SqlCommand();

            command.Connection = connection;
            command.Connection.Open();
            command.CommandType = data.CommandType;
            command.CommandText = data.Command;

            if (data.CommandType == System.Data.CommandType.StoredProcedure && data.Parameters.Count > 0)
            {
                foreach (string key in data.Parameters.Keys)
                {
                    SqlParameter param = this.GetParameter(key, data.Parameters[key]);
                    command.Parameters.Add(param);

                    // command.Parameters.AddWithValue(key, data.Parameters[key]);
                }
            }

            return command;
        }

        private bool ExecuteDatabaseScripts(SqlException sqlException, IAffinionDatabaseScript script)
        {
            try
            {
                // Run only in debug mode to avoid production issues
                if (!HttpContext.Current.IsDebuggingEnabled)
                {
                    return false;
                }

                int sqlErrorCode = 0;
                if (HttpContext.Current.Session["SQLErrorCode"] != null)
                {
                    int.TryParse(HttpContext.Current.Session["SQLErrorCode"].ToString(), out sqlErrorCode);
                }

                // If error code is present, data script execution failed in the current session
                // so do not attempt again
                if (sqlErrorCode != 0 || script == null)
                {
                    return false;
                }

                // sp not found and table not found handled here
                // https://msdn.microsoft.com/en-us/library/cc645728.aspx
                var code = sqlException.Number;
                if (code == 2812 || code == 208)
                {
                    return script.ExecuteScripts(); ;
                }

                return false;
            }
            catch (SqlException ex)
            {
                HttpContext.Current.Session["SQLErrorCode"] = ex.Number;
                return false;
            }
        }

        /// <summary>
        /// Generates the sql parameter
        /// </summary>
        /// <param name="name">Name of the parameter</param>
        /// <param name="data">Value of the parameter</param>
        /// <returns>Returns the sql parameter</returns>
        private SqlParameter GetParameter(string name, object data)
        {
            SqlParameter param = null;

            // If special handling is required construct the parameter with specific type
            // most of the time generic implementation works
            if(data is DateTime)
            {
                param = new SqlParameter(name, System.Data.SqlDbType.DateTime);
                param.Value = data;
            }
            else
            {
                param = new SqlParameter(name, data);
            }

            return param;
        }
    }
}
