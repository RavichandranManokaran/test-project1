﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           BasePrimaryLayout.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.BusinessLogic (VS project name)
/// Description:           Checks the availability of offers
/// </summary>
namespace Affinion.LoyaltyBuild.BusinessLogic.Search.OfferFilters
{

    /// <summary>
    /// Checks the availability of offers
    /// </summary>
    public class OfferAvailabilityFilter : IOfferFilter
    {
        /// <summary>
        /// Returns true if the offer is valid and fine to show on client page
        /// </summary>
        /// <param name="filterData">Filter meta data</param>
        /// <returns>Returns true to include the offer</returns>
        public bool IncludeOffer(OfferFilterData filterData)
        {
            if (filterData != null && filterData.Offer != null)
            {
                return filterData.Offer.Availability > 0;
            }

            return false;
        }
    }
}
