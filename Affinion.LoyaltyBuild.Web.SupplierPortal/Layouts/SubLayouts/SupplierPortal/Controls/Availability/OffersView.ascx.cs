﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.Availability.SupplierPortal.Controls
{
    public partial class OffersView : System.Web.UI.UserControl
    {
        public string Disable;
        public int CurrentMonth { get; set; }
        public OffersView()
        {
        }

        public OffersView(List<SupplierInviteDetail> currentOffer)
            :this()
        {
            this.currentOffers = currentOffer;
        }

        public List<SupplierInviteDetail> currentOffers { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
           
            this.DataBind();
            //  if(Cu
            checkRoles();
        }

        public void checkRoles()
        {
            string currentUserRole = SitecoreUserProfile.GetItem(UserProfileKeys.currentLoggedinUserRole);
            if (currentUserRole.ToLower().Contains(Constants.accountManagement) || currentUserRole.ToLower().Contains(Constants.marketing) || currentUserRole.ToLower().Contains(Constants.IT) || currentUserRole.ToLower().Contains(Constants.superUser))
            {
                manageOfferDisableButtons.Value = "false";
            }
            else if (currentUserRole.ToLower().Contains(Constants.grpHotelRole) || currentUserRole.ToLower().Contains(Constants.hotelSupervisor) || currentUserRole.ToLower().Contains(Constants.lbAdminRole))
            {
                manageOfferDisableButtons.Value = "false";
            }
            else if (currentUserRole.ToLower().Contains(Constants.finance) || currentUserRole.ToLower().Contains(Constants.hotelRole))
            {
                manageOfferDisableButtons.Value = "true";
            }
        }
    }
}