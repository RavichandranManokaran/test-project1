﻿
using Affinion.LoyaltyBuild.UCom.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;
using UCommerce.Marketing.Targets;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class HotelTypeTarget : Target, IOrderLineTarget, IEntity, ITarget
    {
        private int? _hashCode;

        public virtual bool IsSatisfiedBy(OrderLine orderLine)
        {
            OrderProperty forSearch = orderLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.ForSearch);
            if (forSearch != null)
            {
                OrderProperty hotelType = orderLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.HotelType);
                if (hotelType != null)
                {
                    return this.HotelType.Equals(hotelType.Value, StringComparison.InvariantCultureIgnoreCase);
                }
                return false;
            }
            else
            {
                Product product = Product.All().Where(x => x.Sku == orderLine.Sku).FirstOrDefault();
                ProductProperty property = product[ProductDefinationConstant.HotelType];
                if (property != null && this.HotelType == (property.Value))
                {
                    return true;
                }
            }
            return false;

        }

        // Methods
        public new static IQueryable<HotelTypeTarget> All()
        {
            return GetRepo().Select();
        }

        public override void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<HotelTypeTarget, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            HotelTypeTarget objA = obj as HotelTypeTarget;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<HotelTypeTarget, bool>> expression)
        {
            return All().Any<HotelTypeTarget>(expression);
        }

        public static IList<HotelTypeTarget> Find(Expression<Func<HotelTypeTarget, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<HotelTypeTarget>();
        }

        public static HotelTypeTarget FirstOrDefault(Expression<Func<HotelTypeTarget, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<HotelTypeTarget>();
        }

        public new static HotelTypeTarget Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<HotelTypeTarget> GetRepo()
        {
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<HotelTypeTarget>>();
        }


        public override void Save()
        {
            GetRepo().Save(this);
        }

        public static HotelTypeTarget SingleOrDefault(Expression<Func<HotelTypeTarget, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties
        public override int Id
        {
            get
            {
                return this.HotelTypeTargetId;
            }
        }

        public virtual int HotelTypeTargetId { get; protected set; }

        public virtual string HotelType { get; set; }



    }
}
