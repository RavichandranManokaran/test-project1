﻿CREATE TABLE [dbo].[uCommerce_AmountOffUnitAward] (
    [AmountOffUnitAwardId] INT             NOT NULL,
    [AmountOff]            DECIMAL (18, 2) NOT NULL,
    CONSTRAINT [PK_uCommerce_AmountOffUnitAward] PRIMARY KEY CLUSTERED ([AmountOffUnitAwardId] ASC),
    CONSTRAINT [FK_uCommerce_AmountOffUnitAward_uCommerce_Award] FOREIGN KEY ([AmountOffUnitAwardId]) REFERENCES [dbo].[uCommerce_Award] ([AwardId])
);

