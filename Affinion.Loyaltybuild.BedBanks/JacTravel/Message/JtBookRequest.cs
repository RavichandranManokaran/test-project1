﻿using Affinion.LoyaltyBuild.BedBanks.Troika;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Model;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Message.Interfaces;
using System;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Diagnostics;
using Affinion.LoyaltyBuild.BedBanks.General;
using System.Collections.Specialized;

namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Message
{
    [Serializable()]
    [XmlType(TypeName = "BookRequest")]
    public class JtBookRequest : JtRequest, IJtRequest
    {
        public JtBookingDetails BookingDetails { get; set; }

        //Parameterless Constructor - Needed for XML Serialization
        private JtBookRequest() { }
        BedBanksSettings bbsettings;
        //The real constructor
        public JtBookRequest(string pPropertyID, string pPreBookingToken, string pArrivalDate, string pDuration, string pTitle, string pFirstName, string pLastName, string pAddressLine1, string pPostcode, string pBookingCountryID, string pPhone, string pEmail, string pRequest, string pRoomToken, bool pIsDirect, string pMealBasisID, string pRoomInfo, BedBanksSettings bbs)
        {
            this.bbsettings = bbs;
            JtBookingDetails bookingDetails = new JtBookingDetails
            {
                PropertyID = pPropertyID,
                PreBookingToken = pPreBookingToken,
                ArrivalDate = pArrivalDate,
                Duration = pDuration,
                LeadGuestTitle = pTitle,
                LeadGuestFirstName = pFirstName,
                LeadGuestLastName = pLastName,
                LeadGuestAddress1 = pAddressLine1,
                LeadGuestPostcode = pPostcode,
                LeadGuestPhone = pPhone,
                LeadGuestEmail = pEmail,
               
                Request = pRequest
               
                //CCCardTypeID = "0",
                //CCIssueNumber = "0",
                //CCAmount = "0"
            };

            //Split the rooms info down to an array of rooms
            string[] roomsInfoArray = pRoomInfo.Split(',');

            string numberAdults = roomsInfoArray[0];
            string numberChildren = roomsInfoArray[1];
            string numberInfants = roomsInfoArray[2];

            List<JtChildAge> childrenAges = new List<JtChildAge>();

            if (int.Parse(numberChildren) > 0)
            {
                string[] strChildrenAges = roomsInfoArray[3].Split('/');

                foreach (string ca in strChildrenAges)
                {
                    JtChildAge childAge = new JtChildAge
                    {
                        Age = ca
                    };
                    childrenAges.Add(childAge);
                }
            }

            //CREATE GUESTS
            List<JtGuest> guests = new List<JtGuest>();
            for (int i = 1; i <= int.Parse(numberAdults); i++)
            {
                JtGuest guest = new JtGuest
                {
                    Type = "Adult",
                    Title = "Mr",
                    FirstName = "Loyalty",
                    LastName = "Build"
                };
                guests.Add(guest);
            }

            List<RoomBooking> roomBookings = new List<RoomBooking>();
            RoomBooking roomOne = new RoomBooking
            {
                MealBasisID = pMealBasisID,
                Adults = roomsInfoArray[0],
                Children = roomsInfoArray[1],
                Infants = roomsInfoArray[2],
                ChildAges = childrenAges,
                Guests = guests
            };

            if (pIsDirect)
            {
                roomOne.PropertyRoomTypeID = pRoomToken;
            }
            else {
                roomOne.BookingToken = pRoomToken;
            }

            roomBookings.Add(roomOne);
            bookingDetails.RoomBookings = roomBookings;
            if (bbs.BedBanksActualCurrency != null)
            {
                LoginDetails = JtController.GenerateLoginDetails(int.Parse(bbs.BedBanksActualCurrency.BedBanksCurrencyId), bbs.BedBanksUserName, bbs.BedBanksPassword);
            }
            else
            {
                LoginDetails = JtController.GenerateLoginDetails(int.Parse(bbs.BedBanksDefaultCurrency.BedBanksCurrencyId), bbs.BedBanksUserName, bbs.BedBanksPassword);
            }
            BookingDetails = bookingDetails;
        }

        public IJtResponse Fire()
        {
            JtBookResponse bRes=new JtBookResponse();

            Stopwatch apiTimer = Stopwatch.StartNew();
            string response = JtController.PerformApiRequest(this);
            apiTimer.Stop();

            //READ THE RESULTS
            XmlRootAttribute xRoot = new XmlRootAttribute
            {
                ElementName = "BookResponse",
                IsNullable = true
            };

            XmlSerializer serializer = new XmlSerializer(typeof(JtBookResponse), xRoot);
            
            try
            {
                MemoryStream memStream = new MemoryStream(Encoding.UTF8.GetBytes(response));
                bRes = (JtBookResponse)serializer.Deserialize(memStream);
            }
            catch (Exception exc)
            {
                //throw new Exception(exc.Message);
                NameValueCollection exception = new NameValueCollection();
                exception.Add(Constants.ExceptionMessage, exc.Message);
                exception.Add(Constants.ExceptionFlow, Constants.BookRequest);
                SendExceptionMail(exception, bbsettings.ClientItemId);
                bRes.ReturnStatus = new ReturnStatus() { Exception="API exception"};
                bRes.ExtraInfo = exc.Message;
            }
            
            bRes.ResponseTime = apiTimer.ElapsedMilliseconds;
            TroikaDataController.InsertApiResponseTime(1, "Book", bRes.ResponseTime);

            return bRes;
        }
    }
}
