﻿CREATE PROCEDURE [dbo].[OLD_GetPaymentHistory]
 @orderlineid INT	
AS
BEGIN
	--Payment History Total
	SELECT SUM(OLD.TotalAmount) - Sum(OLP.TotalAmount)  [Total],
		   OLD.CurrencyTypeId [Currency]
	FROM uCommerce_OrderLine  OL
		inner join LB_OrderLineDue OLD ON OLD.OrderLineID = OL.OrderLineId 
		inner join LB_OrderLinePayment OLP ON OLD.CurrencyTypeId = OLP.CurrencyTypeId
	WHERE
		OLD.OrderLineID= @orderlineid
	GROUP BY 
		OLD.CurrencyTypeId

	--PaymentHistory PaymentDues 
	SELECT	OLD.OrderLineDueId [OrderLineDueId],
			OLD.OrderLineID [OrderLineID],
			OLD.CurrencyTypeId [Currency],
			OLD.TotalAmount [Amount],
			PM.Name [Name],
			DT.[Description] ,
			OLD.CreatedDate [CreatedDate],
			OLP.CreatedBy [CreatedBy],
			DT.CanBeDeleted [CanBeDeleted]
	FROM LB_OrderLineDue OLD 
		inner join LB_DueTypes DT ON OLD.DueTypeId=dt.DueTypeId
		left outer join LB_OrderLinePayment OLP ON OLP.OrderLineID = OLD.OrderLineID
		inner join uCommerce_PaymentMethod PM ON PM.PaymentMethodID = OLP.PaymentMethodId
	WHERE 
		OLP.OrderLineID=@orderlineid

	--Payment History Payments
	SELECT	OLP.OrderLinePaymentID [OrderLinePaymentId],
			OLP.OrderLineID [oderLineID],
			PM.Name [Name],
			OLP.TotalAmount [Amount],
			OLP.ReferenceId [ReferenceId],
			OLP.CreatedDate [Createddate],
			OLP.CreatedBy [Createdby]
	FROM LB_OrderLinePayment OLP
		inner join uCommerce_PaymentMethod PM ON PM.PaymentMethodId = OLP.PaymentMethodID
		inner join LB_OrderLineDue OLD ON OLD.OrderLineID = OLP.OrderLineID
	WHERE
		OLD.OrderLineID=@orderlineid
    
END
