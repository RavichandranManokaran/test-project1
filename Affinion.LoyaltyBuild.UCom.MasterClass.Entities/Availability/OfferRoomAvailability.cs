﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Availability
{
    public class OfferRoomAvailability : IEntity
    {
        private int? _hashCode;


        // Methods
        public static IQueryable<OfferRoomAvailability> All()
        {
            return GetRepo().Select();
        }

        public virtual void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<OfferRoomAvailability, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            OfferRoomAvailability objA = obj as OfferRoomAvailability;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<OfferRoomAvailability, bool>> expression)
        {
            return All().Any<OfferRoomAvailability>(expression);
        }

        public static IList<OfferRoomAvailability> Find(Expression<Func<OfferRoomAvailability, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<OfferRoomAvailability>();
        }

        public static OfferRoomAvailability FirstOrDefault(Expression<Func<OfferRoomAvailability, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<OfferRoomAvailability>();
        }

        public static OfferRoomAvailability Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<OfferRoomAvailability> GetRepo()
        {
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<OfferRoomAvailability>>();
        }
     
        public virtual void Save()
        {
            GetRepo().Save(this);
        }

        public static OfferRoomAvailability SingleOrDefault(Expression<Func<OfferRoomAvailability, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

		  
	

        // Properties
        public virtual int Id
        {
            get
            {
                return 0;
            }
        }

        public virtual Guid OfferAvailibilityID { get; set; }
        public virtual string Sku { get; set; }
        public virtual int CampaignItem { get; set; }
        public virtual string VariantSku { get; set; }
        public virtual int NumberAllocated { get; set; }
        public virtual int NumberBooked { get; set; }
        public virtual int DateCounter { get; set; }
        public virtual string CreatedBy { get; set; }
        public virtual string UpdateBy { get; set; }
        public virtual DateTime DateCreated { get; set; }
        public virtual DateTime DateLastUpdate { get; set; }
        public virtual bool IsRestrictedForSale { get; set; }


    }
}
