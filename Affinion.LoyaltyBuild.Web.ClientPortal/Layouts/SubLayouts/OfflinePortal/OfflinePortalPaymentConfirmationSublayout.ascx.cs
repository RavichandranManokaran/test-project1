﻿using Affinion.Loyaltybuild.BusinessLogic.Helper;
using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.Common.Exceptions;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.SessionHelper;
using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Data.Items;
using Sitecore.Resources.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model.Payment;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.Search.Helpers;
using System.Data;
using service = Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Payment;
using Affinion.LoyaltyBuild.Web.ClientPortal.Helper;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Email;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    public partial class OfflinePortalPaymentConfirmationSublayout : System.Web.UI.UserControl
    {
        private service.IPaymentService _paymentService;

        public string ResponseResult { get; set; }
        public string ResponseOrderId { get; set; }
        public string ResponseAmount { get; set; }
        public string CurrentSiteUrl { get; set; }
        public string ResponseOther { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "REALEX: Confirmation page");
                CurrentSiteUrl = Sitecore.Globals.ServerUrl;
                Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "REALEX: cur site url in sub: " + CurrentSiteUrl);

                ///Read response POST data
                ResponseResult = Request.Form["RESULT"];

                if (ResponseResult == null)
                {
                    dangerDiv.Visible = true;
                    return;
                }
                ///Initialize properties using the response parameters
                InitializeResponseProperties();

                if (string.Equals(ResponseResult, "00"))
                {
                    Diagnostics.Trace(DiagnosticsCategory.OfflinePortal, "REALEX: " + ResponseResult + "");
                    int orderId = Convert.ToInt32(ResponseOther);
                    UpdateOrderLineTable(orderId);
                }
                else
                {
                    ///Show error div
                    dangerDiv.Visible = true;
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Initialize response properties
        /// </summary>
        private void InitializeResponseProperties()
        {
            ResponseOrderId = Request.Form["ORDER_ID"];
            ResponseAmount = Request.Form["AMOUNT"];
        }

        /// <summary>
        /// Set currency in ascx
        /// </summary>
        /// <param name="orderId">Current order id</param>
        private List<int> SetCurrency(int orderLineId)
        {
            _paymentService = ContainerFactory.Instance.GetInstance<service.IPaymentService>();
            return (_paymentService.GetCurrencyDetailsbyOrderlineID(orderLineId));
        }

        /// <summary>
        /// Update availability table
        /// </summary>
        private void UpdateOrderLineTable(int orderLineId)
        {
            _paymentService = new service.PaymentService(); ContainerFactory.Instance.GetInstance<service.IPaymentService>();
            CardPaymentDetail pymtdetails = new CardPaymentDetail();
            pymtdetails.OrderLineID = orderLineId;
            List<int> currencydetailList = new List<int>();
            currencydetailList = SetCurrency(orderLineId);
            int currencyIDvalue = 0;
            int exchangeRate = 0;
            if (currencydetailList.Count != 0)
            {
                currencyIDvalue = currencydetailList[0];
                exchangeRate = currencydetailList[1];
            }

            pymtdetails.CurrencyTypeId = currencyIDvalue;
            pymtdetails.Amount = Convert.ToDecimal(Convert.ToDecimal(ResponseAmount) / Convert.ToDecimal(exchangeRate));
            pymtdetails.ProviderAmount = 0;
            pymtdetails.CommissionAmount = 0;
            pymtdetails.ProcessingFee = 0;
            pymtdetails.PaymentMethodID = 11; //For RealexOffline
            pymtdetails.ReferenceID = "test";
            //pymtdetails.CreatedBy = "test";
            pymtdetails.CreatedBy = SitecoreUserProfileHelper.GetItem(UserProfileKeys.CreatedBy);
            List<TotalDueItem> tmpDueItem = new List<TotalDueItem>();
            tmpDueItem = _paymentService.GetDuesByOrderlineID(orderLineId, currencyIDvalue);
            ValidationResponse result = _paymentService.AddPayment(pymtdetails);
            //check paymentdues and generate email
            CheckPaymentDuesRecord(orderLineId, tmpDueItem, result);
        }
        /// <summary>
        /// check paymentdues and generate email
        /// </summary>
        private void CheckPaymentDuesRecord(int orderLineId, List<TotalDueItem> tmpDueItem, ValidationResponse result)
        {
            if (tmpDueItem.Count == 0)
            {
                var ClientId = BasketHelper.GetOrderLineClientId(orderLineId);
                MailService mail = new MailService(ClientId);

                mail.GenerateEmail(MailTypes.CustomerConfirmation, orderLineId);
            }



            if (result.IsSuccess)
            {

                string redirectstring = CurrentSiteUrl + "/PaymentHistory?olid=" + orderLineId.ToString();
                string myScript = string.Empty;
                myScript = "<script type=\"text/javascript\" id=\"EventScriptBlock\">";
                myScript += "if (self !== top ){";
                myScript += "window.top.location.href = " + redirectstring;
                myScript += "}";
                myScript += "</script>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "myKey", myScript, false);
            }
        }



    }
}