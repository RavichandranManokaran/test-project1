﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SuperValuTopNavigationSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.SuperValuTopNavigationSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="row">
    <div class="container">
        <div class="col-xs-12">
            <div class="nav_main">
                <a id="startnavigation" name="startnavigation" class="skip"></a>
                <ul>
                    
                    <sc:Placeholder ID="topnavigationlink" Key="topnavigationlink" runat="server" />

                </ul>
            </div>

        </div>
    </div>
</div>