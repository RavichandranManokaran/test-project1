﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model
{
    public class OracleFileInfo
    {
        public int  OracleFileId { get; set; }
        public string OracleFileName { get; set; }
        public string OracleFileType { get; set; }
        public int OracleFileStatus { get; set; }
        public int IsPostedToOracle { get; set; }
        public string ClosedOutBy { get; set; }
        public DateTime DatePostedToOracle { get; set; }
        public int SequenceNo { get; set; }

    }
}
