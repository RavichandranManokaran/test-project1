﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Payment;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;
using Affinion.LoyaltyBuild.AdminPortal.Extensions;
using Sitecore.Data.Items;
using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.BedBanks.General;

namespace Affinion.LoyaltyBuild.AdminPortal.sitecore_modules.Shell.Affinion.Finance
{
    public partial class VATPaymentProcess : System.Web.UI.Page
    {
        IReportsService iReportsService = new ReportsService();
        SearchCriteria objSearchCriteria = new SearchCriteria();
        protected int TotalOfRecords { get; private set; }
        AROraclePayment objAROraclePayment = null;
        ARInvoice objARInvoice = null;
        private IPaymentService _paymentService;
        private static string _sortDirection = "DESC";
        private static string _sortExpresssion = "HotelID";

        Item[] providers = null;
        Item oracleConfiguration = null;
        string ProviderPath = Constants.ProviderPath;
        Sitecore.Data.Database sitecoreDatabase = Sitecore.Configuration.Factory.GetDatabase("master");

        private static List<ARInvoice> _lbARInvoice = new List<ARInvoice>();
        public static List<ARInvoice> LBARInvoice
        {
            get
            {
                return _lbARInvoice;
            }
            set
            {
                _lbARInvoice = value;
            }

        }
        BedBanksSettings bedBankData = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (LBARInvoice != null)
                    LBARInvoice.Clear();
                BindCurrency();
            }
            this.lblMessage.Visible = false;
        }

        private void GetVATPaymentRequest(string searchData)
        {
            try
            {
                iReportsService = new ReportsService();
                objSearchCriteria = new SearchCriteria();

                string[] dates = this.DateRange.Value.ToString().Split('-');
                objSearchCriteria.DateRange = InputDateFormatting(dates[0]);
                objSearchCriteria.DateUpTo = InputDateFormatting(dates[1]);
                if (!string.IsNullOrEmpty(this.Currency.SelectedValue))
                    objSearchCriteria.Currency = this.Currency.SelectedValue;
                if (!string.IsNullOrEmpty(this.PaymentStatus.SelectedValue))
                    objSearchCriteria.PaymentStatus = int.Parse(this.PaymentStatus.SelectedValue);
                else
                    objSearchCriteria.PaymentStatus = null;
                if (!string.IsNullOrEmpty(searchData))
                    objSearchCriteria.SearchText = searchData.Trim();
                else
                    objSearchCriteria.SearchText = null;

                LBARInvoice = iReportsService.GetVATPaymentRequest(objSearchCriteria);

                SetRecordCount();

                if (LBARInvoice != null && LBARInvoice.Count > 0)
                {
                    LBARInvoice.ToList().ForEach(lst =>
                    {
                        if (!string.IsNullOrEmpty(lst.ProviderID))
                        {
                            providers = sitecoreDatabase.SelectItems(ProviderPath);
                            oracleConfiguration = providers.Where(x => Guid.Parse(x.ID.ToString()).ToString().Equals(Guid.Parse(lst.ProviderID.ToString()).ToString(), StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                            if (oracleConfiguration != null && oracleConfiguration.Fields["OracleIDforVAT"].Value.ToString() != string.Empty)
                            {
                                lst.OracleID = oracleConfiguration.Fields["OracleIDforVAT"].Value.ToString();
                            }
                        }
                        else
                        {
                            oracleConfiguration = sitecoreDatabase.GetItem(Sitecore.Data.ID.Parse(lst.ClientID));
                            if (oracleConfiguration != null)
                            {
                                bedBankData = new BedBanksSettings(oracleConfiguration);
                                if (bedBankData != null && bedBankData.BedBanksOracleConfigurations != null && bedBankData.BedBanksOracleConfigurations.OracleIDforVAT.ToString() != string.Empty)
                                {
                                    lst.OracleID = bedBankData.BedBanksOracleConfigurations.OracleIDforVAT;
                                }
                            }
                        }
                    }
                   );

                }

                this.ResultGridView.DataSource = LBARInvoice.SortList(_sortExpresssion, _sortDirection).ToList();
                this.ResultGridView.DataBind();

                if (LBARInvoice != null && LBARInvoice.Count > 0 && ResultGridView != null && ResultGridView.HeaderRow != null)
                {
                    CheckBox chkSelectAll = (CheckBox)ResultGridView.HeaderRow.FindControl("chkSelectAll");
                    int count = LBARInvoice.Where(lst => lst.PaymentStatus == 0).ToList().Count;
                    if (count == 0)
                    {
                        chkSelectAll.Enabled = false;
                    }
                }
               

            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.AdminPortal, ex, null);
                throw;
            }

        }

        private void SetRecordCount()
        {
            TotalOfRecords = 0;
            lblTotalRecords.Text = "0";
            if (LBARInvoice != null && LBARInvoice.Count > 0)
            {
                TotalOfRecords = LBARInvoice.Count;
                lblTotalRecords.Text = LBARInvoice.Count.ToString();
            }
            else
            {
                LBARInvoice = new List<ARInvoice>();
            }

        }

        private void PersistSupplierPaymentDemands(ARInvoice objARInvoice)
        {
            if (LBARInvoice != null && LBARInvoice.Count > 0)
            {
                LBARInvoice.Where(lst => lst.SupplierVatPaymentId == objARInvoice.SupplierVatPaymentId).ToList().
                    ForEach(lstUpdate =>
                    {
                        lstUpdate.IsChecked = objARInvoice.IsChecked;
                    });
            }
        }

        protected void VATPaymentRequest_Click(object sender, EventArgs e)
        {
            ClearValues();
            GetVATPaymentRequest(string.Empty);
            SearchBox.Text = string.Empty;
        }

        private DateTime InputDateFormatting(string date)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(date))
                {
                    return default(DateTime);
                }
                else
                {
                    DateTime tmpDateTime;
                    try
                    {
                        tmpDateTime = DateTime.Parse(date);
                        return tmpDateTime;
                    }
                    catch
                    {
                        string[] words = date.Split('/');
                        string newstring = words[1] + "/" + words[0] + "/" + words[2];
                        DateTime dt = Convert.ToDateTime(newstring);
                        return dt;
                    }
                }
            }
            catch
            {
                return default(DateTime);
            }

        }

        protected void VATPostToOracle_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (GridViewRow gvRow in this.ResultGridView.Rows)
                {
                    objARInvoice = new ARInvoice();
                    HiddenField hdnSupplierVatPaymentId = (HiddenField)gvRow.FindControl("hdnSupplierVatPaymentId");
                    CheckBox chkSelect = (CheckBox)gvRow.FindControl("chkSelect");

                    if (chkSelect.Checked)
                        objARInvoice.IsChecked = true;
                    else
                        objARInvoice.IsChecked = false;

                    objARInvoice.SupplierVatPaymentId = int.Parse(hdnSupplierVatPaymentId.Value);
                    PersistSupplierPaymentDemands(objARInvoice);

                }
                if (LBARInvoice != null && LBARInvoice.Count > 0)
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = string.Empty;
                    if (LBARInvoice.Where(lst => lst.IsChecked == true).ToList().Count == 0)
                    {
                        lblMessage.Text = "Please select hotel demand details";
                        return;
                    }

                    List<AROraclePayment> LBARInvoicePaymentProcess = new List<AROraclePayment>();

                    foreach (ARInvoice objARInvoice in LBARInvoice)
                    {
                        objAROraclePayment = new AROraclePayment();
                        objAROraclePayment.PostedAmount = objARInvoice.SuggestedAmount;
                        objAROraclePayment.SupplierVatPaymentId = objARInvoice.SupplierVatPaymentId;
                        objAROraclePayment.UpdatedBy = Sitecore.Security.Accounts.User.Current.DisplayName;
                        objAROraclePayment.IsChecked = objARInvoice.IsChecked;
                        objAROraclePayment.OracleID = objARInvoice.OracleID;
                        objAROraclePayment.ProviderID = objARInvoice.ProviderID;
                        objAROraclePayment.ClientID = objARInvoice.ClientID;
                        if (objARInvoice.IsChecked && objARInvoice.PaymentStatus==0)
                        LBARInvoicePaymentProcess.Add(objAROraclePayment);
                    }

                    if (LBARInvoicePaymentProcess != null && LBARInvoicePaymentProcess.Count > 0)
                    {
                        LBARInvoicePaymentProcess.Where(lst => lst.IsChecked == true).ToList().
                        ForEach(lstUpdate =>
                        {
                            if (!string.IsNullOrEmpty(lstUpdate.ProviderID))
                            {
                                providers = sitecoreDatabase.SelectItems(ProviderPath);
                                oracleConfiguration = providers.Where(x => Guid.Parse(x.ID.ToString()).ToString().Equals(Guid.Parse(lstUpdate.ProviderID.ToString()).ToString(), StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                                if (oracleConfiguration != null)
                                {
                                    lstUpdate.NominalCode = oracleConfiguration.Fields["AR Nominal Code"].Value.ToString();
                                    if (!string.IsNullOrEmpty(oracleConfiguration.Fields["AR Campaign Accounting ID"].Value))
                                        lstUpdate.CampaignAccountingID = int.Parse(oracleConfiguration.Fields["AR Campaign Accounting ID"].Value.ToString());
                                    lstUpdate.CustomerAddressCode = oracleConfiguration.Fields["Customer Address Code"].Value.ToString();
                                    lstUpdate.LineNumber = 1;
                                    if (!string.IsNullOrEmpty(oracleConfiguration.Fields["AR Invoice ID"].Value))
                                        lstUpdate.InvoiceId = int.Parse(oracleConfiguration.Fields["AR Invoice ID"].Value.ToString());
                                }
                            }
                            else
                            {
                                oracleConfiguration = sitecoreDatabase.GetItem(Sitecore.Data.ID.Parse(lstUpdate.ClientID));
                                if (oracleConfiguration != null)
                                {
                                    bedBankData = new BedBanksSettings(oracleConfiguration);
                                    if (bedBankData != null && bedBankData.BedBanksOracleConfigurations != null)
                                    {
                                        lstUpdate.NominalCode = bedBankData.BedBanksOracleConfigurations.ArNominalCode;
                                        if (!string.IsNullOrEmpty(bedBankData.BedBanksOracleConfigurations.ArCampaignAccountingId))
                                            lstUpdate.CampaignAccountingID = int.Parse(bedBankData.BedBanksOracleConfigurations.ArCampaignAccountingId);
                                        lstUpdate.CustomerAddressCode = bedBankData.BedBanksOracleConfigurations.CustomerAddressCode;
                                        lstUpdate.LineNumber = 1;
                                        if (!string.IsNullOrEmpty(bedBankData.BedBanksOracleConfigurations.ArInvoiceId))
                                            lstUpdate.InvoiceId = int.Parse(bedBankData.BedBanksOracleConfigurations.ArInvoiceId);
                                    }
                                }
                            }
                        });

                        List<AROraclePayment> lstAROraclePayment = new List<AROraclePayment>();
                        lstAROraclePayment = LBARInvoicePaymentProcess.Where(lst => lst.IsChecked == true && !string.IsNullOrEmpty(lst.OracleID)).ToList();
                        iReportsService = new ReportsService();
                        ValidationResponse objValidationResponse = iReportsService.ARPaymentRequest(lstAROraclePayment);

                        if (objValidationResponse.IsSuccess)
                        {
                            this.SearchBox.Text = string.Empty;
                            ClearValues();
                            GetVATPaymentRequest(string.Empty);
                            

                        }
                        lblMessage.Text =  objValidationResponse.Message.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.AdminPortal, ex, null);
                throw;
            }

        }

        private void BindCurrency()
        {
            _paymentService = ContainerFactory.Instance.GetInstance<IPaymentService>();
            List<CurrencyDetail> currencyList = new List<CurrencyDetail>();
            currencyList = _paymentService.GetCurrencyListDetails();
            Currency.DataSource = currencyList;
            Currency.DataTextField = "Name";
            Currency.DataValueField = "Name";
            Currency.DataBind();
            Currency.Items.Insert(0, new ListItem { Text = "All", Value = "" });
        }

        protected void ResultGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HiddenField hdnSupplierVatPaymentId = (HiddenField)e.Row.FindControl("hdnSupplierVatPaymentId");
                CheckBox chkSelect = (CheckBox)e.Row.FindControl("chkSelect");
                ImageButton btnVATPaymentDelete = (ImageButton)e.Row.FindControl("btnVATPaymentDelete");
                Label lblPaidInOracle = (Label)e.Row.FindControl("lblPaidInOracle");
                Label lblDatePostedToOracle = (Label)e.Row.FindControl("lblDatePostedToOracle");
                Label lblPostedAmount = (Label)e.Row.FindControl("lblPostedAmount");
                Label lblOracleID = (Label)e.Row.FindControl("lblOracleID");
                Label lblOracleFileID = (Label)e.Row.FindControl("lblOracleFileID");
                
                if (string.IsNullOrEmpty(lblOracleID.Text))
                {
                    lblOracleID.Text = "Oracle VATID is </br>missing";
                    lblOracleID.ForeColor = System.Drawing.Color.Red;
                    chkSelect.Checked = false;
                    chkSelect.Enabled = false;
                }

                if (string.IsNullOrEmpty(lblOracleFileID.Text))
                {
                    lblOracleFileID.Text = "N/A";
                }

                if (lblPaidInOracle.Text == "0")
                {
                    int count = 0;
                    if (LBARInvoice != null && LBARInvoice.Count > 0)
                    {
                        count = LBARInvoice.Where(lst => lst.SupplierVatPaymentId == int.Parse(hdnSupplierVatPaymentId.Value) && lst.IsChecked == true).ToList().Count;
                    }

                    if (count > 0)
                    {
                        if (chkSelect.Enabled)
                        {
                            chkSelect.Checked = true;
                        }

                    }
                    lblPaidInOracle.Text = "NOT PAID";
                    if (!Sitecore.Security.Accounts.User.Current.DisplayName.ToLower().ToString().Contains("admin"))
                    {
                        btnVATPaymentDelete.Enabled = false;
                    }
                }
                else
                {
                    if (lblPaidInOracle.Text == "1")
                        lblPaidInOracle.Text = "PAID";
                    else
                        lblPaidInOracle.Text = "PENDING";

                    chkSelect.Checked = false;
                    chkSelect.Attributes.Add("style", "display:none");
                    btnVATPaymentDelete.Visible = false;
                }

                

                if (lblPostedAmount.Text=="0")
                {
                    lblPostedAmount.Text = "NOT POSTED YET";
                   
                }

                DateTime postedDate = InputDateFormatting(lblDatePostedToOracle.Text);
                if (postedDate == DateTime.MinValue)
                {
                    lblDatePostedToOracle.Text = "NOT POSTED </br>YET";
                }


            }
        }

        protected void ResultGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            foreach (GridViewRow row in ResultGridView.Rows)
            {
                objARInvoice = new ARInvoice();
                var chkBox = row.FindControl("chkSelect") as CheckBox;
                HiddenField hdnSupplierVatPaymentId = (HiddenField)row.FindControl("hdnSupplierVatPaymentId");

                if (chkBox.Checked)
                {
                    objARInvoice.IsChecked = true;
                }
                else
                {
                    objARInvoice.IsChecked = false;
                }

                objARInvoice.SupplierVatPaymentId = int.Parse(hdnSupplierVatPaymentId.Value);
                PersistSupplierPaymentDemands(objARInvoice);
            }
            ResultGridView.PageIndex = e.NewPageIndex;
            SetRecordCount();
            this.ResultGridView.DataSource = LBARInvoice.SortList(_sortExpresssion, _sortDirection).ToList();
            this.ResultGridView.DataBind();

            if (LBARInvoice != null && LBARInvoice.Count > 0 && ResultGridView != null && ResultGridView.HeaderRow != null)
            {
                CheckBox chkSelectAll = (CheckBox)ResultGridView.HeaderRow.FindControl("chkSelectAll");
                if (hdnSelectAll.Value == "true")
                {
                    chkSelectAll.Checked = true;
                }
                else
                {
                    chkSelectAll.Checked = false;
                }
            }

        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            try
            {
                ClearValues();
                GetVATPaymentRequest(SearchBox.Text);
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.AdminPortal, ex, null);
                throw;
            }

        }
        public void ClearValues()
        {
            hdnSelect.Value = "false";
            hdnSelectAll.Value = "false";
            LBARInvoice.Clear();
        }

        protected void btnVATPaymentDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton button = sender as ImageButton;
            GridViewRow gvrow = button.NamingContainer as GridViewRow;
            HiddenField hdnSupplierVatPaymentId = (HiddenField)gvrow.FindControl("hdnSupplierVatPaymentId");
            int SupplierVatPaymentId = 0;

            if (!string.IsNullOrEmpty(hdnSupplierVatPaymentId.Value))
                SupplierVatPaymentId = (Convert.ToInt32(hdnSupplierVatPaymentId.Value));
           
            try
            {
                ValidationResponse objValidationResponse = iReportsService.DeleteVATPayment(SupplierVatPaymentId);

                if (objValidationResponse.IsSuccess)
                {
                    ClearValues();
                    GetVATPaymentRequest(string.Empty);

                }
                lblMessage.Visible = true;
                lblMessage.Text = objValidationResponse.Message.ToString();
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.AdminPortal, ex, null);
                throw;
            }
        }

        [System.Web.Services.WebMethod]
        public static bool UpdateSelectedCheck(string SelectMode, bool Status, Int32 SupplierVatPaymentId)
        {
            bool isSuccess = false;
            if (SelectMode == "SelectAll")
            {
                if (LBARInvoice != null && LBARInvoice.Count > 0)
                {
                    if (Status)
                    {
                        LBARInvoice.ToList().ForEach(lst => { lst.IsChecked = true; });
                    }
                    else
                    {
                        LBARInvoice.ToList().ForEach(lst => { lst.IsChecked = false; });
                    }
                }
            }
            else if (SelectMode == "Select" && SupplierVatPaymentId > 0)
            {
                if (LBARInvoice != null && LBARInvoice.Count > 0)
                {
                    if (Status)
                    {
                        LBARInvoice.Where(lst => lst.SupplierVatPaymentId == SupplierVatPaymentId).ToList().ForEach(lst => { lst.IsChecked = true; });
                    }
                    else
                    {
                        LBARInvoice.Where(lst => lst.SupplierVatPaymentId == SupplierVatPaymentId).ToList().ForEach(lst => { lst.IsChecked = false; });
                    }
                }
            }

            if (LBARInvoice != null && LBARInvoice.Count > 0)
            {
                int count = 0;
                count = LBARInvoice.Where(lst => lst.IsChecked == true).ToList().Count;
                if (count > 0)
                {
                    isSuccess = true;
                }
            }

            return isSuccess;
        }

        protected void ResultGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            SetSortDirection(_sortDirection);
            if (e != null && e.SortExpression != null)
                _sortExpresssion = e.SortExpression.ToString();

            if (LBARInvoice != null && LBARInvoice.Count > 0)
            {
                SetRecordCount();
                this.ResultGridView.DataSource = LBARInvoice.SortList(_sortExpresssion, _sortDirection).ToList();
                this.ResultGridView.DataBind();
            }
            else
            {
                this.ResultGridView.DataSource = LBARInvoice;
                this.ResultGridView.DataBind();
            }

        }

        protected void SetSortDirection(string sortDirection)
        {
            if (sortDirection == "ASC")
            {
                _sortDirection = "DESC";
            }
            else
            {
                _sortDirection = "ASC";
            }
        }
    
    }
}