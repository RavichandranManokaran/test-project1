﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Affinion.LoyaltyBuild.Common;
using UCommerce.Infrastructure;
using service = Affinion.LoyaltyBuild.Api.Search.ServiceProviders;
using Affinion.LoyaltyBuild.Model.Provider;
using Affinion.LoyaltyBuild.Model.Product;

namespace Affinion.LoyaltyBuild.Api.Booking.Data
{
    /// <summary>
    /// Room Booking
    /// </summary>
    public class RoomBooking : Booking
    {
        /// <summary>
        /// Constructor to load booking info
        /// </summary>
        /// <param name="orderLineId"></param>
        public RoomBooking(int orderLineId)
            : base(orderLineId)
        {
        }

        /// <summary>
        /// No of nights
        /// </summary>
        public int NoOfNights
        {
            get
            {
                int noOfNights = 0;
                int.TryParse(this[Constants.Nights], out noOfNights);
                return noOfNights;
            }
            set
            {
                this[Constants.Nights] = Convert.ToString(value);
            }
        }
        
        /// <summary>
        /// GuestName Property
        /// </summary>
        public string GuestName
        {
            get
            {
                if (!string.IsNullOrEmpty(this[Constants.GuestName]))
                    return this[Constants.GuestName];
                else
                    return string.Empty;
            }
            set
            {
                this[Constants.GuestName] = value;
            }
        }

        /// <summary>
        /// GuestName Property
        /// </summary>
        public string Destination
        {
            get
            {
                if (!string.IsNullOrEmpty(this[Constants.Destination]))
                    return this[Constants.Destination];
                else
                    return string.Empty;
            }
            set
            {
                this[Constants.Destination] = value;
            }
        }

        /// <summary>
        /// NoOfAdults Property
        /// </summary>
        public int NoOfAdults
        {
            get
            {
                int noOfAdults = 0;
                int.TryParse(this[Constants.NoOfAdults], out noOfAdults);
                return noOfAdults;
            }
            set
            {
                this[Constants.NoOfAdults] = Convert.ToString(value);
            }
        }

        /// <summary>
        /// NoOfChildren Property
        /// </summary>
        public int NoOfChildren
        {
            get
            {
                int noOfChildren = 0;
                int.TryParse(this[Constants.NoOfChildren], out noOfChildren);
                return noOfChildren;
            }
            set
            {
                this[Constants.NoOfChildren] = Convert.ToString(value);
            }
        }
        
        #region Implement Abstract Methods
        
        /// <summary>
        /// Get product details
        /// </summary>
        /// <returns></returns>
        public override Model.Product.IProduct GetProduct()
        {
            var product = UCommerce.EntitiesV2.Product.FirstOrDefault(i => i.Sku == this.OrderLine.Sku && i.VariantSku == this.OrderLine.VariantSku);

            return new Room
            {
                Type = ProductType.Room,
                Sku = product.Sku,
                VariantSku = product.VariantSku,
                Name = product.Name
            };
        }

        /// <summary>
        /// Get product details
        /// </summary>
        /// <returns></returns>
        public override Model.Provider.IProvider GetProvider()
        {
            var providerService = ObjectFactory.Instance.Resolve<service.IProviderService>();

            ProviderSearch bbProviderDetails = new ProviderSearch() { ProviderId = this.OrderLine.Sku, ProviderType = ProviderType.LoyaltyBuild };
            service.IServiceProvider bbProvider = providerService.GetServiceProvider(ProviderType.LoyaltyBuild);
            return bbProvider.GetProviderDetails(bbProviderDetails);
        }
        
        #endregion
    }
}