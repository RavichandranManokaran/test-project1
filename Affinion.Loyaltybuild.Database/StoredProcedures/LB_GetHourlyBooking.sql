﻿CREATE PROCEDURE [dbo].[LB_GetHourlyBooking](@currentTime as datetime)
as
begin
	select ca.clientid as [Partner Name],count(ol.OrderLineId)as 'Booking(s)' from LB_CustomerAttribute as ca
	inner join
	uCommerce_Customer as c on ca.CustomerId=c.CustomerId
	inner join 
	uCommerce_PurchaseOrder as po on c.CustomerId=po.CustomerId
	inner join
	uCommerce_OrderLine as ol on po.OrderId=ol.OrderId 
	inner join 
	LB_RoomReservation as rr on ol.OrderLineId=rr.OrderLineId
	where rr.OrderReference is not null and  rr.NewOrderReference is null and rr.OrderReference not in (select NewOrderReference from LB_RoomReservation where NewOrderReference is not null) and
	CAST(ol.CreatedOn AS DATE)=CAST(@currentTime as date) AND  CAST(ol.CreatedOn AS time)<=CAST(@currentTime as time) and CAST(ol.CreatedOn AS TIME)>CAST(DATEADD(HOUR, -1, @currentTime) AS TIME)
	group by ca.clientid
end

GO