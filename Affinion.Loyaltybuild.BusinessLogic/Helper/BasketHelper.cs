﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           BasketHelper.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Utilities
/// Description:           Configuration Helper Class to read configuration settings in Web.config file
/// </summary>

#region Using Directives

using Affinion.LoyaltyBuild.Common;
using Affinion.LoyaltyBuild.Common.Exceptions;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.SessionHelper;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
using Affinion.LoyaltyBuild.Search.Data;
using Affinion.LoyaltyBuild.Search.Helpers;
using Affinion.LoyaltyBuild.UCom.Common.Constants;
using model = Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Subrate;
using Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi;
using Affinion.LoyaltyBuild.UCom.Common.Extension;
using Affinion.LoyaltyBuild.UCom.Common;
//using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.PaymentRuleService;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using UCommerce.Api;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Runtime;
using UCommerce.Transactions;
using UCommerce.Xslt;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Rules;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Rules.Model;
using Affinion.LoyaltyBuild.BedBanks.JacTravel;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Message;


#endregion

namespace Affinion.Loyaltybuild.BusinessLogic.Helper
{
    /// <summary>
    /// BasketHelper
    /// </summary>
    public static class BasketHelper
    {

        #region Public Methods

        private const string ProcessingFee = "CCPROFEE";

        /// <summary>
        /// Add item to basket
        /// </summary>
        /// <param name="commandArgument">string array</param>
        /// <param name="qty">quantity</param>
        /// <param name="basketInfo">BasketInfo object</param>
        //public static void AddToBasket(string[] commandArgument, int qty, BasketInfo basketInfo = null)
        //{
        //    Item clientItem = SitecoreFieldsHelper.GetLookupFieldTargetItem(ItemHelper.RootItem, "ClientDetails");
        //    AddToBasket(clientItem, commandArgument, qty, basketInfo);
        //}

        /// <summary>
        /// Add item to basket
        /// </summary>
        /// <param name="clientItem">Client Item</param>
        /// <param name="commandArgument">string array</param>
        /// <param name="qty">quantity</param>
        /// <param name="basketInfo">BasketInfo object</param>
        //public static void AddToBasket(Item clientItem, string[] commandArgument, int qty, BasketInfo basketInfo = null)
        //{
        //    try
        //    {
        //        int currencyId = 0;
        //        if (Int32.TryParse(commandArgument[1], out currencyId))
        //        {
        //            int campaignItemId;
        //            if (commandArgument.Count() <= 3 || !int.TryParse(commandArgument[3], out campaignItemId))
        //            {
        //                campaignItemId = 0;
        //            }

        //            // store award information for offers
        //            OfferTypes awardType;
        //            if (commandArgument.Count() <= 4 || !Enum.TryParse(commandArgument[4], out awardType))
        //            {
        //                awardType = OfferTypes.UnhandledAward;
        //            }


        //            PerformAddToBasket(commandArgument, qty, currencyId, campaignItemId, awardType, basketInfo, clientItem);
        //            TransactionLibrary.GetBasket(IsBasketExists()).Save();

        //            var order = TransactionLibrary.GetBasket(false).PurchaseOrder;
        //            order[OrderPropertyConstants.ClientId] = clientItem.ID.Guid.ToString("D");
        //            order.Save();

        //            //var orderline = TransactionLibrary.GetBasket(false).PurchaseOrder;
        //            //orderline[OrderPropertyConstants.ClientId] = clientItem.ID.Guid.ToString("D");
        //            //orderline.Save();


        //            Library.ExecuteBasketPipeline();

        //            // Call custom method to insert data
        //            UpdateBasketCustomData(basketInfo, campaignItemId, awardType);

        //            // Add data to subrate table
        //            AddSubrates(clientItem);
        //        }
        //    }
        //    catch (Exception exception)
        //    {
        //        Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
        //        /// Throw a custom exception with a custom message
        //        throw new AffinionException(exception.Message, exception);
        //    }
        //}

        /// <summary>
        ///  Roomwise Add to basket function
        /// </summary>
        /// <param name="commandArgument">commandArgument</param>
        /// <param name="qty">Number of Rooms selected</param>
        /// <param name="currencyId">currencyId</param>
        /// <param name="campaignItemId">Campaign ItemId</param>
        /// <param name="awardType">Type of offers</param>
        /// <param name="basketInfo">BasketInfo object</param>
        //private static void PerformAddToBasket(string[] commandArgument, int qty, int currencyId, int campaignItemId, OfferTypes awardType, BasketInfo basketInfo, Item clientItem)
        //{
        //    for (int x = qty; x > 0; x--)
        //    {
        //        int noOfNights = 1;
        //        int.TryParse(basketInfo.Nights, out noOfNights);
        //        TransactionLibrary.AddToBasket(noOfNights, commandArgument[0], null,null,null, false, true);


        //        var orderLineIds = UCommerce.Api.TransactionLibrary.GetBasket(false).PurchaseOrder.OrderLines.Select(ol => ol.OrderLineId);
        //        int olId = orderLineIds.Max();

        //        // store campaign information for offers
        //        if (campaignItemId > 0)
        //        {
        //            LoyaltyBuildTransactionLibrary.AppliedCampaignOnOrderLine(basketInfo.Sku, campaignItemId, olId);
        //        }

        //        // store award information for offers
        //        LoyaltyBuildTransactionLibrary.AppliedAwardOnOrderLine(basketInfo.Sku, awardType.ToString(), olId);

        //        // store search criteria
            
        //        SearchCriteria criteria = new SearchCriteria();
        //        criteria.ArrivalDate = DateTimeHelper.ParseDate(basketInfo.CheckinDate);
        //        criteria.Night = int.Parse(basketInfo.Nights);
        //        criteria.NumberOfAdult = int.Parse(basketInfo.NoOfAdults);
        //        criteria.NumberOfChild = int.Parse(basketInfo.NoOfChildren);
        //        LoyaltyBuildTransactionLibrary.AddAddOnProductToOrderLine(basketInfo.Sku, null, null, criteria, olId);
        //        Library.ExecuteBasketPipeline();

        //        UpdateOrderLineCustomProperties(olId, basketInfo, clientItem);
        //    }
        //}

        /// <summary>
        /// Updates the OrderlineProperties
        /// </summary>
        /// <param name="orderLineId">orderLineId</param>
        /// <param name="basketInfo">BasketInfo object</param>
        /// <param name="clientItem">clientItem</param>
        private static void UpdateOrderLineCustomProperties(int orderLineId, BasketInfo basketInfo, Item clientItem)
        {
            var orderLine = UCommerce.EntitiesV2.OrderLine.FirstOrDefault(i => i.OrderLineId == orderLineId);

            if (orderLine != null)
            {
                var relation = UCommerce.EntitiesV2.CategoryProductRelation.FirstOrDefault(i => i.Product.Sku == basketInfo.Sku);
                if (relation != null)
                {
                    var property = relation.Category.CategoryProperties.FirstOrDefault(i => i.DefinitionField.Name == ProductDefinationConstant.HotelType);
                    if (property != null && !string.IsNullOrEmpty(property.Value))
                    {
                        var hotelitem = GetPartialPaymentDetail(clientItem, property.Value);//Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(property.Value));

                        if (hotelitem != null)
                        {
                            if (SitecoreFieldsHelper.GetInteger(hotelitem, "Percentage", 0) != 0)
                                orderLine[OrderPropertyConstants.PercentageOfAmount] = SitecoreFieldsHelper.GetInteger(hotelitem, "Percentage", 0).ToString();

                            if (SitecoreFieldsHelper.GetInteger(hotelitem, "Days Before", 0) != 0)
                                orderLine[OrderPropertyConstants.DaysBefore] = SitecoreFieldsHelper.GetInteger(hotelitem, "Days Before", 0).ToString();
                        }
                    }
                }
                orderLine[OrderPropertyConstants.CheckInDate] = basketInfo.CheckinDate;
                orderLine.Save();
            }
        }

        /// <summary>
        /// Gets the partial payment details
        /// </summary>
        /// <param name="clientItem">Client Item</param>
        /// <param name="providerType">Provider Type</param>
        private static Item GetPartialPaymentDetail(Item clientItem, string providerType)
        {
            if (clientItem != null)
            {
                var childItems = clientItem.Axes.GetDescendants().Where(x => x.TemplateID.ToString().Equals(Constants.partialPaymentDetails, StringComparison.InvariantCultureIgnoreCase)).ToList();
                if (childItems != null)
                {
                    foreach (Item childItem in childItems)
                    {
                        var field = SitecoreFieldsHelper.GetDropLinkFieldValue(childItem, "Provider Type", "__display name");

                        if (field.Equals(providerType))
                        {
                            return childItem;
                        }
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Orderline Subrate Updation
        /// </summary>
        /// <param name="orderlineSubrates">List of orderlineSubrates</param>
        /// <param name="orderId">Order Id</param>
        public static void UpdateOrderLineSubrate(List<model.OrderLineSubrate> orderlineSubrates, int orderId)
        {
            if (orderlineSubrates.Count > 0)
            {
                uCommerceConnectionFactory.AddOrderLineSubrates(CreateSubrateDataTable(orderlineSubrates), orderId);
            }
        }

        /// <summary>
        /// Gets Client Id for given Basket
        /// </summary>
        public static string GetBasketClientId()
        {
            if (TransactionLibrary.HasBasket())
                return TransactionLibrary.GetBasket(false).PurchaseOrder[OrderPropertyConstants.ClientId];

            return string.Empty;
        }

        /// <summary>
        /// Gets Client Id for given Order ID
        /// </summary>
        /// <param name="orderId">Order Id</param>
        public static string GetOrderClientId(int orderId)
        {
            var order = UCommerce.EntitiesV2.PurchaseOrder.FirstOrDefault(i => i.OrderId == orderId);
            if (order != null)
            {
                return order[OrderPropertyConstants.ClientId];
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets Client Id for given OrderLine ID
        /// </summary>
        /// <param name="orderLineId">orderLineId</param>
        public static string GetOrderLineClientId(int orderLineId)
        {
            var orderlineid = UCommerce.EntitiesV2.OrderLine.FirstOrDefault(i => i.OrderLineId == orderLineId);
            if (orderlineid != null)
            {


                return orderlineid.PurchaseOrder[OrderPropertyConstants.ClientId];

            }

            return string.Empty;
        }

        /// <summary>
        /// Gets Client Id 
        /// </summary>
        public static string GetClientId()
        {
            string clientId = string.Empty;
            ///Get the client item for online portal
            Item clientItem = SitecoreFieldsHelper.GetLookupFieldTargetItem(ItemHelper.RootItem, "ClientDetails");
            if (clientItem != null)
            {
                clientId = clientItem.ID.Guid.ToString();
            }

            return clientId;
        }

        /// <summary>
        /// Use to remove order line by Sku Id
        /// </summary>
        /// <param name="sku">order Sku id</param>
        public static void RemoveCartItemBySkuId(string sku)
        {
            try
            {
                if (IsBasketExists())
                {
                    var order = UCommerce.Api.TransactionLibrary.GetBasket(false).PurchaseOrder;
                    var orderLine = order.OrderLines.Where(o => o.Sku == sku).FirstOrDefault();
                    if (orderLine != null)
                    {
                        bool isRemoved = order.OrderLines.Remove(orderLine);
                        if (isRemoved)
                        {
                            if (order.OrderLines.Count == 0)
                            {
                                order.Delete();
                            }
                            else
                            {
                                order.Save();
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Get the number of item in basket
        /// </summary>
        /// <returns>Returns the basket item count</returns>
        public static int GetBasketCount()
        {
            try
            {
                if (IsBasketExists())
                    return UCommerce.Api.TransactionLibrary.GetBasket(false).PurchaseOrder.OrderLines.Count;
                else
                    return 0;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Clear the basket
        /// </summary>
        public static void ClearBasket()
        {
            try
            {
                if (IsBasketExists())
                {
                    UCommerce.Api.TransactionLibrary.ClearBasket();
                    //UCommerce.Api.TransactionLibrary.GetBasket(false).PurchaseOrder.Delete();
                    //UCommerce.Api.TransactionLibrary.GetBasket(false).PurchaseOrder.OrderLines.Clear();
                    //UCommerce.Api.TransactionLibrary.GetBasket(false).PurchaseOrder.Delete();
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Clear the basket
        /// </summary>
        /// <param name="orderId">orderId</param>
        public static void ClearBasket(int orderId)
        {
            try
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("REALEX: clearing basket for order id: {0}", orderId.ToString()));
                //if (IsBasketExists())
                //{
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("REALEX: basket exists for order id: {0}", orderId.ToString()));
                PurchaseOrder.Get(orderId).OrderLines.Clear();
                PurchaseOrder.Get(orderId).Delete();
                //}
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Add personal details to OrderAddress table
        /// </summary>
        /// <param name="details">Personal Details object</param>
        public static void AddPersonalDetails(PersonalDetails details)
        {
            try
            {
                uCommerceConnectionFactory.AddPersonalDetails(details);
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Add Guest details to RoomReservation table
        /// </summary>
        /// <param name="details">List of RoomResrevation objects</param>
        public static void AddGuestDetails(List<RoomReservation> details)
        {
            try
            {
                uCommerceConnectionFactory.AddGuestDetails(details);
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Add Guest details to RoomReservation table
        /// </summary>
        /// <param name="details">List of RoomResrevationAgeInfo objects</param>
        public static void AddGuestChildAgeDetails(string age, string orderLineId)
        {
            try
            {
                uCommerceConnectionFactory.AddGuestChildAgeDetails(age, orderLineId);
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Get the purchase Order Id
        /// </summary>
        /// <returns>OrderId of the item in basket</returns>
        public static int GetBasketOrderId()
        {
            try
            {
                int orderID = 0;
                if (IsBasketExists())
                {
                    orderID = TransactionLibrary.GetBasket(false).PurchaseOrder.OrderId;
                    //orderID = TransactionLibrary.GetBasket(false).PurchaseOrder.OrderLines.FirstOrDefault().OrderLineId;
                }
                return orderID;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Sest the booking Reference
        /// </summary>
        /// <param name="customerId">customerId</param>
        /// <param name="customerRefernces">customer Refernces</param>
        public static void SetBookingReferenceBooking(int customerId)
        {
            try
            {
                if (IsBasketExists())
                {
                    var orderLines = TransactionLibrary.GetBasket(false).PurchaseOrder.OrderLines;
                    var r = new Random(); // Seed with what you feel is appropriate

                    foreach (var orderline in orderLines)
                    {
                        var clientID = Constants.HardCodedClientId;
                        Item clientItem = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(clientID));
                        var bookingReferencePrefix = SitecoreFieldsHelper.GetValue(clientItem, "BookingReferencePrefix");
                        string bookingReferenceNumber = string.Format("{0}{1}", bookingReferencePrefix, GenerateBookingReference(r));
                        uCommerceConnectionFactory.SetBookingReferenceNumber(orderline.Id, bookingReferenceNumber);
                    }
                }

            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Get the billing currency
        /// </summary>
        /// <param name="orderId">Current order id</param>
        /// <returns>Returns the currency code for the current order</returns>
        public static string GetCurrencyCode(int orderId)
        {
            try
            {
                //Get the currency code from purchase order passing the order id 
                string currencyCode = PurchaseOrder.Get(orderId).BillingCurrency.ISOCode;

                if (string.IsNullOrEmpty(currencyCode))
                {
                    throw new AffinionException("Currency code cannot be found");
                }

                return currencyCode;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException("Error in retrieving billing currency", exception);
            }
        }

        /// <summary>
        /// Get the list of order lines
        /// </summary>
        /// <returns>Returns the Orderlines in the PurchaseOrder</returns>
        public static DataSet GetPurchaseOrderLines()
        {
            try
            {
                //ICollection<OrderLine> orderLines = null;
                DataSet ds = new DataSet();
                if (IsBasketExists())
                {
                    ds = uCommerceConnectionFactory.GetPurchaseOrderLines(GetBasketOrderId());
                }
                return ds;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Get order line details
        /// </summary>
        /// <param name="orderId">Current order id</param>
        /// <returns>Returns order line details</returns>
        public static DataSet GetOrderLines(int orderId)
        {
            try
            {
                //ICollection<OrderLine> orderLines = null;
                DataSet ds = new DataSet();
                //if (IsBasketExists())
                {
                    ds = uCommerceConnectionFactory.GetOrderLines(orderId);
                }
                return ds;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Get order line details
        /// </summary>
        /// <returns>Returns order line details</returns>
        public static DataSet GetOrderLines()
        {
            try
            {
                //ICollection<OrderLine> orderLines = null;
                DataSet ds = new DataSet();
                if (IsBasketExists())
                {
                    ds = uCommerceConnectionFactory.GetOrderLines(GetBasketOrderId());
                }
                return ds;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Updates the Availability
        /// </summary>
        /// <param name="orderId">Current order id</param>
        public static void UpdateAvailability(int orderId)
        {
            DataSet ds = GetOrderLines(orderId);
            if (ds != null && ds.Tables.Count > 0)
            {
                // Iterate through the records
                foreach (DataRow row in ds.Tables[0].Rows)
                {

                    int orderLineId = Convert.ToInt32(row["OrderLineId"]);
                    //int quantity = Convert.ToInt32(row["Quantity"]);
                    int checkInDate = DateTimeHelper.GetJulianDateCount(Convert.ToDateTime(row["CheckInDate"]));
                    int checkOutDate = DateTimeHelper.GetJulianDateCount(Convert.ToDateTime(row["CheckOutDate"]));
                    string sku = row["Sku"].ToString();
                    string VariantSku = Convert.ToString(row["VariantSku"]);
                    Diagnostics.Trace(DiagnosticsCategory.ClientPortal, "REALEX: inside status update loop");
                    Diagnostics.Trace(DiagnosticsCategory.ClientPortal, "REALEX: orderLineId: " + orderLineId + "");
                    Diagnostics.Trace(DiagnosticsCategory.ClientPortal, "REALEX: checkInDate: " + checkInDate + "");
                    Diagnostics.Trace(DiagnosticsCategory.ClientPortal, "REALEX: checkOutDate: " + checkOutDate + "");
                    Diagnostics.Trace(DiagnosticsCategory.ClientPortal, "REALEX: sku: " + sku + "");
                    Diagnostics.Trace(DiagnosticsCategory.ClientPortal, "REALEX: VariantSku: " + VariantSku + "");
                    //Diagnostics.Trace(DiagnosticsCategory.ClientPortal, "REALEX: quantity: " + quantity + "");

                    OrderLine orderLine = PurchaseOrder.Get(orderId).OrderLines.Where(i => i.OrderLineId == orderLineId).FirstOrDefault();

                    int campaignId = 0;
                    if (orderLine[OrderPropertyConstants.UserSelectedCampaign] != null)
                    {
                        int.TryParse(orderLine[OrderPropertyConstants.UserSelectedCampaign], out campaignId);
                    }

                    bool isBaseProduct = campaignId == 0 ? true : false;

                    uCommerceConnectionFactory.UpdateRoomAvailability(orderLineId, checkInDate, checkOutDate, sku, VariantSku, campaignId, isBaseProduct);
                }
            }
        }

        /// <summary>
        /// Updates the Dues and payments
        /// </summary>
        /// <param name="orderId">Current order id</param>
        public static void UpdateDuesAndPayments(int orderId)
        {
            PurchaseOrder order = PurchaseOrder.Get(orderId);

            if (order != null)
            {
                uCommerceConnectionFactory.UpdateDuesAndPayments(orderId, false);
            }
        }

        /// <summary>
        /// Get the list of order lines and Booking reservation 
        /// </summary>
        /// <returns>Returns the reservation details for the current order id</returns>
        public static DataSet GetOrderReservation(int orderId)
        {
            try
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, string.Format("REALEX: IS BASKET EXIST?: {0}", IsBasketExists()));
                DataSet ds = new DataSet();
                ds = uCommerceConnectionFactory.GetOrderReservation(orderId);
                return ds;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Get the mail of receiver 
        /// </summary>
        /// <param name="orderId">Current order id</param>
        /// <returns>Returns the Receiver mail address</returns>
        public static DataSet GetReciverMail(int orderId)
        {
            try
            {
                //ICollection<OrderLine> orderLines = null;
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, "REALEX: IS BASKET EXIST?: " + IsBasketExists() + "");
                DataSet ds = new DataSet();
                //if (IsBasketExists())
                //{
                ds = uCommerceConnectionFactory.GetReciverMail(orderId);
                //}
                return ds;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Gets the invalid reservation details
        /// </summary>
        /// <returns>Returns the reservation details</returns>
        public static string GetInvalidReservation()
        {
            try
            {
                int orderId = GetBasketOrderId();
                //ICollection<OrderLine> orderLines = null;
                DataSet ds = GetOrderLines();
                bool isValid = true;
                string product = string.Empty;
                if (ds != null && ds.Tables.Count > 0)
                {
                    // Iterate through the records
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        int orderLineId = Convert.ToInt32(row["OrderLineId"]);
                        int checkInDate = DateTimeHelper.GetJulianDateCount(Convert.ToDateTime(row["CheckInDate"]));
                        int checkOutDate = DateTimeHelper.GetJulianDateCount(Convert.ToDateTime(row["CheckOutDate"]));
                        string sku = Convert.ToString(row["Sku"]);
                        int quntity = Convert.ToInt32(row["Quantity"]);
                        string productName = Convert.ToString(row["ProductName"]);

                        OrderLine orderLine = PurchaseOrder.Get(orderId).OrderLines.Where(i => i.OrderLineId == orderLineId).FirstOrDefault();

                        int campaignId = 0;
                        if (orderLine[OrderPropertyConstants.UserSelectedCampaign] != null)
                        {
                            int.TryParse(orderLine[OrderPropertyConstants.UserSelectedCampaign], out campaignId);
                        }

                        isValid = uCommerceConnectionFactory.ValidateBookingAvailability(orderId, orderLineId, checkInDate, checkOutDate, sku, campaignId);
                        Diagnostics.Trace(DiagnosticsCategory.ClientPortal, "REALEX: Is valid product ?: " + isValid + "");
                        if (isValid == false)
                        {
                            product = productName;
                            break;
                        }
                    }
                }
                return product;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Assigns Customer To Order
        /// </summary>
        /// <param name="customerId">CustomerId</param>
        public static void AssignCustomerToOrder(int customerId)
        {
            var purchaseOrder = UCommerce.Api.TransactionLibrary.GetBasket(false).PurchaseOrder;
            var customer = UCommerce.EntitiesV2.Customer.Get(customerId);
            purchaseOrder.Customer = customer;
            purchaseOrder.Save();
        }

        /// <summary>
        /// Gets the Ucommerce Category Id
        /// </summary>
        /// <returns>Returns the Ucommerce Category Id for the Purchase order</returns>
        public static DataSet GetuCommerce_GetUCommerceCategoryIDForPurchaseOrder()
        {
            try
            {
                DataSet ds = new DataSet();

                return ds;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Gets the Order Subrates
        /// </summary>
        /// <param name="orderId">orderId</param>   
        /// <returns>Returns the Order Subrates for the orderId</returns>
        public static List<model.OrderLineSubrate> GetOrderSubrates(int orderId)
        {
            List<model.OrderLineSubrate> orderLineSubrates = new List<model.OrderLineSubrate>();

            foreach (OrderLine orderLine in PurchaseOrder.Get(orderId).OrderLines)
            {
                model.OrderLineSubrate orderLineSubrate = new model.OrderLineSubrate();
                orderLineSubrate.OrderLineId = orderLine.OrderLineId;

                List<model.Subrates> subrates = new List<model.Subrates>();
                subrates = uCommerceConnectionFactory.GetOrderlineSubrates(orderLine.OrderLineId);
                orderLineSubrate.Subrates = subrates;
                orderLineSubrates.Add(orderLineSubrate);
            }

            return orderLineSubrates;
        }

        /// <summary>
        /// use to populate the list of basket info repeater binding
        /// </summary>
        /// <returns>Returns booking summary data</returns>
        public static List<BasketInfo> GetPopulateBastketListFromDB()
        {
            try
            {
                //BasketInfo 
                List<BasketInfo> listBasketInfo = new List<BasketInfo>();
                DataSet ds = GetPurchaseOrderLines();
                if (ds != null && ds.Tables.Count > 0)
                {
                    // Iterate through the records
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        DateTime checkout, checkin;
                        DateTime.TryParse(row["CheckInDate"].ToString(), out checkin);
                        DateTime.TryParse(row["CheckOutDate"].ToString(), out checkout);
                        string coDate = String.Format("{0:dd/MM/yyyy}", checkout);
                        string ciDate = String.Format("{0:dd/MM/yyyy}", checkin);
                        string coTime = String.Format("{0:HH:mm}", checkout);
                        string ciTime = String.Format("{0:HH:mm}", checkin);
                        string nights = string.Empty;
                        int orderLineId = Convert.ToInt32(row["OrderLineId"]);
                        string sku = Convert.ToString(row["Sku"]);
                        string OrderId = Convert.ToString(row["OrderId"]);

                        //nights = (checkout - checkin).ToString("dd");

                        decimal price = 0;

                        var noOfRooms = 1;
                        OrderLine orderLine = UCommerce.Api.TransactionLibrary.GetBasket(false).PurchaseOrder.OrderLines.Where(i => i.OrderLineId == orderLineId).FirstOrDefault();

                        if (orderLine.Total.HasValue)
                        {
                            price = price + (orderLine.Total.Value - (orderLine.VAT * orderLine.Quantity));
                        }

                        listBasketInfo.Add(new BasketInfo()
                            {
                                Sku = sku,
                                CheckinDate = ciDate,
                                CheckoutDate = coDate,
                                Nights = (checkout - checkin).ToString("dd"),
                                //  Price = Convert.ToString(row["Total"]),  
                                Price = price.ToString("0.00"),//    (Convert.ToDouble((row["Price"]))).ToString("0.00"), // Modified the Price from Total
                                NoOfRooms = Convert.ToString(noOfRooms),
                                RoomType = Convert.ToString(row["ProductName"]),
                                GuestName = Convert.ToString(row["GuestName"]),
                                //Adults = Convert.ToString(row["NoOfAdults"]),
                                //People = string.Concat(Convert.ToString(row["NoOfAdults"]), " ", "Adults", " ", Convert.ToString(row["NoOfChilderen"]), " ", "Children"),
                                CategoryId = Convert.ToString(row["CategoryId"]),
                                OrderLineId = Convert.ToString(row["OrderLineId"])
                            }
                            );
                    }
                }

                return listBasketInfo;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// use to populate the list of basket info repeater binding
        /// </summary>
        /// <param name="orderId">Current purchase order id</param>
        /// <param name="OrderLineSubrates">Subrates for orderlines of current order</param>
        /// <returns>Returns booking summary data</returns>
        public static List<BasketInfo> GetPopulateConfirmationListFromDB(int orderId)
        {
            try
            {
                //BasketInfo 
                List<BasketInfo> listBasketInfo = new List<BasketInfo>();

                DataSet ds = GetOrderReservation(orderId);
                decimal processingFee = 0;
                decimal bookingDeposit = 0;

                //if (OrderLineSubrates.Count > 0)
                //{
                //    if (OrderLineSubrates.SelectMany(i => i.Subrates).Count() > 0)
                //    {
                //        processingFee = OrderLineSubrates.SelectMany(i => i.Subrates)
                //                                             .Where(i => i.SubrateCode == ProcessingFee)
                //                                             .Select(t => t.TotalAmount)
                //                                             .Max();
                //    }
                //}

                if (ds != null && ds.Tables.Count > 0)
                {
                    // Iterate through the records
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        DateTime checkout, checkin;
                        DateTime.TryParse(row["CheckInDate"].ToString(), out checkin);
                        DateTime.TryParse(row["CheckOutDate"].ToString(), out checkout);
                        string coDate = String.Format("{0:dd/MM/yyyy}", checkout);
                        string ciDate = String.Format("{0:dd/MM/yyyy}", checkin);
                        string coTime = String.Format("{0:HH:mm}", checkout);
                        string ciTime = String.Format("{0:HH:mm}", checkin);

                        string orderLineId = Convert.ToString(row["OrderLineId"]);
                        decimal price = Convert.ToDecimal((row["Price"]));// ToString("0.00");
                        int noOfRooms = Convert.ToInt32(row["Quantity"]);
                        string nights = (checkout - checkin).ToString("dd");
                        decimal discount = Convert.ToDecimal((row["Discount"]));
                        string roomType = Convert.ToString(row["ProductName"]);

                        OrderLine orderLine = PurchaseOrder.Get(orderId).OrderLines.Where(i => i.OrderLineId == Convert.ToInt32(orderLineId)).FirstOrDefault();


                        decimal totalOrderLinePrice = 0;

                        if (orderLine.Total.HasValue)
                        {
                            totalOrderLinePrice = totalOrderLinePrice + (orderLine.Total.Value - (orderLine.VAT * orderLine.Quantity));
                        }

                        //if (OrderLineSubrates.Count > 0)
                        //{
                        //    List<model.Subrates> subrates = OrderLineSubrates.Where(i => i.OrderLineId.ToString() == orderLineId).FirstOrDefault().Subrates;
                        //    if (subrates.Count > 0)
                        //    {
                        //        bookingDeposit = subrates.Where(i => i.SubrateCode == "LBCOM").Select(i => i.TotalAmount).FirstOrDefault();
                        //    }
                        //}

                        if (orderLine != null && orderLine.Discounts != null && orderLine.Discounts.Count != 0)
                        {
                            roomType = string.Concat(roomType, "(", orderLine.Discounts.First().CampaignItemName, ")");
                        }

                        bool isProFeeCharged = false;
                        bool.TryParse(orderLine[OrderPropertyConstants.IsProcessingFeeCharged], out isProFeeCharged);
                        decimal providerAmountPaid = 0;
                        decimal.TryParse(orderLine[OrderPropertyConstants.ProviderAmountPaidCheckOut], out providerAmountPaid);

                        listBasketInfo.Add(new BasketInfo()
                        {
                            Sku = Convert.ToString(row["Sku"]),
                            OrderLineReference = Convert.ToString(row["BookingNo"]),
                            CheckinDate = ciDate,
                            CheckoutDate = coDate,
                            Nights = nights,
                            //  Price = Convert.ToString(row["Total"]),
                            NoOfChildren = Convert.ToString(row["ChildCount"]),
                            Price = totalOrderLinePrice.ToString("0.00"),
                            NoOfRooms = noOfRooms.ToString(),
                            RoomType = roomType,
                            Adults = Convert.ToString(row["AdultCount"]),
                            People = string.Concat(Convert.ToString(row["AdultCount"]), " ", "Adults", " ", Convert.ToString(row["ChildCount"]), " ", "Children"),
                            CategoryId = Convert.ToString(row["ProductId"]),
                            OrderLineId = orderLineId,
                            GuestName = Convert.ToString(row["GuestName"]),
                            Email = Convert.ToString(row["EmailAddress"]),
                            Discount = discount.ToString("0"),
                            PayableAtAccommodation = (totalOrderLinePrice - bookingDeposit - providerAmountPaid).ToString("0.00"),
                            ProcessingFee = (isProFeeCharged ? processingFee : 0).ToString("0.00"),
                            SpecialRequest = Convert.ToString(row["SpecialRequest"]),
                            DepositPayableToday = (((isProFeeCharged ? processingFee : 0) + bookingDeposit + providerAmountPaid)).ToString("0.00"),
                            TotalPrice = (totalOrderLinePrice + (isProFeeCharged ? processingFee : 0)).ToString("0.00"),
                        }
                            );
                    }
                }

                return listBasketInfo;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }


        /// <summary>
        /// use to populate the list of basket info repeater binding
        /// </summary>
        /// <param name="orderId">Current purchase order id</param>
        /// <param name="OrderLineSubrates">Subrates for orderlines of current order</param>
        /// <returns>Returns booking summary data</returns>
        public static List<BasketInfo> PopulateConfirmationDataForClientPortal(int orderId)
        {
            try
            {
                //BasketInfo 
                List<BasketInfo> listBasketInfo = new List<BasketInfo>();
                DataSet ds = GetOrderReservation(orderId);
                decimal processingFee = 0;
                decimal bookingDeposit = 0;


                if (ds != null && ds.Tables.Count > 0)
                {
                    // Iterate through the records
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        DateTime checkout, checkin;
                        //DateTime.TryParse(row["CheckInDate"].ToString(), out checkin);
                        //DateTime.TryParse(row["CheckOutDate"].ToString(), out checkout);
                        checkin = (row["CheckInDate"].ToString()).ParseFormatDateTime(true);
                        checkout = (row["CheckOutDate"].ToString()).ParseFormatDateTime(true);
                        string coDate = String.Format("{0:dd/MM/yyyy}", checkout);
                        string ciDate = String.Format("{0:dd/MM/yyyy}", checkin);
                        string coTime = String.Format("{0:HH:mm}", checkout);
                        string ciTime = String.Format("{0:HH:mm}", checkin);

                        string orderLineId = Convert.ToString(row["OrderLineId"]);
                        decimal price = Convert.ToDecimal((row["Price"]));// ToString("0.00");
                        int noOfRooms = Convert.ToInt32(row["Quantity"]);
                        string nights = (checkout - checkin).ToString("dd");
                        decimal discount = Convert.ToDecimal((row["Discount"]));
                        string roomType = Convert.ToString(row["ProductName"]);

                        OrderLine orderLine = PurchaseOrder.Get(orderId).OrderLines.Where(i => i.OrderLineId == Convert.ToInt32(orderLineId)).FirstOrDefault();

                        string pType = orderLine.GetOrderProperty("_ProductType").Value;

                        decimal totalOrderLinePrice = 0;

                        if (orderLine.Total.HasValue)
                        {
                            totalOrderLinePrice = totalOrderLinePrice + (orderLine.Total.Value - (orderLine.VAT * orderLine.Quantity));
                        }

                        if (!string.IsNullOrEmpty(pType) && !(pType.ToLower().Equals("bedbanks")))
                        {
                            if (orderLine != null && orderLine.Discounts != null && orderLine.Discounts.Count != 0)
                            {

                                roomType = string.Concat(roomType, "(", orderLine.Discounts.First().CampaignItemName, ")");
                            }
                        }
                        else
                        {
                            roomType = orderLine.GetOrderProperty("RoomInfo").Value;
                        }


                        bool isProFeeCharged = false;
                        bool.TryParse(orderLine[OrderPropertyConstants.IsProcessingFeeCharged], out isProFeeCharged);
                        //bool isProFeeCharged = false;
                       // bool.TryParse(orderLine[OrderPropertyConstants.IsProcessingFeeCharged], out isProFeeCharged);
                        decimal providerAmountPaid = 0;
                        decimal.TryParse(orderLine[OrderPropertyConstants.ProviderAmountPaidCheckOut], out providerAmountPaid);
                        decimal.TryParse(orderLine[OrderPropertyConstants.CommissionFee], out bookingDeposit);
                        decimal.TryParse(orderLine[OrderPropertyConstants.ProcessingFee], out processingFee);

                        if (!string.IsNullOrEmpty(pType) && !(pType.ToLower().Equals("bedbanks")))
                        {
                            listBasketInfo.Add(new BasketInfo()
                            {
                                Sku = Convert.ToString(row["Sku"]),
                                OrderLineReference = Convert.ToString(row["BookingNo"]),
                                CheckinDate = ciDate,
                                CheckoutDate = coDate,
                                Nights = nights,
                                //  Price = Convert.ToString(row["Total"]),
                                NoOfChildren = Convert.ToString(row["ChildCount"]),
                                Price = totalOrderLinePrice.ToString("0.00"),
                                NoOfRooms = noOfRooms.ToString(),
                                RoomType = roomType,
                                Adults = Convert.ToString(row["AdultCount"]),
                                People = string.Concat(Convert.ToString(row["AdultCount"]), " ", "Adults", " ", Convert.ToString(row["ChildCount"]), " ", "Children"),
                                CategoryId = Convert.ToString(row["ProductId"]),
                                OrderLineId = orderLineId,
                                GuestName = Convert.ToString(row["GuestName"]),
                                Email = Convert.ToString(row["EmailAddress"]),
                                Discount = discount.ToString("0"),
                                PayableAtAccommodation = (totalOrderLinePrice - bookingDeposit - providerAmountPaid).ToString("0.00"),
                                ProcessingFee = ( processingFee).ToString("0.00"),
                                SpecialRequest = Convert.ToString(row["SpecialRequest"]),
                                DepositPayableToday = ((( processingFee) + bookingDeposit + providerAmountPaid)).ToString("0.00"),
                                TotalPrice = (totalOrderLinePrice + ( processingFee)).ToString("0.00"),
                            }
                                );
                        }
                        else
                        {
                            listBasketInfo.Add(new BasketInfo()
                            {
                                Sku = Convert.ToString(row["Sku"]),
                                OrderLineReference = Convert.ToString(row["BookingNo"]),
                                CheckinDate = ciDate,
                                CheckoutDate = coDate,
                                Nights = nights,
                                //  Price = Convert.ToString(row["Total"]),
                                NoOfChildren = Convert.ToString(row["ChildCount"]),
                                Price = totalOrderLinePrice.ToString("0.00"),
                                NoOfRooms = noOfRooms.ToString(),
                                RoomType = roomType,
                                Adults = Convert.ToString(row["AdultCount"]),
                                People = string.Concat(Convert.ToString(row["AdultCount"]), " ", "Adults", " ", Convert.ToString(row["ChildCount"]), " ", "Children"),
                                CategoryId = Convert.ToString(row["ProductId"]),
                                OrderLineId = orderLineId,
                                GuestName = Convert.ToString(row["GuestName"]),
                                Email = Convert.ToString(row["EmailAddress"]),
                                Discount = discount.ToString("0"),
                                PayableAtAccommodation = (totalOrderLinePrice - bookingDeposit - providerAmountPaid).ToString("0.00"),
                                ProcessingFee = (isProFeeCharged ? processingFee : 0).ToString("0.00"),
                                SpecialRequest = Convert.ToString(row["SpecialRequest"]),
                                DepositPayableToday = (((isProFeeCharged ? processingFee : 0) + bookingDeposit + providerAmountPaid)).ToString("0.00"),
                                TotalPrice = (totalOrderLinePrice + (isProFeeCharged ? processingFee : 0)).ToString("0.00"),
                                ProductType = pType,
                                BBImageUrl = orderLine.GetOrderProperty("ImageUrl").Value,
                                BBPropertyName = orderLine.GetOrderProperty("PropertyName").Value,
                                BBStarRanking = orderLine.GetOrderProperty("StarRanking").Value,
                                SupplierId = orderLine.GetOrderProperty("PropertyId").Value
                            }
                                                           );
                        }
                    }
                }

                return listBasketInfo;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// use to populate provider mail body
        /// </summary>
        /// <param name="orderId">Current purchase order id</param>
        /// <param name="OrderLineSubrates">Subrates for orderlines of current order</param>
        /// <param name="mailItemSource">Mail Item Source</param>
        /// <returns>Returns booking summary data for Email body</returns>
        public static List<string> GetPopulateProviderMailBody(int orderId, DataSet ds, List<model.OrderLineSubrate> OrderLineSubrates, Item mailItemSource = null)
        {
            try
            {
                List<string> listBasketInfo = new List<string>();
                decimal processingFee = 0;

                if (OrderLineSubrates.Count > 0)
                {
                    if (OrderLineSubrates.SelectMany(i => i.Subrates).Count() > 0)
                    {
                        processingFee = OrderLineSubrates.SelectMany(i => i.Subrates)
                                                             .Where(i => i.SubrateCode == ProcessingFee)
                                                             .Select(t => t.TotalAmount)
                                                             .Max();
                    }
                }

                string bodyMsg = (mailItemSource != null) ? mailItemSource["ProviderConfirmationBody"] : Sitecore.Context.Item["ProviderConfirmationBody"];

                if (ds != null && ds.Tables.Count > 0)
                {
                    // Iterate through the records
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        DateTime checkout, checkin;
                        DateTime.TryParse(row["CheckInDate"].ToString(), out checkin);
                        DateTime.TryParse(row["CheckOutDate"].ToString(), out checkout);
                        string coDate = String.Format("{0:D}", checkout);
                        string ciDate = String.Format("{0:D}", checkin);
                        string coTime = String.Format("{0:HH:mm}", checkout);
                        string ciTime = String.Format("{0:HH:mm}", checkin);
                        ///subrate section
                        string orderLineId = Convert.ToString(row["OrderLineId"]);
                        decimal price = Convert.ToDecimal((row["Price"]));// ToString("0.00");
                        int noOfRooms = Convert.ToInt32(row["Quantity"]);
                        string nights = (checkout - checkin).ToString("dd");
                        decimal discount = Convert.ToDecimal((row["Discount"]));
                        decimal bookingDeposit = 0;

                        OrderLine orderLine = PurchaseOrder.Get(orderId).OrderLines.Where(i => i.OrderLineId == Convert.ToInt32(orderLineId)).FirstOrDefault();


                        bool isProFeeCharged = false;
                        bool.TryParse(orderLine[OrderPropertyConstants.IsProcessingFeeCharged], out isProFeeCharged);
                        decimal providerAmountPaid = 0;
                        decimal.TryParse(orderLine[OrderPropertyConstants.ProviderAmountPaidCheckOut], out providerAmountPaid);

                        decimal totalOrderLinePrice = 0;

                        if (orderLine.Total.HasValue)
                        {
                            totalOrderLinePrice = totalOrderLinePrice + (orderLine.Total.Value - (orderLine.VAT * orderLine.Quantity));
                        }

                        if (OrderLineSubrates.Count > 0)
                        {
                            List<model.Subrates> subrates = OrderLineSubrates.Where(i => i.OrderLineId.ToString() == orderLineId).FirstOrDefault().Subrates;
                            if (subrates.Count > 0)
                            {
                                bookingDeposit = subrates.Where(i => i.SubrateCode == "LBCOM").Select(i => i.TotalAmount).FirstOrDefault();
                            }
                        }
                        string providerRate = bookingDeposit.ToString("0.00");
                        string payableAtAccommodation = (totalOrderLinePrice - bookingDeposit - providerAmountPaid).ToString("0.00");

                        string hotelName = null;
                        Item[] hotels = Sitecore.Context.Database.SelectItems(Constants.BasketPageQueryPath);

                        foreach (Item itm in hotels)
                        {
                            string val = StoreHelper.GetUcommerceCategoryId(itm).ToString();
                            if (Convert.ToString(row["CATEGORYID"]) == val)
                            {
                                hotelName = itm.DisplayName;
                                break;
                            }
                        }
                        string childrenAges = GetReservationChildrenAgeSet(Convert.ToInt32(row["OrderLineId"]));

                        listBasketInfo.Add(string.Format(bodyMsg, Convert.ToString(row["OrderReference"]), Convert.ToString(row["FirstName"]) + " " + Convert.ToString(row["LastName"]), Convert.ToString(row["PhoneNumber"]), Convert.ToString(row["AddressName"]), hotelName, Convert.ToDateTime(row["CheckInDate"]).ToString("dd/MM/yyyy"), Convert.ToDateTime(row["CheckOutDate"]).ToString("dd/MM/yyyy"), (checkout - checkin).ToString("dd"), Convert.ToString(row["ProductName"]), string.Concat(Convert.ToString(row["Quantity"]), " ", "room/unit", " ", Convert.ToString(row["AdultCount"]), " ", "adult/people", " ", Convert.ToString(row["ChildCount"]), " ", "Children"), childrenAges, Convert.ToString(row["SpecialRequest"]), Convert.ToString(row["Note"]), providerRate, payableAtAccommodation, discount.ToString("0.00")));
                    }
                }
                return listBasketInfo;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// use to populate customer mail body
        /// </summary>
        /// <param name="orderId">Current purchase order id</param>
        /// <param name="OrderLineSubrates">Subrates for orderlines of current order</param>
        /// <param name="mailItemSource">Mail Item Source</param>
        /// <returns>Returns booking summary data for Email body</returns>
        public static List<string> GetPopulateCustomerMailBody(int orderId, DataSet ds, List<model.OrderLineSubrate> OrderLineSubrates, Item mailItemSource = null)
        {
            try
            {
                List<string> listBasketInfo = new List<string>();
                decimal processingFee = 0;
                ISitecorePaymentService sitecorePaymentService = new SitecorePaymentService();
                var paymentMethod = sitecorePaymentService.GetPaymentMethod(orderId);

                if (OrderLineSubrates.Count > 0)
                {
                    if (OrderLineSubrates.SelectMany(i => i.Subrates).Count() > 0)
                    {
                        processingFee = OrderLineSubrates.SelectMany(i => i.Subrates)
                                                             .Where(i => i.SubrateCode == ProcessingFee)
                                                             .Select(t => t.TotalAmount)
                                                             .Max();
                    }
                }

                string bodyMsg = Sitecore.Context.Item["CustomerConfirmationBody"];
                string partialbody = Sitecore.Context.Item["PartialPaymentConfirmationBody"];
                if (ds != null && ds.Tables.Count > 0)
                {
                    // Iterate through the records
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        DateTime checkout, checkin;
                        DateTime.TryParse(row["CheckInDate"].ToString(), out checkin);
                        DateTime.TryParse(row["CheckOutDate"].ToString(), out checkout);
                        string coDate = String.Format("{0:D}", checkout);
                        string ciDate = String.Format("{0:D}", checkin);
                        string coTime = String.Format("{0:HH:mm}", checkout);
                        string ciTime = String.Format("{0:HH:mm}", checkin);

                        ///subrate section
                        string orderLineId = Convert.ToString(row["OrderLineId"]);
                        decimal price = Convert.ToDecimal((row["Price"]));// ToString("0.00");
                        int noOfRooms = Convert.ToInt32(row["Quantity"]);
                        string nights = (checkout - checkin).ToString("dd");
                        decimal discount = Convert.ToDecimal((row["Discount"]));

                        decimal bookingDeposit = 0;

                        OrderLine orderLine = PurchaseOrder.Get(orderId).OrderLines.Where(i => i.OrderLineId == Convert.ToInt32(orderLineId)).FirstOrDefault();

                        bool isProFeeCharged = false;
                        bool.TryParse(orderLine[OrderPropertyConstants.IsProcessingFeeCharged], out isProFeeCharged);
                        decimal providerAmountPaid = 0;
                        decimal.TryParse(orderLine[OrderPropertyConstants.ProviderAmountPaidCheckOut], out providerAmountPaid);


                        decimal totalOrderLinePrice = 0;

                        if (orderLine.Total.HasValue)
                        {
                            totalOrderLinePrice = totalOrderLinePrice + (orderLine.Total.Value - (orderLine.VAT * orderLine.Quantity));
                        }

                        if (OrderLineSubrates.Count > 0)
                        {
                            List<model.Subrates> subrates = OrderLineSubrates.Where(i => i.OrderLineId.ToString() == orderLineId).FirstOrDefault().Subrates;
                            if (subrates.Count > 0)
                            {
                                bookingDeposit = subrates.Where(i => i.SubrateCode == "LBCOM").Select(i => i.TotalAmount).FirstOrDefault();
                            }
                        }
                        string providerRate = bookingDeposit.ToString("0.00");
                        string payableAtAccommodation = (totalOrderLinePrice - bookingDeposit - providerAmountPaid).ToString("0.00");
                        string daysBefore = orderLine[OrderPropertyConstants.DaysBefore];
                        decimal.TryParse(orderLine[OrderPropertyConstants.ProviderAmountPaidCheckOut], out providerAmountPaid);
                        decimal payableToday = 0;
                        int percentage = 0;
                        int.TryParse(orderLine[OrderPropertyConstants.PercentageOfAmount], out percentage);
                        payableToday = (Convert.ToDecimal(percentage) / 100) * totalOrderLinePrice + processingFee;
                        decimal orderlineprice = totalOrderLinePrice + processingFee;
                        string hotelName = null;
                        string email = null;
                        string phoneNumber = null;
                        string address1 = null;
                        string address2 = null;
                        string address3 = null;
                        Item[] hotels = Sitecore.Context.Database.SelectItems(Constants.BasketPageQueryPath);

                        foreach (Item itm in hotels)
                        {
                            string val = StoreHelper.GetUcommerceCategoryId(itm).ToString();
                            if (Convert.ToString(row["CATEGORYID"]) == val)
                            {
                                hotelName = itm.DisplayName;
                                address1 = itm["AddressLine1"];
                                address2 = itm["AddressLine2"];
                                address3 = itm["AddressLine3"];
                                email = itm["EmailID2"];
                                phoneNumber = itm["Phone"];
                                break;
                            }
                        }

                        var selectedPayment = paymentMethod.FirstOrDefault(i => i.OrderLineId == orderLine.OrderLineId);
                        string paymentMethodName = string.Empty;
                        if (selectedPayment != null)
                            paymentMethodName = selectedPayment.Paymentmethod;

                        string childrenAges = GetReservationChildrenAgeSet(Convert.ToInt32(row["OrderLineId"]));

                        if (paymentMethodName == Constants.PaymentMethod.PartialPayment)
                            listBasketInfo.Add(string.Format(partialbody, percentage, Convert.ToString(row["OrderReference"]), Convert.ToString(row["GuestName"]), string.Concat(Convert.ToString(row["Quantity"]), " ", "room/unit", " ", Convert.ToString(row["AdultCount"]), " ", "adult/people", " ", Convert.ToString(row["ChildCount"]), " ", "Children"), Convert.ToString(row["ProductName"]), Convert.ToDateTime(row["CheckInDate"]).ToString("dd/MM/yyyy"), Convert.ToDateTime(row["CheckOutDate"]).ToString("dd/MM/yyyy"), Convert.ToString(row["SpecialRequest"]), childrenAges, Convert.ToString(orderlineprice), payableToday, processingFee.ToString("0.00"), daysBefore, payableAtAccommodation, hotelName, address1, address2, address3, phoneNumber, email));
                        else
                            listBasketInfo.Add(string.Format(bodyMsg, Convert.ToString(row["OrderReference"]), Convert.ToString(row["GuestName"]), string.Concat(Convert.ToString(row["Quantity"]), " ", "room/unit", " ", Convert.ToString(row["AdultCount"]), " ", "adult/people", " ", Convert.ToString(row["ChildCount"]), " ", "Children"), Convert.ToString(row["ProductName"]), Convert.ToDateTime(row["CheckInDate"]).ToString("dd/MM/yyyy"), Convert.ToDateTime(row["CheckOutDate"]).ToString("dd/MM/yyyy"), Convert.ToString(row["SpecialRequest"]), childrenAges, Convert.ToString(totalOrderLinePrice), bookingDeposit.ToString("0.00"), processingFee.ToString("0.00"), payableAtAccommodation, discount.ToString("0.00"), "--", hotelName, address1, address2, address3, phoneNumber, email));
                    }
                }

                return listBasketInfo;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Gets the Number of nights for the orderline
        /// </summary>
        /// <param name="orderLine">Current orderLine</param>
        /// <returns>Returns Number of nights</returns>
        private static int GetNumberOfNights(OrderLine orderLine)
        {
            int campaignId = 0;
            int noOfNights = 0;
            if (orderLine[OrderPropertyConstants.UserSelectedCampaign] != null)
            {
                int.TryParse(orderLine[OrderPropertyConstants.UserSelectedCampaign], out campaignId);
            }

            // read award information for offers
            OfferTypes awardType;
            if (!Enum.TryParse(orderLine[OrderPropertyConstants.AppliedAward], out awardType))
            {
                awardType = OfferTypes.UnhandledAward;
            }

            // special offers should not be multiplied by no of nights
            if (campaignId > 0 && awardType == OfferTypes.SpecialPrice)
            {
                noOfNights = 1;
            }
            else
            {
                noOfNights = uCommerceConnectionFactory.GetNoOfNights(orderLine.OrderLineId);
            }

            return noOfNights;
        }

        /// <summary>
        /// Gets reciver email
        /// </summary>
        /// <param name="orderId">orderId</param>
        /// <param name="ds">Dataset</param>
        /// <returns>Returns Customer Email</returns>
        public static string getCustomerEmail(int orderId, DataSet ds)
        {
            try
            {
                string reciverMails = null;

                if (ds != null && ds.Tables.Count > 0)
                {
                    reciverMails = ds.Tables[0].Rows[0]["EmailAddress"].ToString();
                }
                return reciverMails;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Gets provider mail
        /// </summary>
        /// <param name="orderId">orderId</param>
        /// <param name="ds">Dataset</param>
        /// <returns>Returns Provider Email</returns>
        public static List<string> getProviderEmail(int orderId, DataSet ds)
        {
            try
            {
                //string reciverMails = null;
                List<string> reciverMails = new List<string>();

                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        string email = null;
                        Item[] hotels = Sitecore.Context.Database.SelectItems(Constants.BasketPageQueryPath);
                        foreach (Item itm in hotels)
                        {
                            string val = StoreHelper.GetUcommerceCategoryId(itm).ToString();
                            if (Convert.ToString(row["CATEGORYID"]) == val)
                            {
                                email = itm["EmailID1"];
                                reciverMails.Add(email);
                                reciverMails.Add(itm.DisplayName);
                                break;
                            }
                        }
                    }
                }
                return reciverMails;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Updates payment Method
        /// </summary>
        /// <param name="orderId">orderId</param>
        /// <param name="clientId">clientId</param>
        public static void UpdatePaymentMethod(int orderId, string clientId)
        {
            // clientId = GetOrderClientId(orderId);
            ISitecorePaymentService sitecorePaymentService = new SitecorePaymentService();
            var order = TransactionLibrary.GetBasket(false).PurchaseOrder;

            var paymentType = order[OrderPropertyConstants.PaymentType];
            var paymentMethod = order[OrderPropertyConstants.PaymentMethod];
            sitecorePaymentService.UpdatePaymentMethod(clientId, orderId, SiteSettings.PortalType, paymentType, paymentMethod);
        }

        /// <summary>
        /// updates Basket Order Payment details
        /// </summary>
        /// <param name="orderId">orderId</param>
        /// <param name="clientId">clientId</param>
        /// <returns>Returns Basket Info Object</returns>
        public static BasketInfo GetPopulateBasketPaymentDetails(int orderId, string clientId)
        {
            try
            {
                UpdatePaymentMethod(orderId, clientId);

                BasketInfo paymentDetails = new BasketInfo();

                ISitecorePaymentService sitecorePaymentService = new SitecorePaymentService();
                var paymentMethod = sitecorePaymentService.GetPaymentMethod(orderId);

                if (IsBasketExists())
                {
                    if (TransactionLibrary.GetBasket(false).PurchaseOrder.OrderLines.Count > 0)
                    {
                        //SubrateService subrateService = new SubrateService();
                        //List<model.OrderLineSubrate> orderLineSubrateList = GetOrderSubrates(GetBasketOrderId());//subrateService.GetOrderSubrate(orderId, clientId);

                        //decimal price = 0;
                        //decimal processingFee = 0;
                        //decimal commission = 0;
                        ////decimal totalPaybaleToday = 0;
                        //decimal totalPayabaleAtAccomodation = 0;
                        //decimal processingFeeOrderLine = 0;

                        //decimal itemprice = 0;
                        //decimal itemcommission = 0;
                        //decimal itemtotalPaybaleToday = 0;
                        //decimal itemtotalPayabaleAtAccomodation = 0;

                        //if (orderLineSubrateList.Count == 0)
                        //{
                        //    return null;
                        //}

                        //if (orderLineSubrateList.Count > 0)
                        //{
                        //    if (orderLineSubrateList.SelectMany(i => i.Subrates).Count() > 0)
                        //    {
                        //        var processingFeeRecord = orderLineSubrateList.SelectMany(i => i.Subrates.Select(item => new { SubrateCode = item.SubrateCode, TotalAmount = item.TotalAmount, OrderLineId = i.OrderLineId }))
                        //                                             .Where(i => i.SubrateCode == ProcessingFee)
                        //                                             .OrderBy(i => i.TotalAmount)
                        //                                             .Reverse()
                        //                                             .FirstOrDefault();
                        //        processingFee = processingFeeRecord.TotalAmount;
                        //        processingFeeOrderLine = processingFeeRecord.OrderLineId;
                        //    }

                        //    foreach (var orderLineSubrate in orderLineSubrateList)
                        //    {
                        //        itemprice = 0;
                        //        itemtotalPayabaleAtAccomodation = 0;
                        //        itemcommission = 0;
                        //        itemtotalPaybaleToday = 0;

                        //        OrderLine orderLine = UCommerce.Api.TransactionLibrary.GetBasket(false).PurchaseOrder.OrderLines.Where(i => i.OrderLineId == orderLineSubrate.OrderLineId).FirstOrDefault();

                        //        if (orderLine.Total.HasValue)
                        //            itemprice = (orderLine.Total.Value - (orderLine.VAT * orderLine.Quantity));
                        //        else
                        //            throw new AffinionException("No pricing information found for orderline: " + orderLine.OrderLineId);
                        //        price += itemprice;

                        //        if (orderLineSubrate.Subrates.Count == 0)
                        //            break;
                        //        itemcommission = (orderLineSubrate.Subrates.Where(i => i.SubrateCode == "LBCOM").FirstOrDefault().TotalAmount);
                        //        commission += itemcommission;

                        //        var selectedPayment = paymentMethod.FirstOrDefault(i => i.OrderLineId == orderLine.OrderLineId);
                        //        string paymentMethodName = string.Empty;
                        //        if (selectedPayment != null)
                        //            paymentMethodName = selectedPayment.Paymentmethod;

                        //        switch (paymentMethodName)
                        //        {
                        //            case Constants.PaymentMethod.FullPayment:
                        //                break;
                        //            case Constants.PaymentMethod.PartialPayment:
                        //                int percentage = 0;
                        //                int.TryParse(orderLine[OrderPropertyConstants.PercentageOfAmount], out percentage);
                        //                itemtotalPaybaleToday = (Convert.ToDecimal(percentage) / 100) * itemprice;
                        //                itemtotalPayabaleAtAccomodation = itemprice - itemtotalPaybaleToday;
                        //                break;
                        //            case Constants.PaymentMethod.DepositOnly:
                        //            default:
                        //                itemtotalPayabaleAtAccomodation = itemprice - itemcommission;
                        //                break;
                        //        }
                        //        orderLine[OrderPropertyConstants.ProviderAmountPaidCheckOut] = (itemprice - itemcommission - itemtotalPayabaleAtAccomodation).ToString();
                        //        orderLine[OrderPropertyConstants.IsProcessingFeeCharged] = (orderLine.OrderLineId == processingFeeOrderLine).ToString().ToLower();
                        //        orderLine.Save();
                        //        totalPayabaleAtAccomodation += itemtotalPayabaleAtAccomodation;
                        //    }
                        //}

                        //if (!(commission > 0))
                        //    return null;

                        //decimal bookingDeposit = Decimal.Round(commission + processingFee, 2);
                        //decimal processingfee = Decimal.Round(processingFee, 2);

                        //paymentDetails.BookingDeposit = bookingDeposit.ToString("0.00");
                        //paymentDetails.ProcessingFee = processingfee.ToString("0.00");
                        //paymentDetails.TotalPrice = (price + processingfee).ToString("0.00");
                        //paymentDetails.TotalPayableAtAccommodation = totalPayabaleAtAccomodation.ToString("0.00");
                        //paymentDetails.TotalPayableToday = (price + processingfee - totalPayabaleAtAccomodation).ToString("0.00");
                    }
                }
                return paymentDetails;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }

        }

        /// <summary>
        /// Get the total payable today from sub rates
        /// </summary>
        /// <param name="orderId">Current purchase order id</param>
        /// <param name="clientId">Current client id</param>
        /// <returns>Returns Today's Total payable fee for the transaction</returns>
        public static decimal GetTotalPayableToday(int orderId, string clientId)
        {
            decimal processingFee = 0;
            decimal bookingDeposit = 0;

            if (IsBasketExists())
            {
                if (TransactionLibrary.GetBasket(false).PurchaseOrder.OrderLines.Count > 0)
                {
                    SubrateService subrateService = new SubrateService();
                    ///Get subrates for orderlines by order id and client id
                    List<model.OrderLineSubrate> orderLineSubrateList = GetOrderSubrates(orderId);
                    ///Calculate total commission & processing fee for a order
                    if (orderLineSubrateList.Count > 0)
                    {
                        if (orderLineSubrateList.SelectMany(i => i.Subrates).Count() > 0)
                        {
                            processingFee = orderLineSubrateList.SelectMany(i => i.Subrates)
                                                                 .Where(i => i.SubrateCode == ProcessingFee)
                                                                 .Select(t => t.TotalAmount)
                                                                 .Max();
                        }
                    }

                    foreach (var orderLineSubrate in orderLineSubrateList)
                    {
                        bookingDeposit += orderLineSubrate.Subrates.Where(i => i.SubrateCode.Equals("LBCOM")).FirstOrDefault().TotalAmount;
                    }
                }
            }
            return processingFee + bookingDeposit;
        }

        /// <summary>
        /// Get billing currency
        /// </summary>
        /// <returns>Returns the currency</returns>
        public static string GetPaymentCurrency()
        {
            try
            {
                return TransactionLibrary.GetBasket(false).PurchaseOrder.BillingCurrency.ISOCode;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Get the basket order total amount to pay
        /// </summary>
        /// <returns>Returns the payment Total</returns>
        public static double GetPaymentTotal()
        {
            try
            {
                return (double)TransactionLibrary.GetBasket(false).PurchaseOrder.OrderTotal;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Update order status
        /// </summary>
        /// <param name="order">Current purchase order</param>
        /// <param name="status">Status to be</param>
        public static void UpdateOrderStatus(PurchaseOrder order, int status)
        {
            try
            {
                var newOrderStatus = OrderStatus.All().Single(x => x.OrderStatusId == status);
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, "REALEX: newOrderStatus:" + newOrderStatus.OrderStatusId + "");
                var orderService = ObjectFactory.Instance.Resolve<IOrderService>();
                if (orderService == null)
                {
                    Diagnostics.Trace(DiagnosticsCategory.ClientPortal, "REALEX: orderservice instance is null");
                }
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, "REALEX: orderservice instance is NOT null");
                //Diagnostics.Trace(DiagnosticsCategory.ClientPortal, "REALEX: orderservice instance: order id:" + order.OrderId + "");
                orderService.ChangeOrderStatus(order, newOrderStatus);
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Get current purchase order
        /// </summary>
        /// <param name="orderId">orderId</param>
        /// <returns>Returns current purchase order</returns>
        public static PurchaseOrder GetPurchaseOrder(int orderId)
        {
            try
            {
                return PurchaseOrder.Get(orderId);
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Check the basket exists
        /// </summary>
        /// <returns>Returns true if basket exists</returns>
        private static bool IsBasketExists()
        {
            return (UCommerce.Api.TransactionLibrary.HasBasket());
        }

        /// <summary>
        /// Add subrates for the Client Item
        /// </summary>
        /// <param name="clientItem">clientItem</param>
        static void AddSubrates(Item clientItem)
        {

            SubrateService subrateService = new SubrateService();
            string clientId = clientItem.ID.Guid.ToString();
            List<model.OrderLineSubrate> orderLineSubrateList = subrateService.GetOrderSubrate(GetBasketOrderId(), clientId, TransactionLibrary.GetBasket(false).PurchaseOrder.BillingCurrency.ISOCode);
            if (orderLineSubrateList.Count > 0)
            {
                UpdateOrderLineSubrate(orderLineSubrateList, GetBasketOrderId());
            }
        }

        /// <summary>
        /// Add to custom fields to basket related tables
        /// </summary>
        /// <param name="basketInfo"></param>
        /// <param name="campaignItemId">campaignItemId</param>
        /// <param name="awardType">OfferType</param>
        private static void UpdateBasketCustomData(BasketInfo basketInfo, int campaignItemId, OfferTypes awardType)
        {
            try
            {
                //uCommerceConnectionFactory.UpdateCustomOrderLineData(basketInfo, GetBasketOrderId());
                var OrderLineID = from lines in UCommerce.Api.TransactionLibrary.GetBasket(false).PurchaseOrder.OrderLines
                                  where lines.Sku == basketInfo.Sku && lines.PurchaseOrder.OrderId == GetBasketOrderId()
                                  select lines.OrderLineId;

                foreach (var olId in OrderLineID)
                {
                    uCommerceConnectionFactory.AddRoomReservation(basketInfo, olId);

                    //// store campaign information for offers
                    //if (campaignItemId > 0)
                    //{
                    //    LoyaltyBuildTransactionLibrary.AppliedCampaignOnOrderLine(basketInfo.Sku, campaignItemId, olId);
                    //}

                    //// store award information for offers
                    //LoyaltyBuildTransactionLibrary.AppliedAwardOnOrderLine(basketInfo.Sku, awardType.ToString(), olId);

                    //// store search criteria
                    //SearchCriteria criteria = new SearchCriteria();
                    //criteria.ArrivalDate = DateTimeHelper.ParseDate(basketInfo.CheckinDate);
                    //criteria.Night = int.Parse(basketInfo.Nights);
                    //criteria.NumberOfAdult = int.Parse(basketInfo.NoOfAdults);
                    //criteria.NumberOfChild = int.Parse(basketInfo.NoOfChildren);
                    //LoyaltyBuildTransactionLibrary.AddAddOnProductToOrderLine(basketInfo.Sku, null, null, criteria, olId);
                    //Library.ExecuteBasketPipeline();
                }

                // Avoid updating bulk order
                //uCommerceConnectionFactory.AddRoomReservation(basketInfo, OrderLineID.Max());
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Create datatable including subrates for all orderlines
        /// </summary>
        /// <param name="orderlineSubrates">Current orderlines' subrates</param>
        /// <returns>Returns datatable with bulk subrates</returns>
        private static DataTable CreateSubrateDataTable(List<model.OrderLineSubrate> orderlineSubrates)
        {
            DataTable subratesDataTable = new DataTable("SubRatesDataTable");

            // Add three column objects to the table. 
            DataColumn orderLineIdColumn = new DataColumn();
            orderLineIdColumn.DataType = System.Type.GetType("System.Int32");
            orderLineIdColumn.ColumnName = "OrderLineID";
            subratesDataTable.Columns.Add(orderLineIdColumn);

            DataColumn subrateIDColumn = new DataColumn();
            subrateIDColumn.DataType = System.Type.GetType("System.String");
            subrateIDColumn.ColumnName = "SubrateItemCode";
            subratesDataTable.Columns.Add(subrateIDColumn);

            DataColumn amountColumn = new DataColumn();
            amountColumn.DataType = System.Type.GetType("System.Decimal");
            amountColumn.ColumnName = "Amount";
            subratesDataTable.Columns.Add(amountColumn);

            DataColumn discountColumn = new DataColumn();
            discountColumn.DataType = System.Type.GetType("System.Decimal");
            discountColumn.ColumnName = "Discount";
            subratesDataTable.Columns.Add(discountColumn);

            DataColumn isIncludedInUnitPriceColumn = new DataColumn();
            isIncludedInUnitPriceColumn.DataType = System.Type.GetType("System.Boolean");
            isIncludedInUnitPriceColumn.ColumnName = "IsIncludedInUnitPrice";
            subratesDataTable.Columns.Add(isIncludedInUnitPriceColumn);

            foreach (var orderLineSubrate in orderlineSubrates)
            {
                int orderLineId = orderLineSubrate.OrderLineId;
                List<model.Subrates> subrateList = orderLineSubrate.Subrates;
                OrderLine orderLine = TransactionLibrary.GetBasket(false).PurchaseOrder.OrderLines.Where(ol => ol.OrderLineId == orderLineId).FirstOrDefault();

                foreach (model.Subrates subrate in subrateList)
                {
                    DataRow row = subratesDataTable.NewRow();
                    row["OrderLineId"] = orderLineId;
                    row["SubrateItemCode"] = subrate.SubrateCode;

                    if (orderLineSubrate.IsOfferSubrate || string.Equals(subrate.SubrateCode, "CCPROFEE"))
                    {
                        row["Amount"] = subrate.TotalAmount;
                    }
                    else
                    {
                        row["Amount"] = subrate.TotalAmount * orderLine.Quantity;
                    }

                    row["Discount"] = 0;
                    row["IsIncludedInUnitPrice"] = string.Equals(subrate.SubrateCode, "LBCOM") ? true : false;
                    subratesDataTable.Rows.Add(row);
                }
            }

            return subratesDataTable;
        }

        /// <summary>
        /// Get the Room Reservation By Order Line Id
        /// </summary>
        /// <param name="OrderLineId">OrderLineId</param>
        public static List<RoomReservation> GetRoomReservationByOrderLineId(string OrderLineId)
        {
            try
            {
                DataSet ds = uCommerceConnectionFactory.GetRoomReservationByOrderLineId(OrderLineId);
                List<RoomReservation> ListRoomReservation = new List<RoomReservation>();
                int guestnumber = 0, adultnumber = 0, childnumber = 0;

                if (ds != null && ds.Tables.Count > 0)
                {
                    // Iterate through the records
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        int.TryParse((row["NoOfGuest"]).ToString(), out guestnumber);
                        int.TryParse((row["NoOfAdult"]).ToString(), out adultnumber);
                        int.TryParse((row["NoOfChild"]).ToString(), out childnumber);
                        ListRoomReservation.Add(new RoomReservation
                        {
                            //RoomReservationId = Convert.ToString(row["RoomReservationId"]),
                            NoOfGuest = (row.IsNull("NoOfGuest")) ? 1 : guestnumber,
                            NoOfAdults = (row.IsNull("NoOfAdult")) ? 1 : adultnumber,
                            NoOfChildren = (row.IsNull("NoOfChild")) ? 0 : childnumber,
                            GuestName = Convert.ToString(row["GuestName"]),
                            SpecialRequest = Convert.ToString(row["SpecialRequest"]),
                            OrderLineId = Convert.ToString(row["OrderLineId"]),
                            CheckinDate = row["CheckInDate"] != null && !string.IsNullOrEmpty(Convert.ToString(row["CheckInDate"])) ? Convert.ToDateTime(row["CheckInDate"]) : DateTime.MinValue,
                            CheckOutDate = row["CheckOutDate"] != null && !string.IsNullOrEmpty(Convert.ToString(row["CheckOutDate"])) ? Convert.ToDateTime(row["CheckOutDate"]) : DateTime.MinValue,
                        });
                    }
                }
                return ListRoomReservation;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Calculates the basket item price
        /// </summary>
        /// <param name="orderId">Ucommerce orderId</param>
        /// <returns>Price for the basket item</returns>
        public static double GetItemPrice(int orderId)
        {
            try
            {
                double price = 0;
                if (IsBasketExists())
                {
                    if (TransactionLibrary.GetBasket(false).PurchaseOrder.OrderLines.Count > 0)
                    {
                        var orderlines = TransactionLibrary.GetBasket(false).PurchaseOrder.OrderLines.ToList();
                        foreach (var orderLine in orderlines)
                        {
                            int numberOfNight = uCommerceConnectionFactory.GetNoOfNights(orderLine.Id);
                            price = price + (double)Decimal.Round((orderLine.Price - orderLine.Discount) * orderLine.Quantity * numberOfNight, 2);
                        }
                    }
                }
                return price;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Populate Child Age form Database
        /// </summary>
        /// <param name="OrderLineId">OrderLineId</param>
        /// <returns>List of Child Age</returns>
        public static List<int> PopulateChildAgeFromDB(string OrderLineId)
        {
            try
            {
                DataSet ds = uCommerceConnectionFactory.GetChildAgeDetailsByOrderLineID(OrderLineId);
                List<int> ChildAgeList = new List<int>();

                if (ds != null && ds.Tables.Count > 0)
                {
                    // Iterate through the records
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        //ChildAgeList.Add((row.IsNull("Age") ? 0 : (int)(row["Age"])));
                        if (row["Age"] != null)
                        {
                            ChildAgeList.Add((int)(row["Age"]));
                        }
                        else
                        {
                            ChildAgeList.Add(0);
                        }
                    }
                }

                return ChildAgeList;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, null);
                throw;
            }
        }

        /// <summary>
        /// Gets the Child ages at reservation
        /// </summary>
        /// <returns>Returns the child ages at Reservation</returns>
        public static string GetReservationChildrenAgeSet(int orderLineId)
        {
            try
            {
                string childAge = string.Empty;
                DataSet ds = new DataSet();
                ds = uCommerceConnectionFactory.GetRoomReservationAgeInfo(orderLineId);
                List<string> ageList = new List<string>();

                if (ds != null && ds.Tables.Count > 0)
                {
                    // Iterate through the records
                    foreach (DataRow row2 in ds.Tables[0].Rows)
                    {
                        string Age = Convert.ToString((row2["Age"]));
                        ageList.Add(Age);
                    }
                }

                childAge = String.Join<string>(",", ageList);
                return childAge;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Get the Room Reservation By Order Line Id
        /// </summary>
        /// <param name="OrderLineId">OrderLineId</param>
        /// <returns>Returns the Room reservation Details for the orderlineId</returns>
        public static RoomReservation GetRoomReservationDetailsByOrderLineId(string OrderLineId)
        {
            try
            {
                DataSet ds = uCommerceConnectionFactory.GetRoomReservationDetailsByOrderLineId(OrderLineId);
                RoomReservation RoomReservation = new RoomReservation();

                if (ds != null && ds.Tables.Count > 0)
                {
                    // Iterate through the records
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {


                        //RoomReservation.RoomReservationId = Convert.ToString(row["RoomReservationId"]);
                        RoomReservation.NoOfGuest = (row.IsNull("NoOfGuest") ? 1 : (int)(row["NoOfGuest"]));
                        RoomReservation.NoOfAdults = (row.IsNull("NoOfAdult") ? 1 : (int)(row["NoOfAdult"]));
                        RoomReservation.NoOfChildren = (row.IsNull("NoOfChild") ? 0 : (int)(row["NoOfChild"]));
                        RoomReservation.GuestName = Convert.ToString(row["GuestName"]);
                        RoomReservation.SpecialRequest = Convert.ToString(row["SpecialRequest"]);
                        RoomReservation.OrderLineId = Convert.ToString(row["OrderLineId"]);
                        RoomReservation.CheckinDate = row["CheckInDate"] != null && !string.IsNullOrEmpty(Convert.ToString(row["CheckInDate"])) ? Convert.ToDateTime(row["CheckInDate"]) : DateTime.MinValue;
                        RoomReservation.CheckOutDate = row["CheckOutDate"] != null && !string.IsNullOrEmpty(Convert.ToString(row["CheckOutDate"])) ? Convert.ToDateTime(row["CheckOutDate"]) : DateTime.MinValue;

                    }
                }
                return RoomReservation;
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                /// Throw a custom exception with a custom message
                throw new AffinionException(exception.Message, exception);
            }
        }

        /// <summary>
        /// Generate BookingRefence RandomNumber
        /// </summary>
        /// <param name="r">Random number</param>
        public static string GenerateBookingReference(Random r)
        {
            int maxdigits = 10; // Change to needed # of digits
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < maxdigits; i++)
            {
                result.Append(r.Next(10)); // Append a number from 0 to 9
            }
            string key = result.ToString();
            return key;
        }

        /// <summary>
        /// Get PaymentMethod
        /// </summary>
        //public static string SetPayMethod(string clientId, string paymentType, string portal, string providerType)
        //{
        //    PaymentRuleService paymentservicerule=new PaymentRuleService();
        //    paymentservicerule.GetPaymentMethod(clientId, paymentType, portal,providerType);

        //    //TransactionLibrary.GetBasket(IsBasketExists()).Save();
        //    var order = TransactionLibrary.GetBasket(false).PurchaseOrder;
        //    order["clientId"] = PaymentType.ID.ToString();
        //    order.Save();


        //}

        #endregion
    }
}