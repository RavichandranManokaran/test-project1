﻿CREATE TABLE [dbo].[LB_SupplierTarget] (
    [SupplierTargetId] INT            NOT NULL,
    [Name]             NVARCHAR (MAX) NOT NULL,
    [SupplierId]       NVARCHAR (MAX) CONSTRAINT [DF__uCommerce__Suppl__4CC05EF3] DEFAULT (NULL) NOT NULL,
    CONSTRAINT [PK_uCommerce_SupplierTarget] PRIMARY KEY CLUSTERED ([SupplierTargetId] ASC),
    CONSTRAINT [FK_uCommerce_SupplierTarget_uCommerce_SupplierTarget] FOREIGN KEY ([SupplierTargetId]) REFERENCES [dbo].[uCommerce_Target] ([TargetId])
);

