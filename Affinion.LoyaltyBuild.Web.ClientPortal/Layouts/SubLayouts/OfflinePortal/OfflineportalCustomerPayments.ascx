﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflineportalCustomerPayments.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflineportalCustomerPayments" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <sc:text id="PaymentLabel" field="PaymentText" runat="server" />
            </h4>
        </div>
        <div class="panel-body alert-info">
            <div class="panel-body alert-info">

                <div class="col-sm-12 no-padding-m">
                    <div class="content_bg_inner_box alert-info">
                        <div class="col-sm-6 no-padding-m">
                            <div class="row margin-row">
                                <div class="col-sm-5">
                                    <span class="bask-form-titl font-size-12">
                                        <sc:text id="CurrencyLabel" field="CurrencyText" runat="server" />
                                        <span class="accent58-f"></span></span>
                                </div>
                                <div class="col-sm-7">
                                    <asp:DropDownList class="frm_val" ID="ddlCurrency" runat="server" Enabled="False" CssClass="form-control font-size-12" />
                                </div>
                            </div>
                            <div class="row margin-row">
                                <div class="col-sm-5">
                                    <span class="bask-form-titl font-size-12">
                                        <sc:text id="PaymentMethodLabel" field="PaymentMethod" runat="server" />
                                        <span class="accent58-f"></span></span>
                                </div>
                                <div class="col-sm-7">
                                    <asp:DropDownList class="frm_val" ID="ddlPaymentmethod" runat="server" CssClass="form-control font-size-12">
                                        <%-- <asp:ListItem Text="card" value="card"></asp:ListItem>--%>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="row margin-row">
                                <div class="col-sm-5">
                                    <span class="bask-form-titl font-size-12" readonly="True">
                                        <sc:text id="TotalAmountLabel" field="TotalAmount" runat="server" />
                                        <span class="accent58-f"></span></span>
                                </div>
                                <div class="col-sm-7">
                                    <asp:TextBox ID="txtAmount" runat="server" CssClass="form-control font-size-12" Enabled="false" />
                                </div>
                            </div>
                            <div class="row margin-row">
                                <div class="col-sm-12">
                                    <%--<asp:Button ID="BtnPayNow" OnClick="BtnPayNow_Click" class="btn btn-default pro-check" runat="server" Text="Pay" />--%>
                                    <button type="submit" id="BtnPayNow" runat="server" onserverclick="BtnPayNow_Click" class="btn btn-default pro-check">
                                        <sc:text id="PayButtonText" field="PayLabel" runat="server" />
                                    </button>
                                    <a href=<%=GetUrl("/paymenthistory?olid=" + QueryString("olid")) %> class="btn btn-default pro-check">Return</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
