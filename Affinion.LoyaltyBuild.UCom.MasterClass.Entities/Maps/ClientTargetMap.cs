﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Maps
{
    public class ClientTargetMap : SubclassMap<ClientTarget>
    {
        // Methods
        public ClientTargetMap()
        {
            base.Table("LB_ClientTarget");
            base.KeyColumn("ClientTargetId");
            base.Map(x => x.ClientTargetId).ReadOnly();
            base.Map(x => x.ClientGUID).Not.Nullable();
            base.Map(x => x.CampaginItem).Not.Nullable();
        }

    }

}
