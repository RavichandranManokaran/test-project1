﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductDetailsView.ascx.cs" Inherits="Affinion.LoyaltyBuild.UCom.MasterClass.Website.Layouts.Sublayouts.ProductDetailsView" %>

<h1>
    <asp:Label id="ProductSkuLabel" runat="server"/>
</h1>
<asp:DropDownList runat="server" DataTextField="VariantSku" DataValueField="VariantSku" ID="ProductVariantsDropDownList" Visible="false"/>

<asp:Button id="AddToBasketButton" runat="server" OnClick="AddToBasketButton_OnClick" Text="Add to basket" />