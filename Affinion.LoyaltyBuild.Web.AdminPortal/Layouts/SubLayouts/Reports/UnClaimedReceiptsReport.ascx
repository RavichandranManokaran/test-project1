﻿<%@ control language="C#" autoeventwireup="true" codebehind="UnClaimedReceiptsReport.ascx.cs" inherits="Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.Reports.UnClaimedReceiptsReport" %>
<div class="content">
    <div class="content-body">
     <div id="errMsg" runat="server" clientidmode="Static" class="col-sm-12 alert alert-danger" >
         <sc:Text id="scError" runat="server"/>
        </div>
        <div>
            <div class="row">
                <div class="col-sm-12 marT20">
                    <div class="accordian-outer">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading bg-primary claim_titl">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="show-hide-list">Unclaimed Receipts Report</a>
                                    </h4>
                                </div>
                                <div id="collapse1" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div>
                                            <div class="content_bg_inner_box summary-detail receipt_body">
                                                <div class="col-xs-12 col-md-12">
                                                    <div class="mybooking ">
                                                        <h2 class="no-margin accent54-f"><sc:Text id="scFill" runat="server"/></h2>
                                                        <div class="row margin-row">
                                                            <div class="col-sm-5"><span class="bask-form-titl font-size-12"><sc:Text id="scStg" runat="server"/></span>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <input type="text" id="txtSTG_UCR" clientidmode="Static"  runat="server" class="form-control font-size-12">
                                                            </div>
                                                        </div>
                                                        <div class="row margin-row">
                                                            <div class="col-sm-5"><span class="bask-form-titl font-size-12"><sc:Text id="scSEK" runat="server"/></span></div>
                                                            <div class="col-sm-7">
                                                                <input type="text" id="txtSEK_UCR" clientidmode="Static" runat="server" class="form-control font-size-12">
                                                            </div>
                                                        </div>
                                                        <div class="row margin-row">
                                                            <div class="col-sm-5"><span class="bask-form-titl font-size-12"><sc:Text id="scNok" runat="server"/></span></div>
                                                            <div class="col-sm-7">
                                                                <input type="text" id="txtNOK_UCR" clientidmode="Static" runat="server" class="form-control font-size-12">
                                                            </div>
                                                        </div>
                                                        <div class="row margin-row">
                                                            <div class="col-sm-5"><span class="bask-form-titl font-size-12"><sc:Text id="scDKK" runat="server"/></span></div>
                                                            <div class="col-sm-7">
                                                                <input type="text" id="txtDKK_UCR" clientidmode="Static" runat="server" class="form-control font-size-12">
                                                            </div>
                                                        </div>
                                                        <div class="row margin-row">
                                                            <div class="col-sm-5"><span class="bask-form-titl font-size-12"><sc:Text id="scCHF" runat="server"/></span></div>
                                                            <div class="col-sm-7">
                                                                <input type="text" id="txtCHF_UCR" clientidmode="Static" runat="server" class="form-control font-size-12">
                                                            </div>
                                                        </div>
                                                        <div class="row margin-row">
                                                            <div class="col-sm-5"><span class="bask-form-titl font-size-12"><sc:Text id="scUSD" runat="server"/></span></div>
                                                            <div class="col-sm-7">
                                                                <input type="text" id="txtUSD_UCR" clientidmode="Static" runat="server" class="form-control font-size-12">
                                                            </div>
                                                        </div>
                                                        <div class="row margin-row">
                                                            <div class="col-sm-5"><span class="bask-form-titl font-size-12"><sc:Text id="scUnclaimed" runat="server"/>
                                                                  </span></div>
                                                            <div class="col-sm-7">
                                                                <div class="form-group">
                                                                    <div class="input-group date">
                                                                        <input type="text" id="ReceiptDate" runat="server" clientidmode="Static" class="form-control datepicker width-85 pull-left font-size-12" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 pull-right no-padding">
                                                            <asp:Button type="button" id="HtmlReport" runat="server" OnClientClick="return ValidateMandatory();" OnClick="HtmlReport_Click" clientidmode="Static" Text="Html report" class="btn btn-info add-break font-size-12 pull-right"/>
                                                            <asp:Button type="button" id="ExcelReport" runat="server" OnClientClick="return ValidateMandatory();"  OnClick="ExcelReport_Click" clientidmode="Static" Text="Excel report" class="btn btn-info add-break font-size-12 pull-right marginR6"/>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <%--Html Report --%>
            <div class="row" id="htmlReportGrid" runat="server" style="display:none" >
                <div class="col-sm-12 marT20">
                    <div>
                        <div class="panel panel-default">
                            <div class="panel-heading claim_titl">
                                <h4 class="panel-title"><sc:Text id="scUnclaimedReceipts" runat="server"/> 
                                </h4>
                            </div>
                              <div  id="totalCount"  runat="server"  class="totalCountpad"> Unclaimed receipts reports Total of <span> <%= TotalOfRecords %> </span> record(s) found. </div>
                            <div class="panel-body">
                                 <div class="receipt_body">
                                <%-- Result Grid--%>
                                <div runat="server" id="ResultDiv" clientidmode="Static">
                                    <div id="data-grid" class="table-responsive">
                                        <asp:gridview id="ResultGridView" runat="server" allowpaging="True" pagesize="10"
                                            autogeneratecolumns="false"
                                            onpageindexchanging="ResultGridView_PageIndexChanging"
                                            selectedrowstyle-font-bold="true" cssclass="table table-striped table-borderedr" 
                                            PagerSettings-NextPageText=" Next " PagerSettings-PreviousPageText=" Previous " PagerSettings-Mode="NumericFirstLast" PagerSettings-FirstPageText=" First " PagerSettings-LastPageText=" Last " PagerSettings-Position="Bottom" PagerStyle-Font-Bold="true" PagerStyle-CssClass="pagination-grid" >
                                        <Columns>
                                            <asp:BoundField DataField="Count" HeaderText="Bookings" />
                                            <asp:BoundField DataField="Amount" HeaderText="Revenue" DataFormatString="{0:0.00}" />
                                            <asp:BoundField DataField="ISOCode" HeaderText="Currency symbol" />
                                            <asp:BoundField DataField="CurrencyTypeId" HeaderText="Currency ID" />
                                            <asp:BoundField DataField="Month" HeaderText="Month" />
                                            <asp:BoundField DataField="Year" HeaderText="Year" />
                                            <asp:BoundField DataField="AmountInEuro" HeaderText="Euro Equivalent" DataFormatString="{0:0.0000000}" />
                                            <asp:BoundField DataField="ProviderName" HeaderText="Provider Name" />
                                        </Columns>
                                    </asp:gridview>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        jQuery(function () {
            jQuery("#ReceiptDate").datepicker({
                defaultDate: new Date(),
                dateFormat: "<%=Affinion.LoyaltyBuild.Common.Utilities.DateTimeHelper.DateFormatJavaScript%>",
                minDate: -180,
                maxDate: 0,
                numberOfMonths: 1,
                showOn: "both",
                buttonImage: "images/calendar.png",
                buttonImageOnly: false,
                buttonClass: "glyphicon-calendar"
            }).next(".ui-datepicker-trigger").addClass("btn btn-default glyphicon glyphicon-calendar calander-icon pull-left");
        });
    });
    function ValidateMandatory() {

        re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;

                var valSTG_UCR = document.getElementById("txtSTG_UCR").value;      
                var valSEK_UCR = document.getElementById("txtSEK_UCR").value;
                var valNOK_UCR = document.getElementById("txtNOK_UCR").value;
                var valCHF_UCR = document.getElementById("txtCHF_UCR").value;
                var valUSD_UCR = document.getElementById("txtUSD_UCR").value;
                var valDKK_UCR = document.getElementById("txtDKK_UCR").value;
                if ($("#ReceiptDate").val() == "")
                {
                    document.getElementById("errMsg").style.display = 'block';
                    document.getElementById("errMsg").innerText = 'Fill in the Parameters for your UCR Report';
                    return false;
                }
                
                if (!$("#ReceiptDate").val().match(re))
                {
                    document.getElementById("errMsg").style.display = 'block';
                    document.getElementById("errMsg").innerText = 'Invalid  date or date format';
                    return false;
                }

                if (!valSTG_UCR || !valSEK_UCR || !valNOK_UCR || !valCHF_UCR || !valUSD_UCR || !valDKK_UCR) {
                    document.getElementById("errMsg").style.display = 'block';
                    document.getElementById("errMsg").innerText = 'Fill in the Parameters for your UCR Report';
                    return false;
                }
                if (isNaN(valSTG_UCR) || isNaN(valSEK_UCR) || isNaN(valNOK_UCR) || isNaN(valCHF_UCR) || isNaN(valUSD_UCR) || isNaN(valDKK_UCR))
                {
                    document.getElementById("errMsg").style.display = 'block';
                    document.getElementById("errMsg").innerText = 'Fill in the currency with numeric values';
                    return false;
                }
                else
                {
                    return true;
                }
            }
       
</script>
