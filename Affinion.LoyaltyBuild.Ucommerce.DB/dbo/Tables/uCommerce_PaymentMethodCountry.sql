﻿CREATE TABLE [dbo].[uCommerce_PaymentMethodCountry] (
    [PaymentMethodId] INT NOT NULL,
    [CountryId]       INT NOT NULL,
    CONSTRAINT [uCommerce_PK_PaymentMethodCountry] PRIMARY KEY CLUSTERED ([PaymentMethodId] ASC, [CountryId] ASC),
    CONSTRAINT [uCommerce_FK_PaymentMethodCountry_Country] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[uCommerce_Country] ([CountryId]),
    CONSTRAINT [uCommerce_FK_PaymentMethodCountry_PaymentMethod] FOREIGN KEY ([PaymentMethodId]) REFERENCES [dbo].[uCommerce_PaymentMethod] ([PaymentMethodId])
);

