﻿namespace Affinion.Loyaltybuild.BedBanks.CAPI.Model
{
    /// <summary>
    /// Summary description for RoomPricing
    /// </summary>
    public class RoomPricing
    {
        public string CurrencyCode { get; set; }
        public double Tax { get; set; }
        public double Fee { get; set; }
        public double Total { get; set; }
        public double ExchangeRate { get; set; }

        public RoomPricing(string pCurrencycode, double pTax, double pFee, double pTotal, double pExchangerate)
        {
            CurrencyCode = pCurrencycode;
            Tax = pTax;
            Fee = pFee;
            Total = pTotal;
            ExchangeRate = pExchangerate;
        }
    }
}