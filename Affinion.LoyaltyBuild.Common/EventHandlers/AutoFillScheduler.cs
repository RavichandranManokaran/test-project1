﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           AutoFillScheduler.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Handling scheduler tasks
/// </summary>

#region Using Directives
using Sitecore.Data;
using System;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Data.Items;
using System.Collections.Generic;
using Sitecore.Collections;
using System.Linq;
#endregion
namespace Affinion.LoyaltyBuild.Common.EventHandlers
{
    /// <summary>
    /// Class use to handle scheduler related events.
    /// </summary>
    public class AutoFillScheduler
    {

        #region Class variables
        private const string autocompletelistFolderPath = "/sitecore/content/client-portal/global/_supporting-content/autocompletelist";
        private const string autocompletelistSuppliersFolderPath = "/sitecore/content/client-portal/global/_supporting-content/autocompletelistsuppliers";

        private const string autocompletelist = "autocompletelist";
        private const string autocompletelistsuppliers = "autocompletelistsuppliers";

        private const string rootPath = "/sitecore/content/client-portal/global/_supporting-content";
        private const string autoCompletetemplatePath = "/sitecore/templates/Affinion/ClientPortal/AutoCompleteItem";
        private const string folderTemplatePath = "/sitecore/templates/Common/Folder";
        private const string hotelSelectionFastQuery = "/sitecore/content/#admin-portal#/#supplier-setup#//*[@@TemplateID = '{A7235C94-2EF5-4A3B-87DB-0D4DCF10B4DB}']";

        private List<string> listRefrenceData = new List<string>();
        private List<string> refrenceDataSupplierslist = new List<string>();
        #endregion

        /// <summary>
        /// Run the scheduler
        /// </summary>
        public void Run()
        {
            try
            {
                Sitecore.Context.Job.Status.LogInfo("Scheduler started");

                //TODO: Remove item from autocomplete list
                //  item path /sitecore/content/client-portal/global/_supporting-content/autocompletelist
                //  insert new record using autocomplete template
                //
                //  Country, Location, MapLocation - referance data 
                //  Town,HotelName -

                //First get the parent item from the master database
                Database masterDb = Sitecore.Configuration.Factory.GetDatabase("master");

                // Delete the item from the given path
                Item tempItem = masterDb.Items[autocompletelistFolderPath];
                tempItem.DeleteChildren();

                // Delete the item from the given path - Suppliers
                Item tempItemSuppliers = masterDb.Items[autocompletelistSuppliersFolderPath];
                tempItemSuppliers.DeleteChildren();

                // Creating the base folder: autocompletelist under /sitecore/content/client-portal/global/_supporting-content/autocompletelist
                masterDb.AddItem(rootPath, folderTemplatePath, autocompletelist);

                // Creating the base folder: autocompletelist under /sitecore/content/client-portal/global/_supporting-content/autocompletelistsuppliers
                masterDb.AddItem(rootPath, folderTemplatePath, autocompletelistsuppliers);

                // Clear the list before filling with data
                listRefrenceData.Clear();

                // create a string list to hold data.
                Item referenceItem = masterDb.GetItem("{D69F4597-CF81-46EA-A232-FCF88069B6B1}");

                // create list of countries
                PopulatereferenceList(referenceItem);

                // create list of Location
                referenceItem = masterDb.GetItem("{F8D6C775-D559-4775-8A1C-B1C45D3B6080}");
                PopulatereferenceList(referenceItem);

                //  create list of MapLocation
                referenceItem = masterDb.GetItem("{2564BB37-0A90-4146-972C-56FE6AB88313}");
                PopulatereferenceList(referenceItem);

                // Get hotel name and Town name

                //Add data lists
                AddDataTolists();

                Sitecore.Context.Job.Status.LogInfo("Scheduler finished");

            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                throw;
            }
        }

        private void AddDataTolists()
        {
            try
            {
                //First get the parent item from the master database
                Database masterDb = Sitecore.Configuration.Factory.GetDatabase("master");

                Item[] chiledren = masterDb.SelectItems(hotelSelectionFastQuery);

                foreach (var item in chiledren)
                {
                    if (item.Fields["Town"].Value.Length > 0)
                    {
                        listRefrenceData.Add(item.Fields["Town"].Value);
                    }

                    if (item.Fields["Name"].Value.Length > 0)
                    {
                        listRefrenceData.Add(item.Fields["Name"].Value);
                        refrenceDataSupplierslist.Add(item.Fields["Name"].Value);
                    }
                }

                foreach (var item in listRefrenceData.Distinct().ToList())
                {
                    masterDb.AddItem(string.Concat(rootPath, "/", autocompletelist), autoCompletetemplatePath, item);
                }

                foreach (var item in refrenceDataSupplierslist.Distinct().ToList())
                {
                    masterDb.AddItem(string.Concat(rootPath, "/", autocompletelistsuppliers), autoCompletetemplatePath, item);
                }
            }
            catch (Exception exception)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, exception, null);
                throw;
            }

        }


        /// <summary>
        /// Populate the list with deiplay name
        /// </summary>
        /// <param name="item">Item</param>
        private void PopulatereferenceList(Item referenceItem)
        {
            if (referenceItem != null)
            {
                if (referenceItem.HasChildren)
                {
                    foreach (Item item in referenceItem.Children)
                    {
                        listRefrenceData.Add(item.DisplayName);
                    }
                }
            }
        }
    }
}
