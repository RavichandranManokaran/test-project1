﻿USE [Feb15_Ucommerce]
GO
/****** Object:  StoredProcedure [dbo].[LB_GetDuesByOrderLineID]    Script Date: 2/26/2016 1:00:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LB_GetDuesByOrderLineID]
	@OrderLineId INT,
	@CurrencyTypeId INT=NULL
AS
BEGIN
	SELECT SUM([Total]) [Total], [Currency], MAX(OrderLineID)[OrderLineId] FROM
		(SELECT SUM(OLD.Amount) [Total],
				OLD.CurrencyTypeId [Currency],
				MAX(OLD.OrderLineID) [OrderLineId]
		FROM uCommerce_OrderLine  OL
			INNER JOIN LB_OrderLineDue OLD ON OLD.OrderLineID = OL.OrderLineId 
		WHERE
			ISNULL(OLD.IsDeleted,0) = 0 AND 			
			OLD.CurrencyTypeId = (CASE WHEN @CurrencyTypeId IS NOT NULL THEN @CurrencyTypeId ELSE OLD.CurrencyTypeId END) AND			
			OLD.OrderLineID IN (SELECT OrderLineID FROM [dbo].[LB_FnRelatedOrderLines](@OrderLineId))
		GROUP BY 
			OLD.CurrencyTypeId
		UNION ALL
		SELECT -SUM(OLP.Amount) [Total],
				OLP.CurrencyTypeId [Currency],
				MAX(OLP.OrderLineID) [OrderLineId]
		FROM uCommerce_OrderLine  OL
			INNER JOIN LB_OrderLinePayment OLP ON OLP.OrderLineID = OL.OrderLineId 
		WHERE
			OLP.CurrencyTypeId = (CASE WHEN @CurrencyTypeId IS NOT NULL THEN @CurrencyTypeId ELSE OLP.CurrencyTypeId END) AND			
			OLP.OrderLineID IN (SELECT OrderLineID FROM [dbo].[LB_FnRelatedOrderLines](@OrderLineId))
		GROUP BY 
			OLP.CurrencyTypeId) P
	GROUP BY [Currency]
	HAVING
		SUM([Total]) <> 0
END



