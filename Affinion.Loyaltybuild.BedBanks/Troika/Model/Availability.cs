﻿using System;
using System.Collections.Generic;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Message;

namespace Affinion.LoyaltyBuild.BedBanks.Troika.Model
{
    [Serializable]
    public class Availability : JtResponse
    {
        public List<AvailabilityProvider> AvailabilityProviders;
        public List<AvailabilityRate> AvailabilityRates;
        public List<AvailabilityFacility> AvailabilityFacilities;
    }
}
