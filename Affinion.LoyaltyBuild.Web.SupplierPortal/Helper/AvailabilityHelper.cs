﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Availability;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration;
using Sitecore.Analytics.Data.Items;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.CampaignTarget;
using System.Text;
using System.Net.Mail;
using Affinion.LoyaltyBuild.UCom.Common.Extension;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Subrate;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.CategoryImport;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using model = Affinion.LoyaltyBuild.Model.Pricing;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Helper
{
    public class AvailabilityHelper
    {
        public string ProviderId { get; set; }
        Affinion.LoyaltyBuild.Communications.IEmailService emailService = new Affinion.LoyaltyBuild.Communications.Services.EmailService();
        private IRoomAvailabilityService _roomAvailabilityService;
        private ICampaignSearch _campaignSearchService;
        private ICampaignTarget _campaignTarget;
        private ISubrate _subrateServices;
        private ISiteCoreHotel _hotelService;
        Sitecore.Data.Database context = Sitecore.Context.Database;
        public AvailabilityHelper(HttpContext currentcontext)
        {
            //TODO : Pass Logged in Supplier Id (UCom Category GUID)

            // var loggedInProvider=SessionHelper.GetItem<string>(SessionKeys.loggedInSupplier);

           // String loggedInProvider_ucom = Convert.ToString(currentcontext.Session[SessionKeys.loggedInSupplier_ucom]);

            String providerSku = SitecoreUserProfile.SelectedSKU;
            //TODO : Change Code to get SKU
            //loggedInProvider_ucom = "Ar_1122";
            if (!String.IsNullOrEmpty(providerSku))
            {
                if (context.SelectItems(Constants.hotelPath) != null)
                {
                    this.ProviderId = providerSku;
                    /*
                    Item provider = context.SelectItems(Constants.hotelPath).Where(x => x.Name.Equals(loggedInProvider)).FirstOrDefault();
                    if (provider != null)
                    {
                        this.ProviderId = provider.Fields[Constants.uCommerceCategoryID].Value != null ? provider.Fields[Constants.uCommerceCategoryID].Value : string.Empty;

                    }*/
                }
                //this.ProviderId = "6A1D25CB-FD94-489A-971C-6FA980904E25";

            }
            /// this.ProviderId = "6A1D25CB-FD94-489A-971C-6FA980904E25";
            this._roomAvailabilityService = new RoomAvailabilityService();
            this._campaignSearchService = new CampaignSearch();
            this._campaignTarget = new CampaignTargets();
            _hotelService = new SiteCoreHotel();
            _subrateServices = new SubrateService();
        }
        public AvailabilityHelper(string providerId)
        {
            this.ProviderId = providerId;

        }

        public List<ProviderAvialbilityData> RoomAvailibilityByDateRange(DateTime stratDate, DateTime endDate)
        {
            List<ProviderAvialbilityData> avialbilityView = new List<ProviderAvialbilityData>();
            avialbilityView = _roomAvailabilityService.FindRoomAndOfferAvailabiltyByProvider(this.ProviderId, 0, stratDate.ConvertDateToString().ParseFormatDateTime(), endDate.ConvertDateToString().ParseFormatDateTime());
            return avialbilityView;
        }

        public List<RoomAvailabilityByDate> GetAvailabilityy(int startDateCounter, int endDateCounter)
        {
            List<RoomAvailabilityByDate> avialbilityView = new List<RoomAvailabilityByDate>();
            avialbilityView = _roomAvailabilityService.GetRoomAvailabilityByProvider(this.ProviderId, startDateCounter, endDateCounter, string.Empty);
            return avialbilityView;
        }


        public List<decimal> GetAddonPrice(string SKu, string VariantSKU)
        {
            List<decimal> avialbilityView = new List<decimal>();
            avialbilityView = _roomAvailabilityService.GetAddOnPrice(VariantSKU, SKu);
            return avialbilityView;
        }

        public List<ChangeRoomAvailabilityByDate> SaveRoomAvailibility(List<ChangeRoomAvailabilityByDate> data)
        {
            List<ChangeRoomAvailabilityByDate> avialbilityView = new List<ChangeRoomAvailabilityByDate>();
            avialbilityView = _roomAvailabilityService.SaveProviderRoomAvailability(this.ProviderId, data);
            return avialbilityView;
        }

        public List<ChangeOfferRoomAvailabilityByDate> SaveOfferRoomAvailibility(List<ChangeOfferRoomAvailabilityByDate> data)
        {
            List<ChangeOfferRoomAvailabilityByDate> avialbilityView = new List<ChangeOfferRoomAvailabilityByDate>();
            avialbilityView = _roomAvailabilityService.SaveProviderOfferAvailability(this.ProviderId, data);
            return avialbilityView;
        }

        public List<SupplierInviteDetail> GetCampaignDetailBySupplier(DateTime startDate, DateTime endDate, bool loadprice = false, string clientId = null)
        {
            List<SupplierInviteDetail> supplierCampaign = new List<SupplierInviteDetail>();
            supplierCampaign = _campaignSearchService.GetCampaignDetailBySupplier(ProviderId, startDate, endDate, clientId ?? string.Empty, loadprice);
            return supplierCampaign;
        }

        public bool ChangeCampaignSubscribe(int supplierInviteId, bool isSubscribe,string content)
        {
            bool subscribe = _campaignSearchService.ChangeCampaignSubscribe(ProviderId, supplierInviteId, isSubscribe,content);
            if (subscribe)
            {
                int campaignItemId = _campaignSearchService.GetCampaignItemIdBySupplierId(supplierInviteId);
                if (campaignItemId != 0)
                    subscribe = _roomAvailabilityService.ChangeRoomRestriction(ProviderId, campaignItemId, false);
                if (subscribe)
                {
                    SupplierInfo info = GetSupplierInfo(campaignItemId);
                    string msg = CreateEmailTemplate(info, true);
                    try
                    {

                        emailService.Send("info@loyaltybuild.com", "info@loyaltybuild.com", string.Format("Opted In Alert -  {0} – {1}", info.CampaignName, info.SupplierName), msg, true);
                    }
                    catch (Exception ex)
                    {
                        Diagnostics.WriteException(DiagnosticsCategory.SupplierPortal, ex, this);
                        throw;

                    }
                }
            }
            return subscribe;
        }

        private string CreateEmailTemplate(SupplierInfo info, bool isSubscribe)
        {
            string htmlTemplate = SiteConfiguration.GetEmailTemplateHtml(isSubscribe ? 1 : 2);
            StringBuilder emailString = new StringBuilder();
            htmlTemplate = htmlTemplate.Replace("@@suppliername@@", info.SupplierName);
            htmlTemplate = htmlTemplate.Replace("@@supplieraddress@@", info.SupplierAddress);
            htmlTemplate = htmlTemplate.Replace("@@suppliertype@@", info.SupplierType);
            htmlTemplate = htmlTemplate.Replace("@@starrating@@", info.StarRating);
            htmlTemplate = htmlTemplate.Replace("@@campaignname@@", info.CampaignName);
            htmlTemplate = htmlTemplate.Replace("@@arrivaldatesperiod@@", info.ArrivalDatesPeriod);
            htmlTemplate = htmlTemplate.Replace("@@reservationdatesperiod@@", info.ReservationDatesPeriod);
            htmlTemplate = htmlTemplate.Replace("@@client@@", info.Client);
            htmlTemplate = htmlTemplate.Replace("@@country@@", info.Country);
            htmlTemplate = htmlTemplate.Replace("@@date@@", DateTime.Now.ConvertDateToString());
            htmlTemplate = htmlTemplate.Replace("@@optedby@@", SitecoreUserProfile.GetItem(UserProfileKeys.loggedInUserName));

            return htmlTemplate;
            //emailString.Append("Attn : Hotel Supply");
            //emailString.Append(string.Format("<br/> {0} has opted {1}to the following Campaign", info.SupplierName, isSubscribe ? "in" : "out"));
            //emailString.Append(string.Format("<br/> {0}", info.SupplierAddress));
            //emailString.Append(string.Format("<br/> {0}", info.SupplierType));
            //emailString.Append(string.Format("<br/> {0}", info.StarRating));
            //emailString.Append(string.Format("<br/> {0}", info.CampaignName));
            //emailString.Append(string.Format("<br/> {0}", info.ArrivalDatesPeriod));
            //emailString.Append(string.Format("<br/> {0}", info.ReservationDatesPeriod));

            //emailString.Append(string.Format("<br/> {0}", info.Client));
            //emailString.Append(string.Format("<br/> {0}", info.Country));


            //emailString.Append(string.Format("<br/><br/>Opted {1} Date – {0}", DateTime.Now.ConvertDateToString(), isSubscribe ? "In" : "Out"));
            //emailString.Append(string.Format("<br/>Opted {1} By – {0}", SitecoreUserProfile.GetItem(UserProfileKeys.loggedInUserName), isSubscribe ? "In" : "Out"));

            //return emailString.ToString();
        }

        public List<ProviderAvialbilityData> OfferAvailibilityByDateRange(int campaignItemId, DateTime stratDate, DateTime endDate)
        {
            List<ProviderAvialbilityData> avialbilityView = new List<ProviderAvialbilityData>();
            avialbilityView = _roomAvailabilityService.FindRoomAndOfferAvailabiltyByProvider(this.ProviderId, campaignItemId, stratDate.ConvertDateToString().ParseFormatDateTime(), endDate.ConvertDateToString().ParseFormatDateTime());
            return avialbilityView;
        }

        public OfferDetail GetCampaignById(int campaignItemId, int supplierInviteId, bool calculatePrice = false, bool calculateforyear = false)
        {
            OfferDetail detail = _campaignSearchService.GetCampaignById(ProviderId, campaignItemId, supplierInviteId, calculatePrice, calculateforyear);
            //List<int> booking = _roomAvailabilityService.GetOfferBookingByDateRange(ProviderId, campaignItemId);
            //detail.BookingReceived = detail.BookingReceived; // booking[0];
            //detail.FutureArrival = detail.FutureArrival;
            return detail;
        }

        public CampaignDetail GetCampaignDescriptionById(int campaignItemId, int supplierInviteId)
        {
            CampaignDetail detail = _campaignSearchService.GetCampaignDescriptionById(ProviderId, campaignItemId, supplierInviteId);
            //List<int> booking = _roomAvailabilityService.GetOfferBookingByDateRange(ProviderId, campaignItemId);
            //detail.BookingReceived = detail.BookingReceived; // booking[0];
            //detail.FutureArrival = detail.FutureArrival;
            return detail;
        }

        public int GetSupplierInviteId(int campaignItemId)
        {
            return _campaignSearchService.GetSupplierInviteIdByCampaign(campaignItemId, ProviderId);
        }

        public List<OfferPrice> GetOfferPriceByCampaignIds(int campaignItemId)
        {
            List<CampaignOfferDetail> detail = _campaignSearchService.GetOfferPriceByCampaignIds(ProviderId, new List<int> { campaignItemId }, string.Empty);
            if (detail != null && detail.Any())
            {
                return detail.FirstOrDefault().OfferPrice;
            }
            return new List<OfferPrice>();
        }

        public bool UnsubscribeForOffer(int campaignItemId, int supplierInviteId, string reason)
        {
            bool unsubscribe = _campaignSearchService.UnSubScribeForOffer(ProviderId, campaignItemId, supplierInviteId, reason);
            if (unsubscribe)
            {
                unsubscribe = _roomAvailabilityService.ChangeRoomRestriction(ProviderId, campaignItemId, true);
            }
            if (unsubscribe)
            {
                SupplierInfo info = GetSupplierInfo(campaignItemId);
                string msg = CreateEmailTemplate(info, false);
                string subject = string.Format("Opted Out Alert -  {0} – {1}", info.CampaignName, info.SupplierName);
                try
                {
                    EmailSetting setting = SiteConfiguration.GetSupplierEmailConfiguration();
                    if (setting != null)
                    {
                        emailService.Send(setting.SupplierFrom, setting.SupplierTo, subject, msg, true);
                    }
                    else
                    {
                        emailService.Send("info@loyaltybuild.com", "info@loyaltybuild.com", subject, msg, true);
                    }
                }
                catch (Exception ex)
                {
                    Diagnostics.WriteException(DiagnosticsCategory.SupplierPortal, ex, this);


                }
            }
            return unsubscribe;

        }

        public SupplierInfo GetLoggedInSupplierDetailById()
        {
           
            Item[] hotels = Sitecore.Context.Database.SelectItems(string.Format("/sitecore/content/#admin-portal#/#supplier-setup#//*[@@TemplateName='SupplierDetails' and @SKU='{0}']", SitecoreUserProfile.SelectedSKU));

            if (hotels != null && hotels.Length > 0)
            {
                string loggedInSupplierId = hotels[0].ID.Guid.ToString();

                SupplierInfo info = SiteConfiguration.GetSupplierDetailById(loggedInSupplierId);
                return info;
            }
            return null;
        }
        private SupplierInfo GetSupplierInfo(int campaignItemId)
        {
            string loggedInSupplierId = SitecoreUserProfile.GetItem(UserProfileKeys.loggedInProviderId);
            SupplierInfo info = SiteConfiguration.GetSupplierDetailById(loggedInSupplierId);

            CampaignDetail citem = _campaignSearchService.GetCampaignItemById(campaignItemId);
            info.CampaignName = citem.CampaignName;
            List<CampaginItemDateRange> arrivalDate = GetArrivalDate(campaignItemId);
            if (arrivalDate != null && arrivalDate.Any())
            {
                foreach (var item in arrivalDate)
                {
                    info.ArrivalDatesPeriod = string.Format("{0} - {1}", item.StartDate, item.EndDate);
                }
            }
            List<CampaginItemDateRange> resDate = GetReservationDate(campaignItemId);
            if (resDate != null && resDate.Any())
            {
                foreach (var item in resDate)
                {
                    info.ReservationDatesPeriod = string.Format("{0} - {1}", item.StartDate, item.EndDate);
                }
            }
            List<ClientList> clienTarget = _campaignTarget.GetClientTargetByCampaignId(campaignItemId);

            if (clienTarget != null && clienTarget.Any())
            {
                List<ClientDetail> clientList = SiteConfiguration.GetAllClient();
                foreach (var item in clienTarget)
                {
                    ClientDetail clientDetail = clientList.FirstOrDefault(x => x.ClientGUID.Equals(item.ClientId));
                    if (clientDetail != null)
                    {
                        info.Client += string.Format("{0} ,", clientDetail.Name);
                    }
                    if (!string.IsNullOrEmpty(info.Client) && info.Client.Length > 0)
                    {
                        info.Client = info.Client.Substring(0, info.Client.Length - 1);
                    }

                    //info.ArrivalDatesPeriod = string.Format("{0} - {1}", item.ArrivalFromDate.ConvertDateToString(), item.ArrivalToDate.ConvertDateToString());
                }
            }
            return info;
        }

        public List<CampaginItemDateRange> GetReservationDate(int campaignItemId)
        {
            List<CampaginItemDateRange> resDate = _campaignTarget.GetReservaionDateTargetByCampaignId(campaignItemId);
            return resDate;
        }

        public List<CampaginItemDateRange> GetArrivalDate(int campaignItemId)
        {
            List<CampaginItemDateRange> arrivalDate = _campaignTarget.GetArrivalDateTargetByCampaignId(campaignItemId);
            return arrivalDate;
        }

        public LBHotelDetail GetHotelDetail()
        {
            return _hotelService.GetHotelDetailByGUID(ProviderId);
        }


        public List<ProductDetail> GetProductDetail()
        {
            List<ProductDetail> products = _roomAvailabilityService.GetProductDetailByProviderId(ProviderId);
            return products;

        }

        /// <summary>
        /// Get product price by date
        /// </summary>
        /// <param name="sku"></param>
        /// <param name="variantSku"></param>
        /// <param name="date"></param>
        /// <param name="isSubItem"></param>
        /// <returns></returns>
        public model.ProductPrice GetProductPriceByDate(string sku, string variantSku, DateTime date, bool isSubItem = false)
        {
            model.ProductPrice products = _roomAvailabilityService.GetProductPriceByDate(sku, variantSku, date, isSubItem);
            return products;
        }

    }
}