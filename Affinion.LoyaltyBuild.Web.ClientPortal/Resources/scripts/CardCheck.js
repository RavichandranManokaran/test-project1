﻿function ClientValidate(source, arguments) {
    //var pattern = /(5[1-5][0-9]{16})/;
    // var pattern = /(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})/;
    //var pattern = /^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|6(?:011|5[0-9]{2})[0-9]{12}|(?:2131|1800|35\d{3})\d{11})$/;
    if (valid_credit_card(arguments.Value) == false)
        arguments.IsValid = false;
}

function valid_credit_card(creditText) {

    var re = /\d{19}|\d{18}|\d{17}|\d{16}|\d{15}|\d{14}|\d{13}|\d{12}/g;
    var m;
    do {
        m = re.exec(creditText);
        if (m) {
            if (LuchAlgorithm(m[0]) == true)
                return false;
        }
    } while (m);

}

function LuchAlgorithm(value) {

    if (/[^0-9-\s]+/.test(value)) return false;

    // The Luhn Algorithm. It's so pretty.
    var nCheck = 0, nDigit = 0, bEven = false;
    value = value.replace(/\D/g, "");

    for (var n = value.length - 1; n >= 0; n--) {
        var cDigit = value.charAt(n),
              nDigit = parseInt(cDigit, 10);

        if (bEven) {
            if ((nDigit *= 2) > 9) nDigit -= 9;
        }

        nCheck += nDigit;
        bEven = !bEven;
    }

    return (nCheck % 10) == 0;
}