﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           GetFieldsCollection.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common.Utilities
/// Description:           Used to get field of item  to dispaly
/// </summary>
namespace Affinion.LoyaltyBuild.Common.Utilities
{
    #region Using directives
    using Sitecore;
    using Sitecore.Data.Fields;
    using Sitecore.Data.Items;
    using Sitecore.Resources.Media;
    using System.Collections.Generic;
    using System.Linq;
    #endregion

    //public class GridObject
    //{
    //    public string Section { get; set; }
    //    public string Key { get; set; }
    //    public string Type { get; set; }
    //    public string FieldValue { get; set; }
    //    public string TestValue { get; set; }
    //}

    public static class GetFieldsCollection
    {
        /// <summary>
        /// Get all template fields of current item  
        /// </summary>
        /// <returns></returns>
        #region Public method
        //public static List<GridObject> GetItemFieldWithInSection()
        //{
        //    List<TemplateFieldItem> listTemplateFieldItem = new List<TemplateFieldItem>();
        //    List<GridObject> gridObject = new List<GridObject>();
        //    Item item = Sitecore.Context.Item;
        //    if (item != null)
        //    {
        //        TemplateItem templateItem = item.Template;
        //        TemplateSectionItem[] sections = templateItem.GetSections();
        //        TemplateSectionItem[] array = sections;

        //        for (int i = 0; i < array.Length; i++)
        //        {
        //            TemplateSectionItem section = array[i];
        //            TemplateFieldItem[] myItem = templateItem.GetSection(section.DisplayName).GetFields();
        //            listTemplateFieldItem.AddRange(myItem);
        //        }
        //    }

        //    foreach (var v in listTemplateFieldItem)
        //    {

        //        string caseValue = v.Type.ToLower();
        //        switch (caseValue)
        //        {
        //            case ("image"):
        //                string imageURL = string.Empty;
        //                ImageField imageField = item.Fields[v.Key];
        //                if (imageField != null && imageField.MediaItem != null)
        //                {
        //                    MediaItem image = new MediaItem(imageField.MediaItem);
        //                    imageURL = StringUtil.EnsurePrefix('/', MediaManager.GetMediaUrl(image));
        //                }
        //                GridObject imageTest = (from a in listTemplateFieldItem
        //                                        where a.Key == v.Key
        //                                        select new GridObject
        //                                        {
        //                                            Section = a.Section.Key,
        //                                            Key = a.Key,
        //                                            Type = a.Type,
        //                                            FieldValue = imageURL
        //                                        }).FirstOrDefault<GridObject>();
        //                gridObject.Add(imageTest);
        //                break;

        //            case ("droplist"):

        //                GridObject droplistTest = (from a in listTemplateFieldItem
        //                                           where a.Key == v.Key
        //                                           select new GridObject
        //                                           {
        //                                               Section = a.Section.Key,
        //                                               Key = a.Key,
        //                                               Type = a.Type,
        //                                               FieldValue = (item.Fields[a.Key].Value),
        //                                           }).FirstOrDefault<GridObject>();
        //                gridObject.Add(droplistTest);
        //                break;

        //            case ("droplink"):

        //                GridObject dropLinkTest = (from a in listTemplateFieldItem
        //                                           where a.Key == v.Key
        //                                           select new GridObject
        //                                           {
        //                                               Section = a.Section.Key,
        //                                               Key = a.Key,
        //                                               Type = a.Type,
        //                                               FieldValue = (item.Fields[a.Key] == null) ? null : ((item.Fields[a.Key].Value == null) ? null : ((Context.Database.GetItem(item.Fields[a.Key].Value) == null) ? null : Context.Database.GetItem(item.Fields[a.Key].Value).DisplayName))
        //                                           }).FirstOrDefault<GridObject>();
        //                gridObject.Add(dropLinkTest);
        //                break;

        //            case ("multilist"):

        //                GridObject multiListTest = (from a in listTemplateFieldItem
        //                                            where a.Key == v.Key
        //                                            select new GridObject
        //                                            {
        //                                                Section = a.Section.Key,
        //                                                Key = a.Key,
        //                                                Type = a.Type,
        //                                                FieldValue = (item.Fields[a.Key] == null) ? null : ((item.Fields[a.Key].Value == null) ? null : ((Context.Database.GetItem(item.Fields[a.Key].Value) == null) ? null : Context.Database.GetItem(item.Fields[a.Key].Value).DisplayName))
        //                                            }).FirstOrDefault<GridObject>();
        //                gridObject.Add(multiListTest);
        //                break;

        //            default:
        //                GridObject defaultTest = (from a in listTemplateFieldItem
        //                                          where a.Key == v.Key
        //                                          select new GridObject
        //                                          {
        //                                              Section = a.Section.Key,
        //                                              Key = a.Key,
        //                                              Type = a.Type,
        //                                              FieldValue = (item.Fields[a.Key].Value),
        //                                          }).FirstOrDefault<GridObject>();
        //                gridObject.Add(defaultTest);
        //                break;
        //        }
        //    }
        //    return gridObject;
        //}
        #endregion
    }
}
