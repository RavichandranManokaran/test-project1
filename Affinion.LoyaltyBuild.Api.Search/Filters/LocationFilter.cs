﻿using Affinion.LoyaltyBuild.Api.Search.Data;
using Affinion.LoyaltyBuild.UCom.MasterClass.ExtendedApi;
using Sitecore.Data;
using UCommerce.EntitiesV2;
using UCommerce.Runtime;
using Affinion.LoyaltyBuild.DataAccess.uCommerceUtility;
using Affinion.LoyaltyBuild.BedBanks.General;
using System;
using System.Text;
using System.Collections.Generic;
using Affinion.LoyaltyBuild.Common;
using Sitecore.Data.Items;
using Affinion.LoyaltyBuild.Common.Utilities;

namespace Affinion.LoyaltyBuild.Api.Search.Filters
{
    /// <summary>
    /// Implementation for SearchFilter,Bedbank filter, Loyalty Filter
    /// </summary>
    public class LocationFilter : ISearchFilter, IBedBankFilter, ILoyaltyFilter
    {
        private readonly SearchKey _key;
        //sitecore/content/#admin-portal#/#supplier-setup#/#global#/#_supporting-content#/#groups#//*[@@templateid='{26507FB6-AFD2-4DCC-B56F-AD1BB5D60CFA}']";
        string path = "/sitecore/content/#admin-portal#/#client-setup#/#global#/#_supporting-content#/#bedbankprovidersettings#/#jactravel#/#jactravel-currency#//*[@@templateid='{730153DD-E48F-4F75-9BB1-CC16F8F8A432}']";

        /// <summary>
        /// Overloaded constructor
        /// </summary>
        /// <param name="key"></param>
        public LocationFilter(SearchKey key)
        {
            _key = key;
        }

        /// <summary>
        /// Build search criteria
        /// </summary>
        /// <param name="criteria"></param>
        public void BuildSearch(ref SearchCriteria criteria)
        {
            int categoryId = 0;
            if (_key.Location != null)
            {
                switch (_key.Location.Type)
                {
                    case LocationType.City:
                    case LocationType.Country:
                        int.TryParse(_key.Location.ItemId, out categoryId);
                        break;
                    case LocationType.Hotel:
                        var parts = _key.Location.ItemId.Split(',');
                        if (parts.Length == 2)
                        {
                            //set hotel searching for
                            int productId = 0;
                            int.TryParse(parts[0], out productId);
                            if (productId > 0)
                                criteria.SearchHotel = productId;
                            int.TryParse(parts[1], out categoryId);
                        }
                        break;
                    case LocationType.FreeText:
                        var client = Sitecore.Context.Database.GetItem(new ID(_key.ClientId));
                        categoryId = uCommerceConnectionFactory.GetCategoryIDByCatalogIDAndMatchingText(Convert.ToInt32(client.Fields["ProductCatalogID"].Value), _key.Location.Text);
                        break;
                }
            }

            if (categoryId > 0)
            {
                Category currentCategory = UCommerce.Api.CatalogLibrary.GetCategory(categoryId);
                criteria.SearchCategory = currentCategory;
            }
        }

        /// <summary>
        /// Method to set all the search criteria for Bed Bank Location Search
        /// </summary>
        /// <param name="criteria"></param>
        public void BuildbbSearch(ref BBsearchCriteria criteria)
        {
            if (_key.Location.Type != LocationType.City)
               return;

            var client = Sitecore.Context.Database.GetItem(new ID(_key.ClientId));
            
            BedBanksSettings bbs = new BedBanksSettings(client);
            criteria.BedBankData = bbs;
            if (bbs != null && bbs.BedBankLocation != null)
            {
                foreach (var location in bbs.BedBankLocation)
                {
                    if (_key.Location.Text != null && _key.Location.Text.ToLower().Contains(location.LocationName.ToLower()))
                    {
                        criteria.RegionId = location.BedBanksLocationId;

                        Item locationItem = location.SitecoreLocationItem;
                        Item countryItem = (locationItem == null) ? null : SitecoreFieldsHelper.GetLookupFieldTargetItem(locationItem, "Country");
                        if (countryItem != null)
                        {
                            // BedBanksCurrency actualCurr = GetActualCurrency(countryItem);
                            //criteria.BedBankData.BedBanksActualCurrency = actualCurr;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="country"></param>
        /// <returns></returns>
        private BedBanksCurrency GetActualCurrency(Item country)
        {

            Item currencyItem = SitecoreFieldsHelper.GetLookupFieldTargetItem(country, "Currency");
            var currencies = Sitecore.Context.Database.SelectItems(path);

            BedBanksCurrency actualCurr=null;
            foreach (var item in currencies)
            {
                Item ucomCurr = SitecoreFieldsHelper.GetLookupFieldTargetItem(item, "uCommerce Currency Id");
                if (ucomCurr!=null && (ucomCurr.ID == currencyItem.ID))
                {
                    actualCurr = new BedBanksCurrency();
                    actualCurr.BedBanksCurrencyId = item.Fields["Bed Banks Currency Id"].Value;
                    actualCurr.CurrencyName = item.Fields["Currency Name"].Value;
                    actualCurr.uCommerceCurrencyId = item.Fields["uCommerce Currency Id"].Value;
                    break;
                }
            }
            return actualCurr;
        }
    }
}
