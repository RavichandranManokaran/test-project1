﻿/*<summary>
Description: Stored procedure to Add new customer details
<param name="@FirstName">Customer First name</param>
<param name="@LastName">Customer Last name</param>
<param name="@EmailAddress">Customer email</param>
<param name="@Phone">Customer Primary Phone</param>
<param name="@MobilePhone">Customer Secondary Phone</param>
<param name="@ClientId">Client Id</param>
<param name="@Title">Customer Title</param>
<param name="@AddressLine1">Customer Address line1</param>
<param name="@AddressLine2">Customer Address line2</param>
<param name="@AddressLine3">Customer Address line3</param>
<param name="@CountryId">Country Id</param>
<param name="@LocationId">Location Id</param>
<param name="@BranchId">Client Branch customer is registered</param>
<param name="@Subscribe">Customer Order Id</param>
<param name="@BookingReferenceId">Customer Order Id</param>
</summary>*/
CREATE PROCEDURE [dbo].[LB_AddCustomer](
	@FirstName nvarchar(512),
	@LastName nvarchar(512),
	@EmailAddress nvarchar(255),
	@Phone nvarchar(50)=Null,
	@MobilePhone nvarchar(50),
	@LanguageName nvarchar(255)=null,
	@ClientId varchar(50),
	@Title varchar(50)=Null,
	@AddressLine1 nvarchar(50),
	@AddressLine2 nvarchar(50)=Null,
	@AddressLine3 nvarchar(50)=Null,
	@CountryId int,
	@LocationId int,
	@BranchId VARCHAR(50)=Null, 
	@Subscribe int=0, 
	@BookingReferenceId varchar(50)=Null, 
	@Active int=1)

AS
BEGIN TRANSACTION
	DECLARE @NewCustomerId int
	INSERT INTO [dbo].uCommerce_Customer  
		(FirstName,
		LastName,
		EmailAddress,
		PhoneNumber,
		MobilePhoneNumber,
		LanguageName
	)
	 VALUES
	  (@FirstName,
	  @LastName,
	  @EmailAddress,
	  @Phone,
	  @MobilePhone,
	  @LanguageName
	  )
  
     /*If an Error Occurs Rollback Add*/

	If @@ERROR<>0
      BEGIN
	  	ROLLBACK
		RETURN
	  END
	select @NewCustomerId = SCOPE_IDENTITY()
	INSERT INTO [dbo].LB_CustomerAttribute
	(CustomerId,
	ClientId,
	Title,
	AddressLine1,
	AddressLine2,
	AddressLine3,
	CountryId,
	LocationId,
	BranchId,
	Subscribe,
	BookingReferenceId,
	Active
	)
	VALUES(
	@NewCustomerId,
	@ClientId,
	@Title,
	@AddressLine1,
	@AddressLine2,
	@AddressLine3,
	@CountryId,
	@LocationId,
	@BranchId,
	@Subscribe,
	@BookingReferenceId,
	@Active
	)

  /*If an Error Occurs Rollback Add*/

  If @@ERROR<>0
   BEGIN
	ROLLBACK
	RETURN
   END
COMMIT
SELECT @NewCustomerId