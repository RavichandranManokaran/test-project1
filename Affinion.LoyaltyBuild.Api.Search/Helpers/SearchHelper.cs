﻿using Affinion.LoyaltyBuild.Api.Search.Data;
using Affinion.LoyaltyBuild.Model.Provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.Infrastructure;

namespace Affinion.LoyaltyBuild.Api.Search.Helpers
{
    public static class SearchHelper
    {
        private static SearchContext Context
        {
            get
            {
                return ObjectFactory.Instance.Resolve<SearchContext>();
            }
        }

        /// <summary>
        /// Search hotels by search key
        /// </summary>
        /// <param name="key"></param>
        /// <param name="options"></param>
        public static PagedProviderCollection SearchHotels(SearchKey key, SearchOptions options = null)
        {
            return Context.SearchHotels(key, options);
        }

        /// <summary>
        /// Get provider data for the page
        /// </summary>
        /// <param name="collection"></param>
        /// <param name="currentPage"></param>
        /// <returns></returns>
        public static IList<IProvider> GetPageData(PagedProviderCollection collection, int currentPage)
        {
            return Context.GetPageData(collection, currentPage);
        }

        /// <summary>
        /// Get suggestions
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<SearchLocation> GetSuggestion(int catalogId, string term, string hotelDefinitionName, string cityDefinitionName, string countryDefinitionName)
        {
            return Context.GetSuggestion(catalogId, term, hotelDefinitionName, cityDefinitionName, countryDefinitionName);
        }

        /// <summary>
        /// Get Provider details with its product
        /// </summary>
        /// <param name="key"></param>
        /// <param name="providerSearch"></param>
        /// <returns></returns>
        public static IProvider GetProviderInfo(SearchKey key, ProviderSearch providerSearch)
        {
            return Context.GetProviderInfo(key, providerSearch);
        }
    }
}
