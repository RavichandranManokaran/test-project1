﻿CREATE TABLE [dbo].[uCommerce_Customer] (
    [CustomerId]        INT            IDENTITY (1, 1) NOT NULL,
    [FirstName]         NVARCHAR (512) NOT NULL,
    [LastName]          NVARCHAR (512) NOT NULL,
    [EmailAddress]      NVARCHAR (255) NULL,
    [PhoneNumber]       NVARCHAR (50)  NULL,
    [MobilePhoneNumber] NVARCHAR (50)  NULL,
    [MemberId]          NVARCHAR (255) NULL,
    [LanguageName]      NVARCHAR (25)  NULL,
    CONSTRAINT [uCommerce_PK_Customer] PRIMARY KEY CLUSTERED ([CustomerId] ASC)
);

