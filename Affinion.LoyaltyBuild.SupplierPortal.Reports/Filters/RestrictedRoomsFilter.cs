﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.SupplierPortal.Reports.Filters
{
    /// <summary>
    /// Contains all the filters of Restricted rooms report
    /// </summary>
    public class RestrictedRoomsFilter
    {
        /// <summary>
        /// Hotel id
        /// </summary>
        public int? HotelId { get; set; }
        /// <summary>
        /// From date
        /// </summary>
        public DateTime? DateFrom { get; set; }
        /// <summary>
        /// To date
        /// </summary>
        public DateTime? DateTo { get; set; }
        /// <summary>
        /// Provider id
        /// </summary>
        public int? ProviderId { get; set; }
    }
}
