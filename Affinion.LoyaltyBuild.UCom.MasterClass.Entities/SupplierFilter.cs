﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class SupplierFilter : IEntity
    {
        private int? _hashCode;

        // Methods
        public static IQueryable<SupplierFilter> All()
        {
            return GetRepo().Select();
        }

        public virtual void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<SupplierFilter, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            SupplierFilter objA = obj as SupplierFilter;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<SupplierFilter, bool>> expression)
        {
            return All().Any<SupplierFilter>(expression);
        }

        public static IList<SupplierFilter> Find(Expression<Func<SupplierFilter, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<SupplierFilter>();
        }

        public static SupplierFilter FirstOrDefault(Expression<Func<SupplierFilter, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<SupplierFilter>();
        }

        public static SupplierFilter Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<SupplierFilter> GetRepo()
        {
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<SupplierFilter>>();

        }

        public virtual void Save()
        {
            try
            {
                GetRepo().Save(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static SupplierFilter SingleOrDefault(Expression<Func<SupplierFilter, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties

        public virtual int SupplierFilterId { get; protected set; }

        public virtual int Id
        {
            get
            {
                return this.SupplierFilterId;
            }
        }

        public virtual string SearchFilter { get; set; }

        public virtual int CampaignItem { get; set; }
    }
}
