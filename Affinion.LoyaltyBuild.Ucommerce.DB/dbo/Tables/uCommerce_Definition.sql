﻿CREATE TABLE [dbo].[uCommerce_Definition] (
    [DefinitionId]     INT              IDENTITY (1, 1) NOT NULL,
    [Name]             NVARCHAR (512)   NOT NULL,
    [DefinitionTypeId] INT              NOT NULL,
    [Description]      NVARCHAR (MAX)   NOT NULL,
    [Deleted]          BIT              NOT NULL,
    [SortOrder]        INT              CONSTRAINT [DF_uCommerce_Definition_SortOrder] DEFAULT ((0)) NOT NULL,
    [Guid]             UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [BuiltIn]          BIT              DEFAULT ((0)) NOT NULL,
    [CreatedOn]        DATETIME         CONSTRAINT [DF_uCommerce_Definition_CreatedOn] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]        NVARCHAR (50)    DEFAULT ('') NOT NULL,
    [ModifiedOn]       DATETIME         CONSTRAINT [DF_uCommerce_Definition_ModifiedOn] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]       NVARCHAR (50)    DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_uCommerceDefinition] PRIMARY KEY CLUSTERED ([DefinitionId] ASC),
    CONSTRAINT [FK_uCommerce_Definition_uCommerce_DefinitionType] FOREIGN KEY ([DefinitionTypeId]) REFERENCES [dbo].[uCommerce_DefinitionType] ([DefinitionTypeId])
);


GO
CREATE NONCLUSTERED INDEX [IX_uCommerce_Definition]
    ON [dbo].[uCommerce_Definition]([DefinitionTypeId] ASC);

