﻿using System.Collections.Generic;
using Affinion.LoyaltyBuild.BedBanks.JacTravel.Model.Room;

namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Message
{
    /// <summary>
    /// This is a class to hold the search Request Parameters.
    /// It will be passed to the API controller to perform the actual request.
    /// </summary>
    public class JtRoomResponse
    {
        public List<JtRoomType> RoomsList;

        public JtRoomResponse(List<JtRoomType> pRoomslist)
        {
            RoomsList = pRoomslist;
        }
    }
}