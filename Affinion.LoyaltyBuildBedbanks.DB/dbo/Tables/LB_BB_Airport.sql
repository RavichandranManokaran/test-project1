
/****** Object:  Table [dbo].[LB_BB_Airport]    Script Date: 7/19/2016 7:56:57 PM ******/

CREATE TABLE [dbo].[LB_BB_Airport](
	[Airport] [varchar](150) NULL,
	[IATACode] [varchar](50) NULL,
	[AirportID] [varchar](50) NULL,
	[AirportTerminal] [varchar](50) NULL,
	[AirportTerminalID] [varchar](50) NULL
) ON [PRIMARY]

GO
