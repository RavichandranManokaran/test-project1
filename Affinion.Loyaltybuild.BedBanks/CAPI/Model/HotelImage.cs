﻿using System;

namespace Affinion.Loyaltybuild.BedBanks.CAPI.Model
{
    /// <summary>
    /// Summary description for HotelImage
    /// </summary>
    public class HotelImage
    {
        public string Url { get; set; }
        public string Caption { get; set; }
        public string Type { get; set; }
        public Boolean IsThumbnailImage { get; set; }

        public HotelImage(string pUrl, string pCaption, string pType, Boolean pIsThumbnailImage)
        {
            Url = pUrl;
            Caption = pCaption;
            Type = pType;
            IsThumbnailImage = pIsThumbnailImage;
        }
    }
}