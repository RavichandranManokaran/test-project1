﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Model.Product
{
    /// <summary>
    /// Product interface
    /// </summary>
    public interface IProduct
    {
        /// <summary>
        /// Sku
        /// </summary>
        string Sku { get; set; }

        /// <summary>
        /// Variant Sku
        /// </summary>
        string VariantSku { get; set; }

        /// <summary>
        /// Product type
        /// </summary>
        ProductType Type { get; set; }

        // <summary>
        /// Gets or sets the Offer Price
        /// </summary>
        decimal Price { get; set; }

        /// <summary>
        /// Ucommerce currency id
        /// </summary>
        int CurrencyId { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        string Name { get; set; }
    }
}
