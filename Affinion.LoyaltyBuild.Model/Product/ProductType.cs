﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Model.Product
{
    /// <summary>
    /// Enum to return the type of provider type
    /// </summary>
    [Serializable]
    public enum ProductType
    {
        BedBanks,
        Room,
        Package,
        Addon
    }
}
