﻿CREATE TABLE [dbo].[LB_AddOnTarget] (
    [AddOnTargetId] INT            NOT NULL,
    [AddOn]         NVARCHAR (500) NOT NULL,
    CONSTRAINT [PK_uCommerce_AddOn] PRIMARY KEY CLUSTERED ([AddOnTargetId] ASC),
    CONSTRAINT [FK_uCommerce_AddOnTarget_uCommerce_Target] FOREIGN KEY ([AddOnTargetId]) REFERENCES [dbo].[uCommerce_Target] ([TargetId])
);

