﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           InactiveCommentValidationAction.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Used to validate workflow comment box
/// </summary>
#region Using directives
using Sitecore.Data.Items;
using Sitecore.SecurityModel;
using Affinion.LoyaltyBuild.Common.Utilities;
#endregion
namespace Affinion.LoyaltyBuild.Common.Utilities
{
    public class InactiveCommentValidationAction
    {
        #region Public method
        /// <summary>
        /// Set item's icon field to Inacrive notification with workflow inactive action
        /// </summary>
        /// <param name="args">WorkflowPipelineArgs object</param>
        public void Process(Sitecore.Workflows.Simple.WorkflowPipelineArgs args)
        {
            Item item = args.DataItem;
            if (!string.IsNullOrEmpty(args.Comments))
            {
                using (new Sitecore.Security.Accounts.UserSwitcher(ItemHelper.ContentEditingUser))
                {
                    item.Editing.BeginEdit();

                    if (item.Template.ID.ToString() == "{A7235C94-2EF5-4A3B-87DB-0D4DCF10B4DB}")
                    {
                        item.Fields["StatusNote"].Value = args.Comments;
                    }
                    item.Fields["__icon"].Value = ItemHelper.InactiveIcon["__icon"];
                    item.Fields["IsActive"].Value = string.Empty;
                    ItemHelper.ActivateSupplierType(item, "{3E85F816-1D87-4FFA-8261-820C92266305}", "IsActive");
                    item.Editing.EndEdit();
                }
            }
            else
            {
                Sitecore.Web.UI.Sheer.SheerResponse.
                    Alert("You cannot Inactive without comment", string.Empty);
                args.AbortPipeline();
            }
        }
        #endregion
    }
}
