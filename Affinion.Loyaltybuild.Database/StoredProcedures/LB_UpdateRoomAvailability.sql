﻿IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LB_UpdateRoomAvailability]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dbo].[LB_UpdateRoomAvailability]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
/*
*/ 
CREATE PROCEDURE [dbo].[LB_UpdateRoomAvailability]
	@orderlineid int=null,
	@IsCancel bit=0 
AS
BEGIN 
	DECLARE @CampaignItemID int
	DECLARE @Count INT
	DECLARE @Sku VARCHAR(50)
	SET @Count = CASE WHEN @IsCancel= 0 THEN +1 ELSE -1 END
	IF @orderlineid IS NOT NULL
	BEGIN		
		BEGIN TRY
			SELECT TOP 1 @CampaignItemID = CampaignItemId from ucommerce_orderlinecampaignrelation where OrderLineId = @orderlineid
			SELECT TOP 1 @Sku = Sku from ucommerce_orderline where OrderLineId = @orderlineid
			IF @campaignitemID IS NOT NULL
			BEGIN
				--Update Offer Availability table 			
				UPDATE oa SET oa.NumberBooked = (oa.NumberBooked+@Count)  FROM LB_OfferAvailability oa
				INNER JOIN ucommerce_orderline ol on ol.Sku = oa.ProviderOccupancyTypeID
				INNER JOIN LB_RoomReservation rr on rr.orderlineid = ol.orderlineid
				WHERE 
					ol.orderlineid = @orderlineid AND 
					oa.ProviderOccupancyTypeID = @Sku AND
					oa.CampaignItem = @campaignitemID AND
					rr.checkindate <= dbo.GetDateByCounter(oa.datecounter) AND				
					rr.checkoutdate > dbo.GetDateByCounter(oa.datecounter)
			END	
			--Update Room Availability table 
			UPDATE ra SET ra.NumberBooked = (ra.NumberBooked+@Count) FROM LB_RoomAvailability ra
			INNER JOIN ucommerce_orderline ol on ol.Sku = ra.ProviderOccupancyTypeID
			INNER JOIN LB_RoomReservation rr on rr.orderlineid = ol.orderlineid
			WHERE 
				ol.orderlineid = @orderlineid AND 
				ra.ProviderOccupancyTypeID = @Sku AND
				rr.checkindate <= dbo.GetDateByCounter(ra.datecounter) AND				
				rr.checkoutdate > dbo.GetDateByCounter(ra.datecounter)
		END TRY
		BEGIN CATCH
		END CATCH	
	END
 END
 
