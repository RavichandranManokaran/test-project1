﻿CREATE TABLE [dbo].[uCommerce_EmailProfileInformation] (
    [EmailProfileInformationId] INT            IDENTITY (1, 1) NOT NULL,
    [EmailProfileId]            INT            NOT NULL,
    [EmailTypeId]               INT            NOT NULL,
    [FromName]                  NVARCHAR (512) NOT NULL,
    [FromAddress]               NVARCHAR (512) NOT NULL,
    [CcAddress]                 NVARCHAR (512) NULL,
    [BccAddress]                NVARCHAR (512) NULL,
    CONSTRAINT [uCommerce_PK_EmailProfileInformation] PRIMARY KEY CLUSTERED ([EmailProfileInformationId] ASC),
    CONSTRAINT [FK_uCommerce_EmailProfileInformation_uCommerce_EmailProfile] FOREIGN KEY ([EmailProfileId]) REFERENCES [dbo].[uCommerce_EmailProfile] ([EmailProfileId]),
    CONSTRAINT [FK_uCommerce_EmailProfileInformation_uCommerce_EmailType] FOREIGN KEY ([EmailTypeId]) REFERENCES [dbo].[uCommerce_EmailType] ([EmailTypeId])
);

