﻿using Affinion.LoyaltyBuild.Api.Booking.Service;
using Affinion.LoyaltyBuild.Common.Utilities;
using Affinion.LoyaltyBuild.Model.Provider;
using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using UCommerce.Infrastructure;
using data = Affinion.LoyaltyBuild.Api.Booking.Data;
using Affinion.LoyaltyBuild.Web.ClientPortal.Helper;
using System.IO;
using System.Web.UI;
using SelectPdf;
using Affinion.LoyaltyBuild.Model.Product;
using Affinion.LoyaltyBuild.Api.Booking;

namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    public partial class OfflinePortalBookingDetails : BaseSublayout
    {
        int olid = 0;
        /// <summary>
        /// page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            IBookingService bookingService = ObjectFactory.Instance.Resolve<IBookingService>();


            int.TryParse(this.QueryString("olid"), out olid);

            //get booking
            var booking = bookingService.GetBooking(olid);

            //hide booking if it is invalid booking
            if (booking == null)
            {
                divBooking.Visible = false;
                return;
            }

            LoadDates(booking);
            LoadCustomer(booking);
            LoadPayments(booking);
            LoadAccomodation(booking);
            LoadOtherDetails(booking);
        }

        /// <summary>
        /// Load dates
        /// </summary>
        /// <param name="booking"></param>
        private void LoadDates(data.Booking booking)
        {
            var dateFormat = "dd/MM/yyyy-ddd";

            if (booking.CheckinDate != DateTime.MinValue)
                litArrivalDay.Text = booking.CheckinDate.ToString(dateFormat);
            if (booking.CheckoutDate != DateTime.MinValue)
                litDepartureDay.Text = booking.CheckoutDate.ToString(dateFormat);
            if (booking.CurrentStatusDate != null && booking.CurrentStatusDate != DateTime.MinValue)
                litStatusDay.Text = booking.CurrentStatusDate.Value.ToString(dateFormat);
            if (booking.ConfirmedStatusDate != null && booking.ConfirmedStatusDate != DateTime.MinValue)
                litConfirmationDay.Text = booking.ConfirmedStatusDate.Value.ToString(dateFormat);
            if (booking.OrderLine.CreatedOn != null && booking.OrderLine.CreatedOn != DateTime.MinValue)
                litReservationDay.Text = booking.OrderLine.CreatedOn.ToString(dateFormat);

        }

        /// <summary>
        /// Load dates
        /// </summary>
        /// <param name="booking"></param>
        private void LoadCustomer(data.Booking booking)
        {
            var order = UCommerce.EntitiesV2.PurchaseOrder.Get(booking.OrderId);

            if (order == null || order.Customer == null)
            {
                divCustomer.Visible = false;
                return;
            }

            var customer = order.Customer;
            litName.Text = string.Format("{0} {1}", customer.FirstName, customer.LastName);
            litPhone.Text = customer.PhoneNumber;
            litEmail.Text = customer.EmailAddress;
            var address = new List<string>();
            if (customer.Addresses != null && customer.Addresses.Count > 0)
            {
                var firstAddress = customer.Addresses[0];
                if (!string.IsNullOrEmpty(firstAddress.Line1))
                    address.Add(firstAddress.Line1);
                if (!string.IsNullOrEmpty(firstAddress.City))
                    address.Add(firstAddress.City);
                if (!string.IsNullOrEmpty(firstAddress.State))
                    address.Add(firstAddress.State);
                if (firstAddress.Country != null)
                    address.Add(firstAddress.Country.Name);
            }
            litAddress.Text = string.Join(", ", address);
        }

        /// <summary>
        /// Load dates
        /// </summary>
        /// <param name="booking"></param>
        private void LoadPayments(data.Booking booking)
        {
            litPrice.Text = CurrencyHelper.FormatCurrency(booking.Price + booking.ProcessingFee, booking.CurrencyId);
            litDeposit.Text = CurrencyHelper.FormatCurrency(booking.CommissionAmount, booking.CurrencyId);
            litProcesingFee.Text = CurrencyHelper.FormatCurrency(booking.ProcessingFee, booking.CurrencyId);
            var checkout = booking.ProviderAmountPaidOnCheckOut + booking.CommissionAmount + booking.ProcessingFee;
            litCheckout.Text = CurrencyHelper.FormatCurrency(checkout, booking.CurrencyId);
        }

        /// <summary>
        /// Load dates
        /// </summary>
        /// <param name="booking"></param>
        private void LoadAccomodation(data.Booking booking)
        {
            var provider = booking.GetProvider();
            if (provider != null)
            {
                var genericProvider = (IGenericProvider)provider;

                // litAccName.Text = Site
                litAccName.Text = provider.Name;
                litAccAddress.Text = genericProvider.Address;
                if (provider.ProviderType == ProviderType.BedBanks)
                {
                    var bbHotel = (BedBankHotel)provider;
                    litAccPhone.Text = bbHotel.Phone;
                    litAccEmail.Text = bbHotel.Email;
                    plcEmail.Visible = false;
                }
                else
                {
                    litAccPhone.Text = genericProvider.Phone;
                    litAccEmail.Text = genericProvider.Email;
                    plcEmail.Visible = true;
                }
            }

            switch (booking.Type)
            {
                case Affinion.LoyaltyBuild.Model.Product.ProductType.BedBanks:
                    var bbBooking = (data.BedBanksBooking)booking;
                    litNoofAdults.Text = bbBooking.NoOfAdults.ToString();
                    litNoofChildren.Text = bbBooking.NoOfChildren.ToString();
                    litNoOfNights.Text = bbBooking.Duration;
                    break;
                case Affinion.LoyaltyBuild.Model.Product.ProductType.Room:
                case Affinion.LoyaltyBuild.Model.Product.ProductType.Package:
                    var roomBooking = (data.RoomBooking)booking;
                    litNoofAdults.Text = roomBooking.NoOfAdults.ToString();
                    litNoofChildren.Text = roomBooking.NoOfChildren.ToString();
                    litNoOfNights.Text = roomBooking.NoOfNights.ToString();
                    break;
                case Affinion.LoyaltyBuild.Model.Product.ProductType.Addon:
                    break;
                default:
                    break;
            }

            var product = booking.GetProduct();
            if (provider.ProviderType == ProviderType.BedBanks)
            {
                BookingService bbBooking = new BookingService();
                var bookingData = bbBooking.GetBooking(olid);
                litOccupancyType.Text = string.IsNullOrEmpty(bookingData.OrderLine.GetOrderProperty(Constants.RoomInfo).Value) ? string.Empty : bookingData.OrderLine.GetOrderProperty(Constants.RoomInfo).Value;
            }
            else
            {
                if (product != null)
                {
                    litOccupancyType.Text = product.Name;
                }
            }

            litHotelDue.Text = CurrencyHelper.FormatCurrency((booking.ProviderAmount - booking.ProviderAmountPaidOnCheckOut), booking.CurrencyId);
            //litOutstanding
            litCost.Text = CurrencyHelper.FormatCurrency(booking.ProviderAmount, booking.CurrencyId);

            //get outstanding
            var paymentService = ObjectFactory.Instance.Resolve<IPaymentService>();
            var outstandingPayments = paymentService.GetDuesByOrderlineID(booking.OrderLineId, null);
            var payments = new List<string>();
            if (outstandingPayments != null)
            {
                foreach (var item in outstandingPayments)
                {
                    payments.Add(CurrencyHelper.FormatCurrency(item.Amount, item.CurrencyTypeId));
                }
            }
            litOutstanding.Text = string.Join(", ", payments);
        }

        /// <summary>
        /// Load dates
        /// </summary>
        /// <param name="booking"></param>
        private void LoadOtherDetails(data.Booking booking)
        {
            if (booking != null)
            {
                litCreatedBy.Text = booking.CreatedBy;//SitecoreUserProfileHelper.GetItem(UserProfileKeys.CreatedBy);
                litMethod.Text = booking.BookingThrough;
                litLastUpdate.Text = booking.UpdateDate.ToString("dd/MM/yyyy-ddd");
                litBookingRef.Text = booking.BookingReferenceNo;

            }

        }

        /// <summary>
        /// 
        /// </summary>
        private void BindSitecoreTexts()
        {
            Item currentItem = Sitecore.Context.Item;
            PrintButton.Text = currentItem.Fields["PrintText"].Value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PrintButton_Click(object sender, EventArgs e)
        {
            //if (startConversion)
            //{
            /// get html of the page
            TextWriter myWriter = new StringWriter();
            HtmlTextWriter htmlWriter = new HtmlTextWriter(myWriter);
            //this.RenderControl(htmlWriter);
            printcontent.RenderControl(htmlWriter);
            // instantiate a html to pdf converter object
            HtmlToPdf converter = new HtmlToPdf();

            // create a new pdf document converting the html string of the page
            SelectPdf.PdfDocument doc = converter.ConvertHtmlString(myWriter.ToString(), Request.Url.AbsoluteUri);

            // save pdf document
            doc.Save(Response, true, "Your Booking Confirmation.pdf");

            // close pdf document
            doc.Close();
            //}
        }
    }
}







