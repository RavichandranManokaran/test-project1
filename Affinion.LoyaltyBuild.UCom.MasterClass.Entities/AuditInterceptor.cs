﻿using NHibernate.Type;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Marketing.Awards;
using UCommerce.Marketing.Targets;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class AuditInterceptor : NHibernate.EmptyInterceptor
    {

        private int updates;
        private int creates;
        private int loads;

        public override void OnDelete(object entity,
                                      object id,
                                      object[] state,
                                      string[] propertyNames,
                                      IType[] types)
        {
            // do nothing
        }


        public override bool OnFlushDirty(object entity,
                                          object id,
                          object[] currentState,
                          object[] previousState,
                          string[] propertyNames,
                          IType[] types)
        {

            if (entity is ITarget || entity is IAward || entity is CampaignItem || entity is CampaignItemProperty)
            {

                StringBuilder olbString = new StringBuilder();
                StringBuilder newString = new StringBuilder();
                for (int i = 0; i < propertyNames.Length; i++)
                {
                    if (types[i] is NHibernate.Type.PrimitiveType
                        || types[i] is NHibernate.Type.StringType)
                    {
                        int ids = ((IEntity)entity).Id;
                        olbString.Append(string.Format("\"{0}\":\"{1}\"", propertyNames[i], previousState[i]));
                        newString.Append(string.Format("\"{0}\":\"{1}\"", propertyNames[i], currentState[i]));
                        if (currentState[i] == null && previousState[i] == null)
                        {
                        }
                        else if (currentState[i] != null && previousState[i] == null)
                        {
                        }
                        else if (currentState[i] == null && previousState[i] != null)
                        {
                        }
                        else if (currentState[i] != null && previousState[i] != null)
                        {
                            if (!currentState[i].Equals(previousState[i]))
                            {

                            }
                        }
                    }
                    //if ("lastUpdateTimestamp".Equals(propertyNames[i]))
                    //{
                    //    currentState[i] = new DateTime();
                    //    return true;
                    //}
                }
            }
            return true;
        }

        public override bool OnLoad(object entity,
                                    object id,
                    object[] state,
                    string[] propertyNames,
                    IType[] types)
        {
            loads++;

            return true;
        }

        public override bool OnSave(object entity,
                                    object id,
                    object[] state,
                    string[] propertyNames,
                    IType[] types)
        {

            creates++;
            for (int i = 0; i < propertyNames.Length; i++)
            {
                //if ("createTimestamp".Equals(propertyNames[i]))
                //{
                //    state[i] = new DateTime();
                //    return true;
                //}
            }

            return true;
        }

        public override void AfterTransactionCompletion(NHibernate.ITransaction tx)
        {
            if (tx.WasCommitted)
            {
                System.Console.WriteLine("Creations: " + creates + ", Updates: " + updates, "Loads: " + loads);
            }
            updates = 0;
            creates = 0;
            loads = 0;
        }

    }
}
