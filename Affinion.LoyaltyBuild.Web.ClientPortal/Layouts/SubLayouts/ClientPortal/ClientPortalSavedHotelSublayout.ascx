﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientPortalSavedHotelSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortalSavedHotelSublayout" %>

<asp:placeholder runat="server" ID="enableScript">
<script type="text/javascript">
    jQuery(document).ready(function () {
        var savedText ='<%=savedText%>';
        jQuery('.btnShowSavedHotels').click(function () {
            jQuery("#popupdivSavedHotels").dialog({
                title: savedText,
                position: 'absolute',
                stack: true,
                height: '600',
                width: '500',
                modal: true,
                buttons: {
                    "Close": function () {
                        $(this).dialog("close");
                    }
                }
            });
             

            jQuery("div").each(function () {
                if (jQuery(this).attr("aria-describedby") == "popupdivSavedHotels") {
                    var topPos = (jQuery(window).height() - jQuery(this).outerHeight()) / 2;
                    var leftPos = (jQuery(window).width() - jQuery(this).outerWidth()) / 2;
                    if (topPos<0) {
                        topPos = 0;
                    }
                    if (leftPos<0) {
                        leftPos = 0;
                    }
                    jQuery(this).css({
                        top: topPos,
                        left: leftPos
                    });
                }
            });

            return false;
        });
        
    });

</script>
</asp:placeholder>


<a href="#" class="btnShowSavedHotels basket float-right header-right hidden-xs padding-left-small" style="color:white; padding-top:11px;"><span class="glyphicon glyphicon-download"></span><sc:Text id="savedhotelText" runat="server" /></a>
<div id="popupdivSavedHotels"  title="Basic modal dialog" style="display: none; width:500px; height:50px; overflow-y:auto; margin:0 auto !important;" >
    <div id="myDialogBox">
        <asp:Repeater ID="rptHotelList" runat="server" OnItemDataBound="rptHotelList_DataBinding">
            <ItemTemplate>
                <div class="content_bg_inner_box summary-detail alert-info">
                    <div class="col-md-5 row col-sm-12">
                        <div class="col-xs-12 col-md-12">

                            <div class="basket-img">
                                <asp:Image ID="imgHotelImage" runat="server" CssClass="img-responsive img-thumbnail" />
                               <%-- <img alt="" src='<%#Eval("ImgSrc") %>' class="img-responsive img-thumbnail">--%>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7 col-sm-12">

                        <div class="row">
                            <div class="col-sm-12">
                                <span class="accent33-f">
                                    <h2><strong><u><asp:Hyperlink ID="linkHotelName" runat="server"></asp:Hyperlink></u></strong>
                                   <asp:Repeater ID="rptStar" runat="server">
                                       <ItemTemplate>
                                                          <span class="fa fa-star"></span>
                                   </ItemTemplate>
                                           </asp:Repeater>
                                    </h2>
                                    <asp:Label ID="lblLocation" runat="server"></asp:Label><br />
                                    </span>
                                </span>
                            </div>


                        </div>

                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>

