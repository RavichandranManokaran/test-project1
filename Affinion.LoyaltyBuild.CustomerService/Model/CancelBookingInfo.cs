﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model.Payment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    /// <summary>
    /// Entity containing Cancel booking related information
    /// </summary>
    public class CancelBookingInfo
    {
        /// <summary>
        /// OrderLine id
        /// </summary>
        public int OrderLineid { get; set; }

        /// <summary>
        /// Note to Provider
        /// </summary>
        public string noteToProvider { get; set; }

        /// <summary>
        /// Cancel booking Reason
        /// </summary>
        public string cancelReason { get; set; }
        
        /// <summary>
        /// Payment Details
        /// </summary>
        public PaymentDetail paymentDetails { get; set; }
        
        ///<summary>
        /// Createdby userlogin
        /// </summary>
        public string CreatedBy { get; set; }


    }
}
