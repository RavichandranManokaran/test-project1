﻿
CREATE PROCEDURE [dbo].[uCommerce_GetProductView] 
	@ProductCatalogId INT,
	@CategoryID INT,
	@CultureCode NVARCHAR(50),
	@ProductId INT = NULL,
	@IncludeVariants BIT = 0,
	@IncludeInvisibleProducts BIT = 0
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
            dbo.Product.ProductId, 
			dbo.Product.Sku, 
			dbo.Product.VariantSku, 
			dbo.Product.Name, 
			dbo.Product.DisplayOnSite, 
			dbo.Product.PrimaryImageMediaId, 
            dbo.Currency.ISOCode AS Currency, 
			dbo.PriceGroup.VATRate, 
			dbo.PriceGroupPrice.DiscountPrice AS DiscountPriceAmount, 
			dbo.PriceGroupPrice.Price AS PriceAmount, 
            dbo.ProductDefinition.Name AS ProductDefinitionDisplayName, 
			dbo.ProductDescription.CultureCode, dbo.ProductDescription.DisplayName, 
            dbo.ProductDescription.ShortDescription, 
			dbo.ProductDescription.LongDescription, 
			dbo.InventoryRecord.OnHandQuantity, 
			dbo.InventoryRecord.ReservedQuantity,                     
			dbo.InventoryRecord.Location, 
			dbo.ProductCatalog.Name AS ProductCatalogName, 
			dbo.ProductCatalog.ShowPricesIncludingVAT AS ShowPriceIncludingVAT,
			dbo.Product.ThumbnailImageMediaId
	FROM 
			dbo.Product 
			INNER JOIN dbo.ProductDefinition ON dbo.Product.ProductDefinitionId = dbo.ProductDefinition.ProductDefinitionId 
			INNER JOIN dbo.ProductDescription ON dbo.Product.ProductId = dbo.ProductDescription.ProductId -- Sproget version af beskrivelse, en række per sprog
			-- Catalog info
			LEFT JOIN dbo.CategoryProductRelation ON dbo.CategoryProductRelation.ProductId = dbo.Product.ProductId
			LEFT JOIN dbo.Category ON dbo.Category.CategoryId = dbo.CategoryProductRelation.CategoryId
			LEFT JOIN dbo.ProductCatalog ON dbo.ProductCatalog.ProductCatalogId = dbo.Category.ProductCatalogId
			-- Pricing Info
			LEFT JOIN dbo.PriceGroupPrice ON dbo.PriceGroupPrice.ProductId = dbo.Product.ProductId AND dbo.ProductCatalog.PriceGroupId = dbo.PriceGroupPrice.PriceGroupId
			LEFT JOIN dbo.PriceGroup ON dbo.PriceGroup.PriceGroupId = dbo.PriceGroupPrice.PriceGroupId
			LEFT JOIN dbo.Currency ON dbo.Currency.CurrencyId = dbo.PriceGroup.CurrencyId
			-- Inventory info
			LEFT OUTER JOIN dbo.Inventory ON dbo.ProductCatalog.InventoryId = dbo.Inventory.InventoryId 
			LEFT OUTER JOIN dbo.InventoryRecord ON dbo.Product.ProductId = dbo.InventoryRecord.ProductId AND dbo.Inventory.InventoryId = dbo.InventoryRecord.InventoryId
WHERE		
			(dbo.ProductCatalog.ProductCatalogId = @ProductCatalogId) AND 
			(dbo.ProductDescription.CultureCode = @CultureCode) AND 
			(dbo.CategoryProductRelation.CategoryId = @CategoryId) AND 
			(dbo.Product.ProductId = @ProductId OR @ProductId IS NULL) AND
			(dbo.Product.VariantSKU IS NULL OR @IncludeVariants = 1) 
			AND
			(
				(dbo.Product.DisplayOnSite = 1)
				OR
				(@IncludeInvisibleProducts = 1)
			)
END

