﻿CREATE TABLE [dbo].[uCommerce_PaymentMethodProperty] (
    [PaymentMethodPropertyId] INT            IDENTITY (1, 1) NOT NULL,
    [DefinitionFieldId]       INT            NOT NULL,
    [Value]                   NVARCHAR (MAX) NULL,
    [CultureCode]             NVARCHAR (60)  NULL,
    [PaymentMethodId]         INT            NOT NULL,
    CONSTRAINT [PK_uCommerce_PaymentMethodProperty] PRIMARY KEY CLUSTERED ([PaymentMethodPropertyId] ASC),
    CONSTRAINT [FK_uCommerce_PaymentMethodProperty_uCommerce_DefinitionField] FOREIGN KEY ([DefinitionFieldId]) REFERENCES [dbo].[uCommerce_DefinitionField] ([DefinitionFieldId]),
    CONSTRAINT [FK_uCommerce_PaymentMethodProperty_uCommerce_PaymentMethod] FOREIGN KEY ([PaymentMethodId]) REFERENCES [dbo].[uCommerce_PaymentMethod] ([PaymentMethodId]),
    CONSTRAINT [PaymentMethodProperty_Unique_PaymentMethodId_DefinitionFieldId] UNIQUE NONCLUSTERED ([PaymentMethodId] ASC, [CultureCode] ASC, [DefinitionFieldId] ASC)
);



