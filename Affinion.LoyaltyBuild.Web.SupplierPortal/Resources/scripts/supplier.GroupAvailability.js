﻿
function ReAssignHeight() {
    $.each($("table.cal-table tr"), function () {
        if ($(this).attr("trroom") != null) {
            $.each($(this).find("td"), function (index, child) {
                $(child).find('input').css("height", $(child).height());
                $(child).find('span').css("height", $(child).height());
            })
        }
    });
}


function roleDisableButton() {
    var roleBasedDisableButtons = $(".roleBasedDisableButtons").val();
    if (roleBasedDisableButtons == "true") {
        $("div.cal-table-top-left button").each(function () {
            $(this).attr('disabled', true);
        });
        $("div.cal-table-top-right button").each(function () {
            $(this).attr('disabled', true);
        });
        $("div.tabale-top input[type=text]").attr('disabled', true);
        $("div.offers button.Manage_btn").attr('disabled', false);
    }
}

//$("#menu-toggle1").click(function (e) {
//    e.preventDefault();
//    $("#leftbar").toggleClass("toggled");
//    if ($("#leftbar").hasClass('toggled')) {
//        $("#page-content-wrapper2").addClass("manageavailbilityfullwidth");
//    }
//    else {
//        $("#page-content-wrapper2").removeClass("manageavailbilityfullwidth");
//    }
//});

function offerDisableButton() {
    var manageOfferDisableButtons = $(".manageOfferDisableButtons").val();
    if (manageOfferDisableButtons == "true") {
        $("div.offers ul li a.edit i").css("display", "none");

    }
}

Supplier.GroupAvailability = new function () {
    var showallactiveoffer = false;
    var showallinactiveoffer = false;
    this.bulkclick = false;
    this.clearclick = false;
    var hashtable = {};
    var errorhash = {};
    var jsonObj = [];
    this.ChangeAvailability = function (ele, displayalert) {
        var month = $(ele).attr("month");
        if ($(ele).attr("blockedRoom") != null && $(ele).attr("allocatedRoom") != null) {
            Supplier.GroupAvailability.ChangeAllocationValue(ele, displayalert);

        }
        if (displayalert) {
            //$("#btn_" + month).removeAttr("disabled");
            $("#btn_0").removeAttr("disabled");
            $("#btn_1").removeAttr("disabled");
        }

    }

    this.ChangeAllocationValue = function (ele, displayalert) {
        var oldvalue = $(ele).attr("allocatedRoom");
        var newvalue = $(ele).val();

        if ($(ele).val() == '') {
            $(ele).val($(ele).attr("allocatedRoom"));
            $("#dvEmptyMsg").modal();
        }
        if (displayalert && $(ele).val() < parseInt($(ele).attr("blockedRoom"))) {
            $(ele).val(parseInt($(ele).attr("blockedRoom")));
            $("#dvLessRoomAllocationMsg").modal();
        }
        if ($(ele).val() > 999) {
            $(ele).val("999");
        }
        $("#span_" + $(ele).attr("hashid")).html($(ele).val());
        if (parseInt($(ele).val()) == 0 && parseInt($(ele).attr("allocatedRoom")) == 0) {
            if (hashtable.hasOwnProperty($(ele).attr("hashid"))) {
                delete hashtable[$(ele).attr("hashid")];
                Supplier.GroupAvailability.ChangeStyle($("#span_" + $(ele).attr("hashid")));
            }
            return;
        }
        //var weekendClass = $(ele).attr("weekend") == "True" ? "weekend" : "";
        //if (parseInt($(ele).val()) == parseInt($(ele).attr("blockedRoom"))) {
        //    if ($(ele).hasClass("bookable" + weekendClass)) {
        //        $(ele).removeClass("bookable" + weekendClass)
        //        $(ele).addClass("soldOut" + weekendClass)
        //    }
        //}
        //else if (parseInt($(ele).val()) > parseInt($(ele).attr("blockedRoom"))) {
        //    if ($(ele).hasClass("soldOut" + weekendClass)) {
        //        $(ele).removeClass("soldOut" + weekendClass)
        //        $(ele).addClass("bookable" + weekendClass)
        //    }
        //}

        //Supplier.GroupAvailability.ModifiedHashTable(ele);
        Supplier.GroupAvailability.SaveClosedDataInHashTable($("#span_" + $(ele).attr("hashid")));
        Supplier.GroupAvailability.ChangeStyle($("#span_" + $(ele).attr("hashid")));
    }

    this.Reopen = function (mon) {
        if ($("#closeout_" + mon).attr("closeroom") == "false") {
            $("#closeout_" + mon).attr("closeroom", "true")
            $('.EnbleText_' + mon).css('display', 'none');
            $('.EnbleSpan_' + mon).css('display', 'block !important');
            $('.EnbleSpan_' + mon).removeClass('EnbleSpan');


        }
        else {
            $('.EnbleText_' + mon).css('display', 'block !important');
            $('.EnbleSpan_' + mon).css('display', 'none');
            $('.EnbleSpan_' + mon).addClass('EnbleSpan');
            $("#closeout_" + mon).attr("closeroom", "false")
        }
    }
    this.CloseOut = function (mon, type) {
        if (type == 1) {

            $("#reopen_0").attr("reopenroom", "false");
            $("#res_0").attr("removerestriction", "false");
            $("#reopen_1").attr("reopenroom", "false");
            $("#res_1").attr("removerestriction", "false");
            if ($("#closeout_" + mon).attr("closeroom") == "false") {
                $("#closeout_1").attr("closeroom", "true")
                $("#closeout_0").attr("closeroom", "true")
                $("#dvReopenCloseMsg").modal();
                Supplier.GroupAvailability.InputToSpanView(0);
                //Supplier.GroupAvailability.InputToSpanView(mon);
            }
            else {
                $("#closeout_0").attr("closeroom", "false")
                $("#closeout_1").attr("closeroom", "false")
                //Supplier.GroupAvailability.SpanToInputView(mon);
                Supplier.GroupAvailability.SpanToInputView(0);
            }
            Supplier.GroupAvailability.CheckAllSpanforClosed(0);
            //Supplier.GroupAvailability.CheckAllSpanforClosed(mon);
        }
        else if (type == 2) {

            $("#closeout_0").attr("closeroom", "false");
            $("#res_0").attr("removerestriction", "false");
            $("#closeout_1").attr("closeroom", "false");
            $("#res_1").attr("removerestriction", "false");
            if ($("#reopen_" + mon).attr("reopenroom") == "false") {
                $("#reopen_0").attr("reopenroom", "true");
                $("#reopen_1").attr("reopenroom", "true");
                $("#dvReopenCloseMsg").modal();
                //Supplier.GroupAvailability.InputToSpanView(mon);
                Supplier.GroupAvailability.InputToSpanView(0);
            }
            else {
                $("#reopen_0").attr("reopenroom", "false")
                $("#reopen_1").attr("reopenroom", "false")
                //Supplier.GroupAvailability.SpanToInputView(mon);
                Supplier.GroupAvailability.SpanToInputView(0);
            }
            //Supplier.GroupAvailability.CheckAllSpanforClosed(mon);
            Supplier.GroupAvailability.CheckAllSpanforClosed(0);
        }
        else if (type == 3) {
            $("#reopen_0").attr("reopenroom", "false");
            $("#closeout_0").attr("closeroom", "false");
            $("#reopen_1").attr("reopenroom", "false");
            $("#closeout_1").attr("closeroom", "false");
            if ($("#res_" + mon).attr("removerestriction") == "false") {
                $("#res_0").attr("removerestriction", "true")
                $("#res_1").attr("removerestriction", "true")
                Supplier.GroupAvailability.InputToSpanView(0);
                Supplier.GroupAvailability.CheckAllSpanforOfferd(0);
            }
            else {
                $("#res_0").attr("removerestriction", "false");
                $("#res_1").attr("removerestriction", "false");
                Supplier.GroupAvailability.SpanToInputView(0);
            }
        }
    }
    this.SpanToInputView = function (mon) {
        $('.EnbleText_' + mon).css('display', 'block !important');
        $('.EnbleSpan_' + mon).css('display', 'none');
        $('.EnbleSpan_' + mon).addClass('EnbleSpan');
    }
    this.InputToSpanView = function (mon) {
        $('.EnbleText_' + mon).css('display', 'none');
        $('.EnbleSpan_' + mon).css('display', 'block !important');
        $('.EnbleSpan_' + mon).removeClass('EnbleSpan');
    }
    this.CloseOutForMonthEnabled = function (mon) {
        return (($("#closeout_0").attr("closeroom") == "true") || ($("#closeout_1").attr("closeroom") == "true"));
    }
    this.ReopenForMonthEnabled = function (mon) {
        return (($("#reopen_0").attr("reopenroom") == "true") || ($("#reopen_1").attr("reopenroom") == "true"));
    }
    this.RemoveRestrictionForMonthEnabled = function (mon) {
        return (($("#res_0").attr("removerestriction") == "true") || ($("#res_1").attr("removerestriction") == "true"));
    }
    this.BindOnClick = function () {
        $.each($('.EnbleSpan'), function () {

            $(this).click(function (el) {
                if (Supplier.GroupAvailability.CloseOutForMonthEnabled($(this).attr('hashid').split('_')[0])) {
                    Supplier.GroupAvailability.ChangeRoomAllocation($(this).attr('isclosed'), this);
                    Supplier.GroupAvailability.SaveClosedDataInHashTable(this);
                }
                else if (Supplier.GroupAvailability.ReopenForMonthEnabled($(this).attr('hashid').split('_')[0])) {
                    Supplier.GroupAvailability.ChangeRoomAllocation('true', this);
                    Supplier.GroupAvailability.SaveClosedDataInHashTable(this);
                }
                else if (Supplier.GroupAvailability.RemoveRestrictionForMonthEnabled($(this).attr('hashid').split('_')[0])) {
                    Supplier.GroupAvailability.ChangeRoomOfferApplicable($(this).attr('isofferapplicable'), this);
                    Supplier.GroupAvailability.SaveClosedDataInHashTable(this);
                }
            });
        });
        Supplier.GroupAvailability.BindOnlyNumber();
    }
    this.CloseOutRoom = function (obj, month, roomName) {
        var th = (obj);
        if (this.CloseOutForMonthEnabled(month)) {
            var roomolosed = $(th).attr('roomsClosed') == "true";
            $.each($("#dv_" + month).find('span[room=' + roomName + ']'), function (el) {
                if (roomolosed) {
                    Supplier.GroupAvailability.ChangeRoomAllocation('true', this);

                }
                else {
                    Supplier.GroupAvailability.ChangeRoomAllocation('false', this);
                }
                Supplier.GroupAvailability.SaveClosedDataInHashTable(this);

            });
            if ($(th).attr('roomsClosed') == "true") {
                $(th).attr('roomsClosed', 'false');
            }
            else {
                $(th).attr('roomsClosed', 'true');
            }
        }

        if (this.ReopenForMonthEnabled(month)) {

            var roomolosed = $(th).attr('roomsClosed');
            $.each($("#dv_" + month).find('span[room=' + roomName + ']'), function (el) {
                Supplier.GroupAvailability.ChangeRoomAllocation('true', this);
                Supplier.GroupAvailability.SaveClosedDataInHashTable(this);
            });

        }

        if (this.RemoveRestrictionForMonthEnabled(month)) {
            var roomolosed = $(th).attr('roomsRestriction');
            $.each($("#dv_" + month).find('span[room=' + roomName + ']'), function (el) {
                if ($(this).attr('isofferapplicable') == 'true') {
                    Supplier.GroupAvailability.ChangeRoomOfferApplicable('true', this);
                    Supplier.GroupAvailability.SaveClosedDataInHashTable(this);
                }
            });

        }


    }
    this.CloseOutDay = function (obj, month, day) {
        var th = (obj);
        if (this.CloseOutForMonthEnabled(month)) {
            $.each($("#dv_" + month).find('span[day=' + day + ']'), function (el) {

                if ($(th).attr('roomdaysClosed') == "true") {
                    Supplier.GroupAvailability.ChangeRoomAllocation('true', this);
                }
                else {
                    Supplier.GroupAvailability.ChangeRoomAllocation('false', this);
                }
                Supplier.GroupAvailability.SaveClosedDataInHashTable(this);


            });
            if ($(th).attr('roomdaysClosed') == "true") {
                $(th).attr('roomdaysClosed', 'false');
            }
            else {
                $(th).attr('roomdaysClosed', 'true');
            }
        }

        if (this.ReopenForMonthEnabled(month)) {
            $.each($("#dv_" + month).find('span[day=' + day + ']'), function (el) {

                Supplier.GroupAvailability.ChangeRoomAllocation('true', this);
                Supplier.GroupAvailability.SaveClosedDataInHashTable(this);
            });
        }

        if (this.RemoveRestrictionForMonthEnabled(month)) {
            $.each($("#dv_" + month).find('span[day=' + day + ']'), function (el) {
                if ($(this).attr('isofferapplicable') == 'true') {
                    Supplier.GroupAvailability.ChangeRoomOfferApplicable('true', this);
                    Supplier.GroupAvailability.SaveClosedDataInHashTable(this);
                }
            });
        }
    }
    //ROOM means Span 
    this.ChangeRoomAllocation = function (roomAvailable, obj) {
        if (roomAvailable == 'true') {
            $(obj).attr('isclosed', 'false');
            Supplier.GroupAvailability.RemoveAllClass(obj, 'default');
            Supplier.GroupAvailability.RemoveAllClass($("#" + $(obj).attr('hashid')), 'default');

        }
        else {
            $(obj).attr('isclosed', 'true');
            Supplier.GroupAvailability.RemoveAllClass(obj, 'closeOut');
            Supplier.GroupAvailability.RemoveAllClass($("#" + $(obj).attr('hashid')), 'closeOut');
        }

    };
    this.ChangeRoomOfferApplicable = function (offerAvailable, obj) {
        if (offerAvailable == 'true') {
            $(obj).attr('isofferapplicable', 'false');
            Supplier.GroupAvailability.RemoveAllClass(obj, 'default');
            Supplier.GroupAvailability.RemoveAllClass($("#" + $(obj).attr('hashid')), 'default');
        }
    };
    this.CheckAllSpanforOfferd = function (mon) {
        $.each($("#dv_" + mon).find($('.EnbleSpan_' + mon)), function (el) {
            if ($(this).attr('isclosed') == 'true' && $(this).attr('isofferapplicable') == 'true') {
                Supplier.GroupAvailability.RemoveAllClass(this, 'limits bookable');
                Supplier.GroupAvailability.RemoveAllClass($("#" + $(this).attr('hashid')), 'limits bookable');
            }
            ;
        });
    }
    this.CheckAllSpanforClosed = function (mon) {
        $.each($("#dv_" + mon).find($('.EnbleSpan_' + mon)), function (el) {
            if ($(this).attr('isclosed') == 'true' && $(this).attr('isofferapplicable') == 'true') {
                var weekendClass = $(this).attr("weekend") == "True" ? "weekend" : "";
                $(this).removeClass('limits' + weekendClass);
                $("#" + $(this).attr('hashid')).removeClass('limits' + weekendClass);
                $(this).addClass('closeOut' + weekendClass);
                $("#" + $(this).attr('hashid')).addClass('closeOut' + weekendClass);
            };
        });
    }
    this.RemoveAllClass = function (obj, classname) {
        var weekendClass = $(obj).attr("weekend") == "True" ? "weekend" : "";

        $(obj).removeClass('limits' + weekendClass);
        $(obj).removeClass('bookable' + weekendClass);
        $(obj).removeClass('closeOut' + weekendClass);
        $(obj).removeClass('soldOut' + weekendClass);
        var cloneObject;
        if ($(obj).is('span')) {
            cloneObject = obj;
        }
        else if ($(obj).is('input')) {
            cloneObject = $("#span_" + $(obj).attr("hashid"));
        }
        Supplier.GroupAvailability.ChangeStyle(cloneObject);
        $(obj).removeClass('default' + weekendClass);
        $(cloneObject).removeClass('default' + weekendClass);
        if (classname == 'default') {
            if ($(cloneObject).attr('isclosed') == 'true') {
                $(obj).addClass('closeOut' + weekendClass);
            }
            else if ($(cloneObject).attr('isofferapplicable') == 'true') {
                $(obj).addClass('limits' + weekendClass);
            }
            else if ($(cloneObject).attr('isclosed') == 'false' && $(cloneObject).attr('defaultclass') == ' closeOut' + weekendClass) {

                $(obj).addClass('bookable' + weekendClass);
            }
            else if ($(cloneObject).attr('isofferapplicable') == 'false' && $(cloneObject).attr('defaultclass') == ' limits' + weekendClass + ' bookable' + weekendClass) {
                $(obj).addClass('bookable' + weekendClass);
            }
            else {
                $(obj).addClass($(obj).attr('defaultclass'));
            }

            /*
            if ($(obj).attr('defaultclass') == ' limits bookable') {
                if ($(obj).attr('isofferapplicable') == 'true')
                    $(obj).addClass($(obj).attr('defaultclass'));
                else
                    $(obj).addClass('bookable');
            }
            else {
                $(obj).addClass($(obj).attr('defaultclass'));
            }*/

        }
        else if (classname == 'Parentdefault') {
            $(obj).addClass($(obj).attr('defaultclass'));
        }
        else {
            if (classname == 'limits bookable') {
                if ($(obj).attr("weekend") == "True") {
                    classname = 'limits' + weekendClass + ' bookable' + weekendClass;
                }
                $(obj).addClass(classname + weekendClass);
            }
            else {
                $(obj).addClass(classname + weekendClass);
            }
        }

    }
    this.ChangeStyle = function (obj) {
        var newroomvalue = $('#' + $(obj).attr('hashid')).val();
        var oldroomvalue = $('#' + $(obj).attr('hashid')).attr('allocatedroom');
        $(obj).removeClass('changestyle');
        $('#' + $(obj).attr('hashid')).removeClass('changestyle');
        if (parseInt(newroomvalue) != parseInt(oldroomvalue)) {
            $(obj).addClass('changestyle');
            $('#' + $(obj).attr('hashid')).addClass('changestyle');
        }
        else if ($(obj).attr('isclosed') != $(obj).attr('orgisclosed')) {
            $(obj).addClass('changestyle');
            $('#' + $(obj).attr('hashid')).addClass('changestyle');
        }
        else if ($(obj).attr('isofferapplicable') != $(obj).attr('orgisofferapplicable')) {
            $(obj).addClass('changestyle');
            $('#' + $(obj).attr('hashid')).addClass('changestyle');
        }
    }

    this.CreateJsonForUpdate = function (month) {

        for (var k in hashtable) {
            // use hasOwnProperty to filter out keys from the Object.prototype
            if (parseInt(k.split('_')[0]) == parseInt(month)) {
                var data = $("#" + k).attr('uniqidentifier');
                var id = data;
                var roomType = k.split('_')[1].replace('-', ' ');
                var day = k.split('_')[2];
                var availableRoom = hashtable[k][1];
                var item = {}
                //  item["Id"] = id;
                item["ProductSku"] = hashtable[k][2];
                item["DateCounter"] = hashtable[k][5];
                item["AvailableRoom"] = hashtable[k][1];
                item["IsCloseOut"] = hashtable[k][3];
                item["RemoveOffer"] = hashtable[k][4];
                jsonObj.push(item);
            }
        }

    }
    this.UpdateChanges = function (month) {
        jsonObj = [];
        for (var k in hashtable) {
            // use hasOwnProperty to filter out keys from the Object.prototype
            if (month == 0 || parseInt(k.split('_')[0]) == parseInt(month)) {
                var data = $("#" + k).attr('uniqidentifier');
                var id = data;
                var roomType = k.split('_')[1].replace('-', ' ');
                var day = k.split('_')[2];
                var availableRoom = hashtable[k][1];
                var item = {};
                item["ProductSku"] = hashtable[k][2];
                item["DateCounter"] = hashtable[k][5];
                item["AvailableRoom"] = hashtable[k][1];
                item["IsCloseOut"] = hashtable[k][3];
                item["RemoveOffer"] = (hashtable[k][4] == "false" ? true : false);
                jsonObj.push(item);
            }
        }
        if (jsonObj != null && jsonObj.length > 0)
            Supplier.GroupAvailability.SaveUpdateChanges(month);
    }

    //Hash table method
    this.SaveClosedDataInHashTable = function (ele) {
        var data = $(ele).attr('hashid');
        var room = $(ele).attr('room');
        var oldavailbleroom = $(ele).attr('allocatedroom');
        var availbleroom = $("#" + $(ele).attr('hashid')).val();
        var blocked = $(ele).attr('isclosed');
        var dateCounter = $("#" + $(ele).attr('hashid')).attr("datecounter");
        var month = data.split('_')[0];
        var day = data.split('_')[2];
        var year = new Date().getFullYear();
        var dateVar = year + "-" + month + "-" + day;
        var d = new Date(dateVar);
        var offerd = ($(ele).attr('isofferapplicable'));
        var orgBlocked = $(ele).attr('orgisclosed');
        var orgOfferd = $(ele).attr('orgisofferapplicable');
        hashtable[$(ele).attr("hashid")] = [oldavailbleroom, availbleroom, room, blocked, offerd, dateCounter, orgBlocked, orgOfferd];
        if (oldavailbleroom == availbleroom && blocked == orgBlocked && offerd == orgOfferd)
            delete hashtable[$(ele).attr("hashid")];


        /*    if (hashtable.hasOwnProperty($(ele).attr("hashid"))) {
                var k = hashtable[$(ele).attr("hashid")];
                hashtable[$(ele).attr("hashid")] = [oldavailbleroom, availbleroom, room, blocked, offerd, dateCounter, orgBlocked, orgOfferd];
            }
            else {
                hashtable[$(ele).attr("hashid")] = [oldavailbleroom, availbleroom, room, blocked, offerd, dateCounter, orgBlocked, orgOfferd];
            }*/
    }
    this.ClearChanges = function (month) {
        Supplier.GroupAvailability.ValidationBoxForClearChanges(month);

    }
    //Json Call
    this.SaveUpdateChanges = function (month) {
        var url = "/handler/RoomAvailibilityData.ashx";
        $.ajax({
            url: url,
            type: "POST",
            data: JSON.stringify(jsonObj),
            //data: JSON.stringify({ AvailabilityData: jsonObj }),
            contentType: "application/json",
            success: function (data) {
                if (data == "SUCCESS");
                {
                    for (var k in hashtable) {
                        // use hasOwnProperty to filter out keys from the Object.prototype
                        if (month == 0 || parseInt(k.split('_')[0]) == parseInt(month)) {
                            delete hashtable[k];
                        }
                    }
                    if (month == 0) {
                        Supplier.GroupAvailability.ViewByDateRange(0);
                    }
                    else {
                        Supplier.GroupAvailability.LoadRoomAvailibilityData(month);
                    }
                    $("#messageModal").modal();
                }
            },
            error: function (data, status, jqXHR) {
            }
        });
    }
    this.LoadRoomAvailibilityData = function (mon) {
        Supplier.GroupAvailability.ViewByDateRange(mon);
    }
    this.ViewBulkByDateRange = function (mon) {
        Supplier.GroupAvailability.bulkclick = true;
        Supplier.GroupAvailability.clearclick = false;
        hashtable = {};
        Supplier.GroupAvailability.ViewByDateRange(mon, true);
    }
    this.ClearBulkAvailability = function () {
        $.each($(".bulkavailability").find('input[type="text"]'), function (index, el) {
            $(el).val('');
        });
        hashtable = {};
        Supplier.GroupAvailability.clearclick = true;
        Supplier.GroupAvailability.ViewByDateRange(0, true);
    }
    this.ViewSideSearchByDateRange = function (mon) {
        $("#bulkupload-wrapper").removeClass("in");
        Supplier.GroupAvailability.bulkclick = false;
        Supplier.GroupAvailability.ViewByDateRange(mon, false);
    }
    this.ViewByDateRange = function (mon, isbulkupload) {
        if (typeof isbulkupload == 'undefined') {
            isbulkupload = false;
        }
        isbulkupload = Supplier.GroupAvailability.bulkclick;
        if (mon == 0) {
            for (var k in hashtable) {
                Supplier.GroupAvailability.ValidationBoxForClearChanges(0);
                return;
            }
        }
        showallinactiveoffer = false;
        showallactiveoffer = false;
        Supplier.GroupAvailability.ViewOfferByDateRange(0);
        var sDate = isbulkupload ? $('#budatepickerFrom').val() : $('#datepickerFrom').val();
        var eDate = isbulkupload ? $('#budatepickerTo').val() : $('#datepickerTo').val();

        var url = "/handler/GroupAvailabilityByDateRange.ashx";
        myApp.showPleaseWait();
        $.ajax({
            url: url,
            type: "GET",
            data: { "startDate": sDate, "endDate": eDate, "month": mon },
            success: function (data) {
                myApp.hidePleaseWait();
                for (var k in hashtable) {
                    // use hasOwnProperty to filter out keys from the Object.prototype
                    if (mon == 0 || parseInt(k.split('_')[0]) == parseInt(mon)) {
                        delete hashtable[k];
                    }
                }
                var dataLength = $("<div>").append(data).find("input").filter("[datecounter]").length;
                if (dataLength > 0) {
                    $(".btnpanel").show();
                }
                else {
                    $(".btnpanel").hide();
                }
                if (mon == 0) {
                    $('#dvContent').html('');
                    $('#dvContent').html(data);

                    if (isbulkupload) {
                        Supplier.GroupAvailability.LoadBulkAvilability();
                    }
                    else {
                        Supplier.GroupAvailability.ConfigureBulkUpload(true);
                    }
                    Supplier.GroupAvailability.BindOnClick();
                }
                else {
                    $("#dv_" + mon).html('');
                    $("#dv_" + mon).html(data);
                    Supplier.GroupAvailability.BindOnClick();
                }
                roleDisableButton();
                offerDisableButton();
            },
            error: function (data, status, jqXHR) {
                myApp.hidePleaseWait();
                $(".btnpanel").hide();
            }
        });
    }
    this.ViewOfferByDateRange = function (mon) {

        var sDate = $('#datepickerFrom').val();
        var eDate = $('#datepickerTo').val();
        var url = "/handler/OffersParticipating.ashx";
        myApp.showPleaseWait();
        $.ajax({
            url: url,
            type: "GET",
            cache: false,
            data: { "startDate": sDate, "endDate": eDate, "month": mon },
            success: function (data) {
                myApp.hidePleaseWait();
                $('#dvOfferContent').html('');
                $('#dvOfferContent').html(data);

                $("div.tooltip").remove();
                $("#tooltipcontainer a[title]").tooltips();
                $("#offerspart").on("hide.bs.collapse", function () {
                    $(".offerpart").html('<span class="glyphicon glyphicon-plus pull-right"></span>');
                });
                $("#offerspart").on("show.bs.collapse", function () {
                    $(".offerpart").html('<span class="glyphicon glyphicon-minus pull-right"></span>');
                });

                $("#offernotpart").on("hide.bs.collapse", function () {
                    $(".offernotpart").html('<span class="glyphicon glyphicon-plus pull-right"></span>');
                });
                $("#offernotpart").on("show.bs.collapse", function () {
                    $(".offernotpart").html('<span class="glyphicon glyphicon-minus pull-right"></span>');
                });


                if (showallinactiveoffer) {
                    Supplier.GroupAvailability.ShowInActiveOfferTrue();
                }
                if (showallactiveoffer) {
                    Supplier.GroupAvailability.ShowActiveOfferTrue();
                }
            },
            error: function (data, status, jqXHR) {
                myApp.hidePleaseWait();
            }
        });
    }


    this.ChangeCampaignSubscribe = function (supplierId, isSubscribe) {
        var url = "/handler/ChangeCampaignSubscribe.ashx";
        myApp.showPleaseWait();
        $.ajax({
            url: url,
            type: "GET",
            data: { "supplierId": supplierId, "isSubscribe": isSubscribe },
            success: function (data) {
                myApp.hidePleaseWait();
                $('#dvoffersubscribe').modal();
                Supplier.GroupAvailability.ViewOfferByDateRange(0);
            },
            error: function (data, status, jqXHR) {
                myApp.hidePleaseWait();
            }
        });
    }

    this.alertmessage = function () {
        var hasdata = false;
        for (var k in hashtable) {
            hasdata = true;
        }
        return hasdata;
    }

    this.ValidationBoxForClearChanges = function (mon) {
        $("#btnNoModalPopup").unbind("click");
        $("#btnUpdateModalPopup").unbind("click");
        var hasdata = false;
        for (var k in hashtable) {
            if (parseInt(mon) == 0 || parseInt(k.split('_')[0]) == parseInt(mon)) {
                hasdata = true;
                break;
            }
        }
        if (hasdata) {
            $("#btnUpdateModalPopup").click(function () { Supplier.GroupAvailability.UpdateChanges(mon); });
            $("#btnNoModalPopup").click(function () {
                if (mon == 0) {
                    hashtable = {};
                }
                Supplier.GroupAvailability.ViewByDateRange(mon);
            });
            $("#myModal").modal();
        }

    }

    this.SaveAllChanges = function () {

        Supplier.GroupAvailability.UpdateChanges(0);
    }

    this.ShowHideOffer = function (obj, classname, spanname) {


        if ($(obj).attr('isshow') == 'true') {
            $(obj).attr('isshow', 'false');
            $('.' + classname).css('display', 'none');
            $(obj).text('');
            $(obj).html("<span  class=\"glyphicon glyphicon-circle-arrow-down\"></span> Show More ");
            if (classname == "inactiveofferli") {
                showallinactiveoffer = false;
            }
            else {
                showallactiveoffer = false;
            }
        }
        else {
            $(obj).attr('isshow', 'true');
            $('.' + classname).css('display', 'block');
            $(obj).text('');
            $(obj).html("<span class=\"glyphicon glyphicon-circle-arrow-up\"></span> Hide More ");
            if (classname == "inactiveofferli") {
                showallinactiveoffer = true
            }
            else {
                showallactiveoffer = true;
            }

        }
    }

    this.ShowActiveOfferTrue = function () {

        $('#btnactiveoffer').attr('isshow', 'true');
        $('.activeofferli').css('display', 'block');
        $('#btnactiveoffer').text('');
        $('#btnactiveoffer').html("<span  class=\"glyphicon glyphicon-circle-arrow-up\"></span> Hide More ");
    }

    this.ShowInActiveOfferTrue = function () {

        $('#btninactiveoffer').attr('isshow', 'true');
        $('.inactiveofferli').css('display', 'block');
        $('#btninactiveoffer').text('');
        $('#btninactiveoffer').html("<span  class=\"glyphicon glyphicon-circle-arrow-up\"></span> Hide More ");
    }
    this.printDiv = function (id) {
        var html = "";

        $('link').each(function () { // find all <link tags that have
            if ($(this).attr('rel').indexOf('stylesheet') != -1) { // rel="stylesheet"
                html += '<link rel="stylesheet" href="' + $(this).attr("href") + '" />';
            }
        });
        var sDate = $('#datepickerFrom').val();
        var eDate = $('#datepickerTo').val();
        var d = new Date(),
		minutes = d.getMinutes().toString().length == 1 ? '0' + d.getMinutes() : d.getMinutes(),
		hours = d.getHours().toString().length == 1 ? '0' + d.getHours() : d.getHours(),
		ampm = d.getHours() >= 12 ? ' pm' : ' am',
	 months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        var displaystring = "Printed On : " + days[d.getDay()] + ' ' + months[d.getMonth()] + ' ' + d.getDate() + ' ' + d.getFullYear() + ' ' + hours + ':' + minutes + ampm;
        var header = ' <div id="header" style=”background-color:White;text-align:center;”>' + "Availability Report : from " + sDate + ' ' + hours + ':' + minutes + ampm + " To " + eDate + ' ' + hours + ':' + minutes + ampm + '</div><br>';
        var footer = '<br><br><div id="footer" style=”background-color:White;text-align:center;”>' + displaystring + '</div>';
        html += '<body onload="window.focus(); window.print()" >' + header + $("#" + id).html() + footer + '</body>';
        var w = window.open("", "print");
        if (w) { w.document.write(html); w.document.close() }
    };
    this.BindOnlyNumber = function () {
        $('.bindclick input[type="text"]').keypress(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .

            var code = e.keyCode || e.which;
            // if (code != 8 && code != 0 && (code < 48 || code > 57)) {
            // return false;
            // }
            if (code == 13) { e.preventDefault(); }
            if ($.inArray(code, [46, 8, 9, 27, 13]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                (code == 65 && (e.ctrlKey === true || e.metaKey === true))
                // Allow: home, end, left, right, down, up
                 ) {
                // let it happen, don't do anything
                return;
            }

            //// Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (code < 48 || code > 57)) && (code < 96 || code > 105)) {
                e.preventDefault();
            }
        }).on('drop', function (event) {
            event.preventDefault();
        }).on('keyup', function (event) {

            if ($(this).attr("month") != null) {
                var month = parseInt($(this).attr("month"));
                if (parseInt($(this).val()) != parseInt($(this).attr("allocatedRoom"))) {
                    //$("#btn_" + month).attr("disabled", "disabled");
                    $("#btn_0").attr("disabled", "disabled");
                    $("#btn_1").attr("disabled", "disabled");
                }
                else {
                    //$("#btn_" + month).removeAttr("disabled");
                    $("#btn_0").removeAttr("disabled");
                    $("#btn_1").removeAttr("disabled");
                }

            }
            if ($(this).val() != $(this).val().replace(/[^0-9]/g, '')) {
                $(this).val($(this).val().replace(/[^0-9]/g, ''));
            }
            if (event.keyCode == 13) { event.preventDefault(); }
        });
    }
    this.Init = function () {
        $.ajaxSetup({ cache: false });
        $('.container').addClass("fullwidth");
        //disabled enter form submit
        $("form").bind("keypress", function (e) {
            if (e.keyCode == 13) {
                return false;
            }
        });
        //var minDate = new Date($.datepicker.parseDate(Rgen.Config.DatePicker.format, $("#serverdate").val()));
        //var maxDate = new Date($.datepicker.parseDate(Rgen.Config.DatePicker.format, $("#serverdate").val())).addDays(89);

        //var minDate = new Date($.datepicker.parseDate(Rgen.Config.DatePicker.format, $("#serverdate").val())).addDays(-90);
        var minDate = new Date($.datepicker.parseDate(Rgen.Config.DatePicker.format, $("#serverdate").val())).addDays(-90);
        //minDate.setMonth(minDate.getMonth() - 3);
        var maxDate = new Date($.datepicker.parseDate(Rgen.Config.DatePicker.format, $("#serverdate").val()));
        maxDate.setFullYear(maxDate.getFullYear() + 1);

        var bulkminDate = new Date($.datepicker.parseDate(Rgen.Config.DatePicker.format, $("#serverdate").val()));
        var bulkmaxDate = new Date($.datepicker.parseDate(Rgen.Config.DatePicker.format, $("#serverdate").val()));
        bulkmaxDate.setFullYear(bulkmaxDate.getFullYear() + 1);

        $("#datepickerFrom").datepicker({
            dateFormat: Rgen.Config.DatePicker.format,
            showOn: "both",
            buttonText: Rgen.Config.DatePicker.Text,
            minDate: minDate,
            maxDate: maxDate,
            onSelect: function (dateStr) {
                var date = $.datepicker.parseDate(Rgen.Config.DatePicker.format, dateStr);
                $("#datepickerTo").datepicker("option", { minDate: new Date(date) })
            }
        });
        $("#datepickerTo").datepicker({
            dateFormat: Rgen.Config.DatePicker.format,
            showOn: "both",
            buttonText: Rgen.Config.DatePicker.Text,
            minDate: minDate,
            maxDate: maxDate,
        });


        $("#budatepickerFrom").datepicker({
            dateFormat: Rgen.Config.DatePicker.format,
            showOn: "both",
            buttonText: Rgen.Config.DatePicker.Text,
            minDate: bulkminDate,
            maxDate: bulkmaxDate,
            onSelect: function (dateStr) {
                var date = $.datepicker.parseDate(Rgen.Config.DatePicker.format, dateStr);
                $("#budatepickerTo").datepicker("option", { minDate: new Date(date) });
                Supplier.GroupAvailability.ConfigureBulkUpload();
            }
        });
        $("#budatepickerTo").datepicker({
            dateFormat: Rgen.Config.DatePicker.format,
            showOn: "both",
            buttonText: Rgen.Config.DatePicker.Text,
            minDate: bulkminDate,
            maxDate: bulkmaxDate,
            onSelect: function (dateStr) {
                var date = $.datepicker.parseDate(Rgen.Config.DatePicker.format, dateStr);
                Supplier.GroupAvailability.ConfigureBulkUpload();
            }
        });

        Supplier.GroupAvailability.ViewSideSearchByDateRange(0);


        $(window).bind("beforeunload", function (event) {
            if (Supplier.GroupAvailability.alertmessage()) {
                return "You have unsaved changes";
            }
        });

        $("#printavailability").on('click', function () {
            Supplier.GroupAvailability.printDiv("printable");

        });
    }
    this.RedirectToOffer = function (campaignItemId) {
        var url = $("#offerurl").val();
        var sDate = $('#datepickerFrom').val();
        var eDate = $('#datepickerTo').val();

        url = url + "?fromdate=" + sDate + "&todate=" + eDate + "&offerid=" + campaignItemId;
        window.location.href = url;

    }

    this.RemoveOffer = function (offerId, supplierInviteId) {
        $('#removeOfferId').val('0');
        $('#removeSupplierInviteId').val('0');

        var url = "/handler/Offer/RemoveOffer.ashx";
        $.ajax({
            url: url,
            data: { "offerId": offerId, "supplierInviteId": supplierInviteId },
            success: function (data) {
                if (data != "");
                {
                    $('#removeOfferId').val(offerId);
                    $('#removeSupplierInviteId').val(supplierInviteId);
                    $("#removeOfferContent").html(data);
                    $("#removal-offer").modal();
                }

            },
            error: function (data, status, jqXHR) {
            }
        });

    }

    


    this.UnSubScribeOffer = function () {
        var offerId = $('#removeOfferId').val();
        var supplierInviteId = $('#removeSupplierInviteId').val();;
        var reason = $("#drpReasonList").val();
        if (reason == '') {
            $("#dvOfferUnsubscribe").modal();
            return;
        }
        var url = "/handler/Offer/UnsubscribeOffer.ashx";
        $.ajax({
            url: url,
            type: "POST",
            data: { "offerId": offerId, "supplierInviteId": supplierInviteId, "Reason": reason },
            success: function (data) {
                if (data == "SUCCESS") {
                    $("#btncancelremoval").click();
                    Supplier.GroupAvailability.ViewByDateRange(0);
                }
            },
            error: function (data, status, jqXHR) {
            }
        });

    }

    this.ConfigureBulkUpload = function (setempty) {
        if (typeof isbulkupload == 'undefined') {
            setempty = false;
        }
        var startDate = $.datepicker.parseDate(Rgen.Config.DatePicker.format, $('#budatepickerFrom').val());
        var endDate = $.datepicker.parseDate(Rgen.Config.DatePicker.format, $('#budatepickerTo').val());
        var timeDiff = endDate.getTime() - startDate.getTime();
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        diffDays = parseInt(diffDays) + 1;
        if (diffDays < 7) {
            $.each($("table.bulkavailability").find("td,th"), function (index, data) {
                if ($(data).attr("dayofweek") != null) {
                    $(data).hide();
                }
            });
        }
        else {
            $("table.bulkavailability").find("td,th").show();
        }
        $.each($("table.bulkavailability").find('input[type="text"]'), function (index, el) {
            if (setempty) {
                $(el).val('');
            }
            $(el).unbind("blur");
            if (diffDays < 7) {
                $(el).addClass('disabledbooking');
                $(el).removeClass('form-control');
                $(el).removeClass('unallocate');
            }
            else {
                if (index == 0) {
                    //$(el).on("blur", 'Supplier.GroupAvailability.SetDefaultValue(this)');
                    $(el).on("blur", function () {
                        Supplier.GroupAvailability.SetDefaultValue(this);
                    });
                }

                $(el).removeClass('disabledbooking');
                $(el).addClass('form-control');
                $(el).addClass('unallocate');
            }
        });
        if (diffDays < 7) {

            var startdayofWeek = startDate.getDay();
            for (var i = 0; i < diffDays; i++) {
                var disabledday = parseInt(startdayofWeek) + i;
                $.each($("table.bulkavailability").find('td[dayofweek="' + disabledday + '"],th[dayofweek="' + disabledday + '"]'), function (index, data) {
                    $(data).show();
                });
                $.each($("table.bulkavailability").find('input[type="text"][dayofweek="' + disabledday + '"]'), function (chindex, chel) {
                    if (i == 0) {
                        $(chel).on("blur", function () {
                            Supplier.GroupAvailability.SetDefaultValue(this);
                        });
                    }
                    $(chel).removeClass('disabledbooking');
                    $(chel).addClass('form-control');
                    $(chel).addClass('unallocate');

                });

            }

            for (var i = (startdayofWeek + diffDays) ; (i % 7) != startdayofWeek ; i++) {
                var disabledday = (i % 7);
                $.each($("table.bulkavailability").find('input[type="text"][dayofweek="' + disabledday + '"]'), function (chindex, chel) {
                    $(chel).val('');

                });

            }
        }
        else {
            $.each($("table.bulkavailability").find('input[type="text"][dayofweek="1"]'), function (chindex, chel) {
                $(chel).on("blur", function () {
                    Supplier.GroupAvailability.SetDefaultValue(this);
                });
            });
        }

    }

    this.LoadBulkAvilability = function () {
        var showalert = false;
        errorhash = {};
        $("#datedetail").html('');
        $.each($(".bulkavailability").find('input[type="text"]'), function (index, el) {
            if ($(el).val() != '') {
                $.each($("#printable").find('input[type="text"][dayofweek="' + $(el).attr("dayofweek") + '"][room="' + $(el).attr("roomsku") + '"]'), function (chindex, chel) {
                    if (parseInt($(chel).attr("blockedroom")) > $(el).val()) {
                        showalert = true;
                        if (errorhash.hasOwnProperty($(el).attr("roomsku"))) {
                            var data = errorhash[$(el).attr("roomsku")][0] + "," + $(chel).attr("datecounter");
                            errorhash[$(el).attr("roomsku")] = [data, $(el).attr("roomname")];
                        }
                        else {
                            errorhash[$(el).attr("roomsku")] = [$(chel).attr("datecounter"), $(el).attr("roomname")];
                        }

                    }
                });
            }
        });
        if (showalert) {
            var msg = '';
            msg += "<table border=\"1\" class=\"tblbulkerror\" >";
            for (var k in errorhash) {
                msg += "<tr>";
                //var date =  (Rgen.Config.DatePicker.DefaultDate + days);
                if (errorhash.hasOwnProperty(k)) {
                    msg += "<td>";
                    msg += errorhash[k][1];
                    msg += "</td>";
                    var dayInfo = errorhash[k][0].split(",");
                    dayInfo.sort(function (a, b) { return parseInt(a) - parseInt(b) });
                    var defaultData = Rgen.Config.DatePicker.DefaultDate;
                    var dateData = '';
                    $.each(dayInfo, function (index, data) {
                        var customData = new Date(defaultData.getYear(), defaultData.getMonth(), defaultData.getDate());
                        customData.addDays(parseInt(data));
                        dateData += customData.getDate() + "/" + (customData.getMonth() + 1);
                        if (index != (dayInfo.length - 1)) {
                            dateData += ", ";
                        }
                        //alert(customData.addDays(parseInt(data)) + " " + defaultData);

                    });
                    msg += "<td>";
                    msg += dateData;
                    msg += "</td>";
                }
                msg += "</tr>";
            }
            msg += "</table>";
            $("#datedetail").html(msg);
            $("#dvBulkUpload").modal();

            return false;
        }
        else if (!showalert) {
            $.each($(".bulkavailability").find('input[type="text"]'), function (index, el) {
                if ($(el).val() != '') {
                    $.each($("#printable").find('input[type="text"][dayofweek="' + $(el).attr("dayofweek") + '"][room="' + $(el).attr("roomsku") + '"]'), function (chindex, chel) {
                        if (parseInt($(chel).attr("blockedroom")) <= $(el).val()) {
                            $(chel).val($(el).val());
                            Supplier.GroupAvailability.ChangeAllocationValue(chel, false);
                        }
                    });
                }
            });
            if (!Supplier.GroupAvailability.clearclick) {
                $("#dvReopenCloseMsg").modal();
            }
        }
    }

    this.SetDefaultValue = function (obj) {
        var dayOFweek = $(obj).attr("dayofweek");
        var roomSku = $(obj).attr("roomsku")
        var setValue = true;
        var compareValue = false;
        $.each($("table.bulkavailability").find('input[type="text"][roomsku="' + roomSku + '"]'), function (chindex, chel) {
            compareValue = true;
            if (parseInt($(chel).attr("dayofweek")) != parseInt(dayOFweek) && !$(chel).hasClass('disabledbooking')) {
                if ($(chel).val() != '') {
                    setValue = false;
                }
            }
        });
        if (setValue && compareValue) {
            $.each($("table.bulkavailability").find('input[type="text"][roomsku="' + roomSku + '"]'), function (chindex, chel) {
                if (parseInt($(chel).attr("dayofweek")) != parseInt(dayOFweek) && !$(chel).hasClass('disabledbooking')) {
                    $(chel).val($(obj).val());
                }
            });
        }
    }
};
$(function () {
    if ($("#pagetype").val() == "groupavailability") {
        Supplier.GroupAvailability.Init();

    }
});
