﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Subrate;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Configuration;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Subrate
{
    public partial class SubrateTemplate : System.Web.UI.UserControl
    {
        #region Global Var
        public string oClientID = "Global";
        public SubrateHelper _subrateHelper = new SubrateHelper();
        public List<int> lstVatRange = SiteConfiguration.GetVatRangeItem();

        #endregion

        #region EVENTS

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadSubrateTemplate();
                BindSubratesItem();
            }

        }

        protected void gvSubrateTemplates_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            ClearMessage();
            ClearErrorMessage();
            if (e.CommandName == "edittemplate")
            {
                string[] Args = Convert.ToString(e.CommandArgument).Split(new char[] { ',' });

                ItemID.Value = Args[0];
                txtItemName.Text = Args[1];
                txtItemName.Enabled = false;

                BindSubratesItem();

            }
            else if (e.CommandName == "deletetemplate")
            {
                int itemID = Convert.ToInt32(e.CommandArgument.ToString());
                int deleted = _subrateHelper.DeleteTemplate(itemID.ToString());
                if (deleted == 1)
                {
                    ItemID.Value = string.Empty;
                    LoadSubrateTemplate();
                    BindSubratesItem();
                }
            }
        }

        protected void rptSubrates_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                try
                {
                    DropDownList ddlVat = (DropDownList)e.Item.FindControl("ddlVatPercentage");
                    TextBox txtAmount = (TextBox)e.Item.FindControl("txtAmount");

                    ddlVat.DataSource = lstVatRange;
                    ddlVat.DataBind();

                    if (!String.IsNullOrEmpty(ItemID.Value))
                    {
                        var data = (SubratesDetail)e.Item.DataItem;
                        ddlVat.Items.FindByText(data.VAT.ToString()).Selected = true;
                        txtAmount.Text = data.Price.ToString();
                    }
                }
                catch (Exception ex)
                {
                    lblError.Text = ex.Message;
                    Sitecore.Diagnostics.Log.Error(ex.Message, new object());
                }
            }

        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            Reload();
            ClearErrorMessage();
            ClearMessage();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bool newRecord = false;
                if (String.IsNullOrEmpty(ItemID.Value))
                {
                    bool templateExist = _subrateHelper.GetAllSubrateTemplate().Any(x => x.TemplateName.Equals(txtItemName.Text) && (!x.SubrateTemplateCode.Equals(ItemID.Value)));
                    if (templateExist)
                        throw new Exception("Template with same name exist in database");
                    var TemplateCode = "Template_" + DateTime.Now.ToString("yyyyMMddHHmmss");
                    ItemID.Value = _subrateHelper.SaveTemplate(new SubrateTemplatesDetail(TemplateCode, txtItemName.Text, "Admin", DateTime.Now));
                    if (string.IsNullOrEmpty(ItemID.Value))
                    {
                        throw new Exception("Issue while saving template");
                    }
                    newRecord = true;

                }

                List<SubratesDetail> objSubrates = PreparesubratebojectList();

                //TODO : Call save method

                int result = _subrateHelper.SaveSubrates(objSubrates, string.Empty);

                if (result == 1)
                {
                    lblMessage.Text = "Template Saved Successfully";
                    ClearErrorMessage();
                }
                if (newRecord) { LoadSubrateTemplate(); }
                Reload();
            }
            catch (Exception er)
            {
                lblError.Text = er.Message.ToString();
                lblMessage.Text = string.Empty;
            }
        }

        protected void btnCancle_Click(object sender, EventArgs e)
        {
            Reload();
        }

        #endregion

        #region METHODS

        protected void LoadSubrateTemplate()
        {
            try
            {
                List<SubrateTemplatesDetail> objSubrates = _subrateHelper.GetAllSubrateTemplate();
                gvSubrateTemplates.DataSource = objSubrates;
                gvSubrateTemplates.DataBind();
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
                Sitecore.Diagnostics.Log.Error(ex.Message, new object());
            }
        }


        protected void BindSubratesItem()
        {
            try
            {
                //dvEditRate.Visible = true;
                List<SubratesDetail> objSubratesData = new List<SubratesDetail>();

                List<SubratesDetail> objSubrates = new List<SubratesDetail>();

                List<SubrateItemDetail> objSubrateItems = _subrateHelper.GetAllSubRateItems();
                if (!string.IsNullOrEmpty(ItemID.Value))
                {
                    objSubrates = _subrateHelper.GetSubratesBySintelItem(ItemID.Value);
                }
                foreach (var item in objSubrateItems)
                {
                    SubratesDetail rateDetail = new SubratesDetail();
                    SubratesDetail itemDetail = objSubrates.FirstOrDefault(x => x.SubrateItemCode == item.SubrateItemCode) ?? new SubratesDetail();
                    rateDetail.ClientID = ClientID;
                    rateDetail.ItemID = ItemID.Value;
                    rateDetail.ItemName = item.ItemName;
                    rateDetail.SubrateItemCode = item.SubrateItemCode;
                    rateDetail.Price = itemDetail.Price;
                    rateDetail.IsPercentage = itemDetail.IsPercentage;
                    rateDetail.VAT = itemDetail.VAT;

                    objSubratesData.Add(rateDetail);
                }

                rptSubrates.DataSource = objSubratesData;
                rptSubrates.DataBind();
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
                Sitecore.Diagnostics.Log.Error(ex.Message, new object());
            }
        }


        /*    protected void LoadSubratesItem()
            {
                try
                {
                    List<SubrateItemDetail> objSubrateItems = _subrateHelper.GetAllSubRateItems();
                    rptSubrates.DataSource = objSubrateItems;
                    rptSubrates.DataBind();
                }
                catch (Exception ex)
                {
                    lblError.Text = ex.Message;
                    Sitecore.Diagnostics.Log.Error(ex.Message, new object());
                }
            }

            protected void LoadSubratesItem(List<SubratesDetail> objSubrates)
            {
                try
                {
                    rptSubrates.DataSource = objSubrates;
                    rptSubrates.DataBind();
                }
                catch (Exception ex)
                {
                    lblError.Text = ex.Message;
                    Sitecore.Diagnostics.Log.Error(ex.Message, new object());
                }
            }*/

        protected void Reload()
        {
            ItemID.Value = "";
            txtItemName.Text = "";
            txtItemName.Enabled = true;
            BindSubratesItem();
        }

        protected void ClearErrorMessage()
        {
            lblError.Text = "";
        }

        protected void ClearMessage()
        {
            lblMessage.Text = "";
        }

        protected List<SubratesDetail> GetSubratesBySingleItem(string ItemID)
        {
            List<SubratesDetail> objSubRates = new List<SubratesDetail>();
            objSubRates = _subrateHelper.GetSubratesBySintelItem(ItemID);
            return objSubRates;

        }

        protected List<SubratesDetail> PreparesubratebojectList()
        {
            try
            {
                List<SubratesDetail> objSubrate = new List<SubratesDetail>();

                foreach (RepeaterItem item in rptSubrates.Items)
                {
                    objSubrate.Add(Preparesubrateboject(item));
                }
                return objSubrate;
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
                Sitecore.Diagnostics.Log.Error(ex.Message, new object());
                return null;
            }
        }

        protected SubratesDetail Preparesubrateboject(RepeaterItem item)
        {
            try
            {
                SubratesDetail objSubrate = new SubratesDetail();

                objSubrate.ItemID = ItemID.Value;
                objSubrate.SubrateItemCode = ((Label)item.FindControl("lblSRCode")).Text; ;
                objSubrate.UpdtedOn = DateTime.Now;

                var price = ((TextBox)item.FindControl("txtAmount")).Text;

                if (!String.IsNullOrEmpty(price))
                {
                    objSubrate.Price = Convert.ToDecimal(price);
                    objSubrate.IsPercentage = false;
                }

                var VatAmountPercentage = ((DropDownList)item.FindControl("ddlVatPercentage")).SelectedItem.Text;
                objSubrate.IsPercentage = true;
                objSubrate.VAT = Convert.ToDecimal(VatAmountPercentage);

                objSubrate.CreatedBy = "Admin";
                objSubrate.ClientID = "Global";
                return objSubrate;

            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
                Sitecore.Diagnostics.Log.Error(ex.Message, new object());
                return null;
            }
        }

        #endregion
    }
}