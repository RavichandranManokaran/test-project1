﻿using Affinion.LoyaltyBuild.UCom.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Licensing;
using UCommerce.Marketing.Targets;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities
{
    public class PackageTypeTarget : Target, IOrderLineTarget, IEntity, ITarget
    {
        private int? _hashCode;

        public virtual bool IsSatisfiedBy(OrderLine orderLine)
        {
            OrderProperty searchIgnor = orderLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.IgnorCampaignSearchCriteria);
            if (searchIgnor != null)
                return true;
            OrderProperty forSearch = orderLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.ForSearch);
            if (forSearch != null)
            {
                OrderProperty roomTypes = orderLine.OrderProperties.FirstOrDefault(x => x.Key == OrderPropertyConstants.PackageType);
                if (roomTypes != null)
                {
                    return this.PackageType.Equals(roomTypes.Value, StringComparison.InvariantCultureIgnoreCase);
                }
                return false;
            }
            else
            {
                Product p = Product.All().Where(x => x.Sku == orderLine.Sku && x.VariantSku == orderLine.VariantSku).FirstOrDefault();
                if (p.ProductProperties != null && p.ProductProperties.Any(x => x.ProductDefinitionField.Name.Equals(ProductDefinationVairantConstant.PackageType, StringComparison.InvariantCultureIgnoreCase) && x.Value.Equals(this.PackageType, StringComparison.InvariantCultureIgnoreCase)))
                {
                    return true;
                }
            }
            return false;


        }


        // Methods
        public new static IQueryable<PackageTypeTarget> All()
        {
            return GetRepo().Select();
        }

        public override void Delete()
        {
            GetRepo().Delete(this);
        }

        public static void Delete(Expression<Func<PackageTypeTarget, bool>> expression)
        {
            GetRepo().DeleteMany(expression);
        }

        public override bool Equals(object obj)
        {
            PackageTypeTarget objA = obj as PackageTypeTarget;
            if (object.ReferenceEquals(objA, null))
            {
                return false;
            }
            bool flag = object.Equals(objA.Id, 0);
            bool flag2 = object.Equals(this.Id, 0);
            if (flag && flag2)
            {
                return object.ReferenceEquals(this, obj);
            }
            return this.Id.Equals(objA.Id);
        }

        public static bool Exists(Expression<Func<PackageTypeTarget, bool>> expression)
        {
            return All().Any<PackageTypeTarget>(expression);
        }

        public static IList<PackageTypeTarget> Find(Expression<Func<PackageTypeTarget, bool>> expression)
        {
            return GetRepo().Select(expression).ToList<PackageTypeTarget>();
        }

        public static PackageTypeTarget FirstOrDefault(Expression<Func<PackageTypeTarget, bool>> expression)
        {
            return GetRepo().Select(expression).FirstOrDefault<PackageTypeTarget>();
        }

        public new static PackageTypeTarget Get(object id)
        {
            return GetRepo().Get(id);
        }

        public override int GetHashCode()
        {
            if (!this._hashCode.HasValue)
            {
                this._hashCode = new int?(this.Id.GetHashCode());
            }
            return this._hashCode.Value;
        }

        internal static IRepository<PackageTypeTarget> GetRepo()
        {
            LicensingService.AuthenticateLicense();
            return ObjectFactory.Instance.Resolve<IRepository<PackageTypeTarget>>();

        }


        public override void Save()
        {
            GetRepo().Save(this);
        }

        public static PackageTypeTarget SingleOrDefault(Expression<Func<PackageTypeTarget, bool>> expression)
        {
            return GetRepo().SingleOrDefault(expression);
        }

        // Properties
        public override int Id
        {
            get
            {
                return this.PackageTypeTargetId;
            }
        }

        public virtual int PackageTypeTargetId { get; protected set; }

        public virtual string PackageType { get; set; }






    }
}
