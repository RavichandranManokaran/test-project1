﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.Extensions;
using UCommerce.Runtime;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.Layouts.Sublayouts
{
    public partial class CategoryDetails : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var category = SiteContext.Current.CatalogContext.CurrentCategory;
                CategoryNameLabel.Text = category.DisplayName();

                CategoryDescriptionLabel.Text = category.DynamicProperty<int>().Brand;
            }
        }
    }
}