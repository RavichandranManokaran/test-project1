﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    /// <summary>
    /// Search customer
    /// </summary>
    public class SearchCustomer
    {
        /// <summary>
        /// First name of the customer
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Last name of the customer
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Customer AddressLine1
        /// </summary>
        public string AddressLine1 { get; set; }

        /// <summary>
        /// Customer AddressLine1
        /// </summary>
        public string AddressLine2 { get; set; }

        /// <summary>
        /// Customer Phone Number
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Country Id
        /// </summary>
        public int? CountryId { get; set; }

        /// <summary>
        /// Location Id
        /// </summary>
        public int? LocationId { get; set; }
    }
}
