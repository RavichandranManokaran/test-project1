﻿namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal
{
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Sitecore.Data.Items;
    using Affinion.LoyaltyBuild.Common;
    using System;

    public partial class ClientPortalFooterSublayout : BaseSublayout
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Item rootItem = ItemHelper.RootItem;
            if (rootItem == null)
            {
                // this will be always null on standard values item
                return;
            }

           ///get the root path of child item
            string parentItemPath = rootItem.Paths.FullPath + Constants.copyrightpath;
            if (parentItemPath != null)
            {
                Item item = Sitecore.Context.Database.GetItem(parentItemPath);
                if (item != null)
                {
                    using (new Sitecore.Security.Accounts.UserSwitcher(ItemHelper.ContentEditingUser))
                    {
                        item.Editing.BeginEdit();
                        ///get the copy rights fields value 
                        string copyrighttext = item.Fields["Title"].Value;
                        ///get the system current year
                        string currentYear = DateTime.Now.Year.ToString();
                        ///split the year from copyrighttext variable
                        string copyrighttextoldyear = (copyrighttext.Substring(1, 4));
                        ///condition check whether copy right filed year is same to current year 
                        if (copyrighttextoldyear != currentYear)
                        {
                            ///if the year is different replace the system current year
                            item.Fields["Title"].Value = item.Fields["Title"].Value.Replace(copyrighttextoldyear, currentYear);
                        }
                        else
                        {

                        }
                        item.Editing.EndEdit();
                    }
                }
            }

            
        }
    }
}