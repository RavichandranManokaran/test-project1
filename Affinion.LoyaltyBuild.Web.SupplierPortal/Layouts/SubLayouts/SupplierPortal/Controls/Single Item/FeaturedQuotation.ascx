﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FeaturedQuotation.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Single_Item.FeaturedQuotation" %>

<asp:Panel runat="server" ID="pnlQuotation">
  <blockquote>
    <p><sc:FieldRenderer runat="server" FieldName="Quotation" ID="quotation" /></p>
    <p class="autor"><sc:FieldRenderer runat="server" FieldName="Author" ID="author" /></p>
  </blockquote>
</asp:Panel>