﻿<%@ control language="C#" autoeventwireup="true" codebehind="AvailabilityView.ascx.cs" inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.Availability.SupplierPortal.Controls.AvailabilityView" %>
  <div class="container-outer width-full overflow-hidden-att app-bg">
    <div class="row">
      <div class="col-md-12">
      </div>
    </div>
    <div class="middle-body-container">
      <div class="row">
        <div class="col-md-12">
          <h4 class="cal-header">
            <asp:label id="lblInfo" runat="server">
            </asp:label>
          </h4>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <h4 class="cal-header">Manage Availability 
          </h4>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 no-padding">
          <div class="table-cal-header">
            <div class="col-md-2 no-padding" id="leftbar">
              <div class="offers-top">
                <div class="offers datePickerPanel">
                  <h2 class="no-margin">Choose Date Range
                  </h2>
                  <div class="col-md-12 cal-dates padding-top12 no-padding-right clear">
                    <div class="col-md-2 col-xs-2  datepickerTitle">From
                    </div>
                    <div class="col-md-10 col-xs-10  float-right no-padding-right">
                      <input readonly="true" type="text" id="datepickerFrom" placeholder="DD/MM/YYYY" runat="server" class="form-control" clientidmode="Static" />
                    </div>
                  </div>
                  <div class="col-md-12 cal-dates padding-top12 no-padding-right clear">
                    <div class="col-md-2 col-xs-2 datepickerTitle">To
                    </div>
                    <div class="col-md-10 col-xs-10 float-right no-padding-right">
                      <input readonly="true" type="text" id="datepickerTo" placeholder="DD/MM/YYYY" runat="server" class="form-control" clientidmode="Static" />
                      <input readonly="true" type="hidden" id="serverdate" placeholder="DD/MM/YYYY" runat="server" class="form-control" clientidmode="Static" />
                    </div>
                  </div>
                </div>
                <div class="dropdown padding-15 float-right clear">
                  <button type="button" class="btn  btn-info" onclick="Supplier.ManageAvailability.ViewSideSearchByDateRange(0)">View 
                    <span class="glyphicon glyphicon-right-arrow" aria-hidden="true">
                    </span>
                  </button>
                </div>
              </div>
              <div class="offers-top">
                <div class="offers">
                  <h2 class="no-margin">Add-Ons 
                  </h2>
                  <div class=" col-md-12 addon">
                    <asp:Repeater ID="addOnsRepeater" runat="server">
                      <ItemTemplate>
                        <h5>
                          <b>
                            <asp:Label ID="productNamelbl" runat="server" Text='<%#Eval("ProductName")%>' EnableViewState="false" ReadOnly="true">
                            </asp:Label>
                          </b>
                        </h5>
                        <ul class="offers-det" id="offerMore1">
                          <li>Price 
                            <div class="pull-right">
                              <asp:Label ID="productPricelbl" runat="server" Text='<%#Eval("ProductPrice")%>' EnableViewState="false" ReadOnly="true">
                              </asp:Label>
                            </div>
                            <div>&nbsp;
                              <div class="pull-right">
                                <small>
                                  (
                                  <asp:Label ID="priceCalculationLabel" runat="server" Text='<%#Eval("ProviderPrice")%>' EnableViewState="false" ReadOnly="true">
                                  </asp:Label>+ 
                                  <asp:Label ID="lbShareLBL" runat="server" Text='<%#Eval("LBShare")%> ' EnableViewState="false" ReadOnly="true">
                                  </asp:Label>)  
                                </small>
                              </div>
                            </div>
                          </li>
                          <li>Package Price 
                            <div class="pull-right">
                              <asp:Label ID="productPackagePriceLbl" runat="server" Text='<%#Eval("PackagePrice")%>' EnableViewState="false" ReadOnly="true"/>
                            </div>
                            <div>&nbsp;
                              <div class="pull-right">
                                <small>
                                  (
                                  <asp:Label ID="packageProceLbl" runat="server" Text='<%#Eval("ProviderPKGPrice")%>' EnableViewState="false" ReadOnly="true"/>+
                                  <asp:Label ID="lbPackageProceLbl" runat="server" Text='<%#Eval("LBSharePKG")%>' EnableViewState="false" ReadOnly="true"/>)                                                       
                                </small>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </ItemTemplate>
                    </asp:Repeater>
                  </div>
                </div>
              </div>
              <div class="offers-content">
                <input type="hidden" name="offerurl" id="offerurl" runat="server" clientidmode="Static" />
                <input type="hidden" name="pagetype" id="pagetype" value="manageavailability" clientidmode="Static" />
                <asp:panel id="dvOfferContent" runat="server" clientidmode="Static">
                </asp:panel>
              </div>
            </div>
            <div class="col-md-10 no-padding-right" id="page-content-wrapper2">
              <div class="row">
                <div class="col-md-12">
                  <div class=" cal-table ">
                    <a href="#menu-toggle1" class="btn btn-info font-size-12" id="menu-toggle1">
                      <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true">
                      </span>&nbsp;&nbsp;Toggle Menu
                    </a>
                    <%--
                         <button class="print-link btn" id="printavailability">Print
                    </button>--%>
                  <button class="btn btn-info btn-xs" type="button" id="printavailability">
                    <!-- Remove icon change -->
                    <span aria-hidden="true" class="glyphicon glyphicon-print">
                    </span>&nbsp;&nbsp;Print
                    <!-- Remove icon change End-->
                  </button>
                  <span>
                    <span class="unallocate colorguidewidth">
                    </span>Unallocated
                    <span class="bookable colorguidewidth">
                    </span>Bookable
                    <span class="bookable limits colorguidewidth">
                    </span>Bookable with limits
                    <span class="closeOut colorguidewidth">
                    </span>Closed out
                    <span class="soldOut colorguidewidth">
                    </span>Sold out
                  </span>
                  <asp:button id="bckHomeButton" runat="server" text="Home" onclick="bckHomeButton_Click" class="btn btn-info btn-xs" />
                </div>
              </div>
            </div>
            <%
               string style = "background-color: rgb(204, 204, 204);";
               string headstyle = "background-color: rgb(92, 131, 172); ";
               %>
              <div id="bulk-upload">
                <div class="cal-table">
                  <div class="cal-hed">
                     <a data-toggle="collapse" href="#bulkupload-wrapper" class="show-hide-list">
                      <span>Bulk Upload Availaibility for Choosen Dates</span>
                      <%--<span class="glyphicon glyphicon-triangle-bottom pull-right"></span>--%>
                    </a>
                  </div>
                </div>
                <div class="tabale-top collapse" id="bulkupload-wrapper">
                  <div class="col-sm-6 padding-top12 pull-right">
                    <div class="row margin-row" id="bulkupload-date">
                      <div class="col-sm-1 col-xs-1 no-padding-right">
                        <span class="bask-form-titl font-size-12">From:
                        </span>
                      </div>
                      <div class="col-sm-5 col-xs-5 no-padding-right">
                        <input readonly="true" type="text" id="budatepickerFrom" placeholder="DD/MM/YYYY" runat="server" class="form-control" clientidmode="Static" />
                      </div>
                      <div class="col-sm-1 col-xs-1 no-padding-right">
                        <span class="bask-form-titl font-size-12">To:
                        </span>
                      </div>
                      <div class="col-sm-5 col-xs-5 no-padding-right">
                        <input readonly="true" type="text" id="budatepickerTo" placeholder="DD/MM/YYYY" runat="server" class="form-control" clientidmode="Static" />
                      </div>
                    </div>
                  </div>
                  <table class="cal-table bulkavailability marT12">
                    <tr>
                      <th class="left-detail">
                        <div class="col-md-8 font-weight-bold detail-cal no-padding">&nbsp;
                        </div>
                        <div class="col-md-4 no-padding">&nbsp;
                        </div>
                      </th>
                      <%for (int i = 1; i 
                            <= 7; i++)
                      {         
                      %>
                      <th dayofweek="<%=(i%7)%>" style="<%=(i%7==0 || i%7==6? headstyle: "")%>" class="<%=(i%7==0 || i%7==6? "headerSortUp": "")%>" >
                        <span>
                          <%= ((DayOfWeek)(i%7)).ToString() %>
                            </span>
                          <div>Availability / Pricing
                          </div>
                          </th>
                        <%} %>
                          </tr>
                        <tr class="labelRow">
                          <td class="left-detail  transparent">
                            <div class="col-md-8 font-weight-bold detail-cal no-padding">&nbsp;
                            </div>
                            <div class="col-md-4 no-padding">
                              <span class="glyphicon glyphicon-record">
                              </span>
                            </div>
                          </td>
                          <%for (int i = 1; i 
                                <= 7; i++)
                          {    %>
                          <td dayofweek="<%=(i%7)%>" style="<%=(i%7==0 || i%7==6? style: "")%>" class="<%=(i%7==0 || i%7==6? "headerSortdown": "")%>" >
                            <span class="glyphicon glyphicon-download">
                            </span>
                          </td>
                          <%} %>
                            </tr>
                      <%if (productDetail!=null && productDetail.Count > 0)
                        { %>
                          <%foreach (var item in productDetail)
                                    {%>
                      <% if(item.ProductType==1||item.ProductType==2){%>
                            <tr>
                              <td class="left-detail">
                                <div class="col-md-8 font-weight-bold detail-cal no-padding">
                                  <%=item.ProductName %>
                                    </div>
                                  <div class="col-md-4 no-padding">
                                    <span class="glyphicon glyphicon-circle-arrow-right">
                                    </span>
                                  </div>
                                  </td>
                                <%for (int i = 1; i 
                                      <= 7; i++)
                                {    %>
                              <td dayofweek="<%=(i%7)%>" style="<%=(i%7==0 || i%7==6? style: "")%>">
                                <input type="text" value="" roomsku="<%=item.CombineSKU.Trim()%>" roomname="<%=item.ProductName%>" dayofweek="<%=(i%7)%>"  
                                       <%=Disable%> maxlength="3" style="
                                <%=(i%7==0 || i%7==6? style: "")%>">
                                  <div class="slash">/
                                  </div>
                                  <% if(item.ProductType==1){%>
                                  <div class="form-group float-left wid45">
                                  <select data-dropupauto="false" class="form-control bulkpricedropdown show-tick" roomsku="<%=item.CombineSKU.Trim()%>" roomname="<%=item.ProductName%>" dayofweek="<%=(i%7)%>" 
                                          <%=Disable%> 
                                      data-none-selected-text="">
                                  <option data-hidden="true"></option>
                                  <%foreach(var p in item.PriceBand)
                                                { %>
                                      <option data-subtext="(<%= p.Rate%>+ <%= p.Commision%>)" value="<%= p.ItemId %>" price="<%= p.Price%>"><span class="text"> <%=string.Format("{0}", (p.Price == 0 ? "0" : string.Format("{0}",p.Price))) %> </span></option>
                                    <%--<option  data-subtext="(<%= p.Rate%>+ <%= p.Commision%>)" value="<%= p.ItemId%>" price="<%= p.Price%>">
                                    <span class="text">  <%=string.Format("{0}", (p.Price == 0 ? "0" : string.Format("{0}",p.Price))) %> </span>
                                        </option>--%>
                                      <%} %>
                                        </select>
                                      </div>
                                  <%} %>
                                    </td>
                                  <%} %>
                                    </tr>
                                    <%}%>
                                  <%} %>
                                 <%} %>
                                    </table>
                                  <div class="cal-table-top">
                                    <div class="col-md-12">
                                      <div class="cal-table-top-right float-right">
                                        <button type="button" class="btn btn-info font-size-12" data-toggle="modal" data-target="#bulkuploadhelp">
                                          <span class="glyphicon glyphicon-question-sign">
                                          </span>&nbsp;&nbsp;Help
                                        </button>
                                        <button type="button" class="btn btn-info" 
                                                <%=Disable%> onclick="Supplier.ManageAvailability.ClearBulkAvailability(0);">Clear
                                        </button>
                                      <button type="button" class="btn btn-orange" 
                                              <%=Disable%> onclick="Supplier.ManageAvailability.ViewBulkByDateRange(0);">Submit
                                      </button>
                                  </div>
                                  </div>
                            </div>
                            </div>                   
                    </div>
             
                            <div class="cal-table-top no-padding btnpanel" style="display: none">
                                <div class="col-md-12 padding0">
                                    <div class="col-md-6 no-padding">
                                        <div class="cal-table-top-left float-left">
                                            <button type="button" class="btn btn-default btn-xs" id="reopen_0" reopenroom="false" onclick="Supplier.ManageAvailability.CloseOut(0,2)"><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span>Reopen </button>
                                            <button type="button" class="btn btn-default btn-xs" id="closeout_0" closeroom="false" onclick="Supplier.ManageAvailability.CloseOut(0,1)"><span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span>Close Out </button>
                                            <button type="button" class="btn btn-default btn-xs" id="res_0" removerestriction="false" onclick="Supplier.ManageAvailability.CloseOut(0,3)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span>Remove restrictions </button>
                                        </div>
                                    </div>
                                    <div class="col-md-6 no-padding">
                                        <div class="cal-table-top-right float-right">
                                            <button type="button" class="btn btn-info font-size-12" data-toggle="modal" data-target="#help"><span class="glyphicon glyphicon-question-sign"></span>&nbsp;&nbsp;Help</button>
                                            <button type="button" class="btn btn-info" onclick="Supplier.ManageAvailability.ClearChanges(0)">Clear All Changes </button>
                                            <button id="btn_0" type="button" class="btn btn-orange" onclick="Supplier.ManageAvailability.UpdateChanges(0)">Update and Save Changes </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                      <div id="printable" class="padding-top12">
                        <div id="dv_0">
                          <asp:panel id="dvContent" runat="server" clientidmode="Static">
                          </asp:panel>
                        </div>
                      </div>
                     <div class="cal-table-top btnpanel" style="display: none">
                                <div class="col-md-12 padding0">
                                    <div class="col-md-6 no-padding">
                                        <div class="cal-table-top-left float-left">
                                            <button type="button" class="btn btn-default btn-xs" id="reopen_1" reopenroom="false" onclick="Supplier.ManageAvailability.CloseOut(1,2)"><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span>Reopen </button>
                                            <button type="button" class="btn btn-default btn-xs" id="closeout_1" closeroom="false" onclick="Supplier.ManageAvailability.CloseOut(1,1)"><span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span>Close Out </button>
                                            <button type="button" class="btn btn-default btn-xs" id="res_1" removerestriction="false" onclick="Supplier.ManageAvailability.CloseOut(1,3)"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span>Remove restrictions </button>
                                        </div>
                                    </div>
                                    <div class="col-md-6 no-padding">
                                        <div class="cal-table-top-right float-right">
                                            <button type="button" class="btn btn-info font-size-12" data-toggle="modal" data-target="#help"><span class="glyphicon glyphicon-question-sign"></span>&nbsp;&nbsp;Help</button>
                                            <button type="button" class="btn btn-info" onclick="Supplier.ManageAvailability.ClearChanges(0)">Clear All Changes </button>
                                            <button id="btn_1" type="button" class="btn btn-orange" onclick="Supplier.ManageAvailability.UpdateChanges(0)">Update and Save Changes </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>


              </div>
              </div>
          </div>
          <div class="modal fade bs-example-modal-sm" id="myModal" role="dialog">
            <div class="modal-dialog modal-md">
              <div class="modal-content">
                <div class="modal-header  alert-info">
                  <h4 class="modal-title">Clear Changes
                  </h4>
                </div>
                <div class="modal-body">
                  <p>Your changes will not be saved. Would you like to Save Changes Now?
                  </p>
                </div>
                <div class="modal-footer">
                  <button type="button" id="btnUpdateModalPopup" class="btn btn-info" data-dismiss="modal">Update & Save Changes
                  </button>
                  <button type="button" id="btnNoModalPopup" class="btn btn-info" data-dismiss="modal">No
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div class="modal fade bs-example-modal-sm" id="messageModal" role="dialog">
            <div class="modal-dialog modal-md">
              <div class="modal-content">
                <div class="modal-header  alert-info">
                  <h4 class="modal-title">Save
                  </h4>
                </div>
                <div class="modal-body">
                  <p>Your change have been successfully saved
                  </p>
                </div>
                <div class="modal-footer">
                  <button type="button" id="Button2" class="btn btn-info" data-dismiss="modal">Ok
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div class="modal fade bs-example-modal-sm" id="dvLessRoomAllocationMsg" role="dialog">
            <div class="modal-dialog modal-md">
              <div class="modal-content">
                <div class="modal-header  alert-info">
                  <h4 class="modal-title">Validation
                  </h4>
                </div>
                <div class="modal-body">
                  <p>Allocated is less than booked, therefore system has updated allocated to match the booked figure
                  </p>
                </div>
                <div class="modal-footer">
                  <button type="button" id="Button1" class="btn btn-info" data-dismiss="modal">Ok
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div class="modal fade bs-example-modal-sm" id="dvLessPkgRoomAllocationMsg" role="dialog">
            <div class="modal-dialog modal-md">
              <div class="modal-content">
                <div class="modal-header  alert-info">
                  <h4 class="modal-title">Validation
                  </h4>
                </div>
                <div class="modal-body">
                  <p>Package Allocated Room is less than Room , therefore system has updated allocated room to match the room Allocated Figure
                  </p>
                </div>
                <div class="modal-footer">
                  <button type="button" id="Button7" class="btn btn-info" data-dismiss="modal">Ok
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div class="modal fade bs-example-modal-sm" id="dvReopenCloseMsg" role="dialog">
            <div class="modal-dialog modal-md">
              <div class="modal-content">
                <div class="modal-header  alert-info">
                  <h4 class="modal-title">Information
                  </h4>
                </div>
                <div class="modal-body">
                  <p>For changes to reflect make sure to click update and save changes button
                  </p>
                </div>
                <div class="modal-footer">
                  <button type="button" id="Button3" class="btn btn-info" data-dismiss="modal">Ok
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div class="modal fade bs-example-modal-sm" id="help" role="dialog">
            <div class="modal-dialog modal-md">
              <div class="modal-content">
                <div class="modal-header alert-info">
                  <button type="button" class="close" data-dismiss="modal">&times;
                  </button>
                  <h4 class="modal-title">
                    <span class="glyphicon glyphicon-question-sign">
                    </span>&nbsp;&nbsp;Help
                  </h4>
                </div>
                <div class="modal-body">
                  <asp:literal runat="server" id="litHelp">
                  </asp:literal>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-info" data-dismiss="modal">Ok
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div class="modal fade bs-example-modal-sm" id="bulkuploadhelp" role="dialog">
            <div class="modal-dialog modal-md">
              <div class="modal-content">
                <div class="modal-header alert-info">
                  <button type="button" class="close" data-dismiss="modal">&times;
                  </button>
                  <h4 class="modal-title">
                    <span class="glyphicon glyphicon-question-sign">
                    </span>Help
                  </h4>
                </div>
                <div class="modal-body">
                  <asp:literal runat="server" id="litbulkuploadhelp">
                  </asp:literal>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-info" data-dismiss="modal">Ok
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div class="modal fade bs-example-modal-sm" id="dvoffersubscribe" role="dialog">
            <div class="modal-dialog modal-md">
              <div class="modal-content">
                <div class="modal-header alert-info">
                  <button type="button" class="close" data-dismiss="modal">&times;
                  </button>
                  <h4 class="modal-title">
                    <span id="spofferallocated">Promotion Subscribe
                    </span>
                  </h4>
                </div>
                <div class="modal-body">
                  <div id="offerallocated">
                    <div class="alert alert-success">
                      This promotion can now be booked from your general availability
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-info" data-dismiss="modal">Ok
                  </button>
                </div>
              </div>
            </div>
          </div>
      <div class="modal fade bs-example-modal-sm" id="dvOfferContentalert" role="dialog">
            <div class="modal-dialog modal-md">
              <div class="modal-content">
                <div class="modal-header alert-info">
                  <button type="button" class="close" data-dismiss="modal">&times;
                  </button>
                  <h4 class="modal-title">
                    <span id="Span1">Offer information
                    </span>
                  </h4>
                </div>
                <div class="modal-body">
                  <div id="Div2">
                    <div class="alert alert-success">
                      Offer information updated successfully
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-info" data-dismiss="modal">Ok
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div class="modal fade bs-example-modal-sm sigin-modal" id="removal-offer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog modal-md-8 modal-sm-12" role="document">
              <div class="modal-content">
                <div class="modal-header alert-info">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;
                    </span>
                  </button>
                  <h4 class="modal-title" id="removal-offer-label">Removal of promotion
                  </h4>
                </div>
                <div class="modal-body">
                  <input type="hidden" id="removeOfferId" />
                  <input type="hidden" id="removeSupplierInviteId" />
                  <div id="removeOfferContent">
                  </div>
                </div>
                <div class="modal-footer ">
                  <button type="button" class="btn btn-info" 
                          <%=Disable%> onclick="Supplier.ManageAvailability.UnSubScribeOffer()">Update & Save  
                  </button>
                <button type="button" class="btn btn-info" 
                        <%=Disable%> data-dismiss="modal" id="btncancelremoval">Cancel
                </button>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade bs-example-modal-sm sigin-modal" id="dvSubscribePromotion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog modal-md-8 modal-sm-12" role="document">
              <div class="modal-content">
                <div class="modal-header alert-info">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;
                    </span>
                  </button>
                  <h4 class="modal-title" id="subscribeheading">
                  </h4>
                </div>
                <div class="modal-body">
                  <input type="hidden" id="subscribeOfferId" />
                    <input type="hidden" id="hdcontenttype" />
                  <input type="hidden" id="subscribeSupplierInviteId" />
                  <div id="subscribeOfferContent">
                      <textarea id="txtContent" rows="200" cols="300" class="ckeditor"></textarea>
                  </div>
                </div>
                <div class="modal-footer ">
                  <button type="button" class="btn btn-info" 
                          <%=Disable%> onclick="Supplier.ManageAvailability.ChangeCampaignSubscribe()" id="btnsubscribecontent">Subscribe for Promotion  
                  </button>
                <button type="button" class="btn btn-info" 
                        <%=Disable%> data-dismiss="modal" id="btncancelsubscribe">Cancel
                </button>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade bs-example-modal-sm" id="dvOfferUnsubscribe" role="dialog">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            <div class="modal-header  alert-info">
              <h4 class="modal-title">Reason For Unsubscribe
              </h4>
            </div>
            <div class="modal-body">
              <p>Please select valid reason for unsubscribe
              </p>
            </div>
            <div class="modal-footer">
              <button type="button" id="Button4" class="btn btn-info" data-dismiss="modal">Ok
              </button>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade bs-example-modal-sm" id="dvEmptyMsg" role="dialog">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            <div class="modal-header  alert-info">
              <h4 class="modal-title">Validation
              </h4>
            </div>
            <div class="modal-body">
              <p>This field cannot be blank as rooms have been booked, please input a number
              </p>
            </div>
            <div class="modal-footer">
              <button type="button" id="Button5" class="btn btn-info" data-dismiss="modal">Ok
              </button>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade bs-example-modal-sm" id="dvShowEmptyMsg" role="dialog">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            <div class="modal-header  alert-info">
              <h4 class="modal-title">Validation
              </h4>
            </div>
            <div class="modal-body">
              <p>This field cannot be blank, please input a number
              </p>
            </div>
            <div class="modal-footer">
              <button type="button" id="Button6" class="btn btn-info" data-dismiss="modal">Ok
              </button>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade bs-example-modal-sm" id="dvBulkUpload" role="dialog">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            <div class="modal-header  alert-info">
              <h4 class="modal-title">Validation
              </h4>
            </div>
            <div class="modal-body">
              <p>You can not load less rooms that are already booked
              </p>
              <p id="datedetail">
              </p>
            </div>
            <div class="modal-footer">
              <button type="button" id="Button8" class="btn btn-info" data-dismiss="modal">Ok
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false">
      <div class="modal-header">
        <h1>Processing...
        </h1>
      </div>
      <div class="modal-body">
        <div class="progress progress-striped active">
          <div class="bar" style="width: 100%;">
          </div>
        </div>
      </div>
    </div>

