﻿CREATE TABLE [dbo].[IDTable] (
    [ID]         UNIQUEIDENTIFIER CONSTRAINT [DF_IDTable_ID] DEFAULT (newid()) NOT NULL,
    [Prefix]     VARCHAR (255)    NOT NULL,
    [Key]        VARCHAR (255)    NOT NULL,
    [ParentID]   UNIQUEIDENTIFIER NOT NULL,
    [CustomData] VARCHAR (255)    NOT NULL,
    [DAC_Index]  INT              NULL
);


GO
CREATE CLUSTERED INDEX [DAC_Clustered]
    ON [dbo].[IDTable]([DAC_Index] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [ndxID]
    ON [dbo].[IDTable]([ID] ASC);


GO
CREATE NONCLUSTERED INDEX [ndxPrefixKey]
    ON [dbo].[IDTable]([Prefix] ASC, [Key] ASC);

