﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Helper;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.Availability.SupplierPortal.Controls;
using Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Offer;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using Affinion.LoyaltyBuild.UCom.Common.Extension;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Handler.Offer
{
    /// <summary>
    /// Summary description for OfferRoomAvailibilityByDateRange
    /// </summary>
    public class OfferRoomAvailibilityByDateRange : IHttpHandler, IRequiresSessionState
    {


        public void ProcessRequest(HttpContext context)
        {
            try
            {
                context.Response.ContentType = "text/plain";
                string sDate = context.Request["startDate"];
                string eDate = context.Request["endDate"];
                string month = context.Request["month"];
                string offerId = context.Request["OfferId"];
                DateTime startDate = sDate.ParseFormatDateTime();// DateTime.ParseExact(sDate, formats, CultureInfo.InvariantCulture, DateTimeStyles.None);
                DateTime endDate = eDate.ParseFormatDateTime(); // DateTime.ParseExact(eDate, formats, CultureInfo.InvariantCulture, DateTimeStyles.None);
                context.Response.Write(LoadUserControl(context, offerId, startDate, endDate, Convert.ToInt32(month)));
            }
            catch (Exception er)
            {
                context.Response.Write("ERROR" + er.Message.ToString());
                Sitecore.Diagnostics.Log.Error(er.Message, new object());
            }
        }



        protected static string LoadUserControl(HttpContext context, string offerId, DateTime startDate, DateTime endDate, int month)
        {
            using (Page page = new Page())
            {
                StringBuilder s = new StringBuilder();

                List<ProviderAvialbilityData> avialbilityData = new List<ProviderAvialbilityData>();
                AvailabilityHelper staticDataForRoom = new AvailabilityHelper(context);
                int campaignItemId = Convert.ToInt32(offerId);
                if (month == 0)
                {
                    List<SupplierInviteDetail> inviteDetail = staticDataForRoom.GetCampaignDetailBySupplier(startDate, endDate);
                    if (inviteDetail.Any(x => x.IsSubscibe && x.CampaignItemId == campaignItemId))
                    {
                        avialbilityData = staticDataForRoom.OfferAvailibilityByDateRange(campaignItemId, startDate, endDate);
                    }
                }
                else
                {
                    avialbilityData = staticDataForRoom.OfferAvailibilityByDateRange(campaignItemId, startDate, endDate);
                }
                PlaceHolder f = new PlaceHolder();

                if (avialbilityData != null && avialbilityData.Any())
                {
                    foreach (var item in avialbilityData)
                    {
                        if (month != 0 && item.StatDate.GetMonthFromJulianFixed() != month)
                        {
                            continue;
                        }
                        using (StringWriter writer = new StringWriter(CultureInfo.CurrentCulture))
                        {
                            OfferAvailabilityCalendarView avp = (OfferAvailabilityCalendarView)page.LoadControl("~/Layouts/SubLayouts/SupplierPortal/Controls/Offer/OfferAvailabilityCalendarView.ascx");
                            avp.currenItem = item;
                            f.Controls.Add(avp);
                            page.Controls.Add(f);
                            HttpContext.Current.Server.Execute(page, writer, false);
                            s.Clear();
                            s.Append(writer.ToString());
                        }
                    }
                }
                else
                {
                    s.Append("No data found for selected promotion");
                }
                return s.ToString();
            }

        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}