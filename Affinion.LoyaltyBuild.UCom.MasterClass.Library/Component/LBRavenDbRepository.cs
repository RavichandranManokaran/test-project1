﻿using Raven.Abstractions.Commands;
using Raven.Abstractions.Data;
using Raven.Client;
using Raven.Client.Document;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.Infrastructure;
using UCommerce.Search.RavenDB;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Component
{
    public class LBRavenDbRepository : IRepository
    {
        // Fields
        private readonly IRavenDbStoreProvider _ravenDbStoreProvider;

        // Methods
        public LBRavenDbRepository(IRavenDbStoreProvider ravenDbStoreProvider)
        {
            this._ravenDbStoreProvider = ravenDbStoreProvider;
        }

        public void Delete<T>(IEnumerable<T> documents) where T : class
        {
            foreach (T local in documents)
            {
                this.Delete<T>(local);
            }
        }

        public void Delete<T>(IEnumerable<object> ids)
        {
            
            IEnumerable<string> enumerable = ids as IEnumerable<string>;
            Guard.Against.Null<IEnumerable<string>>(enumerable, "ids must be in the form of IEnumerable<string>.");
            using (IDocumentSession session = this._ravenDbStoreProvider.GetStore().OpenSession())
            {
                foreach (string str in enumerable)
                {
                    ICommandData[] commands = new ICommandData[1];
                    DeleteCommandData data = new DeleteCommandData
                    {
                        Key = str
                    };
                    commands[0] = data;
                    session.Advanced.Defer(commands);
                }
                session.SaveChanges();
            }
        }

        public void Delete<T>(T document) where T : class
        {
            using (IDocumentSession session = this._ravenDbStoreProvider.GetStore().OpenSession())
            {
                session.Delete<T>(document);
                session.SaveChanges();
            }
        }

        public T Get<T>(object id) where T : class
        {
            string str = id as string;
            Guard.Against.Null<string>(str, "RavenDB only supports strings for ids. Please make sure you pass in a string for id.");
            using (IDocumentSession session = this._ravenDbStoreProvider.GetStore().OpenSession())
            {
                return session.Load<T>(str);
            }
        }

        public IEnumerable<T> Get<T>(IEnumerable<string> ids)
        {
            using (IDocumentSession session = this._ravenDbStoreProvider.GetStore().OpenSession())
            {
                return session.Load<T>(ids);
            }
        }

        public void Save<T>(T document) where T : class
        {
            using (IDocumentSession session = this._ravenDbStoreProvider.GetStore().OpenSession())
            {
                session.Store(document);
                session.SaveChanges();
            }
        }

        public void SaveMany<T>(IEnumerable<T> documents) where T : class
        {
            try
            {
                IList<T> list = (documents as IList<T>) ?? documents.ToList<T>();
                Guard.Against.NullArgument<IList<T>>(list);
                if (list.Any<T>())
                {
                    if (list.Count<T>() == 1)
                    {
                        this.Save<T>(list[0]);
                    }
                    else
                    {
                        BulkInsertOptions options = new BulkInsertOptions
                        {
                            CheckForUpdates = true,
                            CheckReferencesInIndexes = true
                        };
                        using (BulkInsertOperation operation = this._ravenDbStoreProvider.GetStore().BulkInsert(null, options))
                        {
                            foreach (T local in list)
                            {
                                operation.Store(local);
                            }
                        }
                    }
                }
            }
            catch (Exception er)
            { 
                
            }
        }

        public IQueryable<T> Select<T>() where T : class
        {
            using (IDocumentSession session = this._ravenDbStoreProvider.GetStore().OpenSession())
            {
                return session.Query<T>();
            }
        }

        public IQueryable<T> Select<T>(string index)
        {
            using (IDocumentSession session = this._ravenDbStoreProvider.GetStore().OpenSession())
            {
                return session.Query<T>(index, false);
            }
        }
    }
}
