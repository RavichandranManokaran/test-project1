﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model.Payment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
using Sitecore.Data.Items;
using Affinion.LoyaltyBuild.Common.Utilities;
using Sitecore.Data;
namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal
{
    public partial class OfflinePortalCustomerSearchResultData : BaseSublayout
    {
        public List<SearchQueryResultItem> SearchQueryResultItems { set; protected get; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            { 
            Database currentDB = Sitecore.Configuration.Factory.GetDatabase("web");
            Sitecore.Context.Item  = currentDB.SelectItems("/sitecore/content/#offline-portal#/#home#//*[@@TemplateName='searchcustomerquery']").ToList().FirstOrDefault();
            }
        }
    }
}