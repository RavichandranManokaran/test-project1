﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfflinePortalManageCustomerQuery.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalManageCustomerQuery" %>

<%if(ValidationResponse != null){%>
<div class="col-sm-12 alert alert-<%= ValidationResponse.IsSuccess ? "success" : "danger" %>">
    <%= ValidationResponse.Message %>
</div>
<%} %>

<div id="divErrorMsg" runat="server" class="alert alert-danger" visible ="false">
    <p><sc:Text id="errorMsgText" Field="Error Message Text" runat="server"/></p>
</div>
<div class="col-sm-12">
    <div class="accordian-outer">
        <div class="panel-group" id="accordion">
            <div class="panel panel-default">
                <div class="panel-heading bg-primary">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse0" class="show-hide-list"><sc:Text id="titleText" Field="Title Text" runat="server"/></a>
                    </h4>
                </div>
                <div id="collapse0" class="panel-collapse collapse in">
                    <div class="panel-body alert-info">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel panel-default">

                                    <div>
                                        <div class="table-responsive">
                                            <asp:GridView CssClass="table tbl-calendar" ID="GvCustomerQuery" runat="server" AutoGenerateColumns="false" AllowPaging="true" PageSize="10"
                                                DataKeyNames="ID" OnRowCommand="GvCustomerQuery_RowCommand" OnRowDeleting="GvCustomerQuery_RowDeleting" OnPageIndexChanging="GvCustomerQuery_PageIndexChanging">
                                                <Columns>
                                                    <asp:BoundField ItemStyle-Width="1px" HeaderStyle-CssClass="hiddencol" ItemStyle-CssClass="hiddencol" DataField="ID" HeaderText="ID" />
                                                    <asp:BoundField ItemStyle-Width="100" HeaderStyle-CssClass="hiddencol" ItemStyle-CssClass="hiddencol" DataField="QueryTypeID" HeaderText="QuaryTypeID" />
                                                   <%-- <asp:BoundField ItemStyle-Width="150px" DataField="QueryType" HeaderText="Query Type" />
                                                    <asp:BoundField ItemStyle-Width="450px" DataField="Content" HeaderText="Content" />--%>
                                                    <asp:TemplateField HeaderText="Is Resolved" ItemStyle-Width="100px">
                                                        <ItemTemplate>
                                                            <%# (bool)(Eval("IsSolved"))? "Yes":"No" %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                  <%--   <asp:BoundField ItemStyle-Width="150px" DataField="CreatedBy" HeaderText="Logged By" />
                                                    <asp:BoundField ItemStyle-Width="150px" DataField="CreatedDate" DataFormatString="{0:MMM dd yyyy hh:mm tt}" HeaderText="Logged Date" />--%>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="row margin-row">
                                <div class="col-sm-3">
                                    <div class="col-sm-4"><span class=" font-size-12"><sc:Text id="ddlText" Field="DropDown Label Text" runat="server"/></span><span class="accent58-f"><sc:Text id="mandSymbl1" Field="Mandatory Symbol" runat="server"/></span></div>
                                    <div class="col-sm-8">
                                        <asp:DropDownList ID="ddlQueryType" runat="server"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-9">
                                    <div class="col-sm-2"><span class="font-size-12"><sc:Text id="textAreaText" Field="Content TextArea Label" runat="server"/></span><span class="accent58-f"><sc:Text id="mandSymbl2" Field="Mandatory Symbol" runat="server"/></span></div>
                                    <div class="col-sm-10">
                                        <textarea id="taContent" runat="server" class="form-control font-size-12" rows="5"></textarea>
                                        <asp:CustomValidator id="manageContent" ControlToValidate="taContent" ClientValidationFunction="ClientValidate" Display="Dynamic"  ForeColor="Red" ErrorMessage="*Dont Enter Card Number!" Font-Name="verdana"  Font-Size="10pt" runat="server"><%--<sc:text id="txtDonotenterText" runat="server" />--%></asp:CustomValidator>
                                        <asp:RequiredFieldValidator ID="rfvTaContent" ForeColor="Red" runat="server" ControlToValidate="taContent" Text="This Field is required"><%--<sc:text id="txtContentRequiredText" runat="server" />--%></asp:RequiredFieldValidator>
                                    </div>
                                </div>	
                                <div class="col-sm-12">
                                    <div class="col-sm-6 margin-row"></div>
                                    <div class="col-sm-6 margin-row">
                                        <div class="col-sm-6 font-size-12"><span><sc:Text id="radioBtnTxt" Field="IsResolved RadioButton Label" runat="server"/></span></div>
                                        <div class="col-sm-6">
                                            <label class="checkbox-inline font-size-12">
                                                <asp:RadioButton ID="rbIsSolvedYes" runat="server" Text="" GroupName="IsSolved" /> <sc:Text id="option1" Field="RadioButton Option Text1" runat="server"/></label>
                                            <label class="checkbox-inline font-size-12">
                                                <asp:RadioButton ID="rbIsSolvedNo" runat="server" Text="" Checked="true" GroupName="IsSolved" /> <sc:Text id="option2" Field="RadioButton Option Text2" runat="server"/></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">

                                    <div class="col-sm-2">
                                        <asp:Button type="submit" runat="server" ID="BtnNew" Text="New" Visible="false" OnClick="BtnNew_onclick" class="btn btn-default add-break pull-right margin-row width-full" />
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="reset" class="btn btn-default add-break pull-right margin-row width-full"><sc:Text id="btnRTxt" Field="Reset Button Text" runat="server"/></button>
                                    </div>
                                    <div class="col-sm-2">
                                        <Button type="submit" runat="server" ID="BtnSave" onserverclick="BtnSave_onclick" class="btn btn-default add-break pull-right margin-row width-full"><sc:Text id="btnInTxt" Field="Insert Button Text" runat="server"/></Button>
                                    </div>
                                    <div class="col-sm-2">
                                        <asp:LinkButton ID="LinkButtonConvertExcel" class="btn btn-default add-break pull-right margin-row width-full" OnClick="LinkButtonConvertExcel_Click" runat="server" Text="Convert to Excel " CausesValidation="false" Visible="false"> <sc:Text Field="ConvertExcelText" runat="server"/><i class="glyphicon glyphicon-circle-arrow-down"></i></asp:LinkButton>
                                    </div>
                                    <div class="col-sm-2">
                                        <asp:LinkButton ID="LinkButtonConvertPdf" class="btn btn-default add-break pull-right margin-row width-full" OnClick="LinkButtonConvertPdf_Click" runat="server" Text="Convert to Pdf " CausesValidation="false" Visible="false"> <sc:Text Field="ConvertPdfText" runat="server"/><i class="glyphicon glyphicon-circle-arrow-down"></i></asp:LinkButton>
                                    </div>

                                    <div class="col-sm-2">
                                        <asp:LinkButton ID="LinkButtonConvertDoc" class="btn btn-default add-break pull-right margin-row width-full" OnClick="LinkButtonConvertDoc_Click" runat="server" Text="Convert to Word " CausesValidation="false" Visible="false"> <sc:Text Field="ConvertWordText" runat="server"/><i class="glyphicon glyphicon-circle-arrow-down"></i></asp:LinkButton>
                                    </div>
                                    <asp:HiddenField ID="hfQueryId" runat="server" />
                                </div>

                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


<style type="text/css">
    .hiddencol {
        display: none;
    }
</style>
