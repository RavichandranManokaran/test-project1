﻿using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Model.Provider
{
    /// <summary>
    /// generic provider
    /// </summary>
    public interface IGenericProvider
    {
        /// <summary>
        /// Star Ranking
        /// </summary>
        int StarRanking { get; set; }

        /// <summary>
        /// Address
        /// </summary>
        string Address { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        string Email { get; set; }
        
        /// <summary>
        /// Address
        /// </summary>
        string Phone { get; set; }

        /// <summary>
        /// Facilities
        /// </summary>
        List<Item> Facilities { get; set; }
    }
}
