﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Affinion.LoyaltyBuild.BedBanks.JacTravel.Model
{
    [Serializable()]
    [XmlType(TypeName = "BookingDetails")]
    public class JtBookingDetails
    {
        public string PropertyID { get; set; }
        public string PreBookingToken { get; set; }
        public string ArrivalDate { get; set; }
        public string Duration { get; set; }
        public string LeadGuestTitle { get; set; }
        public string LeadGuestFirstName { get; set; }
        public string LeadGuestLastName { get; set; }
        public string LeadGuestAddress1 { get; set; }
        public string LeadGuestPostcode { get; set; }
        public string LeadGuestBookingCountryID { get; set; }
        public string LeadGuestPhone { get; set; }
        public string LeadGuestEmail { get; set; }
        public string ContractSpecialOfferID { get; set; }
        public string ContractArrangementID { get; set; }
        public string Request { get; set; }
        public string TradeReference { get; set; }
        public string CCCardTypeID { get; set; }
        public string CCIssueNumber { get; set; }
        public string CCAmount { get; set; }
        
        public List<RoomBooking> RoomBookings { get; set; }
    }
}
