﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Reports.Model
{
    public class AROraclePayment : OraclePayment
    {
        public int SupplierVatPaymentId { get; set; }
        public int OrderPaymentId { get; set; }
        public DateTime OrderPaymentDate { get; set; }
        public int SupplierId { get; set; }
        public int CampaignId { get; set; }
        public int ClientId { get; set; }
        public decimal GrossAmount { get; set; }
        public decimal NetAmount { get; set; }
        public string CustomerAddressCode { get; set; }
        public int InvoiceId { get; set; }
       

    }
}
