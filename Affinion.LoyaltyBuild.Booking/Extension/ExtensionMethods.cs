﻿using Affinion.LoyaltyBuild.Api.Booking.Data.Basket;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Affinion.LoyaltyBuild.Api.Booking.Extension
{
    /// <summary>
    /// Extension methods
    /// </summary>
    static class ExtensionMethods
    {
        /// <summary>
        /// Get request from dictionary
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dictionary"></param>
        /// <returns></returns>
        public static T ToBasketRequest<T>(this Dictionary<string, string> dictionary) where T : IAddToBasketRequest, new()
        {
            var result = new T();

            var type = typeof(T);
            foreach (var prop in type.GetProperties())
            {
                var attr = prop.GetCustomAttributes(typeof(OrderPropertyAttribute), true)
                    .Cast<OrderPropertyAttribute>().FirstOrDefault();
                if (attr != null && dictionary.ContainsKey(attr.Name))
                {
                    Type t = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
                    object safeValue = (dictionary[attr.Name] == null) ? null : Convert.ChangeType(dictionary[attr.Name], t);
                    prop.SetValue(result, safeValue);
                }
            }

            return result;
        }

        /// <summary>
        /// get dictionary from request
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static Dictionary<string,string> ToDictionary(this IAddToBasketRequest request)
        {
            var values = new Dictionary<string, string>();
            var type = request.GetType();
            foreach (var prop in type.GetProperties())
            {
                var attr = prop.GetCustomAttributes(typeof(OrderPropertyAttribute), true).FirstOrDefault();
                if (attr != null && prop.GetValue(request) != null)
                {
                    values.Add(((OrderPropertyAttribute)attr).Name,
                        (prop.GetValue(request) ?? string.Empty).ToString());
                }
            }

            return values;
        }

    }
}   
