﻿
CREATE PROCEDURE [dbo].[OLD_affinion_GetCustomerDetail]
(
	@CustomerId int
) 
AS 
BEGIN
	SELECT 
	C.CustomerId
	,C.FirstName
	,C.LastName
	,C.EmailAddress
	,C.PhoneNumber
	,C.MobilePhoneNumber
	,C.MemberId
	,CA.ClientId
	,CA.Title
	,CA.AddressLine1
	,CA.AddressLine2
	,CA.AddressLine3
	,CA.CountryId
	,CA.LocationId
	,CA.BranchId
	,CA.BookingReferenceId
	,CA.Subscribe
	,CA.Active
	FROM 
	[dbo].[uCommerce_Customer] C
	LEFT JOIN [dbo]. uCommerce_CustomerAttribute CA
	on C.CustomerId = CA.CustomerId
	WHERE C.CustomerId =@CustomerId
	
END


