﻿using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.EntitiesV2;
using UCommerce.Presentation.Views.Marketing;
using UCommerce.Presentation.Web;
using UCommerce.Presentation.Web.Controls;
using UCommerce.Presentation.Web.Marketing;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.Ucommerce.Marketing.Awards
{
    public partial class UpgradeProductSkuUi : ViewEnabledControl<IEditCampaignItemView>, IConfigurable
    {
        List<DataTypeEnum> datatypeList;

        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (datatypeList == null)
            {
                DataType roomType = DataType.All().Where(x => x.TypeName == "RoomType").ToList().FirstOrDefault();
                if (roomType != null)
                {
                    datatypeList = roomType.DataTypeEnums.ToList();
                }
                datatypeList.Insert(0, new DataTypeEnum() { Value = "" });
            }
            if (datatypeList != null)
            {
                drpRoomType.DataSource = datatypeList;
                drpRoomType.DataTextField = "Value";
                drpRoomType.DataValueField = "Value";
                drpRoomType.DataBind();
            }
            UpgradeProductSkuAward roomTypeData = this.DataSource as UpgradeProductSkuAward;
            this.DeleteButton.OnClientClick = string.Format("return confirm('Delete');");
            if (datatypeList.Any(x => x.Value == roomTypeData.UpgradedRoomType))
            {
                drpRoomType.SelectedValue = roomTypeData.UpgradedRoomType;
            }
            this.ReadOnlyMessage.Text = this.TargetToString(roomTypeData);
        }

        /// <summary>
        /// Save Room detail
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveButton_Click(object sender, ImageClickEventArgs e)
        {
            UpgradeProductSkuAward roomTypeData = this.DataSource as UpgradeProductSkuAward;
            roomTypeData.UpgradedRoomType = drpRoomType.SelectedValue;
            roomTypeData.Save();
            this.ReadOnlyMessage.Text = this.TargetToString(roomTypeData);
            this.ToggleEditor();
        }

        /// <summary>
        /// enable edit & display view in page
        /// </summary>
        private void ToggleEditor()
        {
            this.NonEditModePlaceHolder.Visible = !this.NonEditModePlaceHolder.Visible;
            this.EditButton.Visible = !this.EditButton.Visible;
            this.EditModePlaceHolder.Visible = !this.EditModePlaceHolder.Visible;
            this.SaveButton.Visible = !this.SaveButton.Visible;
        }

        /// <summary>
        /// View Roomtype Act
        /// </summary>
        /// <param name="UpgradeProductSkuAward"></param>
        /// <returns></returns>
        private string TargetToString(UpgradeProductSkuAward UpgradeProductSkuAward)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendFormat("RoomType ");
            stringBuilder.AppendFormat("{0} ", UpgradeProductSkuAward.UpgradedRoomType);
            return stringBuilder.ToString();
        }

        /// <summary>
        /// Delete room type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteButton_Click(object sender, ImageClickEventArgs e)
        {
            UpgradeProductSkuAward roomTypeData = this.DataSource as UpgradeProductSkuAward;
            if (roomTypeData != null)
            {
                roomTypeData.Delete();
            }
            this.Page.Response.Redirect(this.Page.Request.Url.ToString(), true);
        }

        /// <summary>
        /// Edit room type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditButton_Click(object sender, ImageClickEventArgs e)
        {
            this.ToggleEditor();
        }

        /// <summary>
        /// get set for apply
        /// </summary>
        public bool EnabledForApply
        {
            get;
            set;
        }

        /// <summary>
        /// get set for display
        /// </summary>
        public bool EnabledForDisplay
        {
            get;
            set;
        }

        /// <summary>
        /// Get set for datasource
        /// </summary>
        public object DataSource
        {
            get;
            set;

        }

        /// <summary>
        /// Create new Room Type
        /// </summary>
        /// <returns></returns>
        public object New()
        {

            var UpgradeProductSkuAward = new UpgradeProductSkuAward();
            try
            {
                UpgradeProductSkuAward.CampaignItem = View.CampaignItem;
                UpgradeProductSkuAward.Name = "Upgrade Free";
                UpgradeProductSkuAward.UpgradedRoomType = string.Empty;
                UpgradeProductSkuAward.Save();
            }
            catch (Exception ex)
            {               
                Diagnostics.WriteException(DiagnosticsCategory.MasterClassWebsite, ex, this);
                throw;
            }
            this.DataSource = UpgradeProductSkuAward;
            return UpgradeProductSkuAward;
        }
    }
}