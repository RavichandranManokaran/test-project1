﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    /// <summary>
    /// Entity to hold Search Query Result related information
    /// </summary>
    public class SearchQueryResultItem
    {

        /// <summary>
        /// ID Serial No
        /// </summary>
        public int ID { get; set; }
        
        /// <summary>
        /// QueryId
        /// </summary>
        public int QueryId { get; set; }

        /// <summary>
        /// Client Name
        /// </summary>
        public string Client { get; set; }

        /// <summary>
        /// Client Id
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// FirstName
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// LastName
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// CustomerName
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// Phone
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// HasAttachments
        /// </summary>
        public bool HasAttachments { get; set; }

        /// <summary>
        /// Created
        /// </summary>
        public string Created { get; set; }
        
        /// <summary>
        /// Updated
        /// </summary>
        public string Updated { get; set; } 

        /// <summary>
        /// BookingReference
        /// </summary>
        public string BookingReference { get; set; }

        /// <summary>
        /// IsResolved flag
        /// </summary>
        public bool IsResolved { get; set; }

        /// <summary>
        ///Content
        /// </summary>
        public string Content { get; set; }
    
        /// <summary>
        /// Provider Name
        /// </summary>
        public string ProviderName { get; set; }
    }
}
