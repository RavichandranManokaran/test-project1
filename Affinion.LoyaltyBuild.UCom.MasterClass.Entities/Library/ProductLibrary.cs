﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Entities.Library
{
    public static class ProductLibrary
    {
        public static List<Product> GetAddonProudct(int categoryId)
        {
            Category category = Category.Get(categoryId);
            List<Product> addonProduct = UCommerce.EntitiesV2.Product.GetForCategory(category).Where(x => x.DisplayOnSite && x.AllowOrdering && x.ProductDefinition.Name.Replace(" ", "").Equals("addon", StringComparison.InvariantCultureIgnoreCase)).ToList();
            return addonProduct;
        }

        public static int GetCategoryIdFromSku(string productSku)
        {
            Category pCategory = Product.All().Where(x => x.Sku.Equals(productSku)).FirstOrDefault().GetCategories().FirstOrDefault();
            if (pCategory != null)
            {
                return pCategory.CategoryId;
            }
            throw new Exception("Invalid Product SKU");

        }

        public static Product GetAvilabilityBySku(string productOldSku, string roomType)
        {
            Category pCategory = Product.All().Where(x => x.Sku.Equals(productOldSku)).FirstOrDefault().GetCategories().FirstOrDefault();
            if (pCategory != null)
            {
                int categoryId = pCategory.CategoryId;
                Product avaialbeProduct = Product.GetForCategory(pCategory).Where(x => x.ProductProperties.Any(y => y.ProductDefinitionField.Name.Equals("RoomType", StringComparison.InvariantCultureIgnoreCase) && y.Value.Equals(roomType))).FirstOrDefault();
                if (avaialbeProduct != null)
                {
                    //TODO : Check avaialbility here
                    return avaialbeProduct;
                }
            }
            return null;

        }
    }
}
