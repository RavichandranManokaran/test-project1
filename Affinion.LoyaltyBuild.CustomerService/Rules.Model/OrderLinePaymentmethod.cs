﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Rules.Model
{
     public class OrderLinePaymentmethod
    {

        /// <summary>
        /// orderlineid
        /// </summary>
        public int OrderLineId{ get; set; }
        /// <summary>
        /// payment method
        /// </summary>
        public string Paymentmethod { get; set; }
     }
}
