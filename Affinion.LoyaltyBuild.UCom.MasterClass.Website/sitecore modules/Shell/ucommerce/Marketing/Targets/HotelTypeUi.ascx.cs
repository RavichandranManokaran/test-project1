﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.EntitiesV2;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using UCommerce.Presentation.Views.Marketing;
using UCommerce.Presentation.Web.Controls;
using UCommerce.Presentation.Web.Marketing;
using Affinion.LoyaltyBuild.UCom.Common.Constants;
using Affinion.LoyaltyBuild.UCom.MasterClass.Website.Configuration;
using Affinion.LoyaltyBuild.UCom.MasterClass.Website.CustomSiteCoreDataType;
using Affinion.LoyaltyBuild.Common.Instrumentation;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.ucommerce.Marketing.Targets
{
    public partial class HotelTypeUi : ViewEnabledControl<IEditCampaignItemView>, ITargetUi
    {
        List<string> datatypeList;
        /// <summary>
        /// page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (datatypeList == null)
            //{
            //    DataType hotelType = DataType.All().Where(x => !x.Deleted && x.TypeName == DataTypeConstant.HotelType).ToList().FirstOrDefault();
            //    if (hotelType != null)
            //    {
            //        datatypeList = hotelType.DataTypeEnums.ToList();
            //    }
            //    datatypeList.Insert(0, new DataTypeEnum() { Value = "" });
            //}
            datatypeList = SiteConfiguration.GetSiteCoreItem(CustomDataTypeConstant.SupplierType);
            drpHotelType.Items.Clear();
            foreach (var item in datatypeList)
            {
                ListItem items = new ListItem();
                items.Text = item;
                items.Value = item;
                drpHotelType.Items.Add(items);
            }
            
            HotelTypeTarget hotelTypeData = this.DataSource as HotelTypeTarget;
            this.DeleteButton.OnClientClick = string.Format("return confirm('Delete');");
            if (datatypeList.Any(x => x == hotelTypeData.HotelType))
            {
                drpHotelType.SelectedValue = hotelTypeData.HotelType;
            }
            this.ReadOnlyMessage.Text = this.TargetToString(hotelTypeData);
        }

        /// <summary>
        ///  Save changes for hotel type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveButton_Click(object sender, ImageClickEventArgs e)
        {
            HotelTypeTarget hotelTypeData = this.DataSource as HotelTypeTarget;
            hotelTypeData.HotelType = drpHotelType.SelectedValue;
            hotelTypeData.Save();
            this.ReadOnlyMessage.Text = this.TargetToString(hotelTypeData);
            this.ToggleEditor();
        }

        /// <summary>
        /// enable edit mode view in page
        /// </summary>
        private void ToggleEditor()
        {
            this.NonEditModePlaceHolder.Visible = !this.NonEditModePlaceHolder.Visible;
            this.EditButton.Visible = !this.EditButton.Visible;
            this.EditModePlaceHolder.Visible = !this.EditModePlaceHolder.Visible;
            this.SaveButton.Visible = !this.SaveButton.Visible;
        }


        /// <summary>
        /// Apply hotel type value to View
        /// </summary>
        /// <param name="HotelTypeTarget"></param>
        /// <returns></returns>
        private string TargetToString(HotelTypeTarget HotelTypeTarget)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendFormat("Hotel Type ");
            stringBuilder.AppendFormat("{0} ", HotelTypeTarget.HotelType);
            return stringBuilder.ToString();
        }

        /// <summary>
        /// Delete hotel type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteButton_Click(object sender, ImageClickEventArgs e)
        {
            HotelTypeTarget hotelTypeData = this.DataSource as HotelTypeTarget;
            if (hotelTypeData != null)
            {
                hotelTypeData.Delete();
            }
            this.Page.Response.Redirect(this.Page.Request.Url.ToString(), true);
        }

        /// <summary>
        /// Edit hotel type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditButton_Click(object sender, ImageClickEventArgs e)
        {
            this.ToggleEditor();
        }

        /// <summary>
        ///  get set for apply
        /// </summary>
        public bool EnabledForApply
        {
            get;
            set;
        }

        /// <summary>
        /// get set for display
        /// </summary>
        public bool EnabledForDisplay
        {
            get;
            set;
        }

        /// <summary>
        ///  Get set for datasource
        /// </summary>
        public object DataSource
        {
            get;
            set;

        }

        /// <summary>
        /// Create new hotel Type
        /// </summary>
        /// <returns></returns>
        public object New()
        {
            var HotelTypeTarget = new HotelTypeTarget();
            try
            {
                HotelTypeTarget.CampaignItem = View.CampaignItem;
                HotelTypeTarget.EnabledForApply = true;
                HotelTypeTarget.HotelType = string.Empty;
                HotelTypeTarget.Save();
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.MasterClassWebsite, ex, this);
                throw;
            }
            this.DataSource = HotelTypeTarget;
            return HotelTypeTarget;
        }
    }
}