﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.EntitiesV2.Definitions;
using UCommerce.Search;
using Documents = UCommerce.Documents;
namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Component
{
    public class LBDefinitionIndexer : IIndexer<IDefinition>, IDeindexer<IDefinition>, IIndexer<IEnumerable<IDefinition>>, IDeindexer<IEnumerable<IDefinition>>
    {
        // Fields
        private readonly IRepository _ravenDbRepository;

        // Methods
        public LBDefinitionIndexer(IRepository ravenDbRepository)
        {
            this._ravenDbRepository = ravenDbRepository;
        }

        public void Deindex(IEnumerable<IDefinition> subjects)
        {
            List<string> list = (from x in subjects select string.Format("definitions/{0}", x.Id)).ToList<string>();
            this._ravenDbRepository.Delete<Definition>((IEnumerable<object>)list);
        }

        public void Deindex(IDefinition subjects)
        {
            this.Deindex(new List<IDefinition> { subjects });
        }

        private UCommerce.Documents.PropertyList GetMetaData(DefinitionField field)
        {
            UCommerce.Documents.PropertyList list = new UCommerce.Documents.PropertyList();
            foreach (DefinitionFieldDescription description in field.DefinitionFieldDescriptions)
            {
                list.AddProperty(description.CultureCode, "DisplayName", description.DisplayName);
                list.AddProperty(description.CultureCode, "Description", string.IsNullOrWhiteSpace(description.Description) ? null : description.Description);
            }
            list.AddProperty("DisplayOnWebsite", field.DisplayOnSite);
            list.AddProperty("Facet", false);
            return list;
        }

        private UCommerce.Documents.PropertyList GetMetaData(IDefinitionField field)
        {
            if (field is DefinitionField)
            {
                return this.GetMetaData(field as DefinitionField);
            }
            if (field is ProductDefinitionField)
            {
                return this.GetMetaData(field as ProductDefinitionField);
            }
            return null;
        }

        private Documents.PropertyList GetMetaData(ProductDefinitionField field)
        {
            Documents.PropertyList list = new Documents.PropertyList();
            foreach (ProductDefinitionFieldDescription description in field.ProductDefinitionFieldDescriptions)
            {
                list.AddProperty(description.CultureCode, "DisplayName", description.DisplayName);
                list.AddProperty(description.CultureCode, "Description", string.IsNullOrWhiteSpace(description.Description) ? null : description.Description);
            }
            list.AddProperty("DisplayOnWebsite", field.DisplayOnSite);
            list.AddProperty("IsVariantProperty", field.IsVariantProperty);
            list.AddProperty("Facet", field.Facet);
            return list;
        }

        public void Index(IDefinition subject)
        {
            this.Index(new List<IDefinition> { subject });
        }

        public void Index(IEnumerable<IDefinition> subject)
        {
            List<Documents.Definitions.Definition> items = new List<Documents.Definitions.Definition>();
            foreach (IDefinition definition in subject)
            {
                Documents.Definitions.Definition item = new Documents.Definitions.Definition
                {
                    Id = definition.Guid,
                    Name = definition.Name
                };
                item.ParentDefinitions = (from x in definition.GetParentDefinitions() select "definitions/" + x.Guid.ToString()).ToList<string>();
                foreach (IDefinitionField field in from x in definition.GetDefinitionFields()
                                                   orderby x.SortOrder
                                                   select x)
                {
                    Documents.Definitions.Field field2 = new Documents.Definitions.Field
                    {
                        Multilingual = field.Multilingual,
                        Name = field.Name,
                        RenderInEditor = field.RenderInEditor,
                        SortOrder = field.SortOrder,
                        DataTypeId = string.Format("datatypes/{0}", field.DataType.Guid)
                    };
                    field2.Properties = this.GetMetaData(field);
                    item.Fields.Add(field2);
                }
                items.Add(item);
            }
            this._ravenDbRepository.SaveMany<Documents.Definitions.Definition>(items);
        }
    }

}
