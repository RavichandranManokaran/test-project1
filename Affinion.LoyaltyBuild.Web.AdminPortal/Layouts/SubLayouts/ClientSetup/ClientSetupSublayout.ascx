﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientSetupSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.AdminPortal.Layouts.SubLayouts.ClientSetup.ClientSetupSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
    <div class="container-outer width-full overflow-hidden-att app-bg-cl">
        <div class="container-outer-inner width-full">
            <div class="container">
                <div class="col-md-6 sup-login-layout-outer">
                    <div class="sup-login-layout">
                        <div class="sup-login-layout-inner">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="login-header">
                                        <img src="/Resources/images/LBLogo-home.png" alt="logo" title="" class="img-responsive">
                                    </div>
                                </div>
                            </div>
                            <form>
                                <div class="col-md-12">
                                    <div class="input-set">
                                        <div class="col-md-12">
                                            <h2 class="text-center">Client Setup</h2>
                                        </div>
                                        <div class="col-md-12">
                                            <p>Client Name</p>
                                             <asp:TextBox ID="TextBoxClientName" placeholder="Client Name" runat="server" class="form-control" autocomplete="off" required="" ></asp:TextBox>
                                            <%--<input type="text" placeholder="Client Name" id="username" name="username" autocomplete="off" required="" class="form-control" title="">--%>
                                        </div>
                                       <%-- <div class="col-md-12">
                                            <p>Default language</p>
                                            <select class="form-control">
                                                <option>Select language</option>
                                                <option>English</option>
                                                <option>France</option>
                                                <option>german</option>
                                                <option>italy</option>
                                            </select>
                                        </div>--%>
                                        <div>
                                            <div class="sup-btn-top">
                                                <div class="col-md-12">
                                                    <%--<button type="submit" class="sup-btn">Create</button>--%>
                                                    <asp:Button ID="ButtonCreate" runat="server" Text="Create" OnClick="ButtonCreate_Click" class="sup-btn" />
                                                </div>
                                                <div class="col-md-12">
                                                    <%--<button type="submit" class="sup-btn">Cancel</button>--%>                                                   
                                                    <input type ="reset" value="Cancel" class="sup-btn"  onclick ="redirectToLaunchpad();"/>
                                                </div>
                                                <p class="bis-txt-center"></p>
                                                <a class="bis-back" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

<script type="text/javascript">
    function redirectToLaunchpad()
    {
        window.location.replace("/sitecore/shell/sitecore/client/Applications/Launchpad/");
    }

</script>