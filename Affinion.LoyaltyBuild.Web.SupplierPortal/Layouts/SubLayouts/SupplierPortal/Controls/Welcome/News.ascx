﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="News.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Welcome.News" %>
<%--Group Hotel Name:<asp:Label ID="GroupHotelName" runat="server" Text="Label"></asp:Label>--%>
<%@ register tagprefix="sc" namespace="Sitecore.Web.UI.WebControls" assembly="Sitecore.Kernel" %>
 

<div>
    <div class="col-xs-3 no-padding-left" id="supplierCategoryDropDownSection" runat="server" visible="false">
        <h5 class="title-text font-weight-bold">&nbsp;
            <asp:Label ID="supplierCategoryLabel" runat="server"> <sc:Text id="Selecttype" runat="server" /></asp:Label>
        </h5>
        <asp:DropDownList CssClass="chosenitems" ID="supplierCategoryDropDownList" runat="server" OnSelectedIndexChanged="ddlsupplierCategory_SelectedIndexChanged" AppendDataBoundItems="false" AutoPostBack="True" EnableViewState="false" Height="25px">
            <asp:ListItem Text="--Select One--" Value="0" />
            <asp:ListItem Text="Group" Value="Group" />
            <asp:ListItem Text="Independent" Value="Independent" />
        </asp:DropDownList>
    </div>
    <div class="col-xs-3" id="supplierTypeDropDownSection" runat="server" visible="false">
        <h5 class="title-text font-weight-bold">
            <asp:Label ID="supplierTypeLabel" runat="server" Text="Supplier Type:"></asp:Label>
        </h5>
        <asp:DropDownList CssClass="chosenitems" ID="supplierTypeDropDownList" runat="server" OnSelectedIndexChanged="ddlSupplierType_SelectedIndexChanged" AppendDataBoundItems="false" AutoPostBack="True" Height="25px">
            <asp:ListItem Text="--Select One--" Value="" />
        </asp:DropDownList>
    </div>
    <div class="col-xs-3" id="ddlGroupsSection" runat="server" visible="false">
        <h5 class="title-text font-weight-bold">Group:</h5>
        <asp:DropDownList CssClass="chosenitems" ID="ddlGroups" runat="server" OnSelectedIndexChanged="ddlGroups_SelectedIndexChanged" AppendDataBoundItems="false" AutoPostBack="true">
        </asp:DropDownList>
    </div>
    <div class="col-xs-3" id="ddlHotelsSection" runat="server" visible="false">
        <h5 class="title-text font-weight-bold">Supplier:</h5>
        <asp:DropDownList CssClass="chosenitems" ID="ddlHotels" runat="server" OnSelectedIndexChanged="ddlHotels_SelectedIndexChanged" AppendDataBoundItems="false" AutoPostBack="true">
        </asp:DropDownList>
    </div>
    <div class="col-xs-3" id="dropDownSection" runat="server" visible="false">
        <h5 class="title-text font-weight-bold">
            <asp:Label ID="listHotelLabel" runat="server" Text="Supplier:"></asp:Label>

        </h5>
        <asp:DropDownList CssClass="chosenitems" ID="ddlListHotel" runat="server" OnSelectedIndexChanged="ddlSelectedHotel_SelectedIndexChanged" AppendDataBoundItems="false" AutoPostBack="True" Height="25px">
        </asp:DropDownList>
    </div>
    <div class="clearfix"></div>
</div>




<h4 class="title-text font-weight-bold" id="groupDetailSection" runat="server" enableviewstate="false" visible="false">
    <asp:Label ID="groupHotelLabel" runat="server" Text="GROUP NAME: "></asp:Label>
    <asp:Label ID="groupHotelNameLabel" runat="server" Text="" Width="300px" />
</h4>

<h4 class="title-text font-weight-bold">
    <asp:Label ID="hotelNameLabel" runat="server" EnableViewState="false" Text="SUPPLIER NAME:" Visible="false"></asp:Label>
    <asp:Label ID="HotelName" runat="server" EnableViewState="false" ReadOnly="true"></asp:Label>
</h4>

<div class="item">
    <asp:Label ID="WelcomeLabel" runat="server" Text="Welcome"></asp:Label>
    <asp:Label ID="ContactName" runat="server"></asp:Label>
    <asp:Label ID="LastLoginLabel" runat="server" Text=", Last Login : " Visible="false"></asp:Label>
    <asp:Label ID="lastlogindate" runat="server" ></asp:Label>
</div>


<asp:Repeater ID="newsRepeater" runat="server">
    <ItemTemplate>
        <%if(newsListPriority!=null && newsListPriority.Count> 0){ %>
        <div class="News-box">
            <h2>
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse_<%# Container.ItemIndex + 1 %>" class="show-hide-list">
                    <asp:Label ID="newsTitlelbl" runat="server" Text='<%#Eval("NewsTitle")%>' EnableViewState="false" ReadOnly="true"></asp:Label>
                </a>
             </h2>
            <div id="collapse_<%# Container.ItemIndex + 1 %>" class="News-con collapse in">
                <b><asp:Label ID="publishDate" runat="server" Text='<%#"Date Publish: "+Eval("PublishDate")%>' EnableViewState="false" ReadOnly="true"></asp:Label></b>
                </br></br>
                <asp:Label ID="newsDescriptionlbl" runat="server" Text='<%#Eval("NewsDescription")%>' EnableViewState="false" ReadOnly="true"></asp:Label>
            </div>
        </div>
        <%} %>
    </ItemTemplate>
</asp:Repeater>

<div id="divRoomShortageDetails" runat="server" visible="false">
    <h4>Room Shortage Details</h4>
    <asp:GridView CssClass="shortage_tab table" ID="RoomShortageGridView" runat="server" AutoGenerateColumns="false" AllowPaging="true" PageSize="10">
        <Columns>
            <asp:BoundField ItemStyle-Width="150px" DataField="Offer" HeaderText="Room Name" />
            <asp:BoundField ItemStyle-Width="150px" DataField="Month" HeaderText="Month" />
            <asp:BoundField ItemStyle-Width="150px" DataField="SoldOutDates" HeaderText="Sold out Dates" />
            <asp:BoundField ItemStyle-Width="150px" DataField="ClosedOutDates" HeaderText="Closed out Dates" />
        </Columns>
    </asp:GridView>
</div>

<div id="divOfferShortageDetails" runat="server" visible="false">
    <h4>Promotion Shortage Details</h4>
    <asp:GridView CssClass="shortage_tab table" ID="campaignOfferDetails" runat="server" AutoGenerateColumns="false" AllowPaging="true" PageSize="10">
        <Columns>
            <asp:BoundField ItemStyle-Width="150px" DataField="Name" HeaderText="Campaign Name" />
        </Columns>
    </asp:GridView>
</div>


<div class="item">
    <asp:Label ID="roomShortageLabel" runat="server" EnableViewState="false" ReadOnly="true"></asp:Label><br />
</div>



