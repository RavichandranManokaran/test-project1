﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OfferAvailability.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Availability.OfferAvailability" %>

        <div class="row" style="width:100%!important">
            <div class="col-md-12 ">
                <div class="col-md-2">
                </div>
                <div class="col-md-10">
                </div>
            </div>
            <div class="col-md-12">
                <div class="table-cal-header">
                    <div class="col-md-2 " id="sidebar-wrapper">
                        <div class="offers-top">
                            <div class="offers datePickerPanel">
                                <h2 class="no-margin">B & B +1D 2-3N Promo Rate</h2>
                                <div class="col-md-12 cal-dates padding-top12 no-padding-right clear">
                                    <div class="dropdown padding-15 clear">
                                        <button type="button" class="btn btn-info width-full font-size-12">View Price</button>
                                    </div>
                                    <div class="dropdown padding-15 clear">
                                        <button type="button" class="btn btn-info width-full font-size-12">Bulk Load Availability <span class="glyphicon glyphicon-play-circle"></span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="offers-top">
                            <div class="offers datePickerPanel">
                                <h2 class="no-margin">Choose Date Range</h2>
                                <div class="col-md-12 cal-dates padding-top12 no-padding-right clear">
                                    <div class="col-md-2 col-xs-2  datepickerTitle">From</div>
                                    <div class="col-md-10 col-xs-10  float-right no-padding-right">
                                        <input type="text" id="datepicker1" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-md-12 cal-dates padding-top12 no-padding-right clear">
                                    <div class="col-md-2 col-xs-2 datepickerTitle">To</div>
                                    <div class="col-md-10 col-xs-10 float-right no-padding-right">
                                        <input type="text" id="datepicker2" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="dropdown padding-15 float-right clear">
                                <button type="button" class="btn  btn-info font-size-12">View <span class="glyphicon glyphicon-right-arrow" aria-hidden="true"></span></button>

                            </div>
                        </div>
                        <div class="offers-top">
                            <div class="offers">
                                <h2 class="no-margin">Promotions Participating On </h2>



                                <ul class="offers-det" id="offerMore1">
                                    <li><i class="glyphicon glyphicon-ok-sign eye-ic"></i><span>Start BandB City Complex</span><a href="#" class="edit"><i class="glyphicon glyphicon-remove-circle icons-positionRight"></i></a></li>
                                    <li><i class="glyphicon glyphicon-ok-sign eye-ic"></i><span>Start BandB City Complex</span><a href="#" class="edit"><i class="glyphicon glyphicon-remove-circle icons-positionRight"></i></a></li>
                                    <li><i class="glyphicon glyphicon-ok-sign eye-ic"></i><span>B & B + 1D 2-3N 3*</span><a href="#" class="edit"><i class="glyphicon glyphicon-remove-circle icons-positionRight"></i></a></li>
                                    <li><i class="glyphicon glyphicon-ok-sign eye-ic"></i><span>B & B + 1D 2-3N 3*</span><a href="#" class="edit"><i class="glyphicon glyphicon-remove-circle icons-positionRight"></i></a></li>
                                    <li><i class="glyphicon glyphicon-ok-sign eye-ic"></i><span>B & B + 1D 2-3N 3*</span><a href="#" class="edit"><i class="glyphicon glyphicon-remove-circle icons-positionRight"></i></a></li>
                                </ul>
                                <ul class="offers-det collapse" id="offerMore2">
                                    <li><i class="glyphicon glyphicon-ok-sign eye-ic"></i><span>Start BandB City Complex</span><a href="#" class="edit"><i class="glyphicon glyphicon-remove-circle  icons-positionRight"></i></a></li>
                                    <li><i class="glyphicon glyphicon-ok-sign eye-ic"></i><span>Start BandB City Complex</span><a href="#" class="edit"><i class="glyphicon glyphicon-remove-circle  icons-positionRight"></i></a></li>
                                    <li><i class="glyphicon glyphicon-ok-sign eye-ic"></i><span>Start BandB City Complex</span><a href="#" class="edit"><i class="glyphicon glyphicon-remove-circle  icons-positionRight"></i></a></li>
                                    <li><i class="glyphicon glyphicon-ok-sign eye-ic"></i><span>Start BandB City Complex</span><a href="#" class="edit"><i class="glyphicon glyphicon-remove-circle  icons-positionRight"></i></a></li>
                                </ul>
                                <div class=" padding-15  float-right">
                                    <button type="button" class="btn btnOffer btn-info font-size-12" data-toggle="collapse" data-target="#offerMore2"><span class="glyphicon glyphicon-circle-arrow-down"></span>Show More </button>
                                </div>
                            </div>
                        </div>
                        <div class="offers-top">
                            <div class="offers">
                                <h2 class="no-margin">Offers NOT Participating On </h2>
                                <ul class="offers-det no-offer" id="no-offerMore1">
                                    <li><span>Start BandB+D Ireland City Complex</span><a href="#" class="edit"><i class="glyphicon glyphicon-plus-sign icons-positionRight"></i></a></li>
                                    <li><span>Start BandB+D Ireland City Complex</span><a href="#" class="edit"><i class="glyphicon glyphicon-plus-sign icons-positionRight"></i></a></li>
                                    <li><span>Start BandB+D Ireland City Complex</span><a href="#" class="edit"><i class="glyphicon glyphicon-plus-sign icons-positionRight"></i></a></li>
                                    <li><span>Start BandB+D Ireland City Complex</span><a href="#" class="edit"><i class="glyphicon glyphicon-plus-sign icons-positionRight"></i></a></li>
                                </ul>
                                <ul class="offers-det no-offer collapse" id="no-offerMore2">
                                    <li><span>Start BandB City Complex</span><a href="#" class="edit"><i class="glyphicon glyphicon-plus-sign icons-positionRight"></i></a></li>
                                    <li><span>Start BandB City Complex</span><a href="#" class="edit"><i class="glyphicon glyphicon-plus-sign icons-positionRight"></i></a></li>
                                    <li><span>Start BandB City Complex</span><a href="#" class="edit"><i class="glyphicon glyphicon-plus-sign icons-positionRight"></i></a></li>
                                    <li><span>Start BandB City Complex</span><a href="#" class="edit"><i class="glyphicon glyphicon-plus-sign icons-positionRight"></i></a></li>
                                </ul>
                                <div class=" padding-15  float-right">
                                    <button type="button" class="btn no-btnOffer btn-info font-size-12" data-toggle="collapse" data-target="#no-offerMore2"><span class="glyphicon glyphicon-circle-arrow-down"></span>Show More </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-10" id="page-content-wrapper">
                        <div class="cal-table-top">
                            <div class="col-md-12 padding0">
                                <div class="col-md-6 no-padding">
                                    <div>
                                        <a href="#menu-toggle" class="btn btn-info font-size-12" id="menu-toggle"><span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>Toggle Menu</a>
                                        <button class="btn btn-info font-size-12" type="button"><span aria-hidden="true" class="glyphicon glyphicon-backward"></span>Back to Manage Availbility </button>
                                        <button class="btn btn-info font-size-12" type="button"><span aria-hidden="true" class="glyphicon glyphicon-th"></span>Color Guide </button>
                                    </div>
                                </div>
                                <div class="col-md-6 no-padding">
                                    <div class="cal-table-top-right float-right">
                                        <button type="button" class="btn btn-info font-size-12">Annual Reports</button>
                                        <button type="button" class="btn btn-info font-size-12">Clear Changes </button>
                                        <button type="button" class="btn btn-orange font-size-12">Update and Save Changes </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tabale-top">
                            <table class="cal-table table" id="calendarTable2" data-start-day="sun">
                                <tr> 
                                    <th><span class="cal-hed">Nov 2013 </span></th>
                                    <th><span>01</span><span> Mo</span></th>
                                    <th><span>02</span><span> Tu</span></th>
                                    <th><span>03</span><span> We</span></th>
                                    <th><span>04</span><span> Th</span></th>
                                    <th><span>05</span><span>Fr</span></th>
                                    <th class="headerSortDown"><span>06</span><span>Sa</span></th>
                                    <th class="headerSortDown"><span>07</span><span> Su</span></th>
                                    <th><span>08</span><span> Mo</span></th>
                                    <th><span>09</span><span> Tu</span></th>
                                    <th><span>10</span><span> We</span></th>
                                    <th><span>11</span><span> Th</span></th>
                                    <th><span>12</span><span> Fr</span></th>
                                    <th class="headerSortDown"><span>13</span><span>Sa</span></th>
                                    <th class="headerSortDown"><span>14</span><span> Su</span></th>
                                    <th><span>15</span><span> Mo</span></th>
                                    <th><span>16</span><span> Tu</span></th>
                                    <th><span>17</span><span> We</span></th>
                                    <th><span>18</span><span> Th</span></th>
                                    <th><span>19</span><span> Fr</span></th>
                                    <th class="headerSortDown"><span>20</span><span>Sa</span></th>
                                    <th class="headerSortDown"><span>21</span><span> Su</span></th>
                                    <th><span>22</span><span> Mo</span></th>
                                    <th><span>23</span><span> Tu</span></th>
                                    <th><span>24</span><span> We</span></th>
                                    <th><span>25</span><span> Th</span></th>
                                    <th><span>26</span><span> Fr</span></th>
                                    <th class="headerSortDown"><span>27</span><span>Sa</span></th>
                                    <th class="headerSortDown"><span>28</span><span> Su</span></th>
                                    <th><span>29</span><span> Mo</span></th>
                                    <th><span>30</span><span> Tu</span></th>
                                    <th><span>31</span><span> We</span></th>
                                </tr>
                                <tr class="labelRow">
                                    <td class="left-detail  transparent"></td>
                                    <td><span>v</span></td>
                                    <td><span>v</span></td>
                                    <td><span>v</span></td>
                                    <td><span>v</span></td>
                                    <td><span>v</span></td>
                                    <td><span>v</span></td>
                                    <td><span>v</span></td>
                                    <td><span>v</span></td>
                                    <td><span>v</span></td>
                                    <td><span>v</span></td>
                                    <td><span>v</span></td>
                                    <td><span>v</span></td>
                                    <td><span>v</span></td>
                                    <td><span>v</span></td>
                                    <td><span>v</span></td>
                                    <td><span>v</span></td>
                                    <td><span>v</span></td>
                                    <td><span>v</span></td>
                                    <td><span>v</span></td>
                                    <td><span>v</span></td>
                                    <td><span>v</span></td>
                                    <td><span>v</span></td>
                                    <td><span>v</span></td>
                                    <td><span>v</span></td>
                                    <td><span>v</span></td>
                                    <td><span>v</span></td>
                                    <td><span>v</span></td>
                                    <td><span>v</span></td>
                                    <td><span>v</span></td>
                                    <td><span>v</span></td>
                                    <td><span>v</span></td>
                                </tr>
                                <tr>
                                    <td class="left-detail">
                                        <div class="col-md-12 margin-top-calander-title">
                                            <span class="pull-left">Standard </span>
                                            <div class="pull-right"><i class="glyphicon glyphicon-user md-font"></i><i class="glyphicon glyphicon-user md-font"></i><i class="glyphicon glyphicon-user sm-font"></i></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 margin-top-calander">Total Unsold (All Offers)</div>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0"></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                </tr>
                                <tr>
                                    <td class="left-detail">
                                        <div class="row">
                                            <div class="col-md-12 restriction-detail"><span>This Offer Restriction</span></div>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control bookable" title="" value="5" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control bookable" title="" value="5" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control bookable" title="" value="5" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control bookable" title="" value="5" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control bookable" title="" value="5" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control bookable" title="" value="5" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control bookable" title="" value="5"></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control bookable" title="" value="5" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                </tr>
                                <tr>
                                    <td class="left-detail borderBottom">
                                        <div class="row">
                                            <div class="col-md-12 block-detail">Total Sold (This Offer)</div>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0"></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="9" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                </tr>
                                <tr>
                                    <td class="left-detail">
                                        <div class="col-md-12 margin-top-calander-title">
                                            <span class="pull-left">Double </span>
                                            <div class="pull-right"><i class="glyphicon glyphicon-user md-font"></i><i class="glyphicon glyphicon-user md-font"></i></i></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 margin-top-calander">Total Unsold (All Offers)</div>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0"></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                </tr>
                                <tr>
                                    <td class="left-detail">
                                        <div class="row">
                                            <div class="col-md-12 restriction-detail"><span>This Offer Restriction</span></div>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0"></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                </tr>
                                <tr>
                                    <td class="left-detail borderBottom">
                                        <div class="row">
                                            <div class="col-md-12 block-detail">Total Sold (This Offer)</div>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0"></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                </tr>
                                <tr>
                                    <td class="left-detail">
                                        <div class="col-md-12 margin-top-calander-title">
                                            <span class="pull-left">Twin </span>
                                            <div class="pull-right"><i class="glyphicon glyphicon-user md-font"></i><i class="glyphicon glyphicon-user md-font"></i></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 margin-top-calander">Total Unsold (All Offers)</div>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0"></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                </tr>
                                <tr>
                                    <td class="left-detail">
                                        <div class="row">
                                            <div class="col-md-12 restriction-detail"><span>This Offer Restriction</span></div>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0"></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                </tr>
                                <tr>
                                    <td class="left-detail borderBottom">
                                        <div class="row">
                                            <div class="col-md-12 block-detail">Total Sold (This Offer)</div>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0"></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                </tr>
                                <tr>
                                    <td class="left-detail">
                                        <div class="col-md-12 margin-top-calander-title">
                                            <span class="pull-left">Standard </span>
                                            <div class="pull-right"><i class="glyphicon glyphicon-user md-font"></i><i class="glyphicon glyphicon-user md-font"></i><i class="glyphicon glyphicon-user sm-font"></i></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 margin-top-calander">Total Unsold (All Offers)</div>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0"></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                </tr>
                                <tr>
                                    <td class="left-detail">
                                        <div class="row">
                                            <div class="col-md-12 restriction-detail"><span>This Offer Restriction</span></div>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0"></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                </tr>
                                <tr>
                                    <td class="left-detail borderBottom">
                                        <div class="row">
                                            <div class="col-md-12 block-detail">Total Sold (This Offer)</div>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0"></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                </tr>
                                <tr>
                                    <td class="left-detail">
                                        <div class="col-md-12 margin-top-calander-title">
                                            <span class="pull-left">Standard </span>
                                            <div class="pull-right"><i class="glyphicon glyphicon-user md-font"></i><i class="glyphicon glyphicon-user md-font"></i><i class="glyphicon glyphicon-user sm-font"></i></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 margin-top-calander">Total Unsold (All Offers)</div>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0"></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                </tr>
                                <tr>
                                    <td class="left-detail">
                                        <div class="row">
                                            <div class="col-md-12 restriction-detail"><span>This Offer Restriction</span></div>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0"></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                </tr>
                                <tr>
                                    <td class="left-detail borderBottom">
                                        <div class="row">
                                            <div class="col-md-12 block-detail">Total Sold (This Offer)</div>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0"></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    <td>
                                        <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                </tr>
                            </table>
                            <div class="tabale-top">
                                <div class="cal-table-top padding-top12 ">
                                </div>
                                <table class="cal-table" id="calendarTable2" data-start-day="sun">
                                    <tr>
                                        <th><span class="cal-hed">Nov 2013 </span></th>
                                        <th><span>01</span><span> Mo</span></th>
                                        <th><span>02</span><span> Tu</span></th>
                                        <th><span>03</span><span> We</span></th>
                                        <th><span>04</span><span> Th</span></th>
                                        <th><span>05</span><span>Fr</span></th>
                                        <th class="headerSortDown"><span>06</span><span>Sa</span></th>
                                        <th class="headerSortDown"><span>07</span><span> Su</span></th>
                                        <th><span>08</span><span> Mo</span></th>
                                        <th><span>09</span><span> Tu</span></th>
                                        <th><span>10</span><span> We</span></th>
                                        <th><span>11</span><span> Th</span></th>
                                        <th><span>12</span><span> Fr</span></th>
                                        <th class="headerSortDown"><span>13</span><span>Sa</span></th>
                                        <th class="headerSortDown"><span>14</span><span> Su</span></th>
                                        <th><span>15</span><span> Mo</span></th>
                                        <th><span>16</span><span> Tu</span></th>
                                        <th><span>17</span><span> We</span></th>
                                        <th><span>18</span><span> Th</span></th>
                                        <th><span>19</span><span> Fr</span></th>
                                        <th class="headerSortDown"><span>20</span><span>Sa</span></th>
                                        <th class="headerSortDown"><span>21</span><span> Su</span></th>
                                        <th><span>22</span><span> Mo</span></th>
                                        <th><span>23</span><span> Tu</span></th>
                                        <th><span>24</span><span> We</span></th>
                                        <th><span>25</span><span> Th</span></th>
                                        <th><span>26</span><span> Fr</span></th>
                                        <th class="headerSortDown"><span>27</span><span>Sa</span></th>
                                        <th class="headerSortDown"><span>28</span><span> Su</span></th>
                                        <th><span>29</span><span> Mo</span></th>
                                        <th><span>30</span><span> Tu</span></th>
                                        <th><span>31</span><span> We</span></th>
                                    </tr>
                                    <tr class="labelRow">
                                        <td class="left-detail  transparent"></td>
                                        <td><span>v</span></td>
                                        <td><span>v</span></td>
                                        <td><span>v</span></td>
                                        <td><span>v</span></td>
                                        <td><span>v</span></td>
                                        <td><span>v</span></td>
                                        <td><span>v</span></td>
                                        <td><span>v</span></td>
                                        <td><span>v</span></td>
                                        <td><span>v</span></td>
                                        <td><span>v</span></td>
                                        <td><span>v</span></td>
                                        <td><span>v</span></td>
                                        <td><span>v</span></td>
                                        <td><span>v</span></td>
                                        <td><span>v</span></td>
                                        <td><span>v</span></td>
                                        <td><span>v</span></td>
                                        <td><span>v</span></td>
                                        <td><span>v</span></td>
                                        <td><span>v</span></td>
                                        <td><span>v</span></td>
                                        <td><span>v</span></td>
                                        <td><span>v</span></td>
                                        <td><span>v</span></td>
                                        <td><span>v</span></td>
                                        <td><span>v</span></td>
                                        <td><span>v</span></td>
                                        <td><span>v</span></td>
                                        <td><span>v</span></td>
                                        <td><span>v</span></td>
                                    </tr>
                                    <tr>
                                        <td class="left-detail">
                                            <div class="col-md-12 margin-top-calander-title">
                                                <span class="pull-left">Standard </span>
                                                <div class="pull-right"><i class="glyphicon glyphicon-user md-font"></i><i class="glyphicon glyphicon-user md-font"></i><i class="glyphicon glyphicon-user sm-font"></i></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 margin-top-calander">Total Unsold (All Offers)</div>
                                            </div>
                                        </td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0"></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    </tr>
                                    <tr>
                                        <td class="left-detail">
                                            <div class="row">
                                                <div class="col-md-12 restriction-detail"><span>This Offer Restriction</span></div>
                                            </div>
                                        </td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control bookable" title="" value="5" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control bookable" title="" value="5" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control bookable" title="" value="5" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control bookable" title="" value="5" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control bookable" title="" value="5" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control bookable" title="" value="5" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control bookable" title="" value="5"></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control bookable" title="" value="5" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    </tr>
                                    <tr>
                                        <td class="left-detail borderBottom">
                                            <div class="row">
                                                <div class="col-md-12 block-detail">Total Sold (This Offer)</div>
                                            </div>
                                        </td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0"></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="9" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    </tr>
                                    <tr>
                                        <td class="left-detail">
                                            <div class="col-md-12 margin-top-calander-title">
                                                <span class="pull-left">Double </span>
                                                <div class="pull-right"><i class="glyphicon glyphicon-user md-font"></i><i class="glyphicon glyphicon-user md-font"></i></i></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 margin-top-calander">Total Unsold (All Offers)</div>
                                            </div>
                                        </td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0"></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    </tr>
                                    <tr>
                                        <td class="left-detail">
                                            <div class="row">
                                                <div class="col-md-12 restriction-detail"><span>This Offer Restriction</span></div>
                                            </div>
                                        </td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0"></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    </tr>
                                    <tr>
                                        <td class="left-detail borderBottom">
                                            <div class="row">
                                                <div class="col-md-12 block-detail">Total Sold (This Offer)</div>
                                            </div>
                                        </td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0"></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    </tr>
                                    <tr>
                                        <td class="left-detail">
                                            <div class="col-md-12 margin-top-calander-title">
                                                <span class="pull-left">Twin </span>
                                                <div class="pull-right"><i class="glyphicon glyphicon-user md-font"></i><i class="glyphicon glyphicon-user md-font"></i></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 margin-top-calander">Total Unsold (All Offers)</div>
                                            </div>
                                        </td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0"></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    </tr>
                                    <tr>
                                        <td class="left-detail">
                                            <div class="row">
                                                <div class="col-md-12 restriction-detail"><span>This Offer Restriction</span></div>
                                            </div>
                                        </td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0"></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    </tr>
                                    <tr>
                                        <td class="left-detail borderBottom">
                                            <div class="row">
                                                <div class="col-md-12 block-detail">Total Sold (This Offer)</div>
                                            </div>
                                        </td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0"></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    </tr>
                                    <tr>
                                        <td class="left-detail">
                                            <div class="col-md-12 margin-top-calander-title">
                                                <span class="pull-left">Standard </span>
                                                <div class="pull-right"><i class="glyphicon glyphicon-user md-font"></i><i class="glyphicon glyphicon-user md-font"></i><i class="glyphicon glyphicon-user sm-font"></i></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 margin-top-calander">Total Unsold (All Offers)</div>
                                            </div>
                                        </td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0"></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    </tr>
                                    <tr>
                                        <td class="left-detail">
                                            <div class="row">
                                                <div class="col-md-12 restriction-detail"><span>This Offer Restriction</span></div>
                                            </div>
                                        </td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0"></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    </tr>
                                    <tr>
                                        <td class="left-detail borderBottom">
                                            <div class="row">
                                                <div class="col-md-12 block-detail">Total Sold (This Offer)</div>
                                            </div>
                                        </td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0"></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    </tr>
                                    <tr>
                                        <td class="left-detail">
                                            <div class="col-md-12 margin-top-calander-title">
                                                <span class="pull-left">Standard </span>
                                                <div class="pull-right"><i class="glyphicon glyphicon-user md-font"></i><i class="glyphicon glyphicon-user md-font"></i><i class="glyphicon glyphicon-user sm-font"></i></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 margin-top-calander">Total Unsold (All Offers)</div>
                                            </div>
                                        </td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0"></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    </tr>
                                    <tr>
                                        <td class="left-detail">
                                            <div class="row">
                                                <div class="col-md-12 restriction-detail"><span>This Offer Restriction</span></div>
                                            </div>
                                        </td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0"></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control soldOut" title="" value="0" /></td>
                                    </tr>
                                    <tr>
                                        <td class="left-detail borderBottom">
                                            <div class="row">
                                                <div class="col-md-12 block-detail">Total Sold (This Offer)</div>
                                            </div>
                                        </td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0"></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                        <td>
                                            <input type="text" autocomplete="off" required class="form-control default" title="" value="0" /></td>
                                    </tr>
                                </table>
                                <div class="cal-table-top padding-top12 ">
                                    <div class="col-md-12 padding0">
                                        <div class="col-md-6 no-padding">
                                            <button type="button" class="btn btn-info font-size-12"><span class="glyphicon glyphicon-backward" aria-hidden="true"></span>Back to Manage Availbility </button>
                                        </div>
                                        <div class="col-md-6  no-padding">
                                            <div class="cal-table-top-right float-right">
                                                <button type="button" class="btn btn-info font-size-12">Annual Reports</button>
                                                <button type="button" class="btn btn-info font-size-12">Clear Changes </button>
                                                <button type="button" class="btn btn-orange font-size-12">Update and Save Changes</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
