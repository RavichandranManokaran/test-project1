﻿CREATE TABLE [dbo].[uCommerce_PercentOffShippingTotalAward] (
    [PercentOffShippingTotalAwardId] INT             NOT NULL,
    [PercentOff]                     DECIMAL (18, 2) NOT NULL,
    CONSTRAINT [PK_uCommerce_PercentOffShippingAward] PRIMARY KEY CLUSTERED ([PercentOffShippingTotalAwardId] ASC)
);

