﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UCommerce.EntitiesV2;
using Affinion.LoyaltyBuild.UCom.MasterClass.Entities;
using UCommerce.Presentation.Views.Marketing;
using UCommerce.Presentation.Web.Controls;
using UCommerce.Presentation.Web.Marketing;
using Affinion.LoyaltyBuild.UCom.Common.Constants;
using Affinion.LoyaltyBuild.UCom.MasterClass.Website.Configuration;
using Affinion.LoyaltyBuild.UCom.MasterClass.Website.CustomSiteCoreDataType;
using Affinion.LoyaltyBuild.Common.Instrumentation;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.Website.sitecore_modules.Shell.ucommerce.Marketing.Targets
{
    public partial class AddOnUi : ViewEnabledControl<IEditCampaignItemView>, ITargetUi
    {
        List<string> datatypeList;

        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            AddOnTarget addOnData = this.DataSource as AddOnTarget;
            datatypeList = SiteConfiguration.GetSiteCoreItem(CustomDataTypeConstant.AddOn);
            //DataType addOn = DataType.All().Where(x => x.TypeName == DataTypeConstant.ProductType).ToList().FirstOrDefault();
            //if (addOn != null)
            //{
            //    datatypeList = addOn.DataTypeEnums.ToList();
            //}
            //this.DeleteButton.OnClientClick = string.Format("return confirm('Delete');");
            string[] addons = addOnData.AddOn.Split('|');
            chklstAddOn.Items.Clear();
            foreach (var item in datatypeList)
            {
                //string addon = item.Value;
                ListItem items = new ListItem();
                items.Text = item;
                items.Value = item;
                items.Selected = addons.Any(x => x.ToString() == item);
                chklstAddOn.Items.Add(items);
            }

            this.ReadOnlyMessage.Text = this.TargetToString(addOnData);

        }

        /// <summary>
        /// Save Room detail
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveButton_Click(object sender, ImageClickEventArgs e)
        {
            AddOnTarget addOnData = this.DataSource as AddOnTarget;
            this.ReadOnlyMessage.Text = this.TargetToString(addOnData);

            AddOnTarget dataSource = this.DataSource as AddOnTarget;
            string addOn = string.Empty;
            foreach (ListItem item in chklstAddOn.Items)
            {
                if (item.Selected)
                {
                    addOn += item.Value + "|";
                }
            }
            if (addOn.Length > 0)
            {
                addOn = addOn.Substring(0, addOn.Length - 1);
            }
            dataSource.AddOn = addOn;
            dataSource.Save();
            this.ReadOnlyMessage.Text = this.TargetToString(dataSource);
            this.ToggleEditor();
        }

        /// <summary>
        /// enable edit & display view in page
        /// </summary>
        private void ToggleEditor()
        {
            this.NonEditModePlaceHolder.Visible = !this.NonEditModePlaceHolder.Visible;
            this.EditButton.Visible = !this.EditButton.Visible;
            this.EditModePlaceHolder.Visible = !this.EditModePlaceHolder.Visible;
            this.SaveButton.Visible = !this.SaveButton.Visible;
        }

        /// <summary>
        /// View Roomtype Act
        /// </summary>
        /// <param name="AddOnTarget"></param>
        /// <returns></returns>
        private string TargetToString(AddOnTarget AddOnTarget)
        {

            string[] addonItem = AddOnTarget.AddOn.Split('|').Where(x => !string.IsNullOrEmpty(x)).ToArray();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendFormat("AddOn");
            stringBuilder.AppendFormat("{0} ", string.Join(",", addonItem));
            return stringBuilder.ToString();


        }

        /// <summary>
        /// Delete room type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DeleteButton_Click(object sender, ImageClickEventArgs e)
        {
            AddOnTarget roomTypeData = this.DataSource as AddOnTarget;
            if (roomTypeData != null)
            {
                roomTypeData.Delete();
            }
            this.Page.Response.Redirect(this.Page.Request.Url.ToString(), true);
        }

        /// <summary>
        /// Edit room type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditButton_Click(object sender, ImageClickEventArgs e)
        {
            this.ToggleEditor();
        }

        /// <summary>
        /// get set for apply
        /// </summary>
        public bool EnabledForApply
        {
            get;
            set;
        }

        /// <summary>
        /// get set for display
        /// </summary>
        public bool EnabledForDisplay
        {
            get;
            set;
        }

        /// <summary>
        /// Get set for datasource
        /// </summary>
        public object DataSource
        {
            get;
            set;

        }

        /// <summary>
        /// Create new Room Type
        /// </summary>
        /// <returns></returns>
        public object New()
        {
            var AddOnTarget = new AddOnTarget();
            try
            {
                AddOnTarget.CampaignItem = View.CampaignItem;
                AddOnTarget.EnabledForApply = true;
                AddOnTarget.AddOn = string.Empty;
                AddOnTarget.Save();
            }
            catch (Exception ex )
            {
                Diagnostics.WriteException(DiagnosticsCategory.MasterClassWebsite, ex, this);
                throw;
            }
            this.DataSource = AddOnTarget;
            return AddOnTarget;
        }
    }
}