﻿/*********************************order tables ***************************************/



DELETE FROM [dbo].[LB_BookingInfo]
     
DELETE FROM [dbo].[uCommerce_OrderProperty]


DELETE FROM [dbo].[LB_ReservationNotes]

DELETE FROM [dbo].[LB_OrderLineDue]

DELETE FROM [dbo].[LB_OrderLinePayment]
 
DELETE FROM [dbo].[LB_OrderLineStatusAudit]

DELETE FROM [dbo].[uCommerce_OrderLineDiscountRelation]
 
DELETE FROM [dbo].[uCommerce_OrderLineCampaignRelation]
DELETE FROM [dbo].[uCommerce_PaymentProperty]

DELETE FROM [dbo].[uCommerce_Payment]

UPDATE [dbo].[uCommerce_PurchaseOrder] SET BillingAddressId = NULL

DELETE FROM [dbo].[uCommerce_Shipment]

DELETE FROM [dbo].[uCommerce_OrderAddress]

DELETE FROM [dbo].[uCommerce_OrderStatusAudit]

DELETE FROM [dbo].[uCommerce_PurchaseOrder]

DELETE FROM [dbo].[uCommerce_OrderLine]


 


/*********************************customer tables ***************************************/



DELETE FROM [dbo].[LB_CustomerProperty]

DELETE FROM [dbo].[LB_CustomerQuery]

DELETE FROM [dbo].[LB_CustomerQuery_Attachments]

DELETE FROM [dbo].[uCommerce_Address]
 
DELETE FROM [dbo].[uCommerce_Customer]