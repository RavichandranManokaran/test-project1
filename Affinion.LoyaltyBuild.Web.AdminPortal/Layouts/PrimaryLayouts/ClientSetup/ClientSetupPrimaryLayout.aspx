﻿<%@ Page Language="c#" CodePage="65001" AutoEventWireup="true" Inherits="Affinion.LoyaltyBuild.AdminPortal.Layouts.PrimaryLayouts.ClientSetup.ClientSetupPrimaryLayout" CodeBehind="ClientSetupPrimaryLayout.aspx.cs" %>

<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ OutputCache Location="None" VaryByParam="none" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="CODE_LANGUAGE" content="C#" />
    <meta name="vs_defaultClientScript" content="JavaScript" />
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <sc:VisitorIdentification runat="server" />
    
    <script src="/ResourcesAdmin/scripts/jquery.min.js"></script>
    <script src="/ResourcesAdmin/bootstrap_assets/javascripts/bootstrap.min.js"></script> 
 
    <link rel="stylesheet" href="/ResourcesAdmin/styles/bootstrap.min.css">   
    <link rel="stylesheet" href="/ResourcesAdmin/styles/affinion-admin.min.css">
    <link rel="stylesheet" href="/ResourcesAdmin/styles/theme1/theme1.css">    

    <body>
        <form action="/" method="post" runat="server">
            <sc:Placeholder ID="RegisterClient" Key="RegisterClient" runat="server" />
        </form>
    </body>
</html>
