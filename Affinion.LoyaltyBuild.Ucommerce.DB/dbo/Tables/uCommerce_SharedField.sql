﻿CREATE TABLE [dbo].[uCommerce_SharedField] (
    [SharedFieldId] INT              IDENTITY (1, 1) NOT NULL,
    [ItemId]        UNIQUEIDENTIFIER NOT NULL,
    [FieldId]       UNIQUEIDENTIFIER NOT NULL,
    [FieldValue]    VARCHAR (MAX)    NULL
);

