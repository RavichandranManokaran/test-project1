﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce;
using UCommerce.EntitiesV2;
using UCommerce.EntitiesV2.Queries.Catalog;
using UCommerce.Infrastructure;
using UCommerce.Search;
using Document = UCommerce.Documents;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Component
{
    public class LBRavenCatalogIndexer : IIndexer<ProductCatalog>, IIndexer<IEnumerable<ProductCatalog>>, IDeindexer<ProductCatalog>, IDeindexer<IEnumerable<ProductCatalog>>
    {
        // Fields
        private IList<Category> _categories;
        private readonly IRepository _repository;

        // Methods
        public LBRavenCatalogIndexer(IRepository repository)
        {
            this._repository = repository;
        }

        public void Deindex(IEnumerable<ProductCatalog> subjects)
        {
            List<string> list = (from x in subjects select string.Format("ProductCatalogs/{0}", x.ProductCatalogId)).ToList<string>();
            this._repository.Delete<ProductCatalog>((IEnumerable<object>)list);
        }

        public void Deindex(ProductCatalog subjects)
        {
            this.Deindex(new List<ProductCatalog> { subjects });
        }

        public void Index(IEnumerable<ProductCatalog> subjects)
        {
            List<Document.ProductCatalog> items = subjects.Select<ProductCatalog, Document.ProductCatalog>(new Func<ProductCatalog, Document.ProductCatalog>(this.MapProductCatalogToRavenCatalog)).ToList<Document.ProductCatalog>();
            this._repository.SaveMany<Document.ProductCatalog>(items);
        }

        public void Index(ProductCatalog subjects)
        {
            this.Index(new List<ProductCatalog> { subjects });
        }

        private Document.Category MapCategoryToRavenCategory(Category category)
        {
            Document.Category category2 = new Document.Category
            {
                Id = category.CategoryId,
                Name = category.Name,
                Display = category.DisplayOnSite,
                SortOrder = category.SortOrder
            };
            category2.ProductIds = (from x in category.Products select "products/" + x.ProductId).ToList<string>();
            foreach (CategoryProperty property in category.CategoryProperties)
            {
                string str = property.Value;
                category2.Properties.AddProperty(!string.IsNullOrWhiteSpace(property.CultureCode) ? property.CultureCode : Constants.Search.InvariantCulture, property.DefinitionField.Name, str);
            }
            category2.Properties.AddProperty("ImageMediaId", category.ImageMediaId);
            foreach (CategoryDescription description in category.CategoryDescriptions)
            {
                category2.Properties.AddProperty(!string.IsNullOrWhiteSpace(description.CultureCode) ? description.CultureCode : Constants.Search.InvariantCulture, "DisplayName", description.DisplayName ?? "");
                category2.Properties.AddProperty(!string.IsNullOrWhiteSpace(description.CultureCode) ? description.CultureCode : Constants.Search.InvariantCulture, "Description", description.Description ?? "");
            }
            category2.Categories = (from x in this._categories
                                    where x.ParentCategory == category
                                    select x).ToList<Category>().Select<Category, Document.Category>(new Func<Category, Document.Category>(this.MapCategoryToRavenCategory)).ToList<Document.Category>();
            return category2;
        }

        private Document.ProductCatalog MapProductCatalogToRavenCatalog(ProductCatalog productCatalog)
        {
            this._categories = ObjectFactory.Instance.Resolve<IRepository<Category>>().Select(new CategoriesInCatalogQuery(productCatalog)).ToList<Category>();
            Document.ProductCatalog catalog = new Document.ProductCatalog
            {
                Id = productCatalog.ProductCatalogId,
                Name = productCatalog.Name
            };
            foreach (Category category in from x in this._categories
                                          where (x.ParentCategory == null) && !x.Deleted
                                          orderby x.SortOrder
                                          select x)
            {
                catalog.Categories.Add(this.MapCategoryToRavenCategory(category));
            }
            return catalog;
        }
    }
}

