﻿CREATE TABLE [dbo].[Tasks] (
    [ID]           UNIQUEIDENTIFIER NOT NULL,
    [NextRun]      DATETIME         NOT NULL,
    [taskType]     NVARCHAR (500)   NOT NULL,
    [Parameters]   NTEXT            NOT NULL,
    [Recurrence]   NVARCHAR (200)   NOT NULL,
    [ItemID]       UNIQUEIDENTIFIER NOT NULL,
    [Database]     NVARCHAR (50)    NOT NULL,
    [Pending]      INT              NOT NULL,
    [Disabled]     INT              NOT NULL,
    [InstanceName] NVARCHAR (128)   NULL,
    [DAC_Index]    INT              NULL
);


GO
CREATE CLUSTERED INDEX [DAC_Clustered]
    ON [dbo].[Tasks]([DAC_Index] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [ndxID]
    ON [dbo].[Tasks]([ID] ASC);

