﻿CREATE TABLE [dbo].[uCommerce_UnversionedField] (
    [UnversionedFieldId] INT              IDENTITY (1, 1) NOT NULL,
    [ItemId]             UNIQUEIDENTIFIER NOT NULL,
    [FieldId]            UNIQUEIDENTIFIER NOT NULL,
    [Language]           VARCHAR (MAX)    NULL,
    [FieldValue]         VARCHAR (MAX)    NULL
);

