﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
///Source File:           LanguageRepository.cs
///Sub-system/Module:     Affinion.LoyaltyBuild.Common
///Description:           Entity class for LanguageRepository
///</summary>

#region using Directives
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Sitecore.Data.Items;
using Sitecore.Globalization;
using Sitecore.Links;
using System;
using System.Collections.Generic;
#endregion

namespace Affinion.LoyaltyBuild.Common.Languages
{
    /// <summary>
    /// get languages from Sitecore
    /// </summary>
    public class LanguageRepository
    {
        #region public properties
        /// <summary>
        /// Gets or sets the curent URL
        /// </summary>
        public string CurrentUrl
        {
            get
            {
                UrlOptions urlOptions = new UrlOptions();
                urlOptions.LanguageEmbedding = LanguageEmbedding.Never;
                return LinkManager.GetItemUrl(Sitecore.Context.Item, urlOptions);
            }
        }
        #endregion

        #region private properties
        /// <summary>
        /// sets the Languages Multi List Field Name
        /// </summary>
        private const string languagesField = "Languages";

        #endregion

        #region public methods
        /// <summary>
        /// gets all selected languages 
        /// </summary>
        /// <returns>returns selected languages</returns>
        public List<Country> GetSelectedLanguages()
        {
            List<Country> countries = new List<Country>();

            try
            {
                Sitecore.Data.Fields.MultilistField languageList = Sitecore.Context.Item.Fields[languagesField];
                var items = languageList.GetItems();

                foreach (Item item in items)
                {
                    Language languageConverted;
                    bool result = Language.TryParse(item.Name, out languageConverted);
                    if (result)
                    {
                        Country country = new Country(languageConverted, this.CurrentUrl);
                        countries.Add(country);
                    }

                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, null);
                throw;
            }

            return countries;
        }
        #endregion
    }
}
