﻿CREATE PROCEDURE [dbo].[LB_SearchCustomerQuery]
(	   
	   @queryTypeID int =NULL,
	   @clientId varchar(50) =NULL, 
	   @startDate Date=NULL, 
       @endDate Date=NULL,
	   @firstname nvarchar(512)=NULL, 
       @lastname nvarchar(512)=NULL, 
	   @campaignGroupId nvarchar(50) = NULL,
       @providerId nvarchar(50)=NULL, 
       @bookingReference nvarchar(50)=NULL,
	   @isSolved bit=NULL)
AS 
BEGIN
	SELECT 
	    CQ.QueryId,
	    CQ.Content,
		OP.ClientId,
		C.Name [ProviderName], -- provider name 
		CUS.FirstName,
		CUS.LastName, 
		CUS.EmailAddress,
		isnull(CUS.FirstName,'')+ ' ' + isnull(CUS.LastName,'') +'  ' + isnull(CP._AddressLine1,'') + ' ' + isnull(CP._AddressLine2,'') [Name],
		isnull(CUS.PhoneNumber,MobilePhoneNumber) PhoneNumber,
		RR.BookingNo  BookingReference,
		CQ.IsSolved,
		CQ.CreatedDate,
		CQ.UpdatedDate,
	    ( Select Count(1) from LB_CustomerQuery_Attachments where QueryId = CQ.QueryId) HasAttachments
	FROM [dbo].LB_CustomerQuery CQ
		INNER JOIN [dbo].uCommerce_OrderLine OL ON OL.OrderLineId = CQ.OrderLineId
		INNER JOIN ( select OrderLineId,REPLACE(REPLACE(_ClientId, '{', ''), '}' ,'') ClientId,BookingThrough,_LBCommissionFee, _CreditCardProcessingFee
										from 
										(
											select OrderLineId, [key], [value]
											from uCommerce_OrderProperty 
										) src
										pivot
										(
											max([value])
											for [key] in ( _ClientId,BookingThrough,_LBCommissionFee, _CreditCardProcessingFee)
										) piv
									) OP on  OL.OrderLineId = OP.OrderLineId
		INNER JOIN [dbo].uCommerce_PurchaseOrder PO ON OL.OrderId = PO.OrderId
		INNER JOIN [dbo].LB_BookingInfo RR ON OL.OrderLineId = RR.OrderLineId 
		INNER JOIN [dbo].uCommerce_Product P ON OL.Sku = P.Sku
		INNER JOIN [dbo].uCommerce_CategoryProductRelation CPR ON P.ProductId = CPR.ProductId 
		INNER JOIN [dbo].uCommerce_Category C ON CPR.CategoryId = C.CategoryId
		INNER JOIN [dbo].uCommerce_Customer CUS ON PO.CustomerId = CUS.CustomerId 
		INNER JOIN ( select CustomerId,_BookingReference,_BranchId,_ClientId,_Title,_AddressLine1,_AddressLine2,_AddressLine3

										from 
										(
											select CustomerId, [key], [value]
											from lb_CustomerProperty 
										) src
										pivot
										(
											max([value])
											for [key] in ( _BookingReference,_BranchId,_ClientId,_Title,_AddressLine1,_AddressLine2,_AddressLine3)
										) piv
									) CP on  CUS.CustomerId = CP.CustomerId	

	WHERE (@firstname IS NULL OR CUS.FirstName Like @firstname + '%')
		AND (@lastname IS NULL OR CUS.LastName Like @lastname + '%') 
		AND (@clientId IS NULL OR OP.ClientId = @clientId)
		AND (@providerId IS NULL OR c.[Guid] = @providerId)
		AND (CONVERT(VARCHAR(11),CQ.CreatedDate,111) BETWEEN ISNULL(@startDate,'1900-1-1') AND ISNULL(@endDate, '2099-1-1'))
		AND (@bookingReference IS NULL OR Rr.BookingNo = @bookingReference)
		AND (@IsSolved IS NULL OR @IsSolved = CQ.IsSolved)
		AND (@campaignGroupId IS NULL OR @campaignGroupId = CQ.CampaignId)
		AND (@queryTypeID IS NULL OR @queryTypeID = CQ.QueryTypeId)
		AND CQ.OrderLineId > 0

union
SELECT  		
		CQ.QueryId,
	    CQ.Content,
		CP._ClientId [ClientId],
		'' [ProviderName], -- provider name 
		CUS.FirstName,
		CUS.LastName, 
		CUS.EmailAddress,
		isnull(CUS.FirstName,'')+ ' ' + isnull(CUS.LastName,'') +'  ' + isnull(CP._AddressLine1,'') + ' ' + isnull(CP._AddressLine2,'') [Name],
		isnull(CUS.PhoneNumber,MobilePhoneNumber) PhoneNumber,
		CP._BookingReference BookingReference,
		CQ.IsSolved,
		CQ.CreatedDate,
		CQ.UpdatedDate,
	    ( Select Count(1) from LB_CustomerQuery_Attachments where QueryId = CQ.QueryId) HasAttachments
	FROM [dbo]. LB_CustomerQuery CQ		
		INNER JOIN [dbo].uCommerce_Customer CUS ON CQ.CustomerId = CUS.CustomerId 
		INNER JOIN ( select CustomerId,_BookingReference,_BranchId,REPLACE(REPLACE(_ClientId, '{', ''), '}' ,'') _ClientId,_Title,_AddressLine1,_AddressLine2,_AddressLine3

										from 
										(
											select CustomerId, [key], [value]
											from lb_CustomerProperty 
										) src
										pivot
										(
											max([value])
											for [key] in ( _BookingReference,_BranchId,_ClientId,_Title,_AddressLine1,_AddressLine2,_AddressLine3)
										) piv
									) CP on  CUS.CustomerId = CP.CustomerId	

	WHERE (@firstname IS NULL OR CUS.FirstName Like @firstname + '%')
		AND (@lastname IS NULL OR CUS.LastName Like @lastname + '%') 
		AND (@clientId IS NULL OR CQ.PartnerId = @clientId)
		AND (@providerId IS NULL OR CQ.ProviderId = @providerId)
		AND (CONVERT(VARCHAR(11),CQ.CreatedDate,111) BETWEEN ISNULL(@startDate,'1900-1-1') AND ISNULL(@endDate, '2099-1-1'))
		AND (@bookingReference IS NULL OR CP._BookingReference = @bookingReference)
		AND (@IsSolved IS NULL OR @IsSolved = CQ.IsSolved)
		AND (@campaignGroupId IS NULL OR @campaignGroupId = CQ.CampaignId)
		AND (@queryTypeID IS NULL OR @queryTypeID = CQ.QueryTypeId)
		AND  CQ.CustomerId > 0
END
