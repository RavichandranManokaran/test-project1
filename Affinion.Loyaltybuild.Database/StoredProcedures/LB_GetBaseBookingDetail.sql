﻿/****** Object:  StoredProcedure [dbo].[LB_GetBaseBookingDetail]    Script Date: 2/12/2016 11:40:56 AM ******/
DROP PROCEDURE [dbo].[LB_GetBaseBookingDetail]
GO

/****** Object:  StoredProcedure [dbo].[LB_GetBaseBookingDetail]    Script Date: 2/12/2016 11:40:56 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[LB_GetBaseBookingDetail]
(
	@OrderLineID int
) 
AS 
BEGIN
	SELECT 
		OL.OrderLineId,
		RR.OrderReference [BookingReferance],
		RR.IsCancelled,
		RR.CheckInDate
	FROM 
		[dbo].uCommerce_OrderLine OL 
		INNER JOIN [dbo].LB_RoomReservation RR on RR.OrderLineId = OL.OrderLineId 
	WHERE 
		OL.OrderLineID= @OrderLineID
END


