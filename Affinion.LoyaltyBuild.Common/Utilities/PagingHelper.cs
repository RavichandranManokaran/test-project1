﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           LanguageReader.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Common
/// Description:           Code is used to read key values and return corresponding text value.
/// </summary>

#region Using Directives
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Affinion.LoyaltyBuild.Common.Instrumentation;
#endregion

namespace Affinion.LoyaltyBuild.Common.Utilities
{
    public static class PagingHelper
    {
        public static ICollection<int> CreatePages(int pageCount, int currentPage)
        {
            try
            {
                List<int> pageRange = new List<int>();

                if (pageCount > 10)
                {
                    int startRangeStartPage = 1;
                    int endRangeStartPage = pageCount - 3;

                    if (currentPage <= 5 || (currentPage >= endRangeStartPage && currentPage + 5 >= endRangeStartPage))
                    {
                        startRangeStartPage = 1;
                    }
                    else if (currentPage < endRangeStartPage && currentPage + 5 >= endRangeStartPage)
                    {
                        startRangeStartPage = pageCount - 8;
                    }
                    else
                    {
                        int startPageRange = (int)Math.Floor((double)currentPage / 5);
                        startPageRange = currentPage % 5 == 0 && startPageRange > 1 ? startPageRange - 1 : startPageRange;
                        int lastPage = startPageRange * 5;
                        startRangeStartPage = lastPage + 1;
                    }

                    var startRange = Enumerable.Range(startRangeStartPage, 5);
                    var endRange = Enumerable.Range(endRangeStartPage, 4);

                    pageRange.AddRange(startRange);

                    if (startRangeStartPage + 5 < endRangeStartPage)
                    {
                        pageRange.Add(0);
                    }

                    pageRange.AddRange(endRange);
                }
                else
                {
                    pageRange.AddRange(Enumerable.Range(1, pageCount));
                }

                return pageRange;
            }
            catch (Exception ex) 
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, null);
                throw;
            }
        }
    }
}
