﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Order.ascx.cs" Inherits="Affinion.LoyaltyBuild.UCom.MasterClass.Website.Controls.Order" %>
<%@ Import Namespace="UCommerce" %>

<table>
    <thead>
        <tr>
            <th>Name</th>
            <th>Quantity</th>
            <th>Total</th>
        </tr>
    </thead>
    <tbody>
        <asp:Repeater runat="server" ID="OrderLinesRepeater" ItemType="UCommerce.EntitiesV2.OrderLine">
            <ItemTemplate>
                    
            </ItemTemplate>
        </asp:Repeater>
    </tbody>
    <tfoot>
        <tr>

        </tr>
    </tfoot>
</table>

<asp:Button runat="server" Text="Update cart" Visible="<%# IsEditable %>" ID="UpdateCart" OnClick="UpdateCart_OnClick"></asp:Button>

<a href="/store/billing.aspx">Continue Checkout</a>