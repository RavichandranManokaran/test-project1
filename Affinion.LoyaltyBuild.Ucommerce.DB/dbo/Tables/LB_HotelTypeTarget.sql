﻿CREATE TABLE [dbo].[LB_HotelTypeTarget] (
    [HotelTypeTargetId] INT            NOT NULL,
    [HotelType]         NVARCHAR (500) NOT NULL,
    CONSTRAINT [PK_uCommerce_HotelTypeTarget] PRIMARY KEY CLUSTERED ([HotelTypeTargetId] ASC),
    CONSTRAINT [FK_uCommerce_HotelTypeTarget_uCommerce_Target] FOREIGN KEY ([HotelTypeTargetId]) REFERENCES [dbo].[uCommerce_Target] ([TargetId])
);

