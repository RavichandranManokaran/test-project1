﻿CREATE TABLE [dbo].[uCommerce_ShippingMethodPrice] (
    [ShippingMethodPriceId] INT   IDENTITY (1, 1) NOT NULL,
    [ShippingMethodId]      INT   NOT NULL,
    [PriceGroupId]          INT   NOT NULL,
    [Price]                 MONEY NOT NULL,
    [CurrencyId]            INT   NOT NULL,
    CONSTRAINT [uCommerce_PK_ShippingMethodPrice] PRIMARY KEY CLUSTERED ([ShippingMethodPriceId] ASC),
    CONSTRAINT [uCommerce_FK_ShippingMethodPrice_Currency] FOREIGN KEY ([CurrencyId]) REFERENCES [dbo].[uCommerce_Currency] ([CurrencyId]),
    CONSTRAINT [uCommerce_FK_ShippingMethodPrice_PriceGroup] FOREIGN KEY ([PriceGroupId]) REFERENCES [dbo].[uCommerce_PriceGroup] ([PriceGroupId]),
    CONSTRAINT [uCommerce_FK_ShippingMethodPrice_ShippingMethod] FOREIGN KEY ([ShippingMethodId]) REFERENCES [dbo].[uCommerce_ShippingMethod] ([ShippingMethodId])
);

