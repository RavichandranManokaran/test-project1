﻿<%@ control language="C#" autoeventwireup="true" codebehind="OfflinePortalMenuButtons.ascx.cs" inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.OfflinePortal.OfflinePortalMenuButtons" %>


<div class="col-md-12 margin-bottom-15 ">
    <div class="btn-group" id="bookingMenu">
        <asp:Repeater ID="MenuButtons" runat="server" OnItemDataBound="MenuButtons_ItemDataBound" >
            <ItemTemplate>
                <a href='<%# (Eval("RedirectUrl")=="#"?"javascript:void(0);":Eval("RedirectUrl")) %>' disabled='<%#Eval("Disabled") %>' target='<%# Eval("Target") %>' 
                    runat="server" visible='<%# Eval("Children") == null %>'
                    class='<%# ("btn btn-primary " + Eval("CssClass"))%>'><%# Eval("MenuText") %></a>
                <div class="btn-group" runat="server" visible='<%# Eval("Children") != null %>'>
                    <a href="javascript:void(0);" target='<%# Eval("Target") %>' class="btn btn-primary dropdown-toggle <%# Eval("CssClass") %>" data-toggle="dropdown">
                        <%# Eval("MenuText") %>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <asp:Repeater ID="SubButtons" runat="server">
                            <ItemTemplate>
                                <li class='<%# Eval("CssClass") %>'><a class="dropdown-item" disabled='<%#Eval("Disabled") %>' href='<%# Eval("RedirectUrl") %>'> <%# Eval("MenuText") %></a></li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                 </div>
            </ItemTemplate>
        </asp:Repeater>
        </div>
</div>
<div id="dialog-cancel" style="display: none" >
    <div class="col-md-12">
        <div class="accordian-outer">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div id="collapse0" class="panel-collapse collapse in">
                        <div class="panel-body alert-info">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="panel panel-default">
                                        <div class="divCancel" id="divCancel" runat="server">
                                            <div class="panel-heading bg-primary">
                                                <h4 class="panel-title">Booking Id : <span>
                                                    <asp:Label runat="server" ID="lblBookingReference"></asp:Label>
                                                </span></h4>
                                            </div>
                                            <div class="panel-body alert-info">
                                                <div class="margin-row">
                                                    <h4 id="confirmMsg"></h4>
                                                </div>
                                                <div class="margin-row">
                                                    <h4>Note To Provider</h4>
                                                    <textarea id="taNoteToProvider" runat="server" class="form-control" rows="5"></textarea>
                                                     <asp:CustomValidator id="notetoprovider" ControlToValidate="taNoteToProvider" ClientValidationFunction="ClientValidate" Display="Dynamic" ErrorMessage="*Dont Enter Card Number!" ForeColor="Red" Font-Name="verdana"  Font-Size="10pt" runat="server" />
                                                </div>
                                                <div class="margin-row">
                                                    <h4>Internal Note</h4>
                                                    <textarea id="taCancelReason" runat="server" class="form-control" rows="5"></textarea>
                                                    <asp:CustomValidator id="cancelreason" ControlToValidate="taCancelReason" ClientValidationFunction="ClientValidate" Display="Dynamic" ErrorMessage="*Dont Enter Card Number!" ForeColor="Red" Font-Name="verdana"  Font-Size="10pt" runat="server" />
                                                </div>
                                                <div class="margin-row">
                                                    <button type="button" class="btn btn-default CompleteCancel" id="btnCompleteCancel" >Complete Cancellation</button>
                                                    <button type="button" id="btnCloseDialog" class="btn btn-default" data-dismiss="modal">Cancel Cancellation</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
jQuery(function($){
    $('#bookingMenu a.cancel').click(function(){
        PreCancelBooking();
    });
    $('#btnCloseDialog, #btnClose').click(function(){
         $("#dialog-cancel").dialog('close');
    });
    $('#btnCompleteCancel').click(function(){
        $("#dialog-cancel").dialog('close');
        CancelBooking();
    });
    function PreCancelBooking(olid)
    {
        $.getJSON('/handlers/cs/bookinghandler.ashx?action=precancel&olid=<%=QueryString("olid")%>', function(data){
            if(data.IsSuccess)
            {
                $("#confirmMsg").html(data.Message);
                jQuery("#dialog-cancel").dialog({
                    title: "Cancel Booking:",
                    width: '800',
                });
            }
            else
                CS.Common.ShowAlert(data.Message)
        });
    }
    function CancelBooking()
    {
        var obj = { 
            OrderLineId:<%=QueryString("olid")%>,

            CancelReason:$('#<%= taNoteToProvider.ClientID%>').val(),
            NoteToProvider:$('#<%= taCancelReason.ClientID %>').val(),

            CancelReason:$('#<%= taNoteToProvider.ClientID %>').val(),
            NoteToProvider:$('#<%= taCancelReason.ClientID %>').val(),

            CancelReason:$('#<%= taNoteToProvider.ClientID %>').val(),
            NoteToProvider:$('#<%= taCancelReason.ClientID %>').val(),

        };
        $.post('/handlers/cs/bookinghandler.ashx?action=cancel', JSON.stringify(obj), function(data){
            if(data.IsSuccess)
            {
                CS.Common.ShowAlert(data.Message, '', null, {
                    "Ok":function(){
                        $(this).dialog("close");
                        window.location = '<%= GetUrl("/paymenthistory?olid=" + QueryString("olid"))%>'
                    }
                });
            }
            else
                CS.Common.ShowAlert(data.Message)
        });
    }
});
</script>

