﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SuperValuTopNavigationLinkSublayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.TopNavigationLinkSublayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>

<li class="">
    <p id="PlaceholderText" runat="server">Set the datasource: Required Fields: Link</p>
    <sc:Link Field="Link" ID="Link" CssClass="single" runat="server"></sc:Link>
</li>
