﻿CREATE TABLE [dbo].[uCommerce_Subrate](
	[SubrateItemCode] [nvarchar](50) NOT NULL,
	[ItemID] [nvarchar](50) NOT NULL,
	[VAT] [float] NOT NULL,
	[ClientID] [uniqueidentifier] NOT NULL,
	[IsPercentage] [bit] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedOn] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_uCommerce_Subrate_1] PRIMARY KEY CLUSTERED 
(
	[SubrateItemCode] ASC,
	[ItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[uCommerce_Subrate] ADD  CONSTRAINT [DF_uCommerce_Subrate_VAT]  DEFAULT ((0)) FOR [VAT]
GO

ALTER TABLE [dbo].[uCommerce_Subrate] ADD  CONSTRAINT [DF_uCommerce_Subrate_IsPercentage]  DEFAULT ((0)) FOR [IsPercentage]
GO

ALTER TABLE [dbo].[uCommerce_Subrate] ADD  CONSTRAINT [DF_uCommerce_Subrate_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO

ALTER TABLE [dbo].[uCommerce_Subrate] ADD  CONSTRAINT [DF_uCommerce_Subrate_UpdatedOn]  DEFAULT (getdate()) FOR [UpdatedOn]
GO

ALTER TABLE [dbo].[uCommerce_Subrate] ADD  CONSTRAINT [DF_uCommerce_Subrate_CreatedBy]  DEFAULT (N'Admin') FOR [CreatedBy]
GO