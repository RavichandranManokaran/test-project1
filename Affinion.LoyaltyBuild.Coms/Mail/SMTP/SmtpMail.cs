﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.Comms.Mail.SMTP
{
    /// <summary>
    /// Implementation for IMail
    /// </summary>
    public class SmtpMail : IMail
    {
        /// <summary>
        /// private variables
        /// </summary>
        private string _subject;
        private string _body;
        private List<Recepient> _recepients;
        private string _fromEmail;

        /// <summary>
        /// Constructor for SmtpMail
        /// </summary>
        /// <param name="itemid">itemid</param>
        /// <param name="bodyField">bodyfield</param>
        /// <param name="subjectField">SubjectField</param>
        public SmtpMail(string itemid, string bodyField, string subjectField)
        {
            var mailItem = Sitecore.Context.Database.GetItem(Sitecore.Data.ID.Parse(itemid));
            
            _subject = !string.IsNullOrWhiteSpace(mailItem[bodyField]) ? mailItem[bodyField] : string.Empty;
            _body = !string.IsNullOrWhiteSpace(mailItem[bodyField])?mailItem[bodyField]:string.Empty;
            _recepients = new List<Recepient>();
        }
        
        /// <summary>
        /// Email Subject
        /// </summary>
        public string Subject
        {
            get
            {
                return _subject;
            }
            set
            {
                _subject = value;
            }
        }

        /// <summary>
        /// Email Body
        /// </summary>
        public string Body
        {
            get
            {
                return _body;
            }
            set
            {
                _body = value;
            }

        }

        /// <summary>
        /// List of Receiver Id
        /// </summary>
        public List<Recepient> Recepients
        {
            get
            {
                return _recepients;
            }
        }

        /// <summary>
        /// Method to add Recepient
        /// </summary>
        /// <param name="recepient">recepient</param>
        public void AddRecipient(Recepient recepient)
        {
            _recepients.Add(recepient);
        }

        /// <summary>
        /// Sender Id
        /// </summary>
        public string FromAddress
        {
            get
            {
                return _fromEmail;
            }
            set
            {
                _fromEmail = value;

            }
        }
    }
}
