﻿//<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SearchResultsSublayout.ascx.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal
/// Description:           Used to Bind data to BookingDetailsSubLayout subLayout
/// </summary>


namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal
{
    #region Using Directives
    using Affinion.Loyaltybuild.BusinessLogic.Helper;
    using Affinion.LoyaltyBuild.Presentation.BaseLayouts;
    using Affinion.LoyaltyBuild.Common.Exceptions;
    using Affinion.LoyaltyBuild.Common.Instrumentation;
    using Affinion.LoyaltyBuild.Common.SessionHelper;
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Affinion.LoyaltyBuild.Search.Data;
    using Affinion.LoyaltyBuild.Search.Helpers;
    using Sitecore.Data.Fields;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI.HtmlControls;
    using System.Web.UI.WebControls;
    using Affinion.LoyaltyBuild.Common;
    using System.Collections.ObjectModel;
    using Sitecore.Data;
    using Affinion.LoyaltyBuild.Search.SupplierFilters;
    using Affinion.Loyaltybuild.BusinessLogic;
    using bb = Affinion.LoyaltyBuild.BedBanks;
    using Affinion.LoyaltyBuild.BedBanks.General;
    using Affinion.LoyaltyBuild.BedBanks.Helper;
    using Affinion.LoyaltyBuild.BedBanks.JacTravel.Message;
    using Affinion.LoyaltyBuild.BedBanks.JacTravel.Model;
    using Affinion.LoyaltyBuild.BedBanks.Troika;
    using Affinion.LoyaltyBuild.BedBanks.JacTravel.Model.Room;
    using NewSearchApi = Affinion.LoyaltyBuild.Api.Search.Data;
    using searchhelper = Affinion.LoyaltyBuild.Api.Search.Helpers;
    using Affinion.LoyaltyBuild.Api.Search.Data;
    using Affinion.LoyaltyBuild.Model.Product;
    using Affinion.LoyaltyBuild.Web.ClientPortal.ViewModels;
    using Affinion.LoyaltyBuild.Web.ClientPortal.Helper;
    using Affinion.LoyaltyBuild.Model.Provider;
    using Affinion.LoyaltyBuild.Api.Booking.Data.Basket;
    using Newtonsoft.Json;
    using System.Web;
    using System.Data;
    using System.Text;
    using System.Web.UI;



    #endregion

    public partial class SearchResultsSublayout : BaseSublayout
    {
        #region Properties

        /// <summary>
        /// Checkin date
        /// </summary>
        protected DateTime? CheckInDate
        {
            get
            {
                return checkInDate;
            }
        }

        /// <summary>
        /// is checkin date selected
        /// </summary>
        protected bool IsDateSelected { get; set; }

        /// <summary>
        /// result size
        /// </summary>
        protected int ResultSize { get; set; }

        /// <summary>
        /// page size
        /// </summary>
        protected int PageSize { get; set; }

        /// <summary>
        /// current page
        /// </summary>
        protected int CurrentPage { get; set; }

        /// <summary>
        /// get previous week url
        /// </summary>
        protected string PreviousWeekUrl
        {
            get
            {
                var searchKey = new NewSearchApi.SearchKey(HttpContext.Current);
                //set url
                if (searchKey.IsValid && searchKey.CheckInDate != null)
                {
                    searchKey.CheckInDate = searchKey.CheckInDate.Value.AddDays(-7);
                    return string.Format("{0}?{1}", Request.Url.GetLeftPart(UriPartial.Path), searchKey.CreateSearchString());
                }
                return string.Empty;
            }
        }

        /// <summary>
        /// get previous week url
        /// </summary>
        protected string NextWeekUrl
        {
            get
            {
                var searchKey = new NewSearchApi.SearchKey(HttpContext.Current);
                //set url
                if (searchKey.IsValid && searchKey.CheckInDate != null)
                {
                    searchKey.CheckInDate = searchKey.CheckInDate.Value.AddDays(7);
                    return string.Format("{0}?{1}", Request.Url.GetLeftPart(UriPartial.Path), searchKey.CreateSearchString());
                }
                return string.Empty;
            }
        }

        /// <summary>
        /// get previous week url
        /// </summary>
        protected string BasePageUrl
        {
            get
            {
                var searchKey = new NewSearchApi.SearchKey(HttpContext.Current);
                //set url
                if (searchKey.IsValid && searchKey.CheckInDate != null)
                {
                    return string.Format("{0}?{1}&", Request.Url.GetLeftPart(UriPartial.Path), searchKey.CreateSearchString());
                }
                return string.Empty;
            }
        }


        #endregion

        #region Fields

        private int currentPage = 1;
        private int pageCount;
        private Item clientItem = SitecoreFieldsHelper.GetLookupFieldTargetItem(ItemHelper.RootItem, "ClientDetails");
        private Affinion.LoyaltyBuild.Search.Data.SearchKey keywords;
        private AnchorTagHelperData hotelPageLink = SitecoreFieldsHelper.GetUrl(Sitecore.Context.Item, "HotelInformationPage");
        private string inputChildAges = string.Empty;
        private NewSearchApi.PagedProviderCollection commonProviderColl;
        private IList<Affinion.LoyaltyBuild.Model.Provider.IProvider> searchResult;
        public HotelSupplierResults results = null;
        private DateTime? checkInDate = null;

        /// <summary>
        /// Class to store BasketData from multiple selection
        /// </summary>
        private class BasketData
        {
            public int Qty { get; set; }
            public string[] CommandArgument { get; set; }
        }

        #endregion

        #region public methods

        /// <summary>
        /// Executes on page pre load event
        /// </summary>
        /// <param name="e">The parameter</param>
        public override void PreLoad(EventArgs e)
        {
            try
            {
                Uri pageUri = Request.Url;
                QueryStringHelper helper = new QueryStringHelper(pageUri);
                keywords = new Affinion.LoyaltyBuild.Search.Data.SearchKey(pageUri, clientItem);

                //keywords.ChildrenAges 

                if (this.GetEventTarget("PreviousWeeks"))
                {
                    DateTime checkInDate = keywords.CheckinDate.AddDays(-43);
                    DateTime checkOutDate = keywords.CheckinDate.AddDays(-1);
                    helper.SetValue("CheckinDate", checkInDate.ToString(DateTimeHelper.DateFormat));
                    helper.SetValue("CheckoutDate", checkOutDate.ToString(DateTimeHelper.DateFormat));

                    Response.Redirect(helper.GetUrl().ToString(), true);
                }
                else if (this.GetEventTarget("NextWeeks"))
                {
                    DateTime checkInDate = keywords.CheckoutDate.AddDays(1);
                    DateTime checkOutDate = keywords.CheckoutDate.AddDays(42);
                    helper.SetValue("CheckinDate", checkInDate.ToString(DateTimeHelper.DateFormat));
                    helper.SetValue("CheckoutDate", checkOutDate.ToString(DateTimeHelper.DateFormat));

                    Response.Redirect(helper.GetUrl().ToString(), true);
                }

                //this.SetControlValues(keywords);
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, this);
                throw;
            }

        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Page Load
        /// </summary>        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                BindSitecoreTexts();
                this.AddJavascriptVariable("MapLocations", string.Empty);
                this.AddJavascriptVariable("pageMode", null);
                string currentUrl = Request.Url.ToString();
                Uri pageUri = Request.Url;
                QueryStringHelper helper = new QueryStringHelper(pageUri);
                string pageMode = helper.GetValue("mode");
                if (helper.GetValue("ChildAge") != null)
                {

                    inputChildAges = helper.GetValue("ChildAge");
                }
                if (!string.IsNullOrEmpty(pageMode) && pageMode == "LocationFinder")
                {
                    CountySection.Visible = true;
                    if (!IsPostBack)
                    {
                        this.LoadCountyDropDown();
                    }
                }
                else
                {
                    CountySection.Visible = false;
                }

                this.LoadSearchResults();
                #region LWP-873
                Session["IsClientPortalLoaded"] = null;
                #endregion
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, null);
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="roomDetails"></param>
        /// <returns></returns>
        protected string GetTipText(ProviderInfo roomDetails)
        {
            try
            {
                if (roomDetails == null)
                {
                    return string.Empty;
                }

                string name = string.Empty;

                if (roomDetails.RoomAvailability == 1 && roomDetails.ProviderType == ProviderType.LoyaltyBuild)
                {
                    name += "<span class='text-red'>(";
                    name += "Hurry! only 1 left";
                    name += ")</span>";
                }
                return name;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, this);
                throw;
            }
        }

        protected string GetRoomName(ProviderInfo roomDetails)
        {
            try
            {
                if (roomDetails == null)
                {
                    return string.Empty;
                }
                string name = "<span>" + roomDetails.Name + "</span>";
                return name;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, this);
                throw;
            }
        }

        protected string GetRoomPrice(ProviderInfo offer)
        {
            try
            {
                if (offer == null)
                {
                    return string.Empty;
                }

                decimal price = offer.Price;

                CurrencyInfo ci = new CurrencyInfo();
                return ci.GetPriceWithCurrency(offer.Price);
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, null);
                throw;
            }
        }

        protected string SupplierInformationPageLink(HotelSearchResultItem supplierItem)
        {
            try
            {
                if (supplierItem == null)
                {
                    return @"javascript:void(0)";
                }
                Uri pageUri = Request.Url;
                QueryStringHelper helper = new QueryStringHelper(pageUri);
                string supplierPageLink;
                if (supplierItem.ProviderType == ProviderType.LoyaltyBuild)
                {
                    supplierPageLink = string.Format("{0}?id={1}&{2}", hotelPageLink.Url, supplierItem.SitecoreItemId, helper.QueryStringParameters.ToString());
                }
                else
                {
                    supplierPageLink = string.Format("{0}?providerId=1&propertyRefId={1}&{2}", hotelPageLink.Url, supplierItem.PropertyReferenceId, helper.QueryStringParameters.ToString());
                }
                return supplierPageLink;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, null);
                throw;
            }
        }

        /// <summary>
        /// Sort Link
        /// </summary>
        /// <param name="sortMode">Sort mode</param>
        /// <param name="sortValue">Sort value</param>
        /// <returns></returns>
        protected string SortLink(SortMode sortMode, string sortValue)
        {
            QueryStringHelper stringHelper = new QueryStringHelper(this.Request.Url);
            stringHelper.SetValue("sortmode", sortMode.ToString());
            stringHelper.SetValue("pageid", "1");

            if (!string.IsNullOrEmpty(sortValue))
            {
                stringHelper.SetValue("sortValue", sortValue);
            }

            return stringHelper.GetUrl(false).ToString();
        }

        protected string PaginationLink(int pageNumber)
        {
            if (pageNumber == 0)
            {
                return @"javascript:void(0)";
            }

            QueryStringHelper stringHelper = new QueryStringHelper(this.Request.Url);
            stringHelper.SetValue("PageId", pageNumber.ToString());

            return stringHelper.GetUrl(false).ToString();
        }

        protected string PreviousLink()
        {
            if (currentPage != 1)
            {
                return "<a href=" + this.PaginationLink(currentPage - 1) + ">Previous</a>";
            }

            return string.Empty;
        }

        protected string NextLink()
        {
            if (currentPage != pageCount)
            {
                return "<a href=" + this.PaginationLink(currentPage + 1) + ">Next</a>";
            }

            return string.Empty;
        }

        protected string SelectedPageClass(int pageNumebr)
        {
            if (this.currentPage == pageNumebr)
            {
                return "active";
            }

            return string.Empty;
        }

        protected string SupplierMapLocation(HotelSearchResultItem supplierItem)
        {
            GeoCordinate cordinate = new GeoCordinate();

            if (supplierItem == null)
            {
                return @"javascript:void(0)";
            }

            Item location = SitecoreFieldsHelper.GetDropLinkFieldTargetItem(supplierItem.SitecoreItem, "MapLocation");
            string locationData = SitecoreFieldsHelper.GetValue(location, "MapLocation");
            string supplierID = supplierItem.SitecoreItemId;
            cordinate.Latitude = "0";
            cordinate.Longitute = "0";

            if (!string.IsNullOrWhiteSpace(locationData))
            {
                List<string> latLongList = locationData.Split(',').ToList<string>();
                if (latLongList.Count == 2)
                {
                    cordinate.Latitude = latLongList[0];
                    cordinate.Longitute = latLongList[1];
                }
            }

            string cord = string.Concat(cordinate.Latitude, ",", cordinate.Longitute, ",", supplierID);
            return cord;
        }

        /// <summary>
        /// ItemDataBound of OffersList Repeater
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OffersList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e != null)
                {
                    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                    {
                        DropDownList selectList = e.Item.FindControl("drpAvailability") as DropDownList;


                        int availability = (int)((ProviderInfo)e.Item.DataItem).RoomAvailability;
                        if (((ProviderInfo)e.Item.DataItem).ProviderType == ProviderType.BedBanks)
                        {
                            selectList.DataSource = Enumerable.Range(0, 2);
                        }

                        else if (availability > keywords.NumberOfRooms + 3)
                        {
                            selectList.DataSource = Enumerable.Range(0, keywords.NumberOfRooms + 3);
                        }
                        else
                        {
                            selectList.DataSource = Enumerable.Range(0, availability + 1);
                        }
                        selectList.DataBind();

                        if (availability <= 0)
                        {
                            selectList.Enabled = false;
                        }

                    }
                    else if (e.Item.ItemType == ListItemType.Footer)
                    {
                        var addToBasket = e.Item.FindControl("ButtonAddToBasket") as Button;
                        if (addToBasket != null)
                        {
                            //addToBasket.Click += addToBasket_Click;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, null);
                throw;
            }
        }

        /// <summary>
        /// Add to basket
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ButtonAddToBasket_Click(object sender, EventArgs e)
        {
            try
            {
                Button button = (sender as Button);

                //Get the command argument
                string[] commandArgument = button.CommandArgument.Split(',');
                int qty = 0;

                // ToDO: check the qty value
                foreach (var item in button.Parent.Controls)
                {
                    if (item.GetType().Name.Equals("DropDownList"))
                    {
                        bool isValid = Int32.TryParse((item as DropDownList).SelectedValue, out qty);
                        //qty = (item as DropDownList).SelectedValue;
                    }
                }

                if (qty == 0)
                {

                    return;
                }

                // Create basket session
                //CreateBasketSession(commandArgument[0], commandArgument[2]);
                //BasketHelper.AddToBasket(commandArgument, qty, (BasketInfo)Session["BasketInfo"]);

                //Rfresh the page
                //Response.Redirect(Request.RawUrl);
                Response.Redirect("/Basket");
            }
            catch (Exception ex)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Use to add to basket in one click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ButtonAddToBasketOneClick_Click(object sender, EventArgs e)
        {
            Button btnAddToBasket = sender as Button;
            int qty = 0;
            List<BasketData> BasketDataList = new List<BasketData>();

            try
            {
                if (btnAddToBasket != null)
                {
                    foreach (RepeaterItem repeaterControls in ((System.Web.UI.WebControls.Repeater)btnAddToBasket.NamingContainer.Parent).Controls)
                    {
                        //foreach (RepeaterItem repeaterControls in item.Parent.Controls)
                        //{
                        // Get the Item only from Item Template section (Header, Item and Footer are the section of ASP.Repeater)
                        if (repeaterControls.ItemType == ListItemType.Item || repeaterControls.ItemType == ListItemType.AlternatingItem)
                        {
                            DropDownList drpQty = repeaterControls.Controls[0].FindControl("drpAvailability") as DropDownList;
                            Button buttonAdd = repeaterControls.Controls[0].FindControl("ButtonAddToBasket1") as Button;
                            Label drpvalidation = repeaterControls.Controls[0].FindControl("btnvalidation") as Label;
                            if (drpQty != null)
                            {
                                bool isValid = Int32.TryParse((drpQty).SelectedValue, out qty);
                            }

                            if (buttonAdd != null)
                            {
                                //Get the command argument
                                string[] commandArgument = buttonAdd.CommandArgument.Split(',');


                                //foreach (var result in results.AllResults.Where(y => y.ProviderType == ProviderType.LoyaltyBuild && (y.SitecoreItemId) == commandArgument[2]))
                                //{
                                //    if (qty == 0)
                                //    {

                                //        //drpvalidation.Attributes.Add("Style", "display:block");
                                //        drpvalidation.Text = "select the room";
                                //        break;
                                //    }
                                //}
                                if (qty == 0)
                                {

                                    foreach (var result in results.AllResults.Where(y => y.ProviderType == ProviderType.LoyaltyBuild))//&& (y.SitecoreItemId) == commandArgument[2]))
                                    {
                                        var id = result.SitecoreItemId;
                                        if (id == commandArgument[2])
                                        {
                                            //buttonAdd.OnClientClick = "btnvalidation()";
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "btnvalidation();", true);

                                            break;

                                        }
                                    }
                                }

                               // if (qty > 0)
                                else
                                {
                                    BasketData basketData = new BasketData() { Qty = qty, CommandArgument = commandArgument };
                                    BasketDataList.Add(basketData);
                                }
                            }
                        }
                        // }
                    }

                    // Refresh the page when add to basket success (qty > 0 also ideal)
                    if (BasketDataList.Count > 0)
                    {
                        foreach (BasketData item in BasketDataList)
                        {
                            int currencyID = Convert.ToInt32(item.CommandArgument[1]);
                            IProduct outRoom = CreateBasketSession(item.CommandArgument[0], item.CommandArgument[2]);
                            if (outRoom is Room)
                            {
                                for (int k = 0; k < item.Qty; k++)
                                {
                                    Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.AddToBasket(outRoom, 1, RoomInfo(item.CommandArgument[0], item.CommandArgument[3], currencyID));
                                }
                            }
                            else if (outRoom is BedBankProduct)
                            {
                                string[] roomInfo = GetRoomInfoForBedBanksRequest();
                                for (int k = 0; k < item.Qty; k++)
                                {
                                    string[] roomDetails = roomInfo[k].Split(',');
                                    int bbPropertyId = Convert.ToInt32(item.CommandArgument[2]);
                                    Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.AddToBasket(outRoom, item.Qty, RoomInfo(item.CommandArgument[0], item.CommandArgument[3], currencyID, bbPropertyId, item.CommandArgument[4], item.CommandArgument[5], item.CommandArgument[6], roomDetails[0], roomDetails[1], roomDetails[2]));
                                }
                            }
                        }

                        BasketDataList.Clear();
                        //Refresh the page                                    
                        Response.Redirect("/Basket");
                    }
                }
            }
            catch (Exception ex)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, ex.Message);
                throw;
            }
        }








        public string[] GetRoomInfoForBedBanksRequest()
        {
            int rooms = 0; int adults = 0; int children = 0; string childages = string.Empty;
            rooms = keywords.NumberOfRooms;
            adults = keywords.NumberOfAdults;
            children = keywords.NumberOfChildren;
            childages = inputChildAges;
            string roomInfo = JacTravelApiWrapper.GetRoomType(rooms.ToString(), adults.ToString(), children.ToString(), childages);
            return roomInfo.Split('|');
        }

        private AddRoomRequest RoomInfo(string variatSku, string roomName, int currencyID)
        {

            AddRoomRequest roomre = new AddRoomRequest();
            roomre.CheckinDate = keywords.CheckinDate;
            roomre.CheckOutDate = keywords.CheckoutDate;
            roomre.VariantSku = variatSku;
            roomre.ClientId = clientItem.ID.ToString();
            roomre.NoOfRooms = 1;
            roomre.NoOfAdults = keywords.NumberOfAdults;
            roomre.NoOfChildren = keywords.NumberOfChildren;
            roomre.NoofNights = keywords.NoOfNights;
            roomre.ChildrenAges = inputChildAges;
            roomre.BookingThrough = BookingMethod.Online.ToString();
            roomre.CreatedBy = SitecoreUserProfileHelper.GetItem(UserProfileKeys.CreatedBy);
            roomre.RoomType = roomName;
            roomre.CurrencyId = currencyID;
            return roomre;

        }
        private AddBedBanksRequest RoomInfo(string variatSku, string roomName, int currencyID, int propertyId, string bookingToken, string marginId, string mealBasisId, string NoOfAdults, string NoOfChildren, string ChildAges)
        {
            int temp = 0;
            AddBedBanksRequest roomReq = new AddBedBanksRequest();
            roomReq.PropertyID = propertyId;
            roomReq.ClientId = keywords.Client.ID.ToString();
            roomReq.CheckinDate = keywords.CheckinDate;
            roomReq.CheckoutDate = keywords.CheckoutDate;
            roomReq.NoofNights = keywords.NoOfNights;
            roomReq.NoOfAdults = int.Parse(NoOfAdults);
            roomReq.NoOfChildren = int.Parse(NoOfChildren);
            roomReq.ChildrenAges = ChildAges.Replace('/', ',');
            roomReq.RoomInfo = roomName;
            roomReq.PreBookingToken = bookingToken;
            roomReq.BookingThrough = BookingMethod.Online.ToString();
            roomReq.NoOfRooms = 1;
            roomReq.RoomType = ProductType.BedBanks.ToString();
            roomReq.NoofNights = keywords.NoOfNights;
            roomReq.CurrencyId = currencyID;
            roomReq.IsDirect = int.TryParse(bookingToken, out temp).ToString();
            int marginIdApplied = 0;
            int.TryParse(marginId, out marginIdApplied);
            roomReq.MarginIdApplied = marginIdApplied;
            roomReq.MealBasisID = mealBasisId;
            roomReq.PropertyName = results.AllResults.Where(y => y.ProviderType == ProviderType.BedBanks && int.Parse(y.ProviderId) == propertyId).Select(x => x.Name).FirstOrDefault().ToString();
            roomReq.ImageUrl = results.AllResults.Where(y => y.ProviderType == ProviderType.BedBanks && int.Parse(y.ProviderId) == propertyId).Select(x => x.IntroductionImage).FirstOrDefault().ToString();
            roomReq.StarRanking = int.Parse(results.AllResults.Where(y => y.ProviderType == ProviderType.BedBanks && int.Parse(y.ProviderId) == propertyId).Select(x => x.StarRanking).FirstOrDefault().ToString());
            roomReq.MaxAdults = int.Parse(results.AllResults.Where(y => y.ProviderType == ProviderType.BedBanks && int.Parse(y.ProviderId) == propertyId).Select(x => x.MaxAdults).FirstOrDefault().ToString());
            roomReq.MaxChildren = int.Parse(results.AllResults.Where(y => y.ProviderType == ProviderType.BedBanks && int.Parse(y.ProviderId) == propertyId).Select(x => x.MaxChild).FirstOrDefault().ToString());
            roomReq.MinAdults = int.Parse(results.AllResults.Where(y => y.ProviderType == ProviderType.BedBanks && int.Parse(y.ProviderId) == propertyId).Select(x => x.MinAdults).FirstOrDefault().ToString());
            //roomReq.OldPrice = results.AllResults.Where(y => y.ProviderType == ProviderType.BedBanks && int.Parse(y.ProviderId) == propertyId).Select(x => x.OldPrice).FirstOrDefault().ToString();
            roomReq.CreatedBy = string.Empty;
            decimal marginPercent = 0;
            string mAmount = string.Empty;
            string oldPrice = string.Empty;
            foreach (var result in results.AllResults.Where(y => y.ProviderType == ProviderType.BedBanks && int.Parse(y.ProviderId) == propertyId))
            {
                mAmount = result.RoomCollection.Select(x => x.BBMarginPercentage).FirstOrDefault().ToString();
                oldPrice = result.RoomCollection.Where(y => y.Name == roomName).Select(x => x.Price).FirstOrDefault().ToString("#.##");
            }
            decimal.TryParse(mAmount, out marginPercent);
            roomReq.OldPrice = oldPrice;
            roomReq.BBMarginPercentage = marginPercent;
            roomReq.BBCurrencyId = results.AllResults.Where(y => y.ProviderType == ProviderType.BedBanks && int.Parse(y.ProviderId) == propertyId).Select(x => x.BBCurrencyId).FirstOrDefault().ToString();
            roomReq.PropertyReferenceId = results.AllResults.Where(y => y.ProviderType == ProviderType.BedBanks && int.Parse(y.ProviderId) == propertyId).Select(x => x.PropertyReferenceId).FirstOrDefault().ToString();
            return roomReq;
        }

        /// <summary>
        /// Get the error message if no offer has been found
        /// </summary>
        /// <param name="resultItem">current offer</param>
        /// <returns>Returns the error message</returns>
        protected void SetNoOfferMessage(Hotel resultItem, RepeaterItemEventArgs e)
        {
            Item conditionalMessageItem = GetConditionalMessageItem();
            string message = string.Empty;

            if (e == null)
            {
                throw new ArgumentNullException("e");
            }

            HtmlGenericControl availabilityMessageDiv = e.Item.FindControl("availabilityMessage") as HtmlGenericControl;


            if (resultItem != null && availabilityMessageDiv != null)
            {

                //availabilityMessageDiv.InnerText = LanguageReader.GetText("NoProductPeopleCriteria", "Room selection does not match the criteria of our room search criteria");
                availabilityMessageDiv.InnerText = (conditionalMessageItem != null && conditionalMessageItem.Fields["No Product People Criteria Message"] != null) ? conditionalMessageItem.Fields["No Product People Criteria Message"].Value : string.Empty;

            }
            else if (resultItem != null && keywords.FlexibleDates)
            {
                // handle different error messages depending on the error type in future
                //availabilityMessageDiv.InnerText = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "NoOfferForFlexibleDatesMessage");
                availabilityMessageDiv.InnerText = (conditionalMessageItem != null && conditionalMessageItem.Fields["No Offer for Flexible Dates Message"] != null) ? conditionalMessageItem.Fields["No Offer for Flexible Dates Message"].Value : string.Empty;
            }
            else if (resultItem != null)
            {
                // handle different error messages depending on the error type in future
                //availabilityMessageDiv.InnerText = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "NoOfferFoundMessage");
                availabilityMessageDiv.InnerText = (conditionalMessageItem != null && conditionalMessageItem.Fields["No Room Availability Message"] != null) ? conditionalMessageItem.Fields["No Room Availability Message"].Value : string.Empty;
            }

            //return message;
        }

        #endregion

        #region Private Methods

        private string RoomInfo()
        {
            Uri pageUri = Request.Url;
            QueryStringHelper helper = new QueryStringHelper(pageUri);
            //string NoOfRooms = !string.IsNullOrEmpty)
            return string.Empty;
        }
        /// <summary>
        /// Bind sitecore texts
        /// </summary>
        private void BindSitecoreTexts()
        {
            Item currentPageITem = Sitecore.Context.Item;
            SitecoreFieldsHelper.BindSitecoreText(currentPageITem, "TextCountryFilter", CountryFilterText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageITem, "TextAscending", AscendingText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageITem, "TextDescending", DescendingText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageITem, "TextLowesttoHighest", PriceLowToHighText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageITem, "TextHighesttoLowest", PriceHighToLowText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageITem, "TextLowesttoHighest", RateLowToHighText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageITem, "TextHighesttoLowest", RateHighToLowText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageITem, "TextLowesttoHighest", RecommendLowToHighText);
            SitecoreFieldsHelper.BindSitecoreText(currentPageITem, "TextHighesttoLowest", RecommendHighToLowText);

            //Item conditionalMessageItem = GetConditionalMessageItem();
            //SitecoreFieldsHelper.BindSitecoreText(conditionalMessageItem, "No Results Found Message", ResultsSummary);
        }

        /// <summary>
        /// Get conditional message item
        /// </summary>
        private Item GetConditionalMessageItem()
        {
            Item conditionalMessageItem = this.GetItemFromRenderingParameterPath("Conditional Messages Link");

            if (conditionalMessageItem != null)
                return conditionalMessageItem;
            else
                return null;
        }

        /// <summary>
        /// Load search results
        /// </summary>
        private void LoadSearchResults()
        {
            Uri pageUri = Request.Url;
            QueryStringHelper helper = new QueryStringHelper(pageUri);



            if (!int.TryParse((helper.GetValue("PageId")), out this.currentPage))
            {
                this.currentPage = 1;
            }

            // add default filters
            List<ISupplierFilter> supplierFilters = new List<ISupplierFilter>();
            supplierFilters.Add(new FilterByClient());
            supplierFilters.Add(new FilterByLocation());
            supplierFilters.Add(new FilterByStarRanking());
            supplierFilters.Add(new FilterByFacilities());
            supplierFilters.Add(new FilterByThemes());
            supplierFilters.Add(new FilterByExperience());
            supplierFilters.Add(new FilterByPackage());
            supplierFilters.Add(new FilterByRecommend());
            // add aditional filters depending on the filter criteria


            keywords.SupplierFilters = supplierFilters;
            keywords.Facilities = Server.UrlDecode(keywords.Facilities);
            keywords.Themes = Server.UrlDecode(keywords.Themes);
            keywords.Experiences = Server.UrlDecode(keywords.Experiences);
            keywords.StarRanking = Server.UrlDecode(keywords.StarRanking);
            keywords.CurrencyCode = this.GetCurrencyInformation();
            keywords.LocalRanking = Server.UrlDecode(keywords.LocalRanking);

            if (Sitecore.Context.PageMode.IsPageEditorEditing)
            {
                return;
            }
            //List<string> test = Server.UrlDecode(keywords.LocalRanking);
            // NewSearchApi.SearchKey mySearchKey = new NewSearchApi.SearchKey(System.Web.HttpContext.Current);
            NewSearchApi.SearchKey mySearchKey = new NewSearchApi.SearchKey(this.Context);
            if (!mySearchKey.IsValid)
            {
                this.SortSection.Visible = false;
                return;
            }
            mySearchKey.ClientId = keywords.Client.ID.ToString();
            searchResult = new List<Affinion.LoyaltyBuild.Model.Provider.IProvider>();
            NewSearchApi.PagedProviderCollection providerCollection = searchhelper.SearchHelper.SearchHotels(mySearchKey, SetSortOptions(helper));

            //get current page
            int.TryParse(Request.QueryString["page"], out currentPage);
            //pager config
            if (providerCollection.Count > providerCollection.PageSize)
            {
                ResultSize = providerCollection.Count;
                PageSize = providerCollection.PageSize;
                CurrentPage = currentPage;
            }
            //pager visibility
            if (providerCollection.TotalPages > 1)
                divPager.Visible = true;


            if (providerCollection != null && providerCollection.Count > 0)
            {
                commonProviderColl = providerCollection;
                searchResult = searchhelper.SearchHelper.GetPageData(providerCollection, this.currentPage);
            }
            results = new HotelSupplierResults();
            if (searchResult != null && searchResult.Count > 0)
            {


                results.AllResults = FillData(searchResult).ToArray();

                if (results.AllResults.Count() == 1)
                {
                    if (mySearchKey.Location.Type == LocationType.Hotel)
                    {
                        if (!IsPostBack)
                        {
                            string hotelItemID = results.AllResults[0].SitecoreItemId;
                            string OldrequestUrl = Request.Url.ToString();
                            string tmp = "/hotelinformation?id=" + hotelItemID + "&Destination=";
                            if (OldrequestUrl.Contains("/hotels?Destination="))
                                OldrequestUrl = OldrequestUrl.Replace("/hotels?Destination=", tmp);
                            else
                                OldrequestUrl = OldrequestUrl.Replace("/locations?Destination=", tmp);
                            Response.Redirect(OldrequestUrl, false);
                        }
                    }

                }



                if (results.AllResults != null && results.AllResults.Count() == 0)
                {
                    this.SortSection.Visible = false;
                    Literal RecordsTextValueNew = this.FindControl("litSearchText") as Literal;
                    if (RecordsTextValueNew != null)
                        RecordsTextValueNew.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "NoResultsLabel", "Sorry, No Hotels matched with your search.");
                    return;
                }
                else
                {
                    this.SortSection.Visible = true;
                    SearchResultsRepeater.DataSource = results.AllResults;

                    // may be data binding can be moved to this function
                    this.CustomizePageOnSearchMode(keywords.SearchResultsMode, results, searchResult);


                    int itemsPerPage = SitecoreFieldsHelper.GetInteger(Sitecore.Context.Item, "ItemsPerPage", 10);
                    PagedDataSource resultsWithPaging = SearchHelper.GetHotelSearchPagedDataSource(results.AllResults, this.currentPage, itemsPerPage);
                    MapData coordinates = SearchHelper.GetNewSearchCordinates(resultsWithPaging, keywords);

                    this.AddJavascriptVariable("MapLocations", coordinates);
                    this.AddJavascriptVariable("pageMode", Enum.GetName(typeof(PageMode), keywords.Mode));

                    //Bind the search results to SearchResultsRepeater            
                    SearchResultsRepeater.ItemDataBound += SearchResultsRepeaterItemDataBound;
                    SearchResultsRepeater.DataBind();

                    this.pageCount = SearchHelper.PageCount;
                }
            }
            else
            {
                this.SortSection.Visible = false;
                Literal RecordsTextValueNew = this.FindControl("litSearchText") as Literal;
                if (RecordsTextValueNew != null)
                    RecordsTextValueNew.Text = SitecoreFieldsHelper.GetValue(Sitecore.Context.Item, "NoResultsLabel", "Sorry, No Hotels matched with your search.");
            }
        }

        private List<HotelSearchResultItem> FillData(IList<IProvider> searchResult)
        {
            CurrencyInfo ci = new CurrencyInfo();
            List<HotelSearchResultItem> hotelSearchResultItem = new List<HotelSearchResultItem>();
            foreach (var tmpItem in searchResult)
            {
                HotelSearchResultItem hotelOutputItem = new HotelSearchResultItem();
                if (tmpItem.ProviderType == ProviderType.LoyaltyBuild)
                {
                    Hotel hotelInputItem = (Hotel)tmpItem;
                    hotelOutputItem.ProviderId = hotelInputItem.ProviderId;
                    hotelOutputItem.SitecoreItemId = hotelInputItem.SitecoreItemId;
                    hotelOutputItem.ProviderType = hotelInputItem.ProviderType;
                    hotelOutputItem.IntroductionImage = hotelInputItem.IntroductionImage;
                    hotelOutputItem.Name = hotelInputItem.Name;
                    hotelOutputItem.Town = hotelInputItem.Town;
                    hotelOutputItem.Overview = hotelInputItem.Overview;
                    hotelOutputItem.StarRanking = Convert.ToString(hotelInputItem.StarRanking);
                    hotelOutputItem.SitecoreItem = hotelInputItem.SitecoreItem;
                    hotelOutputItem.Facilities = hotelInputItem.Facilities;
                    hotelOutputItem.Themes = hotelInputItem.Themes;
                    hotelOutputItem.Experiences = hotelInputItem.Experiences;
                    hotelOutputItem.AddressLine1 = hotelInputItem.AddressLine1;
                    hotelOutputItem.AddressLine2 = hotelInputItem.AddressLine2;
                    hotelOutputItem.AddressLine3 = hotelInputItem.AddressLine3;
                    hotelOutputItem.AddressLine4 = hotelInputItem.AddressLine4;
                    hotelOutputItem.Location = hotelInputItem.Location;
                    hotelOutputItem.UCommerceId = hotelInputItem.UCommerceId;
                    hotelOutputItem.Country = hotelInputItem.Country;
                    hotelOutputItem.LocalRanking = hotelInputItem.LocalRanking;
                    hotelOutputItem.LowestPrice = hotelInputItem.LowestPrice;
                    hotelOutputItem.CurrencyID = hotelInputItem.CurrencyID;
                    if (keywords.FlexibleDates)
                    {
                        hotelSearchResultItem.Add(hotelOutputItem);
                    }
                    else
                    {
                        List<ProviderInfo> temp = LoadRoomsData(hotelInputItem);
                        if (temp != null && temp.Count > 0)
                        {
                            hotelOutputItem.RoomCollection = temp;
                            hotelOutputItem.LowestPrice = ci.GetPriceWithCurrency(hotelOutputItem.RoomCollection.OrderBy(x => x.Price).FirstOrDefault().Price);
                            hotelSearchResultItem.Add(hotelOutputItem);
                        }
                    }
                }
                else
                {
                    BedBankHotel bbhotelInputItem = (BedBankHotel)tmpItem;
                    hotelOutputItem.ProviderId = bbhotelInputItem.ProviderId;
                    hotelOutputItem.PropertyReferenceId = bbhotelInputItem.PropertyReferenceId;
                    hotelOutputItem.SitecoreItemId = bbhotelInputItem.SitecoreItemId;
                    hotelOutputItem.ProviderType = bbhotelInputItem.ProviderType;
                    hotelOutputItem.IntroductionImage = bbhotelInputItem.IntroductionImage;
                    hotelOutputItem.Name = bbhotelInputItem.Name;
                    hotelOutputItem.Town = bbhotelInputItem.Region;
                    hotelOutputItem.Overview = bbhotelInputItem.StrapLine;
                    hotelOutputItem.StarRanking = Convert.ToString(bbhotelInputItem.StarRanking);
                    hotelOutputItem.SitecoreItem = null;
                    hotelOutputItem.Facilities = bbhotelInputItem.Facilities;
                    hotelOutputItem.AddressLine1 = bbhotelInputItem.AddressLine1;
                    hotelOutputItem.AddressLine2 = bbhotelInputItem.AddressLine2;
                    hotelOutputItem.Country = bbhotelInputItem.Country;
                    hotelOutputItem.MaxAdults = bbhotelInputItem.MaxAdults;
                    hotelOutputItem.MaxChild = bbhotelInputItem.MaxChildren;
                    hotelOutputItem.MinAdults = bbhotelInputItem.MinAdults;
                    int curr = 0;
                    int.TryParse(bbhotelInputItem.CurrencyId, out curr);
                    hotelOutputItem.CurrencyID = curr;
                    hotelOutputItem.BBCurrencyId = bbhotelInputItem.BBCurrencyId;

                    List<ProviderInfo> temp = LoadRoomsData(bbhotelInputItem);

                    if (temp != null && temp.Count > 0)
                    {
                        hotelOutputItem.RoomCollection = temp;
                        hotelOutputItem.LowestPrice = ci.GetPriceWithCurrency(hotelOutputItem.RoomCollection.OrderBy(x => x.Price).FirstOrDefault().Price);
                        hotelOutputItem.OldPrice = hotelOutputItem.RoomCollection.OrderBy(x => x.Price).FirstOrDefault().Price.ToString("#.##");
                        hotelOutputItem.BBCurrencyId = hotelOutputItem.RoomCollection.Select(x => x.UComCurrencyId).FirstOrDefault();
                        hotelSearchResultItem.Add(hotelOutputItem);
                    }
                }
            }
            return hotelSearchResultItem;
        }

        /// <summary>
        /// Sets Search Options for
        /// </summary>
        /// <param name="helper"></param>
        /// <returns></returns>
        private SearchOptions SetSortOptions(QueryStringHelper helper)
        {
            SearchOptions tmpSearchOptions = new SearchOptions();
            if (helper.GetValue("sortmode") != null)
            {
                switch (helper.GetValue("sortmode"))
                {
                    case "Price":
                        tmpSearchOptions.SortOn = SortField.Price;
                        break;
                    case "SupplierName":
                        tmpSearchOptions.SortOn = SortField.Name;
                        break;
                    case "Rate":
                        tmpSearchOptions.SortOn = SortField.Rate;
                        break;
                    case "Recommend":
                        tmpSearchOptions.SortOn = SortField.Recommended;
                        break;
                }
            }
            if (helper.GetValue("sortValue") != null)
            {
                switch (helper.GetValue("sortValue"))
                {
                    case "0":
                        tmpSearchOptions.SortDirection = Affinion.LoyaltyBuild.Api.Search.Data.SortDirection.Ascending;
                        break;
                    case "1":
                        tmpSearchOptions.SortDirection = Affinion.LoyaltyBuild.Api.Search.Data.SortDirection.Descending;
                        break;
                }
            }
            return tmpSearchOptions;
        }


        /// <summary>
        /// Create basket information in session
        /// </summary>
        private IProduct CreateBasketSession(string argumentValue, string providerid)
        {
            Uri pageUri = Request.Url;
            QueryStringHelper helper = new QueryStringHelper(pageUri);
            IProduct outRoom = null;
            foreach (var tmpItem in searchResult)
            {
                foreach (var room in tmpItem.Products)
                {
                    if (room.VariantSku == argumentValue)
                    {
                        outRoom = room;
                        break;
                    }
                    //  Affinion.LoyaltyBuild.Api.Booking.Helper.BasketHelper.AddToBasket(room, qty, RoomInfo(variatSku));
                }
            }

            BasketInfo basketInfo = new BasketInfo
            {
                VariantSku = argumentValue,
                SupplierId = providerid,
                CheckinDate = helper.GetValue("CheckinDate"),
                CheckoutDate = helper.GetValue("CheckoutDate"),
                Nights = helper.GetValue("NoOfNights"),
                Location = helper.GetValue("Destination"),
                NoOfRooms = helper.GetValue("NoOfRooms"),
                NoOfAdults = helper.GetValue("NoOfAdults"),
                NoOfChildren = helper.GetValue("NoOfChildren"),
                OfferGroup = string.Empty,
                BookingThrough = BookingMethod.Online.ToString(),
                ChildrenAges = inputChildAges
            };

            Session["BasketInfo"] = basketInfo;

            List<BasketInfo> listBasketInfo = new List<BasketInfo>();

            listBasketInfo.Add(basketInfo);

            // bind the unique value to session
            //Session["listBasketInfo"] = listBasketInfo.Select(p => p.Sku).Distinct();
            listBasketInfo = listBasketInfo.Distinct().ToList();
            Session["listBasketInfo"] = listBasketInfo;

            return outRoom;
        }

        /// <summary>
        /// Define search mode
        /// </summary>
        /// <param name="resultsMode"></param>
        /// <param name="results"></param>
        private void CustomizePageOnSearchMode(SearchResultsMode resultsMode, HotelSupplierResults results, IList<Affinion.LoyaltyBuild.Model.Provider.IProvider> newSearchResults)
        {
            Dictionary<string, string> mustacheContext = new Dictionary<string, string>();
            mustacheContext.Add("{{Keyword}}", keywords.Destination);
            mustacheContext.Add("{{ResultCount}}", results.AllResults.Length.ToString());

            if (resultsMode == SearchResultsMode.SupplierName)
            {
                if (results.AllResults.Length == 1)
                {
                    Uri pageUri = Request.Url;
                    QueryStringHelper helper = new QueryStringHelper(pageUri);

                    //Item supplierItem = results.AllResults[0].GetItem();
                    string supplierPageLink = string.Format("{0}?id={1}&{2}", hotelPageLink.Url, newSearchResults[0].SitecoreItemId, helper.QueryStringParameters.ToString());
                    Response.Redirect(supplierPageLink, true);
                }
            }
        }

        /// <summary>
        /// Event handler for binding search result item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchResultsRepeaterItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            try
            {
                QueryStringHelper helper = new QueryStringHelper(Request.Url);

                Repeater facilities = e.Item.FindControl("HotelFacilities") as Repeater;
                HotelSearchResultItem supplierItem = e.Item.DataItem as HotelSearchResultItem;
                HtmlGenericControl divBBMessage = e.Item.FindControl("FullPaymentDiv") as HtmlGenericControl;
                divBBMessage.Visible = false;
                if (supplierItem.SitecoreItem == null)
                {
                    HtmlGenericControl divLB = e.Item.FindControl("LBTownDiv") as HtmlGenericControl;
                    divLB.Visible = false;
                    Image introImage = e.Item.FindControl("imgBBIntroImage") as Image;
                    introImage.ImageUrl = supplierItem.IntroductionImage;
                    Literal BBNameValue = e.Item.FindControl("BBName") as Literal;
                    BBNameValue.Text = supplierItem.Name;
                    Literal TownValue = e.Item.FindControl("BBTown") as Literal;
                    TownValue.Text = supplierItem.Town;
                    Literal BBOverviewValue = e.Item.FindControl("BBOverview") as Literal;
                    BBOverviewValue.Text = supplierItem.Overview;
                    divBBMessage.Visible = true;
                    facilities.DataSource = supplierItem.Facilities;
                    facilities.DataBind();
                }
                Literal litFullPayment = e.Item.FindControl("litFullPayment") as Literal;
                Item conditionalMessageItem = GetConditionalMessageItem();
                litFullPayment.Text = SitecoreFieldsHelper.GetItemFieldValue(conditionalMessageItem, "Bed Banks Limited Stock Message");
                Repeater productsRepeater = e.Item.FindControl("OffersList") as Repeater;
                Repeater starRepeater = e.Item.FindControl("StarRepeater") as Repeater;
                Literal country = e.Item.FindControl("CountryName") as Literal;
                HtmlGenericControl lowestPriceContainer = e.Item.FindControl("LowestPriceContainer") as HtmlGenericControl;

                Image hotelRating = e.Item.FindControl("hotelRating") as Image;

                if (supplierItem == null)
                {
                    return;
                }

                BindHotelRating(hotelRating, supplierItem);

                if (facilities != null && supplierItem.SitecoreItem != null)
                {
                    facilities.DataSource = SitecoreFieldsHelper.GetMutiListItems(supplierItem.SitecoreItem, "Facilities");
                    facilities.DataBind();
                }

                if (country != null && supplierItem.SitecoreItem != null)
                {
                    country.Text = SitecoreFieldsHelper.GetLookupFieldTargetItemValue(supplierItem.SitecoreItem, "Country", "CountryName");
                }
                else
                {
                    Literal CountryNameTxtValue = e.Item.FindControl("CountryNameTxt") as Literal;
                    CountryNameTxtValue.Text = supplierItem.Country;
                }

                if (supplierItem.LowestPrice != null)
                {
                    lowestPriceContainer.Visible = true;
                }
                else
                {
                    lowestPriceContainer.Visible = false;
                }

                if (keywords.FlexibleDates)
                {
                    HtmlGenericControl selectRoomsDiv = e.Item.FindControl("SelectRooms") as HtmlGenericControl;
                    if (selectRoomsDiv != null)
                    {
                        selectRoomsDiv.Visible = false;
                    }
                    HtmlGenericControl lowestPriceContainer1 = e.Item.FindControl("LowestPriceContainer") as HtmlGenericControl;
                    lowestPriceContainer1.Visible = false;
                }
                else
                {
                    BindProductDetails(e, supplierItem, productsRepeater);
                }

                BindStarRankings(supplierItem, starRepeater);
            }


            catch (Exception ex)
            {
                Diagnostics.Trace(DiagnosticsCategory.ClientPortal, ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Bind ranking related fields
        /// </summary>
        /// <param name="item">Sitecore item</param>
        private void BindStarRankings(HotelSearchResultItem item, Repeater starRepeater)
        {
            ///Bind star rank
            string ranking = null;
            bool result = false;
            int starRank = 0;
            if (item.SitecoreItem != null)
            {
                ranking = SitecoreFieldsHelper.GetDropLinkFieldValue(item.SitecoreItem, "StarRanking", "Name");
                if (!string.IsNullOrWhiteSpace(ranking))
                {
                    result = Int32.TryParse(ranking, out starRank);
                }
            }
            else
            {
                starRank = Convert.ToInt32(item.StarRanking);
                result = true;
            }
            if (result && starRank > 0)
            {
                //starRank = int.Parse(ranking);
                List<int> listOfStars = Enumerable.Repeat(starRank, starRank).ToList();
                starRepeater.DataSource = listOfStars;
                starRepeater.DataBind();
            }
            else
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal,
                    new AffinionException("Unable to parse star rank:" + item.Name), this);
            }
        }

        /// <summary>
        /// Bind hotel rating
        /// </summary>
        /// <param name="hotelRating">Image field</param>
        /// <param name="item">Hotel rating item</param>
        private static void BindHotelRating(Image hotelRating, HotelSearchResultItem item)
        {

            if (hotelRating != null && item.SitecoreItem != null)
            {
                LinkField ratingImageField = item.SitecoreItem.Fields["RatingImage"];
                if (!string.IsNullOrWhiteSpace(ratingImageField.Url))
                {
                    hotelRating.ImageUrl = ratingImageField.Url;
                }
                else
                {
                    hotelRating.Visible = false;
                }
            }
            if (hotelRating != null && item.SitecoreItem == null)
            {
                hotelRating.ImageUrl = item.IntroductionImage;
            }
        }

        /// <summary>
        /// Bind offer details
        /// </summary>
        /// <param name="e"></param>
        /// <param name="supplierItem"></param>
        /// <param name="productsRepeater"></param>
        /// <param name="item"></param>
        private void BindProductDetails(RepeaterItemEventArgs e, HotelSearchResultItem supplierItem, Repeater productsRepeater)
        {
            if (productsRepeater != null)
            {
                HtmlGenericControl selectRoomsDiv = e.Item.FindControl("SelectRooms") as HtmlGenericControl;
                HtmlGenericControl availableAlertDiv = e.Item.FindControl("AvailableAlert") as HtmlGenericControl;
                //HtmlGenericControl availabilityMessageDiv = e.Item.FindControl("availabilityMessage") as HtmlGenericControl;

                Literal StartingPriceTxtValue = e.Item.FindControl("StartingPrice") as Literal;
                Literal lowestOccupancyType = e.Item.FindControl("LowestOccupancyType") as Literal;
                List<ProviderInfo> roomsList = null;
                roomsList = supplierItem.RoomCollection;
                lowestOccupancyType.Text = "From";
                StartingPriceTxtValue.Text = this.GetCurrencyInformation() + " " + supplierItem.LowestPrice;
                if (roomsList != null)
                {
                    productsRepeater.DataSource = roomsList;
                    productsRepeater.DataBind();
                }
                else if (selectRoomsDiv != null)
                {
                    selectRoomsDiv.Visible = false;
                }
                //SetNoOfferMessage(supplierItem, e);
            }
        }

        /// <summary>
        /// Code for fetching matching rooms
        /// </summary>
        private List<ProviderInfo> LoadRoomsData(object supplierItem)
        {
            List<ProviderInfo> finallist = new List<ProviderInfo>();
            IList<IProduct> products = new List<IProduct>();
            List<PriceAvailabilityByDate> PriceAvailabilityList = new List<PriceAvailabilityByDate>();
            if (supplierItem is BedBankHotel)
            {
                var tmpSupItem = (BedBankHotel)supplierItem;
                foreach (var roomdetails in tmpSupItem.Products)
                {
                    BedBankProduct product = (BedBankProduct)roomdetails;
                    ProviderInfo pr = new ProviderInfo();
                    pr.ProviderID = tmpSupItem.ProviderId;
                    pr.Name = product.Name;
                    pr.Sku = product.Sku;
                    pr.VariatSku = product.VariantSku;
                    pr.Price = product.FinalPrice;
                    pr.ProviderType = ProviderType.BedBanks;
                    pr.RoomAvailability = keywords.NumberOfRooms;
                    pr.NoOfNights = keywords.NoOfNights;
                    pr.PriceCurrency = Convert.ToString(roomdetails.CurrencyId);
                    pr.BookingTokenForBB = product.TokenForBooking;
                    pr.BBMealBasisId = product.MealBasisID;
                    pr.BBMarginId = product.MarginId;
                    pr.BBMarginPercentage = product.MarginPercentage;
                    pr.UComCurrencyId = product.BBCurrencyId;
                    finallist.Add(pr);
                }
            }
            else
            {
                var tmpItem = (Hotel)supplierItem;
                foreach (var tmpResult in commonProviderColl)
                {
                    if (Guid.Parse(tmpItem.SitecoreItemId).ToString() == tmpResult.ProviderId)
                    {
                        foreach (var roomdetails in tmpResult.Products)
                        {
                            products.Add(roomdetails);
                        }
                        PriceAvailabilityList = HotelSearchHelper.GetAvailabilityAndPriceByDate(products,null, keywords.CheckinDate, keywords.CheckinDate, keywords.NoOfNights);
                        if (PriceAvailabilityList != null && PriceAvailabilityList.Count > 0)
                        {
                            foreach (var priceLst in PriceAvailabilityList)
                            {
                                ProviderInfo pr = new ProviderInfo();
                                pr.Price = priceLst.Price;
                                pr.Sku = priceLst.Sku;
                                pr.RoomAvailability = priceLst.Availability;
                                pr.VariatSku = priceLst.VariantSku;
                                pr.ProviderID = tmpItem.ProviderId;
                                pr.ProviderType = ProviderType.LoyaltyBuild;
                                pr.NoOfNights = priceLst.NoOfNights;
                                pr.PriceCurrency = Convert.ToString(products.First(w => w.VariantSku == priceLst.VariantSku).CurrencyId);
                                pr.Name = products.First(w => w.VariantSku == priceLst.VariantSku).Name;
                                finallist.Add(pr);
                            }
                            break;
                        }
                        //else
                        //{
                        //    int i = 0;
                        //    foreach (var roomdetails in tmpResult.Products)
                        //    {
                        //        ProviderInfo pr = new ProviderInfo();
                        //        pr.ProviderID = tmpResult.ProviderId;
                        //        pr.Name = roomdetails.Name;
                        //        pr.Sku = roomdetails.Sku;
                        //        pr.ProviderType = ProviderType.LoyaltyBuild;
                        //        pr.VariatSku = roomdetails.VariantSku;
                        //        if (roomdetails.Price > 0)
                        //        {
                        //            pr.Price = roomdetails.Price;
                        //        }
                        //        else
                        //        {
                        //            pr.Price = 10M + Convert.ToDecimal(i);
                        //        }
                        //        pr.RoomAvailability = 10;
                        //        pr.NoOfNights = keywords.NoOfNights;
                        //        pr.PriceCurrency = this.GetCurrencyInformation();
                        //        finallist.Add(pr);
                        //    }
                        //    break;
                        //}
                    }
                }
            }
            return finallist;
        }

        /// <summary>
        /// Get the given event target or return null if not found.
        /// </summary>
        /// <param name="key">The key to match the event target</param>
        /// <returns>Returns true if event target found</returns>
        private bool GetEventTarget(string key)
        {

            //if (this.IsPostBack && this.Request != null && this.Request.Params["__EVENTTARGET"] != null && this.Request.Params["__EVENTTARGET"].Contains(key))
            if (this.IsPostBack && this.Request != null)
            {
                string eventTarget = this.Request.Params["__EVENTTARGET"];

                if (eventTarget != null && eventTarget.Contains(key))
                {
                    return true;
                }
            }

            return false;
        }


        /// <summary>
        /// Load data to the county drop down list
        /// </summary>
        private void LoadCountyDropDown()
        {
            string client = Sitecore.Context.Item.Parent.Name;
            //get the master database
            Database currentDB = Sitecore.Context.Database;
            //query for the client item in admin portal
            Item clientItem = currentDB.GetItem(string.Concat(Constants.SupplierLocations, client, ""));
            if (clientItem != null)
            {
                //Bind data to dropdown
                Collection<Item> items = SitecoreFieldsHelper.GetMutiListItems(clientItem, "Regions");
                CountyDropDown.DataSource = SitecoreFieldsHelper.GetDropdownDataSource(items, "Name", "ID");
                CountyDropDown.DataTextField = "Text";
                CountyDropDown.DataValueField = "Text";
                CountyDropDown.DataBind();
                //Set the default list item
                //CountyDropDown.Items.Insert(0, Constants.PleaseSelectText);
                CountyDropDown.Items.Insert(0, LanguageReader.GetText(Constants.PleaseSelectDictionaryKey, Constants.PleaseSelectText));


                ///get the URL of the current page
                Uri pageUri = Request.Url;
                QueryStringHelper helper = new QueryStringHelper(pageUri);
                //string selectedLocation = helper.GetValue("Location");
                string selectedLocation = helper.GetValue(Constants.LocationUrlParameter);

                CountyDropDown.SelectedValue = string.IsNullOrWhiteSpace(selectedLocation) ? string.Empty : selectedLocation;

            }
        }
        #endregion

        protected string GetRoomInformation(ProviderInfo roomDetails)
        {
            try
            {
                if (roomDetails == null)
                {
                    return string.Empty;
                }

                string name = roomDetails.VariatSku + "," + roomDetails.PriceCurrency + "," + roomDetails.ProviderID + "," + roomDetails.Name + "," + roomDetails.BookingTokenForBB + "," + roomDetails.BBMarginId + "," + roomDetails.BBMealBasisId + "," + roomDetails.Sku;


                return name;
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, this);
                throw;
            }
        }

        protected void CountyDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.IsPostBack && this.Request != null && !string.IsNullOrEmpty(this.Request.Params["__EVENTTARGET"]) && this.Request.Params["__EVENTTARGET"].Contains("CountyDropDown"))
                {
                    ///get the URL of the current page
                    Uri pageUri = Request.Url;
                    QueryStringHelper helper = new QueryStringHelper(pageUri);

                    string selectedItemText = CountyDropDown.SelectedItem.Text;
                    ///Get the selected index of the drop down, if the value equals default value reset the value.
                    string location = (selectedItemText.Equals(LanguageReader.GetText(Constants.PleaseSelectDictionaryKey, Constants.PleaseSelectText))) ? string.Empty : selectedItemText;

                    ///Append the selected index value to query string and return
                    helper.SetValue(Constants.LocationUrlParameter, location);
                    Response.Redirect(helper.GetUrl().ToString(), true);
                }
            }
            catch (Exception ex)
            {
                Diagnostics.WriteException(DiagnosticsCategory.ClientPortal, ex, null);
                throw;
            }
        }
    }
}

       
