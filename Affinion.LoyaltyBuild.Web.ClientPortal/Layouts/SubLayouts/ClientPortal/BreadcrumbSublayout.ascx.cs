﻿//<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           BreadcrumbSublayout.ascx.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Web.ClientPortal
/// Description:           SubLayout for breadcrumb generation
/// </summary>
namespace Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal
{
    using Affinion.LoyaltyBuild.Common.Utilities;
    using Sitecore.Data.Items;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// SubLayout for breadcrumb generation
    /// </summary>
    public partial class BreadcrumbSublayout : System.Web.UI.UserControl
    {
        // Stores the item of the url referrer
        private Item referrerItem;

        /// <summary>
        /// Code to execute on page load goes here
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The parameters</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Item rootItem = Sitecore.Context.Database.GetItem(Sitecore.Context.Site.RootPath);
            List<Item> breadcrumbItems = new List<Item>();

            // If root item is null there is something wrong in the site configuration
            if(rootItem != null)
            {
                // Some pages like search result page may want to display the referrer page as the breadcrumb root
                Uri referrer = Request.UrlReferrer;
                string localPath = referrer != null ? referrer.LocalPath : string.Empty;
                if (SitecoreFieldsHelper.GetBoolean(Sitecore.Context.Item, "UseReferrerAsBreadcrumbRoot") && !string.IsNullOrEmpty(localPath))
                {
                    referrerItem = Sitecore.Context.Database.GetItem(Sitecore.Context.Site.RootPath + localPath);

                    if (referrerItem != null)
                    {
                        // As long as the referrer belongs to the current site we consider it as the breadcrumb root
                        if (referrerItem.Axes.IsDescendantOf(rootItem))
                        {
                            breadcrumbItems.Insert(0, referrerItem);
                        }

                        if (referrerItem.ID.ToString() != rootItem.ID.ToString())
                        {
                            breadcrumbItems.Insert(0, rootItem);
                        }
                    }
                    else
                    {
                        breadcrumbItems.Insert(0, rootItem);
                    }
                }
                else
                {
                    // Gets the ancestors of the current item
                    breadcrumbItems = GetAncestors(Sitecore.Context.Item, rootItem);
                }
            }

            // bind the breadcrumb data source
            if(breadcrumbItems.Count > 0)
            {
                this.Breadcrumb.DataSource = breadcrumbItems;
                this.Breadcrumb.DataBind();
            }
        }

        /// <summary>
        /// Gets the display text of the breadcrumb item. If "BreadcrumbTitle" is empty it will take the "Title" field
        /// </summary>
        /// <param name="item">The current item</param>
        /// <returns>Returns the display text of breadcrumb item</returns>
        protected string GetBreadcrumbTitle(Item item)
        {
            if(item != null)
            {
                string breadcrumbTitle = SitecoreFieldsHelper.GetValue(item, "BreadcrumbTitle");
                return string.IsNullOrEmpty(breadcrumbTitle) ? SitecoreFieldsHelper.GetValue(item, "Title") : breadcrumbTitle;
            }

            return string.Empty;
        }

        /// <summary>
        /// Get the Url of the breadcrumb item.
        /// </summary>
        /// <param name="item">The current item</param>
        /// <returns>Returns the item url</returns>
        protected string GetBreadcrumbLink(Item item)
        {
            if (item != null)
            {
                // If current item is referrer, we can give you the url with query string.
                // This may help for pages depend on query string values such as search results page
                if(this.referrerItem != null && item.ID.ToString() == this.referrerItem.ID.ToString())
                {
                    return Request.UrlReferrer.PathAndQuery;
                }
                else
                {
                    return SitecoreFieldsHelper.GetUrlOfItem(item).Url;
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Get ancestor items under given root item 
        /// </summary>
        /// <param name="item">The item to get ancestors</param>
        /// <param name="rootItem">The root item of the current site</param>
        /// <returns>Returns a list of ancestor items</returns>
        private static List<Item> GetAncestors(Item item, Item rootItem)
        {
            List<Item> breadcrumbItems = new List<Item>();
            Item[] ancestors = item.Axes.GetAncestors();

            for (int i = ancestors.Length - 1; i >= 0; i--)
            {
                Item currentItem = ancestors[i];

                // add items upto root item
                if (currentItem.ID.ToString() == rootItem.ID.ToString() || SitecoreFieldsHelper.GetBoolean(currentItem, "IsBreadcrumbRoot"))
                {
                    breadcrumbItems.Insert(0, currentItem);
                    break;
                }

                // Pages have option to be excluded from breadcrumb
                if (SitecoreFieldsHelper.GetBoolean(currentItem, "ExcludeFromBreadcrumb"))
                {
                    continue;
                }

                breadcrumbItems.Insert(0, currentItem);
            }

            return breadcrumbItems;
        }
    }
}