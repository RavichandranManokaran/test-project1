﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion Inernational. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           SiteMapHandler.cs
/// Sub-system/Module:     Common(VS project name)
/// Description:           Contains helpers related to SiteMap
/// </summary>

#region Using Directives
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Xml;
#endregion

namespace Affinion.LoyaltyBuild.Common.Utilities
{
    public class SiteMapHandler
    {
        //List of Site map Items
        public Collection<SiteMapParentItem> mapItems { get; set; }
        //Root Path of current Site
        string rootPath = Sitecore.Context.Site.RootPath;
        private string path;
        public string[] stringList { get; set; }

        public SiteMapHandler(string xmlfileaddress)
        {
            List<string> loc = new List<string>();
            using (XmlTextReader reader = new XmlTextReader(xmlfileaddress))
            {
                while (reader.Read())
                {
                    switch (reader.Name)
                    {
                        case "loc":
                            loc.Add(reader.ReadString());
                            break;
                    }
                }
            }

            mapItems = new Collection<SiteMapParentItem>();

            foreach (var item in loc)
            {
                if (!string.IsNullOrWhiteSpace(item))
                {
                    //List<string> itemUrlList = item.Split('/').ToList<string>();         
                    //Url of current item
                    //string itemUrl = item.Replace(itemUrlList[0] + "//", "");
                    //Uri url = new Uri(itemUrl);
                    Uri url = new Uri(item);
                    //host name of current site
                    string authority = url.Authority;

                    string[] items = item.Split('/');

                    //Current Item Name
                    var lastString = items[items.Count() - 1];
                    //stringList = itemUrl.Split('/');
                    stringList = item.Split('/');
                    if (stringList.Length <= (Array.IndexOf(stringList, authority) + 2))
                    {
                        continue;
                    }

                    //parent node of current item
                    var parentNode = stringList[Array.IndexOf(stringList, authority) + 2];
                    
                    var result = mapItems.Where(p => p.Name == parentNode).ToList().Count();

                    if (result == 0)
                    {
                        AddNewParent(parentNode, item, lastString, authority);
                    }
                    else
                    {
                        AddToCurrentParent(parentNode, item, lastString, authority);
                    }
                }
            }
        }

        #region private methods
        /// <summary>
       /// Add New ParentItem to SiteMap
       /// </summary>
       /// <param name="parentNode">Parent of Current Item</param>
       /// <param name="itemUrl">Current Item Url</param>
       /// <param name="lastString">Item Name</param>
        /// <param name="authority">Host Name with port of url</param>
        private void AddNewParent(string parentNode, string itemUrl, string lastString, string authority)        
        {
            SiteMapParentItem sitemapParentItem = new SiteMapParentItem();
            sitemapParentItem.Name = parentNode;
            path = string.Concat(rootPath, "/", sitemapParentItem.Name);
            var parentItem = Sitecore.Context.Database.GetItem(path);
            if (parentItem == null)
                return;

            if (parentItem.TemplateName == "Folder")
            {
                sitemapParentItem.Url = HttpContext.Current.Request.Url.ToString();
            }
            else
            {
                sitemapParentItem.Url = itemUrl;
            }

            SiteMapChildItem child = new SiteMapChildItem();

            if (parentNode != lastString)
            {
                child.Name = stringList[Array.IndexOf(stringList, authority) + 3];
                path = string.Concat(rootPath, "/", sitemapParentItem.Name, "/", child.Name);
                var childItem = Sitecore.Context.Database.GetItem(path);
                if (childItem == null)
                    return;

                if (childItem.TemplateName == "Folder")
                {
                    child.Url = HttpContext.Current.Request.Url.ToString();
                }
                else
                {
                    //child.Url = string.Concat("http://", authority, "/", sitemapParentItem.Name, "/", child.Name);
                    child.Url = itemUrl;
                }
                sitemapParentItem.ChildItems.Add(child);
            }
            mapItems.Add(sitemapParentItem);
        }

        /// <summary>
        /// Add child Items to Existing parents
        /// </summary>
        /// <param name="parentNode">Parent of Current Item</param>
        /// <param name="itemUrl">Current Item Url</param>
        /// <param name="lastString">Item Name</param>
        /// <param name="authority">Host Name with port of url</param>
        private void AddToCurrentParent(string parentNode, string itemUrl, string lastString, string authority)
        {
            var parent = mapItems.Where(pp => pp.Name == parentNode).First();
            SiteMapChildItem child = new SiteMapChildItem();
            if (parentNode != lastString)
            {
                child.Name = stringList[Array.IndexOf(stringList, authority) + 3];
                //child.Url = string.Concat("http://", authority, "/", parent.Name, "/", child.Name);
                child.Url = itemUrl;
                parent.ChildItems.Add(child);
            }
        }
        #endregion
    }

    //Sitemap Parent Item
    public class SiteMapParentItem
    {
        public string Url { get; set; }
        public string Name { get; set; }
        public Collection<SiteMapChildItem> ChildItems { get; set; }

        public SiteMapParentItem()
        {
            ChildItems = new Collection<SiteMapChildItem>();
        }
    }

    //Sitemap Child Item
    public class SiteMapChildItem
    {
        public string Url { get; set; }
        public string Name { get; set; }
    }
}
