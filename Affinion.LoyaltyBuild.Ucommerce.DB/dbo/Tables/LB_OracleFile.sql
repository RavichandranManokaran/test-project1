CREATE TABLE [dbo].[LB_OracleFile](
	[OracleFileId] [int] IDENTITY(1,1) NOT NULL,
	[OracleFileName] [varchar](50) NOT NULL,
	[OracleFileType] [varchar](2) NOT NULL,
	[IsClosedOff] [tinyint] NULL,
	[IsPostedToOracle] [tinyint] NULL,
	[ClosedOutBy] [varchar](80) NULL,
	[ClosedOutDate] [datetime] NULL,
	[SequenceNo] [int] NULL,
 CONSTRAINT [uCommerce_PK_OracleFileId] PRIMARY KEY CLUSTERED 
(
	[OracleFileId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[LB_OracleFile] ADD  CONSTRAINT [DF_uCommerce_OracleFile_IsClosedOff]  DEFAULT ((0)) FOR [IsClosedOff]
GO

ALTER TABLE [dbo].[LB_OracleFile] ADD  CONSTRAINT [DF_uCommerce_OracleFile_IsPostedToOracle]  DEFAULT ((0)) FOR [IsPostedToOracle]
GO

ALTER TABLE [dbo].[LB_OracleFile] ADD  CONSTRAINT [DF_uCommerce_OracleFile_SequenceNo]  DEFAULT ((0)) FOR [SequenceNo]
GO


