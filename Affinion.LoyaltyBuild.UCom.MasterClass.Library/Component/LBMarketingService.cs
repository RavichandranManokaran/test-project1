﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UCommerce.EntitiesV2;
using UCommerce.EntitiesV2.Queries.Marketing;
using UCommerce.Infrastructure;
using UCommerce.Marketing;
using UCommerce.Marketing.Awards;
using UCommerce.Marketing.Awards.AwardResolvers;
using UCommerce.Marketing.TargetingContextAggregators;
using UCommerce.Marketing.Targets;
using UCommerce.Marketing.Targets.TargetResolvers;
using UCommerce.Extensions;
using Affinion.LoyaltyBuild.UCom.Common.Constants;
using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Queries;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Component
{
    public class LBMarketingService : ITargetingService, IDiscountService
    {
        // Fields
        private IDictionary<CampaignItem, IEnumerable<ITarget>> _applyTargets = new Dictionary<CampaignItem, IEnumerable<ITarget>>();
        private IDictionary<CampaignItem, IEnumerable<IAward>> _awards = new Dictionary<CampaignItem, IEnumerable<IAward>>();
        private IDictionary<CampaignItem, IEnumerable<ITarget>> _displayTargets = new Dictionary<CampaignItem, IEnumerable<ITarget>>();

        // Methods
        public LBMarketingService(IRepository<CampaignItem> campaignItemRepository, ITargetingContextAggregator targetingContextAggregator, ITargetAggregator targetAggregator, IAwardAggregator awardAggregator)
        {
            this.CampaignItemRepository = campaignItemRepository;
            this.TargetingContextAggregator = targetingContextAggregator;
            this.TargetAggregator = targetAggregator;
            this.AwardAggregator = awardAggregator;
        }

        public virtual bool Apply(PurchaseOrder purchaseOrder, CampaignItem campaignItem)
        {
            Func<OrderLine, bool> predicate = null;
            Func<IPurchaseOrderTarget, bool> func10 = null;
            Func<IPurchaseOrderTarget, bool> func11 = null;
            bool anyTargetAppliesAwards = campaignItem.AnyTargetAppliesAwards;

            //LB Change : Added to allow Voucher only

            CampaignItemProperty itemProperty = ((CampaignItemProperty)campaignItem[CampaignDataTyeConstant.IsApprove]);
            bool isApproveCampaign = false;
            if (itemProperty != null)
            {
                if (bool.TryParse(itemProperty.Value, out isApproveCampaign))
                {

                }
            }
            if (!isApproveCampaign)
            {
                return false;
            }


            bool allowNextCampaignItem = campaignItem.AllowNextCampaignItems;
            purchaseOrder["_validCodesForDiscounts"] = "";
            IList<ITarget> applyTargets = this.GetApplyTargets(campaignItem);
            IList<IAward> awards = this.GetAwards(campaignItem);
            IList<IPurchaseOrderTarget> pureOrderTargets = this.GetPureOrderTargets(applyTargets);
            if (anyTargetAppliesAwards && applyTargets.OfType<IPurchaseOrderTarget>().Any<IPurchaseOrderTarget>(x => x.IsSatisfiedBy(purchaseOrder)))
            {
                //LB Change : Added Applied Campaing Id to prevent other campaign to applied
                IList<OrderLine> awardedOrderLine = ApplyCampaignOnOrderLine(purchaseOrder, this.IgnoreGeneratedOrderLines(purchaseOrder.OrderLines, allowNextCampaignItem, campaignItem.CampaignItemId), campaignItem.CampaignItemId);
                foreach (IAward award in awards)
                {
                    award.Apply(purchaseOrder, awardedOrderLine);
                }
                return true;
            }
            List<IOrderLineQualifier> lineQualifiers = applyTargets.OfType<IOrderLineQualifier>().ToList<IOrderLineQualifier>();
            if (!applyTargets.Any<ITarget>(x => (x is IOrderLinesTarget)))
            {
                goto Label_0381;
            }
            Func<IOrderLinesTarget, bool> func3 = null;
            Func<IOrderLinesTarget, bool> func4 = null;
            if (predicate == null)
            {
                predicate = x => lineQualifiers.Any<IOrderLineQualifier>(y => y.IsSatisfiedBy(x));
            }
            IList<OrderLine> qualifiedOrderLines = purchaseOrder.OrderLines.Where<OrderLine>(predicate).ToList<OrderLine>();
            IEnumerable<IOrderLinesTarget> source = applyTargets.OfType<IOrderLinesTarget>();
            List<OrderLine> list4 = new List<OrderLine>();
            if (anyTargetAppliesAwards)
            {
                if (func10 == null)
                {
                    func10 = x => x.IsSatisfiedBy(purchaseOrder);
                }
                if (!pureOrderTargets.Any<IPurchaseOrderTarget>(func10))
                {
                    if (func3 == null)
                    {
                        func3 = x => x.IsSatisfiedBy(qualifiedOrderLines);
                    }
                    if (!source.Any<IOrderLinesTarget>(func3))
                    {
                        goto Label_0336;
                    }
                }
                list4.AddRange(qualifiedOrderLines);
            }
            else
            {
                if (func11 == null)
                {
                    func11 = x => x.IsSatisfiedBy(purchaseOrder);
                }
                if (pureOrderTargets.All<IPurchaseOrderTarget>(func11))
                {
                    if (func4 == null)
                    {
                        func4 = x => x.IsSatisfiedBy(qualifiedOrderLines);
                    }
                    if (source.All<IOrderLinesTarget>(func4))
                    {
                        IEnumerable<IOrderLineTarget> enumerable2 = applyTargets.OfType<IOrderLineTarget>().Where<IOrderLineTarget>(delegate(IOrderLineTarget x)
                        {
                            if (x is IOrderLinesTarget)
                            {
                                return !(x is IOrderLineQualifier);
                            }
                            return true;
                        });
                        using (IEnumerator<OrderLine> enumerator2 = qualifiedOrderLines.GetEnumerator())
                        {
                            while (enumerator2.MoveNext())
                            {
                                Func<IOrderLineTarget, bool> func = null;
                                OrderLine qualifiedOrderLine = enumerator2.Current;
                                if (func == null)
                                {
                                    func = x => x.IsSatisfiedBy(qualifiedOrderLine);
                                }
                                if (enumerable2.All<IOrderLineTarget>(func))
                                {
                                    list4.Add(qualifiedOrderLine);
                                }
                            }
                        }
                        if (!list4.Any<OrderLine>())
                        {
                            enumerable2 = from x in applyTargets.OfType<IOrderLineTarget>()
                                          where !(x is IOrderLinesTarget) && !(x is IOrderLineQualifier)
                                          select x;
                            using (IEnumerator<OrderLine> enumerator3 = qualifiedOrderLines.GetEnumerator())
                            {
                                while (enumerator3.MoveNext())
                                {
                                    Func<IOrderLineTarget, bool> func2 = null;
                                    OrderLine qualifiedOrderLine = enumerator3.Current;
                                    if (func2 == null)
                                    {
                                        func2 = x => x.IsSatisfiedBy(qualifiedOrderLine);
                                    }
                                    if (enumerable2.All<IOrderLineTarget>(func2))
                                    {
                                        list4.Add(qualifiedOrderLine);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        Label_0336:
            if (list4.Any<OrderLine>())
            {
                //LB Change : Added Applied Campaing Id to prevent other campaign to applied
                IList<OrderLine> awardedOrderLine = ApplyCampaignOnOrderLine(purchaseOrder, this.IgnoreGeneratedOrderLines(list4, allowNextCampaignItem, campaignItem.CampaignItemId), campaignItem.CampaignItemId);
                foreach (IAward award2 in awards)
                {
                    award2.Apply(purchaseOrder, awardedOrderLine);
                }
                return true;
            }
        Label_0381:
            if (this.IsSatisfiedBy(purchaseOrder, campaignItem) || anyTargetAppliesAwards)
            {
                IList<OrderLine> satisfiedOrderLines = this.GetSatisfiedOrderLines(purchaseOrder, campaignItem);
                if (this.IsSatisfiedBy(satisfiedOrderLines, campaignItem))
                {
                    //LB Change : Added Applied Campaing Id to prevent other campaign to applied
                    satisfiedOrderLines = ApplyCampaignOnOrderLine(purchaseOrder, this.IgnoreGeneratedOrderLines(satisfiedOrderLines, allowNextCampaignItem, campaignItem.CampaignItemId), campaignItem.CampaignItemId);
                    foreach (IAward award3 in awards)
                    {
                        award3.Apply(purchaseOrder, satisfiedOrderLines);
                    }
                    return true;
                }
                List<IOrderLineTarget> list6 = (from x in applyTargets
                                                where (x is IOrderLineTarget) && !(x is IOrderLineQualifier)
                                                select x).Cast<IOrderLineTarget>().ToList<IOrderLineTarget>();
                Dictionary<IOrderLineQualifier, IList<OrderLine>> dictionary = new Dictionary<IOrderLineQualifier, IList<OrderLine>>();
                using (List<IOrderLineQualifier>.Enumerator enumerator6 = lineQualifiers.GetEnumerator())
                {
                    while (enumerator6.MoveNext())
                    {
                        Func<OrderLine, bool> func6 = null;
                        IOrderLineQualifier qualifier = enumerator6.Current;
                        dictionary.Add(qualifier, new List<OrderLine>());
                        if (func6 == null)
                        {
                            func6 = x => qualifier.IsSatisfiedBy(x);
                        }
                        using (List<OrderLine>.Enumerator enumerator7 = purchaseOrder.OrderLines.Where<OrderLine>(func6).ToList<OrderLine>().GetEnumerator())
                        {
                            while (enumerator7.MoveNext())
                            {
                                Func<IOrderLineTarget, bool> func5 = null;
                                OrderLine orderLine = enumerator7.Current;
                                if (func5 == null)
                                {
                                    func5 = x => x.IsSatisfiedBy(orderLine);
                                }
                                int num = list6.Count<IOrderLineTarget>(func5);
                                if (anyTargetAppliesAwards && (num > 0))
                                {
                                    dictionary[qualifier].Add(orderLine);
                                }
                                else if (num == list6.Count)
                                {
                                    dictionary[qualifier].Add(orderLine);
                                }
                            }
                            continue;
                        }
                    }
                }
                int num2 = dictionary.Count<KeyValuePair<IOrderLineQualifier, IList<OrderLine>>>(x => x.Value.Any<OrderLine>());
                if (lineQualifiers.Any<IOrderLineQualifier>() && ((num2 == lineQualifiers.Count<IOrderLineQualifier>()) || ((num2 > 0) && anyTargetAppliesAwards)))
                {
                    foreach (IAward award4 in awards)
                    {
                        var data = ((from x in dictionary select x.Value).Distinct());
                        //TODO: Add custome para in orderline
                        //  award4.Apply(purchaseOrder, this.IgnoreGeneratedOrderLines(data));
                        //  award4.Apply(purchaseOrder, this.IgnoreGeneratedOrderLines((from x in dictionary select x.Value).Distinct<OrderLine>().ToList<OrderLine>()));
                    }
                    return true;
                }
            }
            if (applyTargets.Any<ITarget>() && applyTargets.All<ITarget>(x => (x is IOrderLineTarget)))
            {
                bool flag2 = false;
                List<IOrderLineTarget> list8 = applyTargets.OfType<IOrderLineTarget>().ToList<IOrderLineTarget>();
                using (IEnumerator<OrderLine> enumerator9 = this.IgnoreGeneratedOrderLines(purchaseOrder.OrderLines, allowNextCampaignItem, campaignItem.CampaignItemId).GetEnumerator())
                {
                    while (enumerator9.MoveNext())
                    {
                        Func<IOrderLineTarget, bool> func7 = null;
                        Func<IOrderLineTarget, bool> func8 = null;
                        OrderLine orderLine = enumerator9.Current;
                        if (list8.Any<IOrderLineTarget>())
                        {
                            if (func7 == null)
                            {
                                func7 = x => x.IsSatisfiedBy(orderLine);
                            }
                            if (!list8.All<IOrderLineTarget>(func7))
                            {
                                if (!anyTargetAppliesAwards)
                                {
                                    continue;
                                }
                                if (func8 == null)
                                {
                                    func8 = x => x.IsSatisfiedBy(orderLine);
                                }
                                if (!list8.Any<IOrderLineTarget>(func8))
                                {
                                    continue;
                                }
                            }

                            //LB Change : Added Applied Campaing Id to prevent other campaign to applied
                            IList<OrderLine> awardedOrderLine = ApplyCampaignOnOrderLine(purchaseOrder, new List<OrderLine> { orderLine }, campaignItem.CampaignItemId);
                            foreach (IAward award5 in awards)
                            {

                                award5.Apply(purchaseOrder, awardedOrderLine);
                            }
                            flag2 = true;
                        }
                    }
                }
                if (flag2)
                {
                    return flag2;
                }
            }
            if ((this.OrderLevelTargetsOnly(applyTargets) || anyTargetAppliesAwards) && this.IsSatisfiedBy(purchaseOrder, campaignItem))
            {
                //LB Change : Added Applied Campaing Id to prevent other campaign to applied
                IList<OrderLine> awardedOrderLine = ApplyCampaignOnOrderLine(purchaseOrder, this.IgnoreGeneratedOrderLines(purchaseOrder.OrderLines, allowNextCampaignItem, campaignItem.CampaignItemId), campaignItem.CampaignItemId);
                foreach (IAward award6 in awards)
                {
                    award6.Apply(purchaseOrder, awardedOrderLine);
                }
                return true;
            }
            return false;
        }


        private IList<OrderLine> ApplyCampaignOnOrderLine(PurchaseOrder purchaseOrder, IList<OrderLine> orderlines, int campaignId)
        {
            IList<OrderLine> orderLinesUpdate = new List<OrderLine>();
            if (orderlines != null && orderlines.Any())
            {
                foreach (var item in orderlines)
                {
                    int index = item.OrderProperties.ToList().FindIndex(x => x.Key == OrderPropertyConstants.AppliedCampaign);
                    if (index == -1)
                    {
                        item.OrderProperties.Add(new OrderProperty() { Key = OrderPropertyConstants.AppliedCampaign, Value = campaignId.ToString(), Order = purchaseOrder, OrderLine = item });
                    }
                    else
                    {
                        item.OrderProperties.ElementAt(index).Value = campaignId.ToString();
                    }
                }
            }
            return orderlines;
        }

        public virtual PurchaseOrder ApplyAwards(PurchaseOrder purchaseOrder)
        {
            while (purchaseOrder.Discounts.Count > 0)
            {
                Discount discount = purchaseOrder.Discounts.First<Discount>();
                purchaseOrder.RemoveDiscount(discount);
            }


            List<int> appliedCampaign = new List<int>();
            foreach (var item in purchaseOrder.OrderLines)
            {
                int appliedCampaignId = 0;
                if (item[OrderPropertyConstants.UserSelectedCampaign] != null && int.TryParse(item[OrderPropertyConstants.UserSelectedCampaign], out appliedCampaignId))
                {
                    if (!appliedCampaign.Any(y => y == appliedCampaignId))
                        appliedCampaign.Add(appliedCampaignId);
                }
            }
            List<CampaignItem> activeCampaignItems = new List<CampaignItem>();
            if (appliedCampaign != null && appliedCampaign.Any())
            {
                activeCampaignItems = CampaignItem.All().Where(x => appliedCampaign.Contains(x.CampaignItemId)).ToList();
            }
            IList<CampaignItem> getVourcherCampaign = this.GetActiveCampaignItems();

            if (purchaseOrder.ProductCatalogGroup != null)
            {
                activeCampaignItems = activeCampaignItems.Where<CampaignItem>(delegate(CampaignItem x)
                {
                    if (!x.Campaign.ProductCatalogGroups.Contains(purchaseOrder.ProductCatalogGroup))
                    {
                        return !x.Campaign.ProductCatalogGroups.Any<ProductCatalogGroup>();
                    }
                    return true;
                }).ToList<CampaignItem>();
            }

            //Added to get only voucher campaign to apply
            if (purchaseOrder.ProductCatalogGroup != null)
            {
                getVourcherCampaign = getVourcherCampaign.Where<CampaignItem>(delegate(CampaignItem x)
                {
                    IList<ITarget> targetList = this.TargetAggregator.GetApplyTargets(x);
                    foreach (var item in targetList)
                    {
                        if (item is VoucherTarget && x.Campaign.ProductCatalogGroups.Contains(purchaseOrder.ProductCatalogGroup))
                        {
                            return true;
                        }
                    }
                    //if (!x.Campaign.ProductCatalogGroups.Contains(purchaseOrder.ProductCatalogGroup))
                    //{
                    //    return !x.Campaign.ProductCatalogGroups.Any<ProductCatalogGroup>();
                    //}

                    return false;
                    //return true;
                }).ToList<CampaignItem>();
            }
            activeCampaignItems.AddRange(getVourcherCampaign);

            foreach (CampaignItem item in activeCampaignItems)
            {
                this.Apply(purchaseOrder, item);
            }
            return purchaseOrder;
        }

        public virtual IList<CampaignItem> GetActiveCampaignItems()
        {
            return this.CampaignItemRepository.Select(new ActiveCampaignItemsQuery()).ToList<CampaignItem>();
        }

        public virtual IList<CampaignItem> GetActiveCampaignItemsByDate(DateTime startDate, DateTime endDate)
        {
            return this.CampaignItemRepository.Select(new LBActiveCampaignItemsQuery(startDate, endDate)).ToList<CampaignItem>();
        }
        public virtual IList<ITarget> GetApplyTargets(CampaignItem campaignItem)
        {
            if (!this._applyTargets.ContainsKey(campaignItem))
            {
                this._applyTargets.Add(campaignItem, this.TargetAggregator.GetApplyTargets(campaignItem));
            }
            return this._applyTargets[campaignItem].ToList<ITarget>();
        }

        public virtual IList<IAward> GetAwards(CampaignItem campaignItem)
        {
            if (!this._awards.ContainsKey(campaignItem))
            {
                this._awards.Add(campaignItem, this.AwardAggregator.GetAwards(campaignItem));
            }
            return this._awards[campaignItem].ToList<IAward>();
        }

        public virtual IList<ITarget> GetDisplayTargets(CampaignItem campaignItem)
        {
            if (!this._displayTargets.ContainsKey(campaignItem))
            {
                this._displayTargets.Add(campaignItem, this.TargetAggregator.GetDisplayTargets(campaignItem));
            }
            return this._displayTargets[campaignItem].ToList<ITarget>();
        }

        private IList<IPurchaseOrderTarget> GetPureOrderTargets(IEnumerable<ITarget> applyTargets)
        {
            return (from x in applyTargets
                    where (((x is IPurchaseOrderTarget) && !(x is IOrderLinesTarget)) && (!(x is IOrderLineTarget) && !(x is IOrderLineQualifier))) && x.EnabledForApply
                    select x).Cast<IPurchaseOrderTarget>().ToList<IPurchaseOrderTarget>();
        }

        private IList<OrderLine> GetSatisfiedOrderLines(PurchaseOrder purchaseOrder, CampaignItem campaignItem)
        {
            List<IOrderLineTarget> list = (from x in this.GetApplyTargets(campaignItem).OfType<IOrderLineTarget>()
                                           where x.EnabledForApply
                                           select x).ToList<IOrderLineTarget>();
            List<OrderLine> source = new List<OrderLine>();
            bool anyTargetAppliesAwards = campaignItem.AnyTargetAppliesAwards;
            foreach (OrderLine line in purchaseOrder.OrderLines)
            {
                bool flag2 = true;
                foreach (IOrderLineTarget target in list)
                {
                    bool flag3 = target.IsSatisfiedBy(line);
                    if (flag3 && anyTargetAppliesAwards)
                    {
                        flag2 = true;
                        break;
                    }
                    if (!flag3 && !anyTargetAppliesAwards)
                    {
                        flag2 = false;
                        break;
                    }
                    flag2 = !anyTargetAppliesAwards;
                }
                if (flag2)
                {
                    source.Add(line);
                }
            }
            return source.Distinct<OrderLine>().ToList<OrderLine>();
        }

        public virtual IList<CampaignItem> GetTargetedCampaignItems()
        {
            IList<CampaignItem> activeCampaignItems = this.GetActiveCampaignItems();
            TargetingContext targetingContext = this.TargetingContextAggregator.Populate();
            return (from x in activeCampaignItems.Where<CampaignItem>(delegate(CampaignItem x)
                    {
                        if (x.Campaign.ProductCatalogGroups.Any<ProductCatalogGroup>())
                        {
                            return targetingContext.ProductCatalogGroups.Any<ProductCatalogGroup>(y => x.Campaign.ProductCatalogGroups.Contains(y));
                        }
                        return true;
                    })
                    where this.IsSatisfiedBy(targetingContext, x)
                    select x).ToList<CampaignItem>();
        }

        private IList<OrderLine> IgnoreGeneratedOrderLines(ICollection<OrderLine> orderLines, bool allowNextCampaignItem, int campaignItemId)
        {
            List<OrderLine> appliedOrderLine = (from x in orderLines
                                                where !x.IsGenerated()
                                                select x).ToList<OrderLine>();

            //LB Change : Added to allow voucher & Specific Campaign 
            appliedOrderLine = appliedOrderLine.Where(x => allowNextCampaignItem || ((x[OrderPropertyConstants.ForSearch] != null && x[OrderPropertyConstants.ForSearch] == "1") || x[OrderPropertyConstants.AppliedCampaign] == campaignItemId.ToString() || x[OrderPropertyConstants.UserSelectedCampaign] == campaignItemId.ToString() || x[OrderPropertyConstants.OrderLinRestrictionCampaign] == campaignItemId.ToString())).ToList();
            return appliedOrderLine;
        }

        public virtual bool IsSatisfiedBy(TargetingContext targetingContext, CampaignItem campaignItem)
        {
            bool flag = !campaignItem.AnyTargetAdvertises;
            IList<ITarget> displayTargets = this.GetDisplayTargets(campaignItem);
            if (!displayTargets.Any<ITarget>())
            {
                return false;
            }
            if (!flag)
            {
                return displayTargets.Cast<IDisplayTarget>().Any<IDisplayTarget>(target => target.IsSatisfiedBy(targetingContext));
            }
            foreach (IDisplayTarget target in displayTargets.Cast<IDisplayTarget>())
            {
                if (!target.IsSatisfiedBy(targetingContext))
                {
                    return false;
                }
            }
            return true;
        }

        public virtual bool IsSatisfiedBy(IEnumerable<OrderLine> orderLines, CampaignItem campaignItem)
        {
            if (!orderLines.Any<OrderLine>())
            {
                return false;
            }
            List<ITarget> source = (from x in this.GetApplyTargets(campaignItem)
                                    where (x is IOrderLineTarget) && x.EnabledForApply
                                    select x).ToList<ITarget>();
            if (!source.Any<ITarget>())
            {
                return false;
            }
            bool anyTargetAppliesAwards = campaignItem.AnyTargetAppliesAwards;
            using (List<ITarget>.Enumerator enumerator = source.GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    Func<OrderLine, bool> predicate = null;
                    IOrderLineTarget orderLineAwardTarget = (IOrderLineTarget)enumerator.Current;
                    if (predicate == null)
                    {
                        predicate = x => !orderLineAwardTarget.IsSatisfiedBy(x);
                    }
                    bool flag2 = !orderLines.Any<OrderLine>(predicate);
                    if (flag2 && anyTargetAppliesAwards)
                    {
                        return true;
                    }
                    if (!flag2 && !anyTargetAppliesAwards)
                    {
                        return false;
                    }
                }
            }
            return !anyTargetAppliesAwards;
        }

        public virtual bool IsSatisfiedBy(PurchaseOrder purchaseOrder, CampaignItem campaignItem)
        {
            foreach (IPurchaseOrderTarget target in (from x in this.GetApplyTargets(campaignItem)
                                                     where (x is IPurchaseOrderTarget) && x.EnabledForApply
                                                     select x).ToList<ITarget>())
            {
                bool flag = target.IsSatisfiedBy(purchaseOrder);
                if (flag && campaignItem.AnyTargetAppliesAwards)
                {
                    return true;
                }
                if (!flag && !campaignItem.AnyTargetAppliesAwards)
                {
                    return false;
                }
            }
            return !campaignItem.AnyTargetAppliesAwards;
        }

        private bool OrderLevelTargetsOnly(IEnumerable<ITarget> applyTargets)
        {
            Guard.Against.NullArgument<IEnumerable<ITarget>>(applyTargets);
            return (applyTargets.Count<ITarget>(x => ((x is IPurchaseOrderTarget) && x.EnabledForApply)) == applyTargets.Count<ITarget>());
        }

        // Properties
        private IAwardAggregator AwardAggregator { get; set; }

        private IRepository<CampaignItem> CampaignItemRepository { get; set; }

        private ITargetAggregator TargetAggregator { get; set; }

        private ITargetingContextAggregator TargetingContextAggregator { get; set; }
    }




}
