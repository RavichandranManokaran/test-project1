﻿CREATE TABLE [dbo].[LB_FixPriceAward] (
    [FixPriceAwardId] INT             NOT NULL,
    [AmountOff]       DECIMAL (18, 2) NOT NULL,
    CONSTRAINT [PK_uCommerce_FixPriceAward] PRIMARY KEY CLUSTERED ([FixPriceAwardId] ASC),
    CONSTRAINT [FK_uCommerce_FixPriceAward_uCommerce_Award] FOREIGN KEY ([FixPriceAwardId]) REFERENCES [dbo].[uCommerce_Award] ([AwardId])
);

