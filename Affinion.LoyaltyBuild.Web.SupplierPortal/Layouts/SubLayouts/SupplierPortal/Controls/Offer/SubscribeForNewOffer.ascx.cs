﻿using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Affinion.LoyaltyBuild.Web.SupplierPortal.Layouts.SubLayouts.SupplierPortal.Controls.Offer
{
    public partial class SubscribeForNewOffer : System.Web.UI.UserControl
    {
        public int SupplierInviteId;

        public int CampaignItemId;

        public CampaignDetail Detail { get; set; }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                txtContent.Text = Detail.Content;               
            }
            this.DataBind();
        }
    }
}