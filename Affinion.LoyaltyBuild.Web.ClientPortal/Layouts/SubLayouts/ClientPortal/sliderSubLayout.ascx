﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SliderSubLayout.ascx.cs" Inherits="Affinion.LoyaltyBuild.Web.ClientPortal.Layouts.SubLayouts.ClientPortal.SliderSubLayout" %>
<%@ Register TagPrefix="sc" Namespace="Sitecore.Web.UI.WebControls" Assembly="Sitecore.Kernel" %>
<%@ Import Namespace="Sitecore.Data.Items" %>

<link href="/Resources/styles/jquery.bxslider.css" rel="stylesheet" />

<script src="/Resources/scripts/jquery.bxslider.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('.bxslider').bxSlider({ auto: true, autoControls: true });
    });
</script>

<div class="width-full">
    <div class="row slider margin-common">
        <div class="col-md-12">
            <div class="slider-top">
                <div id="bb1" class="LBranding">
                    <ul class="bxslider">

                        <asp:Repeater ID="SliderReapeater" runat="server">
                            <ItemTemplate>
                                <li>
                                    <sc:Image Field="Slid_Image" CssClass="img-responsive img-enlarge" Item="<%#(Item)Container.DataItem %>" ID="Image" runat="server" />
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>

                    </ul>
                    <div class="container container-inner-relative">
                        <sc:Placeholder ID="searchbox" Key="searchbox" ClientIDMode="Static" runat="server" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
