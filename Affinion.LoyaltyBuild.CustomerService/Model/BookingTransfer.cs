﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model
{
    public class BookingTransfer
    {
        /// <summary>
        /// Orderlinid of booking
        /// </summary>
        public int OrderLineId { get; set; }

        /// <summary>
        /// Createdon Field
        /// </summary>

        public DateTime CreatedOn { get; set; }

        /// <summary>
        /// ProviderId 
        /// </summary>
        public int ProviderId { get; set; }

        /// <summary>
        /// ProviderGuid
        /// </summary>

        public string ProviderGuid { get; set; }
    }


}