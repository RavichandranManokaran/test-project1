﻿CREATE PROCEDURE [dbo].[LB_GetPreviousDayBooking]
as
begin
SELECT 
   Value as [Provider Name],
   count(Value) as 'Booking(s)'
FROM
     uCommerce_OrderProperty AS OP
Join
     LB_OrderLineStatusAudit AS OS
On OP.OrderLineId = OS.OrderLineId
WHERE [KEY]  = '_clientid' and
     CAST(OS.CreatedOn AS DATE)=CAST(getdate()-1 as date)
     and (OS.StatusId = 6 or -- If the booking is confirmed
              OS.StatusId = 5) --If the booking is provisonal
Group By
     Value
end