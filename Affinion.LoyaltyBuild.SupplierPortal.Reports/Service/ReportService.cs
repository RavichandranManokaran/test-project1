﻿using Affinion.LoyaltyBuild.SupplierPortal.Reports.Comparers;
using Affinion.LoyaltyBuild.SupplierPortal.Reports.Data;
using Affinion.LoyaltyBuild.SupplierPortal.Reports.Filters;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Affinion.LoyaltyBuild.SupplierPortal.Reports.Service
{
    /// <summary>
    /// Report service to get report data
    /// </summary>
    public class ReportService : IReportService
    {
        /// <summary>
        /// Get arrival report
        /// </summary>
        /// <param name="ArrivalReportFilter"></param>
        /// <returns>List of booking</returns>
        public ArrivalReportCollection GetArrivalReport(ArrivalReportFilter arrivalReportFilter)
        {
            var collection = new ArrivalReportCollection();

            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create("uCommerce");

            using (DbCommand cmd = db.GetStoredProcCommand("LB_GetSupplierPortal_ArrivalReportData"))
            {
                db.AddInParameter(cmd, "@OrderLineId", DbType.String, arrivalReportFilter.BookingReference);
                db.AddInParameter(cmd, "@ClientName", DbType.String, arrivalReportFilter.ClientName);
                db.AddInParameter(cmd, "@DateFrom", DbType.DateTime, arrivalReportFilter.DateFrom);
                db.AddInParameter(cmd, "@OfferName", DbType.Int32, arrivalReportFilter.OfferId);
                db.AddInParameter(cmd, "@DateTo", DbType.DateTime, arrivalReportFilter.DateTo);
                db.AddInParameter(cmd, "@Provider", DbType.Int32, arrivalReportFilter.Provider);
                db.AddInParameter(cmd, "@ReportandOrderby", DbType.String, arrivalReportFilter.ReportOrderBy);
                db.AddInParameter(cmd, "@bookingstatus", DbType.Int32, arrivalReportFilter.BookingStatus);

                using (var reader = db.ExecuteReader(cmd))
                {

                    while (reader.Read())
                    {
                        var arrival = new ArrivalReport()
                        {
                            BookingReference = reader.Value<string>(0),
                            OfferName = reader.Value<string>(1),
                            CurrencyId = reader.NullValue<int>(2),
                            CollectAmount = GetNullableDecimal(reader, 3),
                            FirstName = reader.Value<string>(4),
                            LastName = reader.Value<string>(5),
                            Line1 = reader.Value<string>(6),
                            Line2 = reader.Value<string>(7),
                            City = reader.Value<string>(8),
                            Region = reader.Value<string>(9),
                            Country = reader.Value<string>(10),
                            MobilePhoneNumber = reader.Value<string>(11),
                            PhoneNumber = reader.Value<string>(12),
                            Arrival = GetNullableDate(reader, 13),
                            Departure = GetNullableDate(reader, 14),
                            NumberOfAdults = reader.NullValue<int>(15),
                            NumberOfChildren = reader.NullValue<int>(16),
                            ChildAge = reader.Value<string>(17),
                            ReservationDate = GetNullableDate(reader, 18),
                            BookingThrough = reader.Value<string>(19),
                            BookingStatus = reader.Value<string>(20),
                            BookingStatusDate = GetNullableDate(reader, 21),
                            Accommodation = reader.Value<string>(22),
                            Acknowledgement = reader.Value<string>(23),
                            RowColor = reader.Value<string>(24),
                            HotelName = reader.Value<string>(25),
                            Note = reader.Value<string>(26)
                        };
                        collection.Add(arrival);
                    }
                }
            }

            return collection;
        }

        /// <summary>
        /// Get restricted rooms report 
        /// </summary>
        /// <param name="RestrictedRoomsFilter"></param>        
        /// <returns>List of restricted rooms</returns>
        public RestrictedRoomsCollection GetRestrictedRooms(RestrictedRoomsFilter restrictedRoomsFilter)
        {
            var restrictedRoomsData = new List<RestrictedRoomsData>();

            var collection = new RestrictedRoomsCollection();
            var DistinctItems = new RestrictedRoomsCollection();


            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create("uCommerce");

            using (DbCommand cmd = db.GetStoredProcCommand("LB_SupplierPortal_RestrictedRoom"))
            {
                db.AddInParameter(cmd, "DateFrom", DbType.DateTime, restrictedRoomsFilter.DateFrom);
                db.AddInParameter(cmd, "DateTo", DbType.DateTime, restrictedRoomsFilter.DateTo);
                db.AddInParameter(cmd, "Provider", DbType.Int32, restrictedRoomsFilter.ProviderId);

                using (var reader = db.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        restrictedRoomsData.Add(new RestrictedRoomsData()
                        {
                            HotelId = reader.Value<int>(0),
                            HotelName = reader.Value<string>(1),
                            OfferId = reader.Value<int>(2),
                            OfferName = reader.Value<string>(3),
                            Date = reader.Value<DateTime>(4),
                            FirstDate = new DateTime(reader.Value<DateTime>(4).Year, reader.Value<DateTime>(4).Month, 1)
                        });
                    }
                }
            }

            //convert the data to report format
            var data = restrictedRoomsData.GroupBy(i => new { i.HotelId, i.OfferId, i.FirstDate }).Select(
                item => item.First()).ToList();

            foreach (var item in data)
            {
                collection.Add(new RestrictedRoomsReport
                {
                    HotelName = item.HotelName,
                    OfferName = item.OfferName,
                    HotelId = item.HotelId,
                    OfferId = item.OfferId,
                    Month = item.FirstDate,
                    DatesRestricted = string.Join(", ", restrictedRoomsData.Where(i => (i.OfferId == item.OfferId) && (i.FirstDate == item.FirstDate) && (i.HotelId == item.HotelId))
                    .Select(i => i.Date.Day))
                });
            }

            return collection;
        }


        /// <summary>
        /// Get availability report 
        /// </summary>
        /// <param name="AvailabilityReportFilter"></param>
        /// <returns>List of available rooms</returns> 
        public AvailabilityCollection GetAvailability(AvailabilityReportFilter availabilityReportFilter)
        {

            var availabiltyData = new List<AvailabilityData>();

            var collection = new AvailabilityCollection();

            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create("uCommerce");

            using (DbCommand cmd = db.GetStoredProcCommand("LB_GetSupplierPortal_AvailabilityReportData"))
            {
                db.AddInParameter(cmd, "DateFrom", DbType.DateTime, availabilityReportFilter.DateFrom);
                db.AddInParameter(cmd, "DateTo", DbType.DateTime, availabilityReportFilter.DateTo);
                db.AddInParameter(cmd, "Provider", DbType.Int32, availabilityReportFilter.ProviderId);

                using (var reader = db.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        availabiltyData.Add(new AvailabilityData()
                        {
                            HotelId = reader.Value<int>(0),
                            HotelName = reader.Value<string>(1),
                            RoomType = reader.Value<string>(2),
                            Date = reader.Value<DateTime>(3),
                            IsClosedOut = reader.Value<bool>(4),
                            IsSoldOut = reader.Value<bool>(5),
                            FirstDate = new DateTime(reader.Value<DateTime>(3).Year, reader.Value<DateTime>(3).Month, 1)
                        });
                    }
                }
            }

            //convert the data to report format
            var data = availabiltyData.GroupBy(i => new { i.HotelId, i.RoomType, i.FirstDate }).Select(
               item => item.First()).ToList();


            foreach (var item in data)
            {
                collection.Add(new AvailabilityReprot
                {
                    HotelId = item.HotelId,
                    HotelName = item.HotelName,
                    RoomType = item.RoomType,
                    Month = item.FirstDate,
                    ClosedOutDates = string.Join(", ", availabiltyData.Where(i => i.IsClosedOut && (i.RoomType == item.RoomType) && (i.FirstDate == item.FirstDate) && (i.HotelId == item.HotelId))
                        .Select(i => i.Date.Day)),
                    SoldOutDates = string.Join(", ", availabiltyData.Where(i => i.IsSoldOut && (i.RoomType == item.RoomType) && (i.FirstDate == item.FirstDate) && (i.HotelId == item.HotelId))
                        .Select(i => i.Date.Day))
                });
            }

            //return collection
            return collection;
        }

        /// <summary>
        /// Get Revenue report 
        /// </summary>
        /// <param name="RevenueReportFilter"></param>
        /// <returns>List of Revenue report for total revenue</returns>
        public RevenueReportCollection GetRevenueReport(RevenueReportFilter revenueReportFilter)
        {
            var collection = new RevenueReportCollection();

            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create("uCommerce");

            using (DbCommand cmd = db.GetStoredProcCommand("LB_Supplierportal_RevenueReport"))
            {
                db.AddInParameter(cmd, "DateFrom", DbType.DateTime, revenueReportFilter.DateFrom);
                db.AddInParameter(cmd, "DateTo", DbType.DateTime, revenueReportFilter.DateTo);
                db.AddInParameter(cmd, "Partner", DbType.String, revenueReportFilter.Partner);
                db.AddInParameter(cmd, "OfferName", DbType.Int32, revenueReportFilter.OfferName);
                db.AddInParameter(cmd, "Provider", DbType.Int32, revenueReportFilter.Provider);
                db.AddInParameter(cmd, "ProviderGroup", DbType.Int32, revenueReportFilter.ProviderGroup);
                db.AddInParameter(cmd, "BreakdownbyCampaign", DbType.Boolean, revenueReportFilter.BreakdownbyCampaign);
                db.AddInParameter(cmd, "DateType", DbType.String, revenueReportFilter.DateType);
                db.AddInParameter(cmd, "Breakdown", DbType.String, revenueReportFilter.Breakdown);

                using (var reader = db.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        collection.Add(new RevenueReport()
                        {
                            OfferName = reader.Value<string>(0),
                            Date = GetNullableDate(reader, 1),
                            NumberOfBookings = GetNullableInt(reader, 2),
                            TotalRevenue = GetNullableInt(reader, 3),
                        });
                    }
                }
            }

            return collection;
        }

        /// <summary>
        /// Get Revenue report
        /// </summary>
        /// <param name="RevenueReportFilter"></param>
        /// <returns>List of Revenue report for List Revenue</returns>
        public RevenueReportbyProviderCollection GetRevenueReportbyProvider(RevenueReportFilter revenueReportFilter)
        {
            var collection = new RevenueReportbyProviderCollection();


            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create("uCommerce");

            using (DbCommand cmd = db.GetStoredProcCommand("LB_Supplierportal_RevenueReportByProvider"))
            {
                db.AddInParameter(cmd, "@DateFrom", DbType.DateTime, revenueReportFilter.DateFrom);
                db.AddInParameter(cmd, "@DateTo", DbType.DateTime, revenueReportFilter.DateTo);
                db.AddInParameter(cmd, "@Partner", DbType.String, revenueReportFilter.Partner);
                db.AddInParameter(cmd, "@OfferName", DbType.Int32, revenueReportFilter.OfferName);
                db.AddInParameter(cmd, "@Provider", DbType.Int32, revenueReportFilter.Provider);
                db.AddInParameter(cmd, "@ProviderGroup", DbType.Int32, revenueReportFilter.ProviderGroup);
                db.AddInParameter(cmd, "@BreakdownbyCampaign", DbType.Boolean, revenueReportFilter.BreakdownbyCampaign);
                db.AddInParameter(cmd, "@DateType", DbType.String, revenueReportFilter.DateType);
                db.AddInParameter(cmd, "@Breakdown", DbType.String, revenueReportFilter.Breakdown);

                using (var reader = db.ExecuteReader(cmd))
                {
                    while (reader.Read())
                    {
                        collection.Add(new RevenueReportbyProvider()
                        {
                            HotelName = reader.Value<string>(0),
                            OfferName = reader.Value<string>(1),
                            Date = GetNullableDate(reader, 2),
                            NumberOfBookings = GetNullableInt(reader, 3),
                            TotalRevenue = GetNullableInt(reader, 4)
                        });
                    }
                }
            }

            return collection;
        }

        #region Private methods

        /// <summary>
        /// get the reader object for the stored procedure
        /// </summary>
        /// <param name="procName">procedure name</param>
        /// <param name="param">list of parameters</param>
        /// <returns>reader object</returns>
        private IDataReader GetReader(string procname, List<object> param = null)
        {
            DatabaseProviderFactory factory = new DatabaseProviderFactory();
            Database db = factory.Create("uCommerce");

            DbCommand cmd = db.GetStoredProcCommand(procname);
            if (param != null)
                cmd.Parameters.AddRange(param.ToArray());

            return cmd.ExecuteReader();
        }
        private int? GetNullableInt(IDataReader reader, int position)
        {
            if (reader.IsDBNull(position))
                return null;
            else
                return reader.Value<int>(position);
        }

        private DateTime? GetNullableDate(IDataReader reader, int position)
        {
            if (reader.IsDBNull(position))
                return null;
            else
                return reader.Value<DateTime>(position);
        }

        private Decimal? GetNullableDecimal(IDataReader reader, int position)
        {
            if (reader.IsDBNull(position))
                return null;
            else
                return reader.Value<decimal>(position);
        }
        #endregion
        
    }

    internal static class ExtensionMethods
    {
        public static Nullable<T> NullValue<T>(this IDataReader reader, string columnName)
            where T : struct
        {
            if (reader[columnName] == DBNull.Value)
                return null;

            var t = typeof(T);
            t = Nullable.GetUnderlyingType(t) ?? t;

            return (T)Convert.ChangeType(reader[columnName], t);
        }

        public static Nullable<T> NullValue<T>(this IDataReader reader, int column)
            where T : struct
        {
            if (reader[column] == DBNull.Value)
                return null;

            var t = typeof(T);
            t = Nullable.GetUnderlyingType(t) ?? t;

            return (T)Convert.ChangeType(reader[column], t);
        }

        public static T Value<T>(this IDataReader reader, string columnName)
        {
            var t = typeof(T);
            t = Nullable.GetUnderlyingType(t) ?? t;

            if (reader[columnName] == DBNull.Value)
                return default(T);

            return (T)Convert.ChangeType(reader[columnName], t);
        }

        public static T Value<T>(this IDataReader reader, int column)
        {
            var t = typeof(T);
            t = Nullable.GetUnderlyingType(t) ?? t;

            if (reader[column] == DBNull.Value)
                return default(T);

            return (T)Convert.ChangeType(reader[column], t);
        }
    }
}
