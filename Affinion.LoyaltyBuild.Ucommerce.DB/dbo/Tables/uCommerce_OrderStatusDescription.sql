﻿CREATE TABLE [dbo].[uCommerce_OrderStatusDescription] (
    [OrderStatusId] INT            NOT NULL,
    [DisplayName]   NVARCHAR (128) NOT NULL,
    [Description]   NTEXT          NULL,
    [CultureCode]   NVARCHAR (60)  NOT NULL,
    CONSTRAINT [uCommerce_PK_OrderStatusDescription] PRIMARY KEY CLUSTERED ([OrderStatusId] ASC),
    CONSTRAINT [uCommerce_FK_OrderStatusDescription_OrderStatus] FOREIGN KEY ([OrderStatusId]) REFERENCES [dbo].[uCommerce_OrderStatus] ([OrderStatusId])
);

