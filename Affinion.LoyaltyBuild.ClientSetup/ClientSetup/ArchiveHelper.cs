﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           GenericHelpers.cs
/// Sub-system/Module:     ClientSetup(VS project name)
/// Description:           GenericHelpers for uCommerce Initalization
/// </summary>

#region Using Directives
using Sitecore.Data;
using Sitecore.Workflows.Simple;
using System;
using Affinion.LoyaltyBuild.Common.Instrumentation;
using Affinion.LoyaltyBuild.Common.Utilities;
#endregion

namespace Affinion.LoyaltyBuild.ClientSetup.ClientSetup
{
    /// <summary>
    /// Use to archive selected item via WF
    /// </summary>
    public class ArchiveHelper
    {
        /// <summary>
        /// Its a pipeline function
        /// </summary>
        public void Process(WorkflowPipelineArgs args)
        {
            try
            {
                //Sitecore.Data.Items.Item item = Sitecore.Context.Item;

                Sitecore.Diagnostics.Assert.IsNotNull(args.DataItem, "item");

                // Get the archive from master database
                Sitecore.Data.Archiving.Archive archive = Sitecore.Data.Archiving.ArchiveManager.GetArchive("archive", args.DataItem.Database);

                using (new Sitecore.Security.Accounts.UserSwitcher(ItemHelper.ContentEditingUser))
                {

                    if (archive != null)
                    {
                        // archive the item
                        archive.ArchiveItem(args.DataItem);
                        // to archive an individual version instead: archive.ArchiveVersion(child);
                    }
                    else
                    {
                        // recycle the item
                        // no need to check settings and existence of archive
                        args.DataItem.Recycle();
                        // to bypass the recycle bin: item.Delete();
                        // to recycle an individual version: item.RecycleVersion();
                        // to bypass the recycle bin for a version: item.Versions.RemoveVersion();
                    } 
                }

            }
            catch (Exception e)
            {
                Diagnostics.WriteException(DiagnosticsCategory.Common, e, null);
                throw;
            }
        }
    }
}
