﻿
using ucomm = UCommerce.EntitiesV2;
using data = Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
using System;
using System.Data;
/*
© 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
PROPRIETARY INFORMATION The information contained herein (the 
'Proprietary Information') is highly confidential and proprietary to and 
constitutes trade secrets of Affinion International. The Proprietary Information 
is for Affinion International use only and shall not be published, 
communicated, disclosed or divulged to any person, firm, corporation or 
other legal entity, directly or indirectly, without the prior written 
consent of Affinion International.
*/
namespace Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Helpers
{
    /// <summary>
    /// Class for all extension methods used by the libraries
    /// </summary>
    internal static class ExtensionMethods
    {
        /// <summary>
        /// Convert ucommerce purchase order entity to local entity
        /// </summary>
        /// <param name="order">ucommerce purchase order</param>
        /// <returns>local purchase order</returns>
        public static data.PurchaseOrder ToLocalData(this ucomm.PurchaseOrder order)
        {
            return new data.PurchaseOrder
            {
                OrderId = order.OrderId,
                OrderNumber = order.OrderNumber
            };
        }

        /// <summary>
        /// Convert ucommerce customer entity to local entity
        /// </summary>
        /// <param name="customer">ucommerce customer</param>
        /// <returns>local customer</returns>
        public static data.Customer ToLocalData(this ucomm.Customer customer)
        {
            return new data.Customer
            {
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                CustomerId = customer.Id
            };
        }

        /// <summary>
        /// Convert ucommerce purchase order entity to local entity
        /// </summary>
        /// <param name="orderLine">ucommerce order line</param>
        /// <returns>local order line</returns>
        public static data.Booking ToLocalData(this ucomm.OrderLine orderLine)
        {
            return new data.Booking
            {
                
            };
        }

    }
}
