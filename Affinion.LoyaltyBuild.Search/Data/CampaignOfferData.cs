﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion International. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           CampaignOfferData.cs
/// Sub-system/Module:     Affinion.LoyaltyBuild.Search
/// Description:           Stores campaign offer acts.
/// </summary>
namespace Affinion.LoyaltyBuild.Search.Data
{
    using Affinion.LoyaltyBuild.UCom.MasterClass.BusinessLogic.Model;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Stores campaign offer acts.
    /// </summary>
    public class CampaignOfferData
    {
        /// <summary>
        /// Gets or sets the offer name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the arrival date
        /// </summary>
        public List<CampaginItemDateRange> ArrivalDates { get; set; }

        /// <summary>
        /// Gets or sets the campaign start date
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the campaign end date
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating weather the campaign is enabled
        /// </summary>
        public bool Enabled { get; set; }

        public int CampaignItemId { get; set; }

        /// <summary>
        /// No of nights for offer to be applicable.
        /// </summary>
        public ICollection<int> NoOfNights { get; set; }

        public CampaignOfferData()
        {
            this.ArrivalDates = new List<CampaginItemDateRange>();
            this.NoOfNights = new List<int>();
        }

    }
}
