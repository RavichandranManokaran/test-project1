﻿CREATE TABLE [dbo].[uCommerce_ShippingMethodsTarget] (
    [ShippingMethodsTargetId] INT            NOT NULL,
    [ShippingMethodsIdsList]  NVARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([ShippingMethodsTargetId] ASC)
);

