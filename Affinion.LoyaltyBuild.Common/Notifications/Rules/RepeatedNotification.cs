﻿///<summary>
/// © 2015 Affinion International. All rights reserved. CONFIDENTIAL AND 
/// PROPRIETARY INFORMATION The information contained herein (the 
/// 'Proprietary Information') is highly confidential and proprietary to and 
/// constitutes trade secrets of Affinion Inernational. The Proprietary Information 
/// is for Affinion International use only and shall not be published, 
/// communicated, disclosed or divulged to any person, firm, corporation or 
/// other legal entity, directly or indirectly, without the prior written 
/// consent of Affinion International.
///
/// Source File:           RepeatedNotification.cs
/// Sub-system/Module:     Client Setup(VS project name)
/// Description:           Code is used to return rule conditions related results.


#region Using Directives
using Affinion.LoyaltyBuild.Common.Instrumentation;
using System;
using System.Web;
#endregion

namespace Affinion.LoyaltyBuild.Common.Notifications.Rules
{
    class RepeatedNotification: INotificfationRule
    {
        //Get the current HttpContext
        HttpContext context = HttpContext.Current;
        
        #region Public Methods

        /// <summary>
        ///  This method is implemented from INotification rule. Processes the conditions for the RepeatedNotification.
        /// </summary>
        /// <returns>rule result</returns>
        public bool ExecuteDisplayRule()
        {
            try
            {
                bool loopResult = true;
                if (context.Session["LoopedNotification"] == null)
                {
                    RepeatNotification();
                    return true;
                }
                else
                {
                    loopResult = NotificationDisplayed();
                    return loopResult = true ? true : false;
                    
                }
            }
            catch (Exception ex)
            {
                //log the error and return an empty string
                Diagnostics.WriteException(DiagnosticsCategory.Common, ex, this);
                throw;
            }
        }
        
        #endregion

        #region Private Methods
        /// <summary>
        /// Assign the initial value to the session as 1.
        /// </summary>
        private void RepeatNotification()
        {
            context.Session["LoopedNotification"] = 1;
        }

        /// <summary>
        /// Check for the value of the session and returns the respective result.
        /// </summary>
        private bool NotificationDisplayed()
        {
            int loopNumber = (int)(context.Session["LoopedNotification"]);
            if ((loopNumber >= 2) && (loopNumber <= 3))
            {
                loopNumber = loopNumber++;
                context.Session["LoopedNotification"] = loopNumber;
                return true;
            }
            else 
            {
                return false;
            }           
        }

        #endregion
    }
}
